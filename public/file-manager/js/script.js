var show_list;
var sort_type = "alphabetic";
var order = "asc";
var rootDir = undefined;
var gdrive_search = '';
var working_dir = '';

// ======================
// ==  Navbar actions  ==
// ======================

$('#nav-buttons a').click(function (e) {
  e.preventDefault();
});

$('#to-previous').click(function () {
  var previous_dir = getPreviousDir();
  goTo(previous_dir);
});
$('#gdrive-folder-previous-button').on('click', function () {
  var previous_dir = getPreviousDir();
  selectGdriveFolderLoad(previous_dir);
});
$('#gdrive-file-previous-button').on('click', function () {
  var previous_dir = getPreviousDir();
  selectGdriveFileLoad(previous_dir);
});

$('.gdrive-add-folder').click(function () {
  bootbox.prompt(lang['message-name'], function (result) {
    if (result == null) return;
    createFolder(result);
  });
});

$('.gdrive-upload').click(function () {
  $('#uploadModal').modal('show');
});
$(document).on('click', '#send-gdrive-file-as-attachment-button', function(e){
    $('.send-gdrive-file-attach-model-body').removeClass('hide');
});

$(document).on('submit', '#send-gdrive-file-attach-form', function(event) {
	event.preventDefault();
	
	var $this = $(this);
	var msgElement = $('#send-gdrive-file-attach_msg');
    var formData = new FormData($(this)[0]);
    var actionUrl =  $(this).attr('action');
	
	$.ajax({
        url: actionUrl,
        type: 'POST',              
        data: formData,
        dataType: 'json',
        processData: false,
	    contentType: false,
	    cache : false,
	    beforeSend: function() {
	        $('#lfm-loader').show();
	    },
	    success: function(result){
            $('#lfm-loader').hide();
            $('.send-gdrive-file-attach-model-body').addClass('hide');
	        if(result.status == true){
                notify(result.message);
	            $($this)[0].reset();
	            // setTimeout(function(){
	            //     $('#shareModal').modal('hide');
	            // }, 1000);
            }else{
                notify(result.message);
            }
	    },
	    error: function(error){
            $('#lfm-loader').hide();
            notify('Somthing want wrong!');	        
	        console.log({error});
	    }
	});
});

$(document).on('click', '.button-open-share-model-gdrive-folder', function (e) {
    $('#shareModal').modal('show');
    var basename = $(this).data('id');
    var username = $(this).data('username');
    var type = $(this).data('type');
    var url = 'https://drive.google.com/drive/u/2/folders/'+basename;
    $('.send-gdrive-file-attach-model-body').addClass('hide');

    if(type == 'file'){

        var mime_type = $(this).data('mime_type');
        var filename = $(this).data('filename');
        $("#send-gdrive-file-attach-input-file_basename").val(basename);
        $("#send-gdrive-file-attach-input-filename").val(filename);
        $("#send-gdrive-file-attach-input-mime_type").val(mime_type);
        $("#send-gdrive-file-as-attachment-button").removeClass('hide');
        
        
        url = 'https://drive.google.com/file/d/'+basename;        
    }else{
        $("#send-gdrive-file-as-attachment-button").addClass('hide');
    }
    $("#gdrive-share-link").val(url);
    var mailurl = "mailto: ?subject=Ordnerfreigabe von "+ username +"&body=Hi, sieh dir mal diesen Ordner an: "+url;
    $('#share-gdrive-folder-mail-button').attr('href', mailurl);

});
$(document).on('click', '#share-gdrive-folder-copy-button', function (e) {
    lfmCopyText('gdrive-share-link');
});
function lfmCopyText(elementId) {
    /* Get the text field */
    var copyText = document.getElementById(elementId);
  
    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/
  
    /* Copy the text inside the text field */
    document.execCommand("copy");
}

$('#upload-btn').click(function () {
  $(this).html('')
    .append($('<i>').addClass('fa fa-refresh fa-spin'))
    .append(" " + lang['btn-uploading'])
    .addClass('disabled');

  function resetUploadForm() {
    $('#uploadModal').modal('hide');
    $('#upload-btn').html(lang['btn-upload']).removeClass('disabled');
    $('input#upload').val('');
  }

  $('#uploadForm').ajaxSubmit({
    success: function (data, statusText, xhr, $form) {
      resetUploadForm();
      refreshFoldersAndItems(data);
      displaySuccessMessage(data);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      displayErrorResponse(jqXHR);
      resetUploadForm();
    }
  });
});

$('#thumbnail-display').click(function () {
  show_list = 0;
  loadItems();
});

$('#list-display').click(function () {
  show_list = 1;
  loadItems();
});

$("#list-sort-alphabetic").click(function() {
  sort_type = "alphabetic";
  order = "asc";
  loadItems();
});
$("#list-sort-desc-alphabetic").click(function() {
  sort_type = "alphabetic";
  order = "desc";
  loadItems();
});

$(".search_form").submit(function(e) {
  e.preventDefault();
  gdrive_search = $("#search").val();
  loadItems();
});


$('#list-sort-time').click(function() {
  sort_type = 'time';
  loadItems();
});

// ======================
// ==  Folder actions  ==
// ======================

$(document).on('click', '.file-item', function (e) {
  useFile($(this).data('id'));
});

$(document).on('click', '.folder-item', function (e) {
  $("#search").val('');
  gdrive_search = "";
  goTo($(this).data('id'));
});

function goTo(new_dir) {
  // required for file upload.
  $('#working_dir').val(new_dir);
  working_dir = new_dir;
  loadItems();
}

function getPreviousDir() {
  var ds = '/';
  var last_ds = working_dir.lastIndexOf(ds);
  var previous_dir = working_dir.substring(0, last_ds);
  return previous_dir;
}
function showPriviousButton(){
  return (rootDir == working_dir);
}

function dir_starts_with(str) {
  return working_dir.indexOf(str) === 0;
}

function setOpenFolders() {
  var folders = $('.folder-item');

  for (var i = folders.length - 1; i >= 0; i--) {
    // close folders that are not parent
    if (! dir_starts_with($(folders[i]).data('id'))) {
      $(folders[i]).children('i').removeClass('fa-folder-open').addClass('fa-folder');
    } else {
      $(folders[i]).children('i').removeClass('fa-folder').addClass('fa-folder-open');
    }
  }
}

// ====================
// ==  Ajax actions  ==
// ====================

function performLfmRequest(url, parameter, type, requestType) {
  var data = defaultParameters();

  $('#lfm-loader').show();
  if (parameter != null) {
    $.each(parameter, function (key, value) {
      data[key] = value;
    });
  }
  data['_token'] = $('meta[name="csrf-token"]').attr('content');
  requestType = typeof requestType !== 'undefined' ? requestType : 'GET';
  return $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    type: requestType,
    dataType: type || 'text',
    url: lfm_route + '/' + url,
    data: data,
    cache: false
  })
  .always(function(){
    $('#lfm-loader').hide();
  })
  .fail(function (jqXHR, textStatus, errorThrown) {
    displayErrorResponse(jqXHR);
  });
}

function displayErrorResponse(jqXHR) {
  notify('<div style="max-height:50vh;overflow: scroll;">' + jqXHR.responseText + '</div>');
}

function displaySuccessMessage(data){
  if(data == 'OK'){
    var success = $('<div>').addClass('alert alert-success')
      .append($('<i>').addClass('fa fa-check'))
      .append(' File Uploaded Successfully.');
    $('#alerts').append(success);
    setTimeout(function () {
      success.remove();
    }, 2000);
  }
}

var refreshFoldersAndItems = function (data) {
  loadFolders();
  if (data != 'OK') {
    data = Array.isArray(data) ? data.join('<br/>') : data;
    notify(data);
  }
};

var hideNavAndShowEditor = function (data) {
  $('#nav-buttons > ul').addClass('hidden');
  $('#content').html(data);
}

function loadFolders() {
  performLfmRequest('folders', {}, 'html')
    .done(function (data) {
      $('#tree').html(data);
      loadItems();
    });
}
function selectGdriveFolderLoad(dir){
  $('.preloader').show();
  $.ajax({
    type: 'GET',
    dataType: 'html',
    url: lfm_route + '/list-for-select-gdrive-folder/' +'?working_dir='+dir,
    cache: false
  }).fail(function (jqXHR, textStatus, errorThrown) {
    $('.preloader').hide();
  }).done(function (data) {
    var data = JSON.parse(data);
    $("#select-gdrive-folder-content").html(data.html);
    working_dir = data.working_dir;
    if(rootDir == undefined){ rootDir = data.working_dir;}
    if (showPriviousButton()) {
      $('#gdrive-folder-previous-button').addClass('hide');
      } else {
      $('#gdrive-folder-previous-button').removeClass('hide');      
    }
    $('.preloader').hide();
  });
  
}
function selectGdriveFileLoad(dir, currentActiveTabName=''){
  $('.preloader').show();
  $.ajax({
    type: 'GET',
    dataType: 'html',
    url: lfm_route + '/list-for-select-gdrive-file/' +'?working_dir='+dir+'&current_active_tab='+currentActiveTabName,
    cache: false
  }).fail(function (jqXHR, textStatus, errorThrown) {
    $('.preloader').hide();
  }).done(function (data) {
    var data = JSON.parse(data);
    $("#select-gdrive-file-content").html(data.html);
    working_dir = data.working_dir;
    if(rootDir == undefined){ rootDir = data.working_dir;}
    if (showPriviousButton()) {
      $('#gdrive-file-previous-button').addClass('hide');
      } else {
      $('#gdrive-file-previous-button').removeClass('hide');      
    }
    $('.preloader').hide();
  });
  
}


function loadDirectoryFiles(dir) {
  $('.preloader').show();
	$('#viewFileModal').modal('show');
  $.ajax({
    type: 'GET',
    dataType: 'html',
    url: lfm_route +'/view-files/'+'?working_dir='+dir,
    cache: false
  }).fail(function (jqXHR, textStatus, errorThrown) {
    // console.log(jqXHR);
    $('.preloader').hide();
  }).done(function (data) {
		$("#view-gdrive-file-content").html(data);
    $('.preloader').hide();
  });
}
$('.listFilesForDelete').on('click', function(e){
  $('#deleteFileModal').modal('show');
  $('.preloader').show();
	var id = $(this).attr('data-id');
  var type = $(this).attr('data-type');
  var property_id = $(this).attr('data-property_id')
  $.ajax({
    type: 'GET',
    dataType: 'html',
    url: lfm_route +'/list-files-for-delete/'+'?property_id='+property_id+'&type='+type+'&id='+id,
    cache: false
  }).fail(function (jqXHR, textStatus, errorThrown) {
    $('.preloader').hide();
  }).done(function (data) {
    var data = JSON.parse(data);
		$("#delete-gdrive-file-content").html(data.html);
    $('.preloader').hide();
  });
});


$('body').on('click','.deleteGdriveFileLink',function(){
	if (confirm("Are you sure want to delete this?")){
    objData = $(this).attr('data-objData');
    var id = $(this).attr('data-id');
    var objData = JSON.parse(objData);
    objData.id= id;
    console.log(objData);
		$this = $(this);
        $.ajax({
            type : 'POST',
            url : lfm_route +'/gdrive-link-delete',
            data : objData,
            success : function (data) {
				      $this.closest('tr').remove();
            }
        });
    }
})
function gdriveFolderSelectDir(basename, dirname){
  var type = $("#select-gdrive-folder-type").val();
  var callback = $("#select-gdrive-folder-callback").val();
  window[callback](basename, dirname);
}
function gdriveFileSelectDir(basename, dirname, curElement){
  var type = $("#select-gdrive-file-type").val();
  var callback = $("#select-gdrive-file-callback").val();
  window[callback](basename, dirname,curElement, 'dir');
}
function gdriveFileSelectFile(basename, dirname, curElement){
  var type = $("#select-gdrive-file-type").val();
  var callback = $("#select-gdrive-file-callback").val();
  window[callback](basename, dirname,curElement, 'file');
}

function loadItems() {
  performLfmRequest('jsonitems', {show_list: show_list, sort_type: sort_type, order: order, gdrive_search: gdrive_search }, 'html')
    .done(function (data) {
      var response = JSON.parse(data);
      $('#content').html(response.html);
      $('#nav-buttons > ul').removeClass('hidden');
      working_dir = response.working_dir;
      $('#current_dir').text(response.working_dir);
      if(rootDir === undefined){ rootDir = response.working_dir;}
      if (showPriviousButton()) {
        $('#to-previous').addClass('hide');
      } else {
        $('#to-previous').removeClass('hide');
      }
      setOpenFolders();
    });
    
}

function createFolder(folder_name) {
  performLfmRequest('newfolder', {name: folder_name})
    .done(refreshFoldersAndItems);
}

function rename(path, dirname, old_name, extension) {
  bootbox.prompt({
    title: lang['message-rename'],
    value: old_name,
    callback: function (result) {
      if (result == null) return;
      performLfmRequest('rename', {
        path: path,
        dirname: dirname,
        new_name: result,
        extension: extension
      }, 'text', 'POST').done(refreshFoldersAndItems);
    }
  });
}
function renameDir(path, dirname, old_name) {
  bootbox.prompt({
    title: lang['message-rename'],
    value: old_name,
    callback: function (result) {
      if (result == null) return;
      performLfmRequest('renameDir', {
        path: path,
        dirname: dirname,
        new_name: result
      }, 'text', 'POST').done(refreshFoldersAndItems);
    }
  });
}

function trash(item_path) {
  bootbox.confirm(lang['message-delete'], function (result) {
    if (result == true) {
      performLfmRequest('delete', {delete_path: item_path}, 'text', 'POST')
        .done(refreshFoldersAndItems);
    }
  });
}
function trashDir(item_path) {
  bootbox.confirm(lang['message-delete'], function (result) {
    if (result == true) {
      performLfmRequest('deleteDir', {delete_path: item_path}, 'text', 'POST')
        .done(refreshFoldersAndItems);
    }
  });
}

function cropImage(image_name) {
  performLfmRequest('crop', {img: image_name})
    .done(hideNavAndShowEditor);
}

function resizeImage(image_name) {
  performLfmRequest('resize', {img: image_name})
    .done(hideNavAndShowEditor);
}

function download(file_name, file_dir) {
  var data = defaultParameters();
  data['file'] = file_name;
  data['file_dir'] = file_dir;
  // location.href = lfm_route + '/download?' + $.param(data);
  // console.log(lfm_route + '/download?' + $.param(data));
  window.open(lfm_route + '/download?' + $.param(data),'_blank');
}

// ==================================
// ==  Ckeditor, Bootbox, preview  ==
// ==================================

function useFile(file_url) {

  function getUrlParam(paramName) {
    var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
    var match = window.location.search.match(reParam);
    return ( match && match.length > 1 ) ? match[1] : null;
  }

  function useTinymce3(url) {
    var win = tinyMCEPopup.getWindowArg("window");
    win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = url;
    if (typeof(win.ImageDialog) != "undefined") {
      // Update image dimensions
      if (win.ImageDialog.getImageData) {
        win.ImageDialog.getImageData();
      }

      // Preview if necessary
      if (win.ImageDialog.showPreviewImage) {
        win.ImageDialog.showPreviewImage(url);
      }
    }
    tinyMCEPopup.close();
  }

  function useTinymce4AndColorbox(url, field_name) {
    parent.document.getElementById(field_name).value = url;

    if(typeof parent.tinyMCE !== "undefined") {
      parent.tinyMCE.activeEditor.windowManager.close();
    }
    if(typeof parent.$.fn.colorbox !== "undefined") {
      parent.$.fn.colorbox.close();
    }
  }

  function useCkeditor3(url) {
    if (window.opener) {
      // Popup
      window.opener.CKEDITOR.tools.callFunction(getUrlParam('CKEditorFuncNum'), url);
    } else {
      // Modal (in iframe)
      parent.CKEDITOR.tools.callFunction(getUrlParam('CKEditorFuncNum'), url);
      parent.CKEDITOR.tools.callFunction(getUrlParam('CKEditorCleanUpFuncNum'));
    }
  }

  function useFckeditor2(url) {
    var p = url;
    var w = data['Properties']['Width'];
    var h = data['Properties']['Height'];
    window.opener.SetUrl(p,w,h);
  }

  var url = file_url;
  var field_name = getUrlParam('field_name');
  var is_ckeditor = getUrlParam('CKEditor');
  var is_fcke = typeof data != 'undefined' && data['Properties']['Width'] != '';
  var file_path = url.replace(route_prefix, '');

  if (window.opener || window.tinyMCEPopup || field_name || getUrlParam('CKEditorCleanUpFuncNum') || is_ckeditor) {
    if (window.tinyMCEPopup) { // use TinyMCE > 3.0 integration method
      useTinymce3(url);
    } else if (field_name) {   // tinymce 4 and colorbox
      useTinymce4AndColorbox(url, field_name);
    } else if(is_ckeditor) {   // use CKEditor 3.0 + integration method
      useCkeditor3(url);
    } else if (is_fcke) {      // use FCKEditor 2.0 integration method
      useFckeditor2(url);
    } else {                   // standalone button or other situations
      window.opener.SetUrl(url, file_path);
    }

    if (window.opener) {
      window.close();
    }
  } else {
    // No editor found, open/download file using browser's default method
    window.open(url);
  }
}
//end useFile

function defaultParameters() {
  return {
    working_dir: working_dir,
    type: $('#type').val()
  };
}

function notImp() {
  notify('Not yet implemented!');
}

function notify(message) {
  bootbox.alert(message);
}

function fileView(file_url, timestamp) {
  bootbox.dialog({
    title: lang['title-view'],
    message: $('<img>')
      .addClass('img img-responsive center-block')
      .attr('src', file_url + '?timestamp=' + timestamp),
    size: 'large',
    onEscape: true,
    backdrop: true
  });
}


// permission 

$(document).on('click', '.user_permission_select_all', function(){
    $('.for_all_user_permission').prop('checked', this.checked);
});
$(document).on('click', '.for_all_user_permission', function(){
    if(!this.checked){
        $('.user_permission_select_all').prop('checked', false);
    }
});

$(document).on('submit', "#permission_role", function() {
    event.preventDefault();
    var roles = [];
    $('input[name="user_permission"]:checked').each(function() {
            roles.push($(this).val());
    });
    var $form = $( this ),
    url = $form.attr( 'action' );
    $.ajax({
        url: url,
        type: "POST",
        data: { basename: $('#basename').val(),user_role:roles},
        success: function(res) {
            $('#roleModal').modal('toggle');
            return false;
        }
    });
});
// $(document).on('click', ".permission", function() {
//     var basename = $(this).data("id")
//     $(".basename").val(basename)
//     $('#roleModal').modal('show');

//     $.ajax({
//         url : base_route +'/get_permission',
//         type: "GET",
//         data: { id: basename},
//         success: function(res) {
//             var role = [];
//             $.each(res.data, function (index, value) {
//                 role.push(value.user_role);
//             });

//             for(var i=0; i<role.length; i++) { 
//                 role[i] = parseInt(role[i], 10); 
//             }

//             var common = $.grep(res.static_role, function(element) {
//                     return $.inArray(element, role ) !== -1;
//             });

//             $.each(common, function (index, value) {
//                 $(".chk_role.role"+value).prop("checked", false);
//             });
//         }
//     });

// });
$(document).on('click', ".permission", function() {
    var basename = $(this).data("id");
    var file_name = $(this).data("file_name");
    var icon = $(this).data("icon");
    $(".basename").val(basename)
    $('#roleModal').modal('show');
    $("#gdrive_permission_file_name").html('<i class="fa ' + icon + ' fa-fw"></i>' + file_name)

    $.ajax({
        url : base_route +'/get_User_Permission',
        type: "GET",
        data: { id: basename},
        success: function(res) {
            var role = [];
            $.each(res.data, function (index, value) {
                $('#gdrive_permission_for_user_'+value.user_id).prop("checked", false);
                $('.user_permission_select_all').prop("checked", false);
            });
        }
    });

});