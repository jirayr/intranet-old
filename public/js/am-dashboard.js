var load_title; var load_table; var load_url; var table_obj;

$('body').on('click', '.load-table', function() {
    load_title = $(this).attr('data-title');
    load_table = $(this).attr('data-table');
    load_url = $(this).attr('data-url');

    loadTable();
});
function tableDetail(table){
    var data = [];

    if(table == 'table-property-invoice'){
            data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date-time" },null,null,null,null,null,null];
            data['position'] = 5;
            data['order'] = 'desc';
    }
    else if(table == 'table-property-invoice-am'){
            data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,{ "type": "new-date-time" },null,null,null,null,null];
            data['position'] = 5;
            data['order'] = 'desc';
    }
    else if(table == 'table-contract-release'){
        data['column'] = [null,null,null,{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "new-date" }, { "type": "new-date" }, { "type": "new-date" },null,null,{ "type": "new-date-time" },null,null,null,null,null,null];
        data['position'] = 10;
        data['order'] = 'desc';
    }else if(table == 'table-approval-building-insurance' || table == 'table-release-liability'){
      data['column'] = [null,null,{ "type": "numeric-comma" },null,null,null,null,null];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'list-properties-1'){
      data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'table-property-invoice2' || table == 'table-property-invoice1'){
      data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date" },null,null,null];
      data['position'] = 8;
      data['order'] = 'desc';
    }else if(table == 'property-insurance-table'){
      data['column'] = [null,null,null,{ "type": "numeric-comma" },null,null,{"type": "new-date-time"},null,null,null,null,null,null, null, null];
      data['position'] = 5;
      data['order'] = 'desc';
    }else if(table == 'table-property-management-approval'){
      data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null,null,null];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'property-provision-table'){
      data['column'] = [null,null,null,{ "type": "numeric-comma", "class": "text-right" },null,{ "type": "numeric-comma", "class": "text-right" }, { "type": "numeric-comma", "class": "text-right" },{ "type": "numeric-comma", "class": "text-right" },{ "type": "numeric-comma", "class": "text-right" },null];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'added-month'){
      data['column'] = [null,null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" }];
      data['position'] = 0;
      data['order'] = 'asc';
      data['length'] = 50;
    }else if(table == 'list-statusloi'){
      data['column'] = [null, { "type": "numeric-comma" }, { "type": "new-date" }];
      data['position'] = 2;
      data['order'] = 'desc';
    }else if(table == 'dashboard-table-2'){
      data['column'] = [null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'rental-overview-table'){
      data['column'] = [null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'property-default-payer-table'){
      data['column'] = [null,null,null,null,{ "type": "new-date-time" },null,null,null,null,{ "type": "new-date-time" },null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 4;
      data['order'] = 'asc';
    }else if(table == 'bank-properties'){
      data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'financing-per-bank-table'){
      data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 1;
      data['order'] = 'asc';
    }else if(table == 'insurance-table1' || table == 'insurance-table0'){
      data['column'] = [null,{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 2;
      data['order'] = 'desc';
    }else if(table == 'hausmax_table'){
      data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 2;
      data['order'] = 'desc';
    }else if(table == 'mahnung-properties'){
      data['column'] = [null,null,{ "type": "numeric-comma" }];
      data['position'] = 2;
      data['order'] = 'desc';
    }else if(table == 'expired-rental-agreement-table'){
      data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null];
      data['position'] = 4;
      data['order'] = 'asc';
    }else if(table == 'tenant-table4'){
      data['column'] = [{"orderable": false},null,null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null,null];
      data['position'] = 5;
      data['order'] = 'desc';
    }else if(table == 'rental_activity_table'){
      data['column'] = [null,null,null,null,{ "type": "numeric-comma" },null,null,null,null,null,null,null];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'expiring_leases_table'){
      data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null];
      data['position'] = 4;
      data['order'] = 'asc';
    }else if(table == 'expiring-building-insurance-table' || table == 'expiring-liability-insurance-table'){
      data['column'] = [null,null,null,null,{ "type": "numeric-comma" },{ "type": "new-date" },null,{ "type": "numeric-comma" }];
      data['position'] = 5;
      data['order'] = 'asc';
    }else if(table == 'vacancy-per-object-table'){
      data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 4;
      data['order'] = 'desc';
    }else if(table == 'recommendation-table'){
      data['column'] = [null,null,null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }, { "type": "numeric-comma" }, { "type": "numeric-comma" }, { "type": "numeric-comma" }, null, { "type": "new-date" }, null, null, null];
      data['position'] = 14;
      data['order'] = 'desc';
    }else if(table == 'standard-land-value-table'){
      data['column'] = [null,null,null,{ "type": "numeric-comma" }, { "type": "numeric-comma" }];
      data['position'] = 4;
      data['order'] = 'desc';
    }else if(table == 'bka-table'){
      data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'property-mail-table'){
      data['column'] = [null,null,null,null,{ "type": "new-date-time" },null];
      data['position'] = 4;
      data['order'] = 'desc';
    }else if(table == 'liquiplanung-table'){
      data['column'] = [null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "numeric-comma" }];
      data['position'] = 0;
      data['order'] = 'asc';
    }else if(table == 'table-open-angebote'){
      data['column'] = [null,null,{ "type": "numeric-comma" },null,null,null,{ "type": "new-date-time" },null,null,null,null,null];
      data['position'] = 6;
      data['order'] = 'desc';
    }else if(table == 'invoice-table-0' || table == 'invoice-table-1' || table == 'invoice-table-2' || table == 'invoice-table-3' || table == 'invoice-table-4'){
      data['column'] = [null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date" },null,null,null,null,null,null];
      data['position'] = 8;
      data['order'] = 'desc';
    }
    return data;
}
function loadTable(reload = false){

    if(!reload){
        $('#table-data-modal').find('.modal-title').text(load_title);
        $('#table-data-modal').modal('show');
    }

    $.ajax({
      url: load_url,
      type: 'GET',
      beforeSend: function() {
        // if(!reload){
            $('#table-data-modal').find('.modal-body').html('<div class="loading"></div>');
        // }
      },
      success: function(result) {
        $('#table-data-modal').find('.modal-body').html(result);
        if(load_table){
            var tableElement = $('#table-data-modal').find('#'+load_table);
            var tableSetting = tableDetail(load_table);

          console.log({load_table});

          if(load_table=="property-insurance-table")
            tableElement = ".property-insurance-table";

          if(load_table == "table-open-angebote")
            tableElement = ".table-open-angebote";

          if(load_table=="table-approval-building-insurance")
            tableElement = ".table-approval-building-insurance";

          if(load_table=="table-release-liability")
            tableElement = ".table-release-liability";

          if(load_table=="table-property-management-approval")
            tableElement = ".table-property-management-approval";



          var page_length = (typeof tableSetting['length'] !== 'undefined' && tableSetting['length'] != 0 && tableSetting['length'] != '') ? tableSetting['length'] : 10;

            if(typeof tableSetting['column'] !== 'undefined' && tableSetting['column'].length){
                table_obj = makeDatatable(tableElement,tableSetting['column'], tableSetting['order'], tableSetting['position'], page_length);
            }else{
                table_obj = makeDatatable(tableElement);
            }
        }

        if ($('.contract-comment').length)
            $('.contract-comment').editable();
        if ($('.invoice-comment').length)
          $('.invoice-comment').editable();
        if ($('.angebote-comment').length)
          $('.angebote-comment').editable();

        if($('.long-text').length){
          // new showHideText('.long-text');
          new showHideText('.long-text', {
              charQty     : 50,
              ellipseText : "...",
              moreText    : "More",
              lessText    : " Less"
          });
        }
      },
      error: function(error) {
        $('#table-data-modal').find('.modal-body').html('<p class="text-danger">Somthing Want Wrong!</p>');
      }
  });
}
function getcategorypropertieslist3(category){
    $('.cate-pro-div').html("");
    $('#cate-pro-listing').modal();
     var url = url_getcategoryproperty3+"?category2="+category;
        $.ajax({
            url: url,
        }).done(function (data) { 
            
            $('.cate-pro-div').html(data);
            var columns = [
                null,
                null,
            ];
            makeDatatable($('.category-table'), columns,'desc',0);
        });

}

function getassetmanagerprovision(asset_m_id){
  m = $('.pp-month').val();
  y = $('.pp-year').val();
  var url = url_getPropertyProvision+"?type=4&month="+m+"&year="+y+'&asset_m_id='+asset_m_id;
  $.ajax({
      url: url,
  }).done(function (data) { 
      if(asset_m_id!=0)
      {
        $('.list-data').html(data);
        var columns = [
          null,
          null,
          null,
          { "type": "numeric-comma", "class": "text-right" },
          null,
          { "type": "numeric-comma", "class": "text-right" },
          { "type": "numeric-comma", "class": "text-right" }
      ];
      makeDatatable($('#property-provision-list'), columns);


      }else{
          $('.property-provision-div-4').html(data);
          var columns = [
              null,
              null,
          ];
          makeDatatable($('#property-provision-table-4'), columns);
      }
  });
}

function loadDashboardCounter(){
  $.ajax({
        url: url_dashboard_counter,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {

        },
        success: function(result) {

          console.log('Counter script load time : '+result.script_load_time+' second');

          // $('.object-to-be-distributed-total').text(result.object_to_be_distributed);
          // $('.invoice-approval-total').text(result.invoice_approval);
          // $('.contract-total').text(result.contract_release);
          // $('.approval-building-insurance-total').text(result.approval_building_insurance);
          // $('.release-liability-total').text(result.release_liability);
          // $('.pending-invoice-total').text(result.pending_invoice);
          // $('.not-release-invoice-total').text(result.not_release_invoice);
          // $('.property-insurance-total').text(result.property_insurance);
          // $('.property-management-approval-total').text(result.property_management_approval);
          // $('.commission-release-total').text(result.commission_release);
          // $('.purchase-approval-total').text(result.purchase_approval);
          // $('.sales-clearance-total').text(result.sales_clearance);
          // $('.loi-purchase-total').text(result.status_loi);
          // $('.funding-request-total').text(result.funding_request);
          // $('.rental-overview-total').text(result.rental_overview);
          $('.default-payer-total').html(result.default_payer);
          // $('.bank-finance-total').text(result.bank_finance);
          // $('.insurance1-total').text(result.insurance1);
          // $('.insurance0-total').text(result.insurance0);
          $('.house-management-cost-total').text(result.house_management_cost);
          // $('.reminder-total').text(result.reminder);
          // $('.expired-rental-agreement-total').text(result.expired_rental_agreement);
          $('.vacancy-total').text(result.vacancy);
          // $('.expiring-leases-total').text(result.expiring_leases);
          // $('.recommendation-total').text(result.recommendation);
          // $('.diff_rent-total').text(result.diff_rent_total);
          // $('.release-invoice-total').text(result.release_invoice);
          // $('.release-contract-total').text(result.release_contract);
          $('.open-angebote-total').text(result.open_angebote);
        },
        error: function(error) {

        }
    });
}
loadDashboardCounter();
function getcategoryproperties4(){
    
    $.ajax({
        url: url_getcategoryproperty3,
    }).done(function (data) { 
        // $('.category-prop-list1').html(data.table1);
        $('.category-prop-list4').html(data.table);
        // $('.category-prop-list4-total').html(data.table_count);

        $('.category-prop-list5').html(data.table2);
        // $('.category-prop-list5-total').html(data.table2_count);

        $('.category-prop-list6').html(data.table3);
        // $('.category-prop-list6-total').html(data.table3_count);

        var columns = [
            null,
            null,
            { "type": "numeric-comma" }
        ];

        makeDatatable($('.category-table4'), columns, "desc", 1);
        makeDatatable($('.category-table6'), columns, "desc", 2);

    });
}

function getcategorypropertieslist2(category,name){

	$('.cate-pro-div').html("");
	$('#cate-pro-listing').modal();
	var url = url_getcategoryproperty3+"?category="+category+"&name="+name;
   	$.ajax({
       	url: url,
   	}).done(function (data) { 
       
       $('.cate-pro-div').html(data);

       if(category==''){
          	var columns = [
           		null,
           		null,
           		null,
           		null,
           		{ "type": "numeric-comma" },
           		null,
           		null,
           		null,
      	 	];
       		makeDatatable($('.name-rent-table'), columns,'desc',3);
       }else{

          	var columns = [
           		null,
           		null,
           		null,
           		{ "type": "numeric-comma" },
                { "type": "numeric-comma" },
           		null,
           		null,
                null,
       		];

       		makeDatatable($('.category-table'), columns,'desc',2);
       }

   	});

}

function getcategoryproperties(){
     
    $.ajax({
        url: url_getcategoryproperty,
    }).done(function (data) { 
        // $('.category-prop-list1').html(data.table1);
        $('.category-prop-list2').html(data.table2);


        drawchart3('high-container3',data.name,data.actual_net_rent,data.count);

        var columns = [
            null,
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            null,
            { "type": "numeric-comma" },
        ];

        makeDatatable($('.category-properties'), columns, "desc", 1);
        $('.categTotal').html($('.ajax-total-category').html());

    });

}

function getcategorypropertieslist(category,type,check){

    $('.cate-pro-div').html("");
    $('#cate-pro-listing').modal();
    var url = url_getcategoryproperty+"?category="+category+'&type='+type+'&rent_closed='+check;
    $.ajax({
        url: url,
    }).done(function (data) { 
        
        $('.cate-pro-div').html(data);
        var columns = [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            null,
            null,
        ];
        makeDatatable($('.category-table'), columns,'desc',2);
    });

}

function drawchart(cat,mv,vl,sum){
    Highcharts.chart('high-container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Mietverträge MV & VL'
        },
        xAxis: {
            categories: cat,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
              text: 'Miete  p.a.'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.1f} €</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'MV',
            data: mv

        }, {
            name: 'VL',
            data: vl

        }, {
            name: 'Gesamt',
            data: sum

        }]
    });
}

function drawchart3(container,cat,mv,vl){
    Highcharts.chart(container, {
        chart: {
            type: 'column'
        },
        title: {
            text: 'MIETERÜBERSICHT'
        },
        xAxis: {
            categories: cat,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
              text: 'Anzahl'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0,
              dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'IST-Nettokaltmiete p.m.',
            data: mv
        },
        {
            name: 'Anzahl',
            data: vl

        }]
    });
}

function drawchart2(cat,mv,vl){
    Highcharts.chart('high-container2', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Mietverträge & Verlängerungen'
        },
        xAxis: {
            categories: cat,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
              text: 'Anzahl'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
              pointPadding: 0.2,
              borderWidth: 0,
              dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'MV',
            data: mv
        }, {
            name: 'VL',
            data: vl

        }]
    });
}

function moveTodiv(id){
    $('html, body').animate({
     scrollTop: $("#"+id).offset().top
    }, 1000);
}

function getPendingNotReleaseInvoice(status){

    var url = url_getAssetPendingNotReleaseInvoice+"?status="+status;
    $.ajax({
        url: url,
    }).done(function (data) { 
        if(status==1)
            $('.property-notrelease-invoice-div').html(data);
        else
            $('.property-pending-invoice-div').html(data);
        var columns = [
            null,
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ];
        $('.invoice-count-'+status).html($(".invoice-class-"+status).val());

        makeDatatable($('#table-property-invoice'+status),columns, 'desc', 6);

        if ($('.invoice-comment').length) 
            $('.invoice-comment').editable();
    });
}

function getLiquiplanung(){//for cashflow
    $.ajax({
        url: url_get_liquiplanung,
    }).done(function (data) {
        $('.liquiplanung-div').html(data);

        var columns = [
            null,
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "numeric-comma" }
        ];
        makeDatatable($('#liquiplanung-table'),columns);
    });
}

function getLiquiplanung1(){//for Liquiplanung
    $.ajax({
        url: url_get_liquiplanung1,
    }).done(function (data) {
        $('.liquiplanung-1-div').html(data);

        /*var columns = [
            null,
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" }
        ];*/
        // makeDatatable($('#liquiplanung-1-table'));
    });
}

$( document ).ready(function() {

    // getPendingNotReleaseInvoice(1);
    // getPendingNotReleaseInvoice(2);

    if(login_email == 't.raudies@fcr-immobilien.de'){
        getLiquiplanung();
    }

    getLiquiplanung1();
    
    $('#reload-liquiplanung-1-div').on('click', function () {
        getLiquiplanung1();
    });

    $('.btn-export-to-excel').on('click', function () {
        var propertyTableArray = getTableData("#hausmax_table");
        $('input#property-data').val(JSON.stringify(propertyTableArray));
        $('#export-to-excel-form').submit();
    });

    $('body').on('click','.get-category-properties',function(){
        category = $(this).attr('data-category');
        type = $(this).attr('data-type');
        check = $(this).attr('data-rent-closed');
        getcategorypropertieslist(category,type,check)
    });

    $('body').on('click','.get-category-properties3',function(){
        category = $(this).attr('data-category');
        getcategorypropertieslist2(category,"")
    });

    $('body').on('click','.get-category-properties4',function(){
        category = $(this).attr('data-category');
        getcategorypropertieslist3(category)
    });

    $('body').on('click','.get-name-properties',function(){
       name = $(this).attr('data-category');
       category = "";
       getcategorypropertieslist2(category,name)
   	});

   	var columns = [
        null,
        null,
        null,
        null,
        { "type": "numeric-comma" },
        { "type": "new-date" },
        null,
        null
    ];
    makeDatatable($('.class-data-table'), columns, "asc", 5);
    
    $.ajax({url: url_getMahnungAmount}).done(function (data) { 
        $('.mahnungen-30-days').html(data);
    });

    $('.tenancy_item').click(function(){
        $('#tenant-detail-modal').modal();
        id = $(this).attr('data-id');
        var url = url_tenant_detail+"?id="+id;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.tenant-detail').html(data);
        });
    });

    $('body').on('click','.get-rents',function(){
        $('.list-data-rent').html("");
        $('.vlist-data-rent').html("");
        id= $(this).attr('data-value');
        property_id= $(this).attr('data-property');
        type= $(this).attr('data-type');

        if(type == "r")
            $('#rent-list').modal();
        else
            $('#vacant-list').modal();

        var url = url_assetmanagement+"?asset_manager="+id+"&property_id="+property_id+"&type="+type;
        $.ajax({
            url: url,
        }).done(function (data) { 
            if(type == "r")
                $('.list-data-rent').html(data);
            else
                $('.vlist-data-rent').html(data);

            var columns = [
                null,
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" }
            ];
            makeDatatable($('#list-rent'), columns);
        });
    });

    $('body').on('click','.get-tenant-mahnung',function(){
        $('.get-tenant-mahnung-list-data').html("");
        id= $(this).attr('data-id');

        var url = url_getMahnungTenant+"?id="+id;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.get-tenant-mahnung-list-data').html(data);

            var columns = [
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
            ];
            makeDatatable($('#mahnung-tenants'), columns);
        });
    });

    $('body').on('click','.get-asset-property',function(){
        $('.list-data').html("");
        id= $(this).attr('data-value');
        
        $('#property-list').modal();

        var url = url_assetmanagement+"?id="+id;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.list-data').html(data);

            var columns = [
                null,
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
            ];
            makeDatatable($('#list-properties'), columns);
        });
    });

    $('.change-select').change(function(){
        m = $('.pm-month').val();
        y = $('.pm-year').val();

        var url = url_monthyearmanager+"?month="+m+"&year="+y;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.manager-property-list').html(data);

            var columns = [
                null,
                null,
                { "type": "numeric-comma" },
                null,
                { "type": "numeric-comma" },
                null,
                { "type": "numeric-comma" },
                
                null,
                { "type": "numeric-comma" },
                null,
                { "type": "numeric-comma" },
                null,
                { "type": "numeric-comma" },
                null,
                { "type": "numeric-comma" },
                null,
                { "type": "numeric-comma" }
            ];
            makeDatatable($('#added-month'), columns, "asc", 0, 50);

            $('.angebotTotal').text($('#added-month_wrapper .border-top-footer:last').html());
        });
    });

    table = "";

    $('.achange-select').change(function(){
        m = $('.pa-month').val();
        y = $('.pa-year').val();
        t = $('.type-selection').val();

        var url = url_monthyearassetmanager+"?month="+m+"&year="+y+"&type="+t;
         $.ajax({
            url: url,
        }).done(function (data) {

            $('.asset-manager-property-list').html(data.table);
            $('.asset-manager-property-list-total').html(data.subtable);

            var columns = [
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" }
            ];
            table = makeDatatable($('#added-month-asset'), columns, "asc", 0, 25);

            drawchart(data.cat,data.mv,data.vl,data.sum);

            drawchart2(data.m_arr,data.m_arr1,data.m_arr2);
        });
    });

    $('body').on('click','.get-manager-property',function(){
        $('.list-data').html("");
        id= $(this).attr('data-id');
        m = $('.pm-month').val();
        y = $('.pm-year').val();
        status= $(this).attr('data-status');
        

        var url = url_getmanagerproperty+"?id="+id+"&month="+m+"&year="+y+"&status="+status;
         $.ajax({
            url: url,
        }).done(function (data) { 
            $('.list-data').html(data);

            if(status == 12){

                var columns = [
                    null,
                    null,
                    { "type": "new-date" },
                    { "type": "new-date" },
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    null
                ];
                makeDatatable($('#list-properties'), columns, "asc", 2);

            }else if(status == 14 || status == 10){

                var columns = [
                    null,
                    null,
                    { "type": "new-date" },
                    { "type": "new-date" },
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    null
                ];
                makeDatatable($('#list-properties'), columns, "asc", 2);

            } else {

                var columns = [
                    null,
                    null,
                    { "type": "new-date" },
                    { "type": "new-date" },
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    null
                ];
                makeDatatable($('#list-properties'), columns, "asc", 2);
            }
        });
    });

    $('body').on('click','.get-assetmanager-property',function(){
        $('.asset-list-data').html("");
        id= $(this).attr('data-id');

        m = $('.pa-month').val();
        y = $('.pa-year').val();

        t = $(this).attr('data-type');


        var url = url_getassetmanagerproperty+"?id="+id+"&month="+m+"&year="+y+"&type="+t;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.asset-list-data').html(data);

            var columns = [
                null,
                null,
                null,
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" }
            ];
            makeDatatable($('#tenant-table_22'), columns);
        });
    });

    $('body').on('click','.get-name-insurance',function(){
        id = $(this).attr('data-id');
        t = encodeURIComponent($(this).html());
        var url = url_getInsurancePropertyList+"?axa="+id+'&title='+t;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('#iproperty-list').modal();
            $('.insurance-list-data').html(data);
            setTimeout(function(){
                var columns = [
                    null,
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                ];
                makeDatatable($('#insurance-property-table'), columns);
            },1000);
        });  
    });

    /////////////////// Hausmaxx Table Modal //////////////////
    /*$('body').on('click','.get-hausmaxx-data',function(){
        id = $(this).attr('data-id');
        t = encodeURIComponent($(this).html());
        var url = url_get_hausmaxx_data+"?id="+id+'&title='+t;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('#hausmax-list').modal();
            $('.hausmax-list-data').html(data);
            setTimeout(function(){
                var columns = [
                    null,
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                ];
                makeDatatable($('#hausmaxx_list_table'), columns);
            },1000);
        });  
    }); */

    $('body').on('click','.get-hausmaxx-data',function(){
        id = $(this).attr('data-id');
        t = encodeURIComponent($(this).html());

        $('#hausmax-list').find('.modal-title').html($(this).html());
        var url = url_get_hausmaxx_data+"?id="+id+'&title='+t;
        $.ajax({
            url: url,
        }).done(function (data) {
            $('#hausmax-list').modal();
            $('.hausmax-list-data').html(data);
            // setTimeout(function(){
                var columns = [
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    {"type": "new-date"},
                    {"type": "new-date"},
                    {"type": "new-date"}
                ];
                makeDatatable($('#hausmaxx_listdata_table'), columns);
            // },1000);
        });
    });

    getcategoryproperties();
    // getcategoryproperties3();
    getcategoryproperties4();

    $.ajax({
        url: url_getbankproperty,
    }).done(function (data) { 
        $('.bank-prop-list').html(data);
        var columns = [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" }
        ];
        makeDatatable($('#bank-properties'), columns);
        
        $('.bankenfinanTotal').html($('.total-delta2').html())
        
    });

    var url = url_monthyearassetmanager+"?type=0&month="+m+"&year="+y;
    $.ajax({
        url: url,
    }).done(function (data) { 
        $('.mv-count').html($(data.table).find('.mv-total').text());
    });


    /*var url = url_assetmanagement+"?id=-1";
    $.ajax({
        url: url,
    }).done(function (data) {
        $('.table-not-beur-div').html(data);
        var columns = [
            null,
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
        ];
        makeDatatable($('#list-properties-1'), columns);
        $('.vobj-total').html($('.not-beur-sum').val());
    });*/

    var columns = [
        null,
        null,
        { "type": "numeric-comma" },
        { "type": "numeric-comma" },
        { "type": "numeric-comma" }
    ];
    makeDatatable($('#hausmax_table'), columns, "desc", 2);

    $('.comment-text').change(function(){
        var url = url_change_comment+"?";
        var data = {
            _token : _token,
            comment : $(this).val(),
            id:$(this).attr('data-id'),
        };
         $.ajax({
                url: url,
                type:'POST',
                data:data,
            }).done(function (data) { 
                alert(data);                        
        })
    });

    $('.comment-file').change(function(){

        var formdata = new FormData();
        var url = url_change_comment+"?";
        file =$(this).prop('files')[0];
        formdata.append("file", file);
        formdata.append('_token', _token);
        formdata.append('id', $(this).attr('data-id'));


         $.ajax({
            url: url,
            type:'POST',
            contentType: false,
            processData: false,
            data:formdata,
        }).done(function (data) { 
            alert(data);                        
        })
    });
    
    $('.get-verkauf-user').click(function(){
        $('#user-list-popup').modal();
        $('.user-list').html("");
        var active= $(this).attr('data-active');
        var url = url_getusers;
        $.ajax({
            url: url,
            data:{active:active,domain:1},
        }).done(function (data) { 
            $('.user-list').html(data);
            makeDatatable($('.user-list table'));
        });
    });

    $('.get-vermietung-user').click(function(){
        $('#user-list-popup').modal();
        $('.user-list').html("");
        var active= $(this).attr('data-active');
        var url = url_getusers;
        $.ajax({
            url: url,
            data:{active:active,domain:2},
        }).done(function (data) { 
            $('.user-list').html(data);
            makeDatatable($('.user-list table'));
        });
    });

    $('.comment-detail').click(function(){
        $('#comment-title').html($(this).html());
        $('#comment-detail-popup').modal();
        $('.comment-detail-body').html("");
        var url = url_comment_detail;
        $.ajax({
                url: url,
                data:{id:$(this).attr('data-id')},
            }).done(function (data) { 
                $('.comment-detail-body').html(data);
                
        })
    });

    $('.comment-checkbox').click(function(){

        var status = 0;
        if($(this).is(':checked'))
            status = 1;

        var url = url_change_comment+"?";
        var data = {
            _token : _token,
            status : status,
            id:$(this).attr('data-id'),
        };
        $.ajax({
                url: url,
                type:'POST',
                data:data,
        }).done(function (data) { 
            alert(data);                        
        })
    })
    

    var url = url_getMahnungProperties+"?";
    $.ajax({
            url: url,
    }).done(function (data) { 
        $('.mahnung-div').html(data);

        var columns = [
            null,
            null,
            { "type": "numeric-comma" },
        ];
        makeDatatable($('#mahnung-properties'), columns, "desc", 2);
    });

    var url = url_assetmanagement+"?ajax=1";
    $.ajax({
        url: url,
    }).done(function (data) { 
        $('.assetmanagement-list').html(data);
        $('.vermi_percent').html($('body').find('#vermi_percent').val());

        var columns = [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
        ];
        makeDatatable($('#dashboard-table-2'), columns);
    });
    
    var url = url_getPropertyInsurance+"?axa=1";
    $.ajax({
        url: url,
    }).done(function (data) { 
        $('.insurance-div1').html(data[0]);
        $('.axa1Total').html(data[1]);
        setTimeout(function(){
            var columns = [
                null,
                null,
                { "type": "numeric-comma" },
            ];
            makeDatatable($('#insurance-table1'), columns, "desc", 2);
        },1000);
    });

    var url = url_getPropertyInsurance+"?axa=0";
    $.ajax({
        url: url,
    }).done(function (data) { 
        $('.insurance-div2').html(data[0]);
        $('.axa0Total').html(data[1]);
        setTimeout(function(){
            var columns = [
                null,
                null,
                { "type": "numeric-comma" },
            ];
            makeDatatable($('#insurance-table0'), columns, "desc", 2);
        },1000);
    });

    //$('.change-select').trigger('change');
    $('.achange-select').trigger('change');
    
    $.ajax({
        url: url_mailchimp,
    }).done(function (data) { 
        $('.mcount').html(data);
    });

    var columns = [
        null,
        null,
        null,
        { "type": "numeric-comma" },
        { "type": "non-empty-string" },
        { "type": "numeric-comma" },
        { "type": "numeric-comma" },
        null,
        null
    ];
    makeDatatable($('#tenant-table2'), columns, "asc", 4);

    var columns = [
        null,
        null,
        null,
        { "type": "numeric-comma" },
        { "type": "non-empty-string" },
        { "type": "numeric-comma" },
        { "type": "numeric-comma" },
        null,
        null
    ];
    makeDatatable($('#tenant-table3'), columns, "asc", 4);

    $('#assetmanagement').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if( response.success === false )
                return response.msg;
            else{
                location.reload();
            }
        }
    });

    /*var url = url_getPropertyReleaseInvoice+"?";
    $.ajax({
        url: url,
    }).done(function (data) { 
        $('.release-invoice-div').html(data.html);
        $('.release-invoice-total').html(data.count);
        var columns = [
            null,
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            { "type": "new-date-time" },
            null,
            null,
        ];
        makeDatatable($('#table-property-release-invoice') ,columns, 'desc', 5);
    });*/

    $('body').on('click','.show-all-release-invoice',function(){
      
      var url = url_getPropertyReleaseInvoice+"?all=1";
      $.ajax({
          url: url,
      }).done(function (data) { 
          $('.release-invoice-div').html(data.html);
          $('.release-invoice-total').html(data.count);
          var columns = [
              null,
              null,
              null,
              { "type": "new-date" },
              { "type": "numeric-comma" },
              { "type": "new-date-time" },
              null,
              null,
          ];
          makeDatatable($('#table-property-release-invoice') ,columns, 'desc', 5);
      });
    });

    $('body').on('click','.mark-as-paid',function(){
        var id = $(this).attr('data-id');
        $this = $(this);
        var url = url_makeAsPaid+"?id="+id;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $this.closest('tr').remove();
        });

    });

    if(1==2 && login_email == 'u.wallisch@fcr-immobilien.de'){

        url = url_getPropertyProvision+"?type=2";
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.property-provision-div-2').html(data);
            $('.provisionTotal-2').html($('.provisioncount-2').val());
            var columns = [
                null,
                null,
                null,
                { "type": "numeric-comma", "class": "text-right" },
                null,
                { "type": "numeric-comma", "class": "text-right" },
                { "type": "numeric-comma", "class": "text-right" }
            ];
            makeDatatable($('#property-provision-table-2'), columns);
        });

    }

    if(login_email == 'j.klausch@fcr-immobilien.de'){

    	/*var url = url_getPropertyProvision+"?type=2";
      	$.ajax({
          	url: url,
      	}).done(function (data) { 
          	$('.property-provision-div-2').html(data);
          	$('.provisionTotal-2').html($('.provisioncount-2').val());
          	var columns = [
              	null,
              	null,
              	null,
              	{ "type": "numeric-comma", "class": "text-right" },
              	null,
              	{ "type": "numeric-comma", "class": "text-right" },
              	{ "type": "numeric-comma", "class": "text-right" }
          	];
          	makeDatatable($('#property-provision-table-2'), columns);
      	});*/

      	/*var url = url_getPropertyProvision+"?type=3";
      	$.ajax({
          	url: url,
      	}).done(function (data) { 
          	$('.property-provision-div-3').html(data);
          	$('.provisionTotal-3').html($('.provisioncount-3').val());
          	var columns = [
              	null,
                {"type": "new-date-time"},
              	null,
              	null,
              	{ "type": "numeric-comma", "class": "text-right" },
              	null,
              	{ "type": "numeric-comma", "class": "text-right" },
              	{ "type": "numeric-comma", "class": "text-right" }
          	];
          	makeDatatable($('#property-provision-table-3'), columns);
      	});*/

          
        getassetmanagerprovision(0);

      	$('.pchange-select').change(function(){
        	getassetmanagerprovision(0);            
      	});

      	$('body').on('click','.get-provision-property',function(){
        	$('.list-data').html("");
        	id= $(this).attr('data-id');
        	getassetmanagerprovision(id);
        	$('#property-list').modal();
      	});

    }

    if(login_email == 'u.wallisch@fcr-immobilien.de'){

    	var url = url_getPropertyProvision+"?type=5";
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.property-provision-div-5').html(data);
            $('.provisionTotal-5').html($('.provisioncount-5').val());
            var columns = [
                null,
                {"type": "new-date-time"},
                null,
                null,
                { "type": "numeric-comma", "class": "text-right" },
                null,
                { "type": "numeric-comma", "class": "text-right" },
                { "type": "numeric-comma", "class": "text-right" }
            ];
            makeDatatable($('#property-provision-table-5'), columns);
        });
        getassetmanagerprovision(0);

        $('.pchange-select').change(function(){
            getassetmanagerprovision(0);            
        });

        $('body').on('click','.get-provision-property',function(){
            $('.list-data').html("");
            id= $(this).attr('data-id');
            getassetmanagerprovision(id);
            $('#property-list').modal();
        });

    }

    $('.HAUSVERWALTUNGTOTAL').text($('.HAUSVERWALTUNG').html());
    $('.sumauTotal').text($('.sumau').html());
    $('.sumausTotal').text($('.sumaus').html());
            // $('.sumauTotal').text("12");
    setTimeout(function(){
        $('.HAUSVERWALTUNGTOTAL').text($('.HAUSVERWALTUNG').html());
        $('.sumauTotal').text($('.sumau').html());
    },5000);

    $('body').on('click', '#added-month-asset tbody th:first-child', function () {
        var tr = $(this).closest('tr').next();
        if(tr.hasClass('mv')){
            if(tr.hasClass('hidden'))
                tr.removeClass('hidden'); 
            else
                tr.addClass('hidden'); 

            var tr = tr.next();
            if(tr.hasClass('vl')){
                if(tr.hasClass('hidden'))
                    tr.removeClass('hidden'); 
                else
                    tr.addClass('hidden'); 
            }
        }
    });

    // Open invoice table
    
    /*if($('#table-property-open-invoice').length > 0){
        var table_property_open_invoice = $('#table-property-open-invoice').dataTable({
            "ajax": url_amdashboard_get_openinvoice,
            "order": [
                [7, 'desc']
            ],
            "columnDefs": [{
                className: "text-right",
                "targets": [4]
            }],
            "columns": [
                null,
                null,
                null,
                {"type": "new-date"},
                {"type": "numeric-comma"},
                null,
                null, 
                {"type": "new-date-time"},
                null,
                null,
                null,
                null,
            ],
            "drawCallback": function (settings) {
                var response = settings.json;
                if(typeof response !== 'undefined' && typeof response.total_record !== 'undefined'){
                    $('.open-invoice-total').html(response.total_record);
                }
            },
        });
    }*/

    var invoice_reject_id;
    var invoice_rejected_property_id;
    $('body').on('click','.btn-reject',function(){
        invoice_reject_id = $(this).attr('data-id');
        invoice_rejected_property_id = $(this).attr('data-property-id');
        console.log({invoice_reject_id, invoice_rejected_property_id});
        $('#invoice-reject-modal').modal();
    });

    $('body').on('click','.invoice-reject-submit',function(){
        var comment = $('.invoice-reject-comment').val();
        $.ajax({
            type : 'POST',
            url : url_property_invoicemarkreject,
            data : {
                id:invoice_reject_id,
                _token : _token, 
                comment:comment, 
                property_id:invoice_rejected_property_id
            },
            success : function (data) {
                $('#table-property-open-invoice').DataTable().ajax.reload();
            }
        });
    });

    $(document).ajaxComplete(function() {
        if ($('.invoice-comment').length)
            $('.invoice-comment').editable();
    });

    vacant_id = 0;
    vacant_release_type ="";
    $('body').on('click', '.invoice-release-request', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        $('.invoice-release-comment').val("")
        $('#invoice-release-property-modal').modal();


        if(vacant_release_type=="release2")
        {
            $('.i-message').addClass('hidden');
            $('.i-message2').removeClass('hidden');
        }
        else{
            $('.i-message').removeClass('hidden');
            $('.i-message2').addClass('hidden');
        }

    });

    $('body').on('click', '.invoice-release-submit', function() {
        comment = $('.invoice-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_invoicereleaseprocedure,
            data: {
                id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                // property_id: $('#selected_property_id').val()
            },
            success: function(data) {
                getPendingNotReleaseInvoice(1);
                // getPendingNotReleaseInvoice(2);
                $('#table-property-open-invoice').DataTable().ajax.reload();
            }
        });
    });


});
function getdbayer(){
    m = $('.bb-month').val();
    y = $('.bb-year').val();
    var url = url_getPropertyDefaultPayers+"?month="+m+"&year="+y;

    $.ajax({
        url: url,
    }).done(function (data) { 
        $('.property-default-payer-div').html(data);
        var columns = [
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
            null,
            null,
            null,
        ];
        makeDatatable($('#property-default-payer-table'), columns, 'asc', 2);
        $('.default_payerTotal').html($('.input-default_payerTotal').val());
    });
}
$('#default_payer .bchange-select').change(function(){
    getdbayer(0);            
});

getInsurance(1);//for GEBÄUDE 
getInsurance(2);//for HAFTPFLICHT
function getInsurance(type){

    var url = url_getInsurance+"?type="+type;
    $.ajax({
       url: url,
    }).done(function (data) { 
        $('.insurance_type_'+type+'_div').html(data);
        if(type==1)
        $('.gebaTotal').html($('.release_building').val());
        else
        $('.haftTotal').html($('.release_liability').val());
        
        // $('.invoiceTotal').html($('.unpaidinvoice').val());
        var columns = [
            null,
            null,
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            null
        ];
        makeDatatable($('.all_insurance_table_'+type),columns);
    });
}

$(document).on('click', ".liquiplanung-data > a", function(e){
    var td = $(this).closest('td');
    var row = $(td).attr('row');
    var col = $(td).attr('col');
    var title = $(td).closest('tr').find('td:eq(0)').text();

    $('#liquiplanung-data-modal').find('.liquiplanung-data-title').text(title);
    $('#liquiplanung-data-modal').modal('show');

    $.ajax({
       url: url_get_liquiplanung_1_data+'?row='+row+'&col='+col,
    }).done(function (data) {
       $('#liquiplanung-data-table-div').html(data);
        makeDatatable($('#liquiplanung-data-table'));
    });
});

$('body').on('click','.diff',function(){
    var url = $(this).attr('data-url');
    $('#modal_op_list').modal('show');

    $.ajax({
         url: url,
    }).done(function (data) {
        $('#modal_op_list').find('#rent-paid-data').html(data);
        var columns = [
            null,
            null,
            null,
            null,
            { "type": "numeric-comma"},
            { "type": "numeric-comma"},
            null,
            { "type": "numeric-comma"},
            null,
            null,
            null,
            null,
            null,
            null,
        ];
        makeDatatable($('#rent-paid-table'), columns);
        var columns = [
            null,
            null,
            {"type": "new-date-time"}
        ];
        makeDatatable($('#rent-paid-upload-table'), columns, 'desc', 2);
    });
});
$('body').on('click','#default_payer .hide-checkbox',function(){
    id = $(this).attr('data-id');
    var m = $('.bb-month').val();
    var y = $('.bb-year').val();

    load_url = url_get_default_payers+"?month="+m+"&year="+y+"&status="+id+'&amcheck=1';
    loadTable(true);
});

$('body').on('change','#table-data-modal .bchange-select',function(){
    var m = $('#table-data-modal .bb-month').val();
    var y = $('#table-data-modal .bb-year').val();
    load_url = url_get_default_payers+"?month="+m+"&year="+y+"&status=0";
    loadTable(true);
});

vacant_id = vacant_release_type = ""; 
$('body').on('click', '.invoice-release-request-am', function() {
    vacant_release_type = $(this).attr('data-column');
    vacant_id = $(this).attr('data-id');
    property_id = $(this).attr('data-property_id');
    $('.invoice-release-comment-am').val("")
    $('#invoice-release-property-modal-am').modal();

});
$('body').on('click', '.invoice-release-submit-am', function() {
    comment = $('.invoice-release-comment-am').val();
    
    $.ajax({
        type: 'POST',
        url: url_property_invoicereleaseprocedure,
        data: {
            id: vacant_id,
            step: vacant_release_type,
            _token: _token,
            comment: comment,
        },
        success: function(data) {
          loadTable(true);
        }
    });
});
/*--------------------JS FOR PROPERTY COMMENTS- START----------------------------------*/
var c_record_id = '';
var c_property_id = '';
var c_type = '';
var c_is_comment = false;
var c_section = '';
$('body').on('click', '.btn-add-property-comment', function() {

    var $this = $(this);

    c_record_id = $($this).attr('data-record-id');
    c_property_id = $($this).attr('data-property-id');
    c_type = $($this).attr('data-type');

    var comment = $($this).closest('.property-comment-section').find('.property-comment').val();
    var reload = $(this).attr('data-reload');
    var subject = $(this).attr('data-subject');
    var content = $(this).attr('data-content');

    var data = { 
        'property_id': c_property_id,
        'comment': comment,
        'type': c_type,
        'subject': subject,
        'content': content,
    };
    if(c_record_id && c_record_id != ''){
      data.record_id = c_record_id;
    }

    if(comment){
      addPropertyComment($this, data, (reload == '1') ? true : false);
    }
});

$('body').on('click', '.btn-show-property-comment', function() {
  var $this = $(this);
  var show_form = ($($this).attr('data-form') == '1') ? true : false;
  var subject = $(this).attr('data-subject');
  var content = $(this).attr('data-content');

  c_record_id = $($this).attr('data-record-id');
  c_property_id = $($this).attr('data-property-id');
  c_type = $($this).attr('data-type');

  c_section = $($this).closest('.white-box').find('.box-title').text();

  if(show_form){
      $('#property_comment_modal').find('.property-comment-section').removeClass('hidden');
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-record-id', c_record_id);
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-property-id', c_property_id);
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-type', c_type);
      
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-subject', subject);
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-content', content);
  }else{
    $('#property_comment_modal').find('.property-comment-section').addClass('hidden');
  }

  $('#property_comment_modal').modal('show');
  getPropertyComment(c_record_id, c_property_id, c_type);
});

$('body').on('click', '#property_comment_limit', function() {
    var text = $(this).text();
    if(text == 'Show More'){
        $(this).text('Show Less');
    }else{
        $(this).text('Show More');
    }
    getPropertyComment(c_record_id, c_property_id, c_type);
});

$('body').on('click', '.remove-property-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    getPropertyComment(c_record_id, c_property_id, c_type);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

function addPropertyComment(btnobj, data, reload = false){
  $.ajax({
      url: url_add_property_comment,
      type: 'POST',
      data: data,
      dataType: 'json',
      beforeSend: function() {
          $(btnobj).prop('disabled', true);
      },
      success: function(result) {

          $(btnobj).prop('disabled', false);

          if (result.status == true) {

              $(btnobj).closest('.property-comment-section').find('.property-comment').val('');
              $(btnobj).closest('.property-comment-section').find('.property-comment').focus();

              if(reload){
                  c_is_comment = true;
                  getPropertyComment(c_record_id, c_property_id, c_type);
              }

          } else {
              alert(result.message);
          }
      },
      error: function(error) {
          $(btnobj).prop('disabled', false);
          alert('Somthing want wrong!');
      }
  });
}


function getPropertyComment(record_id = '', property_id = '', type = ''){
    var limit_text = $('#property_comment_limit').text();
    var limit = (limit_text == 'Show More') ? 1 : '';

    var new_url = url_get_property_comment+'?property_id='+property_id+'&record_id='+record_id+'&type='+type+'&limit='+limit;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#property_comment_table').find('tbody').empty();
            if(type == 'property_invoices'){
                $('#property_comment_table').find('#th_name').hide();
                $('#property_comment_table').find('#th_date').hide();
            }else{
                $('#property_comment_table').find('#th_name').show();
                $('#property_comment_table').find('#th_date').show();
            }

            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_property_comment.replace(":id", value.id);
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-property-comment">Löschen</a></td>' : '';
                    
                    if(type == 'property_invoices'){
                      var tr = '\
                        <tr>\
                            <td>'+value.user_name+' ('+value.date+') : '+ value.comment +'</td>\
                            <td class="text-center">'+delete_button+'\
                        </tr>\
                      ';
                    }else{
                      var tr = '\
                        <tr>\
                            <td>'+ value.user_name +'</td>\
                            <td>'+ value.comment +'</td>\
                            <td>'+ value.created_at +'</td>\
                            <td>'+delete_button+'\
                        </tr>\
                      ';
                    }

                    $('#property_comment_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

$('#property_comment_modal').on('hidden.bs.modal', function () {
  var ele = $(this).find('.property-comment-section');

  if(!$(ele).hasClass("hidden") && c_is_comment){

      c_is_comment = false;
      
      if(c_section == 'Nicht Freigegeben'){
        getPendingNotReleaseInvoice(1);
      }else if(c_section == 'Pending Rechnungen'){
        getPendingNotReleaseInvoice(2);
      }else if(c_section == 'Offene Rechnungen'){
        $('#table-property-open-invoice').DataTable().ajax.reload();
      }
  }
});
/*--------------------JS FOR PROPERTY COMMENTS- END----------------------------------*/



/*----------------------JS FOR Property Insurance comments - START -------------------------*/
var insurance_detail_comment_url;
var insurance_detail_comment_id;
var insurance_detail_property_id;
var type = '';
var is_comment = false;
$('body').on('click', '.btn-ins-comment', function() {

    insurance_detail_comment_url = $(this).attr('data-url');
    insurance_detail_comment_id  = $(this).attr('data-id');
    insurance_detail_property_id  = $(this).attr('data-property-id');
    type = 'property_insurance_tab_details';

    var limit_text = $('#insurance_detail_comments_limit').text();
    getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
    $('#insurance_detail_comment_modal').find('form').removeClass('hidden');
    $('#insurance_detail_comment_modal').modal('show');
});

$('#insurance_detail_comment_form').submit(function(event) {

    event.preventDefault();
    var $this = $(this);
    var url = $(this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var data = {
        'record_id': insurance_detail_comment_id, 
        'property_id': insurance_detail_property_id,
        'comment': comment,
        'type': type
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);
            if (result.status == true) {
                
                $($this)[0].reset();
                $($this).find('textarea[name="comment"]').focus();
                
                is_comment = true;

                var limit_text = $('#insurance_detail_comments_limit').text();
                getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });
    
});

$('body').on('click', '#insurance_detail_comments_limit', function() {
    var text = $(this).text();
    var limit = '';
    if(text == 'Show More'){
        limit = '';
        $(this).text('Show Less');
    }else{
        limit = 1;
        $(this).text('Show More');
    }
    getInsuranceDetailComments(insurance_detail_comment_url, limit, type);
});

$('body').on('click', '.remove-insurance-detail-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    var limit_text = $('#insurance_detail_comments_limit').text();
                    getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

$('#insurance_detail_comment_modal').on('hidden.bs.modal', function () {
  var form = $(this).find('form');
  
  if(!$(form).hasClass("hidden") && is_comment){
    is_comment = false;
    loadTable(true);
  }
});

function getInsuranceDetailComments(url, limit = '', type=''){
    var new_url = url+'/'+limit+'?type='+type;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#insurance_detail_comments_table').find('tbody').empty();
            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_insurance_detail_comment.replace(":id", value.id);
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-insurance-detail-comment">Löschen</a></td>' : '';
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;

                    var tr = '\
                    <tr>\
                        <td>'+ commented_user +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td>'+delete_button+'\
                    </tr>\
                    ';

                    $('#insurance_detail_comments_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}
/*----------------------JS FOR Property Insurance comments - END -------------------------*/

/*----------------------JS FOR Property Insurance Section comments - START -------------------------*/
$('body').on('click', '.btn-add-angebote-section-comment', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    var comment = $($this).closest('.row').find('.angebote-section-comment').val();
    var record_id = $($this).closest('.row').attr('data-id');
    var property_id = $($this).closest('.row').attr('data-pid');

    var data = {
        'record_id': record_id, 
        'property_id': property_id,
        'comment': comment,
        'type': 'property_insurance_tabs'
    };
    console.log(data);
    if(comment){
      $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',
          beforeSend: function() {
              $($this).prop('disabled', true);
          },
          success: function(result) {

              $($this).prop('disabled', false);

              if (result.status == true) {

                  $($this).closest('.row').find('.angebote-section-comment').val('');
                  $($this).closest('.row').find('.angebote-section-comment').focus();

              } else {
                  alert(result.message);
              }
          },
          error: function(error) {
              $($this).prop('disabled', false);
              alert('Somthing want wrong!');
          }
      });
    }else{
      return false;
    }
});

$('body').on('click', '.btn-show-angebote-section-comment', function() {
  var url = $(this).attr('data-url');
  insurance_detail_comment_url = url;

  var limit_text = $('#insurance_detail_comments_limit').text();
  type = 'property_insurance_tabs';
  getInsuranceDetailComments(url, (limit_text == 'Show More') ? 1 : '', type);
  $('#insurance_detail_comment_modal').find('form').addClass('hidden');
  $('#insurance_detail_comment_modal').modal('show');
});
/*----------------------JS FOR Property Insurance Section comments - START -------------------------*/



/*---------------------------------Open Angebote button js---------------------------------------------*/
$('body').on('click','.btn-not-release-am-insurance',function(){
    not_release_id = $(this).attr('data-id');
    table_id = $(this).closest('table').attr('data-tabid');
    $('#insurance-not-release-am-modal').modal();
    $('.insurance-not-release-am-comment').val("")
});

$('body').on('click','.insurance-not-release-am-submit',function(){
    comment = $('.insurance-not-release-am-comment').val();
    $.ajax({
          type : 'POST',
          url : url_insurance_mark_as_notrelease_am,
          data : {id:not_release_id, _token : _token, comment:comment},
          success : function (data) {
            location.reload(true);
          }
      });
});

$('body').on('click', '.property-insurance-release-request', function() {

    if(is_falk==1){
        // var length = $(this).closest('.modal-body').find('table').find('[data-field="falk_status"]:checked').length;
        // var msg = "Bitte Checkbox Falk auswählen!";
        length = 1;
    }else{
        var length = $(this).closest('.row').find('table').find('[data-field="asset_manager_status"]:checked').length;
        var msg = "Bitte eine AM Checkbox auswählen!";
    }

    if(length > 0){
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        $('.property-insurance-comment').val("")
        $('#property-insurance-release-property-modal').modal();
    }else{
        alert(msg); return false;
    }
});

$('body').on('click', '.property-insurance-submit', function() {
    am_id = fk_id = 0;
    if(is_falk==0)
    {
        $('#insurance-table-'+vacant_id+' .am_falk_status').each(function(){
                if($(this).is(':checked') && $(this).attr('data-field')=="falk_status")
                    fk_id = $(this).attr('data-id');
                if($(this).is(':checked') && $(this).attr('data-field')=="asset_manager_status")
                    am_id = $(this).attr('data-id');
        });
    }
    comment = $('.property-insurance-comment').val();
    $.ajax({
        type: 'POST',
        url: url_property_dealreleaseprocedure,
        data: {
            id: vacant_id,
            step: vacant_release_type,
            _token: _token,
            am_id:am_id,
            fk_id:fk_id,
            comment: comment,
        },
        success: function(data) {
            location.reload();
        }
    });
});

$('body').on('click','.btn-pending-insurance',function(){
    pending_id = $(this).attr('data-id');
    table_id = $(this).closest('table').attr('data-tabid');
    $('#insurance-pending-modal').modal();
    $('.insurance-pending-comment').val("")
});

$('body').on('click','.insurance-pending-submit',function(){
    comment = $('.insurance-pending-comment').val();
    $.ajax({
          type : 'POST',
          url : url_insurance_mark_as_pending,
          data : {id:pending_id, _token : _token, comment:comment},
          success : function (data) {
            location.reload();
          }
      });
});

$('body').on('click','.btn-not-release-insurance',function(){
    not_release_id = $(this).attr('data-id');
    table_id = $(this).closest('table').attr('data-tabid');
    $('#insurance-not-release-modal').modal();
    $('.insurance-not-release-comment').val("")
});

$('body').on('click','.insurance-not-release-submit',function(){
    comment = $('.insurance-not-release-comment').val();
    $.ajax({
          type : 'POST',
          url : url_insurance_mark_as_notrelease,
          data : {id:not_release_id, _token : _token, comment:comment},
          success : function (data) {
                location.reload();
          }
      });
});

$('body').on('click', '.delete-insurance-tab-detail', function() {
    var url = $(this).attr('data-url');

    if(confirm('Are you sure want to delete this record?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    location.reload();
                }else{
                  alert(result.message);
                }
            },
            error: function(error){
                alert('Somthing want wrong!');
            }
        });
    }
    return false;
});

$('body').on('change', '.am_falk_status', function() {
    var field = $(this).attr('data-field');
    var status = ($(this).prop('checked')) ? 1 : 0;
    var url = $(this).attr('data-url');

    $.ajax({
        url: url,
        type: 'POST',
        data:{
            'field' : field,
            'status' : status
        },
        dataType: 'json',
        success: function(result) {
            if(!result.status){
                alert(result.message);
            }
        },
        error: function(error){
          alert('Somthing want wrong!');
        }
    });

});

// $('#modal-forward-to').find('select[name="user"]').select2();

var reload_forward_to = false;
$('body').on('click','.btn-forward-to',function(){
    var property_id = $(this).attr('data-property-id');
    var subject = $(this).attr('data-subject');
    var content = $(this).attr('data-content');
    var title = $(this).attr('data-title');
    var reload = $(this).attr('data-reload');
    var email = $(this).attr('data-email');
    var user = $(this).attr('data-user');
    var section_id = $(this).attr('data-id');
    var section = $(this).attr('data-section');

    reload_forward_to = (reload == "1") ? true : false;
    section = (typeof section !== 'undefined') ? section : '';

    if(typeof email !== 'undefined' && email){
        $('#modal-forward-to').find('input[name="email"]').val(email);
    }

    if(typeof user !== 'undefined' && user){
        $('#modal-forward-to').find('select[name="user"]').val(user).trigger('change');
    }

    $('#modal-forward-to').find('input[name="property_id"]').val(property_id);
    $('#modal-forward-to').find('input[name="subject"]').val(subject);
    $('#modal-forward-to').find('input[name="content"]').val(content);
    $('#modal-forward-to').find('input[name="title"]').val(title);
    $('#modal-forward-to').find('input[name="section_id"]').val(section_id);
    $('#modal-forward-to').find('input[name="section"]').val(section);

    $('#modal-forward-to').modal('show');
});

$('#form-forward-to').submit(function(event) {
    event.preventDefault();
    var $this = $(this);
    var data = new FormData($(this)[0]);
    var url = $(this).attr('action');

    var email = $($this).find('input[name="email"]').val();
    var user = $($this).find('select[name="user"]').val();

    if(email || user){

      $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',
          processData: false,
          contentType: false,
          cache: false,
          beforeSend: function() {
              $($this).find('button[type="submit"]').prop('disabled', true);
          },
          success: function(result){
              $($this).find('button[type="submit"]').prop('disabled', false);

              if(result.status == true){
                  $('#modal-forward-to').modal('hide');
                  $($this)[0].reset();
              }else{
                  alert(result.message);
              }
          },
          error: function(error){
              $($this).find('button[type="submit"]').prop('disabled', false);
              alert('Somthing want wrong!');
          }
      });

    }else{
      alert('Please select user or enter email!');
    }
});
/*---------------------------------Open Angebote button js---------------------------------------------*/


/*---------------------Auto complete for multiple comment - START-------------------------*/
var default_ele = $('.property-comment, #insurance_detail_comment');
makeAutoComplete(default_ele);

function makeAutoComplete(obj){
  $(obj).suggest('@', {
      // data: users,
      data: function( request, response ) {
          $.ajax({
              url : url_get_all_users,
              type: "POST",
              dataType: "json",
              beforeSend: function() {},
              success: function(res){
                if(res.status == true){
                  response(res.data);
                }else{
                }
              }
          });
      },
      map: function(user) {
          return {
            value: user.email,
            text: '<strong>'+user.email+'</strong>'
          }
      },
      onshow: function(e) {
      },
      onselect: function(e, item) {
          // console.log(item);
      },
      onlookup: function(e, item) {
      }
  });
}

/*---------------------Auto complete for multiple comment - ENd-------------------------*/

$('body').on('click','.mark-as-pending-am',function(){
    inv_id = $(this).attr('data-id');
    inv_property_id = $(this).attr('data-property_id');
    $('#pending-modal-am').modal();
    $('.invoice-pending-am-comment').val("")
});

$('body').on('click','.invoice-pending-am-submit',function(){
    comment = $('.invoice-pending-comment').val();
    $.ajax({
        type : 'POST',
        url : url_property_invoicemarkpendingam,
        data : {
            id:inv_id, 
            _token : _token, 
            comment:comment,
            property_id:inv_property_id
        },
        success : function (data) {
            loadTable(true);
        }
      });
});

$('body').on('click','.vacancy_check_all',function(){
    var cells = table_obj.cells( ).nodes();
    $( cells ).find('.vacancy_check').prop('checked', $(this).is(':checked'));
});

$('body').on('click','#btn-export-vacancy',function(){
    var checkedArr = [];
    var cells = table_obj.cells( ).nodes();
    $( cells ).find('.vacancy_check').each(function(){
      if($(this).is(':checked')){
        checkedArr.push($(this).val());
      }
    });
    if(checkedArr.length){
      var myJSON = JSON.stringify(checkedArr);
      $('#modal-vacancy-export').find('input[name="ids"]').val(myJSON);
      $('#modal-vacancy-export').modal('show');
    }else{
      alert('Please select at least one row!');
      return false;
    }
});

$('#form-vacancy-export').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var formData = new FormData($(this)[0]);
    var url = $(this).attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
            $($this).find('button[type="submit"]').html('Senden <i class="fa fa-spinner fa-spin" style="font-size: 20px;"></i>');
        },
        success: function(result) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            $($this).find('button[type="submit"]').html('Senden');
            if (result.status) {
              
              $($this).find('input[name="email"]').val('');
              $($this).find('textarea[name="message"]').val('');

              $('#modal-vacancy-export').modal('hide');
            }else{
              alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            $($this).find('button[type="submit"]').html('Senden');
            alert('Somthing want wrong!');
            console.log({error});
        }
    });
}));
$('body').on('click','#vacancy_search',function(){
  var start = $('#vacancy_start').val();
  var end = $('#vacancy_end').val();
  var url = $('#leerstände').attr('data-url');
  
  load_url = url+'&start='+start+'&end='+end;
  loadTable(true);
});

/*---------------------NEW - JS FOR RELEASE INVOICE - START----------------*/

$('body').on('click', '.btn-inv-release-am', function(){

    var $this = $(this);

    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    var data = {'id': id, 'status': status, 'comment': ''};

    $.ajax({
        url: url_invoice_release_am,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status) {
                loadButtonProcessTable();
            }else{
                alert(result.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

$('body').on('click', '.btn-inv-release-hv', function(){

    var $this = $(this);

    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    var data = {'id': id, 'status': status, 'comment': ''};

    $.ajax({
        url: url_invoice_release_hv,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status) {
                loadButtonProcessTable();
            }else{
                alert(result.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

var inv_usr_release_id;
var inv_usr_release_status;

$('body').on('click', '.btn-inv-release-usr', function(){
    var $this = $(this);
    var modal = $('#invoice-release-usr-modal');

    inv_usr_release_id = $(this).attr('data-id');
    inv_usr_release_status = $(this).attr('data-status');
    var title = $(this).attr('title');

    if(inv_usr_release_status == '1'){
        
        $(modal).find('.modal-title').text(title);
        $(modal).modal('show');
        
    }else{

        var data = {'id': inv_usr_release_id, 'status': inv_usr_release_status, 'comment': ''};

        $.ajax({
            url: url_invoice_release_user,
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(result) {
                if (result.status) {
                    loadButtonProcessTable();
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }
});

$('#invoice-release-usr-form').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var modal = $('#invoice-release-usr-modal');
    var url = $($this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var user_id = $($this).find('select[name="user"]').val();

    var data = {
        'id': inv_usr_release_id, 
        'status': inv_usr_release_status, 
        'comment': '',
        'user_id': user_id
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);

            if (result.status) {

                $($this)[0].reset();
                $('#invoice-release-usr-error').empty();
                $(modal).modal('hide');

                loadButtonProcessTable();

            }else{
              showFlash($('#invoice-release-usr-error'), result.message, 'error');
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            showFlash($('#invoice-release-usr-error'), 'Somthing want wrong!', 'error');
        }
    });
}));

$('body').on('click', '.btn-inv-release-falk', function(){

    var $this = $(this);

    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    var data = {'id': id, 'status': status, 'comment': ''};

    $.ajax({
        url: url_invoice_release_falk,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status) {
                loadButtonProcessTable();
            }else{
                alert(result.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

function loadButtonProcessTable(){
    if($('#table-data-modal').is(':visible')){
      loadTable(true);
    }else{
      location.reload();
    }
}

/*---------------------NEW - JS FOR RELEASE INVOICE - END----------------*/


$('body').on('click', '.btn-delete-property-invoice', function() {
    var id = $(this).attr('data-id');
    var type = $(this).attr('data-type');
    if (confirm('Are you sure want to delete this invoice?')) {
        var url = url_delete_property_invoice;
        url = url.replace(':id', id);
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                  loadTable(true);
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    } else {
        return false;
    }
});
