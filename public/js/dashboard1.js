var tr = "";
var vacant_release_type= 'release2';
var invoice_id= "";
var property_id= "";

function getassetmanagerprovision(asset_m_id){

    m = $('.pp-month').val();
    y = $('.pp-year').val();

    var url = url_get_property_provision+"?type=4&month="+m+"&year="+y+'&asset_m_id='+asset_m_id;

    $.ajax({
        url: url,
    }).done(function (data) { 
        if(asset_m_id != 0){

          	$('.list-data').html(data);

          	var columns = [
            	null,
            	null,
            	null,
            	{ "type": "numeric-comma", "class": "text-right" },
            	null,
            	{ "type": "numeric-comma", "class": "text-right" },
            	{ "type": "numeric-comma", "class": "text-right" }
        	];
        	makeDatatable($('#property-provision-list'), columns);

        }else{
            	$('.property-provision-div-4').html(data);
            	var columns = [
                	null,
                	null,
            	];
            	makeDatatable($('#property-provision-table-4'), columns);
        }
    });
}
getassetmanagerprovision(0);

function loadAllStatusLoiByUser(user_id) {
   	var url = url_getStatusLoiByUserIdDashboard+"?id="+user_id;
   	$.ajax({
       url: url,
   	}).done(function (data) {
       $('#load_all_status_loi_by_user_content').html('');
       $('#load_all_status_loi_by_user_content').html(data);
       $('#load_all_status_loi_by_user').modal('show');
   	});
}

function refreshLoiTablebyMonth() {
   	const month = $('#loi-month').val();
   	var url = url_getstatusloidahboard+'/'+month;

   	$.ajax({
       url: url,
   	}).done(function (data) {
       $('.LOI-list').html(data);
       makeDatatable($('#list-statusloi'));
       $('.loi-list-total').html($('.LOI-list').find('.list-statusloi-sum').val());
   	});
}

function getOpenInvoice(){

   	var url = url_getPropertyInvoice+"?";
   	$.ajax({
       url: url,
   	}).done(function (data) { 
       	$('.property-invoice-div').html(data);
       	$('.invoiceTotal').html($('.unpaidinvoice').val());
       	var columns = [
           	null,
           	null,
            null,
           	null,
            { "type": "new-date" },
           	{ "type": "numeric-comma" },
           	{ "type": "new-date-time" },
           	null,
            null,
           	null,
           	null,
           	null,
            null,
       	];
       	makeDatatable($('#table-property-invoice'),columns, 'desc', 5);
   	});
}

getInsurance(1);//for GEBÄUDE 
getInsurance(2);//for HAFTPFLICHT
getManagement();

function getInsurance(type){

    var url = url_getInsurance+"?type="+type;
    $.ajax({
       url: url,
    }).done(function (data) { 
        $('.insurance_type_'+type+'_div').html(data);
        if(type==1)
        $('.gebaTotal').html($('.release_building').val());
        else
        $('.haftTotal').html($('.release_liability').val());
        
        // $('.invoiceTotal').html($('.unpaidinvoice').val());
        var columns = [
            null,
            null,
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            null
        ];
        makeDatatable($('.all_insurance_table_'+type),columns);
    });
}
function getManagement(){

    var url = url_getManagement;
    $.ajax({
       url: url,
    }).done(function (data) { 
        $('.release-management-div').html(data);
        $('.hausTotal').html($('.release_management').val());
        var columns = [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            null,
            null,
            null
        ];
        makeDatatable($('.property-management-table'),columns);
    });
}
$('body').on('click', '.change-property-manage-status', function() {
        v = 0;
        if ($(this).is(':checked')) v = 1;
        data = {
            property_id: $(this).attr('data-property_id'),
            pk: $(this).attr('data-column'),
            value: v,
            _token: _token,
            id: $(this).attr('data-id')
        };
        $this = $(this);
        $.ajax({
            url: url_property_management_update,
            type: "post",
            data: data,
            success: function(response) {
                // $('.management-table-list').html(response.table);
                // init_manage_table();
            }
        });
    });

function drawchart(cat,mv,vl,sum){
    Highcharts.chart('high-container', {
	    chart: {
	       	type: 'column'
	    },
	    title: {
	       	text: 'Mietverträge MV & VL'
	    },
	    xAxis: {
	       	categories: cat,
	       	crosshair: true
	    },
	    yAxis: {
	       	min: 0,
	       	title: {
	         	text: 'Miete  p.a.'
	       	}
	    },
	    tooltip: {
	       	headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	       	pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	         '<td style="padding:0"><b>{point.y:.1f} €</b></td></tr>',
	       	footerFormat: '</table>',
	       	shared: true,
	       	useHTML: true
	    },
	    plotOptions: {
	       	column: {
	        	pointPadding: 0.2,
	        	borderWidth: 0
	       	}
	    },
	    series: [{
	       	name: 'MV',
	       	data: mv

	    }, {
	       	name: 'VL',
	       	data: vl
	    }, {
	       	name: 'Gesamt',
	       	data: sum
	    }]
	});
}

function drawchart2(container,cat,mv,vl){
	Highcharts.chart(container, {
     	chart: {
       		type: 'column'
     	},
     	title: {
       		text: 'Mietverträge & Verlängerungen'
     	},
     	xAxis: {
       		categories: cat,
       		crosshair: true
     	},
     	yAxis: {
       		min: 0,
       		title: {
         		text: 'Anzahl'
       		},
       		stackLabels: {
           		enabled: true,
           		style: {
               		fontWeight: 'bold',
               		color: ( // theme
                   		Highcharts.defaultOptions.title.style &&
                   		Highcharts.defaultOptions.title.style.color
               		) || 'gray'
           		}
       		}
    	},
    	tooltip: {
       		headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
       		pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
         '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
       		footerFormat: '</table>',
       		shared: true,
       		useHTML: true
    	},
    	plotOptions: {
       		column: {
         		pointPadding: 0.2,
         		borderWidth: 0,
         		dataLabels: {
               		enabled: true
           		}
       		}
    	},
    	series: [{
       		name: 'MV',
       		data: mv
    	},
    	{
       		name: 'VL',
       		data: vl
    	}]
   	});
}

function drawchart3(container,cat,mv,vl){
    Highcharts.chart(container, {
        chart: {
            type: 'column'
        },
	    title: {
	       	text: 'MIETERÜBERSICHT'
	    },
        xAxis: {
           	categories: cat,
           	crosshair: true
        },
        yAxis: {
           	min: 0,
           	title: {
             	text: 'Anzahl'
           	},
           	stackLabels: {
               	enabled: true,
               	style: {
                   	fontWeight: 'bold',
                   	color: ( // theme
                       	Highcharts.defaultOptions.title.style &&
                       	Highcharts.defaultOptions.title.style.color
                   	) || 'gray'
               	}
           	}
        },
        tooltip: {
           	headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
           	pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
             '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
           	footerFormat: '</table>',
           	shared: true,
           	useHTML: true
        },
	    plotOptions: {
	       	column: {
	         	pointPadding: 0.2,
	         	borderWidth: 0,
	         	dataLabels: {
	               	enabled: true
	           	}
	       	}
	    },
        series: [{
           	name: 'Miete netto p.m.',
           	data: mv
        },
        {
           	name: 'Anzahl',
           	data: vl
        }]
    });
}

function moveTodiv(id){
   	$('html, body').animate({
    	scrollTop: $("#"+id).offset().top
   	}, 1000);
}

function getcategoryproperties(){
   	$.ajax({
       	url: url_getcategoryproperty,
   	}).done(function (data) { 
       	// $('.category-prop-list1').html(data.table1);
       	$('.category-prop-list2').html(data.table2);

       	drawchart3('high-container3',data.name,data.actual_net_rent,data.count);

       	var columns = [
           	null,
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	null,
           	{ "type": "numeric-comma" },
       	];

       	makeDatatable($('.category-properties'), columns, "desc", 1);
       	$('.categTotal').html($('.ajax-total-category').html());
   	});
}
   
function getcategoryproperties4(){
   	$.ajax({
       	url: url_getcategoryproperty3,
   	}).done(function (data) { 
       	// $('.category-prop-list1').html(data.table1);
       	$('.category-prop-list4').html(data.table);
       	$('.category-prop-list5').html(data.table2);
       	$('.category-prop-list6').html(data.table3);
       	var columns = [
           	null,
           	null,
           	{ "type": "numeric-comma" }
       	];
       	makeDatatable($('.category-table4'), columns, "desc", 1);
       	makeDatatable($('.category-table6'), columns, "desc", 2);
   	});
}

function getcategorypropertieslist(category,type,check){
   	$('.cate-pro-div').html("");
   	$('#cate-pro-listing').modal();
	var url = url_getcategoryproperty+"?category="+category+'&type='+type+'&rent_closed='+check;
    $.ajax({
        url: url,
    }).done(function (data) {
       	$('.cate-pro-div').html(data);
       	var columns = [
           	null,
           	null,
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	null,
           	null,
       	];
       	makeDatatable($('.category-table'), columns,'desc',2);
   });

}

function getcategorypropertieslist2(category,name){
   	$('.cate-pro-div').html("");
   	$('#cate-pro-listing').modal();
   	var url = url_getcategoryproperty3+"?category="+category+"&name="+name;
   	$.ajax({
       url: url,
   	}).done(function (data) {
       	$('.cate-pro-div').html(data);
       	if(category == ''){
          	var columns = [
           		null,
           		null,
           		null,
           		null,
           		{ "type": "numeric-comma" },
       			  null,
           		null,
           		null,
       		];
       		makeDatatable($('.name-rent-table'), columns,'desc',3);
       	}else{
          	var columns = [
           		null,
           		null,
           		null,
           		{ "type": "numeric-comma" },
              { "type": "numeric-comma" },
           		null,
           		null,
              null,
       		];
       		makeDatatable($('.category-table'), columns,'desc',2);
       	}
   });
}

function getcategorypropertieslist3(category){
   	$('.cate-pro-div').html("");
   	$('#cate-pro-listing').modal();
    var url = url_getcategoryproperty3+"?category2="+category;
   	$.ajax({
       	url: url,
   	}).done(function (data) {
       $('.cate-pro-div').html(data);
       	var columns = [
           	null,
           	null,
       	];
       	makeDatatable($('.category-table'), columns,'desc',0);
   	});

}

function getPendingNotReleaseInvoice(status){

 	var url = url_getPendingNotReleaseInvoice+"?status="+status;
   	$.ajax({
    	url: url,
   	}).done(function (data) { 
       	if(status==1)
       		$('.property-notrelease-invoice-div').html(data);
       	else
       		$('.property-pending-invoice-div').html(data);
       	var columns = [
           	null,
           	null,
            null,
            { "type": "new-date" },
           	{ "type": "numeric-comma" },
           	null,
           	null,
            null,
           	{ "type": "new-date-time" },
           	null,
            null,
       	];
        $('.invoice-count-'+status).html($(".invoice-class-"+status).val());

       	makeDatatable($('#table-property-invoice'+status),columns, 'desc', 6);

       	if ($('.invoice-comment').length) 
        	$('.invoice-comment').editable();
   	});
}

$('#default_payer .hide-checkbox').click(function(){
  id = $(this).attr('data-id');
  if(id==1)
  {
    $('#default_payer table tr').each(function(){
      if($(this).find('.ck').length>0)
        $(this).addClass('hidden');
    })
    $(this).attr('data-id',0)
  }
  else{
      $('#default_payer table tr').each(function(){
          $(this).removeClass('hidden');
      }) 
    $(this).attr('data-id',1)
  }
})

function getdbayer(){
    m = $('.bb-month').val();
    y = $('.bb-year').val();
    var url = url_getPropertyDefaultPayers+"?month="+m+"&year="+y;

    $.ajax({
        url: url,
    }).done(function (data) { 
        $('.property-default-payer-div').html(data);
        var columns = [
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
            null,
            null,
            null,
        ];

        // var counter = $('.input-default_payerTotal').val()+'-'+$('.input-default_payerTotal_second_last').val()+'-'+$('.input-default_payerTotal').val();
        
        $('.default_payerTotal').html($('.input-default_payerTotal').val());
        $('.default_payerTotal-1').html($('.selected-input-default_payerTotal').val());
        makeDatatable($('#property-default-payer-table'), columns, 'asc', 2);
    });
}
function getPropertyInsuranceTab()
{ 
  var url = url_getPropertyInsuranceTab;
    $.ajax({
       url: url,
    }).done(function (data) { 
        $('.insurance_tab_div').html(data);

        $('.angebot-release-count').html($('.property-insurance-release-request').length);

        if($(".tbl-insurance-tab-detail:not(.hidden-table)").length > 0){
          $(".tbl-insurance-tab-detail:not(.hidden-table)").each(function(){
              var tabid = $(this).attr('data-tabid');
              makeInsuranceDataTable(tabid);
          });
      }

    });
}
function makeInsuranceDataTable(tabid){
    var url = url_get_property_insurance_detail.replace(":tab_id", tabid);
    $('#insurance-table-'+tabid).dataTable({
        "ajax": {
            "url" : url,
            // "async": false
        },
        /*"order": [
            [0, "desc"]
        ],*/
        "columns": [
            null,
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            null,
            null,
            null,
        ]
    });
}
$( document ).ready(function() {
    
	$('.pchange-select').change(function(){
        getassetmanagerprovision(0);            
    });

    $('body').on('click','.get-provision-property',function(){
      	$('.list-data').html("");
      	id = $(this).attr('data-id');
      	getassetmanagerprovision(id);
      	$('#property-list').modal();
    });

    $('body').on('click','.invoice-release-request',function(){
       	tr = $(this).closest('tr');
       	vacant_release_type= 'release2';
       	invoice_id= $(this).attr('data-id');
       	property_id= $(this).attr('data-property_id');
       	$('.invoice-release-comment').val("")
       	$('#invoice-release-property-modal').modal();
   	});

   	$('body').on('click','.invoice-release-submit',function(){
       	comment = $('.invoice-release-comment').val();
       	$.ajax({
            type : 'POST',
            url : url_property_invoicereleaseprocedure,
            data : {
            		id:invoice_id,
            		step:vacant_release_type,
            		_token : _token,
            		comment:comment,
            		property_id:property_id
            	},
            success : function (data) {
                 tr.remove();      
            }
        });
     });
   
   
   	$('.btn-export-to-excel').on('click', function () {
       	var propertyTableArray = getTableData("#hausmax_table");
       	$('input#property-data').val(JSON.stringify(propertyTableArray));
       	$('#export-to-excel-form').submit();
   	});
   
   	makeDatatable($('#banken'), [], 'asc', 0, 10, true);
   
   	makeDatatable($('.verkauf-table'), [], 'desc', 1);

   	$('body').on('click', '.send-mail', function() {
       	var $this = $(this);
       	var email = $($this).attr('data-email');
       	var name  = $.trim($($this).closest('tr').find('td:first').text());
   
       	$('#send-mail-to-user-modal').find('#send-mail-to-name').html(name);
       	$('#send-mail-to-user-modal').find('input[name ="name"]').val(name);
       	$('#send-mail-to-user-modal').find('input[name="email"]').val(email);
       	$('#send-mail-to-user-modal').modal('show');
   	});

   	var columns = [
       	null,
       	null,
       	null,
       	null,
       	{ "type": "numeric-comma" },
       	{ "type": "new-date" },
       	null,
       	null,
   	];
   	makeDatatable($('.class-data-table'), columns, "asc", 5);

   	$.ajax({url: url_getMahnungAmount}).done(function (data) { 
        $('.mahnungen-30-days').html(data);
    });

    $('.tenancy_item').click(function(){
    	$('#tenant-detail-modal').modal();
        id = $(this).attr('data-id');
        var url = url_tenant_detail+"?id="+id;
        $.ajax({
               url: url,
        }).done(function (data) { 
            $('.tenant-detail').html(data);
       	});
    });
   
   
   	$('body').on('click','.get-rents',function(){
       	$('.list-data-rent').html("");
       	$('.vlist-data-rent').html("");

       	id          = $(this).attr('data-value');
       	property_id = $(this).attr('data-property');
       	type        = $(this).attr('data-type');

       	if(type == "r")
           	$('#rent-list').modal();
       	else
           	$('#vacant-list').modal();

       	var url = url_assetmanagement+"?asset_manager="+id+"&property_id="+property_id+"&type="+type;

       	$.ajax({
           	url: url,
       	}).done(function (data) { 
           	if(type=="r"){
               	$('.list-data-rent').html(data);
           	}else{
               	$('.vlist-data-rent').html(data);
               	var columns = [
                   	null,
                   	null,
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" }
               	];
               	makeDatatable($('#list-rent'), columns);
           	}
       });
   	});
   
   	$('body').on('click','.get-tenant-mahnung',function(){
       	$('.get-tenant-mahnung-list-data').html("");
       	id= $(this).attr('data-id');

       	$.ajax({
           	url: url_getMahnungTenant+"?id="+id,
       	}).done(function (data) { 
           	$('.get-tenant-mahnung-list-data').html(data);
           	var columns = [
               	null,
               	{ "type": "numeric-comma" },
               	{ "type": "new-date" },
               	{ "type": "new-date" },
               	{ "type": "new-date" },
           	];
           	makeDatatable($('#mahnung-tenants'), columns);
       	});
    });

    $('body').on('click','.get-asset-property',function(){
        $('.list-data').html("");
        id = $(this).attr('data-value');
        $('#property-list').modal();

        $.ajax({
               url: url_assetmanagement+"?id="+id,
        }).done(function (data) { 
            $('.list-data').html(data);
            var columns = [
                null,
               	null,
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
           	];
           	makeDatatable($('#list-properties'), columns);
       });
    });
   
   
   	$('.change-select').change(function(){
       	m = $('.pm-month').val();
       	y = $('.pm-year').val();

       	var url = url_monthyearmanager+"?month="+m+"&year="+y;
        $.ajax({
        	url: url,
       	}).done(function (data) { 
           	$('.manager-property-list').html(data);
           	var columns = [
               	null,
               	null,
               	{ "type": "numeric-comma" },
               	null,
               	{ "type": "numeric-comma" },
               	null,
               	{ "type": "numeric-comma" },
               	null,
               	{ "type": "numeric-comma" },
               	null,
               	{ "type": "numeric-comma" },
               	null,
               	{ "type": "numeric-comma" },
               	null,
               	{ "type": "numeric-comma" }
           	];
            makeDatatable($('#added-month'), columns, "asc", 0, 50);
            $('.angebotTotal').text($('.total_mv_vl').html());
       	});
   	});
   
   	$('.achange-select').change(function(){
       	m = $('.pa-month').val();
       	y = $('.pa-year').val();
       	t = $('.type-selection').val();

       	var url = url_monthyearassetmanager+"?month="+m+"&year="+y+"&type="+t;
        $.ajax({
            url: url,
        }).done(function (data) { 
           	$('.asset-manager-property-list').html(data.table);
           	$('.asset-manager-property-list-total').html(data.subtable);

           	var columns = [
               	null,
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" }
           	];
           	makeDatatable($('#added-month-asset'), columns, "asc", 0, 25);

           	drawchart(data.cat,data.mv,data.vl,data.sum);
           	drawchart2('high-container2',data.m_arr,data.m_arr1,data.m_arr2);
       	});
   	});
   
   	$('body').on('click','.get-manager-property',function(){
       	$('.list-data').html("");
       	id= $(this).attr('data-id');
       	m = $('.pm-month').val();
       	y = $('.pm-year').val();
       	status= $(this).attr('data-status');

       	var url = url_getmanagerproperty+"?id="+id+"&month="+m+"&year="+y+"&status="+status;
       	$.ajax({
           	url: url,
       	}).done(function (data) { 
           	$('.list-data').html(data);

           	if(status == 12){
               	var columns = [
                   	null,
                   	null,
                   	{ "type": "new-date" },
                   	null,
                   	null,
                   	null,
                   	null,
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	null
               	];
               	makeDatatable($('#list-properties'), columns, "asc", 2);
           	}else if (status==14 || status==10) {
               	var columns = [
                   	null,
                   	null,
                   	{ "type": "new-date" },
                   	null,
                   	null,
                   	null,
                   	null,
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	null
               	];
               	makeDatatable($('#list-properties'), columns, "asc", 2);
           	}else{
               	var columns = [
                   	null,
                   	null,
                   	{ "type": "new-date" },
                   	{ "type": "new-date" },
                   	null,
                   	null,
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	null
               	];
               	makeDatatable($('#list-properties'), columns, "asc", 2);
           	}
       	});
   	});

   	$('body').on('click','.get-assetmanager-property',function(){
        $('.asset-list-data').html("");
        id= $(this).attr('data-id');
   
        m = $('.pa-month').val();
        y = $('.pa-year').val();
   
        t = $(this).attr('data-type');
   
        var url = url_getassetmanagerproperty+"?id="+id+"&month="+m+"&year="+y+"&type="+t;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.asset-list-data').html(data);
   
            var columns = [
               	null,
               	null,
               	null,
               	null,
               	{ "type": "new-date" },
               	{ "type": "new-date" },
               	{ "type": "new-date" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" }
           	];
            makeDatatable($('#tenant-table_22'), columns);
        });
    });
   
   	$('body').on('click','.get-name-insurance',function(){
       	id = $(this).attr('data-id');
       	t = encodeURIComponent($(this).html());

        var url = url_getInsurancePropertyList+"?axa="+id+'&title='+t;
        $.ajax({
           	url: url,
       	}).done(function (data) { 
           	$('#iproperty-list').modal();
           	$('.insurance-list-data').html(data);
           	setTimeout(function(){
               	var columns = [
                   	null,
                   	null,
                   	null,
                   	null,
                   	{ "type": "numeric-comma" }
               	];
               	makeDatatable($('#insurance-property-table'), columns);
           	},1000);
       	});  
   	});

   	 /////////////////// Hausmaxx Table Modal //////////////////
   	$('body').on('click','.get-hausmaxx-data',function(){
       	id = $(this).attr('data-id');
       	t = encodeURIComponent($(this).html());
        var url = url_get_hausmaxx_data+"?id="+id+'&title='+t;
        $.ajax({
           	url: url,
       	}).done(function (data) { 
           	$('#hausmax-list').modal();
           	$('.hausmax-list-data').html(data);
           	setTimeout(function(){
               	var columns = [
                   	null,
                   	null,
                   	null,
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
                   	{ "type": "numeric-comma" },
               	];
               	makeDatatable($('#hausmaxx_list_table'), columns);
           	},1000);
       	});
   	});

   	$('body').on('click','.checkbox-is-sent', function () {
       	var id = $(this).attr('data-id');
       	var pk = $(this).attr('data-column');
       	v = 0;
       	if($(this).is(':checked'))
           	v = 1;
   
       	$.ajax({
           	type : 'POST',
           	url : url_statusloi_update+"/"+id,
           	data : { 
           			pk : pk, 
           			value:v, 
           			_token : _token 
           	},
           	success : function (data) {
   
           	}
       	});
   	});

   	$('body').on('click','.get-category-properties',function(){
       	category = $(this).attr('data-category');
       	type = $(this).attr('data-type');
       	check = $(this).attr('data-rent-closed');
       	getcategorypropertieslist(category,type,check)
   	});

   	$('body').on('click','.get-category-properties3',function(){
       	category = $(this).attr('data-category');
       	getcategorypropertieslist2(category,'')
   	});

   	$('body').on('click','.get-category-properties4',function(){
       	category = $(this).attr('data-category');
       	getcategorypropertieslist3(category)
   	});

   	$('body').on('click','.get-name-properties',function(){
       	name = $(this).attr('data-category');
       	category = "";
       	getcategorypropertieslist2(category,name)
   	});

   	getOpenInvoice();

   	$.ajax({
       url: url_releaselogs,
   	}).done(function (data) { 
       $('.vlog').html(data);
       $('.ereleaseTotal').html($('.ereleasecount').val());
       makeDatatable($('#vlog-table'));
   	});
   
   
   	$.ajax({
       	url: url_getbankproperty,
   	}).done(function (data) { 
       	$('.bank-prop-list').html(data);
       	var columns = [
           	null,
           	null,
           	{ "type": "numeric-comma" },
           	{ "type": "new-date" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" }
       	];
       	makeDatatable($('#bank-properties'), columns);
       	$('.bankenfinanTotal').html($('.total-delta2').html())
   	});

   	getcategoryproperties();
   	getcategoryproperties4();
	   
   	$.ajax({
       	url: url_vreleaselogs,
   	}).done(function (data) { 
       	$('.vklog').html(data);
       	$('.vreleaseTotal').html($('.vreleasecount').val());
       	makeDatatable($('#vlog-table2'));
   	});
   
   	m = $('.pa-month').val();
   	y = $('.pa-year').val();
   	t = $('.type-selection').val();

   	$.ajax({
       	url: url_monthyearassetmanager+"?month="+m+"&year="+y+"&type="+t,
   	}).done(function (data) { 
       	$('.mv-count').html($(data.table).find('.mv-total').text());
       	$('.mv-total-gesamt').html($(data.table).find('.mv-total-gesamt').text());
   	});
   
   	var columns = [
       	null,
       	null,
       	{ "type": "numeric-comma" },
       	{ "type": "numeric-comma" },
       	{ "type": "numeric-comma" }
   	];
   	makeDatatable($('#hausmax_table'), columns, "desc", 2);

   	$.ajax({
       url: url_getstatusloidahboard_month,
   	}).done(function (data) {
       $('.LOI-list').html(data);
       makeDatatable($('#list-statusloi'));
       $('.loi-list-total').html($('.LOI-list').find('.list-statusloi-sum').val());
   	});

   	var url = url_assetmanagement+"?id=-1";
   	$.ajax({
       	url: url,
  	}).done(function (data) {
       	$('.table-not-beur-div').html(data);
       	var columns = [
           	null,
           	null,
           	null,
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
       	];
       	makeDatatable($('#list-properties-1'), columns);
        $('.vobj-total').html($('.not-beur-sum').val());
    });
   
   	$('.comment-text').change(function(){
       var url = url_change_comment+"?";
       var data = {
           _token : _token,
           comment : $(this).val(),
           id:$(this).attr('data-id'),
       };
        $.ajax({
           url: url,
           type:'POST',
           data:data,
       }).done(function (data) { 
           alert(data);                        
       });
   	});
   
   	$('.comment-file').change(function(){
       	var formdata = new FormData();
       	var url = url_change_comment+"?";
       	file =$(this).prop('files')[0];
       	formdata.append("file", file);
       	formdata.append('_token', _token);
       	formdata.append('id', $(this).attr('data-id'));

       	$.ajax({
           	url: url,
           	type:'POST',
           	contentType: false,
           	processData: false,
           	data:formdata,
       	}).done(function (data) { 
           	alert(data);                        
       	});
   	});
   
   	$('.get-bank-list').click(function(){
       	
       	$('#bank-list').modal();
       	property_id = $(this).attr('data-id');
       	var url = url_getbanklist+"?id="+property_id;

       	$.ajax({
        	url: url,
       	}).done(function (data) {
           	$('.list-data-banks').html(data);
           	var columns = [
               	null,
               	null,
               	null,
               	null,
               	null,
              	null,
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	{ "type": "numeric-comma" },
               	null,
               	null,
           	];
           	makeDatatable($('#list-banks'), columns);
       	});
   	});
           
   	$('.get-verkauf-user').click(function(){

       	$('#user-list-popup').modal();
       	$('.user-list').html("");
       	var active= $(this).attr('data-active');

       	$.ajax({
           url: url_getusers,
           data:{active:active,domain:1},
       	}).done(function (data) { 
           $('.user-list').html(data);
           makeDatatable($('.user-list table'));
       	});
   	});
   
   	$('.get-vermietung-user').click(function(){
       	$('#user-list-popup').modal();
       	$('.user-list').html("");

       	var active= $(this).attr('data-active');

       	$.ajax({
           url: url_getusers,
           data:{active:active,domain:2},
       	}).done(function (data) { 
           $('.user-list').html(data);
           makeDatatable($('.user-list table'));
       	});
   	});
   
   	$('.comment-detail').click(function(){
       	$('#comment-title').html($(this).html());
       	$('#comment-detail-popup').modal();
       	$('.comment-detail-body').html("");
       	
       	$.ajax({
            url: url_comment_detail,
            data:{id:$(this).attr('data-id')},
        }).done(function (data) { 
            $('.comment-detail-body').html(data);
       	});
   	});
   
   	$('.comment-checkbox').click(function(){

       var status = 0;
       if($(this).is(':checked'))
           status = 1;

       var url = url_change_comment+"?";
       var data = {
           _token : _token,
           status : status,
           id:$(this).attr('data-id'),
       };

        $.ajax({
           	url: url,
           	type:'POST',
           	data:data,
        }).done(function (data) { 
            alert(data);                        
       	});

   	});    
   
   	var url = url_getMahnungProperties+"?";
   	$.ajax({
    	url: url,
   	}).done(function (data) { 
       $('.mahnung-div').html(data);
       var columns = [
           null,
           null,
           { "type": "numeric-comma" },
       ];
       makeDatatable($('#mahnung-properties'), columns, 'desc', 2);
   	});


   	/*var url = url_getInsuranceReleaseLogs+"?";
   	$.ajax({
       	url: url,
   	}).done(function (data) { 
       	$('.building-div').html(data.table1);
       	$('.liability-div').html(data.table2);
       	$('.management-div').html(data.table3);
       	makeDatatable($('.release-table'));

       	$('.gebaTotal').html($('.release_building').val());
       	$('.haftTotal').html($('.release_liability').val());
       	$('.hausTotal').html($('.release_management').val());
   	});*/

   	var url = url_assetmanagement+"?ajax=1";
   	$.ajax({
       	url: url,
   	}).done(function (data) { 
       	$('.assetmanagement-list').html(data);
       	$('.vermi_percent').html($('body').find('#vermi_percent').val());

       	var columns = [
           	null,
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
           	{ "type": "numeric-comma" },
       	];
       	makeDatatable($('#dashboard-table-2'), columns);
   	});
   
   
           
   	var url = url_getPropertyInsurance+"?axa=1";
   	$.ajax({
       	url: url,
   	}).done(function (data) { 
       $('.insurance-div1').html(data[0]);
       $('.axa1Total').html(data[1]);
       setTimeout(function(){
           var columns = [
               null,
               { "type": "numeric-comma" },
               { "type": "numeric-comma" }
           ];
           makeDatatable($('#insurance-table1'), columns, "desc", 2);
       },1000);
   	});

   	var url = url_getPropertyProvision+"?type=1";
   	$.ajax({
    	url: url,
   	}).done(function (data) { 
       $('.property-provision-div').html(data);
       $('.provisionTotal').html($('.provisioncount').val());
       var columns = [
           null,
           null,
           null,
           { "type": "numeric-comma", "class": "text-right" },
           null,
           { "type": "numeric-comma", "class": "text-right" },
           { "type": "numeric-comma", "class": "text-right" },
           null,
       ];
       makeDatatable($('#property-provision-table'), columns);
   	});

   	var url = url_getPropertyInsurance+"?axa=0";
   	$.ajax({
       	url: url,
   	}).done(function (data) { 
       $('.insurance-div2').html(data[0]);
       $('.axa0Total').html(data[1]);

       setTimeout(function(){
           var columns = [
               null,
               null,
               { "type": "numeric-comma" },
           ];
           makeDatatable($('#insurance-table0'), columns, "desc", 2);
       },1000);
	});
   
   	$('.change-select').trigger('change');
   	$('.achange-select').trigger('change');
   
   	$.ajax({
       url: url_mailchimp,
   	}).done(function (data) { 
       $('.mcount').html(data);
   	});
   
   	var columns = [
       	null,
       	null,
       	null,
       	null,
       	{ "type": "new-date" },
       	{ "type": "numeric-comma" },
       	{ "type": "numeric-comma" },
       	null,
       	null,
   	];
   	makeDatatable($('#tenant-table2'), columns, "asc", 4);
   
	var columns = [
	   	null,
	   	null,
	   	null,
	   	null,
	   	{ "type": "new-date" },
	   	{ "type": "numeric-comma" },
	   	{ "type": "numeric-comma" },
	   	null,
	   	null,
	];
    makeDatatable($('#tenant-table3'), columns, "asc", 4);
   
   	var columns = [
       	null,
       	null,
       	null,
       	null,
       	{ "type": "numeric-comma" },
       	{ "type": "numeric-comma" },
       	null,
       	null
   	];
   	makeDatatable($('#tenant-table4'), columns, "desc", 5);

    var columns = [
        null,
        null,
        { "type": "numeric-comma" },
        { "type": "numeric-comma" },
        { "type": "numeric-comma" },
      ];
    makeDatatable($('#sum-tenant-table4'), columns,"desc", 4);

    $('body').on('click','.get-vacancy-list',function(){
        id =$(this).attr('data-id');
        $('#cate-pro-listing').modal();
        var url = url_getvacantlist+"?id="+id;
          $.ajax({
              url: url,
          }).done(function (data) {
              $('.cate-pro-div').html(data);
        
              var columns = [
                null,
                null,
                null,
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                null,
                null
            ];
            makeDatatable($('.vacant-list'), columns, "desc", 5);
          })

    })
   
   
   	$('#assetmanagement').find('a.inline-edit').editable({
       	success: function(response, newValue) {
           	if( response.success === false )
               	return response.msg;
           	else{
               	location.reload();
           	}
       	}
   	});

  	$('body').on('change', '.asset-manager', function() {
      	var am_id = $(this).val();
      	var property_id = $(this).attr('data-property');


      	var url = url_setAssestManagerOnProperty;
      	url = url.replace(':property_id', property_id);
      	url = url.replace(':am_id', am_id);

	    $.ajax({
	        url: url,
	        type: 'GET', 
	        dataType: 'json',
	        success: function(result){
	            if(result.status == false){
	                alert(result.message);
	            }
	        },
	        error: function(error){
	            alert('Somthing want wrong for change AM!');
	        }
	    });
  	});

  	$('.bank-list-to').html($('.sum-bank-list-to').val());
   
   	$('.HAUSVERWALTUNGTOTAL').text($('.HAUSVERWALTUNG').html());
   	$('.sumauTotal').text($('.sumau').html());
   	$('.sumausTotal').text($('.sumaus').html());
    
    // $('.sumauTotal').text("12");
    setTimeout(function(){
        $('.HAUSVERWALTUNGTOTAL').text($('.HAUSVERWALTUNG').html());
        $('.sumauTotal').text($('.sumau').html());
   	},5000);
   
    $('body').on('click','.show-more',function(){
       	$(this).closest('.table-responsive').find('tr').removeClass('hidden');
       	$(this).removeClass('show-more').addClass('hide-more');
       	$(this).html('Show less');
    });

    $('body').on('click','.hide-more',function(){
       	$(this).closest('.table-responsive').find('tr.hide-records').addClass('hidden');
       	$(this).addClass('show-more').removeClass('hide-more');
       	$(this).html('Show more');
    });

    $('body').on('click', '#added-month-asset tbody th:first-child', function () {
        var tr = $(this).closest('tr').next();
        if(tr.hasClass('mv')){
           	if(tr.hasClass('hidden'))
            	tr.removeClass('hidden'); 
           	else
               	tr.addClass('hidden'); 

           	var tr = tr.next();
           	if(tr.hasClass('vl')){
               	if(tr.hasClass('hidden'))
                   	tr.removeClass('hidden'); 
               	else
                   	tr.addClass('hidden'); 
           	}
        }
   	});
   
    vacant_release_type = '';
    vacant_id = "";
  	$('body').on('click','.provision-release-request',function(){
     	tr = $(this).closest('tr');
     	vacant_release_type= $(this).attr('data-column');
     	vacant_id= $(this).attr('data-id');
     	property_id= $(this).attr('data-property_id');
     	$('.provision-release-comment').val("")
     	$('#provision-release-property-modal').modal();
  	});
    
    $('body').on('click','.request-insurance-button',function(){
      tr = $(this).closest('.table-responsive');
      vacant_release_type= $(this).attr('data-type');
      vacant_id= $(this).attr('data-id');
      $('.insurance-release-comment').val("")
      $('#insurance-release-property-modal').modal();
    });
    
    $('body').on('click', '.insurance-release-submit', function() {
        property_id = vacant_id;
        comment = $('.insurance-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_addmaillog,
            data: {
                property_id: property_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment
            },
            success: function(data) {
                tr.remove();
            }
        });
    });

  	$('body').on('click','.provision-release-submit',function(){
      	comment = $('.provision-release-comment').val();
      	$.ajax({
            type : 'POST',
            url : url_property_provisionreleaseprocedure,
            data : {tenant_id:vacant_id,step:vacant_release_type,  _token : _token, comment:comment,property_id:property_id},
            success : function (data) {
                tr.remove();
            }
        });
  	});

  	$.ajax({
       url: url_getSocialFollowers,
       type: 'GET', 
       dataType: 'json',
       success: function(result){
        $('#instagram-followers').html(result.instagram_followers);
        $('#facebook-likes').html(result.facebook_likes);
        $('#website-visitor').html(result.website_visitors);
       },
       error: function(error){
           alert('Somthing want wrong for getting social followers!');
       }
  	});

  	$('body').on('click', '.multiple-invoice-release-request', function() {
     	var selcted_id_invoice = "";
     	arr = [];
     	selcted_id_invoice = "";

      $('.multiple-invoice-release-comment').val("");

     	$('.invoice-checkbox').each(function(){
        	if($(this).is(':checked'))
           		arr.push($(this).attr('data-id'));
     	});

     	selcted_id_invoice = arr.toString();
     
     	// console.log(selcted_id_invoice);
     	if(selcted_id_invoice=="" || selcted_id_invoice==null){
        	alert("Bitte mindestens eine Rechnung auswählen")
     	}
     	else{
        	$('.multiple-invoice-release-comment').val("")
        	$('#multiple-invoice-release-property-modal').modal();
     	}
  	});

  	$('body').on('click', '.multiple-invoice-release-submit', function() {
      	comment = $('.multiple-invoice-release-comment').val();
      	selcted_id_invoice = "";

     	$('.invoice-checkbox').each(function(){
        	if($(this).is(':checked'))
           		arr.push($(this).attr('data-id'));
     	});


     	selcted_id_invoice = arr.toString();
      $('.invoice-checkbox').each(function(){
          if($(this).is(':checked'))
              $(this).closest('tr').remove();
      })
     
      	$.ajax({
          	type: 'POST',
          	url: url_property_invoicereleaseprocedure,
          	data: {
              	id: selcted_id_invoice,
              	step: "release2",
              	_token: _token,
              	comment: comment,
           	},
          	success: function(data) {
              	getOpenInvoice();
                $('.multiple-invoice-release-comment').val("");
          	}
      	});
  	});


   	$('body').on('click','.mark-as-pending',function(){
      not_release_id = $(this).attr('data-id');
      $('#pending-modal').modal();
   	});

   	$('body').on('click','.invoice-pending-submit',function(){
       	comment = $('.invoice-pending-comment').val();
       	$.ajax({
            type : 'POST',
            url : url_property_invoicemarkpending,
            data : {id:not_release_id, _token : _token,comment:comment},
            success : function (data) {
                 // getprovisionbutton();
                 // $('#add_property_invoice_table').DataTable().ajax.reload();
                 // $('#add_property_invoice_table4').DataTable().ajax.reload();    
                 getOpenInvoice();
                 getPendingNotReleaseInvoice(2);
      
            }
        });
    });

   	var not_release_id = 0;
   	$('body').on('click','.mark-as-not-release',function(){
      	not_release_id = $(this).attr('data-id');
      	$('#not-release-modal').modal();
   	});

   	$('body').on('click','.invoice-not-release-submit',function(){
       	comment = $('.invoice-not-release-comment').val();
       	$.ajax({
            type : 'POST',
            url : url_property_invoicenotrelease,
            data : {id:not_release_id, _token : _token, comment:comment},
            success : function (data) {
                 // getprovisionbutton();
                 // $('#add_property_invoice_table').DataTable().ajax.reload();
                 // $('#add_property_invoice_table3').DataTable().ajax.reload();
                  getOpenInvoice();
                  getPendingNotReleaseInvoice(1);
      
                 // $('#invoice_mail_table').DataTable().ajax.reload();
            }
        });
    });
      
	  getPendingNotReleaseInvoice(1);
    getPendingNotReleaseInvoice(2);

    getdbayer(0);
    getPropertyInsuranceTab();

    $('.bchange-select').change(function(){
        getdbayer(0);            
    });

    $('body').on('click', '.versicherunge-checkbox-is-approved', function() {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: url_update_versicherung_approved_status,
            data: {
                pk: pk,
                value: v,
                id: id,
                _token: _token
            },
            success: function(data) {}
        });
    });

    $('body').on('click', '.falk-checkbox-is-checked', function() {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: url_update_falk__status,
            data: {
                pk: pk,
                value: v,
                id: id,
                _token: _token
            },
            success: function(data) {}
        });
    });

    $('body').on('click', '.btn-delete-property-invoice', function() {
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        if (confirm('Are you sure want to delete this invoice?')) {
            var url = url_delete_property_invoice;
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if(type=="request-table")
                    getOpenInvoice();
                    else
                    getPendingNotReleaseInvoice(type);
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });
    
    $('body').on('click', '.asset_manager', function() {
      var inv_property_id = $(this).attr('data-property-id');
      $('#modal_invoice_sendmail_to_am').find('input[name="property_id"]').val(inv_property_id);
      $('#modal_invoice_sendmail_to_am').modal('show');
    });

    $('#form_invoice_sendmail_to_am').on('submit', (function(e) {
        e.preventDefault();

        var $this = $(this);
        var formData = new FormData($(this)[0]);
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status) {
                  $($this)[0].reset();
                  $('#modal_invoice_sendmail_to_am').modal('hide');
                }else{
                  alert(result.message);
                }
            },
            error: function(error) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                alert('Somthing want wrong!');
                console.log({error});
            }
        });
    }));

    $(document).ajaxComplete(function() {
        if ($('.contract-comment').length)
            $('.contract-comment').editable();
    });

    var contracts_table = $('#contracts_table').dataTable({
        "ajax": url_getContract,
        "order": [[ 7, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
            null,
            null,
            // null,
        ],
        "drawCallback": function(settings) {
          var data = settings.json;
          if(typeof data !== 'undefined'){
            $('.contract-total').text(data.count);  
          }
        }
    });

    var contract_vacant_release_type;
    var contract_vacant_id;

    $('body').on('click', '.contract-release-request', function() {
        contract_vacant_release_type = $(this).attr('data-column');
        contract_vacant_id = $(this).attr('data-id');
        $('.contract-release-comment').val("")
        $('#contract-release-property-modal').modal();
    });

    $('body').on('click', '.contract-release-submit', function() {
        comment = $('.contract-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_contractreleaseprocedure,
            data: {
                id: contract_vacant_id,
                step: contract_vacant_release_type,
                _token: _token,
                comment: comment,
            },
            success: function(data) {
                $('#contracts_table').DataTable().ajax.reload();
            }
        });
    });

    var contract_not_release_id;
    var contract_notrelease_property_id;
    $('body').on('click', '.contract-mark-as-not-release', function() {
        contract_not_release_id = $(this).attr('data-id');
        contract_notrelease_property_id = $(this).attr('data-property-id');
        $('#contracts_mark_as_not_release').modal('show');
    });

    $('#form_contracts_mark_as_not_release').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_not_release_id,
            'comment': $(this).find('#input_contracts_mark_as_not_release').val(),
            'property_id': contract_notrelease_property_id,
            '_token': _token
        }
        
        $.ajax({
            url: url_contract_mark_as_notrelease,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_not_release').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                }else{
                    alert(result.message);
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                alert('Somthing want wrong!');
            }
        });
    });

    var contract_pending_id;
    var contract_pending_property_id;
    $('body').on('click', '.contract-mark-as-pending', function() {
        contract_pending_id = $(this).attr('data-id');
        contract_pending_property_id = $(this).attr('data-property-id');
        $('#contracts_mark_as_pending').modal('show');
    });

    $('#form_contract_mark_as_pending').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_pending_id,
            'comment': $(this).find('#input_contracts_mark_as_pending').val(),
            'property_id': contract_pending_property_id,
            '_token': _token
        }
        $.ajax({
            url: url_contract_mark_as_pending,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_pending').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                }else{
                  alert(result.message);
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                alert('Somthing want wrong!');
            }
        });
    });
    
    var rental_activity_table = $('#rental_activity_table').dataTable({
        "ajax": url_get_retnal_activity,
        // "order": [[ 7, 'desc' ]],
        "columns": [
            null,
            null,
            null,
            null,
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            // null,
        ],
        "drawCallback": function(settings) {
          var data = settings.json;
          if(typeof data !== 'undefined'){
            // $('.contract-total').text(data.count);  
          }
        }
    });

});//document.ready over

$('body').on('click', '.property-insurance-release-request', function() {

      var length = $(this).closest('.row').find('table').find('[data-field="falk_status"]:checked').length;
      var msg = "Please select atleast one falk checkbox!";
    

    if(length > 0){
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        $('.property-insurance-comment').val("")
        $('#property-insurance-release-property-modal').modal();
    }else{
        alert(msg); return false;
    }
});

$('body').on('click', '.property-insurance-submit', function() {
    am_id = fk_id = 0;
    $('#insurance-table-'+vacant_id+' .am_falk_status').each(function(){
    if($(this).is(':checked') && $(this).attr('data-field')=="falk_status")
          fk_id = $(this).attr('data-id');
    if($(this).is(':checked') && $(this).attr('data-field')=="asset_manager_status")
          am_id = $(this).attr('data-id');
    });
    comment = $('.property-insurance-comment').val();
    $.ajax({
        type: 'POST',
        url: url_property_dealreleaseprocedure,
        data: {
            id: vacant_id,
            step: vacant_release_type,
            _token: _token,
            am_id:am_id,
            fk_id:fk_id,
            comment: comment,
        },
        success: function(data) {
            $('.section-ins-'+vacant_id).remove();
            $('.angebot-release-count').html($('.property-insurance-release-request').length);

        }
    });
});

var not_release_id = 0;
$('body').on('click','.deal-mark-as-not-release',function(){
    not_release_id = $(this).attr('data-id');
    $('#deal-not-release-modal').modal();
    $('.deal-not-release-comment').val("")
});
$('body').on('click','.deal-not-release-submit',function(){
    comment = $('.deal-not-release-comment').val();
    $.ajax({
          type : 'POST',
          url : url_deal_mark_as_notrelease,
          data : {id:not_release_id, _token : _token, comment:comment},
          success : function (data) {
              $('.section-ins-'+not_release_id).remove();
            $('.angebot-release-count').html($('.property-insurance-release-request').length);

          }
      });
});

$('body').on('change', '.am_falk_status', function() {

        var field = $(this).attr('data-field');
        var status = ($(this).prop('checked')) ? 1 : 0;
        var url = $(this).attr('data-url');
        // var msgElement = $('#insurance_tab_msg');

        $.ajax({
            url: url,
            type: 'POST',
            data:{
                'field' : field,
                'status' : status
            },
            dataType: 'json',
            success: function(result) {
                // if(!result.status){
                    // showFlash(msgElement, result.message, 'error');
                // }
            },
            error: function(error){
                // showFlash(msgElement, 'Somthing want wrong!', 'error');
                // console.log({error});
            }
        });

    });

