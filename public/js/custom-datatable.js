// This function is used to initialize datatable
// @param element              : Current table element for make datatable
// @param columns              : Column array if you needs (optional)
// @param short                : Short column for pass asc or desc as per needs (optional)
// @param shortColumnPosition  : Table column position for short and starting from zero for ex 0 (optional)
// @param pageLength           : Table page length default show 10 (optional)
// @param responsive           : Table responsive true or false default false (optional)
// For ex makeDatatable(element, columns, 'desc', 0);

function changenumberformat(value, decimal = 2){
    if(value){
        value = value.toFixed(decimal);
        value = parseFloat(value);
    }
    return value.toLocaleString("es-ES", {minimumFractionDigits: decimal});
}
function CalculateTableSummary(table) {
    try {

        var intVal = function (i) {

            var a = $("<div>").html(i).text();
            var x = (a == "-") ? 0 : a.split('.').join("");
            x = x.split('€').join("")
            x = x.split('%').join("")
            // x = x.split(',').join("")
            x = x.replace(',',".")
            x = x.split('<br>').join("")
            return parseFloat( x );

            return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '') * 1 :
                    typeof i === 'number' ?
                        i : 0;
        };

        var api = table.api();
        api.columns(".sum").eq(0).each(function (index) {
            var column = api.column(index,{page:'current'});

            var sum = column
               .data()
               .reduce(function (a, b) {
                   //return parseInt(a, 10) + parseInt(b, 10);
                   return intVal(a) + intVal(b);
               }, 0);
             

                console.log(sum);
            if ($(column.footer()).hasClass("show-searched-sum"))
            {
                
                $(column.footer()).html('' + changenumberformat(sum,2)+'€');
            }
            if ($(column.footer()).hasClass("show-searched-sum-int"))
            {
                $(column.footer()).html('' + changenumberformat(sum,0)+'€');   
            }

            // if ($(column.footer()).hasClass("Int")) {
            //     $(column.footer()).html('' + sum.toFixed(0));
            // } else {
            //     $(column.footer()).html('' + sum.toFixed(2));
            // }
          
        });
    } catch (e) {
        console.log('Error in CalculateTableSummary');
        console.log(e)
    }
}
function makeDatatable(element, columns = [], short = "asc", shortColumnPosition = 0, pageLength = 10, responsive = false){

    if(typeof element === 'undefined' || !element || element.length==0){
        console.log('Undefined element!'+element);
        return false;
    }

    if ( $.fn.dataTable.isDataTable( element ) ) {
        $(element).DataTable().clear().destroy();
    }

    if(typeof columns !== 'undefined' && columns.length > 0){
        var table = $(element).DataTable({
            "responsive": responsive,
            "pageLength": pageLength,
            "order": [[ shortColumnPosition, short ]],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;  
                CalculateTableSummary(this);
                return ;       
            },
            "columns": columns
        });
    }else{
        var table = $(element).DataTable({
            "responsive": responsive,
            "pageLength": pageLength,
            "footerCallback": function ( row, data, start, end, display ) {
                console.log('aaya');
                var api = this.api(), data;  
                CalculateTableSummary(this);
                return ;       
            },
            "order": [[ shortColumnPosition, short ]]
        });
    }
    
    return table;
}

// Datatable column type
if(typeof jQuery.fn.dataTableExt !== 'undefined' && typeof jQuery.fn.dataTableExt.oSort !== 'undefined'){

    jQuery.extend( jQuery.fn.dataTableExt.oSort, {
        "numeric-comma-pre": function ( a ) {
            var a = $("<div>").html(a).text();
            var x = (a == "-") ? 0 : a.split('.').join("");
            x = x.split('€').join("")
            x = x.split('%').join("")
            // x = x.split(',').join("")
            x = x.replace(',',".")
            x = x.split('<br>').join("")
            return parseFloat( x );
        },
        "numeric-comma-asc": function ( a, b ) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "numeric-comma-desc": function ( a, b ) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        },
        "formatted-num-pre": function ( a ) {
            a = a.replace(/<(?:.|\n)*?>/gm, '');
            a = (a === "-" || a === "") ? 0 : a.replace(/\./g, '');
            return parseFloat( a );
        },
        "formatted-num-asc": function ( a, b ) {
            return a - b;
        },
        "formatted-num-desc": function ( a, b ) {
            return b - a;
        },
        "formatted-inline-pre": function ( a ) {
            a = a.replace(/<(?:.|\n)*?>/gm, '');
            a = (a === "-" || a === "") ? 0 : a.replace(/\./g, '');
            return parseFloat( a );
        },
        "formatted-inline-asc": function ( a, b ) {
            return a - b;
        },
        "formatted-inline-desc": function ( a, b ) {
            return b - a;
        },
        "date-uk-pre": function ( a ) {
            a = a.split('<br>').join("");
            var ukDatea = a.trim().split('/');
            if(ukDatea!="")
                return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            else
                return 0;
        },
        "date-uk-asc": function ( a, b ) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "date-uk-desc": function ( a, b ) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        },
        "new-date-pre":function(a){
            var a = $("<div>").html(a).text();
            a = a.trim();
            if (a == null || a == "") {
                return 30000000;
            }
            a = a.split(' ').join("");
            a = a.split(':').join("");
            a = a.split(':').join("");
            var ukDatea = a.split('.');
            var d= (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
            return d;
        },
        "new-date-asc": function ( a, b ) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "new-date-desc": function ( a, b ) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        },
        "new-date-time-pre":function(a){
            var a = $("<div>").html(a).text();
            a = a.trim();
            if (a == null || a == "") {
                return 30000000;
            }
            a = a.split(' ').join(".");
            a = a.split(':').join(".");
            a = a.split(':').join(".");
            var ukDatea = a.split('.');
            var d = (ukDatea[2] + ukDatea[1] + ukDatea[0] + ukDatea[3] + ukDatea[4] + ukDatea[5]) * 1;
            return d;
        },
        "new-date-time-asc": function ( a, b ) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "new-date-time-desc": function ( a, b ) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        },
        "non-empty-string-asc": function (str1, str2) {
            if(str1 == "")
                return 1;
            if(str2 == "")
                return -1;
            return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
        },
        "non-empty-string-desc": function (str1, str2) {
            if(str1 == "")
                return 1;
            if(str2 == "")
                return -1;
            return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
        },
    });
}else{
    console.log("Undefined jQuery.fn.dataTableExt.oSort"); 
}
