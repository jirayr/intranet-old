$(document).ready(function(){

	CKEDITOR.replace('comment_text');

	if($('.search').length){
		$(".search").select2({
	        minimumInputLength: 2,
	        multiple: true,
	        tags: true,
	    });
	}

	if($("#emailSelector").length){
	    $("#emailSelector").select2({
	        multiple: true,
	        tags: true,
	    });
	}

    $('.addressenCreate').on('click', function () {
        $('#btn-save').val("create");
        var id = $(this).attr('data-id');
        var prop = $('#propId').val();
        var url ='/properties/'+prop+'/project';
        $('#projectForm').attr('action', url);
        $('#keyword').val('');
        $('#limit_max').val('');
        $('#type').val('');
        $('#title').html('Create Address');
        $('#addressCreate').modal('show');
    });

    $('#projectForm').submit(function (e) {
        e.preventDefault();
        $('#config-loader').css('visibility', 'visible');
        var state = $('#btn-save').val();
        var prop = $('#propId').val();
        var url ='/adressen';

        if (state == "update") {
            var id = $('#projectId').val();
            var url ='/properties/'+prop+'/project/update'+'/'+id;
        }

        $.ajax({
            type : 'post',
            url : url,
            data :   $('form.projectform').serialize(),
            success : function (data)
            {
                $('#config-loader').css('visibility', 'hidden');
                $('#save-config').removeClass('disabled');
                window.setTimeout(function(){
                    $('#addressCreate').modal('hide');
                    sweetAlert("Saved Successfully");
                }, 600);
                location.reload();
            }
        });

    });

    $('.edit').on('click', function () {
        $('#btn-save').val("update");
        $('#title').html('Edit Address');
        var id = $(this).attr('data-id');
        var url ='/demo/update'+'/'+id;
        $('.search').val('');
        $.ajax({
            type : 'get',
            url : url_address_edit+'/'+id,
            success : function (data)
            {
                $('#projectForm').attr('action', url);

                $('#keyword').val(data.keyword);
                $('#limit_max').val(data.limit_max);
                $('#type').val('');
            }

        });
        $('#addressCreate').modal('show');

    });

    $('.email-addressen').on('click', function () {

        $('#attachment-field').css('visibility', 'hidden');
        $('#subject-field').css('visibility', 'hidden');
        $('#message-field').css('visibility', 'hidden');
        $('#file_emails').css('visibility', 'hidden');

        $("#subject").val('');
        $("#file").val('');
        $('#emailSelector').html('');

        CKEDITOR.replace( 'message' );
        CKEDITOR.instances['message'].destroy(true);
        CKEDITOR.replace( 'message' );


        $('#csv_email').modal('show');
        var id = $(this).attr('data-id');
        $("#address_id").val(id);
        var name = $(this).attr('data-filename');


        $.ajax({
            type : 'get',
            data: {
                name: name,
                _token: _token
            },
            url : url_address_file_emails,
            success : function (data)
            {
                $.each(data, function(key) {
                    $('#emailSelector').append('<option  value="'+data[key]+'">'+data[key]+'</option>');
                });

            }
        });
    });

    $('#tag-form-submit').on('click', function () {
        var fd = new FormData();
        var files = $('#file')[0].files[0];
        if (files){
            fd.append('attachment',files);
        }
        fd.append('token',$('#token').val());
        fd.append('address_id',$('#address_id').val());
        fd.append('subject',$('#subject').val());
        fd.append('message',CKEDITOR.instances['message'].getData());
        fd.append('emails',$('.search').val());
        fd.append('file_emails',$('#emailSelector').val());

        if ($('#subject').val() === '')
        {
            $('#subject-field').css('visibility', 'visible');
        }


        if ($('#emailSelector').val() === null)
        {
            $('#file_emails').css('visibility', 'visible');
        }

        if(CKEDITOR.instances['message'].getData() === ''){

            $('#message-field').css('visibility', 'visible');

        }
        if ($('#subject').val() !== ''  &&  CKEDITOR.instances['message'].getData() !== '' && $('#emailSelector').val()!== null) {
            $('#loader').css('visibility', 'visible');

            $('#attachment-field').hide();
            $('#subject-field').hide();
            $('#message-field').hide();
            $('#file_emails').hide();

            $('#tag-form-submit').addClass('disabled');

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            $.ajax({
                type : 'post',
                url : url_sendEmail,
                data : fd,
                contentType: false,
                processData: false,
                success : function (data){
                    var message = "Emails Sent Successfully";
                    $('#loader').css('visibility', 'hidden');
                    $('#csv_email').modal('hide');
                    $('#tag-form-submit').removeClass('disabled');
                    if (data){
                        message = data;
                    }
                    window.setTimeout(function(){
                        sweetAlert(message, "success");
                    }, 600);
                }
            });

        }

    });

    $('body').on('click', '.checkbox-is-sent-addressen', function() {
        var id = $(this).attr('data-id');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: '/demo/update' + "/" + id,
            data: {
                is_status_checked: v,
                _token: $('#token').val()
            },
            success: function(data) {
                sweetAlert("Status Updated Successfully");
            }
        });
    });

    $('.save-ccomment').click(function() {
        var url = url_change_comment;
        var fd = new FormData();
        var file_data = $('.comment-file1')[0].files; // for multiple files
        for (var i = 0; i < file_data.length; i++) {
            fd.append("file1[" + i + "]", file_data[i]);
        }
        var file_data = $('.comment-file2')[0].files; // for multiple files
        for (var i = 0; i < file_data.length; i++) {
            fd.append("file2[" + i + "]", file_data[i]);
        }
        var file_data = $('.comment-file3')[0].files; // for multiple files
        for (var i = 0; i < file_data.length; i++) {
            fd.append("file3[" + i + "]", file_data[i]);
        }
        var other_data = $('#ccoment-form').serializeArray();
        $.each(other_data, function(key, input) {
            fd.append(input.name, input.value);
        });
        fd.append('_token', _token);
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            data: fd,
        }).done(function(data) {
            // alert(data);
            var path = $('#path-properties-show').val();
            window.location.href = path + '?tab=comment_tab';
        });
    });

    $('.searchleasecolumn-btn').click(function() {
        var cal = $('.leasecolumn').val();
        var sort = $('.leasesort').val();
        var property_id = $('#selected_property_id').val();
        if ($('.searchleasecolumn').val() == "") 
        	$('.save-mieter').show();
        else 
        	$('.save-mieter').hide();
        $.ajax({
            url: url_lease_list,
            type: "get",
            data: {
                property_id: property_id,
                column: cal,
                sort: sort,
                search: $('.searchleasecolumn').val()
            },
            success: function(response) {
                $('.lease-log').html(response);
            }
        });
    });

    $('.searchleasecolumn').keypress(function(e) {
        if (e.which == 13) {
            $('.searchleasecolumn-btn').trigger('click');
        }
    });

    $('.save-mieter').click(function() {
        $.ajax({
            url: url_saveleaseactivity,
            type: "post",
            data: $('#mt-form').serialize(),
            success: function(response) {
                var cal = $('.leasecolumn').val();
                var sort = $('.leasesort').val();
                var property_id = $('#selected_property_id').val();
                $.ajax({
                    url: url_lease_list,
                    type: "get",
                    data: {
                        property_id: property_id,
                        column: cal,
                        sort: sort
                    },
                    success: function(response) {
                        $('.lease-log').html(response);
                    }
                });
            }
        });
    });

    $('body').on('click', '.add-more-mieter', function() {
        $('.miter-list').prepend($('.miter').html())
    });

    $('body').on('click', '.remove-mieter', function() {
        $(this).closest('tr').remove();
    });

});