$(document).ready(function(){

	var contracts_table = $('#contracts_table').dataTable({
        "ajax": url_get_contract,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    var pending_contracts_table = $('#pending_contracts_table').dataTable({
        "ajax": url_get_pending_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
        ]
    });

    var release_contracts_table = $('#release_contracts_table').dataTable({
        "ajax": url_get_release_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
        ]
    });

    var old_contracts_table = $('#old_contracts_table').dataTable({
        "ajax": url_get_old_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
        ]
    });

    var not_release_am_contracts_table = $('#not_release_am_contracts_table').dataTable({
        "ajax": url_get_not_release_am_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
        ]
    });

    var not_release_contracts_table = $('#not_release_contracts_table').dataTable({
        "ajax": url_get_not_release_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
        ]
    });

    var contracts_mail_table = $('#contracts_mail_table').dataTable({
        "ajax": url_get_contract_logs,
        "order": [
            [4, "desc"]
        ],
        "columns": [
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            {"type": "new-date"},
        ]
    });

    $(document).ajaxComplete(function() {
        if ($('.contract-comment').length)
            $('.contract-comment').editable();
    });

    $(document).on('click', ".contracts-upload-file-icon", function(){
        $("#contracts_gdrive_file_upload").trigger('click');
    });

    $(document).on('change', "#contracts_gdrive_file_upload", function(e){
        var fileName = e.target.files[0].name;
        $("#contracts_gdrive_file_name_span").text(fileName);
    });

    $(document).on('click', ".link-button-gdrive-contracts", function(e){
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('property_deals');
        $("#select-gdrive-file-callback").val('gdeiveSelectContracts');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_name").val());
    });

    window.gdeiveSelectContracts = function(basename, dirname,curElement, dirOrFile){
        $("#contracts_gdrive_file_basename").val(basename);
        $("#contracts_gdrive_file_dirname").val(dirname);
        $("#contracts_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#contracts_gdrive_file_name").val(text);
        $("#contracts_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    $('#add_contracts_form').submit(function(event) {
        event.preventDefault();

        var $this = $(this);
        var msgElement = $('#add_contracts_msg');
        var formData = new FormData($(this)[0]);
        var is_old = ($('#is_old').prop('checked')==true) ? true : false;

        $.ajax({
            url: url_add_contract,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache : false,
            beforeSend: function() {
                //$($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                //$($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){

                    $($this)[0].reset();
                    $('#contracts_gdrive_file_basename').val('');
                    $('#contracts_gdrive_file_dirname').val('');
                    $('#contracts_gdrive_file_type').val('');
                    $('#contracts_gdrive_file_name').val('');
                    $('#contracts_gdrive_file_name_span').text('');
                    $('#contracts_gdrive_file_upload').val('');
                    
                    $('#add_contracts_modal').modal('hide');

                    if(is_old){
                        $('#old_contracts_table').DataTable().ajax.reload();
                    }else{
                        $('#contracts_table').DataTable().ajax.reload();
                    }

                    showFlash($('#contracts_msg'), result.message, 'success');
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                //$($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    $('body').on('click', '.btn-delete-contract', function() {
        var url = $(this).attr('data-url');
        var msgElement = $('#contracts_msg');
        var type = $(this).attr('data-type');

        if (confirm('Are you sure want to delete this contract?')) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');

                        if(type == 'old_contract'){
                            $('#old_contracts_table').DataTable().ajax.reload();
                        }else{
                            $('#contracts_table').DataTable().ajax.reload();
                        }
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });

    var contract_vacant_release_type;
    var contract_vacant_id;

    $('body').on('click', '.contract-release-request', function() {
        contract_vacant_release_type = $(this).attr('data-column');
        contract_vacant_id = $(this).attr('data-id');
        $('.contract-release-comment').val("")
        $('#contract-release-property-modal').modal();
    });

    $('body').on('click', '.contract-release-submit', function() {
        comment = $('.contract-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_contractreleaseprocedure,
            data: {
                id: contract_vacant_id,
                step: contract_vacant_release_type,
                _token: _token,
                comment: comment,
            },
            success: function(data) {
                $('#contracts_table').DataTable().ajax.reload();
                $('#contracts_mail_table').DataTable().ajax.reload();
            }
        });
    });

    var contract_not_release_id;
    $('body').on('click', '.contract-mark-as-not-release', function() {
        contract_not_release_id = $(this).attr('data-id');
        $('#contracts_mark_as_not_release').modal('show');
    });

    $('#form_contracts_mark_as_not_release').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_not_release_id,
            'comment': $(this).find('#input_contracts_mark_as_not_release').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#contracts_mark_as_not_release_error');
        $.ajax({
            url: url_contract_mark_as_notrelease,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_not_release').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                    $('#not_release_contracts_table').DataTable().ajax.reload();
                    $('#contracts_mail_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var contract_pending_id;
    $('body').on('click', '.contract-mark-as-pending', function() {
        contract_pending_id = $(this).attr('data-id');
        $('#contracts_mark_as_pending').modal('show');
    });

    $('#form_contract_mark_as_pending').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_pending_id,
            'comment': $(this).find('#input_contracts_mark_as_pending').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#contracts_mark_as_pending_error');
        $.ajax({
            url: url_contract_mark_as_pending,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_pending').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                    $('#pending_contracts_table').DataTable().ajax.reload();
                    $('#contracts_mail_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var contract_not_release_am_id;
    $('body').on('click', '.contract-mark-as-not-release-am', function() {
        contract_not_release_am_id = $(this).attr('data-id');
        $('#contracts_mark_as_not_release_am').modal('show');
    });

    $('#form_contracts_mark_as_not_release_am').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_not_release_am_id,
            'comment': $(this).find('#input_contracts_mark_as_not_release_am').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#contracts_mark_as_not_release_am_error');
        $.ajax({
            url: url_contract_mark_as_notrelease_am,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_not_release_am').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                    $('#not_release_am_contracts_table').DataTable().ajax.reload();
                    $('#contracts_mail_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

});