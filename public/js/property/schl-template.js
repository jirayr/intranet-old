$(document).ready(function(){

	$('#schl-template').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) return response.msg;
            else {
                if (typeof(response.reload) != "undefined" && response.reload === false) {} else {
                    var path = $('#path-properties-show').val();
                    window.location.href = path + '?tab=schl-template';
                }
            }
        }
    });

    $('.inline-edit-schl').editable({
        prepend: "not selected",
        source: [{
            value: '3 Monate',
            text: '3 Monate'
        }, {
            value: '4 Wochen',
            text: '4 Wochen'
        }],
    });

    $('#hausmaxx_options').on('change', function() {
        $.ajax({
            type: 'POST',
            url: url_property_update_schl,
            data: {
                pk: 'hausmaxx',
                value: $(this).val(),
                _token: _token
            },
            success: function(data) {
                window.location.reload();
            }
        });
    });

    $('#steuerberater_options').on('change', function() {
        $.ajax({
            type: 'POST',
            url: url_property_update_steuerberater,
            data: {
                pk: 'steuerberater',
                value: $(this).val(),
                _token: _token
            },
            success: function(data) {
                // window.location.reload();
            }
        });
    });
    
    $('#steuerberater1_options').on('change', function() {
        $.ajax({
            type: 'POST',
            url: url_property_update_steuerberater,
            data: {
                pk: 'steuerberater1',
                value: $(this).val(),
                _token: _token
            },
            success: function(data) {
                // window.location.reload();
            }
        });
    });

    $('.change-column-name').on('change', function() {
        $.ajax({
            type: 'POST',
            url: url_property_update_schl,
            data: {
                pk: $(this).attr('data-column'),
                value: $(this).val(),
                _token: _token
            },
            success: function(data) {
                // window.location.reload();
            }
        });
    });

    $('body').on('click', '.delete-service-provider', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_service_provider,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('tr').remove();
                }
            });
        }
    });

    $('body').on('click', '.delete-maintainance', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_maintenance,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('tr').remove();
                }
            });
        }
    });

    $('body').on('click', '.delete-investation', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_investation,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('tr').remove();
                }
            });
        }
    });

    $('body').on('click', '.delete-insurance', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_property_insurance,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('tr').remove();
                }
            });
        }
    });

    $('body').on('change', '.image-file', function() {
        var formdata = new FormData();
        var url = url_schl_file;
        file = $(this).prop('files')[0];
        formdata.append("file", file);
        formdata.append('_token', _token);
        formdata.append('type', $(this).attr('data-type'));
        property_id = $('#selected_property_id').val();
        formdata.append('property_id', property_id);
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            dataType: 'json',
            data: formdata,
        }).done(function(data) {
            if (data.status == 1) {
                $('.upload-image-file').attr('src', data.path);
            } else {
                alert(data.message);
            }
        });
    });

});