function getrelbutton(id) {
    $.ajax({
        url: url_getReleasebutton,
        type: "get",
        dataType: 'json',
        data: {
            id: id
        },
        success: function(response) {
            $('.tenant-release-buttons-' + id).html(response.buttons);
            $('.tenant-release-logs-' + id).html(response.logs);
            $('.release-status-' + id).html($('.tenant-release-buttons-' + id).find('.f-release').html());
        }
    });
}

function loadAllCommentsTable(){

    if($('.recommendation_comments_table').length){
        $('.recommendation_comments_table').each(function(){
            var id = $(this).attr('data-id');
            if(id){
               getRecommendedComment(id, 1); 
            }
        });
    }
}

function getRecommendedComment(id, limit = ''){
    var url = url_get_recommended_comment.replace(":id", id);
    url = url+'/'+limit;

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#recommendation_comments_table_'+id).find('tbody').empty();
            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_recommended_comment.replace(":id", value.id);

                    var tr = '\
                    <tr>\
                        <td>'+ value.user_name +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td><a href="javascript:void(0);" data-url="'+ clear_url +'" data-tenant-id="'+id+'" class="btn-xs btn-danger delete-recommended-comment">Löschen</a></td>\
                    </tr>\
                    ';

                    $('#recommendation_comments_table_'+id).find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

function recommendedTableList(){
    $.ajax({
        url: url_get_recommended_table,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            $('#recommended-table-div-1').html(result.table1);
            $('#recommended-table-div-2').html(result.table2);
            $('#recommended-table-div-3').html(result.table3);

            $('#recommended-table-1').DataTable();
            $('#recommended-table-2').DataTable();

            var columns = [
                null,
                null,
                null,
                {"type": "new-date-time"}
            ];
            makeDatatable($('#recommended-table-3'), columns, 'desc', 3);

            if($('.recommended_last_comment').length){
              new showHideText('.recommended_last_comment', {
                  charQty     : 30,
                  ellipseText : "...",
                  moreText    : "More",
                  lessText    : " Less"
              });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

function getRecommendedCommentById(id, limit = ''){
    var url = url_get_recommended_comment.replace(":id", id);
    url = url+'/'+limit;

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#recommended_comments_table').find('tbody').empty();
            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_recommended_comment.replace(":id", value.id);

                    var tr = '\
                    <tr>\
                        <td>'+ value.user_name +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td><a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-recommended-comment">Löschen</a></td>\
                    </tr>\
                    ';

                    $('#recommended_comments_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

function loadKostenUmbau(property_id, tenant_id){
    var url = url_get_kosten_umbau;
    url = url.replace(':property_id', property_id);
    url = url.replace(':tenant_id', tenant_id);
    $.ajax({
        type: 'GET',
        url: url,
        success: function(html) {
            // $('#kosten_umbau_modal').find('.modal-body').html(html);
            $('#form_kosten_umbau').html(html);
        }
    });
}

$(document).ready(function(){

	$('.show-link-recommended').click(function() {
        var cl = $(this).attr('data-class');
        if ($('.' + cl).hasClass('hidden')) {
            $(this).find('i').removeClass('fa-angle-down');
            $(this).find('i').addClass('fa-angle-up');
            $('.' + cl).removeClass('hidden');
            //ajax code here
            var id = $(this).attr('data-id');
            getrelbutton(id);
        } else {
            // $(this).html('<i class="fa fa-angle-down"></i>');
            $(this).find('i').addClass('fa-angle-down');
            $(this).find('i').removeClass('fa-angle-up');
            $('.' + cl).addClass('hidden');
            // $(this).next().addClass('hidden');
        }
    });

    $('body').on('click', '.delete-vacant', function() {
	    $this = $(this);
	    id = $(this).attr('data-id');
	    if (confirm('Are you sure want to delete?')) {
	        $.ajax({
	            url: url_delete_new_vacant,
	            type: "get",
	            data: {
	                id: id,
	            },
	            success: function(response) {
	                $this.closest('.col-md-6').remove();
	                $('.r-sheet-'+id).remove();

	            }
	        });
	    }
	    return false;
	});

	$('body').on('click', '.add-row-rec', function() {
        $(this).closest('tbody').find('.gesamt_row').before($('.add-row-clone').html());
    });

    $('body').on('click', '.remove-mieter', function() {
        $(this).closest('tr').remove();
    });

    $('body').on('click','.link-button-empfehlung2',function(e) {
        var property_id = $(this).data('property_id');
        var empfehlung_id = $(this).data('empfehlung_id');
        var tenant_id = $(this).data('tenant_id');
        var fileObjId = $(this).attr('id');
        var data = {
            property_id: property_id,
            empfehlung_id: empfehlung_id,
            tenant_id: tenant_id,
            fileObjId: fileObjId
        };
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('empfehlung2');
        $("#select-gdrive-file-callback").val('selectempfehlungSubmit2');
        $("#select-gdrive-file-data").val(JSON.stringify(data));
        // var workingDir = "{{$workingDir}}";
        selectGdriveFileLoad(workingDir);
    });

    window.selectempfehlungSubmit2 = function(basename, dirname, curElement, dirOrFile) {
        var dataObj = $("#select-gdrive-file-data").val();
        dataObj = JSON.parse(dataObj);
        var formdata = new FormData();
        formdata.append('_token', _token);
        formdata.append('property_id', dataObj.property_id);
        formdata.append('empfehlung_id', dataObj.empfehlung_id);
        formdata.append('basename', basename);
        formdata.append('dirname', dirname);
        formdata.append('dirtype', 'empfehlung2');
        formdata.append('dirOrFile', dirOrFile);
        $('.preloader').show();
        $.ajax({
            type: 'POST',
            url: url_property_saveEmpfehlungFile,
            contentType: false,
            processData: false,
            data: formdata,
        }).done(function(data) {
            if (data.success) {
                curElement.remove();
                $('.uploaded-files-empfehlung2.rowt-' + dataObj.empfehlung_id).append(data.element);
                alert(data.msg);
            } else {
                alert(data.msg);
            }
            $('.preloader').hide();
        });
    }

    $('body').on('click','.link-button-empfehlung',function(e) {
        var property_id = $(this).data('property_id');
        var empfehlung_id = $(this).data('empfehlung_id');
        var tenant_id = $(this).data('tenant_id');
        var fileObjId = $(this).attr('id');
        var data = {
            property_id: property_id,
            empfehlung_id: empfehlung_id,
            tenant_id: tenant_id,
            fileObjId: fileObjId
        };
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('empfehlung');
        $("#select-gdrive-file-callback").val('selectempfehlungSubmit');
        $("#select-gdrive-file-data").val(JSON.stringify(data));
        // var workingDir = "{{$workingDir}}";
        selectGdriveFileLoad(workingDir);
    });

    window.selectempfehlungSubmit = function(basename, dirname, curElement, dirOrFile) {
        var dataObj = $("#select-gdrive-file-data").val();
        dataObj = JSON.parse(dataObj);
        var formdata = new FormData();
        formdata.append('_token', _token);
        formdata.append('property_id', dataObj.property_id);
        formdata.append('empfehlung_id', dataObj.empfehlung_id);
        formdata.append('basename', basename);
        formdata.append('dirname', dirname);
        formdata.append('dirOrFile', dirOrFile);
        $('.preloader').show();
        $.ajax({
            type: 'POST',
            url: url_property_saveEmpfehlungFile,
            contentType: false,
            processData: false,
            data: formdata,
        }).done(function(data) {
            if (data.success) {
                curElement.remove();
                $('.uploaded-files-empfehlung.row-' + dataObj.empfehlung_id).append(data.element);
                alert(data.msg);
            } else {
                alert(data.msg);
            }
            $('.preloader').hide();
        });
    }

    $('body').on('click', '.btn-add-recommended-comment', function() {
	    var tenant_id = $(this).attr('data-tenant-id');
	    $('#add_recommended_comment_modal').find('input[name="tenant_id"]').val(tenant_id);
	    $('#add_recommended_comment_modal').modal('show');
	});

	$('#add_recommended_comment_form').submit(function(event) {

	    event.preventDefault();
	    var $this = $(this);
	    var url = $(this).attr('action');
	    var comment = $($this).find('textarea[name="comment"]').val();
	    var tenant_id = $($this).find('input[name="tenant_id"]').val();
	    var data = {'tenant_id': tenant_id, 'comment': comment};

	    $.ajax({
	        url: url,
	        type: 'POST',
	        data: data,
	        dataType: 'json',
	        beforeSend: function() {
	            $($this).find('button[type="submit"]').prop('disabled', true);
	        },
	        success: function(result) {

	            $($this).find('button[type="submit"]').prop('disabled', false);
	            if (result.status == true) {
	                $('#add_recommended_comment_modal').modal('hide');
	                $($this)[0].reset();

	                var limit_text = $('#recommendation_comments_table_'+tenant_id).find('.recommendation_comments_limit').text();
	                getRecommendedComment(tenant_id, (limit_text == 'Show More') ? 1 : '');

	            } else {
	                alert(result.message);
	            }
	        },
	        error: function(error) {
	            $($this).find('button[type="submit"]').prop('disabled', false);
	            alert('Somthing want wrong!');
	        }
	    });
	    
	});

	$('body').on('click', '.recommendation_comments_limit', function() {
	    var id = $(this).closest('table').attr('data-id');
	    var text = $(this).text();
	    var limit = '';
	    if(text == 'Show More'){
	        limit = '';
	        $(this).text('Show Less');
	    }else{
	        limit = 1;
	        $(this).text('Show More');
	    }
	    getRecommendedComment(id, limit);
	});

	$('body').on('click', '.delete-recommended-comment', function() {
	    var url = $(this).attr('data-url');
	    var id = $(this).attr('data-tenant-id');
	    var $this = $(this);
	    if(confirm('Are you sure to delete this comment?')){
	        $.ajax({
	            url: url,
	            type: 'GET',
	            dataType: 'json',
	            success: function(result) {
	                if(result.status){
	                    var limit_text = $($this).closest('table').find('.recommendation_comments_limit').text();
	                    getRecommendedComment(id, (limit_text == 'Show More') ? 1 : '');
	                }else{
	                    alert(result.message);
	                }
	            },
	            error: function(error) {
	                alert('Somthing want wrong!');
	            }
	        });
	    }else{
	        return false;
	    }
	});

	loadAllCommentsTable();

	$('.save-table-list').click(function() {
        var data = $(this).closest('form').serialize();
        var $this = $(this);
        $.ajax({
            url: url_saveractivity,
            type: "post",
            data: data,
            success: function(response) {
                var property_id = $('#selected_property_id').val();
                load_ractivity(property_id, $this.closest('form').find('.tenant_id').val(), $this.closest('form').find('.recommended-section'));
            }
        });
    });

    window.load_ractivity = function(property_id, tenant_id, htmlArea) {
        $.ajax({
            url: url_recommended_list,
            type: "get",
            data: {
                property_id: property_id,
                tenant_id: tenant_id
            },
            success: function(response) {
                htmlArea.html(response);
                getRecommendedComment(tenant_id, 1);
            }
        });
    }

    var vacant_release_type = vacant_id = "";
    $('body').on('click', '#rtab-tab1 .vacant-release-request', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).closest('form').find('.tenant_id').val();
        $('.vacant-release-comment').val("");
        $('#vacant-release-property-modal').modal();
    });

    $('body').on('click', '.vacant-release-submit', function() {
        comment = $('.vacant-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_vacantreleaseprocedure,
            data: {
                tenant_id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                property_id: $('#selected_property_id').val()
            },
            success: function(data) {
                getrelbutton(vacant_id);
            }
        });
    });

    $('body').on('change', '.change-comment-recommended', function() {
        var $this = $(this);
        var tr = $(this).closest('tr');
        var pk = $(this).attr('data-column');

        var data = {
            property_id: $('#selected_property_id').val(),
            pk: $(this).attr('data-column'),
            value: $(this).val(),
            _token: _token,
            tenant_id: $(this).attr('data-id')
        };
        $.ajax({
            url: url_changeempfehlungdetails,
            type: "post",
            data: data,
            success: function(response) {
                // if($this.attr('data-column')=="amount7")
                // getprovisionbutton();
                
                if(typeof tr !== 'undefined' && tr){
                    $(tr).find('.kosten_umbau').text(response.data.kosten_umbau);
                    if(pk != 'amount2'){
                        $(tr).find('.laufzeit').val(response.data.laufzeit);
                    }
                    if(pk != 'amount5'){
                        $(tr).find('.mietfrei').val(response.data.mietfrei);
                    }
                    $(tr).find('.jährl_einnahmen').val(response.data.jährl_einnahmen);
                    $(tr).find('.Gesamteinnahmen').text(response.data.Gesamteinnahmen);
                    $(tr).find('.Gesamteinnahmen_netto').text(response.data.Gesamteinnahmen_netto);
                }
            }
        });
    });

    // js for tabular format
    recommendedTableList();

    var recommended_id;
	$('body').on('click', '.recommended-comment', function() {
	    recommended_id = $(this).attr('data-id');
	    var limit_text = $('#recommended_comments_limit').text();
	    getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');
	    $('#recommended_comment_modal').modal('show');
	});

	$('#recommended_comment_form').submit(function(event) {

	    event.preventDefault();
	    var $this = $(this);
	    var url = $(this).attr('action');
	    var comment = $($this).find('textarea[name="comment"]').val();
	    var data = {'tenant_id': recommended_id, 'comment': comment};

	    $.ajax({
	        url: url,
	        type: 'POST',
	        data: data,
	        dataType: 'json',
	        beforeSend: function() {
	            $($this).find('button[type="submit"]').prop('disabled', true);
	        },
	        success: function(result) {

	            $($this).find('button[type="submit"]').prop('disabled', false);
	            if (result.status == true) {
	                $($this)[0].reset();
	                $($this).find('textarea[name="comment"]').focus();

	                var limit_text = $('#recommended_comments_limit').text();
	                getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');

	            } else {
	                alert(result.message);
	            }
	        },
	        error: function(error) {
	            $($this).find('button[type="submit"]').prop('disabled', false);
	            alert('Somthing want wrong!');
	        }
	    });
	    
	});

	$('body').on('click', '#recommended_comments_limit', function() {
	    var text = $(this).text();
	    var limit = '';
	    if(text == 'Show More'){
	        limit = '';
	        $(this).text('Show Less');
	    }else{
	        limit = 1;
	        $(this).text('Show More');
	    }
	    getRecommendedCommentById(recommended_id, limit);
	});

	$('body').on('click', '.remove-recommended-comment', function() {
	    var url = $(this).attr('data-url');
	    var $this = $(this);
	    if(confirm('Are you sure to delete this comment?')){
	        $.ajax({
	            url: url,
	            type: 'GET',
	            dataType: 'json',
	            success: function(result) {
	                if(result.status){
	                    var limit_text = $('#recommended_comments_limit').text();
	                    getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');
	                }else{
	                    alert(result.message);
	                }
	            },
	            error: function(error) {
	                alert('Somthing want wrong!');
	            }
	        });
	    }else{
	        return false;
	    }
	});

	var vacant_release_type = vacant_id1 = "";
	$('body').on('click', '#rtab-tab2 .vacant-release-request', function() {
	    vacant_release_type = $(this).attr('data-column');
	    vacant_id1 = $(this).attr('data-id');
	    $('.vacant_release_comment').val("");
	    $('#vacant-release-modal').modal('show');
	});

	$('body').on('click', '.vacant_release_submit', function() {
	    comment = $('.vacant_release_comment').val();
	    $.ajax({
	        type: 'POST',
	        url: url_property_vacantreleaseprocedure,
	        data: {
	            tenant_id: vacant_id1,
	            step: vacant_release_type,
	            _token: _token,
	            comment: comment,
	            property_id: $('#selected_property_id').val()
	        },
	        success: function(data) {
	            recommendedTableList();
	            $('#vacant-release-modal').modal('hide');
	        }
	    });
	});

	$('body').on('click', '.kosten_umbau', function() {
	    var property_id = $(this).attr('data-property-id');
	    var tenant_id = $(this).attr('data-tenant-id');
	    loadKostenUmbau(property_id, tenant_id);
	    $('#kosten_umbau_modal').modal('show');
	});

	$('body').on('click', '#save_kosten_umbau', function() {
	    var data = $('#form_kosten_umbau').serialize();
	    var $this = $(this);
	    var property_id = $('#form_kosten_umbau').find("input[name='property_id']").val();
	    var tenant_id = $('#form_kosten_umbau').find("input[name='tenant_id']").val();

	    $.ajax({
	        url: url_saveractivity,
	        type: "post",
	        data: data,
	        success: function(response) {
	            loadKostenUmbau(property_id, tenant_id);
	        }
	    });
	});

	$('#kosten_umbau_modal').on('hidden.bs.modal', function () {
	    recommendedTableList();
	});

	$('body').on('click', '.btn-delete-vacant', function() {
	    $this = $(this);
	    var id = $(this).attr('data-id');
	    if (confirm('Are you sure want to delete?')) {
	        $.ajax({
	            url: url_delete_new_vacant,
	            type: "get",
	            data: {
	                id: id,
	            },
	            success: function(response) {
	                recommendedTableList();
	            }
	        });
	    }
	    return false;
	});

});