function addOrUpdateVacantStatus(url, data, $this){
  	$.ajax({
      	url: url,
      	type: 'POST',
      	data: data,
      	dataType: 'json',
      	success: function(result){
        	if(result.status){
          		if(data.input == 'text'){
            		if(data.value != ''){
              			if (!data.value.match(/^[a-zA-Z]+:\/\//)){
                  			data.value = 'http://' + data.value;
              			}
              			$($this).closest('.row').find('.link').html('<a href="'+data.value+'" target="blank"><i class="fa fa-external-link" style="margin-top: 13px;"></i></a>');
            		}else{
              			$($this).closest('.row').find('.link').empty();
            		}
          		}

          		if(data.field == 'comment'){
            		if(data.value == ""){
              			$($this).closest('.row').find('.updated_date').text('');
            		}else{
              			$($this).closest('.row').find('.updated_date').text(result.data.date);
            		}
          		}
        	}else{
          		alert(result.message);
        	}
      	},
      	error: function(error){
        	alert('Somthing want wrong');
      	}
  	});
}

function loadComment(id){
  	if ( ! $.fn.DataTable.isDataTable('#rental-activity-table') ) {
    	$('#rental-activity-table').dataTable({
        	"ajax": {
          		"url": get_rental_activity_comment+'?id='+id,
        	},
        	"order": [
            	[1, 'desc']
        	],
        	"columns": [
            	null,
            	{"type": "new-date-time"},
        	]
    	});
  	}else{
    	$('#rental-activity-table').DataTable().ajax.url(url_get_rental_activity_comment+'?id='+id).load();
  	}
}

$(document).ready(function(){

	var total1 = 0;
	$('.total1').each(function(){
	      if($(this).html() != "" && $(this).html() != "Empty")
	        total1 += parseFloat($(this).html());
	});
	$('.sum_total1').html(total1);

	var total2 = 0;
	$('.total2').each(function(){
	      if($(this).html() != "" && $(this).html() != "Empty")
	        total2 += parseFloat($(this).html());
	});
	$('.sum_total2').html(total2);

	$.fn.editable.defaults.ajaxOptions = {type: "POST"};


    $('.lbuy-sheet').find('a.inline-edit').editable({
        success: function(response, newValue) {
          setTimeout(function(){
          var total1 = 0;
          $('.total1').each(function(){
              if($(this).html() != "" && $(this).html() != "Empty")
                total1 += parseFloat($(this).html());
          })
          $('.sum_total1').html(total1);

          var total2 = 0;
          $('.total2').each(function(){
              if($(this).html() != "" && $(this).html() != "Empty")
                total2 += parseFloat($(this).html());
          })
          $('.sum_total2').html(total2);
        },500);
        }
    });
 
	$('.checkbox-einkauf').on('click', function () {
	    var type = $(this).attr('data-type');
	    v = 0;
	    if($(this).is(':checked'))
	        v = 1;

	    property_id = $('.property_id').val();

	    $.ajax({
	        type : 'POST',
	        url : url_einkauf_item_update+"/"+property_id+'/'+type,
	        data : {type:type,pk : 'status',value:v, property_id:property_id,  _token : _token },
	        success : function (data) {
	            generate_option_from_json(data, 'country_to_state');
	        }
	    });
	});

	// email_template_javascript1

	$('body').on('click','.show-links',function(){

	      var cl = $(this).attr('data-class');
	      if($('.'+cl).hasClass('hidden')){
	          $(this).find('i').removeClass('fa-angle-down');
	          $(this).find('i').addClass('fa-angle-up');
	          $('.'+cl).removeClass('hidden');

	      }
	      else{
	          // $(this).html('<i class="fa fa-angle-down"></i>');
	          $(this).find('i').addClass('fa-angle-down');
	          $(this).find('i').removeClass('fa-angle-up');
	          $('.'+cl).addClass('hidden');
	          // $(this).next().addClass('hidden');
	      }
	});

	$('body').on('change', '.vacant_status_checkbox', function () {
	    var value = ($(this).prop('checked') == true) ? 1 : 0;
	    var type = $(this).val();
	    var field = $(this).attr('data-field');
	    var url = $(this).closest('.customvacancy').attr('data-url');

	    var data = {
	      field: field,
	      value: value,
	      type: type,
	      input: 'checkbox'
	    };

	    addOrUpdateVacantStatus(url, data, $(this));
	});

	$('body').on('keyup', '.vacant_status_input', function () {
	  	var value = $(this).val();
	  	var type = $(this).attr('data-type');
	  	var field = $(this).attr('data-field');
	  	var url = $(this).closest('.customvacancy').attr('data-url');

	  	var data = {
	      	field: field,
	      	value: value,
	      	type: type,
	      	input: 'text'
	  	};

	  	addOrUpdateVacantStatus(url, data, $(this));
	});

	$('body').on('click', '.delete-upload-image', function () {
	  	var $this = $(this);
	  	if(confirm('Are you sure want to delete?')){
	    
	    	var type = $(this).attr('data-value');
	    	var field = $(this).attr('data-field');
	    	var url = $(this).closest('.customvacancy').attr('data-url');
	    	var data = {
	        	field: field,
	        	value: "",
	        	type: type,
	        	input: 'file'
	      	};

	      	addOrUpdateVacantStatus(url, data, $this);
	      	$($this).closest('.row').find('.updated_date').text('');
	      	$(this).closest('.preview-img').html("");
	  	}
	});
    
	$('body').on('change', '.upload-status-file', function () {
    	var type = $(this).attr('data-value');
    	var field = $(this).attr('data-field');
    	var url = $(this).closest('.customvacancy').attr('data-url');
    	$this = $(this);

    	var formData = new FormData();
    	$.each(jQuery(this)[0].files, function(i, file) {
        	formData.append('file[' + i + ']', file);
    	});

	    $.ajax({
	        url: url_uploadfiles, //Server script to process data
	        type: 'POST',
	        data: formData,
	        contentType: false,
	        processData: false,
	        dataType: 'json',
	        success: function(json) {
	            $($this).val('');
	            $.each(json.files, function(index, value) {
	                // var str = '<input type="hidden" name="img[]" value="' + json.filename[index] + '">';
	                if(type == '6'){
	                  $this.closest('.row').find('.preview-img').html("<a href='" + value + "' target='_blank'>Download</a><br><a href='javascript:void(0)'' class='delete-upload-image' data-field='comment' data-value='"+type+"'><i class='fa fa-times'></i></a>");
	                }else{
	                  $this.closest('.row').find('.preview-img').html("<a href='" + value + "' target='_blank'><img src='" + value + "' style='width:50px; height:50px;' ></a><br><a href='javascript:void(0)'' class='delete-upload-image' data-field='comment' data-value='"+type+"'><i class='fa fa-times'></i></a>");
	                }

	                var data = {
	                  field: field,
	                  value: json.filename[index],
	                  type: type,
	                  input: 'file'
	                };

	                addOrUpdateVacantStatus(url, data, $this);

	            });
	        }
	    });
	});

	var latest_comment;
	$('body').on('click', '.btn-rental-activity-comment', function () {
	  	latest_comment = $(this).closest('.row').find('.latest-comment');
	  	var url = $(this).attr('data-url');
	  	var id = $(this).attr('data-id');

	  	$('#rental-activity-form').attr('action', url);
	  	$('#rental-activity-form').attr('data-id', id);
	  	$('#rental-activity-modal').modal('show');

	  	loadComment(id);
	});

	$('#rental-activity-form').submit(function(event) {
	    event.preventDefault();
	    var $this = $(this);
	    var url = $(this).attr('action');
	    var id = $(this).attr('data-id');

	    var data = {
	      comment: $('#rental-activity-comment').val(),
	      type: 8,
	    };

	    $.ajax({
	      	url: url,
	      	type: 'POST',
	      	data: data,
	      	dataType: 'json',
	      	success: function(result){
	        	if(result.status){
		          	$('#rental-activity-comment').val('');
		          	loadComment(id);
		          	$(latest_comment).html(data.comment);
		          	// $('#rental-activity-modal').modal('hide');
	        	}else{
	          		alert(result.message);
	        	}
	      	},
	      	error: function(error){
	        	alert('Somthing want wrong');
	      	}
	    });
	});

	$('.upload-status-section .link a').each(function(){
	    v = $(this).attr('href');
	    if (!v.match(/^[a-zA-Z]+:\/\//))
	    {
	        v = 'http://' + v;
	        $(this).attr('href',v);
	    }
	});

});