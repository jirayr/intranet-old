window.einkaufSelectDir = "";

$(document).ready(function(){

	$('.sheet-2').html("");

	$('.buy-sheet tr .change-en-user').each(function(){
        if($(this).val()=="")
        {
          if($(this).attr('data-type')==96 || $(this).attr('data-type')==98)
            $(this).val(16)
          else if($(this).attr('data-type')==95)
            $(this).val(10)
          else
            $(this).val(transaction_m_id);
        }  
  	});

  	if($('.share-deal-checkbox').is(':checked')){
	      $('.share-deal').each(function(){
	        $(this).closest('tr').addClass('bg-light-gray');
	      })
	}

  	$('.share-deal-checkbox').click(function(){
    	if($('.share-deal-checkbox').is(':checked')){
        	$('.share-deal').each(function(){
          		$(this).closest('tr').addClass('bg-light-gray');
        	});
    	}else{
        	$('.share-deal').each(function(){
          		$(this).closest('tr').removeClass('bg-light-gray');
        	});
    	}
  	});

  	var total1 = 0;
  	$('.total1').each(function(){
      	if($(this).html() != "" && $(this).html() != "Empty")
        	total1 += parseFloat($(this).html());
  	});

  	$('.sum_total1').html(total1);

  	var total2 = 0;
  	$('.total2').each(function(){
      	if($(this).html() != "" && $(this).html() != "Empty")
        	total2 += parseFloat($(this).html());
  	});

  	$('.sum_total2').html(total2);

  	$('.isend-mail').click(function(){

    	var checkshare = 0;
    	if($('.share-deal-checkbox').is(':checked'))
      		checkshare = 1;

    	error = 0;
    	$('.ein-sheet-1 tr .checkbox-einkauf').each(function(){
        	if(checkshare==1 && $(this).hasClass('share-deal')){

	        }else if($(this).hasClass('share-deal-checkbox')){

        	}else{
          		if($(this).is(':checked')){
          		}else{
              		error =error +  1;
          		}
        	}
    	});

    	$('.ein-sheet-1 tr .change-in-saller').each(function(){

	        if($(this).val()!=""){
	        }
	        else{
	            if($(this).is(":visible"))
	            	error =error + 1;
	        }
    	});

    	$('.ein-sheet-1 tr .change-en-user').each(function(){
      		if(checkshare==1 && $(this).hasClass('share-deal')){

        	}else{
          		if($(this).val()!=""){
          		}else{
              		if($(this).is(":visible"))
              			error =error + 1;
          		}
        	}
    	});

    	if(error>0){
      		alert("Bitte alle Felder ausfüllen");
      		return false;
    	}

    	if($(this).attr('data-status')=="1"){
      		property_id = $('.property_id').val();
      		$.ajax({
          		type : 'POST',
          		url : url_property_einkaufsendmail,
          		data : {property_id:property_id,  _token : _token },
          		success : function (data) {
              		alert(data);
          		}
      		});
    	}

  	});

  	$('.isend-mail2').click(function(){
    	property_id = $('.property_id').val();
    	$.ajax({
        	type : 'POST',
        	url : url_property_einkaufsendmail2,
        	data : {property_id:property_id,  _token : _token },
        	success : function (data) {
            	alert(data);
        	}
    	});
  	});

  	step = d_status = "";
  	$('body').on('click','.release-procedure-1',function(){
      	step = $(this).attr('data-id');
      	$('#release-property-modal').modal();
  	});

  	$('body').on('click','.release-procedure-request',function(){
    	property_id = $('.property_id').val();
    	comment = $('.release-comment').val();
    	$.ajax({
          	type : 'POST',
          	url : url_property_releaseprocedure,
          	data : {property_id:property_id,step:step,  _token : _token,comment:comment,lock_id:$('.lock_id').val() },
          	success : function (data) {
              	// alert(data);
              	var path = $('#path-properties-show').val();
              	window.location.href = path + '?tab=einkauf_tab';
          	}
      	});
  	});

  	
  	$('body').on('click','.release-procedure',function(){
    	id = $(this).attr('data-id');
    	property_id = $('.property_id').val();
    	status = $(this).attr('data-status');
    
    	if((status=="1" && !$(this).hasClass('btn-success')) || ($(this).hasClass('btn-success') && id=='btn1_request') || ($(this).hasClass('btn-success') && id=='btn2_request') || ($(this).hasClass('btn-success') && id=='btn3_request') || ($(this).hasClass('btn-success') && id=='btn4_request') ){
      		if(id=='btn3' && and=="1"){
        		error = 0;
        		$('.ein-sheet-1 tr .change-en-user').each(function(){
            		if($(this).hasClass('share-deal-1')){
                		if($(this).val()!=""){
                		}else
                  			error=1;
            			}
            			if(!$('.share-deal-checkbox-1').is(':checked'))
              				error = 1;
        		})
          		if(error==1){
            		alert("Bitte alle Felder ausfüllen");
            		return false;
          		}
      		}
      		if(id=='btn2' && alex=="1"){
        		error = 0;
        		$('.ein-sheet-3 tr .change-en-user').each(function(){
            		if($(this).hasClass('share-deal-2')){
                		if($(this).val()!=""){
                		}else
                  			error=1;
            		}
            		if(!$('.share-deal-checkbox-2').is(':checked'))
              			error = 1;
        		});

          		if(error==1){
            		alert("Bitte alle Felder ausfüllen");
            		return false;
          		}
      		}
      		step = $(this).attr('data-id');
      		$('#release-property-modal').modal();
    	}
  	});

  	$('.irelease-the-property').click(function(){
    	property_id = $('.property_id').val();
    	comment = $('.f-comment').val();
    	$.ajax({
        	type : 'POST',
        	url : url_property_einkaufsendmail,
        	data : {property_id:property_id,release:1,  _token : _token,comment:comment },
        	success : function (data) {
            	var path = $('#path-properties-show').val();
            	window.location.href = path + '?tab=einkauf_tab';
        	}
    	});
  	});

  	$('.irelease-the-property2').click(function(){
	    property_id = $('.property_id').val();
	    $.ajax({
	        type : 'POST',
	        url : url_property_einkaufsendmail2,
	        data : {property_id:property_id,release:1,  _token : _token },
	        success : function (data) {
	            var path = $('#path-properties-show').val();
	            window.location.href = path + '?tab=einkauf_tab';
	        }
	    });
	});

	$.fn.editable.defaults.ajaxOptions = {type: "POST"};

	$('.buy-sheet').find('a.inline-edit').editable({
        success: function(response, newValue) {
          if( response.success === false ){
            return response.msg;
          }
          else{
            setTimeout(function(){
            var total1 = 0;
            $('.total1').each(function(){
                if($(this).html() != "" && $(this).html() != "Empty")
                  total1 += parseFloat($(this).html());
            })
            $('.sum_total1').html(total1);

            var total2 = 0;
            $('.total2').each(function(){
                if($(this).html() != "" && $(this).html() != "Empty")
                  total2 += parseFloat($(this).html());
            })
            $('.sum_total2').html(total2);
          },500);
          }
        }
    });

	$('.checkbox-einkauf').on('click', function () {
	    var type = $(this).attr('data-type');
	    v = 0;
	    if($(this).is(':checked'))
	        v = 1;

	    property_id = $('.property_id').val();

	    $.ajax({
	        type : 'POST',
	        url : url_einkauf_item_update+"/"+property_id+'/'+type,
	        data : {type:type,pk : 'status',value:v, property_id:property_id,  _token : _token },
	        success : function (data) {
	            generate_option_from_json(data, 'country_to_state');
	        }
	    });
	});

	$('.change-en-user').on('change', function () {
	    type = $(this).attr('data-type');
	    column = $(this).attr('column-name')
	    v = $(this).val();

	    property_id = $('.property_id').val();

	    $.ajax({
	        type : 'POST',
	        url : url_einkauf_item_update+"/"+property_id+'/'+type,
	        data : {type:type,pk : column,value:v, property_id:property_id,  _token : _token },
	        success : function (data) {
	            generate_option_from_json(data, 'country_to_state');
	        }
	    });
	});

	$('.text-content').on('change', function () {
	    type = $(this).attr('data-id');
	    column = 'comment'
	    v = $(this).val();
	    property_id = $('.property_id').val();

	    $.ajax({
	        type : 'POST',
	        url : url_einkauf_item_update+"/"+property_id+'/'+type,
	        data : {type:type,pk : column,value:v, property_id:property_id,  _token : _token },
	        success : function (data) {
	            // generate_option_from_json(data, 'country_to_state');
	        }
	    });
	});

	$('.change-in-saller').on('change', function () {
	    column = 'saller_id';
	    v = $(this).val();
	    property_id = $('.property_id').val();

	    $.ajax({
	        type : 'POST',
	        url : url_einkauf_update+"/"+property_id,
	        data : {pk : column,value:v, property_id:property_id,  _token : _token },
	        success : function (data) {
	            generate_option_from_json(data, 'country_to_state');
	        }
	    });
	});

	// email_template_javascript1

	$('.checkbox-td').each(function(){
	  if($(this).find('select').length)
	  {
	    if($(this).find('select').attr('column-name')=="user2" || $(this).find('select').attr('column-name')=="user3")
	      $(this).remove();
	  }
	});

	$('.show-link-einkauf').click(function(){
		// console.log('inner');
	      var cl = $(this).attr('data-class');
	      if($('.'+cl).hasClass('hidden')){
	          $(this).find('i').removeClass('fa-angle-down');
	          $(this).find('i').addClass('fa-angle-up');
	          $('.'+cl).removeClass('hidden');

	      }
	      else{
	          // $(this).html('<i class="fa fa-angle-down"></i>');
	          $(this).find('i').addClass('fa-angle-down');
	          $(this).find('i').removeClass('fa-angle-up');
	          $('.'+cl).addClass('hidden');
	          // $(this).next().addClass('hidden');
	      }
	});

	$('.upload-button-einkauf').on('click', function(e){
	  $(this).prev().trigger("click");
	});

	$('.link-button-einkauf').on('click', function(e){
	    var type = $(this).attr('data-type');
	    var property_id = $('.property_id').val();
		var data = {
	        type: type,
	        property_id: property_id
		};
		$("#select-gdrive-file-model").modal('show');
		$("#select-gdrive-file-type").val('einkauf');
		$("#select-gdrive-file-callback").val('selecteinkaufSubmit');
		$("#select-gdrive-file-data").val(JSON.stringify(data));
		selectGdriveFileLoad(workingDir);
	});

	$('.file-upload input[type="file"]').change(function(){
	  	if(is_dir_path){
		    var formdata = new FormData();
		    property_id = $('.property_id').val();
		    var url = url_upload_einkauf_file;
		    file =$(this).prop('files')[0];
		    formdata.append("file", file);
		    formdata.append('_token', _token);
		    formdata.append('type', $(this).attr('data-type'));
		    formdata.append('property_id', property_id);
		    formdata.append('working_dir', working_dir);
		    var thisEle = $(this);

	     	$.ajax({
	            url: url,
	            type:'POST',
	            contentType: false,
	            processData: false,
	            data:formdata,
	      	}).done(function (data) {
	        	if(data.success){
	          		thisEle.parent().append(data.element);
	          		alert(data.msg);
	        	}else{
	          		alert(data.msg);
	        	}
	        	$('.preloader').hide();
	      	});
	    }else{
	    	alert("Property directory is not ready to upload file.");
	    }
	});

	window.selecteinkaufSubmit = function(basename, dirname,curElement, dirOrFile){
		
	  	var dataObj = $("#select-gdrive-file-data").val();
	  	dataObj =JSON.parse(dataObj);
	  	var formdata = new FormData();
	  	formdata.append('_token', _token);
	  	formdata.append('property_id', dataObj.property_id);
	  	formdata.append('basename', basename);
	  	formdata.append('dirname', dirname);
	  	formdata.append('type', dataObj.type);
	  	$('.preloader').show();
	  	var url = url_selec_einkaufdir;
	  	if(dirOrFile == 'file'){
	    	url = url_select_einkauf_file;
	  	}

	  	$.ajax({
	    	type: 'POST',
	    	url: url,
	    	contentType: false,
	    	processData: false,
	    	data:formdata,
	  	}).done(function (data) {
	    	if(data.success){
	      		// einkauf_file_link_isPageReload = false;
	      		curElement.remove();
	      		$('.uploaded-files-einkauf.type-'+dataObj.type).append(data.element);
	      		alert(data.msg);
	    	}else{
	      		alert(data.msg);
	    	}
	    	$('.preloader').hide();
	  	});
	}

});