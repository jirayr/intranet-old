$(document).ready(function(){
	
	$('.change-p-user').on('change', function() {
        var propertyId = $('#selected_property_id').val();
        $.ajax({
            url: url_updatepropertyuser,
            type: "get",
            data: {
                'transaction_m_id': $('.change_transaction_m_id').val(),
                'seller_id': $('.change_seller_id').val(),
                'asset_m_id': $('.change_asset_m_id').val(),
                'asset_m_id2': $('.change_asset_m_id2').val(),
                'property_status': $('.change_property_status').val(),
                'construction_m_id': $('.construction_manager').val(),
                'property_id': propertyId
            },
            success: function(data) {
                if (data.status == 0) alert(data.message);
            }
        });
    });

    var propertyId;
    $('body').on('change', '.property-status', function() {
        if ($(this).val() == "1") {
            $('#Modal').modal('show');
        }
        var selection = $('.property-status');
        selection.prop("disabled", true);
        var propertyId = $(this).data('property-id');
        var status = $(this).val();
        var notificationType = $(this).data('notification-type');
        var data = {
            _token: _token,
            status: status,
            property_id: propertyId
        };
        $.ajax({
            type: 'POST',
            url: url_properties_change_status,
            data: data,
            success: function(data) {
                if (data.status == 0) {
                    alert(data.message);
                    selection.prop("disabled", false);
                    $('.property-status').val(data.oldstatus)
                } else {
                    newNotification(propertyId, 3);
                    // location.reload();
                }
            }
        });
    });

    $(".set_as_standard").change(function() {
        $.ajax({
            url: url_property_set_as_standard + '/' + $(this).val(),
            success: function(response) {
                if (response.msg == 'success') {
                    // location.reload();
                }
            }
        });
    });

    let propertyIframe = $('#kalkulation iframe');
    propertyIframe.load(function() {
        $('#kalkulation iframe').contents().find('.bnk-name').find('a.inline-edit').editable({
            success: function(response, newValue) {
                if (response.success === false) return response.msg;
                else {
                    var path = $('#path-properties-show').val();
                    window.location.href = path + '?tab=properties';
                }
            }
        });
        if (isNaN($.cookie('property-zoom-value'))) {
            $.removeCookie('property-zoom-value');
        }
        var zoomValue = 1;
        if ($.cookie('property-zoom-value')) {
            zoomValue = parseFloat(parseFloat($.cookie('property-zoom-value')).toFixed(1));
        } else {
            $.cookie('property-zoom-value', 1);
        }
        propertyIframe = $('#kalkulation iframe').contents().find('.bank-tab iframe');
        $('span#property-zoom-value').text((zoomValue * 100).toFixed(0));
        
        propertyIframe.contents().find('.pr-zoom-in-out-table').css({
            'zoom': zoomValue,
            '-webkit-transform': 'scale(' + zoomValue + ')',
            '-moz-transform': 'scale(' + zoomValue + ')',
            '-ms-transform': 'scale(' + zoomValue + ')',
            '-o-transform': 'scale(' + zoomValue + ')',
            'transform': 'scale(' + zoomValue + ')',
            'transform-origin': 'top left'
        });
        $('#property-zoom-in').on('click', function() {
            zoomValue = parseFloat(parseFloat($.cookie('property-zoom-value')).toFixed(1));
            if (zoomValue < 2.0) {
                zoomValue += 0.1;
                $.cookie('property-zoom-value', zoomValue);
            }
            $('span#property-zoom-value').text((zoomValue * 100).toFixed(0));
            propertyIframe.contents().find('.pr-zoom-in-out-table').css({
                'zoom': zoomValue,
                '-webkit-transform': 'scale(' + zoomValue + ')',
                '-moz-transform': 'scale(' + zoomValue + ')',
                '-ms-transform': 'scale(' + zoomValue + ')',
                '-o-transform': 'scale(' + zoomValue + ')',
                'transform': 'scale(' + zoomValue + ')',
                'transform-origin': 'top left'
            });
        });
        $('#property-zoom-out').on('click', function() {
            zoomValue = parseFloat(parseFloat($.cookie('property-zoom-value')).toFixed(1));
            if (zoomValue >= 0.6) {
                zoomValue -= 0.1;
                $.cookie('property-zoom-value', zoomValue);
            }
            $('span#property-zoom-value').text((zoomValue * 100).toFixed(0));
            propertyIframe.contents().find('.pr-zoom-in-out-table').css({
                'zoom': zoomValue,
                '-webkit-transform': 'scale(' + zoomValue + ')',
                '-moz-transform': 'scale(' + zoomValue + ')',
                '-ms-transform': 'scale(' + zoomValue + ')',
                '-o-transform': 'scale(' + zoomValue + ')',
                'transform': 'scale(' + zoomValue + ')',
                'transform-origin': 'top left'
            });
        });
    });

});