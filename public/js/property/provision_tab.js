function getprovisionbutton() {
    var id = $('#selected_property_id').val();
    $.ajax({
        url: url_getProvisionbutton,
        type: "get",
        dataType: 'json',
        data: {
            id: id
        },
        success: function(response) {
            $('.provision_info-table').html(response.table);
            // $.each(response.buttons1, function(key1, value1) {
            //     $('.tenantbtn1-' + key1).html(value1);
            // });

            $.each(response.buttons2, function(key1, value1) {
                $('.tenantbtn2-' + key1).html(value1);
            });
            $('.provision-release-logs').html(response.logs);
            setTimeout(function() {
                var columns = [
                    null,
                    null, 
                    {"type": "numeric-comma"}, 
                    {"type": "numeric-comma"},
                    {"type": "numeric-comma"},
                    null, 
                    {"type": "numeric-comma"}, 
                    {"type": "numeric-comma"},
                    null,
                    null,
                    null,
                    null,
                ];
                makeDatatable($('#provisiondatatable'), columns);
            }, 100);
            // makeDatatable($('#provisiondatatable'));
        }
    });
}

function getProvisionComment(url, limit = ''){
    $.ajax({
        url: url+'/'+limit,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#provision_comments_table').find('tbody').empty();
            
            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_provision_comment.replace(":id", value.id);
                    var tr = '\
                    <tr>\
                        <td>'+ value.user_name +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td><a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger delete-provision-comment">Löschen</a></td>\
                    </tr>\
                    ';

                    $('#provision_comments_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

$(document).ready(function(){

	getprovisionbutton();

	var provision_id;
	var provision_url;
	$('body').on('click', '.provision-comment', function() {
	    provision_url = $(this).attr('data-url');
	    provision_id = $(this).attr('data-id');
	    var limit_text = $('#provision_comments_limit').text();
	    getProvisionComment(provision_url, (limit_text == 'Show More') ? 1 : '');
	    $('#provision_comment_modal').modal('show');
	});

	$('#provision_comment_form').submit(function(event) {

	    event.preventDefault();
	    var $this = $(this);
	    var url = $(this).attr('action');
	    var comment = $($this).find('textarea[name="comment"]').val();
	    var data = {'provision_id': provision_id, 'comment': comment};

	    $.ajax({
	        url: url,
	        type: 'POST',
	        data: data,
	        dataType: 'json',
	        beforeSend: function() {
	            $($this).find('button[type="submit"]').prop('disabled', true);
	        },
	        success: function(result) {

	            $($this).find('button[type="submit"]').prop('disabled', false);
	            if (result.status == true) {
	                $($this)[0].reset();
	                $($this).find('textarea[name="comment"]').focus();

	                var limit_text = $('#provision_comments_limit').text();
	                getProvisionComment(provision_url, (limit_text == 'Show More') ? 1 : '');

	            } else {
	                alert(result.message);
	            }
	        },
	        error: function(error) {
	            $($this).find('button[type="submit"]').prop('disabled', false);
	            alert('Somthing want wrong!');
	        }
	    });
	    
	});

	$('body').on('click', '#provision_comments_limit', function() {
	    var text = $(this).text();
	    var limit = '';
	    if(text == 'Show More'){
	        limit = '';
	        $(this).text('Show Less');
	    }else{
	        limit = 1;
	        $(this).text('Show More');
	    }
	    getProvisionComment(provision_url, limit);
	});

	$('body').on('click', '.delete-provision-comment', function() {
	    var url = $(this).attr('data-url');
	    var $this = $(this);
	    if(confirm('Are you sure to delete this comment?')){
	        $.ajax({
	            url: url,
	            type: 'GET',
	            dataType: 'json',
	            success: function(result) {
	                if(result.status){
	                    var limit_text = $('#provision_comments_limit').text();
	                    getProvisionComment(provision_url, (limit_text == 'Show More') ? 1 : '');
	                }else{
	                    alert(result.message);
	                }
	            },
	            error: function(error) {
	                alert('Somthing want wrong!');
	            }
	        });
	    }else{
	        return false;
	    }
	});

	$('body').on('click', '.provision-release-request', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).closest('td').attr('data-id');
        $('.provision-release-comment').val("")
        $('#provision-release-property-modal').modal();
    });

    $('body').on('click', '.provision-release-submit', function() {
        comment = $('.provision-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_provisionreleaseprocedure,
            data: {
                tenant_id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                property_id: $('#selected_property_id').val()
            },
            success: function(data) {
                getprovisionbutton();
            }
        });
    });

    $('body').on('click', '.add-new-provision', function() {
        var $this = $(this);
        var data = {
            property_id: $('#selected_property_id').val(),
            pk: 'name',
            value: '',
            _token: _token,
            id: 0
        };
        $.ajax({
            url: url_changeprovisioninfo,
            type: "post",
            data: data,
            success: function(response) {
                getprovisionbutton();
            }
        });
    });

    $('body').on('click', '.delete-provision', function() {
        var $this = $(this);
        var data = {
            id: $(this).attr('data-id'),
            _token: _token,
        };
        if(confirm('Are you sure want to delete?')){
            $.ajax({
                url: url_deleteprovisioninfo,
                type: "post",
                data: data,
                success: function(response) {
                    getprovisionbutton();
                }
            });    
        }
    });

    $('body').on('change', '.change-provision', function() {
        var $this = $(this);
        var data = {
            property_id: $('#selected_property_id').val(),
            pk: $(this).attr('data-column'),
            value: $(this).val(),
            _token: _token,
            id: $(this).attr('data-id')
        };
        $.ajax({
            url: url_changeprovisioninfo,
            type: "post",
            data: data,
            success: function(response) {
                // if($this.attr('data-column')=="amount7")
                getprovisionbutton();
            }
        });
    });

    var provision_id = '';
    $('body').on('click','.btn-provision-not-release',function(){
        provision_id = $(this).attr('data-id');
        $('#provision-not-release-modal').modal('show');
    });

    $('#provision-not-release-form').submit(function(event) {
        event.preventDefault();
        var comment = $(this).find('textarea[name="message"]').val();
        var url = $(this).attr('action')+'/'+provision_id;

        $.ajax({
              type : 'POST',
              url : url,
              data : {comment:comment},
              dataType: 'json',
              success : function (data) {
                if(data.status){
                  // window.location.reload(true);
                  $('#provision-not-release-modal').modal('hide');
                  getprovisionbutton();
                }else{
                    alert(data.message);
                }
              },
              error : function(e){
                alert('Somthing want wrong!');
              }
          });
    });

});