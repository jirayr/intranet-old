function add_versicherungen(type) {
    $('#add_versicherungen_type').val(type);
    $('#add_versicherungen').modal('show');
}
$(document).ready(function(){

	makeDatatable($('#user-form-data'));
    makeDatatable($('#user-form-data1'));

    $('.versicherunge_date').datepicker({
        dateFormat: 'yy-mm-dd'
    });

    if ($('.insurance-inline').length)
    	$('.insurance-inline').editable();

    $('body').on("click", ".insurance-inline", function() {
	    var input_class = $(this).attr('data-inputclass');
	    if(typeof input_class !== 'undefined' && input_class != ''){
	        $('.editableform').find('input').addClass(input_class);
	    }else{
	        $('.editableform').find('input').removeClass('mask-input-number');
	        $('.editableform').find('input').removeClass('mask-input-new-date');
	    }
	});

    var r_type = "";
    $('body').on('click', '.request-button', function() {
        r_type = $(this).attr('data-id');
        $('#insurance-modal-request').modal();
    });

    $('body').on('click', '.insurance-comment-submit', function() {
        property_id = $('.property_id').val();
        comment = $('.insurance-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_addmaillog,
            data: {
                property_id: property_id,
                step: r_type,
                _token: _token,
                comment: comment
            },
            success: function(data) {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=insurance_tab';
            }
        });
    });

    $('body').on("change", ".insurance-notice-period", function() {
	    var url = $(this).attr('data-url');
	    var field = $(this).attr('data-field');
	    var value = $(this).val();

	    $.ajax({
	        url: url,
	        type: 'POST',
	        data: {'pk': field, 'value': value},
	        dataType: 'json',
	        success: function(response) {
	            if(!response.success){
	                alert(response.message);
	            }
	        },
	        error: function(error) {
	            alert('Somthing want wrong!');
	        }   
	    });
	});

	$('body').on('click', '.versicherunge-checkbox-is-approved', function() {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: url_update_versicherung_approved_status,
            data: {
                pk: pk,
                value: v,
                id: id,
                _token: _token
            },
            success: function(data) {}
        });
    });

    $('body').on('click', '.delete-record', function() {
        var r = confirm("Do you really want to Delete this Record!");
        if (r == true) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: 'POST',
                url: url_delete_Property_insurance1,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    location.reload();
                    alert('Record Deleted !');
                }
            });
        }
    });
});