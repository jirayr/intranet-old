function preview_image() {
    var formData = new FormData();
    $.each(jQuery('#images')[0].files, function(i, file) {
        formData.append('file[' + i + ']', file);
    });
    $.ajax({
        url: url_uploadfiles, //Server script to process data
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(json) {
            $('#images').val('')
            $.each(json.files, function(index, value) {
                var str = '<input type="hidden" name="img[]" value="' + json.filename[index] + '">';
                $('#image_preview').append("<div>" + str + "<img src='" + value + "' style='width:150px; height:149px; object-fit:cover; float:left; margin-top: 6px; margin-right: 10px;' ></div>");
            });
        }
    });
}
$(document).ready(function(){

	$('body').on('click', '.imgDeleteBtn', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_ads_images,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('.creating-ads-img-wrap').remove();
                }
            });
        }
    });

    $('.setimagefavourite').on('click', function() {
        var $this = $(this);
        if (confirm("Are you sure want to set as favourite?")) {
            var id = $(this).closest('.img-action-wrap').attr('data-id');
            var key = $(this).attr('data-id');
            $.ajax({
                url: url_set_ad_image_favourite,
                type: "GET",
                data: {
                    id: id,
                    key: key
                },
                success: function(response) {
                    $this.closest('form').find('i').removeClass('fa-star').addClass('fa-star-o');
                    if (response == 1) $this.find('i').removeClass('fa-star-o').addClass('fa-star');
                }
            });
        }
    });

    $("#pdf").change(function() {
        $("#pdf_name").text(this.files[0].name);
    });

    if($('#sortablelist1').length>0){

        new Sortable(sortablelist1, {
            animation: 150,
            ghostClass: 'sortable-ghost',
            onChange: function(evt) {/**Event*/

                var checked_arr = [];
                $('.dr-image-1').each(function(){
                    checked_arr.push($(this).val());
                });

                k=0;
                $('.setasfavourite').each(function(){
                        $(this).attr('data-id',k);
                        k=k+1;
                });

                $.ajax({
                    type : 'POST',
                    url : url_update_image_order1,
                    data : {i_arr:checked_arr, _token : _token,property_id:$('#selected_property_id').val()},
                    success : function (data) {

                    }
                });
            },
        }); 
    }

});