$(document).ready(function(){

	$(".bank-search").select2({
        minimumInputLength: 2,
        language: {
            inputTooShort: function() {
                return "Please enter 2 or more characters";
            }
        },
        tags: false,
        ajax: {
            url: url_searchbank,
            dataType: 'json',
            data: function(query) {
                return query;
            },
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.name,
                            slug: item.slug,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $('body').on('chenge keyup', '.purchase_price', function() {
	    var id = $(this).attr('id');

	    var gesamt_in_eur = makeNumber($('#gesamt_in_eur').val());

	    var building = makeNumber($('#building').val());
	    var building_amount = makeNumber($('#building_amount').val());

	    var plot_of_land = makeNumber($('#plot_of_land').val());
	    var plot_of_land_amount = makeNumber($('#plot_of_land_amount').val());

	    // console.log({id, gesamt_in_eur, building, building_amount, plot_of_land_amount, plot_of_land_amount});

	    if(id == 'building'){
	        if(building > 100){
	            $('#building').val(100);
	            building = 100;
	        }
	        plot_of_land = (100 - building);

	        $('#plot_of_land').val( makeNumberFormat(plot_of_land) );
	        $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
	        $('#plot_of_land_amount').val( makeNumberFormat(gesamt_in_eur * plot_of_land / 100) );

	    }else if(id == 'building_amount'){

	        building =  (building_amount * 100 / gesamt_in_eur);
	        plot_of_land = (100 - building);

	        $('#building').val( makeNumberFormat(building) );
	        $('#plot_of_land').val( makeNumberFormat(plot_of_land) );
	        
	        $('#plot_of_land_amount').val( makeNumberFormat(gesamt_in_eur * plot_of_land / 100) );

	    }else if(id == 'plot_of_land'){
	        if(plot_of_land > 100){
	            $('#plot_of_land').val(100);
	            plot_of_land = 100;
	        }
	        building = (100 - plot_of_land);

	        $('#building').val( makeNumberFormat(building) );
	        $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
	        $('#plot_of_land_amount').val( makeNumberFormat(gesamt_in_eur * plot_of_land / 100) );

	    }else if(id == 'plot_of_land_amount'){
	        plot_of_land = (plot_of_land_amount * 100 / gesamt_in_eur);
	        building = (100 - plot_of_land);

	        $('#building').val( makeNumberFormat(building) );
	        $('#plot_of_land').val( makeNumberFormat(plot_of_land) );

	        $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
	    }
	    
	});

	$('.add-more-tenant').click(function() {
        h = $('.tenant').html();
        $(this).closest('form').find('.sheet-tenant-list').append(h);
        $('.mask-number-input').mask('#.##0,00', {
            reverse: true
        });
        $('.mask-number-input-negetive').maskMoney({
            allowNegative: true,
            thousands: '.',
            decimal: ',',
            affixesStay: false,
            precision: 2
        });
        $('.mask-date-input').mask('00/00/0000');
    });

    $('body').on('click', '.remove-ctenant', function() {
        $(this).closest('tr').remove();
    });

    $('.save-calculation-sheet').on('click', function() {
        var str = "";
        $(this).closest('form').find('.date-change-checkbox').each(function() {
            v = 0;
            if ($(this).is(':checked')) v = 1;
            str = str + v + ",";
        });
        $('.dynamic_list').val(str);
        var str = "";
        $(this).closest('form').find('.is_current_net-checkbox').each(function() {
            v = 0;
            if ($(this).is(':checked')) v = 1;
            str = str + v + ",";
        });
        $('.rent_list').val(str);

        var str = "";
        $(this).closest('form').find('.ank_1').each(function() {
            v = 0;

            if ($(this).is(':checked')) 
                v = 1;

            str = str + v + ",";
        });
        $('.ank_1_list').val(str);

        var str = "";
        $(this).closest('form').find('.ank_2').each(function() {
            v = 0;
            
            if ($(this).is(':checked')) 
                v = 1;

            str = str + v + ",";
        });
        $('.ank_2_list').val(str);

        return true;
    });

    $('body').on('keyup', '.s_purchase_price', function() {
        var total_purchase_price_el = $(this).closest('.row').find('.total_purchase_value');
        var from_bond_per_el = $(this).closest('.row').find('.from_bond');
        var from_bond_amount_el = $(this).closest('.row').find('.from_bond_amount');
        var bank_loan_per_el = $(this).closest('.row').find('.bank_loan');
        var bank_loan_amount_el = $(this).closest('.row').find('.bank_loan_amount');

        var total_purchase_price = makeNumber($(total_purchase_price_el).val());//gesamt_in_eur

        var from_bond_per = makeNumber($(from_bond_per_el).val());//building
        var from_bond_amount = makeNumber($(from_bond_amount_el).val());//building_amount

        var bank_loan_per = makeNumber($(bank_loan_per_el).val());//plot_of_land
        var bank_loan_amount = makeNumber($(bank_loan_amount_el).val());//plot_of_land_amount

        // console.log({id, gesamt_in_eur, building, building_amount, plot_of_land_amount, plot_of_land_amount});

        var id = '';
        if($(this).hasClass('from_bond')){
            id = 'from_bond';
        }else if($(this).hasClass('from_bond_amount')){
            id = 'from_bond_amount';
        }else if($(this).hasClass('bank_loan')){
            id = 'bank_loan';
        }else if($(this).hasClass('bank_loan_amount')){
            id = 'bank_loan_amount';
        }

        if(id == 'from_bond'){
            if(from_bond_per > 100){
                $(from_bond_per_el).val( makeNumberFormat(100, 10) );
                from_bond_per = 100;
            }
            bank_loan_per = (100 - from_bond_per);

            $(bank_loan_per_el).val( makeNumberFormat(bank_loan_per, 10) );
            $(from_bond_amount_el).val( makeNumberFormat((total_purchase_price * from_bond_per / 100), 2) );
            $(bank_loan_amount_el).val( makeNumberFormat((total_purchase_price * bank_loan_per / 100), 2) );

        }else if(id == 'from_bond_amount'){

            from_bond_per =  (from_bond_amount * 100 / total_purchase_price);
            bank_loan_per = (100 - from_bond_per);

            $(from_bond_per_el).val( makeNumberFormat(from_bond_per, 10) );
            $(bank_loan_per_el).val( makeNumberFormat(bank_loan_per, 10) );
            
            $(bank_loan_amount_el).val( makeNumberFormat((total_purchase_price * bank_loan_per / 100), 2) );

        }else if(id == 'bank_loan'){
            if(bank_loan_per > 100){
                $(bank_loan_per_el).val( makeNumberFormat(100, 10) );
                bank_loan_per = 100;
            }
            from_bond_per = (100 - bank_loan_per);

            $(from_bond_per_el).val( makeNumberFormat(from_bond_per, 10) );
            $(from_bond_amount_el).val( makeNumberFormat((total_purchase_price * from_bond_per / 100), 2) );
            $(bank_loan_amount_el).val( makeNumberFormat((total_purchase_price * bank_loan_per / 100), 2) );

        }else if(id == 'bank_loan_amount'){
            bank_loan_per = (bank_loan_amount * 100 / total_purchase_price);
            from_bond_per = (100 - bank_loan_per);

            $(from_bond_per_el).val( makeNumberFormat(from_bond_per, 10) );
            $(bank_loan_per_el).val( makeNumberFormat(bank_loan_per, 10) );

            $(from_bond_amount_el).val( makeNumberFormat((total_purchase_price * from_bond_per / 100), 2) );
        }
        
    });
    
});