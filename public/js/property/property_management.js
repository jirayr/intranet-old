function add_property_management(add) {
    var id = $('#selected_property_id').val();
    $.ajax({
        type: 'POST',
        url: url_property_management_add,
        data: {
            id: id,
            _token: _token,
            add: add
        },
        success: function(data) {
            $('.management-table-list').html(data.table);
            init_manage_table();
        }
    });
}

function init_manage_table() {
    var columns = [
        null, {
            "type": "numeric-comma"
        }, {
            "type": "numeric-comma"
        }, {
            "type": "numeric-comma"
        },
        null,
        null,
        null,
        null,
        null,
    ]
    makeDatatable($('.property-management-table'), columns);
}

function delete_property_management(id) {
    if (confirm("Are you sure want to delete this?")) {
        pid = $('#selected_property_id').val();
        $.ajax({
            type: 'POST',
            url: url_property_management_add,
            data: {
                id: id,
                _token: _token,
                delete: 1,
                property_id: pid
            },
            success: function(data) {
                $('.management-table-list').html(data.table);
                init_manage_table();
            }
        });
    }
}

$(document).ready(function(){

	add_property_management(0);

	$('body').on('click', '.management-request-button', function() {
        if ($(this).attr('data-id') == "management") c = 1;
        else c = 0;
        $('.change-property-manage-status').each(function() {
            if ($(this).is(":checked") && $(this).attr('data-column') == "release_status2") c = 1;
        });
        if (c == 0) {
            alert("Bitte Checkbox auswählen")
        } else {
            r_type = $(this).attr('data-id');
            $('#management-modal-request').modal();
        }
    });

    $('body').on('click', '.management-comment-submit', function() {
        var property_id = $('.property_id').val();
        var comment = $('.management-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_addmaillog,
            data: {
                property_id: property_id,
                step: r_type,
                _token: _token,
                comment: comment
            },
            success: function(data) {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=property_management';
            }
        });
    });

    $('body').on('click', '.change-property-manage-status', function() {
        v = 0;
        if ($(this).is(':checked')) v = 1;
        data = {
            property_id: $('#selected_property_id').val(),
            pk: $(this).attr('data-column'),
            value: v,
            _token: _token,
            id: $(this).attr('data-id')
        };
        $this = $(this);
        $.ajax({
            url: url_property_management_update,
            type: "post",
            data: data,
            success: function(response) {
                $('.management-table-list').html(response.table);
                init_manage_table();
            }
        });
    });

    $('body').on('change', '.change-property-manage', function() {
        data = {
            property_id: $('#selected_property_id').val(),
            pk: $(this).attr('data-column'),
            value: $(this).val(),
            _token: _token,
            id: $(this).attr('data-id')
        };
        $this = $(this);
        $.ajax({
            url: url_property_management_update,
            type: "post",
            data: data,
            success: function(response) {
                $('.management-table-list').html(response.table);
                init_manage_table();
            }
        });
    });

    $('body').on('click','.btn-propert-management-not-release',function(){
        $('#property-management-not-release-modal').modal('show');
        vacant_id = $(this).attr('data-id');
    });

    $('#property-management-not-release-form').submit(function(event) {
        event.preventDefault();
        var comment = $(this).find('textarea[name="message"]').val();
        var url = $(this).attr('action');

        $.ajax({
            type : 'POST',
            url : url,
            data : {comment:comment,id:vacant_id},
            dataType: 'json',
            success : function (data) {
                if(data.status){
                  window.location.reload(true);
                }else{
                    alert(data.message);
                }
            },
            error : function(e){
                alert('Somthing want wrong!');
            }
        });
    });

    $('body').on('click', '.pmanagement-request-button', function() {
        r_type = $(this).attr('data-column');
        property_id = $(this).attr('data-id');
        $('#pmanagement-modal-request').modal();
        
    });

    $('body').on('click', '.pmanagement-comment-submit', function() {
        // property_id = $('.property_id').val();
        comment = $('.pmanagement-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_addmaillog,
            data: {
                property_id: property_id,
                step: r_type,
                _token: _token,
                comment: comment
            },
            success: function(data) {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=property_management';
            }
        });
    });

});