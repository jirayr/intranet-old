String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};

function makeNumber(value){
    value = value.replaceAll(".", "");
    value = value.replaceAll(",", ".");
    value = value.replaceAll("%", "");
    value = value.replaceAll("€", "");
    value = (!isNaN(value) && value != '') ? parseFloat(value) : 0;
    return value;
}

function makeNumberFormat(value, decimal = 2){
    if(value){
        value = value.toFixed(decimal);
        value = parseFloat(value);
    }
    return value.toLocaleString("es-ES", {minimumFractionDigits: decimal});
}

function showFlash(element, msg, type) {
    if (type == 'error') {
        $(element).html('<p class="alert alert-danger">' + msg + '</p>');
    } else {
        $(element).html('<p class="alert alert-success">' + msg + '</p>');
    }
    setTimeout(function() {
        $(element).empty();
    }, 5000);
}

function generate_option_from_json(jsonData, fromLoad){
    //Load Category Json Data To Brand Select
    var select_a_brand = ('#select_a_brand').val();

    if (fromLoad === 'category_to_brand'){
        var option = '';
        if (jsonData.length > 0) {
            option += '<option value="0" selected> '+select_a_brand+' </option>';
            for ( i in jsonData){
                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
            }
            $('#brand_select').html(option);
            $('#brand_select').select2();
        }else {
            $('#brand_select').html('');
            $('#brand_select').select2();
        }
        $('#brand_loader').hide('slow');
    }else if(fromLoad === 'country_to_state'){
        var option = '';
        if (jsonData.length > 0) {
            option += '<option value="0" selected>  Bundesland auswählen  </option>';
            for ( i in jsonData){
                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
            }
            $('#state_select').html(option);
            //$('#state_select').select2();
        }else {
            $('#state_select').html('');
           // $('#state_select').select2();
        }
        $('#state_loader').hide('slow');

    }else if(fromLoad === 'vcountry_to_state'){
        var option = '';
        if (jsonData.length > 0) {
            option += '<option value="0" selected>  Bundesland auswählen  </option>';
            for ( i in jsonData){
                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
            }
            $('#vstate_select').html(option);
            //$('#state_select').select2();
        }else {
            $('#vstate_select').html('');
           // $('#state_select').select2();
        }
        $('#state_loader').hide('slow');

    }
    else if(fromLoad === 'excountry_to_state'){
        var option = '';
        if (jsonData.length > 0) {
            option += '<option value="0" selected>  Bundesland auswählen  </option>';
            for ( i in jsonData){
                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
            }
            $('#excountry_to_state').html(option);
            //$('#state_select').select2();
        }else {
            $('#excountry_to_state').html('');
           // $('#state_select').select2();
        }
        $('#state_loader').hide('slow');

    }else if(fromLoad === 'state_to_city'){
        var option = '';
        if (jsonData.length > 0) {
            option += '<option value="0" selected>  Stadt auswählen  </option>';
            for ( i in jsonData){
                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
            }
            $('#city_select').html(option);
            //$('#city_select').select2();
        }else {
            $('#city_select').html('');
            //$('#city_select').select2();
        }
        $('#city_loader').hide('slow');
    }
}

$(document).ready(function(){

	$('.nav-tabs > li').on("click",function(e){
	    e.preventDefault();
	    
	    var tab = $(this).attr('data-tab');
	    if(typeof tab !== 'undefined' && tab != ''){
	    	var url = $('#property_tab').attr('data-url');
	    	window.location = url+"?tab="+tab;
	    }
	});

	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on("focus", ".mask-input-date", function() {
        $(this).mask('00/00/0000');
    });

    $(document).on("focus", ".mask-input-new-date", function() {
        $(this).mask('00.00.0000');
    });

    $(document).on("focus", ".mask-input-number", function() {
        $(this).mask('#.##0,00', {
            reverse: true
        });
    });

    $(document).on("focus", ".mask-input-number-six-decimal", function() {
        $(this).mask('#.##0,000000', {
            reverse: true
        });
    });

    $(document).on("focus", ".mask-input-number-ten-decimal", function() {
        $(this).mask('#.##0,0000000000', {
            reverse: true
        });
    });

    $(document).on("focus", ".budget-table input", function() {
        $(this).mask('#.##0,00', {
            reverse: true
        });
    });

    $(document).on("focus", ".mask-number-input-nodecimal", function() {
        $(this).mask('#.##0,00', {
            reverse: true
        });
    });

    $(document).on("focus", ".mask-number-input-negetive", function() {
        $(this).maskMoney({
            allowNegative: true,
            thousands: '.',
            decimal: ',',
            affixesStay: false,
            precision: 2
        });
    });

    $(document).on("focus", ".mask-number-input", function() {
    	$(this).mask('#.##0,00', {
	        reverse: true
	    });
    });

    $(document).on("focus", ".mask-number-input2", function() {
    	$(this).mask('00,000000');
    });

    $(document).on("focus", ".mask-date-input", function() {
    	$(this).mask('00/00/0000');
    });

    $(document).on("focus", ".mask-date-input-new", function() {
    	$(this).mask('00.00.0000');
    });

    $('body').on('click', '#mail-an-am, #mail-an-hv-bu, #mail-an-hv-pm, #mail-an-ek, #mail-an-sb', function() {
        var title = $(this).text();
        var type = $(this).attr('data-type');
        // var user_id = $(this).attr('data-user-id');

        $('#mail_an_am_modal').find('.modal-title').text(title);
        $('#mail_an_am_modal').find('#mail_type').val(type);
        // $('#mail_an_am_modal').find('#am_id').val(user_id);
        $('#mail_an_am_modal').modal('show');
    });

});