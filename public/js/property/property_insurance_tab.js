function makeInsuranceDataTable(tabid){
    var url = url_get_property_insurance_detail.replace(":tab_id", tabid);
    $('#insurance-table-'+tabid).dataTable({
        "ajax": {
            "url" : url,
            // "async": false
        },
        /*"order": [
            [0, "desc"]
        ],*/
        "columns": [
            null,
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            null,
            null,
            null,
            null,
            null,
        ]
    });
}

function getAngebotButton(){
	var id = $('#selected_property_id').val();

    $.ajax({
        type : 'GET',
        url : url_get_angebot_button,
        data : { id : id },
        success : function (data) {
            // generate_option_from_json(data, 'state_to_city');
            $.each(data.btn, function(key, value) {
                $('.ins-btn-' + key).html(value);
            });
        }
    });
}

$(document).ready(function(){

	if($(".tbl-insurance-tab-detail:not(.hidden-table)").length > 0){
        $(".tbl-insurance-tab-detail:not(.hidden-table)").each(function(){
            var tabid = $(this).attr('data-tabid');
            makeInsuranceDataTable(tabid);
        });
    }
    if(is_falk==0)
        getAngebotButton();

    $(document).ajaxComplete(function() {
        if ($('.angebote-comment').length)
            $('.angebote-comment').editable();
    });

    var insurance_tab_log = $('#insurance-tab-log').dataTable({
        "ajax": url_get_property_insurance_detail_log,
        "order": [
            [6, "desc"]
        ],
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            {"type": "new-date-time"}
        ]
    });

    $('body').on('click', '.btn-insurance-tab-detail', function() {
        var tab_id = $(this).attr('data-tabid');
        if(tab_id != ''){
            $('#add_insurance_tab_detail_modal').find('#form_add_insurance_tab_detail').attr('action', url_add_property_insurance_detail);
            $('#add_insurance_tab_detail_modal').find('#property_insurance_tab_id').val(tab_id);
            $('#add_insurance_tab_detail_modal').modal('show');
        }else{
           showFlash($('#insurance_tab_msg'), 'Somthing want wrong!', 'error');
        }
    });

    $('#form_add_insurance_tab_title').submit(function(event) {
        event.preventDefault();

        var $this = $(this);
        var msgElement = $('#add_insurance_tab_title_msg');
        var url = $(this).attr('action');
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache : false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $($this)[0].reset();
                    $('#add_insurance_tab_title_modal').modal('hide');
                    showFlash($('#insurance_tab_msg'), result.message, 'success');

                    var title_delete_url = url_delete_property_insurance_title.replace(':id', result.data.id);

                    var html = $('#insurance-tab-hidden-html').html();
                    html = html.replace(/##ID##/g, result.data.id);
                    html = html.replace('##TITLE##', result.data.title);
                    html = html.replace('##TITLE-DELETE-URL##', title_delete_url);
                    html = html.replace('hidden-table', '');
                    $('#insurance-table-content').append(html);

                    makeInsuranceDataTable(result.data.id);
                    if(is_falk==0)
                        getAngebotButton();

                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    $('body').on('click', '.delete-title', function() {
        var url = $(this).attr('data-url');

        var $this = $(this);
        var msgElement = $('#insurance_tab_msg');

        if(confirm('Are you sure want to delete this title?')){
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if(result.status){
                        $($this).closest('.main_card').remove();
                        $('#insurance-tab-log').DataTable().ajax.reload();
                    }else{
                      showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error){
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                    console.log({error});
                }
            });
        }
        return false;

    });

    $(document).on('click', ".insurance-tab-upload-file-icon", function(){
        $("#insurance_tab_gdrive_file_upload").trigger('click');
    });

    $(document).on('change', "#insurance_tab_gdrive_file_upload", function(e){
        var fileName = e.target.files[0].name;
        $("#insurance_tab_gdrive_file_name_span").text(fileName);
    });

    $(document).on('click', ".link-button-gdrive-insurance-tab", function(e){
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('property_deals');
        $("#select-gdrive-file-callback").val('gdeiveSelectPropertyInsurance');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir, $("#current_tab_name_insurance_tab").val());
    });

    window.gdeiveSelectPropertyInsurance = function(basename, dirname,curElement, dirOrFile){
        $("#insurance_tab_gdrive_file_basename").val(basename);
        $("#insurance_tab_gdrive_file_dirname").val(dirname);
        $("#insurance_tab_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#insurance_tab_gdrive_file_name").val(text);
        $("#insurance_tab_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    $('#form_add_insurance_tab_detail').submit(function(event) {
        event.preventDefault();

        var $this = $(this);
        var msgElement = $('#add_insurance_tab_detail_msg');
        var url = $(this).attr('action');
        var formData = new FormData($(this)[0]);
        formData.append("property_id", current_property_id);

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache : false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){

                    $($this)[0].reset();
                    $('#insurance_tab_gdrive_file_basename').val('');
                    $('#insurance_tab_gdrive_file_dirname').val('');
                    $('#insurance_tab_gdrive_file_type').val('');
                    $('#insurance_tab_gdrive_file_name').val('');
                    $('#insurance_tab_gdrive_file_name_span').text('');
                    $('#insurance_tab_gdrive_file_upload').val('');

                    $('#add_insurance_tab_detail_modal').modal('hide');
                    showFlash($('#insurance_tab_msg'), result.message, 'success');
                    $('#insurance-table-'+result.data.property_insurance_tab_id).DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    $('body').on('click', '.property-insurance-release-request', function() {

        if(is_falk==1){
            // var length = $(this).closest('.modal-body').find('table').find('[data-field="falk_status"]:checked').length;
            // var msg = "Bitte Checkbox Falk auswählen!";
            length = 1;
        }else{
            var length = $(this).closest('.modal-body').find('table').find('[data-field="asset_manager_status"]:checked').length;
            var msg = "Bitte eine AM Checkbox auswählen!";
        }

        if(length > 0){
            vacant_release_type = $(this).attr('data-column');
            vacant_id = $(this).attr('data-id');
            $('.property-insurance-comment').val("")
            $('#property-insurance-release-property-modal').modal();
        }else{
            alert(msg); return false;
        }
    });

    $('body').on('click', '.property-insurance-submit', function() {
        am_id = fk_id = 0;
        if(is_falk==0)
        {
            $('#insurance-table-'+vacant_id+' .am_falk_status').each(function(){
                    if($(this).is(':checked') && $(this).attr('data-field')=="falk_status")
                        fk_id = $(this).attr('data-id');
                    if($(this).is(':checked') && $(this).attr('data-field')=="asset_manager_status")
                        am_id = $(this).attr('data-id');
            });    
        }
        comment = $('.property-insurance-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_dealreleaseprocedure,
            data: {
                id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                am_id:am_id,
                fk_id:fk_id,
                comment: comment,
            },
            success: function(data) {
                if(vacant_release_type=="insurance_request")
                        getAngebotButton();
                $('#insurance-tab-log').DataTable().ajax.reload();
                $('.sec-'+vacant_id).find('.btn').hide();
                $('.sec-'+vacant_id).find('table .btn').show();
                $('.sec-'+vacant_id).find('.am_falk_status').each(function(){
                    $(this).removeClass('am_falk_status');
                });
                // $('#property_deal_table').DataTable().ajax.reload();
                // $('#deal_mail_table').DataTable().ajax.reload();
            }
        });
    });

    $('body').on('click','.deal-mark-as-not-release',function(){
        not_release_id = $(this).attr('data-id');
        $('#deal-not-release-modal').modal();
        $('.deal-not-release-comment').val("")
    });

    $('body').on('click','.deal-not-release-submit',function(){
        comment = $('.deal-not-release-comment').val();
        $.ajax({
              type : 'POST',
              url : url_deal_mark_as_notrelease,
              data : {id:not_release_id, _token : _token, comment:comment},
              success : function (data) {
                  // getprovisionbutton();
                  // $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#insurance-tab-log').DataTable().ajax.reload();
              }
          });
    });

    $('body').on('change', '.am_falk_status', function() {

        var field = $(this).attr('data-field');
        var status = ($(this).prop('checked')) ? 1 : 0;
        var url = $(this).attr('data-url');
        var msgElement = $('#insurance_tab_msg');

        $.ajax({
            url: url,
            type: 'POST',
            data:{
                'field' : field,
                'status' : status
            },
            dataType: 'json',
            success: function(result) {
                if(!result.status){
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });

    });

    $('body').on('click', '.delete-insurance-tab-detail', function() {
        var tab_id = $(this).closest('table').attr('data-tabid');
        var url = $(this).attr('data-url');
        var msgElement = $('#insurance_tab_msg');

        if(confirm('Are you sure want to delete this record?')){
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if(result.status){
                        $('#insurance-table-'+tab_id).DataTable().ajax.reload();
                        $('#insurance-tab-log').DataTable().ajax.reload();
                    }else{
                      showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error){
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                    console.log({error});
                }
            });
        }
        return false;
    });

    $('body').on('click', '.edit-insurance-tab-detail', function() {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result){
                    // $('#add_insurance_tab_detail_modal').find("input[name='name']").val(result.name);
                    $('#add_insurance_tab_detail_modal').find("input[name='amount']").val(result.amount);
                    $('#add_insurance_tab_detail_modal').find("textarea[name='comment']").val(result.comment);
                }
            },
        });

        $('#add_insurance_tab_detail_modal').find('#form_add_insurance_tab_detail').attr('action', url_add_property_insurance_detail+'/'+id);
        $('#add_insurance_tab_detail_modal').modal('show');
    });

    $('body').on('click','.btn-not-release-insurance',function(){
        not_release_id = $(this).attr('data-id');
        table_id = $(this).closest('table').attr('data-tabid');
        $('#insurance-not-release-modal').modal();
        $('.insurance-not-release-comment').val("")
    });

    $('body').on('click','.insurance-not-release-submit',function(){
        comment = $('.insurance-not-release-comment').val();
        $.ajax({
              type : 'POST',
              url : url_insurance_mark_as_notrelease,
              data : {id:not_release_id, _token : _token, comment:comment},
              success : function (data) {
                $('#insurance-not-release-modal').modal('hide');
                $('#insurance-table-'+table_id).DataTable().ajax.reload();
              }
          });
    });

});