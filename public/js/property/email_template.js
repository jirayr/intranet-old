function t_here(){
				
	$("#t_here").html('');
	
	var a5 = $("#a5").val();
	if(a5 == "123"){
		
		$("#t_here").html(a5);
	}
	
}

function statusloi_list() {
    property_id = $('#selected_property_id').val();
    var url = url_getstatusloi + "?id=" + property_id;
    $.ajax({
        url: url,
    }).done(function(data) {
        $('.list-statusloi-div').html(data);
        $('#list-statusloi').DataTable()
    });
}

function loadAllStatusLoiByUser(user_id) {
    var url = url_getStatusLoiByUserId + "?id=" + user_id + "&property_id=" + $('#selected_property_id').val();
    $.ajax({
        url: url,
    }).done(function(data) {
        $('#load_all_status_loi_by_user_content').html('');
        $('#load_all_status_loi_by_user_content').html(data);
        $('#load_all_status_loi_by_user').modal('show');
    });
}

$(document).ready(function(){

	t_here();

	CKEDITOR.replace( 'custom_text' );

	statusloi_list();

	$('#receiver').tagsInput();
    $('#cc').tagsInput();

	$('#datepicker').datepicker({
        dateFormat: 'dd.mm.yy'
    });

    $('#email_template_users').on('change', function () {

        $.ajax({
            type : 'POST',
            url : url_email_template_users,
            dataType: "json",
            data : {user_id:$(this).val(), id: main_id,_token : _token },
            beforeSend: function () {
                $('.user_signature').removeAttr('src');
                $('.user_signature').attr('src', url_gif);
            },
            success : function (data) {
                
                $('.email_input').val('');
                $('.email_input').val(data.email);
                $('.user_signature').removeAttr('src');
                if(data.signature=="")
                    $('.user_signature').hide();
                else{
                    $('.user_signature').show();
                    $('.user_signature').attr('src',data.signature);
                }
            }
        });
    });

    $('.replica-input').keypress( function () {
        // alert($(this).attr("id").concat(1));
        var x = document.getElementById($(this).attr("id")).value;
        var id = $(this).attr("id").concat(1);
        document.getElementById(id).value = x;

    });

    $('body').on('click', '.remove-status-loi', function() {
        id = $(this).attr('data-id');
        if (confirm("Are you sure want to delete?")) {
            $.ajax({
                url: url_removestatusloi,
                type: "get",
                data: {
                    id: id
                },
                success: function(response) {
                    statusloi_list();
                }
            });
        }
    });

    $('body').on('click', '.checkbox-is-sent', function() {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: url_statusloi_update + "/" + id,
            data: {
                pk: pk,
                value: v,
                _token: _token
            },
            success: function(data) {}
        });
    });

});