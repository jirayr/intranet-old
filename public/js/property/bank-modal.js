var resultData = [];

function setBankMailField(email, bankId) {
	$('#bank-id'+bankId).attr('selected-mail-'+bankId,email);
}

function deleteBankEmailLog(id) {
	swal({
		title: "Bist du sicher?",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	})
	.then((willDelete) => {
		if (willDelete) {
			$.ajax({
				type : 'get',
				url : url_delete_bank_email_log+'/'+id,
				success : function (data)
				{
					sweetAlert("Erfolgreich gelöscht");
					location.reload();
				}
			});
		}
	})

}

function validate() {
    Error = 0;
    $("#sendemailtobankers .required").each(function() {
        if (!$.trim($(this).val())) {
            var placeholder = $(this).attr('placeholder');
            $(this).focus();
            toastr.error("Field " + placeholder + " is required");
            $('html, body').animate({
                scrollTop: 0
            }, 500);
            Error = 1;
            return false;
        }
    });
    return Error;
}

function showResult(index) {
    if (resultData[index]) {
        var tblhtml = '';
        $.each(resultData[index], function(k, v) {
            tblhtml += "<tr><td colspan='2' bgcolor='aliceblue'><b>" + k + "</b></td></tr>";
            $.each(v, function(k1, v1) {
                if (k1 == 0) {
                    tblhtml += "<tr><td colspan='2'>" + v1 + "</td></tr>";
                } else {
                    tblhtml += "<tr><td>" + k1 + ":</td><td>" + v1 + "</td></tr>";
                }
            });
        });
        $('#result-table tbody').html(tblhtml);
        $('#result-modal').modal('show');
    } else {
        alert("Data not available");
    }
}

$(document).ready(function(){

	CKEDITOR.replace('mail_message');

	$('#radiusSearch').on('click', function () {
		var property_id = $('#p_id').val();
		var radius = $('#radius').val();
		$.ajax({
			type : 'get',
			url : url_radius_search,
			data : {p_id:property_id,rad:radius,  _token : _token},
			success : function (data) {
				if(data.length !== 0){
					$('.table-row').remove();
					$.each(data, function(key) {
						$(".table-body").append(
								'<tr class="table-row">'+
								'<td><div class="col-md-4 col-md-offset-4 no-padding">'+
								'<input type="checkbox" name="bank_ids[]" value="'+data[key].id+'" class="form-control bank_ids"/>' +
								'</div></td>'+
								'<td>'+'<a href="javascrip:void(0)">'+data[key].name+'</a>'+'</td>'+
								'<td>'+data[key].fullName+'</td>'+
								'<td>'+
								'<button class="btn btn-success show-result-list" type="button" data-email="'+data[key].contact_email+'">'+
								'<i class="fa fa-eye"></i>'+
								'</button>'+'</td>'+
								'<td>'+data[key].contact_email+'</td>'+
								'<td>'+'<a class="btn btn-primary btn-sm send_email_to_banks"  data-bank="'+data[key].id+'" data-property="'+property_id+'" >E-Mail senden</a>'+'</td>'+
								'</tr>'
						);
					});
				}else {
					alert('No Data Found');
				}

			}
		});
	});

	$('.email').on('click', function () {
		$('#noteText').val('');
		$('#attachment-field').css('visibility', 'hidden');
		$('#subject-field').css('visibility', 'hidden');
		$('#message-field').css('visibility', 'hidden');
		$("#subject").val('');
		$("#file").val('');

		$('#emailConfigModel').modal('show');
		var id = $(this).attr('data-id');
		$("#banken_id").val(id);
		var note = $(this).attr('data-note');
		$('#noteText').val(note);
	});

	$('#emailConfigModel').submit(function (e) {
		$('#config-loader').css('visibility', 'visible');
		e.preventDefault();

		$.ajax({
			type : 'post',
			url : url_savenote,
			data :   $('form.config-form').serialize(),
			success : function (data)
			{
				$('#config-loader').css('visibility', 'hidden');
				$('#save-config').removeClass('disabled');
				window.setTimeout(function(){
					$('#emailConfigModel').modal('hide');
					sweetAlert("Saved Successfully");
				}, 600);
				location.reload();
			}
		});

	});

	$('.FinanzierungsangeboteCreate').on('click', function () {
		$('#btn-save').val("create");

		var id = $(this).attr('data-id');
		var prop = $('#bankFinanceId').val();
		// var url ='/properties/'+prop+'/project';
		// $('#finanzierungsanfragen-form').attr('action', url);
		$('.hide-custom').show();
		$('#telefonnotiz').val('');
		$('#interest_rate').val('');
		$('#tilgung').val('');
		$('#fk_share_percentage').val('');
		$('#fk_share_nominal').val('');
		$('.bank').html('');
		$('#FinanzierungsangeboteCreate').modal('show');

	});

	$('#FinanzierungsangeboteCreate').submit(function (e) {

		var state = $('#btn-save').val();
		var url = url_bank_financing_offer;

		if (state == "update") {
			var id 	= $('#bankFinanceId').val();
			var url = url_bank_financing_offer_update+'/'+id;
		}


		e.preventDefault();
		$.ajax({
			type : 'post',
			url : url,
			data :   $('form.finanzierungsanfragen-form').serialize(),
			success : function (data)
			{
				window.setTimeout(function(){
					$('#emailConfigModel').modal('hide');
					sweetAlert("Saved Successfully");
				}, 600);
				location.reload();
			}
		});

	});

	$('.delete-bank-finance-offer').on('click', function () {
		var id = $(this).attr('data-id');

		if(confirm('Are you sure to delete this record ?')) {
			$.ajax({
				url :  url_bank_financing_offer_delete+'/'+id,
				success : function (data)
				{
					sweetAlert("Deleted Successfully");
					location.reload();
				}
			});

		}

	});

	$('.banking-remote').editable({
        select2: {
            placeholder: 'Banken',
            allowClear: true,
            minimumInputLength: 3,
            id: function (item) {
                return item.id;
            },
            ajax: {
                url: url_get_banks,
                dataType: 'json',
                data: function (term, page) {
                    return { query: term };
                },
                results: function (data, page) {
                    return { results: data };
                }
            },
            formatResult: function (item) {
                return item.name;
            },
            formatSelection: function (item) {
                return item.name;
            },

        },
        success: function(response, newValue) {
                location.reload();
        }
    });

    $(".bank").select2({
		minimumInputLength: 2,
		language: {
			inputTooShort: function () {
				return "Please enter 2 or more characters"
			}
		},
		multiple: false,
		tags: false,
		ajax: {
			url : url_get_banks,
			dataType: 'json',
			data: function (term) {
				return {
					query: term
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text:  item.name,
							id: item.id,
						}

					})

				};
			}
		}
	});

	$('.FinanzierungsangeboteEdit').on('click', function () {
		$('#btn-save').val("update");
		$('#title').html('Edit Finanzierungsanfragen');
		var id = $(this).attr('data-id');
		$('#bankFinanceId').val(id);
		$('.hide-custom').hide();
		var url = url_bank_financing_offer+'/'+id;
		$('.search').val('');

		$.ajax({
			type : 'get',
			url : url,
			success : function (data){
				$('#finanzierungsanfragen-form').attr('action', url);
				$('#telefonnotiz').val(data.telefonnotiz);
				$('#interest_rate').val(data.interest_rate);
				$('#tilgung').val(data.tilgung);
				$('#fk_share_percentage').val(data.fk_share_percentage);
				$('#fk_share_nominal').val(data.fk_share_nominal);
				$('.bank').append('<option selected value="'+data.bank.id+'">'+data.bank.name+'</option>');
			}
		});

		$('#FinanzierungsangeboteCreate').modal('show');
	});

	$(document).on("click", '.send_test_email', function(e) {
        if ($('#testEmail').val() == '') {
            toastr.error("Test Email field is required");
    	} else if($('#bank-sheet').val() == ''){
            toastr.error("Kalkulation wählen field is required");

        } else {
        	$(this).html('');
        	$(this).html('<i class="fa fa-spin fa-spinner"></i>');
       	 	bank = $(this).attr('data-bank');
        	$("#bank_id").val(722);
        	$("#isTestEmail").val(1);
        	$('#sendemailtobankers').submit();
    	}
    	e.preventDefault();
    });

    $('#sendemailtobankers').on('submit', function(e) {
        e.preventDefault();
        var selectedFrame = $("#bank-sheet").val();
        var propID = $("#bank-sheet  option:checked").attr('data-id');

        $("#sheet_prop_id").val(propID);
        var myFrame = window.frames["propertyIframeParent"].contentDocument.getElementById(selectedFrame);
        var propertyTableArray = myFrame.contentWindow.getTableData("table.pr-zoom-in-out-table");
        var tenantTableArray = myFrame.contentWindow.getTableData(".tenent-table");
        var note = myFrame.contentWindow.getTableData(".note-table");
        $('input#property-data-second').val(JSON.stringify(propertyTableArray));
        $('input#tenant-data-second').val(JSON.stringify(tenantTableArray));
        $('input#note-data-second').val(JSON.stringify(note));
        var formData = new FormData(this);
        $("#LoadingImage").show();
        $.ajax({
            url: url_sendemailtobankers,
            type: "post",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                console.log(response);
                if (response == 'success') {
                    $('#testEmailButton').html('Testmail senden');
                    $('.send_email_to_banks').html('E-Mail senden');

                    toastr.success("Anfrage wurde gesendet!");
                } else {
                    alert(response);
                    toastr.error("Please contact to administrator");
                }
            },
            error:function (response) {
                alert(response.responseJSON.message);

            }
        });
    });

    $('body').on('click', '.new-banken', function() {
        $('#nue-banken').modal();
    });

    $('.save-new-bank').on('click', function() {
        $.ajax({
            url: url_add_new_banken,
            type: "post",
            data: $('#add-new-banken').serialize(),
            success: function(response) {
                alert(response.message);
                if (response.status == 1) {
                    $('#nue-banken').modal('hide');
                    $('#add-new-banken')[0].reset();
                    bank_list();
                }
            }
        });
    });

    $('.all_green_btn').on('click', function(e) {
        checked_bank_ids = $(".bank_ids:checked").length;
        if (validate() == 0) {
            if (checked_bank_ids > 0) {
                if (confirm("Are you sure to send email to all bankers!")) {
                    $("#bank_id").val(null);
                    $(this).html('');
                    $(this).html('<i class="fa fa-spin fa-spinner"></i>');
                    $('#sendemailtobankers').submit();
                }
            } else {
                toastr.success("Bankers are not available here!");
            }
        }
        e.preventDefault();
    });
    
    $(document).on("click", '.show-result-list', function(event) {
        var email = $(this).data('email');
        $.ajax({
            url: url_showresult,
            type: "post",
            data: {
                email: email
            },
            success: function(response) {
                var response = JSON.parse(response);
                var tablehtml = '';
                response.forEach(function(item, index) {
                    tablehtml += "<tr><td>" + item.email_id + "</td><td>" + item.email_to_send + "</td><td><button class='btn btn-primary show-result' type='button' onclick='showResult(" + index + ")'><i class='fa fa-eye'></i></button></td></tr>";
                    resultData[index] = JSON.parse(item.user_data);
                });
                if (tablehtml) {
                    $('#result-list-table tbody').html(tablehtml);
                    $('#result-list').modal('show');
                } else {
                    alert("Data not available");
                }
            }
        });
    });

    $(document).on("click", '.send_email_to_banks', function(e) {
        if (validate() == 0) {
            $(this).html('');
            $(this).html('<i class="fa fa-spin fa-spinner"></i>');
            bank = $(this).attr('data-bank');
            email = $(this).attr('selected-mail-'+bank);
            $("#bank_id").val(bank);
            $("#bank-email").val(email);
            $('#sendemailtobankers').submit();
        }
        e.preventDefault();
    });

    $('body').on('click','#listing-banks_wrapper a,#listing-banks_wrapper .sorting,#listing-banks_wrapper .sorting_desc,#listing-banks_wrapper .sorting_asc',function(){
	    $('#listing-banks').find('a.inline-edit').editable({
	            success: function(response, newValue) {
	                if (response.success === false) return response.msg;
	                else {
	                    // bank_list();
	                }
	            }
	        });
	});

	var columns = [
	    null,
	    null,
	    null,
	    null,
	    null, {
	        "type": "numeric-comma"
	    }, {
	        "type": "numeric-comma"
	    }, {
	        "type": "numeric-comma"
	    }, {
	        "type": "numeric-comma"
	    },
	    null,
	    null, {
	        "width": "200px"
	    }
	];
	makeDatatable($('#listing-banks'), columns);
	$('#listing-banks').find('a.inline-edit').editable({
	    success: function(response, newValue) {
	        if (response.success === false) return response.msg;
	        else {
	        }
	    }
	});

});