$(document).ready(function(){

	$('#swot-template').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) return response.msg;
            else {
                var path = $('#path-properties-show').val();
                // window.location.href = path + '?tab=swot-template';
            }
        }
    });

    $('[id^=rate_box_]').on('change', function() {
        var summe = 0;
        var fruits = [];

        $('[id^=rate_box_]').each(function() {
            var select_box = $(this).attr("id");
            var choosen_value = $("#" + select_box).val();
            fruits.push(choosen_value);
            if (choosen_value !== 0) {
                summe = parseInt(summe) + parseInt(choosen_value);
            }
            $('#total_rate').val(summe);
        });

        id = $('#selected_property_id').val();

        $.ajax({
            type: 'POST',
            url: url_property_update_schl_1 + "/" + id,
            data: {
                pk: 'swot_json',
                value: JSON.stringify(fruits),
                _token: _token
            },
            success: function(data) {
                // generate_option_from_json(data, 'country_to_state');
            }
        });
        
    });

});