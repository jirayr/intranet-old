
function setBankMailField(email, bankId) {
    $('#bank-id'+bankId).attr('selected-mail-'+bankId,email);
}
$('#radiusSearch').on('click', function () {
    property_id = $('#p_id').val();
    radius = $('#radius').val();
    $.ajax({
        type : 'get',
        url : '/banks/radius-search',
        data : {p_id:property_id,rad:radius},
        success : function (data) {
            console.log(data.length);
            if(data.length !== 0){
                $('.table-row').remove();
                $.each(data, function(key) {
                    $(".table-body").append(
                        '<tr class="table-row">'+
                        '<td><div class="col-md-4 col-md-offset-4 no-padding">'+
                        '<input type="checkbox" name="bank_ids[]" value="'+data[key].id+'" class="form-control bank_ids"/>' +
                        '</div></td>'+
                        '<td>'+'<a href="javascrip:void(0)">'+data[key].name+'</a>'+'</td>'+
                        '<td>'+data[key].fullName+'</td>'+
                        '<td>'+
                        '<button class="btn btn-success show-result-list" type="button" data-email="'+data[key].contact_email+'">'+
                        '<i class="fa fa-eye"></i>'+
                        '</button>'+'</td>'+
                        '<td>'+data[key].contact_email+'</td>'+
                        '<td>'+'<a class="btn btn-primary btn-sm send_email_to_banks"  data-bank="'+data[key].id+'" data-property="'+property_id+'" >E-Mail senden</a>'+'</td>'+
                        '</tr>'
                    );
                });
            }else {
                alert('No Data Found');
            }

        }
    });
});

$('.email').on('click', function () {
    $('#noteText').val('');

    $('#attachment-field').css('visibility', 'hidden');
    $('#subject-field').css('visibility', 'hidden');
    $('#message-field').css('visibility', 'hidden');
    $("#subject").val('');
    $("#file").val('');

    $('#emailConfigModel').modal('show');
    var id = $(this).attr('data-id');
    $("#banken_id").val(id);
    var note = $(this).attr('data-note');
    console.log(note);
    $('#noteText').val(note);

});


$(document).ready(function () {
    $('#emailConfigModel').submit(function (e) {
        $('#config-loader').css('visibility', 'visible');

        e.preventDefault();
        $.ajax({
            type : 'post',
            url : '/savenote',
            data :   $('form.config-form').serialize(),
            success : function (data)
            {
                $('#config-loader').css('visibility', 'hidden');
                $('#save-config').removeClass('disabled');
                window.setTimeout(function(){
                    $('#emailConfigModel').modal('hide');
                    sweetAlert("Saved Successfully");
                }, 600);
                location.reload();
            }
        });

    });
});

$('.FinanzierungsangeboteCreate').on('click', function () {
    $('#btn-save').val("create");

    var id = $(this).attr('data-id');
    var prop = $('#bankFinanceId').val();
    // var url ='/properties/'+prop+'/project';
    // $('#finanzierungsanfragen-form').attr('action', url);
    $('#telefonnotiz').val('');
    $('#interest_rate').val('');
    $('#tilgung').val('');
    $('#fk_share_percentage').val('');
    $('#fk_share_nominal').val('');
    $('.bank').html('');
    $('#FinanzierungsangeboteCreate').modal('show');

});


$('#FinanzierungsangeboteCreate').submit(function (e) {

    var state = $('#btn-save').val();
    var url = '/bank-financing-offer';

    if (state == "update") {
        var id = $('#bankFinanceId').val();
        var url ='/bank-financing-offer/update'+'/'+id;
    }


    e.preventDefault();
    $.ajax({
        type : 'post',
        url : url,
        data :   $('form.finanzierungsanfragen-form').serialize(),
        success : function (data)
        {
            window.setTimeout(function(){
                $('#emailConfigModel').modal('hide');
                sweetAlert("Saved Successfully");
            }, 600);
            location.reload();
        }
    });

});


$('.delete-bank-finance-offer').on('click', function () {
    var id = $(this).attr('data-id');

    if(confirm('Are you sure to delete this record ?')) {
        $.ajax({
            url :  '/bank-financing-offer/delete'+'/'+id,
            success : function (data)
            {
                sweetAlert("Deleted Successfully");
                location.reload();
            }
        });

    }

});


$(document).ready(function() {

    $(".bank").select2({

        minimumInputLength: 2,

        language: {

            inputTooShort: function () {
                return "<?php echo 'Please enter 2 or more characters'; ?>"
            }
        },
        multiple: false,
        tags: true,

        ajax: {

            url : '/get-banks',
            dataType: 'json',
            data: function (term) {
                return {
                    query: term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text:  item.name,
                            id: item.id,
                        }

                    })

                };
            }

        }

    });

});


$('.FinanzierungsangeboteEdit').on('click', function () {
    $('#btn-save').val("update");
    $('#title').html('Edit Finanzierungsanfragen');
    var id = $(this).attr('data-id');
    $('#bankFinanceId').val(id);
    var url ='/bank-financing-offer/'+id;
    $('.search').val('');
    $.ajax({
        type : 'get',
        url : "/bank-financing-offer"+'/'+id,
        success : function (data)
        {
            $('#finanzierungsanfragen-form').attr('action', url);
            $('#telefonnotiz').val(data.telefonnotiz);
            $('#interest_rate').val(data.interest_rate);
            $('#tilgung').val(data.tilgung);
            $('#fk_share_percentage').val(data.fk_share_percentage);
            $('#fk_share_nominal').val(data.fk_share_nominal);
            $('.bank').append('<option selected value="'+data.bank.id+'">'+data.bank.name+'</option>');
        }
    });
    $('#FinanzierungsangeboteCreate').modal('show');
});

