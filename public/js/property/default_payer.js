$(document).ready(function(){

	var default_payer_table = $('#default_payer_table').dataTable({
        "ajax": url_get_default_payer,
        "order": [
            [7, 'desc']
        ],
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null, {
                "type": "new-date-time"
            },
            null,
            null,
        ]
    });

    $(document).ajaxComplete(function() {
        if ($('.default-payer-comment').length)
        	$('.default-payer-comment').editable();
    });

	$(document).on('click', ".default-payer-upload-file-icon", function() {
        $("#default_payer_gdrive_file_upload").trigger('click');
    });

    $(document).on('click', ".default-payer-upload-file-icon2", function() {
        $("#default_payer_gdrive_file_upload2").trigger('click');
    });

    $(document).on('change', "#default_payer_gdrive_file_upload", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span").text(fileName);
    });

    $(document).on('change', "#default_payer_gdrive_file_upload2", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span2").text(fileName);
    });

    $(document).on('click', ".link-button-gdrive-default-payer", function(e) {
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('default_payer');
        $("#select-gdrive-file-callback").val('gdeiveSelectDefaultPayer');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_name_payer").val());
    });

    window.gdeiveSelectDefaultPayer = function(basename, dirname, curElement, dirOrFile) {
        $("#default_payer_gdrive_file_basename").val(basename);
        $("#default_payer_gdrive_file_dirname").val(dirname);
        $("#default_payer_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#default_payer_gdrive_file_name").val(text);
        $("#default_payer_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    $('#add_default_payer_form').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var msgElement = $('#add_default_payer_msg');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_add_default_payer,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $this.find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);

            },
            success: function(result) {
                $this.find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status == true) {
                    showFlash($('#default_payer_msg'), result.message, 'success');
                    $this[0].reset();
                    $("#default_payer_gdrive_file_name_span").text("");
                    $('#default_payer_table').DataTable().ajax.reload();
                    setTimeout(function() {
                        $('#add_default_payer_modal').modal('hide');
                    }, 1000);
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                $this.find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({
                    error
                });
            }
        });
    });
    

    $('body').on('click', '.btn-delete-default-payer', function() {
        var url = $(this).attr('data-url');
        var msgElement = $('#default_payer_msg');
        if (confirm('Are you sure want to delete this default payer?')) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');
                        $('#default_payer_table').DataTable().ajax.reload();
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });

});