function ajax_verkauf_tab_load(){
	$.ajax({ 
    	url : url_ajax_verkauf_tab_load,
        context: document.body,
        success: function(response){
          	$('#ajax_tab_loader').html('');
          	var ajax_tab_loader_new = $('#ajax_tab_loader').html(response);   
          	ajax_tab_loader_new.find('a.inline-edit').editable({
            	success: function(response, newValue) {
              		//location.reload(true);
              		ajax_verkauf_tab_load();
              	}
            });
        }
	});
}

$(document).ready(function(){

	$('.checkbox-td').each(function(){
	  if($(this).find('select').length)
	  {
	    if($(this).find('select').attr('column-name')=="user2" || $(this).find('select').attr('column-name')=="user3")
	      $(this).remove();
	  }
	});
	
	step = d_status = "";
  	$('body').on('click','.ver-release-procedure-1',function(){
      	step = $(this).attr('data-id');
      	$('#ver-release-property-modal').modal();
  	});

  	$('body').on('click','.ver-release-procedure-request',function(){
    	property_id = $('.property_id').val();
    	comment = $('.ver-release-comment').val();
	    $.ajax({
	        type : 'POST',
	        url : url_verkaufreleaseprocedure,
	        data : {property_id:property_id,step:step,  _token : _token,comment:comment },
	        success : function (data) {
	            var path = $('#path-properties-show').val();
	            window.location.href = path + '?tab=verkauf_tab';
	        }
	    });
  	});
  	
  	$('body').on('click','.ver-release-procedure',function(){
    	id = $(this).attr('data-id');
    	property_id = $('.property_id').val();
    	status = $(this).attr('data-status');
    
	    if((status=="1" && !$(this).hasClass('btn-success')) || ($(this).hasClass('btn-success') && id=='btn1_request') || ($(this).hasClass('btn-success') && id=='btn2_request') || ($(this).hasClass('btn-success') && id=='btn3_request') || ($(this).hasClass('btn-success') && id=='btn4_request') ){
      		step = $(this).attr('data-id');
      		$('#ver-release-property-modal').modal();
    	}
 	});

  	$('.ver-irelease-the-property').click(function(){
    	property_id = $('.property_id').val();
    	comment = $('.f-comment').val();
    	$.ajax({
        	type : 'POST',
        	url : url_einkaufsendmail,
        	data : {property_id:property_id,release:1,  _token : _token, comment:comment },
        	success : function (data) {
            	var path = $('#path-properties-show').val();
            	window.location.href = path + '?tab=verkauf_tab';
        	}
    	});
  	});

  	$('.send-mail-confirm').click(function(){
    	$('#v-request-modal').modal();
  	});
  
	$('.v-request-confirm').click(function(){
	    property_id = $('.property_id').val();
	    $.ajax({
	        type : 'POST',
	        url : url_sendmail,
	        data : {property_id:property_id,step:'vbtn1_request',  _token : _token, comment:$('.v-request-comment').val() },
	        success : function (data) {
	            alert(data);
	            window.location.href = path + '?tab=verkauf_tab';
	        }
	    });
	  })

  	$('.release-the-property').click(function(){
    	property_id = $('.property_id').val();
    	$.ajax({
        	type : 'POST',
        	url : url_sendmail,
        	data : {property_id:property_id,release:1,step:'vbtn1',  _token : _token, comment:$('.v-ver-release-comment').val() },
        	success : function (data) {
            	// alert(data);
            	var path = $('#path-properties-show').val();
            	window.location.href = path + '?tab=verkauf_tab';
        	}
    	});
  	});

  	$.fn.editable.defaults.ajaxOptions = {type: "POST"};
  
    $('.preisverk').editable({
        success: function(response, newValue) {
        }
    });

    $('.sale-sheet').find('a.inline-edit').editable({
        success: function(response, newValue) {
        }
    });
    
    $('.date-c').editable({
        success: function(response, newValue) {
        }
    });


	$('.Verkauf_tab_editable').find('a.inline-edit').editable({

		success: function(response, newValue) {
	   		//location.reload(true);
	  		ajax_verkauf_tab_load();

	    }
	});
 
	$('.checkbox-verkauf').on('click', function () {
	    var type = $(this).attr('data-type');
	    v = 0;
	    if($(this).is(':checked'))
	        v = 1;

	    property_id = $('.property_id').val();

	    $.ajax({
	        type : 'POST',
	        url : url_verkauf_item_update+"/"+property_id+'/'+type,
	        data : {type:type,pk : 'status',value:v, property_id:property_id,  _token : _token },
	        success : function (data) {
	            generate_option_from_json(data, 'country_to_state');
	        }
	    });
	});

	$('.change-v-user').on('change', function () {
	    type = $(this).attr('data-type');
	    column = $(this).attr('column-name')
	    v = $(this).val();
	    property_id = $('.property_id').val();
	    $.ajax({
	        type : 'POST',
	        url : url_verkauf_item_update+"/"+property_id+'/'+type,
	        data : {type:type,pk : column,value:v, property_id:property_id,  _token : _token },
	        success : function (data) {
	            generate_option_from_json(data, 'country_to_state');
	        }
	    });
	});

	$('.change-l-user').on('change', function () {
	    column = 'saller_id';
	    v = $(this).val();
	    property_id = $('.property_id').val();

	    $.ajax({
	        type : 'POST',
	        url : url_verkauf_update+"/"+property_id,
	        data : {pk : column,value:v, property_id:property_id,  _token : _token },
	        success : function (data) {
	            generate_option_from_json(data, 'country_to_state');
	        }
	    });
	});

	$('#month_select').on('change', function (e) {
	    var optionSelected = $("option:selected", this);
	    var valueSelected = this.value;

	    $.ajax({
	        type : 'POST',
	        url : url_update_month_verkauf_tab,
	        data : {monat : valueSelected, _token : _token },
	        success : function (data) {
	             console.log(data);
	        }
	    });
	   
	});

	$('.show-link-einkauf').click(function(){
      var cl = $(this).attr('data-class');
      if($('.'+cl).hasClass('hidden')){
          $(this).find('i').removeClass('fa-angle-down');
          $(this).find('i').addClass('fa-angle-up');
          $('.'+cl).removeClass('hidden');

      }
      else{
          // $(this).html('<i class="fa fa-angle-down"></i>');
          $(this).find('i').addClass('fa-angle-down');
          $(this).find('i').removeClass('fa-angle-up');
          $('.'+cl).addClass('hidden');
          // $(this).next().addClass('hidden');
      }
	});

});