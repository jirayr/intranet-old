$(document).ready(function(){

	$('.pojectCreate').on('click', function () {
		$('#btn-save').val("create");

		var id = $(this).attr('data-id');
		var prop = $('#propId').val();
		var url ='/properties/'+prop+'/project';
		$('#projectForm').attr('action', url);
		$('#name').val('');
		$('#description').val('');
		$('#notes').val('');
		$('.search').html('');
		$('.user-search').html('');
		$('#title').html('Create Project');

		$('#projectCreate').modal('show');

	});

	$('.edit').on('click', function () {
		$('#btn-save').val("update");
		$('#title').html('Edit Project');
		var id = $(this).attr('data-id');
		$('#projectId').val(id);
		var prop = $('#propId').val();
		var url ='/properties/'+prop+'/project/update'+'/'+id;
		$('.search').val('');
		$.ajax({
			type : 'get',
			url : url_project_edit+'/'+id,
			success : function (data)
			{
				$('#projectForm').attr('action', url);
				$('#name').val(data.name);
				$('#description').val(data.description);
				$('#notes').val(data.note);
				// $('#datetimepicker1').datepicker('setDate', data.date);
				$.each(data.employees, function(key) {
					$('.search').append('<option selected value="'+data.employees[key].id+'">'+data.employees[key].vorname+data.employees[key].nachname+'</option>');
				});
				$.each(data.user_employees, function(key) {
					$('.user-search').append('<option selected value="'+data.user_employees[key].id+'">'+data.user_employees[key].name+'</option>');
				});


			}

		});
		$('#projectCreate').modal('show');

	});

	$('#projectForm').submit(function (e) {
		$('#config-loader').css('visibility', 'visible');
		var state = $('#btn-save').val();
		e.preventDefault();
		var prop = $('#propId').val();
		var url ='/properties/'+prop+'/project';

		if (state == "update") {
			var id = $('#projectId').val();
			var url ='/properties/'+prop+'/project/update'+'/'+id;
		}

		$.ajax({
			type : 'post',
			url : url,
			data :   $('form.projectform').serialize(),
			success : function (data)
			{
				$('#config-loader').css('visibility', 'hidden');
				$('#save-config').removeClass('disabled');
				window.setTimeout(function(){
					$('#projectCreate').modal('hide');
					sweetAlert("Saved Successfully");
				}, 600);
				location.reload();
			}
		});

	});

	$('.delete').on('click', function () {
		var id = $(this).attr('data-id');
		swal({
			title: 'Delete this Account?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then(function (isConfirm) {
				if (isConfirm.dismiss !== 'cancel' && isConfirm){
					$.ajax({
						url :  url_project_delete+'/'+id,
						success : function (data)
						{
							sweetAlert("Deleted Successfully");
							location.reload();
						}
					});
				}

		})

	});

	$(".user-search").select2({
		minimumInputLength: 2,
		language: {
			inputTooShort: function () {
				return "Please enter 2 or more characters"
			}
		},
		multiple: true,
		tags: true,
		ajax: {

			url: url_autocompleteSearchUserByName,
			dataType: 'json',
			data: function (term) {
				return {
					query: term
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text:  item.name,
							id: item.id,
						}

					})

				};
			}

		}

	});

	$(".search").select2({
		minimumInputLength: 2,
		language: {
			inputTooShort: function () {
				return "Please enter 2 or more characters"
			}
		},
		multiple: true,
		tags: true,
		ajax: {
			url: url_autocompleteByName,
			dataType: 'json',
			data: function (term) {
				return {
					query: term
				};
			},
			processResults: function (data) {
				return {
					results: $.map(data, function (item) {
						return {
							text:  item.vorname+''+item.nachname+'-'+( item.company !== null ? '('+item.company.name +')':'else'),
							id: item.id,
							slug: item.keyword

						}

					})

				};
			}
		}

	});

	$('#datetimepicker1').datepicker();

	var columns = [
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
	];
	makeDatatable($('#project_table'), columns);
});