$(document).ready(function(){

	// 1) open invoice
    var add_property_invoice_table = $('#add_property_invoice_table').dataTable({
        "ajax": url_get_property_invoice,
        "order": [
            [9, 'desc']
        ],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            {"type": "numeric-comma"},
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            // { "type": "new-date" },
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 2) Pending invoice
    var add_property_invoice_table4 = $('#add_property_invoice_table4').dataTable({
        "ajax": url_get_pending_invoice,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
            null,
        ]
    });

    // 3) Released invoice
    var add_property_invoice_table2 = $('#add_property_invoice_table2').dataTable({
        "ajax": url_get_release_property_invoice,
        "order": [
            [9, 'desc']
        ],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            {"type": "numeric-comma"},
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            null,
            null,
        ]
    });

    // 4) Not release invoice (AM)
    var table_rejected_invoice = $('#table_rejected_invoice').dataTable({
        "ajax": url_get_not_released_invoice_am,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
        ]
    });

    // 5) Not release invoice (Falk)
    var add_property_invoice_table3 = $('#add_property_invoice_table3').dataTable({
        "ajax": url_get_not_released_invoice,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
        ]
    });

    // 6) invoice mail log
    var invoice_mail_table = $('#invoice_mail_table').dataTable({
        "ajax": url_get_invoice_logs,
        "order": [
            [5, "desc"]
        ],
        "columns": [
            null,
            null,
            null,
            { "type": "new-date" },
            null,
            {"type": "new-date-time"},
        ]
    });

    $(document).ajaxComplete(function() {
        if ($('.invoice-comment').length)
        	$('.invoice-comment').editable();
    });

    $(document).on('click', ".property_invoice-upload-file-icon", function() {
        $("#property_invoice_gdrive_file_upload").trigger('click');
    });

    $(document).on('change', "#property_invoice_gdrive_file_upload", function(e) {
        var fileName = e.target.files[0].name;
        $("#property_invoice_gdrive_file_name_span").text(fileName);
    });

    $(document).on('click', ".link-button-gdrive-invoice", function(e) {
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('property_invoice');
        $("#select-gdrive-file-callback").val('gdeiveSelectPropertyInvoice');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_nam_invoice").val());
    });

    window.gdeiveSelectPropertyInvoice = function(basename, dirname, curElement, dirOrFile) {
        $("#property_invoice_gdrive_file_basename").val(basename);
        $("#property_invoice_gdrive_file_dirname").val(dirname);
        $("#property_invoice_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#property_invoice_gdrive_file_name").val(text);
        $("#property_invoice_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    $('#add_property_invoice_form').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var msgElement = $('#add_property_invoice_msg');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_add_property_invoice,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                // $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                // $($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status == true) {
                    showFlash(msgElement, result.message, 'success');

                    $($this)[0].reset();
                    $('#property_invoice_gdrive_file_basename').val('');
                    $('#property_invoice_gdrive_file_dirname').val('');
                    $('#property_invoice_gdrive_file_type').val('');
                    $('#property_invoice_gdrive_file_name').val('');
                    $('#property_invoice_gdrive_file_name_span').text('');
                    $('#property_invoice_gdrive_file_upload').val('');

                    $('#add_property_invoice_table').DataTable().ajax.reload();
                    setTimeout(function() {
                        $('#add_property_invoice_modal').modal('hide');
                    }, 1000);
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                // $($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var invoice_reject_id;
    $('body').on('click','.btn-reject',function(){
        invoice_reject_id = $(this).attr('data-id');
        $('#invoice-reject-modal').modal();
        $('.invoice-reject-comment').val("");
    });

    $('body').on('click','.invoice-reject-submit',function(){
        comment = $('.invoice-reject-comment').val();
        $.ajax({
            type : 'POST',
            url : url_property_invoicemarkreject,
            data : {
                id:invoice_reject_id,
                _token : _token,
                comment:comment,
                property_id:$('#selected_property_id').val()
            },
            success : function (data) {
                  $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#table_rejected_invoice').DataTable().ajax.reload();
                  $('#invoice_mail_table').DataTable().ajax.reload();
            }
        });
    });

    $('body').on('click', '.invoice-release-request', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        $('.invoice-release-comment').val("")
        $('#invoice-release-property-modal').modal();

        if(vacant_release_type=="release2"){
            $('.i-message').addClass('hidden');
            $('.i-message2').removeClass('hidden');
        }else{
            $('.i-message').removeClass('hidden');
            $('.i-message2').addClass('hidden');
        }

    });

    $('body').on('click', '.invoice-release-submit', function() {
        comment = $('.invoice-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_invoicereleaseprocedure,
            data: {
                id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                property_id: $('#selected_property_id').val()
            },
            success: function(data) {
                // getprovisionbutton();
                $('#add_property_invoice_table').DataTable().ajax.reload();
                $('#add_property_invoice_table2').DataTable().ajax.reload();
                $('#invoice_mail_table').DataTable().ajax.reload();
            }
        });
    });

    var not_release_id = 0;
    $('body').on('click','.mark-as-not-release',function(){
        not_release_id = $(this).attr('data-id');
        $('#not-release-modal').modal();
    });

    $('body').on('click','.invoice-not-release-submit',function(){
        comment = $('.invoice-not-release-comment').val();
        $.ajax({
              type : 'POST',
              url : url_property_invoicenotrelease,
              data : {id:not_release_id, _token : _token,comment:comment,property_id:$('#selected_property_id').val()},
              success : function (data) {
                  // getprovisionbutton();
                  $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#add_property_invoice_table3').DataTable().ajax.reload();
                  $('#invoice_mail_table').DataTable().ajax.reload();
              }
          });
    });

    $('body').on('click','.mark-as-pending',function(){
        not_release_id = $(this).attr('data-id');
        $('#pending-modal').modal();
        $('.invoice-pending-comment').val("")
    });

    $('body').on('click','.invoice-pending-submit',function(){
        comment = $('.invoice-pending-comment').val();
        $.ajax({
              type : 'POST',
              url : url_property_invoicemarkpending,
              data : {id:not_release_id, _token : _token, comment:comment,property_id:$('#selected_property_id').val()},
              success : function (data) {
                  // getprovisionbutton();
                  $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#add_property_invoice_table4').DataTable().ajax.reload();
                  $('#invoice_mail_table').DataTable().ajax.reload();
              }
          });
    });

    $('body').on('click', '.btn-delete-property-invoice', function() {

        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var msgElement = $('#property_invoice_msg');

        if (confirm('Are you sure want to delete this invoice?')) {
            var url = url_delete_property_invoice;
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');
                        if(type == 'open-invoice'){
                            $('#add_property_invoice_table').DataTable().ajax.reload();
                        }
                        else if(type == 'not-release-invoice'){
                            $('#add_property_invoice_table3').DataTable().ajax.reload();
                        }
                        else if(type == 'not-release-invoice-am'){
                            $('#table_rejected_invoice').DataTable().ajax.reload();
                        }
                        else{
                            $('#add_property_invoice_table4').DataTable().ajax.reload();
                        }
                        $('#invoice_mail_table').DataTable().ajax.reload();
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });

    $('body').on('click', '.multiple-invoice-release-request', function() {
		var selcted_id_invoice = "";
		arr = [];
		selcted_id_invoice = "";

		$('.invoice-checkbox').each(function(){
			if($(this).is(':checked'))
				arr.push($(this).attr('data-id'));
		})
		selcted_id_invoice = arr.toString();

		// console.log(selcted_id_invoice);
		if(selcted_id_invoice=="" || selcted_id_invoice==null){
			alert("Bitte mindestens eine Rechnung auswählen")
		}
		else{
	    	$('.multiple-invoice-release-comment').val("")
	    	$('#multiple-invoice-release-property-modal').modal();
		}

	});

	$('body').on('click', '.multiple-invoice-release-submit', function() {
	    comment = $('.multiple-invoice-release-comment').val();
	    selcted_id_invoice = "";

		$('.invoice-checkbox').each(function(){
			if($(this).is(':checked'))
				arr.push($(this).attr('data-id'));
		})
		selcted_id_invoice = arr.toString();


	    $('.invoice-checkbox').each(function(){
	        if($(this).is(':checked'))
	            $(this).closest('tr').remove();
	    })

	    $.ajax({
	        type: 'POST',
	        url: url_property_invoicereleaseprocedure,
	        data: {
	            id: selcted_id_invoice,
	            step: "release2",
	            _token: _token,
	            comment: comment,
	            property_id: $('#selected_property_id').val()
	        },
	        success: function(data) {
	            // getprovisionbutton();
	            $('#add_property_invoice_table').DataTable().ajax.reload();
	            $('#add_property_invoice_table2').DataTable().ajax.reload();
	            $('#add_property_invoice_table4').DataTable().ajax.reload();
	            $('#invoice_mail_table').DataTable().ajax.reload();
	        }
	    });
	});

	$('body').on('click', '.mark-as-paid', function() {
        var id = $(this).attr('data-id');
        $this = $(this);
        var url = url_makeAsPaid + "?id=" + id;
        $.ajax({
            url: url,
        }).done(function(data) {
            $this.removeClass('btn-primary');
            $this.removeClass('mark-as-paid');
            $this.addClass('btn-success');
            $this.html("Erledigt");
        });
    });

    $('body').on("click", "#btn-invoice-filter", function() {
        var field = $('#invoice-filter-field').val();
        var start_date = $('#invoice-filter-start-date').val();
        var end_date = $('#invoice-filter-end-date').val();

        var url = url_get_release_property_invoice+'?field='+field+'&start_date='+start_date+'&end_date='+end_date;

        $('#add_property_invoice_table2').DataTable().ajax.url(url).load();
    });

    $('body').on('change', '.row_checkall', function() {
        var table_obj;
        var table = $(this).closest('.main_div').find('table').attr('id');

        if(table == 'add_property_invoice_table'){
            table_obj = add_property_invoice_table;
        }else if(table == 'add_property_invoice_table4'){
            table_obj = add_property_invoice_table4;
        }else if(table == 'add_property_invoice_table2'){
            table_obj = add_property_invoice_table2;
        }else if(table == 'table_rejected_invoice'){
            table_obj = table_rejected_invoice;
        }else if(table == 'add_property_invoice_table3'){
            table_obj = add_property_invoice_table3;
        }
        
        if($(this).is(':checked')){
            table_obj.$(".row_checkbox").prop('checked', true).trigger('change');
        }else{
            table_obj.$(".row_checkbox").prop('checked', false).trigger('change');
        }
    });

    $('body').on('click', '.download_invoice', function() {

        var table_id = $(this).closest('.main_div').find('table').attr('id');
        var url = $(this).attr('data-url');
        var checked_arr = [];
        var table_obj;

        if(table_id == 'add_property_invoice_table'){
            table_obj = add_property_invoice_table;
        }else if(table_id == 'add_property_invoice_table4'){
            table_obj = add_property_invoice_table4;
        }else if(table_id == 'add_property_invoice_table2'){
            table_obj = add_property_invoice_table2;
        }else if(table_id == 'table_rejected_invoice'){
            table_obj = table_rejected_invoice;
        }else if(table_id == 'add_property_invoice_table3'){
            table_obj = add_property_invoice_table3;
        }

        table_obj.$('.row_checkbox').each(function(){
            if($(this).prop('checked')){
                checked_arr.push($(this).val());
            }
        });

        if(checked_arr.length > 0){
            var ids = checked_arr.join(',');
            var url_load = url+'?id='+ids+'&tab=invoice';
            location.href = url_load;
            return false;
        }else{
            alert('Select at least one row!');
        }
    });

});