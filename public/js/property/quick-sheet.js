$(document).ready(function(){
	
    $(".city-search").select2({
        minimumInputLength: 2,
        language: {
            inputTooShort: function() {
                return "Please enter 2 or more characters";
            }
        },
        tags: false,
        ajax: {
            url: url_searchcity,
            dataType: 'json',
            data: function(term) {
                return {
                    query: term
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.code,
                            slug: item.slug,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $('.save-city').on('click', function() {
        $.ajax({
            url: url_add_new_city,
            type: "post",
            data: $('#add-ort').serialize(),
            success: function(response) {
                alert(response.message);
                if (response.status == 1) {
                    $('#add-city').modal('hide');
                    $('#add-ort')[0].reset();
                }
            }
        });
    });

    $('.add-new-p-comment').click(function() {
    	$('#add_new_property_comment').modal('show');
        $('.p-comment').val("");
        $('.propertyappenddata').html("");
        getsheetcomment(1);
    });

    $('body').on('click', '.show-all-p', function() {
        id = $('#selected_property_id').val();
        getsheetcomment(-1);
    });

    $('body').on('click', '.delete-sheet-cm', function() {
        if (confirm('Are you sure?')) {
            $this = $(this);
            $.ajax({
                type: 'GET',
                url: $(this).attr('href'),
                success: function(data) {
                    $this.closest('.sec').remove();
                }
            });
        }
        return false;
    });

    $('body').on('click', '.show-less-p', function() {
        getsheetcomment(1);
    });

    $('body').on('click', '.save-p-comment', function() {
        $.ajax({
            type: 'POST',
            url: url_properties_comment_save,
            data: $('#my-comment-form').serialize(),
            success: function(data) {
                getsheetcomment(1);
                $('.p-comment').val("");
            }
        });
        return false;
    });

    function getsheetcomment(show) {
	    $('.propertyappenddata').html("");
	    if (show == 1) lab = 'show all';
	    else lab = 'show less';
	    id = $('#selected_property_id').val();
	    $.ajax({
	        type: 'GET',
	        url: url_properties_comment_get_comments,
	        data: {
	            id: id,
	            show: show
	        },
	        success: function(data) {
	            i = 0
	            $.each(data, function(key) {
	                if (data[key].comment != "") {
	                    if (i == 0) {
	                        // $('.sheet-comment').html(data[key].comment);
	                    }
	                    i = 1;
	                    $('.propertyappenddata').append('<tr class="sec"><td>' + data[key].name + '</td>' + '<td>' + data[key].comment + '</td>' + '<td>' + data[key].f_date + '</td><td class="pull-right"><a href="' + '/properties-comment/delete-sheet-comment' + '/' + data[key].id + '" class="btn-xs btn-danger delete-sheet-cm" data-type="" data-pk="" data-url="" data-title="">Löschen</a></td>' + '</td></tr>');
	                }
	            });
	            if (i == 1) {
	                if (show == 1) $('.propertyappenddata').append("<button style='margin:10px;' type='button' class='btn-sm btn show-all-p'>" + lab + "</button>");
	                else $('.propertyappenddata').append("<button style='margin:10px;' type='button' class='btn-sm btn show-less-p'>" + lab + "</button>");
	            }
	        }
	    });
	}
	getsheetcomment(1);
});