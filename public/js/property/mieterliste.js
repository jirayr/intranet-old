var elem = document.documentElement;

function openFullscreen() {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
}

function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
}

function getRentPaidData(){
    $.ajax({
       url: url_get_rent_paid_data,
    }).done(function (data) {
        $('#rent-paid-data').html(data);
        var columns = [
            null,
            null,
            null,
            null,
            { "type": "numeric-comma"},
            { "type": "numeric-comma"},
            null,
            { "type": "numeric-comma"},
            null,
            null,
            null,
            null,
            null,
            null,
        ];
        makeDatatable($('#rent-paid-table'), columns);
        /*var columns = [
            null,
            null,
            {"type": "new-date-time"}
        ];
        makeDatatable($('#rent-paid-upload-table'), columns, 'desc', 2);*/

    });
}

function checktab(href){
    if(href == '#rent_paid'){
        $('.mieterliste_tab').find('.btn-group').hide();
        $('.mieterliste_tab').find('.btn-fullscreen').hide();
        $('.mieterliste_tab').find('.wmd-view-topscroll').hide();
        $('.mieterliste_tab').find('.scroll-div1').hide();
    }else{
        $('.mieterliste_tab').find('.btn-group').show();
        $('.mieterliste_tab').find('.btn-fullscreen').show();
        $('.mieterliste_tab').find('.wmd-view-topscroll').show();
        $('.mieterliste_tab').find('.scroll-div1').show();
    }
}

getRentPaidData();

$(document).ready(function(){

	$('#tenancy-schedule').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) return response.msg;
            else {
                var path = $('#path-properties-show').val();
                var selectingTenancySchedule = response.selecting_tenancy_schedule;
                // window.location.href = path + "?tab=tenancy-schedule&selecting_tenancy_schedule=" + selectingTenancySchedule;
            }
        }
    });

    $('.inline-edit-use').editable({
        prepend: "not selected",
        source: [{
            value: 'Einzelhandel',
            text: 'Einzelhandel'
        }, {
            value: 'Büro/Praxen',
            text: 'Büro/Praxen'
        }, {
            value: 'Lager',
            text: 'Lager'
        }, {
            value: 'Stellplätze',
            text: 'Stellplätze'
        }, {
            value: 'Sonstiges',
            text: 'Sonstiges'
        }, {
            value: 'Wohnungen',
            text: 'Wohnungen'
        }, {
            value: 'Gastronomie',
            text: 'Gastronomie'
        }],
    });

    $(".year").hide();
    $('.btn-year').on('click', function () {
      $(".year").toggle();
    });

    $('.commentAdd').on('click', function () {
        $(".appenddata").empty();
        var id = $(this).attr('data-id');
        $('#item').val(id);

        $.ajax({
            type : 'GET',
            url : url_get_comments,
            data : { id : id,  _token : _token },
            success : function (data) {
                $.each(data, function(key) {
                    if(data[key].comment != "") {
                        $('.appenddata').append(
                            '<div><b style="font-weight:700">' + data[key].user_name + '</b></div>' +
                            '<div>' + data[key].comment +
                            '<a href="' + '/delete-comment' + '/' + data[key].id + '/item/' + data[key].item_id + '" class="btn-sm btn-danger" style="float:right" data-type="" data-pk="" data-url="" data-title="">Löschen</a>'+
                            '<div style="font-size: 11px"><b style="font-weight:700">'+'Erstellt am : '+'</b>'+data[key].created_at+'</div>'+
                            '</div><hr>'
                        );
                    }
                });
            }
        });

        $.ajax({
            type : 'GET',
            url : url_items_find,
            data : { id : id,  _token : _token },
            success : function (data) {
                if(data.comment != null) {

                    $('.appenddata').append(
                    	'<div>' + data.comment +'</div><hr>'
                    );
                }
            }
        });
    });

    $('.commentTwo').on('click', function () {
        $(".appendcomment2").empty();
        var id = $(this).attr('data-id');
        $('#itemId').val(id);

        $.ajax({
            type : 'GET',
            url : url_get_comments,
            data : { id : id,  _token : _token },
            success : function (data) {

                $.each(data, function(key) {
                   if(data[key].external_comment != ""){
                       $('.appendcomment2').append(
                           '<div><b style="font-weight:700">'+data[key].user_name+'</b></div>'+
                           '<div>'+data[key].external_comment+
                           '<a href="'+'/delete-comment'+'/'+ data[key].id+'/item/'+data[key].item_id+'" class="btn-sm btn-danger" style="float:right" data-type="" data-pk="" data-url="" data-title="">Löschen</a>' +
                           '<div style="font-size: 11px"><b style="font-weight:700">'+'Erstellt am : '+'</b>'+data[key].created_at+'</div>'+
                           '</div><hr>'
                           );
                   }

               });

            }
        });

        $.ajax({
            type : 'GET',
            url : url_items_find,
            data : { id : id,  _token : _token },
            success : function (data) {
                if(data.comment2 != null) {
                    $('.appendcomment2').append(
                        '<div>' + data.comment2 +'</div><hr>'
                    );
                }
            }
        });
    });

    window.onload = function () {
        $('#scroll2').scroll( function () {
           $('.test').css("top", $('#scroll2').scrollTop());
       });
    };

    var lastScrollLeft = 0;
    $('tbody').scroll(function() {
        var documentScrollLeft = $('tbody').scrollLeft();
        if (lastScrollLeft != documentScrollLeft) {
            $(".seprate").addClass("active");
        }
        if(documentScrollLeft == 0){
            $(".seprate").removeClass("active");
        }
    });

    $('tbody').scroll(function(e) { //detect a scroll event on the tbody
        /*
      	Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
      	of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.
      	*/
        $('thead').css("left", -$("tbody").scrollLeft()); //fix the thead relative to the body scrolling
        $('thead th:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first cell of the header
        $('tbody th:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first column of tdbody
    });

    $('.checkbox-is-new').on('click', function () {
        var id = $(this).attr('data-id');
        v = 0;
        if($(this).is(':checked'))
            v = 1;

        $.ajax({
            type : 'POST',
            url : url_item_update+"/"+id,
            data : { pk : 'is_new', value:v,  _token : _token },
            success : function (data) {
            	// generate_option_from_json(data, 'country_to_state');
            }
        });

    });

    $('.checkbox-is-vacant').on('click', function () {
        var id = $(this).attr('data-id');
        v = 0;
        if($(this).is(':checked'))
            v = 1;

        $.ajax({
            type : 'POST',
            url : url_item_update+"/"+id,
            data : { pk : 'vacancy_on_purcahse', value:v,  _token : _token },
            success : function (data) {
                // generate_option_from_json(data, 'country_to_state');
            }
        });
    });

    $('.change-asset-user').on('change', function () {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-pk');
        v = $(this).val();
        $.ajax({
            type : 'POST',
            url : url_item_update+"/"+id,
            data : { pk : pk, value:v,  _token : _token },
            success : function (data) {
                    // generate_option_from_json(data, 'country_to_state');
                }
            });
    });

    $('.option-check').on('click', function () {
        var id = $(this).attr('data-id');
        v = -1;
        if($(this).is(':checked'))
            v = $(this).val();

        $.ajax({
            type : 'POST',
            url : url_item_update+"/"+id,
            data : { pk : 'selected_option', value:v,  _token : _token },
            success : function (data) {
                // generate_option_from_json(data, 'country_to_state');
            }
        });

    });

    $('.warning-price-check').on('click', function () {
        var id = $(this).attr('data-id');
        v = 'NULL';
        if($(this).is(':checked'))
            v = $(this).val();

        var pk = $(this).attr('data-pk');

        $.ajax({
            type : 'POST',
            url : url_item_update+"/"+id,
            data : { pk : pk, value:v,  _token : _token },
            success : function (data) {
                // generate_option_from_json(data, 'country_to_state');
            }
        });

    });

    $('.btn-export-tenancy-to-pdf').on('click', function () {
        $('#export-tenancy-to-pdf-form').submit();
    });

    $('.btn-export-tenancy-to-excel').on('click', function () {
        var tenancyScheduleID = $(this).data('tenancy-id');
        $('input#tenancy-id').val(tenancyScheduleID);
        var selector = 'div#tenancy-schedule-' + tenancyScheduleID + ' table';
        var tenancyTableArray = getTableData(selector);

        $('input#tenancy-data').val(JSON.stringify(tenancyTableArray));
         // console.log(JSON.stringify(tenancyTableArray));
        tenancyTableArray = getTableClass(selector);

        $('input#tenancy-class').val(JSON.stringify(tenancyTableArray));

        tenancyTableArray = getTableTdClass(selector);
        $('input#tenancy-td').val(JSON.stringify(tenancyTableArray));

        $('#export-tenancy-to-excel-form').submit();
    });


    var tenancySchedulesTable = $('.tenancy-schedules table');
    if (isNaN($.cookie('tenancy-schedule-zoom-value'))){
        $.removeCookie('tenancy-schedule-zoom-value');
    }

    var zoomValue = 1;
    if ($.cookie('tenancy-schedule-zoom-value')){
        zoomValue = parseFloat(parseFloat($.cookie('tenancy-schedule-zoom-value')).toFixed(1));
    }
    else {
        $.cookie('tenancy-schedule-zoom-value', 1);
    }

    $('span#tenancy-schedule-zoom-value').text((zoomValue * 100).toFixed(0));
    tenancySchedulesTable.css({
        'zoom'              : zoomValue,
        '-webkit-transform' : 'scale(' + zoomValue + ')',
        '-moz-transform'    : 'scale(' + zoomValue + ')',
        '-ms-transform'     : 'scale(' + zoomValue + ')',
        '-o-transform'      : 'scale(' + zoomValue + ')',
        'transform'         : 'scale(' + zoomValue + ')',
        'transform-origin'	: 'top left'
    });

    $('#tenacy-schedule-zoom-in').on('click', function () {
        zoomValue = parseFloat(parseFloat($.cookie('tenancy-schedule-zoom-value')).toFixed(1));
        if (zoomValue < 2.0) {
            zoomValue += 0.1;
            $.cookie('tenancy-schedule-zoom-value', zoomValue);
        }
        $('span#tenancy-schedule-zoom-value').text((zoomValue * 100).toFixed(0));
        tenancySchedulesTable.css({
            'zoom'              : zoomValue,
            '-webkit-transform' : 'scale(' + zoomValue + ')',
            '-moz-transform'    : 'scale(' + zoomValue + ')',
            '-ms-transform'     : 'scale(' + zoomValue + ')',
            '-o-transform'      : 'scale(' + zoomValue + ')',
            'transform'         : 'scale(' + zoomValue + ')',
            'transform-origin'	: 'top left'
        });
    });

    $('#tenacy-schedule-zoom-out').on('click', function () {
        zoomValue = parseFloat(parseFloat($.cookie('tenancy-schedule-zoom-value')).toFixed(1));
        if (zoomValue >= 0.6) {
            zoomValue -= 0.1;
            $.cookie('tenancy-schedule-zoom-value', zoomValue);
        }
        $('span#tenancy-schedule-zoom-value').text((zoomValue * 100).toFixed(0));
        tenancySchedulesTable.css({
            'zoom'              : zoomValue,
            '-webkit-transform' : 'scale(' + zoomValue + ')',
            '-moz-transform'    : 'scale(' + zoomValue + ')',
            '-ms-transform'     : 'scale(' + zoomValue + ')',
            '-o-transform'      : 'scale(' + zoomValue + ')',
            'transform'         : 'scale(' + zoomValue + ')',
            'transform-origin'	: 'top left'
        });
    });

    $('body').on('click', '.new-date', function() {
        $('.date-item-id').val($(this).attr('data-id'));
    });

    $(".new_date,.new_start_date").keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            id = $('.date-item-id').val();
            new_date = $('.new_date').val();
            new_start_date = $('.new_start_date').val();
            $.ajax({
                type: 'POST',
                url: url_change_date,
                data: {
                    id: id,
                    _token: _token,
                    new_date: new_date,
                    new_start_date: new_start_date
                },
                success: function(data) {
                    if (typeof(data.success) != "undefined" && data.success === false) {
                        alert(data.msg);
                    } else {
                        var path = $('#path-properties-show').val();
                        window.location.href = path + '?tab=mieterliste';
                    }
                }
            });
        }
    });

    $('body').on('click', '.save-date', function() {
        id = $('.date-item-id').val();
        new_date = $('.new_date').val();
        new_start_date = $('.new_start_date').val();

        $.ajax({
            type: 'POST',
            url: url_change_date,
            data: {
                id: id,
                _token: _token,
                new_date: new_date,
                new_start_date: new_start_date
            },
            success: function(data) {
                if (typeof(data.success) != "undefined" && data.success === false) {
                    alert(data.msg);
                } else {
                    var path = $('#path-properties-show').val();
                    window.location.href = path + '?tab=mieterliste';
                }
            }
        });
    });

    $(document).on('click', ".default-payer-upload-file-icon", function() {
        $("#default_payer_gdrive_file_upload").trigger('click');
    });
    $(document).on('click', ".default-payer-upload-file-icon2", function() {
        $("#default_payer_gdrive_file_upload2").trigger('click');
    });
    $(document).on('change', "#default_payer_gdrive_file_upload", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span").text(fileName);
    });
    $(document).on('change', "#default_payer_gdrive_file_upload2", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span2").text(fileName);
    });
    $(document).on('click', ".link-button-gdrive-default-payer", function(e) {
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('default_payer');
        $("#select-gdrive-file-callback").val('gdeiveSelectDefaultPayer');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_name_payer").val());
    });
    window.gdeiveSelectDefaultPayer = function(basename, dirname, curElement, dirOrFile) {
        $("#default_payer_gdrive_file_basename").val(basename);
        $("#default_payer_gdrive_file_dirname").val(dirname);
        $("#default_payer_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#default_payer_gdrive_file_name").val(text);
        $("#default_payer_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    $(document).on('click', ".rent-paid-month-wise-detail", function(e){
        var url = $(this).attr('data-url');
        var title = $(this).text();
        $('#rent-paid-month-wise-detail-modal').find('.modal-title').text(title);
        $('#rent-paid-month-wise-detail-modal').modal('show');
        $.ajax({
           url: url,
        }).done(function (data) {
           $('#rent-paid-month-wise-detail-div').html(data);
           var columns = [
                null,
                null,
                null,
                { "type": "numeric-comma"}
            ];
            makeDatatable($('#rent-paid-monthwise-table'), columns);
        });
    });

    $(".mieterliste_tab li a").each(function(){
        var is_active = $(this).closest('li').hasClass('active');
        if(is_active){
            var href = $(this).attr('href');
            checktab(href);
        }
    });

    $('body').on("click", ".mieterliste_tab li a", function() {
        var href = $(this).attr('href');
        checktab(href);
    });

    $('#year-select').change(function(){
        var month = $(this).val();
        $.ajax({
            url: url_get_rent_paid_data+'?month='+month,
            data:{month:month}
        }).done(function (data) {
            $('#rent-paid-data').html(data);
            var columns = [
                null,
                null,
                null,
                { "type": "numeric-comma"},
                { "type": "numeric-comma"},
                null,
                { "type": "numeric-comma"},
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ];
            makeDatatable($('#rent-paid-table'), columns);
        });
    });


    /*------------------------Default payer--------------------------*/

    var default_payer_table = $('#default_payer_table').dataTable({
        "ajax": url_get_default_payer,
        "order": [
            [7, 'desc']
        ],
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null, {
                "type": "new-date-time"
            },
            null,
            null,
        ]
    });

    $(document).ajaxComplete(function() {
        if ($('.default-payer-comment').length)
            $('.default-payer-comment').editable();
    });

    $('body').on('click', '.btn-delete-default-payer', function() {
        var url = $(this).attr('data-url');
        var msgElement = $('#default_payer_msg');
        if (confirm('Are you sure want to delete this default payer?')) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');
                        $('#default_payer_table').DataTable().ajax.reload();
                        $('#year-select').trigger('change');
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', ".default-payer-upload-file-icon", function() {
        $("#default_payer_gdrive_file_upload").trigger('click');
    });

    $(document).on('click', ".default-payer-upload-file-icon2", function() {
        $("#default_payer_gdrive_file_upload2").trigger('click');
    });

    $(document).on('change', "#default_payer_gdrive_file_upload", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span").text(fileName);
    });

    $(document).on('change', "#default_payer_gdrive_file_upload2", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span2").text(fileName);
    });

    $(document).on('click', ".link-button-gdrive-default-payer", function(e) {
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('default_payer');
        $("#select-gdrive-file-callback").val('gdeiveSelectDefaultPayer');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_name_payer").val());
    });

    window.gdeiveSelectDefaultPayer = function(basename, dirname, curElement, dirOrFile) {
        $("#default_payer_gdrive_file_basename").val(basename);
        $("#default_payer_gdrive_file_dirname").val(dirname);
        $("#default_payer_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#default_payer_gdrive_file_name").val(text);
        $("#default_payer_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    $('#add_default_payer_form').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var msgElement = $('#add_default_payer_msg');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_add_default_payer,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $this.find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);

            },
            success: function(result) {
                $this.find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status == true) {
                    showFlash($('#default_payer_msg'), result.message, 'success');
                    $this[0].reset();
                    $("#default_payer_gdrive_file_name_span").text("");
                    $('#default_payer_table').DataTable().ajax.reload();
                    setTimeout(function() {
                        $('#add_default_payer_modal').modal('hide');
                        $('#year-select').trigger('change');
                    }, 1000);
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                $this.find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({
                    error
                });
            }
        });
    });

    /*------------------------Default payer--------------------------*/

});