function preview_image2() {
    var formData = new FormData();
    $.each(jQuery('#images2')[0].files, function(i, file) {
        formData.append('file[' + i + ']', file);
    });
    $.ajax({
        url: url_uploadfiles, //Server script to process data
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(json) {
            $('#images2').val('')
            $.each(json.files, function(index, value) {
                var str = '<input type="hidden" name="img[]" value="' + json.filename[index] + '">';
                $('#image_preview2').append("<div>" + str + "<img src='" + value + "' style='width:150px; height:149px; object-fit:cover; float:left; margin-top: 6px; margin-right: 10px;' ></div>");
            });
        }
    });
}

$(document).ready(function(){

	$("#pdf2").change(function() {
        $("#pdf_name2").text(this.files[0].name);
    });

    $('body').on('click','.show-links',function(){

        var cl = $(this).attr('data-class');
        if($('.'+cl).hasClass('hidden')){
            $(this).find('i').removeClass('fa-angle-down');
            $(this).find('i').addClass('fa-angle-up');
            $('.'+cl).removeClass('hidden');
        }else{
            // $(this).html('<i class="fa fa-angle-down"></i>');
            $(this).find('i').addClass('fa-angle-down');
            $(this).find('i').removeClass('fa-angle-up');
            $('.'+cl).addClass('hidden');
            // $(this).next().addClass('hidden');
        }
    });

    $('.vermi-upload').click(function() {
        $('#post_list').val(1);
        $('#vadsPostForm').submit();
    });

    $('body').on('click', '.add-custom-vacant', function() {
        var $this = $(this);
        $(this).attr('disable', 'disabled');
        $.ajax({
            url: url_add_new_vacant,
            type: "get",
            data: {
                property_id: $(this).attr('data-id')
            },
            success: function(response) {
                var url = $('#path-properties-show').val();
                alert(response.message);
                $('#vacant-space-wrapper').load(url + ' #vacant_space', function() {
                    $('#vacant_space .multi-category').select2();
                    $this.attr('disable', false);
                });
            }
        });
        return false;
    });

    $('.save-ad-post').on('click', function() {
        $.ajax({
            url: url_save_new_post,
            type: "post",
            data: $('#adsPostForm').serialize(),
            success: function(response) {}
        });
    });
});