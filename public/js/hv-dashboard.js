var load_title; var load_table; var load_url; var table_obj;

function loadTable(reload = false){

	if(!reload){
		$('#table-data-modal').find('.modal-title').text(load_title);
		$('#table-data-modal').modal('show');
	}

	$.ajax({
      	url: load_url,
      	type: 'GET',
      	beforeSend: function() {
      		// if(!reload){
      			$('#table-data-modal').find('.modal-body').html('<div class="loading"></div>');
      		// }
      	},
      	success: function(result) {
        
	        if(load_table == 'table-property-release-invoice'){
	      	   $('#table-data-modal').find('.modal-body').html(result.html);
	        }else{
	            $('#table-data-modal').find('.modal-body').html(result);
	        }
	        
	        if(load_title.trim()=="Liquiplanung"){
	            $('.liquiplanung-data').each(function(){
	              str = $(this).text();
	              if(str.includes('-'))
	              {
	                $(this).addClass('text-red');
	              }
	            })            
	        }

	        if(load_table){
	        	var tableElement = $('#table-data-modal').find('#'+load_table);
	        	var tableSetting = tableDetail(load_table);

	          	if(load_table=="property-insurance-table"){
	            	tableElement = ".property-insurance-table";
	            	var autocomplete_element = $('#table-data-modal').find('.angebote-section-comment');
	            	makeAutoComplete(autocomplete_element);
	          	}

	          	if(load_table=="table-approval-building-insurance"){
	            	tableElement = ".table-approval-building-insurance";
	            	var autocomplete_element = $('#table-data-modal').find('.property-comment');
	            	makeAutoComplete(autocomplete_element);
	          	}

	          	if(load_table=="table-release-liability"){
	            	tableElement = ".table-release-liability";
	            	var autocomplete_element = $('#table-data-modal').find('.property-comment');
	            	makeAutoComplete(autocomplete_element);
	          	}

	          	if(load_table=="table-property-management-approval"){
	            	tableElement = ".table-property-management-approval";
	            	var autocomplete_element = $('#table-data-modal').find('.property-comment');
	            	makeAutoComplete(autocomplete_element);
	          	}

          		var page_length = (typeof tableSetting['length'] !== 'undefined' && tableSetting['length'] != 0 && tableSetting['length'] != '') ? tableSetting['length'] : 10;

	        	if(typeof tableSetting['column'] !== 'undefined' && tableSetting['column'].length){
	        		table_obj = makeDatatable(tableElement,tableSetting['column'], tableSetting['order'], tableSetting['position'], page_length);
	        	}else{
	        		table_obj = makeDatatable(tableElement);
	        	}
        	}

	        if ($('.contract-comment').length)
	            $('.contract-comment').editable();
	        if ($('.invoice-comment').length)
	          $('.invoice-comment').editable();
	        if ($('.angebote-comment').length)
	          $('.angebote-comment').editable();

	        if($('.long-text').length){
	          	new showHideText('.long-text', {
	              charQty     : 150,
	              ellipseText : "...",
	              moreText    : "More",
	              lessText    : " Less"
	          	});
	        }
      	},
      	error: function(error) {
      		$('#table-data-modal').find('.modal-body').html('<p class="text-danger">Somthing Want Wrong!</p>');
      	}
  	});
}

function tableDetail(table){
	var data = [];

	if(table == 'table-property-invoice'){
			data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date-time" },null,null,null,null,null,null];
			data['position'] = 9;
			data['order'] = 'desc';
	}else if(table == 'table-property-invoice-am'){
      data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date-time" },null,null,null,null];
      data['position'] = 3;
      data['order'] = 'desc';
  }else if(table == 'table-contract-release'){
		data['column'] = [null,null,null,{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "new-date" }, { "type": "new-date" }, { "type": "new-date" },null,null,{ "type": "new-date-time" },null,null,null,null,null];
		data['position'] = 10;
		data['order'] = 'desc';
	}else if(table == 'table-approval-building-insurance' || table == 'table-release-liability'){
    data['column'] = [null,null,{ "type": "numeric-comma" },null,null,null,null,null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'list-properties-1'){
    data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'table-property-invoice2' || table == 'table-property-invoice1'){
    data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date" },null,null,null];
    data['position'] = 9;
    data['order'] = 'desc';
  }else if(table == 'property-insurance-table'){
    data['column'] = [null,null,null,{ "type": "numeric-comma" },null,null,{"type": "new-date-time"},null,null,null,null,null,null, null, null];
    data['position'] = 6;
    data['order'] = 'desc';
  }else if(table == 'table-property-management-approval'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null,null,null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'property-provision-table'){
    data['column'] = [null,null,null,{ "type": "numeric-comma", "class": "text-right" },null,{ "type": "numeric-comma", "class": "text-right" }, { "type": "numeric-comma", "class": "text-right" },{ "type": "numeric-comma", "class": "text-right" },{ "type": "numeric-comma", "class": "text-right" },null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'added-month'){
    data['column'] = [null,null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
    data['length'] = 50;
  }else if(table == 'list-statusloi'){
    data['column'] = [null, { "type": "numeric-comma" }, { "type": "new-date" }];
    data['position'] = 2;
    data['order'] = 'desc';
  }else if(table == 'dashboard-table-2'){
    data['column'] = [null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'rental-overview-table'){
    data['column'] = [null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'property-default-payer-table'){
    data['column'] = [null,null,null,null,{ "type": "new-date-time" },null,null,null,null,{ "type": "new-date-time" },null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 4;
    data['order'] = 'asc';
  }else if(table == 'oplist-table'){
    data['column'] = [null,null,null,null,{ "type": "new-date-time" },null,null,null,null,null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "new-date" },null,null];
    data['position'] = 4;
    data['order'] = 'asc';
  }
  else if(table == 'bank-properties'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'financing-per-bank-table'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 1;
    data['order'] = 'asc';
  }else if(table == 'insurance-table1' || table == 'insurance-table0'){
    data['column'] = [null,{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 2;
    data['order'] = 'desc';
  }else if(table == 'hausmax_table'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 2;
    data['order'] = 'desc';
  }else if(table == 'mahnung-properties'){
    data['column'] = [null,null,{ "type": "numeric-comma" }];
    data['position'] = 2;
    data['order'] = 'desc';
  }else if(table == 'expired-rental-agreement-table'){
    data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null];
    data['position'] = 4;
    data['order'] = 'asc';
  }else if(table == 'tenant-table4'){
    data['column'] = [{"orderable": false},null,null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null,null];
    data['position'] = 5;
    data['order'] = 'desc';
  }else if(table == 'rental_activity_table'){
    data['column'] = [null,null,null,null,{ "type": "numeric-comma" },null,null,null,null,null,null,null,null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'expiring_leases_table'){
    data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null];
    data['position'] = 5;
    data['order'] = 'asc';
  }else if(table == 'expiring-building-insurance-table' || table == 'expiring-liability-insurance-table'){
    data['column'] = [null,null,null,null,{ "type": "numeric-comma" },{ "type": "new-date" },null,{ "type": "numeric-comma" }];
    data['position'] = 5;
    data['order'] = 'asc';
  }else if(table == 'vacancy-per-object-table'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 4;
    data['order'] = 'desc';
  }else if(table == 'recommendation-table'){
    data['column'] = [null,null,null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }, { "type": "numeric-comma" }, { "type": "numeric-comma" }, { "type": "numeric-comma" }, null, null, { "type": "new-date" }, null, null, null, null];
    data['position'] = 14;
    data['order'] = 'desc';
  }else if(table == 'standard-land-value-table'){
    data['column'] = [null,null,null,{ "type": "numeric-comma" }, { "type": "numeric-comma" }];
    data['position'] = 4;
    data['order'] = 'desc';
  }else if(table == 'bka-table'){
    data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'property-mail-table'){
    data['column'] = [null,null,null,null,null,null,{ "type": "new-date-time" },null];
    data['position'] = 4;
    data['order'] = 'desc';
  }else if(table == 'liquiplanung-table'){
    data['column'] = [null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'table-property-release-invoice'){
    data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "new-date-time" }, null, null];
    data['position'] = 5;
    data['order'] = 'desc';
  }else if(table == 'table-utility-prices'){
    data['column'] = [null,null,null,null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'invoice-table-0' || table == 'invoice-table-1' || table == 'invoice-table-2' || table == 'invoice-table-3' || table == 'invoice-table-4'){
    data['column'] = [null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date" },null,null,null,null,null,null];
    data['position'] = 8;
    data['order'] = 'desc';
  }
  return data;
}

function removeDeactiveUser(){
  if(in_active_user_arr.length > 0){

    if($('.asset_manager').length){
      $('.asset_manager').each(function(){
        var $this = $(this);
        var username = $.trim($(this).text());
        if(in_active_user_arr.indexOf(username) !== -1){
          $($this).html("");
        }
      });
    }

    if($('.custom_user').length){
      $('.custom_user').each(function(){
        var $this = $(this);
        var username = $.trim($(this).text());
        if(in_active_user_arr.indexOf(username) !== -1){
          $($this).html("");
        }
      });
    }
  }
}

$(document).ready(function(){

	$('body').on('click', '.load-table', function() {
  	load_title = $(this).attr('data-title');
  	load_table = $(this).attr('data-table');
  	load_url = $(this).attr('data-url');

  	loadTable();
  });

  $(document).ajaxComplete(function() {
      removeDeactiveUser();
  });

  vacant_id = vacant_release_type = ""; 
	$('body').on('click', '.invoice-release-request-am', function() {
	    vacant_release_type = $(this).attr('data-column');
	    vacant_id = $(this).attr('data-id');
	    property_id = $(this).attr('data-property_id');
	    $('.invoice-release-comment-am').val("")
	    $('#invoice-release-property-modal-am').modal();

	});
	
	$('body').on('click', '.invoice-release-submit-am', function() {
	    comment = $('.invoice-release-comment-am').val();
	    
	    $.ajax({
	        type: 'POST',
	        url: url_property_invoicereleaseprocedure,
	        data: {
	            id: vacant_id,
	            step: vacant_release_type,
	            _token: _token,
	            comment: comment,
	            property_id: property_id
	        },
	        success: function(data) {
	          loadTable(true);
	        }
	    });
	});

  $('body').on('click', '#card-release-invoice', function() {
      var url = $(this).attr('data-url');
      $('#release-invoice-modal').modal('show');

      var release_invoice_table;

      if ( ! $.fn.DataTable.isDataTable( '#release-invoice-table' ) ) {

        release_invoice_table = $('#release-invoice-table').DataTable({
          processing: true,
          serverSide: true,
          "order": [
              [8, 'desc']
          ],
          ajax: url,
          columns: [
              {data: 'DT_RowIndex', name: 'pi.id'},
              {data: 'name_of_property', name: 'p.name_of_property'},
              {data: 'invoice', name: 'pi.invoice'},
              {data: 'date', name: 'pi.date'},
              {data: 'amount', name: 'pi.amount', className: 'text-right'},
              {data: 'latest_comment', orderable: false, searchable: false},
              {data: 'checkbox', orderable: false, searchable: false},
              {data: 'name', name: 'u.name'},
              {data: 'created_at', name: 'pml.created_at'},
              {data: 'comment', name: 'pml.comment'},
              {data: 'is_paid', orderable: false, searchable: false},
              {data: 'forward_button', orderable: false, searchable: false}
          ],
          "drawCallback": function( settings ) {
              if($('.long-text').length){
                // new showHideText('.long-text');
                new showHideText('.long-text', {
                    charQty     : 150,
                    ellipseText : "...",
                    moreText    : "More",
                    lessText    : " Less"
                });
              }
          }
        });

      }else{
        $('#release-invoice-table').DataTable().ajax.reload(null, false);
      }

  });

  /*--------------------JS FOR PROPERTY COMMENTS- START----------------------------------*/
  var c_record_id = '';
  var c_property_id = '';
  var c_type = '';
  var c_is_comment = false;
  $('body').on('click', '.btn-add-property-comment', function() {

      var $this = $(this);

      c_record_id = $($this).attr('data-record-id');
      c_property_id = $($this).attr('data-property-id');
      c_type = $($this).attr('data-type');

      var comment = $($this).closest('.property-comment-section').find('.property-comment').val();
      var reload = $(this).attr('data-reload');
      var subject = $(this).attr('data-subject');
      var content = $(this).attr('data-content');

      var data = { 
          'property_id': c_property_id,
          'comment': comment,
          'type': c_type,
          'subject': subject,
          'content': content,
      };
      if(c_record_id && c_record_id != ''){
        data.record_id = c_record_id;
      }

      if(comment){
        addPropertyComment($this, data, (reload == '1') ? true : false);
      }
  });

  $('body').on('click', '.btn-show-property-comment', function() {
    var $this = $(this);
    var show_form = ($($this).attr('data-form') == '1') ? true : false;
    var subject = $(this).attr('data-subject');
    var content = $(this).attr('data-content');

    c_record_id = $($this).attr('data-record-id');
    c_property_id = $($this).attr('data-property-id');
    c_type = $($this).attr('data-type');



    if(show_form){
        $('#property_comment_modal').find('.property-comment-section').removeClass('hidden');
        $('#property_comment_modal').find('.btn-add-property-comment').attr('data-record-id', c_record_id);
        $('#property_comment_modal').find('.btn-add-property-comment').attr('data-property-id', c_property_id);
        $('#property_comment_modal').find('.btn-add-property-comment').attr('data-type', c_type);
        
        $('#property_comment_modal').find('.btn-add-property-comment').attr('data-subject', subject);
        $('#property_comment_modal').find('.btn-add-property-comment').attr('data-content', content);
    }else{
      $('#property_comment_modal').find('.property-comment-section').addClass('hidden');
    }

    if(c_type=='properties_default_payers')
      c_record_id = 0;

    $('#property_comment_modal').modal('show');
    getPropertyComment(c_record_id, c_property_id, c_type);
  });

  $('body').on('click', '#property_comment_limit', function() {
      var text = $(this).text();
      if(text == 'Show More'){
          $(this).text('Show Less');
      }else{
          $(this).text('Show More');
      }
      getPropertyComment(c_record_id, c_property_id, c_type);
  });

  $('body').on('click', '.remove-property-comment', function() {
      var url = $(this).attr('data-url');
      var $this = $(this);
      if(confirm('Are you sure to delete this comment?')){
          $.ajax({
              url: url,
              type: 'GET',
              dataType: 'json',
              success: function(result) {
                  if(result.status){
                      getPropertyComment(c_record_id, c_property_id, c_type);
                  }else{
                      alert(result.message);
                  }
              },
              error: function(error) {
                  alert('Somthing want wrong!');
              }
          });
      }else{
          return false;
      }
  });

  $('#property_comment_modal').on('hidden.bs.modal', function () {
    var ele = $(this).find('.property-comment-section');
    
    if(!$(ele).hasClass("hidden") && c_is_comment){

      c_is_comment = false;

      if($('#release-invoice-modal').hasClass('in')){
        $('#release-invoice-table').DataTable().ajax.reload(null, false);
      }else if($('#release-contract-modal').hasClass('in')){
        $('#release-contract-table').DataTable().ajax.reload(null, false);
      }else{
        loadTable(true);
      }
    }
  });

  $('body').on('click', '.load_property_comment_section', function() {
      var $this = $(this);
      var url = $(this).attr('data-url');
      var text = $(this).text();
      var closest = $(this).attr('data-closest');
      closest = (closest) ? closest : 'div';

      var limit = '';

      if(text == 'Show More'){
          $($this).text('Show Less');
          limit = '';
      }else{
          $($this).text('Show More');
          limit = 2;
      }

      var new_url = url+'&limit='+limit;

      $.ajax({
          url: new_url,
          type: 'GET',
          dataType: 'json',
          success: function(result) {
              $($this).closest(closest).find('.show_property_cmnt_section').empty();
              var html = '';
              if(result){
                  $.each(result, function(key, value) {
                      var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                      var commented_user = value.user_name+''+company;
                      html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
                  });
              }
              $($this).closest(closest).find('.show_property_cmnt_section').html(html);
          },
          error: function(error) {
              console.log({error});
          }
      });
  });

  function addPropertyComment(btnobj, data, reload = false){
    $.ajax({
        url: url_add_property_comment,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $(btnobj).prop('disabled', true);
        },
        success: function(result) {

            $(btnobj).prop('disabled', false);

            if (result.status == true) {

                $(btnobj).closest('.property-comment-section').find('.property-comment').val('');
                $(btnobj).closest('.property-comment-section').find('.property-comment').focus();

                if(reload){
                    c_is_comment = true;
                    getPropertyComment(c_record_id, c_property_id, c_type);
                }

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $(btnobj).prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });
  }


  function getPropertyComment(record_id = '', property_id = '', type = ''){
      var limit_text = $('#property_comment_limit').text();
      var limit = (limit_text == 'Show More') ? 1 : '';

      var new_url = url_get_property_comment+'?property_id='+property_id+'&record_id='+record_id+'&type='+type+'&limit='+limit;

      $.ajax({
          url: new_url,
          type: 'GET',
          dataType: 'json',
          success: function(result) {

              $('#property_comment_table').find('tbody').empty();
              
              if(type == 'property_invoices'){
                  $('#property_comment_table').find('#th_name').hide();
                  $('#property_comment_table').find('#th_date').hide();
              }else{
                  $('#property_comment_table').find('#th_name').show();
                  $('#property_comment_table').find('#th_date').show();
              }

              if(result){
                  $.each(result, function(key, value) {
                      var clear_url = url_delete_property_comment.replace(":id", value.id);
                      var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-property-comment">Löschen</a></td>' : '';
                      var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                      var commented_user = value.user_name+''+company;

                      if(type == 'property_invoices'){
                        var tr = '\
                          <tr>\
                              <td>'+commented_user+' ('+value.date+') : '+ value.comment +'</td>\
                              <td class="text-center">'+delete_button+'\
                          </tr>\
                        ';
                      }else{
                        var tr = '\
                          <tr>\
                              <td>'+ commented_user +'</td>\
                              <td>'+ value.comment +'</td>\
                              <td>'+ value.created_at +'</td>\
                              <td>'+delete_button+'\
                          </tr>\
                        ';
                      }

                      $('#property_comment_table').find('tbody').append(tr);
                  });
              }
          },
          error: function(error) {
              console.log({error});
          }
      });
  }
  /*--------------------JS FOR PROPERTY COMMENTS- END----------------------------------*/

  var reload_custom_user = 0;
  $('body').on('click', '.custom_user', function() {
    var property_id = $(this).attr('data-property-id');
    var user_id = $(this).attr('data-user-id');
    var subject = $(this).attr('data-subject');
    var content = $(this).attr('data-content');
    var reload = $(this).attr('data-reload');
    var email = $(this).attr('data-email');
    var mail_type = $(this).attr('data-title');

    var id = $(this).attr('data-id');
    var section = $(this).attr('data-section');

    reload_custom_user = (typeof reload !== 'undefined' && reload == "1") ? 1 : 0;
    email = (typeof email !== 'undefined') ? email : "";
    mail_type = (typeof mail_type !== 'undefined') ? mail_type : "";

    id = (typeof id !== 'undefined') ? id : '';
    section = (typeof section !== 'undefined') ? section : '';

    if(section){
      reload_custom_user = 1
    }

    $('#modal_sendmail_to_custom_user').find('input[name="property_id"]').val(property_id);
    $('#modal_sendmail_to_custom_user').find('input[name="user_id"]').val(user_id);
    $('#modal_sendmail_to_custom_user').find('input[name="subject"]').val(subject);
    $('#modal_sendmail_to_custom_user').find('input[name="content"]').val(content);
    $('#modal_sendmail_to_custom_user').find('input[name="email"]').val(email);
    $('#modal_sendmail_to_custom_user').find('input[name="mail_type"]').val(mail_type);

    $('#modal_sendmail_to_custom_user').find('input[name="id"]').val(id);
    $('#modal_sendmail_to_custom_user').find('input[name="section"]').val(section);

    $('#modal_sendmail_to_custom_user').modal('show');
  });

  $('#form_modal_sendmail_to_custom_user').on('submit', (function(e) {
      e.preventDefault();

      var $this = $(this);
      var formData = new FormData($(this)[0]);
      var url = $(this).attr('action');

      $.ajax({
          url: url,
          type: 'POST',
          data: formData,
          dataType: 'json',
          processData: false,
          contentType: false,
          cache: false,
          beforeSend: function() {
              $($this).find('button[type="submit"]').prop('disabled', true);
          },
          success: function(result) {
              $($this).find('button[type="submit"]').prop('disabled', false);
              if (result.status) {
                $($this)[0].reset();
                $('#modal_sendmail_to_custom_user').modal('hide');
                if(reload_custom_user == 1){
                  loadTable(true);
                }
              }else{
                alert(result.message);
              }
          },
          error: function(error) {
              $($this).find('button[type="submit"]').prop('disabled', false);
              alert('Somthing want wrong!');
              console.log({error});
          }
      });
  }));

  // $('#form-forward-to').find('select[name="user"]').select2();

  var reload_forward_to = false;
  $('body').on('click','.btn-forward-to',function(){
      var property_id = $(this).attr('data-property-id');
      var subject = $(this).attr('data-subject');
      var content = $(this).attr('data-content');
      var title = $(this).attr('data-title');
      var reload = $(this).attr('data-reload');
      var email = $(this).attr('data-email');
      var user = $(this).attr('data-user');
      var section_id = $(this).attr('data-id');
      var section = $(this).attr('data-section');

      section = (typeof section !== 'undefined') ? section : '';
      reload_forward_to = (reload == "1") ? true : false;

      if(typeof email !== 'undefined' && email){
          $('#modal-forward-to').find('input[name="email"]').val(email);
      }

      if(typeof user !== 'undefined' && user){
          $('#modal-forward-to').find('select[name="user"]').val(user);
      }

      $('#modal-forward-to').find('input[name="property_id"]').val(property_id);
      $('#modal-forward-to').find('input[name="subject"]').val(subject);
      $('#modal-forward-to').find('input[name="content"]').val(content);
      $('#modal-forward-to').find('input[name="title"]').val(title);
      $('#modal-forward-to').find('input[name="section_id"]').val(section_id);
      $('#modal-forward-to').find('input[name="section"]').val(section);

      $('#modal-forward-to').modal('show');
  });

  $('#form-forward-to').submit(function(event) {
      event.preventDefault();
      var $this = $(this);
      var data = new FormData($(this)[0]);
      var url = $(this).attr('action');

      var email = $($this).find('input[name="email"]').val();
      var user = $($this).find('select[name="user"]').val();

      if(email || user){

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#modal-forward-to').modal('hide');
                    $($this)[0].reset();
                    if(reload_forward_to){
                      loadTable(true);
                    }
                }else{
                    alert(result.message);
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                alert('Somthing want wrong!');
            }
        });

      }else{
        alert('Please select user or enter email!');
      }
  });

  $('body').on('click','.mark-as-pending-am',function(){
      inv_id = $(this).attr('data-id');
      inv_property_id = $(this).attr('data-property_id');
      $('#pending-modal-am').modal();
      $('.invoice-pending-am-comment').val("")
  });

  $('body').on('click','.invoice-pending-am-submit',function(){
      comment = $('.invoice-pending-comment').val();
      $.ajax({
          type : 'POST',
          url : url_property_invoicemarkpendingam,
          data : {
              id:inv_id, 
              _token : _token, 
              comment:comment,
              property_id:inv_property_id
          },
          success : function (data) {
              loadTable(true);
          }
        });
  });

  var load_release_angebote_url;

  $('body').on('click', '#released-angebote', function(){
    var url = $(this).attr('data-url');
    load_release_angebote_url = url;
    var title = $(this).attr('data-title');

    $('#table-released-angebote-modal').find('.modal-title').text(title);
    $('#table-released-angebote-modal').modal('show');
    loadreleasedAngebote();
  });

  function loadreleasedAngebote(){
    var columns = [
          null,
          null,
          null,
          null,
          null,
          {"type": "new-date-time"},
          {"type": "new-date-time"},
          null,
          { "visible": false },
          null,
          null,
          null,
          null,
          null,
          null,
      ];
      var dom = "<'row'<'col-md-2'l><'col-md-3 dt_custom_info'i><'col-md-2 custom_button'><'col-md-5'f>>"+
            "<'row'<'col-md-12'tr>>"+
            "<'row'<'col-md-12'p>>";

    $.ajax({
        url: load_release_angebote_url,
        type: 'GET',
        beforeSend: function() {
            $('#table-released-angebote-modal').find('.modal-body').html('<div class="loading"></div>');
        },
        success: function(result) {
          $('#table-released-angebote-modal').find('.modal-body').html(result);

          $('.tbl-insurance-tab-detail').dataTable({
            "dom": dom,
            "order": [[6, "desc"]],
            "columns": columns,
          });

          if( $('#table-released-angebote-modal').find('.tbl-insurance-tab-detail').length > 0 ){
              $('#table-released-angebote-modal').find('.tbl-insurance-tab-detail').each(function(){
                  var tabid = $(this).attr('data-tabid');
                  $('#insurance-table-'+tabid+'_paginate').addClass('hidden');
                  $('#insurance-table-'+tabid).closest('.dataTables_wrapper').find('.custom_button').html('<button type="button" class="btn btn-primary btn-xs btn-show-more-record">Weitere Angebote anzeigen</button>');
              });
          }

        },
        error: function(error) {
          $('#table-released-angebote-modal').find('.modal-body').html('<p class="text-danger">Somthing Want Wrong!</p>');
        }
    });
  }

  $('body').on('click', '.btn-show-more-record', function(){
      var text = $(this).text();
      var tableid = $(this).closest('.dataTables_wrapper').find('table').attr('id');


      if(text == 'Weitere Angebote anzeigen'){
          $(this).text('Freigegebenes Angebot anzeigen');
          $('#'+tableid).find('tbody tr').removeClass('hidden');
          $('#'+tableid+'_paginate').removeClass('hidden');
      }else{
          $(this).text('Weitere Angebote anzeigen');
          // $('#'+tableid).find('tbody tr').not(':first').addClass('hidden');
          $('#'+tableid).find('tbody tr').each(function(i){
              var release_date = $.trim($(this).find('td:eq(6)').text());
              if(!release_date){
                  $(this).addClass('hidden');
              }
          });

          $('#'+tableid+'_paginate').addClass('hidden');
      }
  });

  $('body').on('click', '.load_comment', function() {
      var $this = $(this);
      var url = $(this).attr('data-url');
      var text = $(this).text();
      var limit = '';

      if(text == 'Show More'){
          $($this).text('Show Less');
          limit = '';
      }else{
          $($this).text('Show More');
          limit = 2;
      }

      var new_url = url+'/'+limit+'?type=property_insurance_tab_details';

      $.ajax({
          url: new_url,
          type: 'GET',
          dataType: 'json',
          success: function(result) {
              $($this).closest('td').find('.show_cmnt').empty();
              var html = '';
              if(result){
                  $.each(result, function(key, value) {
                      var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                      var commented_user = value.user_name+''+company;
                      html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
                  });
              }
              $($this).closest('td').find('.show_cmnt').html(html);
          },
          error: function(error) {
              console.log({error});
          }
      });
  });

  /*----------------------JS FOR Property Insurance comments - START -------------------------*/
  var insurance_detail_comment_url;
  var insurance_detail_comment_id;
  var type = '';
  var ins_table;
  var ins_property_id = '';
  var ins_is_reload = false;

  $('body').on('click', '.btn-ins-comment', function() {
      ins_table = $(this).closest('table').attr('id');
      insurance_detail_comment_url = $(this).attr('data-url');
      insurance_detail_comment_id  = $(this).attr('data-id');
      type = 'property_insurance_tab_details';
      ins_property_id = $(this).attr('data-property-id');

      var limit_text = $('#insurance_detail_comments_limit').text();
      getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
      // $('#insurance_detail_comment_modal').find('form').show();
      $('#insurance_detail_comment_modal').modal('show');
  });

  $('#insurance_detail_comment_form').submit(function(event) {

      event.preventDefault();
      var $this = $(this);
      var url = $(this).attr('action');
      var comment = $($this).find('textarea[name="comment"]').val();
      var data = {
          'record_id': insurance_detail_comment_id,
          'property_id': ins_property_id,
          'comment': comment,
          'type': type
      };

      $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',
          beforeSend: function() {
              $($this).find('button[type="submit"]').prop('disabled', true);
          },
          success: function(result) {

              $($this).find('button[type="submit"]').prop('disabled', false);
              if (result.status == true) {
                  $($this)[0].reset();
                  $($this).find('textarea[name="comment"]').focus();

                  var limit_text = $('#insurance_detail_comments_limit').text();
                  getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);

                  ins_is_reload = true;

              } else {
                  alert(result.message);
              }
          },
          error: function(error) {
              $($this).find('button[type="submit"]').prop('disabled', false);
              alert('Somthing want wrong!');
          }
      });

  });

  $('#insurance_detail_comment_modal').on('hidden.bs.modal', function () {
      if(ins_is_reload){
          ins_is_reload = false;
          loadreleasedAngebote();
      }
  });

  $('body').on('click', '#insurance_detail_comments_limit', function() {
      var text = $(this).text();
      var limit = '';
      if(text == 'Show More'){
          limit = '';
          $(this).text('Show Less');
      }else{
          limit = 1;
          $(this).text('Show More');
      }
      getInsuranceDetailComments(insurance_detail_comment_url, limit, type);
  });

  $('body').on('click', '.remove-insurance-detail-comment', function() {
      var url = $(this).attr('data-url');
      var $this = $(this);
      if(confirm('Are you sure to delete this comment?')){
          $.ajax({
              url: url,
              type: 'GET',
              dataType: 'json',
              success: function(result) {
                  if(result.status){
                      ins_is_reload = true;
                      var limit_text = $('#insurance_detail_comments_limit').text();
                      getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
                  }else{
                      alert(result.message);
                  }
              },
              error: function(error) {
                  alert('Somthing want wrong!');
              }
          });
      }else{
          return false;
      }
  });

  function getInsuranceDetailComments(url, limit = '', type=''){
      var new_url = url+'/'+limit+'?type='+type;

      $.ajax({
          url: new_url,
          type: 'GET',
          dataType: 'json',
          success: function(result) {

              $('#insurance_detail_comments_table').find('tbody').empty();
              if(result){
                  $.each(result, function(key, value) {
                      var clear_url = url_delete_insurance_detail_comment.replace(":id", value.id);
                      var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-insurance-detail-comment">Löschen</a></td>' : '';
                      var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                      var commented_user = value.user_name+''+company;

                      var tr = '\
                      <tr>\
                          <td>'+ commented_user +'</td>\
                          <td>'+ value.comment +'</td>\
                          <td>'+ value.created_at +'</td>\
                          <td>'+delete_button+'\
                      </tr>\
                      ';

                      $('#insurance_detail_comments_table').find('tbody').append(tr);
                  });
              }
          },
          error: function(error) {
              console.log({error});
          }
      });
  }

  $('body').on('click', '.btn-show-angebote-section-comment', function() {
    var url = $(this).attr('data-url');
    var id = $(this).attr('data-id');
    ins_property_id = $(this).attr('data-property-id');

    insurance_detail_comment_url = url;
    insurance_detail_comment_id = id;

    var limit_text = $('#insurance_detail_comments_limit').text();
    type = 'property_insurance_tabs';
    getInsuranceDetailComments(url, (limit_text == 'Show More') ? 1 : '', type);
    // $('#insurance_detail_comment_modal').find('form').hide();
    $('#insurance_detail_comment_modal').modal('show');
  });
  /*----------------------JS FOR Property Insurance comments - END -------------------------*/

  $('body').on('click', '.delete-insurance-tab-detail', function() {
      var tab_id = $(this).closest('table').attr('data-tabid');
      var url = $(this).attr('data-url');

      if(confirm('Are you sure want to delete this record?')){
          $.ajax({
              url: url,
              type: 'GET',
              dataType: 'json',
              success: function(result) {
                  if(result.status){
                      loadreleasedAngebote();
                  }else{
                    alert(result.message);
                  }
              },
              error: function(error){
                  alert('Somthing want wrong!');
                  console.log({error});
              }
          });
      }
      return false;
  });

  $('body').on('click', '.mark-as-paid', function() {
      var id = $(this).attr('data-id');
      $this = $(this);
      var url = url_makeAsPaid + "?id=" + id;
      $.ajax({
          url: url,
      }).done(function(data) {
          $this.removeClass('btn-primary');
          $this.removeClass('mark-as-paid');
          $this.addClass('btn-success');
          $this.html("Erledigt");
      });
  });

  /*---------------------NEW - JS FOR RELEASE INVOICE - START----------------*/

  $('body').on('click', '.btn-inv-release-am', function(){

      var $this = $(this);

      var id = $(this).attr('data-id');
      var status = $(this).attr('data-status');

      var data = {'id': id, 'status': status, 'comment': ''};

      $.ajax({
          url: url_invoice_release_am,
          type: 'POST',
          data: data,
          dataType: 'json',
          success: function(result) {
              if (result.status) {
                  loadTable(true);
              }else{
                  alert(result.message);
              }
          },
          error: function(error) {
              alert('Somthing want wrong!');
          }
      });
  });

  $('body').on('click', '.btn-inv-release-hv', function(){

      var $this = $(this);

      var id = $(this).attr('data-id');
      var status = $(this).attr('data-status');

      var data = {'id': id, 'status': status, 'comment': ''};

      $.ajax({
          url: url_invoice_release_hv,
          type: 'POST',
          data: data,
          dataType: 'json',
          success: function(result) {
              if (result.status) {
                  loadTable(true);
              }else{
                  alert(result.message);
              }
          },
          error: function(error) {
              alert('Somthing want wrong!');
          }
      });
  });

  var inv_usr_release_id;
  var inv_usr_release_status;

  $('body').on('click', '.btn-inv-release-usr', function(){
    var $this = $(this);
    var modal = $('#invoice-release-usr-modal');

    inv_usr_release_id = $(this).attr('data-id');
    inv_usr_release_status = $(this).attr('data-status');
    var title = $(this).attr('title');

    if(inv_usr_release_status == '1'){
        
        $(modal).find('.modal-title').text(title);
        $(modal).modal('show');
        
    }else{

        var data = {'id': inv_usr_release_id, 'status': inv_usr_release_status, 'comment': ''};

        $.ajax({
            url: url_invoice_release_user,
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(result) {
                if (result.status) {
                    loadTable(true);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }
  });

  $('#invoice-release-usr-form').on('submit', (function(e) {
      e.preventDefault();

      var $this = $(this);
      var modal = $('#invoice-release-usr-modal');
      var url = $($this).attr('action');
      var comment = $($this).find('textarea[name="comment"]').val();
      var user_id = $($this).find('select[name="user"]').val();

      var data = {
          'id': inv_usr_release_id, 
          'status': inv_usr_release_status, 
          'comment': '',
          'user_id': user_id
      };

      $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',
          beforeSend: function() {
              $($this).find('button[type="submit"]').prop('disabled', true);
          },
          success: function(result) {

              $($this).find('button[type="submit"]').prop('disabled', false);

              if (result.status) {

                  $($this)[0].reset();
                  $('#invoice-release-usr-error').empty();
                  $(modal).modal('hide');

                  loadTable(true);

              }else{
                showFlash($('#invoice-release-usr-error'), result.message, 'error');
              }
          },
          error: function(error) {
              $($this).find('button[type="submit"]').prop('disabled', false);
              showFlash($('#invoice-release-usr-error'), 'Somthing want wrong!', 'error');
          }
      });
  }));

  $('body').on('click', '.btn-inv-release-falk', function(){

      var $this = $(this);

      var id = $(this).attr('data-id');
      var status = $(this).attr('data-status');

      var data = {'id': id, 'status': status, 'comment': ''};

      $.ajax({
          url: url_invoice_release_falk,
          type: 'POST',
          data: data,
          dataType: 'json',
          success: function(result) {
              if (result.status) {
                  loadTable(true);
              }else{
                  alert(result.message);
              }
          },
          error: function(error) {
              alert('Somthing want wrong!');
          }
      });
  });

  /*---------------------NEW - JS FOR RELEASE INVOICE - END----------------*/

  $('body').on('click', '.btn-delete-property-invoice', function() {
      var id = $(this).attr('data-id');
      var type = $(this).attr('data-type');
      if (confirm('Are you sure want to delete this invoice?')) {
          var url = url_delete_property_invoice;
          url = url.replace(':id', id);
          $.ajax({
              url: url,
              type: 'GET',
              dataType: 'json',
              success: function(result) {
                    loadTable(true);
              },
              error: function(error) {
                  alert('Somthing want wrong!');
              }
          });
      } else {
          return false;
      }
  });

});