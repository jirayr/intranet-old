var load_title; var load_table; var load_url; var table_obj;

function loadTable(reload = false){

	if(!reload){
		$('#table-data-modal').find('.modal-title').text(load_title);
		$('#table-data-modal').modal('show');
	}

	$.ajax({
      	url: load_url,
      	type: 'GET',
      	beforeSend: function() {
      		$('#table-data-modal').find('.modal-body').html('<div class="loading"></div>');
      	},
      	success: function(result) {
        
	        if(load_table == 'table-property-release-invoice'){
	      	   $('#table-data-modal').find('.modal-body').html(result.html);
	        }else if(load_table == 'insurance-table1' || load_table == 'insurance-table0'){
	        	$('#table-data-modal').find('.modal-body').html(result[0]);
	        }else{
	            $('#table-data-modal').find('.modal-body').html(result);
	        }
        
	        if(load_title.trim()=="Liquiplanung"){
	            $('.liquiplanung-data').each(function(){
	              str = $(this).text();
	              if(str.includes('-'))
	              {
	                $(this).addClass('text-red');
	              }
	            })            
	        }

	        if(load_table){
	        	var tableElement = $('#table-data-modal').find('#'+load_table);
	        	var tableSetting = tableDetail(load_table);

	          	if(load_table=="property-insurance-table"){
	            	tableElement = ".property-insurance-table";
	            	var autocomplete_element = $('#table-data-modal').find('.angebote-section-comment');
	            	makeAutoComplete(autocomplete_element);
	          	}

	          	if(load_table=="table-approval-building-insurance"){
	            	tableElement = ".table-approval-building-insurance";
	            	var autocomplete_element = $('#table-data-modal').find('.property-comment');
	            	makeAutoComplete(autocomplete_element);
	          	}

	          	if(load_table=="table-release-liability"){
	            	tableElement = ".table-release-liability";
	            	var autocomplete_element = $('#table-data-modal').find('.property-comment');
	            	makeAutoComplete(autocomplete_element);
	          	}

	          	if(load_table=="table-property-management-approval"){
	            	tableElement = ".table-property-management-approval";
	            	var autocomplete_element = $('#table-data-modal').find('.property-comment');
	            	makeAutoComplete(autocomplete_element);
	          	}

	          	var page_length = (typeof tableSetting['length'] !== 'undefined' && tableSetting['length'] != 0 && tableSetting['length'] != '') ? tableSetting['length'] : 10;

	        	if(typeof tableSetting['column'] !== 'undefined' && tableSetting['column'].length){
	        		table_obj = makeDatatable(tableElement,tableSetting['column'], tableSetting['order'], tableSetting['position'], page_length);
	        	}else{
	        		table_obj = makeDatatable(tableElement);
	        	}
	        }

	        if ($('.contract-comment').length)
	            $('.contract-comment').editable();
	        if ($('.invoice-comment').length)
	          $('.invoice-comment').editable();
	        if ($('.angebote-comment').length)
	          $('.angebote-comment').editable();

	        if($('.long-text').length){
	          new showHideText('.long-text', {
	              charQty     : 150,
	              ellipseText : "...",
	              moreText    : "More",
	              lessText    : " Less"
	          });
	        }
      	},
      	error: function(error) {
      		$('#table-data-modal').find('.modal-body').html('<p class="text-danger">Somthing Want Wrong!</p>');
      	}
  	});
}

function tableDetail(table){
	var data = [];

	if(table == 'tenant-table4'){
      	data['column'] = [{"orderable": false},null,null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null,null];
      	data['position'] = 5;
      	data['order'] = 'desc';
    }else if(table == 'invoice-table-0' || table == 'invoice-table-1' || table == 'invoice-table-2' || table == 'invoice-table-3' || table == 'invoice-table-4'){
      	data['column'] = [null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date" },null,null,null,null,null,null];
      	data['position'] = 8;
      	data['order'] = 'desc';
    }else if(table == 'list-properties-1'){
      	data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      	data['position'] = 0;
      	data['order'] = 'asc';
    }else if(table == 'dashboard-table-2'){
    	data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      	data['position'] = 0;
      	data['order'] = 'asc';
    }else if(table == 'bank-properties'){
      	data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      	data['position'] = 8;
      	data['order'] = 'asc';
    }else if(table == 'insurance-table1'){
      	data['column'] = [null,null,{ "type": "numeric-comma" }];
      	data['position'] = 2;
      	data['order'] = 'desc';
    }else if(table == 'expiring-building-insurance-table' || table == 'expiring-liability-insurance-table'){
	    data['column'] = [null,null,null,null,{ "type": "numeric-comma" },{ "type": "new-date" },null,{ "type": "numeric-comma" }];
	    data['position'] = 5;
	    data['order'] = 'asc';
	}else if(table == 'insurance-table0'){
	    data['column'] = [null,null,{ "type": "numeric-comma" }];
	    data['position'] = 2;
	    data['order'] = 'desc';
	}else if(table == 'hausmax_table'){
	    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
	    data['position'] = 2;
	    data['order'] = 'desc';
	}else if(table == 'mahnung-properties'){
	    data['column'] = [null,null,{ "type": "numeric-comma" }];
	    data['position'] = 2;
	    data['order'] = 'desc';
	}else if(table == 'expired-rental-agreement-table'){
	    data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null];
	    data['position'] = 4;
	    data['order'] = 'asc';
	}else if(table == 'expiring_leases_table'){
	    data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null];
	    data['position'] = 5;
	    data['order'] = 'asc';
	}else if(table == 'property-default-payer-table'){
      data['column'] = [null,null,null,null,{ "type": "new-date-time" },null,null,null,null,{ "type": "new-date-time" },null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
      data['position'] = 4;
      data['order'] = 'asc';
    }else if(table == 'table-open-angebote'){
      data['column'] = [null,null,{ "type": "numeric-comma" },null,null,{ "type": "new-date-time" },null,null,null,null,null];
      data['position'] = 5;
      data['order'] = 'desc';
    }
  	return data;
}

function drawchart3(container,cat,mv,vl){
    Highcharts.chart(container, {
        chart: {
            type: 'column'
        },
      title: {
          text: 'MIETERÜBERSICHT'
      },
        xAxis: {
            categories: cat,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
              text: 'Anzahl'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
             '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
      plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                  enabled: true
              }
          }
      },
        series: [{
            name: 'Miete netto p.m.',
            data: mv
        },
        {
            name: 'Anzahl',
            data: vl
        }]
    });
}

$(document).ready(function(){

	$('body').on('click', '.load-table', function() {
  	load_title = $(this).attr('data-title');
  	load_table = $(this).attr('data-table');
  	load_url = $(this).attr('data-url');

  	loadTable();
  });

  $('body').on('click','#rental-overview',function(){
      $('#rental_overview_modal').modal('show');
      $.ajax({
          url: url_getcategoryproperty,
      }).done(function (data) {
          // $('.category-prop-list1').html(data.table1);
          $('.category-prop-list2').html(data.table2);

          drawchart3('high-container3',data.name,data.actual_net_rent,data.count);

          var columns = [
              null,
              { "type": "numeric-comma" },
              { "type": "numeric-comma" },
              { "type": "numeric-comma" },
              null,
              { "type": "numeric-comma" },
          ];

          makeDatatable($('.category-properties'), columns, "desc", 1);
          $('.categTotal').html($('.ajax-total-category').html());
      });

      $.ajax({
          url: url_getcategoryproperty3,
      }).done(function (data) {
          // $('.category-prop-list1').html(data.table1);
          $('.category-prop-list4').html(data.table);
          $('.category-prop-list5').html(data.table2);
          $('.category-prop-list6').html(data.table3);
          var columns = [
              null,
              null,
              { "type": "numeric-comma" }
          ];
          makeDatatable($('.category-table4'), columns, "desc", 1);
          makeDatatable($('.category-table6'), columns, "desc", 2);
      });
  });

  $('body').on('click','#vacancy_search',function(){
    var start = $('#vacancy_start').val();
    var end = $('#vacancy_end').val();
    var url = $('#leerstände').attr('data-url');
    
    load_url = url+'&start='+start+'&end='+end;
    loadTable(true);
  });

  $('body').on('click','#btn-export-vacancy',function(){
    var checkedArr = [];
    var cells = table_obj.cells( ).nodes();
    $( cells ).find('.vacancy_check').each(function(){
      if($(this).is(':checked')){
        checkedArr.push($(this).val());
      }
    });
    if(checkedArr.length){
      var myJSON = JSON.stringify(checkedArr);
      $('#modal-vacancy-export').find('input[name="ids"]').val(myJSON);
      $('#modal-vacancy-export').modal('show');
    }else{
      alert('Please select at least one row!');
      return false;
    }
  });

  $('#form-vacancy-export').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var formData = new FormData($(this)[0]);
    var url = $(this).attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
            $($this).find('button[type="submit"]').html('Senden <i class="fa fa-spinner fa-spin" style="font-size: 20px;"></i>');
        },
        success: function(result) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            $($this).find('button[type="submit"]').html('Senden');
            if (result.status) {
              
              $($this).find('input[name="email"]').val('');
              $($this).find('textarea[name="message"]').val('');

              $('#modal-vacancy-export').modal('hide');
            }else{
              alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            $($this).find('button[type="submit"]').html('Senden');
            alert('Somthing want wrong!');
            console.log({error});
        }
    });
  }));
	
});