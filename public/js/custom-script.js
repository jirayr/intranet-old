function showFlash(element, msg, type) {
    if (type == 'error') {
        $(element).html('<p class="alert alert-danger">' + msg + '</p>');
    } else {
        $(element).html('<p class="alert alert-success">' + msg + '</p>');
    }
    setTimeout(function() {
        $(element).empty();
    }, 5000);
}
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	
	$("#select_bank").on('change', function() {
		var allVals = $(this).val();
        allVals = allVals == null ? [] : allVals;
        
		if(allVals.length === 0){
			$("#selectbank_create_btn").prop('disabled', true);
            $("#skip").prop('disabled', false);
		}else{
			$("#selectbank_create_btn").prop('disabled', false);
            $("#skip").prop('disabled', true);
		}
	})

    $('.select2').select2({
        width: '100%'
    });

    $('select[name="schl_banks"]').on('change', function(e){
        e.preventDefault();
        var $this = $(this);
        var val = $this.val();
        var prop_id = $this.data('id');
        $this.attr('disabled', 'disabeld');
        $.ajax({
            type: 'POST',
            url: '/index.php/property/save-shcl-banks/' + prop_id,
            data: {
                schl_banks: val
            }
        }).done(function(response){
            console.log(response);
            if( response )
                $this.removeAttr('disabled', 'disabeld');
        });
        return false;
    });
		
});

function getTableData(selector) {
    var myTableArray = [];

    var table = $(selector).first();

    table.find("tr").each(function() {
        var arrayOfThisRow = [];
        var tableData = $(this).find('td,th');
        if (tableData.length > 0) {
            tableData.each(function() {
                var data = $(this).text();
                var inlineEditData = $(this).find('a.inline-edit');
                if (inlineEditData.length > 0){
                    data = inlineEditData.closest('td,th').text();
                }
                if($(this).find('select').length > 0)
                {
                    data = $(this).find('select').val();
                }


                var colspanCount = $(this).attr("colspan");
                arrayOfThisRow.push(data);
                if (colspanCount > 1) {
                    for (var i = 1; i < colspanCount; i++) {
                        arrayOfThisRow.push('');
                    }
                }
            });
            myTableArray.push(arrayOfThisRow);
        }
    });
    return myTableArray;
}

function getTableTdClass(selector) {
    var myTableArray = [];

    var table = $(selector).first();

    table.find("tr").each(function() {
        var arrayOfThisRow = [];
        var tableData = $(this).find('td,th');
        if (tableData.length > 0) {
            tableData.each(function() {
                var data = $(this).attr('class');
                


                var colspanCount = $(this).attr("colspan");
                arrayOfThisRow.push(data);
                if (colspanCount > 1) {
                    for (var i = 1; i < colspanCount; i++) {
                        arrayOfThisRow.push('');
                    }
                }
            });
            myTableArray.push(arrayOfThisRow);
        }
    });
    return myTableArray;
}
function getTableClass(selector) {
    var myTableArray = [];

    var table = $(selector).first();

    table.find("tr").each(function() {
        var data = $(this).attr('class');
        myTableArray.push(data);
    });
    return myTableArray;
}

function newNotification(propertyId, type) {
    var data = {
        type : type,
        property_id : propertyId
    };

    $.ajax({
        type:'POST',
        url:'/index.php/new-notification',
        data: data,
        success:function(data){
            selection.prop( "disabled", false );
            console.log("Change successfully! ");
        }
    });
}

function seenNotification() {
    var isExpanded = document.getElementById('notification').getAttribute('aria-expanded');
    if (isExpanded != "true") {
        $.ajax({
            type: 'POST',
            url: '/index.php/seen-notification',
            data: [],
            success: function (data) {
                var newNotification = document.getElementsByClassName('new-notification');
                for (var i = 0; i < newNotification.length; i++) {
                    newNotification[i].style.display = "none";
                }
                console.log("Seen all notification!");
            }
        });
    }
}

function getMonthYear(date) {
    var month = [];
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var m = month[date.getMonth()];
    var y = date.getUTCFullYear();
    return m + ', ' + y;
}

/*------------ Show / Hide Text ------------*/
function showHideText(sSelector, options) {
    // Def. options
    var defaults = {
        charQty     : 100,
        ellipseText : "...",
        moreText    : "Show more",
        lessText    : "Show less"
    };

    var settings = $.extend( {}, defaults, options );

    var s = this;

        s.container = $(sSelector);
        s.containerH = s.container.height();

        s.container.each(function() {
            var content = $(this).html();

            if(content.length > settings.charQty) {

                var visibleText = content.substr(0, settings.charQty);
                var hiddenText  = content.substr(settings.charQty-1, content.length - settings.charQty);

                var html = visibleText
                         + '<span class="moreellipses">' +
                           settings.ellipseText
                         + '</span><span class="morecontent"><span>' +
                           hiddenText
                         + '</span><a href="" class="morelink">' +
                           settings.moreText
                         + '</a></span>';

                $(this).html(html);
            }

        });

        s.showHide = function(event) {
            event.preventDefault();
            if($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(settings.moreText);

                $(this).prev().fadeToggle('fast', function() {
                    $(this).parent().prev().fadeIn();
                });
            } else {
                $(this).addClass("less");
                $(this).html(settings.lessText);

                $(this).parent().prev().hide();
                $(this).prev().fadeToggle('fast');
            }
        }

        $(".morelink").bind('click', s.showHide);
}
/*------------------------------------------*/

/*-------------JS FOR EXPORT CONFIRM PASSWORD - START---------*/
$('body').on('click', '.btn-export', function(){
    var target  = $(this).attr('data-target');
    target = (target == '_blank') ? true : false;

    var url     = $(this).attr('data-url');

    var is_post = $(this).attr('data-post');
    is_post = (is_post == '1') ? true : false;

    var formid = $(this).attr('data-form-id');

    var data_name = $(this).attr('data-name');
    
    if(data_name == 'HAUSVERWALTUNG KOSTEN'){
        var propertyTableArray = getTableData("#hausmax_table");
        $('input#property-data').val(JSON.stringify(propertyTableArray));
    }

    export_file(url, target, is_post, formid);
});

var c_p_url     = '';
var c_p_target  = false;
var c_is_post   = false;
var c_formid    = '';

function export_file(url = '', target=false, is_post = false, formid = ''){
    $('#confirm-password-modal').modal('show');
    $('#confirm-password-modal').find('input[name="password"]').val('').focus();
    c_p_url     = url;
    c_p_target  = target;
    c_is_post   = is_post
    c_formid    = formid
}

$('#confirm-password-form').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var formData = new FormData($(this)[0]);
    var url = $(this).attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            if (result.status) {
                $('#confirm-password-modal').modal('hide');
                if(c_is_post){
                    $('#'+c_formid).submit();
                }else{
                    open_url(c_p_url, c_p_target);
                }
            }else{
              showFlash($('#confirm-password-error'), result.message, 'error');
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            showFlash($('#confirm-password-error'), 'Somthing want wrong!', 'error');
        }
    });
}));

function open_url(url, target = false){
    if(target){
        window.open(url,'_blank');
    }else{
        window.location.href = url;
    }
}
/*-------------JS FOR EXPORT CONFIRM PASSWORD - END---------*/