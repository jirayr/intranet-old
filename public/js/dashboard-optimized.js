var load_title; var load_table; var load_url; var table_obj;

function loadDashboardCounter(){
  $.ajax({
        url: url_dashboard_counter,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {

        },
        success: function(result) {

          console.log('Counter script load time : '+result.script_load_time+' second');

          $('.object-to-be-distributed-total').text(result.object_to_be_distributed);
          $('.invoice-approval-total').text(result.invoice_approval);
          $('.contract-total').text(result.contract_release);
          $('.approval-building-insurance-total').text(result.approval_building_insurance);
          $('.release-liability-total').text(result.release_liability);
          $('.pending-invoice-total').text(result.pending_invoice);
          $('.not-release-invoice-total').text(result.not_release_invoice);
          $('.property-insurance-total').text(result.property_insurance);
          $('.property-management-approval-total').text(result.property_management_approval);
          $('.commission-release-total').text(result.commission_release);
          $('.purchase-approval-total').text(result.purchase_approval);
          $('.sales-clearance-total').text(result.sales_clearance);
          $('.loi-purchase-total').text(result.status_loi);
          $('.funding-request-total').text(result.funding_request);
          $('.rental-overview-total').text(result.rental_overview);
          $('.default-payer-total').html(result.default_payer);
          $('.bank-finance-total').text(result.bank_finance);
          $('.insurance1-total').text(result.insurance1);
          $('.insurance0-total').text(result.insurance0);
          $('.house-management-cost-total').text(result.house_management_cost);
          $('.reminder-total').text(result.reminder);
          $('.expired-rental-agreement-total').text(result.expired_rental_agreement);
          $('.vacancy-total').text(result.vacancy);
          $('.expiring-leases-total').text(result.expiring_leases);
          $('.recommendation-total').text(result.recommendation);
          $('.diff_rent-total').text(result.diff_rent_total);
          $('.release-invoice-total').text(result.release_invoice);
          $('.release-contract-total').text(result.release_contract);
          $('.open-invoice-total').text(result.open_invoice);
          
          $('.new_open_invoice_total').text(result.open_invoice_new);
          $('.new_request_invoice_total').text(result.request_invoice_new);
          $('.new_release_invoice_total').text(result.release_invoice_new);
          $('.new_pending_invoice_total').text(result.pending_invoice_new);
          $('.not_release_invoice_total').text(result.not_release_invoice_new);
        },
        error: function(error) {

        }
    });
}

function loadDashboardCounter1(){
  $.ajax({
        url: url_dashboard_counter1,
        type: 'GET',
        dataType: 'json',
        beforeSend: function() {

        },
        success: function(result) {
          $('.real-estate-observe-total').text(result.real_estate_observe_total);
          $('.rental-activity-total-1').text(result.neue_mv);
          $('.rental-activity-total-2').text(result.neue_vl);
          $('.existing-property-total').text(result.rented_out);
        },
        error: function(error) {

        }
    });
}

function loadTable(reload = false){

	if(!reload){
		$('#table-data-modal').find('.modal-title').text(load_title);
		$('#table-data-modal').modal('show');
	}

	$.ajax({
      url: load_url,
      type: 'GET',
      beforeSend: function() {
      	// if(!reload){
      		$('#table-data-modal').find('.modal-body').html('<div class="loading"></div>');
      	// }
      },
      success: function(result) {
        
        if(load_table == 'table-property-release-invoice'){
      	   $('#table-data-modal').find('.modal-body').html(result.html);
        }else{
            $('#table-data-modal').find('.modal-body').html(result);
        }
        
        if(load_title.trim()=="Liquiplanung")
          {
            $('.liquiplanung-data').each(function(){
              str = $(this).text();
              if(str.includes('-'))
              {
                $(this).addClass('text-red');
              }
            })            
          }
        if(load_table){
        	var tableElement = $('#table-data-modal').find('#'+load_table);
        	var tableSetting = tableDetail(load_table);

          if(load_table=="property-insurance-table"){
            tableElement = ".property-insurance-table";
            var autocomplete_element = $('#table-data-modal').find('.angebote-section-comment');
            makeAutoComplete(autocomplete_element);
          }

          if(load_table=="table-approval-building-insurance"){
            tableElement = ".table-approval-building-insurance";
            var autocomplete_element = $('#table-data-modal').find('.property-comment');
            makeAutoComplete(autocomplete_element);
          }

          if(load_table=="table-release-liability"){
            tableElement = ".table-release-liability";
            var autocomplete_element = $('#table-data-modal').find('.property-comment');
            makeAutoComplete(autocomplete_element);
          }

          if(load_table=="table-property-management-approval"){
            tableElement = ".table-property-management-approval";
            var autocomplete_element = $('#table-data-modal').find('.property-comment');
            makeAutoComplete(autocomplete_element);
          }

          
          



          var page_length = (typeof tableSetting['length'] !== 'undefined' && tableSetting['length'] != 0 && tableSetting['length'] != '') ? tableSetting['length'] : 10;

        	if(typeof tableSetting['column'] !== 'undefined' && tableSetting['column'].length){
        		table_obj = makeDatatable(tableElement,tableSetting['column'], tableSetting['order'], tableSetting['position'], page_length);
        	}else{
        		table_obj = makeDatatable(tableElement);
        	}
        }

        if ($('.contract-comment').length)
            $('.contract-comment').editable();
        if ($('.invoice-comment').length)
          $('.invoice-comment').editable();
        if ($('.angebote-comment').length)
          $('.angebote-comment').editable();

        if($('.long-text').length){
          // new showHideText('.long-text');
          new showHideText('.long-text', {
              charQty     : 150,
              ellipseText : "...",
              moreText    : "More",
              lessText    : " Less"
          });
        }
      },
      error: function(error) {
      	$('#table-data-modal').find('.modal-body').html('<p class="text-danger">Somthing Want Wrong!</p>');
      }
  });
}

function tableDetail(table){
	var data = [];

	if(table == 'table-property-invoice'){
			data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date-time" },null,null,null,null];
			data['position'] = 9;
			data['order'] = 'desc';
	}else if(table == 'table-contract-release'){
		data['column'] = [null,null,null,{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "new-date" }, { "type": "new-date" }, { "type": "new-date" },null,null,{ "type": "new-date-time" },null,null,null,null,null];
		data['position'] = 10;
		data['order'] = 'desc';
	}else if(table == 'table-approval-building-insurance' || table == 'table-release-liability'){
    data['column'] = [null,null,{ "type": "numeric-comma" },null,null,null,null,null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'list-properties-1'){
    data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'table-property-invoice2' || table == 'table-property-invoice1'){
    data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date" },null,null,null];
    data['position'] = 9;
    data['order'] = 'desc';
  }else if(table == 'property-insurance-table'){
    data['column'] = [null,null,null,{ "type": "numeric-comma" },null,null,null,{"type": "new-date-time"},null,null,null,null,null,null, null, null];
    data['position'] = 7;
    data['order'] = 'desc';
  }else if(table == 'table-property-management-approval'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null,null,null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'property-provision-table'){
    data['column'] = [null,null,null,{ "type": "numeric-comma", "class": "text-right" },null,{ "type": "numeric-comma", "class": "text-right" }, { "type": "numeric-comma", "class": "text-right" },{ "type": "numeric-comma", "class": "text-right" },{ "type": "numeric-comma", "class": "text-right" },null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'added-month'){
    data['column'] = [null,null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
    data['length'] = 50;
  }else if(table == 'list-statusloi'){
    data['column'] = [null, { "type": "numeric-comma" }, { "type": "new-date" }];
    data['position'] = 2;
    data['order'] = 'desc';
  }else if(table == 'dashboard-table-2'){
    data['column'] = [null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'rental-overview-table'){
    data['column'] = [null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'property-default-payer-table'){
    data['column'] = [null,null,null,null,{ "type": "new-date-time" },null,null,null,null,{ "type": "new-date-time" },null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 4;
    data['order'] = 'asc';
  }else if(table == 'oplist-table'){
    data['column'] = [null,null,null,null,{ "type": "new-date-time" },null,null,null,null,null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "new-date" },null,null];
    data['position'] = 4;
    data['order'] = 'asc';
  }
  else if(table == 'bank-properties'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'financing-per-bank-table'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 1;
    data['order'] = 'asc';
  }else if(table == 'insurance-table1' || table == 'insurance-table0'){
    data['column'] = [null,{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 2;
    data['order'] = 'desc';
  }else if(table == 'hausmax_table'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 2;
    data['order'] = 'desc';
  }else if(table == 'mahnung-properties'){
    data['column'] = [null,null,{ "type": "numeric-comma" }];
    data['position'] = 2;
    data['order'] = 'desc';
  }else if(table == 'expired-rental-agreement-table'){
    data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null];
    data['position'] = 4;
    data['order'] = 'asc';
  }else if(table == 'tenant-table4'){
    data['column'] = [{"orderable": false},null,null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null,null];
    data['position'] = 5;
    data['order'] = 'desc';
  }else if(table == 'rental_activity_table'){
    data['column'] = [null,null,null,null,{ "type": "numeric-comma" },null,null,null,null,null,null,null,null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'expiring_leases_table'){
    data['column'] = [null,null,null,null,{ "type": "new-date" },{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },null,null];
    data['position'] = 5;
    data['order'] = 'asc';
  }else if(table == 'expiring-building-insurance-table' || table == 'expiring-liability-insurance-table'){
    data['column'] = [null,null,null,null,{ "type": "numeric-comma" },{ "type": "new-date" },null,{ "type": "numeric-comma" }];
    data['position'] = 5;
    data['order'] = 'asc';
  }else if(table == 'vacancy-per-object-table'){
    data['column'] = [null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }];
    data['position'] = 4;
    data['order'] = 'desc';
  }else if(table == 'recommendation-table'){
    data['column'] = [null,null,null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" }, { "type": "numeric-comma" }, { "type": "numeric-comma" }, { "type": "numeric-comma" }, null, null, { "type": "new-date" }, null, null, null, null];
    data['position'] = 14;
    data['order'] = 'desc';
  }else if(table == 'standard-land-value-table'){
    data['column'] = [null,null,null,{ "type": "numeric-comma" }, { "type": "numeric-comma" }];
    data['position'] = 4;
    data['order'] = 'desc';
  }else if(table == 'bka-table'){
    data['column'] = [null,null,null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null,{ "type": "new-date" },{ "type": "numeric-comma" }, { "type": "new-date" },null];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'property-mail-table'){
    data['column'] = [null,null,null,null,null,null,{ "type": "new-date-time" },null];
    data['position'] = 4;
    data['order'] = 'desc';
  }else if(table == 'liquiplanung-table'){
    data['column'] = [null,null,null,{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "numeric-comma" },{ "type": "new-date" },{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }else if(table == 'table-property-release-invoice'){
    data['column'] = [null,null,null,{ "type": "new-date" },{ "type": "numeric-comma" },{ "type": "new-date-time" }, null, null];
    data['position'] = 5;
    data['order'] = 'desc';
  }else if(table == 'table-utility-prices'){
    data['column'] = [null,null,null,null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" },null,{ "type": "numeric-comma" }];
    data['position'] = 0;
    data['order'] = 'asc';
  }
  else if(table == 'table-property-open-invoice'){
    data['column'] = [null,null,{ "type": "new-date" },{ "type": "numeric-comma" },null,null,null,null,{ "type": "new-date" },null,null,null,null,null];
    data['position'] = 8;
    data['order'] = 'desc';
  }
  
  return data;
}

function drawchart(cat,mv,vl,sum){
    Highcharts.chart('high-container', {
      chart: {
          type: 'column'
      },
      title: {
          text: 'Mietverträge MV & VL'
      },
      xAxis: {
          categories: cat,
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
            text: 'Miete  p.a.'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
           '<td style="padding:0"><b>{point.y:.1f} €</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0
          }
      },
      series: [{
          name: 'MV',
          data: mv

      }, {
          name: 'VL',
          data: vl
      }, {
          name: 'Gesamt',
          data: sum
      }]
    });
}

function drawchart2(container,cat,mv,vl){
  Highcharts.chart(container, {
      chart: {
          type: 'column'
      },
      title: {
          text: 'Mietverträge & Verlängerungen'
      },
      xAxis: {
          categories: cat,
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
            text: 'Anzahl'
          },
          stackLabels: {
              enabled: true,
              style: {
                  fontWeight: 'bold',
                  color: ( // theme
                      Highcharts.defaultOptions.title.style &&
                      Highcharts.defaultOptions.title.style.color
                  ) || 'gray'
              }
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
         '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                  enabled: true
              }
          }
      },
      series: [{
          name: 'MV',
          data: mv
      },
      {
          name: 'VL',
          data: vl
      }]
  });
}

function drawchart3(container,cat,mv,vl){
    Highcharts.chart(container, {
        chart: {
            type: 'column'
        },
      title: {
          text: 'MIETERÜBERSICHT'
      },
        xAxis: {
            categories: cat,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
              text: 'Anzahl'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
             '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
      plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0,
            dataLabels: {
                  enabled: true
              }
          }
      },
        series: [{
            name: 'Miete netto p.m.',
            data: mv
        },
        {
            name: 'Anzahl',
            data: vl
        }]
    });
}

function neue_mv(){
  var m = $('.pa-month').val();
  var y = $('.pa-year').val();
  var t = $('.type-selection').val();

  console.log('t => '+t);

  $('#neue_mv_modal').find('#neue_mv_html').html('<div class="loading"></div>');

  var url = url_monthyearassetmanager+"?month="+m+"&year="+y+"&type="+t;
  $.ajax({
      url: url,
  }).done(function (data) {
      $('#neue_mv_html').html(data.table);
      // $('#high-container').hide();
      // $('#high-container2').hide();
      // $('.asset-manager-property-list').html(data.table);
      // $('.asset-manager-property-list-total').html(data.subtable);

      var columns = [
          null,
          { "type": "numeric-comma" },
          { "type": "numeric-comma" },
          { "type": "numeric-comma" },
          { "type": "numeric-comma" },
          { "type": "numeric-comma" },
          { "type": "numeric-comma" },
          { "type": "numeric-comma" },
          { "type": "numeric-comma" }
      ];
      makeDatatable($('#added-month-asset'), columns, "asc", 0, 25);

      drawchart(data.cat,data.mv,data.vl,data.sum);
      drawchart2('high-container2',data.m_arr,data.m_arr1,data.m_arr2);
  });
}

function getcategorypropertieslist2(category,name){
    $('.cate-pro-div').html("");
    $('#cate-pro-listing').modal();
    var url = url_getcategoryproperty3+"?category="+category+"&name="+name;
    $.ajax({
       url: url,
    }).done(function (data) {
        $('.cate-pro-div').html(data);
        if(category == ''){
            var columns = [
              null,
              null,
              null,
              null,
              { "type": "numeric-comma" },
              null,
              null,
              null,
          ];
          makeDatatable($('.name-rent-table'), columns,'desc',3);
        }else{
            var columns = [
              null,
              null,
              null,
              { "type": "numeric-comma" },
              { "type": "numeric-comma" },
              null,
              null,
              null,
          ];
          makeDatatable($('.category-table'), columns,'desc',2);
        }
   });
}

function getcategorypropertieslist3(category){
    $('.cate-pro-div').html("");
    $('#cate-pro-listing').modal();
    var url = url_getcategoryproperty3+"?category2="+category;
    $.ajax({
        url: url,
    }).done(function (data) {
       $('.cate-pro-div').html(data);
        var columns = [
            null,
            null,
        ];
        makeDatatable($('.category-table'), columns,'desc',0);
    });

}

function getassetmanagerprovision(asset_m_id){

    m = $('.pp-month').val();
    y = $('.pp-year').val();

    var url = url_get_property_provision+"?type=4&month="+m+"&year="+y+'&asset_m_id='+asset_m_id;

    if(asset_m_id != 0){
      $('.list-data').html('<div class="loading"></div>');
    }else{
      $('.property-provision-div-4').html('<div class="loading"></div>');
    }

    $.ajax({
        url: url,
    }).done(function (data) {
        if(asset_m_id != 0){

            $('.list-data').html(data);

            var columns = [
              null,
              null,
              null,
              { "type": "numeric-comma", "class": "text-right" },
              null,
              { "type": "numeric-comma", "class": "text-right" },
              { "type": "numeric-comma", "class": "text-right" }
          ];
          makeDatatable($('#property-provision-list'), columns);

        }else{
              $('.property-provision-div-4').html(data);
              var columns = [
                  null,
                  null,
              ];
              makeDatatable($('#property-provision-table-4'), columns);
        }
    });
}

function loadAllStatusLoiByUser(user_id) {
    var url = url_getStatusLoiByUserIdDashboard+"?id="+user_id;
    $.ajax({
       url: url,
    }).done(function (data) {
       $('#load_all_status_loi_by_user_content').html('');
       $('#load_all_status_loi_by_user_content').html(data);
       $('#load_all_status_loi_by_user').modal('show');
    });
}

function removeDeactiveUser(){
  if(in_active_user_arr.length > 0){

    if($('.asset_manager').length){
      $('.asset_manager').each(function(){
        var $this = $(this);
        var username = $.trim($(this).text());
        if(in_active_user_arr.indexOf(username) !== -1){
          $($this).html("");
        }
      });
    }

    if($('.custom_user').length){
      $('.custom_user').each(function(){
        var $this = $(this);
        var username = $.trim($(this).text());
        if(in_active_user_arr.indexOf(username) !== -1){
          $($this).html("");
        }
      });
    }
  }
}

$( document ).ready(function() {

    loadDashboardCounter();//load dashboard counter

    loadDashboardCounter1();//load counter for long script

    $(document).ajaxComplete(function() {
        removeDeactiveUser();
    });

    $('#modal-forward-to').find('select[name="user"]').select2();

    $('body').on('click', '.load-table', function() {
    	load_title = $(this).attr('data-title');
    	load_table = $(this).attr('data-table');
    	load_url = $(this).attr('data-url');

    	loadTable();
    });

    $('body').on('click', '.btn-paginate', function() {
      load_url = $(this).attr('data-url');
      loadTable(true);
    });

    $.ajax({
      url: url_get_sales_portal_count,
      type: 'GET',
      dataType: 'json',
      success: function(result) {
          $('#makler_userall').text(result.makler_userall);
          $('#activecountall').text(result.activecountall);
          $('#makler_user').text(result.makler_user);
          $('#activecount').text(result.activecount);
          $('#vmakler_userall').text(result.vmakler_userall);
          $('#vactivecountall').text(result.vactivecountall);
          $('#vmakler_user').text(result.vmakler_user);
          $('#vactivecount').text(result.vactivecount);
      },
      error: function(error) {

      }
    });

    $('.get-verkauf-user').click(function(){

        $('#user-list-popup').modal();
        $('.user-list').html("");
        var active= $(this).attr('data-active');

        $.ajax({
           url: url_getusers,
           data:{active:active,domain:1},
        }).done(function (data) {
           $('.user-list').html(data);
           makeDatatable($('.user-list table'));
        });
    });

    $('.get-vermietung-user').click(function(){
        $('#user-list-popup').modal();
        $('.user-list').html("");

        var active= $(this).attr('data-active');

        $.ajax({
           url: url_getusers,
           data:{active:active,domain:2},
        }).done(function (data) {
           $('.user-list').html(data);
           makeDatatable($('.user-list table'));
        });
    });
    $('#selectedYear').change(function(){
    loadGraph();
});
loadGraph();
function loadGraph()
{
    $.ajax({
        url: "/dashboard-optimize/get_barchart_data/"+$('#selectedYear').val(),

        type: 'GET',
        dataType: 'json',
        success: function(result) {
            var myChart = echarts.init(document.getElementById('basic-bar'));
            myChart.on('click', function (params) {
                // printing data name in console
                if("Mieteinnahmen pro Monat (€)" == params.seriesName && (params.seriesType=="bar" || params.seriesType=="line"))
                {
                var table = document.getElementById("classTable");


                $('#detail_graph tbody').empty();
                $('#classTable tbody').empty();
                $('#testTable tbody').empty();

                //or use :  var table = document.all.tableid;
                $('#classTable tbody').append('<tr></tr>');
                for(var i = 0 ; i <result.property_array[params.dataIndex].length; i++)
                {
                    if(result.property_array[params.dataIndex][i].data){
                    $('#classTable tr:last').after('<tr><td>'+result.property_array[params.dataIndex][i].data[0]+'</td><td>'+result.property_array[params.dataIndex][i].data[2]+'</td><td>'+result.property_array[params.dataIndex][i].data[3]+'</td><td>'+formatNumber(result.property_array[params.dataIndex][i].data[1])+'€</td></tr>');
                    }
                }


                $('#classTable tr:last').after('<tr><td>Total Rent</td><td></td><td></td><td>'+params.data+'</td></tr>');


                $('#testTable tbody').append('<tr></tr>');
                  var  a=   result.finArray[params.dataIndex]['data'];
                console.log(result.finArray[params.dataIndex]['data'].length);
                for(var i = 0 ; i < result.finArray[params.dataIndex]['data'].length; i++)
                {
                    // console. log(result.finarray[params.dataIndex][i]);
                    // if(result.finArray[params.dataIndex][i]){
                        $('#testTable tr:last').after('<tr><td>'+result.finArray[params.dataIndex]['data'][i][0]+'</td><td>'+formatNumber(result.finArray[params.dataIndex]['data'][i][1])+'€</td></tr>');
                    // }
                }


                    $('#detail_graph').modal('show');
                }
            });
            // specify chart configuration item and data
            var option = {
                // Setup grid
              grid: {
                 left: '1%',
                 right: '2%',
                 bottom: '3%',
                 containLabel: true
              },

              // Add Tooltip
              tooltip : {
                 trigger: 'axis'
              },
              legend: {
                x: 'left',
                 data:['Mieteinnahmen pro Monat (€)','NK netto p.m']
              },
              toolbox: {
                 show : true,
                 feature : {
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                 }
              },
              color: ["#2962FF", "#4fc3f7"],
              calculable : true,
              xAxis : [
                 {
                    type : 'category',
                    // data : ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sept','Okt','Nov','Dez']
                    data : result.displayMonth
                 }
              ],
              yAxis : [
                 {
                     type : 'value',
                     min : '0'
                 }
              ],
              series : [
                 {
                    name:'Mieteinnahmen pro Monat (€)',
                    type:'bar',
                    data: result.year_graph,
                    markPoint : {
                       data : [
                          {type : 'max', name: 'Max'},
                          {type : 'min', name: 'Min'}
                       ]
                    },
                    markLine : {
                       data : [
                          {type : 'average', name: 'Average'}
                       ]
                    }
                 },
                 {
                    name:'NK netto p.m',
                    type:'bar',
                    data: result.netto_array, // add yours
                    markPoint : {
                       data : [
                          {type : 'max', name: 'Max'},
                          {type : 'min', name: 'Min'}
                       ]
                    },
                    markLine : {
                       data : [
                          {type : 'average', name: 'Average'}
                       ]
                    }
                 }
              ]
            };

            // use configuration item and data specified to show chart
            myChart.setOption(option);
           //}

        },
        error: function(error) {

        }
      });
}
    function formatNumber(num) {
       return new Intl.NumberFormat('de-DE').format(num)
}

    // Property invoice js
    $('body').on('click', '.btn-delete-property-invoice', function() {
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        if (confirm('Are you sure want to delete this invoice?')) {
            var url = url_delete_property_invoice;
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                      loadTable(true);
                },
                error: function(error) {
                    alert('Somthing want wrong!');
                }
            });
        } else {
            return false;
        }
    });

    $('body').on('click','.invoice-release-request',function(){
       	// tr = $(this).closest('tr');
       	vacant_release_type= 'release2';
       	invoice_id= $(this).attr('data-id');
       	property_id= $(this).attr('data-property_id');
       	$('.invoice-release-comment').val("")
       	$('#invoice-release-property-modal').modal();
   	});

    $('body').on('click', '.invoice-release-request2', function() {
        vacant_release_type = $(this).attr('data-column');
        invoice_id = $(this).attr('data-id');
        property_id= $(this).attr('data-property_id');
        
        $('.invoice-release-comment2').val("")
        $('#invoice-release-property-modal2').modal();


        if(vacant_release_type=="release2")
        {
            $('.i-message').addClass('hidden');
            $('.i-message2').removeClass('hidden');
        }
        else{
            $('.i-message').removeClass('hidden');
            $('.i-message2').addClass('hidden');
        }

    });

   	$('body').on('click','.invoice-release-submit',function(){
       	comment = $('.invoice-release-comment').val();
       	$.ajax({
            type : 'POST',
            url : url_property_invoicereleaseprocedure,
            data : {
            		id:invoice_id,
            		step:vacant_release_type,
            		_token : _token,
            		comment:comment,
            		property_id:property_id
            	},
            success : function (data) {
                 // tr.remove();
                 loadTable(true);
            }
        });
    });
    $('body').on('click', '.invoice-release-request-am', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        property_id = $(this).attr('data-property_id');
        $('.invoice-release-comment-am').val("")
        $('#invoice-release-property-modal-am').modal();

    });
    $('body').on('click', '.invoice-release-submit-am', function() {
        comment = $('.invoice-release-comment-am').val();
        
        $.ajax({
            type: 'POST',
            url: url_property_invoicereleaseprocedure,
            data: {
                id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
            },
            success: function(data) {
              loadTable(true);
            }
        });
    });

    $('body').on('click','.release-op-list',function(){
        $this = $(this);
        $.ajax({
            type : 'POST',
            url : url_releaseoplist,
            data : {
                id:$(this).attr('data-id'),
                step:$(this).attr('data-type'),
                _token : _token,
              },
            success : function (data) {
                 $this.removeClass('release-op-list');
                 $this.removeClass('btn-primary');
                 $this.addClass('btn-success');
                 // $this.html('Freigegeben');
            }
        });
    });

    

    var not_release_id = 0;
    $('body').on('click','.mark-as-not-release',function(){
        not_release_id = $(this).attr('data-id');
        $('#not-release-modal').modal();
    });

    $('body').on('click','.invoice-not-release-submit',function(){
        comment = $('.invoice-not-release-comment').val();
        $.ajax({
            type : 'POST',
            url : url_property_invoicenotrelease,
            data : {id:not_release_id, _token : _token, comment:comment},
            success : function (data) {
                loadTable(true);
            }
        });
    });

    $('body').on('click','.mark-as-pending',function(){
      not_release_id = $(this).attr('data-id');
      $('#pending-modal').modal();
    });

    $('body').on('click','.invoice-pending-submit',function(){
        comment = $('.invoice-pending-comment').val();
        $.ajax({
            type : 'POST',
            url : url_property_invoicemarkpending,
            data : {id:not_release_id, _token : _token,comment:comment},
            success : function (data) {
              loadTable(true);
            }
        });
    });

    /*$('body').on('click', '.multiple-invoice-release-request', function() {
     	var selcted_id_invoice = "";
     	arr = [];
     	selcted_id_invoice = "";

      	$('.multiple-invoice-release-comment').val("");

     	$('.invoice-checkbox').each(function(){
        	if($(this).is(':checked'))
           		arr.push($(this).attr('data-id'));
     	});

     	selcted_id_invoice = arr.toString();

     	// console.log(selcted_id_invoice);
     	if(selcted_id_invoice=="" || selcted_id_invoice==null){
        	alert("Bitte mindestens eine Rechnung auswählen")
     	}else{
        	$('.multiple-invoice-release-comment').val("");
        	$('#multiple-invoice-release-property-modal').modal();
     	}
  	});*/

  	$('body').on('click', '.multiple-invoice-release-submit', function() {
  		var arr = [];
      	var comment = $('.multiple-invoice-release-comment').val();
      	var selcted_id_invoice = "";

     	$('.invoice-checkbox').each(function(){
        	if($(this).is(':checked'))
           		arr.push($(this).attr('data-id'));
     	});


     	selcted_id_invoice = arr.toString();
      	$('.invoice-checkbox').each(function(){
          if($(this).is(':checked'))
              $(this).closest('tr').remove();
      	});

      	$.ajax({
          	type: 'POST',
          	url: url_property_invoicereleaseprocedure,
          	data: {
              	id: selcted_id_invoice,
              	step: "release2",
              	_token: _token,
              	comment: comment,
           	},
          	success: function(data) {
              	loadTable(true);
                $('.multiple-invoice-release-comment').val("");
          	}
      	});
  	});

    $('body').on('click', '.asset_manager', function() {
      var inv_property_id = $(this).attr('data-property-id');
      var content = $(this).attr('data-content');
      var subject = $(this).attr('data-subject');
      var mail_type = $(this).attr('data-title');

      var id = $(this).attr('data-id');
      var section = $(this).attr('data-section');

      subject = (typeof subject !== 'undefined') ? subject : '';
      mail_type = (typeof mail_type !== 'undefined') ? mail_type : '';

      id = (typeof id !== 'undefined') ? id : '';
      section = (typeof section !== 'undefined') ? section : '';

      $('#modal_invoice_sendmail_to_am').find('input[name="property_id"]').val(inv_property_id);
      $('#modal_invoice_sendmail_to_am').find('input[name="content"]').val(content);
      $('#modal_invoice_sendmail_to_am').find('input[name="subject"]').val(subject);
      $('#modal_invoice_sendmail_to_am').find('input[name="mail_type"]').val(mail_type);

      $('#modal_invoice_sendmail_to_am').find('input[name="id"]').val(id);
      $('#modal_invoice_sendmail_to_am').find('input[name="section"]').val(section);

      $('#modal_invoice_sendmail_to_am').modal('show');
    });

    var reload_custom_user = 0;
    $('body').on('click', '.custom_user', function() {
      var property_id = $(this).attr('data-property-id');
      var user_id = $(this).attr('data-user-id');
      var subject = $(this).attr('data-subject');
      var content = $(this).attr('data-content');
      var reload = $(this).attr('data-reload');
      var email = $(this).attr('data-email');
      var mail_type = $(this).attr('data-title');

      var id = $(this).attr('data-id');
      var section = $(this).attr('data-section');

      reload_custom_user = (typeof reload !== 'undefined' && reload == "1") ? 1 : 0;
      email = (typeof email !== 'undefined') ? email : "";
      mail_type = (typeof mail_type !== 'undefined') ? mail_type : "";

      id = (typeof id !== 'undefined') ? id : '';
      section = (typeof section !== 'undefined') ? section : '';

      if(section){
        reload_custom_user = 1
      }

      $('#modal_sendmail_to_custom_user').find('input[name="property_id"]').val(property_id);
      $('#modal_sendmail_to_custom_user').find('input[name="user_id"]').val(user_id);
      $('#modal_sendmail_to_custom_user').find('input[name="subject"]').val(subject);
      $('#modal_sendmail_to_custom_user').find('input[name="content"]').val(content);
      $('#modal_sendmail_to_custom_user').find('input[name="email"]').val(email);
      $('#modal_sendmail_to_custom_user').find('input[name="mail_type"]').val(mail_type);

      $('#modal_sendmail_to_custom_user').find('input[name="id"]').val(id);
      $('#modal_sendmail_to_custom_user').find('input[name="section"]').val(section);

      $('#modal_sendmail_to_custom_user').modal('show');
    });

  	// Property Contract js
  	var contract_vacant_release_type;
    var contract_vacant_id;

    $('body').on('click', '.contract-release-request', function() {
        contract_vacant_release_type = $(this).attr('data-column');
        contract_vacant_id = $(this).attr('data-id');
        $('.contract-release-comment').val("")
        $('#contract-release-property-modal').modal();
    });

    $('body').on('click', '.contract-release-submit', function() {
        comment = $('.contract-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_contractreleaseprocedure,
            data: {
                id: contract_vacant_id,
                step: contract_vacant_release_type,
                _token: _token,
                comment: comment,
            },
            success: function(data) {
                loadTable(true);
            }
        });
    });

    var contract_not_release_id;
    var contract_notrelease_property_id;
    $('body').on('click', '.contract-mark-as-not-release', function() {
        contract_not_release_id = $(this).attr('data-id');
        contract_notrelease_property_id = $(this).attr('data-property-id');
        $('#contracts_mark_as_not_release').modal('show');
    });

    $('#form_contracts_mark_as_not_release').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_not_release_id,
            'comment': $(this).find('#input_contracts_mark_as_not_release').val(),
            'property_id': contract_notrelease_property_id,
            '_token': _token
        }

        $.ajax({
            url: url_contract_mark_as_notrelease,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_not_release').modal('hide');
                    $($this)[0].reset();
                    loadTable(true);
                }else{
                    alert(result.message);
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                alert('Somthing want wrong!');
            }
        });
    });

    var contract_pending_id;
    var contract_pending_property_id;
    $('body').on('click', '.contract-mark-as-pending', function() {
        contract_pending_id = $(this).attr('data-id');
        contract_pending_property_id = $(this).attr('data-property-id');
        $('#contracts_mark_as_pending').modal('show');
    });

    $('#form_contract_mark_as_pending').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_pending_id,
            'comment': $(this).find('#input_contracts_mark_as_pending').val(),
            'property_id': contract_pending_property_id,
            '_token': _token
        }
        $.ajax({
            url: url_contract_mark_as_pending,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_pending').modal('hide');
                    $($this)[0].reset();
                    loadTable(true);
                }else{
                  alert(result.message);
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                alert('Somthing want wrong!');
            }
        });
    });

    // Insurance

    $('body').on('click', '.versicherunge-checkbox-is-approved', function() {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: url_update_versicherung_approved_status,
            data: {
                pk: pk,
                value: v,
                id: id,
                _token: _token
            },
            success: function(data) {}
        });
    });

    $('body').on('click', '.falk-checkbox-is-checked', function() {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: url_update_falk__status,
            data: {
                pk: pk,
                value: v,
                id: id,
                _token: _token
            },
            success: function(data) {}
        });
    });

    $('body').on('click','.request-insurance-button',function(){
      vacant_release_type= $(this).attr('data-type');
      vacant_id= $(this).attr('data-id');
      $('.insurance-release-comment').val("")
      $('#insurance-release-property-modal').modal();
    });
    $('body').on('click','.btn-not-release-management',function(){
       vacant_id= $(this).attr('data-id');
       $('.not-release-management-comment').val("")
       $('#not-release-management-modal').modal();
    });
    
    
    $('body').on('click', '.not-release-management-submit', function() {
        // property_id = vacant_id;
        comment = $('.not-release-management-comment').val();

        $.ajax({
            type: 'POST',
            url: url_propertyManagementNotRelease,
            data: {
                id: vacant_id,
                _token: _token,
                comment: comment
            },
            success: function(data) {
                loadTable(true);
                loadDashboardCounter();
            }
        });
    });


    $('body').on('click', '.insurance-release-submit', function() {
        property_id = vacant_id;
        comment = $('.insurance-release-comment').val();

        $.ajax({
            type: 'POST',
            url: url_property_addmaillog,
            data: {
                property_id: property_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment
            },
            success: function(data) {
                loadTable(true);
                loadDashboardCounter();
            }
        });
    });

    // ZU VERTEILENDE OBJEKTE JS
    $('body').on('change', '.asset-manager', function() {
        var am_id = $(this).val();
        var property_id = $(this).attr('data-property');


        var url = url_setAssestManagerOnProperty;
        url = url.replace(':property_id', property_id);
        url = url.replace(':am_id', am_id);

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result){
                if(result.status == false){
                    alert(result.message);
                }
            },
            error: function(error){
                alert('Somthing want wrong for change AM!');
            }
        });
    });

    $('body').on('click','.get-rents',function(){
        $('.list-data-rent').html("");
        $('.vlist-data-rent').html("");

        var id          = $(this).attr('data-value');
        var property_id = $(this).attr('data-property');
        var type        = $(this).attr('data-type');

        if(type == "r")
            $('#rent-list').modal();
        else
            $('#vacant-list').modal();

        var url = url_assetmanagement+"?asset_manager="+id+"&property_id="+property_id+"&type="+type;

        $.ajax({
            url: url,
        }).done(function (data) {
            if(type=="r"){
                $('.list-data-rent').html(data);
            }else{
                $('.vlist-data-rent').html(data);
                var columns = [
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" }
                ];
                makeDatatable($('#list-rent'), columns);
            }
       });
    });

    // Property insurance tab
    $('body').on('change', '.am_falk_status', function() {
        var field = $(this).attr('data-field');
        var status = ($(this).prop('checked')) ? 1 : 0;
        var url = $(this).attr('data-url');

        $.ajax({
            url: url,
            type: 'POST',
            data:{
                'field' : field,
                'status' : status
            },
            dataType: 'json',
            success: function(result) {
                if(!result.status){
                    alert(result.message);
                }
            },
            error: function(error){
              alert('Somthing want wrong!');
            }
        });

    });

    $('body').on('click', '.delete-insurance-tab-detail', function() {
        var tab_id = $(this).closest('table').attr('data-tabid');
        var url = $(this).attr('data-url');

        if(confirm('Are you sure want to delete this record?')){
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if(result.status){
                        loadTable(true);
                    }else{
                      alert(result.message);
                    }
                },
                error: function(error){
                    alert('Somthing want wrong!');
                }
            });
        }
        return false;
    });

    $('body').on('click', '.property-insurance-release-request', function() {

      var length = 1; //$(this).closest('.row').find('table').find('[data-field="falk_status"]:checked').length;
      var msg =  "Bitte Checkbox Falk auswählen!";
      if(length > 0){
          // $this = $(this).closest('.row').find('table').find('[data-field="falk_status"]:checked');
          vacant_release_type = $(this).attr('data-column');
          vacant_id = $(this).attr('data-id');
          // alert(vacant_release_type);
          $('.property-insurance-comment').val("");
          $('#property-insurance-release-property-modal').modal();
      }else{
          alert(msg); return false;
      }
    });

    $('body').on('click', '.property-insurance-submit', function() {
      var am_id = fk_id = 0;
          // fk_id = $this.attr('data-id');

      var comment = $('.property-insurance-comment').val();

      $.ajax({
        type: 'POST',
        url: url_property_dealreleaseprocedure,
        data: {
            id: vacant_id,
            step: vacant_release_type,
            _token: _token,
            am_id:am_id,
            fk_id:fk_id,
            comment: comment,
        },
        success: function(data) {
            loadTable(true);
            loadDashboardCounter();
        }
      });
    });

    var not_release_id = 0;
    $('body').on('click','.deal-mark-as-not-release',function(){
        not_release_id = $(this).attr('data-id');
        $('#deal-not-release-modal').modal();
        $('.deal-not-release-comment').val("")
    });

    $('body').on('click','.btn-not-release-insurance',function(){
        not_release_id = $(this).attr('data-id');
        $('#insurance-not-release-modal').modal();
        $('.insurance-not-release-comment').val("")
    });

    $('body').on('click','.insurance-not-release-submit',function(){
        comment = $('.insurance-not-release-comment').val();
        $.ajax({
              type : 'POST',
              url : url_insurance_mark_as_notrelease,
              data : {id:not_release_id, _token : _token, comment:comment},
              success : function (data) {
                  loadTable(true);
                  loadDashboardCounter();
              }
          });
    });

    $('body').on('click','.btn-not-release-am-insurance',function(){
        not_release_id = $(this).attr('data-id');
        table_id = $(this).closest('table').attr('data-tabid');
        $('#insurance-not-release-am-modal').modal();
        $('.insurance-not-release-am-comment').val("")
    });

    $('body').on('click','.insurance-not-release-am-submit',function(){
        comment = $('.insurance-not-release-am-comment').val();
        $.ajax({
              type : 'POST',
              url : url_insurance_mark_as_notrelease_am,
              data : {id:not_release_id, _token : _token, comment:comment},
              success : function (data) {
                  loadTable(true);
                  loadDashboardCounter();
              }
          });
    });

    $('body').on('click','.btn-pending-insurance',function(){
        pending_id = $(this).attr('data-id');
        table_id = $(this).closest('table').attr('data-tabid');
        $('#insurance-pending-modal').modal();
        $('.insurance-pending-comment').val("")
    });

    $('body').on('click','.insurance-pending-submit',function(){
        comment = $('.insurance-pending-comment').val();
        $.ajax({
              type : 'POST',
              url : url_insurance_mark_as_pending,
              data : {id:pending_id, _token : _token, comment:comment},
              success : function (data) {
                  loadTable(true);
                  loadDashboardCounter();
              }
          });
    });
    

    $('body').on('click','.deal-not-release-submit',function(){
        comment = $('.deal-not-release-comment').val();
        $.ajax({
              type : 'POST',
              url : url_deal_mark_as_notrelease,
              data : {id:not_release_id, _token : _token, comment:comment},
              success : function (data) {
                  loadTable(true);
                  loadDashboardCounter();
              }
          });
    });
    
    

    $('body').on('click', '.change-property-manage-status', function() {
        var v = 0;
        if ($(this).is(':checked')) v = 1;
        var data = {
            property_id: $(this).attr('data-property_id'),
            pk: $(this).attr('data-column'),
            value: v,
            _token: _token,
            id: $(this).attr('data-id')
        };

        var $this = $(this);
        $.ajax({
            url: url_property_management_update,
            type: "post",
            data: data,
            success: function(response) {
            }
        });
    });



    $('body').on('click','.provision-release-request',function(){
      vacant_release_type= $(this).attr('data-column');
      vacant_id= $(this).attr('data-id');
      property_id= $(this).attr('data-property_id');
      $('.provision-release-comment').val("")
      $('#provision-release-property-modal').modal();
    });

    $('body').on('click','.provision-release-submit',function(){
        comment = $('.provision-release-comment').val();
        $.ajax({
            type : 'POST',
            url : url_property_provisionreleaseprocedure,
            data : {tenant_id:vacant_id,step:vacant_release_type,  _token : _token, comment:comment,property_id:property_id},
            success : function (data) {
                loadTable(true);
            }
        });
    });

    $('body').on('change','.change-select',function(){
        var m = $('.pm-month').val();
        var y = $('.pm-year').val();
        load_url = url_monthyearmanager+"?month="+m+"&year="+y;

        loadTable(true);
    });

    $('body').on('click','.get-manager-property',function(){
        $('.list-data').html("");
        id= $(this).attr('data-id');
        m = $('.pm-month').val();
        y = $('.pm-year').val();
        status= $(this).attr('data-status');

        var url = url_getmanagerproperty+"?id="+id+"&month="+m+"&year="+y+"&status="+status;
        $.ajax({
            url: url,
        }).done(function (data) {
            $('.list-data').html(data);

            if(status == 12){
                var columns = [
                    null,
                    null,
                    { "type": "new-date" },
                    null,
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    null
                ];
                makeDatatable($('#list-properties'), columns, "asc", 2);
            }else if (status==14 || status==10) {
                var columns = [
                    null,
                    null,
                    { "type": "new-date" },
                    null,
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    null
                ];
                makeDatatable($('#list-properties'), columns, "asc", 2);
            }else{
                var columns = [
                    null,
                    null,
                    { "type": "new-date" },
                    { "type": "new-date" },
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    null
                ];
                makeDatatable($('#list-properties'), columns, "asc", 2);
            }
        });
    });

    $('body').on('change','#loi-month',function(){
      const month = $(this).val();
      var url = url_getloi;
      load_url = url.replace(":month", month);
      loadTable(true);
    });

    $('body').on('click','.checkbox-is-sent', function () {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if($(this).is(':checked'))
            v = 1;

        $.ajax({
            type : 'POST',
            url : url_statusloi_update+"/"+id,
            data : {
                pk : pk,
                value:v,
                _token : _token
            },
            success : function (data) {

            }
        });
    });

    $('body').on('click','.get-bank-list', function () {

        $('#bank-list').modal();
        property_id = $(this).attr('data-id');
        var url = url_getbanklist+"?id="+property_id;

        $.ajax({
          url: url,
        }).done(function (data) {
            $('.list-data-banks').html(data);
            var columns = [
                null,
                null,
                null,
                null,
                null,
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                null,
                null,
            ];
            makeDatatable($('#list-banks'), columns);
        });
    });

    $('body').on('click','.get-bank-finance-offers', function () {

        $('#bank-list').modal();
        property_id = $(this).attr('data-id');
        var url = url_bank_finance_offers_manual+"?id="+property_id;

        $.ajax({
            url: url,
        }).done(function (data) {
            $('.list-data-banks').html(data);
            var columns = [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ];
            makeDatatable($('#list-banks'), columns);
        });
    });
    $('body').on('click','.get-bank-email-logs', function () {

        $('#bank-list').modal();
        property_id = $(this).attr('data-id');
        var url = url_bank_finance_offers_automatic+"?id="+property_id;

        $.ajax({
            url: url,
        }).done(function (data) {
            $('.list-data-banks').html(data);
            var columns = [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ];
            makeDatatable($('#list-banks'), columns);
        });
    });

    $('body').on('click','.get-asset-property',function(){
        $('.list-data').html("");
        id = $(this).attr('data-value');
        $('#property-list').modal();

        $.ajax({
               url: url_assetmanagement+"?id="+id,
        }).done(function (data) {
            $('.list-data').html(data);
            var columns = [
                null,
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
            ];
            makeDatatable($('#list-properties'), columns);
       });
    });

    $('body').on('click','.get-category-properties',function(){
        var category = $(this).attr('data-category');
        var type = $(this).attr('data-type');
        var check = $(this).attr('data-rent-closed');
        // getcategorypropertieslist(category,type,check)

        $('.cate-pro-div').html("");
        $('#cate-pro-listing').modal();

        var url = url_get_rental_overview+"?category="+category+'&type='+type+'&rent_closed='+check;

        $.ajax({
            url: url,
        }).done(function (data) {
            $('.cate-pro-div').html(data);
            var columns = [
                null,
                null,
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                null,
                null,
            ];
            makeDatatable($('.category-table'), columns,'desc',3);
       });
    });

    $('body').on('change','.bchange-select',function(){
      var m = $('.bb-month').val();
      var y = $('.bb-year').val();
      load_url = url_get_default_payers+"?month="+m+"&year="+y+"&status=0";
      loadTable(true);
    });
    $('body').on('change','.oplistchange-select',function(){
      var m = $('.oplist-month').val();
      var y = $('.oplist-year').val();
      load_url = url_get_oplist+"?month="+m+"&year="+y+"&status=0";
      loadTable(true);
    });
    $('body').on('click','.show-all-release-invoice',function(){
      load_url = $(this).attr("data-url");
      loadTable(true);
    });

    

    $('body').on('click','#default_payer .hide-checkbox',function(){
        id = $(this).attr('data-id');
        var m = $('.bb-month').val();
        var y = $('.bb-year').val();

        load_url = url_get_default_payers+"?month="+m+"&year="+y+"&status="+id;
        loadTable(true);
    });

    $('body').on('click','.btn-export-to-excel',function(){
        var propertyTableArray = getTableData("#hausmax_table");
        $('input#property-data').val(JSON.stringify(propertyTableArray));
        $('#export-to-excel-form').submit();
    });

    $('body').on('click','.get-hausmaxx-data',function(){
        id = $(this).attr('data-id');
        t = encodeURIComponent($(this).html());

        $('#hausmax-list').find('.modal-title').html($(this).html());
        var url = url_get_hausmaxx_data+"?id="+id+'&title='+t;
        $.ajax({
            url: url,
        }).done(function (data) {
            $('#hausmax-list').modal();
            $('.hausmax-list-data').html(data);
            // setTimeout(function(){
                var columns = [
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    {"type": "new-date"},
                    {"type": "new-date"},
                    {"type": "new-date"}
                ];
                makeDatatable($('#hausmaxx_listdata_table'), columns);
            // },1000);
        });
    });


    $('body').on('click','.get-tenant-mahnung',function(){
        $('.get-tenant-mahnung-list-data').html("");
        id= $(this).attr('data-id');

        $.ajax({
            url: url_getMahnungTenant+"?id="+id,
        }).done(function (data) {
            $('.get-tenant-mahnung-list-data').html(data);
            var columns = [
                null,
                { "type": "numeric-comma" },
                { "type": "new-date" },
                { "type": "new-date" },
                { "type": "new-date" },
            ];
            makeDatatable($('#mahnung-tenants'), columns);
        });
    });

    $('body').on('click','.tenancy_item',function(){
      $('#tenant-detail-modal').modal();
        id = $(this).attr('data-id');
        var url = url_tenant_detail+"?id="+id;
        $.ajax({
               url: url,
        }).done(function (data) {
            $('.tenant-detail').html(data);
        });
    });

    $.ajax({
       url: url_mailchimp,
    }).done(function (data) {
       $('.newsletter-subscriptions-total').html(data);
    });

    $.ajax({
       url: url_getSocialFollowers,
       type: 'GET',
       dataType: 'json',
       success: function(result){
        $('#instagram-followers').html(result.instagram_followers);
        $('#facebook-likes').html(result.facebook_likes);
        $('#website-visitor').html(result.website_visitors);
       },
       error: function(error){
           alert('Somthing want wrong for getting social followers!');
       }
    });

    $('body').on('click','.neue_mv, .neue_vl',function(){


        var d = new Date();
        var year = d.getFullYear();
        if($(this).hasClass('neue_mv'))
          v= 1;
        else
          v= 2;

        $('#neue_mv_modal').find('.type-selection').val(v);
        $('#neue_mv_modal').find('.pa-month').val('');
        $('#neue_mv_modal').find('.pa-year').val(year);

        var name = $(this).find('.card-title').html();
        $('#neue_mv_modal').find('.modal-title').html(name);

        var export_url = $('#neue_mv_modal').find('.btn-export').attr('data-org-url');
        export_url = export_url+'&section_type='+name;

        $('#neue_mv_modal').find('.btn-export').attr('data-url', export_url);

        neue_mv();

        $('#neue_mv_modal').modal('show');
    });

    $('body').on('click', '#added-month-asset tbody th:first-child', function () {
        var tr = $(this).closest('tr').next();
        if(tr.hasClass('mv')){
            if(tr.hasClass('hidden'))
              tr.removeClass('hidden');
            else
                tr.addClass('hidden');

            var tr = tr.next();
            if(tr.hasClass('vl')){
                if(tr.hasClass('hidden'))
                    tr.removeClass('hidden');
                else
                    tr.addClass('hidden');
            }
        }
    });

    $('body').on('click','.get-assetmanager-property',function(){
        $('.asset-list-data').html("");
        var id= $(this).attr('data-id');

        var m = $('.pa-month').val();
        var y = $('.pa-year').val();

        var t = $(this).attr('data-type');

        var url = url_getassetmanagerproperty+"?id="+id+"&month="+m+"&year="+y+"&type="+t;
        $.ajax({
            url: url,
        }).done(function (data) {
            $('.asset-list-data').html(data);

            var columns = [
                null,
                null,
                null,
                null,
                { "type": "new-date" },
                { "type": "new-date" },
                { "type": "new-date" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
            ];
            makeDatatable($('#tenant-table_22'), columns);
        });
    });

    $('body').on('change','.achange-select',function(){
      neue_mv();
    });

    $('body').on('click','#rental-overview',function(){
        $('#rental_overview_modal').modal('show');
        $.ajax({
            url: url_getcategoryproperty,
        }).done(function (data) {
            // $('.category-prop-list1').html(data.table1);
            $('.category-prop-list2').html(data.table2);

            drawchart3('high-container3',data.name,data.actual_net_rent,data.count);

            var columns = [
                null,
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                { "type": "numeric-comma" },
                null,
                { "type": "numeric-comma" },
            ];

            makeDatatable($('.category-properties'), columns, "desc", 1);
            $('.categTotal').html($('.ajax-total-category').html());
        });

        $.ajax({
            url: url_getcategoryproperty3,
        }).done(function (data) {
            // $('.category-prop-list1').html(data.table1);
            $('.category-prop-list4').html(data.table);
            $('.category-prop-list5').html(data.table2);
            $('.category-prop-list6').html(data.table3);
            var columns = [
                null,
                null,
                { "type": "numeric-comma" }
            ];
            makeDatatable($('.category-table4'), columns, "desc", 1);
            makeDatatable($('.category-table6'), columns, "desc", 2);
        });
    });

    $('body').on('click','.get-category-properties3',function(){
        category = $(this).attr('data-category');
        getcategorypropertieslist2(category,'')
    });

    $('body').on('click','.get-category-properties4',function(){
        category = $(this).attr('data-category');
        getcategorypropertieslist3(category)
    });

    $('body').on('click','.get-name-insurance',function(){
        id = $(this).attr('data-id');

        $('#iproperty-list').find('.modal-title').html($(this).html());
        t = encodeURIComponent($(this).html());

        var url = url_getInsurancePropertyList+"?axa="+id+'&title='+t;
        $.ajax({
            url: url,
        }).done(function (data) {
            $('#iproperty-list').modal();
            $('.insurance-list-data').html(data);
            setTimeout(function(){
                var columns = [
                    null,
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" }
                ];
                makeDatatable($('#insurance-property-table'), columns);
            },1000);
        });
    });


    $('body').on('click','.get-name-properties',function(){
        name = $(this).attr('data-category');
        category = "";
        getcategorypropertieslist2(category,name)
    });

    $('body').on('click','#released-commission-card',function(){
      getassetmanagerprovision(0);
      $('#released_commission_modal').modal('show');
    });

    $('body').on('change','.pchange-select',function(){
        getassetmanagerprovision(0);
    });

    $('body').on('click','.get-provision-property',function(){
        $('.list-data').html("");
        id = $(this).attr('data-id');
        getassetmanagerprovision(id);
        $('#property-list').modal();
    });

    $('body').on('click','.get-vacancy-list',function(){
        id =$(this).attr('data-id');
        $('#cate-pro-listing').modal();
        var url = url_getvacantlist+"?id="+id;
        $.ajax({
            url: url,
        }).done(function (data) {
            $('.cate-pro-div').html(data);
            var columns = [
              null,
              null,
              null,
              null,
              { "type": "numeric-comma" },
              { "type": "numeric-comma" },
              null,
              null
            ];
            makeDatatable($('.vacant-list'), columns, "desc", 5);
        });
    });

    $('body').on('click','#sales_portal_evaluation',function(){
      var url = $(this).attr('data-url');
      $('#sales-portal-evaluation-modal').modal('show');

      $.ajax({
        url: url,
        type: 'GET',
        beforeSend: function() {
          $('#sales-portal-evaluation-modal').find('.modal-body').html('<div class="loading"></div>');
        },
        success: function(result) {
          $('#sales-portal-evaluation-modal').find('.modal-body').html(result);
          makeDatatable($('.verkauf-table'), [], 'desc', 1);
        },
        error: function(error) {
          $('#sales-portal-evaluation-modal').find('.modal-body').html('<p class="text-danger">Somthing Want Wrong!</p>');
        }
      });
    });

    $('body').on('click', '.send-mail', function() {
        var $this = $(this);
        var email = $($this).attr('data-email');
        var name  = $.trim($($this).closest('tr').find('td:first').text());

        $('#send-mail-to-user-modal').find('#send-mail-to-name').html(name);
        $('#send-mail-to-user-modal').find('input[name ="name"]').val(name);
        $('#send-mail-to-user-modal').find('input[name="email"]').val(email);
        $('#send-mail-to-user-modal').modal('show');
    });

    $('body').on('click','.show-more',function(){
        $(this).closest('.table-responsive').find('tr').removeClass('hidden');
        $(this).removeClass('show-more').addClass('hide-more');
        $(this).html('Show less');
    });

    $('body').on('click','.hide-more',function(){
        $(this).closest('.table-responsive').find('tr.hide-records').addClass('hidden');
        $(this).addClass('show-more').removeClass('hide-more');
        $(this).html('Show more');
    });

    var vacant_release_type = vacant_id = "";
    $('body').on('click', '.vacant-release-request', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        property_id = $(this).attr('data-property_id');
        $('.vacant-release-comment').val("");
        // tr = $(this).closest('tr');
        $('#vacant-release-property-modal').modal();
    });

    $('body').on('click', '.vacant-release-submit', function() {
        comment = $('.vacant-release-comment').val();
        // tr.remove();
        $.ajax({
            type: 'POST',
            url: url_property_vacantreleaseprocedure,
            data: {
                tenant_id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                property_id: property_id
            },
            success: function(data) {
                 loadTable(true);
            }
        });
    });

    $('body').on('click', '.vacant-not-release', function() {
        vacant_id = $(this).attr('data-id');
        $('.vacant-not-release-comment').val("");
        // tr = $(this).closest('tr');
        $('#vacant-not-release-property-modal').modal();
    });

    $('body').on('click', '.vacant-not-release-submit', function() {
        comment = $('.vacant-not-release-comment').val();
        // tr.remove();
        $.ajax({
            type: 'POST',
            url: url_property_vacantmarkednotrelease,
            data: {
                tenant_id: vacant_id,
                _token: _token,
                comment: comment,
            },
            success: function(data) {
                 loadTable(true);
            }
        });
    });

    $('body').on('click', '.comment-detail', function() {
        $('#comment-title').html($(this).html());
        $('#comment-detail-popup').modal();
        $('.comment-detail-body').html("");

        $.ajax({
            url: url_comment_detail,
            data:{id:$(this).attr('data-id')},
        }).done(function (data) {
            $('.comment-detail-body').html(data);
        });
    });

    $('#form_invoice_sendmail_to_am').on('submit', (function(e) {
        e.preventDefault();

        var $this = $(this);
        var formData = new FormData($(this)[0]);
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status) {
                  $($this)[0].reset();
                  $('#modal_invoice_sendmail_to_am').modal('hide');
                }else{
                  alert(result.message);
                }
            },
            error: function(error) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                alert('Somthing want wrong!');
                console.log({error});
            }
        });
    }));

    $('#form_modal_sendmail_to_custom_user').on('submit', (function(e) {
        e.preventDefault();

        var $this = $(this);
        var formData = new FormData($(this)[0]);
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status) {
                  $($this)[0].reset();
                  $('#modal_sendmail_to_custom_user').modal('hide');
                  if(reload_custom_user == 1){
                    loadTable(true);
                  }
                }else{
                  alert(result.message);
                }
            },
            error: function(error) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                alert('Somthing want wrong!');
                console.log({error});
            }
        });
    }));

    $('.modal').on("hidden.bs.modal", function (e) {
        if ($('.modal:visible').length) {
            $('body').addClass('modal-open');
        }
    });

    $('body').on('click', '#card-release-invoice', function() {
        var url = $(this).attr('data-url');
        $('#release-invoice-modal').modal('show');

        var release_invoice_table;

        if ( ! $.fn.DataTable.isDataTable( '#release-invoice-table' ) ) {

          release_invoice_table = $('#release-invoice-table').DataTable({
            processing: true,
            serverSide: true,
            "order": [
                [8, 'desc']
            ],
            ajax: url,
            columns: [
                {data: 'DT_RowIndex', name: 'pi.id'},
                {data: 'name_of_property', name: 'p.name_of_property'},
                {data: 'invoice', name: 'pi.invoice'},
                {data: 'date', name: 'pi.date'},
                {data: 'amount', name: 'pi.amount', className: 'text-right'},
                {data: 'latest_comment', orderable: false, searchable: false},
                {data: 'checkbox', orderable: false, searchable: false},
                {data: 'name', name: 'u.name'},
                {data: 'created_at', name: 'pml.created_at'},
                {data: 'comment', name: 'pml.comment'},
                {data: 'forward_button', orderable: false, searchable: false}
            ],
            "drawCallback": function( settings ) {
                if($('.long-text').length){
                  // new showHideText('.long-text');
                  new showHideText('.long-text', {
                      charQty     : 150,
                      ellipseText : "...",
                      moreText    : "More",
                      lessText    : " Less"
                  });
                }
            }
          });

        }else{
          $('#release-invoice-table').DataTable().ajax.reload(null, false);
        }

    });

    $('body').on('click', '#card-release-contract', function() {
        var url = $(this).attr('data-url');
        $('#release-contract-modal').modal('show');

        var release_invoice_table;

        if ( ! $.fn.DataTable.isDataTable( '#release-contract-table' ) ) {

          release_invoice_table = $('#release-contract-table').DataTable({
            processing: true,
            serverSide: true,
            "order": [
                [6, 'asc']
            ],
            ajax: url,
            columns: [
                {data: 'DT_RowIndex', name: 'pc.id'},
                {data: 'name_of_property', name: 'p.name_of_property'},
                {data: 'invoice', name: 'pc.invoice'},
                {data: 'amount', name: 'pc.amount', className: 'text-right'},
                {data: 'date', name: 'pc.date'},
                {data: 'completion_date', name: 'pc.completion_date'},
                {data: 'termination_date', name: 'pc.termination_date'},
                {data: 'termination_am', name: 'pc.termination_am'},
                {data: 'latest_comment', orderable: false, searchable: false},
                {data: 'name', name: 'u.name'},
                {data: 'pml_created_at', name: 'pml.created_at'},
                {data: 'category', name: 'pc.category'},
                // {data: 'comment2', name: 'pc.comment2'},
                {data: 'forward_button', orderable: false, searchable: false}
            ],
            "drawCallback": function( settings ) {
                if($('.long-text').length){
                  // new showHideText('.long-text');
                  new showHideText('.long-text', {
                      charQty     : 150,
                      ellipseText : "...",
                      moreText    : "More",
                      lessText    : " Less"
                  });
                }
            }
          });

        }else{
          $('#release-contract-table').DataTable().ajax.reload(null, false);
        }

    });

    $('body').on('click', '.mark-as-paid', function() {
        var id = $(this).attr('data-id');
        $this = $(this);

        var url = url_makeAsPaid + "?id=" + id;
        $.ajax({
            url: url,
        }).done(function(data) {
            $this.removeClass('btn-primary');
            $this.removeClass('mark-as-paid');
            $this.addClass('btn-success');
            $this.html("Erledigt");
        });
    });

    $('body').on('change','#bka-year',function(){
      var year = $(this).val();
      var url = $(this).attr('data-url');
      load_url = url+'?year='+year;
      loadTable(true);
    });

    $('body').on('click','.email-finance', function () {
        $('.list-data-banks').html('');
        property_id = $(this).attr('data-id');
        var url = url_get_by_status +"?status=16";

        $.ajax({
            url: url,
            beforeSend: function() {
                $('#email-template').find('.modal-body').html('<br><div class="loading"></div><br><br>');

            },
        }).done(function (data) {
            $('.email-template-data').html(data);
            $('#email-template-table').DataTable();
        });

        $('#email-template').modal();

    });

    function emailTemplate(){
        $('.list-data-banks').html('');
        var status = $('.property-status').val();
        load_url = url_get_by_status+"?status="+status;

        $.ajax({
            url: load_url,
            beforeSend: function() {
                $('#email-template').find('.modal-body').html('<br><div class="loading"></div><br><br>');

            },
        }).done(function (data) {
            $('.email-template-data').html(data);
            $('#email-template-table').DataTable();
        });

        $('#email-template').modal();
    }

    $('body').on('change','.change-status',function(){


        emailTemplate();
    });

    $('body').on('click','.finance_per_property', function () {
        var lender = $(this).attr('data-lender');
        var url = url_getbankproperty+'?lender='+encodeURIComponent(lender);

        $('#finance-per-bank-modal').modal('show');
        $.ajax({
            url: url,
        }).done(function(html) {
          $('#finance-per-bank-modal').find('.modal-body').html(html);
          var table = $('#finance-per-bank-modal').find('#bank-properties');

          var columns = [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" },
            { "type": "numeric-comma" }
          ];

          makeDatatable(table, columns);
        });

    });

    $('body').on('click', '.mail-mark-as-done', function() {
        var url = $(this).attr('data-url');
        var tr = $(this).closest('tr');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            beforeSend: function() {

            },
            success: function(result) {
               if(result.status){
                $('#property-mail-table').DataTable().row(tr).remove().draw();
               }else{
                alert(result.message);
               }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    });

});

$('body').on('click','.einkauf-release',function(){
    property_id = $(this).attr('data-property_id');
    $('#irelease-property-release').modal();
    $('.f-comment').val("");
});
$('body').on('click','.irelease-the-property',function(){
    // property_id = $('.property_id').val();
    comment = $('.f-comment').val();
    $.ajax({
        type : 'POST',
        url : url_einkaufsendmail,
        data : {property_id:property_id,release:1,  _token : _token,comment:comment },
        success : function (data) {
              $('#erelease').trigger('click');
            // alert(data);
            // var path = $('#path-properties-show').val();
            // window.location.href = path + '?tab=einkauf_tab';
        }
    });
  })
$('body').on('click','.einkauf-release-proc',function(){
    property_id = $(this).attr('data-property_id');
    step = $(this).attr('data-btn');
    $('#irelease-property-release').modal();
    $('.einkauf-comment').val("");
});
$('body').on('click','.einkauf-release-submit',function(){
    comment = $('.einkauf-comment').val();
    $.ajax({
        type : 'POST',
        url : url_releaseprocedure,
        data : {property_id:property_id,step:step,  _token : _token,comment:comment },
        success : function (data) {
              $('#erelease').trigger('click');
            // alert(data);
            // var path = $('#path-properties-show').val();
            // window.location.href = path + '?tab=einkauf_tab';
        }
    });
  })

var reload_forward_to = false;
$('body').on('click','.btn-forward-to',function(){
    var property_id = $(this).attr('data-property-id');
    var subject = $(this).attr('data-subject');
    var content = $(this).attr('data-content');
    var title = $(this).attr('data-title');
    var reload = $(this).attr('data-reload');
    var email = $(this).attr('data-email');
    var user = $(this).attr('data-user');
    var section_id = $(this).attr('data-id');
    var section = $(this).attr('data-section');

    section = (typeof section !== 'undefined') ? section : '';
    reload_forward_to = (reload == "1") ? true : false;

    if(typeof email !== 'undefined' && email){
        $('#modal-forward-to').find('input[name="email"]').val(email);
    }

    if(typeof user !== 'undefined' && user){
        $('#modal-forward-to').find('select[name="user"]').val(user);
    }

    $('#modal-forward-to').find('input[name="property_id"]').val(property_id);
    $('#modal-forward-to').find('input[name="subject"]').val(subject);
    $('#modal-forward-to').find('input[name="content"]').val(content);
    $('#modal-forward-to').find('input[name="title"]').val(title);
    $('#modal-forward-to').find('input[name="section_id"]').val(section_id);
    $('#modal-forward-to').find('input[name="section"]').val(section);

    $('#modal-forward-to').modal('show');
});

$('#form-forward-to').submit(function(event) {
    event.preventDefault();
    var $this = $(this);
    var data = new FormData($(this)[0]);
    var url = $(this).attr('action');

    var email = $($this).find('input[name="email"]').val();
    var user = $($this).find('select[name="user"]').val();

    if(email || user){

      $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',
          processData: false,
          contentType: false,
          cache: false,
          beforeSend: function() {
              $($this).find('button[type="submit"]').prop('disabled', true);
          },
          success: function(result){
              $($this).find('button[type="submit"]').prop('disabled', false);

              if(result.status == true){
                  $('#modal-forward-to').modal('hide');
                  $($this)[0].reset();
                  if(reload_forward_to){
                    loadTable(true);
                  }
              }else{
                  alert(result.message);
              }
          },
          error: function(error){
              $($this).find('button[type="submit"]').prop('disabled', false);
              alert('Somthing want wrong!');
          }
      });

    }else{
      alert('Please select user or enter email!');
    }
});

$('body').on('click','.diff',function(){
    var url = $(this).attr('data-url');
    $('#modal_op_list').modal('show');

    $.ajax({
         url: url,
    }).done(function (data) {
        $('#modal_op_list').find('#rent-paid-data').html(data);
        var columns = [
            null,
            null,
            null,
            null,
            null,
            { "type": "numeric-comma"},
            { "type": "numeric-comma"},
            null,
            { "type": "numeric-comma"},
            {"type": "new-date-time"},
            {"type": "new-date-time"},
            null,
            null,
            null,
            null,
        ];
        makeDatatable($('#rent-paid-table'), columns);
        var columns = [
            null,
            null,
            {"type": "new-date-time"}
        ];
        makeDatatable($('#rent-paid-upload-table'), columns, 'desc', 2);
    });
});

$(document).on('click', ".rent-paid-month-wise-detail", function(e){
    var url = $(this).attr('data-url');
    var title = $(this).text();
    $('#rent-paid-month-wise-detail-modal').find('.modal-title').text(title);
    $('#rent-paid-month-wise-detail-modal').modal('show');
    $.ajax({
       url: url,
    }).done(function (data) {
       $('#rent-paid-month-wise-detail-div').html(data);
       var columns = [
            null,
            null,
            null,
            { "type": "numeric-comma"}
        ];
        makeDatatable($('#rent-paid-monthwise-table'), columns);
    });
});

$(document).on('click', ".liquiplanung-data > a", function(e){
    var td = $(this).closest('td');
    var row = $(td).attr('row');
    var col = $(td).attr('col');
    var title = $(td).closest('tr').find('td:eq(0)').text();

    $('#liquiplanung-data-modal').find('.liquiplanung-data-title').text(title);
    $('#liquiplanung-data-modal').modal('show');

    $.ajax({
       url: url_get_liquiplanung_1_data+'?row='+row+'&col='+col+'&header='+title,
    }).done(function (data) {
       $('#liquiplanung-data-table-div').html(data);

        if(row == '1' || row == 2 || row == 3 || row == 4){
          var columns = [
            null,
            null,
            null,
            {"type": "numeric-comma"},
            {"type": "numeric-comma"},
            {"type": "new-date"}
          ];

          makeDatatable($('#liquiplanung-data-table'), columns);
        }else if(row == '6'){
          var columns = [
            null,
            null,
            null,
            {"type": "numeric-comma"},
            {"type": "numeric-comma"}
          ];

          makeDatatable($('#liquiplanung-data-table'), columns);
        }else if(row == '7'){
          var columns = [
            null,
            null,
            null,
            {"type": "numeric-comma"},
            {"type": "numeric-comma"}
          ];

          makeDatatable($('#liquiplanung-data-table'), columns);
        }else if(row == '8' || row == '9' || row == '16'){
          var columns = [
            null,
            null,
            null,
            {"type": "numeric-comma"}
          ];

          makeDatatable($('#liquiplanung-data-table'), columns);
        }else if(row == '12'){
           var columns = [
            null,
            null,
            null,
            {"type": "new-date"},
            {"type": "new-date"},
            {"type": "numeric-comma"}
          ];

          makeDatatable($('#liquiplanung-data-table'), columns);
        }else if(row == '11'){
           var columns = [
            null,
            null,
            null,
            {"type": "new-date"},
            {"type": "numeric-comma"}
          ];

          makeDatatable($('#liquiplanung-data-table'), columns);
        }else if(row == '13'){
           var columns = [
            null,
            null,
            null,
            {"type": "new-date"},
            {"type": "numeric-comma"},
            {"type": "numeric-comma"}
          ];

          makeDatatable($('#liquiplanung-data-table'), columns);
        }else{
          makeDatatable($('#liquiplanung-data-table'));
        }
        
    });
});

/*----------------------JS FOR Property Insurance comments - START -------------------------*/
var insurance_detail_comment_url;
var insurance_detail_comment_id;
var insurance_detail_property_id;
var type = '';
var is_comment = false;
$('body').on('click', '.btn-ins-comment', function() {

    insurance_detail_comment_url = $(this).attr('data-url');
    insurance_detail_comment_id  = $(this).attr('data-id');
    insurance_detail_property_id  = $(this).attr('data-property-id');
    type = 'property_insurance_tab_details';

    var limit_text = $('#insurance_detail_comments_limit').text();
    getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
    $('#insurance_detail_comment_modal').find('form').removeClass('hidden');
    $('#insurance_detail_comment_modal').modal('show');
});

$('#insurance_detail_comment_form').submit(function(event) {

    event.preventDefault();
    var $this = $(this);
    var url = $(this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var data = {
        'record_id': insurance_detail_comment_id, 
        'property_id': insurance_detail_property_id,
        'comment': comment,
        'type': type
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);
            if (result.status == true) {
                
                $($this)[0].reset();
                $($this).find('textarea[name="comment"]').focus();
                
                is_comment = true;

                var limit_text = $('#insurance_detail_comments_limit').text();
                getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });
    
});

$('body').on('click', '#insurance_detail_comments_limit', function() {
    var text = $(this).text();
    var limit = '';
    if(text == 'Show More'){
        limit = '';
        $(this).text('Show Less');
    }else{
        limit = 1;
        $(this).text('Show More');
    }
    getInsuranceDetailComments(insurance_detail_comment_url, limit, type);
});

$('body').on('click', '.remove-insurance-detail-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    var limit_text = $('#insurance_detail_comments_limit').text();
                    getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

$('#insurance_detail_comment_modal').on('hidden.bs.modal', function () {
  var form = $(this).find('form');
  
  if(!$(form).hasClass("hidden") && is_comment){
    is_comment = false;
    loadTable(true);
  }
});

function getInsuranceDetailComments(url, limit = '', type=''){
    var new_url = url+'/'+limit+'?type='+type;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#insurance_detail_comments_table').find('tbody').empty();
            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_insurance_detail_comment.replace(":id", value.id);
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-insurance-detail-comment">Löschen</a></td>' : '';
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;

                    var tr = '\
                    <tr>\
                        <td>'+ commented_user +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td>'+delete_button+'\
                    </tr>\
                    ';

                    $('#insurance_detail_comments_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}
/*----------------------JS FOR Property Insurance comments - END -------------------------*/

/*----------------------JS FOR Property Insurance Section comments - START -------------------------*/
$('body').on('click', '.btn-add-angebote-section-comment', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    var comment = $($this).closest('.row').find('.angebote-section-comment').val();
    var record_id = $($this).closest('.row').attr('data-id');
    var property_id = $($this).closest('.row').attr('data-pid');

    var data = {
        'record_id': record_id, 
        'property_id': property_id,
        'comment': comment,
        'type': 'property_insurance_tabs'
    };
    console.log(data);
    if(comment){
      $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',
          beforeSend: function() {
              $($this).prop('disabled', true);
          },
          success: function(result) {

              $($this).prop('disabled', false);

              if (result.status == true) {

                  $($this).closest('.row').find('.angebote-section-comment').val('');
                  $($this).closest('.row').find('.angebote-section-comment').focus();

              } else {
                  alert(result.message);
              }
          },
          error: function(error) {
              $($this).prop('disabled', false);
              alert('Somthing want wrong!');
          }
      });
    }else{
      return false;
    }
});

$('body').on('click', '.btn-show-angebote-section-comment', function() {
  var url = $(this).attr('data-url');
  insurance_detail_comment_url = url;

  var limit_text = $('#insurance_detail_comments_limit').text();
  type = 'property_insurance_tabs';
  getInsuranceDetailComments(url, (limit_text == 'Show More') ? 1 : '', type);
  $('#insurance_detail_comment_modal').find('form').addClass('hidden');
  $('#insurance_detail_comment_modal').modal('show');
});
/*----------------------JS FOR Property Insurance Section comments - START -------------------------*/


/*--------------------JS FOR PROPERTY COMMENTS- START----------------------------------*/
var c_record_id = '';
var c_property_id = '';
var c_type = '';
var c_is_comment = false;
$('body').on('click', '.btn-add-property-comment', function() {

    var $this = $(this);

    c_record_id = $($this).attr('data-record-id');
    c_property_id = $($this).attr('data-property-id');
    c_type = $($this).attr('data-type');

    var comment = $($this).closest('.property-comment-section').find('.property-comment').val();
    var reload = $(this).attr('data-reload');
    var subject = $(this).attr('data-subject');
    var content = $(this).attr('data-content');

    var data = { 
        'property_id': c_property_id,
        'comment': comment,
        'type': c_type,
        'subject': subject,
        'content': content,
    };
    if(c_record_id && c_record_id != ''){
      data.record_id = c_record_id;
    }

    if(comment){
      addPropertyComment($this, data, (reload == '1') ? true : false);
    }
});

$('body').on('click', '.btn-show-property-comment', function() {
  var $this = $(this);
  var show_form = ($($this).attr('data-form') == '1') ? true : false;
  var subject = $(this).attr('data-subject');
  var content = $(this).attr('data-content');

  c_record_id = $($this).attr('data-record-id');
  c_property_id = $($this).attr('data-property-id');
  c_type = $($this).attr('data-type');



  if(show_form){
      $('#property_comment_modal').find('.property-comment-section').removeClass('hidden');
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-record-id', c_record_id);
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-property-id', c_property_id);
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-type', c_type);
      
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-subject', subject);
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-content', content);
  }else{
    $('#property_comment_modal').find('.property-comment-section').addClass('hidden');
  }

  if(c_type=='properties_default_payers')
    c_record_id = 0;

  $('#property_comment_modal').modal('show');
  getPropertyComment(c_record_id, c_property_id, c_type);
});

$('body').on('click', '#property_comment_limit', function() {
    var text = $(this).text();
    if(text == 'Show More'){
        $(this).text('Show Less');
    }else{
        $(this).text('Show More');
    }
    getPropertyComment(c_record_id, c_property_id, c_type);
});

$('body').on('click', '.remove-property-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    getPropertyComment(c_record_id, c_property_id, c_type);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

$('#property_comment_modal').on('hidden.bs.modal', function () {
  var ele = $(this).find('.property-comment-section');
  
  if(!$(ele).hasClass("hidden") && c_is_comment){

    c_is_comment = false;

    if($('#release-invoice-modal').hasClass('in')){
      $('#release-invoice-table').DataTable().ajax.reload(null, false);
    }else if($('#release-contract-modal').hasClass('in')){
      $('#release-contract-table').DataTable().ajax.reload(null, false);
    }else{
      loadTable(true);
    }
  }
});

$('body').on('click', '.load_property_comment_section', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    var text = $(this).text();
    var closest = $(this).attr('data-closest');
    closest = (closest) ? closest : 'div';

    var limit = '';

    if(text == 'Show More'){
        $($this).text('Show Less');
        limit = '';
    }else{
        $($this).text('Show More');
        limit = 2;
    }

    var new_url = url+'&limit='+limit;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            $($this).closest(closest).find('.show_property_cmnt_section').empty();
            var html = '';
            if(result){
                $.each(result, function(key, value) {
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;
                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
                });
            }
            $($this).closest(closest).find('.show_property_cmnt_section').html(html);
        },
        error: function(error) {
            console.log({error});
        }
    });
});

function addPropertyComment(btnobj, data, reload = false){
  $.ajax({
      url: url_add_property_comment,
      type: 'POST',
      data: data,
      dataType: 'json',
      beforeSend: function() {
          $(btnobj).prop('disabled', true);
      },
      success: function(result) {

          $(btnobj).prop('disabled', false);

          if (result.status == true) {

              $(btnobj).closest('.property-comment-section').find('.property-comment').val('');
              $(btnobj).closest('.property-comment-section').find('.property-comment').focus();

              if(reload){
                  c_is_comment = true;
                  getPropertyComment(c_record_id, c_property_id, c_type);
              }

          } else {
              alert(result.message);
          }
      },
      error: function(error) {
          $(btnobj).prop('disabled', false);
          alert('Somthing want wrong!');
      }
  });
}


function getPropertyComment(record_id = '', property_id = '', type = ''){
    var limit_text = $('#property_comment_limit').text();
    var limit = (limit_text == 'Show More') ? 1 : '';

    var new_url = url_get_property_comment+'?property_id='+property_id+'&record_id='+record_id+'&type='+type+'&limit='+limit;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#property_comment_table').find('tbody').empty();
            
            if(type == 'property_invoices'){
                $('#property_comment_table').find('#th_name').hide();
                $('#property_comment_table').find('#th_date').hide();
            }else{
                $('#property_comment_table').find('#th_name').show();
                $('#property_comment_table').find('#th_date').show();
            }

            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_property_comment.replace(":id", value.id);
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-property-comment">Löschen</a></td>' : '';
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;

                    if(type == 'property_invoices'){
                      var tr = '\
                        <tr>\
                            <td>'+commented_user+' ('+value.date+') : '+ value.comment +'</td>\
                            <td class="text-center">'+delete_button+'\
                        </tr>\
                      ';
                    }else{
                      var tr = '\
                        <tr>\
                            <td>'+ commented_user +'</td>\
                            <td>'+ value.comment +'</td>\
                            <td>'+ value.created_at +'</td>\
                            <td>'+delete_button+'\
                        </tr>\
                      ';
                    }

                    $('#property_comment_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}
/*--------------------JS FOR PROPERTY COMMENTS- END----------------------------------*/

$('body').on('click','.mark-as-paid',function(){
    var id = $(this).attr('data-id');
    $this = $(this);
    var url = url_makeAsPaid+"?id="+id;
    $.ajax({
        url: url,
    }).done(function (data) { 
        $this.closest('tr').remove();
    });

});

$('body').on('click','.vacancy_check_all',function(){
    var cells = table_obj.cells( ).nodes();
    $( cells ).find('.vacancy_check').prop('checked', $(this).is(':checked'));
});

$('body').on('click','#btn-export-vacancy',function(){
    var checkedArr = [];
    var cells = table_obj.cells( ).nodes();
    $( cells ).find('.vacancy_check').each(function(){
      if($(this).is(':checked')){
        checkedArr.push($(this).val());
      }
    });
    if(checkedArr.length){
      var myJSON = JSON.stringify(checkedArr);
      $('#modal-vacancy-export').find('input[name="ids"]').val(myJSON);
      $('#modal-vacancy-export').modal('show');
    }else{
      alert('Please select at least one row!');
      return false;
    }
});

$('#form-vacancy-export').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var formData = new FormData($(this)[0]);
    var url = $(this).attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
            $($this).find('button[type="submit"]').html('Senden <i class="fa fa-spinner fa-spin" style="font-size: 20px;"></i>');
        },
        success: function(result) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            $($this).find('button[type="submit"]').html('Senden');
            if (result.status) {
              
              $($this).find('input[name="email"]').val('');
              $($this).find('textarea[name="message"]').val('');

              $('#modal-vacancy-export').modal('hide');
            }else{
              alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            $($this).find('button[type="submit"]').html('Senden');
            alert('Somthing want wrong!');
            console.log({error});
        }
    });
}));

$('body').on('click','#vacancy_search',function(){
  var start = $('#vacancy_start').val();
  var end = $('#vacancy_end').val();
  var url = $('#leerstände').attr('data-url');
  
  load_url = url+'?start='+start+'&end='+end;
  loadTable(true);
});

$('body').on('click', '.btn-delete-vacant', function() {
    $this = $(this);
    var id = $(this).attr('data-id');
    if (confirm('Are you sure want to delete?')) {
        $.ajax({
            url: url_delete_new_vacant,
            type: "get",
            data: {
                id: id,
            },
            success: function(response) {
                loadTable(true);
            }
        });
    }
    return false;
});
  
$('body').on('click', '.show_ao_cost', function() {
  var url = $(this).attr('data-url');
  var title = $(this).closest('tr').find("td:eq(1)").html();
  $('#show_ao_cost').find('.modal-title').html(title);

  $('#show_ao_cost').modal('show');

  if ( ! $.fn.DataTable.isDataTable( '#table_ao_cost' ) ) {
      var table_ao_cost = $('#table_ao_cost').dataTable({
          "ajax": url,
          "columnDefs": [{
              className: "text-right",
              "targets": [2]
          }],
          "columns": [
              null,
              null,
              { "type": "numeric-comma" },
              null,
              null,
              {'visible' : false },
          ]
      });
  }else{
      $('#table_ao_cost').DataTable().ajax.url(url).load();
  }

});


/*------------------ITEM COMMENT START----------------*/
var item_id;
var item_type;
var item_property_id;
var item_subject;
var item_content;
var is_comment_added = false;

$('body').on('click', '.btn-show-item-comment', function() {
    item_id   = $(this).attr('data-id'); //item id
    item_type = $(this).attr('data-type');//1 = external and 0 = comment

    item_property_id = $(this).attr('data-property-id');
    item_subject     = $(this).attr('data-subject');
    item_content     = $(this).attr('data-content');

    getItemComment();
    $('#item_comment_modal').modal('show');
});

$('body').on('click', '.btn-add-item-comment', function() {
    var $this = $(this);
    var comment = $.trim($('.item-comment').val());
    if(comment){

      var data = {
          'item_id': item_id,
          'comment': comment,
          'type': item_type,
          'property_id': item_property_id,
          'subject': item_subject,
          'content': item_content,
        };

      $.ajax({
        url: url_addItemComment,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).prop('disabled', true);
        },
        success: function(result) {

            $($this).prop('disabled', false);

            if (result.status == true) {

                $($this).closest('.item-comment-section').find('.item-comment').val('');
                $($this).closest('.item-comment-section').find('.item-comment').focus();

                is_comment_added = true;

                getItemComment();

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $($this).prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });

    }
});

$('body').on('click', '#item_comment_limit', function() {
    var text = $(this).text();
    if(text == 'Show More'){
        $(this).text('Show Less');
    }else{
        $(this).text('Show More');
    }
    getItemComment();
});

$('body').on('click', '.remove-item-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    is_comment_added = true;
                    getItemComment();
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

$('#item_comment_modal').on('hidden.bs.modal', function () {
  if(is_comment_added){
    is_comment_added = false;
    loadTable(true);
  }
});

function getItemComment(){
    var limit_text = $('#item_comment_limit').text();
    var limit = (limit_text == 'Show More') ? 1 : '';

    var new_url = url_getItemComment+'?item_id='+item_id+'&limit='+limit+'&type='+item_type;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#item_comment_table').find('tbody').empty();

            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_deleteItemComment+'?id='+value.id;
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-item-comment">Löschen</a></td>' : '';
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = ( (value.name) ? value.name : value.user_name )+''+company;
                    var comment = (item_type == '1') ? value.external_comment : value.comment;
                    var tr = '\
                        <tr>\
                            <td>'+ commented_user +'</td>\
                            <td>'+ comment +'</td>\
                            <td>'+ value.created_at +'</td>\
                            <td>'+delete_button+'\
                        </tr>\
                    ';

                    $('#item_comment_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

/*------------------ITEM COMMENT END----------------*/

var vacant_url;
$('body').on('click', '.btn-recommended-pending', function() {
    
    vacant_url = $(this).attr('data-url');

    $('.vacant_pending_comment').val("");
    $('#vacant-pending-modal').modal('show');
});

$('body').on('click', '.vacant_pending_submit', function() {
    var comment = $('.vacant_pending_comment').val();
    $.ajax({
        type: 'POST',
        url: vacant_url,
        data: {
            _token: _token,
            comment: comment,
            property_id: $('#selected_property_id').val()
        },
        success: function(data) {
            loadTable(true);
            $('#vacant-pending-modal').modal('hide');
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});


/*---------------------Auto complete for multiple comment - START-------------------------*/
var default_ele = $('.property-comment, #insurance_detail_comment, .item-comment');
makeAutoComplete(default_ele);

function makeAutoComplete(obj){
  $(obj).suggest('@', {
      // data: users,
      data: function( request, response ) {
          $.ajax({
              url : url_get_all_users,
              type: "POST",
              dataType: "json",
              beforeSend: function() {},
              success: function(res){
                if(res.status == true){
                  response(res.data);
                }else{
                }
              }
          });
      },
      map: function(user) {
          return {
            value: user.email,
            text: '<strong>'+user.email+'</strong>'
          }
      },
      onshow: function(e) {
      },
      onselect: function(e, item) {
          // console.log(item);
      },
      onlookup: function(e, item) {
      }
  });
}

/*---------------------Auto complete for multiple comment - ENd-------------------------*/


/*---------------Recommented comment---------------------------*/
var recommended_id;
var is_recommended_added = false;

$('body').on('click', '.recommended-comment', function() {
    recommended_id = $(this).attr('data-id');
    var limit_text = $('#recommended_comments_limit').text();
    getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');
    $('#recommended_comment_modal').modal('show');
});

$('#recommended_comment_form').submit(function(event) {

    event.preventDefault();
    var $this = $(this);
    var url = $(this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var data = {'tenant_id': recommended_id, 'comment': comment};

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);
            if (result.status == true) {
                $($this)[0].reset();
                $($this).find('textarea[name="comment"]').focus();
                is_recommended_added = true;

                var limit_text = $('#recommended_comments_limit').text();
                getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });

});

$('body').on('click', '#recommended_comments_limit', function() {
    var text = $(this).text();
    var limit = '';
    if(text == 'Show More'){
        limit = '';
        $(this).text('Show Less');
    }else{
        limit = 1;
        $(this).text('Show More');
    }
    getRecommendedCommentById(recommended_id, limit);
});

$('body').on('click', '.remove-recommended-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    is_recommended_added = true;
                    var limit_text = $('#recommended_comments_limit').text();
                    getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

$('body').on('click', '.load_rec_comment_section', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    var text = $(this).text();
    var limit = '';

    if(text == 'Show More'){
        $($this).text('Show Less');
        limit = '';
    }else{
        $($this).text('Show More');
        limit = 2;
    }

    var new_url = url+'/'+limit;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            $($this).closest('td').find('.show_rec_cmnt_section').empty();
            var html = '';
            if(result){
                $.each(result, function(key, value) {
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;
                    html += '<p class="long-text"><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
                });
            }
            $($this).closest('td').find('.show_rec_cmnt_section').html(html);
        },
        error: function(error) {
            console.log({error});
        }
    });
});

$('#recommended_comment_modal').on('hidden.bs.modal', function () {
  if(is_recommended_added){
    is_recommended_added = false;
    loadTable(true);
  }
});

function getRecommendedCommentById(id, limit = ''){
    var url = url_get_recommended_comment.replace(":id", id);
    url = url+'/'+limit;

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#recommended_comments_table').find('tbody').empty();
            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_recommended_comment.replace(":id", value.id);
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-recommended-comment">Löschen</a></td>' : '';

                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;

                    var tr = '\
                    <tr>\
                        <td>'+ commented_user +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td>'+delete_button+'\
                    </tr>\
                    ';

                    $('#recommended_comments_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

/*---------------------NEW - JS FOR RELEASE INVOICE - START----------------*/

$('body').on('click', '.btn-inv-release-am', function(){

    var $this = $(this);

    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    var data = {'id': id, 'status': status, 'comment': ''};

    $.ajax({
        url: url_invoice_release_am,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status) {
                loadTable(true);
            }else{
                alert(result.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

$('body').on('click', '.btn-inv-release-hv', function(){

    var $this = $(this);

    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    var data = {'id': id, 'status': status, 'comment': ''};

    $.ajax({
        url: url_invoice_release_hv,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status) {
                loadTable(true);
            }else{
                alert(result.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

var inv_usr_release_id;
var inv_usr_release_status;

$('body').on('click', '.btn-inv-release-usr', function(){
    var $this = $(this);
    var modal = $('#invoice-release-usr-modal');

    inv_usr_release_id = $(this).attr('data-id');
    inv_usr_release_status = $(this).attr('data-status');
    var title = $(this).attr('title');

    if(inv_usr_release_status == '1'){
        
        $(modal).find('.modal-title').text(title);
        $(modal).modal('show');
        
    }else{

        var data = {'id': inv_usr_release_id, 'status': inv_usr_release_status, 'comment': ''};

        $.ajax({
            url: url_invoice_release_user,
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(result) {
                if (result.status) {
                    loadTable(true);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }
});

$('#invoice-release-usr-form').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var modal = $('#invoice-release-usr-modal');
    var url = $($this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var user_id = $($this).find('select[name="user"]').val();

    var data = {
        'id': inv_usr_release_id, 
        'status': inv_usr_release_status, 
        'comment': '',
        'user_id': user_id
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);

            if (result.status) {

                $($this)[0].reset();
                $('#invoice-release-usr-error').empty();
                $(modal).modal('hide');

                loadTable(true);

            }else{
              showFlash($('#invoice-release-usr-error'), result.message, 'error');
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            showFlash($('#invoice-release-usr-error'), 'Somthing want wrong!', 'error');
        }
    });
}));

$('body').on('click', '.btn-inv-release-falk', function(){

    var $this = $(this);

    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    var data = {'id': id, 'status': status, 'comment': ''};

    $.ajax({
        url: url_invoice_release_falk,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status) {
                loadTable(true);
            }else{
                alert(result.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

/*---------------------NEW - JS FOR RELEASE INVOICE - END----------------*/

$('body').on('click', '.multiple-invoice-release-request', function() {
    var arr = [];
    $('#table-property-invoice').find('.falk_release_check').each(function(){
        if($(this).is(':checked')){
            arr.push($(this).attr('data-id'));
        }
    });

    if(arr.length){
      if(confirm('Möchten Sie die überprüfte Rechnung wirklich freigeben?')){

          var id = arr.toString();

          var data = {'id': id, 'status': 2, 'comment': ''};

          $.ajax({
            url: url_invoice_release_falk,
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(result) {
                if (result.status) {
                    loadTable(true);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
          });
      }else{
        return false;
      }
    }else{
        alert("Bitte mindestens eine Rechnung auswählen");
    }

});