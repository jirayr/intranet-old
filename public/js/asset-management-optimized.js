$(document).ready(function () {

	$('body').on('click','.get-rents',function(){
        $('.list-data-rent').html("");
        $('.vlist-data-rent').html("");

        var id 			= $(this).attr('data-value');
        var property_id	= $(this).attr('data-property');
        var type 		= $(this).attr('data-type');

        if(type == "r")
        	$('#rent-list').modal();
    	else
    		$('#vacant-list').modal();

        var url = url_assetmanagement_optimized+"?asset_manager="+id+"&property_id="+property_id+"&type="+type;

        $.ajax({
            url: url,
        }).done(function (data) { 
        	if(type=="r")
            	$('.list-data-rent').html(data);
        	else
        		$('.vlist-data-rent').html(data);

        	var columns = [
				null,
				null,
				{ "type": "numeric-comma" },
				{ "type": "numeric-comma" }
			];
			makeDatatable($('#list-rent'), columns);
        });
    });

    $('body').on('click','.get-asset-property',function(){
        $('.list-data').html("");
        var id= $(this).attr('data-value');
        
        $('#property-list').modal();

        var url = url_assetmanagement_optimized+"?id="+id;
         $.ajax({
            url: url,
        }).done(function (data) { 
            $('.list-data').html(data);

            var columns = [
				null,
				null,
				{ "type": "numeric-comma" },
				{ "type": "numeric-comma" },
				{ "type": "numeric-comma" },
				{ "type": "numeric-comma" },
				{ "type": "numeric-comma" },
				{ "type": "numeric-comma" },
				{ "type": "numeric-comma" },
				{ "type": "numeric-comma" },
			];
			makeDatatable($('#list-properties'), columns);
        });
    });

    var columns =  [
		null,
		null,
		{ "type": "formatted-num" },
		{ "type": "formatted-num" }
	];
	makeDatatable($('#tenant-table1'), columns);

	var columns = [
		null,
		null,
		null,
		null,
		{ "type": "new-date" },
		{ "type": "formatted-num" },
		{ "type": "formatted-num" },
		null,
		null
	];
	makeDatatable($('#tenant-table2'), columns, "asc", 4);

	var columns = [
		null,
		null,
		null,
		null,
		{ "type": "formatted-num" },
		{ "type": "formatted-num" },
		null,
		null
	];
	makeDatatable($('#tenant-table3'), columns);

	var columns = [
		null,
		null,
		{ "type": "numeric-comma" },
		{ "type": "numeric-comma" },
		{ "type": "numeric-comma" },
		{ "type": "numeric-comma" },
		{ "type": "numeric-comma" },
		{ "type": "numeric-comma" },
		{ "type": "numeric-comma" },
		{ "type": "numeric-comma" },
	];
	makeDatatable($('#dashboard-table-2'), columns);

    $('#assetmanagement').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if( response.success === false )
                return response.msg;
            else{
                location.reload();
            }
        }
    });

});