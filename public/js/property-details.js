$(document).ready(function() {
    $('#receiver').tagsInput();
    $('#cc').tagsInput();
    CKEDITOR.replace('mail_message');
    window.selectempfehlungSubmit = function(basename, dirname, curElement, dirOrFile) {
        var dataObj = $("#select-gdrive-file-data").val();
        dataObj = JSON.parse(dataObj);
        var formdata = new FormData();
        formdata.append('_token', _token);
        formdata.append('property_id', dataObj.property_id);
        formdata.append('empfehlung_id', dataObj.empfehlung_id);
        formdata.append('basename', basename);
        formdata.append('dirname', dirname);
        formdata.append('dirOrFile', dirOrFile);
        $('.preloader').show();
        $.ajax({
            type: 'POST',
            url: url_property_saveEmpfehlungFile,
            contentType: false,
            processData: false,
            data: formdata,
        }).done(function(data) {
            if (data.success) {
                curElement.remove();
                $('.uploaded-files-empfehlung.row-' + dataObj.empfehlung_id).append(data.element);
                alert(data.msg);
            } else {
                alert(data.msg);
            }
            $('.preloader').hide();
        });
    }
    window.selectempfehlungSubmit2 = function(basename, dirname, curElement, dirOrFile) {
        var dataObj = $("#select-gdrive-file-data").val();
        dataObj = JSON.parse(dataObj);
        var formdata = new FormData();
        formdata.append('_token', _token);
        formdata.append('property_id', dataObj.property_id);
        formdata.append('empfehlung_id', dataObj.empfehlung_id);
        formdata.append('basename', basename);
        formdata.append('dirname', dirname);
        formdata.append('dirtype', 'empfehlung2');
        formdata.append('dirOrFile', dirOrFile);
        $('.preloader').show();
        $.ajax({
            type: 'POST',
            url: url_property_saveEmpfehlungFile,
            contentType: false,
            processData: false,
            data: formdata,
        }).done(function(data) {
            if (data.success) {
                curElement.remove();
                $('.uploaded-files-empfehlung2.rowt-' + dataObj.empfehlung_id).append(data.element);
                alert(data.msg);
            } else {
                alert(data.msg);
            }
            $('.preloader').hide();
        });
    }

    $('body').on('click','.link-button-empfehlung',function(e) {
        var property_id = $(this).data('property_id');
        var empfehlung_id = $(this).data('empfehlung_id');
        var tenant_id = $(this).data('tenant_id');
        var fileObjId = $(this).attr('id');
        var data = {
            property_id: property_id,
            empfehlung_id: empfehlung_id,
            tenant_id: tenant_id,
            fileObjId: fileObjId
        };
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('empfehlung');
        $("#select-gdrive-file-callback").val('selectempfehlungSubmit');
        $("#select-gdrive-file-data").val(JSON.stringify(data));
        // var workingDir = "{{$workingDir}}";
        selectGdriveFileLoad(workingDir);
    });
    $('body').on('click','.link-button-empfehlung2',function(e) {
        var property_id = $(this).data('property_id');
        var empfehlung_id = $(this).data('empfehlung_id');
        var tenant_id = $(this).data('tenant_id');
        var fileObjId = $(this).attr('id');
        var data = {
            property_id: property_id,
            empfehlung_id: empfehlung_id,
            tenant_id: tenant_id,
            fileObjId: fileObjId
        };
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('empfehlung2');
        $("#select-gdrive-file-callback").val('selectempfehlungSubmit2');
        $("#select-gdrive-file-data").val(JSON.stringify(data));
        // var workingDir = "{{$workingDir}}";
        selectGdriveFileLoad(workingDir);
    });
    $(document).on('click', ".empfehlung-upload-file-icon", function() {
        $(this).parent().find('.empfehlung-upload-file-control').trigger('click');
    });
    $(document).on('click', ".empfehlung-upload-file-control", function() {
        var property_id = $(this).data('property_id');
        var empfehlung_id = $(this).data('empfehlung_id');
        var tenant_id = $(this).data('tenant_id');
        var fileObjId = $(this).attr('id');
        var data = {
            property_id: property_id,
            empfehlung_id: empfehlung_id,
            tenant_id: tenant_id,
            fileObjId: fileObjId
        };
        $("#select-gdrive-folder-model").modal('show');
        $("#select-gdrive-folder-type").val('empfehlung');
        $("#select-gdrive-folder-callback").val('uploadEmpfehlungSubmit');
        $("#select-gdrive-folder-data").val(JSON.stringify(data));
        // var workingDir = "{{$workingDir}}";
        selectGdriveFolderLoad(workingDir);
    });
    window.uploadEmpfehlungSubmit = function(basename, dirname) {
        console.log('empfehlung-upload-file-callback call');
        var data = $("#select-gdrive-folder-data").val();
        data = JSON.parse(data);
        var formdata = new FormData();
        formdata.append('_token', _token);
        formdata.append('property_id', data.property_id);
        formdata.append('empfehlung_id', data.empfehlung_id);
        formdata.append('basename', basename);
        formdata.append('dirname', dirname);
        formdata.append('file', document.getElementById(data.fileObjId).files[0]);
        $('.preloader').show();
        $.ajax({
            type: 'POST',
            url: url_saveEmpfehlungFile,
            contentType: false,
            processData: false,
            data: formdata,
        }).fail(function(jqXHR, textStatus, errorThrown) {
            $('.preloader').hide();
            $("#select-gdrive-folder-model").modal('hide');
        }).done(function(rdata) {
            if (rdata.success) {
                $("#select-gdrive-folder-model").modal('hide');
                load_ractivity(data.property_id, data.tenant_id, $('#' + data.fileObjId).closest('form').find('.recommended-section'));
                alert(rdata.msg);
            } else {
                $("#select-gdrive-folder-model").modal('hide');
                alert(rdata.msg);
            }
            $('.preloader').hide();
        });
    }
    r_type = "";
    $('body').on('click', '.request-button', function() {
        r_type = $(this).attr('data-id');
        $('#insurance-modal-request').modal();
    });
    $('body').on('click', '.management-request-button', function() {
        if ($(this).attr('data-id') == "management") c = 1;
        else c = 0;
        $('.change-property-manage-status').each(function() {
            if ($(this).is(":checked") && $(this).attr('data-column') == "release_status2") c = 1;
        });
        if (c == 0) {
            alert("Bitte Checkbox auswählen")
        } else {
            r_type = $(this).attr('data-id');
            $('#management-modal-request').modal();
        }
    });
    $('body').on('click', '.pmanagement-request-button', function() {
        r_type = $(this).attr('data-column');
        property_id = $(this).attr('data-id');
        $('#pmanagement-modal-request').modal();

    });
    $('body').on('click', '.insurance-comment-submit', function() {
        property_id = $('.property_id').val();
        comment = $('.insurance-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_addmaillog,
            data: {
                property_id: property_id,
                step: r_type,
                _token: _token,
                comment: comment
            },
            success: function(data) {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=insurance_tab';
            }
        });
    });
    $('body').on('click', '.management-comment-submit', function() {
        property_id = $('.property_id').val();
        comment = $('.management-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_addmaillog,
            data: {
                property_id: property_id,
                step: r_type,
                _token: _token,
                comment: comment
            },
            success: function(data) {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=property_management';
            }
        });
    });
    $('body').on('click', '.pmanagement-comment-submit', function() {
        // property_id = $('.property_id').val();
        comment = $('.pmanagement-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_addmaillog,
            data: {
                property_id: property_id,
                step: r_type,
                _token: _token,
                comment: comment
            },
            success: function(data) {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=property_management';
            }
        });
    });
    $('.add-new-p-comment').click(function() {
        $('.p-comment').val("");
        $('.propertyappenddata').html("");
        getsheetcomment(1);
    });
    $('body').on('click', '.show-all-p', function() {
        id = $('#selected_property_id').val();
        getsheetcomment(-1);
    });
    $('body').on('click', '.delete-sheet-cm', function() {
        if (confirm('Are you sure?')) {
            $this = $(this);
            $.ajax({
                type: 'GET',
                url: $(this).attr('href'),
                success: function(data) {
                    $this.closest('.sec').remove();
                }
            });
        }
        return false;
    });
    $('body').on('click', '.show-less-p', function() {
        getsheetcomment(1);
    });
    $('body').on('click', '.save-p-comment', function() {
        $.ajax({
            type: 'POST',
            url: url_properties_comment_save,
            data: $('#my-comment-form').serialize(),
            success: function(data) {
                getsheetcomment(1);
                $('.p-comment').val("");
            }
        });
        return false;
    });
    getprovisionbutton();
    getsheetcomment(1);
    $('body').on('click', '.add-row-rec', function() {
        $(this).closest('tbody').find('.gesamt_row').before($('.add-row-clone').html());
    });
    $('body').on('click', '.add-more-mieter', function() {
        $('.miter-list').prepend($('.miter').html())
    });
    $('body').on('click', '.remove-mieter', function() {
        $(this).closest('tr').remove();
    });


    $('body').on('change', '.change-comment-recommended', function() {

        var $this = $(this);
        var tr = $(this).closest('tr');
        var pk = $(this).attr('data-column');

        var data = {
            property_id: $('#selected_property_id').val(),
            pk: $(this).attr('data-column'),
            value: $(this).val(),
            _token: _token,
            tenant_id: $(this).attr('data-id')
        };
        $.ajax({
            url: url_changeempfehlungdetails,
            type: "post",
            data: data,
            success: function(response) {
                // if($this.attr('data-column')=="amount7")
                // getprovisionbutton();

                if(typeof tr !== 'undefined' && tr){
                    $(tr).find('.kosten_umbau').text(response.data.kosten_umbau);
                    if(pk != 'amount2'){
                        $(tr).find('.laufzeit').val(response.data.laufzeit);
                    }
                    if(pk != 'amount5'){
                        $(tr).find('.mietfrei').val(response.data.mietfrei);
                    }
                    $(tr).find('.jahrl_einnahmen').text(response.data.jahrl_einnahmen);
                    $(tr).find('.Gesamteinnahmen').text(response.data.Gesamteinnahmen);
                    $(tr).find('.Gesamteinnahmen_netto').text(response.data.Gesamteinnahmen_netto);
                }
            }
        });
    });

    $('body').on('change', '.rental_space', function() {
        setsize($(this).closest('tr'));
    });

    $('body').on('keyup', '.revenue_amount', function() {
        setsize($(this).closest('tr'));
    });

    function setsize($tr){

        var size        = $($tr).find('.rental_space option:selected').attr('data-size');
        size = (typeof size !== 'undefined') ? makeNumber(size) : 0;
        var einnahmen   = makeNumber($($tr).find('.revenue_amount').val());

        var m = (size) ? (einnahmen / size) : 0;
        $($tr).find('.m_size').text(makeNumberFormat(m, 2));
    }


    $('body').on('change', '.change-provision', function() {
        var $this = $(this);
        var data = {
            property_id: $('#selected_property_id').val(),
            pk: $(this).attr('data-column'),
            value: $(this).val(),
            _token: _token,
            id: $(this).attr('data-id')
        };
        $.ajax({
            url: url_changeprovisioninfo,
            type: "post",
            data: data,
            success: function(response) {
                // if($this.attr('data-column')=="amount7")
                getprovisionbutton();
            }
        });
    });
    $('body').on('click', '.add-new-provision', function() {
        var $this = $(this);
        var data = {
            property_id: $('#selected_property_id').val(),
            pk: 'name',
            value: '',
            _token: _token,
            id: 0
        };
        $.ajax({
            url: url_changeprovisioninfo,
            type: "post",
            data: data,
            success: function(response) {
                getprovisionbutton();
            }
        });
    });
    $('body').on('click', '.delete-provision', function() {
        var $this = $(this);
        var data = {
            id: $(this).attr('data-id'),
            _token: _token,
        };
        if(confirm('Are you sure want to delete?')){
            $.ajax({
                url: url_deleteprovisioninfo,
                type: "post",
                data: data,
                success: function(response) {
                    getprovisionbutton();
                }
            });
        }
    });
    $('.save-table-list').click(function() {
        var data = $(this).closest('form').serialize();
        var $this = $(this);
        $.ajax({
            url: url_saveractivity,
            type: "post",
            data: data,
            success: function(response) {
                var property_id = $('#selected_property_id').val();
                load_ractivity(property_id, $this.closest('form').find('.tenant_id').val(), $this.closest('form').find('.recommended-section'));
            }
        });
    });
    window.load_ractivity = function(property_id, tenant_id, htmlArea) {
        $.ajax({
            url: url_recommended_list,
            type: "get",
            data: {
                property_id: property_id,
                tenant_id: tenant_id
            },
            success: function(response) {
                htmlArea.html(response);
                getRecommendedComment(tenant_id, 1);
            }
        });
    }
    $('.save-mieter').click(function() {
        $.ajax({
            url: url_saveleaseactivity,
            type: "post",
            data: $('#mt-form').serialize(),
            success: function(response) {
                var cal = $('.leasecolumn').val();
                var sort = $('.leasesort').val();
                var property_id = $('#selected_property_id').val();
                $.ajax({
                    url: url_lease_list,
                    type: "get",
                    data: {
                        property_id: property_id,
                        column: cal,
                        sort: sort
                    },
                    success: function(response) {
                        $('.lease-log').html(response);
                    }
                });
            }
        });
    });
    $('.searchleasecolumn').keypress(function(e) {
        if (e.which == 13) {
            $('.searchleasecolumn-btn').trigger('click');
        }
    });
    $('body').on('click', '.save-row-mieter', function() {
        var data1 = $(this).closest('tr').find("td:eq(0) input").val();
        if ($(this).closest('tr').find("td:eq(1) input").length) var data2 = $(this).closest('tr').find("td:eq(1) input").val();
        else var data2 = $(this).closest('tr').find("td:eq(1) select").val();
        if ($(this).closest('tr').find("td:eq(2) input").length) var data3 = $(this).closest('tr').find("td:eq(2) input").val();
        else var data3 = $(this).closest('tr').find("td:eq(2) select").val();
        var data4 = $(this).closest('tr').find("td:eq(3) textarea").val();
        var id = $(this).attr('data-id');
        $this = $(this);
        var data = {
            _token: _token,
            id: $(this).attr('data-id'),
            property_id: $('#selected_property_id').val(),
            data1: data1,
            data2: data2,
            data3: data3,
            data4: data4,
        };
        $.ajax({
            url: url_updatelease,
            type: "post",
            data: data,
            success: function(response) {
                $this.closest('tr').html(response);
            }
        });
    });
    $('.searchleasecolumn-btn').click(function() {
        var cal = $('.leasecolumn').val();
        var sort = $('.leasesort').val();
        var property_id = $('#selected_property_id').val();
        if ($('.searchleasecolumn').val() == "") $('.save-mieter').show();
        else $('.save-mieter').hide();
        $.ajax({
            url: url_lease_list,
            type: "get",
            data: {
                property_id: property_id,
                column: cal,
                sort: sort,
                search: $('.searchleasecolumn').val()
            },
            success: function(response) {
                $('.lease-log').html(response);
            }
        });
    });
    $('body').on('click', '.head-title-sort th', function() {
        var cal = $(this).attr('data-column');
        var sort = $('.leasesort').val();
        var property_id = $('#selected_property_id').val();
        $('.leasecolumn').val(cal);
        if (sort == 'asc') sort = 'desc';
        else sort = 'asc';
        $('.leasesort').val(sort);
        $.ajax({
            url: url_lease_list,
            type: "get",
            data: {
                property_id: property_id,
                column: cal,
                sort: sort
            },
            success: function(response) {
                $('.lease-log').html(response);
            }
        });
    });
    //turn to inline mode
    $.fn.editable.defaults.onblur = 'ignore';
    $.fn.editable.defaults.ajaxOptions = {
        type: "POST"
    };
    /*$('.property-details').find('a.inline-edit').editable({
          success: function(response, newValue) {
              if( response.success === false )
                  return response.msg;
              else{
                  var path = $('#path-properties-show').val();
                  window.location.href = path + '?tab=properties';
    	}
          }
    });*/
    $('.budget-table').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) return response.msg;
            else {
                var path = $('#path-properties-show').val();
                // window.location.href = path + '?tab=budget';
            }
        }
    });
    $('.bank-name').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) {
                return response.msg;
            } else {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=properties';
            }
        }
    });
    $('.inline-edit-use').editable({
        prepend: "not selected",
        source: [{
            value: 'Einzelhandel',
            text: 'Einzelhandel'
        }, {
            value: 'Büro/Praxen',
            text: 'Büro/Praxen'
        }, {
            value: 'Lager',
            text: 'Lager'
        }, {
            value: 'Stellplätze',
            text: 'Stellplätze'
        }, {
            value: 'Sonstiges',
            text: 'Sonstiges'
        }, {
            value: 'Wohnungen',
            text: 'Wohnungen'
        }, {
            value: 'Gastronomie',
            text: 'Gastronomie'
        }],
    });
    $('#tenant-listing').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) {
                return response.msg;
            } else {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=tenant-listing';
            }
        }
    });
    $('#swot-template').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) return response.msg;
            else {
                var path = $('#path-properties-show').val();
                // window.location.href = path + '?tab=swot-template';
            }
        }
    });
    $('.save-ad-post').on('click', function() {
        $.ajax({
            url: url_save_new_post,
            type: "post",
            data: $('#adsPostForm').serialize(),
            success: function(response) {}
        });
    });
    $('body').on('click', '.delete-custom-sheet', function() {
        $this = $(this);
        if (confirm('Are you sure want to delete?')) {
            $.ajax({
                url: url_add_new_vacant,
                type: "get",
                data: {
                    id: $(this).attr('data-id'),
                    delete: 1
                },
                success: function(response) {
                    var url = $('#path-properties-show').val();
                    alert(response.message);
                    $('#vacant-space-wrapper').load(url + ' #vacant_space', function() {
                        $('#vacant_space .multi-category').select2();
                    });
                }
            });
        }
        return false;
    });
    $('body').on('click', '.delete-comment-file', function() {
        var $this = $(this);
        if (confirm('Are you sure want to delete this file?')) {
            var propertyId = $('#selected_property_id').val();
            var data = {
                _token: _token,
                id: $(this).attr('data-id'),
                property_id: propertyId,
                file: $(this).attr('data-file')
            };
            $.ajax({
                url: url_delete_comment_file,
                type: "post",
                data: data,
                success: function(response) {
                    $this.closest('.comment-sec').remove();
                }
            });
        }
    });
    $('body').on('click', '.add-custom-vacant', function() {
        var $this = $(this);
        $(this).attr('disable', 'disabled');
        $.ajax({
            url: url_add_new_vacant,
            type: "get",
            data: {
                property_id: $(this).attr('data-id')
            },
            success: function(response) {
                var url = $('#path-properties-show').val();
                alert(response.message);
                $('#vacant-space-wrapper').load(url + ' #vacant_space', function() {
                    $('#vacant_space .multi-category').select2();
                    $this.attr('disable', false);
                });
            }
        });
        return false;
    });
    $('body').on('click', '.save-template-info', function() {
        var $this = $(this);
        var formData = new FormData($(this).closest('form')[0]);
        formData.append('message', CKEDITOR.instances.mail_message.getData())
        $.ajax({
            url: url_savebankinfoformail,
            type: "post",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function(response) {
                alert("saved")
                location.reload();

            }
        });
    });
    $('body').on('click', '.save-vacant', function() {
        var $this = $(this);
        var upload = $(this).attr('data-upload');
        var formData = new FormData($(this).closest('form')[0]);
        $.ajax({
            url: url_save_vacant_space + "?upload=" + upload,
            type: "post",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function(response) {
                $this.closest('form').find(".vacant-pdf").val("");
                $this.closest('form').find(".vacant-image").val("");
                // $this.closest('form').find(".vacant_id").val(response.vacant_id);
                var url = $('#path-properties-show').val();
                //note: the space before #div1 is very important
                alert(response.message);
                if (response.image != "") $this.closest('form').find('.im').html(response.image);
                if (response.pdf != "") $this.closest('form').find('.pd').html(response.pdf);
                if (response.planpdf != "") $this.closest('form').find('.planpd').html(response.planpdf);
                $('#vacant-space-wrapper').load(url + ' #vacant_space', function() {
                    $('#vacant_space .multi-category').select2();
                    /*$('body').on('click','#vacant_space .show-links',function(){

		      			var cl = $(this).attr('data-class');
		      			if($('.'+cl).hasClass('hidden')){
		          			$(this).find('i').removeClass('fa-angle-down');
		          			$(this).find('i').addClass('fa-angle-up');
		          			$('.'+cl).removeClass('hidden');
		      			}else{
		          			// $(this).html('<i class="fa fa-angle-down"></i>');
		          			$(this).find('i').addClass('fa-angle-down');
		          			$(this).find('i').removeClass('fa-angle-up');
		          			$('.'+cl).addClass('hidden');
		          			// $(this).next().addClass('hidden');
		      			}
					})*/
                });
            }
        });
        return false;
    });
    $('.save-vermietung').on('click', function() {
        var vitem_id = $(this).closest('.vermi-form').find('.vitem_id').val();
        var formData = new FormData($(this).closest('form')[0]);
        $.ajax({
            url: url_save_vacant_data + "?upload=0",
            type: "post",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function(response) {
                $(".vacant-pdf").val("");
                $(".vacant-image").val("");
                alert(response.message);
                if (response.image != "") $('.im-' + vitem_id).html(response.image);
                if (response.pdf != "") $('.pd-' + vitem_id).html(response.pdf);
                if (response.planpdf != "") $('.planpd-' + vitem_id).html(response.planpdf);
            }
        });
        return false;
    });

    var property_status;
    var old_val = $('.property-status').val();

    $('body').on('change', '.property-status', function(){
        property_status = $(this);
        if(old_val == '6'){
            $('#property-status-confirm-password-modal').modal('show');
            $('#property-status-confirm-password-modal').find('input[name="password"]').val('').focus();
        }else{
            changePropertyStatus(property_status);
        }
    });

    $('body').on('click', '.cancel-confirm-password', function(){
        $('.property-status').val(old_val);
    });

    $('#property-status-confirm-password-form').on('submit', (function(e) {
        e.preventDefault();

        var $this = $(this);
        var formData = new FormData($(this)[0]);
        var url = $(this).attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status) {
                    $('#property-status-confirm-password-modal').modal('hide');
                    changePropertyStatus(property_status);
                }else{
                  showFlash($('#property-status-confirm-password-error'), result.message, 'error');
                }
            },
            error: function(error) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash($('#property-status-confirm-password-error'), 'Somthing want wrong!', 'error');
            }
        });
    }));

    var propertyId;
    function changePropertyStatus(obj){
        if ($(obj).val() == "1") {
            $('#Modal').modal('show');
        }
        obj.prop("disabled", true);
        propertyId = $(obj).data('property-id');
        var status = $(obj).val();
        var notificationType = $(obj).data('notification-type');
        var data = {
            _token: _token,
            status: status,
            oldstatus: old_val,
            property_id: propertyId
        };

        $.ajax({
            type: 'POST',
            url: url_properties_change_status,
            data: data,
            success: function(data) {
                if (data.status == 0) {
                    alert(data.message);
                    obj.prop("disabled", false);
                    $('.property-status').val(data.oldstatus)
                } else {
                    newNotification(propertyId, 3);
                    // location.reload();
                }
            }
        });
    }

    /*$('body').on('change', '.property-status', function() {
        if ($(this).val() == "1") {
            $('#Modal').modal('show');
        }
        var selection = $('.property-status');
        selection.prop("disabled", true);
        var propertyId = $(this).data('property-id');
        var status = $(this).val();
        var notificationType = $(this).data('notification-type');
        var data = {
            _token: _token,
            status: status,
            property_id: propertyId
        };
        $.ajax({
            type: 'POST',
            url: url_properties_change_status,
            data: data,
            success: function(data) {
                if (data.status == 0) {
                    alert(data.message);
                    selection.prop("disabled", false);
                    $('.property-status').val(data.oldstatus)
                } else {
                    newNotification(propertyId, 3);
                    // location.reload();
                }
            }
        });
    });*/


    $('.vermi-upload').click(function() {
        $('#post_list').val(1);
        $('#vadsPostForm').submit();
    });
    $('.delete-file-multiple').click(function() {
        if (confirm("Are you sure want to delete this?")) {
            var id = $(this).attr('data-id');
            var property_id = $('.property_id').val();
            var type = $(this).attr('data-type');
            var $this = $(this);
            $.ajax({
                url: url_remove_uploaded_file,
                type: "GET",
                data: {
                    id: id,
                    property_id: property_id,
                    type: type
                },
                success: function(response) {
                    var v = 0;
                    $this.prev().remove();
                    $this.closest('.uploaded-files').find('span.delete-file-multiple').each(function() {
                        $(this).attr('data-id', v);
                        v += 1;
                    });
                    $this.remove();
                }
            });
            return false;
        }
        return false;
    });
    $('.bundesland').change(function() {
        var option = $('option:selected', this).attr('data-id');
        $('.real_estate_taxes').val(option);
    });
    $('.save-upload-vermietung').on('click', function() {
        var vitem_id = $(this).closest('.vermi-form').find('.vitem_id').val();
        var formData = new FormData($(this).closest('form')[0]);
        $.ajax({
            url: "{{route('save_vacant_data')}}?upload=1",
            type: "post",
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            success: function(response) {
                $(".vacant-pdf").val("");
                $(".vacant-image").val("");
                alert(response.message);
                if (response.image != "") $('.im-' + vitem_id).html(response.image);
                if (response.pdf != "") $('.pd-' + vitem_id).html(response.pdf);
                if (response.planpdf != "") $('.planpd-' + vitem_id).html(response.planpdf);
                $('.v-item' + item + ' .fa').css('color', 'green');
            }
        });
        return false;
    });
    $('.deletevermitund').on('click', function() {
        if (confirm("Are you sure want to delete this?")) {
            var id = $(this).closest('.img-action-wrap').attr('id');
            var $this = $(this);
            $.ajax({
                url: url_remove_vacant_media,
                type: "GET",
                data: {
                    id: id
                },
                success: function(response) {
                    $this.closest('.creating-ads-img-wrap').remove();
                }
            });
        }
    });
    $('.setasfavourite').on('click', function() {
        var $this = $(this);
        if (confirm("Are you sure want to set as favourite?")) {
            var id = $(this).closest('.img-action-wrap').attr('id');
            $.ajax({
                url: url_set_vacant_media_favourite,
                type: "GET",
                data: {
                    id: id
                },
                success: function(response) {
                    if (response == 1) $this.find('i').removeClass('fa-star-o').addClass('fa-star');
                    else $this.find('i').removeClass('fa-star').addClass('fa-star-o');
                }
            });
        }
    });
    $('.setimagefavourite').on('click', function() {
        var $this = $(this);
        if (confirm("Are you sure want to set as favourite?")) {
            var id = $(this).closest('.img-action-wrap').attr('data-id');
            var key = $(this).attr('data-id');
            $.ajax({
                url: url_set_ad_image_favourite,
                type: "GET",
                data: {
                    id: id,
                    key: key
                },
                success: function(response) {
                    $this.closest('form').find('i').removeClass('fa-star').addClass('fa-star-o');
                    if (response == 1) $this.find('i').removeClass('fa-star-o').addClass('fa-star');
                }
            });
        }
    });
    $('body').on('click', '.setcustomimagefavourite', function() {
        var $this = $(this);
        if (confirm("Are you sure want to set as favourite?")) {
            var key = $(this).closest('.img-action-wrap').attr('data-id');
            var id = $(this).closest('.img-action-wrap').attr('data-item-id');
            $.ajax({
                url: url_set_vacantitem_favourite,
                type: "GET",
                data: {
                    id: id,
                    key: key
                },
                success: function(response) {
                    $this.closest('form').find('i').removeClass('fa-star').addClass('fa-star-o');
                    if (response == 1) $this.find('i').removeClass('fa-star-o').addClass('fa-star');
                }
            });
        }
    });
    $('body').on('click', '.deletevacantimage', function() {
        if (confirm("Are you sure want to delete?")) $(this).closest('.creating-ads-img-wrap').remove();
    });
    $('.exportsetasfavourite').on('click', function() {
        var $this = $(this);
        if (confirm("Are you sure want to set as favourite?")) {
            var id = $(this).attr('data-id');
            var key = $(this).attr('data-key');
            $.ajax({
                url: url_set_export_media_favourite,
                type: "GET",
                data: {
                    id: key,
                    key: id
                },
                success: function(response) {
                    $('.export-medias').find('i').removeClass('fa-star').addClass('fa-star-o');
                    if (response == 1) $this.find('i').removeClass('fa-star-o').addClass('fa-star');
                    else $this.find('i').removeClass('fa-star').addClass('fa-star-o');
                }
            });
        }
    });
    $('#new-tenant').on('click', function() {
        var propertyId = $(this).data('property-id');
        $.ajax({
            url: url_tenants_store,
            type: "post",
            data: {
                'nk_pm': 0,
                'nk_pa': 0,
                'mv_begin': '',
                'mv_end': '',
                'property_id': propertyId
            },
            success: function(response) {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=tenant-listing';
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    });
    $('#schl-template').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) return response.msg;
            else {
                if (typeof(response.reload) != "undefined" && response.reload === false) {} else {
                    var path = $('#path-properties-show').val();
                    window.location.href = path + '?tab=schl-template';
                }
            }
        }
    });
    $('#clever-fit-rental-offer').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) return response.msg;
            else {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=clever-fit-rental-offer';
            }
        }
    });
    $('#planung-berechnung-wohnen').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) return response.msg;
            else {
                var path = $('#path-properties-show').val();
                window.location.href = path + '?tab=planung-berechnung-wohnen';
            }
        }
    });
    $('#tenancy-schedule').find('a.inline-edit').editable({
        success: function(response, newValue) {
            if (response.success === false) return response.msg;
            else {
                var path = $('#path-properties-show').val();
                var selectingTenancySchedule = response.selecting_tenancy_schedule;
                // window.location.href = path + "?tab=tenancy-schedule&selecting_tenancy_schedule=" + selectingTenancySchedule;
            }
        }
    });
    $("#select_bank_s").on('change', function() {
        var allVals = $(this).val();
        allVals = allVals == null ? [] : allVals;
        if (allVals.length === 0) {
            $("#selectbank_create_btn_s").prop('disabled', true);
        } else {
            $("#selectbank_create_btn_s").prop('disabled', false);
        }
    });
    $('.edit_comment').click(function() {
        var id = $(this).data();
        $('.comment_edit_modal').val(id.content);
        $('.comment_id_modal').val(id.value);
    });
    let propertyIframe = $('#properties-tab iframe');
    propertyIframe.load(function() {
        // ropertyIframe = $('#properties-tab iframe').contents().find('.bank-tab iframe');
        // console.log($('#properties-tab iframe').contents().find('.bnk-name'));
        $('#properties-tab iframe').contents().find('.bnk-name').find('a.inline-edit').editable({
            success: function(response, newValue) {
                if (response.success === false) return response.msg;
                else {
                    var path = $('#path-properties-show').val();
                    window.location.href = path + '?tab=properties';
                }
            }
        });
        if (isNaN($.cookie('property-zoom-value'))) {
            $.removeCookie('property-zoom-value');
        }
        var zoomValue = 1;
        if ($.cookie('property-zoom-value')) {
            zoomValue = parseFloat(parseFloat($.cookie('property-zoom-value')).toFixed(1));
        } else {
            $.cookie('property-zoom-value', 1);
        }
        propertyIframe = $('#properties-tab iframe').contents().find('.bank-tab iframe');
        $('span#property-zoom-value').text((zoomValue * 100).toFixed(0));
        // console.log(propertyIframe.contents().find('.pr-zoom-in-out-table'));
        propertyIframe.contents().find('.pr-zoom-in-out-table').css({
            'zoom': zoomValue,
            '-webkit-transform': 'scale(' + zoomValue + ')',
            '-moz-transform': 'scale(' + zoomValue + ')',
            '-ms-transform': 'scale(' + zoomValue + ')',
            '-o-transform': 'scale(' + zoomValue + ')',
            'transform': 'scale(' + zoomValue + ')',
            'transform-origin': 'top left'
        });
        $('#property-zoom-in').on('click', function() {
            zoomValue = parseFloat(parseFloat($.cookie('property-zoom-value')).toFixed(1));
            if (zoomValue < 2.0) {
                zoomValue += 0.1;
                $.cookie('property-zoom-value', zoomValue);
            }
            $('span#property-zoom-value').text((zoomValue * 100).toFixed(0));
            propertyIframe.contents().find('.pr-zoom-in-out-table').css({
                'zoom': zoomValue,
                '-webkit-transform': 'scale(' + zoomValue + ')',
                '-moz-transform': 'scale(' + zoomValue + ')',
                '-ms-transform': 'scale(' + zoomValue + ')',
                '-o-transform': 'scale(' + zoomValue + ')',
                'transform': 'scale(' + zoomValue + ')',
                'transform-origin': 'top left'
            });
        });
        $('#property-zoom-out').on('click', function() {
            zoomValue = parseFloat(parseFloat($.cookie('property-zoom-value')).toFixed(1));
            if (zoomValue >= 0.6) {
                zoomValue -= 0.1;
                $.cookie('property-zoom-value', zoomValue);
            }
            $('span#property-zoom-value').text((zoomValue * 100).toFixed(0));
            propertyIframe.contents().find('.pr-zoom-in-out-table').css({
                'zoom': zoomValue,
                '-webkit-transform': 'scale(' + zoomValue + ')',
                '-moz-transform': 'scale(' + zoomValue + ')',
                '-ms-transform': 'scale(' + zoomValue + ')',
                '-o-transform': 'scale(' + zoomValue + ')',
                'transform': 'scale(' + zoomValue + ')',
                'transform-origin': 'top left'
            });
        });
    });
    $('.save-ccomment').click(function() {
        var url = url_change_comment;
        var fd = new FormData();
        var file_data = $('.comment-file1')[0].files; // for multiple files
        for (var i = 0; i < file_data.length; i++) {
            fd.append("file1[" + i + "]", file_data[i]);
        }
        var file_data = $('.comment-file2')[0].files; // for multiple files
        for (var i = 0; i < file_data.length; i++) {
            fd.append("file2[" + i + "]", file_data[i]);
        }
        var file_data = $('.comment-file3')[0].files; // for multiple files
        for (var i = 0; i < file_data.length; i++) {
            fd.append("file3[" + i + "]", file_data[i]);
        }
        var other_data = $('#ccoment-form').serializeArray();
        $.each(other_data, function(key, input) {
            fd.append(input.name, input.value);
        });
        fd.append('_token', _token);
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            data: fd,
        }).done(function(data) {
            // alert(data);
            var path = $('#path-properties-show').val();
            window.location.href = path + '?tab=comment_tab';
        });
    });
    $('a.pdf-edit').editable({
        success: function(response, newValue) {}
    });
    $("#pdf").change(function() {
        $("#pdf_name").text(this.files[0].name);
    });
    $("#pdf2").change(function() {
        $("#pdf_name2").text(this.files[0].name);
    });
    $('body').on('click', '.imgDeleteBtn', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_ads_images,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('.creating-ads-img-wrap').remove();
                }
            });
        }
    })
    $('body').on('click', '.deleteImage', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            image_name = $(this).attr('data-img-name');
            type = $(this).attr('data-tab-type');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_ad_images,
                data: {
                    name:image_name,
                    id: id,
                    type:type,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('.creating-ads-img-wrap').remove();
                }
            });
        }
    })

    $('body').on('click', '.pdfDeleteBtn1', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_ads_pdf,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('.creating-ads-img-wrap').remove();
                }
            });
        }
    });
    $('#deleteFileModal').on('hidden.bs.modal', function() {
        location.reload();
    });
    $('#select-gdrive-file-model').on('hidden.bs.modal', function() {
        $("#select-gdrive-file-content").html('');
        if (einkauf_file_link_isPageReload) {
            location.reload();
        }
    });
    $('#select-gdrive-folder-model').on('hidden.bs.modal', function() {
        $("#select-gdrive-folder-content").html('');
    });
    $('body').on('click', '.pdfDeleteBtn', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_property_pdf,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('.creating-ads-img-wrap').remove();
                }
            });
        }
    });
    $('body').on('click', '.delete-insurance', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_property_insurance,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('tr').remove();
                }
            });
        }
    });
    $('body').on('click', '.delete-service-provider', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_service_provider,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('tr').remove();
                }
            });
        }
    });
    $('body').on('click', '.delete-maintainance', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_maintenance,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('tr').remove();
                }
            });
        }
    });
    $('body').on('click', '.delete-investation', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_investation,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $this.closest('tr').remove();
                }
            });
        }
    });
    $('body').on('click', '.new-date', function() {
        $('.date-item-id').val($(this).attr('data-id'));
    });
    $(".new_date,.new_start_date").keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            id = $('.date-item-id').val();
            new_date = $('.new_date').val();
            new_start_date = $('.new_start_date').val();
            $.ajax({
                type: 'POST',
                url: url_change_date,
                data: {
                    id: id,
                    _token: _token,
                    new_date: new_date,
                    new_start_date: new_start_date
                },
                success: function(data) {
                    if (typeof(data.success) != "undefined" && data.success === false) {
                        alert(data.msg);
                    } else {
                        var path = $('#path-properties-show').val();
                        window.location.href = path + '?tab=tenancy-schedule';
                    }
                }
            });
        }
    });
    $('body').on('click', '.save-date', function() {
        id = $('.date-item-id').val();
        new_date = $('.new_date').val();
        new_start_date = $('.new_start_date').val();
        $.ajax({
            type: 'POST',
            url: url_change_date,
            data: {
                id: id,
                _token: _token,
                new_date: new_date,
                new_start_date: new_start_date
            },
            success: function(data) {
                if (typeof(data.success) != "undefined" && data.success === false) {
                    alert(data.msg);
                } else {
                    var path = $('#path-properties-show').val();
                    window.location.href = path + '?tab=tenancy-schedule';
                }
            }
        });
    });
    $(".set_as_standard").change(function() {
        $.ajax({
            url: url_property_set_as_standard + '/' + $(this).val(),
            success: function(response) {
                if (response.msg == 'success') {
                    // location.reload();
                }
            }
        });
    });
    $('body').on('click', '.remove-tenant', function() {
        if (confirm("Are you sure want to delete this?")) {
            id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                type: 'POST',
                url: url_delete_tenant,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    location.reload();
                }
            });
        }
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': _token
        }
    });
    $(document).on("focus", ".mask-input-date", function() {
        $(this).mask('00/00/0000');
    });

    $('.percent-mask-input').mask('##0,00%', {reverse: true});
    $(document).on("focus", ".mask-input-new-date", function() {
        $(this).mask('00.00.0000');
    });
    $(document).on("focus", ".mask-input-number", function() {
        $(this).mask('#.##0,00', {
            reverse: true
        });
    });
    $(document).on("focus", ".mask-input-number-six-decimal", function() {
        $(this).mask('#.##0,000000', {
            reverse: true
        });
    });
    $(document).on("focus", ".mask-input-number-ten-decimal", function() {
        $(this).mask('#.##0,0000000000', {
            reverse: true
        });
    });
    $(document).on("focus", ".budget-table input", function() {
        $(this).mask('#.##0,00', {
            reverse: true
        });
    });
    $(document).on("focus", ".mask-number-input-nodecimal", function() {
        $(this).mask('#.##0,00', {
            reverse: true
        });
    });
    $(document).on("focus", ".mask-number-input-negetive", function() {
        $(this).maskMoney({
            allowNegative: true,
            thousands: '.',
            decimal: ',',
            affixesStay: false,
            precision: 2
        });
    });
    // user-form-data datatable initialization
    $(window).load(function() {
        makeDatatable($('#user-form-data'));
        makeDatatable($('#user-form-data1'));

        if ($('.insurance-inline').length)
            $('.insurance-inline').editable();

        var columns = [
            null,
            null,
            null,
            null,
            null, {
                "type": "numeric-comma"
            }, {
                "type": "numeric-comma"
            }, {
                "type": "numeric-comma"
            }, {
                "type": "numeric-comma"
            },
            null,
            null, {
                "width": "200px"
            }
        ];
        makeDatatable($('#listing-banks'), columns);
        $('#listing-banks').find('a.inline-edit').editable({
            success: function(response, newValue) {
                if (response.success === false) return response.msg;
                else {
                    // bank_list();
                }
            }
        });


    });
    $('.add-more-tenant').click(function() {
        h = $('.tenant').html();
        $(this).closest('form').find('.sheet-tenant-list').append(h);
        $('.mask-number-input').mask('#.##0,00', {
            reverse: true
        });
        $('.mask-number-input-negetive').maskMoney({
            allowNegative: true,
            thousands: '.',
            decimal: ',',
            affixesStay: false,
            precision: 2
        });
        $('.mask-date-input').mask('00/00/0000');
    });
    $('.save-calculation-sheet').on('click', function() {
        var str = "";
        $(this).closest('form').find('.date-change-checkbox').each(function() {
            v = 0;
            if ($(this).is(':checked')) v = 1;
            str = str + v + ",";
        });
        $('.dynamic_list').val(str);
        var str = "";
        $(this).closest('form').find('.is_current_net-checkbox').each(function() {
            v = 0;
            if ($(this).is(':checked')) v = 1;
            str = str + v + ",";
        });
        $('.rent_list').val(str);

        var str = "";
        $(this).closest('form').find('.ank_1').each(function() {
            v = 0;

            if ($(this).is(':checked'))
                v = 1;

            str = str + v + ",";
        });
        $('.ank_1_list').val(str);

        var str = "";
        $(this).closest('form').find('.ank_2').each(function() {
            v = 0;

            if ($(this).is(':checked'))
                v = 1;

            str = str + v + ",";
        });
        $('.ank_2_list').val(str);

        return true;
    });
    $('body').on('click', '.remove-ctenant', function() {
        $(this).closest('tr').remove();
    })
    $('body').on('click', '.create-new-soll', function() {
        // setTimeout(function(){
        // 	var path = $('#main-properties-path').val();
        //       window.location.href = path;
        // },2000)
    });
    ////////// Upload Property Form API //////////////
    $('.immoscout_form').on('submit', (function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        $('.immo_loader').removeClass('hide');
        locatiom_url = url_properties_upload_listing_to_api;
        $.ajax({
            type: 'POST',
            url: locatiom_url,
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.immo_loader').addClass('hide');
                $('#response').html('');
                $('#response').html(data);
                jQuery('html,body').animate({
                    scrollTop: 0
                }, 0);
            },
            error: function(data) {
                $('.immo_loader').addClass('hide');
                alert(data.massage);
            }
        });
    }));
    // upload images
    $('#upload_attachment_images').on('submit', (function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        $('.immo_loader').removeClass('hide');
        locatiom_url = url_properties_upload_attachment_images;
        $.ajax({
            type: 'POST',
            url: locatiom_url,
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.immo_loader').addClass('hide');
                $('#response').html('');
                $('#response').html(data);
                jQuery('html,body').animate({
                    scrollTop: 0
                }, 0);
                window.location.reload();
            },
            error: function(data) {
                $('.immo_loader').addClass('hide');
                alert(data.massage);
            }
        });
    }));
    // Edit Form
    $('.immoscout_form_edit').on('submit', (function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        $('.immo_loader').removeClass('hide');
        locatiom_url = url_properties_upload_listing_to_api;
        $.ajax({
            type: 'POST',
            url: locatiom_url,
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.immo_loader').addClass('hide');
                $('#response').html('');
                $('#response').html(data);
                jQuery('html,body').animate({
                    scrollTop: 0
                }, 0);
            },
            error: function(data) {
                $('.immo_loader').addClass('hide');
                alert('Something went wrong!');
            }
        });
    }));
    $('#email_template_users').on('change', function() {
        $.ajax({
            type: 'POST',
            url: url_email_template_users,
            data: {
                user_id: $(this).val(),
                id: id,
                _token: _token
            },
            beforeSend: function() {
                $('#user_signature').removeAttr('src');
                $('#user_signature').attr('src', user_signature);
            },
            success: function(data) {
                $('#user_signature').removeAttr('src');
                $('#user_signature').attr('src', data);
            }
        });
    });
    ///// del image
    var old_asset_m_id  = $('select[name="asset_m_id"]').val();
    var old_asset_m_id2 = $('select[name="asset_m_id2"]').val();

    $('.change-p-user').on('change', function() {

        var changed_field = $(this).attr('name');

        if($(this).hasClass('change_property_status') && $('.property-status').val()=="6"){
            alert('invalid');
            return false;
        }

        var propertyId = $('#selected_property_id').val();

        var data = {
                'transaction_m_id': $('.change_transaction_m_id').val(),
                'seller_id': $('.change_seller_id').val(),
                'asset_m_id': $('.change_asset_m_id').val(),
                'asset_m_id2': $('.change_asset_m_id2').val(),
                'property_status': $('.change_property_status').val(),
                'construction_m_id': $('.construction_manager').val(),
                'property_id': propertyId,
                'changed_field': changed_field,
                'old_asset_m_id': old_asset_m_id,
                'old_asset_m_id2': old_asset_m_id2,
            };

        $.ajax({
            url: url_updatepropertyuser,
            type: "get",
            data: data,
            success: function(res) {
                if (res.status == 0){
                    alert(res.message);
                }else{
                    old_asset_m_id = data.asset_m_id;
                    old_asset_m_id2 = data.asset_m_id2;
                }
            }
        });
    });
    $('.image_del').on('submit', (function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        var image_row = $('.image_row').val();
        $('.immo_loader').removeClass('hide');
        locatiom_url = url_properties_api_imag_del;
        $.ajax({
            type: 'POST',
            url: locatiom_url,
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                $('.immo_loader').addClass('hide');
                $('#response').html('');
                $('#response').html(data);
                jQuery('html,body').animate({
                    scrollTop: 0
                }, 0);
                $('#' + image_row).html('');
            },
            error: function(data) {
                $('.immo_loader').addClass('hide');
                alert(data.massage);
            }
        });
    }));
    $(function() {
        var columns = [
            null, {
                "type": "numeric-comma"
            }, {
                "type": "numeric-comma"
            }, {
                "type": "numeric-comma"
            },
            null, {
                "type": "numeric-comma"
            }, {
                "type": "numeric-comma"
            },
            null,
            null,
        ];
        makeDatatable($('#rdatatable'), columns);
        $('.save-city').on('click', function() {
            $.ajax({
                url: url_add_new_city,
                type: "post",
                data: $('#add-ort').serialize(),
                success: function(response) {
                    alert(response.message);
                    if (response.status == 1) {
                        $('#add-city').modal('hide');
                        $('#add-ort')[0].reset();
                    }
                }
            });
        });
        $('.save-pbank').on('click', function() {
            $.ajax({
                url: url_add_property_bank,
                type: "post",
                data: $('#add-p-bank').serialize(),
                success: function(response) {
                    alert(response.message);
                    if (response.status == 1) {
                        $('#add-property-bank-modal').modal('hide');
                        $('#add-p-bank')[0].reset();
                        bank_list();
                    }
                }
            });
        });
        $('.save-new-bank').on('click', function() {
            $.ajax({
                url: url_add_new_banken,
                type: "post",
                data: $('#add-new-banken').serialize(),
                success: function(response) {
                    alert(response.message);
                    if (response.status == 1) {
                        $('#nue-banken').modal('hide');
                        $('#add-new-banken')[0].reset();
                        bank_list();
                    }
                }
            });
        });
        $('.edit-existing-bank').on('click', function() {
            $.ajax({
                url: url_update_bank,
                type: "post",
                data: $('#edit-banl-banken').serialize(),
                success: function(response) {
                    alert(response.message);
                    if (response.status == 1) {
                        $('#edit-banken').modal('hide');
                        bank_list();
                    }
                }
            });
        });
        $(".property-bank").select2({
            minimumInputLength: 2,
            language: {
                inputTooShort: function() {
                    return "<?php echo 'Please enter 2 or more characters'; ?>"
                }
            },
            tags: false,
            ajax: {
                url: url_searchpbank,
                dataType: 'json',
                data: function(term) {
                    return {
                        query: term
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.code,
                                slug: item.slug,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
        $(".city-search").select2({
            minimumInputLength: 2,
            language: {
                inputTooShort: function() {
                    return "<?php echo 'Please enter 2 or more characters'; ?>"
                }
            },
            tags: false,
            ajax: {
                url: url_searchcity,
                dataType: 'json',
                data: function(term) {
                    return {
                        query: term
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.code,
                                slug: item.slug,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
        $(".bank-search").select2({
            minimumInputLength: 2,
            language: {
                inputTooShort: function() {
                    return "<?php echo 'Please enter 2 or more characters'; ?>"
                }
            },
            tags: false,
            ajax: {
                url: url_searchbank,
                dataType: 'json',
                data: function(query) {
                    return query;
                },
                processResults: function(data) {
                    return {
                        results: $.map(data, function(item) {
                            return {
                                text: item.name,
                                slug: item.slug,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
    });
    $('#steuerberater_options').on('change', function() {
        $.ajax({
            type: 'POST',
            url: url_property_update_steuerberater,
            data: {
                pk: 'steuerberater',
                value: $(this).val(),
                _token: _token
            },
            success: function(data) {
                // window.location.reload();
            }
        });
    });
    $('#steuerberater1_options').on('change', function() {
        $.ajax({
            type: 'POST',
            url: url_property_update_steuerberater,
            data: {
                pk: 'steuerberater1',
                value: $(this).val(),
                _token: _token
            },
            success: function(data) {
                // window.location.reload();
            }
        });
    });
    $('#hausmaxx_options').on('change', function() {
        $.ajax({
            type: 'POST',
            url: url_property_update_schl,
            data: {
                pk: 'hausmaxx',
                value: $(this).val(),
                _token: _token
            },
            success: function(data) {
                window.location.reload();
            }
        });
    });
    $('.change-column-name').on('change', function() {
        $.ajax({
            type: 'POST',
            url: url_property_update_schl,
            data: {
                pk: $(this).attr('data-column'),
                value: $(this).val(),
                _token: _token
            },
            success: function(data) {
                // window.location.reload();
            }
        });
    });


    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('body').on('change', '.change-property-manage', function() {
        data = {
            property_id: $('#selected_property_id').val(),
            pk: $(this).attr('data-column'),
            value: $(this).val(),
            _token: _token,
            id: $(this).attr('data-id')
        };
        $this = $(this);
        $.ajax({
            url: url_property_management_update,
            type: "post",
            data: data,
            success: function(response) {
                $('.management-table-list').html(response.table);
                init_manage_table();
            }
        });
    });
    $('body').on('click', '.change-property-manage-status', function() {
        v = 0;
        if ($(this).is(':checked')) v = 1;
        data = {
            property_id: $('#selected_property_id').val(),
            pk: $(this).attr('data-column'),
            value: v,
            _token: _token,
            id: $(this).attr('data-id')
        };
        $this = $(this);
        $.ajax({
            url: url_property_management_update,
            type: "post",
            data: data,
            success: function(response) {
                $('.management-table-list').html(response.table);
                init_manage_table();
            }
        });
    });
    $('#versicherunge_laufzeit').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $('#versicherunge_laufzeit_end').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $('.versicherunge_date').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $('body').on('change', '.image-file', function() {
        var formdata = new FormData();
        var url = url_schl_file;
        file = $(this).prop('files')[0];
        formdata.append("file", file);
        formdata.append('_token', _token);
        formdata.append('type', $(this).attr('data-type'));
        property_id = $('.property_id').val();
        formdata.append('property_id', property_id);
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            dataType: 'json',
            data: formdata,
        }).done(function(data) {
            if (data.status == 1) {
                $('.upload-image-file').attr('src', data.path);
            } else {
                alert(data.message);
            }
            // bank_list();
        });
    });
    $('body').on('change', '.bank-file-upload input[type="file"]', function() {
        var formdata = new FormData();
        var url = url_upload_bank_file;
        file = $(this).prop('files')[0];
        formdata.append("file", file);
        formdata.append('_token', _token);
        formdata.append('id', $(this).attr('data-id'));
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            data: formdata,
        }).done(function(data) {
            alert(data);
            bank_list();
        });
    });
    $('body').on('change', '.email-file-attachments', function() {
        var formdata = new FormData();
        var url = url_uploadattachments;
        $.each($(this)[0].files, function(i, file) {
            formdata.append('email_attachment[' + i + ']', file);
        });
        formdata.append('_token', _token);
        formdata.append('property_id', $('#selected_property_id').val());
        $this = $(this);
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            data: formdata,
        }).done(function(data) {
            $this.val("");
            $('.uploadeds').html(data);
        });
    });
    $('body').on('click', '.delete-attachment', function() {
        if (confirm("Are you sure want to delete this?")) {
            var id = $(this).attr('data-id');
            $this = $(this);
            $.ajax({
                url: url_remove_attachment_file,
                type: "GET",
                data: {
                    id: id
                },
                success: function(response) {
                    $this.closest('li').remove();
                }
            });
            return false;
        }
        return false;
    });
    $('body').on('click', '.add-property-bank', function() {
        $('#add-property-bank-modal').modal()
    });
    $('body').on('click', '.remove-p-bank', function() {
        id = $(this).attr('data-id');
        if (confirm("Are you sure want to delete?")) {
            $.ajax({
                url: url_removepropertybanken,
                type: "get",
                data: {
                    id: id
                },
                success: function(response) {
                    bank_list();
                }
            });
        }
    });
    $('body').on('click', '.remove-status-loi', function() {
        id = $(this).attr('data-id');
        if (confirm("Are you sure want to delete?")) {
            $.ajax({
                url: url_removestatusloi,
                type: "get",
                data: {
                    id: id
                },
                success: function(response) {
                    statusloi_list();
                }
            });
        }
    });
    CKEDITOR.replace('comment_text');
    statusloi_list();
    add_property_management(0);
    makeDatatable($('#inquiries'));
    var columns = [
        null,
        null,
        null,
        null, {
            "type": "new-date"
        }
    ];

    var columns = [
        null,
        null,
        null,
        {"type": "new-date-time"}
    ];
    makeDatatable($('#am_mail_table'), columns, 'desc', 3);
    // makeDatatable($('#hv_bu_mail_table'), columns, 'desc', 3);
    // makeDatatable($('#hv_pm_mail_table'), columns, 'desc', 3);
    // makeDatatable($('#ek_mail_table'), columns, 'desc', 3);
    // makeDatatable($('#user_mail_table'), columns, 'desc', 3);

    setTimeout(function() {
        bank_list();

        $('#list-banks').find('a.inline-edit').editable({
            success: function(response, newValue) {
                if (response.success === false) return response.msg;
                else {
                    bank_list();
                }
            }
        });
    }, 1000);

    $(document).on("focus", ".mask-number-input", function() {
        $(this).mask('#.##0,00', {
            reverse: true
        });
    });
    $('.mask-number-input2').mask('00,000000');
    $('.mask-date-input').mask('00/00/0000');
    $('.mask-date-input-new').mask('00.00.0000');
    makeDatatable($('.data-table'));
    var showall = request_showall;
    if (showall) {
        $('.nav-tabs a[href=#bank-modal]').click()
    }
    $(document).on("click", '.send_email_to_banks', function(e) {
        if (validate() == 0) {
            $(this).html('');
            $(this).html('<i class="fa fa-spin fa-spinner"></i>');
            bank = $(this).attr('data-bank');
            email = $(this).attr('selected-mail-'+bank);
            $("#bank_id").val(bank);
            $("#bank-email").val(email);
            $('#sendemailtobankers').submit();
        }
        e.preventDefault();
    });
    $(document).on("click", '.send_test_email', function(e) {
            if ($('#testEmail').val() == '') {
                toastr.error("Test Email field is required");
        } else if($('#bank-sheet').val() == ''){
                toastr.error("Kalkulation wählen field is required");

            } else {
            $(this).html('');
            $(this).html('<i class="fa fa-spin fa-spinner"></i>');
            bank = $(this).attr('data-bank');
            $("#bank_id").val(722);
            $("#isTestEmail").val(1);
            $('#sendemailtobankers').submit();
        }
        e.preventDefault();
    });
    $('body').on('click', '.checkbox-is-sent', function() {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: url_statusloi_update + "/" + id,
            data: {
                pk: pk,
                value: v,
                _token: _token
            },
            success: function(data) {}
        });
    });
    $('body').on('click', '.versicherunge-checkbox-is-approved', function() {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: url_update_versicherung_approved_status,
            data: {
                pk: pk,
                value: v,
                id: id,
                _token: _token
            },
            success: function(data) {}
        });
    });
    $('body').on('click', '.falk-checkbox-is-checked', function() {
        var id = $(this).attr('data-id');
        var pk = $(this).attr('data-column');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: url_update_falk__status,
            data: {
                pk: pk,
                value: v,
                id: id,
                _token: _token
            },
            success: function(data) {}
        });
    });
    $('body').on('click', '.delete-record', function() {
        var r = confirm("Do you really want to Delete this Record!");
        if (r == true) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: 'POST',
                url: url_delete_Property_insurance1,
                data: {
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    location.reload();
                    alert('Record Deleted !');
                }
            });
        }
    });
    ///////////// Send emails to all banks forcurrent property /////////////
    $('.all_green_btn').on('click', function(e) {
        checked_bank_ids = $(".bank_ids:checked").length;
        if (validate() == 0) {
            if (checked_bank_ids > 0) {
                if (confirm("Are you sure to send email to all bankers!")) {
                    $("#bank_id").val(null);
                    $(this).html('');
                    $(this).html('<i class="fa fa-spin fa-spinner"></i>');
                    $('#sendemailtobankers').submit();
                }
            } else {
                toastr.success("Bankers are not available here!");
            }
        }
        e.preventDefault();
    });
    $('#sendemailtobankers').on('submit', function(e) {
        e.preventDefault();
        var selectedFrame = $("#bank-sheet").val();
        console.log();
        var propID = $("#bank-sheet  option:checked").attr('data-id');
        // alert(propID);
        $("#sheet_prop_id").val(propID);
        var myFrame = window.frames["propertyIframeParent"].contentDocument.getElementById(selectedFrame);
        var propertyTableArray = myFrame.contentWindow.getTableData("table.pr-zoom-in-out-table");
        var tenantTableArray = myFrame.contentWindow.getTableData(".tenent-table");
        var note = myFrame.contentWindow.getTableData(".note-table");
        $('input#property-data-second').val(JSON.stringify(propertyTableArray));
        $('input#tenant-data-second').val(JSON.stringify(tenantTableArray));
        $('input#note-data-second').val(JSON.stringify(note));
        var formData = new FormData(this);
        $("#LoadingImage").show();
        $.ajax({
            url: url_sendemailtobankers,
            type: "post",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(response) {
                console.log(response);
                if (response == 'success') {
                    $('#testEmailButton').html('Testmail senden');
                    $('.send_email_to_banks').html('E-Mail senden');

                    toastr.success("Anfrage wurde gesendet!");
                } else {
                    alert(response);
                    toastr.error("Please contact to administrator");
                }
            },
            error:function (response) {
                alert(response.responseJSON.message);

            }
        });
    });
    $('[id^=rate_box_]').on('change', function() {
        var summe = 0;
        var fruits = [];
        $('[id^=rate_box_]').each(function() {
            var select_box = $(this).attr("id");
            var choosen_value = $("#" + select_box).val();
            fruits.push(choosen_value);
            if (choosen_value !== 0) {
                summe = parseInt(summe) + parseInt(choosen_value);
            }
            $('#total_rate').val(summe);
        });
        id = $('#selected_property_id').val();
        $.ajax({
            type: 'POST',
            url: url_property_update_schl_1 + "/" + id,
            data: {
                pk: 'swot_json',
                value: JSON.stringify(fruits),
                _token: _token
            },
            success: function(data) {
                // generate_option_from_json(data, 'country_to_state');
            }
        });
    });
    var resultData = [];
    $(document).on("click", '.show-result-list', function(event) {
        var email = $(this).data('email');
        $.ajax({
            url: url_showresult,
            type: "post",
            data: {
                email: email
            },
            success: function(response) {
                var response = JSON.parse(response);
                var tablehtml = '';
                response.forEach(function(item, index) {
                    tablehtml += "<tr><td>" + item.email_id + "</td><td>" + item.email_to_send + "</td><td><button class='btn btn-primary show-result' type='button' onclick='showResult(" + index + ")'><i class='fa fa-eye'></i></button></td></tr>";
                    resultData[index] = JSON.parse(item.user_data);
                });
                if (tablehtml) {
                    $('#result-list-table tbody').html(tablehtml);
                    $('#result-list').modal('show');
                } else {
                    alert("Data not available");
                }
                //alert(response);
            }
        });
    });
    /*$(".from_bond").keyup(function() {
        var form_bond_value = $("from_bond").val();
        var value = 100 - form_bond_value;
        $("#bank_loan").val(value);
    });
    $("#bank_loan").keyup(function() {
        var form_bond_value = $("#bank_loan").val()
        var value = 100 - form_bond_value;
        $("#from_bond").val(value);
    });*/
    $('body').on('click', '.new-banken', function() {
        $('#nue-banken').modal();
        $('#bank_email_contact').select2({
            tags: true,
            minimumResultsForSearch: -1
        });
    });
    $('#genrate_property_pdf').on('click', function(event) {
        var formdata_pdf = new FormData($('#pdfadsPostForm')[0]);
        //var formdata_pdf = $('#pdfadsPostForm').serialize();
        //console.log(formdata_pdf);
        $.ajax({
            url: url_genarate_property_pdf,
            type: 'POST',
            data: formdata_pdf,
            processData: false,
            contentType: false
        }).done(function() {
            var download_url = $('#download_property_pdf').attr('href');
            window.location.href = download_url;
        }).fail(function() {
            //console.log("An error occurred, the files couldn't be sent!");
        });
    });
    var vacant_release_type = vacant_id = "";
    // $('body').on('click', '.vacant-release-request', function() {
    //     vacant_release_type = $(this).attr('data-column');
    //     vacant_id = $(this).closest('form').find('.tenant_id').val();
    //     $('.vacant-release-comment').val("");
    //     $('#vacant-release-property-modal').modal();
    // });
    $('body').on('click', '.vacant-release-submit', function() {
        comment = $('.vacant-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_vacantreleaseprocedure,
            data: {
                tenant_id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                property_id: $('#selected_property_id').val()
            },
            success: function(data) {
                getrelbutton(vacant_id);
            }
        });
    });
    $('body').on('click', '.provision-release-request', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).closest('td').attr('data-id');
        $('.provision-release-comment').val("")
        $('#provision-release-property-modal').modal();
    });
    $('body').on('click', '.provision-release-submit', function() {
        comment = $('.provision-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_provisionreleaseprocedure,
            data: {
                tenant_id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                property_id: $('#selected_property_id').val()
            },
            success: function(data) {
                getprovisionbutton();
            }
        });
    });
    $('body').on('click', '.invoice-release-request', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        $('.invoice-release-comment').val("")
        $('#invoice-release-property-modal').modal();

        if(vacant_release_type=="release2")
        {
            $('.i-message').addClass('hidden');
            $('.i-message2').removeClass('hidden');
        }
        else{
            $('.i-message').removeClass('hidden');
            $('.i-message2').addClass('hidden');
        }

    });

    $('body').on('click', '.invoice-release-request-am', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        am_user = $(this).attr('data-user-id');
        $('.invoice-release-comment-am').val("")

        $('.invoice_asset_manager').val(am_user);
        if(am_user!="0" || vacant_release_type=="release1")
        {
            $('.am-list').addClass('hidden');
        }
        else{
            $('.am-list').removeClass('hidden');
        }
        $('#invoice-release-property-modal-am').modal();

    });
    $('body').on('click', '.invoice-release-submit-am', function() {
        comment = $('.invoice-release-comment-am').val();
        invoice_asset_manager = $('.invoice_asset_manager').val();
        if(invoice_asset_manager=="")
        {
            alert('Select AM');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: url_property_invoicereleaseprocedure,
            data: {
                id: vacant_id,
                invoice_asset_manager:invoice_asset_manager,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                property_id: $('#selected_property_id').val()
            },
            success: function(data) {
                // getprovisionbutton();
                $('.invoice_asset_manager').val("");

                $('#add_property_invoice_table4').DataTable().ajax.reload();
                $('#add_property_invoice_table').DataTable().ajax.reload();
                $('#add_property_invoice_table2').DataTable().ajax.reload();
                $('#invoice_mail_table').DataTable().ajax.reload();
            }
        });
    });

    $('body').on('click', '.invoice-release-submit', function() {
        comment = $('.invoice-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_invoicereleaseprocedure,
            data: {
                id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                property_id: $('#selected_property_id').val()
            },
            success: function(data) {
                // getprovisionbutton();
                $('#add_property_invoice_table').DataTable().ajax.reload();
                $('#add_property_invoice_table2').DataTable().ajax.reload();
                $('#invoice_mail_table').DataTable().ajax.reload();
            }
        });
    });
    $('body').on('click', '.deal-release-request', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        $('.deal-release-comment').val("")
        $('#deal-release-property-modal').modal();
    });
    $('body').on('click', '.deal-release-submit', function() {
        comment = $('.deal-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_dealreleaseprocedure,
            data: {
                id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
            },
            success: function(data) {
                $('#property_deal_table').DataTable().ajax.reload();
                $('#deal_mail_table').DataTable().ajax.reload();
            }
        });
    });
    $('.non-editable').find('input').attr('readonly', true);
    $('.non-editable').find('textarea').attr('readonly', true);
    $('.non-editable').find('.add-row-rec').hide();
    $('.non-editable').find('.remove-mieter').hide();
    $('.show-link-recommended').click(function() {
        var cl = $(this).attr('data-class');
        if ($('.' + cl).hasClass('hidden')) {
            $(this).find('i').removeClass('fa-angle-down');
            $(this).find('i').addClass('fa-angle-up');
            $('.' + cl).removeClass('hidden');
            //ajax code here
            var id = $(this).attr('data-id');
            getrelbutton(id);
        } else {
            // $(this).html('<i class="fa fa-angle-down"></i>');
            $(this).find('i').addClass('fa-angle-down');
            $(this).find('i').removeClass('fa-angle-up');
            $('.' + cl).addClass('hidden');
            // $(this).next().addClass('hidden');
        }
    });
    /*----------------JS FOR ADD PROPERTY INVOICE - START-----------------*/
    $(document).ajaxComplete(function() {
        if ($('.invoice-comment').length)
        	$('.invoice-comment').editable();
        if ($('.default-payer-comment').length)
        	$('.default-payer-comment').editable();
        if ($('.property-deal-comment').length)
        	$('.property-deal-comment').editable();
        if ($('.contract-comment').length)
            $('.contract-comment').editable();
        if ($('.angebote-comment').length)
            $('.angebote-comment').editable();
        if ($('.insurance-inline').length)
            $('.insurance-inline').editable();
    });

    // 1) open invoice
    var add_property_invoice_table = $('#add_property_invoice_table').dataTable({
        "ajax": url_get_property_invoice,
        "order": [
            [9, 'desc']
        ],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            {"type": "numeric-comma"},
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            // { "type": "new-date" },
            // null,
            // null,
            // null,
            // null,
            // null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 2) Released invoice
    var add_property_invoice_table2 = $('#add_property_invoice_table2').dataTable({
        "ajax": url_get_release_property_invoice,
        "order": [
            [9, 'desc']
        ],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            {"type": "numeric-comma"},
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 3) pending AM invoice
    var pending_am_invoice_table = $('#pending_am_invoice_table').dataTable({
        "ajax": url_get_pending_am_invoice,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 4) pending Hv invoice
    var pending_hv_invoice_table = $('#pending_hv_invoice_table').dataTable({
        "ajax": url_get_pending_hv_invoice,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 5) pending User invoice
    var pending_user_invoice_table = $('#pending_user_invoice_table').dataTable({
        "ajax": url_get_pending_user_invoice,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            // null,
            // null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 6) Pending Falk invoice
    var add_property_invoice_table4 = $('#add_property_invoice_table4').dataTable({
        "ajax": url_get_pending_invoice,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            // null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 7) Not release invoice (AM)
    var table_rejected_invoice = $('#table_rejected_invoice').dataTable({
        "ajax": url_get_not_released_invoice_am,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 8) Not release invoice (HV)
    var table_not_release_hv_invoice = $('#table_not_release_hv_invoice').dataTable({
        "ajax": url_get_not_released_invoice_hv,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 9) Not release invoice (User)
    var table_not_release_user_invoice = $('#table_not_release_user_invoice').dataTable({
        "ajax": url_get_not_released_invoice_user,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 10) Not release invoice (Falk)
    var add_property_invoice_table3 = $('#add_property_invoice_table3').dataTable({
        "ajax": url_get_not_released_invoice,
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 11) invoice mail log
    var invoice_mail_table = $('#invoice_mail_table').dataTable({
        "ajax": url_get_invoice_logs,
        "order": [
            [5, "desc"]
        ],
        "columns": [
            null,
            null,
            null,
            { "type": "new-date" },
            null,
            {"type": "new-date-time"},
            null
        ]
    });

    $('body').on('change', '.row_checkall', function() {
        var table_obj;
        var table = $(this).closest('.main_div').find('table').attr('id');

        if(table == 'add_property_invoice_table'){
            table_obj = add_property_invoice_table;
        }else if(table == 'add_property_invoice_table4'){
            table_obj = add_property_invoice_table4;
        }else if(table == 'add_property_invoice_table2'){
            table_obj = add_property_invoice_table2;
        }else if(table == 'table_rejected_invoice'){
            table_obj = table_rejected_invoice;
        }else if(table == 'add_property_invoice_table3'){
            table_obj = add_property_invoice_table3;
        }

        if($(this).is(':checked')){
            table_obj.$(".row_checkbox").prop('checked', true).trigger('change');
        }else{
            table_obj.$(".row_checkbox").prop('checked', false).trigger('change');
        }
    });

    $('body').on('click', '.download_invoice', function() {

        var table_id = $(this).closest('.main_div').find('table').attr('id');
        var url = $(this).attr('data-url');
        var checked_arr = [];
        var table_obj;

        if(table_id == 'add_property_invoice_table'){
            table_obj = add_property_invoice_table;
        }else if(table_id == 'add_property_invoice_table4'){
            table_obj = add_property_invoice_table4;
        }else if(table_id == 'add_property_invoice_table2'){
            table_obj = add_property_invoice_table2;
        }else if(table_id == 'table_rejected_invoice'){
            table_obj = table_rejected_invoice;
        }else if(table_id == 'add_property_invoice_table3'){
            table_obj = add_property_invoice_table3;
        }

        table_obj.$('.row_checkbox').each(function(){
            if($(this).prop('checked')){
                checked_arr.push($(this).val());
            }
        });

        if(checked_arr.length > 0){
            var ids = checked_arr.join(',');
            var url_load = url+'?id='+ids+'&tab=invoice';
            location.href = url_load;
            return false;
        }else{
            alert('Select at least one row!');
        }
    });

    $('body').on('click', '.mark-as-paid', function() {
        var id = $(this).attr('data-id');
        $this = $(this);
        var url = url_makeAsPaid + "?id=" + id;
        $.ajax({
            url: url,
        }).done(function(data) {
            $this.removeClass('btn-primary');
            $this.removeClass('mark-as-paid');
            $this.addClass('btn-success');
            $this.html("Erledigt");
        });
    });
    $(document).on('click', ".property_invoice-upload-file-icon", function() {
        $("#property_invoice_gdrive_file_upload").trigger('click');
    });
    $(document).on('change', "#property_invoice_gdrive_file_upload", function(e) {
        var fileName = e.target.files[0].name;
        $("#property_invoice_gdrive_file_name_span").text(fileName);
    });
    $(document).on('click', ".link-button-gdrive-invoice", function(e) {
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('property_invoice');
        $("#select-gdrive-file-callback").val('gdeiveSelectPropertyInvoice');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_nam_invoice").val());
    });
    window.gdeiveSelectPropertyInvoice = function(basename, dirname, curElement, dirOrFile) {
        $("#property_invoice_gdrive_file_basename").val(basename);
        $("#property_invoice_gdrive_file_dirname").val(dirname);
        $("#property_invoice_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#property_invoice_gdrive_file_name").val(text);
        $("#property_invoice_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }
    $('#add_property_invoice_form').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var msgElement = $('#add_property_invoice_msg');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_add_property_invoice,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                // $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                // $($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status == true) {
                    showFlash(msgElement, result.message, 'success');

                    $($this)[0].reset();
                    $('#property_invoice_gdrive_file_basename').val('');
                    $('#property_invoice_gdrive_file_dirname').val('');
                    $('#property_invoice_gdrive_file_type').val('');
                    $('#property_invoice_gdrive_file_name').val('');
                    $('#property_invoice_gdrive_file_name_span').text('');
                    $('#property_invoice_gdrive_file_upload').val('');

                    $('#add_property_invoice_table').DataTable().ajax.reload();
                    setTimeout(function() {
                        $('#property_invoice_modal').modal('hide');
                        $('.modal-backdrop').removeClass("modal-backdrop");

                    }, 1000);
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                // $($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({
                    error
                });
            }
        });
    });
    $('#add_banken_contact_employee_form').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var msgElement = $('#add_banken_contact_employee_msg');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_addBankenContactEmployee,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                // $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                // $($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status == true) {
                    showFlash(msgElement, result.message, 'success');
                    $($this)[0].reset();
                    $('#list-banks').DataTable().ajax.reload();
                    $('#add_banken_contact_employee_model').modal('hide');
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                // $($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({
                    error
                });
            }
        });
    });
    $(document).on('click', '.add-bank-contact-btn', function(e) {
        var id = $(this).attr('data-id');
        $("#add_banken_contact_employee_model").modal('show');
        $('#add_banken_contact_employee_banken_id').val(id);
    });
    $('body').on('click', '.btn-delete-property-invoice', function() {

        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var msgElement = $('#property_invoice_msg');

        if (confirm('Are you sure want to delete this invoice?')) {
            var url = url_delete_property_invoice;
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');
                        if(type == 'open-invoice'){
                            $('#add_property_invoice_table').DataTable().ajax.reload();
                        }
                        else if(type == 'not-release-invoice'){
                            $('#add_property_invoice_table3').DataTable().ajax.reload();
                        }
                        else if(type == 'not-release-invoice-am'){
                            $('#table_rejected_invoice').DataTable().ajax.reload();
                        }
                        else if(type == 'pending-am-invoice'){
                            $('#pending_am_invoice_table').DataTable().ajax.reload();
                        }
                        else{
                            $('#add_property_invoice_table4').DataTable().ajax.reload();
                        }
                        $('#invoice_mail_table').DataTable().ajax.reload();
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });
    $('body').on('change', '.property-invoice-user', function() {
        var email = $(this).val();
        var id = $(this).attr('data-id');
        var msgElement = $('#property_invoice_msg');
        var url = url_set_property_invoice_email;
        url = url.replace(':email', email);
        url = url.replace(':id', id);
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if (result.status == true) {
                    $('#add_property_invoice_table').DataTable().ajax.reload();
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                showFlash(msgElement, 'Somthing want wrong!', 'error');
            }
        });
    });

    var not_release_id = 0;
    $('body').on('click','.mark-as-not-release',function(){
        not_release_id = $(this).attr('data-id');
        $('#not-release-modal').modal();
    });

    $('body').on('click','.invoice-not-release-submit',function(){
        comment = $('.invoice-not-release-comment').val();
        $.ajax({
              type : 'POST',
              url : url_property_invoicenotrelease,
              data : {id:not_release_id, _token : _token,comment:comment,property_id:$('#selected_property_id').val()},
              success : function (data) {
                  // getprovisionbutton();
                  $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#add_property_invoice_table3').DataTable().ajax.reload();
                  $('#invoice_mail_table').DataTable().ajax.reload();
              }
          });
    });

    $('body').on('click','.mark-as-pending',function(){
        not_release_id = $(this).attr('data-id');
        $('#pending-modal').modal();
        $('.invoice-pending-comment').val("")
    });
    $('body').on('click','.invoice-pending-submit',function(){
        comment = $('.invoice-pending-comment').val();
        $.ajax({
              type : 'POST',
              url : url_property_invoicemarkpending,
              data : {id:not_release_id, _token : _token, comment:comment,property_id:$('#selected_property_id').val()},
              success : function (data) {
                  // getprovisionbutton();
                  $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#add_property_invoice_table4').DataTable().ajax.reload();
                  $('#invoice_mail_table').DataTable().ajax.reload();
              }
          });
    });

    $('body').on('click','.mark-as-pending-am',function(){
        inv_id = $(this).attr('data-id');
        $('#pending-modal-am').modal();
        $('.invoice-pending-am-comment').val("")
    });

    $('body').on('click','.invoice-pending-am-submit',function(){
        comment = $('.invoice-pending-comment').val();
        $.ajax({
            type : 'POST',
            url : url_property_invoicemarkpendingam,
            data : {
                id:inv_id,
                _token : _token,
                comment:comment,
                property_id:$('#selected_property_id').val()
            },
            success : function (data) {
                $('#add_property_invoice_table').DataTable().ajax.reload();
                $('#pending_am_invoice_table').DataTable().ajax.reload();
                $('#invoice_mail_table').DataTable().ajax.reload();
            }
          });
    });

    $('body').on('click','.deal-mark-as-not-release',function(){
        not_release_id = $(this).attr('data-id');
        $('#deal-not-release-modal').modal();
        $('.deal-not-release-comment').val("")
    });
    $('body').on('click','.deal-not-release-submit',function(){
        comment = $('.deal-not-release-comment').val();
        $.ajax({
              type : 'POST',
              url : url_deal_mark_as_notrelease,
              data : {id:not_release_id, _token : _token, comment:comment},
              success : function (data) {
                  // getprovisionbutton();
                  // $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#insurance-tab-log').DataTable().ajax.reload();
              }
          });
    });

    var invoice_reject_id;
    $('body').on('click','.btn-reject',function(){
        invoice_reject_id = $(this).attr('data-id');
        $('#invoice-reject-modal').modal();
        $('.invoice-reject-comment').val("");
    });

    $('body').on('click','.invoice-reject-submit',function(){
        comment = $('.invoice-reject-comment').val();
        $.ajax({
            type : 'POST',
            url : url_property_invoicemarkreject,
            data : {
                id:invoice_reject_id,
                _token : _token,
                comment:comment,
                property_id:$('#selected_property_id').val()
            },
            success : function (data) {
                  $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#table_rejected_invoice').DataTable().ajax.reload();
                  $('#invoice_mail_table').DataTable().ajax.reload();
            }
        });
    });

    $('#modal-forward-to').find('select[name="user"]').select2();

    var reload_forward_to = false;
    $('body').on('click','.btn-forward-to',function(){
        var property_id = $(this).attr('data-property-id');
        var subject = $(this).attr('data-subject');
        var content = $(this).attr('data-content');
        var title = $(this).attr('data-title');
        var reload = $(this).attr('data-reload');
        var email = $(this).attr('data-email');
        var user = $(this).attr('data-user');
        var section_id = $(this).attr('data-id');
        var section = $(this).attr('data-section');

        reload_forward_to = (reload == "1") ? true : false;
        section = (typeof section !== 'undefined') ? section : '';

        if(typeof email !== 'undefined' && email){
            $('#modal-forward-to').find('input[name="email"]').val(email);
        }

        if(typeof user !== 'undefined' && user){
            $('#modal-forward-to').find('select[name="user"]').val(user).trigger('change');
        }

        $('#modal-forward-to').find('input[name="property_id"]').val(property_id);
        $('#modal-forward-to').find('input[name="subject"]').val(subject);
        $('#modal-forward-to').find('input[name="content"]').val(content);
        $('#modal-forward-to').find('input[name="title"]').val(title);
        $('#modal-forward-to').find('input[name="section_id"]').val(section_id);
        $('#modal-forward-to').find('input[name="section"]').val(section);

        $('#modal-forward-to').modal('show');
    });

    $('#form-forward-to').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = new FormData($(this)[0]);
        var url = $(this).attr('action');

        var email = $($this).find('input[name="email"]').val();
        var user = $($this).find('select[name="user"]').val();

        if(email || user){

          $.ajax({
              url: url,
              type: 'POST',
              data: data,
              dataType: 'json',
              processData: false,
              contentType: false,
              cache: false,
              beforeSend: function() {
                  $($this).find('button[type="submit"]').prop('disabled', true);
              },
              success: function(result){
                  $($this).find('button[type="submit"]').prop('disabled', false);

                  if(result.status == true){
                      $('#modal-forward-to').modal('hide');
                      $($this)[0].reset();

                      // if(reload_forward_to){
                        loadAllMail();
                      // }
                      sweetAlert("Mail wurde erfolgreich gesendet");

                  }else{
                      alert(result.message);
                  }
              },
              error: function(error){
                  $($this).find('button[type="submit"]').prop('disabled', false);
                  alert('Somthing want wrong!');
              }
          });

        }else{
          alert('Please select user or enter email!');
        }
    });

    /*----------------JS FOR ADD PROPERTY INVOICE - END-----------------*/

    /*----------------JS FOR ADD DEFAULT PAYER - START------------------*/
    $(document).on('click', ".default-payer-upload-file-icon", function() {
        $("#default_payer_gdrive_file_upload").trigger('click');
    });
    $(document).on('click', ".default-payer-upload-file-icon2", function() {
        $("#default_payer_gdrive_file_upload2").trigger('click');
    });
    $(document).on('change', "#default_payer_gdrive_file_upload", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span").text(fileName);
    });
    $(document).on('change', "#default_payer_gdrive_file_upload2", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span2").text(fileName);
    });
    $(document).on('click', ".link-button-gdrive-default-payer", function(e) {
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('default_payer');
        $("#select-gdrive-file-callback").val('gdeiveSelectDefaultPayer');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_name_payer").val());
    });
    window.gdeiveSelectDefaultPayer = function(basename, dirname, curElement, dirOrFile) {
        $("#default_payer_gdrive_file_basename").val(basename);
        $("#default_payer_gdrive_file_dirname").val(dirname);
        $("#default_payer_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#default_payer_gdrive_file_name").val(text);
        $("#default_payer_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }
    $('#add_default_payer_form').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var msgElement = $('#add_default_payer_msg');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_add_default_payer,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $this.find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);

            },
            success: function(result) {
                $this.find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status == true) {
                    showFlash($('#default_payer_msg'), result.message, 'success');
                    $this[0].reset();
                    $("#default_payer_gdrive_file_name_span").text("");
                    $('#default_payer_table').DataTable().ajax.reload();
                    setTimeout(function() {
                        $('#add_default_payer_modal').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                        $('#year-select').trigger('change');
                        // location.reload()
                    }, 1000);
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                $this.find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({
                    error
                });
            }
        });
    });
    var default_payer_table = $('#default_payer_table').dataTable({
        "ajax": url_get_default_payer,
        "order": [
            [6, 'desc']
        ],
        "columns": [
            null,
            // null,
            // null,
            null,
            null,
            null,
            null, {
                "type": "new-date-time"
            },
            // null,
            null,
        ]
    });

    $('body').on('click', '.btn-delete-default-payer', function() {
        var url = $(this).attr('data-url');
        var msgElement = $('#default_payer_msg');
        if (confirm('Are you sure want to delete this default payer?')) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');
                        $('#default_payer_table').DataTable().ajax.reload();
                        $('#year-select').trigger('change');
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });
    /*----------------JS FOR ADD DEFAULT PAYER - END------------------*/

    /*----------------JS FOR ADD PROPERTY DEALS - START----------------*/

	/*$(document).on('click', ".property-deal-upload-file-icon", function(){
		$("#property_deal_gdrive_file_upload").trigger('click');
	});

	$(document).on('change', "#property_deal_gdrive_file_upload", function(e){
	    var fileName = e.target.files[0].name;
	    $("#property_deal_gdrive_file_name_span").text(fileName);
	});

	$(document).on('click', ".link-button-gdrive-property-deal", function(e){
		$("#select-gdrive-file-model").modal('show');
		$("#select-gdrive-file-type").val('property_deals');
		$("#select-gdrive-file-callback").val('gdeiveSelectPropertyDeal');
		$("#select-gdrive-file-data").val('');
		selectGdriveFileLoad(workingDir);
	});

	window.gdeiveSelectPropertyDeal = function(basename, dirname,curElement, dirOrFile){
	    $("#property_deal_gdrive_file_basename").val(basename);
	    $("#property_deal_gdrive_file_dirname").val(dirname);
	    $("#property_deal_gdrive_file_type").val(dirOrFile);
	    var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
	    $("#property_deal_gdrive_file_name").val(text);
	    $("#property_deal_gdrive_file_name_span").text(text);
	    $("#select-gdrive-file-model").modal('hide');
	    // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
	}

	$('#add_property_deal_form').submit(function(event) {
		event.preventDefault();

		var $this = $(this);
		var msgElement = $('#add_property_deal_msg');
		var formData = new FormData($(this)[0]);

		$.ajax({
	  		url: url_add_property_deal,
	  		type: 'POST',
	  		data: formData,
	  		dataType: 'json',
	  		processData: false,
			contentType: false,
			cache : false,
			beforeSend: function() {
				//$($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
				$($this).find('button[type="submit"]').prop('disabled', true);
			},
	  		success: function(result){
	  			//$($this).find('button[type="submit"]').html('Speichern');
				$($this).find('button[type="submit"]').prop('disabled', false);

				if(result.status == true){
					showFlash($('#property_deal_msg'), result.message, 'success');
					$($this)[0].reset();
					$('#property_deal_table').DataTable().ajax.reload();
					setTimeout(function(){
						$('#add_property_deal_modal').modal('hide');
					}, 1000);
				}else{
					showFlash(msgElement, result.message, 'error');
				}
	  		},
	  		error: function(error){
	  			//$($this).find('button[type="submit"]').html('Speichern');
				$($this).find('button[type="submit"]').prop('disabled', false);
				showFlash(msgElement, 'Somthing want wrong!', 'error');
	      		console.log({error});
	  		}
		});
	});

	var property_deal_table = $('#property_deal_table').dataTable({
		"ajax": url_get_property_deals,
		"order": [[ 7, 'desc' ]],
		"columns": [
			null,
			null,
			{ "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
			null,
			null,
			{ "type": "new-date-time" },
            null,
			null,
            null,
            null,
            null,
		]
	});

    var pending_property_deal_table = $('#pending_property_deal_table').dataTable({
        "ajax": url_get_pending_deals,
        "order": [[ 7, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
        ]
    });

    var not_release_property_deal_table = $('#not_release_property_deal_table').dataTable({
        "ajax": url_get_not_release_deals,
        "order": [[ 7, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
        ]
    });

    var deal_mail_table = $('#deal_mail_table').dataTable({
        "ajax": url_get_deal_logs,
        "order": [
            [4, "desc"]
        ],
        "columns": [
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            {"type": "new-date"},
        ]
    });

    $('body').on('click', '.btn-delete-property-deal', function() {
        var url = $(this).attr('data-url');
        var msgElement = $('#property_deal_msg');
        if (confirm('Are you sure want to delete this deal?')) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');
                        $('#property_deal_table').DataTable().ajax.reload();
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });

    var not_release_id;
    $('body').on('click', '.deal-mark-as-not-release', function() {
        not_release_id = $(this).attr('data-id');
        $('#deal_mark_as_not_release').modal('show');
    });

    $('#form_mark_as_not_release').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': not_release_id,
            'comment': $(this).find('#input_deal_mark_as_not_release').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#deal_mark_as_not_release_error');
        $.ajax({
            url: url_deal_mark_as_notrelease,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#deal_mark_as_not_release').modal('hide');
                    $($this)[0].reset();
                    $('#property_deal_table').DataTable().ajax.reload();
                    $('#not_release_property_deal_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var pending_id;
    $('body').on('click', '.deal-mark-as-pending', function() {
        pending_id = $(this).attr('data-id');
        $('#deal_mark_as_pending').modal('show');
    });

    $('#form_mark_as_pending').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': pending_id,
            'comment': $(this).find('#input_deal_mark_as_pending').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#deal_mark_as_pending_error');
        $.ajax({
            url: url_deal_mark_as_pending,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#deal_mark_as_pending').modal('hide');
                    $($this)[0].reset();
                    $('#property_deal_table').DataTable().ajax.reload();
                    $('#pending_property_deal_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });*/

	/*----------------JS FOR ADD PROPERTY DEALS - END------------------*/

    /*--------------------Js FOR CONTRACT START------------------------*/
    $('body').on('click', '#btn-add-contract', function() {
        var modal = $('#add_contracts_modal');
        var property_id = $('#selected_property_id').val();

        $(modal).find("input[name='id']").val('');
        $(modal).find("input[name='property_id']").val(property_id);

        $(modal).find('#contract_file_div').show();
        $(modal).find('#contract_comment_div').show();

        $(modal).modal('show');
    });

    $('body').on('click', '.edit-contract', function() {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        var modal = $('#add_contracts_modal');

        $(modal).find('#contract_file_div').hide();
        $(modal).find('#contract_comment_div').hide()

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    $(modal).find("input[name='id']").val(result.data.id);
                    $(modal).find("input[name='property_id']").val(result.data.property_id);
                    $(modal).find("input[name='amount']").val(result.data.amount);
                    $(modal).find("input[name='date']").val(result.data.date);
                    $(modal).find("input[name='completion_date']").val(result.data.completion_date);
                    $(modal).find("input[name='termination_date']").val(result.data.termination_date);
                    $(modal).find("input[name='termination_am']").val(result.data.termination_am);
                    $(modal).find("select[name='category']").val(result.data.category);
                    $(modal).find("input[name='is_old']").prop('checked', (result.data.is_old == '1') ? true : false);
                }else{
                    alert(result.message);
                }
            },
            error: function(){
                alert('Somthing want wrong!');
            }
        });

        // $('#add_insurance_tab_detail_modal').find('#form_add_insurance_tab_detail').attr('action', url_add_property_insurance_detail+'/'+id);
        $(modal).modal('show');
    });

    $(document).on('click', ".contracts-upload-file-icon", function(){
        $("#contracts_gdrive_file_upload").trigger('click');
    });

    $(document).on('change', "#contracts_gdrive_file_upload", function(e){
        var fileName = e.target.files[0].name;
        $("#contracts_gdrive_file_name_span").text(fileName);
    });

    $(document).on('click', ".link-button-gdrive-contracts", function(e){
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('property_deals');
        $("#select-gdrive-file-callback").val('gdeiveSelectContracts');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_name").val());
    });

    window.gdeiveSelectContracts = function(basename, dirname,curElement, dirOrFile){
        $("#contracts_gdrive_file_basename").val(basename);
        $("#contracts_gdrive_file_dirname").val(dirname);
        $("#contracts_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#contracts_gdrive_file_name").val(text);
        $("#contracts_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    $('#add_contracts_form').submit(function(event) {
        event.preventDefault();

        var $this = $(this);
        var msgElement = $('#add_contracts_msg');
        var formData = new FormData($(this)[0]);
        var is_old = ($('#is_old').prop('checked')==true) ? true : false;

        $.ajax({
            url: url_add_contract,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache : false,
            beforeSend: function() {
                //$($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                //$($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){

                    $($this)[0].reset();
                    $('#contracts_gdrive_file_basename').val('');
                    $('#contracts_gdrive_file_dirname').val('');
                    $('#contracts_gdrive_file_type').val('');
                    $('#contracts_gdrive_file_name').val('');
                    $('#contracts_gdrive_file_name_span').text('');
                    $('#contracts_gdrive_file_upload').val('');

                    $('#add_contracts_modal').modal('hide');

                    if(is_old){
                        $('#old_contracts_table').DataTable().ajax.reload();
                    }else{
                        $('#contracts_table').DataTable().ajax.reload();
                    }

                    showFlash($('#contracts_msg'), result.message, 'success');
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                //$($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    $('body').on('click', '.btn-delete-contract', function() {
        var url = $(this).attr('data-url');
        var msgElement = $('#contracts_msg');
        var type = $(this).attr('data-type');

        if (confirm('Are you sure want to delete this contract?')) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');

                        if(type == 'old_contract'){
                            $('#old_contracts_table').DataTable().ajax.reload();
                        }else{
                            $('#contracts_table').DataTable().ajax.reload();
                        }
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });

    var contract_not_release_id;
    $('body').on('click', '.contract-mark-as-not-release', function() {
        contract_not_release_id = $(this).attr('data-id');
        $('#contracts_mark_as_not_release').modal('show');
    });

    $('#form_contracts_mark_as_not_release').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_not_release_id,
            'comment': $(this).find('#input_contracts_mark_as_not_release').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#contracts_mark_as_not_release_error');
        $.ajax({
            url: url_contract_mark_as_notrelease,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_not_release').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                    $('#not_release_contracts_table').DataTable().ajax.reload();
                    $('#contracts_mail_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var contract_not_release_am_id;
    $('body').on('click', '.contract-mark-as-not-release-am', function() {
        contract_not_release_am_id = $(this).attr('data-id');
        $('#contracts_mark_as_not_release_am').modal('show');
    });

    $('#form_contracts_mark_as_not_release_am').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_not_release_am_id,
            'comment': $(this).find('#input_contracts_mark_as_not_release_am').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#contracts_mark_as_not_release_am_error');
        $.ajax({
            url: url_contract_mark_as_notrelease_am,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_not_release_am').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                    $('#not_release_am_contracts_table').DataTable().ajax.reload();
                    $('#contracts_mail_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var contract_pending_id;
    $('body').on('click', '.contract-mark-as-pending', function() {
        contract_pending_id = $(this).attr('data-id');
        $('#contracts_mark_as_pending').modal('show');
    });

    $('#form_contract_mark_as_pending').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_pending_id,
            'comment': $(this).find('#input_contracts_mark_as_pending').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#contracts_mark_as_pending_error');
        $.ajax({
            url: url_contract_mark_as_pending,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_pending').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                    $('#pending_contracts_table').DataTable().ajax.reload();
                    $('#contracts_mail_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var contract_vacant_release_type;
    var contract_vacant_id;

    $('body').on('click', '.contract-release-request', function() {
        contract_vacant_release_type = $(this).attr('data-column');
        contract_vacant_id = $(this).attr('data-id');
        $('.contract-release-comment').val("")
        $('#contract-release-property-modal').modal();
    });

    $('body').on('click', '.contract-release-submit', function() {
        comment = $('.contract-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_contractreleaseprocedure,
            data: {
                id: contract_vacant_id,
                step: contract_vacant_release_type,
                _token: _token,
                comment: comment,
            },
            success: function(data) {
                $('#contracts_table').DataTable().ajax.reload();
                $('#contracts_mail_table').DataTable().ajax.reload();
            }
        });
    });

    var contracts_table = $('#contracts_table').dataTable({
        "ajax": url_get_contract,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    var pending_contracts_table = $('#pending_contracts_table').dataTable({
        "ajax": url_get_pending_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            // null,
            null,
        ]
    });

    var release_contracts_table = $('#release_contracts_table').dataTable({
        "ajax": url_get_release_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            // null,
            null,
        ]
    });

    var old_contracts_table = $('#old_contracts_table').dataTable({
        "ajax": url_get_old_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            null,
            null,
        ]
    });

    var not_release_am_contracts_table = $('#not_release_am_contracts_table').dataTable({
        "ajax": url_get_not_release__am_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            // null,
            null,
        ]
    });

    var not_release_contracts_table = $('#not_release_contracts_table').dataTable({
        "ajax": url_get_not_release_contracts,
        "order": [[ 9, 'desc' ]],
        "columns": [
            null,
            null,
            { "type": "numeric-comma" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            { "type": "new-date" },
            null,
            null,
            { "type": "new-date-time" },
            null,
            // null,
            null,
        ]
    });

    var contracts_mail_table = $('#contracts_mail_table').dataTable({
        "ajax": url_get_contract_logs,
        "order": [
            [4, "desc"]
        ],
        "columns": [
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            {"type": "new-date"},
            null,
        ]
    });

    /*--------------------Js FOR CONTRACT END--------------------------*/

    /*--------------------JS FOR PROPERTY INSURANCE TAB START-----------------*/

    $('#form_add_insurance_tab_title').submit(function(event) {
        event.preventDefault();

        var $this = $(this);
        var msgElement = $('#add_insurance_tab_title_msg');
        var url = $(this).attr('action');
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache : false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $($this)[0].reset();
                    $('#add_insurance_tab_title_modal').modal('hide');
                    showFlash($('#insurance_tab_msg'), result.message, 'success');

                    var title_delete_url = url_delete_property_insurance_title.replace(':id', result.data.id);

                    var html = $('#insurance-tab-hidden-html').html();
                    html = html.replace(/##ID##/g, result.data.id);
                    html = html.replace('##TITLE##', result.data.title);
                    html = html.replace('##TITLE-DELETE-URL##', title_delete_url);
                    html = html.replace('hidden-table', '');
                    $('#insurance-table-content').append(html);

                    if(is_falk==0)
                        getAngebotButton();

                    makeInsuranceDataTable(result.data.id);

                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    $('body').on('click', '.delete-title', function() {
        var url = $(this).attr('data-url');

        var $this = $(this);
        var msgElement = $('#insurance_tab_msg');

        if(confirm('Are you sure want to delete this title?')){
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if(result.status){
                        $($this).closest('.main_card').remove();
                        $('#insurance-tab-log').DataTable().ajax.reload();
                    }else{
                      showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error){
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                    console.log({error});
                }
            });
        }
        return false;

    });

    $('body').on('click', '.btn-insurance-tab-detail', function() {
        var tab_id = $(this).attr('data-tabid');
        if(tab_id != ''){
            $('#add_insurance_tab_detail_modal').find('#form_add_insurance_tab_detail').attr('action', url_add_property_insurance_detail);
            $('#add_insurance_tab_detail_modal').find('#property_insurance_tab_id').val(tab_id);
            $('#add_insurance_tab_detail_modal').modal('show');
        }else{
           showFlash($('#insurance_tab_msg'), 'Somthing want wrong!', 'error');
        }
    });

    $('#form_add_insurance_tab_detail').submit(function(event) {
        event.preventDefault();

        var $this = $(this);
        var msgElement = $('#add_insurance_tab_detail_msg');
        var url = $(this).attr('action');
        var formData = new FormData($(this)[0]);
        formData.append("property_id", current_property_id);

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache : false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){

                    $($this)[0].reset();
                    $('#insurance_tab_gdrive_file_basename').val('');
                    $('#insurance_tab_gdrive_file_dirname').val('');
                    $('#insurance_tab_gdrive_file_type').val('');
                    $('#insurance_tab_gdrive_file_name').val('');
                    $('#insurance_tab_gdrive_file_name_span').text('');
                    $('#insurance_tab_gdrive_file_upload').val('');

                    $('#add_insurance_tab_detail_modal').modal('hide');
                    showFlash($('#insurance_tab_msg'), result.message, 'success');
                    $('#insurance-table-'+result.data.property_insurance_tab_id).DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    if($(".tbl-insurance-tab-detail:not(.hidden-table)").length > 0){
        $(".tbl-insurance-tab-detail:not(.hidden-table)").each(function(){
            var tabid = $(this).attr('data-tabid');
            makeInsuranceDataTable(tabid);
        });
    }

    $('body').on('click', '.edit-insurance-tab-detail', function() {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result){
                    // $('#add_insurance_tab_detail_modal').find("input[name='name']").val(result.name);
                    $('#add_insurance_tab_detail_modal').find("input[name='amount']").val(result.amount);
                    $('#add_insurance_tab_detail_modal').find("textarea[name='comment']").val(result.comment);
                }
            },
        });

        $('#add_insurance_tab_detail_modal').find('#form_add_insurance_tab_detail').attr('action', url_add_property_insurance_detail+'/'+id);
        $('#add_insurance_tab_detail_modal').modal('show');
    });

    $('body').on('click', '.delete-insurance-tab-detail', function() {
        var tab_id = $(this).closest('table').attr('data-tabid');
        var url = $(this).attr('data-url');
        var msgElement = $('#insurance_tab_msg');

        if(confirm('Are you sure want to delete this record?')){
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if(result.status){
                        $('#insurance-table-'+tab_id).DataTable().ajax.reload();
                        $('#insurance-tab-log').DataTable().ajax.reload();
                    }else{
                      showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error){
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                    console.log({error});
                }
            });
        }
        return false;
    });

    $('body').on('change', '.am_falk_status', function() {

        var field = $(this).attr('data-field');
        var status = ($(this).prop('checked')) ? 1 : 0;
        var url = $(this).attr('data-url');
        var msgElement = $('#insurance_tab_msg');

        $.ajax({
            url: url,
            type: 'POST',
            data:{
                'field' : field,
                'status' : status
            },
            dataType: 'json',
            success: function(result) {
                if(!result.status){
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });

    });

    $('body').on('click', '.property-insurance-release-request', function() {

        if(is_falk==1){
            // var length = $(this).closest('.modal-body').find('table').find('[data-field="falk_status"]:checked').length;
            // var msg = "Bitte Checkbox Falk auswählen!";
            length = 1;
        }else{
            var length = $(this).closest('.modal-body').find('table').find('[data-field="asset_manager_status"]:checked').length;
            var msg = "Bitte eine AM Checkbox auswählen!";
        }

        if(length > 0){
            vacant_release_type = $(this).attr('data-column');
            vacant_id = $(this).attr('data-id');
            $('.property-insurance-comment').val("")
            $('#property-insurance-release-property-modal').modal();
        }else{
            alert(msg); return false;
        }
    });

    $('body').on('click', '.property-insurance-submit', function() {
        am_id = fk_id = 0;
        if(is_falk==0)
        {
            $('#insurance-table-'+vacant_id+' .am_falk_status').each(function(){
                    if($(this).is(':checked') && $(this).attr('data-field')=="falk_status")
                        fk_id = $(this).attr('data-id');
                    if($(this).is(':checked') && $(this).attr('data-field')=="asset_manager_status")
                        am_id = $(this).attr('data-id');
            });
        }
        comment = $('.property-insurance-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_dealreleaseprocedure,
            data: {
                id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                am_id:am_id,
                fk_id:fk_id,
                comment: comment,
            },
            success: function(data) {
                if(vacant_release_type=="insurance_request")
                    getAngebotButton();
                $('#insurance-tab-log').DataTable().ajax.reload();
                $('.sec-'+vacant_id).find('.btn').hide();
                $('.sec-'+vacant_id).find('table .btn').show();
                $('.sec-'+vacant_id).find('.am_falk_status').each(function(){
                    $(this).removeClass('am_falk_status');
                });
                // $('#property_deal_table').DataTable().ajax.reload();
                // $('#deal_mail_table').DataTable().ajax.reload();
            }
        });
    });

    $(document).on('click', ".insurance-tab-upload-file-icon", function(){
        $("#insurance_tab_gdrive_file_upload").trigger('click');
    });

    $(document).on('change', "#insurance_tab_gdrive_file_upload", function(e){
        var fileName = e.target.files[0].name;
        $("#insurance_tab_gdrive_file_name_span").text(fileName);
    });

    $(document).on('click', ".link-button-gdrive-insurance-tab", function(e){
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('property_deals');
        $("#select-gdrive-file-callback").val('gdeiveSelectPropertyInsurance');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir, $("#current_tab_name_insurance_tab").val());
    });

    window.gdeiveSelectPropertyInsurance = function(basename, dirname,curElement, dirOrFile){
        $("#insurance_tab_gdrive_file_basename").val(basename);
        $("#insurance_tab_gdrive_file_dirname").val(dirname);
        $("#insurance_tab_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').attr('title');
        $("#insurance_tab_gdrive_file_name").val(text);
        $("#insurance_tab_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    var insurance_tab_log = $('#insurance-tab-log').dataTable({
        "ajax": url_get_property_insurance_detail_log,
        "order": [
            [6, "desc"]
        ],
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            null,
        ]
    });

    /*--------------------JS FOR PROPERTY INSURANCE TAB START-----------------*/


    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        var tab = $(this).attr('data-active');

        if(typeof tab !== 'undefined' && tab != ''){
            var url = window.location.href;
            if(url.indexOf('?')){
                var urlArray = url.split("?");
                url = urlArray[0];
            }
            url = url+'?tab='+tab;
            history.pushState(null, null, url);
        }
    });

    $('.inline-edit-schl').editable({
        prepend: "not selected",
        source: [{
            value: '3 Monate',
            text: '3 Monate'
        }, {
            value: '4 Wochen',
            text: '4 Wochen'
        }],
    });

    getRentPaidData();

    $(document).on('click', ".rent-paid-month-wise-detail", function(e){
        var url = $(this).attr('data-url');
        var title = $(this).text();
        $('#rent-paid-month-wise-detail-modal').find('.modal-title').text(title);
        $('#rent-paid-month-wise-detail-modal').modal('show');
        $.ajax({
           url: url,
        }).done(function (data) {
           $('#rent-paid-month-wise-detail-div').html(data);
           var columns = [
            null,
            null,
            null,
            null,
            { "type": "numeric-comma"},
            { "type": "numeric-comma"},
            null,
            { "type": "numeric-comma"},
            {"type": "new-date-time"},
            {"type": "new-date-time"},
            null,
            null,
            null,
            null,
        ];
            // makeDatatable($('#rent-paid-table'), columns);

            makeDatatable($('#rent-paid-table-popup'), columns);
        });
    });

});

function getRentPaidData(){

    var month = $('select[name="upload_month"]#year-select').val();

    $.ajax({
       url: url_get_rent_paid_data+'?month='+month,
    }).done(function (data) {
        $('#rent-paid-data').html(data);
        var columns = [
            null,
            null,
            null,
            null,
            null,
            { "type": "numeric-comma"},
            { "type": "numeric-comma"},
            null,
            { "type": "numeric-comma"},
            {"type": "new-date-time"},
            {"type": "new-date-time"},
            null,
            null,
            null,
            null,
        ];
        makeDatatable($('#rent-paid-table'), columns);
        /*var columns = [
            null,
            null,
            {"type": "new-date-time"}
        ];
        makeDatatable($('#rent-paid-upload-table'), columns, 'desc', 2);*/

    });


    $.ajax({
       url: url_get_rent_paid_data+'?summery=1&month='+month,
    }).done(function (data) {
        $('#rent-paid-data-summery').html(data);
        var columns = [
            null,
            { "type": "numeric-comma"},
            { "type": "numeric-comma"},
            { "type": "numeric-comma"},
        ];
        makeDatatable($('.summery-table'), columns);
    });

}

function makeInsuranceDataTable(tabid){
    var url = url_get_property_insurance_detail.replace(":tab_id", tabid);
    var title = $('#insurance-table-'+tabid).attr('data-type');
    var sub = $('#insurance-table-'+tabid).attr('data-sub');

    // var dom = '<lf<tr>ip>';
    var dom = "<'row'<'col-md-2'l><'col-md-3 dt_custom_info'i><'col-md-7'f>>"+
              "<'row'<'col-md-12'tr>>"+
              "<'row'<'col-md-12'p>>";

    if(title == 'OFFENE ANGEBOTE'){
        var columns = [
            null,
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            { "visible": false },
            null,
            { "visible": false },
            null,
            null,
            null,
            null,
            null,
            null,
        ];
    }else if(title == 'VON FALK FREIGEGEBEN'){
        var columns = [
            null,
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            {"type": "new-date-time"},
            null,
            { "visible": false },
            null,
            null,
            null,
            null,
            null,
            null,
        ];
        dom = "<'row'<'col-md-2'l><'col-md-3 dt_custom_info'i><'col-md-2 custom_button'><'col-md-5'f>>"+
              "<'row'<'col-md-12'tr>>"+
              "<'row'<'col-md-12'p>>";
    }else{
        var columns = [
            null,
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            { "visible": false },
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ];
    }

    var order = (title == 'VON FALK FREIGEGEBEN') ? 6 : 5;

    var instable = $('#insurance-table-'+tabid).dataTable({
        "ajax": {
            "url" : url+'?type='+sub,
            // "async": false
        },
        dom: dom,
        "order": [[order, "desc"]],
        "columns": columns,
        "initComplete": function(settings, json) {
            /*var node = instable.fnGetNodes();

            $(node).find('.btn-forward-to').each(function(){
                var s = $(this).attr('data-subject');
                var subject = sub+': '+s;

                $(this).attr('data-subject', subject);
                $(this).attr('data-title', title);
            });*/
            if(sub == 'Von Falk freigegeben'){
                $('#insurance-table-'+tabid).find('tbody tr').each(function(i){
                    /*if(i != 0){
                        $(this).addClass('hidden');
                    }*/
                    var release_date = $.trim($(this).find('td:eq(6)').text());
                    if(!release_date){
                        $(this).addClass('hidden');
                    }
                });
                $('#insurance-table-'+tabid+'_paginate').addClass('hidden');
                $('#insurance-table-'+tabid).closest('.dataTables_wrapper').find('.custom_button').html('<button type="button" class="btn btn-primary btn-xs btn-show-more-record">Weitere Angebote anzeigen</button>');
            }
        },
        "drawCallback": function( settings ) {
            $('#insurance-table-'+tabid).find('.btn-forward-to').each(function(){
                var s = $(this).attr('data-subject');
                var subject = sub+': '+s;

                $(this).attr('data-subject', subject);
                $(this).attr('data-title', title);
            });
        }
    });
}

$('body').on('click', '.btn-show-more-record', function(){
    var text = $(this).text();
    var tableid = $(this).closest('.modal-body').find('table').attr('id');


    if(text == 'Weitere Angebote anzeigen'){
        $(this).text('Freigegebenes Angebot anzeigen');
        $('#'+tableid).find('tbody tr').removeClass('hidden');
        $('#'+tableid+'_paginate').removeClass('hidden');
    }else{
        $(this).text('Weitere Angebote anzeigen');
        // $('#'+tableid).find('tbody tr').not(':first').addClass('hidden');
        $('#'+tableid).find('tbody tr').each(function(i){
            var release_date = $.trim($(this).find('td:eq(6)').text());
            if(!release_date){
                $(this).addClass('hidden');
            }
        });

        $('#'+tableid+'_paginate').addClass('hidden');
    }
});

function getsheetcomment(show) {
    $('.propertyappenddata').html("");
    if (show == 1) lab = 'show all';
    else lab = 'show less';
    id = $('#selected_property_id').val();
    $.ajax({
        type: 'GET',
        url: url_properties_comment_get_comments,
        data: {
            id: id,
            show: show
        },
        success: function(data) {
            i = 0
            $.each(data, function(key) {
                if (data[key].comment != "") {
                    if (i == 0) {
                        // $('.sheet-comment').html(data[key].comment);
                    }
                    i = 1;
                    $('.propertyappenddata').append('<tr class="sec"><td>' + data[key].name + '</td>' + '<td>' + data[key].comment + '</td>' + '<td>' + data[key].f_date + '</td><td class="pull-right"><a href="' + '/properties-comment/delete-sheet-comment' + '/' + data[key].id + '" class="btn-xs btn-danger delete-sheet-cm" data-type="" data-pk="" data-url="" data-title="">Löschen</a></td>' + '</td></tr>');
                }
            });
            if (i == 1) {
                if (show == 1) $('.propertyappenddata').append("<button style='margin:10px;' type='button' class='btn-sm btn show-all-p'>" + lab + "</button>");
                else $('.propertyappenddata').append("<button style='margin:10px;' type='button' class='btn-sm btn show-less-p'>" + lab + "</button>");
            }
        }
    });
}

function getrelbutton(id) {
    $.ajax({
        url: url_getReleasebutton,
        type: "get",
        dataType: 'json',
        data: {
            id: id
        },
        success: function(response) {
            $('.tenant-release-buttons-' + id).html(response.buttons);
            $('.tenant-release-logs-' + id).html(response.logs);
            $('.release-status-' + id).html($('.tenant-release-buttons-' + id).find('.f-release').html());
        }
    });
}

function getprovisionbutton() {
    var id = $('#selected_property_id').val();
    $.ajax({
        url: url_getProvisionbutton,
        type: "get",
        dataType: 'json',
        data: {
            id: id
        },
        success: function(response) {
            $('.provision_info-table').html(response.table);
            $('.provision_release_info-table').html(response.table1);
            // $.each(response.buttons1, function(key1, value1) {
            //     $('.tenantbtn1-' + key1).html(value1);
            // });
            if(typeof response.buttons2 !== 'undefined'){
                $.each(response.buttons2, function(key1, value1) {
                    if($('.tenantbtn2-' + key1).length){
                        $('.tenantbtn2-' + key1).html(value1);
                    }
                });
            }

            $('.provision-release-logs').html(response.logs);
            setTimeout(function() {

                makeDatatable($('#provisiondatatable'));

                makeDatatable($('#release-provisiondatatable'));

                // makeDatatable($('#rdatatable'), columns);
            }, 100);
            // makeDatatable($('#provisiondatatable'));
        }
    });
}
var elem = document.documentElement;

function openFullscreen() {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
}

function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
    }
}

function preview_image() {
    var formData = new FormData();
    $.each(jQuery('#images')[0].files, function(i, file) {
        formData.append('file[' + i + ']', file);
    });
    $.ajax({
        url: url_uploadfiles, //Server script to process data
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(json) {
            $('#images').val('')
            $.each(json.files, function(index, value) {
                var str = '<input type="hidden" name="img[]" value="' + json.filename[index] + '">';
                $('#image_preview').append("<div>" + str + "<img src='" + value + "' style='width:150px; height:149px; object-fit:cover; float:left; margin-top: 6px; margin-right: 10px;' ></div>");
            });
        }
    });
}

function preview_image3() {
    var formData = new FormData();
    $.each(jQuery('#images3')[0].files, function(i, file) {
        formData.append('file[' + i + ']', file);
    });
    $.ajax({
        url: url_uploadfiles, //Server script to process data
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(json) {
            $('#images3').val('')
            $.each(json.files, function(index, value) {
                var str = '<input type="hidden" name="img[]" value="' + json.filename[index] + '">';
                $('#image_preview3').append("<div>" + str + "<img src='" + value + "' style='width:150px; height:149px; object-fit:cover; float:left; margin-top: 6px; margin-right: 10px;' ></div>");
            });
        }
    });
}

function preview_image2() {
    var formData = new FormData();
    $.each(jQuery('#images2')[0].files, function(i, file) {
        formData.append('file[' + i + ']', file);
    });
    $.ajax({
        url: url_uploadfiles, //Server script to process data
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(json) {
            $('#images2').val('')
            $.each(json.files, function(index, value) {
                var str = '<input type="hidden" name="img[]" value="' + json.filename[index] + '">';
                $('#image_preview2').append("<div>" + str + "<img src='" + value + "' style='width:150px; height:149px; object-fit:cover; float:left; margin-top: 6px; margin-right: 10px;' ></div>");
            });
        }
    });
}

function validate() {
    Error = 0;
    $("#sendemailtobankers .required").each(function() {
        if (!$.trim($(this).val())) {
            var placeholder = $(this).attr('placeholder');
            $(this).focus();
            toastr.error("Field " + placeholder + " is required");
            $('html, body').animate({
                scrollTop: 0
            }, 500);
            Error = 1;
            return false;
        }
    });
    return Error;
}

function bank_list() {
    property_id = $('#selected_property_id').val();
    var url = url_getbanklist + "?id=" + property_id;
    $.ajax({
        url: url,
    }).done(function(data) {
        $('.list-bank-div').html(data);

    });
}





function statusloi_list() {
    property_id = $('#selected_property_id').val();
    var url = url_getstatusloi + "?id=" + property_id;
    $.ajax({
        url: url,
    }).done(function(data) {
        $('.list-statusloi-div').html(data);
        makeDatatable($('#list-statusloi'));
    });
}

function showModalGebäude(properties) {
    $('#versicherungen_type').val(1);
    $('#versicherunge_name').val(properties.axa);
    $('#versicherunge_betrag').val(properties.gebaude_betrag);
    $('#versicherunge_laufzeit').val(properties.gebaude_laufzeit_from);
    $('#versicherunge_laufzeit_end').val(properties.gebaude_laufzeit_to);

    if(properties.gebaude_kundigungsfrist == '3 Monate' || properties.gebaude_kundigungsfrist == '4 Wochen'){
        $('#versicherunge_kündigungsfrist').val(properties.gebaude_kundigungsfrist);
        $('#versicherunge_kündigungsfrist_dup').text('');
    }else{
        $('#versicherunge_kündigungsfrist').val('');
        $('#versicherunge_kündigungsfrist_dup').text(properties.gebaude_kundigungsfrist);
    }
    // $('#versicherunge_kündigungsfrist').val(properties.gebaude_kundigungsfrist);

    $('#versicherunge_kommentar').val(properties.gebaude_comment);
    $('#edit_versicherungen').modal('show');
}

function showModalHaftplicht(properties) {
    $('#versicherungen_type').val(2);
    $('#versicherunge_name').val(properties.allianz);
    $('#versicherunge_betrag').val(properties.haftplicht_betrag);
    $('#versicherunge_laufzeit').val(properties.haftplicht_laufzeit_from);
    $('#versicherunge_laufzeit_end').val(properties.haftplicht_laufzeit_to);

    if(properties.haftplicht_kundigungsfrist == '3 Monate' || properties.haftplicht_kundigungsfrist == '4 Wochen'){
        $('#versicherunge_kündigungsfrist').val(properties.haftplicht_kundigungsfrist);
        $('#versicherunge_kündigungsfrist_dup').text('');
    }else{
        $('#versicherunge_kündigungsfrist').val('');
        $('#versicherunge_kündigungsfrist_dup').text(properties.haftplicht_kundigungsfrist);
    }

    // $('#versicherunge_kündigungsfrist').val(properties.haftplicht_kundigungsfrist);

    $('#versicherunge_kommentar').val(properties.haftplicht_comment);
    $('#edit_versicherungen').modal('show');
}

function showModalInsouranceModal(insurance) {
    $('#versicherungen_type').val(insurance.type);
    $('#versicherungen_insurance_id').val(insurance.id);
    $('#versicherunge_name').val(insurance.name);
    $('#versicherunge_betrag').val(insurance.amount);
    $('#versicherunge_laufzeit').val(insurance.date_from);
    $('#versicherunge_laufzeit_end').val(insurance.date_to);

    if(insurance.kundigungsfrist == '3 Monate' || insurance.kundigungsfrist == '4 Wochen'){
        $('#versicherunge_kündigungsfrist').val(insurance.kundigungsfrist);
        $('#versicherunge_kündigungsfrist_dup').text('');
    }else{
        $('#versicherunge_kündigungsfrist').val('');
        $('#versicherunge_kündigungsfrist_dup').text(insurance.kundigungsfrist);
    }
    $('#versicherunge_kommentar').val(insurance.note);
    $('#edit_versicherungen').modal('show');
}

function add_versicherungen(type) {
    $('#add_versicherungen_type').val(type);
    $('#add_versicherungen').modal('show');
}

function init_manage_table() {
    var columns = [
        null, {
            "type": "numeric-comma"
        }, {
            "type": "numeric-comma"
        }, {
            "type": "numeric-comma"
        },
        null,
        null,
        null,
        null,
        null,
    ]
    makeDatatable($('.property-management-table'), columns);
}

function add_property_management(add) {
    id = $('#selected_property_id').val();
    $.ajax({
        type: 'POST',
        url: url_property_management_add,
        data: {
            id: id,
            _token: _token,
            add: add
        },
        success: function(data) {
            $('.management-table-list').html(data.table);
            init_manage_table();
        }
    });
}

function delete_property_management(id) {
    if (confirm("Are you sure want to delete this?")) {
        pid = $('#selected_property_id').val();
        $.ajax({
            type: 'POST',
            url: url_property_management_add,
            data: {
                id: id,
                _token: _token,
                delete: 1,
                property_id: pid
            },
            success: function(data) {
                $('.management-table-list').html(data.table);
                init_manage_table();
            }
        });
    }
}

function loadAllStatusLoiByUser(user_id) {
    var url = url_getStatusLoiByUserId + "?id=" + user_id + "&property_id=" + $('#selected_property_id').val();
    $.ajax({
        url: url,
    }).done(function(data) {
        $('#load_all_status_loi_by_user_content').html('');
        $('#load_all_status_loi_by_user_content').html(data);
        $('#load_all_status_loi_by_user').modal('show');
    });
}

function showResult(index) {
    if (resultData[index]) {
        var tblhtml = '';
        $.each(resultData[index], function(k, v) {
            tblhtml += "<tr><td colspan='2' bgcolor='aliceblue'><b>" + k + "</b></td></tr>";
            $.each(v, function(k1, v1) {
                if (k1 == 0) {
                    tblhtml += "<tr><td colspan='2'>" + v1 + "</td></tr>";
                } else {
                    tblhtml += "<tr><td>" + k1 + ":</td><td>" + v1 + "</td></tr>";
                }
            });
        });
        $('#result-table tbody').html(tblhtml);
        $('#result-modal').modal('show');
    } else {
        alert("Data not available");
    }
}

function deletesheet(id) {
    if (confirm("Are you sure want to delete this sheet?")) {
        $this = $(this);
        $.ajax({
            url: url_deletesheet,
            data: {
                id: id
            },
            success: function(data) {
                if (data != "1") {
                    alert(data);
                } else location.reload();
            }
        });
    }
}

$('body').on('click', '.multiple-invoice-release-request', function() {
	var selcted_id_invoice = "";
	arr = [];
	selcted_id_invoice = "";

	$('.invoice-checkbox').each(function(){
		if($(this).is(':checked'))
			arr.push($(this).attr('data-id'));
	})
	selcted_id_invoice = arr.toString();

	// console.log(selcted_id_invoice);
	if(selcted_id_invoice=="" || selcted_id_invoice==null){
		alert("Bitte mindestens eine Rechnung auswählen")
	}
	else{
    	$('.multiple-invoice-release-comment').val("")
    	$('#multiple-invoice-release-property-modal').modal();
	}

});

$('body').on('click', '.multiple-invoice-release-submit', function() {
    comment = $('.multiple-invoice-release-comment').val();
    selcted_id_invoice = "";

	$('.invoice-checkbox').each(function(){
		if($(this).is(':checked'))
			arr.push($(this).attr('data-id'));
	})
	selcted_id_invoice = arr.toString();


    $('.invoice-checkbox').each(function(){
        if($(this).is(':checked'))
            $(this).closest('tr').remove();
    })

    $.ajax({
        type: 'POST',
        url: url_property_invoicereleaseprocedure,
        data: {
            id: selcted_id_invoice,
            step: "release2",
            _token: _token,
            comment: comment,
            property_id: $('#selected_property_id').val()
        },
        success: function(data) {
            // getprovisionbutton();
            $('#add_property_invoice_table').DataTable().ajax.reload();
            $('#add_property_invoice_table2').DataTable().ajax.reload();
            $('#add_property_invoice_table4').DataTable().ajax.reload();
            $('#invoice_mail_table').DataTable().ajax.reload();
        }
    });
});

$('body').on('click', '.delete-vacant', function() {
    $this = $(this);
    id = $(this).attr('data-id');
    if (confirm('Are you sure want to delete?')) {
        $.ajax({
            url: url_delete_new_vacant,
            type: "get",
            data: {
                id: id,
            },
            success: function(response) {
                $this.closest('.col-md-6').remove();
                $('.r-sheet-'+id).remove();

            }
        });
    }
    return false;
});

String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};

function makeNumber(value){
    value = value.replaceAll(".", "");
    value = value.replaceAll(",", ".");
    value = value.replaceAll("%", "");
    value = (!isNaN(value) && value != '') ? parseFloat(value) : 0;
    return value;
}

function makeNumberFormat(value, decimal = 2){
    if(value){
        value = value.toFixed(decimal);
        value = parseFloat(value);
    }
    return value.toLocaleString("es-ES", {minimumFractionDigits: decimal});
}

$('body').on('change keyup', '#others', function() {
    var gesamt_in_eur = makeNumber($('#gesamt_in_eur').val());
    // console.log(gesamt_in_eur);
    var building = makeNumber($(this).val());
    building = gesamt_in_eur * building / 100;
    // $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
    $(this).closest('form').find('#others_amount').val( makeNumberFormat(building) );
});
$('body').on('change keyup', '#buffer', function() {
    var gesamt_in_eur = makeNumber($('#gesamt_in_eur').val());
    // console.log(gesamt_in_eur);
    var building = makeNumber($(this).val());
    building = gesamt_in_eur * building / 100;
    // $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
    $(this).closest('form').find('#buffer_amount').val( makeNumberFormat(building) );
});

$('body').on('change keyup', '#others_amount', function() {
    var gesamt_in_eur = makeNumber($('#gesamt_in_eur').val());
    // console.log(gesamt_in_eur);
    var building = makeNumber($(this).val());
    building1 = 0;
    if(gesamt_in_eur!=0)
    building1 = 100*building/gesamt_in_eur;
    // $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
    $(this).closest('form').find('#others').val( makeNumberFormat(building1, 6) );
});
$('body').on('change keyup', '#buffer_amount', function() {
    var gesamt_in_eur = makeNumber($('#gesamt_in_eur').val());
    // console.log(gesamt_in_eur);
    var building = makeNumber($(this).val());
    building1 = 0;
    if(gesamt_in_eur!=0)
    building1 = 100*building/gesamt_in_eur;
    // $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
    $(this).closest('form').find('#buffer').val( makeNumberFormat(building1, 6) );
});



$('body').on('change keyup', '.purchase_price', function() {
    var id = $(this).attr('id');

    var gesamt_in_eur = makeNumber($('#gesamt_in_eur').val());

    var building = makeNumber($('#building').val());
    var building_amount = makeNumber($('#building_amount').val());

    var plot_of_land = makeNumber($('#plot_of_land').val());
    var plot_of_land_amount = makeNumber($('#plot_of_land_amount').val());

    // console.log({id, gesamt_in_eur, building, building_amount, plot_of_land_amount, plot_of_land_amount});

    if(id == 'building'){
        if(building > 100){
            $('#building').val(100);
            building = 100;
        }
        plot_of_land = (100 - building);

        $('#plot_of_land').val( makeNumberFormat(plot_of_land) );
        $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
        $('#plot_of_land_amount').val( makeNumberFormat(gesamt_in_eur * plot_of_land / 100) );

    }else if(id == 'building_amount'){

        building =  (building_amount * 100 / gesamt_in_eur);
        plot_of_land = (100 - building);

        $('#building').val( makeNumberFormat(building) );
        $('#plot_of_land').val( makeNumberFormat(plot_of_land) );

        $('#plot_of_land_amount').val( makeNumberFormat(gesamt_in_eur * plot_of_land / 100) );

    }else if(id == 'plot_of_land'){
        if(plot_of_land > 100){
            $('#plot_of_land').val(100);
            plot_of_land = 100;
        }
        building = (100 - plot_of_land);

        $('#building').val( makeNumberFormat(building) );
        $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
        $('#plot_of_land_amount').val( makeNumberFormat(gesamt_in_eur * plot_of_land / 100) );

    }else if(id == 'plot_of_land_amount'){
        plot_of_land = (plot_of_land_amount * 100 / gesamt_in_eur);
        building = (100 - plot_of_land);

        $('#building').val( makeNumberFormat(building) );
        $('#plot_of_land').val( makeNumberFormat(plot_of_land) );

        $('#building_amount').val( makeNumberFormat(gesamt_in_eur * building / 100) );
    }

});

$('body').on('click', '#mail-an-am, #mail-an-hv-bu, #mail-an-hv-pm, #mail-an-ek, #mail-an-sb', function() {
    var title = $(this).text();
    var type = $(this).attr('data-type');
    // var user_id = $(this).attr('data-user-id');

    $('#mail_an_am_modal').find('.modal-title').text(title);
    $('#mail_an_am_modal').find('#mail_type').val(type);
    // $('#mail_an_am_modal').find('#am_id').val(user_id);
    $('#mail_an_am_modal').modal('show');
});

$('body').on('click', '.btn-add-recommended-comment', function() {
    var tenant_id = $(this).attr('data-tenant-id');
    $('#add_recommended_comment_modal').find('input[name="tenant_id"]').val(tenant_id);
    $('#add_recommended_comment_modal').modal('show');
});

$('#add_recommended_comment_form').submit(function(event) {

    event.preventDefault();
    var $this = $(this);
    var url = $(this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var tenant_id = $($this).find('input[name="tenant_id"]').val();
    var data = {'tenant_id': tenant_id, 'comment': comment};

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);
            if (result.status == true) {
                $('#add_recommended_comment_modal').modal('hide');
                $($this)[0].reset();

                var limit_text = $('#recommendation_comments_table_'+tenant_id).find('.recommendation_comments_limit').text();
                getRecommendedComment(tenant_id, (limit_text == 'Show More') ? 1 : '');

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });

});

$('body').on('click', '.recommendation_comments_limit', function() {
    var id = $(this).closest('table').attr('data-id');
    var text = $(this).text();
    var limit = '';
    if(text == 'Show More'){
        limit = '';
        $(this).text('Show Less');
    }else{
        limit = 1;
        $(this).text('Show More');
    }
    getRecommendedComment(id, limit);
});

$('body').on('click', '.delete-recommended-comment', function() {
    var url = $(this).attr('data-url');
    var id = $(this).attr('data-tenant-id');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    var limit_text = $($this).closest('table').find('.recommendation_comments_limit').text();
                    getRecommendedComment(id, (limit_text == 'Show More') ? 1 : '');
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

$('#recommended_comment_modal').on('hidden.bs.modal', function () {
    recommendedTableList();
});

loadAllCommentsTable();

function loadAllCommentsTable(){

    if($('.recommendation_comments_table').length){
        $('.recommendation_comments_table').each(function(){
            var id = $(this).attr('data-id');
            if(id){
               getRecommendedComment(id, 1);
            }
        });
    }
}

function getRecommendedComment(id, limit = ''){
    var url = url_get_recommended_comment.replace(":id", id);
    url = url+'/'+limit;

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#recommendation_comments_table_'+id).find('tbody').empty();
            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_recommended_comment.replace(":id", value.id);

                    var tr = '\
                    <tr>\
                        <td>'+ value.user_name +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td><a href="javascript:void(0);" data-url="'+ clear_url +'" data-tenant-id="'+id+'" class="btn-xs btn-danger delete-recommended-comment">Löschen</a></td>\
                    </tr>\
                    ';

                    $('#recommendation_comments_table_'+id).find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

$('body').on('click', '.load_rec_comment_section', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    var text = $(this).text();
    var limit = '';

    if(text == 'Show More'){
        $($this).text('Show Less');
        limit = '';
    }else{
        $($this).text('Show More');
        limit = 2;
    }

    var new_url = url+'/'+limit;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            $($this).closest('td').find('.show_rec_cmnt_section').empty();
            var html = '';
            if(result){
                $.each(result, function(key, value) {
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;
                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
                });
            }
            $($this).closest('td').find('.show_rec_cmnt_section').html(html);
        },
        error: function(error) {
            console.log({error});
        }
    });
});

var provision_id;
var provision_url;
$('body').on('click', '.provision-comment', function() {
    provision_url = $(this).attr('data-url');
    provision_id = $(this).attr('data-id');
    var limit_text = $('#provision_comments_limit').text();
    getProvisionComment(provision_url, (limit_text == 'Show More') ? 1 : '');
    $('#provision_comment_modal').modal('show');
});

$('#provision_comment_form').submit(function(event) {

    event.preventDefault();
    var $this = $(this);
    var url = $(this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var data = {'provision_id': provision_id, 'comment': comment};

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);
            if (result.status == true) {
                $($this)[0].reset();
                $($this).find('textarea[name="comment"]').focus();

                var limit_text = $('#provision_comments_limit').text();
                getProvisionComment(provision_url, (limit_text == 'Show More') ? 1 : '');

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });

});

$('body').on('click', '#provision_comments_limit', function() {
    var text = $(this).text();
    var limit = '';
    if(text == 'Show More'){
        limit = '';
        $(this).text('Show Less');
    }else{
        limit = 1;
        $(this).text('Show More');
    }
    getProvisionComment(provision_url, limit);
});

$('body').on('click', '.delete-provision-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    var limit_text = $('#provision_comments_limit').text();
                    getProvisionComment(provision_url, (limit_text == 'Show More') ? 1 : '');
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

function getProvisionComment(url, limit = ''){
    $.ajax({
        url: url+'/'+limit,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#provision_comments_table').find('tbody').empty();

            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_provision_comment.replace(":id", value.id);
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;

                    var tr = '\
                    <tr>\
                        <td>'+ commented_user +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td><a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger delete-provision-comment">Löschen</a></td>\
                    </tr>\
                    ';

                    $('#provision_comments_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}
$('body').on('click','#listing-banks_wrapper a,#listing-banks_wrapper .sorting,#listing-banks_wrapper .sorting_desc,#listing-banks_wrapper .sorting_asc',function(){
    $('#listing-banks').find('a.inline-edit').editable({
            success: function(response, newValue) {
                if (response.success === false) return response.msg;
                else {
                    // bank_list();
                }
            }
        });
});
recommendedTableList();
function recommendedTableList(){
    $.ajax({
        url: url_get_recommended_table,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            $('#recommended-table-div-1').html(result.table1);
            $('#recommended-table-div-2').html(result.table2);
            $('#recommended-table-div-3').html(result.table3);
            $('#recommended-table-div-4').html(result.table4);
            $('#recommended-table-div-5').html(result.table5);

            $('#recommended-table-1').DataTable();
            $('#recommended-table-2').DataTable();
            $('#recommended-table-3').DataTable();
            $('#recommended-table-4').DataTable();

            var columns = [
                null,
                null,
                null,
                {"type": "new-date-time"},
                null
            ];
            makeDatatable($('#recommended-table-5'), columns, 'desc', 3);

            if($('.recommended_last_comment').length){
              new showHideText('.recommended_last_comment', {
                  charQty     : 30,
                  ellipseText : "...",
                  moreText    : "More",
                  lessText    : " Less"
              });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

var recommended_id;
$('body').on('click', '.recommended-comment', function() {
    recommended_id = $(this).attr('data-id');
    var limit_text = $('#recommended_comments_limit').text();
    getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');
    $('#recommended_comment_modal').modal('show');
});

function getRecommendedCommentById(id, limit = ''){
    var url = url_get_recommended_comment.replace(":id", id);
    url = url+'/'+limit;

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#recommended_comments_table').find('tbody').empty();
            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_recommended_comment.replace(":id", value.id);
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-recommended-comment">Löschen</a></td>' : '';

                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;

                    var tr = '\
                    <tr>\
                        <td>'+ commented_user +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td>'+delete_button+'\
                    </tr>\
                    ';

                    $('#recommended_comments_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

$('#recommended_comment_form').submit(function(event) {

    event.preventDefault();
    var $this = $(this);
    var url = $(this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var data = {'tenant_id': recommended_id, 'comment': comment};

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);
            if (result.status == true) {
                $($this)[0].reset();
                $($this).find('textarea[name="comment"]').focus();

                var limit_text = $('#recommended_comments_limit').text();
                getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });

});

$('body').on('click', '#recommended_comments_limit', function() {
    var text = $(this).text();
    var limit = '';
    if(text == 'Show More'){
        limit = '';
        $(this).text('Show Less');
    }else{
        limit = 1;
        $(this).text('Show More');
    }
    getRecommendedCommentById(recommended_id, limit);
});

$('body').on('click', '.remove-recommended-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    var limit_text = $('#recommended_comments_limit').text();
                    getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

var vacant_release_type = vacant_id1 = "";
$('body').on('click', '.vacant-release-request', function() {
    vacant_release_type = $(this).attr('data-column');
    vacant_id1 = $(this).attr('data-id');
    $('.vacant_release_comment').val("");
    $('#vacant-release-modal').modal('show');
});

$('body').on('click', '.vacant_release_submit', function() {
    comment = $('.vacant_release_comment').val();
    $.ajax({
        type: 'POST',
        url: url_property_vacantreleaseprocedure,
        data: {
            tenant_id: vacant_id1,
            step: vacant_release_type,
            _token: _token,
            comment: comment,
            property_id: $('#selected_property_id').val()
        },
        success: function(data) {
            recommendedTableList();
            $('#vacant-release-modal').modal('hide');
        }
    });
});

var vacant_url;
$('body').on('click', '.btn-recommended-pending', function() {

    vacant_url = $(this).attr('data-url');

    $('.vacant_pending_comment').val("");
    $('#vacant-pending-modal').modal('show');
});

$('body').on('click', '.vacant_pending_submit', function() {
    var comment = $('.vacant_pending_comment').val();
    $.ajax({
        type: 'POST',
        url: vacant_url,
        data: {
            _token: _token,
            comment: comment,
            property_id: $('#selected_property_id').val()
        },
        success: function(data) {
            recommendedTableList();
            $('#vacant-pending-modal').modal('hide');
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

var vacant_notrelease_url;
$('body').on('click', '.btn-recommended-not-release', function() {

    vacant_notrelease_url = $(this).attr('data-url');

    $('.vacant_not_release_comment').val("");
    $('#vacant-not-release-modal').modal('show');
});

$('body').on('click', '.vacant_not_release_submit', function() {
    var comment = $('.vacant_not_release_comment').val();
    $.ajax({
        type: 'POST',
        url: vacant_notrelease_url,
        data: {
            _token: _token,
            comment: comment,
            property_id: $('#selected_property_id').val()
        },
        success: function(data) {
            recommendedTableList();
            $('#vacant-not-release-modal').modal('hide');
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

$('body').on('click', '.kosten_umbau', function() {
    var property_id = $(this).attr('data-property-id');
    var tenant_id = $(this).attr('data-tenant-id');
    loadKostenUmbau(property_id, tenant_id);
    $('#kosten_umbau_modal').modal('show');
});

function loadKostenUmbau(property_id, tenant_id){
    var url = url_get_kosten_umbau;
    url = url.replace(':property_id', property_id);
    url = url.replace(':tenant_id', tenant_id);
    $.ajax({
        type: 'GET',
        url: url,
        success: function(html) {
            // $('#kosten_umbau_modal').find('.modal-body').html(html);
            $('#form_kosten_umbau').html(html);
        }
    });
}

$('body').on('click', '#save_kosten_umbau', function() {
    var data = $('#form_kosten_umbau').serialize();
    var $this = $(this);
    var property_id = $('#form_kosten_umbau').find("input[name='property_id']").val();
    var tenant_id = $('#form_kosten_umbau').find("input[name='tenant_id']").val();

    $.ajax({
        url: url_saveractivity,
        type: "post",
        data: data,
        success: function(response) {
            loadKostenUmbau(property_id, tenant_id);
        }
    });
});

$('#kosten_umbau_modal').on('hidden.bs.modal', function () {
    recommendedTableList();
});

$('body').on('click', '.btn-delete-vacant', function() {
    $this = $(this);
    var id = $(this).attr('data-id');
    if (confirm('Are you sure want to delete?')) {
        $.ajax({
            url: url_delete_new_vacant,
            type: "get",
            data: {
                id: id,
            },
            success: function(response) {
                recommendedTableList();
            }
        });
    }
    return false;
});

var link_button_datei;

$('body').on('click','.link-button-datei',function(e) {
    link_button_datei = $(this);
    var property_id = $(this).attr('data-property-id');
    var item_id     = $(this).attr('data-item-id');
    var year        = $(this).attr('data-year');
    var obj         = $(this).attr('id');

    var data = {
        property_id: property_id,
        item_id: item_id,
        year: year,
        obj: obj
    };
    $("#select-gdrive-file-model").modal('show');
    $("#select-gdrive-file-type").val('Betriebskostenabrechnung');
    $("#select-gdrive-file-callback").val('dateiSubmit');
    $("#select-gdrive-file-data").val(JSON.stringify(data));
    selectGdriveFileLoad(workingDir);
});

window.dateiSubmit = function(basename, dirname, curElement, dirOrFile) {
    var dataObj = $("#select-gdrive-file-data").val();
    dataObj = JSON.parse(dataObj);
    var formdata = new FormData();
    formdata.append('_token', _token);
    formdata.append('property_id', dataObj.property_id);
    formdata.append('item_id', dataObj.item_id);
    formdata.append('year', dataObj.year);
    formdata.append('basename', basename);
    formdata.append('dirname', dirname);
    formdata.append('dirtype', 'Betriebskostenabrechnung');
    formdata.append('dirOrFile', dirOrFile);
    $('.preloader').show();
    $.ajax({
        type: 'POST',
        url: url_saveMieterlisteFile,
        contentType: false,
        processData: false,
        data: formdata,
    }).done(function(data) {
        if (data.success) {
            // curElement.remove();
            // $('.uploaded-files-empfehlung2.rowt-' + dataObj.empfehlung_id).append(data.element);
            // alert(data.msg);
            $(link_button_datei).closest('.datei').append(data.html);
            // location.reload();
        } else {
            alert(data.msg);
        }
        $('.preloader').hide();
    });
}

$('body').on('click', '.delete_tenant_payment_file', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    if (confirm('Are you sure want to delete?')) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(response) {
                if(response.status){
                    $($this).closest('td').find('a').not(".link-button-datei").remove();
                }else{
                    alert(response.message);
                }
            }
        });
    }
    return false;
});

$('body').on("click", ".insurance-inline", function() {
    var input_class = $(this).attr('data-inputclass');
    if(typeof input_class !== 'undefined' && input_class != ''){
        $('.editableform').find('input').addClass(input_class);
    }else{
        $('.editableform').find('input').removeClass('mask-input-number');
        $('.editableform').find('input').removeClass('mask-input-new-date');
    }
});

$('body').on("click", ".angebote-comment", function() {
    var input_class = $(this).attr('data-inputclass');
    if(typeof input_class !== 'undefined' && input_class != ''){
        $('.editableform').find('input').addClass(input_class);
    }else{
        $('.editableform').find('input').removeClass('mask-input-number');
    }
});

$('body').on("change", ".insurance-notice-period", function() {
    var url = $(this).attr('data-url');
    var field = $(this).attr('data-field');
    var value = $(this).val();

    $.ajax({
        url: url,
        type: 'POST',
        data: {'pk': field, 'value': value},
        dataType: 'json',
        success: function(response) {
            if(!response.success){
                alert(response.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

$('body').on("click", "#btn-invoice-filter", function() {
    var field = $('#invoice-filter-field').val();
    var start_date = $('#invoice-filter-start-date').val();
    var end_date = $('#invoice-filter-end-date').val();

    var url = url_get_release_property_invoice+'?field='+field+'&start_date='+start_date+'&end_date='+end_date;

    $('#add_property_invoice_table2').DataTable().ajax.url(url).load();
});

$(".mieterliste_tab li a").each(function(){
    var is_active = $(this).closest('li').hasClass('active');
    if(is_active){
        var href = $(this).attr('href');
        checktab(href);
    }
});

$('body').on("click", ".mieterliste_tab li a", function() {
    var href = $(this).attr('href');
    checktab(href);
});

function checktab(href){
    if(href == '#rent_paid'){
        $('.mieterliste_tab').find('.btn-group').hide();
        $('.mieterliste_tab').find('.btn-fullscreen').hide();
        $('.mieterliste_tab').find('.wmd-view-topscroll').hide();
        $('.mieterliste_tab').find('.scroll-div1').hide();
    }else{
        $('.mieterliste_tab').find('.btn-group').show();
        $('.mieterliste_tab').find('.btn-fullscreen').show();
        $('.mieterliste_tab').find('.wmd-view-topscroll').show();
        $('.mieterliste_tab').find('.scroll-div1').show();
    }
}

$('body').on('click','.btn-not-release-insurance',function(){
    not_release_id = $(this).attr('data-id');
    table_id = $(this).closest('table').attr('data-tabid');
    $('#insurance-not-release-modal').modal();
    $('.insurance-not-release-comment').val("")
});

$('body').on('click','.insurance-not-release-submit',function(){
    comment = $('.insurance-not-release-comment').val();
    $.ajax({
          type : 'POST',
          url : url_insurance_mark_as_notrelease,
          data : {id:not_release_id, _token : _token, comment:comment},
          success : function (data) {
                location.reload();
                // $('#insurance-not-release-modal').modal('hide');
                // $('#insurance-table-'+table_id).DataTable().ajax.reload();
          }
      });
});

$('body').on('click','.btn-not-release-am-insurance',function(){
    not_release_id = $(this).attr('data-id');
    table_id = $(this).closest('table').attr('data-tabid');
    $('#insurance-not-release-am-modal').modal();
    $('.insurance-not-release-am-comment').val("")
});

$('body').on('click','.insurance-not-release-am-submit',function(){
    comment = $('.insurance-not-release-am-comment').val();
    $.ajax({
          type : 'POST',
          url : url_insurance_mark_as_notrelease_am,
          data : {id:not_release_id, _token : _token, comment:comment},
          success : function (data) {
            // $('#insurance-not-release-am-modal').modal('hide');
            // $('#insurance-table-'+table_id).DataTable().ajax.reload();
            location.reload();
          }
      });
});

$('body').on('click','.btn-pending-insurance',function(){
    pending_id = $(this).attr('data-id');
    table_id = $(this).closest('table').attr('data-tabid');
    $('#insurance-pending-modal').modal();
    $('.insurance-pending-comment').val("")
});

$('body').on('click','.insurance-pending-submit',function(){
    comment = $('.insurance-pending-comment').val();
    $.ajax({
          type : 'POST',
          url : url_insurance_mark_as_pending,
          data : {id:pending_id, _token : _token, comment:comment},
          success : function (data) {
            // $('#insurance-pending-modal').modal('hide');
            // $('#insurance-table-'+table_id).DataTable().ajax.reload();
            location.reload();
          }
      });
});

$('body').on('click','.btn-propert-management-not-release',function(){
    $('#property-management-not-release-modal').modal('show');
    vacant_id = $(this).attr('data-id');
});

$('#property-management-not-release-form').submit(function(event) {
    event.preventDefault();
    var comment = $(this).find('textarea[name="message"]').val();
    var url = $(this).attr('action');

    $.ajax({
          type : 'POST',
          url : url,
          data : {comment:comment,id:vacant_id},
          dataType: 'json',
          success : function (data) {
            if(data.status){
              window.location.reload(true);
            }else{
                alert(data.message);
            }
          },
          error : function(e){
            alert('Somthing want wrong!');
          }
      });
});

var provision_id = '';
$('body').on('click','.btn-provision-not-release',function(){
    provision_id = $(this).attr('data-id');
    $('#provision-not-release-modal').modal('show');
});

$('#provision-not-release-form').submit(function(event) {
    event.preventDefault();
    var comment = $(this).find('textarea[name="message"]').val();
    var url = $(this).attr('action')+'/'+provision_id;

    $.ajax({
          type : 'POST',
          url : url,
          data : {comment:comment},
          dataType: 'json',
          success : function (data) {
            if(data.status){
              // window.location.reload(true);
              $('#provision-not-release-modal').modal('hide');
              getprovisionbutton();
            }else{
                alert(data.message);
            }
          },
          error : function(e){
            alert('Somthing want wrong!');
          }
      });
});

$('body').on('keyup', '.s_purchase_price', function() {
    var total_purchase_price_el = $(this).closest('.row').find('.total_purchase_value');
    var from_bond_per_el = $(this).closest('.row').find('.from_bond');
    var from_bond_amount_el = $(this).closest('.row').find('.from_bond_amount');
    var bank_loan_per_el = $(this).closest('.row').find('.bank_loan');
    var bank_loan_amount_el = $(this).closest('.row').find('.bank_loan_amount');

    var total_purchase_price = makeNumber($(total_purchase_price_el).val());//gesamt_in_eur

    var from_bond_per = makeNumber($(from_bond_per_el).val());//building
    var from_bond_amount = makeNumber($(from_bond_amount_el).val());//building_amount

    var bank_loan_per = makeNumber($(bank_loan_per_el).val());//plot_of_land
    var bank_loan_amount = makeNumber($(bank_loan_amount_el).val());//plot_of_land_amount

    // console.log({id, gesamt_in_eur, building, building_amount, plot_of_land_amount, plot_of_land_amount});

    var id = '';
    if($(this).hasClass('from_bond')){
        id = 'from_bond';
    }else if($(this).hasClass('from_bond_amount')){
        id = 'from_bond_amount';
    }else if($(this).hasClass('bank_loan')){
        id = 'bank_loan';
    }else if($(this).hasClass('bank_loan_amount')){
        id = 'bank_loan_amount';
    }

    if(id == 'from_bond'){
        if(from_bond_per > 100){
            $(from_bond_per_el).val( makeNumberFormat(100, 10) );
            from_bond_per = 100;
        }
        bank_loan_per = (100 - from_bond_per);

        $(bank_loan_per_el).val( makeNumberFormat(bank_loan_per, 10) );
        $(from_bond_amount_el).val( makeNumberFormat((total_purchase_price * from_bond_per / 100), 2) );
        $(bank_loan_amount_el).val( makeNumberFormat((total_purchase_price * bank_loan_per / 100), 2) );

    }else if(id == 'from_bond_amount'){

        from_bond_per =  (from_bond_amount * 100 / total_purchase_price);
        bank_loan_per = (100 - from_bond_per);

        $(from_bond_per_el).val( makeNumberFormat(from_bond_per, 10) );
        $(bank_loan_per_el).val( makeNumberFormat(bank_loan_per, 10) );

        $(bank_loan_amount_el).val( makeNumberFormat((total_purchase_price * bank_loan_per / 100), 2) );

    }else if(id == 'bank_loan'){
        if(bank_loan_per > 100){
            $(bank_loan_per_el).val( makeNumberFormat(100, 10) );
            bank_loan_per = 100;
        }
        from_bond_per = (100 - bank_loan_per);

        $(from_bond_per_el).val( makeNumberFormat(from_bond_per, 10) );
        $(from_bond_amount_el).val( makeNumberFormat((total_purchase_price * from_bond_per / 100), 2) );
        $(bank_loan_amount_el).val( makeNumberFormat((total_purchase_price * bank_loan_per / 100), 2) );

    }else if(id == 'bank_loan_amount'){
        bank_loan_per = (bank_loan_amount * 100 / total_purchase_price);
        from_bond_per = (100 - bank_loan_per);

        $(from_bond_per_el).val( makeNumberFormat(from_bond_per, 10) );
        $(bank_loan_per_el).val( makeNumberFormat(bank_loan_per, 10) );

        $(from_bond_amount_el).val( makeNumberFormat((total_purchase_price * from_bond_per / 100), 2) );
    }

});

$('#tenant-edit-tab').find('a.inline-edit').editable({
    success: function(response, newValue) {
        if (response.success === false){
            return response.msg;
        }
    }
});

/*----------------------JS FOR Property Insurance comments - START -------------------------*/
var insurance_detail_comment_url;
var insurance_detail_comment_id;
var type = '';
var ins_table;

$('body').on('click', '.btn-ins-comment', function() {
    ins_table = $(this).closest('table').attr('id');
    insurance_detail_comment_url = $(this).attr('data-url');
    insurance_detail_comment_id  = $(this).attr('data-id');
    type = 'property_insurance_tab_details';

    var limit_text = $('#insurance_detail_comments_limit').text();
    getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
    // $('#insurance_detail_comment_modal').find('form').show();
    $('#insurance_detail_comment_modal').modal('show');
});

$('#insurance_detail_comment_form').submit(function(event) {

    event.preventDefault();
    var $this = $(this);
    var url = $(this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var data = {
        'record_id': insurance_detail_comment_id,
        'property_id': $('#selected_property_id').val(),
        'comment': comment,
        'type': type
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);
            if (result.status == true) {
                $($this)[0].reset();
                $($this).find('textarea[name="comment"]').focus();

                var limit_text = $('#insurance_detail_comments_limit').text();
                getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);

                if(type == 'property_insurance_tabs'){
                    /*if($('#insurance-table-'+insurance_detail_comment_id).closest('.modal-body').find('.section_comment_div').find('.hidden').length == 1){
                        $('#insurance-table-'+insurance_detail_comment_id).closest('.modal-body').find('.section_comment_div').find('.hidden').removeClass('hidden');
                    }
                    $('#insurance-table-'+insurance_detail_comment_id).closest('.modal-body').find('.show_cmnt_section').prepend('<p><span class="commented_user">'+result.data.name+'</span>: '+result.data.comment+' ('+result.data.created_at+')</p>');*/
                    $('#insurance-table-'+insurance_detail_comment_id).closest('.main_card').find('.section_latest_comment').html('<p><span class="commented_user">'+result.data.name+'</span>: '+result.data.comment+' ('+result.data.created_at+')</p>');
                }

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });

});

$('#insurance_detail_comment_modal').on('hidden.bs.modal', function () {
    if(ins_table){
        $('#'+ins_table).DataTable().ajax.reload();
        ins_table = '';
    }
});

$('body').on('click', '#insurance_detail_comments_limit', function() {
    var text = $(this).text();
    var limit = '';
    if(text == 'Show More'){
        limit = '';
        $(this).text('Show Less');
    }else{
        limit = 1;
        $(this).text('Show More');
    }
    getInsuranceDetailComments(insurance_detail_comment_url, limit, type);
});

$('body').on('click', '.remove-insurance-detail-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    var limit_text = $('#insurance_detail_comments_limit').text();
                    getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

function getInsuranceDetailComments(url, limit = '', type=''){
    var new_url = url+'/'+limit+'?type='+type;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#insurance_detail_comments_table').find('tbody').empty();
            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_insurance_detail_comment.replace(":id", value.id);
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-insurance-detail-comment">Löschen</a></td>' : '';
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;

                    var tr = '\
                    <tr>\
                        <td>'+ commented_user +'</td>\
                        <td>'+ value.comment +'</td>\
                        <td>'+ value.created_at +'</td>\
                        <td>'+delete_button+'\
                    </tr>\
                    ';

                    $('#insurance_detail_comments_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}
/*----------------------JS FOR Property Insurance comments - END -------------------------*/

/*----------------------JS FOR Property Insurance Section comments - START -------------------------*/
$('body').on('click', '.btn-add-angebote-section-comment', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    var comment = $($this).closest('.row').find('.angebote-section-comment').val();
    var record_id = $($this).closest('.row').attr('data-id');
    var property_id = $($this).closest('.row').attr('data-pid');

    var data = {
        'record_id': record_id,
        'property_id': property_id,
        'comment': comment,
        'type': 'property_insurance_tabs'
    };

    if(comment){
      $.ajax({
          url: url,
          type: 'POST',
          data: data,
          dataType: 'json',
          beforeSend: function() {
              $($this).prop('disabled', true);
          },
          success: function(result) {

              $($this).prop('disabled', false);

              if (result.status == true) {

                  $($this).closest('.row').find('.angebote-section-comment').val('');
                  $($this).closest('.row').find('.angebote-section-comment').focus();

                  if($($this).closest('.row').find('.hidden').length == 1){
                    $($this).closest('.row').find('.hidden').removeClass('hidden');
                  }
                    $($this).closest('.row').find('.show_cmnt_section').prepend('<span>'+result.data.name+': '+result.data.comment+'</span><br>');

              } else {
                  alert(result.message);
              }
          },
          error: function(error) {
              $($this).prop('disabled', false);
              alert('Somthing want wrong!');
          }
      });
    }else{
      return false;
    }
});

$('body').on('click', '.btn-show-angebote-section-comment', function() {
  var url = $(this).attr('data-url');
  var id = $(this).attr('data-id');

  insurance_detail_comment_url = url;
  insurance_detail_comment_id = id;

  var limit_text = $('#insurance_detail_comments_limit').text();
  type = 'property_insurance_tabs';
  getInsuranceDetailComments(url, (limit_text == 'Show More') ? 1 : '', type);
  // $('#insurance_detail_comment_modal').find('form').hide();
  $('#insurance_detail_comment_modal').modal('show');
});
/*----------------------JS FOR Property Insurance Section comments - START -------------------------*/


$('body').on('keyup', '#zinsaufwand, #tilgung', function() {

    var zinsaufwand = makeNumber($('#darlehensspiegelnew').find('#zinsaufwand').val());
    var tilgung = makeNumber($('#darlehensspiegelnew').find('#tilgung').val());

    $('#darlehensspiegelnew').find('#kapitaldienst').val( makeNumberFormat((zinsaufwand + tilgung), 2) );
});

$('body').on('click', '.load_comment', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    var text = $(this).text();
    var limit = '';

    if(text == 'Show More'){
        $($this).text('Show Less');
        limit = '';
    }else{
        $($this).text('Show More');
        limit = 2;
    }

    var new_url = url+'/'+limit+'?type=property_insurance_tab_details';

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            $($this).closest('td').find('.show_cmnt').empty();
            var html = '';
            if(result){
                $.each(result, function(key, value) {
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;
                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
                });
            }
            $($this).closest('td').find('.show_cmnt').html(html);
        },
        error: function(error) {
            console.log({error});
        }
    });
});



$('body').on('click', '.load_comment_section', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    var text = $(this).text();
    var limit = '';

    if(text == 'Show More'){
        $($this).text('Show Less');
        limit = '';
    }else{
        $($this).text('Show More');
        limit = 2;
    }

    var new_url = url+'/'+limit+'?type=property_insurance_tabs';

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            $($this).closest('div').find('.show_cmnt_section').empty();
            var html = '';
            if(result){
                $.each(result, function(key, value) {
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;
                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
                });
            }
            $($this).closest('div').find('.show_cmnt_section').html(html);
        },
        error: function(error) {
            console.log({error});
        }
    });
});

$('body').on('change', 'select[name="zusatzsicherheit"]', function() {
    var val = $(this).val();

    if(val == 'Garantie Sonstiges'){
        $('#sonstiges_div').removeClass('hidden');
    }else{
        $('#sonstiges_div').addClass('hidden');
    }
});

$('body').on('change', 'select[name="euribor"]', function() {
    var val = $(this).val();

    if(val == '3M'){
        $('#höhe_euribor_3_monate_div').removeClass('hidden');
        $('#höhe_euribor_6_monate_div').addClass('hidden');
    }else{
        $('#höhe_euribor_3_monate_div').addClass('hidden');
        $('#höhe_euribor_6_monate_div').removeClass('hidden');
    }
    AktuellerZins();
});

$('body').on('change', 'select[name="behandlung_negativzins"]', function() {

    var val = $(this).val();

    var anfangszins = makeNumber($('#anfangszins').val());
    var höhe_euribor_3_monate = makeNumber($('#höhe_euribor_3_monate').attr('data-val'));
    var höhe_euribor_6_monate = makeNumber($('#höhe_euribor_6_monate').attr('data-val'));

    if(val == 'Euribor=0'){

        if(höhe_euribor_3_monate < 0){
            $('#höhe_euribor_3_monate').val(makeNumberFormat(anfangszins));
        }else{
            $('#höhe_euribor_3_monate').val(makeNumberFormat(höhe_euribor_3_monate));
        }

        if(höhe_euribor_6_monate < 0){
            $('#höhe_euribor_6_monate').val(makeNumberFormat(anfangszins));
        }else{
            $('#höhe_euribor_6_monate').val(makeNumberFormat(höhe_euribor_6_monate));
        }

    }else{
        $('#höhe_euribor_3_monate').val(makeNumberFormat(höhe_euribor_3_monate));
        $('#höhe_euribor_6_monate').val(makeNumberFormat(höhe_euribor_6_monate));
    }

    AktuellerZins();
});

$('body').on('keyup', '#mabgeblicher_euribor, #veränderungsgrenze, #anfangszins', function() {
    AktuellerZins();
});

AktuellerZins();
function AktuellerZins(){

    if($('#höhe_euribor_3_monate').length==0)
        return;


    var behandlung_negativzins = $('select[name="behandlung_negativzins"]').val();
    var mabgeblicher_euribor = $('#mabgeblicher_euribor').val();
    var veränderungsgrenze = $('#veränderungsgrenze').val();
    var anfangszins = $('#anfangszins').val();

    var höhe_euribor = 0;
    var aktueller_zins = 0

    if($('select[name="euribor"]').val() == '3M'){
        höhe_euribor = $('#höhe_euribor_3_monate').val();
    }else{
        höhe_euribor = $('#höhe_euribor_6_monate').val();
    }
    höhe_euribor = makeNumber(höhe_euribor);
    mabgeblicher_euribor = makeNumber(mabgeblicher_euribor);
    veränderungsgrenze = makeNumber(veränderungsgrenze);
    anfangszins = makeNumber(anfangszins);

    if(behandlung_negativzins == 'Feld "Höhe Euribor"'){

        if(höhe_euribor > 0){
            if( (höhe_euribor - mabgeblicher_euribor) < veränderungsgrenze ){
                aktueller_zins = anfangszins;
            }

            if( (höhe_euribor - mabgeblicher_euribor) > veränderungsgrenze){
                aktueller_zins = anfangszins + höhe_euribor;
            }
        }

        if(höhe_euribor < 0){
            if( (höhe_euribor - mabgeblicher_euribor) < veränderungsgrenze ){
                aktueller_zins = anfangszins;
            }

            if( (höhe_euribor - mabgeblicher_euribor) > veränderungsgrenze ){
                aktueller_zins = anfangszins + (höhe_euribor - mabgeblicher_euribor);
            }
        }

    }

    $('#aktueller_zins').val(makeNumberFormat(aktueller_zins, 2));

    changeDarlehensart();
}

$('body').on('keyup change', '#verkehrswert, #aktuelle_restschuld', function() {
    var verkehrswert = makeNumber($('#verkehrswert').val());
    var aktuelle_restschuld = makeNumber($('#aktuelle_restschuld').val());

    var delta = (verkehrswert - aktuelle_restschuld);
    $('#delta4').val( makeNumberFormat(delta, 2) );
});

$('body').on('keyup change', 'input[name="tilgungssatz"], input[name="anfangszins"], input[name="darlehensbetrag"], input[name="aktuelle_restschuld"], input[name="aktueller_zins"], input[name="kapitaldienst"], input[name="zinsaufwand"], input[name="tilgung"]', function() {
    changeDarlehensart();
});

$('body').on('change', '#darlehensart', function(){
    changeDarlehensart();
});

function changeDarlehensart(){//loan type

    // console.log('changeDarlehensart calling...');
    if($('#darlehensart').length==0){
        return;
    }

    var darlehensart        = $('#darlehensart').val();
    var tilgungssatz        = makeNumber($('input[name="tilgungssatz"]').val());//Anfänglicher Tilgungssatz p.a.
    var anfangszins         = makeNumber($('input[name="anfangszins"]').val());//Anfangszins p.a
    var darlehensbetrag     = makeNumber($('input[name="darlehensbetrag"]').val());//Darlehensbetrag
    var aktuelle_restschuld = makeNumber($('input[name="aktuelle_restschuld"]').val());//Aktuelle Restschuld
    var aktueller_zins      = makeNumber($('input[name="aktueller_zins"]').val());//Aktueller Zins

    if(darlehensart == 'Annuitätendarlehen'){

        var kapitaldienst = ( (tilgungssatz + anfangszins) * darlehensbetrag);//Kapitaldienst p.a.
        var zinsaufwand   = (aktuelle_restschuld * aktueller_zins);//Zinsaufwand p.a.
        var tilgung       = (kapitaldienst - zinsaufwand);//Tilgung p.a.

        /*console.log({tilgungssatz, anfangszins, darlehensbetrag});
        console.log('kapitaldienst = '+tilgungssatz+' + '+anfangszins+' * '+darlehensbetrag+' = '+kapitaldienst);
        console.log('zinsaufwand = '+aktuelle_restschuld+' * '+aktueller_zins+' = '+zinsaufwand);
        console.log('tilgung = '+kapitaldienst+' - '+zinsaufwand+' = '+tilgung);*/

        $('input[name="kapitaldienst"]').val( makeNumberFormat(kapitaldienst, 2) );
        $('input[name="zinsaufwand"]').val( makeNumberFormat(zinsaufwand, 2) );
        $('input[name="tilgung"]').val( makeNumberFormat(tilgung, 2) );

    }else if(darlehensart == 'Tilgungsdarlehen'){

        var tilgung       = makeNumber($('input[name="tilgung"]').val());//Tilgung p.a.
        var zinsaufwand   = (aktuelle_restschuld * aktueller_zins);//Zinsaufwand p.a.
        var kapitaldienst = (tilgung + zinsaufwand);//Kapitaldienst p.a.

        $('input[name="kapitaldienst"]').val( makeNumberFormat(kapitaldienst, 2) );
        $('input[name="zinsaufwand"]').val( makeNumberFormat(zinsaufwand, 2) );
        // $('input[name="tilgung"]').val( makeNumberFormat(tilgung, 2) );
    }
}

$('body').on('click', '.custom_user', function() {
  var property_id = $(this).attr('data-property-id');
  var user_id = $(this).attr('data-user-id');
  var subject = $(this).attr('data-subject');
  var content = $(this).attr('data-content');
  var reload = $(this).attr('data-reload');
  var email = $(this).attr('data-email');
  var mail_type = $(this).attr('data-title');

  var id = $(this).attr('data-id');
  var section = $(this).attr('data-section');

  reload_custom_user = (typeof reload !== 'undefined' && reload == "1") ? 1 : 0;
  email = (typeof email !== 'undefined') ? email : "";
  mail_type = (typeof mail_type !== 'undefined') ? mail_type : "";

  id = (typeof id !== 'undefined') ? id : '';
  section = (typeof section !== 'undefined') ? section : '';

  $('#modal_sendmail_to_custom_user').find('input[name="property_id"]').val(property_id);
  $('#modal_sendmail_to_custom_user').find('input[name="user_id"]').val(user_id);
  $('#modal_sendmail_to_custom_user').find('input[name="subject"]').val(subject);
  $('#modal_sendmail_to_custom_user').find('input[name="content"]').val(content);
  $('#modal_sendmail_to_custom_user').find('input[name="email"]').val(email);
  $('#modal_sendmail_to_custom_user').find('input[name="mail_type"]').val(mail_type);

  $('#modal_sendmail_to_custom_user').find('input[name="id"]').val(id);
  $('#modal_sendmail_to_custom_user').find('input[name="section"]').val(section);

  $('#modal_sendmail_to_custom_user').modal('show');
});

$('#form_modal_sendmail_to_custom_user').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var formData = new FormData($(this)[0]);
    var url = $(this).attr('action');

    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        cache: false,
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            if (result.status) {
              $($this)[0].reset();
              $('#modal_sendmail_to_custom_user').modal('hide');
              // if(reload_custom_user){
                loadAllMail();
              // }
              sweetAlert("Mail wurde erfolgreich gesendet");
            }else{
              alert(result.message);
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            alert('Somthing want wrong!');
            console.log({error});
        }
    });
}));


/*-------------------------------------------------------------------------*/
    var current_ele;
    var file_column_type = 0
    $('.link-button-pdf-mieterliste').on('click', function(e){
        current_ele = $(this);
        var id = $(this).attr('data-id');
        file_column_type = $(this).attr('data-file_column_type');
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('mieterliste');
        $("#select-gdrive-file-callback").val('selectMieterlisteSubmit');
        $("#select-gdrive-file-data").val(id);
        selectGdriveFileLoad(workingDir);
    });

    window.selectMieterlisteSubmit = function(basename, dirname,curElement, dirOrFile){
      var id = $("#select-gdrive-file-data").val();

      var formdata = new FormData();
      formdata.append('_token', _token);
      formdata.append('id', id);
      formdata.append('file_column_type', file_column_type);
      formdata.append('basename', basename);
      formdata.append('dirname', dirname);
      formdata.append('type', dirOrFile);
      $('.preloader').show();
        var url = url_select_mieterliste_dir;
        if(dirOrFile == 'file'){
            url = url_select_mieterliste_file;
        }
      $.ajax({
        type: 'POST',
        url: url,
        contentType: false,
        processData: false,
        data:formdata,
      }).done(function (data) {
        if(data.success){
            $('#select-gdrive-file-model').modal('hide');
            $(current_ele).closest('th').append(data.element);
        }else{
          alert(data.msg);
        }
        $('.preloader').hide();
      });
    }

/*-------------------------------------------------------------------------*/

/*-------------JS FOR AO COST Liquiplanung tab - start ---------------------*/

    loadAoCosts();
    loadAoEinnahmen();
    $('body').on('click', '.btn-add-ao-cost', function(){
        var type = $(this).attr('data-type');
        var text = $(this).text();

        /*if(type == "1"){
            $('#ao_cost_modal').find('input[name="amount"]').removeClass('mask-number-input-negetive').addClass('mask-number-input');
        }else{
            $('#ao_cost_modal').find('input[name="amount"]').removeClass('mask-number-input').addClass('mask-number-input-negetive');
        }*/
        $('#ao_cost_modal').find('.modal-title').text(text);
        $('#ao_cost_modal').modal('show').find('input[name="type"]').val(type);
    });

    $('#form_ao_cost').on('submit', (function(e) {
        e.preventDefault();

        var $this = $(this);
        var formData = new FormData($(this)[0]);
        var url = $(this).attr('action');
        var type = $($this).find('input[name="type"]').val();

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status) {

                    $($this)[0].reset();
                    $('#ao_cost_modal').modal('hide');

                    if(type == '1'){
                        loadAoEinnahmen();
                    }else{
                        loadAoCosts();
                    }
                }else{
                  alert(result.message);
                }
            },
            error: function(error) {
                $($this).find('button[type="submit"]').prop('disabled', false);
                alert('Somthing want wrong!');
                console.log({error});
            }
        });
    }));

    $('body').on('click', '.btn-delete-ao_cost', function() {
        var url = $(this).attr('data-url');
        var type = $(this).attr('data-type');

        var $this = $(this);
        if(confirm('Are you sure to delete this record?')){
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if(result.status){
                        if(type == '1'){
                            loadAoEinnahmen();
                        }else{
                            loadAoCosts();
                        }
                    }else{
                        alert(result.message);
                    }
                },
                error: function(error) {
                    alert('Somthing want wrong!');
                }
            });
        }else{
            return false;
        }
    });

    function loadAoCosts(){

        var url = $('#table_ao_cost').attr('data-url');

        if ( ! $.fn.DataTable.isDataTable( '#table_ao_cost' ) ) {
            var table_ao_cost = $('#table_ao_cost').dataTable({
                "ajax": url,
                "columnDefs": [{
                    className: "text-right",
                    "targets": [2]
                }],
                "columns": [
                    null,
                    null,
                    { "type": "numeric-comma" },
                    null,
                    null,
                    null,
                ]
            });
        }else{
            $('#table_ao_cost').DataTable().ajax.reload();
        }
    }

    function loadAoEinnahmen(){

        var url = $('#table_ao_einnahmen').attr('data-url');

        if ( ! $.fn.DataTable.isDataTable( '#table_ao_einnahmen' ) ) {
            var table_ao_einnahmen = $('#table_ao_einnahmen').dataTable({
                "ajax": url,
                "columnDefs": [{
                    className: "text-right",
                    "targets": [2]
                }],
                "columns": [
                    null,
                    null,
                    { "type": "numeric-comma" },
                    null,
                    null,
                    null,
                ]
            });
        }else{
            $('#table_ao_einnahmen').DataTable().ajax.reload();
        }
    }

/*-------------JS FOR AO COST Liquiplanung tab - end -----------------------*/


/*--------------------JS FOR PROPERTY COMMENTS- START----------------------------------*/
var c_record_id = '';
var c_property_id = '';
var c_type = '';
var c_table = '';

$('body').on('click', '.btn-add-property-comment', function() {

    var $this = $(this);

    c_record_id = $($this).attr('data-record-id');
    c_property_id = $($this).attr('data-property-id');
    c_type = $($this).attr('data-type');

    var comment = $($this).closest('.property-comment-section').find('.property-comment').val();
    var reload = $(this).attr('data-reload');
    var subject = $(this).attr('data-subject');
    var content = $(this).attr('data-content');

    var data = {
        'property_id': c_property_id,
        'comment': comment,
        'type': c_type,
        'subject': subject,
        'content': content,
    };
    if(c_record_id && c_record_id != ''){
      data.record_id = c_record_id;
    }

    if(comment){
      addPropertyComment($this, data, (reload == '1') ? true : false);
    }
});

$('body').on('click', '.btn-show-property-comment', function() {
  var $this = $(this);
  var show_form = ($($this).attr('data-form') == '1') ? true : false;
  var subject = $(this).attr('data-subject');
  var content = $(this).attr('data-content');

  c_record_id = $($this).attr('data-record-id');
  c_property_id = $($this).attr('data-property-id');
  c_type = $($this).attr('data-type');

  if(c_type == 'property_contracts' || c_type == 'property_invoices' || c_type == 'properties_default_payers' || c_type == 'send_mail_to_ams'){
    c_table = $($this).closest('table').attr('id');
  }

  if(show_form){
      $('#property_comment_modal').find('.property-comment-section').show();
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-record-id', c_record_id);
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-property-id', c_property_id);
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-type', c_type);

      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-subject', subject);
      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-content', content);
  }else{
    $('#property_comment_modal').find('.property-comment-section').hide();
  }

  $('#property_comment_modal').modal('show');
  getPropertyComment(c_record_id, c_property_id, c_type);
});

$('body').on('click', '#property_comment_limit', function() {
    var text = $(this).text();
    if(text == 'Show More'){
        $(this).text('Show Less');
    }else{
        $(this).text('Show More');
    }
    getPropertyComment(c_record_id, c_property_id, c_type);
});

$('body').on('click', '.remove-property-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    getPropertyComment(c_record_id, c_property_id, c_type);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

$('body').on('click', '.load_property_comment_section', function() {
    var $this = $(this);
    var url = $(this).attr('data-url');
    var text = $(this).text();
    var closest = $(this).attr('data-closest');
    closest = (closest) ? closest : 'div';

    var limit = '';

    if(text == 'Show More'){
        $($this).text('Show Less');
        limit = '';
    }else{
        $($this).text('Show More');
        limit = 2;
    }

    var new_url = url+'&limit='+limit;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            $($this).closest(closest).find('.show_property_cmnt_section').empty();
            var html = '';
            if(result){
                $.each(result, function(key, value) {
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;
                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
                });
            }
            $($this).closest(closest).find('.show_property_cmnt_section').html(html);
        },
        error: function(error) {
            console.log({error});
        }
    });
});

$('#property_comment_modal').on('hidden.bs.modal', function () {
    if(c_table){
        if(c_table == 'property-mail-table'){
            loadAllMail();
        }else{
            $('#'+c_table).DataTable().ajax.reload();
        }
        c_table = '';
    }
});

function addPropertyComment(btnobj, data, reload = false){
  $.ajax({
      url: url_add_property_comment,
      type: 'POST',
      data: data,
      dataType: 'json',
      beforeSend: function() {
          $(btnobj).prop('disabled', true);
      },
      success: function(result) {

          $(btnobj).prop('disabled', false);

          if (result.status == true) {

              $(btnobj).closest('.property-comment-section').find('.property-comment').val('');
              $(btnobj).closest('.property-comment-section').find('.property-comment').focus();

              if(reload){
                  getPropertyComment(c_record_id, c_property_id, c_type);
              }

          } else {
              alert(result.message);
          }
      },
      error: function(error) {
          $(btnobj).prop('disabled', false);
          alert('Somthing want wrong!');
      }
  });
}


function getPropertyComment(record_id = '', property_id = '', type = ''){
    var limit_text = $('#property_comment_limit').text();
    var limit = (limit_text == 'Show More') ? 1 : '';

    var new_url = url_get_property_comment+'?property_id='+property_id+'&record_id='+record_id+'&type='+type+'&limit='+limit;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#property_comment_table').find('tbody').empty();
            if(type == 'property_invoices'){
                $('#property_comment_table').find('#th_name').hide();
                $('#property_comment_table').find('#th_date').hide();
            }else{
                $('#property_comment_table').find('#th_name').show();
                $('#property_comment_table').find('#th_date').show();
            }

            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_delete_property_comment.replace(":id", value.id);
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-property-comment">Löschen</a></td>' : '';
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = value.user_name+''+company;

                    if(type == 'property_invoices'){
                      var tr = '\
                        <tr>\
                            <td>'+commented_user+' ('+value.date+') : '+ value.comment +'</td>\
                            <td class="text-center">'+delete_button+'\
                        </tr>\
                      ';
                    }else{
                      var tr = '\
                        <tr>\
                            <td>'+ commented_user +'</td>\
                            <td>'+ value.comment +'</td>\
                            <td>'+ value.created_at +'</td>\
                            <td>'+delete_button+'\
                        </tr>\
                      ';
                    }

                    $('#property_comment_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}
/*--------------------JS FOR PROPERTY COMMENTS- END----------------------------------*/

$('body').on('click', '.btn-delete-log', function() {
    var $this = $(this);
    var id = $(this).attr('data-id');
    var table = $(this).attr('data-tbl');
    if(table && id){
        if(confirm('Are you sure to delete this record?')){
            $.ajax({
                url: url_delete_log,
                type: 'GET',
                data:{'id': id, 'tbl': table},
                dataType: 'json',
                success: function(result) {
                    if(result.status){
                        $($this).closest('tr').remove();
                    }else{
                        alert(result.message);
                    }
                },
                error: function(error) {
                    alert('Somthing want wrong!');
                }
            });
        }
    }
    return false;
});

$('body').on('click', '#tenant-edit-tab .show-links', function() {
    var is_open = ($(this).find('i').hasClass('fa-angle-down')) ? true : false;
    var cls = $(this).attr('data-class');

    var src = $('.'+cls).find('iframe').attr('data-src');

    if(is_open){
        $('.'+cls).find('iframe').attr('src', src);
    }
});

/*---------------------------------PROVISION ATTECHMENT----------------------------------------*/
    var current_ele;
    $('body').on('click', '.provision-attachment', function(e){
        current_ele = $(this);
        var id = $(this).attr('data-id');
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('provision');
        $("#select-gdrive-file-callback").val('selectProvosionSubmit');
        $("#select-gdrive-file-data").val(id);
        selectGdriveFileLoad(workingDir);
    });

    window.selectProvosionSubmit = function(basename, dirname,curElement, dirOrFile){
      var id = $("#select-gdrive-file-data").val();

      var formdata = new FormData();
      formdata.append('_token', _token);
      formdata.append('id', id);
      formdata.append('basename', basename);
      formdata.append('dirname', dirname);
      formdata.append('type', dirOrFile);
      $('.preloader').show();
        var url = url_add_provision_dir;
        if(dirOrFile == 'file'){
            url = url_add_provision_file;
        }
      $.ajax({
        type: 'POST',
        url: url,
        contentType: false,
        processData: false,
        data:formdata,
      }).done(function (data) {
        if(data.success){
            $('#select-gdrive-file-model').modal('hide');
            $(current_ele).closest('td').append(data.element);
        }else{
          alert(data.msg);
        }
        $('.preloader').hide();
      });
    }

/*--------------------------------PROVISION ATTECHMENT-----------------------------------------*/

$('body').on('click', '.show_item_detail', function() {

    var title = $(this).attr('data-title');
    var type = $(this).attr('data-type');

    if(type == 'provision'){
        var item_id = $(this).attr('data-id');
    }else{
        var item_id = $(this).closest('td').find('select').val();
    }

    $('#tenancy-detail-modal').find('.modal-title').text(title);
    $('#tenancy-detail-modal').modal('show');

    if(item_id){
        var url = url_get_tenancy_item_detail+'?id='+item_id;
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    var html = '<tr>\
                                    <td>'+result.data.name+'</td>\
                                    <td>'+result.data.date+'</td>\
                                    <td>'+result.data.space+'</td>\
                                    <td>'+result.data.files+'</td>\
                                </tr>';
                    $('#tenancy-detail-modal').find('tbody').html(html);
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        $('#tenancy-detail-modal').find('tbody').empty();
    }
});

loadAllMail();

function loadAllMail(){
    var url = $('#all_mail').attr('data-url');
    $.ajax({
        url: url,
        type: 'GET',
        success: function(html) {
            $('#all_mail').html(html);

            var columns = [
                null,
                { "visible": false },//property name
                null,
                null,
                null,
                null,
                { "type": "new-date-time" },
                { "visible": false } //open button
            ];

            makeDatatable($('#property-mail-table'), columns);
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
}

$('body').on('keyup', '.p_cost, .p_einnahmen_pm, .p_laufzeit_in_monate, .p_mietfrei', function() {

    var $this = $(this);
    var $tr   = $(this).closest('tr');

    var cost                = makeNumber($($tr).find('.p_cost').val());
    var einnahmen_pm        = makeNumber($($tr).find('.p_einnahmen_pm').val());
    var laufzeit_in_monate  = makeNumber($($tr).find('.p_laufzeit_in_monate').val());
    var mietfrei            = makeNumber($($tr).find('.p_mietfrei').val());

    var jahrl_einnahmen = (einnahmen_pm * 12);
    var rent            = (einnahmen_pm * laufzeit_in_monate);
    var net_income      = (rent - cost - mietfrei);

    $($tr).find('.p_jahrl_einnahmen').text(makeNumberFormat(jahrl_einnahmen, 2));
    $($tr).find('.p_rent').text(makeNumberFormat(rent, 2));
    $($tr).find('.p_net_income').text(makeNumberFormat(net_income, 2));

});

$('body').on('change', '.liquiplanung_input', function(){
    var value = $(this).val();
    var field = $(this).attr('name');
    var url   = url_update_liquiplanung_data;

    if(field == 'not_consider_in_liquid'){
        value = ($(this).prop("checked") == true) ? 1 : 0;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: {'field': field, 'value': value, '_token': _token},
        dataType: 'json',
        success: function(result) {

            if (result.status == true) {

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $(btnobj).prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });
});

$('body').on('click', '#am-log', function(){

    $('#am-log-modal').modal('show');

    var url = $(this).attr('data-url');

    if ( ! $.fn.DataTable.isDataTable( '#am-log-table' ) ) {
        var am_log_table = $('#am-log-table').dataTable({
            "ajax": url,
            "order": [
                [1, "desc"]
            ],
            "columns": [
                null,
                { "type": "new-date" },
                null,
            ]
        });
    }else{
        $('#am-log-table').DataTable().ajax.reload();
    }

});

var log_upload_btn;
$('body').on('click','.btn-am-log-upload', function(e){

    log_upload_btn  = $(this);
    var type        = $(this).attr('data-type');
    var property_id = $(this).attr('data-property-id');
    var id          = $(this).attr('data-id');

    var data = {
        type: type,
        property_id: property_id,
        id: id,
    };

    $("#select-gdrive-file-model").modal('show');
    $("#select-gdrive-file-type").val('am_log');
    $("#select-gdrive-file-callback").val('selectAmLogSubmit');
    $("#select-gdrive-file-data").val(JSON.stringify(data));

    selectGdriveFileLoad(workingDir);
});

window.selectAmLogSubmit = function(basename, dirname,curElement, dirOrFile){
    var dataObj = $("#select-gdrive-file-data").val();
    dataObj =JSON.parse(dataObj);
    var formdata = new FormData();
    formdata.append('_token', _token);
    formdata.append('property_id', dataObj.property_id);
    formdata.append('type', dataObj.type);
    formdata.append('id', dataObj.id);
    formdata.append('basename', basename);
    formdata.append('dirname', dirname);
    $('.preloader').show();
    var url = $(log_upload_btn).closest('table').attr('data-dir-url');
    if(dirOrFile == 'file'){
        url = $(log_upload_btn).closest('table').attr('data-file-url');
    }
    $.ajax({
        type: 'POST',
        url: url,
        contentType: false,
        processData: false,
        data:formdata,
    }).done(function (data) {
        if(data.status){
            $('#am-log-table').DataTable().ajax.reload();
        }else{
            alert(data.message);
        }
        $('.preloader').hide();
    });
}

/*------------------ITEM COMMENT START----------------*/
var item_id;
var item_type;
var item_property_id;
var item_subject;
var item_content;
var is_comment_added = false;
var item_td_obj;

$('body').on('click', '.btn-show-item-comment', function() {
    item_id   = $(this).attr('data-id'); //item id
    item_type = $(this).attr('data-type');//1 = external and 0 = comment

    item_property_id = $(this).attr('data-property-id');
    item_subject     = $(this).attr('data-subject');
    item_content     = $(this).attr('data-content');

    item_td_obj = $(this).closest('td');

    getItemComment();
    $('#item_comment_modal').modal('show');
});

$('body').on('click', '.btn-add-item-comment', function() {
    var $this = $(this);
    var comment = $.trim($('.item-comment').val());
    if(comment){

      var data = {
          'item_id': item_id,
          'comment': comment,
          'type': item_type,
          'property_id': item_property_id,
          'subject': item_subject,
          'content': item_content,
        };

      $.ajax({
        url: url_addItemComment,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).prop('disabled', true);
        },
        success: function(result) {

            $($this).prop('disabled', false);

            if (result.status == true) {

                $($this).closest('.item-comment-section').find('.item-comment').val('');
                $($this).closest('.item-comment-section').find('.item-comment').focus();

                is_comment_added = true;

                getItemComment();

            } else {
                alert(result.message);
            }
        },
        error: function(error) {
            $($this).prop('disabled', false);
            alert('Somthing want wrong!');
        }
    });

    }
});

$('body').on('click', '#item_comment_limit', function() {
    var text = $(this).text();
    if(text == 'Show More'){
        $(this).text('Show Less');
    }else{
        $(this).text('Show More');
    }
    getItemComment();
});

$('body').on('click', '.remove-item-comment', function() {
    var url = $(this).attr('data-url');
    var $this = $(this);
    if(confirm('Are you sure to delete this comment?')){
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    is_comment_added = true;
                    getItemComment();
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }else{
        return false;
    }
});

$('#item_comment_modal').on('hidden.bs.modal', function () {
  if(is_comment_added){
    is_comment_added = false;
    loadItemCommentSection(item_td_obj, 2);
  }
});

$('body').on('click', '.load_item_comment_section', function() {
    var $this = $(this);
    var text = $(this).text();
    var tdobj = $(this).closest('td');
    var limit = '';

    if(text == 'Show More'){
        $($this).text('Show Less');
        limit = '';
    }else{
        $($this).text('Show More');
        limit = 2;
    }

    loadItemCommentSection(tdobj, limit);
});

function loadItemCommentSection(tdobj, limit = ''){
    var url = $(tdobj).find('.load_item_comment_section').attr('data-url');
    var item_type = $(tdobj).find('.load_item_comment_section').attr('data-type');

    if($(tdobj).find('.load_item_comment_section').hasClass('hidden')){
        $(tdobj).find('.load_item_comment_section').removeClass('hidden');
    }

    var new_url = url+'&limit='+limit;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            $(tdobj).find('.show_item_cmnt_section').empty();
            var html = '';
            if(result){
                $.each(result, function(key, value) {

                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = ( (value.name) ? value.name : value.user_name )+''+company;
                    var comment = (item_type == '1') ? value.external_comment : value.comment;

                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+comment+' ('+value.created_at+')</p>';
                });
            }
            $(tdobj).find('.show_item_cmnt_section').html(html);
        },
        error: function(error) {
            console.log({error});
        }
    });
}

function getItemComment(){
    var limit_text = $('#item_comment_limit').text();
    var limit = (limit_text == 'Show More') ? 1 : '';

    var new_url = url_getItemComment+'?item_id='+item_id+'&limit='+limit+'&type='+item_type;

    $.ajax({
        url: new_url,
        type: 'GET',
        dataType: 'json',
        success: function(result) {

            $('#item_comment_table').find('tbody').empty();

            if(result){
                $.each(result, function(key, value) {
                    var clear_url = url_deleteItemComment+'?id='+value.id;
                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-item-comment">Löschen</a></td>' : '';
                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    var commented_user = ( (value.name) ? value.name : value.user_name )+''+company;
                    var comment = (item_type == '1') ? value.external_comment : value.comment;
                    var tr = '\
                        <tr>\
                            <td>'+ commented_user +'</td>\
                            <td>'+ comment +'</td>\
                            <td>'+ value.created_at +'</td>\
                            <td>'+delete_button+'\
                        </tr>\
                    ';

                    $('#item_comment_table').find('tbody').append(tr);
                });
            }
        },
        error: function(error) {
            console.log({error});
        }
    });
}

/*------------------ITEM COMMENT END----------------*/


/*---------------------------------------------------------------------------*/

$('body').on('change', '#darlehensart', function() {
    var val = $(this).val();
    if(val == 'Annuitätendarlehen'){
        $('.annuitatendarlehen-div').removeClass('hidden');
        $('.tilgungsdarlehen-div').addClass('hidden');
    }else if(val == 'Tilgungsdarlehen'){
        $('.annuitatendarlehen-div').addClass('hidden');
        $('.tilgungsdarlehen-div').removeClass('hidden');
    }else{
         $('.annuitatendarlehen-div').addClass('hidden');
        $('.tilgungsdarlehen-div').addClass('hidden');
    }
});

$('body').on('change', '#annuitatendarlehen-table .kapitaldienst_manuelle, #annuitatendarlehen-table .aktuelle_restschuld_manuelle', function() {
    var $this = $(this);
    calculateTable1();

    if($($this).hasClass('aktuelle_restschuld_manuelle')){
        $.ajax({
            url: url_update_property_custom_field,
            type: 'POST',
            data: {'field': 'annuitatendarlehen_aktuelle_restschuld', 'value': $($this).val()},
            dataType: 'json',
            success: function(result) {
                if (!result.status) {
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }
});

$('body').on('change', '#annuitatendarlehen-table .kapitaldienst_manuelle, #annuitatendarlehen-table .aktuelle_restschuld_manuelle', function() {
    calculateTable1();
});

$('body').on('click', '#annuitatendarlehen-table .z_show_more', function() {
    var text = $(this).text();
    if(text == 'Show More'){
        showMoreTable1($(this));
    }else{
        showLessTable1($(this));
    }
});

var monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

var Aktuelle_Restschuld = $.trim($('#annuitatendarlehen-table').find('.aktuelle_restschuld_manuelle').val());
if(Aktuelle_Restschuld != ''){
    calculateTable1();
}

function calculateTable1(){
    var table = $('#annuitatendarlehen-table');
    var kapitaldienst_manuelle = makeNumber($(table).find('.kapitaldienst_manuelle').val());
    var aktuelle_restschuld_manuelle = makeNumber($(table).find('.aktuelle_restschuld_manuelle').val());

    var prev = new Array();
    var total_zinsaufwand = 0;

    for (var i = 1; i <= 12; i++) {

        if(i == 1){

            var aktuelle_restschuld = aktuelle_restschuld_manuelle;
            var kapitaldienst = kapitaldienst_manuelle;
            var zinsaufwand = (aktuelle_restschuld_manuelle * 0.013 / 12);
            var tilgung = (kapitaldienst - zinsaufwand);

            total_zinsaufwand += zinsaufwand;
            var data = [];

            data['aktuelle_restschuld'] = aktuelle_restschuld;
            data['kapitaldienst'] = kapitaldienst;
            data['zinsaufwand'] = zinsaufwand;
            data['tilgung'] = tilgung;

            prev[i] = data;

        }else{
            var prev_row = (i-1);
            var prevvalue = prev[prev_row];

            var aktuelle_restschuld = ( prevvalue['aktuelle_restschuld'] - prevvalue['tilgung'] );
            var kapitaldienst = kapitaldienst_manuelle;
            var zinsaufwand = (aktuelle_restschuld * 0.013 / 12);
            var tilgung = (kapitaldienst - zinsaufwand);

            total_zinsaufwand += zinsaufwand;
            var data = [];

            data['aktuelle_restschuld'] = aktuelle_restschuld;
            data['kapitaldienst'] = kapitaldienst;
            data['zinsaufwand'] = zinsaufwand;
            data['tilgung'] = tilgung;

            prev[i] = data;
        }

        $(table).find('.aktuelle_restschuld_'+i).val( makeNumberFormat(prev[i]['aktuelle_restschuld']) );
        $(table).find('.kapitaldienst_'+i).val( makeNumberFormat(prev[i]['kapitaldienst']) );
        $(table).find('.zinsaufwand_'+i).val( makeNumberFormat(prev[i]['zinsaufwand']) );
        $(table).find('.tilgung_'+i).val( makeNumberFormat(prev[i]['tilgung']) );
    }


    // var kapitaldienst_p_a = (6 * aktuelle_restschuld_manuelle) / 100;
    var kapitaldienst_p_a = makeNumber($(table).find('.kapitaldienst_p_a').val());
    var zinsaufwand_p_a = total_zinsaufwand;
    var tilgung_p_a = (kapitaldienst_p_a - zinsaufwand_p_a);
    tilgung_p_a = (tilgung_p_a >= 0) ? tilgung_p_a : 0;

    $(table).find('.kapitaldienst_p_a').val( makeNumberFormat(kapitaldienst_p_a) );
    $(table).find('.zinsaufwand_p_a').val( makeNumberFormat(zinsaufwand_p_a) );
    $(table).find('.tilgung_p_a').val( makeNumberFormat(tilgung_p_a) );

    $('#kapitaldienst').val( makeNumberFormat(kapitaldienst_p_a) );
    $('#zinsaufwand').val( makeNumberFormat(zinsaufwand_p_a) );
    $('#tilgung').val( makeNumberFormat(tilgung_p_a) );
    $('#aktuelle_restschuld').val( makeNumberFormat(aktuelle_restschuld_manuelle) );
}

function showMoreTable1(obj){
    var table = $('#annuitatendarlehen-table');
    // var last_tr = $(table).find('#month_row_12');

    var aktuelle_restschuld_12 = makeNumber($(table).find('.aktuelle_restschuld_12').val());
    var kapitaldienst_12 = makeNumber($(table).find('.kapitaldienst_12').val());
    var zinsaufwand_12 = makeNumber($(table).find('.zinsaufwand_12').val());
    var tilgung_12 = makeNumber($(table).find('.tilgung_12').val());

    var main_aktuelle_restschuld = aktuelle_restschuld_12;
    var total_zinsaufwand = 0;
    var i = 13;
    var prev = new Array();

    if(aktuelle_restschuld_12 > 0){

        $(obj).closest('tr').remove();

        var data = [];
        data['aktuelle_restschuld'] = aktuelle_restschuld_12;
        data['kapitaldienst'] = kapitaldienst_12;
        data['zinsaufwand'] = zinsaufwand_12;
        data['tilgung'] = tilgung_12;
        prev[12] = data;

        while (main_aktuelle_restschuld >= 0) {

            var prev_row = (i-1);
            var prevvalue = prev[prev_row];

            var aktuelle_restschuld = ( prevvalue['aktuelle_restschuld'] - prevvalue['tilgung'] );
            var kapitaldienst = kapitaldienst_12;
            var zinsaufwand = (aktuelle_restschuld * 0.013 / 12);
            var tilgung = (kapitaldienst - zinsaufwand);

            main_aktuelle_restschuld = aktuelle_restschuld;
            var data = [];

            data['aktuelle_restschuld'] = aktuelle_restschuld;
            data['kapitaldienst'] = kapitaldienst;
            data['zinsaufwand'] = zinsaufwand;
            data['tilgung'] = tilgung;

            prev[i] = data;
            i++;
        }

        prev.forEach(function (item, index) {
            if(index > 12){

                total_zinsaufwand += item['zinsaufwand'];

                var date = new Date();
                var newDate = new Date(date.setMonth(date.getMonth()+(index-1)));
                var month = monthShortNames[newDate.getMonth()];
                var year = newDate.getFullYear().toString().substr(-2);
                var full_month_year = month+'/'+year;

                var html = '\
                    <tr id="month_row_'+index+'" class="expand_row">\
                        <td>'+full_month_year+'</td>\
                        <td></td>\
                        <td><input type="text" class="mask-number-input aktuelle_restschuld_'+index+'" name="" value="'+makeNumberFormat( (item['aktuelle_restschuld'] >= 0) ? item['aktuelle_restschuld'] : 0 )+'" readonly=""></td>\
                        <td><input type="text" class="mask-number-input kapitaldienst_'+index+'" name="" value="'+makeNumberFormat(item['kapitaldienst'])+'" readonly=""></td>\
                        <td><input type="text" class="mask-number-input zinsaufwand_'+index+'" name="" value="'+makeNumberFormat(item['zinsaufwand'])+'" readonly=""></td>\
                        <td><input type="text" class="mask-number-input tilgung_'+index+'" name="" value="'+makeNumberFormat(item['tilgung'])+'" readonly=""></td>\
                    </tr>\
                ';
                $(table).find('tbody').append(html);
            }
        });

        $(table).find('tbody').append('<tr><td colspan="6"><button type="button" class="btn btn-xs btn-primary pull-right z_show_more">Show Less</button></td></tr>');

        // var kapitaldienst_p_a = (6 * aktuelle_restschuld_manuelle) / 100;
        /*var kapitaldienst_p_a = makeNumber($(table).find('.kapitaldienst_p_a').val());
        var zinsaufwand_p_a = makeNumber($(table).find('.zinsaufwand_p_a').val()) + total_zinsaufwand;
        var tilgung_p_a = (kapitaldienst_p_a - zinsaufwand_p_a);
        tilgung_p_a = (tilgung_p_a >= 0) ? tilgung_p_a : 0;

        $(table).find('.kapitaldienst_p_a').val( makeNumberFormat(kapitaldienst_p_a) );
        $(table).find('.zinsaufwand_p_a').val( makeNumberFormat(zinsaufwand_p_a) );
        $(table).find('.tilgung_p_a').val( makeNumberFormat(tilgung_p_a) );*/
    }

}

function showLessTable1(obj){
    var table = $('#annuitatendarlehen-table');
    if($(table).find('.expand_row').length){
        $(table).find('.expand_row').remove();
        $(table).find('.z_show_more').text('Show More');
        calculateTable1();
    }
}

function calculateTable2(){
    var table = $('#tilgungsdarlehen-table');
    var tilgung_manuelle = makeNumber($(table).find('.tilgung_manuelle').val());
    var aktuelle_restschuld_manuelle = makeNumber($(table).find('.aktuelle_restschuld_manuelle').val());

    var prev = new Array();
    var total_kapitaldienst = 0;
    var total_zins = 0;

    for (var i = 1; i <= 12; i++) {

        if(i == 1){

            var aktuelle_restschuld = aktuelle_restschuld_manuelle;
            var zins = (aktuelle_restschuld * 0.013 / 12);
            var tilgung = tilgung_manuelle;
            var kapitaldienst = (zins+tilgung);

            total_kapitaldienst += kapitaldienst;
            total_zins += zins;
            var data = [];

            data['aktuelle_restschuld'] = aktuelle_restschuld;
            data['zins'] = zins;
            data['tilgung'] = tilgung;
            data['kapitaldienst'] = kapitaldienst;

            prev[i] = data;

        }else{
            var prev_row = (i-1);
            var prevvalue = prev[prev_row];

            var aktuelle_restschuld = ( prevvalue['aktuelle_restschuld'] - prevvalue['tilgung'] );
            var zins = (aktuelle_restschuld * 0.013 / 12);
            var tilgung = tilgung_manuelle;
            var kapitaldienst = (zins+tilgung);

            total_kapitaldienst += kapitaldienst;
            total_zins += zins;
            var data = [];

            data['aktuelle_restschuld'] = aktuelle_restschuld;
            data['zins'] = zins;
            data['tilgung'] = tilgung;
            data['kapitaldienst'] = kapitaldienst;

            prev[i] = data;
        }

        $(table).find('.aktuelle_restschuld_'+i).val( makeNumberFormat(prev[i]['aktuelle_restschuld']) );
        $(table).find('.kapitaldienst_'+i).val( makeNumberFormat(prev[i]['kapitaldienst']) );
        $(table).find('.zins_'+i).val( makeNumberFormat(prev[i]['zins']) );
        $(table).find('.tilgung_'+i).val( makeNumberFormat(prev[i]['tilgung']) );
    }


    var kapitaldienst_p_a = total_kapitaldienst;
    var zinsaufwand_p_a = total_zins;
    var tilgung_p_a = (kapitaldienst_p_a - zinsaufwand_p_a);

    $(table).find('.kapitaldienst_p_a').val( makeNumberFormat(kapitaldienst_p_a) );
    $(table).find('.zinsaufwand_p_a').val( makeNumberFormat(zinsaufwand_p_a) );
    $(table).find('.tilgung_p_a').val( makeNumberFormat(tilgung_p_a) );
}

/*---------------------------------------------------------------------------*/

/*---------------------Auto complete for multiple comment - START-------------------------*/
$('.property-comment, #recommended_comment, #provision_comment, #insurance_detail_comment, .item-comment').suggest('@', {
    // data: users,
    data: function( request, response ) {
        $.ajax({
            url : url_get_all_users,
            type: "POST",
            dataType: "json",
            beforeSend: function() {},
            success: function(res){
              if(res.status == true){
                response(res.data);
              }else{
              }
            }
        });
    },
    map: function(user) {
        return {
          value: user.email,
          text: '<strong>'+user.email+'</strong>'
        }
    },
    onshow: function(e) {
    },
    onselect: function(e, item) {
        // console.log(item);
    },
    onlookup: function(e, item) {
    }
});
/*---------------------Auto complete for multiple comment - ENd-------------------------*/

$('body').on('click',".show_rec_cmnt_section .show-more a", function() {
    var $this = $(this);
    var $content = $this.parent().prev("div.comment-content");
    var linkText = $this.text().toUpperCase();

    if(linkText === "SHOW MORE"){
        linkText = "Show less";
        $content.switchClass("hideContent", "showContent", 400);
    } else {
        linkText = "Show more";
        $content.switchClass("showContent", "hideContent", 400);
    };

    $this.text(linkText);
});

$('body').on('click',".btn-edit-recommended", function() {
    $(this).closest('tr').find('input.change-comment-recommended').prop('readonly', false);
    $(this).closest('tr').find('select.change-comment-recommended').prop('disabled', false);
    $(this).closest('tr').find('.kosten_umbau_td span').addClass('hidden');
    $(this).closest('tr').find('.kosten_umbau_td a').removeClass('hidden');
});


/*---------------------JS FOR RELEASE INVOICE - START----------------*/
/*var current_btn;

var inv_am_release_id;
var inv_am_release_status;

$('body').on('click', '.btn-inv-release-am', function(){
    var $this = $(this);
    current_btn = $this;
    var modal = $('#invoice-release-am-modal');

    inv_am_release_id = $(this).attr('data-id');
    inv_am_release_status = $(this).attr('data-status');
    var title = $(this).attr('title');

    $(modal).find('.modal-title').text(title);
    $(modal).modal('show');
});

$('#invoice-release-am-form').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var modal = $('#invoice-release-am-modal');
    var url = $($this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();

    var data = {'id': inv_am_release_id, 'status': inv_am_release_status, 'comment': comment};

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);

            if (result.status) {

                $($this)[0].reset();
                $('#invoice-release-am-error').empty();
                $(modal).modal('hide');

                loadButtonProcessTable();

            }else{
              showFlash($('#invoice-release-am-error'), result.message, 'error');
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            showFlash($('#invoice-release-am-error'), 'Somthing want wrong!', 'error');
        }
    });
}));

var inv_hv_release_id;
var inv_hv_release_status;

$('body').on('click', '.btn-inv-release-hv', function(){
    var $this = $(this);
    current_btn = $this;
    var modal = $('#invoice-release-hv-modal');

    inv_hv_release_id = $(this).attr('data-id');
    inv_hv_release_status = $(this).attr('data-status');
    var title = $(this).attr('title');

    $(modal).find('.modal-title').text(title);
    $(modal).modal('show');
});

$('#invoice-release-hv-form').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var modal = $('#invoice-release-hv-modal');
    var url = $($this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();

    var data = {'id': inv_hv_release_id, 'status': inv_hv_release_status, 'comment': comment};

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);

            if (result.status) {

                $($this)[0].reset();
                $('#invoice-release-hv-error').empty();
                $(modal).modal('hide');

                loadButtonProcessTable();

            }else{
              showFlash($('#invoice-release-hv-error'), result.message, 'error');
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            showFlash($('#invoice-release-hv-error'), 'Somthing want wrong!', 'error');
        }
    });
}));

var inv_usr_release_id;
var inv_usr_release_status;

$('body').on('click', '.btn-inv-release-usr', function(){
    var $this = $(this);
    current_btn = $this;
    var modal = $('#invoice-release-usr-modal');

    inv_usr_release_id = $(this).attr('data-id');
    inv_usr_release_status = $(this).attr('data-status');
    var title = $(this).attr('title');

    $(modal).find('.modal-title').text(title);
    $(modal).modal('show');
});

$('#invoice-release-usr-form').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var modal = $('#invoice-release-usr-modal');
    var url = $($this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var user_id = $($this).find('select[name="user"]').val();

    var data = {
        'id': inv_usr_release_id,
        'status': inv_usr_release_status,
        'comment': comment,
        'user_id': user_id
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);

            if (result.status) {

                $($this)[0].reset();
                $('#invoice-release-usr-error').empty();
                $(modal).modal('hide');

                loadButtonProcessTable();

            }else{
              showFlash($('#invoice-release-usr-error'), result.message, 'error');
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            showFlash($('#invoice-release-usr-error'), 'Somthing want wrong!', 'error');
        }
    });
}));

var inv_falk_release_id;
var inv_falk_release_status;

$('body').on('click', '.btn-inv-release-falk', function(){
    var $this = $(this);
    current_btn = $this;
    var modal = $('#invoice-release-falk-modal');

    inv_falk_release_id = $(this).attr('data-id');
    inv_falk_release_status = $(this).attr('data-status');
    var title = $(this).attr('title');

    $(modal).find('.modal-title').text(title);
    $(modal).modal('show');
});

$('#invoice-release-falk-form').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var modal = $('#invoice-release-falk-modal');
    var url = $($this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();

    var data = {'id': inv_falk_release_id, 'status': inv_falk_release_status, 'comment': comment};

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);

            if (result.status) {

                $($this)[0].reset();
                $('#invoice-release-falk-error').empty();
                $(modal).modal('hide');

                loadButtonProcessTable();

            }else{
              showFlash($('#invoice-release-falk-error'), result.message, 'error');
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            showFlash($('#invoice-release-falk-error'), 'Somthing want wrong!', 'error');
        }
    });
}));

function loadButtonProcessTable(){
    var table1 = $(current_btn).closest('table').attr('id');
    var table2 = '';
    var status = $(current_btn).attr('data-status');

    if( $(current_btn).hasClass('btn-inv-release-am') ){

        if(status == '1'){//request
            table2 = 'add_property_invoice_table';
        }else if(status == '2'){//release
            table2 = 'add_property_invoice_table';
        }else if(status == '3'){//pending
            table2 = 'pending_am_invoice_table';
        }else if(status == '4'){//not release
            table2 = 'table_rejected_invoice';
        }

    }else if( $(current_btn).hasClass('btn-inv-release-hv') ){

        if(status == '1'){//request
            table2 = 'add_property_invoice_table';
        }else if(status == '2'){//release
            table2 = 'add_property_invoice_table';
        }else if(status == '3'){//pending
            table2 = 'pending_hv_invoice_table';
        }else if(status == '4'){//not release
            table2 = 'table_not_release_hv_invoice';
        }

    }else if( $(current_btn).hasClass('btn-inv-release-usr') ){

        if(status == '1'){//request
            table2 = 'add_property_invoice_table';
        }else if(status == '2'){//release
            table2 = 'add_property_invoice_table';
        }else if(status == '3'){//pending
            table2 = 'pending_user_invoice_table';
        }else if(status == '4'){//not release
            table2 = 'table_not_release_user_invoice';
        }

    }else if( $(current_btn).hasClass('btn-inv-release-falk') ){

        if(status == '1'){//request
            table2 = 'add_property_invoice_table';
        }else if(status == '2'){//release
            table2 = 'add_property_invoice_table2';
        }else if(status == '3'){//pending
            table2 = 'add_property_invoice_table4';
        }else if(status == '4'){//not release
            table2 = 'add_property_invoice_table3';
        }
    }

    if(typeof table1 !== 'undefined' && table1){
        $('#'+table1).DataTable().ajax.reload();
    }
    if(typeof table2 !== 'undefined' && table2 && table2 != table1){
        $('#'+table2).DataTable().ajax.reload();
    }
    $('#invoice_mail_table').DataTable().ajax.reload();
}*/

/*---------------------JS FOR RELEASE INVOICE - END----------------*/

/*---------------------NEW - JS FOR RELEASE INVOICE - START----------------*/

var current_btn;

$('body').on('click', '.btn-inv-release-am', function(){

    var $this = $(this);
    current_btn = $this;

    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    var data = {'id': id, 'status': status, 'comment': ''};

    $.ajax({
        url: url_invoice_release_am,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status) {
                loadButtonProcessTable();
            }else{
                alert(result.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

$('body').on('click', '.btn-inv-release-hv', function(){

    var $this = $(this);
    current_btn = $this;

    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    var data = {'id': id, 'status': status, 'comment': ''};

    $.ajax({
        url: url_invoice_release_hv,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status) {
                loadButtonProcessTable();
            }else{
                alert(result.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

var inv_usr_release_id;
var inv_usr_release_status;

$('body').on('click', '.btn-inv-release-usr', function(){
    var $this = $(this);
    current_btn = $this;
    var modal = $('#invoice-release-usr-modal');

    inv_usr_release_id = $(this).attr('data-id');
    inv_usr_release_status = $(this).attr('data-status');
    var title = $(this).attr('title');

    if(inv_usr_release_status == '1'){

        $(modal).find('.modal-title').text(title);
        $(modal).modal('show');

    }else{

        var data = {'id': inv_usr_release_id, 'status': inv_usr_release_status, 'comment': ''};

        $.ajax({
            url: url_invoice_release_user,
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function(result) {
                if (result.status) {
                    loadButtonProcessTable();
                }else{
                    alert(result.message);
                }
            },
            error: function(error) {
                alert('Somthing want wrong!');
            }
        });
    }
});

$('#invoice-release-usr-form').on('submit', (function(e) {
    e.preventDefault();

    var $this = $(this);
    var modal = $('#invoice-release-usr-modal');
    var url = $($this).attr('action');
    var comment = $($this).find('textarea[name="comment"]').val();
    var user_id = $($this).find('select[name="user"]').val();

    var data = {
        'id': inv_usr_release_id,
        'status': inv_usr_release_status,
        'comment': '',
        'user_id': user_id
    };

    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        dataType: 'json',
        beforeSend: function() {
            $($this).find('button[type="submit"]').prop('disabled', true);
        },
        success: function(result) {

            $($this).find('button[type="submit"]').prop('disabled', false);

            if (result.status) {

                $($this)[0].reset();
                $('#invoice-release-usr-error').empty();
                $(modal).modal('hide');

                loadButtonProcessTable();

            }else{
              showFlash($('#invoice-release-usr-error'), result.message, 'error');
            }
        },
        error: function(error) {
            $($this).find('button[type="submit"]').prop('disabled', false);
            showFlash($('#invoice-release-usr-error'), 'Somthing want wrong!', 'error');
        }
    });
}));

$('body').on('click', '.btn-inv-release-falk', function(){

    var $this = $(this);
    current_btn = $this;

    var id = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    var data = {'id': id, 'status': status, 'comment': ''};

    $.ajax({
        url: url_invoice_release_falk,
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(result) {
            if (result.status) {
                loadButtonProcessTable();
            }else{
                alert(result.message);
            }
        },
        error: function(error) {
            alert('Somthing want wrong!');
        }
    });
});

function loadButtonProcessTable(){
    var table1 = $(current_btn).closest('table').attr('id');
    var table2 = '';
    var status = $(current_btn).attr('data-status');

    if( $(current_btn).hasClass('btn-inv-release-am') ){

        if(status == '1'){//request
            table2 = 'add_property_invoice_table';
        }else if(status == '2'){//release
            table2 = 'add_property_invoice_table';
        }else if(status == '3'){//pending
            table2 = 'pending_am_invoice_table';
        }else if(status == '4'){//not release
            table2 = 'table_rejected_invoice';
        }

    }else if( $(current_btn).hasClass('btn-inv-release-hv') ){

        if(status == '1'){//request
            table2 = 'add_property_invoice_table';
        }else if(status == '2'){//release
            table2 = 'add_property_invoice_table';
        }else if(status == '3'){//pending
            table2 = 'pending_hv_invoice_table';
        }else if(status == '4'){//not release
            table2 = 'table_not_release_hv_invoice';
        }

    }else if( $(current_btn).hasClass('btn-inv-release-usr') ){

        if(status == '1'){//request
            table2 = 'add_property_invoice_table';
        }else if(status == '2'){//release
            table2 = 'add_property_invoice_table';
        }else if(status == '3'){//pending
            table2 = 'pending_user_invoice_table';
        }else if(status == '4'){//not release
            table2 = 'table_not_release_user_invoice';
        }

    }else if( $(current_btn).hasClass('btn-inv-release-falk') ){

        if(status == '1'){//request
            table2 = 'add_property_invoice_table';
        }else if(status == '2'){//release
            table2 = 'add_property_invoice_table2';
        }else if(status == '3'){//pending
            table2 = 'add_property_invoice_table4';
        }else if(status == '4'){//not release
            table2 = 'add_property_invoice_table3';
        }
    }

    if(typeof table1 !== 'undefined' && table1){
        $('#'+table1).DataTable().ajax.reload();
    }
    if(typeof table2 !== 'undefined' && table2 && table2 != table1){
        $('#'+table2).DataTable().ajax.reload();
    }
    $('#invoice_mail_table').DataTable().ajax.reload();
}
/*---------------------NEW - JS FOR RELEASE INVOICE - END----------------*/
