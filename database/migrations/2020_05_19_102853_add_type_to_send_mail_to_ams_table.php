<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToSendMailToAmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('send_mail_to_ams', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0)->after('message')->comment('0 = AM, 1 = HV BU, 2 = HV PM ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('send_mail_to_ams', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
