<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePDFTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pdf', function (Blueprint $table) {
            $table->string('file_basename')->nullable();
            $table->string('file_path')->nullable();
            $table->string('file_href')->nullable();
            $table->string('extension')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pdf', function (Blueprint $table) {
			$table->dropColumn('file_basename');
			$table->dropColumn('file_path');
			$table->dropColumn('file_href');
			$table->dropColumn('extension');
        });
    }
}
