<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlacheToProvisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provisions', function (Blueprint $table) {
            $table->string('flache')->nullable();
            $table->double('einnahmen_pm')->default(0)->nullable();
            $table->double('laufzeit_in_monate')->default(0)->nullable();
            $table->double('mietfrei')->default(0)->nullable();
            $table->double('jahrl_einnahmen')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provisions', function (Blueprint $table) {
            $table->dropColumn('flache');
            $table->dropColumn('einnahmen_pm');
            $table->dropColumn('laufzeit_in_monate');
            $table->dropColumn('mietfrei');
            $table->dropColumn('jahrl_einnahmen');
        });
    }
}
