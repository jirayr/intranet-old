<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmStatusToPropertyInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_invoices', function (Blueprint $table) {
            $table->tinyInteger('am_status')->default(0)->after('not_release_status');
            $table->tinyInteger('hv_status')->default(0)->after('am_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_invoices', function (Blueprint $table) {
            $table->dropColumn('am_status');
            $table->dropColumn('hv_status');
        });
    }
}
