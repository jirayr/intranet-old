<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCleverFitFieldsToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->text('clever_fit_fields')->nullable();
//            $table->string('entkernung_erdgeschoss',255)->nullable();
//            $table->string('jahr_nettokaltmiete_1',255)->nullable();
//            $table->string('jahr_em_nkm_1',255)->nullable();
//            $table->string('jahr_nkm_pa_1',255)->nullable();
//            $table->string('nettokaltmiete_1',255)->nullable();
//            $table->string('em_nkm_1',255)->nullable();
//            $table->string('nkm_pa_1',255)->nullable();
//            $table->string('jahr_nettokaltmiete_2',255)->nullable();
//            $table->string('jahr_em_nkm_2',255)->nullable();
//            $table->string('jahr_nkm_pa_2',255)->nullable();
//            $table->string('nettokaltmiete_2',255)->nullable();
//            $table->string('em_nkm_2',255)->nullable();
//            $table->string('nkm_pa_2',255)->nullable();
//            $table->string('jahr_nettokaltmiete_3',255)->nullable();
//            $table->string('jahr_em_nkm_3',255)->nullable();
//            $table->string('jahr_nkm_pa_3',255)->nullable();
//            $table->string('nettokaltmiete_3',255)->nullable();
//            $table->string('em_nkm_3',255)->nullable();
//            $table->string('nkm_pa_3',255)->nullable();
//            $table->string('schliebung_der_galerie',255)->nullable();
//            $table->string('jahr_nettokaltmiete_4',255)->nullable();
//            $table->string('jahr_em_nkm_4',255)->nullable();
//            $table->string('jahr_nkm_pa_4',255)->nullable();
//            $table->string('nettokaltmiete_4',255)->nullable();
//            $table->string('em_nkm_4',255)->nullable();
//            $table->string('nkm_pa_4',255)->nullable();
//            $table->string('ausbaukostenzuschuss_clever_fit',255)->nullable();
//            $table->string('jahr_nettokaltmiete_5',255)->nullable();
//            $table->string('jahr_em_nkm_5',255)->nullable();
//            $table->string('jahr_nkm_pa_5',255)->nullable();
//            $table->string('nettokaltmiete_5',255)->nullable();
//            $table->string('em_nkm_5',255)->nullable();
//            $table->string('nkm_pa_5',255)->nullable();
//            $table->string('neuinstallation_heizung_elektro_luftung',255)->nullable();
//            $table->string('jahr_nettokaltmiete_6',255)->nullable();
//            $table->string('jahr_em_nkm_6',255)->nullable();
//            $table->string('jahr_nkm_pa_6',255)->nullable();
//            $table->string('entsorgung',255)->nullable();
//            $table->string('jahr_nettokaltmiete_7',255)->nullable();
//            $table->string('jahr_em_nkm_7',255)->nullable();
//            $table->string('jahr_nkm_pa_7',255)->nullable();
//            $table->string('jahr_nettokaltmiete_8',255)->nullable();
//            $table->string('jahr_em_nkm_8',255)->nullable();
//            $table->string('jahr_nkm_pa_8',255)->nullable();
//            $table->string('nettokaltmiete_8',255)->nullable();
//            $table->string('em_nkm_8',255)->nullable();
//            $table->string('nkm_pa_8',255)->nullable();
//            $table->string('jahr_nettokaltmiete_9',255)->nullable();
//            $table->string('jahr_em_nkm_9',255)->nullable();
//            $table->string('jahr_nkm_pa_9',255)->nullable();
//            $table->string('jahr_nettokaltmiete_10',255)->nullable();
//            $table->string('jahr_em_nkm_10',255)->nullable();
//            $table->string('jahr_nkm_pa_10',255)->nullable();
//            $table->string('calculation_invest_total',255)->nullable();
//            $table->string('jahr_nettokaltmiete_11',255)->nullable();
//            $table->string('jahr_em_nkm_11',255)->nullable();
//            $table->string('jahr_nkm_pa_11',255)->nullable();
//            $table->string('jahr_nettokaltmiete_12',255)->nullable();
//            $table->string('jahr_em_nkm_12',255)->nullable();
//            $table->string('jahr_nkm_pa_12',255)->nullable();
//            $table->string('jahr_nettokaltmiete_13',255)->nullable();
//            $table->string('jahr_em_nkm_13',255)->nullable();
//            $table->string('jahr_nkm_pa_13',255)->nullable();
//            $table->string('jahr_nettokaltmiete_14',255)->nullable();
//            $table->string('jahr_em_nkm_14',255)->nullable();
//            $table->string('jahr_nkm_pa_14',255)->nullable();
//            $table->string('jahr_nettokaltmiete_15',255)->nullable();
//            $table->string('jahr_em_nkm_15',255)->nullable();
//            $table->string('jahr_nkm_pa_15',255)->nullable();
//            $table->string('mittelwert_1',255)->nullable();
//            $table->string('mittelwert_2',255)->nullable();
//            $table->string('summe_1',255)->nullable();
//            $table->string('moglicher_verkaufspreis_1',255)->nullable();
//            $table->string('moglicher_verkaufspreis_2',255)->nullable();
//            $table->string('moglicher_verkaufspreis_3',255)->nullable();
//            $table->string('moglicher_verkaufspreis_4',255)->nullable();
//            $table->string('moglicher_verkaufspreis_5',255)->nullable();
//            $table->string('einkaufspreis_buchwert_1',255)->nullable();
//            $table->string('einkaufspreis_buchwert_2',255)->nullable();
//            $table->string('einkaufspreis_buchwert_3',255)->nullable();
//            $table->string('einkaufspreis_buchwert_4',255)->nullable();
//            $table->string('einkaufspreis_buchwert_5',255)->nullable();
//            $table->string('delta_1',255)->nullable();
//            $table->string('delta_2',255)->nullable();
//            $table->string('delta_3',255)->nullable();
//            $table->string('delta_4',255)->nullable();
//            $table->string('delta_5',255)->nullable();
//            $table->string('invest_inkl_ekruckzahlung_1',255)->nullable();
//            $table->string('invest_inkl_ekruckzahlung_2',255)->nullable();
//            $table->string('invest_inkl_ekruckzahlung_3',255)->nullable();
//            $table->string('invest_inkl_ekruckzahlung_4',255)->nullable();
//            $table->string('clever_fit_total_1',255)->nullable();
//            $table->string('clever_fit_total_2',255)->nullable();
//            $table->string('clever_fit_total_3',255)->nullable();
//            $table->string('clever_fit_total_4',255)->nullable();
//            $table->string('clever_fit_total_5',255)->nullable();
//            $table->string('clever_fit_total_6',255)->nullable();
//            $table->string('clever_fit_total_7',255)->nullable();
//            $table->string('tedi_total_1',255)->nullable();
//            $table->string('tedi_total_2',255)->nullable();
//            $table->string('tedi_total_3',255)->nullable();
//            $table->string('tedi_total_4',255)->nullable();
//            $table->string('tedi_total_5',255)->nullable();
//            $table->string('tedi_total_6',255)->nullable();
//            $table->string('tedi_total_7',255)->nullable();
//            $table->string('tedi_total_8',255)->nullable();
//            $table->string('gewerbe_vermietet_1',255)->nullable();
//            $table->string('gewerbe_vermietet_2',255)->nullable();
//            $table->string('pot_kellerflachen_f_wohnungen_1',255)->nullable();
//            $table->string('pot_kellerflachen_f_wohnungen_2',255)->nullable();
//            $table->string('pot_wohnungen_1',255)->nullable();
//            $table->string('pot_wohnungen_2',255)->nullable();
//            $table->string('gewerbe_leerstand_1',255)->nullable();
//            $table->string('gewerbe_leerstand_2',255)->nullable();
//            $table->string('gewerbe_leerstand_3',255)->nullable();
//            $table->string('gewerbe_leerstand_4',255)->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('clever_fit_fields');
//            Schema::dropColumn('entkernung_erdgeschoss');
//            Schema::dropColumn('jahr_nettokaltmiete_1');
//            Schema::dropColumn('jahr_em_nkm_1');
//            Schema::dropColumn('jahr_nkm_pa_1');
//            Schema::dropColumn('nettokaltmiete_1');
//            Schema::dropColumn('em_nkm_1');
//            Schema::dropColumn('nkm_pa_1');
//            Schema::dropColumn('jahr_nettokaltmiete_2');
//            Schema::dropColumn('jahr_em_nkm_2');
//            Schema::dropColumn('jahr_nkm_pa_2');
//            Schema::dropColumn('nettokaltmiete_2');
//            Schema::dropColumn('em_nkm_2');
//            Schema::dropColumn('nkm_pa_2');
//            Schema::dropColumn('jahr_nettokaltmiete_3');
//            Schema::dropColumn('jahr_em_nkm_3');
//            Schema::dropColumn('jahr_nkm_pa_3');
//            Schema::dropColumn('nettokaltmiete_3');
//            Schema::dropColumn('em_nkm_3');
//            Schema::dropColumn('nkm_pa_3');
//            Schema::dropColumn('schliebung_der_galerie');
//            Schema::dropColumn('jahr_nettokaltmiete_4');
//            Schema::dropColumn('jahr_em_nkm_4');
//            Schema::dropColumn('jahr_nkm_pa_4');
//            Schema::dropColumn('nettokaltmiete_4');
//            Schema::dropColumn('em_nkm_4');
//            Schema::dropColumn('nkm_pa_4');
//            Schema::dropColumn('ausbaukostenzuschuss_clever_fit');
//            Schema::dropColumn('jahr_nettokaltmiete_5');
//            Schema::dropColumn('jahr_em_nkm_5');
//            Schema::dropColumn('jahr_nkm_pa_5');
//            Schema::dropColumn('nettokaltmiete_5');
//            Schema::dropColumn('em_nkm_5');
//            Schema::dropColumn('nkm_pa_5');
//            Schema::dropColumn('neuinstallation_heizung_elektro_luftung');
//            Schema::dropColumn('jahr_nettokaltmiete_6');
//            Schema::dropColumn('jahr_em_nkm_6');
//            Schema::dropColumn('jahr_nkm_pa_6');
//            Schema::dropColumn('entsorgung');
//            Schema::dropColumn('jahr_nettokaltmiete_7');
//            Schema::dropColumn('jahr_em_nkm_7');
//            Schema::dropColumn('jahr_nkm_pa_7');
//            Schema::dropColumn('jahr_nettokaltmiete_8');
//            Schema::dropColumn('jahr_em_nkm_8');
//            Schema::dropColumn('jahr_nkm_pa_8');
//            Schema::dropColumn('nettokaltmiete_8');
//            Schema::dropColumn('em_nkm_8');
//            Schema::dropColumn('nkm_pa_8');
//            Schema::dropColumn('jahr_nettokaltmiete_9');
//            Schema::dropColumn('jahr_em_nkm_9');
//            Schema::dropColumn('jahr_nkm_pa_9');
//            Schema::dropColumn('jahr_nettokaltmiete_10');
//            Schema::dropColumn('jahr_em_nkm_10');
//            Schema::dropColumn('jahr_nkm_pa_10');
//            Schema::dropColumn('calculation_invest_total');
//            Schema::dropColumn('jahr_nettokaltmiete_11');
//            Schema::dropColumn('jahr_em_nkm_11');
//            Schema::dropColumn('jahr_nkm_pa_11');
//            Schema::dropColumn('jahr_nettokaltmiete_12');
//            Schema::dropColumn('jahr_em_nkm_12');
//            Schema::dropColumn('jahr_nkm_pa_12');
//            Schema::dropColumn('jahr_nettokaltmiete_13');
//            Schema::dropColumn('jahr_em_nkm_13');
//            Schema::dropColumn('jahr_nkm_pa_13');
//            Schema::dropColumn('jahr_nettokaltmiete_14');
//            Schema::dropColumn('jahr_em_nkm_14');
//            Schema::dropColumn('jahr_nkm_pa_14');
//            Schema::dropColumn('jahr_nettokaltmiete_15');
//            Schema::dropColumn('jahr_em_nkm_15');
//            Schema::dropColumn('jahr_nkm_pa_15');
//            Schema::dropColumn('mittelwert_1');
//            Schema::dropColumn('mittelwert_2');
//            Schema::dropColumn('summe_1');
//            Schema::dropColumn('moglicher_verkaufspreis_1');
//            Schema::dropColumn('moglicher_verkaufspreis_2');
//            Schema::dropColumn('moglicher_verkaufspreis_3');
//            Schema::dropColumn('moglicher_verkaufspreis_4');
//            Schema::dropColumn('moglicher_verkaufspreis_5');
//            Schema::dropColumn('einkaufspreis_buchwert_1');
//            Schema::dropColumn('einkaufspreis_buchwert_2');
//            Schema::dropColumn('einkaufspreis_buchwert_3');
//            Schema::dropColumn('einkaufspreis_buchwert_4');
//            Schema::dropColumn('einkaufspreis_buchwert_5');
//            Schema::dropColumn('delta_1');
//            Schema::dropColumn('delta_2');
//            Schema::dropColumn('delta_3');
//            Schema::dropColumn('delta_4');
//            Schema::dropColumn('delta_5');
//            Schema::dropColumn('invest_inkl_ekruckzahlung_1');
//            Schema::dropColumn('invest_inkl_ekruckzahlung_2');
//            Schema::dropColumn('invest_inkl_ekruckzahlung_3');
//            Schema::dropColumn('invest_inkl_ekruckzahlung_4');
//            Schema::dropColumn('invest_inkl_ekruckzahlung_5');
//            Schema::dropColumn('clever_fit_total_1');
//            Schema::dropColumn('clever_fit_total_2');
//            Schema::dropColumn('clever_fit_total_3');
//            Schema::dropColumn('clever_fit_total_4');
//            Schema::dropColumn('clever_fit_total_5');
//            Schema::dropColumn('clever_fit_total_6');
//            Schema::dropColumn('clever_fit_total_7');
//            Schema::dropColumn('tedi_total_1');
//            Schema::dropColumn('tedi_total_2');
//            Schema::dropColumn('tedi_total_3');
//            Schema::dropColumn('tedi_total_4');
//            Schema::dropColumn('tedi_total_5');
//            Schema::dropColumn('tedi_total_6');
//            Schema::dropColumn('tedi_total_7');
//            Schema::dropColumn('tedi_total_8');
//            Schema::dropColumn('gewerbe_vermietet_1');
//            Schema::dropColumn('gewerbe_vermietet_2');
//            Schema::dropColumn('pot_kellerflachen_f_wohnungen_1');
//            Schema::dropColumn('pot_kellerflachen_f_wohnungen_2');
//            Schema::dropColumn('pot_wohnungen_1');
//            Schema::dropColumn('gewerbe_leerstand_1');
//            Schema::dropColumn('gewerbe_leerstand_2');
//            Schema::dropColumn('gewerbe_leerstand_3');
//            Schema::dropColumn('gewerbe_leerstand_4');

        });
    }
}
