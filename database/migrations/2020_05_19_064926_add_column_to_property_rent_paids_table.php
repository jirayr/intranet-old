<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToPropertyRentPaidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_rent_paids', function (Blueprint $table) {
            $table->string('sv')->nullable();
            $table->string('dta')->nullable();
            $table->string('zahlungsdatum')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_rent_paids', function (Blueprint $table) {
            $table->string('sv')->nullable();
            $table->string('dta')->nullable();
            $table->string('zahlungsdatum')->nullable();
        });
    }
}
