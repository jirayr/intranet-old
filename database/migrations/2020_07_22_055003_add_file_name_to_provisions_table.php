<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileNameToProvisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provisions', function (Blueprint $table) {
            $table->string('file_name')->nullable();
            $table->string('file_basename')->nullable();
            $table->string('file_path')->nullable();
            $table->string('file_href')->nullable();
            $table->string('file_extension')->nullable();
            $table->string('file_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provisions', function (Blueprint $table) {
            $table->dropColumn('file_name');
            $table->dropColumn('file_basename');
            $table->dropColumn('file_path');
            $table->dropColumn('file_href');
            $table->dropColumn('extension');
            $table->dropColumn('file_type');
        });
    }
}
