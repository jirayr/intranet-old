<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties_deals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('invoice',255)->nullable();
            $table->string('file_basename',255)->nullable();
            $table->string('file_dirname',255)->nullable();
            $table->string('file_type',255)->nullable();
            $table->text('comment')->nullable();
            $table->text('comment2')->nullable();
            $table->string('email',100)->nullable();
            $table->boolean('is_paid')->default(0)->nullable();
            $table->double('amount')->default(0)->nullable();
            $table->date('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties_deals');
    }
}
