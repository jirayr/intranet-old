<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenancyScheduleItemFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenancy_schedule_item_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->unsignedInteger('tenancy_schedule_item_id');
            $table->foreign('tenancy_schedule_item_id')->references('id')->on('tenancy_schedule_items');
            $table->string('type')->nullable();
            $table->string('file_name')->nullable();
            $table->string('file_basename')->nullable();
            $table->string('file_path')->nullable();
            $table->string('file_href')->nullable();
            $table->string('extension')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenancy_schedule_item_files');
    }
}
