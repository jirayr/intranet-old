<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationFieldsInProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // added in property_subfields because properties table have 200+ columns
        Schema::table('property_subfields', function (Blueprint $table) {
            $table->string('location_latitude')->nullable();
            $table->string('location_longitude')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_subfields', function (Blueprint $table) {
            $table->dropColumn('location_latitude');
            $table->dropColumn('location_longitude');
        });
    }
}
