<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFields2ToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('properties', function (Blueprint $table) {
            $table->text('strasse')->nullable();
            $table->text('plz_ort')->nullable();
            $table->text('gesamtflache_qm')->nullable();
            $table->text('grunderwerbssteuer')->nullable();
            $table->text('einkaufspreis')->nullable();
            $table->text('verkaufspreis')->nullable();
            $table->text('verkaufer')->nullable();
            $table->text('hausmaxx')->nullable();
            $table->text('hausmeister')->nullable();
            $table->text('telefon')->nullable();
            $table->text('schl_wault')->nullable();
            $table->text('flache_in_qm1')->nullable();
            $table->text('flache_in_qm2')->nullable();
            $table->text('flache_in_qm3')->nullable();
            $table->text('mietbeginn1')->nullable();
            $table->text('mietbeginn2')->nullable();
            $table->text('mietbeginn3')->nullable();
            $table->text('gewerbe_flachenanteil')->nullable();
            $table->text('buro_praxen_flachenanteil')->nullable();
            $table->text('wohnungen_flachenanteil')->nullable();
            $table->text('lager_flachenanteil')->nullable();
            $table->text('stellplatze_flachenanteil')->nullable();
            $table->text('sonstiges_flachenanteil')->nullable();
            $table->text('flache1')->nullable();
            $table->text('flache4')->nullable();
            $table->text('flache5')->nullable();
            $table->text('potentiellemiete1')->nullable();
            $table->text('potentiellemiete2')->nullable();
            $table->text('potentiellemiete3')->nullable();
            $table->text('potentiellemiete4')->nullable();
            $table->text('potentiellemiete5')->nullable();
            $table->text('instandhaltung')->nullable();
            $table->text('investition_vermietung')->nullable();
            $table->text('instandhaltung_ma')->nullable();
            $table->text('investition_vermietung_ma')->nullable();
            $table->text('entkernung')->nullable();
            $table->text('elektro_heizung_sprinkler')->nullable();
            $table->text('axa')->nullable();
            $table->text('allianz')->nullable();
            $table->text('gebaude_betrag')->nullable();
            $table->text('gebaude_laufzeit_from')->nullable();
            $table->text('gebaude_laufzeit_to')->nullable();
            $table->text('gebaude_kundigungsfrist')->nullable();
            $table->text('haftplicht_betrag')->nullable();
            $table->text('haftplicht_laufzeit_from')->nullable();
            $table->text('haftplicht_laufzeit_to')->nullable();
            $table->text('haftplicht_kundigungsfrist')->nullable();
            $table->text('denstleister1')->nullable();
            $table->text('denstleister2')->nullable();
            $table->text('denstleister3')->nullable();
            $table->text('denstleister4')->nullable();
            $table->text('heizung')->nullable();
            $table->text('exklusivitat1')->nullable();
            $table->text('exklusivitat2')->nullable();
            $table->text('exklusivitat3')->nullable();
            $table->text('exklusivitat4')->nullable();
            $table->text('sonstiges3')->nullable();
            $table->text('schl_banks')->nullable();
            $table->text('kommentar')->nullable();
            $table->text('ankermieter1')->nullable();
            $table->text('ankermieter2')->nullable();
            $table->text('ankermieter3')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('strasse');
            $table->dropColumn('plz_ort');
            $table->dropColumn('gesamtflache_qm');
            $table->dropColumn('grunderwerbssteuer');
            $table->dropColumn('einkaufspreis');
            $table->dropColumn('verkaufspreis');
            $table->dropColumn('verkaufer');
            $table->dropColumn('hausmaxx');
            $table->dropColumn('hausmeister');
            $table->dropColumn('telefon');
            $table->dropColumn('schl_wault');
            $table->dropColumn('flache_in_qm1');
            $table->dropColumn('flache_in_qm2');
            $table->dropColumn('flache_in_qm3');
            $table->dropColumn('mietbeginn1');
            $table->dropColumn('mietbeginn2');
            $table->dropColumn('mietbeginn3');
            $table->dropColumn('gewerbe_flachenanteil');
            $table->dropColumn('buro_praxen_flachenanteil');
            $table->dropColumn('wohnungen_flachenanteil');
            $table->dropColumn('lager_flachenanteil');
            $table->dropColumn('stellplatze_flachenanteil');
            $table->dropColumn('sonstiges_flachenanteil');
            $table->dropColumn('flache1');
            $table->dropColumn('flache4');
            $table->dropColumn('flache5');
            $table->dropColumn('potentiellemiete1');
            $table->dropColumn('potentiellemiete2');
            $table->dropColumn('potentiellemiete3');
            $table->dropColumn('potentiellemiete4');
            $table->dropColumn('potentiellemiete5');
            $table->dropColumn('instandhaltung');
            $table->dropColumn('investition_vermietung');
            $table->dropColumn('instandhaltung_ma');
            $table->dropColumn('investition_vermietung_ma');
            $table->dropColumn('entkernung');
            $table->dropColumn('elektro_heizung_sprinkler');
            $table->dropColumn('axa');
            $table->dropColumn('allianz');
            $table->dropColumn('gebaude_betrag');
            $table->dropColumn('gebaude_laufzeit_from');
            $table->dropColumn('gebaude_laufzeit_to');
            $table->dropColumn('gebaude_kundigungsfrist');
            $table->dropColumn('haftplicht_betrag');
            $table->dropColumn('haftplicht_laufzeit_from');
            $table->dropColumn('haftplicht_laufzeit_to');
            $table->dropColumn('haftplicht_kundigungsfrist');
            $table->dropColumn('denstleister1');
            $table->dropColumn('denstleister2');
            $table->dropColumn('denstleister3');
            $table->dropColumn('denstleister4');
            $table->dropColumn('heizung');
            $table->dropColumn('exklusivitat1');
            $table->dropColumn('exklusivitat2');
            $table->dropColumn('exklusivitat3');
            $table->dropColumn('exklusivitat4');
            $table->dropColumn('sonstiges3');
            $table->dropColumn('schl_banks');
            $table->dropColumn('kommentar');
            $table->dropColumn('ankermieter1');
            $table->dropColumn('ankermieter2');
            $table->dropColumn('ankermieter3');
            $table->dropColumn('Grundbuch');
            $table->dropColumn('Ref_CF_Salzgitter');
            $table->dropColumn('Ref_GuV_Salzgitter');
            $table->dropColumn('AHK_Salzgitter');
            $table->dropColumn('WAULT_of_Erwerb');
            $table->dropColumn('Leerstandsquote');
            $table->dropColumn('gesamt_in_eur');
        });
    }
}
