<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PropertyManagementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_managements', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('property_id')->nullable();
            $table->string('name',100)->nullable();
            $table->float('percent')->nullable();
            $table->tinyInteger('release_status1')->default('0');
            $table->tinyInteger('release_status2')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_managements', function (Blueprint $table) {
            //
        });
    }
}
