<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyEinkaufFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_einkauf_file', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->integer('property_buy_detail_type');
            $table->string('type')->nullable();
            $table->string('file_name')->nullable();
            $table->string('file_basename')->nullable();
            $table->string('file_path')->nullable();
            $table->string('file_href')->nullable();
            $table->string('extension')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_einkauf_file');
    }
}
