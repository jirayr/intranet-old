<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoOposPropertiesDefaultPayers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties_default_payers', function (Blueprint $table) {
            //
            $table->tinyInteger('no_opos_status')->default(0)->after('is_paid');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties_default_payers', function (Blueprint $table) {
            //
        });
    }
}
