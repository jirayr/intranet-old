<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenancy_schedule_item_files', function (Blueprint $table) {
            //
            $table->tinyInteger('file_column_type')->default(0)->comment('0=mietvertrag 1=Grundriss');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenancy_schedule_item_files', function (Blueprint $table) {
            //
        });
    }
}
