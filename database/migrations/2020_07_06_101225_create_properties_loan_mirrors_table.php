<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesLoanMirrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties_loan_mirrors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->integer('user_id');
            $table->double('delta')->nullable(); //Delta
            $table->double('delta2')->nullable(); //Darlehen 31.12.2019 HGB
            $table->double('delta3')->nullable(); //Darlehen 30.06.2020 HGB
            $table->double('zins')->nullable(); //Zins
            $table->double('interest_extend')->nullable(); //Zinsaufwand p.a. ann. per 31.12.2019
            $table->double('loan_service')->nullable(); //Kapitaldienst p.a. ann. per 31.12.2019
            $table->double('loan_service_month')->nullable(); //Kapitaldienst p.m. ann. per 31.12.2019
            $table->double('darlehensbetrag')->nullable(); //Darlehensbetrag
            $table->double('darlehensnummer')->nullable(); //Darlehensnummer
            $table->string('darlehensart')->nullable(); //Darlehensart
            $table->date('laufzeitende')->nullable(); //Laufzeitende
            $table->string('zahlungsrythmus')->nullable(); //Zahlungsrythmus
            $table->double('bereitstellungszinsen')->nullable();//Bereitstellungszinsen
            $table->double('tilgungssatz')->nullable();//Tilgungssatz
            $table->string('zinsart')->nullable(); //Zahlungsrythmus
            $table->double('anfangszins')->nullable();//Anfangszins
            $table->double('aktueller_zins')->nullable();//Aktueller Zins
            $table->string('zinsberechnungsmethode')->nullable(); //Zinsberechnungsmethode
            $table->date('zinsbindungsende')->nullable(); //Zinsbindungsende
            $table->string('euribor')->nullable(); //Euribor
            $table->double('höhe_euribor')->nullable();//Höhe Euribor
            $table->string('behandlung_negativzins')->nullable(); //Behandlung Negativzins
            $table->double('veränderungsgrenze')->nullable();//Veränderungsgrenze
            $table->double('mabgeblicher_euribor')->nullable();//Maßgeblicher Euribor
            $table->string('ermittlungsturnus')->nullable(); //Ermittlungsturnus
            $table->string('ermittlungsstichtag')->nullable(); //Ermittlungsstichtag
            $table->double('aktuelle_restschuld')->nullable();//Aktuelle Restschuld
            $table->double('restschuld_per_1')->nullable();//Restschuld per 30.06.
            $table->double('restschuld_per_2')->nullable();//Restschuld per 31.12.
            $table->double('zinsaufwand')->nullable();//Zinsaufwand p.a.
            $table->double('tilgung')->nullable();//Tilgung p.a.
            $table->double('delta4')->nullable(); //Delta
            $table->double('grundschuld')->nullable(); //Grundschuld
            $table->string('mietabtretung')->nullable(); //Mietabtretung
            $table->string('zusatzsicherheit')->nullable(); //Zusatzsicherheit
            $table->double('höhe_zusatzsicherheit')->nullable(); //Höhe Zusatzsicherheit
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties_loan_mirrors');
    }
}
