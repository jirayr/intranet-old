<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenancyScheduleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenancy_schedule_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->integer('tenancy_schedule_id');
            $table->string('name')->nullable();
            $table->date('rent_begin')->nullable();
            $table->date('rent_end')->nullable();
            $table->string('use')->nullable();
            $table->float('rental_space', 11, 2)->nullable();
            $table->float('actual_net_rent',11,2)->nullable();
            $table->float('vacancy_in_qm',11,2)->nullable();
            $table->float('vacancy_in_eur',11,2)->nullable();
            $table->float('remaining_time', 11, 2)->nullable();
            $table->text('comment')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenancy_schedule_items');
    }
}
