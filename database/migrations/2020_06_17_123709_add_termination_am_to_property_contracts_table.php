<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTerminationAmToPropertyContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_contracts', function (Blueprint $table) {
            $table->date('termination_am')->nullable()->after('termination_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_contracts', function (Blueprint $table) {
            $table->dropColumn('termination_am');
        });
    }
}
