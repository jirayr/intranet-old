<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankFinancingOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_financing_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->nullable()->unsigned();
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');
            $table->integer('property_id')->nullable();
            $table->string('telefonnotiz');
            $table->string('tilgung');
            $table->string('interest_rate');
            $table->string('fk_share_percentage');
            $table->string('fk_share_nominal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_financing_offers');
    }
}
