<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMieterItemIdToProvisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provisions', function (Blueprint $table) {
            $table->integer('mieter_item_id')->nullable()->after('name');
            $table->integer('flache_item_id')->nullable()->after('flache');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('provisions', function (Blueprint $table) {
            $table->dropColumn('mieter_item_id');
            $table->dropColumn('flache_item_id');
        });
    }
}
