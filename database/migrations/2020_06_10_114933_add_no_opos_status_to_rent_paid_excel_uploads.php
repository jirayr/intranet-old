<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoOposStatusToRentPaidExcelUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rent_paid_excel_uploads', function (Blueprint $table) {
            //
            $table->tinyInteger('no_opos_status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rent_paid_excel_uploads', function (Blueprint $table) {
            //
        });
    }
}
