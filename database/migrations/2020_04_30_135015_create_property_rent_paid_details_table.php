<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyRentPaidDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_rent_paid_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_rent_paid_id');
            $table->string('month',30)->nullable();
            $table->string('year',255)->nullable();
            $table->double('amount')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_rent_paid_details');
    }
}
