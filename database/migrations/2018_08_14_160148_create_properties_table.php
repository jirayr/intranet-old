<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
        	$table->increments('id');
            $table->integer('user_id');
            $table->double('net_rent')->nullable(); //C7
			$table->double('net_rent_empty')->nullable(); //C8
			$table->double('maintenance')->nullable(); //C10
			$table->double('operating_costs')->nullable(); //C11
			$table->double('object_management')->nullable(); //C12
			$table->double('tax')->nullable(); //C25
			$table->double('plot_of_land_m2')->nullable(); //L11
			$table->double('plot_of_land')->nullable(); //C37
			$table->integer('construction_year')->nullable(); //L12
			$table->double('land_value')->nullable(); //P31
			$table->double('building')->nullable(); //C36
			$table->double('plot')->nullable(); //K42
			$table->double('real_estate_taxes')->nullable(); //G36
			$table->double('estate_agents')->nullable(); //G37
			$table->double('notary_land_register')->nullable(); //G38
			$table->double('evaluation')->nullable(); //G39
			$table->double('others')->nullable(); //G40
			$table->double('buffer')->nullable(); //G41
			$table->double('rent')->nullable(); //Not used
			$table->double('rent_whg')->nullable(); //N36
			$table->double('vacancy')->nullable(); //Not used
			$table->double('vacancy_whg')->nullable(); //N37
			$table->double('wault')->nullable(); //K40
			$table->double('anchor_tenant')->nullable(); //K41
			$table->double('with_real_ek')->nullable(); //C47
			$table->double('from_bond')->nullable(); //C48
			$table->double('bank_loan')->nullable(); //C49
			$table->double('interest_bank_loan')->nullable(); //G48
			$table->double('eradication_bank')->nullable(); //G49
			$table->double('interest_bond')->nullable(); //G52
			$table->double('maintenance_nk')->nullable(); //C59
			$table->double('operating_costs_nk')->nullable(); //C60
			$table->double('object_management_nk')->nullable(); //C61
			$table->double('depreciation_nk')->nullable(); //C62
			$table->double('property_value')->nullable(); //C66
			$table->date('duration_from')->nullable(); //O38
			$table->double('nk_anchor_tenants')->nullable(); //N40
			$table->double('nk_tenant1')->nullable(); //N41
			$table->double('nk_tenant2')->nullable(); //N42
			$table->double('nk_tenant3')->nullable(); //N43
			$table->double('nk_tenant4')->nullable(); //N44
			$table->double('nk_tenant5')->nullable(); //N45
            $table->double('nk_pm_anchor_tenants')->nullable(); //N40
            $table->double('nk_pm_tenant1')->nullable();
            $table->double('nk_pm_tenant2')->nullable();
            $table->double('nk_pm_tenant3')->nullable();
            $table->double('nk_pm_tenant4')->nullable();
            $table->double('nk_pm_tenant5')->nullable();
            $table->date('mv_begin_anchor_tenants')->nullable();
            $table->date('mv_begin_tenant1')->nullable();
            $table->date('mv_begin_tenant2')->nullable();
            $table->date('mv_begin_tenant3')->nullable();
            $table->date('mv_begin_tenant4')->nullable();
            $table->date('mv_begin_tenant5')->nullable();
			$table->date('mv_anchor_tenants')->nullable();
			$table->date('mv_tenant1')->nullable(); //O41
			$table->date('mv_tenant2')->nullable(); //O42
			$table->date('mv_tenant3')->nullable(); //O43
			$table->date('mv_tenant4')->nullable(); //O44
			$table->date('mv_tenant5')->nullable(); //O45
			$table->string('buy_recommendation',255)->nullable();
			$table->string('location',255)->nullable();
			$table->integer('status')->nullable();
			
			$table->double('total_purchase_price')->nullable(); //D38
			$table->double('maintenance_nk_money')->nullable(); //D59
			$table->string('city_place',255)->nullable(); //K31
            $table->string('address',255)->nullable();
            $table->double('total_commercial_sqm')->nullable();
			$table->double('financing_structure_sum')->nullable(); //C50
			$table->double('building_eur')->nullable(); //D36
			$table->double('plot_of_land_eur')->nullable(); //D37
			$table->double('net_buy_price')->nullable(); //D40
			$table->double('additional_cost')->nullable(); //D41
			$table->double('total_nbpac')->nullable(); //D42
			$table->double('with_real_ek_money')->nullable(); //D47
			$table->double('from_bond_money')->nullable(); //D48
			$table->double('bank_loan_money')->nullable(); //D49
			$table->double('net_rent_pa')->nullable(); //D56
			
			$table->double('operating_costs_nk_money')->nullable(); //D60
			$table->double('object_management_nk_money')->nullable(); //D61
			$table->double('depreciation_nk_money')->nullable(); //D62
			$table->double('net_rent_increase_year1')->nullable(); //E7
			$table->double('net_rent_increase_year2')->nullable(); //F7
			$table->double('net_rent_increase_year3')->nullable(); //G7
			$table->double('net_rent_increase_year4')->nullable(); //H7
			$table->double('net_rent_increase_year5')->nullable(); //I7
			$table->double('maintenance_increase_year1')->nullable(); //E10
			$table->double('maintenance_increase_year2')->nullable(); //F10
			$table->double('maintenance_increase_year3')->nullable(); //G10
			$table->double('maintenance_increase_year4')->nullable(); //H10
			$table->double('maintenance_increase_year5')->nullable(); //I10
			$table->double('operating_cost_increase_year1')->nullable(); //E11
			$table->double('operating_cost_increase_year2')->nullable(); //F11
			$table->double('operating_cost_increase_year3')->nullable(); //G11
			$table->double('operating_cost_increase_year4')->nullable(); //H11
			$table->double('operating_cost_increase_year5')->nullable(); //I11
			$table->double('property_management_increase_year1')->nullable(); //E12
			$table->double('property_management_increase_year2')->nullable(); //F12
			$table->double('property_management_increase_year3')->nullable(); //G12	
			$table->double('property_management_increase_year4')->nullable(); //H12
			$table->double('property_management_increase_year5')->nullable(); //I12
			$table->double('ebitda_year_1')->nullable(); //E14
			$table->double('ebitda_year_2')->nullable(); //F14
			$table->double('ebitda_year_3')->nullable(); //G14
			$table->double('ebitda_year_4')->nullable(); //H14
			$table->double('ebitda_year_5')->nullable(); //I14
			$table->double('depreciation_year_1')->nullable(); //E16
			$table->double('depreciation_year_2')->nullable(); //F16
			$table->double('depreciation_year_3')->nullable(); //G16
			$table->double('depreciation_year_4')->nullable(); //H16
			$table->double('depreciation_year_5')->nullable(); //I16
			$table->double('ebit_year_1')->nullable(); //E18
			$table->double('ebit_year_2')->nullable(); //F18
			$table->double('ebit_year_3')->nullable(); //G18
			$table->double('ebit_year_4')->nullable(); //H18
			$table->double('ebit_year_5')->nullable(); //I18
			$table->double('Interest_bank_loan_year_1')->nullable(); //E20
			$table->double('Interest_bank_loan_year_2')->nullable(); //F20
			$table->double('Interest_bank_loan_year_3')->nullable(); //G20
			$table->double('Interest_bank_loan_year_4')->nullable(); //H20
			$table->double('Interest_bank_loan_year_5')->nullable(); //I20
			$table->double('Interest_bond_year1')->nullable(); //E21
			$table->double('Interest_bond_year2')->nullable(); //F21
			$table->double('Interest_bond_year3')->nullable(); //G21
			$table->double('Interest_bond_year4')->nullable(); //H21
			$table->double('Interest_bond_year5')->nullable(); //I21
			$table->double('EBT_year_1')->nullable(); //E23
			$table->double('EBT_year_2')->nullable(); //F23
			$table->double('EBT_year_3')->nullable(); //G23
			$table->double('EBT_year_4')->nullable(); //H23
			$table->double('EBT_year_5')->nullable(); //I23
			$table->double('Tax_year_1')->nullable(); //E25
			$table->double('Tax_year_2')->nullable(); //F25
			$table->double('Tax_year_3')->nullable(); //G25
			$table->double('Tax_year_4')->nullable(); //H25
			$table->double('Tax_year_5')->nullable(); //I25
			$table->double('EAT_year_1')->nullable(); //E27
			$table->double('EAT_year_2')->nullable(); //F27
			$table->double('EAT_year_3')->nullable(); //G27
			$table->double('EAT_year_4')->nullable(); //H27
			$table->double('EAT_year_5')->nullable(); //I27
			$table->double('Eradication_bank_year1')->nullable(); //E29
			$table->double('Eradication_bank_year2')->nullable(); //F29
			$table->double('Eradication_bank_year3')->nullable(); //G29
			$table->double('Eradication_bank_year4')->nullable(); //H29
			$table->double('Eradication_bank_year5')->nullable(); //I29
			$table->double('Cash_flow_after_taxes_year_1')->nullable(); //E31
			$table->double('Cash_flow_after_taxes_year_2')->nullable(); //F31
			$table->double('Cash_flow_after_taxes_year_3')->nullable(); //H31
			$table->double('Cash_flow_after_taxes_year_4')->nullable(); //G31
			$table->double('Cash_flow_after_taxes_year_5')->nullable(); //I31
			$table->double('real_estate_taxes_in_EUR')->nullable(); //H36
			$table->double('estate_agents_in_EUR')->nullable(); //H37
			$table->double('notary_land_register_in_EUR')->nullable(); //H38
			$table->double('Evaluation_in_EUR')->nullable(); //H39
			$table->double('Others_in_EUR')->nullable(); //H40
			$table->double('Buffer_in_EUR')->nullable(); //H41
			$table->double('value_end_of_year_1')->nullable(); //H47
			$table->double('value_end_of_year_2')->nullable(); //I47
			$table->double('value_end_of_year_3')->nullable(); //J47
			$table->double('value_end_of_year_4')->nullable(); //K47
			$table->double('value_end_of_year_5')->nullable(); //L47
			$table->double('interest_bank_loan_end_of_year_1')->nullable(); //H48
			$table->double('interest_bank_loan_end_of_year_2')->nullable(); //I48
			$table->double('interest_bank_loan_end_of_year_3')->nullable(); //J48
			$table->double('interest_bank_loan_end_of_year_4')->nullable(); //K48
			$table->double('interest_bank_loan_end_of_year_5')->nullable(); //L48
			$table->double('eradication_bank_end_of_year_1')->nullable(); //H49
			$table->double('eradication_bank_end_of_year_2')->nullable(); //I49
			$table->double('eradication_bank_end_of_year_3')->nullable(); //J49
			$table->double('eradication_bank_end_of_year_4')->nullable(); //K49
			$table->double('eradication_bank_end_of_year_5')->nullable(); //L49
			$table->double('sum_end_year_1')->nullable(); //H50
			$table->double('sum_end_year_2')->nullable(); //I50
			$table->double('sum_end_year_3')->nullable(); //J50
			$table->double('sum_end_year_4')->nullable(); //K50
			$table->double('sum_end_year_5')->nullable(); //L50
			$table->double('interest_bond_from_bond')->nullable(); //H52
			$table->double('gross_yield')->nullable(); //G58
			$table->double('Gross_desperate')->nullable(); //G59
			$table->double('EK_Rendite_CF')->nullable(); //G61
			$table->double('EK_Rendite_GuV')->nullable(); //G62
			$table->double('Increase_p_a_year1')->nullable(); //F66
			$table->double('Increase_p_a_year2')->nullable(); //G66
			$table->double('Increase_p_a_year3')->nullable(); //H66
			$table->double('Increase_p_a_year4')->nullable(); //I66
			$table->double('Increase_p_a_year5')->nullable(); //J66
			$table->double('end_of_year_1')->nullable(); //F67
			$table->double('end_of_year_2')->nullable(); //G67
			$table->double('end_of_year_3')->nullable(); //H67
			$table->double('end_of_year_4')->nullable(); //I67
			$table->double('end_of_year_5')->nullable(); //J67
			$table->double('from_sales_proceeds')->nullable(); //J69
			$table->double('from_revaluation')->nullable(); //J70
			$table->double('anchor_tenants')->nullable(); //L7
			$table->double('Rental_area_in_m2')->nullable(); //L10
			$table->double('Rent_euro_m2')->nullable(); //L14
			$table->double('KP_€_m2_p_NF')->nullable(); //L16
			$table->double('plots')->nullable(); //L18
			$table->double('occupancy_rate')->nullable(); //L20
			$table->double('Factor_net')->nullable(); //K25
			$table->date('Lease_term_up_to')->nullable(); //M7
			//$table->float('Land_value')->nullable();
			$table->double('WAULT__of_anchor_tenants')->nullable(); //P40
			$table->double('WAULT__of_tenant1')->nullable(); //P41
			$table->double('WAULT__of_tenant2')->nullable(); //P42
			$table->double('WAULT__of_tenant3')->nullable(); //P43
			$table->double('WAULT__of_tenant4')->nullable(); //P44
			$table->double('WAULT__of_tenant5')->nullable(); //P45
			$table->double('Rental_income_of_anchor_tenants')->nullable(); //Q40
			$table->double('Rental_income_of_tenant1')->nullable(); //Q41
			$table->double('Rental_income_of_tenant2')->nullable(); //Q42
			$table->double('Rental_income_of_tenant3')->nullable(); //Q43
			$table->double('Rental_income_of_tenant4')->nullable(); //Q44
			$table->double('Rental_income_of_tenant5')->nullable(); //Q45
			$table->double('Total_of_WAULT')->nullable(); //P46
			$table->double('Total_of_Rental_income')->nullable(); //Q46
			$table->double('Total_Land_value')->nullable(); //Q47
			$table->double('Value_totally')->nullable(); //Q48
			$table->double('non_recoverable_ancillary_costs_and_maintenance_via_the_WAULT')->nullable(); //Q49
			$table->double('risk_value')->nullable(); //Q50
			$table->double('risk_value_percent')->nullable(); //Q51
			$table->double('WHG_qm_of_WAULT')->nullable(); //L40
			$table->double('WHG_qm_of_anchor_tenant')->nullable(); //L41
			$table->double('WHG_qm_of_plot')->nullable(); //L42
			$table->boolean('is_extra')->default(false);
			$table->boolean('is_in_negotiation')->default(false);
			$table->integer('bank')->nullable();
			$table->text('bank_ids')->nullable();
			$table->text('strengths')->nullable();
			$table->text('weaknesses')->nullable();
			$table->text('opportunities')->nullable();
			$table->text('threats')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
