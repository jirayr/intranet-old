<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToLiquiplanungAoCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('liquiplanung_ao_costs', function (Blueprint $table) {
            $table->tinyInteger('type')->nullable()->default(0)->after('year')->comment('0 = ao Kosten hinzufügen, 1 = ao Einnahmen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('liquiplanung_ao_costs', function (Blueprint $table) {
            Schema::dropIfExists('type');
        });
    }
}
