<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDelta3ToPropertyLoansMirror extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_loans_mirror', function (Blueprint $table) {
            $table->double('delta3')->nullable()->after('delta2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_loans_mirror', function (Blueprint $table) {
            //
        });
    }
}
