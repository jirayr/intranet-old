<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForecastKindsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forecast_kinds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('forecast_id');
            $table->string('kind', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('status', 255)->nullable();
            $table->date('notary')->nullable();
            $table->date('bnl')->nullable();
            $table->bigInteger('total_purchase_price')->nullable();
            $table->bigInteger('fk_or_sale')->nullable();
            $table->bigInteger('reflux')->nullable();
            $table->text('note')->nullable();
            $table->integer('type');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forecast_kinds');
    }
}
