<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtrasFieldsInToProperties1227 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('properties', function (Blueprint $table){
            $table->double('gesamt_in_eur')->nullable(); //H38
            $table->double('Ref_CF_Salzgitter')->nullable(); //J58
            $table->double('Ref_GuV_Salzgitter')->nullable(); //J59
            $table->double('AHK_Salzgitter')->nullable(); //J60
            $table->double('Grundbuch')->nullable(); //G38
            $table->double('WAULT_of_Erwerb')->nullable(); //K40
            $table->double('Leerstandsquote')->nullable(); //K40
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('properties', function($table) {
            $table->dropColumn('gesamt_in_eur');
            $table->dropColumn('Ref_CF_Salzgitter');
            $table->dropColumn('Ref_GuV_Salzgitter');
            $table->dropColumn('AHK_Salzgitter');
            $table->dropColumn('Grundbuch');
            $table->dropColumn('WAULT_of_Erwerb');
            $table->dropColumn('Leerstandsquote');
        });
    }
}
