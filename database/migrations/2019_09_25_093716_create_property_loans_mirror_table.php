<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyLoansMirrorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_loans_mirror', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->string('borrower')->nullable(); //Darlehensnehmer
            $table->string('lender')->nullable(); //darlehensgeber
            $table->double('original_loan')->nullable(); //ursprungsdarlehen
            $table->date('payout_date')->nullable();  //auszahlungsdatum
            $table->double('early_interest')->nullable(); //anfangszins
            $table->string('calculation_method')->nullable(); //berechnungsmethode
            $table->string('variable_interest')->nullable(); //variabler_zins
            $table->string('reference_interest')->nullable(); //referenzzinssatz
            $table->string('check_the_refinance_1')->nullable();//Überprüfung des RefZinses	
            $table->string('check_the_refinance_3')->nullable();//Überprüfung des RefZinses	
            $table->double('change_limit')->nullable(); //veränderungsgrenze
            $table->string('treatment_at_negative')->nullable();//Behandlung bei Negativzins
            $table->string('loan')->nullable(); //Darlehensart
            $table->string('running_time')->nullable();//Laufzeit
            $table->string('running_time_type')->nullable(); //Laufzeit
            $table->double('standby_interest')->nullable(); //Bereitstellungszins
            $table->date('standby_interest_from')->nullable();  //Bereitstellungszins ab	
            $table->double('management_fee')->nullable();
            $table->double('loan_costs')->nullable();//Darlehenskosten	
            $table->double('registry_fees')->nullable();//Grundbuchkosten
            $table->double('exit_fee')->nullable();//Exitfee
            $table->double('mortgage')->nullable();//Grundschuld
            $table->double('mortgage_in_bond')->nullable();//Grundschuld
            $table->string('mortgage_guarantor')->nullable();
            $table->double('mortgage_extent_of_guarantee')->nullable();
            $table->string('is_shareholder_loan')->nullable();
            $table->string('shareholder_loan')->nullable();//Gesellschafterdarlehensbelassung
            $table->string('guarantor')->nullable(); //Drittgarantie
            $table->double('extent_of_guarantee')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_loans_mirror');
    }
}
