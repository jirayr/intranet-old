<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTenantPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tenant_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenancy_schedule_item_id')->nullable();
            $table->integer('year')->nullable();
            $table->date('bka_date')->nullable();
            $table->float('amount')->nullable();
            $table->date('payment_date')->nullable();
            $table->string('file_name')->nullable();
            $table->string('file_basename')->nullable();
            $table->string('file_dirname')->nullable();
            $table->string('file_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
