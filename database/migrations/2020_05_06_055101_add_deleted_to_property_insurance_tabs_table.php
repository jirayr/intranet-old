<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedToPropertyInsuranceTabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_insurance_tabs', function (Blueprint $table) {
            $table->tinyInteger('deleted')->default(0)->after('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_insurance_tabs', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });
    }
}
