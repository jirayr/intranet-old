<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeletedToPropertyInsuranceTabDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_insurance_tab_details', function (Blueprint $table) {
            $table->tinyInteger('deleted')->default(0)->after('falk_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_insurance_tab_details', function (Blueprint $table) {
            $table->dropColumn('deleted');
        });
    }
}
