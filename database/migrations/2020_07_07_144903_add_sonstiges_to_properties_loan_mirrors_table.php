<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSonstigesToPropertiesLoanMirrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties_loan_mirrors', function (Blueprint $table) {
            $table->string('sonstiges')->nullable()->after('verkehrswert');
            $table->date('ab')->nullable()->after('sonstiges');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties_loan_mirrors', function (Blueprint $table) {
            $table->dropColumn('sonstiges');
            $table->dropColumn('ab');
        });
    }
}
