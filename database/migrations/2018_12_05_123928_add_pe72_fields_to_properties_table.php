<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPe72FieldsToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->double('rent_retail')->nullable(); //K36
            $table->double('rent_office')->nullable(); //L36
            $table->double('rent_industry')->nullable(); //M36
            $table->double('vacancy_retail')->nullable(); //K37
            $table->double('vacancy_office')->nullable(); //L37
            $table->double('vacancy_industry')->nullable(); //M37


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('rent_retail');
            $table->dropColumn('rent_office');
            $table->dropColumn('rent_industry');
            $table->dropColumn('vacancy_retail');
            $table->dropColumn('vacancy_office');
            $table->dropColumn('vacancy_industry');
        });
    }
}
