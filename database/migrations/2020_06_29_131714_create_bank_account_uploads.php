<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->string('account_designation');
            $table->string('iban');
            $table->string('account_owner');
            $table->string('currency');
            $table->string('account_category');
            $table->string('book_balance');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account_uploads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->string('account_designation');
            $table->string('iban');
            $table->string('account_owner');
            $table->string('currency');
            $table->string('account_category');
            $table->string('book_balance');
            $table->timestamps();
        });
    }
}
