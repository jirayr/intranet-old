<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->increments('id');
            $table->double('nk_pa')->nullable(); //nk_* in table "properties"
            $table->double('nk_pm')->nullable(); //nk_pm_* in table "properties"
            $table->date('mv_begin')->nullable(); //mv_begin_* in table "properties"
            $table->date('mv_end')->nullable(); //mv_* in table "properties"
            $table->double('wault')->nullable(); //WAULT__* in table "properties"
            $table->double('rental_income')->nullable(); // Rental_income_* in table "properties"
            $table->integer('property_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
