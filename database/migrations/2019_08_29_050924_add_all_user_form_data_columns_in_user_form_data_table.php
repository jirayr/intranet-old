<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllUserFormDataColumnsInUserFormDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_form_data', function (Blueprint $table) {
			$table->string('property_id')->nullable()->after('user_data');
			$table->string('doc_key')->nullable()->after('property_id');
			$table->string('institut')->nullable()->after('doc_key');
			$table->text('anschrift')->nullable()->after('institut');
			$table->string('ansprechpartner')->nullable()->after('anschrift');
			$table->string('telefon')->nullable()->after('ansprechpartner');
			$table->string('email')->nullable()->after('telefon');
			$table->string('finanzierungstyp')->nullable()->after('email');
			$table->bigInteger('kreditbetrag')->nullable()->after('finanzierungstyp');
			$table->integer('variabler_zinssatz')->nullable()->after('kreditbetrag');
			$table->integer('tilgung')->nullable()->after('variabler_zinssatz');
			$table->integer('laufzeit')->nullable()->after('tilgung');
			$table->text('anmerkungen')->nullable()->after('laufzeit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_form_data', function (Blueprint $table) {
			$table->dropColumn('property_id');
			$table->dropColumn('doc_key');
			$table->dropColumn('institut');
			$table->dropColumn('anschrift');
			$table->dropColumn('ansprechpartner');
			$table->dropColumn('telefon');
			$table->dropColumn('email');
			$table->dropColumn('finanzierungstyp');
			$table->dropColumn('kreditbetrag');
			$table->dropColumn('variabler_zinssatz');
			$table->dropColumn('tilgung');
			$table->dropColumn('laufzeit');
			$table->dropColumn('anmerkungen');
        });
    }
}
