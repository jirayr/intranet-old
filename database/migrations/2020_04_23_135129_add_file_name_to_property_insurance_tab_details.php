<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileNameToPropertyInsuranceTabDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_insurance_tab_details', function (Blueprint $table) {
            $table->string('file_name',255)->nullable();
            $table->string('file_basename',255)->nullable();
            $table->string('file_dirname',255)->nullable();
            $table->string('file_type',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_insurance_tab_details', function (Blueprint $table) {
            $table->dropColumn('file_name');
            $table->dropColumn('file_basename');
            $table->dropColumn('file_dirname');
            $table->dropColumn('file_type');
        });
    }
}
