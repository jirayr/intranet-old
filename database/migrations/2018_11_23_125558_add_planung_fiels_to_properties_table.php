<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlanungFielsToPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('properties', function (Blueprint $table) {
            $table->text('planung_fields')->nullable();
//            $table->string('nutzflache_flur',30)->nullable();
//            $table->string('zimmerwohnung_1',30)->nullable();
//            $table->string('zimmerwohnung_2',30)->nullable();
//            $table->string('zimmerwohnung_3',30)->nullable();
//            $table->string('zimmerwohnung_4',30)->nullable();
//            $table->string('zimmerwohnung_5',30)->nullable();
//            $table->string('zimmerwohnung_6',30)->nullable();
//            $table->string('zimmerwohnung_7',30)->nullable();
//            $table->string('zimmerwohnung_8',30)->nullable();
//            $table->string('zimmerwohnung_9',30)->nullable();
//            $table->string('zimmerwohnung_10',30)->nullable();
//            $table->string('zimmerwohnung_11',30)->nullable();
//            $table->string('zimmerwohnung_12',30)->nullable();
//            $table->string('zimmerwohnung_13',30)->nullable();
//            $table->string('insgesamt_pm_1',30)->nullable();
//            $table->string('insgesamt_pm_2',30)->nullable();
//            $table->string('insgesamt_pm_3',30)->nullable();
//            $table->string('pro_wohnung_1',30)->nullable();
//            $table->string('pro_wohnung_2',30)->nullable();
//            $table->string('pro_wohnung_3',30)->nullable();
//            $table->string('insgesamt_pm_4',30)->nullable();
//            $table->string('insgesamt_pm_5',30)->nullable();
//            $table->string('verkauf_zu_faktor_1',30)->nullable();
//            $table->string('verkauf_zu_faktor_2',30)->nullable();
//            $table->string('verkauf_zu_faktor_3',30)->nullable();
//            $table->string('verkauf_zu_faktor_4',30)->nullable();
//            $table->string('verkauf_zu_faktor_5',30)->nullable();
//            $table->string('moglicher_verkaufspreis_1',30)->nullable();
//            $table->string('moglicher_verkaufspreis_2',30)->nullable();
//            $table->string('moglicher_verkaufspreis_3',30)->nullable();
//            $table->string('moglicher_verkaufspreis_4',30)->nullable();
//            $table->string('moglicher_verkaufspreis_5',30)->nullable();
//            $table->string('einkaufspreis_buchwert_1',30)->nullable();
//            $table->string('einkaufspreis_buchwert_2',30)->nullable();
//            $table->string('einkaufspreis_buchwert_3',30)->nullable();
//            $table->string('einkaufspreis_buchwert_4',30)->nullable();
//            $table->string('einkaufspreis_buchwert_5',30)->nullable();
//            $table->string('invest_inkl_ek_ruckzahlung_1',30)->nullable();
//            $table->string('invest_inkl_ek_ruckzahlung_2',30)->nullable();
//            $table->string('invest_inkl_ek_ruckzahlung_3',30)->nullable();
//            $table->string('invest_inkl_ek_ruckzahlung_4',30)->nullable();
//            $table->string('invest_inkl_ek_ruckzahlung_5',30)->nullable();
//            $table->string('delta_1',30)->nullable();
//            $table->string('delta_2',30)->nullable();
//            $table->string('delta_3',30)->nullable();
//            $table->string('delta_4',30)->nullable();
//            $table->string('delta_5',30)->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('planung_fields');
//            $table->dropColumn('planung_total_area');
//            $table->dropColumn('nutzflache_flur');
//            $table->dropColumn('zimmerwohnung_1');
//            $table->dropColumn('zimmerwohnung_2');
//            $table->dropColumn('zimmerwohnung_3');
//            $table->dropColumn('zimmerwohnung_4');
//            $table->dropColumn('zimmerwohnung_5');
//            $table->dropColumn('zimmerwohnung_6');
//            $table->dropColumn('zimmerwohnung_7');
//            $table->dropColumn('zimmerwohnung_8');
//            $table->dropColumn('zimmerwohnung_9');
//            $table->dropColumn('zimmerwohnung_10');
//            $table->dropColumn('zimmerwohnung_11');
//            $table->dropColumn('zimmerwohnung_12');
//            $table->dropColumn('zimmerwohnung_13');
//            $table->dropColumn('insgesamt_pm_1');
//            $table->dropColumn('insgesamt_pm_2');
//            $table->dropColumn('insgesamt_pm_3');
//            $table->dropColumn('pro_wohnung_1');
//            $table->dropColumn('pro_wohnung_2');
//            $table->dropColumn('pro_wohnung_3');
//            $table->dropColumn('insgesamt_pm_4');
//            $table->dropColumn('insgesamt_pm_5');
//            $table->dropColumn('verkauf_zu_faktor_1');
//            $table->dropColumn('verkauf_zu_faktor_2');
//            $table->dropColumn('verkauf_zu_faktor_3');
//            $table->dropColumn('verkauf_zu_faktor_4');
//            $table->dropColumn('verkauf_zu_faktor_5');
//            $table->dropColumn('moglicher_verkaufspreis_1');
//            $table->dropColumn('moglicher_verkaufspreis_2');
//            $table->dropColumn('moglicher_verkaufspreis_3');
//            $table->dropColumn('moglicher_verkaufspreis_4');
//            $table->dropColumn('moglicher_verkaufspreis_5');
//            $table->dropColumn('einkaufspreis_buchwert_1');
//            $table->dropColumn('einkaufspreis_buchwert_2');
//            $table->dropColumn('einkaufspreis_buchwert_3');
//            $table->dropColumn('einkaufspreis_buchwert_4');
//            $table->dropColumn('einkaufspreis_buchwert_5');
//            $table->dropColumn('invest_inkl_ek_ruckzahlung_1');
//            $table->dropColumn('invest_inkl_ek_ruckzahlung_2');
//            $table->dropColumn('invest_inkl_ek_ruckzahlung_3');
//            $table->dropColumn('invest_inkl_ek_ruckzahlung_4');
//            $table->dropColumn('invest_inkl_ek_ruckzahlung_5');
//            $table->dropColumn('delta_1');
//            $table->dropColumn('delta_2');
//            $table->dropColumn('delta_3');
//            $table->dropColumn('delta_4');
//            $table->dropColumn('delta_5');

        });
    }
}
