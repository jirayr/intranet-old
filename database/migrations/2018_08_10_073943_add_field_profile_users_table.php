<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldProfileUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
			$table->string('image',255)->nullable();
			$table->string('first_name',50)->nullable();
			$table->string('last_name',50)->nullable();
			$table->dateTime('birthday')->nullable();
			$table->string('phone',20)->nullable();
			$table->string('address',255)->nullable();
			$table->integer('gender')->nullable();
			$table->string('website',255)->nullable();
			$table->string('postal_code',100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
			Schema::dropIfExists('users');
        });
    }
}
