<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyRentPaidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_rent_paids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('nr',30)->nullable();
            $table->string('mieter',255)->nullable();
            $table->double('soll')->default(0)->nullable();
            $table->double('ist')->default(0)->nullable();
            $table->double('diff')->default(0)->nullable();
            $table->string('info',255)->nullable();
            $table->tinyInteger('mkz_aktuell')->default(0)->nullable();
            $table->tinyInteger('vorschlag_mkz')->default(0)->nullable();
            $table->string('bemerkung_ok',255)->nullable();
            $table->string('bemerkung_fcr',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_rent_paids');
    }
}
