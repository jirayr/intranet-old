<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedDateToVacantUploadingStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vacant_uploading_statuses', function (Blueprint $table) {
            $table->dateTime('updated_date')->nullable()->after('comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacant_uploading_statuses', function (Blueprint $table) {
            $table->dropColumn('updated_date');
        });
    }
}
