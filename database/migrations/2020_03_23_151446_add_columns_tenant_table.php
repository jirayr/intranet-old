<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenancy_schedule_items', function (Blueprint $table) {
            $table->tinyInteger('tenant_closed')->default(0);
            $table->integer('rent_payment')->nullable();
            $table->text('comment3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenancy_schedule_items', function (Blueprint $table) {
            //
        });
    }
}
