<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('description')->nullable(); 
			$table->date('start');
			$table->date('end')->nullable(); 
			$table->integer('user_id')->nullable(); 
			$table->integer('property_id')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_calendars');
    }
}
