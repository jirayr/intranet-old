<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmailMappings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('email_id');
            $table->integer('account_id');
            $table->integer('email_type_id');
            $table->boolean('is_read');
            $table->boolean('is_starred');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_mappings');

    }
}
