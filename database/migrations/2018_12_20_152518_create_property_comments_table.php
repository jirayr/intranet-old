<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('comment')->nullable();
			$table->text('status')->nullable();
			$table->unsignedInteger('user_id')->nullable();
			$table->unsignedInteger('property_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_comments');
    }
}
