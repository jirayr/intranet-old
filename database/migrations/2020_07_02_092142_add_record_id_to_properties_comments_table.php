<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecordIdToPropertiesCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties_comments', function (Blueprint $table) {
            $table->integer('record_id')->nullable()->after('comment');
            $table->string('type')->nullable()->after('record_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties_comments', function (Blueprint $table) {
            $table->dropColumn('record_id');
            $table->dropColumn('type');
        });
    }
}
