<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnkToPropertiesTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties_tenants', function (Blueprint $table) {
            $table->tinyInteger('ank_1')->default(0);
            $table->tinyInteger('ank_2')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties_tenants', function (Blueprint $table) {
            $table->dropColumn('ank_1');
            $table->dropColumn('ank_2');
        });
    }
}
