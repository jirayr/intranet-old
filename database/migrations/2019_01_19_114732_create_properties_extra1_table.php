<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesExtra1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties_extra1', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('propertyId');
			$table->string('tenant',255)->nullable();
            $table->double('net_rent_p_a')->nullable(); //C10
			$table->string('mv_end',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties_extra1');
    }
}
