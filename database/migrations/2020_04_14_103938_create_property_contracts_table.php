<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('invoice',255)->nullable();
            $table->string('file_basename',255)->nullable();
            $table->string('file_dirname',255)->nullable();
            $table->string('file_type',255)->nullable();
            $table->text('comment')->nullable();
            $table->text('comment2')->nullable();
            $table->string('email',100)->nullable();
            $table->boolean('is_paid')->default(0)->nullable();
            $table->double('amount')->default(0)->nullable();
            $table->date('date')->nullable();
            $table->date('termination_date')->nullable();
            $table->tinyInteger('not_release_status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_contracts');
    }
}
