<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreFieldsInPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->text('asset_manager1')->nullable();
            $table->text('ubername_des_objekts')->nullable();
            $table->text('asset_manager2')->nullable();
            $table->text('ubername_des_objekts2')->nullable();
            $table->text('gewerbe')->nullable();
            $table->text('buropraxen')->nullable();
            $table->text('wohnungen')->nullable();
            $table->text('lager')->nullable();
            $table->text('stellplatze')->nullable();
            $table->text('sonstiges')->nullable();
            $table->text('vermietet')->nullable();
            $table->text('vermietet2')->nullable();
            $table->text('leerstand')->nullable();
            $table->text('leerstand2')->nullable();
            $table->text('mietende')->nullable();
            $table->text('mietende2')->nullable();
            $table->text('mietende3')->nullable();
            $table->text('mietende4')->nullable();
            $table->text('mietanpassung')->nullable();
            $table->text('mietanpassung2')->nullable();
            $table->text('mietanpassung3')->nullable();
            $table->text('mietanpassung4')->nullable();
            $table->text('ansprechp')->nullable();
            $table->text('ansprechp2')->nullable();
            $table->text('ansprechp3')->nullable();
            $table->text('wohnungen2')->nullable();
            $table->text('optionen')->nullable();
            $table->text('optionen2')->nullable();
            $table->text('optionen3')->nullable();
            $table->text('lager2')->nullable();
            $table->text('stellplatze2')->nullable();
            $table->text('sonstiges2')->nullable();
            $table->text('vermietet3')->nullable();
            $table->text('vermietet4')->nullable();
            $table->text('leerstand3')->nullable();
            $table->text('leerstand4')->nullable();
            $table->text('flache')->nullable();
            $table->text('flache2')->nullable();
            $table->text('flache3')->nullable();
            $table->text('potentiellemiete')->nullable();
            $table->text('vermietungstand')->nullable();
            $table->text('miete')->nullable();
            $table->text('miete2')->nullable();
            $table->text('miete3')->nullable();
            $table->text('miete4')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('asset_manager1');
            $table->dropColumn('ubername_des_objekts');
            $table->dropColumn('asset_manager2');
            $table->dropColumn('ubername_des_objekts2');
            $table->dropColumn('gewerbe');
            $table->dropColumn('buropraxen');
            $table->dropColumn('wohnungen');
            $table->dropColumn('lager');
            $table->dropColumn('stellplatze');
            $table->dropColumn('sonstiges');
            $table->dropColumn('vermietet');
            $table->dropColumn('vermietet2');
            $table->dropColumn('leerstand');
            $table->dropColumn('leerstand2');
            $table->dropColumn('mietende');
            $table->dropColumn('mietende2');
            $table->dropColumn('mietende3');
            $table->dropColumn('mietende4');
            $table->dropColumn('mietanpassung');
            $table->dropColumn('mietanpassung2');
            $table->dropColumn('mietanpassung3');
            $table->dropColumn('mietanpassung4');
            $table->dropColumn('ansprechp');
            $table->dropColumn('ansprechp2');
            $table->dropColumn('ansprechp3');
            $table->dropColumn('wohnungen2');
            $table->dropColumn('optionen');
            $table->dropColumn('optionen2');
            $table->dropColumn('optionen3');
            $table->dropColumn('lager2');
            $table->dropColumn('stellplatze2');
            $table->dropColumn('sonstiges2');
            $table->dropColumn('vermietet3');
            $table->dropColumn('vermietet4');
            $table->dropColumn('leerstand3');
            $table->dropColumn('leerstand4');
            $table->dropColumn('flache');
            $table->dropColumn('flache2');
            $table->dropColumn('flache3');
            $table->dropColumn('potentiellemiete');
            $table->dropColumn('vermietungstand');
            $table->dropColumn('miete');
            $table->dropColumn('miete2');
            $table->dropColumn('miete3');
            $table->dropColumn('miete4');
        });
    }
}
