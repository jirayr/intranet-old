<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnderungWirksamPerToPropertiesLoanMirrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties_loan_mirrors', function (Blueprint $table) {
            $table->date('anderung_wirksam_per')->nullable()->after('ermittlungsstichtag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties_loan_mirrors', function (Blueprint $table) {
            Schema::dropIfExists('anderung_wirksam_per');
        });
    }
}
