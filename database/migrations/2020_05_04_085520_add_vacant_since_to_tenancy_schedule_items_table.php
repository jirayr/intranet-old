<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVacantSinceToTenancyScheduleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenancy_schedule_items', function (Blueprint $table) {
            $table->date('vacant_since')->nullable();
            $table->date('is_upload_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenancy_schedule_items', function (Blueprint $table) {
            $table->dropColumn('vacant_since');
            $table->dropColumn('is_upload_date');
        });
    }
}
