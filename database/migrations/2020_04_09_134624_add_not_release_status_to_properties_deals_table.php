<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotReleaseStatusToPropertiesDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties_deals', function (Blueprint $table) {
            $table->tinyInteger('not_release_status')->default(0)->nullable()->after('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties_deals', function (Blueprint $table) {
            $table->dropColumn('not_release_status');
        });
    }
}
