<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGdriveUploadFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gdrive_upload_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('parent_type')->nullable();

            $table->string('file_name')->nullable();
            $table->string('file_basename')->nullable();
            $table->string('file_path')->nullable();
            $table->string('file_href')->nullable();
            $table->string('extension')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gdrive_upload_files');
    }
}
