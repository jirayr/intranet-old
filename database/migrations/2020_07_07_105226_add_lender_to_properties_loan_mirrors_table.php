<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLenderToPropertiesLoanMirrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties_loan_mirrors', function (Blueprint $table) {
            $table->string('lender')->nullable()->after('höhe_zusatzsicherheit');
            $table->string('borrower')->nullable()->after('lender');
            $table->date('payout_date')->nullable()->after('borrower');
            $table->double('verkehrswert')->nullable()->after('payout_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties_loan_mirrors', function (Blueprint $table) {
            $table->dropColumn('lender');
            $table->dropColumn('borrower');
            $table->dropColumn('payout_date');
            $table->dropColumn('verkehrswert');
        });
    }
}
