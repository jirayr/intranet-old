<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyInsuranceTabDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_insurance_tab_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_insurance_tab_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('name',255)->nullable();
            $table->double('amount')->default(0)->nullable();
            $table->text('comment')->nullable();
            $table->tinyInteger('asset_manager_status')->default(0)->nullable();
            $table->tinyInteger('falk_status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_insurance_tab_details');
    }
}
