<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('picture');
            $table->string('name');
            $table->integer('user_id')->unsigned();
            $table->double('with_real_ek')->nullable(); //C47
            $table->double('from_bond')->nullable(); //C48
            $table->double('bank_loan')->nullable(); //C49
            $table->double('interest_bank_loan')->nullable(); //G48
            $table->double('eradication_bank')->nullable(); //G49
            $table->double('interest_bond')->nullable(); //G52
            $table->timestamps();
        });

        Schema::table('banks', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
