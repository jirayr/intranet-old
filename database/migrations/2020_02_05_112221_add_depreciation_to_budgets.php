<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepreciationToBudgets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budgets', function (Blueprint $table) {
            $table->double('bank_interest_year_1')->nullable();
            $table->double('bank_interest_year_2')->nullable();
            $table->double('bank_interest_year_3')->nullable();
            $table->double('bank_interest_year_4')->nullable();
            $table->double('bank_interest_year_5')->nullable();

            $table->double('interest_rate_bond_year_1')->nullable();
            $table->double('interest_rate_bond_year_2')->nullable();
            $table->double('interest_rate_bond_year_3')->nullable();
            $table->double('interest_rate_bond_year_4')->nullable();
            $table->double('interest_rate_bond_year_5')->nullable();

            $table->double('ebt_year_1')->nullable();
            $table->double('ebt_year_2')->nullable();
            $table->double('ebt_year_3')->nullable();
            $table->double('ebt_year_4')->nullable();
            $table->double('ebt_year_5')->nullable();

            $table->double('tax_year_1')->nullable();
            $table->double('tax_year_2')->nullable();
            $table->double('tax_year_3')->nullable();
            $table->double('tax_year_4')->nullable();
            $table->double('tax_year_5')->nullable();

            $table->double('eat_year_1')->nullable();
            $table->double('eat_year_2')->nullable();
            $table->double('eat_year_3')->nullable();
            $table->double('eat_year_4')->nullable();
            $table->double('eat_year_5')->nullable();


            $table->double('redemption_bank_year_1')->nullable();
            $table->double('redemption_bank_year_2')->nullable();
            $table->double('redemption_bank_year_3')->nullable();
            $table->double('redemption_bank_year_4')->nullable();
            $table->double('redemption_bank_year_5')->nullable();

            $table->double('cashflow_year_1')->nullable();
            $table->double('cashflow_year_2')->nullable();
            $table->double('cashflow_year_3')->nullable();
            $table->double('cashflow_year_4')->nullable();
            $table->double('cashflow_year_5')->nullable();

            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('budgets', function (Blueprint $table) {
            $table->dropColumn(['bank_interest_year_1'
            ,'bank_interest_year_2'
            ,'bank_interest_year_3'
            ,'bank_interest_year_4'
            ,'bank_interest_year_5'

            ,'interest_rate_bond_year_1'
            ,'interest_rate_bond_year_2'
            ,'interest_rate_bond_year_3'
            ,'interest_rate_bond_year_4'
            ,'interest_rate_bond_year_5'

            ,'ebt_year_1'
            ,'ebt_year_2'
            ,'ebt_year_3'
            ,'ebt_year_4'
            ,'ebt_year_5'

            ,'tax_year_1'
            ,'tax_year_2'
            ,'tax_year_3'
            ,'tax_year_4'
            ,'tax_year_5'

            ,'eat_year_1'
            ,'eat_year_2'
            ,'eat_year_3'
            ,'eat_year_4'
            ,'eat_year_5'


            ,'redemption_bank_year_1'
            ,'redemption_bank_year_2'
            ,'redemption_bank_year_3'
            ,'redemption_bank_year_4'
            ,'redemption_bank_year_5'

            ,'cashflow_year_1'
            ,'cashflow_year_2'
            ,'cashflow_year_3'
            ,'cashflow_year_4'
            ,'cashflow_year_5']);
        });
    }
}
