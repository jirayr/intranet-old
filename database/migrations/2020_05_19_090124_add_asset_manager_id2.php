<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssetManagerId2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenancy_schedule_items', function (Blueprint $table) {
            //
            $table->integer('asset_manager_id2')->after('asset_manager_id')->nullable('asset_manager_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenancy_schedule_items', function (Blueprint $table) {
            //
        });
    }
}
