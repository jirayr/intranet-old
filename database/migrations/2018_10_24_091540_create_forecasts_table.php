<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForecastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forecasts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('missing_info')->nullable();
            $table->text('todo')->nullable();
            $table->string('name', 255)->nullable();
            $table->float('credit_fcr', 13, 2)->nullable();
            $table->float('credit_spv', 13, 2)->nullable();
            $table->float('possible_transfer', 13, 2)->nullable();
            $table->float('buffer', 13, 2)->nullable();
            $table->float('available_1', 13, 2)->nullable();
            $table->float('available_2', 13, 2)->nullable();
            $table->float('available_3', 13, 2)->nullable();
            $table->float('available_4', 13, 2)->nullable();
            $table->float('available_5', 13, 2)->nullable();
            $table->float('available_6', 13, 2)->nullable();
            $table->float('available_7', 13, 2)->nullable();
            $table->float('available_8', 13, 2)->nullable();
            $table->float('available_9', 13, 2)->nullable();
            $table->float('available_10', 13, 2)->nullable();
            $table->float('available_11', 13, 2)->nullable();
            $table->float('available_12', 13, 2)->nullable();
            $table->date('date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forecasts');
    }
}
