<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZinszahlungsbeginnToPropertiesLoanMirrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties_loan_mirrors', function (Blueprint $table) {
            $table->date('zinszahlungsbeginn')->nullable()->after('zahlungsrythmus');
            $table->date('tilgungsbeginn')->nullable()->after('zinszahlungsbeginn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties_loan_mirrors', function (Blueprint $table) {
            Schema::dropIfExists('zinszahlungsbeginn');
            Schema::dropIfExists('tilgungsbeginn');
        });
    }
}
