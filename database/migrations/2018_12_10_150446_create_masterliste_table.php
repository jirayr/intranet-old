<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterlisteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterliste', function (Blueprint $table) {
            $table->increments('id');
			$table->string('object', 255)->nullable();
			$table->integer('asset_manager')->nullable();
			$table->float('flat_in_qm', 13, 2)->nullable();
			$table->float('vacancy', 13, 2)->nullable();
			$table->float('vacancy_structurally', 13, 2)->nullable();
			$table->float('technical_ff', 13, 2)->nullable();
			$table->float('rented_area', 13, 2)->nullable();
			$table->float('rent_net_pm', 13, 2)->nullable();
			$table->float('avg_rental_fee', 13, 2)->nullable();
			$table->float('potential_pm', 13, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masterliste');
    }
}
