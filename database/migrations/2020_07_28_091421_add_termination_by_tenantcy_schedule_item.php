<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTerminationByTenantcyScheduleItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenancy_schedule_items', function (Blueprint $table) {
            //
            $table->date('termination_by')->nullable()->after('rent_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenancy_schedule_items', function (Blueprint $table) {
            //
        });
    }
}
