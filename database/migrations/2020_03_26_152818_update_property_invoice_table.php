<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePropertyInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_invoices', function (Blueprint $table) {
            $table->string('file_basename')->nullable()->after('invoice');
            $table->string('file_dirname')->nullable()->after('file_basename');
            $table->string('file_type')->nullable()->after('file_dirname');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_invoices', function (Blueprint $table) {
            $table->dropColumn('file_basename');
            $table->dropColumn('file_dirname');
            $table->dropColumn('file_type');
        });
    }
}
