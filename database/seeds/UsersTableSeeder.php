<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'role' => config('auth.role.admin'),
            'password' => bcrypt('123456'),
            'image' => 'no-avatar.png'
        ];
        DB::table('users')->insert($admin);
    }
}
