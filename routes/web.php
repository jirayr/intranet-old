<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
///Route::get('/teststts', function () {
// 2   return 'hheheheheh';
//});

//if (env('APP_ENV') === 'production') {
//   URL::forceSchema('https');
//}

Route::get('test_file', 'HomeController@exposeTabPdfTest');

use App\Imports\BankDataImport;
use App\Models\StatusLoi;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

Route::group(array('middleware' => ['addlog']), function()
{
    Route::get('verkaufsportal', function (){

        return view('verkaufsportal');
    });

    Route::get('vermietungsportal', function (){
        return view('vermietungsportal');
    });

    Route::get('tawk', function (){
        return view('tawk');
    });

    Route::get('rashid', 'HomeController@rashid');
    Route::get('phpinfo', function (){

        phpinfo();
    });

    Route::group(array('middleware' => ['auth', 'admin']), function(){
        Route::get('getPropertyInsurance', ['as'=>'getPropertyInsurance', 'uses' => 'HomeController@getPropertyInsurance']);
        Route::get('getInsurancePropertyList', ['as'=>'getInsurancePropertyList', 'uses' => 'HomeController@getInsurancePropertyList']);
        Route::get('get_hausmaxx_data', ['as'=>'get_hausmaxx_data', 'uses' => 'HomeController@get_hausmaxx_data']);
    });


    Route::post('email_template_users', ['as'=>'email_template_users', 'uses' => 'HomeController@email_template_users']);

    Route::post('schl-file', ['as'=>'schl-file', 'uses' => 'PropertiesController@uploadSchlFile']);





    Route::get('/office', 'HomeController@trello');

    Route::get('comment-detail', 'HomeController@commentDetail')->name('comment-detail');

    Route::get('getusers', 'HomeController@getusers')->name('getusers');



    Route::get('/clear_cache', function() {
        $exitCode = Artisan::call('view:clear');
        return 'DONE'; //Return anything
    });

    Auth::routes();


/////////// Webmail Routes ////////////
    Route::get('/webmail','Webmail@index');
    Route::get('/webmail-read/{id}','Webmail@read');
    Route::post('/webmail-reply','Webmail@reply');

    Route::post('/saveContactdata','AdsController@saveContactdata');


    Route::get('/webmail-delete/{id}','Webmail@delete');

    Route::get('/webmail-delete-sent/{id}','Webmail@delete_sent');
    Route::get('/webmail-delete-spam/{id}','Webmail@delete_spam');

    Route::get('/webmail-trash','Webmail@trash');
    Route::get('/webmail-spam','Webmail@spam');
    Route::get('/webmail-sent','Webmail@sent');

    Route::get('/webmail-trash_read/{id}','Webmail@trash_read');
    Route::get('/webmail-sent_read/{id}','Webmail@sent_read');
    Route::get('/webmail-spam_read/{id}','Webmail@spam_read');

    Route::get('/webmail-permanent-delete/{id}','Webmail@permanent_delete');

    Route::get('/webmail-empty-trash','Webmail@empty_trash');

    Route::get('/webmail-login',function(){
        return view('webmail.login');
    });
    Route::post('/webmail-login-post','Webmail@webmail_login_post');

////////////////// End Webmail Routes /////////////////////


    Route::get('/email_template','HomeController@email_template')->name('email_template');

    Route::post('/email_template_save','HomeController@email_template_save')->name('email_template_save');
    Route::get('/send_email_template','HomeController@send_email_template')->name('send_email_template');
    Route::post('/save_status_loi','HomeController@save_status_loi')->name('save_status_loi');
    Route::post('/update_versicherung','PropertiesController@update_versicherung');
    Route::post('/update_versicherung_approved_status','PropertiesController@update_versicherung_approved_status');
    Route::post('/update_falk__status','PropertiesController@updateFalkStatus');
    Route::post('/delete_Property_insurance','PropertiesController@deletePropertyInsurance');
    Route::post('/add_versicherung','PropertiesController@add_versicherung');

    Route::post('/email_template2_save','HomeController@email_template2_save')->name('email_template2_save');
    Route::post('/send_email_template2','HomeController@send_email_template2')->name('send_email_template2');
    Route::post('/send_email_template2_to_all_banks','HomeController@send_email_template2_to_all_banks')->name('send_email_template2_to_all_banks');


    Route::post('/send_email_template2_sending_info','HomeController@send_email_template2_sending_info')->name('send_email_template2_sending_info');
    Route::post('/sendemailtobankers','HomeController@sendEmailToBankers')->name('sendemailtobankers');

    Route::post('/savebankinfoformail','HomeController@savebankinfoformail')->name('savebankinfoformail');
    Route::post('/savenote','PropertiesController@addNote');
    Route::get('/delete_bank_email_log/{id}','PropertiesController@deleteBankEmailLog');
    Route::post('/savenoteofpropertyrentpaid','PropertiesController@saveNotesOfPropertyRentPaid');


    Route::get('/signin', 'AuthController@signin');
    Route::get('/authorize', 'AuthController@gettoken');
    Route::get('/mail', 'OutlookController@mail')->name('mail');
//Route::get('/calendar', 'OutlookController@calendar')->name('calendar');
    Route::get('/contacts', 'OutlookController@contacts')->name('contacts');

    Route::get('/read_message/{id}', 'OutlookController@read_message');
    Route::get('/reply_message/{id}', 'OutlookController@reply_message');

    Route::get('/rashid', 'AdsController@rashid');
    Route::get('/set_seller_data', 'PropertiesController@set_seller_data');
    Route::get('/panga', 'PropertiesController@panga');

    Route::post('/ad_pdf', 'AdsController@ad_pdf');
    Route::post('/update_image_order', 'AdsController@update_image_order')->name('update_image_order');
    Route::post('/update_image_order1', 'AdsController@update_image_order1')->name('update_image_order1');

    Route::get('/delete-property-single-image/{id}/{image}', 'HomeController@delete_property_single_image');
    Route::get('getPropertyInfo/{id}', 'AdsController@getPropertyInfo')->name('getPropertyInfo');

    Route::group(['middleware' => ['web']], function () {


########## Ads Routes
        Route::post('create_new_post', 'AdsController@store')->name('save_new_post');
        Route::post('delete-images', 'AdsController@deleteImages')->middleware('auth')->name('delete_ads_images');
        Route::post('delete-ad-images', 'AdsController@deleteAdImages')->middleware('auth')->name('delete_ad_images');
        Route::post('delete-ads-pdf', 'AdsController@deletePDF')->middleware('auth')->name('delete_ads_pdf');
        Route::post('importtenant', 'AdsController@importtenant')->name('importtenant');

        Route::post('get-state-by-country', ['as'=>'get_state_by_country', 'uses' => 'AdsController@getStateByCountry']);
        Route::post('get-city-by-state', ['as'=>'get_city_by_state', 'uses' => 'AdsController@getCityByState']);

        Route::post('uploadfiles', ['as'=>'uploadfiles', 'uses' => 'AdsController@uploadfiles']);
        Route::post('uploadattachments', ['as'=>'uploadattachments', 'uses' => 'HomeController@uploadattachments']);
        Route::get('remove_attachment_file', ['as'=>'remove_attachment_file', 'uses' => 'HomeController@remove_attachment_file']);



        Route::post('delete_comment_file', 'PropertiesController@delete_comment_file')->middleware('auth')->name('delete_comment_file');






        Route::get('trello', 'DashboardController@trello')->name('trello');
        Route::get('factsheet', 'DashboardController@factsheet')->name('factsheet');
        Route::get('releaselogs', 'DashboardController@releaselogs')->name('releaselogs');
        Route::get('vreleaselogs', 'DashboardController@vreleaselogs')->name('vreleaselogs');
        Route::get('export-wault-bestand', 'DashboardController@exportWaultBestand')->name('export_wault_bestand');


        Route::post('save_vacant_data', 'AdsController@saveVacantdata')->name('save_vacant_data');
        Route::post('save_vacant_space', 'AdsController@saveVacantSpace')->name('save_vacant_space');

        Route::get('remove_vacant_media', 'AdsController@removeVacantMedia')->name('remove_vacant_media');
        Route::get('add_new_vacant', 'AdsController@add_new_vacant')->name('add_new_vacant');

        Route::get('set_vacant_media_favourite', 'AdsController@set_vacant_media_favourite')->name('set_vacant_media_favourite');
        Route::get('set_ad_image_favourite', 'AdsController@set_ad_image_favourite')->name('set_ad_image_favourite');
        Route::get('set_vacantitem_favourite', 'AdsController@set_vacantitem_image_favourite')->name('set_vacantitem_favourite');



        Route::get('set_export_media_favourite', 'AdsController@set_export_media_favourite')->name('set_export_media_favourite');
        Route::get('remove_uploaded_file', 'PropertiesController@remove_uploaded_file')->name('remove_uploaded_file');


        Route::get('lease_list', 'PropertiesController@lease_list')->name('lease_list');
        Route::get('recommended_list', 'PropertiesController@recommended_list')->name('recommended_list');



####### new dashborad
        Route::group(array('middleware' => ['auth', 'admin']), function(){

            Route::get('dashboard', 'HomeController@dashborad')->name('dashboard');
            Route::get('dashboard2', 'RoleDashboardController@dashboard2')->name('dashboard2');
            Route::get('dashboardtest', 'HomeController@dashboardtest')->name('dashboardtest');

            Route::get('/updatepropertyuser', 'PropertiesController@updatepropertyuser')->name('updatepropertyuser');
            Route::get('/removebank', 'PropertiesController@removebank')->name('removebank');
            Route::get('/deletesheet', 'PropertiesController@deletesheet')->name('deletesheet');

            Route::get('/searchpbank', 'PropertiesController@searchpbank')->name('searchpbank');
            Route::get('/removepropertybanken', 'PropertiesController@removepropertybanken')->name('removepropertybanken');
            Route::get('/removestatusloi', 'PropertiesController@removestatusloi')->name('removestatusloi');
            Route::post('/addBankenContactEmployee', 'PropertiesController@addBankenContactEmployee')->name('addBankenContactEmployee');

            Route::get('/export-properties-haus/{slug}', 'HomeController@exportPropertiesHaus')->name('export-properties-haus');

            Route::post('/statusloi/update/{id}', 'PropertiesController@updatestatusloi')->name('updatestatusloi');
            // Route::post('/changeempfehlungdetails', 'PropertiesController@changeempfehlungdetails')->name('changeempfehlungdetails');
            Route::post('/changeprovisioninfo', 'PropertiesController@changeprovisioninfo')->name('changeprovisioninfo');
            Route::post('/deleteprovisioninfo', 'PropertiesController@deleteprovisioninfo')->name('deleteprovisioninfo');


            Route::get('getReleasebutton', ['as'=>'getReleasebutton', 'uses' => 'PropertiesController@getReleasebutton']);
            Route::get('getProvisionbutton', ['as'=>'getProvisionbutton', 'uses' => 'PropertiesController@getProvisionbutton']);
            Route::get('get-provision-comment/{id}/{limit?}', ['as'=>'get_provision_comment', 'uses' => 'PropertiesController@getProvisionComment']);
            Route::post('add-provision-comment', ['as'=>'add_provision_comment', 'uses' => 'PropertiesController@addProvisionComment']);
            Route::get('delete-provision-comment/{id}', ['as'=>'delete_provision_comment', 'uses' => 'PropertiesController@deleteProvisionComment']);
            Route::post('/add_property_bank', 'PropertiesController@add_property_bank')->name('add_property_bank');
            Route::post('/updatebanken/{id}', 'PropertiesController@updatebanken')->name('updatebanken');
            Route::get('/searchbank', 'PropertiesController@searchbank')->name('searchbank');
            Route::get('/removeextrarow', 'PropertiesController@removeextrarow')->name('removeextrarow');

            Route::get('tenant-detail', ['as'=>'tenant-detail', 'uses' => 'HomeController@getTenantDetail']);
            Route::get('getmanagerproperty', ['as'=>'getmanagerproperty', 'uses' => 'HomeController@getmanagerproperty']);
            Route::get('getbankproperty', ['as'=>'getbankproperty', 'uses' => 'HomeController@getbankproperty']);
            Route::get('getFinancingPerBank', ['as'=>'getFinancingPerBank', 'uses' => 'HomeController@getFinancingPerBank']);
            Route::get('getPropertiesDetail', ['as'=>'getPropertiesDetail', 'uses' => 'HomeController@getPropertiesDetail']);
            Route::get('getcategoryproperty', ['as'=>'getcategoryproperty', 'uses' => 'HomeController@getcategoryproperty']);
            Route::get('getPropertyInsuranceTab', ['as'=>'getPropertyInsuranceTab', 'uses' => 'DashboardController@getPropertyInsuranceTab']);




            Route::get('getcategoryproperty2', ['as'=>'getcategoryproperty2', 'uses' => 'HomeController@getcategoryproperty2']);
            Route::get('getcategoryproperty3', ['as'=>'getcategoryproperty3', 'uses' => 'HomeController@getcategoryproperty3']);


            Route::get('getMahnungProperties', ['as'=>'getMahnungProperties', 'uses' => 'HomeController@getMahnungProperties']);
            Route::get('getPropertyInvoice', ['as'=>'getPropertyInvoice', 'uses' => 'HomeController@getPropertyInvoice']);
            Route::get('getRecommendation', ['as'=>'getRecommendation', 'uses' => 'HomeController@getRecommendation']);

            Route::get('getRecommendationData', ['as'=>'getRecommendationData', 'uses' => 'HomeController@getRecommendationData']);

            Route::get('getContract', ['as'=>'getContract', 'uses' => 'HomeController@getContract']);
            Route::get('getPendingNotReleaseInvoice', ['as'=>'getPendingNotReleaseInvoice', 'uses' => 'HomeController@getPendingNotReleaseInvoice']);
            Route::get('getAssetPendingNotReleaseInvoice', ['as'=>'getAssetPendingNotReleaseInvoice', 'uses' => 'DashboardController@getAssetPendingNotReleaseInvoice']);

            Route::get('getInsurance', ['as'=>'getInsurance', 'uses' => 'HomeController@getInsurance']);
            Route::get('getManagement', ['as'=>'getManagement', 'uses' => 'HomeController@getManagement']);
            Route::get('getVacantList', ['as'=>'getVacantList', 'uses' => 'HomeController@getVacantList']);

            Route::get('getPropertyReleaseInvoice', ['as'=>'getPropertyReleaseInvoice', 'uses' => 'HomeController@getPropertyReleaseInvoice']);
            Route::get('release-invoice', ['as'=>'release_invoice', 'uses' => 'HomeController@releaseInvoice']);
            Route::get('release-contract', ['as'=>'release_contract', 'uses' => 'HomeController@releaseContract']);
            Route::get('getInsuranceReleaseLogs', ['as'=>'getInsuranceReleaseLogs', 'uses' => 'HomeController@getInsuranceReleaseLogs']);
            Route::get('getMahnungTenant', ['as'=>'getMahnungTenant', 'uses' => 'HomeController@getMahnungTenant']);
            Route::get('getMahnungAmount', ['as'=>'getMahnungAmount', 'uses' => 'HomeController@getMahnungAmount']);

            Route::get('getassetmanagerproperty', ['as'=>'getassetmanagerproperty', 'uses' => 'HomeController@getassetmanagerproperty']);

            Route::get('mailchimp', ['as'=>'mailchimp', 'uses' => 'HomeController@mailchimp']);
            Route::get('monthyearmanager', ['as'=>'monthyearmanager', 'uses' => 'HomeController@monthYearManager']);
            Route::get('monthyearassetmanager', ['as'=>'monthyearassetmanager', 'uses' => 'HomeController@monthYearAssetmanager']);


            Route::post('delete-pdf', 'PropertiesController@deletePdf')->name('delete_property_pdf');
            Route::post('add_new_bank', 'PropertiesController@addNewBank')->name('add_new_bank');
            Route::post('add_new_banken', 'PropertiesController@addNewBanken')->name('add_new_banken');
            Route::post('update_bank', 'PropertiesController@updateBank')->name('update_bank');
            Route::get('get_bank/{id}', 'PropertiesController@getBank')->name('get_bank');

            Route::get('getbanklist', ['as'=>'getbanklist', 'uses' => 'PropertiesController@getbanklist']);
            Route::get('getBankFinanceOffers', ['as'=>'getBankFinanceOffers', 'uses' => 'PropertiesController@getBankFinanceOffers']);
            Route::get('getAllBankMailLogsOfProperty/', ['as'=>'getAllBankMailLogsOfProperty', 'uses' => 'PropertiesController@getAllBankMailLogsOfProperty']);
            Route::get('getstatusloi', ['as'=>'getstatusloi', 'uses' => 'PropertiesController@getstatusloi']);
            Route::get('getStatusLoiByUserId', ['as'=>'getStatusLoiByUserId', 'uses' => 'PropertiesController@getStatusLoiByUserId']);
            Route::get('getstatusloidahboard/{month}', ['as'=>'getstatusloidahboard', 'uses' => 'DashboardController@getstatusloi']);
            Route::get('getStatusLoiByUserIdDashboard', ['as'=>'getStatusLoiByUserIdDashboard', 'uses' => 'DashboardController@getStatusLoiByUserId']);
            Route::get('export_properties', ['as'=>'export_properties', 'uses' => 'PropertiesController@export_properties']);
            // Route::get('add-new-vacant', ['as'=>'add-new-vacant', 'uses' => 'PropertiesController@addNewVacant']);

            Route::get('delete_new_vacant', ['as'=>'delete_new_vacant', 'uses' => 'PropertiesController@delete_new_vacant']);




            Route::resource('/', 'DashboardController');
            Route::get('/external', 'DashboardController@external')->name('external');

            Route::get('/getstatustable', 'DashboardController@getstatustable')->name('getstatustable');
            Route::get('/export_property_pdf', 'DashboardController@export_property_pdf')->name('export_property_pdf');
            Route::get('/searchproperty', 'DashboardController@searchproperty')->name('searchproperty');

            Route::get('/getstatustabledashboard', 'DashboardController@getstatustabledashboard')->name('getstatustabledashboard');

            //
            Route::post('/property_management/add', 'PropertyManagementController@add')->name('property_management.add');
            Route::post('/property_management/update_row', 'PropertyManagementController@update_row')->name('property_management.update');

            Route::get('/assetmanagement', 'AssetManagementController@index')->name('assetmanagement');
            Route::get('/assetmanagement-optimized', 'AssetManagementController@assetManagementOptimized')->name('assetmanagement_optimized');
            Route::get('/getRentCompare', 'AssetManagementController@getRentCompare')->name('getRentCompare');
            Route::get('/getRentCompareList', 'AssetManagementController@getRentCompareList')->name('getRentCompareList');
            Route::get('/assetmanagement/chart', 'ChartController@index')->name('assetmanagement.chart');
            Route::resource('/users', 'UserController');
            Route::resource('/properties', 'PropertiesController');
            Route::resource('/calendar', 'CalendarController');

            Route::get('/property/{id}/comments', 'PropertyCommentsController@show')->name('propertycomments.show');
            Route::get('/property/comments', 'PropertyCommentsController@_show')->name('propertycomments._show');

            Route::get('dashboard/get-retnal-activity', 'HomeController@getRentalActivity')->name('get_retnal_activity');
        });
        Route::get('add-new-vacant', ['as'=>'add-new-vacant', 'uses' => 'PropertiesController@addNewVacant'])->middleware('auth');
        Route::post('/changeempfehlungdetails', 'PropertiesController@changeempfehlungdetails')->name('changeempfehlungdetails')->middleware('auth');

        Route::post('/assetmanagement', 'AssetManagementController@index')->name('assetmanagementfilter');
        Route::post('property/saveEmpfehlungFile','PropertiesController@saveEmpfehlungFile')->name('saveEmpfehlungFile');
        Route::post('property/saveMieterlisteFile','PropertiesController@saveMieterlisteFile')->name('saveMieterlisteFile');
        Route::get('property/delete-tenant-payment-file/{id}','PropertiesController@deleteTenantPaymentFile')->name('delete_tenant_payment_file');

        Route::post('save_sheet', 'AdsController@saveSheet')->name('save_sheet');
        Route::get('create_soll/{id}/{bank_id}', 'AdsController@saveSoll')->name('create_soll');

        Route::post('save_gsheet', 'AdsController@saveGSheet')->name('save_gsheet');





//Heat Map
        Route::get('/heatmap', 'HomeController@heatmap')->name('heatmap');

        Route::get('/home', 'HomeController@index')->name('home');
        Route::post('/property/create-comment','PropertyCommentsController@create')->name('create_propertycomment');
        Route::get('/property/{id}/edit-comment','PropertyCommentsController@edit')->name('edit_propertycomment');
        Route::post('/property/update-comment','PropertyCommentsController@update_comment')->name('update_propertycomment');

        Route::post('/dashboard/change-comment','HomeController@changeComment')->name('change-comment');

        Route::post('upload-einkauf-file','HomeController@uploadBuyFiles')->name('upload-einkauf-file');

        Route::post('upload-bank-file','HomeController@uploadBankFiles')->name('upload-bank-file');





        Route::post('/property/edit_comment_iframe/{id}','PropertyCommentsController@edit_comment_show_iframe')->name('edit_comment_show_iframe');


        Route::post('/verkauf-item/update/{id}/{type}','PropertiesController@update_verkauf_item')->name('update_verkauf_item');
        Route::post('/verkauf/update/{id}','PropertiesController@update_verkauf')->name('update_verkauf');

        Route::get('/property/set_as_standard/{orignal_prop}/{pid}', 'PropertiesController@set_as_standard');

        Route::post('/einkauf-item/update/{id}/{type}','PropertiesController@update_einkauf_item')->name('update_einkauf_item');
        Route::post('/einkauf/update/{id}','PropertiesController@update_einkauf')->name('update_einkauf');



        Route::post('property/comment-date', array(
            'as' => 'update.comment.date',
            'uses' => 'PropertyCommentsController@updateCommentDate'));

        Route::post('/properties/{id}/update_property','PropertiesController@update_property')->name('update_property');
        Route::get('/property-comparison','PropertiesController@comparsion')->name('property_comparison');
        Route::post('/property-comparison','PropertiesController@comparsion')->name('property_comparison');
        Route::post('/calendar/create_calendar','CalendarController@create_calendar')->name('create_calendar');
        Route::post('/calendar/edit_event','CalendarController@edit_event')->name('edit_event');
        Route::get('/users/{id}/edit-profile','UserController@getProfile')->name('edit-profile');
        Route::post('/users/{id}/edit-profile','UserController@postProfile')->name('edit-profile');
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
        Route::get('/change-password', 'UserController@getChangePassword')->name('change-password');
        Route::post('/change-password', 'UserController@postChangePassword')->name('change-password');
        Route::get('/login-history', 'UserController@loginHistory')->name('login_history');
        Route::get('/login-history-data', 'UserController@loginHistoryData')->name('login_history_data');
        Route::get('/export-history-data', 'UserController@exportHistoryData')->name('export_history_data');

        Route::get('/property-history', 'UserController@propertyHistory')->name('property_history');
        Route::get('/property-history-data', 'UserController@propertyHistoryData')->name('property_history_data');

        Route::post('/change-property-status', 'PropertiesController@changeStatus')->name('properties.change_status');
        Route::get('/user-activities', 'UserController@showUserActivities')->middleware('auth')->name('user-activities');
        Route::post('/user-confirm-password', 'UserController@confirmPassword')->middleware('auth')->name('confirm_password');

############ Mietflächen save ############
        Route::post('/mietflachen', 'PropertiesController@mietflachen')->name('properties.mietflachen');



// Google Map
        Route::resource('/google-map', 'GoogleMapController')->middleware('auth');

//Notifications
        Route::post('/new-notification', 'NotificationsController@newNotification');
        Route::post('/seen-notification', 'NotificationsController@seenAllNotification');
        Route::get('/get-new-notification', 'NotificationsController@getNewNotification');

// Update property by field
        Route::post('property/update/{id}/','PropertiesController@update_property_by_field');
        Route::post('property/create_direct','PropertiesController@create_property_direct')->name('create_property_direct');
        Route::post('property/update_direct','PropertiesController@update_property_direct')->name('update_property_direct');


// Update property by Ist, Soll

        Route::post('property/update_property_ist_soll/{tab}/{bank_id}','PropertiesController@update_property_ist_soll');
        Route::post('property/update_property_budget/{id}','PropertiesController@update_property_budget');

        Route::post('property/saveleaseactivity','PropertiesController@saveleaseactivity')->name('saveleaseactivity');
        Route::post('property/saveractivity','PropertiesController@saveractivity')->name('saveractivity');
        Route::post('property/updatelease','PropertiesController@updatelease')->name('updatelease');




        Route::post('property/update_pdf_name/{id}','PropertiesController@update_pdf_name');





        Route::post('property/updateStatusInHold', 'PropertiesController@changeStatusInHold');
        Route::post('property/update/schl/{id}/','PropertiesController@update_property_schl_by_field');
        Route::post('property/update/schl2/{id}/','PropertiesController@update_property_schl_by_field2');

        Route::post('property/update/bankoffer/{id}/','PropertiesController@update_bankoffer');
        Route::post('property/update/emailsendinginfo/{id}/','PropertiesController@emailsendinginfo');



        Route::post('property/update/bank/{id}/{bank_id}','PropertiesController@update_property_bank');
        Route::post('property/update/steuerberater/{id}/','PropertiesController@update_property_Steuerberater');



        Route::post('property/save-shcl-banks/{id}/', 'PropertiesController@saveSchlBanks');
        Route::post('property/update/planung-berechnung-wohnen/{id}/','PropertiesController@update_property_planung_by_field');
        Route::post('property/update/clever-fit/{id}/','PropertiesController@update_property_clever_fit_by_field');

        Route::post('property/create_property_insurance','PropertiesController@create_property_insurance')->name('create_property_insurance');
        Route::post('property/update/insurance/{id}/','PropertiesController@update_property_insurance_by_field');
        Route::post('property/delete_property_insurance','PropertiesController@delete_property_insurance')->name('delete_property_insurance');

        Route::post('property/create_maintenance','PropertiesController@create_maintenance')->name('create_maintenance');
        Route::post('property/update/maintenance/{id}/','PropertiesController@update_maintenance_by_field');
        Route::post('property/delete_maintenance','PropertiesController@delete_maintenance')->name('delete_maintenance');

        Route::post('property/change_date','TenancyScheduleController@change_date')->name('change_date');

        Route::post('property/delete_tenant','TenancyScheduleController@delete_tenant')->name('delete_tenant');


        Route::post('property/create_investation','PropertiesController@create_investation')->name('create_investation');
        Route::post('property/update/investation/{id}/','PropertiesController@update_investation_by_field');
        Route::post('property/delete_investation','PropertiesController@delete_investation')->name('delete_investation');

        Route::post('property/update/invoice/{id}/','PropertiesController@update_invoice_by_field');
        Route::post('update-insurance-by-field{id}/','PropertiesController@updateInsuranceByField')->name('update_insurance_by_field');
        Route::get('/setAssestManagerOnProperty/{property_id}/{am_id?}', 'AssetManagementController@setAssestManagerOnProperty')->middleware('auth')->name('setAssestManagerOnProperty');



        Route::post('property/create_service_provider','PropertiesController@create_service_provider')->name('create_service_provider');
        Route::post('property/update/serviceprovider/{id}/','PropertiesController@update_property_service_provider_by_field');
        Route::post('property/delete_service_provider','PropertiesController@delete_service_provider')->name('delete_service_provider');

        Route::post('property/create_loans_mirror','PropertiesController@create_loans_mirror')->name('create_loans_mirror');
        Route::post('property/save_planning_data','PropertyController@save_planning_data')->name('save_planning_data');
        Route::post('property/add-loan-mirror/{property_id}','PropertiesController@addLoanMirror')->name('add_loan_mirror');

        Route::get('forecasts', 'ForecastsController@index')->name('forecast.index');
        Route::post('forecast/create', 'ForecastsController@store');
        Route::post('forecast/update/{id}', 'ForecastsController@update_by_field');
        Route::post('forecast/delete/{id}', 'ForecastsController@delete');
        Route::post('forecast/{id}/forecast-kind/create', 'ForecastKindsController@store');
        Route::post('forecast-kind/update/{id}', 'ForecastKindsController@update_by_field');


        Route::group(array('middleware' => ['auth', 'admin']), function(){
            //Tenancy Schedule

            Route::group(['prefix' => 'tenancy-schedules'], function () {
                Route::get('/', 'TenancyScheduleController@index')->name('tenancy-schedules.index');
                Route::post('/create', 'TenancyScheduleController@create')->name('tenancy-schedules.create');
                Route::post('/update/{id}', 'TenancyScheduleController@update_by_field')->name('tenancy-schedules.update-by-field');
                Route::post('/delete/{id}', 'TenancyScheduleController@delete')->name('tenancy-schedules.delete');
                Route::post('/delete/{id}/item/{itemId}', 'TenancyScheduleController@deleteItem')->name('tenancy-schedules.item.delete');
            });

            Route::group(['prefix' => 'masterliste'], function () {
                Route::get('/', 'MasterlisteController@index')->name('masterliste.index');
                Route::get('/listing', 'MasterlisteController@show')->name('masterliste.listing');
            });

            Route::post('tenancy-payment/update/{id}/{year}', 'TenancyScheduleController@updatePayment')->name('tenancy-schedules.update-payment-by-field');


            Route::group(['prefix' => 'tenancy-schedule-items'], function () {
                Route::post('/create', 'TenancyScheduleController@createItem')->name('tenancy-schedules.create-item');
                Route::post('/update/{id}', 'TenancyScheduleController@update_item_by_field')->name('tenancy-schedules.update-item-by-field');


                Route::post('/delete/{id}', 'TenancyScheduleController@deleteItem')->name('tenancy-schedules.delete');
                Route::post('/store-comments', 'TenancyScheduleController@storeComments');
                Route::post('/store-Comment2', 'TenancyScheduleController@storeComment2');
                Route::get('/get-comments', 'TenancyScheduleController@getComments');
                Route::get('/find', 'TenancyScheduleController@find');

                Route::get('/comment/get', 'TenancyScheduleController@getItemComment')->name('getItemComment');
                Route::post('/comment/add', 'TenancyScheduleController@addItemComment')->name('addItemComment');
                Route::get('/comment/delete', 'TenancyScheduleController@deleteItemComment')->name('deleteItemComment');

            });
            // Banks
            Route::group(['prefix' => 'banks'], function () {
                Route::get('/', 'BanksController@index')->name('banks');;
                Route::get('/add', 'BanksController@showAdd')->name('bank-add');
                Route::get('/edit/{id}', 'BanksController@showEdit')->name('bank-edit');
                Route::get('/view/{id}', 'BanksController@view')->name('bank-view');
                Route::post('/add', 'BanksController@add');
                Route::post('/direct_create', 'BanksController@direct_create')->name('bank-direct-create');;
                Route::post('/update/{id}', 'BanksController@update');
                Route::post('/updatefield/{id}', 'BanksController@update_banks_by_field');
                Route::post('/delete', 'BanksController@delete')->name('bank-delete');
                Route::get('/radius-search', 'BanksController@radiusSearch');

            });
            Route::group(['prefix' => 'contacts'], function () {
                Route::get('/', 'ContactController@index')->name('contacts');
                Route::get('/view/{id}', 'ContactController@view')->name('contact-view');
                Route::post('/delete', 'ContactController@delete')->name('contact-delete');
                Route::post('add_new_contact', 'ContactController@addNewContact')->middleware('auth')->name('add_new_contact');
                Route::get('detail', 'ContactController@detail')->name('contact-detail');
            });
        });
        Route::get('/delete-comment/{id}/item/{itemId}', 'TenancyScheduleController@deleteComment');

        //Update tenants by field
        Route::post('tenant/update/{id}', 'TenantsController@updateTenantByField')->name('tenants.update');
        Route::post('tenant/create', 'TenantsController@createTenant')->name('tenants.store');






        Route::post('property/select_bank/{id}/','PropertiesController@update_property_bank_fields')->name('properties.select_bank');

// Verkauf tab add new row
        Route::post('property/verkauf_new_row/','PropertiesController@verkauf_new_row');
        Route::post('property/update_verkauf_tab_row/{row_id}','PropertiesController@update_verkauf_tab_row');
        Route::get('property/delete_verkauf_tab_row/{row_id}','PropertiesController@delete_verkauf_tab_row');
        Route::post('property/update_verkauf_tab_row_others/','PropertiesController@update_verkauf_tab_row_others');
        Route::post('property/update_row_11_12_name/','PropertiesController@update_row_11_12_name');
        Route::get('property/ajax_verkauf_tab_load/{property_id}/{g59}','PropertiesController@ajax_verkauf_tab_load');
        Route::post('property/update_month_verkauf_tab/{p_id}','PropertiesController@update_month_verkauf_tab');

        Route::post('properties-comment/save','PropertiesCommentController@saveNewComment');
        Route::get('properties-comment/get-comments','PropertiesCommentController@getComments');
        Route::get('properties-comment/delete-sheet-comment/{id}','PropertiesCommentController@deleteComment');



        // Charts
        Route::get('charts','PropertiesController@showCharts')->name('show_charts');

        //Update masterliste by field
        Route::post('masterliste/update/{id}', 'MasterlisteController@updateMasterlisteByField')->name('masterliste.update');
        Route::post('masterliste/create', 'MasterlisteController@createMasterliste')->name('masterliste.store');
        Route::post('masterliste/delete', 'MasterlisteController@delete')->name('masterliste.delete');

        Route::get('/sync_all_masterliste','MasterlisteController@sync_all_masterliste')->name('masterliste.sync');

        Route::get('feedback','FeedbackController@index')->middleware('auth')->name('feedback');
        Route::post('feedback/add','FeedbackController@add')->middleware('auth')->name('feedback.add');


        //Property iframe
        Route::get('propertyIframe/{id}','PropertiesController@showIframe')->name('properties.showIframe');

        //banks iframe
        Route::get('banksIframe/{id}/{bank_id}/{tab}','PropertiesController@banksIframe');


        Route::get('make_clone','BanksController@make_clone');


        Route::get('/tenant/pdf/{id}','PropertiesController@exportPdf');


        Route::post('addPropertiesExtra1Row','PropertiesController@addPropertiesExtra1Row')->name('properties.addPropertiesExtra1Row');
        Route::post('propertiesExtra1/update/{sheet}/{id}','PropertiesController@updatePropertiesExtra1')->name('properties.updatePropertiesExtra1');
        Route::get('export/csv', 'HomeController@exportCSV');

        Route::post('property/export-to-excel', 'PropertiesController@export_to_excel');
        Route::post('property/export-to-excel-second', 'PropertiesController@export_to_excel_second');
        Route::get('bankexport/{id}', 'PropertiesController@bankexport')->name('bankexport');

        Route::post('export-to-excel-haus', 'HomeController@export_to_excel_haus');


        Route::post('property/export-tenancy-to-pdf', 'PropertiesController@export_tenancy_to_pdf');


        Route::post('property/export-tenancy-to-excel', 'PropertiesController@export_tenancy_to_excel');
        Route::get('property/export-object-data-list-excel/{id}', 'PropertiesController@exportDataListObjectOfProperty');

        Route::group(['prefix' => 'cron'], function () {
            Route::get('/update-masterliste', 'CronController@update_masterliste')->name('update_masterliste');
            Route::get('/tenant-expiration-notification', 'CronController@sendTenantExpireRemainder')->name('tenant-expiration-notification');
            Route::get('/insurance-expiration-notification', 'CronController@sendInsuranceExpireRemainder')->name('insurance-expiration-notification');
            Route::get('/purchase-notification', 'CronController@sendPurchaseRemainder')->name('purchase-notification');
            Route::get('/update_properties', 'CronController@update_properties')->name('update_properties');
            Route::get('/invoice_overview', 'CronController@invoiceOverview')->name('invoice_overview');
            Route::get('/expiry_overview', 'CronController@expiryOverview')->name('expiry_overview');
            Route::get('/updateverkauflisting', 'CronController@updateverkauflisting')->name('updateverkauflisting');
            Route::get('/store-liquiplanung', 'CronController@storeLiquiplanung')->name('store_liquiplanung');

            Route::get('/logoutall', 'CronController@logoutAll')->name('logoutall');


        });


        ##### upload pdf
        Route::post('property_upload_pdf', 'PropertiesController@property_upload_pdf');

        Route::post('property/sendmail', 'PropertiesController@sendmail');
        Route::post('property/einkaufsendmail', 'PropertiesController@einkaufsendmail');
        Route::post('property/einkaufsendmail2', 'PropertiesController@einkaufsendmail2');

        Route::post('property/releaseprocedure', 'PropertiesController@releaseprocedure');
        Route::post('property/vacantreleaseprocedure', 'PropertiesController@vacantreleaseprocedure');
        Route::post('property/vacantmarkednotrelease', 'PropertiesController@vacantmarkednotrelease')->name('vacant_mark_as_not_release');
        Route::post('property/vacant-mark-as-pending/{vacant_id}', 'PropertiesController@vacantMarkAsPending')->name('vacant_mark_as_pending');


        Route::post('property/provisionreleaseprocedure', 'PropertiesController@provisionreleaseprocedure');
        Route::post('property/provision-mark-as-not-release/{property_id}/{id?}', 'PropertiesController@provisionMarkAsNotRelease')->name('provision_mark_as_not_release');

        Route::post('property/verkaufreleaseprocedure', 'PropertiesController@verkaufreleaseprocedure');
        Route::post('releaseoplist', 'PropertiesController@releaseoplist')->name('releaseoplist');


        Route::post('property/invoicereleaseprocedure', 'PropertiesController@invoicereleaseprocedure');
        Route::post('property/dealreleaseprocedure', 'PropertiesController@dealreleaseprocedure');

        Route::post('property/invoicenotrelease', 'HomeController@markAsNotReleased');
        Route::post('property/invoicemarkpending', 'HomeController@markAsPending');
        Route::post('property/invoicemarkpendingam', 'HomeController@markAsPendingAm');
        Route::post('property/invoicemarkreject', 'PropertiesController@invoiceMarkAsReject');
        Route::post('property/addmaillog', 'PropertiesController@addmaillog');
        Route::post('property/property-management-not-release', 'PropertiesController@propertyManagementNotRelease')->name('propertyManagementNotRelease');



        Route::post('property/deal-mark-as-notrelease', 'PropertiesController@dealMarkAsNotReleased')->name('deal_mark_as_notrelease');
        Route::post('property/insurance-mark-as-notrelease', 'PropertiesController@insuranceMarkAsNotReleased')->name('insurance_mark_as_notrelease');
        Route::post('property/insurance-mark-as-notrelease-am', 'PropertiesController@insuranceMarkAsNotReleasedAM')->name('insurance_mark_as_notrelease_am');
        Route::post('property/insurance-mark-as-pending', 'PropertiesController@insuranceMarkAsPending')->name('insurance_mark_as_pending');


        Route::post('property/deal-mark-as-pending', 'PropertiesController@dealMarkAsPending')->name('deal_mark_as_pending');;

    });



    Route::get('/working', 'UserController@working');





    Route::get('/add_api_contacts', 'HomeController@add_api_contacts')->name('users.add_api_contacts');
    Route::post('/post_api_contacts', 'HomeController@post_api_contacts')->name('users.post_api_contacts');

    Route::get('/api_contacts_list', 'HomeController@api_contact_list');

    Route::get('/delete_user/{id}', 'HomeController@delete_user');
    Route::post('/properties/upload_listing_to_api', 'HomeController@upload_listing_to_api');
    Route::post('/properties/api_imag_del', 'HomeController@api_imag_del');

    Route::group(array('middleware' => ['auth']), function(){
        ####### new dashborad
        Route::post('/api_listing_post', 'HomeController@api_listing_post')->name('api_command');
        Route::post('/properties/upload_attachment_images', 'HomeController@upload_attachment_images');


        Route::get('banks/search', 'BanksController@search')->name('banks.search');
        Route::get('banks/search_result', 'BanksController@search_result')->name('banks.search_result');

        /////////// Questions Routes ////////////
        Route::get('/question','QuestionController@index')->name('question');
        Route::get('/question/create','QuestionController@create')->name('question');
        Route::get('/question/{id}','QuestionController@edit')->name('question');
        Route::post('/question/update','QuestionController@update')->name('question');

        /***** Banks COntroller ****/
        Route::group(['middleware' => ['password_protected_page']], function() {
            Route::get('/bank_accounts_balance','BanksController@getAllBankAccountsBalance')->name('bankAccountsBalance');
            Route::get('import_bank_connection','BanksController@importBankConnectionView');
            Route::get('/bank-connection','BanksController@getAllBankConnections');
            Route::post('/import_bank_connection','BanksController@importBankConnection');
            Route::get('/bank/{id}','BanksController@getBank')->name('getBank');
            Route::get('/account/','BanksController@getAccount')->name('getAccount');
            Route::get('/show-accounts', 'BankAccountUploadController@showBankData');

        });


        Route::get('/get_property_record','DashboardController@getData');
        Route::get('file-manager/list-for-select-gdrive-folder', 'FileManagerController@listForSelectGdriveFolder');
        Route::get('file-manager/list-for-select-gdrive-file', 'FileManagerController@listForSelectGdriveFile');
        Route::get('file-manager/view-files', 'FileManagerController@viewFiles');
        Route::get('file-manager/list-files-for-delete', 'FileManagerController@listFilesForeDelete');
        Route::post('file-manager/gdrive-link-delete', 'FileManagerController@deleteGdriveFile');
        Route::post('file-manager/send-gdrive-file-attachment-mail', 'FileManagerController@sendGdriveFileAttachmentMail');

        Route::post('/dir_permission', 'FileManagerController@directory_Permission');
        Route::get('/get_permission', 'FileManagerController@get_Permission');
        Route::get('/get_User_Permission', 'FileManagerController@get_User_Permission');


        Route::group(['prefix' => '/fcr_drive'], function () {
            Route::get('/', 'MainFileManagerController@showDriveOnMainPage');
            Route::get('/layout', 'MainFileManagerController@index');
            Route::get('/folders', 'MainFileManagerController@listDirSidebar');
            Route::get('/newfolder', 'MainFileManagerController@newfolder');
            Route::post('/upload', 'MainFileManagerController@upload')->name('fm.upload');
            Route::get('/jsonitems', 'MainFileManagerController@listMain');
            Route::post('/deleteDir', 'MainFileManagerController@deleteDir');
            Route::post('/delete', 'MainFileManagerController@deleteFile');
            Route::post('/rename', 'MainFileManagerController@renameFile');
            Route::post('/renameDir', 'MainFileManagerController@renameDir');
            Route::get('/create_new_folder_for_all_properties', 'MainFileManagerController@createNewFolderForAllProperties');
            Route::get('/errors', function(){
                return [];
            });
            Route::get('/download', 'MainFileManagerController@download');
            Route::get('/download_invoice', 'MainFileManagerController@downloadInvoices');

            Route::get('/download_invoicezip', 'MainFileManagerController@downloadInvoiceszip')->name('download_invoicezip');
        });
        Route::get('/drive_file/{fileId}', 'FileManagerController@showFile');
        // Route::get('/download_property_pdf', function(){
        // return view('pdf.index');
        // });


        Route::post('select-einkauf-file', 'HomeController@selectEinkaufFile');
        Route::post('select-einkauf-dir', 'HomeController@selectEinkaufDir');

        Route::post('select-am-log-file', 'PropertiesController@selectAmLogFile')->name('select_am_log_file');
        Route::post('select-am-log-dir', 'PropertiesController@selectAmLogDir')->name('select_am_log_dir');

        Route::post('select-mieterliste-file/{property_id}', 'PropertiesController@selectMieterlisteFile')->name('select_mieterliste_file');
        Route::post('select-mieterliste-dir/{property_id}', 'PropertiesController@selectMieterlisteDir')->name('select_mieterliste_dir');

        Route::post('add-provision-file/{property_id}', 'PropertiesController@addProvisionFile')->name('add_provision_file');
        Route::post('add-provision-dir/{property_id}', 'PropertiesController@addProvisionDir')->name('add_provision_dir');

        Route::get('/company', 'CompanyController@index')->name('company');
        Route::get('/company/add', 'CompanyController@create')->name('company.add');
        Route::post('/company/store', 'CompanyController@store')->name('company.store');
        Route::get('/company/edit/{id}', 'CompanyController@edit')->name('company.edit');
        Route::post('/company/update/{id}', 'CompanyController@update')->name('company.update');
        Route::get('/company/delete/{id}', 'CompanyController@delete')->name('company.delete');
        Route::post('/company/search', 'CompanyController@SearchEmployee');
        Route::get('autocomplete', 'CompanyController@autocomplete')->name('autocomplete');
        Route::get('/company/{id}/email/type/{typeId}', 'CompanyController@getEmails');


        Route::get('/company/employee/{id}', 'EmployeeController@index')->name('employee');
        Route::get('/company/{id}/employee/add', 'EmployeeController@create');
        Route::post('employee/store', 'EmployeeController@store');
        Route::get('/employee/edit/{id}', 'EmployeeController@edit');
        Route::post('/employee/update/{id}', 'EmployeeController@update');
        Route::get('/employee/delete/{id}', 'EmployeeController@delete');
        Route::get('/excel-file-import', 'EmployeeController@importExcelFile');
        Route::get('/bank-csv-upload', 'AddressesJobController@uploadBankToDb');
        Route::post('/sendEmail', 'AddressesJobController@sendEmailFromCsv');
        Route::post('/emailConfig', 'EmailConfigController@store');
        Route::get('/emailConfig', 'EmailConfigController@find');



//        Route::get('/mail-box', 'MailBoxController@index');
        Route::get('/mail-box/search', 'MailBoxController@getReceivedEmails');
        Route::get('/mail-box', 'MailBoxController@testView');
        Route::get('/sync-email', 'MailBoxController@syncEmails');


        Route::post('/properties/sendmail-to-am', 'PropertiesController@sendmail_to_am')->name('sendmail_to_am');
        Route::post('/properties/sendmail-to-all', 'PropertiesController@sendmail_to_all')->name('sendmail_to_all');

        Route::post('/properties/add-recommended-comment', 'PropertiesController@addRecommendedComment')->name('add_recommended_comment');
        Route::get('/properties/delete-recommended-comment/{id}', 'PropertiesController@deleteRecommendedComment')->name('delete_recommended_comment');
        Route::get('/properties/get-recommended-comment/{id}/{limit?}', 'PropertiesController@getRecommendedComment')->name('get_recommended_comment');

        Route::post('/dashboard/invoice-sendmail-to-am', 'HomeController@invoiceSendmailToAM')->name('invoice_sendmail_to_am');
        Route::post('/dashboard/sendmail-to-custom-user', 'HomeController@sendmailToCustomUser')->name('sendmail_to_custom_user');
        Route::post('/dashboard/sendmail-to-loi-user', 'HomeController@sendmailToLoiUser')->name('sendmail_to_loi_user');

        Route::post('/dashboard/sendmail-to-user', 'HomeController@sendmail_to_user')->name('sendmail_to_user');
        Route::post('/properties/add-property-invoice', 'PropertiesController@addPropertyInvoice')->name('add_property_invoice');
        Route::get('/properties/get-property-invoice/{property_id}', 'PropertiesController@getPropertyInvoice')->name('get_property_invoice');
        Route::get('/properties/get-invoice-logs/{property_id}', 'PropertiesController@getInvoiceLogs')->name('get_invoice_logs');
        Route::get('/properties/get-deal-logs/{property_id}', 'PropertiesController@getDealLogs')->name('get_deal_logs');

        Route::get('/properties/get-release-property-invoice/{property_id}', 'PropertiesController@getReleasePropertyInvoice')->name('get_release_property_invoice');

        Route::get('get-not-release-invoice/{id}', 'PropertiesController@getNotReleaseInvoice')->name('get_not_released_invoice');
        Route::get('get-not-release-invoice-am/{id}', 'PropertiesController@getNotReleaseInvoiceAM')->name('get_not_released_invoice_am');
        Route::get('get-not-release-invoice-hv/{id}', 'PropertiesController@getNotReleaseInvoiceHV')->name('get_not_released_invoice_hv');
        Route::get('get-not-release-invoice-user/{id}', 'PropertiesController@getNotReleaseInvoiceUser')->name('get_not_released_invoice_user');


        Route::get('get-pending-invoice/{id}', 'PropertiesController@getPendingInvoice')->name('get_pending_invoice');
        Route::get('get-pending-am-invoice/{id}', 'PropertiesController@getPendingAMInvoice')->name('get_pending_am_invoice');
        Route::get('get-pending-hv-invoice/{id}', 'PropertiesController@getPendingHVInvoice')->name('get_pending_hv_invoice');
        Route::get('get-pending-user-invoice/{id}', 'PropertiesController@getPendingUserInvoice')->name('get_pending_user_invoice');




        Route::get('/properties/delete-property-invoice/{id}', 'PropertiesController@deletePropertyInvoice')->name('delete_property_invoice');
        Route::get('/properties/set-property-invoice-email/{id}/{email?}', 'PropertiesController@setPropertyInvoiceEmail')->name('set_property_invoice_email');
        Route::get('/properties/download-property-invoice/{file_name}', 'PropertiesController@downloadPropertyInvoice')->name('download_property_invoice');
        Route::get('getPropertyProvision', ['as'=>'getPropertyProvision', 'uses' => 'HomeController@getPropertyProvision']);
        Route::get('getSocialFollowers', ['as'=>'getSocialFollowers', 'uses' => 'HomeController@getSocialFollowers']);
        Route::get('makeAsPaid', ['as'=>'makeAsPaid', 'uses' => 'HomeController@makeAsPaid']);
        Route::get('getPropertyDefaultPayers', ['as'=>'getPropertyDefaultPayers', 'uses' => 'DashboardController@getPropertyDefaultPayers']);
        Route::get('getPropertyInvoiceRequest', ['as'=>'getPropertyInvoiceRequest', 'uses' => 'DashboardController@getPropertyInvoiceRequest']);






        Route::resource('/adressen', 'AddressesJobController');
        Route::get('address/file-upload/{id}','AddressesJobController@fileCreate')->name('file');
        Route::get('address/edit/{id}','AddressesJobController@edit');
        Route::post('demo/update/{id}','AddressesJobController@update');
        Route::get('/excel/{id}','AddressesJobController@excelView');
        Route::get('address/email/logs','AddressesJobEmailLogController@index');
        Route::get('address/logs','AddressesJobEmailLogController@create');
        Route::get('address/file-emails','AddressesJobController@getEmailsFromFile');
        Route::get('property/{id}/address/email/logs','AddressesJobEmailLogController@emailLog');


    });

    Route::group(array('middleware' => ['auth']), function(){
        Route::get('/extern_user', 'FileManagerController@externUserDrive');

        Route::group(['prefix' => '/property/{propertyId}/file-manager'], function () {
            Route::get('/', 'FileManagerController@index');
            Route::get('/Rechnungen', 'FileManagerController@index')->defaults('chield_dir', 'Rechnungen');
            Route::get('/folders', 'FileManagerController@listDirSidebar');
            Route::get('/newfolder', 'FileManagerController@newfolder');
            Route::post('/upload', 'FileManagerController@upload')->name('fm.upload');
            Route::get('/jsonitems', 'FileManagerController@listMain');
            Route::get('/list-for-select', 'FileManagerController@listForSelect');
            Route::post('/deleteDir', 'FileManagerController@deleteDir');
            Route::post('/delete', 'FileManagerController@deleteFile');
            Route::post('/rename', 'FileManagerController@renameFile');
            Route::post('/renameDir', 'FileManagerController@renameDir');
            Route::get('/errors', function(){
                return [];
            });
            Route::get('/download', 'FileManagerController@download');
            Route::get('/removeDuplicate', 'FileManagerController@removeDuplicateSubDirectory');
            Route::get('/propertyDirectoryName', 'FileManagerController@checkPropertyDirectoryName');
        });
        Route::get('/extern_user', 'ExternalUserController@index')->name('extern_home');
        Route::get('/invoiceobjects/{propertyId}', 'ExternalUserController@invoiceobjects')->name('invoiceobjects');
        Route::get('/extern_user/property/{propertyId}', 'ExternalUserController@showProperty');
        Route::get('/extern_user/dashboard', 'ExternalUserController@dashboard');
    });

    /////Routes for user forms///////////////
    Route::get('forms/{token}/{step}/viewform','PropertiesController@ShowUserForm')->name('forms');
    route::get('saveuserForm', function(){
        Log::info('Route get called for saveuserForm');
    });
    Route::get('saveuserForm/success', function(){
        Log::info('Route get called for saveuserForm-success');
        return view('user_form.success');
    });
    Route::get('saveuserForm/fail', function(){
        Log::info('Route get called for saveuserForm-fail');
        return view('user_form.fail');
    });

    Route::post('showresult','PropertiesController@showResult')->name('showresult');


    Route::get('/password_for_protected_page_access', function () {
        return view('banks.grant_access_to_protected_page');
    });

    Route::post('/access_to_protected_page','BanksController@accessToProtectedPage')->name('checkPassword');

});








Route::post('genarate_property_pdf','PropertiesController@expose_genrate_pdf');

Route::get('download_property_pdf/{id}','PropertiesController@expose_download_pdf');
Route::get('outlook/email','OutlookApiController@view');
Route::get('/getCode','OutlookApiController@getCode');
Route::post('outlook/email','OutlookApiController@getMailsByCategory');

Route::get('/get-token','FinApiController@authenticate');
Route::get('/get-all-banks','FinApiController@getAllBanks');

Route::get('/outlook/get-token','OutlookApiController@redirectToAuthFlow');

Route::post('add_new_city', 'PropertiesController@addNewCity')->name('add_new_city');
Route::get('/property/create','PropertiesController@guestcreateproperties')->name('guestcreateproperties');
Route::post('guestcreatepropertiespost', 'PropertiesController@guestcreatepropertiespost')->name('guestcreatepropertiespost');
Route::get('/searchcity', 'PropertiesController@searchcity')->name('searchcity');




Route::get('/store-property/long-lat', 'BanksController@getPropertyGeoCodeAndStore');
Route::get('/store-bank/long-lat', 'BanksController@getBankGeoCodeAndStore');


Route::post('/properties/add-default-payer', 'PropertiesController@addPropertyDefaultPayer')->middleware('auth')->name('add_default_payer');
Route::get('/properties/get-default-payer/{property_id}', 'PropertiesController@getDefaultPayer')->middleware('auth')->name('get_default_payer');
Route::get('/properties/delete-default-payer/{id}', 'PropertiesController@deleteDefaultPayer')->name('delete_default_payer');
Route::post('property/update/default-payer/{id}/','PropertiesController@update_default_payer_by_field');

Route::post('/properties/add-property-deal', 'PropertiesController@addPropertyDeal')->middleware('auth')->name('add_property_deal');
Route::get('/properties/get-property-deals/{property_id}', 'PropertiesController@getPropertyDeals')->middleware('auth')->name('get_property_deals');
Route::post('property/update/deal/{id}/','PropertiesController@update_deal_by_field');
Route::get('/properties/delete-property-deals/{id}', 'PropertiesController@deletePropertyDeal')->name('delete_property_deal');
Route::get('/get-pending-deals/{id}', 'PropertiesController@getPendingDeals')->name('get_pending_deals');
Route::get('/get-not-release-deals/{id}', 'PropertiesController@getNotReleaseDeals')->name('get_not_release_deals');


// Admin route
Route::group(array('middleware' => ['auth', 'admin']), function(){
    Route::get('/module_permission', 'ModulePermissionController@index');
    Route::get('/module_permission/get_user_permission', 'ModulePermissionController@getUserPermission');
    Route::post('module_permission/update', 'ModulePermissionController@update');
});
Route::post('/properties/add-contract', 'PropertiesController@addContract')->middleware('auth')->name('add_contract');
Route::get('/properties/get-contract-by-id/{id}', ['as'=>'getContractById', 'uses' => 'PropertiesController@getContractById'])->middleware('auth');
Route::get('/properties/get-contract/{property_id}', 'PropertiesController@getContract')->middleware('auth')->name('get_contract');
Route::post('property/update/contract/{id}/','PropertiesController@update_contract_by_field')->middleware('auth');
Route::get('/properties/delete-contract/{id}', 'PropertiesController@deleteContract')->name('delete_contract')->middleware('auth');
Route::post('property/contract-mark-as-notrelease', 'PropertiesController@contractMarkAsNotReleased')->name('contract_mark_as_notrelease')->middleware('auth');
Route::post('property/contract-mark-as-notrelease-am', 'PropertiesController@contractMarkAsNotReleasedAm')->name('contract_mark_as_notrelease_am')->middleware('auth');
Route::post('property/contract-mark-as-pending', 'PropertiesController@contractMarkAsPending')->name('contract_mark_as_pending')->middleware('auth');
Route::post('property/contractreleaseprocedure', 'PropertiesController@contractreleaseprocedure')->middleware('auth');;
Route::get('/get-pending-contracts/{id}', 'PropertiesController@getPendingContracts')->name('get_pending_contracts')->middleware('auth');;
Route::get('/get-not-release-contracts/{id}', 'PropertiesController@getNotReleaseContracts')->name('get_not_release_contracts')->middleware('auth');
Route::get('/get-not-release-am-contracts/{id}', 'PropertiesController@getNotReleaseAMContracts')->name('get_not_release_am_contracts')->middleware('auth');
Route::get('/get-release-contracts/{id}', 'PropertiesController@getReleaseContracts')->name('get_release_contracts')->middleware('auth');
Route::get('/get-old-contracts/{id}', 'PropertiesController@getOldContracts')->name('get_old_contracts')->middleware('auth');
Route::get('/properties/get-contract-logs/{property_id}', 'PropertiesController@getContractLogs')->name('get_contract_logs')->middleware('auth');

Route::get('/am-dashboard/get-open-invoice/', 'HomeController@getOpenInvoice')->name('amdashboard_get_openinvoice')->middleware('auth');
Route::get('/get_angebot_button', 'PropertiesController@get_angebot_button')->name('get_angebot_button')->middleware('auth');


Route::group(array('middleware' => ['auth']), function(){
    Route::post('property/add-property-insurance-title/{property_id}', 'PropertiesController@addPropertyInsuranceTitle')->name('add_property_insurance_title');
    Route::post('property/add-property-insurance_detail/{id?}', 'PropertiesController@addPropertyInsuranceDetail')->name('add_property_insurance_detail');
    Route::get('property/get-property-insurance-detail/{tab_id}', 'PropertiesController@getPropertyInsuranceDetail')->name('get_property_insurance_detail');

    Route::get('property/get-property-insurance-detail-comment/{id}/{limit?}', 'PropertiesController@getPropertyInsuranceDetailComment')->name('get_property_insurance_detail_comment');
    Route::post('/property/add-property-insurance-comment', 'PropertiesController@addPropertyInsuranceComment')->name('add_property_insurance_comment');
    Route::get('/property/delete-insurance-detail-comment/{id}', 'PropertiesController@deleteInsuranceDetailComment')->name('delete_insurance_detail_comment');

    Route::post('property/update-property-insurance-detail-by-field/{id}', 'PropertiesController@updatePropertyInsuranceDetailByField')->name('update_property_insurance_detail_by_field');
    Route::get('property/get-property-insurance-detail-by-id/{id}', 'PropertiesController@getPropertyInsuranceDetailById')->name('get_property_insurance_detail_by_id');
    Route::get('property/delete-property-insurance-detail/{id}', 'PropertiesController@deletePropertyInsuranceDetail')->name('delete_property_insurance_detail');
    Route::post('property/update-property-insurance-status/{id}', 'PropertiesController@updatePropertyInsuranceStatus')->name('update_property_insurance_status');
    Route::get('property/get-property-insurance-detail-log/{property_id}', 'PropertiesController@getPropertyInsuranceDetailLog')->name('get_property_insurance_detail_log');
    Route::get('property/delete-property-insurance-title/{id}', 'PropertiesController@deletePropertyInsuranceTitle')->name('delete_property_insurance_title');

//    Route::post('property/import-rent-paid/{property_id}', 'PropertiesController@importRentPaid')->name('import_rent_paid');
    Route::get('property/get-rent-paid-data/{property_id}', 'PropertiesController@getRentPaidData')->name('get_rent_paid_data');
    Route::get('property/get-rent-paid-monthwise-data/{id}', 'PropertiesController@getRentPaidMonthwiseData')->name('get_rent_paid_monthwise_data');
    Route::get('property/delete-rent-paid-data/{property_id}', 'PropertiesController@deleteRentPaidData')->name('delete_rent_paid_data');

    Route::post('property/vacant_uploading_statuse/{vacant_id}', 'PropertiesController@vacantUploadingStatuse')->name('vacant_uploading_statuse');
    Route::post('property/add-rental-activity-comment/{vacant_id}', 'PropertiesController@addRentalActivityComment')->name('add_rental_activity_comment');
    Route::get('property/get-rental-activity-comment', 'PropertiesController@getRentalActivityComment')->name('get_rental_activity_comment');
    Route::get('getSumOfPropertyRentByMonth', 'PropertiesController@getSumOfPropertyRentByMonth');
    Route::delete('file-upload-data/delete/{id}', 'PropertiesController@deletePropertyRentPaid');

});



Route::group(array('middleware' => ['auth']), function(){
    Route::prefix('/properties/{property_id}')->group(function () {
        Route::get('/project/create', 'PropertyProjectController@create');
        Route::post('/project', 'PropertyProjectController@store');
        Route::get('/project/edit/{id}', 'PropertyProjectController@edit');
        Route::post('/project/update/{id}', 'PropertyProjectController@update');
        Route::get('/project/delete/{id}', 'PropertyProjectController@delete');

        /****************************Project Emails***************************************/


        Route::get('/project/{id}/email/type/{type}', 'PropertyProjectController@getEmails');



    });
});

Route::get('autocompleteByName', 'CompanyController@autocompleteByName')->name('autocompleteByName');
Route::get('autocompleteSearchUserByName', 'UserController@searchUserByName')->name('autocompleteSearchUserByName');

Route::group(array('middleware' => ['auth']), function(){
    Route::get('/get-banks', 'BanksController@getAll');
    Route::get('/export_bank_data/{id}', 'BanksController@export_bank_data')->name('export_bank_data');
    Route::get('/bank-financing-offer', 'BankFinancingOffersController@index');
    Route::post('/bank-financing-offer', 'BankFinancingOffersController@store');
    Route::get('/bank-financing-offer/{id}', 'BankFinancingOffersController@edit');
    Route::post('/bank-financing-offer/update/{id}', 'BankFinancingOffersController@update');
    Route::get('/bank-financing-offer/delete/{id}', 'BankFinancingOffersController@delete');

});
Route::post('/bank-financing-offer-import', 'PropertiesController@bankFinancingOfferImport');
Route::get('/bank-financing-view', function (){
    return view('excel-file-upload');

});
Route::post('/property/import-rent-paid/{property_id}', 'PropertiesController@excelImportMieterliste')->name('import_rent_paid');
Route::get('/property/get-recommended-table/{property_id}', 'PropertiesController@getRecommendedTable')->name('get_recommended_table');
Route::get('/property/get-get-kosten-umbau/{property_id}/{tenant_id}', 'PropertiesController@getKostenUmbau')->name('get_kosten_umbau');

/*------------New optimize dashboard routes-------------------*/
Route::group(['prefix' => '/dashboard-optimize', 'middleware' => ['auth']], function(){
    Route::get('/', 'HomeController@optimizeDashboard')->name('optimize_dashboard');
    Route::get('/get-counter', 'HomeController@getDashboardCounter')->name('get_dashboard_counter');
    Route::get('/get-counter1', 'HomeController@getDashboardCounter1')->name('get_dashboard_counter1');
    Route::get('/get-contract-release', 'HomeController@getContractRelease')->name('get_contract_release');
    Route::get('/get-approval-building-insurance/{type}/{id?}', 'HomeController@getApprovalBuildingInsurance')->name('get_approval_building_insurance');
    Route::get('/get_property_insurance_data/{id?}', 'HomeController@getPropertyInsuranceData')->name('get_property_insurance_data');
    Route::get('/get_property_management/{id?}', 'HomeController@getPropertyManagement')->name('get_property_management');
    Route::get('/get-monthyearmanager', ['as'=>'get_monthyearmanager', 'uses' => 'HomeController@getMonthYearManager']);
    Route::get('/getloi/{month}', ['as'=>'getloi', 'uses' => 'HomeController@getLoi']);
    Route::get('/get-funding-request', ['as'=>'get_funding_request', 'uses' => 'HomeController@getFundingRequest']);
    Route::get('/get-rental-overview', ['as'=>'get_rental_overview', 'uses' => 'HomeController@getRentalOverview']);
    Route::get('/get-default-payers', ['as'=>'get_default_payers', 'uses' => 'HomeController@getDefaultPayers']);
    Route::get('/get-oplist', ['as'=>'get_oplist', 'uses' => 'HomeController@getOplist']);
    Route::get('/get-insurance-data', ['as'=>'get_insurance_data', 'uses' => 'HomeController@getInsuranceData']);
    Route::get('/get-house-management-cost', ['as'=>'get_house_management_cost', 'uses' => 'HomeController@getHouseManagementCost']);
    Route::get('/get-expired-rental-agreement', ['as'=>'get_expired_rental_agreement', 'uses' => 'HomeController@getExpiredRentalAgreement']);
    Route::get('/get-vacancy', ['as'=>'get_vacancy', 'uses' => 'HomeController@getVacancy']);
    Route::post('/vacancy-export', ['as'=>'vacancy_export', 'uses' => 'HomeController@vacancyExport']);

    Route::get('/get-rental-activity-data', ['as'=>'get_rental_activity_data', 'uses' => 'HomeController@getRentalActivityData']);
    Route::get('/expiring-leases', ['as'=>'expiring_leases', 'uses' => 'HomeController@expiringLeases']);
    Route::get('/get-expiring-building-insurance', ['as'=>'get_expiring_building_insurance', 'uses' => 'HomeController@getExpiringBuildingInsurance']);
    Route::get('/get-expiring-liability-insurance', ['as'=>'get_expiring_liability_insurance', 'uses' => 'HomeController@getExpiringLiabilityInsurance']);
    Route::get('/get-vacance-per-object', ['as'=>'get_vacance_per_object', 'uses' => 'HomeController@getVacancePerObject']);
    Route::get('/get-comments', ['as'=>'get_comments', 'uses' => 'HomeController@getComments']);
    Route::get('/sales-portal-evaluation', ['as'=>'sales_portal_evaluation', 'uses' => 'HomeController@salesPortalEvaluation']);
    Route::get('/get-sales-portal-count', ['as'=>'get_sales_portal_count', 'uses' => 'HomeController@getSalesPortalCount']);
    Route::get('/get_barchart_data/{id}', ['as'=>'get_barchart_data', 'uses' => 'HomeController@getBarchartData']);
    Route::get('/get-standard-land-value', ['as'=>'get_standard_land_value', 'uses' => 'HomeController@getStandardLandValue']);
    Route::get('/get-bka', ['as'=>'get_bka', 'uses' => 'HomeController@getBKA']);
    Route::get('/getemailTemplateAndFinance', ['as'=>'getemailTemplateAndFinance', 'uses' => 'PropertiesController@getEmailTemplateAndFinanceOffers']);
    Route::get('/get-property-mail', ['as'=>'get_property_mail', 'uses' => 'HomeController@getPropertyMail']);
    Route::get('/mail-mark-as-done/{id}', ['as'=>'mail_mark_as_done', 'uses' => 'HomeController@mailMarkAsDone']);
    Route::post('/mail-forward-to', ['as'=>'mail_forward_to', 'uses' => 'HomeController@mailForwardTo']);
    Route::get('/get-liquiplanung', ['as'=>'get_liquiplanung', 'uses' => 'HomeController@getLiquiplanung']);
    Route::get('/get-liquiplanung-excel', ['as'=>'get_liquiplanung_excel', 'uses' => 'HomeController@getLiquiplanungExcel']);
    Route::get('/get-liquiplanung-1', ['as'=>'get_liquiplanung_1', 'uses' => 'HomeController@getLiquiplanung1']);
    Route::get('/get-liquiplanung-1-data', ['as'=>'get_liquiplanung_1_data', 'uses' => 'HomeController@getLiquiplanung1Data']);
    Route::get('/get-liquiplanung-export', ['as'=>'get-liquiplanung-export', 'uses' => 'HomeController@getExportLiquiplanungData']);
    Route::get('/get-utility-prices', ['as'=>'get_utility_prices', 'uses' => 'HomeController@getUtilityPrices']);

    Route::get('/am', 'HomeController@optimizeDashboardAM')->name('optimize_dashboard_am');
});


Route::group(array('middleware' => ['auth']), function(){
    Route::get('/get-properties', 'PropertiesController@getProperty');
    Route::post('/keine-opos-update', 'PropertiesController@updateCheckboxKeinOpos');



    //Route::get('/show-accounts', 'TinkLinkBankApiController@getAccounts');
    Route::get('/get-auth-token', 'TinkLinkBankApiController@getAccessToken');
//Route::get('/show-accounts', 'TinkLinkBankApiController@getAccounts');
//    Route::get('/show-accounts', 'BankAccountUploadController@showBankData');
    Route::post('/upload-bank-account-file', 'BankAccountUploadController@BankFileUpload');
    Route::post('/update-property-account', 'BankAccountUploadController@updatePropertyInAccount');
    Route::post('/delete-account', 'BankAccountUploadController@deleteBankAccount');
    Route::get('/account/transaction/create', 'BankAccountUploadController@create');
    Route::post('/account/transaction/store', 'BankAccountUploadController@store');

});
/*------------New optimize property routes - start -------------------*/
Route::group(['prefix' => '/property', 'middleware' => ['auth']], function(){
    Route::get('/{id}', 'PropertyController@show')->name('show_property');
    Route::post('/property_upload_pdf', 'PropertyController@property_upload_pdf')->name('property_upload_pdf');
    Route::post('/save_gsheet', 'PropertyController@saveGSheet')->name('property_save_gsheet');
});
/*------------New optimize property routes- end -------------------*/

//Route::get('/show-accounts', 'TinkLinkBankApiController@getAccounts');
Route::get('/get-auth-token', 'TinkLinkBankApiController@getAccessToken');
//Route::get('/show-accounts', 'TinkLinkBankApiController@getAccounts');
Route::post('/upload-bank-account-file', 'BankAccountUploadController@BankFileUpload');
Route::post('/update-property-account', 'BankAccountUploadController@updatePropertyInAccount');
Route::post('/delete-account', 'BankAccountUploadController@deleteBankAccount');
Route::get('/account/transaction/create', 'BankAccountUploadController@create');
Route::post('/account/transaction/store', 'BankAccountUploadController@store');
Route::get('/get-selection-properties', 'DashboardController@getSelectionProperties')->name('get_selection_properties');
Route::post('tenant-master/update/{id}','PropertiesController@update_tenant_master')->name('update_tenant_master');
Route::post('tenant-master/save/{id}/{item_id?}','PropertiesController@saveTenantMaster')->name('save_tenant_master');
Route::get('tenant-master/iframe/{id}','PropertiesController@iframeTenantMaster')->name('iframe_tenant_master');

Route::get('/get-gdrive',function (){
    $files = Storage::cloud()->listContents('1jmEub4s6hqUhgpXVN3UTODcfmKhAv768/12_8xSXgKaMGXAyo_H2myB6dgdom9Hpar/1co2JroRKrfCO_rZPTYG6tbq0UdbPhMWY', false);
    foreach ($files as $file )
    {
      $rawData = Storage::cloud()->get( $file['path']);
      $array = Excel::import(new BankDataImport,public_path('/Saldenübersicht.csv'));
      $fileName  = $file['name'];
//        return response($rawData, 200)
//            ->header('ContentType', $file['mimetype'])
//            ->header('Content-Disposition', "attachment; filename=$fileName");

    }
//    $user = \App\User::find(2);
//    Auth::login($user);
////    return redirect(url('/cars/add'));

});

Route::group(['prefix' => '/property-comment', 'middleware' => ['auth']], function(){
    Route::get('/get', 'HomeController@getPropertyComment')->name('get_property_comment');
    Route::post('/add', 'HomeController@addPropertyComment')->name('add_property_comment');
    Route::get('/delete/{id}', 'HomeController@deletePropertyComment')->name('delete_property_comment');
});


//Route::get('/send-custom-email','AddressesJobController@sendCustomEmails');

Route::group(['prefix' => '/properties', 'middleware' => ['auth']], function(){
    Route::get('/ao-cost/get/{property_id}', 'PropertiesController@getAoCost')->name('get_ao_cost');
    Route::post('/ao-cost/add/{property_id}', 'PropertiesController@addAoCost')->name('add_ao_cost');
    Route::get('/ao-cost/delete/{id}', 'PropertiesController@deleteAoCost')->name('delete_ao_cost');
});

Route::get('/delete-log', 'PropertiesController@deleteLog')->name('delete_log')->middleware('auth');
Route::get('/get-tenancy-item-detail', 'PropertiesController@getTenancyItemDetail')->name('get_tenancy_item_detail')->middleware('auth');
Route::post('/update-liquiplanung-data/{property_id}','PropertiesController@updateLiquiplanungData')->name('update_liquiplanung_data');
Route::get('/get-am-log/{property_id}','PropertiesController@getAmLog')->name('get_am_log');
Route::get('/get-open-angebote','HomeController@getOpenAngebote')->name('getOpenAngebote')->middleware('auth');

Route::post('/update-property-custom-field/{property_id}','PropertiesController@updatePropertyCustomField')->name('update_property_custom_field')->middleware('auth');

Route::group(['prefix' => '/property-permission', 'middleware' => ['auth']], function(){
    Route::get('/','PropertyPermissionController@index');
    Route::post('/store','PropertyPermissionController@store');
});

Route::post('/get-all-users','UserController@gtAllUsers')->name('get_all_users')->middleware('auth');
Route::get('/get-released-angebote','PropertiesController@getReleasedAngebote')->name('getReleasedAngebote')->middleware('auth');

Route::group(['prefix' => '/property-invoice', 'middleware' => ['auth']], function(){
    Route::post('/invoice-release-am','InvoiceController@invoiceReleaseAM')->name('invoice_release_am');
    Route::post('/invoice-release-hv','InvoiceController@invoiceReleaseHV')->name('invoice_release_hv');
    Route::post('/invoice-release-user','InvoiceController@invoiceReleaseUser')->name('invoice_release_user');
    Route::post('/invoice-release-falk','InvoiceController@invoiceReleaseFalk')->name('invoice_release_falk');
    Route::get('/get-invoice-for-am/{status}','InvoiceController@getInvoiceForAM')->name('get_invoice_for_am');
    Route::get('/get-invoice-for-hv/{status}','InvoiceController@getInvoiceForHV')->name('get_invoice_for_hv');
});


Route::get('/bank-transaction','BankTransactionController@index');
Route::get('/accounts','AccountController@index');
Route::post('/accounts-import', 'BankTransactionController@AccountsImport');
Route::get('/bank-transaction/{id}','BankTransactionController@getAccountBankTransactions');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
