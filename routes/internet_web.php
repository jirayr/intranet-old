<?php


Route::get('rashid', 'HomeController@rashid');
Route::get('phpinfo', function (){

    phpinfo();
});

Route::get('/clear_cache', function() { 
    $exitCode = Artisan::call('view:clear');
    return 'DONE'; //Return anything
});

Route::get('/office', 'HomeController@trello');

Auth::routes();

 
/////////// Webmail Routes //////////// 
Route::get('/webmail','Webmail@index');
Route::get('/webmail-read/{id}','Webmail@read');
Route::post('/webmail-reply','Webmail@reply');
Route::get('/webmail-delete/{id}','Webmail@delete'); 

Route::get('/webmail-delete-sent/{id}','Webmail@delete_sent');
Route::get('/webmail-delete-spam/{id}','Webmail@delete_spam');

Route::get('/webmail-trash','Webmail@trash'); 
Route::get('/webmail-spam','Webmail@spam'); 
Route::get('/webmail-sent','Webmail@sent');  

Route::get('/webmail-trash_read/{id}','Webmail@trash_read');
Route::get('/webmail-sent_read/{id}','Webmail@sent_read');
Route::get('/webmail-spam_read/{id}','Webmail@spam_read');

Route::get('/webmail-permanent-delete/{id}','Webmail@permanent_delete'); 
  
Route::get('/webmail-empty-trash','Webmail@empty_trash');  

Route::get('/webmail-login',function(){
     return view('webmail.login');
});
Route::post('/webmail-login-post','Webmail@webmail_login_post');

////////////////// End Webmail Routes /////////////////////

Route::get('getPropertyInfo/{id}', 'AdsController@getPropertyInfo');


Route::get('/email_template','HomeController@email_template')->name('email_template');  

Route::post('/email_template_save','HomeController@email_template_save')->name('email_template_save');  
Route::post('/send_email_template','HomeController@send_email_template')->name('send_email_template');  

Route::post('/email_template2_save','HomeController@email_template2_save')->name('email_template2_save');  
Route::post('/send_email_template2','HomeController@send_email_template2')->name('send_email_template2'); 




Route::get('/signin', 'AuthController@signin');
Route::get('/authorize', 'AuthController@gettoken');
Route::get('/mail', 'OutlookController@mail')->name('mail');
//Route::get('/calendar', 'OutlookController@calendar')->name('calendar');
Route::get('/contacts', 'OutlookController@contacts')->name('contacts');

Route::get('/read_message/{id}', 'OutlookController@read_message');
Route::get('/reply_message/{id}', 'OutlookController@reply_message');

Route::get('/rashid', 'HomeController@rashid');
Route::get('/set_seller_data', 'PropertiesController@set_seller_data');
Route::get('/panga', 'PropertiesController@panga');

Route::get('/delete-property-single-image/{id}/{image}', 'HomeController@delete_property_single_image');

 
Route::group(['middleware' => ['web']], function () {


########## Ads Routes
Route::post('create_new_post', 'AdsController@store')->name('save_new_post');
Route::post('get-state-by-country', ['as'=>'get_state_by_country', 'uses' => 'AdsController@getStateByCountry']);
Route::post('get-city-by-state', ['as'=>'get_city_by_state', 'uses' => 'AdsController@getCityByState']);


Route::post('save_vacant_data', 'AdsController@saveVacantdata')->name('save_vacant_data');
Route::get('remove_vacant_media', 'AdsController@removeVacantMedia')->name('remove_vacant_media');

 
####### new dashborad
Route::get('/dashboard', 'HomeController@dashborad')->middleware('auth')->name('dashboard');

Route::get('/updatepropertyuser', 'PropertiesController@updatepropertyuser')->middleware('auth')->name('updatepropertyuser');
Route::get('/removebank', 'PropertiesController@removebank')->middleware('auth')->name('removebank');


Route::get('/removeextrarow', 'PropertiesController@removeextrarow')->middleware('auth')->name('removeextrarow');

Route::get('/searchcity', 'PropertiesController@searchcity')->middleware('auth')->name('searchcity');
Route::get('/searchbank', 'PropertiesController@searchbank')->middleware('auth')->name('searchbank');






Route::get('getmanagerproperty', ['as'=>'getmanagerproperty', 'uses' => 'HomeController@getmanagerproperty'])->middleware('auth');


Route::get('getPropertyInsurance', ['as'=>'getPropertyInsurance', 'uses' => 'HomeController@getPropertyInsurance'])->middleware('auth');
Route::get('getInsurancePropertyList', ['as'=>'getInsurancePropertyList', 'uses' => 'HomeController@getInsurancePropertyList'])->middleware('auth');



Route::get('getassetmanagerproperty', ['as'=>'getassetmanagerproperty', 'uses' => 'HomeController@getassetmanagerproperty'])->middleware('auth');

Route::get('mailchimp', ['as'=>'mailchimp', 'uses' => 'HomeController@mailchimp'])->middleware('auth');
Route::get('monthyearmanager', ['as'=>'monthyearmanager', 'uses' => 'HomeController@monthYearManager'])->middleware('auth');
Route::get('monthyearassetmanager', ['as'=>'monthyearassetmanager', 'uses' => 'HomeController@monthYearAssetmanager'])->middleware('auth');

Route::post('delete-pdf', 'PropertiesController@deletePdf')->middleware('auth')->name('delete_property_pdf');
Route::post('add_new_city', 'PropertiesController@addNewCity')->middleware('auth')->name('add_new_city');
Route::post('add_new_bank', 'PropertiesController@addNewBank')->middleware('auth')->name('add_new_bank');


Route::get('export_properties', ['as'=>'export_properties', 'uses' => 'PropertiesController@export_properties'])->middleware('auth');





//Heat Map
Route::get('/heatmap', 'HomeController@heatmap')->name('heatmap');

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/', 'DashboardController')->middleware('auth');

Route::get('/getstatustable', 'DashboardController@getstatustable')->middleware('auth')->name('getstatustable');

Route::get('/assetmanagement', 'AssetManagementController@index')->middleware('auth')->name('assetmanagement');
Route::get('/assetmanagement/chart', 'ChartController@index')->middleware('auth')->name('assetmanagement.chart');
Route::post('/assetmanagement', 'AssetManagementController@index')->name('assetmanagementfilter');
Route::resource('/users', 'UserController')->middleware('auth');
Route::resource('/properties', 'PropertiesController')->middleware('auth');
Route::resource('/calendar', 'CalendarController')->middleware('auth');

Route::get('/property/{id}/comments', 'PropertyCommentsController@show')->middleware('auth')->name('propertycomments.show');
Route::post('/property/create-comment','PropertyCommentsController@create')->name('create_propertycomment');
Route::get('/property/{id}/edit-comment','PropertyCommentsController@edit')->name('edit_propertycomment');
Route::post('/property/update-comment','PropertyCommentsController@update_comment')->name('update_propertycomment');
Route::get('/property/comments', 'PropertyCommentsController@_show')->middleware('auth')->name('propertycomments._show');


Route::post('/property/edit_comment_iframe/{id}','PropertyCommentsController@edit_comment_show_iframe')->name('edit_comment_show_iframe');


Route::post('/verkauf-item/update/{id}/{type}','PropertiesController@update_verkauf_item')->name('update_verkauf_item');
Route::post('/verkauf/update/{id}','PropertiesController@update_verkauf')->name('update_verkauf');

Route::get('/property/set_as_standard/{orignal_prop}/{pid}', 'PropertiesController@set_as_standard'); 

Route::post('/einkauf-item/update/{id}/{type}','PropertiesController@update_einkauf_item')->name('update_einkauf_item');
Route::post('/einkauf/update/{id}','PropertiesController@update_einkauf')->name('update_einkauf');



Route::post('property/comment-date', array(
    'as' => 'update.comment.date',
    'uses' => 'PropertyCommentsController@updateCommentDate'));

Route::post('/properties/{id}/update_property','PropertiesController@update_property')->name('update_property');
Route::get('/property-comparison','PropertiesController@comparsion')->name('property_comparison');
Route::post('/property-comparison','PropertiesController@comparsion')->name('property_comparison');
Route::post('/calendar/create_calendar','CalendarController@create_calendar')->name('create_calendar');
Route::post('/calendar/edit_event','CalendarController@edit_event')->name('edit_event');
Route::get('/users/{id}/edit-profile','UserController@getProfile')->name('edit-profile');
Route::post('/users/{id}/edit-profile','UserController@postProfile')->name('edit-profile');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/change-password', 'UserController@getChangePassword')->name('change-password');
Route::post('/change-password', 'UserController@postChangePassword')->name('change-password');

Route::post('/change-property-status', 'PropertiesController@changeStatus')->name('properties.change_status');
Route::get('/user-activities', 'UserController@showUserActivities')->middleware('auth')->name('user-activities');

############ Mietflächen save ############
Route::post('/mietflachen', 'PropertiesController@mietflachen')->name('properties.mietflachen');
 


// Google Map
Route::resource('/google-map', 'GoogleMapController')->middleware('auth');

//Notifications
Route::post('/new-notification', 'NotificationsController@newNotification');
Route::post('/seen-notification', 'NotificationsController@seenAllNotification');
Route::get('/get-new-notification', 'NotificationsController@getNewNotification');

// Update property by field
Route::post('property/update/{id}/','PropertiesController@update_property_by_field');
Route::post('property/create_direct','PropertiesController@create_property_direct')->name('create_property_direct');
Route::post('property/update_direct','PropertiesController@update_property_direct')->name('update_property_direct');


// Update property by Ist, Soll

Route::post('property/update_property_ist_soll/{tab}/{bank_id}','PropertiesController@update_property_ist_soll');



Route::post('property/updateStatusInHold', 'PropertiesController@changeStatusInHold');
Route::post('property/update/schl/{id}/','PropertiesController@update_property_schl_by_field');
Route::post('property/update/bank/{id}/{bank_id}','PropertiesController@update_property_bank');


Route::post('property/save-shcl-banks/{id}/', 'PropertiesController@saveSchlBanks');
Route::post('property/update/planung-berechnung-wohnen/{id}/','PropertiesController@update_property_planung_by_field');
Route::post('property/update/clever-fit/{id}/','PropertiesController@update_property_clever_fit_by_field');

Route::post('property/create_property_insurance','PropertiesController@create_property_insurance')->name('create_property_insurance');
Route::post('property/update/insurance/{id}/','PropertiesController@update_property_insurance_by_field');
Route::post('property/delete_property_insurance','PropertiesController@delete_property_insurance')->name('delete_property_insurance');

Route::post('property/create_maintenance','PropertiesController@create_maintenance')->name('create_maintenance');
Route::post('property/update/maintenance/{id}/','PropertiesController@update_maintenance_by_field');
Route::post('property/delete_maintenance','PropertiesController@delete_maintenance')->name('delete_maintenance');

Route::post('property/change_date','TenancyScheduleController@change_date')->name('change_date');

Route::post('property/delete_tenant','TenancyScheduleController@delete_tenant')->name('delete_tenant');


Route::post('property/create_investation','PropertiesController@create_investation')->name('create_investation');
Route::post('property/update/investation/{id}/','PropertiesController@update_investation_by_field');
Route::post('property/delete_investation','PropertiesController@delete_investation')->name('delete_investation');


Route::post('property/create_service_provider','PropertiesController@create_service_provider')->name('create_service_provider');
Route::post('property/update/serviceprovider/{id}/','PropertiesController@update_property_service_provider_by_field');
Route::post('property/delete_service_provider','PropertiesController@delete_service_provider')->name('delete_service_provider');

Route::get('forecasts', 'ForecastsController@index')->name('forecast.index');
Route::post('forecast/create', 'ForecastsController@store');
Route::post('forecast/update/{id}', 'ForecastsController@update_by_field');
Route::post('forecast/delete/{id}', 'ForecastsController@delete');
Route::post('forecast/{id}/forecast-kind/create', 'ForecastKindsController@store');
Route::post('forecast-kind/update/{id}', 'ForecastKindsController@update_by_field');


//Tenancy Schedule
Route::group(['prefix' => 'tenancy-schedules', 'middleware' => 'auth'], function () {
    Route::get('/', 'TenancyScheduleController@index')->name('tenancy-schedules.index');
    Route::post('/create', 'TenancyScheduleController@create')->name('tenancy-schedules.create');
    Route::post('/update/{id}', 'TenancyScheduleController@update_by_field')->name('tenancy-schedules.update-by-field');
    Route::post('/delete/{id}', 'TenancyScheduleController@delete')->name('tenancy-schedules.delete');
    Route::post('/delete/{id}/item/{itemId}', 'TenancyScheduleController@deleteItem')->name('tenancy-schedules.item.delete');
});

Route::group(['prefix' => 'masterliste', 'middleware' => 'auth'], function () {
    Route::get('/', 'MasterlisteController@index')->name('masterliste.index');
    Route::get('/listing', 'MasterlisteController@show')->name('masterliste.listing');
});

Route::group(['prefix' => 'tenancy-schedule-items', 'middleware' => 'auth'], function () {
    Route::post('/create', 'TenancyScheduleController@createItem')->name('tenancy-schedules.create-item');
    Route::post('/update/{id}', 'TenancyScheduleController@update_item_by_field')->name('tenancy-schedules.update-item-by-field');
    Route::post('/delete/{id}', 'TenancyScheduleController@deleteItem')->name('tenancy-schedules.delete');

});

//Update tenants by field
Route::post('tenant/update/{id}', 'TenantsController@updateTenantByField')->name('tenants.update');
Route::post('tenant/create', 'TenantsController@createTenant')->name('tenants.store');

// Banks
Route::group(['prefix' => 'banks', 'middleware' => 'auth'], function () {
    Route::get('/', 'BanksController@index')->name('banks');;
    Route::get('/add', 'BanksController@showAdd')->name('bank-add');
    Route::get('/edit/{id}', 'BanksController@showEdit')->name('bank-edit');
    Route::get('/view/{id}', 'BanksController@view')->name('bank-view');
    Route::post('/add', 'BanksController@add');
    Route::post('/direct_create', 'BanksController@direct_create')->name('bank-direct-create');;
    Route::post('/update/{id}', 'BanksController@update');
    Route::post('/updatefield/{id}', 'BanksController@update_banks_by_field');
    Route::post('/delete', 'BanksController@delete')->name('bank-delete');
});
  
Route::post('property/select_bank/{id}/','PropertiesController@update_property_bank_fields')->name('properties.select_bank');

// Verkauf tab add new row
Route::post('property/verkauf_new_row/','PropertiesController@verkauf_new_row');
Route::post('property/update_verkauf_tab_row/{row_id}','PropertiesController@update_verkauf_tab_row');
Route::get('property/delete_verkauf_tab_row/{row_id}','PropertiesController@delete_verkauf_tab_row');
Route::post('property/update_verkauf_tab_row_others/','PropertiesController@update_verkauf_tab_row_others'); 
Route::post('property/update_row_11_12_name/','PropertiesController@update_row_11_12_name');
Route::get('property/ajax_verkauf_tab_load/{property_id}/{g59}','PropertiesController@ajax_verkauf_tab_load');
Route::post('property/update_month_verkauf_tab/{p_id}','PropertiesController@update_month_verkauf_tab'); 


// Charts
Route::get('charts','PropertiesController@showCharts')->name('show_charts');

//Update masterliste by field
Route::post('masterliste/update/{id}', 'MasterlisteController@updateMasterlisteByField')->name('masterliste.update');
Route::post('masterliste/create', 'MasterlisteController@createMasterliste')->name('masterliste.store');
Route::post('masterliste/delete', 'MasterlisteController@delete')->name('masterliste.delete');

Route::get('/sync_all_masterliste','MasterlisteController@sync_all_masterliste')->name('masterliste.sync');

Route::get('feedback','FeedbackController@index')->middleware('auth')->name('feedback');
Route::post('feedback/add','FeedbackController@add')->middleware('auth')->name('feedback.add');


 
//banks iframe
Route::get('banksIframe/{id}/{bank_id}/{tab}','PropertiesController@banksIframe');


Route::get('make_clone','BanksController@make_clone');




Route::post('addPropertiesExtra1Row','PropertiesController@addPropertiesExtra1Row')->name('properties.addPropertiesExtra1Row');
Route::post('propertiesExtra1/update/{sheet}/{id}','PropertiesController@updatePropertiesExtra1')->name('properties.updatePropertiesExtra1');

Route::post('property/export-to-excel', 'PropertiesController@export_to_excel');
Route::post('property/export-tenancy-to-excel', 'PropertiesController@export_tenancy_to_excel');

Route::group(['prefix' => 'cron'], function () {
    Route::get('/update-masterliste', 'CronController@update_masterliste')->name('update_masterliste');

});

##### upload pdf
#
Route::post('property_upload_pdf', 'PropertiesController@property_upload_pdf');

Route::post('property/sendmail', 'PropertiesController@sendmail');
Route::post('property/einkaufsendmail', 'PropertiesController@einkaufsendmail');
Route::post('property/einkaufsendmail2', 'PropertiesController@einkaufsendmail2');



});



Route::get('/working', 'UserController@working');
####### new dashborad
Route::post('/api_listing_post', 'HomeController@api_listing_post')->middleware('auth')->name('api_command');
Route::post('/properties/upload_attachment_images', 'HomeController@upload_attachment_images')->middleware('auth');





Route::get('/add_api_contacts', 'HomeController@add_api_contacts')->name('users.add_api_contacts');
Route::post('/post_api_contacts', 'HomeController@post_api_contacts')->name('users.post_api_contacts');

Route::get('/api_contacts_list', 'HomeController@api_contact_list');

Route::get('/delete_user/{id}', 'HomeController@delete_user');
Route::post('/properties/upload_listing_to_api', 'HomeController@upload_listing_to_api');
Route::post('/properties/api_imag_del', 'HomeController@api_imag_del');


Route::get('banks/search', 'BanksController@search')->name('banks.search')->middleware('auth');
Route::get('banks/search_result', 'BanksController@search_result')->name('banks.search_result')->middleware('auth');