<?php

use Illuminate\Http\Request;
use App\Services\Events;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/calendar/create_calendar', function(Request $request) {
    return $request;
});


Route::get('/get/oldest/{status?}', 'ApiController@oldest');
Route::get('/update/{id?}/{status?}', 'ApiController@update');

Route::post('/email-account/create', 'EmailAccountController@store');
Route::get('/email-account/accounts', 'EmailAccountController@getAccounts');
Route::get('/email-account/accounts/account/edit/{id}', 'EmailAccountController@edit');
Route::post('/email-account/accounts/account/update/{id}', 'EmailAccountController@update');
Route::delete('/email-account/accounts/account/delete/{id}', 'EmailAccountController@delete');
Route::get('/email-account/accounts', 'EmailAccountController@getAccounts');
Route::get('/fetch/{id}/type/{typeId}', 'MailBoxController@getEmailsByAccountAndEmailType');
Route::post('/validate-account/', 'MailBoxController@validateAccount');
Route::get('/emails/type/{id}', 'MailBoxController@getAllEmails');
Route::post('/emails/search/', 'MailBoxController@emailSearch');
