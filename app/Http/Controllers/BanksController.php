<?php
namespace App\Http\Controllers;

use App\Models\Properties;
use App\Models\Banks;
use App\Models\PropertySubfields;
use App\Services\FinApiService;
use App\Services\NotificationsService;
use GuzzleHttp\Client;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use App\User;
use App\Services\UserService;
use Auth;
use App\Services\PropertiesService;
use App\email_template2_sending_info;
use App\Models\BankFinancingOffer;
use App\Services\BanksService;
use function PHPSTORM_META\elementType;
use Symfony\Component\HttpKernel\Profiler\Profile;
use Illuminate\Support\MessageBag;
use DB;
ini_set('memory_limit','-1');
ini_set('max_execution_time', 180); //3 minutes


class BanksController extends Controller
{
    protected $finApiService;

    public function __construct(FinApiService $finApiService)
    {
        $this->finApiService = $finApiService;
    }

    public function index() {
        $perPages 	= 25;
        $user_id 	= Auth::id();
        if(Auth::user()->isAdmin()){
            $banks = Banks::where('id','!=',null)->paginate($perPages);
        }else{
            $banks = Banks::where('user_id','=',$user_id)->paginate($perPages);
        }
        return view( 'banks.index', compact('banks') );
    }

    public function export_bank_data($id)
    {
        // echo $id;
        $sendEmails = email_template2_sending_info::where('property_id', $id)->get();
        $banksFinancingOffers = BankFinancingOffer::where('property_id', $id)->get();

        $array = array();
        $array[0][] = "Auto. angefragt";
        $array[0][] = "Bank";
        $array[0][] = "Ort";
        $array[0][] = "Ansprechpartner";
        $array[0][] = "Notizen";
        $array[0][] = "Zinsatz";
        $array[0][] = "Tilgung";
        $array[0][] = "FK-Anteil proz.";
        $array[0][] = "FK-Anteil nominal";
        $array[0][] = "Datum";
        $array[0][] = "Uhrzeit";
        // $array[0][] = "Aktion";

        foreach($sendEmails as $list)
        {
            $a1 = '0,00';
            $a2 = '0,00';
            $a3 = '0,00';
            $a4 = '0,00';
            if($list->interest_rate)
                $a1= show_number($list->interest_rate,2);

            if($list->tilgung)
                $a2= show_number($list->tilgung,2);

            if($list->fk_share_percentage)
                $a3= show_number($list->fk_share_percentage,2);

            if($list->fk_share_nominal)
                $a4= show_number($list->fk_share_nominal,2);

            $bk = $list->getBank($list->email_to_send);
            $bname = "";
            if($bk)
            $bname = $bk->name;

            $array[] = array('Ja',$bname,$list->address,$list->contact_person,$list->notizen,$a1,$a2,$a3,$a4,show_datetime_format($list->created_at,'d.m.Y'),show_datetime_format($list->created_at,'H:i'));
        }


        foreach ($banksFinancingOffers as $key => $banksFinancingOffer) {
            # code...

$bankname = "";
$bankaddress = "";
$bankcontact_name = "";
if(isset($banksFinancingOffer->bank))
{
    $bankname = $banksFinancingOffer->bank->name;
    $bankaddress .= $banksFinancingOffer->bank->address;
    $bankcontact_name .= $banksFinancingOffer->bank->contact_name;
    $bankcontact_name .= $banksFinancingOffer->bank->contact_phone;
    $bankcontact_name .= $banksFinancingOffer->bank->contact_email;
}
$array[] =
array('Nein',$bankname,
$bankaddress,
$bankcontact_name,$banksFinancingOffer->telefonnotiz, show_number($banksFinancingOffer->interest_rate,2), show_number($banksFinancingOffer->tilgung,2),show_number($banksFinancingOffer->fk_share_percentage,2),show_number($banksFinancingOffer->fk_share_nominal,2),show_datetime_format($banksFinancingOffer->created_at,'d.m.Y'),show_datetime_format($banksFinancingOffer->created_at,'H:i'));
        }


        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Bankenanfrage");
        $sheet->fromArray($array);
        $sheet->getDefaultColumnDimension()->setWidth(20);

        $style_array_th_grey = PropertiesService::get_style_array(['bold', 'light-grey']);
        $sheet->getStyle('A1:K1')->applyFromArray($style_array_th_grey);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $file_name = "Bankenanfrage-$id.xlsx";
        header('Content-Disposition: attachment; filename=' . $file_name);
        $writer->save("php://output");
        die;
        // pre($array);



    }

    public function search() {
        $perPages 	= 25;
        $user_id 	= Auth::id();
        $banks = array();
        $type = 'search_result';

        if(isset($_POST['name']) && $_POST['name'] != '')
        {
            $banks = Banks::where('name','like','%abc%')->paginate($perPages);
        }
        //if(Auth::user()->isAdmin()){

        //}else{
        //$banks = Banks::where('user_id','=',$user_id)->paginate($perPages);
        //}
        return view( 'banks.search', compact('type','banks') );
    }

    public function search_result(Request $request) {
        $perPages 	= 25;
        $user_id 	= Auth::id();
        $banks = array();

        $name = $request->name;
        $banks = Banks::where('name','like','%'.$name.'%')->paginate($perPages);

        //if(Auth::user()->isAdmin()){

        //}else{
        //$banks = Banks::where('user_id','=',$user_id)->paginate($perPages);
        //}
        return view( 'banks.index', compact('banks') );
    }

    public function showAdd(Request $request) {
        // Hack
        $banks = '';
        $type = 'add';

        return view( 'banks.edit', compact( 'type', 'banks' ) );
    }

    public function showEdit($id) {
        $banks = Banks::where('id', '=', $id)->first();
        $type = 'update';
        if( empty( $banks ) )
            return redirect('banks/add');
        return view( 'banks.edit', compact( 'type', 'banks' ) );
    }

    public function view($id) {
        $banks = Banks::where('id', '=', $id)->first();
        $type = 'view';
        if( empty( $banks ) )
            return redirect('banks/index');
        return view( 'banks.view', compact( 'type', 'banks' ) );
    }

    public function add(Request $request) {
        $validator = $this->bankValidation($request);
        $input = $request->all();

        // User creator
        $user_id =  Auth::id();

        // Upload picture
        $picture = '';
        if($request->hasFile('picture')) {
            $file = $request->picture;
            if ( $file->move('upload', $file->getClientOriginalName()) ) {
                $picture = '/upload/' . $file->getClientOriginalName();
            }
        }
        $input['picture'] = $picture;

        if ($bank = BanksService::create($input)) {

        }

        return redirect('banks');
    }


    public function make_clone() {

        $properties = Properties::where('Ist', '!=', 0)->get();
        foreach ($properties as $property){

            Properties::where('id', $property->id)->update([

                'Ist' => 0,
                'Ist_bank_id' => $property->Ist
            ]);
        }
        echo '<pre>';
        print_r($properties);
        echo '</pre>';


        die();


        $property_data = Properties::get();
        foreach($property_data as $data) {
            if($data->bank_ids != '""'){

                //echo 'id'.$data->id.'<br>';

                $bank_ids = json_decode($data->bank_ids) ? json_decode($data->bank_ids) : [];

                foreach($bank_ids as $bank_id) {

                    $rec = json_decode( json_encode( $data ), true );
                    for($x = 0; $x < 2; $x++ ){

                        if($x == 0 ){

                            $rec['soll'] = $bank_id;
                        }

                        elseif($x == 1 ){

                            $rec['Ist'] = $bank_id;
                            $rec['soll'] =0;

                        }
                        unset($rec['id']);

                        Properties::insert($rec);
                        echo "<br>";echo "<br>";echo "<br>";echo "<br>";echo "<br>";echo "<br>";
                    }

                }
            }


        }




    }





    public function direct_create(Request $request) {
        $input = $request->all();

        $property_data = Properties::where('id','=',$input['property_id'])->first();
        $bank_ids = json_decode($property_data->bank_ids) ? json_decode($property_data->bank_ids) : [];

        if(isset($bank_ids[0])){
            $bank_init = Banks::where('id', '=',$bank_ids[0])->first();
            $bank_init['with_real_ek']*=100;
            $bank_init['from_bond']*=100;
            $bank_init['bank_loan']*=100;
            $bank_init['interest_bank_loan']*=100;
            $bank_init['eradication_bank']*=100;
            $bank_init['interest_bond']*=100;
        }else{
            $bank_init =[
                'name'=> 'Bank name',
                'picture'=>'',
                'with_real_ek' => '0',
                'from_bond' => 20,
                'bank_loan' => 80,
                'interest_bank_loan' => 2,
                'eradication_bank' => 4,
                'interest_bond' => 6
            ];
        }

        // Upload picture
        if ($bank = BanksService::create($bank_init)) {

            $property = $property_data;
            $list_fields_percent = [
                'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
            ];
            foreach ($list_fields_percent as $field) {
                if( isset( $property_data[$field] ) )
                    $property_data[$field] *= 100;
            }

            array_push($bank_ids, $bank->id);
            $property_data->bank_ids = json_encode($bank_ids);
            PropertiesService::update($property_data,$property);

            /*
                $Properties_clone = Properties::find($input['property_id']);
                $Properties_New = $Properties_clone->replicate();
                $Properties_New->Ist = $bank->id;
                $Properties_New->for_bank_tab = 'yes';
                $Properties_New->save();

                $Properties_clone = Properties::find($input['property_id']);
                $Properties_New = $Properties_clone->replicate();
                $Properties_New->soll = $bank->id;
                $Properties_New->for_bank_tab = 'yes';
                $Properties_New->save();
            */
            DB::beginTransaction();
            try {
                $original_properties = Properties::find($input['property_id']);



                $Properties_clone = Properties::find($input['property_id']);
                $Properties_New = $Properties_clone->replicate();
                $Properties_New->Ist = $bank->id;
                $Properties_New->properties_bank_id = $bank->id;
                $Properties_New->bank_ids = $original_properties->bank_ids;
                $Properties_New->user_id = Auth::user()->id;
                $Properties_New->for_bank_tab = 'yes';
                $Properties_New->main_property_id = $input['property_id'];
                $Properties_New->save();

                $Properties_clone = Properties::find($input['property_id']);
                $Properties_New = $Properties_clone->replicate();
                $Properties_New->soll = $bank->id;
                $Properties_New->properties_bank_id = $bank->id;
                $Properties_New->bank_ids = $original_properties->bank_ids;
                $Properties_New->main_property_id = $input['property_id'];
                $Properties_New->user_id =Auth::user()->id;
                $Properties_New->for_bank_tab = 'yes';
                $Properties_New->save();
                DB::commit();

            } catch (\Exception $e) {
                DB::rollback();
                die($e->getMessage());
            }




        }
        return  back();
    }

    public function update(Request $request, $id) {
        $bank = Banks::where('id', '=', $id)->first();
        $validator = $this->bankValidation($request);

        $input = $request->all();

        // Upload picture
        $picture = $bank->picture;
        $input['picture'] = $picture;
        if($request->hasFile('picture')) {
            $file = $request->picture;
            if ( $file->move('upload', $file->getClientOriginalName()) ) {
                $picture = '/upload/' . $file->getClientOriginalName();
                $input['picture'] = $picture;
            }
        }

        if ( BanksService::update( $input, $bank ) ) {

        }
        return redirect('banks');
    }


    public function update_banks_by_field(Request $request, $prop_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $bank = Banks::where('id', '=', $prop_id)->first();

        if( empty( $bank ) ) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        // Clone property
        $input = $bank;
        $list_fields_percent = [
            'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek',
            'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank',
            'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
        ];

        foreach ($list_fields_percent as $field) {
            if( isset( $input[$field] ) )
                $input[$field] *= 100;
        }

        // New value
        $new_value = str_replace('.','',$request->value);
        $new_value = str_replace(',','.',$new_value);
        $input[$request->pk] = $new_value;

        if (BanksService::update( $input, $bank )) {
            $response = [
                'success' => true,
                'msg' => ''
            ];
        }

        return response()->json($response);
    }

    protected function bankValidation( $request ) {
        $rules = [
            'picture' => 'image|mimes:jpg,jpeg,png|max:5000',
            'name' => 'required|max:255',
            'with_real_ek' => 'numeric|min:0|max:100',
            'from_bond' => 'numeric|min:0|max:100',
            'bank_loan' => 'numeric|min:0|max:100',
            'interest_bank_loan' => 'numeric|min:0|max:100',
            'eradication_bank' => 'numeric|min:0|max:100',
            'interest_bond' => 'numeric|min:0|max:100',
        ];
        $this->validate( $request , $rules);
    }

    public function delete(Request $request)
    {
        $input = $request->all();
        $bank_id = isset( $input['bank_id'] ) ? $input['bank_id'] : 0;
        $bank = Banks::where('id', '=', $bank_id)->first();
        if( BanksService::delete( $bank ) ) {

        }
        return redirect('banks');
    }

    public function edit_contact_show($id)
    {
        $banks = Banks::where('id', '=', $id)->first();
        $contact = DB::table('bank_contact_details')->where('bank_id', $id)->paginate(20);
        if( empty( $banks ) )
            return redirect('banks/add');
        return view( 'banks.edit_contact_details', compact( 'type', 'banks', 'contact'));

    }

    public function contact_delails_edit(Request $request){

        //dd($request->all());
        $db_array = [
            'user_id'       => Auth::id(),
            'bank_id'       => $request->bank_id,
            'firstName'     => $request->firstName,
            'surname' 	  => $request->surname,
            'contact_name'  => $request->contact_name,
            'contact_phone' => $request->contact_phone,
            'contact_email' => $request->contact_email,
            'address' 	  => $request->address,
            'city' 		  => $request->city,
            'postal_code'   => $request->postal_code,
            'district' 	  => $request->district,
            'fax' 		  => $request->fax
        ];

        $contact = DB::table('bank_contact_details')->where('bank_id', $request->bank_id)->first();

        if($request->contact_id == 0){
            //dd("insert");
            $bank_contact_details = DB::table('bank_contact_details')->insert($db_array);
            return redirect()->back()->with('success', 'Successfully Inserted!');
        }else{
            //dd("update");
            $bank_contact_details = DB::table('bank_contact_details')->where('id', $request->contact_id)->update($db_array);
            return redirect()->back()->with('success', 'Updated Successfully!');
        }

    }

    public function contact_fetch(Request $request){

        $contact = DB::table('bank_contact_details')->where('id', $request->edit_id)->first();

        return \Response::json($contact);

    }

    public function delete_contact_info($id){

        $contact_delete = DB::table('bank_contact_details')->where('id', $id)->delete();

        return redirect()->back()->with('success', 'Successfully Deleted!');

    }

    public function getAllBankAccountsBalance()
    {
        $response = $this->finApiService->getAllBanks();

        return view('banks.bank_accounts', ['accounts' => $response ]);
    }

    public function accessToProtectedPage(Request $request)
    {
        if ($request->password == 'admin123')
        {
            $request->session()->put('granted_protected_page_access', $request->password);
            return redirect('/show-accounts');
        }
        else
        {
            return redirect()->back()->withError('Incorrect Password ');
        }

    }

    public function importBankConnectionView()
    {
        return view('banks.import_bank_connection');
    }

    public function importBankConnection(Request $request)
    {
        return  $this->finApiService->importBankConnection($request)->with('success','Created!');
    }

    public function getAllBankConnections()
    {
        $bankConnections = $this->finApiService->getAllBankConnections();
        return view('banks.bank_connection_list',compact('bankConnections'));
    }

    public function getBank($bankId)
    {
        $bank = $this->finApiService->getBank($bankId);
        return view('banks.bank',compact('bank'));
    }

    public function getAccount(Request $request)
    {
        $account = $this->finApiService->getAccount($request->accountIds);
        return view('banks.account',compact('account'));
    }

    public function radiusSearch(Request $request)
    {
        $property = Properties::where('id',$request->p_id)->first();
        if ($property)
        {
            $globalLongLat = $this->convertAddressToLongLat($property->address);
            $data = Banks::select('*')
                ->selectRaw('( 6371 * acos( cos( radians(?) ) *
                           cos( radians( location_latitude ) )
                           * cos( radians(location_longitude) - radians(?)
                           ) + sin( radians(?) ) *
                           sin( radians( location_latitude ) ) )
                         ) AS distance', [ $globalLongLat['results'][0]['geometry']['location']['lat'], $globalLongLat['results'][0]['geometry']['location']['lng'],  $globalLongLat['results'][0]['geometry']['location']['lat']])
                ->havingRaw("distance < ?", [$request->rad])->get();
            return $data;
        }

    }


    public function convertAddressToLongLat($address)
    {
        $client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),));
        $response = $client->request(
            'GET',
            'https://maps.googleapis.com/maps/api/geocode/json?',
            [
                "query" => [
                    "address" => $address,
                    "key" => config('env.GOOGLEMAP_API_KEY'),
                ],
            ]
        );
        return json_decode($response->getBody(), true);
    }

    public function getPropertyGeoCodeAndStore()
    {
        $properties = Properties::get();
        foreach ($properties as $property)
        {
            $address = '';
            $address = $property->address? $property->address : $property->name_of_property;
            $findProperty = PropertySubfields::where('property_id',$property->id)->first();
            if ($address)
            {
                $globalLongLat = $this->convertAddressToLongLat($address);
                if ($findProperty)
                {
                    $findProperty->update([
                        'location_latitude' => $globalLongLat['results']?$globalLongLat['results'][0]['geometry']['location']['lat']:'',
                        'location_longitude' => $globalLongLat['results']?$globalLongLat['results'][0]['geometry']['location']['lng']:''
                    ]);

                }else
                {
                    $propertySubField =  new PropertySubfields;
                    $propertySubField->create([
                        'property_id' => $property->id,
                        'location_latitude' => $globalLongLat['results']?$globalLongLat['results'][0]['geometry']['location']['lat']:'',
                        'location_longitude' => $globalLongLat['results']?$globalLongLat['results'][0]['geometry']['location']['lng']:''
                    ]);
                }


            }

        }
        return 'done';
    }

    public function getBankGeoCodeAndStore()
    {
        $banks = Banks::get();
        foreach ($banks as $bank)
        {
            $address = '';
            $address = $bank->address? $bank->address : $bank->name;
            if ($address)
            {
                $globalLongLat = $this->convertAddressToLongLat($address);
                $bank->update([
                    'location_latitude' =>$globalLongLat['results']?$globalLongLat['results'][0]['geometry']['location']['lat']:'',
                    'location_longitude' => $globalLongLat['results']?$globalLongLat['results'][0]['geometry']['location']['lng']:''
                ]);
            }
        }
        return 'done';
    }

    public function getAll(Request $request)
    {
        return Banks::where("name","LIKE","%{$request['query']['term']}%")->get();
    }


}
