<?php

namespace App\Http\Controllers;

use App\AddressesJob;
use App\AddressesJobEmailLog;
use App\Imports\AdressenImport;
use App\Imports\BankCsvFileImport;
use App\Mail\CustomEmail;
use App\Mail\emailnotify;
use App\Services\AddressesJobEmailLogService;
use App\Services\EmailConfigService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class AddressesJobController extends Controller
{
    protected $addressesJob;
    protected $emailConfigService;
    protected $addressesJobEmailLog;


    public function __construct(AddressesJob $addressesJob, EmailConfigService $emailConfigService,
                                AddressesJobEmailLogService $addressesJobEmailLog)
    {
        $this->addressesJob = $addressesJob;
        $this->emailConfigService = $emailConfigService;
        $this->addressesJobEmailLog = $addressesJobEmailLog;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addressesJobs =   $this->addressesJob->whereNull('property_id')->orWhere('property_id',0)->orderBy('id', 'DESC')->get();

        return view('adressen.index',compact('addressesJobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adressen.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->addressesJob->create([
            'limit_max' => $request->limit_max,
            'keyword' => $request->keyword,
            'status' => 'unprocessed',
            'type' => $request->type,
            'is_bank' => $request->is_bank,
            'property_id' => $request->property_id?$request->property_id :''
        ]);

        return redirect(url('/adressen'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data =  $this->addressesJob->find($id);

        return view('adressen.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->addressesJob->find($id);

        if ($request->hasFile('file_data'))
        {
            $file = $request->file('file_data');
            $name = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path('/address');
            $file->move($destinationPath, $name);
            $request['result'] = $name;

        }


        $data->update($request->all());

        return redirect(url('/adressen'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function fileCreate($id)
    {
        return view('adressen.file-upload',compact('id'));
    }

    public function excelView($fileName)
    {
        $content = Excel::toArray(new AdressenImport(),public_path('address/'.$fileName) );
        return view('adressen.excel',compact('content'));

    }


    public function uploadBankToDb(Request $request)
    {
        $data = $this->addressesJob->find($request->b_id);
        Excel::import(new BankCsvFileImport(), public_path('address/'.$data->result));
        return 'success';
    }

    public function sendEmailFromCsv(Request $request)
    {
        $addressesJob = $this->addressesJob->find($request->address_id);
        $excludedEmails = $this->emailConfigService->find();
        $logsEmails = $this->addressesJobEmailLog->getAllMails();
//        $content = Excel::toArray(new AdressenImport(),public_path('address/'.$addressesJob->result) );
//        $emailArray = array();
//        foreach ($content as $data)
//         {
//             foreach ($data as $email)
//             {
//                 if(isset($email[22]))
//                 {
//                     $searchForValue = ',';
//                     $stringValue = $email[22];
//
//                     if( strpos($stringValue, $searchForValue) !== false ) {
//                         $domain =  substr($email[22], 0,strrpos($email[22],',' ));
//                     }else{
//                         $domain =  $email[22];
//                     }
//                     if(filter_var($domain, FILTER_VALIDATE_EMAIL)) {
//                         array_push($emailArray,$domain);
//                     }
//
//                 }
//             }
//          }
        $emailArray =  explode(',',$request->file_emails);
        $emailArray = array_unique($emailArray);

        if (isset($excludedEmails) && $excludedEmails[0] != ''){
            $emailArray = array_diff( $emailArray,$excludedEmails);

        }
        if ($logsEmails){
            $emailArray = array_diff( $emailArray,$logsEmails);
        }

        if ($emailArray)
        {
            array_push($emailArray, Auth::user()->email); /**** Send a mail copy to the logged in user as well ***/
            Mail::to(array_values($emailArray)[0])
                ->bcc($emailArray)
                ->send(new emailnotify($request));
            unset($emailArray[array_search(Auth::user()->email, $emailArray)]); /**** Remove logged in user mail  ***/
            $this->addressesJobEmailLog->store($emailArray,$addressesJob);
        }else{

            return 'Emails in file are already sent or in excluded section !';
        }


    }


    public function getEmailsFromFile(Request $request)
    {
        $content = Excel::toArray(new AdressenImport(),public_path('address/'.$request->name) );
        $emailArray = array();
        foreach ($content as $data)
        {
            foreach ($data as $email)
            {
                if(isset($email[22]))
                {
                    $searchForValue = ',';
                    $stringValue = $email[22];

                    if( strpos($stringValue, $searchForValue) !== false ) {
                        $domain =  substr($email[22], 0,strrpos($email[22],',' ));
                    }else{
                        $domain =  $email[22];
                    }
                    if(filter_var($domain, FILTER_VALIDATE_EMAIL)) {
                        array_push($emailArray,$domain);
                    }

                }
            }
        }

        return $emailArray;
    }


    public function sendCustomEmails ()
    {
        $addressesJob = $this->addressesJob->create([
           'property_id'=> 1197,
        ]);
        $emailArray = [
            'info@eger-magdeburg.de',
            'praxis@lamare-aesthetik.de',
            'info@klinik-am-rosental.de',
            'info@clinic-im-centrum.de',
            'info@m1-beauty.de',
            'info@dr-bodo.de',
            'info@medical-one.de',
            'info@klinikamopernplatz.de',
            'info@dr-von-wild.de'

        ];
        Mail::to(array_values($emailArray)[0])
            ->bcc($emailArray)
            ->send(new CustomEmail());

        $this->addressesJobEmailLog->store($emailArray,$addressesJob);

        return 'done';
    }

}
