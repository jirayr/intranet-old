<?php

namespace App\Http\Controllers;

use App\Services\FinApiService;
use Illuminate\Http\Request;

class FinApiController extends Controller
{
    protected $finApiService;

    public function __construct(FinApiService $finApiService)
    {
        $this->finApiService = $finApiService;
    }

    public function authenticate()
    {
       $this->finApiService->authenticate();
    }

    public function getAllBanks()
    {
      return  $this->finApiService->getAllBanks();
    }


}
