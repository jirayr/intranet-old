<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Ad;
use App\Vad;
use App\Models\TenancySchedule;
use App\Models\TenancyScheduleItem;
use App\Models\VermietungItem;
use App\Models\VacantSpace;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function postlisting($created_ad)
    {
        $ads = Ad::with('city', 'country', 'state')->whereId($created_ad)->first()->toArray();

         // print_r($ads); 

        if($ads)
        {
            $url = "https://verkauf.fcr-immobilien.de/saveIntranetlist";
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($ads));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $str = curl_exec($curl);
            curl_close($curl);  
            // print_r($str); die;  
            return $str;
        }
    }
    public function postlisting1($created_ad,$pid)
    {
        $ads = Vad::with('city', 'country', 'state')->whereId($created_ad)->first()->toArray();


        $items = VermietungItem::selectRaw('vermietung_items.*')->join('tenancy_schedule_items','tenancy_schedule_items.id','=','vermietung_items.item_id')->join('tenancy_schedules','tenancy_schedules.id','=','tenancy_schedule_items.tenancy_schedule_id')->where('tenancy_schedules.property_id',$pid)->get()->toArray();

        $ads['items'] = $items;

        $items = VacantSpace::where('property_id',$pid)->get()->toArray();
        $ads['customitems'] = $items;


        if($ads)
        {
            $url = "https://vermietung.fcr-immobilien.de/saveIntranetlist";
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($ads));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $str = curl_exec($curl);
            curl_close($curl);  
            // print_r($str); die;  
            return $str;
        }
    }

    public function postVacantspace($pid,$item_id)
    {
        $items = VermietungItem::selectRaw('vermietung_items.*')->join('tenancy_schedule_items','tenancy_schedule_items.id','=','vermietung_items.item_id')->join('tenancy_schedules','tenancy_schedules.id','=','tenancy_schedule_items.tenancy_schedule_id')->where('tenancy_schedules.property_id',$pid)->where('vermietung_items.item_id',$item_id)->get()->toArray();
        $ads['items'] = $items;
        // print_r($items); die;

        
        if($items)
        {
            $url = "https://vermietung.fcr-immobilien.de/saveVacantspace";
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($ads));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $str = curl_exec($curl);
            curl_close($curl);  
            // print_r($str); die;  
            return $str;
        }
    }

    public function postCustomVacantspace($item_id)
    {
        $items = VacantSpace::where('id',$item_id)->get()->toArray();
        $ads['customitems'] = $items;

        // print_r($items); die;
        
        if($items)
        {
            $url = "https://vermietung.fcr-immobilien.de/saveCustomVacantspace";
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($ads));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $str = curl_exec($curl);
            curl_close($curl);  
            // print_r($str); die;  
            return $str;
        }
    }



    function short_name($string)
    {
        if($string){
            $arr = explode(" ", $string);
            if(count($arr)>1)
                return substr($arr[0], 0,1).$arr[1];
            else
                return $string;
        }
        return $string;
    }
    public function propertiesids()
    {
        return array(121);
    }

    public function getdata21()
    {
        $arr['name'] = " Florian, Andrea und Claudia";
        $arr['email'] = "rastatt@fcr-immobilien.de";
        // $arr['email'] = "yiicakephp+rastatt@gmail.com";

        return $arr;
    }

    public function mail_not_send(){
        $data = ['info@intranet.fcr-immobilien.de'];

        // add deactive users
        $users = \DB::table('users')->select('email')->where('user_status', 0)->get();
        if($users){
            foreach ($users as $key => $user) {
                if($user->email)
                    $data[] = $user->email;
            }
        }
        return $data;
    }
}
