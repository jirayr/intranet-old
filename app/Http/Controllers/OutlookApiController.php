<?php

namespace App\Http\Controllers;

use App\Services\OutlookHTTPService;
use Illuminate\Http\Request;

class OutlookApiController extends Controller
{
    protected $outlookHttpService;

    public function __construct(OutlookHTTPService $outlookHTTPService)
    {
        $this->outlookHttpService = $outlookHTTPService;
    }

    public function authenticate($code)
    {
      return $this->outlookHttpService->authenticate($code);
    }

    public function view()
    {
        $categories = $this->outlookHttpService->getcategories();
        return view('properties.email',compact('categories'));
    }

    public function getMailsByCategory(Request $request)
    {
       $category = explode(' ',trim($request->category));
       $emails = $this->outlookHttpService->getMailsByCategory($category[0]);
       $categories = $this->outlookHttpService->getcategories();
       return view('properties.email',compact('categories','emails'));
    }

    public function getCode(Request $request)
    {
      return  $this->outlookHttpService->authenticate($request->code);
    }

    public function redirectToAuthFlow()
    {
       return $this->outlookHttpService->redirectToAuthFlow();
    }
}
