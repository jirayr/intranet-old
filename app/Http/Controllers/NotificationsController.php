<?php

namespace App\Http\Controllers;

use App\Models\Notifications;
use App\Models\NotificationStatuses;
use App\Services\NotificationsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function newNotification(Request $request)
    {
        $data = $request->all();
        $input = [
            'type' => $data['type'],
            'property_id' => $data['property_id']
        ];
        $notification = NotificationsService::create($input);
        echo 1;

        die;
    }

    public function seenAllNotification()
    {
        $notification_ids = Notifications::pluck('id')->toArray();

        foreach ($notification_ids as $notification_id) {
            if (NotificationStatuses::where('user_id', Auth::id())->where('notification_id', $notification_id)->count() == 0) {
                NotificationsService::seenNotification($notification_id);
            }
        }

        echo 1;
        die;
    }

    public function getNewNotification(Request $request)
    {
        $notifications = NotificationsService::getAllNotifications();
        $data = json_decode($request->all()['data'], true);
        $results = [];
        foreach ($notifications as $notification) {
            if ($notification['is_seen'] == false && !in_array($notification['id'], $data)) {
                array_push($results, $notification);
            }
        }
        echo json_encode($results);
        die;
    }
}
