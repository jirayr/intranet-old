<?php

namespace App\Http\Controllers;

use App\EmailConfig;
use App\Services\EmailConfigService;
use Illuminate\Http\Request;
use function PHPSTORM_META\elementType;


class EmailConfigController extends Controller
{
    protected $emailConfigService ;

    public function __construct(EmailConfigService $emailConfigService)
    {
        $this->emailConfigService = $emailConfigService;
    }

    public function store(Request $request)
    {
       $this->emailConfigService->store($request);
    }

    public function find()
    {
     return  $this->emailConfigService->find();
    }
}
