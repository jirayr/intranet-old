<?php

namespace App\Http\Controllers;

use App\Address;
use App\Company;
use App\CompanyEmployee;
use App\Imports\ExcelFileImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
ini_set('memory_limit', '-1');
class EmployeeController extends Controller
{
    protected $company;
    protected $address;
    protected $employee;
    public function __construct(Company $company, Address $address,CompanyEmployee $employee)
    {
        $this->company = $company;
        $this->address = $address;
        $this->employee = $employee;
    }

    public function index($id)
    {
        $company = $this->company->find($id);
        $employees = $this->employee->where('company_id',$id)->with('employeeAddress')->paginate(100);
        return view('company_employees.index',compact('company','employees'));
    }

    public function create()
    {
        return view('company_employees.add');
    }

    public function store(Request $request)
    {
        $companyId = $request->company_id;
        $employee = $this->employee->create($request->all());
        $this->address->create([
            'employee_id' => $employee->id,
            'zip' => $request->zip,
            'street'=> $request->street,
            'city'=> $request->city,
            'part_of_city'=>$request->part_of_city
        ]);

        return redirect(url('/company/employee/'.$companyId));
    }

    public function edit($id)
    {
        $employee = $this->employee->where('id',$id)->with('employeeAddress')->first();
        return view('company_employees.edit',compact('employee'));
    }

    public function update(Request $request ,$id)

    {

      $employee = '';
      $employee = $this->employee->find($id);
      $employee->update($request->all());
        $this->address->where('company_id',$id)->update([
            'company_id' => $request->id,
            'zip' => $request->zip,
            'street'=> $request->street,
            'city'=> $request->city,
            'part_of_city'=>$request->part_of_city
        ]);
         return redirect(url('/company/employee/'.$employee->company_id));
    }

    public function delete($id)
    {
        $this->employee->find($id)->delete();
        $this->address->where('employee_id',$id)->delete();
        return redirect(route('company'));
    }

    public function importExcelFile()
    {
        Excel::import(new ExcelFileImport, public_path('Flowfact.XLSX'));

    }
}
