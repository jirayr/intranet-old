<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\ExportAd;
use App\Helpers\ExportPdfHelperClass;
use App\Helpers\PropertiesHelperClass;
use App\Mail\SendLoiEmail;
use App\Models\BankFinancingOffer;
use App\Models\PropertiesExtra1;
use App\Models\PropertiesTenants;
use App\Models\PropertyStatus;
use Auth;
use App\User;
use App\TenancyScheduleComment;
use App\Models\Properties;
use App\Models\PropertyLoansMirror;
use App\Models\Banks;
use App\Models\Comment;
use App\Models\Attachment;
use App\Models\Notifications;
use App\Models\PropertyInvoices;
use App\Models\PropertiesMailLog;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\PropertiesService;
use App\Models\Masterliste;
use App\Models\TenancySchedule;
use App\Services\TenancySchedulesService;
use App\Models\TenancyScheduleItem;
use App\Models\PropertyComments;
use App\Models\PropertiesBuyDetail;
use App\Models\PropertiesBanken;
use App\Models\StatusLoi;
use App\Models\BankingRequest;
use DB;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use mysql_xdevapi\Exception;
use Response;
use App\Profit_loss_graph;
use Carbon\CarbonPeriod;
use Mail;
use App\email_template2_sending_info;
use Illuminate\Support\Str;
use App\Mail\DemoEmail;
use Validator;
use File;
use Illuminate\Support\Facades\Storage;
use App\Models\PropertyEinkaufFile;
use App\Models\PropertiesDirectory;
use Lang;
use PDF;
use Analytics;
use Spatie\Analytics\Period;
use App\Models\SendMailToAm;
use App\Models\PropertiesSaleDetail;

ini_set('memory_limit','-1');

class RoleDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $api_url = "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/realestate{parm}";
    public function __construct()
    {
        $this->middleware('auth');
    }


public function dashboard2() {
        

        $user = Auth::user();
		if($user->second_role!=2 && $user->second_role!=4)
		{
			 return redirect()->back();
		}
        
        
        if($user->second_role==2)
        {

            $tenancy_items_2 = TenancyScheduleItem::where('type',1)->orWhere('type', '=', 2)->get();
            foreach($tenancy_items_2 as $key => $tenancy_item){


                if($tenancy_item->tenancy_schedule_id){
                    $tenancy_schedule = TenancySchedule::where('id',$tenancy_item->tenancy_schedule_id)->first();
                    if(!$tenancy_schedule)
                    {
                        unset($tenancy_items_2[$key]);
                        continue;
                    }
                    $properties = Properties::where('id', $tenancy_schedule->property_id)->where('Ist',0)->where('soll',0)->where('status',6)->first();
                    if ($properties != null && $properties->masterliste_id != null){
                        $tenancy_item->object_name  = $properties->name_of_property;
                        $tenancy_item->property_id  = $properties->id;

                        $singleComment = TenancyScheduleComment::where('item_id',$tenancy_item->id)->where('comment', '!=', '')->latest()->first();

                        // pre($singleComment); die;
                        if($singleComment && $singleComment->comment)
                            $tenancy_item->comment = $singleComment->comment;


                        $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                        if($masterliste != null){


                            $masterliste->asset_manager = $tenancy_item->asset_manager_id;

                            $u = User::where('id', $masterliste->asset_manager)->first();

                            $tenancy_item->creator_name = "";
                            if($masterliste->asset_manager && $u)
                                $tenancy_item->creator_name = $u->name;

                        }else{
                            unset($tenancy_items_2[$key]);
                        }
                        if($tenancy_item->type == 2){
                            $tenancy_item->type = 'Gewerbe';
                        }elseif ($tenancy_item->type == 1){
                            $tenancy_item->type = 'Wohnen';
                        }

                    }else{
                        unset($tenancy_items_2[$key]);
                    }

                }
            }
            //Get Masterliste stats for current user
            $masterliste_stats  = Masterliste::where('asset_manager',Auth::user()->id)->first();

            if(isset($input['asset_manager']))
                $analytics = Properties::selectRaw('asset_m_id,count(properties.id) as id,group_concat(masterliste_id) as masterliste_id')->
                join('masterliste','masterliste.id','=','properties.masterliste_id')->where('asset_m_id',$input['asset_manager'])
                    ->where('Ist',0)->where('soll',0)->groupBy('asset_m_id')->get();
            else
                $analytics = Properties::selectRaw('asset_m_id,count(properties.id) as id,group_concat(masterliste_id) as masterliste_id')->
                join('masterliste','masterliste.id','=','properties.masterliste_id')
                    ->where('Ist',0)->where('soll',0)->groupBy('asset_m_id')->get();

            $analytics_array = array();

            foreach ($analytics as $key => $value) {
                $user = User::find($value->asset_m_id);

                if(!$user)
                    continue;


                if(!isset($input['asset_manager'])):
                    $analytics_array[$value->asset_m_id]['flat_in_qm'] = 0;
                    $analytics_array[$value->asset_m_id]['rented_area'] = 0;
                    $analytics_array[$value->asset_m_id]['vacancy'] = 0;
                    $analytics_array[$value->asset_m_id]['rent_net_pm'] = 0;
                    $analytics_array[$value->asset_m_id]['potential_pm'] = 0;
                    $analytics_array[$value->asset_m_id]['object'] = '';
                endif;

                $m_list = Masterliste::whereIn('id',explode(',', $value->masterliste_id))->get();
                $count = 0;
                foreach ($m_list as $index => $masterliste_2) {

                    // $masterliste->asset_manager = $value->asset_m_id;
                    if(isset($input['asset_manager'])):
                        $masterliste = $masterliste_2;

                        $masterliste->asset_manager_name = $user->name;
                        $m_property=$masterliste->property;
                        $masterliste->property_id = ($m_property)
                            ? $m_property->id :null;
                        $tenancy_schedule_data = TenancySchedulesService::get($masterliste->property_id);
                        $masterliste->tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : new TenancySchedule();
                        $analytics_array[$index]['count'] = 1;
                        $analytics_array[$index]['asset_manager_name'] = $masterliste['asset_manager_name'];
                        // working
                        $analytics_array[$index]['flat_in_qm'] = $masterliste['flat_in_qm'];
                        $analytics_array[$index]['rented_area'] = $masterliste['rented_area'];
                        $analytics_array[$index]['vacancy'] =  $masterliste['vacancy'];
                        $analytics_array[$index]['rent_net_pm'] = $masterliste['rent_net_pm'];
                        $analytics_array[$index]['potential_pm'] = $masterliste->tenancy_schedule->calculations['potenzial_eur_jahr'];
                        $analytics_array[$index]['asset_manager'] = $masterliste['asset_manager'];
                        $analytics_array[$index]['object'] = $masterliste['object'];

                        $query = str_replace('.', '', $masterliste['object']);

                        $properties_exe_2 = $properties = Properties::where('Ist',0)->where('soll',0)->where('id', $masterliste->property_id)->first();

                        if(isset($properties->id)){
                            $analytics_array[$index]['id'] = $properties->id;
                        }
                        if(isset($properties_exe_2->id)){
                            $analytics_array[$index]['id'] = $properties_exe_2->id;
                        }
                        else{
                            $analytics_array[$index]['id'] = 0;
                        }


                    else:
                        $m_property=$masterliste_2->property;
                        $masterliste_2->property_id = ($m_property)
                            ? $m_property->id :null;
                        $tenancy_schedule_data = TenancySchedulesService::get($masterliste_2->property_id);
                        $masterliste_2->tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : new TenancySchedule();


                        $count++;
                        $analytics_array[$value->asset_m_id]['asset_manager'] = $masterliste_2->asset_manager;
                        $analytics_array[$value->asset_m_id]['id'] = $masterliste_2->id;


                        $analytics_array[$value->asset_m_id]['count'] = $count;
                        $analytics_array[$value->asset_m_id]['asset_manager_name'] = $user->name;



                        $analytics_array[$value->asset_m_id]['flat_in_qm'] += $masterliste_2->tenancy_schedule->calculations['mi9'] + $masterliste_2->tenancy_schedule->calculations['mi10'];
                        $analytics_array[$value->asset_m_id]['rented_area'] += $masterliste_2->tenancy_schedule->calculations['total_live_rental_space'] + $masterliste_2->tenancy_schedule->calculations['total_business_rental_space'];
                        $analytics_array[$value->asset_m_id]['vacancy'] += $masterliste_2->tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $masterliste_2->tenancy_schedule->calculations['total_business_vacancy_in_qm'];
                        $analytics_array[$value->asset_m_id]['rent_net_pm'] += $masterliste_2->tenancy_schedule->calculations['total_actual_net_rent'];

                        $analytics_array[$value->asset_m_id]['potential_pm'] += $masterliste_2->tenancy_schedule->calculations['potenzial_eur_jahr'];

                        $analytics_array[$value->asset_m_id]['object'] .= (isset($masterliste_2->object) ? $masterliste_2->object.'<br/> ':' ');
                    endif;
                }
            }


            $is_asset_manager=isset($input['asset_manager']);
            return view('dashboard.dashboard_transaction_manager',compact('tenancy_items_2','masterliste_stats','analytics_array','is_asset_manager'));
            die;
        }



        $instacount = 0;

        $perPages = 500;
        $user = Auth::user();
        $user_id = $user->id;
        $analysis = array();
        // $analysis_asset = array();
        if($user->second_role==1){
            $date = date('Y-m-');
            $analysis = Properties::selectRaw('user_id,count(id) as id,sum(gesamt_in_eur
                                + (real_estate_taxes * gesamt_in_eur)
                                + (estate_agents * gesamt_in_eur)
                                + ((Grundbuch * gesamt_in_eur)/100)
                                + (evaluation * gesamt_in_eur)
                                + (others * gesamt_in_eur)
                                + (buffer * gesamt_in_eur)) as total_purchase_price')->where('status','=',config('properties.status.offer'))->where('Ist',0)->where('soll',0)->where('created_at','like','%'.$date.'%')->groupBy('user_id')->get();


        }else{
        }
        $banks = Banks::all();
        $logs = Notifications::orderBy('id', 'desc')->take(3)->get();

        foreach($logs as $log){
            if($log->created_user_id){
                $log->created_user_name = (User::where('id', $log->created_user_id)->first())?User::where('id', $log->created_user_id)->first()->name:"";
            }
            if($log->property_id){
                $log->property_name = (Properties::where('id', $log->property_id)->where('Ist',0)->where('soll',0)->first())?Properties::where('id', $log->property_id)->where('Ist',0)->where('soll',0)->first()->name_of_property:"";
            }
            $log->time_ago = strtotime($log->created_at);
        }


        
        // $tenancy_items = TenancyScheduleItem::where('type',3)->orWhere('type', '=', 4)->orderBy('rent_end','asc')->get();
        $tenancy_items_2 = TenancyScheduleItem::where('type',1)->orWhere('type', '=', 2)->orderBy('rent_end','asc')->get();

        // $tenancy_items_4 = TenancyScheduleItem::orderBy('id', 'desc')->where('type',1)->orWhere('type', '=', 2)->orderBy('rent_end','asc')->get();

        $date = date('Y-m-');
        $tenancy_items_4 = TenancyScheduleItem::orderBy('id', 'desc')->whereIn('type',[1, 2])->where('created_at','like','%'.$date.'%')->where('is_new',1)->where('status',1)->orderBy('rent_begin','asc')->get();


        $tenancy_items = TenancySchedule::leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->where('type',3)->orWhere('type', '=', 4)->get();

        $pa_array = $pa_array2 = array();
        foreach($tenancy_items as $key => $tenancy_item){
            if($tenancy_item->tenancy_schedule_id){
                $properties = Properties::where('Ist',0)->where('soll',0)->where('id', $tenancy_item->property_id)->first();
                if ($properties != null && $properties->masterliste_id != null){
                    $tenancy_item->object_name  = $properties->name_of_property;
                    // $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                    if(isset($master_array[$properties->masterliste_id])){

                        $asset_manager = $master_array[$properties->masterliste_id];

                        $asset_manager = $properties->asset_m_id;


                        $tenancy_item->creator_name = "";
                        if(isset($asset_name_array[$asset_manager]))
                            $tenancy_item->creator_name = $asset_name_array[$asset_manager];

                        if(isset($pa_array[$asset_manager]))
                            $pa_array[$asset_manager] += $tenancy_item->actual_net_rent;
                        else
                            $pa_array[$asset_manager] = $tenancy_item->actual_net_rent;

                        if(isset($pa_array2[$tenancy_item->property_id]))
                            $pa_array2[$tenancy_item->property_id] += $tenancy_item->actual_net_rent;
                        else
                            $pa_array2[$tenancy_item->property_id] = $tenancy_item->actual_net_rent;


                    }else{
                        unset($tenancy_items[$key]);
                    }
                    if($tenancy_item->type == 4){
                        $tenancy_item->type = 'Gewerbe';
                    }elseif ($tenancy_item->type == 3){
                        $tenancy_item->type = 'Wohnen';
                    }



                }else{
                    unset($tenancy_items[$key]);
                }

            }
        }




        $masterlistes_new = array();
        $masterlistes_new_1 = array();
        $masterlistes_new_2 = array();

        $summ_vermietet_m2 = 0;
        $summ_vermietet_percent = 0;
        $summ_leerstand_m2 = 0;
        $summ_leerstand_percent = 0;
        $summ_differenz_miete_in_euro = 0;
        $summ_differenz_miete_pa_in_euro = 0;

        $graph_data = [];
        $graph_data['sum_vermietet_m2_this_month'] = 0;
        $graph_data['sum_vermietet_m2_last_month'] = 0;
        $graph_data['sum_vermietet_m2_next_6_month'] = 0;
        $graph_data['sum_leerstand_m2_this_month'] = 0;
        $graph_data['sum_leerstand_m2_last_month'] = 0;
        $graph_data['sum_leerstand_m2_next_6_month'] = 0;
        $graph_data['sum_differenz_miete_this_month'] = 0;
        $graph_data['sum_differenz_miete_last_month'] = 0;
        $graph_data['sum_differenz_miete_next_6_month'] = 0;

        $thisMonthStart = mktime(0, 0, 0, date("n"), 1);
        $thisMonthEnd  = mktime(23, 59, 59, date("n"), date("t"));


        $nextMonthStart = mktime(0, 0, 0, date("n",strtotime("+1 month")), 1);
        $next6MonthEnd = mktime(23, 59, 59, date("n",strtotime("+6 month")), date("t",strtotime("+6 month")));

        $lastMonthStart = mktime(0, 0, 0, date("n",strtotime("-1 month")), 1);
        $lastMonthEnd = mktime(23, 59, 59, date("n",strtotime("-1 month")), date("t",strtotime("-1 month")));




        $sum_rented_area=0;
        $sum_flat_in_qm=0;

        $s = Masterliste::selectRaw('sum(rented_area) as rented_area,sum(flat_in_qm) as flat_in_qm,sum(potential_pm) as potential_pm')->get();

        if(isset($s[0]['rented_area'])&& $s[0]['rented_area'])
            $sum_rented_area = $s[0]['rented_area'];
        if(isset($s[0]['flat_in_qm'])&& $s[0]['flat_in_qm'])
            $sum_flat_in_qm = $s[0]['flat_in_qm'];
        //if(isset($s[0]['potential_pm'])&& $s[0]['potential_pm'])
        //   $summ_differenz_miete_in_euro = $s[0]['potential_pm'];


        $o = Properties::whereIn('status',array(6,11))->where('Ist',0)->where('soll',0)->get();
        $wv = $p_count =  0;

        foreach ($o as $key => $value) {
            $tr = TenancySchedule::where('property_id',$value->id)->first();
            if($tr && $tr->text_json)
            {
                $arrj = json_decode($tr->text_json,true);
                // $masterliste->rented_area = $arrj['total_live_rental_space'] + $arrj['total_business_rental_space'];
                // $masterliste->flat_in_qm = $arrj['mi9'] + $arrj['mi10'];
                $summ_differenz_miete_in_euro += $arrj['potenzial_eur_jahr'];

                if($value->status==6 && isset($arrj['wault']) && $arrj['wault'])
                {
                    $p_count = $p_count + 1;
                    $temp = 0;
                    if(isset($arrj['wault']))
                        $temp = $arrj['wault'];
                    // else
                    // $temp = ($json_array['total_business_actual_net_rent']!=0) ? number_format(($json_array['total_remaining_time_in_eur'])/(12 * $json_array['total_business_actual_net_rent']),1) : 0 ;
                    $wv += $temp;
                }

            }
        }

        $avg_wault = 0;
        if($p_count)
        {
            $avg_wault = $wv/$p_count;
        }



        //dd($masterlistes);
        $masterlistes = array();
        
        if($sum_flat_in_qm!=0)
        {
            $summ_vermietet_percent = $sum_rented_area / $sum_flat_in_qm * 100;
            $summ_leerstand_percent = $summ_leerstand_m2 / $sum_flat_in_qm * 100;
        }

        // dd($summ_differenz_miete_in_euro);

        foreach($masterlistes_new as $masterliste){
            if(1==2){
                $asset_manager_name_check = substr( $masterliste['asset_manager_name'],  0, 11);
                if($asset_manager_name_check != 'Entwicklung'){

                    $masterlistes_new_1[$masterliste["asset_manager"]]['count'] = 0;
                    $masterlistes_new_1[$masterliste["asset_manager"]]['asset_manager_name'] = '';
                    $masterlistes_new_1[$masterliste["asset_manager"]]['flat_in_qm'] = 0;
                    $masterlistes_new_1[$masterliste["asset_manager"]]['rented_area'] = 0;
                    $masterlistes_new_1[$masterliste["asset_manager"]]['vacancy'] = 0;
                    $masterlistes_new_1[$masterliste["asset_manager"]]['rent_net_pm'] = 0;
                    $masterlistes_new_1[$masterliste["asset_manager"]]['potential_pm'] = 0;
                    $masterlistes_new_1[$masterliste["asset_manager"]]['object'] = '';

                    $masterlistes_new_1[$masterliste["asset_manager"]]['asset_manager'] = $masterliste["asset_manager"];
                    $masterlistes_new_1[$masterliste["asset_manager"]]['count'] = $masterliste['count'];
                    $masterlistes_new_1[$masterliste["asset_manager"]]['asset_manager_name'] = $masterliste['asset_manager_name'];
                    $masterlistes_new_1[$masterliste["asset_manager"]]['flat_in_qm'] += $masterliste['flat_in_qm'];
                    $masterlistes_new_1[$masterliste["asset_manager"]]['rented_area'] += $masterliste['rented_area'];
                    $masterlistes_new_1[$masterliste["asset_manager"]]['vacancy'] += $masterliste['vacancy'];
                    $masterlistes_new_1[$masterliste["asset_manager"]]['rent_net_pm'] += $masterliste['rent_net_pm'];
                    $masterlistes_new_1[$masterliste["asset_manager"]]['potential_pm'] += $masterliste['potential_pm'];
                    $masterlistes_new_1[$masterliste["asset_manager"]]['object'] .= $masterliste['object'];
                }
                else{
                    $masterlistes_new_2[$masterliste["asset_manager"]]['count'] = 0;
                    $masterlistes_new_2[$masterliste["asset_manager"]]['asset_manager_name'] = '';
                    $masterlistes_new_2[$masterliste["asset_manager"]]['flat_in_qm'] = 0;
                    $masterlistes_new_2[$masterliste["asset_manager"]]['rented_area'] = 0;
                    $masterlistes_new_2[$masterliste["asset_manager"]]['vacancy'] = 0;
                    $masterlistes_new_2[$masterliste["asset_manager"]]['rent_net_pm'] = 0;
                    $masterlistes_new_2[$masterliste["asset_manager"]]['potential_pm'] = 0;
                    $masterlistes_new_2[$masterliste["asset_manager"]]['object'] = '';


                    $masterlistes_new_2[$masterliste["asset_manager"]]['count'] = $masterliste['count'];
                    $masterlistes_new_2[$masterliste["asset_manager"]]['asset_manager_name'] = $masterliste['asset_manager_name'];
                    $masterlistes_new_2[$masterliste["asset_manager"]]['flat_in_qm'] += $masterliste['flat_in_qm'];
                    $masterlistes_new_2[$masterliste["asset_manager"]]['rented_area'] += $masterliste['rented_area'];
                    $masterlistes_new_2[$masterliste["asset_manager"]]['vacancy'] += $masterliste['vacancy'];
                    $masterlistes_new_2[$masterliste["asset_manager"]]['rent_net_pm'] += $masterliste['rent_net_pm'];
                    $masterlistes_new_2[$masterliste["asset_manager"]]['potential_pm'] += $masterliste['potential_pm'];
                    $masterlistes_new_2[$masterliste["asset_manager"]]['object'] .= $masterliste['object'];
                }
            }
            $summ_differenz_miete_in_euro += $masterliste['potential_pm'];

        }

        // foreach ($Revenues as $Revenue){
        //     $sum_Revenue=$sum_Revenue+$Revenue['total_purchase_price'];
        // }
//      $summ_leerstand_percent /= count($masterlistes);
//       $summ_vermietet_percent /= count($masterlistes);




        foreach($tenancy_items_2 as $key => $tenancy_item){


            if($tenancy_item->tenancy_schedule_id )
            {
                $tenancy_schedule = TenancySchedule::where('id',$tenancy_item->tenancy_schedule_id)->first();
                if($tenancy_schedule)
                {
                    $properties = Properties::selectRaw('masterliste_id,name_of_property,asset_m_id,id')->where('id', $tenancy_schedule->property_id)->where('Ist',0)->where('soll',0)->where('status',6)->first();
                    if ($properties != null && $properties->masterliste_id != null && $tenancy_item->status)
                    {
                        $tenancy_item->property_id  = $properties->id;
                        $tenancy_item->object_name  = $properties->name_of_property;
                        $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                        if($masterliste != null){

                            $singleComment = TenancyScheduleComment::where('item_id',$tenancy_item->id)->where('comment', '!=', '')->latest()->first();

                            // pre($singleComment); die;
                            if($singleComment && $singleComment->comment)
                                $tenancy_item->comment = $singleComment->comment;


                            $masterliste->asset_manager = $properties->asset_m_id;

                            $tenancy_item->creator_name = "";
                            if($masterliste->asset_manager)
                                $tenancy_item->creator_name = User::where('id', $masterliste->asset_manager)->first()->name;
                        }else{
                            unset($tenancy_items_2[$key]);
                        }
                        if($tenancy_item->type == 2){
                            $tenancy_item->type = 'Gewerbe';
                        }elseif ($tenancy_item->type == 1){
                            $tenancy_item->type = 'Wohnen';
                        }

                    }
                    else{
                        unset($tenancy_items_2[$key]);
                    }
                }


            }
        }
        $asset_manager_array = array();

        foreach($tenancy_items_4 as $key => $tenancy_item_33){

            if($tenancy_item_33->tenancy_schedule_id){
                $tenancy_schedule = TenancySchedule::where('id',$tenancy_item_33->tenancy_schedule_id)->first();
                $properties = Properties::where('id', $tenancy_schedule->property_id)->where('Ist',0)->where('soll',0)->first();
                if ($properties != null && $properties->masterliste_id != null){
                    $tenancy_item_33->object_name  = $properties->name_of_property;
                    $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                    if($masterliste != null){


                        $masterliste->asset_manager = $tenancy_item_33->asset_manager_id;

                        $tenancy_item_33->creator_name = "";
                        if($masterliste->asset_manager)
                            $tenancy_item_33->creator_name = User::where('id', $masterliste->asset_manager)->first()->name;


                        $asset_manager_array[$masterliste->asset_manager]['name'] = $tenancy_item_33->creator_name;

                        $date1 = $tenancy_item_33->rent_begin;
                        $date2 = $tenancy_item_33->rent_end;

                        $date1 = date_create($date1);
                        $date2 = date_create($date2);
                        $diff = date_diff($date1,$date2);
                        $diff =  $diff->days/365;

                        if(isset($asset_manager_array[$masterliste->asset_manager]['count'])){
                            $asset_manager_array[$masterliste->asset_manager]['count'] += 1;
                            $asset_manager_array[$masterliste->asset_manager]['amount'] += $tenancy_item_33->actual_net_rent;

                            $asset_manager_array[$masterliste->asset_manager]['camount'] += $tenancy_item_33->actual_net_rent * $diff *12;
                        }
                        else{
                            $asset_manager_array[$masterliste->asset_manager]['count'] = 1;
                            $asset_manager_array[$masterliste->asset_manager]['amount'] = $tenancy_item_33->actual_net_rent;
                            $asset_manager_array[$masterliste->asset_manager]['camount'] = $tenancy_item_33->actual_net_rent * $diff *12;
                        }

                    }else{
                        unset($tenancy_items_2[$key]);
                    }
                    if($tenancy_item_33->type == 2){
                        $tenancy_item->type = 'Gewerbe';
                    }elseif ($tenancy_item_33->type == 1){
                        $tenancy_item_33->type = 'Wohnen';
                    }

                }else{
                    unset($tenancy_items_4[$key]);
                }



            }
        }
        ################################
        ################### scond query end
        ################################

        ############ Comments
        $comments = PropertyComments::selectRaw('property_comments.*,properties.name_of_property')->join('properties','properties.main_property_id','=','property_comments.property_id')->orderBy('id', 'desc')->whereNotNull('comment')->where('standard_property_status',1)->get();

        ########### User Total income

        // $total_rarnings = DB::table('users')->join('properties', 'properties.user_id', 'users.id')->join('tenancy_schedules', 'tenancy_schedules.property_id', 'properties.id')->join('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->select('users.id', 'users.name', 'users.image', DB::raw("SUM(tenancy_schedule_items.actual_net_rent) as total_sum"))->groupBy('users.id', 'users.name', 'users.image')->get();
        $makler_user = 0;
        $activecount = 0;
        $makler_userall = $activecountall = 0;

        $vmakler_user = 0;
        $vactivecount = 0;
        $vmakler_userall = $vactivecountall = 0;
        $logo_users = $download_users = 0;

        $url = "https://verkauf.fcr-immobilien.de/getregisterusercount";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str = curl_exec($curl);
        curl_close($curl);
        $v_analytic = array();
        if($str)
        {
            $v_analytic = $ids = json_decode($str,true);
            if(isset($ids['count']))
                $makler_user = $ids['count'];
            if(isset($ids['activecount']))
                $activecount = $ids['activecount'];

            if(isset($ids['countall']))
                $makler_userall = $ids['countall'];
            if(isset($ids['activecountall']))
                $activecountall = $ids['activecountall'];

            if(isset($ids['logo_users']))
                $logo_users = $ids['logo_users'];

            if(isset($ids['download_users']))
                $download_users = $ids['download_users'];
        }

        $url = "https://vermietung.fcr-immobilien.de/getregisterusercount";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str = curl_exec($curl);
        curl_close($curl);
        if($str)
        {
            $ids = json_decode($str,true);
            if(isset($ids['count7']))
                $vmakler_user = $ids['count7'];
            if(isset($ids['activecount7']))
                $vactivecount = $ids['activecount7'];

            if(isset($ids['countall']))
                $vmakler_userall = $ids['countall'];
            if(isset($ids['activecountall']))
                $vactivecountall = $ids['activecountall'];
        }


        /*=============================================
        =           Dashboard Graph data                =
        =============================================*/
        $current_month = \Carbon\Carbon::now();
        $month = [];
        $displayMonth = [];

        for ($i = 1; $i <= 10; $i++) {
            $current_month->subMonth();
            // $month[] = $current_month->format('Y-m');
        }
        // $current_month = \Carbon\Carbon::now();
        for ($i = 1; $i <= 17; $i++) {
            $month[] = $current_month->format('Y-m');
            $str = Lang::get('calander.'.$current_month->format('M').'');
            $str = "'".$str."-".$current_month->format('y')."'";
            $displayMonth[] = $str;
            $current_month->addMonth();
        }

        $graph = Session::get('graph');

        if($graph)
        {
            $year_graph = json_decode($graph,true);
            // print_r($year_graph); die;
        }
        else{





            $tenancy_items_3 = Properties::select(
                'properties.id as property_id',
                'properties.status as property_status',
                'profit_loss_graph.id',
                'profit_loss_graph.rent_begin',
                'profit_loss_graph.rent_end',
                'profit_loss_graph.actual_net_rent',
                'profit_loss_graph.schedule_item_id'

            )->join('tenancy_schedules', 'tenancy_schedules.property_id', '=', 'properties.id')
                ->join('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', '=', 'tenancy_schedules.id')
                ->join('profit_loss_graph', 'profit_loss_graph.schedule_item_id', '=', 'tenancy_schedule_items.id')

                ->where(function ($query){

                    $query->orWhere('properties.status', 6);
                    $query->orWhere('properties.status', 11);
                })
                ->where('profit_loss_graph.actual_net_rent','!=',null)
                ->whereRaw('profit_loss_graph.rent_end > profit_loss_graph.rent_begin')
                ->get();

            if(isset($_GET['rashid'])){

                echo '<pre>';
                print_r(json_decode(json_encode($tenancy_items_3), true));
                echo '</pre>';
                die();
            }


            $data = array();
            $rent_data = array();
            $count = 0;
            //$tenancy_items_3 = Profit_loss_graph::where('actual_net_rent','!=',null)->whereRaw('rent_end > rent_begin')->get();
            $rented_amount = 0;

            for($j=0; $j < count($month); $j++){

                for($i=0; $i < count($tenancy_items_3); $i++){


                    try{

                        $this_month = $month[$j];
                        $rent_begin = explode('-', $tenancy_items_3[$i]['rent_begin']);
                        $rent_begin = $rent_begin[0].'-'.$rent_begin[1];

                        $rent_end = explode('-', $tenancy_items_3[$i]['rent_end']);
                        $rent_end = $rent_end[0].'-'.$rent_end[1];

                        $this_month = \Carbon\Carbon::parse($month[$j]);
                        $first = \Carbon\Carbon::parse($rent_begin);
                        $second = \Carbon\Carbon::parse($rent_end);
                        $check = \Carbon\Carbon::parse($this_month)->between($first, $second);

                        $rentedDays = \Carbon\Carbon::parse($tenancy_items_3[$i]['rent_begin'])->diffInDays($tenancy_items_3[$i]['rent_end']);
                        $perDay = $tenancy_items_3[$i]['actual_net_rent'];

                        if($check == true){
                            if($month[$j] == $rent_end){
                                $endOfMonth = \Carbon\Carbon::parse($this_month)->endOfMonth();
                                $day = explode('-', $tenancy_items_3[$i]['rent_end']);
                                $diffInDays = $diffInDays = $day[2];
                                if($diffInDays == 0){
                                    $dataD = explode('-', $rent_end);
                                    $rented_amount = $perDay;
                                }
                                else{
                                    $rented_amount = $perDay/$diffInDays;
                                }
                            }
                            else{
                                $dataD = explode('-', $rent_end);
                                $rented_amount = $perDay;
                            }
                            $rent_data[$count][] =  $rented_amount;
                        }else{
                            $rent_data[$count][] = 0;
                        }
                    }
                    catch (\Exception $e){

                        echo $e->getMessage();
                    }
                }
                $count++;
            }


            $year_graph_array = array();
            foreach ($rent_data as $value_graph) {
                $year_graph_array[] = round(array_sum($value_graph),2);
            }



            array_shift($year_graph_array);
            $year_graph = $year_graph_array;

            Session::put('graph', json_encode($year_graph));
        }


        /*=====  End Dashboard Graph data ======*/


        if(isset($_GET['da'])){

            print_r($year_graph);
            die();
        }
        // echo "here"; die;

        $hausmax_properties =  Properties::groupBy('hausmaxx')
            ->whereNotNull('hausmaxx')
            ->whereNotNull('hausmaxeuro_monat')
            ->selectRaw('*, sum(hausmaxeuro_monat) as sumhausmaxeuro_monat, count(id) as properties_count')
            ->where('status',6)
            ->get();


        $g1 =  Properties::whereNotNull('gebaude_laufzeit_to')
            ->selectRaw('*')
            ->where('status',6)
            ->whereRaw('gebaude_laufzeit_to != "" and gebaude_laufzeit_to!=" "')
            ->where('gebaude_laufzeit_to','>=',date('Y-m-d'))
            ->where('gebaude_laufzeit_to','>=','2020-09-01')
            ->orderBy('gebaude_laufzeit_to','asc')
            ->get();

        $g2 =  Properties::whereNotNull('haftplicht_laufzeit_to')
            ->selectRaw('*')
            ->where('status',6)
            ->whereRaw('haftplicht_laufzeit_to != "" and haftplicht_laufzeit_to!=" "')
            ->where('haftplicht_laufzeit_to','>=',date('Y-m-d'))
            ->where('haftplicht_laufzeit_to','>=','2020-09-01')

            ->orderBy('haftplicht_laufzeit_to','asc')
            ->get();

        // print_r($g1);
        // print_r($g2); die;

        $tenancy_items_34 = TenancySchedule::leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->where('type',3)->orWhere('type', '=', 4)->get();

        $pa_array = $pa_array2 = array();

        $masterlistes = Masterliste::selectRaw('masterliste.*,properties.id as property_id')->join('properties','masterliste.id','=','properties.masterliste_id')
            ->where('Ist',0)->where('soll',0)
            ->get();

        // print_r($masterlistes); die;

        $master_array = array();
        foreach ($masterlistes as $key => $masterliste) {
            $master_array[$masterliste->id] = $masterliste->asset_manager;
        }

        foreach($tenancy_items_34 as $key => $tenancy_item){
            if($tenancy_item->tenancy_schedule_id){
                $properties = Properties::where('Ist',0)->where('soll',0)->where('id', $tenancy_item->property_id)->first();
                if ($properties != null && $properties->masterliste_id != null){
                    $tenancy_item->object_name  = $properties->name_of_property;
                    $tenancy_item->property_id  = $properties->id;

                    $singleComment = TenancyScheduleComment::where('item_id',$tenancy_item->id)->where('comment', '!=', '')->latest()->first();
                    if($singleComment && $singleComment->comment)
                        $tenancy_item->comment = $singleComment->comment;


                    if(isset($master_array[$properties->masterliste_id])){

                        $asset_manager = $master_array[$properties->masterliste_id];

                        // $asset_manager = $tenancy_item->asset_manager_id;

                        $asset_manager = $properties->asset_m_id;

                        $tenancy_item->creator_name = "";
                        if(isset($asset_name_array[$asset_manager]))
                            $tenancy_item->creator_name = $asset_name_array[$asset_manager];

                        if(isset($properties->asset_manager->name))
                            $tenancy_item->creator_name = $properties->asset_manager->name;


                    }else{
                        unset($tenancy_items_34[$key]);
                    }
                    if($tenancy_item->type == 4){
                        $tenancy_item->type = 'Gewerbe';
                    }elseif ($tenancy_item->type == 3){
                        $tenancy_item->type = 'Wohnen';
                    }



                }else{
                    unset($tenancy_items_34[$key]);
                }

            }
        }
        // print "<pre>";
        // print_r($tenancy_items_34); die();





        $ccomments = Comment::selectRaw('comments.*,properties.name_of_property,users.name')->join('properties','properties.main_property_id','=','comments.property_id')
            ->leftjoin('users','users.id','=','properties.asset_m_id')
            ->whereNotNull('comment')->where('standard_property_status',1)->where('comments.status',1)->get();

        if($user->second_role==4)
            $view_name = "dashboard.dashboard_assetmanager";




        $bankens = array();

        return view($view_name,compact('banks','logs',
            'g1','g2',
            'logo_users',
            'download_users',
            'displayMonth',
            'masterlistes',
            'ccomments',
            'bankens',
            'avg_wault',
            'summ_vermietet_percent',
            'summ_differenz_miete_in_euro',
            'comments','tenancy_items_2',
            'makler_user','activecount',
            'makler_userall','activecountall',
            'vmakler_user','vactivecount',
            'vmakler_userall','vactivecountall',
            'year_graph',
            // 'tenancy_schedule_data',
            'tenancy_items_34',
            'v_analytic',
            'asset_manager_array',
            'instacount',
            'analysis',
            'hausmax_properties'
        ));
    }

}
