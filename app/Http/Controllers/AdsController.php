<?php

namespace App\Http\Controllers;

use App\Ad;
use App\City;
use App\Country;
use App\ExportAd;
use App\Http\Requests;
use App\Models\ContactQuery;
use App\Models\Properties;
use App\Models\PropertiesDirectory;
use App\Models\PropertiesTenants;
use App\Models\PropertyComments;
use App\Models\SheetProperties;
use App\Models\TenancySchedule;
use App\Models\VacantSpace;
use App\Models\TenancyScheduleItem;
use App\Models\VermietungItem;
use App\Services\PropertiesService;
use App\Services\TenancySchedulesService;
use App\State;
use App\User;
use App\Vad;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use PDF;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Models\PropertySubfields;


//use App\Brand;
//use App\Category;
//use App\Media;
//use App\Payment;
//use App\Report_ad;
//use App\Sub_Category;


class AdsController extends Controller
{
    public function getPropertyInfo($id)
    {
        $arr['email'] = $arr['name'] = $arr['phone'] = "";
        $pr = Properties::find($id);

        if ($pr && isset($pr->asset_manager)) {
            $arr['email'] = $pr->asset_manager->email;
            $arr['name'] = $pr->asset_manager->name;
            $arr['phone'] = $pr->asset_manager->phone;
        }

        return $arr;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function postlisting($created_ad)
    // {
    //     $ads = Ad::with('city', 'country', 'state')->whereId($created_ad)->first()->toArray();

    //     if($ads)
    //     {
    //         $url = "https://verkauf.fcr-immobilien.de/saveIntranetlist";
    //         $curl = curl_init();
    //         curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    //         curl_setopt($curl, CURLOPT_HEADER, false);
    //         curl_setopt($curl, CURLOPT_URL, $url);
    //         curl_setopt($curl, CURLOPT_POST, 1);
    //         curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($ads));
    //         curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    //         $str = curl_exec($curl);
    //         curl_close($curl);
    //     }
    // }
    public function saveSoll($id, $prid)
    {
        $Properties_clone = Properties::find($prid);
        $Properties_New = $Properties_clone->replicate();
        $Properties_New->Ist = 0;
        $Properties_New->sheet_title = "";
        if ($Properties_clone->Ist)
            $Properties_New->soll = $Properties_clone->Ist;
        else
            $Properties_New->soll = $Properties_clone->soll;
        $Properties_New->properties_bank_id = $Properties_clone->properties_bank_id;
        $Properties_New->standard_property_status = 0;
        $Properties_New->user_id = Auth::user()->id;
        $Properties_New->for_bank_tab = 'yes';
        $Properties_New->save();

        $insert = array();

        $a = PropertiesTenants::where('propertyId', $prid)->get();
        foreach ($a as $extra) {
            $insert[] = array('propertyId' => $Properties_New->id, 'tenant' => $extra->tenant, 'net_rent_p_a' => $extra->net_rent_p_a, 'mv_end' => $extra->mv_end, 'mietvertrag_text' => $extra->mietvertrag_text, 'type' => $extra->type, 'flache' => $extra->flache, 'mv_end2' => $extra->mv_end2, 'is_dynamic_date' => $extra->is_dynamic_date,'is_current_net'=>$extra->is_current_net);
        }
        if ($insert)
            PropertiesTenants::insert($insert);


        $Properties_New = Properties::find($Properties_New->id);
        $input = $Properties_New->toArray();

        $list_fields_percent = [
            'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
        ];
        foreach ($list_fields_percent as $field) {
            if (isset($input[$field]))
                $input[$field] *= 100;
        }
        PropertiesService::update($input, $Properties_New);

        return back();
    }

    public function saveGSheet(Request $request)
    {
        // print_r($_POST); die;
        $value1 = $value = $value2 = $value3 = $Rental_area_in_m2= 0;
        if ($request->gesamt_in_eur) {
            $value = str_replace('.', '', $request->gesamt_in_eur);
            $value = str_replace(',', '.', $value);
            $value = str_replace('%', '', $value);
            $value = str_replace('€', '', $value);
        }
        if ($request->Rental_area_in_m2) {
            $Rental_area_in_m2 = str_replace('.', '', $request->Rental_area_in_m2);
            $Rental_area_in_m2 = str_replace(',', '.', $Rental_area_in_m2);
        }
        if ($request->maklerpreis) {
            $value3 = str_replace('.', '', $request->maklerpreis);
            $value3 = str_replace(',', '.', $value3);
            $value3 = str_replace('%', '', $value3);
            $value3 = str_replace('€', '', $value3);
        }
        if ($request->net_rent_pa) {
            $value2 = str_replace('.', '', $request->net_rent_pa);
            $value2 = str_replace(',', '.', $value2);
            $value2 = str_replace('%', '', $value2);
            $value2 = str_replace('€', '', $value2);
        }
        if ($request->real_estate_taxes) {
            $value1 = str_replace('.', '', $request->real_estate_taxes);
            $value1 = str_replace(',', '.', $value1);
        }

        $properties_subfields = PropertySubfields::where('property_id', $request->main_property_id)->first();
        if(!$properties_subfields){
            $properties_subfields = new PropertySubfields();
            $properties_subfields->property_id = $request->main_property_id;
            $properties_subfields->is_new_directory_strucure = 0;
        }
        $properties_subfields->location_latitude = $request->location_latitude;
        $properties_subfields->location_longitude = $request->location_longitude;
        $properties_subfields->save();


        $update_data1 = DB::table('properties')->whereRaw('(main_property_id=' . $request->main_property_id . ' OR id=' . $request->main_property_id.') and lock_status=0')
            ->update([
                "name_of_property" => $request->name_of_property,
                "plz_ort" => $request->plz_ort,
                "ort" => $request->ort,
                "strasse" => $request->strasse,
                "hausnummer" => $request->hausnummer,
                "niedersachsen" => $request->niedersachsen,
                "anbieterkontakt" => $request->anbieterkontakt,
                "firma" => $request->firma,
                "first_name" => $request->first_name,
                "net_rent_pa" => $value2,
                "maklerpreis" => $value3,
                "transaction_m_id" => $request->transaction_m_id,
                "investor_name" => $request->investor_name,
                "property_type" => $request->property_type,
                "notizen" => $request->notizen,
                "offer_from" => $request->offer_from,
                "Rental_area_in_m2" => $Rental_area_in_m2,
                "real_estate_taxes" => $value1 / 100,
                "is_not_an_existing" => (isset($request->is_not_an_existing)) ? 1 : 0,
            ]);

        $update_data1 = DB::table('properties')->whereRaw('Ist!=0 and lock_status=0 and  (main_property_id=' . $request->main_property_id . ' OR id=' . $request->main_property_id . ')')
            ->update([
                "gesamt_in_eur" => $value,
            ]);

        $s = 0;
        if ($request->niedersachsen) {
            $state = State::where('state_name', $request->niedersachsen)->first();
            if ($state)
                $s = $state->id;

        }


        $update_data = DB::table('vads')->whereRaw('property_id=' . $request->main_property_id)
            ->update([
                "type" => $request->property_type,
                "state_id" => $s
            ]);
        $update_data = DB::table('ads')->whereRaw('property_id=' . $request->main_property_id)
            ->update([
                "type" => $request->property_type,
                "state_id" => $s
            ]);
        $update_data = DB::table('export_ads')->whereRaw('property_id=' . $request->main_property_id)
            ->update([
                "type" => $request->property_type,
                "state_id" => $s
            ]);
        // if($update_data1){
        return redirect('properties/' . $request->main_property_id);
        // }
        // else{
        //     echo "issue in saving data";
        // }
    }

    public function importtenant(Request $request)
    {

        $this->validate($request, [
            'select_file' => 'required|mimes:xls,xlsx'
        ]);

        $path = $request->file('select_file')->getRealPath();

        $e = $request->file('select_file')->getClientOriginalExtension();
        $inputFileType = ucfirst($e);

        $inputFileType = "Xlsx";
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($path);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $v  =1;
        $key = 0;
        $arr = array();
        foreach ($sheetData as $k => $list) {
            if ($k > 1) {
            if (is_numeric($list['C'])) {
                $UNIX_DATE = ($list['C'] - 25569);
                $Date1 = '1970-01-01';
                $date = new \DateTime($Date1);
                $date->add(new \DateInterval('P' . $UNIX_DATE . 'D')); // P1D means a period of 1 day
                $list['C'] = $date->format('d.m.Y');
            }
            else{
                if($list['C'])
                {
                    $v = validatedate($list['C']);
                    if($v==0)
                        break;
                }
            }
            }
        }

        // echo $v; die;

        if($v==0)
        {

            return redirect('properties/' . $request->main_property_id.'?tab=sheet')->with('error', trans('Please select date with dd.mm.yyyy format'));
        }

        foreach ($sheetData as $k => $list) {
            if ($k > 1) {


                if (is_numeric($list['B']))
                    $a = $list['B'];
                else {
                    $a = str_replace('.', '', $list['B']);
                    $a = str_replace(',', '.', $a);
                    $a = str_replace('%', '', $a);
                    $a = str_replace('€', '', $a);
                }
                if (is_numeric($list['D']))
                    $b = $list['D'];
                else {
                    $b = str_replace('.', '', $list['D']);
                    $b = str_replace(',', '.', $b);
                    $b = str_replace('%', '', $b);
                    $b = str_replace('€', '', $b);
                }

                if (is_numeric($list['C'])) {
                    $UNIX_DATE = ($list['C'] - 25569);
                    $Date1 = '1970-01-01';
                    $date = new \DateTime($Date1);
                    $date->add(new \DateInterval('P' . $UNIX_DATE . 'D')); // P1D means a period of 1 day
                    $list['C'] = $date->format('d.m.Y');
                }

                // echo $a; echo $b; die;


                // if(date_format(  date_create(str_replace('.', '-', $list['C'])) , 'Y-m-d'))
                // {
                //     $rent_end = date_format(  date_create(str_replace('.', '-', $list['C'])) , 'Y-m-d');
                // }

                // echo $rent_end; die;

                $arr[$key]['propertyId'] = $request->property_id;
                $arr[$key]['tenant'] = $list['A'];
                $arr[$key]['net_rent_p_a'] = $a;
                $arr[$key]['mv_end'] = NULL;
                if($list['C'])
                $arr[$key]['mv_end'] = $list['C'];
                $arr[$key]['flache'] = $b;
                $arr[$key]['mietvertrag_text'] = $list['E'];
                $arr[$key]['is_dynamic_date'] = 1;

                $key += 1;
            }
        }

        DB::table('properties_tenants')->where('propertyId', $request->property_id)->delete();
        if ($arr) {
            PropertiesTenants::insert($arr);
        }
        return redirect('properties/' . $request->main_property_id.'?tab=sheet');

    }

    public function saveSheet(Request $request)
    {   error_reporting(E_ALL);
        $data = $request->all();
        $global_sheet = 0;
        if($request->global_sheet && $request->purchase_date!="")
            $global_sheet = 1;

        // print_r($data); die;

        $a1['tenant'] = array();
        if (isset($data['tenant']))
            $a1['tenant'] = $data['tenant'];
        if (isset($data['net_rent_p_a']))
            $a1['net_rent_p_a'] = $data['net_rent_p_a'];
        if (isset($data['mv_end']))
            $a1['mv_end'] = $data['mv_end'];
        if (isset($data['mv_end2']))
            $a1['mv_end2'] = $data['mv_end2'];
        if (isset($data['mietvertrag_text']))
            $a1['mietvertrag_text'] = $data['mietvertrag_text'];
        if (isset($data['type']))
            $a1['type'] = $data['type'];

        if (isset($data['flache']))
            $a1['flache'] = $data['flache'];

        if (isset($data['dynamic_list']) && $data['dynamic_list'])
            $a2['dynamic_list'] = explode(',', $data['dynamic_list']);

        if (isset($data['rent_list']) && $data['rent_list'])
            $a2['rent_list'] = explode(',', $data['rent_list']);

        if (isset($data['ank_1_list']) && $data['ank_1_list'])
            $a2['ank_1_list'] = explode(',', $data['ank_1_list']);

        if (isset($data['ank_2_list']) && $data['ank_2_list'])
            $a2['ank_2_list'] = explode(',', $data['ank_2_list']);

        if (isset($data['is_dynamic_date']))
            unset($data['is_dynamic_date']);

        if (isset($data['ank_1_list']))
            unset($data['ank_1_list']);

        if (isset($data['ank_2_list']))
            unset($data['ank_2_list']);

        if (isset($data['global_sheet']))
            unset($data['global_sheet']);

        // print_r($a1['dynamic_list']); die;

        $copy_data = 0;
        $main_property_id = 0;


        if (isset($data['copy_data'])) {
            $copy_data = 1;
            unset($data['copy_data']);
        }
        if (isset($data['main_property_id'])) {
            $main_property_id = $data['main_property_id'];
            unset($data['main_property_id']);
        }

        /*$update_data = DB::table('properties')->whereRaw('main_property_id=' . $main_property_id . ' OR id=' . $main_property_id)
            ->update([
                "quick_status" => 0,

            ]);*/


        $arr = array();
        foreach ($a1['tenant'] as $key => $value) {
            if ($a1['net_rent_p_a'][$key]) {
                $a1['net_rent_p_a'][$key] = str_replace('.', '', $a1['net_rent_p_a'][$key]);
                $a1['net_rent_p_a'][$key] = str_replace(',', '.', $a1['net_rent_p_a'][$key]);
                $a1['net_rent_p_a'][$key] = str_replace('%', '', $a1['net_rent_p_a'][$key]);
                $a1['net_rent_p_a'][$key] = str_replace('€', '', $a1['net_rent_p_a'][$key]);


                $a1['flache'][$key] = str_replace('.', '', $a1['flache'][$key]);
                $a1['flache'][$key] = str_replace(',', '.', $a1['flache'][$key]);
                $a1['flache'][$key] = str_replace('%', '', $a1['flache'][$key]);
                $a1['flache'][$key] = str_replace('€', '', $a1['flache'][$key]);

                $mv_end = '';
                if (date_format(date_create(str_replace('.', '-', $a1['mv_end'][$key])), 'Y-m-d')) {
                    $mv_end = date_format(date_create(str_replace('.', '-', $a1['mv_end'][$key])), 'Y-m-d');
                }


                $arr[$key]['propertyId'] = $data['property_id'];
                $arr[$key]['type'] = $a1['type'][$key];
                $arr[$key]['tenant'] = $a1['tenant'][$key];
                $arr[$key]['net_rent_p_a'] = $a1['net_rent_p_a'][$key];
                $arr[$key]['mv_end'] = $mv_end;
                $arr[$key]['mv_end2'] = $a1['mv_end2'][$key];
                $arr[$key]['mietvertrag_text'] = $a1['mietvertrag_text'][$key];
                $arr[$key]['is_dynamic_date'] = $a2['dynamic_list'][$key];
                $arr[$key]['flache'] = $a1['flache'][$key];
                $arr[$key]['is_current_net'] = $a2['rent_list'][$key];
                $arr[$key]['ank_1'] = (isset($a2['ank_1_list'][$key])) ? $a2['ank_1_list'][$key] : 0;
                $arr[$key]['ank_2'] = (isset($a2['ank_2_list'][$key])) ? $a2['ank_2_list'][$key] : 0;

            }
        }
        DB::table('properties_tenants')->where('propertyId', $data['property_id'])->delete();
        if ($arr) {
            PropertiesTenants::insert($arr);

            if ($copy_data && $main_property_id) {


                $tenancy_schedule = TenancySchedule::where('property_id', $main_property_id)->first();
                if (!$tenancy_schedule) {
                    $input = [
                        'object' => 'New Tenancy Schedule',
                        'creator_id' => Auth::id(),
                        'property_id' => $main_property_id
                    ];
                    $tenancy_schedule = TenancySchedulesService::create($input);

                }

                DB::table('tenancy_schedule_items')->where('tenancy_schedule_id', $tenancy_schedule->id)->delete();


                $properties = Properties::select('asset_m_id')->find($main_property_id);
                $asset_magager_id = 0;
                if ($properties)
                    $asset_magager_id = $properties->asset_m_id;

                $input = array();

                foreach ($arr as $key => $value) {

                    $rent_end = '';
                    if (date_format(date_create(str_replace('.', '-', $value['mv_end'])), 'Y-m-d')) {
                        $rent_end = date_format(date_create(str_replace('.', '-', $value['mv_end'])), 'Y-m-d');
                    }

                    $common_item_id = 1;
                    $i = TenancyScheduleItem::max('common_item_id');
                    if($i)
                        $common_item_id = $i+1;


                    $input = [
                        'tenancy_schedule_id' => $tenancy_schedule->id,
                        'type' => $value['type'],
                        'name' => $value['tenant'],
                        'common_item_id'=>$common_item_id,
                        'rental_space' => $value['flache'],
                        'actual_net_rent' => $value['net_rent_p_a'] / 12,
                        'comment' => $value['mietvertrag_text'],
                        'rent_end' => $rent_end,
                        'asset_manager_id' => $asset_magager_id
                    ];
                    TenancySchedulesService::createItem($input);
                }


            }

        }


        $data['tenants'] = json_encode($arr);

        unset($data['_token']);
        if (isset($data['mv_end']))
            unset($data['mv_end']);
        if (isset($data['mv_end2']))
            unset($data['mv_end2']);
        if (isset($data['tenant']))
            unset($data['tenant']);
        if (isset($data['net_rent_p_a']))
            unset($data['net_rent_p_a']);
        if (isset($data['mietvertrag_text']))
            unset($data['mietvertrag_text']);
        if (isset($data['type']))
            unset($data['type']);
        if (isset($data['dynamic_list']))
            unset($data['dynamic_list']);

        if (isset($data['flache']))
            unset($data['flache']);

        if (isset($data['rent_list']))
            unset($data['rent_list']);

        if (isset($data['ank_1_list']))
            unset($data['ank_1_list']);

        if (isset($data['ank_2_list']))
            unset($data['ank_2_list']);

        if (isset($data['ank_1']))
            unset($data['ank_1']);

        if (isset($data['ank_2']))
            unset($data['ank_2']);

        $c_array = array('name_of_property', 'strasse', 'plz_ort', 'hausnummer', 'ort', 'niedersachsen', 'exklusivität_bis', 'duration_from_O38', 'tenants', 'ankermieter_option1', 'ankermieter_option2', 'miete_text', 'construction_year_note', 'plot_of_land_m2_text', 'position', 'Ankermieter_note', 'datum_lol', 'purchase_date', 'purchase_date2', 'sheet_title');

        $obj = SheetProperties::where('property_id', $data['property_id'])->first();

        $list_fields_percent = [
            'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
        ];
        // $input[$field] *= 100;


        foreach ($data as $key => $value) {

            if ($key == "duration_from_O38" && $value) {
                if (strtotime(str_replace('.', '-', $value)))
                    $data[$key] = date('Y-m-d', strtotime(str_replace('.', '-', $value)));
                elseif (strtotime($value))
                    $data[$key] = date('Y-m-d', strtotime($value));
                else
                    $data[$key] = "";
            }
            if ($key == "exklusivität_bis" && $value) {
                if (strtotime(str_replace('.', '-', $value)))
                    $data[$key] = date('Y-m-d', strtotime(str_replace('.', '-', $value)));
                elseif (strtotime($value))
                    $data[$key] = date('Y-m-d', strtotime($value));
                else
                    $data[$key] = "";
            }
            if ($key == "datum_lol" && $value) {
                if (strtotime(str_replace('.', '-', $value)))
                    $data[$key] = date('Y-m-d', strtotime(str_replace('.', '-', $value)));
                elseif (strtotime($value))
                    $data[$key] = date('Y-m-d', strtotime($value));
                else
                    $data[$key] = "";
            }
            if ($key == "purchase_date" && $value) {
                if (strtotime(str_replace('.', '-', $value)))
                    $data[$key] = date('Y-m-d', strtotime(str_replace('.', '-', $value)));
                elseif (strtotime($value))
                    $data[$key] = date('Y-m-d', strtotime($value));
                else
                    $data[$key] = "";
            } else if (!$value && !in_array($key, $c_array))
                $data[$key] = 0;
            else if ($value && !in_array($key, $c_array)) {
                if ($key != "real_estate_taxes")
                    $value = str_replace('.', '', $value);
                $value = str_replace(',', '.', $value);
                $value = str_replace('%', '', $value);
                $value = str_replace('€', '', $value);
                $data[$key] = $value;
                // $value = $value;
            }
        }


        if ($obj) {
            $data['updated_at'] = date('Y-m-d H:i:s');
            DB::table('sheetproperties')->where('id', $obj->id)->update($data);
            // $obj->update($data);
        } else {
            $data['created_at'] = date('Y-m-d H:i:s');
            SheetProperties::insert($data);
        }
        $cm = PropertyComments::where('property_id', $data['property_id'])->first();
        if ($cm) {
            $cm->Ankermieter_note = $data['Ankermieter_note'];
            $cm->save();
        } else {
            $cm = new PropertyComments;
            $cm->property_id = $data['property_id'];
            $cm->Ankermieter_note = $data['Ankermieter_note'];
            $cm->save();
        }
        unset($data['Ankermieter_note']);


        $prs = Properties::whereRaw('id=' . $data['property_id'])->get();

        unset($data['tenants']);
        unset($data['property_id']);
        unset($data['created_at']);
        unset($data['updated_at']);

        // print_r($data['real_estate_taxes']); die;
        foreach ($prs as $key => $properties) {

            if($global_sheet && $properties->purchase_date !=$data['purchase_date'])
            {
                $url = route('properties.show',['property'=>$properties->main_property_id]).'?tab=sheet';
                $url = '<a href="'.$url.'">'.$url.'</a>';
                $name = "";
                $replyemail = "";
                if(isset($properties->transaction_manager) && isset($properties->transaction_manager->name)){
                    $name = $properties->transaction_manager->name;
                    $replyemail = $properties->transaction_manager->email;
                }

                $text = "Hallo Janine, ".$name." hat den Notartermin am ".$properties->purchase_date." für das Objekt ".$properties->plz_ort.' '.$properties->ort." eingetragen: ".$url;


                // $email = "j.klausch@fcr-immobilien.de";
                // $email = "c.wiedemann@fcr-immobilien.de";
                $subject = "Notartermin  ".$properties->plz_ort.' '.$properties->ort;
                // Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                //     $message->to($email)
                //     ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                //     ->replyTo($replyemail,$name)
                //     ->subject($subject);
                // });
            }

            $input = $properties;
            $list_fields_percent = [
                'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
            ];
            foreach ($list_fields_percent as $field) {
                if (isset($input[$field]))
                    $input[$field] *= 100;
            }

            foreach ($data as $key => $value) {
                $input[$key] = $value;
            }

            // print_r($input['real_estate_taxes']); die;



            // $property_sheet->plz_ort = $properties->plz_ort;
            // $property_sheet->ort = $properties->ort;
            // $property_sheet->strasse = $properties->strasse;
            // $property_sheet->hausnummer = $properties->hausnummer;
            // $property_sheet->niedersachsen = $properties->niedersachsen;
            // echo $properties->purchase_date;
            // echo $input['purchase_date'];
            PropertiesService::update($input, $properties);



            Properties::where('id', $properties->main_property_id)->update(['plz_ort' => $data['plz_ort'], 'ort' => $data['ort'], 'strasse' => $data['strasse'], 'hausnummer' => $data['hausnummer'], 'niedersachsen' => $data['niedersachsen'],'properties_bank_id'=> $data['properties_bank_id']]);
        }


        $arr['status'] = 1;
        $arr['message'] = "saved";


        return redirect('properties/' . $properties->main_property_id);

        // return $arr;
    }

    public function set_ad_image_favourite()
    {
        $check = Ad::where('id', $_REQUEST['id'])->first();
        $check->fav_image = $_REQUEST['key'];
        $check->save();

        echo "1";
        die;
    }

    public function set_vacantitem_image_favourite()
    {
        $check = VacantSpace::where('id', $_REQUEST['id'])->first();
        $check->favourite = $_REQUEST['key'];
        $check->save();

        echo "1";
        die;
    }

    public function set_vacant_media_favourite()
    {
        $check = VermietungItem::where('id', $_REQUEST['id'])->first();

        if ($check->is_favourite) {
            echo 0;
            $check->is_favourite = 0;
            $check->save();
        } else {
            VermietungItem::where('item_id', $check->item_id)->update(['is_favourite' => 0]);
            $check->is_favourite = 1;
            $check->save();
            echo 1;
        }
        die;

    }

    public function set_export_media_favourite(Request $request)
    {
        $exportad = ExportAd::where('id', $request->id)->first();
        if ($exportad) {
            $exportad->image_title = $request->key;
            $exportad->save();
        }
        echo "1";
        die;

    }

    public function update_image_order(Request $request)
    {
            // print_r($_REQUEST);die;
        $i = 0;
        $exportad = ExportAd::where('property_id', $request->property_id)->first();
        if($exportad)
        {
            $arr = explode(',', $exportad->images);
            if($exportad->image_title)
            $i = $exportad->image_title;

            if(!isset($arr[$i]))
                $i = 0;

            $fv_img = $arr[$i];

            $exportad->images = implode(',', $_POST['i_arr']);
            $exportad->image_title = array_search($fv_img,$_POST['i_arr']);
            $exportad->save();
            $i = $exportad->image_title;

        }
        echo $i; die;
    }
    public function update_image_order1(Request $request)
    {
            // print_r($_REQUEST);die;
        $i = 0;
        $exportad = Ad::where('property_id', $request->property_id)->first();
        if($exportad)
        {
            $arr = explode(',', $exportad->images);
            if($exportad->fav_image)
            $i = $exportad->fav_image;

            if(!isset($arr[$i]))
                $i = 0;

            $fv_img = $arr[$i];

            $exportad->images = implode(',', $_POST['i_arr']);
            $exportad->fav_image = array_search($fv_img,$_POST['i_arr']);
            $exportad->save();
            $i = $exportad->fav_image;

        }
        echo $i; die;
    }

    public function index()
    {
        $title = trans('app.all_ads');
        $ads = Ad::with('city', 'country', 'state')->whereStatus(1)->orderBy('id', 'desc')->paginate(20);

        return view('admin.all_ads', compact('title', 'ads'));
    }

    public function add_new_vacant(Request $request)
    {
        if ($request->delete) {
            $modal = VacantSpace::where('id', $request->id)->first();
            $modal->delete();


            $url = "https://vermietung.fcr-immobilien.de/deleteCustomVacantspace?id=" . $request->id;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $str = curl_exec($curl);
            curl_close($curl);

            $arr['message'] = "deleted" . $str;
            return $arr;
            die;
        }
        $modal = new VacantSpace;
        $modal->property_id = $request->property_id;
        $modal->title = "Title";
        $modal->save();

        $arr['message'] = "added";
        return $arr;
        die;
    }

    public function saveVacantSpace(Request $request)
    {
        $arr['message'] = "saved";
        $arr['image'] = "";
        $arr['pdf'] = "";
        $old = array();


        //

        $modal = new VacantSpace;
        if ($request->vacant_id) {
            $v = VacantSpace::where('id', $request->vacant_id)->first();
            if ($v) {
                $modal = $v;
                // if($v->image)
                // $old = explode(',',$v->image);
            }
        }


        if ($request->images)
            $old = $request->images;

        $oldpdf = "";
        if ($request->pdfs)
            $oldpdf = $request->pdfs;

        $oldplanpdf = "";
        if ($request->planpdfs)
            $oldplanpdf = $request->planpdfs;


        $modal->property_id = $request->property_id;
        $modal->title = $request->title;
        $modal->description = $request->description;
        if ($request->category)
            $modal->category = implode(',', $request->category);
        $modal->size = $request->size;
        $modal->price = $request->price;


        if ($request->file('image')) {
            $arr['sizeimage'] = $request->file('image')->getSize() / 1024;
            $maxfilesize = 4 * 1024;

            if ($arr['sizeimage'] < $maxfilesize) {

                $extension = $request->file('image')->getClientOriginalExtension();
                $dir = 'ad_files_upload/';
                $pdf_filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('image')->move($dir, $pdf_filename);
                $old[] = $pdf_filename;


            } else {
                $arr['message'] = 'Max upload file size is 4MB';
            }
        }
        $modal->image = NULL;
        if ($old) {
            $modal->image = implode(',', $old);
        }
        $modal->pdf = $oldpdf;
        $modal->planpdf = $oldplanpdf;


        if ($request->file('pdf')) {
            $arr['sizepdf'] = $request->file('pdf')->getSize() / 1024;
            $maxfilesize = 4 * 1024;

            if ($arr['sizepdf'] < $maxfilesize) {
                $extension = $request->file('pdf')->getClientOriginalExtension();
                $dir = 'ad_files_upload/';
                $pdf_filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('pdf')->move($dir, $pdf_filename);

                $modal->pdf = $pdf_filename;

                $arr['pdf'] = '<div class="creating-ads-img-wrap media-common-class">
                            <a href="' . asset('ad_files_upload/' . $modal->pdf) . '" class="img-responsive" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
                            <div class="img-action-wrap" id="' . $modal->id . '">
                                <a href="javascript:;" class="imgDeleteBtn deletevermitund"><i class="fa fa-trash-o"></i> </a>
                            </div>
                        </div>';
            } else {
                $arr['message'] = 'Max upload file size is 4MB';
            }

        }

        if ($request->file('planpdf')) {
            $arr['sizeplanpdf'] = $request->file('planpdf')->getSize() / 1024;

            $maxfilesize = 4 * 1024;

            if ($arr['sizeplanpdf'] < $maxfilesize) {
                $extension = $request->file('planpdf')->getClientOriginalExtension();
                $dir = 'ad_files_upload/';
                $pdf_filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('planpdf')->move($dir, $pdf_filename);

                $modal->planpdf = $pdf_filename;

                $arr['planpdf'] = '<div class="creating-ads-img-wrap media-common-class">
                            <a href="' . asset('ad_files_upload/' . $modal->planpdf) . '" class="img-responsive" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
                            <div class="img-action-wrap" id="' . $modal->id . '">
                                <a href="javascript:;" class="imgDeleteBtn deletevermitund"><i class="fa fa-trash-o"></i> </a>
                            </div>
                        </div>';

            } else {
                $arr['message'] = 'Max upload file size is 4MB';
            }


        }
        $modal->save();
        $arr['vacant_id'] = $modal->id;

        if ($request->upload) {
            $s = $this->postCustomVacantspace($modal->id);
            // print_r($s); die;
        }

        foreach ($old as $key => $value) {

            if ($modal->is_favourite == 0)
                $arr['image'] .= '<div class="creating-ads-img-wrap media-common-class">
                            <img src="' . asset('ad_files_upload/' . $value) . '" class="img-responsive" />
                            <div class="img-action-wrap" id="' . $modal->id . '" data-key="' . $key . '">
                                <a href="javascript:;" class="imgDeleteBtn deletevacant"><i class="fa fa-trash-o"></i> </a>

                                <a href="javascript:;" class="setasfavourite" data-key="' . $key . '" data-id="' . $modal->id . '"><i class="fa fa-star-o"></i></a>
                            </div>
                        </div>';
            else
                $arr['image'] .= '<div class="creating-ads-img-wrap media-common-class">
                            <img src="' . asset('ad_files_upload/' . $value) . '" class="img-responsive" />
                            <div class="img-action-wrap" id="' . $modal->id . '" data-key="' . $key . '">
                                <a href="javascript:;" class="imgDeleteBtn deletevacant"><i class="fa fa-trash-o"></i> </a>
                                <a href="javascript:;" class="setasfavourite" data-key="' . $key . '" data-id="' . $modal->id . '"><i class="fa fa-star"></i></a>
                                
                            </div>
                        </div>';
            # code...
        }


        return $arr;


    }

    public function saveVacantdata(Request $request)
    {
        $arr['message'] = "saved";
        $arr['image'] = "";
        $arr['pdf'] = "";
        // print_r($_FILES); die;


        if ($request->item_id) {
            $item_id = $request->item_id;


            $modal = new VermietungItem;
            $check = VermietungItem::where('item_id', $item_id)->where('name', 'title')->first();
            if ($check) {
                $modal = $check;
            }
            $modal->item_id = $item_id;
            $modal->property_id = $request->property_id;
            $modal->name = "title";
            $modal->value = $request->title;
            $modal->save();


            $modal = new VermietungItem;
            $check = VermietungItem::where('item_id', $item_id)->where('name', 'description')->first();
            if ($check) {
                $modal = $check;
            }
            $modal->item_id = $item_id;
            $modal->property_id = $request->property_id;
            $modal->name = "description";
            $modal->value = $request->description;
            $modal->save();

            $modal = new VermietungItem;
            $check = VermietungItem::where('item_id', $item_id)->where('name', 'size')->first();
            if ($check) {
                $modal = $check;
            }
            $modal->item_id = $item_id;
            $modal->property_id = $request->property_id;
            $modal->name = "size";
            $modal->value = $request->size;
            $modal->save();

            $modal = new VermietungItem;
            $check = VermietungItem::where('item_id', $item_id)->where('name', 'price')->first();
            if ($check) {
                $modal = $check;
            }
            $modal->item_id = $item_id;
            $modal->property_id = $request->property_id;
            $modal->name = "price";
            $modal->value = $request->price;
            $modal->save();


            /*$modal = new VermietungItem;
            $check = VermietungItem::where('item_id',$item_id)->where('name','price_per_meter')->first();
            if($check)
            {
                $modal = $check;
            }
            $modal->item_id = $item_id;
            $modal->property_id = $request->property_id;
            $modal->name = "price_per_meter";
            $modal->value = $request->price_per_meter;
            $modal->save();*/

            $modal = new VermietungItem;
            $check = VermietungItem::where('item_id', $item_id)->where('name', 'category')->first();
            if ($check) {
                $modal = $check;
            }
            $modal->item_id = $item_id;
            $modal->property_id = $request->property_id;
            $modal->name = "category";
            if($request->category)
            $modal->value = implode(',', $request->category);
            $modal->save();


            if ($request->file('image')) {
                $arr['sizeimage'] = $request->file('image')->getSize() / 1024;
                $maxfilesize = 4 * 1024;

                if ($arr['sizeimage'] < $maxfilesize) {

                    $extension = $request->file('image')->getClientOriginalExtension();
                    $dir = 'ad_files_upload/';
                    $pdf_filename = uniqid() . '_' . time() . '.' . $extension;
                    $request->file('image')->move($dir, $pdf_filename);

                    $modal = new VermietungItem;
                    $modal->item_id = $item_id;
                    $modal->property_id = $request->property_id;
                    $modal->name = "image";
                    $modal->value = $pdf_filename;
                    $modal->save();
                } else {
                    $arr['message'] = 'Max upload file size is 4MB';
                }


            }


            if ($request->file('pdf')) {
                $arr['sizepdf'] = $request->file('pdf')->getSize() / 1024;
                $maxfilesize = 4 * 1024;

                if ($arr['sizepdf'] < $maxfilesize) {
                    $extension = $request->file('pdf')->getClientOriginalExtension();
                    $dir = 'ad_files_upload/';
                    $pdf_filename = uniqid() . '_' . time() . '.' . $extension;
                    $request->file('pdf')->move($dir, $pdf_filename);

                    $modal = new VermietungItem;
                    $check = VermietungItem::where('item_id', $item_id)->where('name', 'pdf')->first();
                    if ($check) {
                        $modal = $check;
                    }
                    $modal->item_id = $item_id;
                    $modal->property_id = $request->property_id;
                    $modal->name = "pdf";
                    $modal->value = $pdf_filename;
                    $modal->save();

                    $arr['pdf'] = '<div class="creating-ads-img-wrap media-common-class">
                            <a href="' . asset('ad_files_upload/' . $modal->value) . '" class="img-responsive" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
                            <div class="img-action-wrap" id="' . $modal->id . '">
                                <a href="javascript:;" class="imgDeleteBtn deletevermitund"><i class="fa fa-trash-o"></i> </a>
                            </div>
                        </div>';
                } else {
                    $arr['message'] = 'Max upload file size is 4MB';
                }

            }

            if ($request->file('planpdf')) {
                $arr['sizeplanpdf'] = $request->file('planpdf')->getSize() / 1024;

                $maxfilesize = 4 * 1024;

                if ($arr['sizeplanpdf'] < $maxfilesize) {
                    $extension = $request->file('planpdf')->getClientOriginalExtension();
                    $dir = 'ad_files_upload/';
                    $pdf_filename = uniqid() . '_' . time() . '.' . $extension;
                    $request->file('planpdf')->move($dir, $pdf_filename);

                    $modal = new VermietungItem;
                    $check = VermietungItem::where('item_id', $item_id)->where('name', 'planpdf')->first();
                    if ($check) {
                        $modal = $check;
                    }
                    $modal->item_id = $item_id;
                    $modal->property_id = $request->property_id;
                    $modal->name = "planpdf";
                    $modal->value = $pdf_filename;
                    $modal->save();

                    $arr['planpdf'] = '<div class="creating-ads-img-wrap media-common-class">
                            <a href="' . asset('ad_files_upload/' . $modal->value) . '" class="img-responsive" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
                            <div class="img-action-wrap" id="' . $modal->id . '">
                                <a href="javascript:;" class="imgDeleteBtn deletevermitund"><i class="fa fa-trash-o"></i> </a>
                            </div>
                        </div>';

                } else {
                    $arr['message'] = 'Max upload file size is 4MB';
                }


            }

            $check = VermietungItem::where('item_id', $item_id)->where('name', 'image')->get();

            foreach ($check as $key => $modal) {

                if ($modal->is_favourite == 0)
                    $arr['image'] .= '<div class="creating-ads-img-wrap media-common-class">
                            <img src="' . asset('ad_files_upload/' . $modal->value) . '" class="img-responsive" />
                            <div class="img-action-wrap" id="' . $modal->id . '">
                                <a href="javascript:;" class="imgDeleteBtn deletevermitund"><i class="fa fa-trash-o"></i> </a>

                                <a href="javascript:;" class="setasfavourite" data-id="' . $modal->id . '"><i class="fa fa-star-o"></i></a>
                            </div>
                        </div>';
                else
                    $arr['image'] .= '<div class="creating-ads-img-wrap media-common-class">
                            <img src="' . asset('ad_files_upload/' . $modal->value) . '" class="img-responsive" />
                            <div class="img-action-wrap" id="' . $modal->id . '">
                                <a href="javascript:;" class="imgDeleteBtn deletevermitund"><i class="fa fa-trash-o"></i> </a>
                                <a href="javascript:;" class="setasfavourite" data-id="' . $modal->id . '"><i class="fa fa-star"></i></a>
                                
                            </div>
                        </div>';
                # code...
            }

        }


        if (isset($_REQUEST['upload']) && $_REQUEST['upload']) {
            $this->postVacantspace($request->property_id, $item_id);
        }

        return $arr;

    }

    public function removeVacantMedia()
    {

        $check = VermietungItem::where('id', $_REQUEST['id'])->delete();
        echo "1";
        die;

    }

    public function rashid()
    {
        # die('rashid ');
        $banks = DB::table('banks')->select('name')->where('id', '>', 403)->groupBy('name')->get();

        foreach ($banks as $bank) {

            $find_banks = DB::table('banks')->where('name', $bank->name)->get();
            if (count($find_banks) > 1) {

                $data = array();
                $internet = '';
                $keyword = '';
                $keyword2 = '';
                $fax = '';
                $district = '';
                $city = '';
                $postal_code = '';
                $salutation = '';
                $firstName = '';
                $surname = '';
                $contact_phone = '';
                $contact_email = '';
                $address = '';
                $notes = '';
                $fulltName = '';

                $data['name'] = $bank->name;
                foreach ($find_banks as $find_bank) {

                    if (!is_null($find_bank->internet)) {
                        $internet = $internet . ' || ' . $find_bank->internet;
                    }

                    if (!is_null($find_bank->keyword)) {
                        $keyword = $keyword . ' || ' . $find_bank->keyword;
                    }

                    if (!is_null($find_bank->keyword2)) {
                        $keyword2 = $keyword2 . ' || ' . $find_bank->keyword2;
                    }


                    if (!is_null($find_bank->fax)) {
                        $fax = $fax . ' || ' . $find_bank->fax;
                    }

                    if (!is_null($find_bank->district)) {
                        $district = $district . ' || ' . $find_bank->district;
                    }

                    if (!is_null($find_bank->city)) {
                        $city = $city . ' || ' . $find_bank->city;
                    }

                    if (!is_null($find_bank->postal_code)) {
                        $postal_code = $postal_code . ' || ' . $find_bank->postal_code;
                    }

                    if (!is_null($find_bank->salutation)) {
                        $salutation = $salutation . ' || ' . $find_bank->salutation;
                    }

                    if (!is_null($find_bank->firstName) && !is_null($find_bank->surname)) {
                        $fulltName = $fulltName . ' || ' . $find_bank->firstName . ' ' . $find_bank->surname;
                    }


                    if (!is_null($find_bank->firstName)) {
                        $firstName = $firstName . ' || ' . $find_bank->firstName;
                    }

                    if (!is_null($find_bank->surname)) {
                        $surname = $surname . ' || ' . $find_bank->surname;
                    }

                    if (!is_null($find_bank->contact_phone)) {
                        $contact_phone = $contact_phone . ' || ' . $find_bank->contact_phone;
                    }

                    if (!is_null($find_bank->contact_email)) {
                        $contact_email = $contact_email . ' || ' . $find_bank->contact_email;
                    }

                    if (!is_null($find_bank->address)) {
                        $address = $address . ' || ' . $find_bank->address;
                    }

                    if (!is_null($find_bank->notes)) {
                        $notes = $notes . ' || ' . $find_bank->notes;
                    }

                }
                $data['internet'] = ltrim($internet, ' || ');
                $data['keyword'] = ltrim($keyword, ' || ');
                $data['keyword2'] = ltrim($keyword2, ' || ');
                $data['fax'] = ltrim($fax, ' || ');
                $data['district'] = ltrim($district, ' || ');
                $data['city'] = ltrim($city, ' || ');
                $data['postal_code'] = ltrim($postal_code, ' || ');
                $data['salutation'] = ltrim($salutation, ' || ');
                $data['firstName'] = ltrim($firstName, ' || ');
                $data['surname'] = ltrim($surname, ' || ');
                $data['contact_phone'] = ltrim($contact_phone, ' || ');
                $data['contact_email'] = ltrim($contact_email, ' || ');
                $data['address'] = ltrim($address, ' || ');
                $data['notes'] = ltrim($notes, ' || ');
                $data['user_id'] = 2;
                $data['fullName'] = ltrim($fulltName, ' || ');;


                DB::table('banks')->where('name', $bank->name)->delete();
                DB::table('banks')->insert($data);


            }

        }

    }


    public function adminPendingAds()
    {
        $title = trans('app.pending_ads');
        $ads = Ad::with('city', 'country', 'state')->whereStatus(0)->orderBy('id', 'desc')->paginate(20);

        return view('admin.all_ads', compact('title', 'ads'));
    }

    public function adminBlockedAds()
    {
        $title = trans('app.blocked_ads');
        $ads = Ad::with('city', 'country', 'state')->whereStatus(2)->orderBy('id', 'desc')->paginate(20);

        return view('admin.all_ads', compact('title', 'ads'));
    }

    public function myAds()
    {
        $title = trans('app.my_ads');

        $user = Auth::user();
        $ads = $user->ads()->with('city', 'country', 'state')->orderBy('id', 'desc')->paginate(20);

        return view('admin.my_ads', compact('title', 'ads'));
    }

    public function pendingAds()
    {
        $title = trans('app.my_ads');

        $user = Auth::user();
        $ads = $user->ads()->whereStatus(0)->with('city', 'country', 'state')->orderBy('id', 'desc')->paginate(20);

        return view('admin.pending_ads', compact('title', 'ads'));
    }

    public function favoriteAds()
    {
        $title = trans('app.favourite_ads');

        $user = Auth::user();
        $ads = $user->favourite_ads()->with('city', 'country', 'state')->orderBy('id', 'desc')->paginate(20);

        return view('admin.favourite_ads', compact('title', 'ads'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::user()->id;
        $title = trans('app.post_an_ad');
        $categories = Category::all();
        $distances = Brand::all();
        $countries = Country::all();
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();

        $previous_states = State::where('country_id', old('country'))->get();
        $previous_cities = City::where('state_id', old('state'))->get();


        return view('admin.create_ad', compact('title', 'categories', 'countries', 'ads_images', 'distances', 'previous_states', 'previous_cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_id = Auth::user()->id;
        $title = $request->ad_title;

        if ($request->vermietung)
            $ad = Vad::where('property_id', $request->property_id)->first();
        else
            $ad = Ad::where('property_id', $request->property_id)->first();

        // if($input['status']==11)
        // {
        //       $this->postlisting($ad->id);
        // }
        $property = Properties::where('id', $request->property_id)->where('Ist', 0)->where('soll', 0)->first();


        $is_negotialble = $request->negotiable ? $request->negotiable : 0;
        $mark_ad_urgent = $request->mark_ad_urgent ? $request->mark_ad_urgent : 0;
        $video_url = $request->video_url ? $request->video_url : '';

        $amenities = serialize($request->amenities);
        $distances = serialize($request->distances);

        $i = $p = $p1 = "";

        if ($request->img) {
            $i = implode(',', $request->img);
        }
        if ($request->ipdf) {
            $p = implode(',', $request->ipdf);
        }
        if ($request->vipdf) {
            $p1 = implode(',', $request->vipdf);
        }


        if ($request->hasFile('images')) {
//            $validator = Validator::make($request->all(),
//                [
//                    'images.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
//                ]);
//            if ($validator->fails()) {
//                return response()->json(array(
//                    'alert_type' => 'danger',
//                    'msg_text' => $validator->errors()
//                ));
//            }

            $upload_path = 'ad_files_upload/';
            $file_for_db = '';
            foreach ($request->images as $file) {
                $extension = $file->getClientOriginalExtension();
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $file->move($upload_path, $filename);
                $file_for_db .= $filename . ',';

            }
            $file_for_db = substr($file_for_db, 0, -1);
            if ($i)
                $file_for_db .= ',' . $i;
        } else {
            $file_for_db = $i;
        }

        if ($request->hasFile('pdf')) {

            $attributeNames = array(
                'pdf.*' => 'API File',
            );
            $validator = Validator::make($request->all(),
                [
                    'pdf.*' => 'mimes:pdf|max:2048',
                ]);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {
                return response()->json(array(
                    'alert_type' => 'danger',
                    'msg_text' => $validator->errors()->first()
                ));
            }

            $extension = $request->file('pdf')->getClientOriginalExtension();
            $dir = 'ad_files_upload/';
            $pdf_filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pdf')->move($dir, $pdf_filename);

            if ($p)
                $pdf_filename .= ',' . $p;

        } else {
            $pdf_filename = $p;
        }


        if ($request->hasFile('vpdf')) {

            $attributeNames = array(
                'vpdf.*' => 'API File',
            );
            $validator = Validator::make($request->all(),
                [
                    'vpdf.*' => 'mimes:pdf|max:2048',
                ]);
            $validator->setAttributeNames($attributeNames);
            if ($validator->fails()) {
                return response()->json(array(
                    'alert_type' => 'danger',
                    'msg_text' => $validator->errors()->first()
                ));
            }

            $extension = $request->file('vpdf')->getClientOriginalExtension();
            $dir = 'ad_files_upload/';
            $vpdf_filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('vpdf')->move($dir, $vpdf_filename);

            if ($p)
                $vpdf_filename .= ',' . $p1;

        } else {
            $vpdf_filename = $p1;

        }

        if ($request->ad_city_name) {
            $duplicate = City::whereCityName($request->ad_city_name)->first();
            if ($duplicate)
                $request->city = $duplicate->id;
            else {
                $data = [
                    'city_name' => $request->ad_city_name,
                    'state_id' => $request->state,
                ];

                $s = City::create($data);
                $request->city = $s->id;
            }
        }

        if ($request->price) {
            $request->price = str_replace('.', '', $request->price);
            $request->price = str_replace(',', '.', $request->price);
        }
        if ($request->price_per_unit) {
            $request->price_per_unit = str_replace('.', '', $request->price_per_unit);
            $request->price_per_unit = str_replace(',', '.', $request->price_per_unit);
        }
        if ($request->square_unit_space) {
            $request->square_unit_space = str_replace('.', '', $request->square_unit_space);
            $request->square_unit_space = str_replace(',', '.', $request->square_unit_space);
        }
        if ($request->rentable_area) {
            $request->rentable_area = str_replace('.', '', $request->rentable_area);
            $request->rentable_area = str_replace(',', '.', $request->rentable_area);
        }
        if ($request->living_space) {
            $request->living_space = str_replace('.', '', $request->living_space);
            $request->living_space = str_replace(',', '.', $request->living_space);
        }
        if ($request->commercial_space) {
            $request->commercial_space = str_replace('.', '', $request->commercial_space);
            $request->commercial_space = str_replace(',', '.', $request->commercial_space);
        }
        if ($request->energy_value) {
            $request->energy_value = str_replace('.', '', $request->energy_value);
            $request->energy_value = str_replace(',', '.', $request->energy_value);
        }
        if ($request->annual_rent) {
            $request->annual_rent = str_replace('.', '', $request->annual_rent);
            $request->annual_rent = str_replace(',', '.', $request->annual_rent);
        }
        if ($request->equity) {
            $request->equity = str_replace('.', '', $request->equity);
            $request->equity = str_replace(',', '.', $request->equity);
        }
        if ($request->wault) {
            // $request->wault = str_replace('.','',$request->wault);
            $request->wault = str_replace(',', '.', $request->wault);
        }
        if ($request->stellplatze) {
            $request->stellplatze = str_replace('.', '', $request->stellplatze);
            $request->stellplatze = str_replace(',', '.', $request->stellplatze);
        }

        $data = [
            'title' => $request->ad_title,
            'property' => $request->ad_property,
            'property_id' => $request->property_id,
            'construction_year' => $request->const_year,
            'purchase_price' => $request->ad_purchase_price,
            'slug' => null,
            'description' => $request->ad_description,
            'type' => $request->type,
            'price' => $request->price,
            'is_negotiable' => null,
            'images' => $file_for_db,
            'pdf' => $pdf_filename,
            'vpdf' => $vpdf_filename,

            'purpose' => $request->purpose,
            'price_per_unit' => $request->price_per_unit,
            'unit_type' => $request->price_unit,
            'square_unit_space' => $request->square_unit_space,
            'floor' => $request->floor,
            'beds' => $request->beds,
            'attached_bath' => $request->attached_bath,
            'common_bath' => $request->common_bath,
            'balcony' => $request->balcony,
            'dining_space' => $request->dining_space,
            'living_room' => $request->living_room,
            'amenities' => $amenities,
            'distances' => $distances,
            'ad_city_name' => $request->ad_city_name,

            'seller_name' => $request->seller_name,
            'seller_email' => $request->seller_email,
            'seller_phone' => $request->seller_phone,
            'country_id' => $request->country,
            'state_id' => $request->state,
            'city_id' => $request->city,
            'address' => $request->address,

            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'street' => $request->street,
            'postcode' => $request->postcode,
            'location' => $request->location,

            'rentable_area' => $request->rentable_area,
            'living_space' => $request->living_space,
            'commercial_space' => $request->commercial_space,
            'current_state' => $request->current_state,
            'const_year' => $request->const_year,
            'heating' => $request->heating,
            'energy_available' => $request->energy_available,
            'energy_value' => $request->energy_value,
            'energy_valid' => $request->energy_valid,
            'provision' => $request->provision,

            'tenants' => $request->tenants,
            'annual_rent' => $request->annual_rent,
            'equity' => $request->equity,
            'wault' => $request->wault,
            'stellplatze' => $request->stellplatze,


            'video_url' => $video_url,
            'price_plan' => $request->price_plan,
            'mark_ad_urgent' => $mark_ad_urgent,
            'status' => 0,
            'user_id' => $user_id,
			// 'sonstiges' => $request->sonstiges,

        ];


        if ($ad) {
            $updated_ad = $ad->update($data);
            $created_ad = $ad->id;
        } else {
            if ($request->vermietung)
                $created_ad = DB::table('vads')->insertGetId($data);
            else
                $created_ad = DB::table('ads')->insertGetId($data);
        }

        //store ads images in google drive
//        if ($request->hasFile('images')) {
//            $validator = Validator::make(
//                $request->all(),
//                [
//                    'images.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
//                ]
//            );
//            if ($validator->fails()) {
//                return response()->json(array(
//                    'alert_type' => 'danger',
//                    'msg_text' => $validator->errors()
//                ));
//            }
//
//            $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
//            if (!$propertyMainDir) {
//                return redirect()->back()->with('error', 'Property directory not created in google Drive.');
//            }
//            $propertyAnhangeDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name', 'LIKE', 'Ads')->first();
//            if (!$propertyAnhangeDir) {
//                return redirect()->back()->with('error', 'Property Ads directory not created in google Drive.');
//            }
//            foreach ($request->images as $file) {
//                $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
//                $originalExt = $file->getClientOriginalExtension();
//                $folder = $propertyAnhangeDir->dir_path;
//                $fileData = File::get($file);
//
//                //upload file
//                $file2 = Storage::cloud()->put($folder . '/' . $originalName . '.' . $originalExt, $fileData);
//
//                //get google drive id
//                $contents = collect(Storage::cloud()->listContents($folder, false));
//
//                $file = $contents->where('type', 'file')->where('filename', $originalName)->where('extension', $originalExt)->first();
//
//                $images = [
//                    'ads_id' => $created_ad,
//                    'file_name' => $file['name'],
//                    'file_basename' => $file['basename'],
//                    'file_path' => $file['path'],
//                    'file_href' => 'https://drive.google.com/file/d/' . $file['basename'],
//                    'extension' => $file['extension']
//                ];
//                DB::table('ads_images')->insert($images);
//            }
//        }

        /**
         * iF add created
         */
        if ($created_ad) {
            if ($property && $request->vermietung) {
                $d = $this->postlisting1($created_ad, $request->property_id);
            } elseif ($property) {
                $d = $this->postlisting($created_ad);
            }
            return redirect()->back()->with('success', trans('Ad created successfully'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $user_id = $user->id;

        $title = trans('app.edit_ad');
        $ad = Ad::find($id);

        if (!$ad)
            return view('admin.error.error_404');

        if (!$user->is_admin()) {
            if ($ad->user_id != $user_id) {
                return view('admin.error.error_404');
            }
        }

        $countries = Country::all();
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->get();

        $previous_states = State::where('country_id', $ad->country_id)->get();
        $previous_cities = City::where('state_id', $ad->state_id)->get();

        $categories = Category::all();
        $distances = Brand::all();

        return view('admin.edit_ad', compact('title', 'categories', 'countries', 'ads_images', 'ad', 'distances', 'previous_states', 'previous_cities'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $ad = Ad::find($id);
        $user = Auth::user();
        $user_id = $user->id;

        if (!$user->is_admin()) {
            if ($ad->user_id != $user_id) {
                return view('admin.error.error_404');
            }
        }
        $mark_ad_urgent = $request->mark_ad_urgent ? $request->mark_ad_urgent : 0;

        $rules = [
            'ad_title' => 'required',
            // 'ad_property'  => 'required',
            // 'ad_construction_year'  => 'required',
            // 'ad_purchase_price'  => 'required',
            'ad_description' => 'required',
            'type' => 'required',
            'purpose' => 'required',
            'country' => 'required',
            'seller_name' => 'required',
            'seller_email' => 'required',
            'seller_phone' => 'required',
            'address' => 'required',
            'street' => 'required',
            'postcode' => 'required',
            'location' => 'required',
        ];
        $this->validate($request, $rules);

        $title = $request->ad_title;
        //$slug = unique_slug($title);

        $sub_category = Category::find($request->category);
        $is_negotialble = $request->negotiable ? $request->negotiable : 0;
        $brand_id = $request->brand ? $request->brand : 0;
        $video_url = $request->video_url ? $request->video_url : '';

        $amenities = serialize($request->amenities);
        $distances = serialize($request->distances);

        $data = [
            'title' => $request->ad_title,
            'property' => $request->ad_property,
            'construction_year' => $request->const_year,
            'purchase_price' => $request->ad_purchase_price,
            'description' => $request->ad_description,
            'type' => $request->type,
            'price' => $request->price,
            'is_negotiable' => $is_negotialble,

            'purpose' => $request->purpose,
            'price_per_unit' => $request->price_per_unit,
            'unit_type' => $request->price_unit,
            'square_unit_space' => $request->square_unit_space,
            'floor' => $request->floor,
            'beds' => $request->beds,
            'attached_bath' => $request->attached_bath,
            'common_bath' => $request->common_bath,
            'balcony' => $request->balcony,
            'dining_space' => $request->dining_space,
            'living_room' => $request->living_room,
            'amenities' => $amenities,
            'distances' => $distances,

            'seller_name' => $request->seller_name,
            'seller_email' => $request->seller_email,
            'seller_phone' => $request->seller_phone,
            'country_id' => $request->country,
            'state_id' => $request->state,
            'city_id' => $request->city,
            'address' => $request->address,

            'latitude' => $request->latitude,
            'longitude' => $request->longitude,

            'street' => $request->street,
            'postcode' => $request->postcode,
            'location' => $request->location,

            'rentable_area' => $request->rentable_area,
            'living_space' => $request->living_space,
            'commercial_space' => $request->commercial_space,
            'current_state' => $request->current_state,
            'const_year' => $request->const_year,
            'heating' => $request->heating,
            'energy_available' => $request->energy_available,
            'energy_value' => $request->energy_value,
            'energy_valid' => $request->energy_valid,
            'provision' => $request->provision,

            'tenants' => $request->tenants,
            'annual_rent' => $request->annual_rent,
            'equity' => $request->equity,
            'wault' => $request->wault,

            'video_url' => $video_url,
            //'mark_ad_urgent' => $mark_ad_urgent,

        ];

        $updated_ad = $ad->update($data);

        /**
         * iF add created
         */
        if ($updated_ad) {
            //Attach all unused media with this ad
            Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->update(['ad_id' => $ad->id]);
        }

        return redirect()->back()->with('success', trans('app.ad_updated'));
    }


    public function adStatusChange(Request $request)
    {
        $slug = $request->slug;
        $ad = Ad::whereSlug($slug)->first();
        if ($ad) {
            $value = $request->value;
            /*
            $ad->status = $value;
            $ad->save();*/
            ad_status_change($ad->id, $value);
            if ($value == 1) {
                return ['success' => 1, 'msg' => trans('app.ad_approved_msg')];
            } elseif ($value == 2) {
                return ['success' => 1, 'msg' => trans('app.ad_blocked_msg')];
            } elseif ($value == 3) {
                return ['success' => 1, 'msg' => trans('app.ad_archived_msg')];
            }
        }
        return ['success' => 0, 'msg' => trans('app.error_msg')];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $slug = $request->slug;
        $ad = Ad::whereSlug($slug)->first();
        if ($ad) {
            $media = Media::whereAdId($ad->id)->get();
            if ($media->count() > 0) {
                foreach ($media as $m) {
                    $storage = Storage::disk($m->storage);
                    if ($storage->has('uploads/images/' . $m->media_name)) {
                        $storage->delete('uploads/images/' . $m->media_name);
                    }
                    if ($m->type == 'image') {
                        if ($storage->has('uploads/images/thumbs/' . $m->media_name)) {
                            $storage->delete('uploads/images/thumbs/' . $m->media_name);
                        }
                    }
                    $m->delete();
                }
            }
            $ad->delete();
            return ['success' => 1, 'msg' => trans('app.media_deleted_msg')];
        }
        return ['success' => 0, 'msg' => trans('app.error_msg')];
    }

    public function getSubCategoryByCategory(Request $request)
    {
        $category_id = $request->category_id;
        $brands = Sub_Category::whereCategoryId($category_id)->select('id', 'category_name', 'category_slug')->get();
        return $brands;
    }

    public function getBrandByCategory(Request $request)
    {
        $category_id = $request->category_id;
        $brands = Brand::whereCategoryId($category_id)->select('id', 'brand_name')->get();
        return $brands;
    }

    public function getStateByCountry(Request $request)
    {
        $country_id = $request->country_id;
        $states = State::whereCountryId($country_id)->select('id', 'state_name')->get();
        return $states;
    }

    public function getCityByState(Request $request)
    {
        $state_id = $request->state_id;
        $cities = City::whereStateId($state_id)->select('id', 'city_name')->get();
        return $cities;
    }

    public function getParentCategoryInfo(Request $request)
    {
        $category_id = $request->category_id;
        $sub_category = Category::find($category_id);
        $category = Category::find($sub_category->category_id);
        return $category;
    }

    public function uploadAdsPdf(Request $request)
    {
        $user_id = Auth::user()->id;

        if ($request->hasFile('pdf')) {
            $image = $request->file('pdf');
            $valid_extensions = ['pdf'];

            if (!in_array(strtolower($image->getClientOriginalExtension()), $valid_extensions)) {
                return ['success' => 0, 'msg' => implode(',', $valid_extensions) . ' ' . trans('app.valid_extension_msg')];
            }

            $file_base_name = str_replace('.' . $image->getClientOriginalExtension(), '', $image->getClientOriginalName());


            $image_name = strtolower(time() . str_random(5) . '-' . str_slug($file_base_name)) . '.' . $image->getClientOriginalExtension();

            $imageFileName = 'uploads/images/' . $image_name;

            try {
                //Upload original image
                $is_uploaded = current_disk()->put($imageFileName, \File::get($image), 'public');

                if ($is_uploaded) {
                    //Save image name into db
                    $created_img_db = Media::create(['user_id' => $user_id, 'media_name' => $image_name, 'type' => 'file', 'folder_path' => 'public/', 'storage' => get_option('default_storage'), 'ref' => 'ad']);

                    //upload thumb image
                    // current_disk()->put($imageThumbName, $resized_thumb->__toString(), 'public');
                    $img_url = media_url($created_img_db, false);
                    return ['success' => 1, 'img_url' => $img_url];
                } else {
                    return ['success' => 0];
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }

        }
    }

    public function uploadAdsImage(Request $request)
    {
        $user_id = Auth::user()->id;

        if ($request->hasFile('images')) {
            $image = $request->file('images');
            $valid_extensions = ['jpg', 'jpeg', 'png'];

            if (!in_array(strtolower($image->getClientOriginalExtension()), $valid_extensions)) {
                return ['success' => 0, 'msg' => implode(',', $valid_extensions) . ' ' . trans('app.valid_extension_msg')];
            }

            $file_base_name = str_replace('.' . $image->getClientOriginalExtension(), '', $image->getClientOriginalName());

            $resized = Image::make($image)->stream();
            $resized_thumb = Image::make($image)->resize(320, 213)->stream();

            $image_name = strtolower(time() . str_random(5) . '-' . str_slug($file_base_name)) . '.' . $image->getClientOriginalExtension();

            $imageFileName = 'uploads/images/' . $image_name;
            $imageThumbName = 'uploads/images/thumbs/' . $image_name;

            try {
                //Upload original image
                $is_uploaded = current_disk()->put($imageFileName, $resized->__toString(), 'public');

                if ($is_uploaded) {
                    //Save image name into db
                    $created_img_db = Media::create(['user_id' => $user_id, 'media_name' => $image_name, 'type' => 'image', 'folder_path' => 'public/', 'storage' => get_option('default_storage'), 'ref' => 'ad']);

                    //upload thumb image
                    current_disk()->put($imageThumbName, $resized_thumb->__toString(), 'public');
                    $img_url = media_url($created_img_db, false);
                    return ['success' => 1, 'img_url' => $img_url];
                } else {
                    return ['success' => 0];
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }

        }
    }

    /**
     * @param Request $request
     * @return array
     */

    public function deleteMedia(Request $request)
    {
        $media_id = $request->media_id;
        $media = Media::find($media_id);

        $storage = Storage::disk($media->storage);
        if ($storage->has('uploads/images/' . $media->media_name)) {
            $storage->delete('uploads/images/' . $media->media_name);
        }

        if ($media->type == 'image') {
            if ($storage->has('uploads/images/thumbs/' . $media->media_name)) {
                $storage->delete('uploads/images/thumbs/' . $media->media_name);
            }
        }

        $media->delete();
        return ['success' => 1, 'msg' => trans('app.media_deleted_msg')];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function featureMediaCreatingAds(Request $request)
    {
        $user_id = Auth::user()->id;
        $media_id = $request->media_id;

        Media::whereUserId($user_id)->whereAdId('0')->whereRef('ad')->update(['is_feature' => '0']);
        Media::whereId($media_id)->update(['is_feature' => '1']);

        return ['success' => 1, 'msg' => trans('app.media_featured_msg')];
    }

    /**
     * @return mixed
     */

    public function appendMediaImage()
    {
        $user_id = Auth::user()->id;
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->whereType('image')->get();

        return view('admin.append_media', compact('ads_images'));
    }

    public function appendMediaPdf()
    {
        $user_id = Auth::user()->id;
        $ads_images = Media::whereUserId($user_id)->whereAdId(0)->whereRef('ad')->whereType('file')->get();

        return view('admin.append_media', compact('ads_images'));
    }


    /**
     * Listing
     */

    public function listing(Request $request)
    {
        $ads = Ad::active();

        $premium_ads = Ad::activePremium();

        if ($request->q) {
            $ads = $ads->where(function ($ads) use ($request) {
                $ads->where('title', 'like', "%{$request->q}%")->orWhere('description', 'like', "%{$request->q}%");
            });
        }
        if ($request->type) {
            $ads = $ads->whereType($request->type);
        }
        if ($request->country) {
            $ads = $ads->whereCountryId($request->country);
        }
        if ($request->state) {
            $ads = $ads->whereStateId($request->state);
        }
        if ($request->city) {
            $ads = $ads->whereCityId($request->city);
        }
        if ($request->min_price) {
            $ads = $ads->where('price', '>=', $request->min_price);
        }
        if ($request->max_price) {
            $ads = $ads->where('price', '<=', $request->max_price);
        }
        if ($request->min_square_unit_space) {
            $ads = $ads->where('square_unit_space', '>=', $request->min_square_unit_space);
        }
        if ($request->max_square_unit_space) {
            $ads = $ads->where('square_unit_space', '<=', $request->max_square_unit_space);
        }
        if ($request->min_square_unit_space) {
            $ads = $ads->where('square_unit_space', '>=', $request->min_square_unit_space);
        }
        if ($request->max_square_unit_space) {
            $ads = $ads->where('square_unit_space', '<=', $request->max_square_unit_space);
        }
        if ($request->min_square_unit_space) {
            $ads = $ads->where('square_unit_space', '>=', $request->min_square_unit_space);
        }
        if ($request->max_square_unit_space) {
            $ads = $ads->where('square_unit_space', '<=', $request->max_square_unit_space);
        }
        if ($request->min_rentable_area) {
            $ads = $ads->where('rentable_area', '>=', $request->min_rentable_area);
        }
        if ($request->max_rentable_area) {
            $ads = $ads->where('rentable_area', '<=', $request->max_rentable_area);
        }
        if ($request->min_living_space) {
            $ads = $ads->where('living_space', '>=', $request->min_living_space);
        }
        if ($request->max_living_space) {
            $ads = $ads->where('living_space', '<=', $request->max_living_space);
        }
        if ($request->min_commercial_space) {
            $ads = $ads->where('commercial_space', '>=', $request->min_commercial_space);
        }
        if ($request->max_commercial_space) {
            $ads = $ads->where('commercial_space', '<=', $request->max_commercial_space);
        }
        if ($request->min_const_year) {
            $ads = $ads->where('const_year', '>=', $request->min_const_year);
        }
        if ($request->max_const_year) {
            $ads = $ads->where('const_year', '<=', $request->max_const_year);
        }
        if ($request->min_energy_value) {
            $ads = $ads->where('energy_value', '>=', $request->min_energy_value);
        }
        if ($request->max_energy_value) {
            $ads = $ads->where('energy_value', '<=', $request->max_energy_value);
        }
        if ($request->min_annual_rent) {
            $ads = $ads->where('annual_rent', '>=', $request->min_annual_rent);
        }
        if ($request->max_energy_value) {
            $ads = $ads->where('annual_rent', '<=', $request->max_annual_rent);
        }
        if ($request->adType) {
            if ($request->adType == 'business') {
                $ads = $ads->business();
            } elseif ($request->adType == 'personal') {
                $ads = $ads->personal();
            }
        }
        if ($request->user_id) {
            $ads = $ads->whereUserId($request->user_id);
        }
        if ($request->shortBy) {
            switch ($request->shortBy) {
                case 'price_high_to_low':
                    $ads = $ads->orderBy('price', 'desc');
                    break;
                case 'price_low_to_height':
                    $ads = $ads->orderBy('price', 'asc');
                    break;
                case 'latest':
                    $ads = $ads->orderBy('id', 'desc');
                    break;
            }
        } else {
            $ads = $ads->orderBy('id', 'desc');
        }

        $ads_per_page = get_option('ads_per_page');
        $ads = $ads->with('city');
        $ads = $ads->paginate($ads_per_page);

        $login = Auth::user();

        if (isset($_REQUEST) && $_REQUEST) {

            $arr1 = $_REQUEST;
            if (isset($arr1['_ga']))
                unset($arr1['_ga']);
            if (isset($arr1['_gid']))
                unset($arr1['_gid']);
            if (isset($arr1['XSRF-TOKEN']))
                unset($arr1['XSRF-TOKEN']);
            if (isset($arr1['laravel_session']))
                unset($arr1['laravel_session']);

            // print_r($arr1); die;


            $array_activity['user_id'] = $login->id;
            $array_activity['type'] = 4;                //4=Search Listing
            $array_activity['search_text'] = json_encode($arr1);
            if ($login->user_type != "admin" && $arr1)
                save_agent_activity($array_activity);
        }


        //Check max impressions
        $max_impressions = get_option('premium_ads_max_impressions');
        $premium_ads = $premium_ads->where('max_impression', '<', $max_impressions);
        $take_premium_ads = get_option('number_of_premium_ads_in_listing');
        if ($take_premium_ads > 0) {
            $premium_order_by = get_option('order_by_premium_ads_in_listing');
            $premium_ads = $premium_ads->take($take_premium_ads);
            $last_days_premium_ads = get_option('number_of_last_days_premium_ads');

            $premium_ads = $premium_ads->where('created_at', '>=', Carbon::now()->timezone(get_option('default_timezone'))->subDays($last_days_premium_ads));

            if ($premium_order_by == 'latest') {
                $premium_ads = $premium_ads->orderBy('id', 'desc');
            } elseif ($premium_order_by == 'random') {
                $premium_ads = $premium_ads->orderByRaw('RAND()');
            }

            $premium_ads = $premium_ads->get();

        } else {
            $premium_ads = false;
        }

        $title = trans('app.search_properties');
        $countries = Country::all();

        $selected_categories = Category::find($request->category);
        $selected_sub_categories = Category::find($request->sub_category);

        $selected_countries = Country::find($request->country);
        $selected_states = State::find($request->state);
        //dd($selected_countries->states);

        $agents = User::whereActiveStatus('1')->whereFeature('1')->whereUserType('user')->take(10)->orderBy('id', 'desc')->get();

        return view($this->theme . 'listing', compact('top_categories', 'ads', 'title', 'countries', 'selected_categories', 'selected_sub_categories', 'selected_countries', 'selected_states', 'premium_ads', 'agents'));
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function singleAd($slug)
    {

        // $data['name'] = $data['email'] = $data['message'] = "Hiiiiiiiiii";
        // Mail::send('emails.reply_by_email',['data' => $data], function ($m)  {
        //     $m->from(get_option('email_address'), get_option('site_name'));
        //     $m->to("yiicakephp@gmail.com", 'test')->subject('query from test');
        //     $m->replyTo("info@amcodr.co", "amcodr");
        // });

        //  echo "here"; die;


        $login = Auth::user();


        $limit_regular_ads = get_option('number_of_free_ads_in_home');
        $ad = Ad::whereSlug($slug)->first();

        if (!$ad) {
            return view('theme.error_404');
        }

        if (!$ad->is_published()) {
            if (Auth::check()) {
                $user_id = Auth::user()->id;
                if ($user_id != $ad->user_id) {
                    return view('theme.error_404');
                }
            } else {
                return view('theme.error_404');
            }
        } else {
            $ad->view = $ad->view + 1;
            $ad->save();
        }

        $title = $ad->title;
        $ad_agent_id = $ad->user_id;

        if ($login)
            $array_activity['user_id'] = $login->id;
        $array_activity['type'] = 3;                //3=View Ad
        $array_activity['ad_id'] = $ad->id;
        if ($login && $login->user_type != "admin")
            save_agent_activity($array_activity);


        //Get distances
        $distances = Brand::all();
        $indore_ammenties = Category::whereCategoryType('indoor')->get();
        $outdoor_ammenties = Category::whereCategoryType('outdoor')->get();
        //Get Related Ads, add [->whereCountryId($ad->country_id)] for more specific results
        $related_ads = Ad::active()->whereUserId($ad_agent_id)->where('id', '!=', $ad->id)->with('city')->limit($limit_regular_ads)->orderByRaw('RAND()')->get();

        $agents = User::whereActiveStatus('1')->whereFeature('1')->whereUserType('user')->take(10)->orderBy('id', 'desc')->get();

        return view($this->theme . 'single_ad', compact('ad', 'title', 'distances', 'indore_ammenties', 'outdoor_ammenties', 'related_ads', 'agents', 'login'));
    }

    public function embeddedAd($slug)
    {
        $ad = Ad::whereSlug($slug)->first();
        return view($this->theme . 'embedded_ad', compact('ad'));
    }

    /**
     * @param Request $request
     */

    public function switchGridListView(Request $request)
    {
        session(['grid_list_view' => $request->grid_list_view]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function reportAds(Request $request)
    {
        $ad = Ad::whereSlug($request->slug)->first();
        if ($ad) {
            $data = [
                'ad_id' => $ad->id,
                'reason' => $request->reason,
                'email' => $request->email,
                'message' => $request->message,
            ];
            Report_ad::create($data);
            return ['status' => 1, 'msg' => trans('app.ad_reported_msg')];
        }
        return ['status' => 0, 'msg' => trans('app.error_msg')];
    }


    public function reports()
    {
        $reports = Report_ad::orderBy('id', 'desc')->with('ad')->paginate(20);
        $title = trans('app.ad_reports');

        return view('admin.ad_reports', compact('title', 'reports'));
    }

    public function deleteReports(Request $request)
    {
        Report_ad::find($request->id)->delete();
        return ['success' => 1, 'msg' => trans('app.report_deleted_success')];
    }

    public function reportsByAds($slug)
    {
        $user = Auth::user();

        if ($user->is_admin()) {
            $ad = Ad::whereSlug($slug)->first();
        } else {
            $ad = Ad::whereSlug($slug)->whereUserId($user->id)->first();
        }

        if (!$ad) {
            return view('admin.error.error_404');
        }

        $reports = $ad->reports()->paginate(20);

        $title = trans('app.ad_reports');
        return view('admin.reports_by_ads', compact('title', 'ad', 'reports'));

    }

    public function uploadfiles(Request $request)
    {
        $arr1 = $arr = array();
        if ($request->hasFile('file')) {
            $validator = Validator::make($request->all(),
                [
                    'images.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
                ]);
            if ($validator->fails()) {

            }
            else{
                $upload_path = 'ad_files_upload/';
//                foreach ($request->file as $file) {
                    $file = $request->file;
                    $image = $file;
                    $extension = $file->getClientOriginalExtension();
                    $filename = uniqid() . '_' . time() . '.' . $extension;
                    $file->move($upload_path, $filename);
                    $arr[] = asset('ad_files_upload/'.$filename);
                    $arr1[] = $filename;
                }
//            }
        }
        $f_arr['files'] = $arr;
        $f_arr['filename'] = $arr1;
        return $f_arr;
    }


    public function ad_pdf(Request $request)
    {

        ini_set('max_execution_time', 0);


        // print "<pre>";
        // print_r($_FILES); die;
        $user_id = Auth::user()->id;
        $title = $request->ad_title;

        $property = Properties::where('id', $request->property_id)->where('Ist', 0)->where('soll', 0)->first();


        $exportad = ExportAd::where('property_id', $request->property_id)->first();


        $is_negotialble = $request->negotiable ? $request->negotiable : 0;
        $mark_ad_urgent = $request->mark_ad_urgent ? $request->mark_ad_urgent : 0;
        $video_url = $request->video_url ? $request->video_url : '';

        $amenities = serialize($request->amenities);
        $distances = serialize($request->distances);

        $i = $p = $p1 = "";

        if ($request->img) {
            $i = implode(',', $request->img);
        }
        if ($request->ipdf) {
            $p = implode(',', $request->ipdf);
        }
        if ($request->vipdf) {
            $p1 = implode(',', $request->vipdf);
        }
        $file_for_db = $i;


        if ($request->hasFile('images')) {
            $validator = Validator::make($request->all(),
                [
                    'images.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
                ]);
            if ($validator->fails()) {
                return response()->json(array(
                    'alert_type' => 'danger',
                    'msg_text' => $validator->errors()
                ));
            }

            $upload_path = 'ad_files_upload/';

            // print_r($request->images); die;
            foreach ($request->images as $file) {

                $image = $file;

                $extension = $file->getClientOriginalExtension();
                $filename = uniqid() . '_' . time() . '.' . $extension;

                // $destinationPath = $upload_path;
                // $img = Image::make($image->getRealPath());
                // $img->resize(750, 500, function ($constraint) {
                //   $constraint->aspectRatio();
                // })->save($destinationPath.'/'.$filename);


                //$destinationPath = $upload_path;
                //$image->move($destinationPath, $filename);


                $file->move($upload_path, $filename);
                $file_for_db .= ',' . $filename;

            }
        }


        $pdf_filename = $p;
        $vpdf_filename = $p1;


        // echo $pdf_filename;
        // echo $file_for_db;
        // die;
        if ($request->ad_city_name) {
            $duplicate = City::whereCityName($request->ad_city_name)->first();
            if ($duplicate)
                $request->city = $duplicate->id;
            else {
                $data = [
                    'city_name' => $request->ad_city_name,
                    'state_id' => $request->state,
                ];

                $s = City::create($data);
                $request->city = $s->id;
            }
        }

        $slug = time();

        if ($request->price) {
            $request->price = str_replace('.', '', $request->price);
            $request->price = str_replace(',', '.', $request->price);
        }
        if ($request->price_per_unit) {
            $request->price_per_unit = str_replace('.', '', $request->price_per_unit);
            $request->price_per_unit = str_replace(',', '.', $request->price_per_unit);
        }
        if ($request->square_unit_space) {
            $request->square_unit_space = str_replace('.', '', $request->square_unit_space);
            $request->square_unit_space = str_replace(',', '.', $request->square_unit_space);
        }
        if ($request->rentable_area) {
            $request->rentable_area = str_replace('.', '', $request->rentable_area);
            $request->rentable_area = str_replace(',', '.', $request->rentable_area);
        }
        if ($request->living_space) {
            $request->living_space = str_replace('.', '', $request->living_space);
            $request->living_space = str_replace(',', '.', $request->living_space);
        }
        if ($request->commercial_space) {
            $request->commercial_space = str_replace('.', '', $request->commercial_space);
            $request->commercial_space = str_replace(',', '.', $request->commercial_space);
        }
        if ($request->energy_value) {
            $request->energy_value = str_replace('.', '', $request->energy_value);
            $request->energy_value = str_replace(',', '.', $request->energy_value);
        }
        if ($request->annual_rent) {
            $request->annual_rent = str_replace('.', '', $request->annual_rent);
            $request->annual_rent = str_replace(',', '.', $request->annual_rent);
        }
        if ($request->equity) {
            $request->equity = str_replace('.', '', $request->equity);
            $request->equity = str_replace(',', '.', $request->equity);
        }
        if ($request->wault) {
            // $request->wault = str_replace('.','',$request->wault);
            $request->wault = str_replace(',', '.', $request->wault);
        }
        if ($request->stellplatze) {
            // $request->stellplatze = str_replace('.','',$request->stellplatze);
            $request->stellplatze = str_replace(',', '.', $request->stellplatze);
        }

        $data = [
            'title' => $request->ad_title,
            'property' => $request->ad_property,
            'property_id' => $request->property_id,
            'construction_year' => $request->const_year,
            'purchase_price' => $request->ad_purchase_price,
            'slug' => $slug,
            'description' => $request->ad_description,
            'type' => $request->type,
            'price' => $request->price,
            'is_negotiable' => null,
            'images' => $file_for_db,
            'pdf' => $pdf_filename,
            'vpdf' => $vpdf_filename,

            'purpose' => $request->purpose,
            'price_per_unit' => $request->price_per_unit,
            'unit_type' => $request->price_unit,
            'square_unit_space' => $request->square_unit_space,
            'floor' => $request->floor,
            'beds' => $request->beds,
            'attached_bath' => $request->attached_bath,
            'common_bath' => $request->common_bath,
            'balcony' => $request->balcony,
            'dining_space' => $request->dining_space,
            'living_room' => $request->living_room,
            'amenities' => $amenities,
            'distances' => $distances,
            'ad_city_name' => $request->ad_city_name,

            'seller_name' => $request->seller_name,
            'seller_email' => $request->seller_email,
            'seller_phone' => $request->seller_phone,
            'country_id' => $request->country,
            'state_id' => $request->state,
            'city_id' => $request->city,
            'address' => $request->address,

            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'street' => $request->street,
            'postcode' => $request->postcode,
            'location' => $request->location,

            'rentable_area' => $request->rentable_area,
            'living_space' => $request->living_space,
            'commercial_space' => $request->commercial_space,
            'current_state' => $request->current_state,
            'const_year' => $request->const_year,
            'heating' => $request->heating,
            'energy_available' => $request->energy_available,
            'energy_value' => $request->energy_value,
            'energy_valid' => $request->energy_valid,
            'provision' => $request->provision,

            'tenants' => $request->tenants,
            'annual_rent' => $request->annual_rent,
            'equity' => $request->equity,
            'wault' => $request->wault,


            'video_url' => $video_url,
            'price_plan' => $request->price_plan,
            'mark_ad_urgent' => $mark_ad_urgent,
            'status' => 0,
            'user_id' => $request->user_id,

            'total_area' => $request->total_area,
            'number_of_parking' => $request->number_of_parking,
            'note' => $request->note,
            'domestic_equipments' => $request->domestic_equipments,
            'sonstiges' => $request->sonstiges,
        ];

        // print_r($data); die;

        if ($exportad) {
            $updated_ad = $exportad->update($data);

        } else {
            $created_ad = DB::table('export_ads')->insertGetId($data);
        }
        // echo public_path().'/dompdf/lib/fonts/dompdf_font_family_cache.php'; die;

        if (file_exists(public_path() . '/dompdf/lib/fonts/dompdf_font_family_cache.php'))
            unlink(public_path() . '/dompdf/lib/fonts/dompdf_font_family_cache.php');


        $ad = json_decode(json_encode($data));

        // print_r($file_for_db);die;
        $ad_images = array();
        if ($file_for_db)
            $ad_images = explode(',', $file_for_db);

        if ($request->download) {
            $data = ExportAd::where('property_id', $request->property_id)->first();
            $p = properties::select('verkaufspreis')->where('id', $request->property_id)->first();

            // print_r($data); die;
            if ($data['images']) {
                $ad_images = explode(',', $data['images']);
                if ($ad_images[0] == "")
                    unset($ad_images[0]);
                $ad_images = array_values($ad_images);
                if ($data->image_title && isset($ad_images[$data->image_title])) {
                    $image_array[] = $ad_images[$data->image_title];
                    unset($ad_images[$data->image_title]);

                    foreach ($ad_images as $key => $value) {
                        $image_array[] = $value;
                    }
                    $ad_images = $image_array;
                }

                $data['img'] = $ad_images;
            }
            // print_r($data['img']);die;

            //return view('pdf.index', compact('data'));

            $pdf = PDF::loadView('pdf.index1', compact('data', 'p'));
            $slug = rand();
            $filename = str_replace('/', '-', $property->name_of_property). '-Exposé.pdf';


            $file_to_save = public_path().'/ad_files_upload/'.$filename;
            file_put_contents($file_to_save, $pdf->output());

            // $file_to_save = public_path().'/ad_files_upload/5d95e96e26479_1570105710.pdf';

            $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
            if(!$propertyMainDir){
                $result = ['status' => false, 'message' => 'Property directory not created in google Drive.', 'data' => []];
                // return response()->json($result);
            }
            $propertyRechnungenDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Exposé')->first();
            if(!$propertyRechnungenDir){
                $result = ['status' => false, 'message' => 'Property Rechnungen directory not created in google Drive.', 'data' => []];
                // return response()->json($result);
            }
            if($propertyRechnungenDir)
            {
                $folder = $propertyRechnungenDir->dir_path;
                $fileData = File::get($file_to_save);
                $file2 =  Storage::cloud()->put($folder.'/'.$filename, $fileData);
                // pre($fileData);
            }
            unlink($file_to_save);
            return $pdf->download($filename);
            die;
        } else {
            return redirect('properties/' . $request->property_id);
        }
    }

    public function saveContactdata(Request $request)
    {
        if (!isset($_POST['intranet_listing_id']))
            die;
        if (isset($_POST['contact_from']) && $_POST['contact_from'] == 1) {
            $ad = Ad::where('id', $_POST['intranet_listing_id'])->first();
        } else {
            $ad = Vad::where('id', $_POST['intranet_listing_id'])->first();
        }
        $m = new ContactQuery;
        $m->name = $request->name;
        $m->contact_from = $request->contact_from;
        $m->email = $request->email;
        $m->phone = $request->phone_number;
        $m->property_id = $ad->property_id;
        $m->message = $request->message;
        $m->save();

    }

    public function deleteImages(Request $request)
    {
        $properties_pdf = DB::table('ads_images')->where('id', $request->id)->first();
        if ($properties_pdf) {
            Storage::cloud()->delete($properties_pdf->file_path);
            $response = [
                'success' => true,
                'msg' => 'File delete successfully.'
            ];

            DB::table('ads_images')->where('id', $request->id)->delete();

            // $properties_pdf->delete();
        }
        echo "1";
        die;
    }

    public function deleteAdImages(Request $request)
    {
        if ($request->type == 'expose'){
            $ad = DB::table('export_ads')->where('id', $request->id)->first();
            if ($ad)
            {
                $imagesName = explode(',',$ad->images);
                if (($key = array_search($request->name, $imagesName)) !== false) {
                    unset($imagesName[$key]);
                }

                DB::table('export_ads')->where('id', $request->id)->update([
                    'images' => implode(',',$imagesName)
                ]);
                $dir = 'ad_files_upload/';
                unlink($dir.$request->name);
            }
        }elseif ($request->type == 'verkaufsportal'){
            $ad = DB::table('ads')->where('id', $request->id)->first();
            if ($ad)
            {
                $imagesName = explode(',',$ad->images);
                if (($key = array_search($request->name, $imagesName)) !== false) {
                    unset($imagesName[$key]);
                }

                DB::table('ads')->where('id', $request->id)->update([
                    'images' => implode(',',$imagesName)
                ]);
                $dir = 'ad_files_upload/';
                unlink($dir.$request->name);
            }
        }

        echo "1";
        die;
    }
}
