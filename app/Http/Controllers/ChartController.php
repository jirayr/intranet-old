<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Properties;
use App\Models\TenancySchedule;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Services\PropertiesService;
use App\Services\TenancySchedulesService;
use App\Models\TenancyScheduleItem;
use App\Models\Masterliste;

class ChartController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$input = $request->all();
        $status = config('properties.status.buy');
        $user = sizeof(User::all());
        $Revenues = Properties::where('status',$status)->where('Ist',0)->where('soll',0)->get();
        $sum_Revenue=0;
        $properties=Properties::where('Ist',0)->where('soll',0)->get();
        $masterlistes = Masterliste::all();
        $tenancy_items = TenancyScheduleItem::where('type',3)->orWhere('type', '=', 4)->get();
        $tenancy_items_2 = TenancyScheduleItem::where('type',1)->orWhere('type', '=', 2)->get();

        $masterlistes_new = array();
        $masterlistes_new_1 = array();
        $masterlistes_new_2 = array();

        $summ_vermietet_m2 = 0;
        $summ_vermietet_percent = 0;
        $summ_leerstand_m2 = 0;
        $summ_leerstand_percent = 0;
        $summ_differenz_miete_in_euro = 0;
        $summ_differenz_miete_pa_in_euro = 0;

        $graph_data = [];
        $graph_data['sum_vermietet_m2_this_month'] = 0;
        $graph_data['sum_vermietet_m2_last_month'] = 0;
        $graph_data['sum_vermietet_m2_next_6_month'] = 0;
        $graph_data['sum_leerstand_m2_this_month'] = 0;
        $graph_data['sum_leerstand_m2_last_month'] = 0;
        $graph_data['sum_leerstand_m2_next_6_month'] = 0;
        $graph_data['sum_differenz_miete_this_month'] = 0;
        $graph_data['sum_differenz_miete_last_month'] = 0;
        $graph_data['sum_differenz_miete_next_6_month'] = 0;
		
		$thisMonthStart = mktime(0, 0, 0, date("n"), 1);
		$thisMonthEnd  = mktime(23, 59, 59, date("n"), date("t"));
		
		
		$nextMonthStart = mktime(0, 0, 0, date("n",strtotime("+1 month")), 1);
		$next6MonthEnd = mktime(23, 59, 59, date("n",strtotime("+6 month")), date("t",strtotime("+6 month")));

		$lastMonthStart = mktime(0, 0, 0, date("n",strtotime("-1 month")), 1);
		$lastMonthEnd = mktime(23, 59, 59, date("n",strtotime("-1 month")), date("t",strtotime("-1 month")));
		
		
		
		
        $sum_rented_area=0;
        $sum_flat_in_qm=0;

        foreach($masterlistes as $masterliste){

            //Get graph data
            if (Properties::where('masterliste_id', $masterliste->id)->where('Ist',0)->where('soll',0)->count() > 0){
				
				$pre_total_live_rental_space1 = 0;
				$pre_total_live_rental_space2 = 0;
				$pre_total_live_rental_space3 = 0;
				$pre_total_live_actual_net_rent1 =0;
				$pre_total_live_actual_net_rent2 =0;
				$pre_total_live_actual_net_rent3 =0;
				$pre_total_business_rental_space1 =0;
				$pre_total_business_rental_space2 =0;
				$pre_total_business_rental_space3 =0;
				$pre_total_business_actual_net_rent1 =0;
				$pre_total_business_actual_net_rent2 =0;
				$pre_total_business_actual_net_rent3 =0;


                $p_id = Properties::where('masterliste_id', $masterliste->id)->where('Ist',0)->where('soll',0)->first()->id;
                $t_id = TenancySchedule::where('property_id', $p_id)->first();
                if(isset($t_id->id)){

                    $_t_id = $t_id->id ;
                }
                $t_items = TenancyScheduleItem::where('tenancy_schedule_id', $_t_id)->get();

				foreach($t_items as $t_item){
					$t_item_date = date_parse_from_format("Y-m-d", $t_item->rent_end);
					$t_item_timestamp = mktime(
										$t_item_date['hour'], 
										$t_item_date['minute'], 
										$t_item_date['second'], 
										$t_item_date['month'], 
										$t_item_date['day'], 
										$t_item_date['year']
					);
					if($t_item_timestamp >= $lastMonthStart && $t_item_timestamp <= $lastMonthEnd){
						switch ($t_item->type){
							case config('tenancy_schedule.item_type.live'):
								$pre_total_live_rental_space1 += $t_item->rental_space;
								$pre_total_live_actual_net_rent1 += $t_item->actual_net_rent;
							break;
							case config('tenancy_schedule.item_type.business'):
								$pre_total_business_rental_space1 += $t_item->rental_space;
								$pre_total_business_actual_net_rent1 += $t_item->actual_net_rent;
							break;
						}
						
					}
					if($t_item_timestamp >= $thisMonthStart && $t_item_timestamp <= $thisMonthEnd){
						switch ($t_item->type){
							case config('tenancy_schedule.item_type.live'):
								$pre_total_live_rental_space2 += $t_item->rental_space;
								$pre_total_live_actual_net_rent2 += $t_item->actual_net_rent;
							break;
							case config('tenancy_schedule.item_type.business'):
								$pre_total_business_rental_space2 += $t_item->rental_space;
								$pre_total_business_actual_net_rent2 += $t_item->actual_net_rent;
							break;
						}
					}
					if($t_item_timestamp >= $nextMonthStart && $t_item_timestamp <= $next6MonthEnd){
						switch ($t_item->type){
							case config('tenancy_schedule.item_type.live'):
								$pre_total_live_rental_space3 += $t_item->rental_space;
								$pre_total_live_actual_net_rent3 += $t_item->actual_net_rent;
							break;
							case config('tenancy_schedule.item_type.business'):
								$pre_total_business_rental_space3 += $t_item->rental_space;
								$pre_total_business_actual_net_rent3 += $t_item->actual_net_rent;
							break;
						}
					}
					
					
				}
				
			

				$total_live_vacancy_in_eur1 = 0;
				$total_live_vacancy_in_eur2 = 0;
				$total_live_vacancy_in_eur3 = 0;

				$total_business_vacancy_in_eur1 = 0;
				$total_business_vacancy_in_eur2 = 0;
				$total_business_vacancy_in_eur3 = 0;

				$total_live_vacancy_in_qm = 0;
				
				foreach($t_items as $t_item){
					switch ($t_item->type){
						case config('tenancy_schedule.item_type.live_vacancy'):
							if( $pre_total_live_rental_space1 != 0 ){
								$cust_condition1  = $pre_total_live_actual_net_rent1 / $pre_total_live_rental_space1;
							}else{
								$cust_condition1 = 0;
							}
							$total_live_vacancy_in_eur1 += $t_item->vacancy_in_qm*$cust_condition1*0.8;
							
							if( $pre_total_live_rental_space2 != 0 ){
								$cust_condition2  = $pre_total_live_actual_net_rent2 / $pre_total_live_rental_space2;
							}else{
								$cust_condition2 = 0;
							}
							$total_live_vacancy_in_eur2 += $t_item->vacancy_in_qm*$cust_condition2*0.8;
							
							if( $pre_total_live_rental_space3 != 0 ){
								$cust_condition3  = $pre_total_live_actual_net_rent3 / $pre_total_live_rental_space3;
							}else{
								$cust_condition3 = 0;
							}
							$total_live_vacancy_in_eur3 += $t_item->vacancy_in_qm*$cust_condition3*0.8;
							
							$total_live_vacancy_in_qm += $t_item->vacancy_in_qm;
							
						break;
						case config('tenancy_schedule.item_type.business_vacancy'):
							if( $pre_total_business_rental_space1 != 0 ){
								$cust_condition1  = $pre_total_business_actual_net_rent1 / $pre_total_business_rental_space1;
							}else{
								$cust_condition1 = 0;
							}
							$total_business_vacancy_in_eur1 += $t_item->vacancy_in_qm*$cust_condition1*0.8;
							
							if( $pre_total_business_rental_space2 != 0 ){
								$cust_condition2  = $pre_total_business_actual_net_rent2 / $pre_total_business_rental_space2;
							}else{
								$cust_condition2 = 0;
							}
							$total_business_vacancy_in_eur2 += $t_item->vacancy_in_qm*$cust_condition2*0.8;
							
							if( $pre_total_business_rental_space3 != 0 ){
								$cust_condition3  = $pre_total_business_actual_net_rent3 / $pre_total_business_rental_space3;
							}else{
								$cust_condition3 = 0;
							}
							$total_business_vacancy_in_eur3 += $t_item->vacancy_in_qm*$cust_condition3*0.8;
						break;
					}
				}
				
				
				$graph_data['sum_vermietet_m2_last_month'] += $pre_total_live_rental_space1 + $pre_total_business_rental_space1;
				$graph_data['sum_vermietet_m2_this_month'] += $pre_total_live_rental_space2 + $pre_total_business_rental_space2;
				$graph_data['sum_vermietet_m2_next_6_month'] += $pre_total_live_rental_space3 + $pre_total_business_rental_space3;
                
				$graph_data['sum_leerstand_m2_last_month'] += $total_live_vacancy_in_qm;
				$graph_data['sum_leerstand_m2_this_month'] += $total_live_vacancy_in_qm;
				$graph_data['sum_leerstand_m2_next_6_month'] += $total_live_vacancy_in_qm;
				
				$graph_data['sum_differenz_miete_last_month'] += ($total_live_vacancy_in_eur1 + $total_business_vacancy_in_eur1) * 12;
				$graph_data['sum_differenz_miete_this_month'] += ($total_live_vacancy_in_eur2 + $total_business_vacancy_in_eur2) * 12;
				$graph_data['sum_differenz_miete_next_6_month'] += ($total_live_vacancy_in_eur3 + $total_business_vacancy_in_eur3) * 12;
				

            }

            foreach ($graph_data as $key => $value){
                $graph_data[$key] = round($value, 2);
            }


            $count = 0;
            if($masterliste->asset_manager){
                $masterliste->asset_manager_name = $masterliste->user->name;
            }

            $masterlistes_new[$masterliste->asset_manager]['flat_in_qm'] = 0;
            $masterlistes_new[$masterliste->asset_manager]['rented_area'] = 0;
            $masterlistes_new[$masterliste->asset_manager]['vacancy'] = 0;
            $masterlistes_new[$masterliste->asset_manager]['rent_net_pm'] = 0;
            $masterlistes_new[$masterliste->asset_manager]['potential_pm'] = 0;
            $masterlistes_new[$masterliste->asset_manager]['object'] = '';

            foreach($masterlistes as $masterliste_2){

                if ($masterliste->asset_manager == $masterliste_2->asset_manager) {
                    $m_property=$masterliste_2->property;
                    $masterliste_2->property_id = ($m_property)
                        ? $m_property->id :null;
                    $tenancy_schedule_data = TenancySchedulesService::get($masterliste_2->property_id);
                    $masterliste_2->tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : new TenancySchedule();


                    $count++;
                    $masterlistes_new[$masterliste->asset_manager]['asset_manager'] = $masterliste_2->asset_manager;
                    $masterlistes_new[$masterliste->asset_manager]['count'] = $count;
                    $masterlistes_new[$masterliste->asset_manager]['asset_manager_name'] = $masterliste_2->asset_manager_name;
                    $masterlistes_new[$masterliste->asset_manager]['flat_in_qm'] += $masterliste_2->tenancy_schedule->calculations['mi9'] + $masterliste_2->tenancy_schedule->calculations['mi10'];
                    $masterlistes_new[$masterliste->asset_manager]['rented_area'] += $masterliste_2->tenancy_schedule->calculations['total_live_rental_space'] + $masterliste_2->tenancy_schedule->calculations['total_business_rental_space'];
                    $masterlistes_new[$masterliste->asset_manager]['vacancy'] += $masterliste_2->tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $masterliste_2->tenancy_schedule->calculations['total_business_vacancy_in_qm'];
                    $masterlistes_new[$masterliste->asset_manager]['rent_net_pm'] += $masterliste_2->tenancy_schedule->calculations['total_actual_net_rent'];

                    $masterlistes_new[$masterliste->asset_manager]['potential_pm'] += $masterliste_2->tenancy_schedule->calculations['potenzial_eur_jahr'];

                    $masterlistes_new[$masterliste->asset_manager]['object'] .= (isset($masterliste_2->object) ? $masterliste_2->object.'<br/> ':' ');
                }
            }


            $rented_area = isset( $masterliste->rented_area ) ? $masterliste->rented_area : 0;
            $summ_vermietet_m2 += $rented_area;
            if( isset( $masterliste->flat_in_qm )){
                $sum_rented_area+=$masterliste->rented_area;
                $sum_flat_in_qm+=$masterliste->flat_in_qm;
            }


            $vacancy = isset($masterliste->vacancy) ? $masterliste->vacancy : 0;
            $summ_leerstand_m2 += $vacancy;



            $potential_pm = isset($masterliste->potential_pm) ? $masterliste->potential_pm : 0;
//            $summ_differenz_miete_in_euro += $potential_pm;
            $summ_differenz_miete_pa_in_euro += $potential_pm * 12;
        }
        if($sum_flat_in_qm!=0)
        {
            $summ_vermietet_percent = $sum_rented_area / $sum_flat_in_qm * 100;
            $summ_leerstand_percent = $summ_leerstand_m2 / $sum_flat_in_qm * 100;
        }


        foreach($masterlistes_new as $masterliste){
            $asset_manager_name_check = substr( $masterliste['asset_manager_name'],  0, 11);
            if($asset_manager_name_check != 'Entwicklung'){
	
                $masterlistes_new_1[$masterliste["asset_manager"]]['count'] = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['asset_manager_name'] = '';
                $masterlistes_new_1[$masterliste["asset_manager"]]['flat_in_qm'] = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['rented_area'] = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['vacancy'] = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['rent_net_pm'] = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['potential_pm'] = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['object'] = '';

				$masterlistes_new_1[$masterliste["asset_manager"]]['asset_manager'] = $masterliste["asset_manager"];
                $masterlistes_new_1[$masterliste["asset_manager"]]['count'] = $masterliste['count'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['asset_manager_name'] = $masterliste['asset_manager_name'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['flat_in_qm'] += $masterliste['flat_in_qm'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['rented_area'] += $masterliste['rented_area'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['vacancy'] += $masterliste['vacancy'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['rent_net_pm'] += $masterliste['rent_net_pm'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['potential_pm'] += $masterliste['potential_pm'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['object'] .= $masterliste['object'];
		   }
            else{
                $masterlistes_new_2[$masterliste["asset_manager"]]['count'] = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['asset_manager_name'] = '';
                $masterlistes_new_2[$masterliste["asset_manager"]]['flat_in_qm'] = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['rented_area'] = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['vacancy'] = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['rent_net_pm'] = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['potential_pm'] = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['object'] = '';


                $masterlistes_new_2[$masterliste["asset_manager"]]['count'] = $masterliste['count'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['asset_manager_name'] = $masterliste['asset_manager_name'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['flat_in_qm'] += $masterliste['flat_in_qm'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['rented_area'] += $masterliste['rented_area'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['vacancy'] += $masterliste['vacancy'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['rent_net_pm'] += $masterliste['rent_net_pm'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['potential_pm'] += $masterliste['potential_pm'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['object'] .= $masterliste['object'];
            }
            $summ_differenz_miete_in_euro += $masterliste['potential_pm'];
        }
        foreach ($Revenues as $Revenue){
            $sum_Revenue=$sum_Revenue+$Revenue['total_purchase_price'];
        }
//		$summ_leerstand_percent /= count($masterlistes);
//		 $summ_vermietet_percent /= count($masterlistes);
		 
		 
		 
		 if(isset($input['asset_manager'])){
			$masterlistes_new_1 = array();
			$masterlistes = Masterliste::where('asset_manager','=',$input['asset_manager'])->get();
			foreach($masterlistes as $index=>$masterliste){

                    $masterliste->asset_manager_name = User::where('id', $masterliste->asset_manager)->first()->name;
                    $m_property=$masterliste->property;
                    $masterliste->property_id = ($m_property)
                        ? $m_property->id :null;
                    $tenancy_schedule_data = TenancySchedulesService::get($masterliste->property_id);
                    $masterliste->tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : new TenancySchedule();
                    $masterlistes_new_1[$index]['count'] = 1;
                    $masterlistes_new_1[$index]['asset_manager_name'] = $masterliste['asset_manager_name'];
                    $masterlistes_new_1[$index]['flat_in_qm'] = $masterliste['flat_in_qm'];
                    $masterlistes_new_1[$index]['rented_area'] = $masterliste['rented_area'];
                    $masterlistes_new_1[$index]['vacancy'] =  $masterliste['vacancy'];
                    $masterlistes_new_1[$index]['rent_net_pm'] = $masterliste['rent_net_pm'];
                    $masterlistes_new_1[$index]['potential_pm'] = $masterliste->tenancy_schedule->calculations['potenzial_eur_jahr'];
                    $masterlistes_new_1[$index]['asset_manager'] = $masterliste['asset_manager'];
                    $masterlistes_new_1[$index]['object'] = $masterliste['object'];
                    


			}
		 }
		 
        foreach($tenancy_items as $key => $tenancy_item){
            if($tenancy_item->tenancy_schedule_id){
                $tenancy_schedule = TenancySchedule::where('id',$tenancy_item->tenancy_schedule_id)->first();
                $properties = Properties::where('id', $tenancy_schedule->property_id)->where('Ist',0)->where('soll',0)->first();
                if ($properties != null && $properties->masterliste_id != null){
                    $tenancy_item->object_name  = $properties->name_of_property;
                    $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                    if($masterliste != null){
                        $tenancy_item->creator_name = User::where('id', $masterliste->asset_manager)->first()->name;
                    }else{
                        unset($tenancy_items[$key]);
                    }
                    if($tenancy_item->type == 4){
                        $tenancy_item->type = 'Gewerbe';
                    }elseif ($tenancy_item->type == 3){
                        $tenancy_item->type = 'Wohnen';
                    }
                }else{
                    unset($tenancy_items[$key]);
                }

            }
        }

        foreach($tenancy_items_2 as $key => $tenancy_item){
            if($tenancy_item->tenancy_schedule_id){
                $tenancy_schedule = TenancySchedule::where('id',$tenancy_item->tenancy_schedule_id)->first();
                $properties = Properties::where('id', $tenancy_schedule->property_id)->where('Ist',0)->where('soll',0)->first();
                if ($properties != null && $properties->masterliste_id != null){
                    $tenancy_item->object_name  = $properties->name_of_property;
                    $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                    if($masterliste != null){
                        $tenancy_item->creator_name = User::where('id', $masterliste->asset_manager)->first()->name;
                    }else{
                        unset($tenancy_items_2[$key]);
                    }
                    if($tenancy_item->type == 2){
                        $tenancy_item->type = 'Gewerbe';
                    }elseif ($tenancy_item->type == 1){
                        $tenancy_item->type = 'Wohnen';
                    }
                }else{
                    unset($tenancy_items_2[$key]);
                }
            }
        }
		 $is_asset_manager=isset($input['asset_manager']);
        return view('assetmanagement.chart',compact('user', 'Revenues', 'sum_Revenue','properties', 'masterlistes', 'masterlistes_new_1', 'masterlistes_new_2', 'summ_vermietet_m2', 'summ_vermietet_percent', 'summ_leerstand_m2', 'summ_leerstand_percent', 'summ_differenz_miete_in_euro', 'summ_differenz_miete_pa_in_euro','is_asset_manager','tenancy_items','tenancy_items_2', 'graph_data'));
		 
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
