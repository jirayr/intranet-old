<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }
    public function login(Request $request)
    {
        $remember_me = $request->has('remember') ? true : false;
        // echo $remember_me; die;

        if (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'user_deleted' => 0, 'user_status' => 1], $remember_me))
        {
            $time = time();
            $user = Auth::user();
            $user->last_login_time = $time;
            $user->save();
            Session::put('last_login_time', $time);
            return redirect('/');
        }

        return back()->withInput()->with('message', 'Login Failed');
    }

}
