<?php

namespace App\Http\Controllers;

use App\Models\ModulePermission;
use App\User;

use Illuminate\Support\Facades\Storage;
use File;
use App\Models\PropertiesDirectory;
use App\Models\Directory_Permission;
use App\Models\Properties;
use App\Helpers\PropertiesHelperClass;
use Illuminate\Support\Facades\Log;
use Validator;
use Mail;

use Illuminate\Http\Request;

class ModulePermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $permissions = config('module_permission.modules');
        $allUsers = User::all()->groupBy('role');
        // foreach($allUsers as $role => $users){
        //     dd($role);
        // }
        $roles = config('auth.role');
        $roles = array_flip($roles);
        $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) {
            $join->on('properties.id', '=', 'properties_directories.property_id')
                ->whereIn('properties.status', array(6,11,13))
                ->where('properties_directories.parent_id', '=', 0);
        })->get();
        // dd($roles);

        return view('module_permission.index', compact( 'allUsers', 'roles', 'propertiesDirectory'));
    }

    public function getUserPermission(Request $request){

        $permission = $request->permission;
        $data = ModulePermission::where('permission', $permission)->get();
        return response()->json(array(
			'success' => true,
			'data' => $data
		));

    }

    
    public function update(Request $request)
    {

        $res = $request->user_role;
        if(!is_array($res)){
            return response()->json(array(
                'success' => false,
                'message' => "Not saved"                
            ));
        }
        $old_permission = ModulePermission::where('permission', $request->permission)->pluck('user_id')->toArray();
        $allUsers = User::pluck('id')->toArray();
        $new_user = [];
        $remove_user = [];
        foreach($allUsers as $userId){
            if(in_array($userId, $res))
            {
                if(in_array($userId, $old_permission)){
                    $remove_user[] = $userId;
                }
            }else{
                if(!in_array($userId, $old_permission)){
                    $new_user[] = [
                        'permission' => $request->permission,
                        'user_id' => $userId
                    ];
                }
            }
        }
        ModulePermission::where('permission', $request->permission)->whereIn('user_id', $remove_user)->delete();
        ModulePermission::insert($new_user);
        return response()->json(array(
			'success' => true,
			'message' => 'User permission saved successfully.',
			'data' => []
        ));
        
    }

}
