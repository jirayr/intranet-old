<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Exports\ExcelExport;
use App\Exports\ExportWaultBestand;
use App\Models\Banks;
use App\Models\Notifications;
use App\Models\Properties;
use App\Models\PropertiesBuyDetail;
use App\Models\PropertiesSaleDetail;
use App\Models\StatusLoi;
use App\Models\TenancyScheduleItem;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Services\ExportHistoryService;

ini_set('memory_limit','-1');

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function vreleaselogs(Request $request)
    {
        // echo "releaselogs"; die;
        $statuss = array(6);
        $strmat  = '""';
        $q       = '';

        $properties_all = Properties::select('main_property_id', 'plz_ort', 'ort')
            ->whereRaw('main_property_id!=0 and bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat . $q)
            ->whereIn('properties.status', $statuss)
            ->where('property_status', 0)
            ->groupBy('properties.main_property_id')
            ->get();

        $ids      = array();
        $plz_name = array();
        $ort_name = array();
        foreach ($properties_all as $key => $value) {
            $ids[]                              = $value->main_property_id;
            $plz_name[$value->main_property_id] = $value->plz_ort;
            $ort_name[$value->main_property_id] = $value->ort;
        }

        $data = PropertiesSaleDetail::whereIn('type', array('btn1', 'btn2', 'btn3', 'btn4', 'btn5'))->whereIn('property_id', $ids)->get();

        $name = array();
        foreach ($data as $key => $value) {
            $name[$value->property_id]['plz']        = $plz_name[$value->property_id];
            $name[$value->property_id]['ort']        = $ort_name[$value->property_id];
            $name[$value->property_id][$value->type] = date_formatting(date('d/m/Y', strtotime($value->created_at)));
        }
        $table_id = "vlog-table2";

        $a = View::make('dashboard.vreleaselogs', array('name' => $name, 'table_id' => $table_id))->render();
        return $a;

        print_r($d);

    }

    public function releaselogs(Request $request)
    {
        // echo "releaselogs"; die;
        $statuss = array(15, 7, 5, 14, 16, 10, 12);
        $strmat  = '""';
        $q       = '';

        $properties_all = Properties::select('properties.id', 'properties.name_of_property', 'properties.main_property_id', 'properties.plz_ort', 'properties.ort', 'am_u.id as am_id', 'am_u.name as am_name', 'tm_u.id as tm_id', 'tm_u.name as tm_name')
            ->leftJoin('users as am_u', 'am_u.id', '=', 'properties.asset_m_id')
            ->leftJoin('users as tm_u', 'tm_u.id', '=', 'properties.transaction_m_id')
            ->whereRaw('properties.main_property_id!=0 and properties.bank_ids is not null and properties.bank_ids!="" and properties.bank_ids!=' . $strmat . $q)
            ->whereIn('properties.status', $statuss)
            ->where('properties.property_status', 0)
            ->groupBy('properties.main_property_id')
            ->get();

        $ids = $plz_name = $asset_manager = $transaction_manager = $property_name =  array();

        foreach ($properties_all as $key => $value) {
            $ids[]                              = $value->main_property_id;
            $plz_name[$value->main_property_id] = $value->plz_ort;
            $ort_name[$value->main_property_id] = $value->ort;
            $asset_manager[$value->main_property_id] = ['id' => $value->am_id, 'name' => $value->am_name, 'p_id' => $value->id];
            $transaction_manager[$value->main_property_id] = ['id' => $value->tm_id, 'name' => $value->tm_name, 'p_id' => $value->id];
            $property_name[$value->main_property_id] = $value->name_of_property;
        }

        $data = PropertiesBuyDetail::whereIn('type', array('btn1', 'btn2', 'btn3', 'btn4', 'btn5', 'btn6'))->whereIn('property_id', $ids)->get();
        
        $name = array();
        foreach ($data as $key => $value) {
            $name[$value->property_id]['plz']        = $plz_name[$value->property_id];
            $name[$value->property_id]['ort']        = $ort_name[$value->property_id];
            $name[$value->property_id][$value->type] = date_formatting(date('d/m/Y', strtotime($value->created_at)));
            $name[$value->property_id]['am']         = $asset_manager[$value->property_id];
            $name[$value->property_id]['tm']         = $transaction_manager[$value->property_id];
            $name[$value->property_id]['name']       = $property_name[$value->property_id];
        }

        $table_id = "vlog-table";

        $a = View::make('dashboard.releaselogs', array('name' => $name, 'table_id' => $table_id))->render();
        return $a;

        print_r($d);

    }
    public function factsheet(Request $request)
    {

        $data = array();
        $url  = "https://verkauf.fcr-immobilien.de/getuploadlist";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $str = curl_exec($curl);
        curl_close($curl);
        $ids = array();
        if ($str) {
            $ids = json_decode($str, true);
        }

        $ads = Ad::whereIn('ads.id', $ids)->join('states', 'states.id', '=', 'state_id')
            ->join('properties', 'ads.property_id', '=', 'main_property_id')
            ->where('purpose', 'sale')->where('property_status', 0)->orderBy('plz_ort', 'asc')->groupBy('ads.id')->get();

        $prs = Properties::join('ads', 'ads.property_id', '=', 'main_property_id')->whereIn('ads.id', $ids)->where('purpose', 'sale')->where('property_status', 0)->groupBy('ads.id')->orderBy('plz_ort', 'asc')->get();

        $mf_array = array();
        foreach ($ads as $key => $value) {
            $mf_array[$value->property_id] = $value->rentable_area;
        }

        $a = array('shopping_mall' => 'Einkaufszentrum', 'retail_center' => 'Fachmarktzentrum', 'specialists' => 'Fachmarkt', 'retail_shop' => 'Einzelhandel', 'office' => 'Büro', 'logistik' => 'Logistik', 'commercial_space' => 'Kommerzielle Fläche', 'land' => 'Grundstück', 'apartment' => 'Wohnung', 'condos' => 'Eigentumswohnung', 'house' => 'Haus', 'living_and_business' => 'Wohn- und Geschäftshaus', 'villa' => 'Villa', 'hotel' => 'Hotel', 'nursing_home' => 'Pflegeheim', 'multifamily_house' => 'Mehrfamilienhaus');

        foreach ($prs as $key => $value) {
            if ($value['type'] && isset($a[$value['type']])) {
                $value->property_type = $a[$value['type']];
            }

        }

        $ads2 = Ad::whereIn('ads.id', $ids)->join('states', 'states.id', '=', 'state_id')
            ->where('purpose', 'sale')->first();

        $pdf = PDF::loadView('pdf.factsheet', compact('data', 'ads', 'prs', 'mf_array', 'ads2'))->setPaper('a4', 'landscape');

        $filename = 'factsheet_'.rand().'.pdf';
        $path = public_path().'/export/'.$filename;
        $pdf->save($path);

        ExportHistoryService::create('Export Verkaufsobjekte', $filename);

        return $pdf->stream($filename);
        // $slug = rand();
        // $filename = Auth::user()->id . $slug . '.pdf';
        // return $pdf->download($filename);
    }

    public function searchproperty(Request $request)
    {
        if ($request->search) {
            $q              = $request->search;
            $properties_all = Properties::
                whereRaw('(main_property_id!=0) and (plz_ort LIKE "%' . $q . '%" OR  ort LIKE "%' . $q . '%" OR  notizen LIKE "%' . $q . '%" OR  name_of_property LIKE "%' . $q . '%") and standard_property_status=1 and property_status=' . $request->property_status)
                ->groupBy('properties.main_property_id')
                ->get();

            $a = View::make('dashboard.search_result', array('data' => $properties_all))->render();
            return $a;
        }
    }

    public function export_property_pdf(Request $request)
    {
        // echo "here"; die;
        $banks   = Banks::all();
        $statuss = array(10, 12, 14, 16, 600);
        $strmat  = '""';

        foreach ($statuss as $key => $status) {

            $data = array();

            $custom_status = 0;
            if ($status == 600) {
                $status        = 6;
                $custom_status = 1;
            }

            $array[$status] = array();

            $q = ' and properties.status=' . $status;
            if ($custom_status) {
                $q .= " and ads.purpose='exclusive'";
                // print_r($q);die;
            }

            $properties_all = Properties::select('properties.bank_ids', 'properties.id', 'properties.status', 'users.name')
                ->leftJoin('users', 'users.id', '=', 'properties.transaction_m_id')
                ->leftJoin('ads', 'properties.id', '=', 'ads.property_id')
                ->where('Ist', 0)
                ->where('soll', 0)
                ->where('property_status', 0)
                ->whereRaw('bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat . $q)
                ->where('properties.status', $status)
                ->groupBy('properties.id')
                ->groupBy('properties.bank_ids')
                ->groupBy('properties.status')
                ->orderBy('users.name', 'asc')
                ->paginate(50000);

            $properties_all = json_decode(json_encode($properties_all), true);
            if (count($properties_all['data']) > 0) {

                for ($i = 0; $i < count($properties_all['data']); $i++) {

                    $bank_ids = $properties_all['data'][$i]['bank_ids'];

                    if ($bank_ids !== '""') {

                        $bank_ids = json_decode($bank_ids, true);

                        for ($j = 0; $j < count($bank_ids); $j++) {

                            $ind = $bank_ids[$j];

                            $propertiesAll = Properties::select('properties.*', 'ads.id as adid', 'users.name')
                                ->leftJoin('ads', 'properties.main_property_id', '=', 'ads.property_id')
                                ->leftJoin('users', 'users.id', '=', 'properties.seller_id')
                                ->where('main_property_id', $properties_all['data'][$i]['id'])
                                ->where('standard_property_status', 1)
                                ->where('property_status', 0)
                                ->first();

                            if (isset($propertiesAll->id)) {

                                $data[$i] = json_decode(json_encode($propertiesAll), true);
                                break;
                            }
                        }

                        $data[$i]['property_actual_id']     = $properties_all['data'][$i]['id'];
                        $data[$i]['property_actual_status'] = $properties_all['data'][$i]['status'];

                        $array[$status] = json_decode(json_encode($data));

                    }
                }
            }
        }

        $pdf = PDF::loadView('pdf.properties', compact('array'));

        $filename = 'Transaction_Meeting_Auszug_Intranet_'.rand().'.pdf';
        $path = public_path().'/export/'.$filename;
        $pdf->save($path);

        ExportHistoryService::create('Transaction Meeting', $filename);

        return $pdf->stream($filename);
    }

    public function getstatustabledashboard(Request $request)
    {
        $banks  = Banks::all();
        $status = array(10, 12);
        $strmat = '""';

        $properties_all = Properties::select('properties.bank_ids', 'properties.id', 'properties.status')
            ->leftJoin('ads', 'properties.id', '=', 'ads.property_id')
            ->where('Ist', 0)
            ->where('soll', 0)
            ->whereRaw('bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat)
            ->whereIn('properties.status', $status)
            ->where('property_status', 0)
            ->groupBy('properties.id')
            ->groupBy('properties.bank_ids')
            ->groupBy('properties.status')
            ->paginate(50000);

        $properties_all = json_decode(json_encode($properties_all), true);
        if (count($properties_all['data']) > 0) {

            for ($i = 0; $i < count($properties_all['data']); $i++) {

                $bank_ids = $properties_all['data'][$i]['bank_ids'];

                if ($bank_ids !== '""') {

                    $bank_ids = json_decode($bank_ids, true);

                    for ($j = 0; $j < count($bank_ids); $j++) {

                        $ind = $bank_ids[$j];

                        $propertiesAll = Properties::select('properties.*', 'ads.id as adid', 'users.name')
                            ->leftJoin('ads', 'properties.main_property_id', '=', 'ads.property_id')
                            ->leftJoin('users', 'users.id', '=', 'properties.seller_id')
                            ->where('main_property_id', $properties_all['data'][$i]['id'])
                            ->where('standard_property_status', 1)
                            ->first();

                        if (isset($propertiesAll->id)) {

                            $data[$i] = json_decode(json_encode($propertiesAll), true);
                            break;
                        }
                    }

                    $data[$i]['property_actual_id']     = $properties_all['data'][$i]['id'];
                    $data[$i]['property_actual_status'] = $properties_all['data'][$i]['status'];
                }
            }
        }
        $a = View::make('dashboard.dashboard_table_partial2', array('data' => json_decode(json_encode($data))))->render();
        return $a;
    }

    public function getstatustable(Request $request){

        $perPages      = 500;
        $banks         = Banks::all();
        $ids           = array();
        $strmat        = '""';
        $custom_status = 0;

        if($request->status != 0){

            if ($request->status == 600) {
                $custom_status   = 1;
                $request->status = 6;
            }

            if ($request->status == 11 || $request->status == 8 || $request->status == 6) {
                // $ads = Ad::join()->get()->toArray();

                $url  = "https://verkauf.fcr-immobilien.de/getuploadlist";
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $str = curl_exec($curl);
                curl_close($curl);
                if ($str) {
                    $ids = json_decode($str, true);
                }

            }
            //
            //            $properties_all = Properties::select('properties.*','ads.id as adid')
            //                             ->leftJoin('ads','properties.id','=','ads.property_id')
            //                             ->where('Ist',0)
            //                             ->where('soll',0)
            //                             ->where('properties.status','=',$request->status)
            //            ->paginate($perPages);

            $q = ' and properties.status=' . $request->status;
            // if($request->status==15)
            // $q = " and quick_status=1";
            if ($custom_status) {
                $q .= " and ads.purpose='exclusive'";
            }




            $data           = array();
            $properties_all = Properties::select('properties.bank_ids', 'properties.id', 'properties.status')
                ->leftJoin('ads', 'properties.id', '=', 'ads.property_id')
                ->where('Ist', 0)
                ->where('soll', 0)
                ->where('property_status', $request->property_status)
                ->whereRaw('bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat . $q)
                ->groupBy('properties.id')
                ->groupBy('properties.bank_ids')
                ->groupBy('properties.status')
                ->paginate($perPages);

            $properties_all = json_decode(json_encode($properties_all), true);
            if (count($properties_all['data']) > 0) {

                for ($i = 0; $i < count($properties_all['data']); $i++) {

                    $bank_ids = $properties_all['data'][$i]['bank_ids'];

                    if ($bank_ids !== '""') {

                        $bank_ids = json_decode($bank_ids, true);

                        for ($j = 0; $j < count($bank_ids); $j++) {

                            $ind = $bank_ids[$j];

                            $propertiesAll = Properties::select('properties.*', 'ads.id as adid', 'users.name')
                                ->leftJoin('ads', 'properties.main_property_id', '=', 'ads.property_id')
                                ->leftJoin('users', 'users.id', '=', 'properties.seller_id')
                            // ->where(function($query) use ($ind){
                            //     $query->OrWhere('Ist',$ind);
                            //     // $query->OrWhere('soll',$ind);
                            // })
                                ->where('main_property_id', $properties_all['data'][$i]['id'])
                                ->where('standard_property_status', 1)
                                ->first();

                            if (isset($propertiesAll->id)) {

                                $data[$i] = json_decode(json_encode($propertiesAll), true);
                                break;
                            }

                        }

                        $data[$i]['property_actual_id']     = $properties_all['data'][$i]['id'];
                        $data[$i]['property_actual_status'] = $properties_all['data'][$i]['status'];

                        /*
                    $properties_all = Properties::select('properties.*','ads.id as adid')
                    ->leftJoin('ads','properties.id','=','ads.property_id')
                    ->where('properties.standard_property_status','=', '1')
                    ->whereIn()
                    ->first($perPages);

                    echo '<pre>';
                    print_r($bank_ids);
                    echo '</pre>';

                     */

                    }

                }

            }

            if (isset($_REQUEST['export'])) {

                $status = $request->status;
                $data   = json_decode(json_encode($data));
                $banks  = $banks;
                $ids    = $ids;

                $status_name = "";

                $property_data       = array();
                $i                   = 0;
                $property_data[$i][] = "#";
                $property_data[$i][] = __('dashboard.property_name');

                if ($status >= 1 || $status <= 14) {
                    $property_data[$i][] = "TM";
                }

                if ($status >= 1 || $status <= 14) {
                    $property_data[$i][] = "AM";
                }

                if ($status == config('properties.status.in_sale') || $status == config('properties.status.sold')) {
                    $property_data[$i][] = config('dashboard.seller');
                }

                if ($status == "6") {
                    $property_data[$i][] = "BNL";
                }

                $property_data[$i][] = "EK Preis";

                $property_data[$i][] = __('dashboard.total_purchase_price');
                $property_data[$i][] = __('dashboard.gross_return');
                $property_data[$i][] = "EK/GuV";
                $property_data[$i][] = __('dashboard.ek_cf');

                if ($status != "6" && $status != "8") {
                    if ($status != "11") {
                        $property_data[$i][] = __('dashboard.maklerpreis');
                    }
                }

                if ($status == "6" || $status == "8"):
                    $property_data[$i][] = "Miete p.a";
                    $property_data[$i][] = "VK Preis";
                endif;
                if ($status == "11") {
                    $property_data[$i][] = "Verkehrswert";
                }

                if ($status != "6" && $status != "8") {
                    if ($status != "11") {
                        $property_data[$i][] = __('dashboard.price_difference');
                    }
                }

                if ($status == 14) {
                    $property_data[$i][] = "D. Exkl.";
                }

                if ($status == 10) {
                    $property_data[$i][] = "Notartermin";
                }

                if ($status == 6) {
                    $property_data[$i][] = "Fläche in m2";
                    $property_data[$i][] = "Vermietet m2";
                    $property_data[$i][] = "Potential p.a.";
                }

                if(isset($_REQUEST['cmw']))
                {
                    $property_data[$i][] = "Ankermieter";
                    $property_data[$i][] = "Wault";
                }


                $property_data[$i][] = __('property.listing.status');

                $i = 1;
                $not_shown_array  = config('properties.not_shown_properties_id');
                $notShowIds = Properties::select('id')->where('is_not_an_existing', 1)->get();
                if($notShowIds){
                    foreach ($notShowIds as $notShowId) {
                        $not_shown_array[] = $notShowId->id;
                    }
                }


                foreach ($data as $property):

                    if (!isset($property->bank_ids)) {
                        continue;
                    }
                    // $not_shown_array[] = 119;
                    if(in_array($property->property_actual_id, $not_shown_array))
                        continue;

                    $propertiesExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();
                    $total_ccc         = 0;
                    $a = array();
                    $neitoo = 0;
                    $Wault = 0;
                    if ($status == 6 || $status == 8) {
                        $tenancy_schedule_data = \App\Services\TenancySchedulesService::get($property->property_actual_id, 0);
                        $area = $rented_area = $pot_pa = 0;
                        foreach ($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule) {

                            $Wault = $tenancy_schedule->calculations['wault'];
                            $neitoo += 12 * $tenancy_schedule->calculations['total_business_actual_net_rent'];
                            $neitoo += 12 * $tenancy_schedule->calculations['total_live_actual_net_rent'];

                            $rented_area = $tenancy_schedule->calculations['total_live_rental_space'] + $tenancy_schedule->calculations['total_business_rental_space'];
                            $area = $tenancy_schedule->calculations['total_rental_space'];

                            $pot_pa = $tenancy_schedule->calculations['potenzial_eur_jahr'];
                            foreach($tenancy_schedule->items as $item){

                                if($item->type == config('tenancy_schedule.item_type.business'))
                                {
                                    if($item->status)
                                    {
                                        $a[$item->actual_net_rent] = $item->name;
                                    }
                                }
                            }
                        }

                    }

                    krsort($a);
                    $a  = array_values($a);
        

                    foreach ($propertiesExtra1s as $propertiesExtra1):
                        $total_ccc += $propertiesExtra1->net_rent_p_a;
                    endforeach;
                    $property->net_rent_pa = $total_ccc;

                    $property->net_rent_increase_year1 = $total_ccc;
                    $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1 * $property->net_rent;
                    $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2 * $property->net_rent;
                    $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3 * $property->net_rent;
                    $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4 * $property->net_rent;

                    $property->operating_cost_increase_year1 = $property->net_rent_increase_year1 * $property->operating_costs_nk;

                    $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1 * $property->operating_costs;

                    // echo $property->operating_cost_increase_year1;
                    // echo "<br>";
                    // echo $property->operating_costs_nk;

                    $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2 * $property->operating_costs;

                    $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3 * $property->operating_costs;
                    $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4 * $property->operating_costs;

                    $property->property_management_increase_year1 = $property->net_rent_increase_year1 * $property->object_management_nk;

                    $property->property_management_increase_year2 = $property->property_management_increase_year1 + $property->property_management_increase_year1 * $property->object_management;

                    $property->property_management_increase_year3 = $property->property_management_increase_year2 + $property->property_management_increase_year2 * $property->object_management;

                    $property->property_management_increase_year4 = $property->property_management_increase_year3 + $property->property_management_increase_year3 * $property->object_management;

                    $property->property_management_increase_year5 = $property->property_management_increase_year4 + $property->property_management_increase_year4 * $property->object_management;

                    $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                    $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                    $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                    $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                    $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                    $property->ebit_year_1 = $property->ebitda_year_1 - $property->depreciation_nk_money;
                    $property->ebit_year_2 = $property->ebitda_year_2 - $property->depreciation_nk_money;
                    $property->ebit_year_3 = $property->ebitda_year_3 - $property->depreciation_nk_money;
                    $property->ebit_year_4 = $property->ebitda_year_4 - $property->depreciation_nk_money;
                    $property->ebit_year_5 = $property->ebitda_year_5 - $property->depreciation_nk_money;

                    /*if(json_decode($property->bank_ids,true)){
                    $bank_ids = json_decode($property->bank_ids,true);
                    if($bank_ids != null) {
                    foreach ($banks as $b) {
                    if(isset($bank_ids[0]) && $b->id == $bank_ids[0]){
                    $bank = $b;
                    break;
                    }
                    }
                    }
                    }*/

                    $D42 = $property->gesamt_in_eur
                         + ($property->real_estate_taxes * $property->gesamt_in_eur)
                         + ($property->estate_agents * $property->gesamt_in_eur)
                         + (($property->Grundbuch * $property->gesamt_in_eur) / 100)
                         + ($property->evaluation * $property->gesamt_in_eur)
                         + ($property->others * $property->gesamt_in_eur)
                         + ($property->buffer * $property->gesamt_in_eur);

                    $L25              = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                    $price_difference = ($property->maklerpreis == 0) ? 1 : 1 - ($property->gesamt_in_eur / $property->maklerpreis);

                    // if(isset($bank)){
                    $E18 = ($property->net_rent_increase_year1
                         - $property->maintenance_increase_year1
                         - $property->operating_cost_increase_year1
                         - $property->property_management_increase_year1)
                     - $property->depreciation_nk_money;

                    $D49 = $property->bank_loan * $D42;
                    $D48 = $property->from_bond * $D42;
                    $H48 = $D49 * $property->interest_bank_loan;
                    $H49 = $D49 * $property->eradication_bank;
                    $H52 = $D48 * $property->interest_bond;

                    $E23 = $E18 - $H48 - $H52;
                    $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                    $G61 = ($D48 == 0) ? 0 : $E31 / $D48;
                    $G62 = ($D48 == 0) ? 0 : $E23 / $D48;
                    // }
                    $G58 = ($D42 == 0) ? 0 : $property->net_rent_pa / $D42;

                    if (!isset($G61)) {
                        $G61 = 0;
                    }
                    $property_data[$i][] = $property->property_actual_id;
                    $property_data[$i][] = $property->name_of_property;

                    if ($status >= 1 || $status <= 14):
                        $user = DB::table('properties')->join('users', 'users.id', '=', 'properties.transaction_m_id')->where('properties.id', $property->property_actual_id)->first();
                        if ($user) {
                            $property_data[$i][] = $this->short_name($user->name);
                        } else {
                            $property_data[$i][] = "";
                        }

                    endif;

                    if ($status >= 1 || $status <= 14):
                        $user = DB::table('properties')->join('users', 'users.id', '=', 'properties.asset_m_id')->where('properties.id', $property->property_actual_id)->first();

                        if ($user) {
                            $property_data[$i][] = $this->short_name($user->name);
                        } else {
                            $property_data[$i][] = "";
                        }

                    endif;

                    if ($status == config('properties.status.in_sale') || $status == config('properties.status.sold')):

                        $user = DB::table('properties')->join('users', 'users.id', '=', 'properties.seller_id')->where('properties.id', $property->property_actual_id)->first();

                        if ($user) {
                            $property_data[$i][] = $this->short_name($user->name);
                        } else {
                            $property_data[$i][] = "";

                        }
                    endif;

                    if ($status == "6") {
                        $buyd = DB::table('properties_buy_details')->whereRaw('(type=402) and property_id=' . $property->property_actual_id)->first();
                        if ($buyd && $buyd->comment) {
                            $property_data[$i][] = show_date_format($buyd->comment);
                        } else {
                            $property_data[$i][] = "";
                        }

                    }

                    $property_data[$i][] = number_format(($property->gesamt_in_eur), 0, ",", ".") . "€";

                    $property_data[$i][] = number_format($D42, 0, ",", ".") . "€";
                    $property_data[$i][] = number_format(($G58 * 100), 2, ",", ".") . "%";
                    $property_data[$i][] = number_format(($G62 * 100), 2, ",", ".") . "%";

                    $property_data[$i][] = number_format(($G61 * 100), 2, ",", ".") . "%";
                    if ($status != "6" && $status != "8") {
                        if ($status != "11") {
                            $property_data[$i][] = number_format(($property->maklerpreis), 0, ",", ".") . "€";
                        }
                    }

                    if ($status == "6" || $status == "8"):
                        $property_data[$i][] = number_format(($neitoo), 0, ",", ".") . "€";
                        $property_data[$i][] = number_format(($property->preisverk), 0, ",", ".") . "€";
                    endif;
                    if ($status == "11") {
                        $property_data[$i][] = number_format(($property->preisverk), 0, ",", ".") . "€";
                    }

                    if ($status != "6" && $status != "8") {
                        if ($status != "11") {
                            $property_data[$i][] = number_format(($price_difference) * 100, 2, ",", ".") . "%";
                        }
                    }

                    if ($status == "14"):
                        if ($property->exklusivität_bis && strtotime(str_replace('/', '-', $property->exklusivität_bis))) {
                            $property_data[$i][] = date('d/m/Y', strtotime(str_replace('/', '-', $property->exklusivität_bis)));
                        } else {
                            $property_data[$i][] = "";
                        }

                    endif;

                    if ($status == "10"):
                        if ($property->purchase_date && $property->purchase_date != "0000-00-00") {
                            $property_data[$i][] = date('d/m/Y', strtotime($property->purchase_date));
                        } else {
                            $property_data[$i][] = "";
                        }

                    endif;

                    if ($status == "6"):
                    $property_data[$i][] = show_number($area,2);
                    $property_data[$i][] = show_number($rented_area,2);
                    $property_data[$i][] = show_number($pot_pa,2);
                    endif;

                    if(isset($_REQUEST['cmw']))
                    {
                        $property_data[$i][] = (isset($a[0])) ? $a[0] : "";
                        $property_data[$i][] = $Wault;
                    }


                    if ($property->property_actual_status == config('properties.status.buy')) {
                        $status_name = $property_data[$i][] = __('property.buy');
                    }

                    if ($property->property_actual_status == config('properties.status.hold')) {
                        $status_name = $property_data[$i][] = __('property.hold');
                    }

                    if ($property->property_actual_status == config('properties.status.decline')) {
                        $status_name = $property_data[$i][] = __('property.decline');
                    }

                    if ($property->property_actual_status == config('properties.status.declined')) {
                        $status_name = $property_data[$i][] = __('property.declined');
                    }

                    if ($property->property_actual_status == config('properties.status.offer')) {
                        $status_name = $property_data[$i][] = __('property.offer');
                    }

                    if ($property->property_actual_status == config('properties.status.duration')) {
                        $status_name = $property_data[$i][] = __('property.duration');
                    }

                    if ($property->property_actual_status == config('properties.status.in_purchase')) {
                        $status_name = $property_data[$i][] = __('property.in_purchase');
                    }

                    if ($property->property_actual_status == config('properties.status.sold')) {
                        $status_name = $property_data[$i][] = __('property.sold');
                    }

                    if ($property->property_actual_status == config('properties.status.lost')) {
                        $status_name = $property_data[$i][] = __('property.lost');
                    }

                    if ($property->property_actual_status == config('properties.status.exclusivity')) {
                        $status_name = $property_data[$i][] = __('property.exclusivity');
                    }

                    if ($property->property_actual_status == config('properties.status.certified')) {
                        $status_name = $property_data[$i][] = __('property.certified');
                    }

                    if ($property->property_actual_status == config('properties.status.exclusive')) {
                        $status_name = $property_data[$i][] = __('property.exclusive');
                    }

                    $i++;

                endforeach;

                // print_r($property_data); die;

                $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
                $sheet       = $spreadsheet->getActiveSheet();
                $sheet->setTitle($status_name);
                $sheet->fromArray($property_data);
                $sheet->getDefaultColumnDimension()->setWidth(20);

                $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

                $filename = 'transaction_status_'.rand().'.xlsx';
                $path = public_path().'/export/'.$filename;
                $writer->save($path);

                ExportHistoryService::create($status_name, $filename);

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                $file_name = $status_name . "-Property-list.xlsx";
                header('Content-Disposition: attachment; filename=' . $file_name);
                $writer->save("php://output");

                die;
            }

            $a = View::make('dashboard.dashboard_table_partial', array('status' => $request->status, 'data' => json_decode(json_encode($data)), 'banks' => $banks, 'ids' => $ids, 'custom_status' => $custom_status, 'property_status' => $request->property_status, 'type' => $request->type, 'subtype' => $request->subtype, 'actual_status' => $request->status))->render();
            return $a;

        } else {
            $data           = array();
            $properties_all = Properties::select('properties.bank_ids', 'properties.id', 'properties.status')
                ->leftJoin('ads', 'properties.id', '=', 'ads.property_id')
                ->where('Ist', 0)
                ->where('soll', 0)
                ->where('property_status', $request->property_status)
                ->whereRaw('bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat)
                ->groupBy('properties.id')
                ->groupBy('properties.bank_ids')
                ->groupBy('properties.status')
                ->paginate($perPages);

            $properties_all = json_decode(json_encode($properties_all), true);

            // print_r($properties_all); die;

            if (count($properties_all['data']) > 0) {

                for ($i = 0; $i < count($properties_all['data']); $i++) {

                    $bank_ids = $properties_all['data'][$i]['bank_ids'];
                    // $bank_ids = str_replace('[', '', $bank_ids);
                    // $bank_ids = str_replace(']', '', $bank_ids);

                    if ($bank_ids !== '""') {

                        // $bank_ids = explode(',', $bank_ids);
                        $bank_ids = json_decode($bank_ids, true);

                        for ($j = 0; $j < count($bank_ids); $j++) {

                            $ind = $bank_ids[$j];

                            $propertiesAll = Properties::select('properties.*', 'ads.id as adid')
                                ->leftJoin('ads', 'properties.id', '=', 'ads.property_id')
                            // ->where(function($query) use ($ind){
                            //     $query->OrWhere('Ist',$ind);
                            //     $query->OrWhere('soll',$ind);
                            // })
                                ->where('main_property_id', $properties_all['data'][$i]['id'])
                                ->where('standard_property_status', 1)
                                ->first();

                            if (isset($propertiesAll->id)) {

                                $data[$i] = json_decode(json_encode($propertiesAll), true);
                                break;
                            }

                        }

                        $data[$i]['property_actual_id']     = $properties_all['data'][$i]['id'];
                        $data[$i]['property_actual_status'] = $properties_all['data'][$i]['status'];

                        /*
                    $properties_all = Properties::select('properties.*','ads.id as adid')
                    ->leftJoin('ads','properties.id','=','ads.property_id')
                    ->where('properties.standard_property_status','=', '1')
                    ->whereIn()
                    ->first($perPages);

                    echo '<pre>';
                    print_r($bank_ids);
                    echo '</pre>';

                     */

                    }

                }

            }

            $a = View::make('dashboard.dashboard_table_partial', array('status' => $request->status, 'data' => json_decode(json_encode($data)), 'banks' => $banks, 'ids' => $ids, 'custom_status' => $custom_status, 'property_status' => $request->property_status, 'actual_status' => $request->status))->render();
            return $a;

        }
    }

    public function getData()
    {
        $perPages = 500;
        $banks    = Banks::all();
        $ids      = array();
        $strmat   = '""';
//
        //            $properties_all = Properties::select('properties.*','ads.id as adid')
        //                             ->leftJoin('ads','properties.id','=','ads.property_id')
        //                             ->where('Ist',0)
        //                             ->where('soll',0)
        //                             ->where('properties.status','=',$request->status)
        //            ->paginate($perPages);

        $data           = array();
        $properties_all = Properties::select('properties.bank_ids', 'properties.id', 'properties.status')
            ->leftJoin('ads', 'properties.id', '=', 'ads.property_id')
            ->where('Ist', 0)
            ->where('soll', 0)
            ->where('property_status', 0)
            ->where('properties.status', '=', 6)
            ->whereRaw('bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat)
            ->groupBy('properties.id')
            ->groupBy('properties.bank_ids')
            ->groupBy('properties.status')
            ->paginate($perPages);

        $properties_all = json_decode(json_encode($properties_all), true);

        if (count($properties_all['data']) > 0) {

            for ($i = 0; $i < count($properties_all['data']); $i++) {

                $bank_ids = $properties_all['data'][$i]['bank_ids'];

                if ($bank_ids !== '""') {

                    $bank_ids = json_decode($bank_ids, true);

                    for ($j = 0; $j < count($bank_ids); $j++) {
                        $ind           = $bank_ids[$j];
                        $propertiesAll = Properties::select('properties.*', 'ads.id as adid', 'users.name')
                            ->leftJoin('ads', 'properties.main_property_id', '=', 'ads.property_id')
                            ->leftJoin('users', 'users.id', '=', 'properties.seller_id')
                        // ->where(function($query) use ($ind){
                        //     $query->OrWhere('Ist',$ind);
                        //     // $query->OrWhere('soll',$ind);
                        // })
                            ->where('main_property_id', $properties_all['data'][$i]['id'])
                            ->where('standard_property_status', 1)
                            ->first();

                        if (isset($propertiesAll->id)) {
                            $data[$i] = json_decode(json_encode($propertiesAll), true);
                            break;
                        }
                    }

                    $data[$i]['property_actual_id']     = $properties_all['data'][$i]['id'];
                    $data[$i]['property_actual_status'] = $properties_all['data'][$i]['status'];

                    /*
                $properties_all = Properties::select('properties.*','ads.id as adid')
                ->leftJoin('ads','properties.id','=','ads.property_id')
                ->where('properties.standard_property_status','=', '1')
                ->whereIn()
                ->first($perPages);

                echo '<pre>';
                print_r($bank_ids);
                echo '</pre>';

                 */
                }

            }

        }

        $someData = array();
        foreach (json_decode(json_encode($data)) as $value) {
            array_push($someData, $value->property_actual_id);
        }

        $propertiesAll = TenancyScheduleItem::select('tenancy_schedule_items.*', 'properties.name_of_property', 'asset_manager_id', 'properties.id')
            ->join('tenancy_schedules', 'tenancy_schedules.id', '=', 'tenancy_schedule_items.tenancy_schedule_id')
            ->join('properties', 'properties.id', '=', 'tenancy_schedules.property_id')
            ->whereIn('properties.id', $someData)
            ->where(function ($query) {
                $query->where('tenancy_schedule_items.type', 1)
                    ->orWhere('tenancy_schedule_items.type', '=', 2);
            })
            ->orderBy('properties.name_of_property')
            ->get();

        $newData = array();

        foreach ($propertiesAll as $item) {
            array_push($newData, [
                'name'             => $item->name,
                'name_of_property' => $item->name_of_property,
                'rent_begin'       => $item->rent_begin,
                'rent_end'         => $item->rent_end,
                'use'              => $item->use,
                'rental_space'     => $item->rental_space,
                'actual_net_rent'  => $item->actual_net_rent,
            ]);
        }
        $header = ['Mieter', 'Objekt', 'Mietbeginn', 'Mietende', 'Nutzung', 'Fläche (m2)', 'Miete netto (€)'];
        return Excel::download(new ExcelExport($header, $newData), 'excel.xlsx');

    }

    public function index()
    {
        if(Auth::user()->role >= 6 || Auth::user()->second_role >= 6){
            return redirect('/extern_user');
        }
        $properties_in_purchase = array();
        $properties_sold        = array();
        $properties_duration    = array();
        $properties_declined    = array();
        $properties_lost        = array();
        $properties_exclusivity = array();
        $properties_in_sale     = array();
        $properties_certified   = array();
        $properties_offer       = array();
        $properties_all         = array();
        $analysis_array         = array();

        $total_count       = 0;
        $user_id           = Auth::user()->id;
        $bestand_total     = 0;
        $vc_total = $vc_total1 = $vc_total2 = 0;
        $analysis_array    = array();
        $ek_analysis_array = array();

        $strmat = '""';

        $url  = "https://verkauf.fcr-immobilien.de/getuploadlist";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $str = curl_exec($curl);
        curl_close($curl);
        if ($str) {
            $ids = json_decode($str, true);
        }
        $not_shown_array  = config('properties.not_shown_properties_id');

        $notShowIds = Properties::select('id')->where('is_not_an_existing', 1)->get();
        if($notShowIds){
            foreach ($notShowIds as $notShowId) {
                $not_shown_array[] = $notShowId->id;
            }
        }

        $analysis = Properties::selectRaw('status,count(id) as id,sum(gesamt_in_eur) as gesamt_in_eur,sum(preisverk) as preisverk,group_concat(main_property_id) as prids')->where('standard_property_status', 1)->whereNotIn('main_property_id', $not_shown_array)->groupBy('status')->where('property_status', 0)->get()->toArray();

        $bestandcustomcount  = 0;
        $bestandcustomamount = 0;
        $bestandcustomvk     = 0;
        $hhh                 = "";
        $cu_arr              = array();
        $d48_array           = array();
        $market_array        = array();
        foreach ($analysis as $key => $value) {

            $ar1 = explode(',', $value['prids']);

            $prrr                           = Properties::selectRaw('market_value,from_bond,gesamt_in_eur,real_estate_taxes,estate_agents,Grundbuch,evaluation,others,buffer')->where('standard_property_status', 1)->whereIn('main_property_id', $ar1)->get();
            $d48_array[$value['status']]    = 0;
            $market_array[$value['status']] = 0;
            foreach ($prrr as $key => $properties) {
                $D42 = $properties->gesamt_in_eur
                     + ($properties->real_estate_taxes * $properties->gesamt_in_eur)
                     + ($properties->estate_agents * $properties->gesamt_in_eur)
                     + (($properties->Grundbuch * $properties->gesamt_in_eur) / 100)
                     + ($properties->evaluation * $properties->gesamt_in_eur)
                     + ($properties->others * $properties->gesamt_in_eur)
                     + ($properties->buffer * $properties->gesamt_in_eur);
                $D46 = $D42;

                $D47 = $properties->with_real_ek * $D46; //C47*D46
                $D48 = $properties->from_bond * $D46;
                $d48_array[$value['status']] += $D48;
                $market_array[$value['status']] += $properties->market_value;

            }

            $bestandmarket = 0;
            if ($value['status'] == 6 || $value['status'] == 11) {
                $bestand_total += $value['preisverk'];
                if ($value['status'] == 6) {
                    $verkaufdata = DB::table('ads')->select(DB::raw('count(main_property_id) as id,gesamt_in_eur as gesamt_in_eur,preisverk as preisverk,main_property_id,market_value'))->join('properties', 'properties.main_property_id', '=', 'property_id')->where('standard_property_status', 1)->where('properties.status', $value['status'])->where('purpose', 'exclusive')->where('property_status', 0)->groupBy('properties.main_property_id')->get();
                    foreach ($verkaufdata as $value2) {
                        if (!in_array($value2->main_property_id, $cu_arr)) {
                            $cu_arr[] = $value2->main_property_id;
                            $bestandcustomcount += 1;
                            $bestandcustomamount += $value2->gesamt_in_eur;
                            $bestandcustomvk += $value2->preisverk;
                            $bestandmarket += $value2->market_value;
                        }

                    }

                    $market_array[600] = $bestandmarket;
                }

            }
            if ($value['status'] == 8) {
                $vc_total += $value['preisverk'];
            }
            $properties_count[$value['status']]  = $value['id'];
            $ek_analysis_array[$value['status']] = $value['gesamt_in_eur'];
            $total_count += $value['id'];

            if($value['status'] == config('properties.status.sold')){

                $status1 = $status2 = $gesamt_in_eur1 = $gesamt_in_eur2 = $market_value1 = $market_value2 = 0;

                $res = Properties::selectRaw('properties.main_property_id, IFNULL(properties.gesamt_in_eur, 0) as gesamt_in_eur, IFNULL(properties.preisverk, 0) as preisverk, IFNULL(properties.market_value, 0) as market_value ,psd.comment')
                        ->leftJoin('properties_sale_details as psd', function($join){
                                $join->on('psd.property_id', '=', 'properties.main_property_id')
                                ->where('psd.type', 101);
                        })
                        ->where('properties.standard_property_status', 1)
                        ->whereNotIn('properties.main_property_id', $not_shown_array)
                        ->where('properties.property_status', 0)
                        ->where('properties.status', config('properties.status.sold'))
                        ->get();

                        $market_array[$value['status']] += $properties->market_value;

                if($res){
                    foreach ($res as  $res1) {
                        if(date('Y', strtotime($res1->comment)) != 2020){
                            $status1++;
                            $gesamt_in_eur1 += $res1->gesamt_in_eur;
                            $vc_total1 += $res1->preisverk;
                            $market_value1 += $res1->market_value;
                        }else{
                            $status2++;
                            $gesamt_in_eur2 += $res1->gesamt_in_eur;
                            $vc_total2 += $res1->preisverk;
                            $market_value2 += $res1->market_value;
                        }
                    }
                }

                $properties_count[$value['status'].'-1']  = $status1;
                $properties_count[$value['status'].'-2']  = $status2;

                $ek_analysis_array[$value['status'].'-1'] = $gesamt_in_eur1;
                $ek_analysis_array[$value['status'].'-2'] = $gesamt_in_eur2;

                $market_array[$value['status'].'-1'] = $market_value1;
                $market_array[$value['status'].'-2'] = $market_value2;
            }
        }

        // pre($market_array);
        $banks = Banks::all();
        $logs  = Notifications::orderBy('id', 'desc')->take(3)->get();

        foreach ($logs as $log) {
            if ($log->created_user_id) {
                $log->created_user_name = (User::where('id', $log->created_user_id)->first()) ? User::where('id', $log->created_user_id)->first()->name : "";
            }
            if ($log->property_id) {
                $log->property_name = (Properties::where('id', $log->property_id)->first()) ? Properties::where('id', $log->property_id)->first()->name_of_property : "";
            }
            $log->time_ago = strtotime($log->created_at);
        }
        $properties_portfolio = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->where('property_status', 0)->paginate(500);

        // pre($d48_array);

        $propertyStatus = [];
        $propertyStatusRes = DB::table('properties')->selectRaw('COUNT(id) as total, status')->where('Ist', 0)->where('soll', 0)->groupBy('status')->get();
        if($propertyStatusRes){
            foreach ($propertyStatusRes as $p) {
                $propertyStatus[$p->status] = $p->total;
            }
        }

        return view('dashboard.index',
            compact('properties_portfolio', 'properties_declined', 'properties_offer', 'properties_duration', 'properties_in_purchase', 'properties_sold', 'properties_lost', 'banks', 'logs',
                'total_count', 'bestand_total', 'vc_total', 'vc_total1', 'vc_total2',
                'analysis_array', 'ek_analysis_array',
                'properties_count', 'bestandcustomcount', 'bestandcustomamount', 'bestandcustomvk', 'd48_array', 'market_array', 'propertyStatus'
            ));
    }

    public function external()
    {
        $properties_in_purchase = array();
        $properties_sold        = array();
        $properties_duration    = array();
        $properties_declined    = array();
        $properties_lost        = array();
        $properties_exclusivity = array();
        $properties_in_sale     = array();
        $properties_certified   = array();
        $properties_offer       = array();
        $properties_all         = array();
        $analysis_array         = array();

        $total_count       = 0;
        $user_id           = Auth::user()->id;
        $bestand_total     = 0;
        $vc_total          = 0;
        $analysis_array    = array();
        $ek_analysis_array = array();

        $strmat = '""';

        $url  = "https://verkauf.fcr-immobilien.de/getuploadlist";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $str = curl_exec($curl);
        curl_close($curl);
        if ($str) {
            $ids = json_decode($str, true);
        }

        $analysis = Properties::selectRaw('status,count(id) as id,sum(gesamt_in_eur) as gesamt_in_eur,sum(preisverk) as preisverk,group_concat(main_property_id) as prids')->where('standard_property_status', 1)->where('main_property_id', '!=', 0)->groupBy('status')->where('property_status', 1)->get()->toArray();

        $bestandcustomcount  = 0;
        $bestandcustomamount = 0;
        $bestandcustomvk     = 0;

        foreach ($analysis as $key => $value) {
            if ($value['status'] == 6 || $value['status'] == 11) {
                $bestand_total += $value['preisverk'];
                $verkaufdata = DB::table('ads')->select(DB::raw('count(main_property_id) as id,sum(gesamt_in_eur) as gesamt_in_eur,sum(preisverk) as preisverk,group_concat(main_property_id) as prids'))->join('properties', 'properties.main_property_id', '=', 'property_id')->whereIn('ads.id', $ids)->where('standard_property_status', 1)->where('properties.status', $value['status'])->where('purpose', 'exclusive')->where('property_status', 1)->get();
                foreach ($verkaufdata as $value2) {
                    $bestandcustomcount += $value2->id;
                    $bestandcustomamount += $value2->gesamt_in_eur;
                    $bestandcustomvk += $value2->preisverk;
                }
            }
            if ($value['status'] == 8) {
                $vc_total += $value['preisverk'];
            }
            $properties_count[$value['status']]  = $value['id'];
            $ek_analysis_array[$value['status']] = $value['gesamt_in_eur'];
            $total_count += $value['id'];
        }

        $banks = Banks::all();
        $logs  = Notifications::orderBy('id', 'desc')->take(3)->get();

        foreach ($logs as $log) {
            if ($log->created_user_id) {
                $log->created_user_name = (User::where('id', $log->created_user_id)->first()) ? User::where('id', $log->created_user_id)->first()->name : "";
            }
            if ($log->property_id) {
                $log->property_name = (Properties::where('id', $log->property_id)->first()) ? Properties::where('id', $log->property_id)->first()->name_of_property : "";
            }
            $log->time_ago = strtotime($log->created_at);
        }
        $properties_portfolio = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->where('property_status', 1)->paginate(500);

        return view('dashboard.external',
            compact('properties_portfolio', 'properties_declined', 'properties_offer', 'properties_duration', 'properties_in_purchase', 'properties_sold', 'properties_lost', 'banks', 'logs',
                'total_count', 'bestand_total', 'vc_total',
                'analysis_array', 'ek_analysis_array',
                'properties_count', 'bestandcustomcount', 'bestandcustomamount', 'bestandcustomvk'
            ));
    }

    public function trello()
    {
        return view('dashboard.trello');
    }

    public function index1()
    {
        $perPages = 500;

        $properties_in_purchase = array();
        $properties_sold        = array();
        $properties_duration    = array();
        $properties_declined    = array();
        $properties_lost        = array();
        $properties_exclusivity = array();
        $properties_in_sale     = array();
        $properties_certified   = array();
        $properties_offer       = array();
        $properties_all         = array();
        $analysis_array         = array();

        $ek_analysis_array = array();

        $total_count = 0;

        // $masterlistes = Masterliste::all();
        $user_id = Auth::user()->id;
        //if(Auth::user()->isAdmin()){

        $bestand_total = 0;
        $vc_total      = 0;

        if (!Auth::user()->isGuest()) {

            $strmat   = '""';
            $analysis = Properties::selectRaw('status,count(id) as id,sum(gesamt_in_eur
                                + (real_estate_taxes * gesamt_in_eur)
                                + (estate_agents * gesamt_in_eur)
                                + ((Grundbuch * gesamt_in_eur)/100)
                                + (evaluation * gesamt_in_eur)
                                + (others * gesamt_in_eur)
                                + (buffer * gesamt_in_eur)) as total_purchase_price,group_concat(id) as prids')->where('Ist', 0)->where('soll', 0)->whereRaw('bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat)->groupBy('status')->get();

            $mm = array();
            foreach ($analysis as $key => $value) {

                $arr = explode(',', $value->prids);

                $prs                 = Properties::whereIn('main_property_id', $arr)->whereRaw('soll=0 and Ist!=0')->get();
                $amount              = 0;
                $amount1             = 0;
                $count               = 0;
                $main_property_array = array();
                foreach ($prs as $key => $property) {
                    if (!in_array($property->main_property_id, $main_property_array)) {
                        $amount += $property->gesamt_in_eur
                             + ($property->real_estate_taxes * $property->gesamt_in_eur)
                             + ($property->estate_agents * $property->gesamt_in_eur)
                             + (($property->Grundbuch * $property->gesamt_in_eur) / 100)
                             + ($property->evaluation * $property->gesamt_in_eur)
                             + ($property->others * $property->gesamt_in_eur)
                             + ($property->buffer * $property->gesamt_in_eur);

                        $count++;

                        $main_property_array[] = $property->main_property_id;
                        if ($value->status == 6 || $value->status == 11) {
                            $bestand_total += $property->preisverk;
                        }

                        if ($value->status == 8) {
                            $mm[$property->id] = $property->preisverk;
                            $vc_total += $property->preisverk;
                        }

                        // $property->standard_property_status=1;
                        // $property->save();

                        $amount1 += $property->gesamt_in_eur;

                        $main_property_array[] = $property->main_property_id;
                    }

                }

                $ek_analysis_array[$value->status] = $amount1;

                $analysis_array[$value->status]   = $amount;
                $properties_count[$value->status] = $count;

                $total_count += $count;
            }

            // print_r($mm);

            // $properties_declined = Properties::where('status','=',config('properties.status.declined'))->where('Ist',0)->where('soll',0)->paginate($perPages);
            // $properties_offer = Properties::where('status','=',config('properties.status.offer'))->where('Ist',0)->where('soll',0)->paginate($perPages);
            // $properties_duration = Properties::where('status','=',config('properties.status.duration'))
            //     //->orwhereNotNull('masterliste_id')
            //     //->orWhere('masterliste_id','<>', 0)
            // ->where('Ist',0)->where('soll',0)
            //     ->paginate($perPages);

            // $properties_in_purchase = Properties::where('status','=',config('properties.status.in_purchase'))->where('Ist',0)->where('soll',0)->paginate($perPages);
            // $properties_sold = Properties::where('status','=',config('properties.status.sold'))->where('Ist',0)->where('soll',0)->paginate($perPages);
            // $properties_lost = Properties::where('status','=',config('properties.status.lost'))->where('Ist',0)->where('soll',0)->paginate($perPages);
            // $properties_exclusivity = Properties::where('status','=',config('properties.status.exclusivity'))->where('Ist',0)->where('soll',0)->paginate($perPages);
            // $properties_in_sale = Properties::where('status','=',config('properties.status.in_sale'))->where('Ist',0)->where('soll',0)->paginate($perPages);
            // $properties_certified = Properties::where('status','=',config('properties.status.certified'))->where('Ist',0)->where('soll',0)->paginate($perPages);

            // $properties_all = Properties::where('Ist',0)->where('soll',0)->paginate($perPages);
        } else {

            $properties_duration = Properties::where('status', '=', config('properties.status.duration'))
            //->orwhereNotNull('masterliste_id')
            //->orWhere('masterliste_id','<>', 0)
                ->where('Ist', 0)->where('soll', 0)
                ->paginate($perPages);

            $properties_declined = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->paginate($perPages);
            $properties_offer    = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->paginate($perPages);

            $properties_in_purchase = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->paginate($perPages);
            $properties_sold        = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->paginate($perPages);

            $properties_exclusivity = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->paginate($perPages);
            $properties_in_sale     = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->where('Ist', 0)->where('soll', 0)->paginate($perPages);
            $properties_certified   = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->paginate($perPages);

            $properties_lost = Properties::where('status', '=', 'abc')
                ->where('Ist', 0)->where('soll', 0)
                ->paginate($perPages);

            $properties_portfolio = Properties::where('status', '=', 'abc')->where('Ist', 0)->where('soll', 0)->paginate($perPages);

        }

        // }else{
        //     $properties_declined = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.declined')]])->paginate($perPages);
        //     $properties_offer = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.offer')]])->paginate($perPages);
        //     $properties_duration = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.duration')]])
        //         ->orwhereNotNull('masterliste_id')
        //         ->orWhere('masterliste_id','<>', 0)->paginate($perPages);
        //     $properties_in_purchase = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.in_purchase')]])->paginate($perPages);
        //     $properties_sold = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.sold')]])->paginate($perPages);
        //     $properties_lost = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.lost')]])->paginate($perPages);
        //     $properties_exclusivity = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.exclusivity')]])->paginate($perPages);
        //     $properties_in_sale = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.in_sale')]])->paginate($perPages);
        //     $properties_certified = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.certified')]])->paginate($perPages);
        // }
        $banks = Banks::all();
        $logs  = Notifications::orderBy('id', 'desc')->take(3)->get();

        foreach ($logs as $log) {
            if ($log->created_user_id) {
                $log->created_user_name = (User::where('id', $log->created_user_id)->first()) ? User::where('id', $log->created_user_id)->first()->name : "";
            }
            if ($log->property_id) {
                $log->property_name = (Properties::where('id', $log->property_id)->first()) ? Properties::where('id', $log->property_id)->first()->name_of_property : "";
            }
            $log->time_ago = strtotime($log->created_at);
        }

        // if(Auth::user()->isAdmin()){

        //          $_properties_declined = Properties::where('status','=',config('properties.status.declined'))->where('Ist',0)->where('soll',0)->get();
        //          $_properties_offer = Properties::where('status','=',config('properties.status.offer'))->where('Ist',0)->where('soll',0)->get();
        //          $_properties_duration = Properties::where('status','=',config('properties.status.duration'))->where('Ist',0)->where('soll',0)->get();
        //          $_properties_in_purchase = Properties::where('status','=',config('properties.status.in_purchase'))->where('Ist',0)->where('soll',0)->get();
        //          $_properties_sold = Properties::where('status','=',config('properties.status.sold'))->where('Ist',0)->where('soll',0)->get();
        //          $_properties_lost = Properties::where('status','=',config('properties.status.lost'))->where('Ist',0)->where('soll',0)->get();

        //      }else{
        //          $user_id =  Auth::id();
        //          $_properties_declined = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.declined')]])->where('Ist',0)->where('soll',0)->get();
        //          $_properties_offer = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.offer')]])->where('Ist',0)->where('soll',0)->get();
        //          $_properties_duration = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.duration')]])->where('Ist',0)->where('soll',0)->get();
        //          $_properties_in_purchase = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.in_purchase')]])->where('Ist',0)->where('soll',0)->get();
        //          $_properties_sold = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.sold')]])->where('Ist',0)->where('soll',0)->get();
        //          $_properties_lost = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.lost')]])->where('Ist',0)->where('soll',0)->get();
        //      }

        return view('dashboard.index',
            compact('properties_portfolio', 'properties_declined', 'properties_offer', 'properties_duration', 'properties_in_purchase', 'properties_sold', 'properties_lost', 'banks', 'logs',
                // '_properties_declined',
                // '_properties_offer',
                // '_properties_duration',
                // '_properties_in_purchase',
                // '_properties_sold',
                // '_properties_lost',
                // 'tenancy_items_2',
                'total_count', 'bestand_total', 'vc_total',
                'masterlistes', 'analysis_array', 'ek_analysis_array',
                'properties_count',
                'properties_exclusivity',
                'properties_in_sale',
                'properties_certified',
                'properties_all'
            ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getstatusloi($month)
    {
        $data = StatusLoi::selectRaw('status_loi.*,users.name,properties.name_of_property')->join('users', 'users.id', '=', 'status_loi.user_id')->
            join('properties', 'properties.id', '=', 'status_loi.property_id');
        if ($month != 0) {
            $data = $data->whereMonth('status_loi.date', $month);
        }
        $data = $data->orderby('status_loi.date', 'desc')->get()->groupby('user_id');

        // dd($data);

        $a = View::make('properties.statusloilist', array('data' => $data))->render();
        return $a;

    }

    public function getStatusLoiByUserId(Request $request)
    {
        $data = StatusLoi::selectRaw('status_loi.*,users.name')->with('property')->join('users', 'users.id', '=', 'status_loi.user_id')
            ->where('user_id', $request->id)->get();

        $a = View::make('properties.transaction_manager_status_loi', array('data' => $data))->render();
        return $a;
    }


    public function getPropertyDefaultPayers(Request $request)
    {
        $status = $request->status;
        $year = date('Y');
        $month = date('n');
        $d = '1=1';
        if($request->year){
            $year = $request->year;
            $d .= ' and year='.$year;
        }
        if($request->month){
            $month = $request->month;
            $d .= ' and month='.$month;
        }
        $m[] = __('dashboard.'.date('F',strtotime('2000-'.$month.'-01')));

        $user = Auth::user();
        $str = "";
        if($request->amcheck)
            $str = ' and p.asset_m_id='.$user->id;

        $data = DB::table('properties as p')
                    ->select('pdp.*','u.name','p.name_of_property','p.id')
                    ->join('users as u', 'u.id', '=', 'p.asset_m_id')
                    ->leftJoin('properties_default_payers as pdp', function($join) use($d){
                        $join->on('p.id', '=', 'pdp.property_id')->whereRaw($d);
                    })
                    ->where('status',6)
                    ->where('Ist',0)
                    ->where('soll',0)
                    ->whereRaw('main_property_id=0 '.$str)
                    ->orderBy('pdp.id', 'desc')
                    ->groupBy('p.id')
                    ->get();


        $first_last = 0;
        //Second last month
        $second_last_arr = explode("-", date('m-Y', strtotime('-2 month')) );
        $second_last_month = $second_last_arr[0];
        $second_last_year = $second_last_arr[1];

        $m[] = __('dashboard.'.date('F',strtotime('2000-'.$second_last_month.'-01')));

        $second_last_res = DB::table('properties as p')->select('month')->join('users as u', 'u.id', '=', 'p.asset_m_id')
                    ->leftJoin('properties_default_payers as pdp', function($join) use($second_last_year, $second_last_month){
                        $join->on('p.id', '=', 'pdp.property_id')
                        ->where('year', $second_last_year)
                        ->where('month', $second_last_month);
                    })->where('status',6)->where('Ist',0)->where('soll',0)->whereRaw('main_property_id=0 '.$str)->orderBy('pdp.id', 'desc')->groupBy('p.id')->get();

        $second_last = 0;
        foreach ($second_last_res as $element) {
            if(!$element->month)
                $second_last +=1;
        }

        //Second last month
        $third_last_arr = explode("-", date('m-Y', strtotime('-3 month')) );
        $third_last_month = $third_last_arr[0];
        $third_last_year = $third_last_arr[1];

        $m[] = __('dashboard.'.date('F',strtotime('2000-'.$third_last_month.'-01')));


        $third_last_res = DB::table('properties as p')->select('month')->join('users as u', 'u.id', '=', 'p.asset_m_id')
                    ->leftJoin('properties_default_payers as pdp', function($join) use($third_last_year, $third_last_month){
                        $join->on('p.id', '=', 'pdp.property_id')
                        ->where('year', $third_last_year)
                        ->where('month', $third_last_month);
                    })->where('status',6)->where('Ist',0)->where('soll',0)->whereRaw('main_property_id=0 '.$str)->orderBy('pdp.id', 'desc')->groupBy('p.id')->get();

        $third_last = 0;
        foreach ($third_last_res as $element) {
            if(!$element->month)
                $third_last +=1;
        }

        $a = View::make('dashboard.property_default_payers', array('data' => $data, 'first_last' => $first_last, 'second_last' => $second_last, 'third_last' => $third_last,'m'=>$m,'status'=>$status))->render();
        return $a;

    }

    public function getPropertyInsuranceTab(){
        $data = DB::table('property_insurance_tabs as pi')
                    ->select('pi.*','p.name_of_property', DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurance_release' AND record_id = pi.id AND tab='insurance_tab') THEN 1 ELSE 0 END as release_status"),DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurance_request' AND record_id = pi.id AND tab='insurance_tab') THEN 1 ELSE 0 END as request_status"))
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->where('pi.deleted', 0)
                    ->where('pi.not_release_status', 0)
                    ->havingRaw('release_status != 1 and request_status=1')
                    ->groupBy('pi.id')
                    ->get();

                    // pre($data);

        $html = View::make('dashboard.property_insurance_tabs', array('data' => $data))->render();
        return $html;
    }

    public function exportWaultBestand() {
        $filename = 'Export_Wault_Bestand_'.rand().'.xlsx';
        $path = public_path().'/export/'.$filename;

        Excel::store(new ExportWaultBestand(), $filename, 'customer_uploads');

        ExportHistoryService::create('Export Wault Bestand', $filename);

        return Excel::download(new ExportWaultBestand(), $filename);
    }


    public function getAssetPendingNotReleaseInvoice(Request $request)
    {
      $status = $request->status;
      $user = Auth::user();

        $condition = '(p.asset_m_id='.$user->id.' OR p.asset_m_id2='.$user->id.')';
        if( !in_array($user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$user->id.'")';
        }

      $res = DB::table('property_invoices as pi')
                    ->select('pi.*','am.name as assetmanager','u.name','p.name_of_property')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->leftJoin('users as am', 'am.id', '=', 'p.asset_m_id')
                    // ->where('pi.not_release_status', $status)
                    ->whereRaw($condition)
                    ->where('pi.deleted', 0);
                    if($status == 1){
                        $res = $res->where('falk_status', 4);
                    }elseif ($status == 2) {
                        $res = $res->where('falk_status', 3);
                    }
                    $res = $res->get();

        if($res){
            foreach ($res as $key => $value) {
                $value->button = app('App\Http\Controllers\PropertiesController')->getInvoiceButton($value, $user);
            }
        }

        $html = View::make('dashboard.pending_notrelease_invoice', array('res' => $res,'status'=>$status))->render();
        return $html;
    }

    public function getSelectionProperties(Request $request){

        $export = ($request->export) ? $request->export : 0;
        $ids = ($request->ids) ? explode(",", $request->ids) : [];

        if($export != 1){

            $data = DB::table('properties')
                    ->selectRaw('id, name_of_property, status')
                    ->where('Ist', 0)
                    ->where('soll', 0)
                    ->where('status',$request->status)
                    ->where('status', '!=', config('properties.status.quicksheet'))
                    ->get();

            $html = View::make('dashboard.selection_property', array('data' => $data,))->render();
            return $html;

        }else{

            $tenancy_schedule_arr = [];
            $tenancy_schedules_item = DB::table('tenancy_schedule_items as tsi')
                                ->selectRaw('ts.property_id, tsi.type, IFNULL(tsi.nk_netto, 0) as nk_netto, IFNULL(tsi.actual_net_rent, 0) as actual_net_rent, tsi.rent_begin, tsi.rent_end')
                                ->join('tenancy_schedules as ts', 'ts.id', '=', 'tsi.tenancy_schedule_id')
                                ->whereIn('tsi.type',array(config('tenancy_schedule.item_type.live'),config('tenancy_schedule.item_type.business')))
                                ->get();
            if($tenancy_schedules_item){
                foreach ($tenancy_schedules_item as $item) {

                    if(($item->rent_begin != '' && $item->rent_begin > date('Y-m-d')) || ($item->rent_end != '' && $item->rent_end < date('Y-m-d')))
                        continue;

                    if(isset($tenancy_schedule_arr[$item->property_id])){
                        $tenancy_schedule_arr[$item->property_id]['total_actual_net_rent'] += $item->actual_net_rent;
                    }else{
                        $tenancy_schedule_arr[$item->property_id]['total_actual_net_rent'] = $item->actual_net_rent;
                    }
                }
            }

            $data = DB::table('properties')
                    ->selectRaw('id, name_of_property, market_value as verkehrswert, plz_ort, ort, niedersachsen, strasse, hausnummer, net_rent_pa, gesamt_in_eur, property_type, status')
                    ->where('Ist', 0)
                    ->where('soll', 0)
                    ->whereIn('id', $ids)
                    ->get();
            if($data){
                foreach ($data as $key => $value) {
                   $data[$key]->total_actual_net_rent = (isset($tenancy_schedule_arr[$value->id])) ? ($tenancy_schedule_arr[$value->id]['total_actual_net_rent'] * 12) : 0; 
                }
            }
            
            
            $pdf = PDF::loadView('pdf.single_selection', ['data' => $data])->setPaper('a4', 'landscape');

            $filename = 'objekt_'.rand().'.pdf';
            $path = public_path().'/export/'.$filename;
            $pdf->save($path);

            ExportHistoryService::create('Export Einzelauswahl', $filename);

            return $pdf->stream($filename);
        }
    }


    public function getPropertyInvoiceRequest(){

        $user = Auth::user();
        
        $condition = '1=1';
        if($user->email != config('users.falk_email')){
            $condition = '(p.asset_m_id='.$user->id.' OR p.asset_m_id2='.$user->id.')';
        }

        $data = DB::table('property_invoices as pi')
            ->select('pi.is_paid','pi.need_to_pay','pi.id','pi.amount', 'pi.invoice','pi.property_id','am.name as assetmanager', 'u.name', 'p.name_of_property', 'pi.file_basename', 'pi.file_type', 'pi.created_at','pi.date', 'pi.comment', 'pi.user_id', 'pi.towards_tenant', 'pi.foldable', 'pi.am_status', 'pi.hv_status', 'pi.user_status', 'pi.falk_status', 'pi.email', DB::raw("(SELECT req2.created_at FROM properties_mail_logs req2 WHERE req2.type = 'request2' AND req2.record_id = pi.id ORDER BY req2.id DESC LIMIT 1) as request2_date"),DB::raw("(SELECT CONCAT(pcu.name, ': ', pc.comment) from properties_comments as pc JOIN users as pcu ON pcu.id = pc.user_id WHERE pc.record_id = pi.id AND pc.property_id = pi.property_id AND pc.type = 'property_invoices' ORDER BY pc.created_at DESC LIMIT 1) as latest_comment"))
            ->join('properties as p', 'p.id', '=', 'pi.property_id')
            ->join('users as u', 'u.id', '=', 'pi.user_id')
            ->leftJoin('users as am', 'am.id', '=', 'p.asset_m_id')
            // ->where('pi.not_release_status', 0)
            ->where('pi.deleted', 0)
            ->where('pi.email', $user->id)
            ->where('pi.user_status', 1)
            ->whereRaw($condition)
            // ->havingRaw('is_release2 != 1 and is_request2=1')
            ->groupBy('pi.id')
            ->get();

        if($data){
            foreach ($data as $key => $value) {
                $value->button = app('App\Http\Controllers\PropertiesController')->getInvoiceButton($value, $user);
            }
        }
        
        $html = View::make('dashboard.property_invoice_am', array('data' => $data))->render();
        return $html;

    }

}
