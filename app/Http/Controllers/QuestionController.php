<?php

namespace App\Http\Controllers;

use App\Models\Properties;
use App\Models\Question;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if (!Auth::user()->isAdmin()){
            return redirect("/");
        }

    	//$perPages = 25;
    	$questions = Question::orderBy('id','ASC')->get();
		$properties=[];
		foreach($questions as $question)
		{
			
			$property = Properties::FindOrFail($question->property_id);
			$properties[$question->id]=$property->name_of_property;
		}
		
		
        return view('question.index',compact('questions','properties')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->isAdmin()){
            return redirect("/");
        }
		
		$properties = Properties::all();
		//echo "<pre>";print_r($properties);
		return view('question.create',compact('properties'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->isAdmin()){
            return redirect("/");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->isAdmin()){
            return redirect("/");
        }
    	$question = Question::FindOrFail($id);
		$properties = Properties::all();
		return view('question.edit',compact('question','properties'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();
		
		//echo "<pre>";print_r($input);die;
		
		
		if(isset($input['id']))
		{
			$status = Question::where(['id'=>$input['id']])->update(['questions'=>json_encode($input['questions']),'qn_heading'=>$input['qn_heading'],'property_id'=>$input['property']]);
		}
		else
		{
			echo json_encode($input['questions']);die;
			$status = Question::insert(['questions'=>json_encode($input['questions']),'qn_heading'=>$input['qn_heading'],'property_id'=>$input['property']]);
			echo'<pre>';print_r($status);die;
		}
		

		if($status){
			$msg=isset($input['id'])?'updated':'created';
				return redirect()->action('QuestionController@index')
					->with('message', 'Question '.$msg);
		}
		else
		{
			return redirect()->action('QuestionController@index')
					->with('error', 'your request could not be completed because of an error');
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$user = User::where('id','=',$id)->first();
		if(isset($user)){
			if (UserService::delete($user)) {
				return back()->with('message', trans('user.user_controller.deleted_success'));
			}
		}
		return back()->with('error', trans('user.user_controller.deleted_fail'));
    }

    public function getProfile($id){
        if ($id != Auth::user()->id)
            return redirect('/');

    	$user = User::where('id','=',$id)->first();
		$url_image_profile = "/user-images/".$user->image;
		return view('user.edit-profile',compact('id','user','url_image_profile'));
	}

	public function postProfile(Request $request, $id)
	{
		$input = $request->all();
		$user = User::where('id','=',$id)->first();
		$email = $input['email'];
		$find_email = User::where('email','=',$email)
			->where('id','<>',$id)->first();

		//check email before edit
		if(isset($find_email)){
			if($find_email->count()>0){
				return redirect()->action('UserController@getProfile',$id)
					->with('error', trans('user.user_controller.email_already'));
			}
		}
		
		if(isset(request()->image)){
			if($user->image !=''){//remove only has a picture before
				$file_path_old = public_path('user-images').'/'.$user->image;
				if (file_exists($file_path_old)){
                    unlink($file_path_old);
                }
			}
			//save image
			$imageName = $id.".".request()->image->getClientOriginalExtension();
			request()->image->move(public_path('user-images'), $imageName);
			$input['image'] = $imageName;
		}
		else
		{
			//when user no update profile picture
			$input['image'] = $user->image;
		}

		$date=date_create($input['birthday']);
		$input['birthday'] = date_format($date,"Y/m/d");
		if (UserService::updateProfile($input, $user)) {

			return redirect()->route('edit-profile', ["id"=>$id])
				->with('message', trans('user.user_controller.updated_success'));
		}

		return redirect()->route('edit-profile', ["id"=>$id])
			->with('error', trans('user.user_controller.updated_fail'));

	}

	public function getChangePassword(){
        return view('user.change-password');
    }

    public function postChangePassword(Request $request){
        $user = User::findOrFail(Auth::user()->id);
        $input = $request->all();
        if (password_verify($input['current_password'], $user->password)){
            if ($input['new_password1'] === $input['new_password2']){
                $user->update([
                    'password' => Hash::make($input['new_password1'])
                ]);
                return redirect()->route('change-password')
                    ->with('message', trans('user.user_controller.password_changed_success'));
            }
            else {
                return redirect()->route('change-password')
                    ->with('error', trans('user.user_controller.not_matched'));
            }
        }
        else {
            return redirect()->route('change-password')
                ->with('error', trans('user.user_controller.not_true'));
        }

    }

    public function showUserActivities(){
        $current_date = date("Y-m-d");
        if (!isset($_GET['type'])){
            $_GET['type'] = 'day';
        }
        $perPages = 25;
        $users = User::orderBy('id','ASC')->paginate($perPages);
        foreach ($users as $user){
            if ($_GET['type'] == 'day'){
                $filter = date('d', strtotime($current_date));
                $user->number_of_properties = count(Properties::where('user_id', $user->id)
                    ->whereDay('duration_from', $filter)
                    ->where('Ist',0)->where('soll',0)
                    ->get());
            }
            else if($_GET['type'] == 'week'){
                $user->number_of_properties = count(Properties::where('user_id', $user->id)->where('Ist',0)->where('soll',0)
                    ->whereBetween('duration_from', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
                    ->get());
            }
            else { //type == month
                $filter = date('m', strtotime($current_date));
                $user->number_of_properties = count(Properties::where('user_id', $user->id)->where('Ist',0)->where('soll',0)
                    ->whereMonth('duration_from', $filter)
                    ->get());
            }

        }

        foreach ($users as $user) {
            $user_id = $user->id;
            $user->_properties_declined = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.declined')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_offer = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.offer')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_duration = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.duration')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_in_purchase = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.in_purchase')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_sold = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.sold')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_lost = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.lost')]])->where('Ist',0)->where('soll',0)->get();
        }

//        duration_from
        return view('user.user-activities', compact('users'));
    }


    function api_contacts(){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/contact?oauth_consumer_key=08211746Key&oauth_token=4822392b-7eeb-42a2-9a40-36b478698a0a&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1553782181&oauth_nonce=TP9C4k&oauth_version=1.0&oauth_signature=lisiKX1ETzuP147owHUEq%2FHpK7o%3D",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/xml"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return redirect()->back()->with(['error' => 'ups something went wrong!']);
        } else {


            echo '<pre>';
            $xml = new \SimpleXMLElement($response);
            $response = json_encode($xml, JSON_PRETTY_PRINT);
            print_r(json_decode($response, TRUE));
            echo '</pre>';

        }



    }



    public function working(){


        $properties = DB::table('properties')->select('id', 'soll', 'Ist', 'created_at', 'bank_ids')
            ->where('maintenance_increase_year2', '=', 0)
            ->where('Ist', '!=', 0)
            ->get();

        echo '<pre>';
        echo count($properties);
        echo '</br>';
        print_r($properties);
        echo '</pre>';

        echo '<br/>==================================================================';

        $properties = DB::table('properties')->select('id', 'soll', 'Ist', 'created_at', 'bank_ids')
            ->where('maintenance_increase_year2', '=', 0)
            ->where('Ist', '=', 0)
            ->where('soll', '=', 0)

            ->get();

        echo '<pre>';
        echo count($properties);
        echo '</br>';
        print_r($properties);
        echo '</pre>';

    }

    /*
     *
     *
     * */
    function delete_user(){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/contact/95100526?oauth_consumer_key=08211746Key&oauth_token=4822392b-7eeb-42a2-9a40-36b478698a0a&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1553787354&oauth_nonce=xzmkpi&oauth_version=1.0&oauth_signature=dBbQMwhZgBFRmSi8TYwADHxyags%3D",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/xml",
                "postman-token: 44e0ade9-814f-a17a-7798-9fdd953b8ce9"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return redirect()->back()->with(['error' => 'ups something went wrong!']);
        } else {


            echo '<pre>';
            $xml = new \SimpleXMLElement($response);
            $response = json_encode($xml, JSON_PRETTY_PRINT);
            print_r(json_decode($response, TRUE));
            echo '</pre>';

        }


    }














}
