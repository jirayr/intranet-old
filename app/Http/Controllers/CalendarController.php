<?php

namespace App\Http\Controllers;
use App\Models\Events;
use Illuminate\Http\Request;
use Auth;
use App\Models\Properties;
use App\Services\PropertiesService;
use App\Services\EventsService;
use Symfony\Component\HttpKernel\Profiler\Profile;
class CalendarController  extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPages = 1000;
        $user_id =  Auth::id();
        $events=Events::where('user_id','=',$user_id)->paginate($perPages);

        //Add 1 day to events.end to fix bug in PE-32
        foreach ($events as $event){
            $event->end = date('Y-m-d',strtotime($event->end . ' +1 day'));
        }
        return view('calendar.index',compact('events'));
    }
    public function create_calendar(Request $request){
        $input = $request->all();
        if($input['start']>$input['end']&&isset($input['end'])){
            return "End date should be after or equal the Start date";
        }
        else{
            $input['name']= $input['title'];
        EventsService::create( $input);
        return redirect('/calendar');
        }
    }
    public function edit_event(Request $request){
        $input = $request->all();
        $event=Events::where('id','=',$input['id']);
        if(isset($input['delete'])){
        EventsService::delete($event);
        }
        else{
            $input['name']= $input['title'];
            EventsService::update( $input,$event);
        }  
        return redirect('/calendar');
    }
}
?>