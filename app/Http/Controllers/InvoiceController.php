<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Properties;
use App\Models\PropertyInvoices;
use App\Models\PropertiesMailLog;
use Auth;
use Validator;
use DB;
use View;

class InvoiceController extends Controller
{

    public function invoiceReleaseAM(Request $request){
    	if($request->ajax()) {

            $rules = array('id' => 'required', 'status' => 'required');
            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

            	$user = Auth::user();
                
                $modal = PropertyInvoices::find($request->id);
                $modal->am_status = $request->status;
                $modal->last_process_type = 'am_status';

                if($modal->save()){

                	$log = [1 => 'request_invoice_am', 2 => 'release_invoice_am', 3 => 'pending_invoice_am', 4 => 'not_release_invoice_am'];

                	// Add mail log
		            $submodal = new PropertiesMailLog;
		            $submodal->property_id = $modal->property_id;
		            $submodal->record_id = $request->id;
		            $submodal->type = $log[$request->status];
		            $submodal->tab = 'invoice';
		            if($request->comment)
		                  $submodal->comment = $request->comment;
		            $submodal->user_id = $user->id;
		            $submodal->save();

                    $this->invoiceReleaseAMSendMail($request->id, $request->status, $request->comment);

                    $result = ['status' => true, 'message' => 'Status update success.'];
                }else{
                    $result = ['status' => false, 'message' => 'Status update fail!'];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!'];
        }
        return response()->json($result);
    }

    public function invoiceReleaseAMSendMail($invoice_id, $status, $comment = ''){

        $user = Auth::user();
        $email_not_send_array = $this->mail_not_send();
        $email_not_send_array[] = $user->email;

        $invoice = DB::table('property_invoices as pi')
                    ->select('pi.*', 'u.name as user_name', 'u.email as user_email', 'u.role as user_role', 'am1.name as am1_name', 'am1.email as am1_email', 'am2.name as am2_name', 'am2.email as am2_email', 'hvbu.name as hvbu_name', 'hvbu.email as hvbu_email', 'hvpm.name as hvpm_name', 'hvpm.email as hvpm_email', 'p.plz_ort', 'p.ort', 'p.name_of_property')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->leftjoin('users as am1', 'am1.id', '=', 'p.asset_m_id')
                    ->leftjoin('users as am2', 'am2.id', '=', 'p.asset_m_id2')
                    ->leftjoin('users as hvbu', 'hvbu.id', '=', 'p.hvbu_id')
                    ->leftjoin('users as hvpm', 'hvpm.id', '=', 'p.hvpm_id')
                    ->where('pi.id', $invoice_id)
                    ->first();
        if($invoice){

            $file_name = ($invoice->invoice) ? '('.$invoice->invoice.')' : '';

            $url = route('properties.show',['property'=> $invoice->property_id]).'?tab=property_invoice';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $extern_url = route('extern_home').'?property_id='.base64_encode($invoice->property_id);
            $extern_url = '<a href="'.$extern_url.'">'.$extern_url.'</a>';

            if($invoice->user_role == 6){
                $iu_url = $extern_url;
            }else{
                $iu_url = $url;
            }

            $glink = "https://drive.google.com/drive/u/2/folders/".$invoice->file_basename;
            if($invoice->file_type == "file"){
                $glink = "https://drive.google.com/file/d/".$invoice->file_basename;
            }
            $glink = '<a href="'.$glink.'">'.$glink.'</a>';

            $reply_email = $user->email;
            $reply_name = $user->name;

            if($status == 1){ //request status

                $email_not_send_array[] = config('users.falk_email');

                // send mail to AM 1
                if($invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array)){

                    $email_not_send_array[] = $invoice->am1_email;

                    $subject = "Rechnungsfreigabe  ".$invoice->plz_ort.' '.$invoice->ort;

                    $text = "Hallo ".$invoice->am1_name.",<br>".$reply_name." benötigt deine Freigabe bei dem Objekt ".$invoice->plz_ort.' '.$invoice->ort." für die folgende Rechnung ".$file_name.": Hier kannst du die Rechnung freigeben: ".$url;
                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->am1_email;
                    $name  = $invoice->am1_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }

                // send mail to AM 2
                if($invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->am2_email;

                    $subject = "Rechnungsfreigabe  ".$invoice->plz_ort.' '.$invoice->ort;

                    $text = "Hallo ".$invoice->am2_name.",<br>".$reply_name." benötigt deine Freigabe bei dem Objekt ".$invoice->plz_ort.' '.$invoice->ort." für die folgende Rechnung ".$file_name.": Hier kannst du die Rechnung freigeben: ".$url;
                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->am2_email;
                    $name  = $invoice->am2_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }

            }elseif ($status == 2) { //release status

                $email_not_send_array[] = config('users.falk_email');

                $cc_array = [];
                if( $invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am1_email;
                }
                if( $invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am2_email;
                }
                if( $invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvbu_email;
                }
                if( $invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvpm_email;
                }

                if(!empty($cc_array)){
                    $cc_array = array_unique($cc_array);
                }

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at).' für '.$invoice->plz_ort.' '.$invoice->ort." wurde von $reply_name freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." wurde von $reply_name freigegeben. Link zum Intranet intern: ".$url;
                    $text .= "<br>Link zum Intranet extern: ".$extern_url;

                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name, $cc_array) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->cc($cc_array)
                        ->subject($subject);
                    });
                }
                
            }elseif ($status == 3) { //pending status

                $email_not_send_array[] = config('users.falk_email');

                $cc_array = [];
                if( $invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am1_email;
                }
                if( $invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am2_email;
                }
                if( $invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvbu_email;
                }
                if( $invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvpm_email;
                }

                if(!empty($cc_array)){
                    $cc_array = array_unique($cc_array);
                }

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde noch nicht freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." ".$glink." wurde von ".$user->name." noch nicht freigegeben. Link zum Intranet: ".$iu_url;
                    if($comment){
                        $text .= "<br><br>Rechnung: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name, $cc_array) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->cc($cc_array)
                        ->subject($subject);
                    });
                }
                
            }elseif ($status == 4) { //not release status
                $email_not_send_array[] = config('users.falk_email');

                $cc_array = [];
                if( $invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am1_email;
                }
                if( $invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am2_email;
                }
                if( $invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvbu_email;
                }
                if( $invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvpm_email;
                }

                if(!empty($cc_array)){
                    $cc_array = array_unique($cc_array);
                }

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde nicht freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." ".$glink." wurde von ".$user->name." nicht freigegeben. Link zum Intranet: ".$iu_url;
                    if($comment){
                          $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name, $cc_array) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->cc($cc_array)
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }
            }

        }
        return true;
    }

    public function invoiceReleaseHV(Request $request){
    	if($request->ajax()) {

            $rules = array('id' => 'required', 'status' => 'required');
            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

            	$user = Auth::user();
                
                $modal = PropertyInvoices::find($request->id);
                $modal->hv_status = $request->status;
                $modal->last_process_type = 'hv_status';

                if($modal->save()){

                	$log = [1 => 'request_invoice_hv', 2 => 'release_invoice_hv', 3 => 'pending_invoice_hv', 4 => 'not_release_invoice_hv'];

                	// Add mail log
		            $submodal = new PropertiesMailLog;
		            $submodal->property_id = $modal->property_id;
		            $submodal->record_id = $request->id;
		            $submodal->type = $log[$request->status];
		            $submodal->tab = 'invoice';
		            if($request->comment)
		                  $submodal->comment = $request->comment;
		            $submodal->user_id = $user->id;
		            $submodal->save();

                    $this->invoiceReleaseHVSendMail($request->id, $request->status, $request->comment);

                    $result = ['status' => true, 'message' => 'Status update success.'];
                }else{
                    $result = ['status' => false, 'message' => 'Status update fail!'];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!'];
        }
        return response()->json($result);
    }

    public function invoiceReleaseHVSendMail($invoice_id, $status, $comment = ''){
        $user = Auth::user();
        $email_not_send_array = $this->mail_not_send();
        $email_not_send_array[] = $user->email;


        $invoice = DB::table('property_invoices as pi')
                    ->select('pi.*', 'u.name as user_name', 'u.email as user_email', 'u.role as user_role', 'hvbu.name as hvbu_name', 'hvbu.email as hvbu_email', 'hvpm.name as hvpm_name', 'hvpm.email as hvpm_email', 'am1.name as am1_name', 'am1.email as am1_email', 'am2.name as am2_name', 'am2.email as am2_email', 'p.plz_ort', 'p.ort', 'p.name_of_property')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->leftjoin('users as hvbu', 'hvbu.id', '=', 'p.hvbu_id')
                    ->leftjoin('users as hvpm', 'hvpm.id', '=', 'p.hvpm_id')
                    ->leftjoin('users as am1', 'am1.id', '=', 'p.asset_m_id')
                    ->leftjoin('users as am2', 'am2.id', '=', 'p.asset_m_id2')
                    ->where('pi.id', $invoice_id)
                    ->first();
        if($invoice){

            $file_name = ($invoice->invoice) ? '('.$invoice->invoice.')' : '';

            $url = route('properties.show',['property'=> $invoice->property_id]).'?tab=property_invoice';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $extern_url = route('extern_home').'?property_id='.base64_encode($invoice->property_id);
            $extern_url = '<a href="'.$extern_url.'">'.$extern_url.'</a>';

            if($invoice->user_role == 6){
                $iu_url = $extern_url;
            }else{
                $iu_url = $url;
            }

            $glink = "https://drive.google.com/drive/u/2/folders/".$invoice->file_basename;
            if($invoice->file_type == "file"){
                $glink = "https://drive.google.com/file/d/".$invoice->file_basename;
            }
            $glink = '<a href="'.$glink.'">'.$glink.'</a>';

            $reply_email = $user->email;
            $reply_name = $user->name;

            if($status == 1){ //request status

                $email_not_send_array[] = config('users.falk_email');

                // send mail to HVBU
                if($invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array)){

                    $email_not_send_array[] = $invoice->hvbu_email;

                    $subject = "Rechnungsfreigabe  ".$invoice->plz_ort.' '.$invoice->ort;

                    $text = "Hallo ".$invoice->hvbu_name.",<br>".$reply_name." benötigt deine Freigabe bei dem Objekt ".$invoice->plz_ort.' '.$invoice->ort." für die folgende Rechnung ".$file_name.": Hier kannst du die Rechnung freigeben: ".$url;
                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->hvbu_email;
                    $name  = $invoice->hvbu_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }

                // send mail to HVPM
                if($invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->hvpm_email;

                    $subject = "Rechnungsfreigabe  ".$invoice->plz_ort.' '.$invoice->ort;

                    $text = "Hallo ".$invoice->hvpm_name.",<br>".$reply_name." benötigt deine Freigabe bei dem Objekt ".$invoice->plz_ort.' '.$invoice->ort." für die folgende Rechnung ".$file_name.": Hier kannst du die Rechnung freigeben: ".$url;
                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->hvpm_email;
                    $name  = $invoice->hvpm_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }

            }elseif ($status == 2) { //release status

                $email_not_send_array[] = config('users.falk_email');

                $cc_array = [];
                if( $invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am1_email;
                }
                if( $invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am2_email;
                }
                if( $invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvbu_email;
                }
                if( $invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvpm_email;
                }

                if(!empty($cc_array)){
                    $cc_array = array_unique($cc_array);
                }
                
                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at).' für '.$invoice->plz_ort.' '.$invoice->ort." wurde von $reply_name freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." wurde von $reply_name freigegeben. Link zum Intranet intern: ".$url;
                    $text .= "<br>Link zum Intranet extern: ".$extern_url;

                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name, $cc_array) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->cc($cc_array)
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }
                
            }elseif ($status == 3) { //pending status

                $email_not_send_array[] = config('users.falk_email');

                $cc_array = [];
                if( $invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am1_email;
                }
                if( $invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am2_email;
                }
                if( $invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvbu_email;
                }
                if( $invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvpm_email;
                }

                if(!empty($cc_array)){
                    $cc_array = array_unique($cc_array);
                }
                
                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde noch nicht freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." ".$glink." wurde von ".$user->name." noch nicht freigegeben. Link zum Intranet: ".$iu_url;
                    if($comment){
                        $text .= "<br><br>Rechnung: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name, $cc_array) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->cc($cc_array)
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }
                
            }elseif ($status == 4) { //not release status
                $email_not_send_array[] = config('users.falk_email');

                $cc_array = [];
                if( $invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am1_email;
                }
                if( $invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am2_email;
                }
                if( $invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvbu_email;
                }
                if( $invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvpm_email;
                }

                if(!empty($cc_array)){
                    $cc_array = array_unique($cc_array);
                }

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde nicht freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." ".$glink." wurde von ".$user->name." nicht freigegeben. Link zum Intranet: ".$iu_url;
                    if($comment){
                          $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name, $cc_array) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->cc($cc_array)
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }
            }

        }
        return true;
    }

    public function invoiceReleaseUser(Request $request){
    	if($request->ajax()) {

            $rules = array('id' => 'required', 'status' => 'required');

            if($request->status == 1){
                $rules = array('id' => 'required', 'status' => 'required', 'user_id' => 'required');
            }

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

            	$user = Auth::user();
                
                $modal = PropertyInvoices::find($request->id);
                $modal->user_status = $request->status;

                if($request->status == 1)
                    $modal->email = $request->user_id;

                $modal->last_process_type = 'user_status';

                if($modal->save()){

                	$log = [1 => 'request1', 2 => 'release1', 3 => 'pending_invoice_user', 4 => 'not_release_invoice_user'];

                	// Add mail log
		            $submodal = new PropertiesMailLog;
		            $submodal->property_id = $modal->property_id;
		            $submodal->record_id = $request->id;
		            $submodal->type = $log[$request->status];
		            $submodal->tab = 'invoice';
		            if($request->comment)
		                  $submodal->comment = $request->comment;
		            $submodal->user_id = $user->id;
		            $submodal->save();

                    $this->invoiceReleaseUserSendMail($request->id, $request->status, $request->comment);

                    $result = ['status' => true, 'message' => 'Status update success.'];
                }else{
                    $result = ['status' => false, 'message' => 'Status update fail!'];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!'];
        }
        return response()->json($result);
    }

    public function invoiceReleaseUserSendMail($invoice_id, $status, $comment = ''){
        $user = Auth::user();
        $email_not_send_array = $this->mail_not_send();
        $email_not_send_array[] = $user->email;


        $invoice = DB::table('property_invoices as pi')
                    ->select('pi.*', 'u.name as user_name', 'u.email as user_email', 'u.role as user_role', 'u1.name as selected_user_name', 'u1.email as selected_user_email', 'p.plz_ort', 'p.ort', 'p.name_of_property')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->join('users as u1', 'u1.id', '=', 'pi.email')
                    ->where('pi.id', $invoice_id)
                    ->first();
        if($invoice){

            $file_name = ($invoice->invoice) ? '('.$invoice->invoice.')' : '';

            $url = route('properties.show',['property'=> $invoice->property_id]).'?tab=property_invoice';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $extern_url = route('extern_home').'?property_id='.base64_encode($invoice->property_id);
            $extern_url = '<a href="'.$extern_url.'">'.$extern_url.'</a>';

            if($invoice->user_role == 6){
                $iu_url = $extern_url;
            }else{
                $iu_url = $url;
            }

            $glink = "https://drive.google.com/drive/u/2/folders/".$invoice->file_basename;
            if($invoice->file_type == "file"){
                $glink = "https://drive.google.com/file/d/".$invoice->file_basename;
            }
            $glink = '<a href="'.$glink.'">'.$glink.'</a>';

            $reply_email = $user->email;
            $reply_name = $user->name;

            if($status == 1){ //request status

                $email_not_send_array[] = config('users.falk_email');

                // send mail to selected user
                if($invoice->selected_user_email && !in_array($invoice->selected_user_email, $email_not_send_array)){

                    $email_not_send_array[] = $invoice->selected_user_email;

                    $subject = "Rechnungsfreigabe  ".$invoice->plz_ort.' '.$invoice->ort;

                    $text = "Hallo ".$invoice->selected_user_name.",<br>".$reply_name." benötigt deine Freigabe bei dem Objekt ".$invoice->plz_ort.' '.$invoice->ort." für die folgende Rechnung ".$file_name.": Hier kannst du die Rechnung freigeben: ".$url;
                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->selected_user_email;
                    $name  = $invoice->selected_user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }

            }elseif ($status == 2) { //release status

                $email_not_send_array[] = config('users.falk_email');

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at).' für '.$invoice->plz_ort.' '.$invoice->ort." wurde von $reply_name freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." wurde von $reply_name freigegeben. Link zum Intranet intern: ".$url;
                    $text .= "<br>Link zum Intranet extern: ".$extern_url;

                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }
                
            }elseif ($status == 3) { //pending status

                $email_not_send_array[] = config('users.falk_email');

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde noch nicht freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." ".$glink." wurde von ".$user->name." noch nicht freigegeben. Link zum Intranet: ".$iu_url;
                    if($comment){
                        $text .= "<br><br>Rechnung: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }
                
            }elseif ($status == 4) { //not release status

                $email_not_send_array[] = config('users.falk_email');

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde nicht freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." ".$glink." wurde von ".$user->name." nicht freigegeben. Link zum Intranet: ".$iu_url;
                    if($comment){
                          $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }
            }

        }
        return true;
    }

    public function invoiceReleaseFalk(Request $request){
    	if($request->ajax()) {

            $rules = array('id' => 'required', 'status' => 'required');
            $validator = Validator::make($request->all(), $rules);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

                $user = Auth::user();
                $ids = explode(",", $request->id);
                $log = [1 => 'request2', 2 => 'release2', 3 => 'pending_invoice', 4 => 'notrelease_invoice'];

                if(!empty($ids)){
                    foreach ($ids as $id) {

                        $modal = PropertyInvoices::find($id);
                        $modal->falk_status = $request->status;
                        $modal->last_process_type = 'falk_status';
                        $modal->not_release_status = 0;

                        if($modal->save()){

                            // Add mail log
                            $submodal = new PropertiesMailLog;
                            $submodal->property_id = $modal->property_id;
                            $submodal->record_id = $id;
                            $submodal->type = $log[$request->status];
                            $submodal->tab = 'invoice';
                            if($request->comment)
                                  $submodal->comment = $request->comment;
                            $submodal->user_id = $user->id;
                            $submodal->save();

                            if($request->status != 1){
                                $this->invoiceReleaseFalkSendMail($id, $request->status, $request->comment);
                            }

                        }
                    }

                    $result = ['status' => true, 'message' => 'Status update success.'];
                    
                }else{
                    $result = ['status' => false, 'message' => 'Invoice id not found!'];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!'];
        }
        return response()->json($result);
    }

    public function invoiceReleaseFalkSendMail($invoice_id, $status, $comment = ''){
        $user = Auth::user();
        $email_not_send_array = $this->mail_not_send();
        $email_not_send_array[] = $user->email;

        $invoice = DB::table('property_invoices as pi')
                    ->select('pi.*', 'u.name as user_name', 'u.email as user_email', 'u.role as user_role', 'am1.name as am1_name', 'am1.email as am1_email', 'am2.name as am2_name', 'am2.email as am2_email', 'hvbu.name as hvbu_name', 'hvbu.email as hvbu_email', 'hvpm.name as hvpm_name', 'hvpm.email as hvpm_email', 'p.plz_ort', 'p.ort', 'p.name_of_property')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->leftjoin('users as am1', 'am1.id', '=', 'p.asset_m_id')
                    ->leftjoin('users as am2', 'am2.id', '=', 'p.asset_m_id2')
                    ->leftjoin('users as hvbu', 'hvbu.id', '=', 'p.hvbu_id')
                    ->leftjoin('users as hvpm', 'hvpm.id', '=', 'p.hvpm_id')
                    ->where('pi.id', $invoice_id)
                    ->first();
        if($invoice){

            $file_name = ($invoice->invoice) ? '('.$invoice->invoice.')' : '';

            $url = route('properties.show',['property'=> $invoice->property_id]).'?tab=property_invoice';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $extern_url = route('extern_home').'?property_id='.base64_encode($invoice->property_id);
            $extern_url = '<a href="'.$extern_url.'">'.$extern_url.'</a>';

            if($invoice->user_role == 6){
                $iu_url = $extern_url;
            }else{
                $iu_url = $url;
            }

            $glink = "https://drive.google.com/drive/u/2/folders/".$invoice->file_basename;
            if($invoice->file_type == "file"){
                $glink = "https://drive.google.com/file/d/".$invoice->file_basename;
            }
            $glink = '<a href="'.$glink.'">'.$glink.'</a>';

            $reply_email = $user->email;
            $reply_name = $user->name;

            if ($status == 2){ //release status

                $email_not_send_array[] = config('users.falk_email');

                $cc_array = [];
                if( $invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am1_email;
                }
                if( $invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am2_email;
                }
                if( $invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvbu_email;
                }
                if( $invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvpm_email;
                }

                if(!empty($cc_array)){
                    $cc_array = array_unique($cc_array);
                }

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $latestCommentRes = DB::table('properties_comments as pc')->selectRaw('pc.comment, u.name')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.record_id', $invoice_id)->where('pc.type', 'property_invoices')->orderBy('pc.created_at', 'desc')->first();

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at).' für '.$invoice->plz_ort.' '.$invoice->ort." wurde von $reply_name freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br>deine Rechnung ".$file_name." wurde von $reply_name freigegeben. Link zum Intranet intern: ".$url;
                    $text .= "<br>Link zum Intranet extern: ".$extern_url;

                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    if(isset($latestCommentRes->comment) && $latestCommentRes->comment){
                        $text .= "<br><br>Letzter Kommentar ".$latestCommentRes->name.": ".$latestCommentRes->comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name, $cc_array) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->cc($cc_array)
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }
                
            }elseif ($status == 3) { // pending status

                $email_not_send_array[] = config('users.falk_email');

                $cc_array = [];
                if( $invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am1_email;
                }
                if( $invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am2_email;
                }
                if( $invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvbu_email;
                }
                if( $invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvpm_email;
                }

                if(!empty($cc_array)){
                    $cc_array = array_unique($cc_array);
                }

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde noch nicht freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br> deine Rechnung ".$file_name." ".$glink." wurde von Falk noch nicht freigegeben. Link zum Intranet: ".$iu_url;
                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name, $cc_array) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->cc($cc_array)
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }

            }elseif ($status == 4) { // not release status
                
                $email_not_send_array[] = config('users.falk_email');

                $cc_array = [];
                if( $invoice->am1_email && !in_array($invoice->am1_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am1_email;
                }
                if( $invoice->am2_email && !in_array($invoice->am2_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->am2_email;
                }
                if( $invoice->hvbu_email && !in_array($invoice->hvbu_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvbu_email;
                }
                if( $invoice->hvpm_email && !in_array($invoice->hvpm_email, $email_not_send_array) ){
                    $cc_array[] = $invoice->hvpm_email;
                }

                if(!empty($cc_array)){
                    $cc_array = array_unique($cc_array);
                }

                // send mail to uploaded user
                if($invoice->user_email && !in_array($invoice->user_email, $email_not_send_array)){
                    
                    $email_not_send_array[] = $invoice->user_email;

                    $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde nicht freigegeben";

                    $text = "Hallo ".$invoice->user_name.",<br> deine Rechnung ".$file_name." ".$glink." wurde von Falk nicht freigegeben. Link zum Intranet: ".$iu_url;
                    if($comment){
                        $text .= "<br><br>Kommentar: ".$comment;
                    }

                    $email = $invoice->user_email;
                    $name  = $invoice->user_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email, $name, $subject, $reply_email, $reply_name, $cc_array) {
                        $message->to($email,  $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->cc($cc_array)
                        ->replyTo($reply_email, $reply_name)
                        ->subject($subject);
                    });
                }

            }

        }
        return true;
    }

    public function getInvoiceForAM($status){
    	
    	$user = Auth::user();

    	$invoice = DB::table('property_invoices as pi')
    		->select('pi.*', 'p.name_of_property', 'u.name as user_name', 'u.email as user_email')
    		->join('properties as p', 'p.id', '=', 'pi.property_id')
    		->join('users as u', 'u.id', '=', 'pi.user_id')
            ->whereRaw('(p.asset_m_id = "'.$user->id.'" OR p.asset_m_id2 = "'.$user->id.'" OR pi.email = "'.$user->id.'")');
    		// ->whereRaw('(p.asset_m_id = "'.$user->id.'" OR p.asset_m_id2 = "'.$user->id.'")');
            
            if($status == 4){//not release
                $invoice = $invoice->whereRaw('(pi.am_status = 4 OR pi.hv_status = 4 OR pi.user_status = 4 OR pi.falk_status = 4)');
            }elseif($status == 3){//pending
                $invoice = $invoice->whereRaw('(pi.am_status = 3 OR pi.hv_status = 3 OR pi.user_status = 3 OR pi.falk_status = 3)');
            }elseif($status == 2){//release
                $invoice = $invoice->where('pi.falk_status', 2);
            }elseif($status == 0){//open
                $invoice = $invoice->whereRaw('((pi.am_status = 1 OR pi.am_status = 2) OR (pi.hv_status = 1 OR pi.hv_status = 2) OR (pi.user_status = 1 OR pi.user_status = 2) OR (pi.falk_status = 1)) AND pi.falk_status<>2');
            }else{//default 1 for request
                // $invoice = $invoice->where('pi.am_status', $status);
                $invoice = $invoice->whereRaw(' ( pi.am_status = "'.$status.'" OR (pi.email = "'.$user->id.'" AND pi.user_status = "'.$status.'") ) ');
            }
    		$invoice = $invoice->where('pi.deleted', 0)->get();

        if($invoice){
            foreach ($invoice as $key => $value) {
                $value->button = app('App\Http\Controllers\PropertiesController')->getInvoiceButton($value, $user);
            }
        }

    	return View::make('invoice.invoice-for-am', ['invoice' => $invoice,'user'=> $user, 'status' => $status])->render();
    }

    public function getInvoiceForHV($status){

    	$user = Auth::user();

    	$invoice = DB::table('property_invoices as pi')
    		->select('pi.*', 'p.name_of_property', 'u.name as user_name', 'u.email as user_email')
    		->join('properties as p', 'p.id', '=', 'pi.property_id')
    		->join('users as u', 'u.id', '=', 'pi.user_id');

    		/*if($user->role == 7){
    			$invoice = $invoice->where('p.hvbu_id', $user->id);
    		}elseif ($user->role == 8) {
    			$invoice = $invoice->where('p.hvpm_id', $user->id);
    		}*/

            if($user->role == 7){
                $invoice = $invoice->whereRaw('(p.hvbu_id = "'.$user->id.'" OR pi.email = "'.$user->id.'")');
            }elseif ($user->role == 8) {
                $invoice = $invoice->whereRaw('(p.hvpm_id = "'.$user->id.'" OR pi.email = "'.$user->id.'")');
            }

            if($status == 4){//not release
                $invoice = $invoice->whereRaw('(pi.am_status = 4 OR pi.hv_status = 4 OR pi.user_status = 4 OR pi.falk_status = 4)');
            }elseif($status == 3){//pending
                $invoice = $invoice->whereRaw('(pi.am_status = 3 OR pi.hv_status = 3 OR pi.user_status = 3 OR pi.falk_status = 3)');
            }elseif($status == 2){//release
                $invoice = $invoice->where('pi.falk_status', 2);
            }elseif($status == 0){//open
                $invoice = $invoice->whereRaw('((pi.am_status = 1 OR pi.am_status = 2) OR (pi.hv_status = 1 OR pi.hv_status = 2) OR (pi.user_status = 1 OR pi.user_status = 2) OR (pi.falk_status = 1)) AND pi.falk_status<>2');
            }else{//default 1 for request
                // $invoice = $invoice->where('pi.hv_status', $status);
                $invoice = $invoice->whereRaw(' ( pi.hv_status = "'.$status.'" OR (pi.email = "'.$user->id.'" AND pi.user_status = "'.$status.'") ) ');
            }

    		$invoice = $invoice->where('pi.deleted', 0)->get();

            if($invoice){
                foreach ($invoice as $key => $value) {
                    $value->button = app('App\Http\Controllers\PropertiesController')->getInvoiceButton($value, $user);
                }
            }

    	return View::make('invoice.invoice-for-hv', ['invoice' => $invoice,'user'=> $user, 'status' => $status])->render();
    }
}
