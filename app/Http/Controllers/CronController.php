<?php

namespace App\Http\Controllers;

use App\Models\Masterliste;
use App\Models\Properties;
use App\Models\PropertiesCustomField;
use App\Models\PropertyAccountBalance;
use DB;
use App\Models\PropertyLoansMirror;
use App\Models\PropertyRentPaids;
use App\Models\PropertiesDefaultPayers;
use App\Models\TenancyScheduleItem;
use App\Services\MasterlisteService;
use App\Services\TenancySchedulesService;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\TenancySchedule;
use App\Models\PropertiesBuyDetail;
use App\Models\PropertiesSaleDetail;
use App\Models\PropertiesTenants;
use App\Models\Banks;
use App\Ad;

class CronController extends Controller
{
    public function logoutAll()
    {
        DB::table('users')->update(['last_login_time' => rand(1,9)]);

    }
    public function sendPurchaseRemainder()
    {
        $date = date('Y-m-d',strtotime('-14 days'));
        $properties = DB::table('properties')->select('users.email','properties.name_of_property','users.name','properties.id','properties.main_property_id')
            ->join('users', 'users.id', '=', 'properties.asset_m_id')->where('properties.purchase_date', '=',$date)->get();
        $properties_all = json_decode(json_encode($properties), TRUE);
         echo $date;
         print_r($properties_all); 

        foreach ($properties_all as $key => $properties) {

            $url = route('properties.show',['property'=>$properties['main_property_id']]);
        

            //$text = 'Kündigungsfrist Versicherung '.$properties["axa"].' für das Objekt '.$properties["name_of_property"].' von '.$properties["name"].' beachten! Laufzeit Ende '.$properties["gebaude_laufzeit_to"].'  mit Kündigungsfrist  '.$properties["gebaude_kundigungsfrist"].' '.$url;

            $text = 'Hallo '.$properties["name"].', bitte trage für das Objekt '.$properties["name_of_property"].' die Hausverwaltung ein unter: '.$url;


            $email = $properties['email'];
            $email = "c.wiedemann@fcr-immobilien.de";
            //$email = "yiicakephp@gmail.com";
            if( !in_array($email, $this->mail_not_send()) ){

                Mail::raw($text, function ($message) use($email,$text) {
                            $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    // ->setBody($text, 'text/html')
                    ->subject('Hausverwaltung eintragen');

                });
            }    
        }
        echo "done";

        
    }




    public function sendInsuranceExpireRemainder()
    {
        $date = date('Y-m-d',strtotime('+6 months'));
        $properties = DB::table('properties')->select('users.email', 'properties.axa', 'properties.name_of_property', 'properties.asset_manager1', 'properties.gebaude_laufzeit_to', 'properties.gebaude_kundigungsfrist', 'users.name','properties.id')
            ->join('users', 'users.id', '=', 'properties.asset_m_id')->where('properties.gebaude_laufzeit_to', '=',$date)->get();
        $properties_all = json_decode(json_encode($properties), TRUE);
        // echo $date;
        // print_r($properties_all); die;

        foreach ($properties_all as $key => $properties) {

            $url = route('properties.show',['property'=>$properties['id']]);
        

            $text = 'Kündigungsfrist Versicherung '.$properties["axa"].' für das Objekt '.$properties["name_of_property"].' von '.$properties["name"].' beachten! Laufzeit Ende '.$properties["gebaude_laufzeit_to"].'  mit Kündigungsfrist  '.$properties["gebaude_kundigungsfrist"].' '.$url;


            $email = $properties['email'];
            //$email = "c.wiedemann@fcr-immobilien.de";
            //$email = "yiicakephp@gmail.com";

            if( !in_array($email, $this->mail_not_send()) ){

                Mail::raw($text, function ($message) use($email) {
                            $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->subject('Erinnerung Kündigung Versicherung');
                });
            }    
        }

        $properties = DB::table('properties')->select('users.email', 'properties.axa', 'properties.name_of_property', 'properties.asset_manager1', 'properties.haftplicht_laufzeit_to', 'properties.haftplicht_kundigungsfrist', 'users.name','properties.id')
            ->join('users', 'users.id', '=', 'properties.asset_m_id')->where('properties.haftplicht_laufzeit_to', '=',$date)->get();
        $properties_all = json_decode(json_encode($properties), TRUE);
        // echo $date;
        // print_r($properties_all); die;

        foreach ($properties_all as $key => $properties) {

            $url = route('properties.show',['property'=>$properties['id']]);

            $text = 'Kündigungsfrist Versicherung '.$properties["axa"].' für das Objekt '.$properties["name_of_property"].' von '.$properties["name"].' beachten! Laufzeit Ende '.$properties["haftplicht_laufzeit_to"].'  mit Kündigungsfrist  '.$properties["haftplicht_kundigungsfrist"].' '.$url;

            $email = $properties['email'];
            //$email = "c.wiedemann@fcr-immobilien.de";
            //$email = "yiicakephp@gmail.com";
            if( !in_array($email, $this->mail_not_send()) ){

                Mail::raw($text, function ($message) use($email) {
                            $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->subject('Erinnerung Kündigung Versicherung');
                });
            }
        }


        $properties = DB::table('properties')->selectRaw('users.email, property_insurances.name as axa,properties.name_of_property,properties.asset_manager1,property_insurances.date_to,property_insurances.note,users.name,properties.id')
            ->join('users', 'users.id', '=', 'properties.asset_m_id')
            ->join('property_insurances', 'property_insurances.property_id', '=', 'properties.id')
            
            ->where('property_insurances.date_to', '=',$date)->get();
        $properties_all = json_decode(json_encode($properties), TRUE);
        // echo $date;
        // print_r($properties_all); die;

        foreach ($properties_all as $key => $properties) {

            $url = route('properties.show',['property'=>$properties['id']]);

            $text = 'Kündigungsfrist Versicherung '.$properties["axa"].' für das Objekt '.$properties["name_of_property"].' von '.$properties["name"].' beachten! Laufzeit Ende '.$properties["date_to"].'  mit Kündigungsfrist  '.$properties["note"].' '.$url;

            $email = $properties['email'];
            //$email = "c.wiedemann@fcr-immobilien.de";
            //$email = "yiicakephp@gmail.com";
            if( !in_array($email, $this->mail_not_send()) ){
                
                Mail::raw($text, function ($message) use($email) {
                            $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->subject('Erinnerung Kündigung Versicherung');
                });
            }
        }
        echo "done";

        
    }

    public function sendTenantExpireRemainder()
    {
        $date = date('Y-m-d',strtotime('+1 year'));

        // $date = "2019-06-30";

        $data = TenancyScheduleItem::where('rent_end',$date)->join('tenancy_schedules','tenancy_schedule_items.tenancy_schedule_id','=','tenancy_schedules.id')->whereRaw('status=1')->get();
        print "<pre>";

        print_r($data);

        foreach ($data as $key => $value) {
            $pr = Properties::where('id',$value->property_id)->where('status',6)->first();

            $tenenat_name = $value->name;

            if($pr && isset($pr->asset_manager->email) && $pr->asset_manager->email)
            {
                $email =  $pr->asset_manager->email;
                $name = $v_name =  $pr->asset_manager->name;

                $name_of_property = $pr->name_of_property;

                $bank = 0;
                $bank_ids = json_decode($pr->bank_ids,true);
                if($bank_ids != null && isset($bank_ids[0]))
                {
                    $bank = $bank_ids[0];
                }
                if($bank){
                        $propert  =  DB::table('properties')->where('properties.Ist', $bank)->where('main_property_id',$pr->id)->first();
                         if($propert)
                             $name_of_property = $propert->name_of_property;

                }

                // $email = "c.wiedemann@fcr-immobilien.de";

                // echo $tenenat_name; die;


                $url = route('properties.show',['property'=>$value->property_id]);
                if( !in_array($email, $this->mail_not_send()) ){
                    try{ 
                        Mail::send('emails.tenant_expire', ['v_name'=>$v_name,'tenenat_name'=>$tenenat_name,'name_of_property'=>$name_of_property,'url'=>$url], function ($m) use ($email,$name) {
                          $m->from('info@intran.fcr-immobilien.de', 'FCR INTRANET');
                          $m->to($email, $name)->subject("Dein Mietvertrag läuft aus");
                        });
                    }catch (\Exception $e){
                 
                    }
                }
            }
        }

    }
    //
    public function update_masterliste(){
        $bestand_properties = Properties::where('status','=',config('properties.status.duration'))->where('Ist',0)->where('soll',0)->get();
        foreach($bestand_properties as $bestand_propertie){

            $tenancy_schedule_data = TenancySchedulesService::get( $bestand_propertie->id);
//            $tenancy_schedule= isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : null;
            if(!$bestand_propertie->masterliste_id){

                $masterliste= Masterliste::create(
                    [
                        'object'=>$bestand_propertie->name_of_property,
                        'asset_manager'=>$bestand_propertie->user_id,
                        'flat_in_qm'=>0,
                        'vacancy'=>0,
                        'vacancy_structurally'=>0,
                        'technical_ff'=>0,
                        'rented_area'=>0,
                        'rent_net_pm'=>0,
                        'avg_rental_fee'=>0,
                        'potential_pm'=>0,
                    ]);
                if($masterliste){
                    $bestand_propertie->masterliste_id=$masterliste->id;
                    $bestand_propertie->save();
                }
            }
//            else{
//                $masterliste = Masterliste::where('id', '=', $bestand_propertie->masterliste_id)->first();
////                var_dump(json_encode($masterliste));die;
//                $masterliste['vacancy'] = $masterliste['flat_in_qm'] - $masterliste['rented_area'];
//                $masterliste['avg_rental_fee'] = ($masterliste['rented_area']) ? $masterliste['rent_net_pm'] / $masterliste['rented_area'] : 0;
//                $masterliste['potential_pm'] = $masterliste['avg_rental_fee'] * $masterliste['vacancy'];
//
//
//                if (MasterlisteService::update( $masterliste, $masterliste )) {
//                    $response = [
//                        'success' => true,
//                        'msg' => ''
//                    ];
//                }
//            }

        }
        MasterlisteService::sync_all_masterliste();


        // $tenancy_schedule_items = TenancyScheduleItem::all();
        // $today = date('Y-m-d');
        // foreach ($tenancy_schedule_items as $item){
        //     if ($item->rent_end != null) {
        //         if ($item->rent_end < $today){
        //             $item->delete();
        //         }
        //     }
        // }
    }
    public function update_properties()
    {

        $path = "FCR_Konten_2020.08.24-new.xlsx";
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($path);

        /**  Create a new Reader of the type that has been identified  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $data = $spreadsheet->getActiveSheet()->toArray();
        
        // pre($data);

        foreach ($data as $key => $value) {
            

            $value1 = str_replace(' ', '', $value[4]);
            $value1 = str_replace(',', '', $value1);
            $value1 = str_replace('%', '', $value1);
            $value1= str_replace('€', '', $value1);

            if($key>18 && is_numeric($value1) && $value[2]){
                // echo $value1; die;
                $check = PropertiesCustomField::where('property_id', $value[2])->where('slug','current_balance')->first();
                
                if($check)
                    $modal = $check;
                else
                    $modal = new PropertiesCustomField;
                
                $modal->property_id = $value[2];
                $modal->slug = 'current_balance';
                $modal->content = $value1;
                $modal->updated_at = date('Y-m-d H:i:s');
                $modal->save();

                $modal1 =new PropertyAccountBalance;
                $modal1->property_id = $value[2];
                $modal1->current_balance = $value1;
                $modal1->save();
            }
        
        }

        $link = route('dashboard').'?section=liquiplanung-1';

        $email = config('users.falk_email');
        // $email = "yiicakephp@gmail.com";
        
        $text = " Hallo Falk, die Kontostände wurden aktualisiert. Der Liquiplan ist nun auf dem aktuellen Stand: ".$link;
        $subject = "Liquiplan aktualisiert";

        $cc = array('a.lauterbach@fcr-immobilien.de','t.raudies@fcr-immobilien.de');
        // $cc = array('bhalodiyaravi@gmail.com');


        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$cc) {
            $message->to($email)
            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
            ->cc($cc)
            ->subject($subject);
        });



        $this->storeLiquiplanung();
        echo "update done";

        die;

        $path = "Bank.XLSX";
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($path);

        /**  Create a new Reader of the type that has been identified  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $data = $spreadsheet->getActiveSheet()->toArray();
        
        // pre($data);

        foreach ($data as $key => $value) {
            # code...
            // pre($value);
            if($key>0)
            {
                // $value = $value[0];
                $value[1] = trim($value[1]);

                $bank = Banks::where("name","LIKE",$value[1])->first();
                if(!$bank)
                {
                    $model = new Banks;
                    $model->name = $value[1];
                    $model->user_id = 1;
                    $model->firstName = $value[3];
                    $model->surname = $value[4];
                    $model->contact_name = $value[3].' '.$value[4];
                    $model->contact_email = $value[11];
                    $model->contact_phone = $value[9];
                    $model->postal_code = $value[6];
                    $model->district = $value[8];
                    $model->fax = $value[10];
                    $model->internet = $value[12];
                    $model->address = $value[5].' '.$value[6].' '.$value[7].' '.$value[8];
                    $model->notes = $value[13];
                    $model->save();
                }
            }
        }
        echo "done";
        die;




        $path = 'bank_data.xlsx';
        
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($path);

        /**  Create a new Reader of the type that has been identified  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($path);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $data = $spreadsheet->getActiveSheet()->toArray();
        
        foreach ($data as $key => $value) {
            if($key>=5 && trim($value[4]))
            {
                // pre($value);
                $property_id = trim($value[4]);
                $lender = trim($value[6]);
                $n = str_replace(',', '', trim($value[7]));
                if(is_numeric($n))
                    $market_value= $n*1000;
                    
                $delta = "";
                if(trim($value[8]) && trim($value[8])!="n/a"){
                    $n = str_replace(',', '', trim($value[8]));
                    if(is_numeric($n))
                    $delta= $n*1000;
                    // $delta= str_replace(',', '', trim($value[8]))*1000;
                }
                $delta2 = "";
                if(trim($value[9]) && trim($value[9])!="n/a")
                {
                    $n = str_replace(',', '', trim($value[9]));
                    if(is_numeric($n))
                    $delta2= $n*1000;
                }
                $gsbank = "";
                if(trim($value[10]) && trim($value[10])!="n/a")
                {
                    $n = str_replace(',', '', trim($value[10]));
                    if(is_numeric($n))
                        $gsbank= $n*1000;
                }
                $variable_interest = "";
                if(trim($value[11]) && trim($value[11])!="n/a")
                {
                    $n = str_replace('%', '', trim($value[11]));
                    if(is_numeric($n))
                    $variable_interest = $n;
                }
                $interest_extend = "";
                if(trim($value[12]) && trim($value[12])!="n/a")
                $interest_extend = str_replace(',', '', trim($value[12]))*1000;
                $loan_service = "";
                $loan_service_month = "";
                if(trim($value[13]) && trim($value[13])!="n/a")
                {
                    $loan_service = str_replace(',', '',trim($value[13]));
                    if(is_numeric($loan_service)){
                        $loan_service = $loan_service*1000;
                        $loan_service_month = $loan_service/12;
                    }
                }   
                $loansMirrorData = PropertyLoansMirror::where('property_id', $property_id)->first();
                if(!$loansMirrorData){
                    $loansMirrorData = new PropertyLoansMirror;
                }
                
                $loansMirrorData->property_id = $property_id;
                // $loansMirrorData->lender = $lender;
                $loansMirrorData->original_loan = $gsbank;
                $loansMirrorData->delta = $delta;
                $loansMirrorData->delta2 = $delta2;
                $loansMirrorData->zins = $variable_interest;
                $loansMirrorData->interest_extend = $interest_extend;
                $loansMirrorData->loan_service = $loan_service;
                $loansMirrorData->loan_service_month = $loan_service_month;
                $loansMirrorData->save();

                $update_data1 = DB::table('properties')->whereRaw('(main_property_id=' . $property_id . ' OR id=' . $property_id.') and lock_status=0')
            ->update([
                "market_value" => $market_value]);

                // echo $property_id;die;
                

            }
        }
    }

    public function invoiceOverview(){

        //Open invoices
        $open_invoice = $data = DB::table('property_invoices as pi')
                    ->selectRaw('IFNULL(COUNT(pi.id), 0) as total')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    // ->where('pi.not_release_status', 0)
                    ->where('pi.deleted', 0)
                    // ->havingRaw('is_release2 != 1 and is_request2=1')
                    ->where('pi.falk_status', 1)
                    ->first()->total;

        // pending and not release invoice
        $res = DB::table('property_invoices as pi')
                    ->select(DB::raw("IFNULL( SUM(pi.falk_status = 4), 0) AS not_releas"), DB::raw("IFNULL( SUM(pi.falk_status = 3), 0) AS pending"))
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->whereNotIn('pi.falk_status', [0,1,2])
                    ->where('pi.deleted', 0)
                    ->first();
        
        /*$offer_release_res = DB::table('property_insurance_tabs as pi')
                    ->select(DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurance_release' AND record_id = pi.id AND tab='insurance_tab') THEN 1 ELSE 0 END as release_status"),DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurance_request' AND record_id = pi.id AND tab='insurance_tab') THEN 1 ELSE 0 END as request_status"))
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')->where('pi.deleted', 0)->where('pi.not_release_status', 0)->havingRaw('release_status != 1 and request_status=1')->groupBy('pi.id')->get()->count();*/


        $data_property_insurance = DB::table('property_insurance_tabs as pi')
                ->select('pi.id', DB::raw("CASE WHEN EXISTS (SELECT id FROM properties_mail_logs WHERE (type = 'insurance_release' OR type = 'insurancetab_release') AND record_id = pi.id AND (tab='insurance_tab' OR tab='insurancetab2')) THEN 1 ELSE 0 END as release_status"),DB::raw("CASE WHEN EXISTS (SELECT id FROM properties_mail_logs WHERE type = 'insurance_request' AND record_id = pi.id AND tab='insurance_tab') THEN 1 ELSE 0 END as request_status"))
                ->where('pi.deleted', 0)->where('pi.not_release_status', 0)->havingRaw('release_status != 1 and request_status=1')->groupBy('pi.id')->get();

        $offer_release_res = 0;

        foreach ($data_property_insurance as $key => $property_insurance) {
            $detail = DB::table('property_insurance_tab_details as pid')
                ->select('pid.id', 'pid.amount', 'pid.comment', 'pid.created_at', 'pid.file_basename', 'pid.file_type','pid.file_name', 'pid.asset_manager_status', 'pid.falk_status','u.name as user_name', 'u.id as user_id', 'pid.falk_comment',
                    DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurancetab_release' AND selected_id = pid.id AND tab='insurancetab2' ) THEN 1 ELSE 0 END as is_release"),
                    DB::raw("(SELECT comment from properties_comments WHERE record_id = pid.id AND type = 'property_insurance_tab_details' ORDER BY created_at DESC LIMIT 1) as latest_comment")
                )
                ->join('users as u', 'u.id', '=', 'pid.user_id')
                ->where('property_insurance_tab_id', $property_insurance->id)
                ->where('pid.deleted', 0)
                ->where('pid.not_release', 0)
                ->havingRaw('is_release=0')
                ->orderBy('id', 'DESC')
                ->get();


           $detail1 = DB::table('property_insurance_tab_details as pid')
                ->select('pid.id',
                    DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurancetab_release' AND selected_id = pid.id AND tab='insurancetab2' ) THEN 1 ELSE 0 END as is_release"))
                ->where('property_insurance_tab_id', $property_insurance->id)
                ->where('pid.deleted', 0)
                ->where('pid.not_release', 0)
                ->havingRaw('is_release=1')
                ->orderBy('id', 'DESC')
                ->get();
            
            if(count($detail) && count($detail1)==0)
                $offer_release_res +=1; 
        }

        // ZU VERTEILENDE OBJEKTE
        //$object_distributed_res = Properties::selectRaw('IFNULL(COUNT(properties.id), 0) as total')->whereIn('status', [10, 12])->where('Ist', 0)->where('soll', 0)->first();

        // FREIGABE GEBÄUDEVERS
        $approval_building_res = DB::table('properties')->select('id',DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'release_building' AND property_id = properties.id) THEN 1 ELSE 0 END as is_release2"),DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'building' AND property_id = properties.id) THEN 1 ELSE 0 END as is_request2"))->where('Ist', 0)->where('soll', 0)->where('main_property_id', 0)->havingRaw('is_release2=0 and is_request2=1')->get()->count();

        // FREIGABE HAFTPFLICHTVERS.
        $release_liability_res = DB::table('properties')->select('id',DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'release_liability' AND property_id = properties.id) THEN 1 ELSE 0 END as is_release2"),DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'liability' AND property_id = properties.id) THEN 1 ELSE 0 END as is_request2"))->where('Ist', 0)->where('soll', 0)->where('main_property_id', 0)->havingRaw('is_release2=0 and is_request2=1')->get()->count();

        // FREIGABE HAUSVERWALTUNG
        $approval_house_mnagement_res = DB::table('property_managements as pi')
                    ->select(DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'release_management' AND property_id = pi.property_id) THEN 1 ELSE 0 END as is_release2"),DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'management' AND property_id = pi.property_id) THEN 1 ELSE 0 END as is_request2"))->join('properties as p', 'p.id', '=', 'pi.property_id')->havingRaw('is_release2 != 1 and is_request2=1')->groupBy('pi.property_id')->get()->count();

        // PROVISIONSFREIGABEN
        $commission_release_res = DB::table('provisions as pr')
                ->select('pr.id','p.name_of_property','p.asset_m_id', 'u1.name as am_name2', 'u.name as am_name', 'pr.property_id','pr.name','pr.rent','pr.cost','pr.net_income','pr.commision_percent','mv_vl', DB::raw("CASE WHEN EXISTS (SELECT * FROM empfehlung_details WHERE slug = 'pbtn2' AND provision_id = pr.id) THEN 1 ELSE 0 END as is_pbtn2"), DB::raw("CASE WHEN EXISTS (SELECT * FROM empfehlung_details WHERE slug = 'pbtn2_request' AND provision_id = pr.id) THEN 1 ELSE 0 END as is_pbtn2_request"), DB::raw("CASE WHEN EXISTS (SELECT * FROM empfehlung_details WHERE slug = 'provosion_mark_as_not_release' AND provision_id = pr.id) THEN 1 ELSE 0 END as is_not_release"))
                ->join('properties as p', 'p.id', '=', 'pr.property_id')
                ->leftJoin('users as u1', 'u1.id', '=', 'p.asset_m_id')
                ->leftJoin('users as u', 'u.id', '=', 'pr.asset_m_id')
                ->where('pr.deleted',0)
                ->havingRaw('is_pbtn2 != 1 AND is_pbtn2_request = 1 AND is_not_release != 1')
                ->groupBy('pr.id')->get()->count();

        // EINKAUFSFREIGABEN
        $statuss = array(15, 7, 5, 14, 16, 10, 12);
        $strmat  = '""';
        $q       = '';

        $properties_all = Properties::select('main_property_id', 'plz_ort', 'ort')
            ->whereRaw('main_property_id!=0 and bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat . $q)
            ->whereIn('properties.status', $statuss)
            ->where('property_status', 0)
            ->groupBy('properties.main_property_id')
            ->get();

        $ids = array();
        if($properties_all){
            foreach ($properties_all as $key => $value) {
                $ids[] = $value->main_property_id;
            }
        }

        $shopping_approval_res = PropertiesBuyDetail::whereIn('type', array('btn3','btn4'))->whereIn('property_id', $ids)->get();
        // pre($ids);

        $name = array();
        foreach ($shopping_approval_res as $key => $value) {
            $name[$value->property_id][$value->type] = date_formatting(date('d/m/Y', strtotime($value->created_at)));
        }
        // pre($shopping_approval_res);
        $shopping_approval_res = 0;
        $shopping_approval_res_and = 0;
        foreach ($name as $key => $list) {
            if(!isset($list['btn4'])){
                $shopping_approval_res +=1; 
            }
            if(!isset($list['btn3'])){
                $shopping_approval_res_and +=1; 
            }
        }

        // VERKAUFSFREIGABEN
        $statuss = array(6);
        $strmat  = '""';
        $q       = '';

        $properties_all = Properties::select('main_property_id', 'plz_ort', 'ort')
            ->whereRaw('main_property_id!=0 and bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat . $q)
            ->whereIn('properties.status', $statuss)
            ->where('property_status', 0)
            ->groupBy('properties.main_property_id')
            ->get();

        $ids      = array();
        if($properties_all){
            foreach ($properties_all as $key => $value) {
                $ids[] = $value->main_property_id;
            }
        }
        $sales_release_res = PropertiesSaleDetail::whereIn('type', array('btn3', 'btn4'))->whereIn('property_id', $ids)->get();

        $name = array();
        foreach ($sales_release_res as $key => $value) {
            $name[$value->property_id][$value->type] = date_formatting(date('d/m/Y', strtotime($value->created_at)));
        }
        $sales_release_res = 0;
        $sales_release_res_and = 0;
        foreach ($name as $key => $list) {
            if(!isset($list['btn4'])){
                $sales_release_res +=1; 
            }
            if(!isset($list['btn3'])){
                $sales_release_res_and +=1; 
            }
        }

        // FREIGEGEBEN VERTRÄGE
        $released_contract_res = DB::table('property_contracts as pc')
                      ->select(DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'contract_request' AND record_id = pc.id) THEN 1 ELSE 0 END as contract_request"), DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'contract_release' AND record_id = pc.id) THEN 1 ELSE 0 END as contract_release"))
                      ->join('users as u', 'u.id', '=', 'pc.user_id')->join('properties as p', 'p.id', '=', 'pc.property_id')
                      ->where('pc.not_release_status', 0)->havingRaw('contract_request = 1 and contract_release != 1')->get()->count();


        

        $empfeh_count = DB::table('empfehlung')
                ->select('empfehlung.id', 'empfehlung.tenant_id', DB::raw('SUM(amount) as amount'), DB::raw("CASE WHEN EXISTS (SELECT id FROM empfehlung_details WHERE slug = 'btn2' AND tenant_id = empfehlung.tenant_id) THEN 1 ELSE 0 END as btn2"), DB::raw("CASE WHEN EXISTS (SELECT id FROM empfehlung_details WHERE slug = 'btn2_request' AND tenant_id = empfehlung.tenant_id) THEN 1 ELSE 0 END as btn2_request"),'property_vacants.property_id','users.name','properties.name_of_property','property_vacants.tenant_id as pvid')
                ->join('property_vacants','empfehlung.tenant_id','=','property_vacants.id')
                ->join('properties','property_vacants.property_id','=','properties.id')
                ->leftJoin('users','properties.asset_m_id','=','users.id')
                ->where('deleted', 0)
                ->where('property_vacants.not_release_status', 0)
                ->havingRaw('btn2 != 1 and btn2_request = 1')
                ->groupby('tenant_id')
                ->get()->count();


        $pending_invoice = $res->pending;
        $not_released_invoices = $res->not_releas;
        $offer_release = $offer_release_res;
        //$object_distributed = $object_distributed_res->total;
        $approval_building = $approval_building_res;
        $release_liability = $release_liability_res;
        $approval_house_mnagement = $approval_house_mnagement_res;
        $commission_release = $commission_release_res;
        $shopping_approval = $shopping_approval_res;
        $sales_release = $sales_release_res;
        $released_contract = $released_contract_res;

        
        $link_open_invoice = route('dashboard').'?section=invoice';
        $link_pending_invoice = route('dashboard').'?section=pending-invoice';
        $link_not_release_invoice = route('dashboard').'?section=notrelease-invoice';
        $link_offer_release = route('dashboard').'?section=angebot-release-section';
        $link_object_distributed = route('dashboard').'?section=vobj';
        $link_approval_building = route('dashboard').'?section=gabuderelease';
        $link_approval_building = route('dashboard').'?section=gabuderelease';
        $link_release_liability = route('dashboard').'?section=haftrelease';
        $link_approval_house_mnagement = route('dashboard').'?section=hausrelease';
        $link_commission_release = route('dashboard').'?section=provision';
        $link_shopping_approval = route('dashboard').'?section=erelease';
        $link_sales_release = route('dashboard').'?section=vrelease';
        $link_released_contract = route('dashboard').'?section=contract';
        $link_empfehlung = route('dashboard').'?section=recommendation-section';

        $email = config('users.falk_email');
        // $email = "yiicakephp@gmail.com";
        $subject = 'Aktuelle Freigabeübersicht';


       $text = '
            <p>Hallo Falk, <br><br>folgend die aktuelle Übersicht der Freigaben</p>
            <p>1. '. $open_invoice .' offene Rechnungen: <a href="'. $link_open_invoice .'">'. $link_open_invoice .'</a></p>
            <p>2. '. $pending_invoice .' Pending Rechnungen: <a href="'. $link_pending_invoice .'">'. $link_pending_invoice .'</a></p>
            <p>3. '. $not_released_invoices .' nichtfreigegebene Rechnungen: <a href="'. $link_not_release_invoice .'">'. $link_not_release_invoice .'</a></p>
            <p>4. '. $offer_release .' Angebotsfreigaben: <a href="'. $link_offer_release .'">'. $link_offer_release .'</a></p>
            <p>5. '. $approval_building .' Gebäudeversicherungsfreigaben: <a href="'. $link_approval_building .'">'. $link_approval_building .'</a></p>
            <p>6. '. $release_liability .' Haftpflichtversicherungsfreigaben: <a href="'. $link_release_liability .'">'. $link_release_liability .'</a></p>
            <p>7. '. $approval_house_mnagement .' Hausverwaltungsfreigaben: <a href="'. $link_approval_house_mnagement .'">'. $link_approval_house_mnagement .'</a></p>
            <p>8. '. $commission_release .' Provisionsfreigaben: <a href="'. $link_commission_release .'">'. $link_commission_release .'</a></p>
            <p>9. '. $shopping_approval .' Einkaufsfreigaben: <a href="'. $link_shopping_approval .'">'. $link_shopping_approval .'</a></p>
            <p>10. '. $sales_release .' Verkaufsfreigaben: <a href="'. $link_sales_release .'">'. $link_sales_release .'</a></p>
            <p>11. '. $released_contract .' Vertragsfreigaben: <a href="'. $link_released_contract .'">'. $link_released_contract .'</a></p>
            <p>12. '. $empfeh_count .' Vermietungsempfehlungen: <a href="'. $link_released_contract .'">'. $link_empfehlung .'</a></p>';

        if( !in_array($email, $this->mail_not_send()) ){

            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject) {
                $message->to($email)
                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                ->subject($subject);
            });
        } 

        $property_not_show = [];
        $notShowIds = Properties::select('id')->where('is_not_an_existing', 1)->get();
        if($notShowIds){
            foreach ($notShowIds as $notShowId) {
                $property_not_show[] = $notShowId->id;
            }
        }

        $released = DB::table('property_invoices as pi')
                ->select('pi.id','pi.amount', 'pi.invoice','pi.property_id', 'u.name', 'p.name_of_property', 'p.status', 'pi.file_basename', 'pi.file_type','pi.date', 'pi.created_at', 'pi.comment', DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'release2' AND record_id = pi.id) THEN 1 ELSE 0 END as is_release2"),'p.hvbu_id','p.hvpm_id','p.hausmaxx')
                ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pi.id')
                ->join('properties as p', 'p.id', '=', 'pi.property_id')
                ->join('users as u', 'u.id', '=', 'pi.user_id')
                ->where('is_paid',0)
                ->where('pi.deleted', 0)
                ->where('pi.property_id','!=', 1197)
                ->havingRaw('is_release2 = 1')
                ->groupBy('pi.id')
                ->get();

        if(count($released) > 0){
            $pr_arr = array(3363,3417,3390,3394,2179);
            foreach ($released as $key => $element) {

                $amount_condition = 0;
                if(!in_array($element->property_id, $pr_arr))
                    $amount_condition = 10000;


                if($element->status==6)
                    $amount_condition = 10000;
                else
                    $amount_condition = 0;

                if($element->property_id==121)
                    $amount_condition = 30000;

                

                if(!$element->hvbu_id && !$element->hvpm_id)
                    $amount_condition = 0;                  
                
                if($element->status==8)
                    $amount_condition = 0;  

                if($element->hausmaxx=="Eigenverwaltung")
                    $amount_condition = 0;
                


                // if(in_array($element->property_id, array(3471, 3363, 3417, 3394, 3473, 183, 182, 1197, 3652, 1194, 3912, 3925,4024,4267)))
                //     $amount_condition = 0;

                if(in_array($element->property_id, $property_not_show)){
                    $amount_condition = 0;
                }
                
                if($element->amount > $amount_condition){
                    continue;
                }else{
                   unset($released[$key]); 
                }
            }
        }

        // $html = \View::make('emails.released_invoice', array('released' => $released))->render();

        $email = 't.raudies@fcr-immobilien.de';
        // $email = "yiicakephp@gmail.com";
        
        $name = 'Thorsten Raudies';
        $subject = 'Freigegebene Rechnungen';

        if( count($released) > 0 && !in_array($email, $this->mail_not_send()) ){
            Mail::send('emails.released_invoice', ['released' => $released], function ($m) use ($email, $name, $subject) {
                $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                $m->to($email, $name)->subject($subject);
            });
        }
        
        echo 'Invoice overview mail send successfully';exit();
    }

    public function updateverkauflisting()
    {
        $ids = array();
        $url  = "https://verkauf.fcr-immobilien.de/getuploadlist";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $str = curl_exec($curl);
        curl_close($curl);
        if ($str) {
            $ids = json_decode($str, true);
        }
        // $ids = array(126);
        $ads = Ad::select('ads.*','properties.verkaufspreis as vprice')->whereIn('ads.id',$ids)
        ->join('properties','properties.main_property_id','=','ads.property_id')
        ->groupBy('ads.property_id')
        ->get();
        $update_date = 1;

        // pre($ads);

        foreach ($ads as $key => $value) {
            $tenancy_schedule_data = TenancySchedulesService::get($value->property_id, $update_date);

            $an_rent1 = 0;
            $wault = $value->wault;
            foreach($tenancy_schedule_data['tenancy_schedules'] as $tenancy_schedule){
                $an_rent1 = $tenancy_schedule->calculations['total_actual_net_rent'] * 12;
                $wault =  $tenancy_schedule->calculations['verkauf_wault'];
            }

            $value->wault = $wault; 
            $m_price = $value->price;

            $mtn = 0;
            if($m_price)
                $mtn = 100*$an_rent1/$m_price;

            $value->annual_rent = $an_rent1;
            $value->equity = $mtn;
            $value->save();
            // pre($value);
            // pre($tenancy_schedule_data);
        
            $this->postlisting($value->id);
        }

        echo "saved";
        // pre($ads);
        
    }

    public function update_jsondata()
    {
        $update_date = 1;

        $d = TenancySchedule::where('property_id','!=',0)->get();
        // pre($d);
        foreach($d as $value)
        $tenancy_schedule_data = TenancySchedulesService::get($value->property_id, $update_date);


    }

    public function expiryOverview(){

        $this->update_jsondata();

        // EINKAUFSFREIGABEN
        $statuss = array(15, 7, 5, 14, 16, 10, 12);
        $strmat  = '""';
        $q       = '';

        $properties_all = Properties::select('main_property_id', 'plz_ort', 'ort')
            ->whereRaw('main_property_id!=0 and bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat . $q)
            ->whereIn('properties.status', $statuss)
            ->where('property_status', 0)
            ->groupBy('properties.main_property_id')
            ->get();

        $ids = array();
        if($properties_all){
            foreach ($properties_all as $key => $value) {
                $ids[] = $value->main_property_id;
            }
        }

        $shopping_approval_res = PropertiesBuyDetail::whereIn('type', array('btn3','btn4'))->whereIn('property_id', $ids)->get();
        // pre($ids);

        $name = array();
        foreach ($shopping_approval_res as $key => $value) {
            $name[$value->property_id][$value->type] = date_formatting(date('d/m/Y', strtotime($value->created_at)));
        }
        // pre($shopping_approval_res);
        $shopping_approval_res = 0;
        $shopping_approval_res_and = 0;
        foreach ($name as $key => $list) {
            if(!isset($list['btn4'])){
                $shopping_approval_res +=1; 
            }
            if(!isset($list['btn3'])){
                $shopping_approval_res_and +=1; 
            }
        }

        // VERKAUFSFREIGABEN
        $statuss = array(6);
        $strmat  = '""';
        $q       = '';

        $properties_all = Properties::select('main_property_id', 'plz_ort', 'ort')
            ->whereRaw('main_property_id!=0 and bank_ids is not null and bank_ids!="" and bank_ids!=' . $strmat . $q)
            ->whereIn('properties.status', $statuss)
            ->where('property_status', 0)
            ->groupBy('properties.main_property_id')
            ->get();

        $ids      = array();
        if($properties_all){
            foreach ($properties_all as $key => $value) {
                $ids[] = $value->main_property_id;
            }
        }
        $sales_release_res = PropertiesSaleDetail::whereIn('type', array('btn3', 'btn4'))->whereIn('property_id', $ids)->get();

        $name = array();
        foreach ($sales_release_res as $key => $value) {
            $name[$value->property_id][$value->type] = date_formatting(date('d/m/Y', strtotime($value->created_at)));
        }
        $sales_release_res = 0;
        $sales_release_res_and = 0;
        foreach ($name as $key => $list) {
            if(!isset($list['btn4'])){
                $sales_release_res +=1; 
            }
            if(!isset($list['btn3'])){
                $sales_release_res_and +=1; 
            }
        }
        $commission_release_res = DB::table('provisions as pr')
                    ->select(DB::raw("CASE WHEN EXISTS (SELECT * FROM empfehlung_details WHERE slug = 'pbtn2' AND provision_id = pr.id) THEN 1 ELSE 0 END as is_pbtn2"), DB::raw("CASE WHEN EXISTS (SELECT * FROM empfehlung_details WHERE slug = 'pbtn2_request' AND provision_id = pr.id) THEN 1 ELSE 0 END as is_pbtn2_request"))
                    ->join('properties as p', 'p.id', '=', 'pr.property_id')->havingRaw('is_pbtn2 != 1 AND is_pbtn2_request = 1')->groupBy('pr.id')->get()->count();




        $email = "a.raudies@fcr-immobilien.de";
        // $email = "yiicakephp@gmail.com";
        $subject = 'Aktuelle Freigabeübersicht';


        $link_commission_release = route('dashboard').'?section=provision';
        $link_shopping_approval = route('dashboard').'?section=erelease';
        $link_sales_release = route('dashboard').'?section=vrelease';


       $text = '
            <p>Hallo Andrea, <br><br>folgend die aktuelle Übersicht der Freigaben</p>
            <p>1. '. $commission_release_res .' Provisionsfreigaben: <a href="'. $link_commission_release .'">'. $link_commission_release .'</a></p>
            <p>2. '. $shopping_approval_res_and .' Einkaufsfreigaben: <a href="'. $link_shopping_approval .'">'. $link_shopping_approval .'</a></p>
            <p>3. '. $sales_release_res_and .' Verkaufsfreigaben: <a href="'. $link_sales_release .'">'. $link_sales_release .'</a></p>';
        
        if(1==2)
        {   
            if( !in_array($email, $this->mail_not_send()) ){

                Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject) {
                    $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->subject($subject);
                });
            }
                
        }
        
         
        $asset_manager = DB::table('users')->select('id', 'name', 'email')->whereRaw('(role=4 or second_role=4) and id!=1 and id!=29 and id!=36 and user_deleted=0 and user_status=1')->get();

        if(!empty($asset_manager)){
            foreach ($asset_manager as $am) {

                if($am->email == config('users.falk_email')){

                    $not_release_invoice = $pending_invoice = array();

                }else{

                    // 1 PENDING INVOICE
                    $pending_invoice = DB::table('property_invoices as pi')
                                        ->select('pi.*','u.name','p.name_of_property')
                                        ->join('users as u', 'u.id', '=', 'pi.user_id')
                                        ->join('properties as p', 'p.id', '=', 'pi.property_id')
                                        // ->where('pi.not_release_status', 2)
                                        ->where('pi.falk_status', 3)
                                        ->where('pi.deleted', 0);
                                        if($am->email != config('users.falk_email') ){
                                            $pending_invoice = $pending_invoice->whereRaw('(p.asset_m_id='.$am->id.' OR p.asset_m_id2='.$am->id.')');
                                        }
                                        $pending_invoice = $pending_invoice->get();

                                        // pre($pending_invoice);

                    // 2 NOT RELEASE INVOICE
                    $not_release_invoice = DB::table('property_invoices as pi')
                                        ->select('pi.*','u.name','p.name_of_property')
                                        ->join('users as u', 'u.id', '=', 'pi.user_id')
                                        ->join('properties as p', 'p.id', '=', 'pi.property_id')
                                        // ->where('pi.not_release_status', 1)
                                        ->where('pi.falk_status', 4)
                                        ->where('pi.deleted', 0);
                                        if($am->email != config('users.falk_email') ){
                                            $not_release_invoice = $not_release_invoice->whereRaw('(p.asset_m_id='.$am->id.' OR p.asset_m_id2='.$am->id.')');
                                        }
                                        $not_release_invoice = $not_release_invoice->get();
                }


                // 3 AUSLAUFENDE MIETVERTRÄGE
                $rental_agreements1 = TenancySchedule::selectRaw('users.name as am_name, tenancy_schedules.property_id, properties.name_of_property, tenancy_schedule_items.id, tenancy_schedule_items.name, tenancy_schedule_items.comment,tenancy_schedule_items.rent_end, tenancy_schedule_items.termination_by, tenancy_schedule_items.rental_space, tenancy_schedule_items.actual_net_rent, tenancy_schedule_items.type, (SELECT tsc.comment FROM tenancy_schedule_comments tsc WHERE tsc.item_id = tenancy_schedule_items.id ORDER BY tsc.id DESC LIMIT 1) as comment1')
                                                    ->leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')
                                                    ->join('properties', 'tenancy_schedules.property_id', '=', 'properties.id')
                                                    ->join('users', 'properties.asset_m_id', '=', 'users.id')
                                                    ->whereIn('type',[1,2])
                                                    ->where('properties.status',6)
                                                    ->where('tenancy_schedule_items.status', '=', 1)
                                                    ->where('rent_end', '>', date('Y-m-d'))
                                                    ->where('rent_end', '<', date('Y-m-d', strtotime('+2 months')));
                                                    // ->whereNotNull('tenancy_schedule_items.termination_by');
                                                    if($am->email != config('users.falk_email') ){
                                                        $rental_agreements1 = $rental_agreements1
                                                        ->whereRaw('(properties.asset_m_id='.$am->id.' OR properties.asset_m_id2='.$am->id.')');
                                                    }
                                                    $rental_agreements1 = $rental_agreements1->orderBy('tenancy_schedule_items.termination_by','desc')->get();
                
                // 4 ABGELAUFENE MIETVERTRÄGE
                $rental_agreements2 = TenancySchedule::selectRaw('users.name as am_name, tenancy_schedules.property_id, properties.name_of_property, tenancy_schedule_items.id, tenancy_schedule_items.name,tenancy_schedule_items.comment, tenancy_schedule_items.rent_end, tenancy_schedule_items.rental_space, tenancy_schedule_items.actual_net_rent, tenancy_schedule_items.type, (SELECT tsc.comment FROM tenancy_schedule_comments tsc WHERE tsc.item_id = tenancy_schedule_items.id ORDER BY tsc.id DESC LIMIT 1) as comment1')
                                                    ->leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')
                                                    ->join('properties', 'tenancy_schedules.property_id', '=', 'properties.id')
                                                    ->join('users', 'properties.asset_m_id', '=', 'users.id')
                                                    ->whereIn('type',[1,2])
                                                    ->where('properties.status',6)
                                                    ->where('tenancy_schedule_items.status', '=', 1)
                                                    ->where('rent_end', '<', date('Y-m-d'))
                                                    ->where('rent_end', '>', date('Y-m-d', strtotime('-6 months')));
                                                    if($am->email != config('users.falk_email') ){
                                                        $rental_agreements2 = $rental_agreements2
                                                        ->whereRaw('(properties.asset_m_id='.$am->id.' OR properties.asset_m_id2='.$am->id.')');
                                                    }
                                                    $rental_agreements2 = $rental_agreements2->orderBy('rent_end','asc')->get();

                // 5 AUSLAUFENDE GEBÄUDEVERSICHERUNGEN
                $building_insurance =  Properties::selectRaw('properties.id, name_of_property, axa, gebaude_comment, gebaude_betrag, gebaude_laufzeit_to, gebaude_kundigungsfrist, u.name as asset_name')
                                        ->leftJoin('users as u', 'u.id', '=', 'properties.asset_m_id')
                                        ->whereNotNull('gebaude_laufzeit_to')
                                        ->where('status',6)
                                        ->whereRaw('gebaude_laufzeit_to != "" and gebaude_laufzeit_to!=" "')
                                        ->where('gebaude_laufzeit_to', '>', date('Y-m-d'))
                                        ->where('gebaude_laufzeit_to', '<', date('Y-m-d', strtotime('+6 months')))
                                        ->where('gebaude_laufzeit_to','>=','2020-09-01');
                                        if($am->email != config('users.falk_email') ){
                                            $building_insurance = $building_insurance
                                                        ->whereRaw('(asset_m_id='.$am->id.' OR asset_m_id2='.$am->id.')');
                                        }
                                        $building_insurance = $building_insurance->orderBy('gebaude_laufzeit_to','asc')->get();

                //6 AUSLAUFENDE HAFTPFLICHTVERSICHERUNGEN
                $liability_insurance = Properties::selectRaw('properties.id, name_of_property, allianz, haftplicht_comment, haftplicht_betrag, haftplicht_laufzeit_to, haftplicht_kundigungsfrist, u.name as asset_name')
                                        ->leftJoin('users as u', 'u.id', '=', 'properties.asset_m_id')
                                        ->whereNotNull('haftplicht_laufzeit_to')
                                        ->where('status',6)
                                        ->whereRaw('haftplicht_laufzeit_to != "" and haftplicht_laufzeit_to!=" "')
                                        ->where('haftplicht_laufzeit_to', '>', date('Y-m-d'))
                                        ->where('haftplicht_laufzeit_to', '<', date('Y-m-d', strtotime('+6 months')))
                                        ->where('haftplicht_laufzeit_to','>=','2020-09-01');
                    
                                        if($am->email != config('users.falk_email') ){
                                            $liability_insurance = $liability_insurance
                                                        ->whereRaw('(asset_m_id='.$am->id.' OR asset_m_id2='.$am->id.')');
                                        }
                                        $liability_insurance = $liability_insurance->orderBy('haftplicht_laufzeit_to','asc')->get();

                //7) Leerstände (€)
                $vacancies = TenancySchedule::selectRaw('calculated_vacancy_in_eur,tenancy_schedule_items.id,pu.name as creator_name, tenancy_schedules.property_id, properties.name_of_property as object_name, tenancy_schedule_items.name, tenancy_schedule_items.vacancy_in_qm, tenancy_schedule_items.vacancy_in_eur, (SELECT comment FROM tenancy_schedule_comments tsc WHERE tsc.item_id = tenancy_schedule_items.id AND tsc.comment != "" ORDER BY tsc.id DESC LIMIT 1) as comment, tenancy_schedule_items.comment as comment1, (CASE WHEN tenancy_schedule_items.type = "3" THEN "Wohnen" WHEN tenancy_schedule_items.type = "4" THEN "Gewerbe" ELSE tenancy_schedule_items.type END) as type')
                        ->leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')
                        ->join('properties', function($join){
                                $join->on('properties.id', '=', 'tenancy_schedules.property_id')
                                ->where('properties.Ist', 0)
                                ->where('properties.soll', 0)
                                ->where('properties.masterliste_id', '!=', '')
                                ->whereNotNull('properties.masterliste_id');
                        })
                        ->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
                        ->join('users AS mu', 'mu.id', '=', 'masterliste.asset_manager')
                        ->leftjoin('users AS pu', 'pu.id', '=', 'properties.asset_m_id')
                        ->whereRaw('(type = 3 OR type = 4)');
                        if($am->email != config('users.falk_email') ){
                            $vacancies = $vacancies->whereRaw('(properties.asset_m_id='.$am->id.' OR properties.asset_m_id2='.$am->id.')');
                        }
                        $vacancies = $vacancies->get();

                //8) AUSLAUFENDE VERTRÄGE
                $release_contract  = DB::table('property_contracts as pc')
                                    ->select('pc.*','u.name','p.name_of_property')
                                    ->join('users as u', 'u.id', '=', 'pc.user_id')
                                    ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pc.id')
                                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                                    ->where('pml.type', 'contract_release')
                                    ->where('pml.tab', 'contracts');
                                    if($am->email != config('users.falk_email') ){
                                        $release_contract = $release_contract->whereRaw('(p.asset_m_id='.$am->id.' OR p.asset_m_id2='.$am->id.')');
                                    }
                                    $release_contract = $release_contract->orderBy('termination_date','desc')->groupby('pc.id')->get();
                
                /*$building_insurance_html = \View::make('emails.expiry_overview', array('building_insurance' => $building_insurance, 'liability_insurance' => $liability_insurance, 'rental_agreements1' => $rental_agreements1, 'rental_agreements2' => $rental_agreements2, 'vacancies' => $vacancies, 'pending_invoice' => $pending_invoice, 'not_release_invoice' => $not_release_invoice, 'release_contract' => $release_contract))->render();
                echo $building_insurance_html;exit;*/
                
                $email = $am->email;
                $name = $am->name;
                $subject = 'Übersicht auslaufende MV, Versicherungen und Verträge';
                // $email = "yiicakephp@gmail.com";
                // $email = "c.wiedemann@fcr-immobilien.de";
                if( true || count($rental_agreements1) > 0 || count($rental_agreements2) > 0 || count($building_insurance) > 0 || count($liability_insurance) > 0 || count($vacancies) > 0 || count($pending_invoice) > 0 || count($not_release_invoice) > 0 || count($release_contract) > 0){

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.expiry_overview', ['building_insurance' => $building_insurance, 'liability_insurance' => $liability_insurance, 'rental_agreements1' => $rental_agreements1, 'rental_agreements2' => $rental_agreements2, 'vacancies' => $vacancies, 'pending_invoice' => $pending_invoice, 'not_release_invoice' => $not_release_invoice, 'release_contract' => $release_contract], function ($m) use ($email, $name, $subject) {

                            $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                            $m->to($email, $name)->subject($subject);
                            
                        });
                    }
                    
                }
            }
            echo 'Mail Send successfully.';
        }else{
            echo 'Assets manager not found!';
        }





        $asset_manager = DB::table('users')->select('id', 'name', 'email')->whereRaw('email="l.liebscher@fcr-immobilien.de"')->get();

        if(!empty($asset_manager)){
            foreach ($asset_manager as $am) {

                $rental_agreements2 = $rental_agreements1 = $not_release_invoice = $pending_invoice = array();
                // 5 AUSLAUFENDE GEBÄUDEVERSICHERUNGEN
                $building_insurance =  Properties::selectRaw('properties.id, name_of_property, axa, gebaude_comment, gebaude_betrag, gebaude_laufzeit_to, gebaude_kundigungsfrist, u.name as asset_name')
                                        ->leftJoin('users as u', 'u.id', '=', 'properties.asset_m_id')
                                        ->whereNotNull('gebaude_laufzeit_to')
                                        ->where('status',6)
                                        ->whereRaw('gebaude_laufzeit_to != "" and gebaude_laufzeit_to!=" "')
                                        ->where('gebaude_laufzeit_to', '>', date('Y-m-d'))
                                        ->where('gebaude_laufzeit_to', '<', date('Y-m-d', strtotime('+6 months')))
                                        ->where('gebaude_laufzeit_to','>=','2020-09-01');
                                        $building_insurance = $building_insurance->orderBy('gebaude_laufzeit_to','asc')->get();

                //6 AUSLAUFENDE HAFTPFLICHTVERSICHERUNGEN
                $liability_insurance = Properties::selectRaw('properties.id, name_of_property, allianz, haftplicht_comment, haftplicht_betrag, haftplicht_laufzeit_to, haftplicht_kundigungsfrist, u.name as asset_name')
                                        ->leftJoin('users as u', 'u.id', '=', 'properties.asset_m_id')
                                        ->whereNotNull('haftplicht_laufzeit_to')
                                        ->where('status',6)
                                        ->whereRaw('haftplicht_laufzeit_to != "" and haftplicht_laufzeit_to!=" "')
                                        ->where('haftplicht_laufzeit_to', '>', date('Y-m-d'))
                                        ->where('haftplicht_laufzeit_to', '<', date('Y-m-d', strtotime('+6 months')))
                                        ->where('haftplicht_laufzeit_to','>=','2020-09-01');
                                        $liability_insurance = $liability_insurance->orderBy('haftplicht_laufzeit_to','asc')->get();

                //7) Leerstände (€)
                $vacancies = array();

                // pre($vacancies);
                /*$building_insurance_html = \View::make('emails.expiry_overview', array('building_insurance' => $building_insurance, 'liability_insurance' => $liability_insurance, 'rental_agreements1' => $rental_agreements1, 'rental_agreements2' => $rental_agreements2, 'vacancies' => $vacancies, 'pending_invoice' => $pending_invoice, 'not_release_invoice' => $not_release_invoice))->render();
                echo $building_insurance_html;exit;*/
                
                $email = $am->email;
                $name = $am->name;
                $subject = 'Übersicht auslaufende MV, Versicherungen und Verträge';
                // $email = "yiicakephp@gmail.com";
                // $email = "c.wiedemann@fcr-immobilien.de";
                if(true ||  count($rental_agreements1) > 0 || count($rental_agreements2) > 0 || count($building_insurance) > 0 || count($liability_insurance) > 0 || count($vacancies) > 0 || count($pending_invoice) > 0 || count($not_release_invoice) > 0){

                    if( !in_array($email, $this->mail_not_send()) ){
                        Mail::send('emails.expiry_overview', ['building_insurance' => $building_insurance, 'liability_insurance' => $liability_insurance, 'rental_agreements1' => $rental_agreements1, 'rental_agreements2' => $rental_agreements2, 'vacancies' => $vacancies, 'pending_invoice' => $pending_invoice, 'not_release_invoice' => $not_release_invoice], function ($m) use ($email, $name, $subject) {

                            $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                            $m->to($email, $name)->subject($subject);
                            
                        });
                    }
                    
                }
            }
            echo 'Mail Send successfully.';
        }else{
            echo 'Assets manager not found!';
        }
        // $this->update_wault();



    }


    public function update_wault()
    {
        $b = Properties::where('main_property_id','!=',0)->get();
        // pre($b); die;
        foreach ($b as $key => $properties) {

             $propertiesExtra1s = PropertiesTenants::where('propertyId', $properties->id)->get();


            $total_ccc = 0;
                    $ankdate1 = $ankdate2  = "";
                    foreach($propertiesExtra1s as $k=>$propertiesExtra1):
                            if($propertiesExtra1->is_current_net)
                            $total_ccc += $propertiesExtra1->net_rent_p_a; 

                            if($k==0)
                            $ankdate1 = $propertiesExtra1->mv_end;
                            if($k==1)
                            $ankdate2 = $propertiesExtra1->mv_end;



                            endforeach;
                    if($total_ccc){
                        $properties->net_rent_pa = $total_ccc;
                    }
                    else{
                        $total_ccc = $properties->net_rent_pa;
                    }
            $Q53 = 0;
            $sm = 0;
                            foreach($propertiesExtra1s as $propertiesExtra1):
                                
                                $sm += $propertiesExtra1->net_rent_p_a;
                                
                                    if($propertiesExtra1->is_dynamic_date)
                                        $propertiesExtra1->mv_end2 = date('d.m.Y');



                                    if( strpos($properties->duration_from_O38, "unbefristet") !== false){
                                        $value = 1;
                                    }else{
                                        $properties->duration_from_O38 = $propertiesExtra1->mv_end2;

                                        $date1=date_create(str_replace('/', '-', $propertiesExtra1->mv_end));
                                        $date2=date_create(str_replace('/', '-', $propertiesExtra1->mv_end2));

                                        $diff=date_diff($date1,$date2);

                                        $value =  $diff->format("%a")/ 365;

                                    }

                                    if (strpos($propertiesExtra1->mv_end, '2099') !== false)
                                        $value = 0.5;

                                    $Q53 += $value*$propertiesExtra1->net_rent_p_a;
                                endforeach;

                                 $P53 = ($properties->net_rent_pa == 0) ? 0 : ($Q53 / $properties->net_rent_pa) ;

                                $ist_soll_properties = Properties::where('id', $properties->id)->update(['wault' => $P53]);
                            // die





        }
    }

    public function storeLiquiplanung(){
       $data = app('App\Http\Controllers\HomeController')->liquiplanung();
       $jsonData = json_encode($data);

       $modal = new PropertiesCustomField;
       $modal->slug = 'liquiplanung_data';
       $modal->content = $jsonData;
       if($modal->save()){
            echo 'success';
       }else{
            echo 'fail';
       }
       exit;
    }
}