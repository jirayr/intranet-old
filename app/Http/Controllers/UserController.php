<?php

namespace App\Http\Controllers;

use App\Models\Properties;
use App\PropertyPermission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Validator;

class UserController extends Controller
{
    protected $propertyPermission;

    public function __construct(PropertyPermission $propertyPermission)
    {
        $this->propertyPermission = $propertyPermission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->isAdmin()){
            return redirect("/");
        }

    	// $perPages = 25;
    	// $users = User::where('user_deleted', 0)->orderBy('id','ASC')->paginate($perPages);
        $users = User::where('user_deleted', 0)->orderBy('id','ASC')->get();

        return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Auth::user()->isAdmin()){
            return redirect("/");
        }
		return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$input = $request->all();
		$email = $input['email'];
		$find_email = User::where('email','=',$email)->first();

		//check email before create
		if(isset($find_email)){
			if($find_email->count()>0){
				return redirect()->action('UserController@index')
					->with('error', trans('user.user_controller.email_already'));
			}
		}

        $signature = "";
        if ($request->hasFile('signature')) {
            $image = $request->file('signature');
            $signature = uniqid().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('signature/');
            $image->move($destinationPath, $signature);
        }

        $input['signature'] = $signature;

        $input['user_status'] = ($request->has('user_status')) ? $request->user_status : 1;

        if ($user = UserService::create($input)) {

            if (isset($request->property_id )){
                foreach ($request->property_id as $pId)
                {
                    $this->propertyPermission->create([
                        'property_id' => $pId,
                        'user_id' => $user->id
                    ]);
                }

            }


			return redirect()->action('UserController@index')
				->with('message', trans('user.user_controller.created_success'));
		}

		return redirect()->action('UserController@index')
			->with('error', trans('user.user_controller.created_fail'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Auth::user()->isAdmin()){
            return redirect("/");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!Auth::user()->isAdmin()){
            return redirect("/");
        }
    	$user = User::FindOrFail($id);

		return view('user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

		$email = $input['email'];
		$find_email = User::where('email','=',$email)
							->where('id','<>',$id)->first();

		//check email before edit
		if(isset($find_email)){
			if($find_email->count()>0){
				return redirect()->action('UserController@index')
					->with('error', trans('user.user_controller.email_already'));
			}
		}

		$user_update = User::where('id','=',$id)->first();

        $signature = $user_update->signature;
        if ($request->hasFile('signature')) {
            $image = $request->file('signature');
            $signature = uniqid().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('signature/');
            $image->move($destinationPath, $signature);
        }

        if($request->has('user_status')){
            $input['user_status'] = $request->user_status;
        }else{
            $input['user_status'] = $user_update->user_status;
        }

        $input['signature'] = $signature;

		if (UserService::update($input, $user_update)) {
            $this->propertyPermission->where('user_id',$id)->delete();
		   if(isset($request->property_id)){
               foreach ($request->property_id as $pId)
               {
                   $this->propertyPermission->create([
                       'property_id' => $pId,
                       'user_id' => $id
                   ]);
               }

           }

			return redirect()->action('UserController@index')
				->with('message', trans('user.user_controller.updated_success'));
		}
		return redirect()->action('UserController@index')
			->with('error', trans('user.user_controller.updated_fail'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$user = User::where('id','=',$id)->first();
		if(isset($user)){
			if (UserService::delete($user)) {
				return back()->with('message', trans('user.user_controller.deleted_success'));
			}
		}
		return back()->with('error', trans('user.user_controller.deleted_fail'));
    }

    public function getProfile($id){
        if ($id != Auth::user()->id)
            return redirect('/');

    	$user = User::where('id','=',$id)->first();
		$url_image_profile = "/user-images/".$user->image;
		return view('user.edit-profile',compact('id','user','url_image_profile'));
	}

	public function postProfile(Request $request, $id)
	{
		$input = $request->all();
		$user = User::where('id','=',$id)->first();
		$email = $input['email'];
		$find_email = User::where('email','=',$email)
			->where('id','<>',$id)->first();

		//check email before edit
		if(isset($find_email)){
			if($find_email->count()>0){
				return redirect()->action('UserController@getProfile',$id)
					->with('error', trans('user.user_controller.email_already'));
			}
		}

		if(isset(request()->image)){
			if($user->image !=''){//remove only has a picture before
				$file_path_old = public_path('user-images').'/'.$user->image;
				if (file_exists($file_path_old)){
                    unlink($file_path_old);
                }
			}
			//save image
			$imageName = $id.".".request()->image->getClientOriginalExtension();
			request()->image->move(public_path('user-images'), $imageName);
			$input['image'] = $imageName;
		}
		else
		{
			//when user no update profile picture
			$input['image'] = $user->image;
		}

		$date=date_create($input['birthday']);
		$input['birthday'] = date_format($date,"Y/m/d");
		if (UserService::updateProfile($input, $user)) {

			return redirect()->route('edit-profile', ["id"=>$id])
				->with('message', trans('user.user_controller.updated_success'));
		}

		return redirect()->route('edit-profile', ["id"=>$id])
			->with('error', trans('user.user_controller.updated_fail'));

	}

	public function getChangePassword(){
        return view('user.change-password');
    }

    public function postChangePassword(Request $request){
        $user = User::findOrFail(Auth::user()->id);
        $input = $request->all();
        if (password_verify($input['current_password'], $user->password)){
            if ($input['new_password1'] === $input['new_password2']){
                $user->update([
                    'password' => Hash::make($input['new_password1']),
                    'password_change_date' => date('Y-m-d H:i:s'),
                ]);
                return redirect()->route('change-password')
                    ->with('message', trans('user.user_controller.password_changed_success'));
            }
            else {
                return redirect()->route('change-password')
                    ->with('error', trans('user.user_controller.not_matched'));
            }
        }
        else {
            return redirect()->route('change-password')
                ->with('error', trans('user.user_controller.not_true'));
        }

    }

    public function showUserActivities(){
        $current_date = date("Y-m-d");
        if (!isset($_GET['type'])){
            $_GET['type'] = 'day';
        }
        $perPages = 25;
        $users = User::orderBy('id','ASC')->paginate($perPages);
        foreach ($users as $user){
            if ($_GET['type'] == 'day'){
                $filter = date('d', strtotime($current_date));
                $user->number_of_properties = count(Properties::where('user_id', $user->id)
                    ->whereDay('duration_from', $filter)
                    ->where('Ist',0)->where('soll',0)
                    ->get());
            }
            else if($_GET['type'] == 'week'){
                $user->number_of_properties = count(Properties::where('user_id', $user->id)->where('Ist',0)->where('soll',0)
                    ->whereBetween('duration_from', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])
                    ->get());
            }
            else { //type == month
                $filter = date('m', strtotime($current_date));
                $user->number_of_properties = count(Properties::where('user_id', $user->id)->where('Ist',0)->where('soll',0)
                    ->whereMonth('duration_from', $filter)
                    ->get());
            }

        }

        foreach ($users as $user) {
            $user_id = $user->id;
            $user->_properties_declined = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.declined')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_offer = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.offer')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_duration = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.duration')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_in_purchase = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.in_purchase')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_sold = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.sold')]])->where('Ist',0)->where('soll',0)->get();
            $user->_properties_lost = Properties::where([['user_id','=',$user_id],['status','=',config('properties.status.lost')]])->where('Ist',0)->where('soll',0)->get();
        }

//        duration_from
        return view('user.user-activities', compact('users'));
    }


    function api_contacts(){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/contact?oauth_consumer_key=08211746Key&oauth_token=4822392b-7eeb-42a2-9a40-36b478698a0a&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1553782181&oauth_nonce=TP9C4k&oauth_version=1.0&oauth_signature=lisiKX1ETzuP147owHUEq%2FHpK7o%3D",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/xml"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return redirect()->back()->with(['error' => 'ups something went wrong!']);
        } else {


            echo '<pre>';
            $xml = new \SimpleXMLElement($response);
            $response = json_encode($xml, JSON_PRETTY_PRINT);
            print_r(json_decode($response, TRUE));
            echo '</pre>';

        }



    }



    public function working(){


        $properties = DB::table('properties')->select('id', 'soll', 'Ist', 'created_at', 'bank_ids')
            ->where('maintenance_increase_year2', '=', 0)
            ->where('Ist', '!=', 0)
            ->get();

        echo '<pre>';
        echo count($properties);
        echo '</br>';
        print_r($properties);
        echo '</pre>';

        echo '<br/>==================================================================';

        $properties = DB::table('properties')->select('id', 'soll', 'Ist', 'created_at', 'bank_ids')
            ->where('maintenance_increase_year2', '=', 0)
            ->where('Ist', '=', 0)
            ->where('soll', '=', 0)

            ->get();

        echo '<pre>';
        echo count($properties);
        echo '</br>';
        print_r($properties);
        echo '</pre>';

    }

    /*
     *
     *
     * */
    function delete_user(){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/contact/95100526?oauth_consumer_key=08211746Key&oauth_token=4822392b-7eeb-42a2-9a40-36b478698a0a&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1553787354&oauth_nonce=xzmkpi&oauth_version=1.0&oauth_signature=dBbQMwhZgBFRmSi8TYwADHxyags%3D",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/xml",
                "postman-token: 44e0ade9-814f-a17a-7798-9fdd953b8ce9"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return redirect()->back()->with(['error' => 'ups something went wrong!']);
        } else {


            echo '<pre>';
            $xml = new \SimpleXMLElement($response);
            $response = json_encode($xml, JSON_PRETTY_PRINT);
            print_r(json_decode($response, TRUE));
            echo '</pre>';

        }


    }


    public function searchUserByName(Request $request)
    {
        return User::where("name","LIKE","%{$request['query']['term']}%")->get();
    }

    public function loginHistory(){
        $current_user = Auth::user();
        if(strtolower($current_user->email)==config('users.falk_email') || strtolower($current_user->email)=="c.wiedemann@fcr-immobilien.de" || strtolower($current_user->email)=="l.schaut@fcr-immobilien.de" || strtolower($current_user->email)=="m.heinrich@fcr-immobilien.de" || strtolower($current_user->email)=="a.raudies@fcr-immobilien.de" || strtolower($current_user->email)=="yiicakephp@gmail.com"){
            return view('user.login-history');
        }else{
            return redirect("/");
        }
    }
    public function loginHistoryData(Request $request){

            
        if($request->user_id)
        {
            $log = DB::table('login_logs as lg')
                ->selectRaw('lg.id, lg.ip_address, lg.agent, lg.created_at, u.name,u.id as user_id')
                ->join('users as u', 'u.id', '=', 'lg.user_id')
                ->where('lg.user_id',$request->user_id);
        }
        else{
            $log = DB::table('users as u')
            ->selectRaw('lg.id, lg.ip_address, lg.agent, lg.created_at, u.name,u.id as user_id')
            ->leftJoin('login_logs as lg', function($join){
                $join->on('u.id', '=', 'lg.user_id')
                ->whereRaw('lg.id IN (select MAX(a2.id) from login_logs as a2 group by a2.user_id)');
            });
        }

            
         return datatables()::of($log)
            ->addIndexColumn()
            ->editColumn('name', function($row) {
                return '<a href="'. route('login_history')."?user_id=".$row->user_id.'">'.$row->name.'</a>';
            })
            ->editColumn('created_at', function($row) {
                return show_datetime_format($row->created_at);
            })
            ->filterColumn('lg.created_at', function($query, $keyword) {
                if((bool)strtotime($keyword)){
                    $query->whereRaw("DATE_FORMAT(lg.created_at, '%d.%m.%Y %H:%i:%s') LIKE '%{$keyword}%'");
                }
            })
            ->escapeColumns([])
            ->make(true);
        
    }

    public function exportHistoryData(Request $request){
        $log = DB::table('export_histories as eh')
                ->selectRaw('eh.id, eh.user_id, eh.section_type, eh.file_name, eh.created_at, u.name')
                ->join('users as u', 'u.id', '=', 'eh.user_id');
                if($request->user_id){
                    $log = $log->where('user_id', $request->user_id);
                }

                return datatables()::of($log)
                ->addIndexColumn()
                ->editColumn('name', function($row) {
                                    return '<a href="'. route('login_history')."?export_user_id=".$row->user_id.'#export-log-table">'.$row->name.'</a>';
                })
                ->editColumn('created_at', function($row) {
                    return show_datetime_format($row->created_at);
                })
                ->editColumn('file_name', function($row) {
                    $path = asset('export/'.$row->file_name);
                    return ($row->file_name) ? '<a target="_blank" href="'. $path .'">'.$row->file_name.'</a>' : '';
                })
                ->filterColumn('eh.created_at', function($query, $keyword) {
                    if((bool)strtotime($keyword)){
                        $query->whereRaw("DATE_FORMAT(eh.created_at, '%d.%m.%Y %H:%i:%s') LIKE '%{$keyword}%'");
                    }
                })
                ->escapeColumns([])
                ->make(true);
    }

    public function propertyHistory(){

        $current_user = Auth::user();
        $allow_email = [config('users.falk_email'), 'c.wiedemann@fcr-immobilien.de', 'l.schaut@fcr-immobilien.de', 'm.heinrich@fcr-immobilien.de', 'a.raudies@fcr-immobilien.de', 'yiicakephp@gmail.com'];

        if(in_array(strtolower($current_user->email), $allow_email)){
            return view('user.property-history');
        }else{
            return redirect("/");
        }
    }

    public function propertyHistoryData(Request $request){

            
        $history = DB::table('property_histories as ph')
                            ->selectRaw('ph.id, ph.old_value, ph.new_value, ph.user_id, ph.property_id, ph.created_at, u.name, p.name_of_property')
                            ->join('properties as p', 'p.id', '=', 'ph.property_id')
                            ->join('users as u', 'u.id', '=', 'ph.user_id')
                            ->where('ph.field_type', 'status');
                            if($request->status_user_id){
                                $history = $history->where('ph.user_id', $request->status_user_id);
                            }
        
         return datatables()::of($history)
            ->addIndexColumn()
            ->editColumn('name', function($row) {
                return '<a href="'. route('login_history')."?status_user_id=".$row->user_id.'">'.$row->name.'</a>';
            })
            ->editColumn('name_of_property', function($row) {
                return '<a href="'. route('properties.show',['property' => $row->property_id]) .'">'.$row->name_of_property.'</a>';
            })
            ->editColumn('created_at', function($row) {
                return show_datetime_format($row->created_at);
            })
            ->editColumn('old_value', function($row) {
                return get_property_status($row->old_value);
            })
            ->editColumn('new_value', function($row) {
                return get_property_status($row->new_value);
            })
            ->filterColumn('ph.created_at', function($query, $keyword) {
                if((bool)strtotime($keyword)){
                    $query->whereRaw("DATE_FORMAT(ph.created_at, '%d.%m.%Y %H:%i:%s') LIKE '%{$keyword}%'");
                }
            })
            ->filterColumn('ph.old_value', function($query, $keyword) use ($request) {
                $status = get_property_status_value($request->search['value']);
                if($status){
                    $query->where('ph.old_value', $status);
                }
            })
            ->filterColumn('ph.new_value', function($query, $keyword) use ($request) {
                $status = get_property_status_value($request->search['value']);
                if($status){
                    $query->where('ph.new_value', $status);
                }
            })
            ->escapeColumns([])
            ->make(true);
        
    }

    public function confirmPassword(Request $request){
        if($request->ajax()) {
            $validator = Validator::make($request->all(), [
                "password"    => "required",
            ]);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{
                $user = Auth::user();
                $check = false;

                if($request->check_login_password == 1){
                    $check = password_verify($request->password, $user->password);
                }else{
                    $check = ($request->password == "FCR2020$!") ? true : false;
                }

                if ($check) {
                    $result = ['status' => true, 'message' => 'Password match successfully.', 'data' => $user];
                }else{
                    $result = ['status' => false, 'message' => 'Invalid password!', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function gtAllUsers(Request $request){
        $user = User::select('id', 'email', 'name')->where('user_deleted', 0)->where('user_status', 1)->get();
        if($user && count($user)){
            $response = ['status' => true, 'message' => 'User found.', 'data' => $user];
        }else{
            $response = ['status' => true, 'message' => 'User not found!', 'data' => []];
        }
        return response()->json($response);
    }
}
