<?php

namespace App\Http\Controllers;

use App\Address;
use App\Company;
use App\CompanyEmployee;
use App\Models\Email;
use App\Repositories\EmailMappingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyController extends Controller
{
    protected $company;
    protected $address;
    protected $employee;
    protected $emailMappingRepository;
    public function __construct(Company $company, Address $address, CompanyEmployee $employee, EmailMappingRepository $emailMappingRepository)
    {
        $this->company = $company;
        $this->address = $address;
        $this->employee = $employee;
        $this->emailMappingRepository = $emailMappingRepository;
    }

    public function index()
    {
        $companies = $this->company->with('companyAddress')->paginate(100);
        $keywords = $this->employee->take(10)->pluck('keyword');
        return view('company.index',compact('companies','keywords'));
    }

    public function create()
    {
        return view('company.add');
    }

    public function store(Request $request)
    {
       $company = $this->company->create($request->all());
       $this->address->create([
           'company_id' => $company->id,
           'zip' => $request->zip,
           'street'=> $request->street,
           'city'=> $request->city,
           'part_of_city'=>$request->part_of_city
       ]);

       return redirect(route('company'));
    }

    public function edit($id)
    {
       $company = $this->company->find($id)->with('companyAddress')->first();
       return view('company.edit',compact('company'));
    }

    public function update(Request $request ,$id)
    {
        $this->company->find($id)->update($request->all());
        $this->address->where('company_id',$id)->update([
            'company_id' => $request->id,
            'zip' => $request->zip,
            'street'=> $request->street,
            'city'=> $request->city,
            'part_of_city'=>$request->part_of_city
        ]);

        return redirect(route('company'));
    }

    public function delete($id)
    {
        $this->company->find($id)->delete();
        $this->address->where('company_id',$id)->delete();
        return redirect(route('company'));
    }

    public function SearchEmployee(Request $request)
    {
        $data = [];
        $companies = '';
        $employees = '';
        foreach ($request['key'] as $val){
             $requestData = (int)$val;
              if ($requestData != 0)
              {
                 $data = $this->employee->whereIn('id',$request['key'])->get()->pluck('keyword');
              }
               else{
                    $data = $request['key'];
                }
            $employees =  $this->employee
                ->Where(function ($query) use($data) {
                     for ($i = 0; $i < count($data); $i++){
                        $query->orWhere('keyword', 'LIKE',  '%' . $data[$i] .'%');
                        $query ->orWhere('vorname','LIKE', '%' .$data[$i] . '%');
                        $query ->orWhere('nachname','LIKE', '%' .$data[$i] . '%');
                    }
                    })->paginate(100);

//            if ($employees){
//                $companies = $this->company->whereIn('id',$employees)->with('companyAddress')->paginate(100);
//            }

        }

        return view('company_employees.search',compact('employees'));
    }

    public function autocomplete(Request $request)
    {
           $data = $this->employee
            ->where("keyword","LIKE","%{$request['query']['term']}%")
            ->groupBy('keyword')
            ->get();

        return $data;
    }

    public function getEmails($companyId,$emailType)
    {

       $employeeEmails = $this->employee->where('company_id',$companyId)->whereNotNull('email')->select('email')->first();
       $domain =  substr($employeeEmails->email,strrpos($employeeEmails->email, '@')+1);
//       $emails = $this->emailMappingRepository->getEmailsByDomainAndEmailType($domain,$emailType);
       $data = Email::where('email_sent_from','LIKE','%'.$domain.'%')->with(['emailMapping'=> function($query)use($emailType) {
           $query->where('email_type_id',$emailType)->get();}
       ])->orderBy('date_of_email','DESC')->paginate(10);

       return view('company.emails',compact('data','emailType'));
     }


    public function autocompleteByName(Request $request)
    {
        $data = $this->employee
            ->where("vorname","LIKE","%{$request['query']['term']}%")
            ->orWhere("nachname","LIKE","%{$request['query']['term']}%")
            ->with(['company'])
            ->get();

        return $data;
    }


}
