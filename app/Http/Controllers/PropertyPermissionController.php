<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PropertyPermission;
use Validator;

class PropertyPermissionController extends Controller
{
    public function index(){
    	$users = User::select('id', 'name')->get();
        return view('property_permissions.index', ['users' => $users]);
    }

    public function store(Request $request){

    	$rules =  [
	      'user_id' => ['required', 'numeric'],
	      'property_id' => ['required', 'numeric'],
	    ];

	    $validator = Validator::make($request->all(), $rules);

	    if($validator->fails()){
            return redirect()->back()->with('error', $validator->errors()->first())->withInput();;
        }else{

        	$check = PropertyPermission::where('user_id', $request->user_id)->where('property_id', $request->property_id)->first();
        	if($check){
        		return redirect()->back()->with('error', 'Already assign this property id to this user!')->withInput();;
        	}else{
	        	$modal = new PropertyPermission;
	        	$modal->user_id = $request->user_id;
	        	$modal->property_id = $request->property_id;

	        	if($modal->save()){
	        		return redirect()->back()->with('success', 'Permission added successfully.');
	        	}else{
	        		return redirect()->back()->with('error', 'Permission added fail!')->withInput();;
	        	}
	        }
        }
    }
}
