<?php

namespace App\Http\Controllers;

use App\AddressesJob;
use App\BankAccountUpload;
use App\Exports\ExcelDataListView;
use App\Exports\ObjectDataListView;
use App\Imports\BankDataImport;
use App\Imports\BankFinancingOfferExport;
use App\Imports\ExcelImportMieterliste;
use App\Models\BankFinancingOffer;
use App\Models\Forecast;
use App\Models\Attachment;
use App\Models\ForecastKind;
use App\Models\Properties;
use App\Models\Budget;
use App\Models\Provision;
use App\email_template2;
use App\Models\Masterliste;
use App\Models\Banks;
use App\Models\PropertiesBanken;
use App\Models\TenancySchedule;
use App\Models\TenancyScheduleItemFile;
use App\Models\Tenants;
use App\Models\PropertyVacant;
use App\ExportAd;
use App\Models\PropertiesExtra1;
use App\Models\PropertyManagement;
use App\Models\PropertiesExtra2;
use App\Models\Comment;
use App\Models\BankingRequest;
use App\Models\StatusLoi;
use App\Models\LeasingActivity;
use App\Models\Empfehlung;
use App\Models\PropertiesCustomField;
use App\Models\EmpfehlungDetail;
use App\Services\MasterlisteService;
use App\Services\NotificationsService;
use App\Services\OutlookHTTPService;
use App\Services\PropertyProjectService;
use App\Services\TenancySchedulesService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use PDF;
use Illuminate\Http\Request;
use App\User;
use App\State;
use App\City;
use App\Services\UserService;
use Auth;
use App\Services\PropertiesService;
use App\Services\BanksService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Conditional;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Symfony\Component\HttpKernel\Profiler\Profile;
use App\Models\TenancyScheduleItem;
use App\Models\PropertyInsurance;
use App\Models\PropertyInvestation;
use App\Models\PropertyMaintenance;
use App\Models\PropertiesSaleDetail;
use App\Models\PropertiesSale;
use App\Models\TenancyExtension;
use App\Models\PropertiesBuyDetail;
use App\Models\PropertiesBuy;
use App\Models\PropertiesMailLog;
use App\Models\PropertiesTenants;
use App\Models\PropertySubfields;
use App\Models\PropertyServiceProvider;
use Validator;
use Illuminate\Support\Facades\File;
//use PDF;
use App\Models\PropertyComments;
use App\Ad;
use App\RateCity;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use App\email_template2_sending_info;
use App\Verkauf_tab;
use App\RentPaidExcelUpload;
use App\Models\UserFormData;
use App\Models\VacantSpace;
use App\Mail\UserFormEmail;
use App\Models\PropertyLoansMirror;
use App\Models\PropertiesDirectory;
use App\Models\PropertyEinkaufFile;
use Illuminate\Support\Facades\Storage;
use App\Helpers\PropertiesHelperClass;
use App\Models\GdriveUploadFiles;
use App\Models\SendMailToAm;
use App\Company;
use App\CompanyEmployee;
use App\Models\PropertyInvoices;
use App\Models\PropertiesDefaultPayers;
use App\Models\PropertiesDeals;
use App\Models\BankenContactEmployee;
use App\Models\PropertyContracts;
use App\Models\PropertyInsuranceTab;
use App\Models\PropertyInsuranceTabDetails;
use App\Models\PropertyRentPaids;
use App\Models\PropertyRentPaidDetails;
use App\Models\VacantUploadingStatus;
use App\Models\EmpfehlungComments;
use App\Models\ProvisionComments;
use App\Models\TenantPayment;
use App\Models\TenantMaster;
use App\Models\PropertiesComment;
use App\Models\PropertiesLoanMirrors;
use App\Models\LiquiplanungAoCosts;
use App\Models\PropertyHistory;
use Carbon\Carbon;


ini_set('memory_limit','-1');


class PropertiesController extends Controller {

    protected $outlookHttpService;
    protected $propertyProjectService;
    public function __construct(OutlookHTTPService $outlookHTTPService,
                                 PropertyProjectService $propertyProjectService)
    {
        $this->outlookHttpService = $outlookHTTPService;
        $this->middleware('userAccessOnProperty')->only(['show']);
        $this->propertyProjectService = $propertyProjectService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete_comment_file(Request $request)
    {
        $modal = Comment::where('type',$request->id)->where('property_id',$request->property_id)->first();
        if($modal && $modal->files)
        {
            $f = json_decode($modal->files,true);

            $dir = 'pdf_upload/';
            if(file_exists($dir.$request->file))
                unlink($dir.$request->file);

            foreach ($f as $key => $value) {
                if($value==$request->file)
                {
                    unset($f[$key]);
                }
            }
           $f =  array_values($f);
           $modal->files = json_encode($f);
           $modal->save();
        }
    }
    public function addmaillog(Request $request)
    {
        $record_id = NULL;
        // die;
        if($request->step=="management")
        {
            $update_data = DB::table('property_managements')->where('property_id', $request->property_id)->update(['not_release' => 0]);
        }
        if($request->step=="release_management")
        {
            $property = PropertyManagement::find($request->property_id);
            $property->not_release = 0;
            $property->save();

            $record_id = $request->property_id;
            $request->property_id  = $property->property_id;
        }
        $id = $request->property_id;
        if($request->step)
        {
            $url = route('properties.show',['property'=>$id]).'?tab=insurance_tab';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $p = Properties::find($id);
            $user = Auth::user();
            $name = $user->name;
            $replyemail = $user->email;

            $am2_name = $am2_email = "";

            if(isset($p->asset_manager) && isset($p->asset_manager->name)){
                $name = $p->asset_manager->name;
                $replyemail = $p->asset_manager->email;
            }

            if(in_array($p->id, $this->propertiesids()))
            {
                $p21 = $this->getdata21();
                $name = $p21['name'];
                $replyemail = $p21['email'];
            }

            if(isset($p->asset_manager_two) && isset($p->asset_manager_two->name)){
                $am2_name = $p->asset_manager_two->name;
                $am2_email = $p->asset_manager_two->email;
            }

            $modal = new PropertiesMailLog;
            $modal->property_id = $id;
            $modal->type = $request->step;
            if($request->comment)
                $modal->comment = $request->comment;
            else
                $modal->comment = "";

            $modal->record_id = $record_id;
            $modal->user_id = Auth::user()->id;
            $modal->save();


            if($request->step=="building"){
                $text = "Hallo Falk,<br><br> ".$name." benötigt deine Freigabe für die neue Gebäudeversicherung von ".$p->plz_ort.' '.$p->ort." ".$url;

                $subject = "Gebäudeversicherung für  ".$p->plz_ort.' '.$p->ort;
                $email = "f.raudies@fcr-immobilien.de";
                $am2_email = "";
            }
            if($request->step=="liability"){
                $text = "Hallo Falk,<br><br> ".$name." benötigt deine Freigabe für die neue Haftpflichtversicherung von ".$p->plz_ort.' '.$p->ort." ".$url;

                $subject = "Haftpflichtversicherung für  ".$p->plz_ort.' '.$p->ort;
                $email = "f.raudies@fcr-immobilien.de";
                $am2_email = "";
            }
            if($request->step=="release_building"){
                $text = "Hallo ".$name.",<br><br> Falk hat die neue Gebäudeversicherung für ".$p->plz_ort.' '.$p->ort." freigegeben: ".$url;
                $text2 = "Hallo ".$am2_name.",<br><br> Falk hat die neue Gebäudeversicherung für ".$p->plz_ort.' '.$p->ort." freigegeben: ".$url;

                $subject = "Freigabe Gebäudeversicherung  ".$p->plz_ort.' '.$p->ort;
                $email = $replyemail;
                $replyemail = "f.raudies@fcr-immobilien.de";
                $name = "Falk";
            }
            if($request->step=="release_liability"){
                $text = "Hallo ".$name.",<br><br> Falk hat die neue Haftpflichtversicherung für ".$p->plz_ort.' '.$p->ort." freigegeben: ".$url;
                $text2 = "Hallo ".$am2_name.",<br><br> Falk hat die neue Haftpflichtversicherung für ".$p->plz_ort.' '.$p->ort." freigegeben: ".$url;

                $subject = "Freigabe Haftpflichtversicherung ".$p->plz_ort.' '.$p->ort;
                $email = $replyemail;
                $replyemail = "f.raudies@fcr-immobilien.de";
                $name = "Falk";
            }
            if($request->step=="release_management"){
                $url = route('properties.show',['property'=>$id]).'?tab=property_management';
                $url = '<a href="'.$url.'">'.$url.'</a>';

                $text = "Hallo ".$name.",<br><br> Falk hat die Hausverwaltung von dem Objekt ".$p->plz_ort.' '.$p->ort.". freigegeben: ".$url;
                $text2 = "Hallo ".$am2_name.",<br><br> Falk hat die Hausverwaltung von dem Objekt ".$p->plz_ort.' '.$p->ort.". freigegeben: ".$url;

                $subject = "Hausverwaltung ".$p->plz_ort.' '.$p->ort." wurde freigegeben";
                $email = $replyemail;
                $replyemail = "f.raudies@fcr-immobilien.de";
                $name = "Falk";

            }
            if($request->step=="management"){
                $url = route('properties.show',['property'=>$id]).'?tab=property_management';
                $url = '<a href="'.$url.'">'.$url.'</a>';

                $text = "Hallo Falk,<br><br>".$name." benötigt deine Freigabe für die Hausverwaltung von dem Objekt ".$p->plz_ort.' '.$p->ort.". Link zur Freigabe: ".$url;

                $subject = "Freigabe Hausverwaltung ".$p->plz_ort.' '.$p->ort;
                $email = "f.raudies@fcr-immobilien.de";
                $am2_email = "";
            }

            if($request->comment){
                $text .= "<br><br>Kommentar: ".$request->comment;
                if(isset($text2))
                    $text2 .= "<br><br>Kommentar: ".$request->comment;
            }
            // echo "1"; die;
            if( !in_array($email, $this->mail_not_send()) ){

                Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail,$name) {
                    $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->replyTo($replyemail,$name)
                    ->subject($subject);
                });
            }

            if( $am2_email && $am2_email != $email && !in_array($am2_email, $this->mail_not_send()) ){
                Mail::send('emails.general', ['text' => $text2], function ($message) use($am2_email,$subject,$replyemail,$name) {
                    $message->to($am2_email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->replyTo($replyemail,$name)
                    ->subject($subject);
                });
            }
        }
        echo "1";
        die;
    }
    public function releaseoplist(Request $request)
    {
        $user = Auth::user();


        $modal = new PropertiesMailLog;
        $modal->property_id = $request->id;
        $modal->type = $request->step;
        $modal->user_id = $user->id;
        $modal->save();

        echo "1";
        die;


    }

    public function propertyManagementNotRelease(Request $request){
        $user = Auth::user();

        $pm = PropertyManagement::find($request->id);
        $pm->not_release = 1;
        $pm->save();

        $id = $pm->property_id;

        $modal = new PropertiesMailLog;
        $modal->property_id = $pm->property_id;
        $modal->type = 'not_release_property_management';
        if($request->comment)
            $modal->comment = $request->comment;
        $modal->user_id = $user->id;
        if($modal->save()){

            $property = DB::table('properties as p')
                        ->selectRaw('p.name_of_property, p.plz_ort, p.ort, u.id as am_id, u.name as am_name, u.email as am_email')
                        ->join('users as u', 'u.id', '=', 'p.asset_m_id')
                        ->where('p.id', $id)
                        ->first();

            if($property){

                $email = $property->am_email;
                $name = $property->am_name;
                $replyemail = $user->email;
                $replyname = $user->name;

                $subject = "Hausverwaltung ".$property->plz_ort.' '.$property->ort." wurde nicht freigegeben";

                $url = route('properties.show',['property'=>$id]).'?tab=property_management';
                $url = '<a href="'.$url.'">'.$url.'</a>';

                $text = "Hallo ".$name.",<br><br>Falk hat die Hausverwaltung von dem Objekt ".$property->plz_ort.' '.$property->ort." nicht freigegeben: ".$url;
                if($request->comment){
                    $text .= "<br><br>Kommentar: ".$request->comment;
                }

                if( !in_array($email, $this->mail_not_send()) ){
                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                        $message->to($email, $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemail, $replyname)
                        ->subject($subject);
                    });
                }
            }

            $response = ['status' => true, 'message' => 'Property Management mark as not release successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Property Management mark as not release fail!'];
        }

        return response()->json($response);
    }

    public function getverkauftable($p)
    {
        $id = $p->id;
        $kaufprice = "<br><br><table border='1'><tbody>";

        $verkaufspreis = DB::table('verkauf_tab')->where('property_id', $id)->where('row_name','verkaufspreis')->first();


        if($verkaufspreis && $verkaufspreis->tab_value)
        $kaufprice .= "<tr><th>Verkaufspreis</th><td>".number_format($verkaufspreis->tab_value,2,",",".")." €</td></tr>";
        else
            $kaufprice .= "<tr><th>Verkaufspreis</th><td></td></tr>";




        $mainp = Properties::where('main_property_id',$p->id)->whereRaw('Ist!=0')->first();

        $kaufprice .= "<tr><th>Verkaufspreis Portal</th>";
        if($mainp && is_numeric($mainp->preisverk)){
            $kaufprice .= "<td>".number_format($mainp->preisverk,2,",",".")." €</td></tr>";
        }
        else
            $kaufprice .= "<td></td></tr>";

        if(is_numeric($p->gesamt_in_eur))
            $kaufprice .= "<tr><th>EK-Preis</th><td>".number_format($p->gesamt_in_eur,2,",",".")." €</td></tr>";
        else
            $kaufprice .= "<tr><th>EK-Preis</th><td></td></tr>";




        $check103 = PropertiesSaleDetail::where('property_id', $id)->where('type', 103)->first();

        $kaufprice .= "<tr><th>Exklusivität bis</th>";
        if($check103 && $check103->comment)
        {
            $date=date_create($check103->comment);
            $date =  date_format($date,"d.m.Y");
            $kaufprice .= "<td>".$date."</td></tr>";
        }
        else{
            $kaufprice .= "<td></td></tr>";
        }

        $check101 = PropertiesSaleDetail::where('property_id', $id)->where('type', 101)->first();

        $kaufprice .= "<tr><th>Notartermin</th>";
        if($check101 && $check101->comment)
        {
            $date=date_create($check101->comment);
            $date =  date_format($date,"d.m.Y");
            $kaufprice .= "<td>".$date."</td></tr>";
        }
        else{
            $kaufprice .= "<td></td></tr>";
        }

        $check102 = PropertiesSaleDetail::where('property_id', $id)->where('type', 102)->first();

        $kaufprice .= "<tr><th>BNL Termin</th>";
        if($check102 && $check102->comment)
        {
            $date=date_create($check102->comment);
            $date =  date_format($date,"d.m.Y");
            $kaufprice .= "<td>".$date."</td></tr>";
        }
        else{
            $kaufprice .= "<td></td></tr>";
        }


        $data  = PropertiesSaleDetail::whereIn('type',array('btn1','btn2','btn3','btn4','btn5'))->where('property_id',$id)->get();

        $name = array();
        foreach ($data as $key => $value) {
            $name[$value->type] = date_formatting(date('d/m/Y',strtotime($value->created_at)));
        }

        $bnl = PropertiesBuyDetail::where('property_id', $id)->where('type', 402)->first();

        if($bnl && $bnl->comment)
            $kaufprice .= "<tr><th>BNL Einkauf</th><td>".date_format(  date_create(str_replace('/', '-', $bnl->comment)) , 'd.m.Y')."</td></tr>";
        else
            $kaufprice .= "<tr><th>BNL Einkauf</th><td></td></tr>";


        if(isset($name['btn1']))
        $kaufprice .= "<tr><th>Verkaufsentscheidung  (AR)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Verkaufsentscheidung  (AR)</th><td>Nein</td></tr>";


        if(isset($name['btn2']))
        $kaufprice .= "<tr><th>Verkaufsformalitäten  (AL)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Verkaufsformalitäten  (AL)</th><td>Nein</td></tr>";

        if(isset($name['btn3']))
        $kaufprice .= "<tr><th>Verkaufsdatenblatt  (AR)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Verkaufsdatenblatt  (AR)</th><td>Nein</td></tr>";


        if(isset($name['btn4']))
        $kaufprice .= "<tr><th>Generelle Entscheidung (FR)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Generelle Entscheidung (FR)</th><td>Nein</td></tr>";


        if(isset($name['btn5']))
        $kaufprice .= "<tr><th>Checkliste nach Verkauf (Verkäufer)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Checkliste nach Verkauf (Verkäufer)</th><td>Nein</td></tr>";

        $kaufprice .= "</tbody></table>";

        return $kaufprice;
    }


    public function verkaufreleaseprocedure(Request $request)
    {
        // die;
        $id = $request->property_id;
        if($request->step)
        {
            $url = route('properties.show',['property'=>$id]).'?tab=verkauf_tab';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $p = Properties::find($id);
            $user = Auth::user();
            $name = $user->name;
            $replyemail = $user->email;

            if(isset($p->transaction_manager) && isset($p->transaction_manager->name)){
                $name = $p->transaction_manager->name;
                $replyemail = $p->transaction_manager->email;
            }

            $check = PropertiesSaleDetail::where('property_id', $id)->where('type', $request->step)->first();
            if(!$check || $request->step=='btn1_request' || $request->step=='btn2_request' || $request->step=='btn3_request')
            {
                $modal = new PropertiesSaleDetail;
                $modal->property_id = $id;
                $modal->type = $request->step;
                if($request->comment)
                    $modal->comment = $request->comment;
                else
                    $modal->comment = "";
                $modal->user1 = Auth::user()->id;
                $modal->save();
            }
            $kaufprice = $this->getverkauftable($p);
            if(!$check)
            {



                if($request->step=="btn1")
                {
                    $text = "Hallo ".$name.",<br><br> Andrea hat die Verkaufsentscheidung für das Objekt  ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zum Objekt: ".$url;
                    $email = "a.raudies@fcr-immobilien.de";

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $text .= $kaufprice;


                    //$replyemail = $email = "c.wiedemann@fcr-immobilien.de";
                    $cc = array('f.raudies@fcr-immobilien.de','a.lauterbach@fcr-immobilien.de');
                    //$cc = array('mendparasukunj27@gmail.com');
                   //$replyemail = "c.wiedemann@fcr-immobilien.de";
                    $subject = "Freigabe Verkaufsentscheidung  ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$cc) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Andrea')
                            ->cc($cc)
                            ->subject($subject);
                        });
                    }
                }
                if($request->step=="btn2")
                {
                    $text = "Hallo ".$name.",<br><br> Alex hat die Verkaufsformalitäten für das Objekt ".$p->plz_ort.' '.$p->ort." eingetragen und das Objekt freigegeben. Hier kommst du zum Objekt: ".$url;
                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;
                    $email = "a.lauterbach@fcr-immobilien.de";
                    //$replyemail = $email = "c.wiedemann@fcr-immobilien.de";
                    $cc = array('f.raudies@fcr-immobilien.de','a.raudies@fcr-immobilien.de');
                    //$cc = array('yiicakephp@gmail.com');
                    //$email = "yiicakephp@gmail.com";
                    $subject = "Verkaufsformalitäten  ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$cc) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Alex')
                            ->cc($cc)
                            ->subject($subject);
                        });
                    }
                }
                if($request->step=="btn3")
                {
                    $text = "Hallo ".$name.",<br><br> Andrea hat das Verkaufsdatenblatt für das Objekt  ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zum Objekt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;
                    $email = "a.raudies@fcr-immobilien.de";
                    //$replyemail = $email = "c.wiedemann@fcr-immobilien.de";
                    $cc = array('f.raudies@fcr-immobilien.de');
                    //$cc = array('yiicakephp@gmail.com');
                    //$replyemail = "yiicakephp@gmail.com";
                    $subject = "Freigabe Verkaufsdatenblatt ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$cc) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Andrea')
                            ->cc($cc)
                            ->subject($subject);
                        });
                    }
                }
                if($request->step=="btn4")
                {
                    $text = "Hallo ".$name.",<br><br> Falk hat die generelle Verkaufsentscheidung für das Objekt ".$p->plz_ort.' '.$p->ort."freigegeben. Hier kommst du zum Objekt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;
                    $email = "a.raudies@fcr-immobilien.de";
                    //$replyemail = $email = "c.wiedemann@fcr-immobilien.de";
                    $cc = array('a.raudies@fcr-immobilien.de','a.lauterbach@fcr-immobilien.de','t.raudies@fcr-immobilien.de','j.klausch@fcr-immobilien.de');
                    //$cc = array('yiicakephp@gmail.com');
                    //$replyemail = "yiicakephp@gmail.com";
                    $subject = "Freigabe Transaktionsentscheidung ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$cc) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Andrea')
                            ->cc($cc)
                            ->subject($subject);
                        });
                    }
                }


                if(false && $request->step=="btn5_request")
                {
                    $text = "Hallo Andrea,<br><br> ".$name." hat alle Punkte der Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort." erledigt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $email = "a.raudies@fcr-immobilien.de";
                    //$cc = array('yiicakephp@gmail.com');
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail,$name)
                            ->subject($subject);
                        });
                    }

                    $text = "Hallo Falk,<br><br> ".$name." hat alle Punkte der Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort." erledigt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $email = "f.raudies@fcr-immobilien.de";
                    //$cc = array('yiicakephp@gmail.com');
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail,$name)
                            ->subject($subject);
                        });

                    }

                    $text = "Hallo Thorsten,<br><br> ".$name." hat alle Punkte der Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort." erledigt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $email = "t.raudies@fcr-immobilien.de";
                    //$cc = array('yiicakephp@gmail.com');
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail,$name)
                            ->subject($subject);
                        });
                    }

                    $text = "Hallo Janine,<br><br> ".$name." hat alle Punkte der Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort." erledigt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $email = "j.klausch@fcr-immobilien.de";
                    //$cc = array('yiicakephp@gmail.com');
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail,$name)
                            ->subject($subject);
                        });
                    }
                }

            }





                if($request->step=="btn1_request")
                {
                    $text = "Hallo Andrea,<br><br> ".$name." benötigt deine Freigabe zur Verkaufsentscheidung von ".$p->plz_ort.' '.$p->ort.". Hier kannst du das Objekt freigeben: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;

                    $email = "a.raudies@fcr-immobilien.de";
                    //$email = "c.wiedemann@fcr-immobilien.de";
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Freigabe Verkaufsentscheidung ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail, $name)
                            ->subject($subject);
                        });
                    }

                }
                if($request->step=="btn2_request")
                {
                    $text = "Hallo Alex,<br><br> bitte trage die Verkaufsformalitäten für das Objekt ".$p->plz_ort.' '.$p->ort." ein. Hier kannst du die Daten eintragen und das Objekt freigeben: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;

                    $email = "a.lauterbach@fcr-immobilien.de";
                    //$email = "c.wiedemann@fcr-immobilien.de";
                    //$email = "yiicakephp@gmail.com";
                    $subject = "Verlaufsformalitäten  ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail, $name)
                            ->subject($subject);
                        });
                    }
                }
                if($request->step=="btn3_request")
                {
                    $text = "Hallo Andrea,<br><br> ".$name." benötigt deine Freigabe zum Verkaufsdatenblatt von ".$p->plz_ort.' '.$p->ort.". Hier kannst du das Objekt freigeben: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;

                    $email = "a.raudies@fcr-immobilien.de";
                    //$email = "c.wiedemann@fcr-immobilien.de";
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Freigabe Verkaufsdatenblatt ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail, $name)
                            ->subject($subject);
                        });
                    }
                }
                if($request->step=="btn4_request")
                {
                    $text = "Hallo Falk,<br><br> ".$name." benötigt deine generelle Freigabe zum Verkauf von ".$p->plz_ort.' '.$p->ort.". Hier kannst du das Objekt freigeben: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;
                    $email = "f.raudies@fcr-immobilien.de";
                    //$email = "c.wiedemann@fcr-immobilien.de";
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Freigabe Transaktionsentscheidung ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail, $name)
                            ->subject($subject);
                        });
                    }
                }

        }
    }
    public function geteinkauftable($p)
    {
        $id = $p->id;

        $gesamt_in_eur = $p->gesamt_in_eur;

        $mainp = Properties::where('main_property_id',$p->id)->whereRaw('Ist!=0')->first();
        if($mainp)
            $gesamt_in_eur = $mainp->gesamt_in_eur;


        $kaufprice = "<br><br><table border='1'><tbody>";
        if(is_numeric($p->gesamt_in_eur))
            $kaufprice .= "<tr><th>Kaufpreis</th><td>".number_format($gesamt_in_eur,2,",",".")." €</td></tr>";
        else
            $kaufprice .= "<tr><th>Kaufpreis</th><td></td></tr>";



        if($mainp && $mainp->exklusivität_bis){
            $date=date_create($mainp->exklusivität_bis);
            $date =  date_format($date,"d.m.Y");
            $kaufprice .= "<tr><th>Exklusivität bis</th><td>".$date."</td></tr>";


        }
        else
            $kaufprice .= "<tr><th>Exklusivität bis</th><td></td></tr>";

        if($mainp && $mainp->purchase_date)
            $kaufprice .= "<tr><th>Notartermin</th><td>".date_format(  date_create(str_replace('/', '-', $mainp->purchase_date)) , 'd.m.Y')."</td></tr>";
        else
            $kaufprice .= "<tr><th>Notartermin</th><td></td></tr>";

        $bnl = PropertiesBuyDetail::where('property_id', $id)->where('type', 402)->first();

        if($bnl && $bnl->comment)
            $kaufprice .= "<tr><th>BNL Termin</th><td>".date_format(  date_create(str_replace('/', '-', $bnl->comment)) , 'd.m.Y')."</td></tr>";
        else
            $kaufprice .= "<tr><th>BNL Termin</th><td></td></tr>";


        $data  = PropertiesBuyDetail::whereIn('type',array('btn1','btn2','btn3','btn4','btn5','btn6'))->where('property_id',$id)->get();

        $name2 = array();
        foreach ($data as $key => $value) {
            $name2[$value->type] = date_formatting(date('d/m/Y',strtotime($value->created_at)));
        }

        // if(isset($name2['btn6']))
        // $kaufprice .= "<tr><th>Einkaufsanfrage (AR)</th><td>Ja</td></tr>";
        // else
        // $kaufprice .= "<tr><th>Einkaufsanfrage (AR)</th><td>Nein</td></tr>";

        if(isset($name2['btn1']))
        $kaufprice .= "<tr><th>Einkaufsentscheidung (AR)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Einkaufsentscheidung (AR)</th><td>Nein</td></tr>";


        if(isset($name2['btn2']))
        $kaufprice .= "<tr><th>Liquiditätsentscheidung (AL)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Liquiditätsentscheidung (AL)</th><td>Nein</td></tr>";

        if(isset($name2['btn3']))
        $kaufprice .= "<tr><th>Einkaufsdatenblatt (AR)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Einkaufsdatenblatt (AR)</th><td>Nein</td></tr>";


        if(isset($name2['btn4']))
        $kaufprice .= "<tr><th>Transaktionsentscheidung (FR)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Transaktionsentscheidung (FR)</th><td>Nein</td></tr>";


        if(isset($name2['btn5']))
        $kaufprice .= "<tr><th>Checkliste n. Einkauf (TM)</th><td>Ja</td></tr>";
        else
        $kaufprice .= "<tr><th>Checkliste n. Einkauf (TM)</th><td>Nein</td></tr>";

        $kaufprice .= "</tbody></table>";

        return $kaufprice;
    }

    public function releaseprocedure(Request $request)
    {
        $id = $request->property_id;
        if($request->step)
        {
            $url = route('properties.show',['property'=>$id]).'?tab=einkauf_tab';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $p = Properties::find($id);
            $user = Auth::user();
            $name = $user->name;
            $replyemail = $user->email;

            if(isset($p->transaction_manager) && isset($p->transaction_manager->name)){
                $name = $p->transaction_manager->name;
                $replyemail = $p->transaction_manager->email;
            }

            $check = PropertiesBuyDetail::where('property_id', $id)->where('type', $request->step)->first();
            if(!$check || $request->step=='btn1_request' || $request->step=='btn2_request' || $request->step=='btn3_request' || $request->step=='btn6_request')
            {
                $modal = new PropertiesBuyDetail;
                $modal->property_id = $id;
                $modal->type = $request->step;
                if($request->comment)
                    $modal->comment = $request->comment;
                else
                    $modal->comment = "";
                $modal->user1 = Auth::user()->id;
                $modal->save();
            }

            // echo "1"; die;
            $kaufprice = $this->geteinkauftable($p);

            if(!$check)
            {



                if($request->step=="btn6")
                {
                    if($request->lock_id)
                    {
                        $Properties_clone = Properties::find($request->lock_id);
                        $Properties_New = $Properties_clone->replicate();
                        $Properties_New->sheet_title = "Freigegeben";
                        $Properties_New->lock_status = 1;
                        $Properties_New->standard_property_status = 0;
                        $Properties_New->save();


                        $propertiesExtra1s = PropertiesTenants::where('propertyId', $request->lock_id)->get();


                        $arr = array();
                        foreach ($propertiesExtra1s as $key => $value) {
                            $arr[$key]['propertyId'] = $Properties_New->id;
                            $arr[$key]['type'] = $value->type;
                            $arr[$key]['tenant'] = $value->tenant;
                            $arr[$key]['net_rent_p_a'] = $value->net_rent_p_a;
                            $arr[$key]['mv_end'] = $value->mv_end;
                            $arr[$key]['mv_end2'] = $value->mv_end2;
                            $arr[$key]['mietvertrag_text'] = $value->mietvertrag_text;
                            $arr[$key]['is_dynamic_date'] = $value->is_dynamic_date;
                            $arr[$key]['flache'] = $value->flache;
                            $arr[$key]['is_current_net'] = $value->is_current_net;
                        }

                        PropertiesTenants::insert($arr);




                    }


                    $text = "Hallo ".$name.",<br><br> Andrea hat die Einkaufsanfrage für das Objekt ".$p->plz_ort.' '.$p->ort.". freigegeben. Hier kommst du zum Objekt: ".$url;
                    $email = "a.raudies@fcr-immobilien.de";

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $text .= $kaufprice;

                    // $replyemail = $email = "c.wiedemann@fcr-immobilien.de";
                    $cc = array('f.raudies@fcr-immobilien.de','a.lauterbach@fcr-immobilien.de');
                    // $cc = array('mendparasukunj27@gmail.com');
                    // $replyemail = "c.wiedemann@fcr-immobilien.de";
                    $subject = "Freigabe Einkaufsanfrage ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$cc) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Andrea')
                            ->cc($cc)
                            ->subject($subject);
                        });
                    }
                }

                if($request->step=="btn1")
                {
                    $text = "Hallo ".$name.",<br><br> Andrea hat die Einkaufsentscheidung für das Objekt ".$p->plz_ort.' '.$p->ort.". freigegeben. Hier kommst du zum Objekt: ".$url;
                    $email = "a.raudies@fcr-immobilien.de";

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $text .= $kaufprice;

                    //$replyemail = $email = "c.wiedemann@fcr-immobilien.de";
                    $cc = array('f.raudies@fcr-immobilien.de','a.lauterbach@fcr-immobilien.de');
                    // $cc = array('mendparasukunj27@gmail.com');
                    // $replyemail = "c.wiedemann@fcr-immobilien.de";
                    $subject = "Freigabe Einkaufsentscheidung ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$cc) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Andrea')
                            ->cc($cc)
                            ->subject($subject);
                        });
                    }
                }
                if($request->step=="btn2")
                {
                    $text = "Hallo ".$name.",<br><br> Falk hat die Liquiditätsentscheidung für das Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zum Objekt: ".$url;
                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;

                    $email = config('users.falk_email');
                    // $email = "a.lauterbach@fcr-immobilien.de";
                    //$replyemail = $email = "c.wiedemann@fcr-immobilien.de";
                    $cc = array('f.raudies@fcr-immobilien.de','a.raudies@fcr-immobilien.de');
                    //$cc = array('yiicakephp@gmail.com');
                    //$email = "yiicakephp@gmail.com";
                    $subject = "Freigabe Einkaufsentscheidung ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$cc) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Falk')
                            ->cc($cc)
                            ->subject($subject);
                        });

                    }
                }
                if($request->step=="btn3")
                {
                    $text = "Hallo ".$name.",<br><br> Andrea hat das Einkaufsdatenblatt für das Objekt ".$p->plz_ort.' '.$p->ort.". freigegeben. Hier kommst du zum Objekt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;

                    $email = "a.raudies@fcr-immobilien.de";
                    // $replyemail = $email = "c.wiedemann@fcr-immobilien.de";
                    $cc = array('f.raudies@fcr-immobilien.de');
                    // $cc = array('yiicakephp@gmail.com');
                    // $replyemail = "yiicakephp@gmail.com";
                    $subject = "Freigabe Einkaufsdatenblatt ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$cc) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Andrea')
                            ->cc($cc)
                            ->subject($subject);
                        });
                    }
                }


                if($request->step=="btn5_request")
                {
                    $text = "Hallo Andrea,<br><br> ".$name." hat alle Punkte der Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort." erledigt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $text .= $kaufprice;

                    $email = "a.raudies@fcr-immobilien.de";
                    //$cc = array('yiicakephp@gmail.com');
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail,$name)
                            ->subject($subject);
                        });
                    }

                    $text = "Hallo Falk,<br><br> ".$name." hat alle Punkte der Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort." erledigt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $email = "f.raudies@fcr-immobilien.de";
                    //$cc = array('yiicakephp@gmail.com');
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail,$name)
                            ->subject($subject);
                        });

                    }




                    $text = "Hallo Thorsten,<br><br> ".$name." hat alle Punkte der Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort." erledigt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $email = "t.raudies@fcr-immobilien.de";
                    //$cc = array('yiicakephp@gmail.com');
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail,$name)
                            ->subject($subject);
                        });
                    }

                    $text = "Hallo Janine,<br><br> ".$name." hat alle Punkte der Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort." erledigt: ".$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $email = "j.klausch@fcr-immobilien.de";
                    //$cc = array('yiicakephp@gmail.com');
                    // $email = "yiicakephp@gmail.com";
                    $subject = "Checkliste nach dem Einkauf von ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail,$name)
                            ->subject($subject);
                        });
                    }
                }

            }

            if($request->step=="btn1_request")
            {
                $text = "Hallo Andrea,<br><br> ".$name." benötigt deine Freigabe zur Einkaufsentscheidung von ".$p->plz_ort.' '.$p->ort.". Hier kannst du das Objekt freigeben: ".$url;

                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;

                $text .= $kaufprice;

                $email = "a.raudies@fcr-immobilien.de";
                //$email = "c.wiedemann@fcr-immobilien.de";
                // $email = "yiicakephp@gmail.com";
                $subject = "Freigabe Einkaufsentscheidung ".$p->plz_ort.' '.$p->ort;

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemail, $name)
                        ->subject($subject);
                    });
                }

            }

            if($request->step=="btn6_request")
            {
                $text = "Hallo Andrea,<br><br> ".$name." benötigt deine Freigabe zur Einkaufsanfrage von ".$p->plz_ort.' '.$p->ort.". Hier kannst du das Objekt freigeben: ".$url;

                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;

                $text .= $kaufprice;

                $email = "a.raudies@fcr-immobilien.de";
                //$email = "c.wiedemann@fcr-immobilien.de";
                // $email = "yiicakephp@gmail.com";
                $subject = "Freigabe Einkaufsanfrage ".$p->plz_ort.' '.$p->ort;
                Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                    $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->replyTo($replyemail, $name)
                    ->subject($subject);
                });

            }

            if($request->step=="btn2_request")
            {
                //$text = "Hallo Alex,<br><br> ".$name." benötigt deine Freigabe zur Liquiditätsentscheidung von ".$p->plz_ort.' '.$p->ort.". Hier kannst du das Objekt freigeben: ".$url;
                $text = "Hallo Falk,<br><br> ".$name." benötigt deine Freigabe zur Liquiditätsentscheidung von ".$p->plz_ort.' '.$p->ort.". Hier kannst du das Objekt freigeben: ".$url;

                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;

                $text .= $kaufprice;

                $email = config('users.falk_email');

                // $email = "a.lauterbach@fcr-immobilien.de";
                //$email = "c.wiedemann@fcr-immobilien.de";
                //$email = "yiicakephp@gmail.com";
                $subject = "Freigabe Liquiditätsentscheidung ".$p->plz_ort.' '.$p->ort;

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemail, $name)
                        ->subject($subject);
                    });
                }
            }

            if($request->step=="btn3_request")
            {
                $text = "Hallo Andrea,<br><br> ".$name." benötigt deine Freigabe zur Einkaufsentscheidung von ".$p->plz_ort.' '.$p->ort.". Hier kannst du das Objekt freigeben: ".$url;

                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;

                $text .= $kaufprice;

                $email = "a.raudies@fcr-immobilien.de";
                //$email = "c.wiedemann@fcr-immobilien.de";
                //$email = "yiicakephp@gmail.com";
                $subject = "Freigabe Einkaufsdatenblatt ".$p->plz_ort.' '.$p->ort;

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                                $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemail, $name)
                        ->subject($subject);
                    });
                }
            }

        }
    }

    public function vacantreleaseprocedure(Request $request)
    {
        $id = $request->property_id;
        if($request->step)
        {
            $url = route('properties.show',['property'=>$id]).'?tab=recommended_tab';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $tenant = PropertyVacant::find($request->tenant_id);
            $custom = 0;
            $tenant_name = "";

            /*if($tenant){
                if(!$tenant->tenant_id)
                    $custom = 1;
                $tenant_name = $tenant->name;
            }*/

            if($tenant){

                $mv_vl_res = EmpfehlungDetail::where('slug','mv_vl')->where('tenant_id',$request->tenant_id)->first();
                if($mv_vl_res){
                    $custom = ($mv_vl_res->value == 'vl') ? 1 : 0;
                }else{
                    if(!$tenant->tenant_id)
                        $custom = 1;
                }
                $tenant_name = $tenant->name;
            }


            $mieter_name = "";

            $b = EmpfehlungDetail::where('slug','tenant_name')->where('tenant_id',$request->tenant_id)->first();
            if($b)
            {
                $mieter_name = $b->value;
                if(is_numeric($b->value) && $custom)
                {
                    $i = TenancyScheduleItem::where('id',$b->value)->first();
                    if($i)
                        $mieter_name = $i->name;
                }


            }


            $p = Properties::find($id);
            $user = Auth::user();
            $name = $user->name;
            $replyemail = $user->email;

            $am2_name = $am2_email = "";

            if(isset($p->asset_manager) && isset($p->asset_manager->name)){
                $name = $p->asset_manager->name;
                $replyemail = $p->asset_manager->email;
            }

            if(in_array($p->id, $this->propertiesids()))
            {
                $p21 = $this->getdata21();
                $name = $p21['name'];
                $replyemail = $p21['email'];
            }

            if(isset($p->asset_manager_two) && isset($p->asset_manager_two->name)){
                $am2_name = $p->asset_manager_two->name;
                $am2_email = $p->asset_manager_two->email;
            }

            $check = EmpfehlungDetail::where('slug',$request->step)->where('tenant_id',$request->tenant_id)->first();
            if(!$check || $request->step=='btn1_request' || $request->step=='btn2_request')
            {
                $modal = new EmpfehlungDetail;
                $modal->property_id = $id;
                $modal->tenant_id = $request->tenant_id;
                $modal->slug = $request->step;
                if($request->comment)
                    $modal->value = $request->comment;
                else
                    $modal->value = "";
                $modal->user_id = Auth::user()->id;
                $modal->save();
            }

            if(!$check)
            {

                if($request->step=="btn1")
                {
                    $text = "Hallo ".$name.",<br><br> Janine hat deine Vermietungsempfehlung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die Leerstandsfläche $tenant_name freigegeben. Hier kommst du zur Freigabe: ".$url;

                    $text2 = "Hallo ".$am2_name.",<br><br> Janine hat deine Vermietungsempfehlung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die Leerstandsfläche $tenant_name freigegeben. Hier kommst du zur Freigabe: ".$url;

                    $email = "j.klausch@fcr-immobilien.de";
                    $subject = "Freigabe $mieter_name für die Leerstandsfläche $tenant_name für das Objekt ".$p->plz_ort.' '.$p->ort;

                    if($custom)
                    {
                        $text = "Hallo ".$name.",<br><br> Janine hat deine Verlängerung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;

                        $text2 = "Hallo ".$am2_name.",<br><br> Janine hat deine Verlängerung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;

                         $subject = "Freigabe $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;
                    }
                    if($request->comment){
                        $text .= "<br><br>Kommentar: ".$request->comment;
                        $text2 .= "<br><br>Kommentar: ".$request->comment;
                    }

                    if( $replyemail && !in_array($replyemail, $this->mail_not_send()) ){
                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Janine')
                            ->subject($subject);
                        });
                    }

                    if( $am2_email && $am2_email != $replyemail && !in_array($am2_email, $this->mail_not_send()) ){
                        Mail::send('emails.general', ['text' => $text2], function ($message) use($email,$subject,$am2_email, $name) {
                            $message->to($am2_email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Janine')
                            ->subject($subject);
                        });
                    }
                }

                if($request->step=="btn2")
                {
                    $tenant->not_release_status = 0;
                    $tenant->save();

                    $text = "Hallo ".$name.",<br><br> Falk hat deine Vermietungsempfehlung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die Leerstandsfläche $tenant_name freigegeben. Hier kommst du zur Freigabe: ".$url;
                    $text2 = "Hallo ".$am2_name.",<br><br> Falk hat deine Vermietungsempfehlung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die Leerstandsfläche $tenant_name freigegeben. Hier kommst du zur Freigabe: ".$url;

                    $email = config('users.falk_email');
                    $subject = "Freigabe $mieter_name für die Leerstandsfläche $tenant_name für das Objekt ".$p->plz_ort.' '.$p->ort;

                    if($custom)
                    {
                        $text = "Hallo ".$name.",<br><br> Falk hat deine Verlängerung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;
                        $text2 = "Hallo ".$am2_name.",<br><br> Falk hat deine Verlängerung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;
                        $subject = "Freigabe $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;
                    }
                    if($request->comment){
                        $text .= "<br><br>Kommentar: ".$request->comment;
                        $text2 .= "<br><br>Kommentar: ".$request->comment;
                    }

                    if( $replyemail && !in_array($replyemail, $this->mail_not_send()) ){
                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Falk')
                            ->subject($subject);
                        });
                    }

                    if( $am2_email && $am2_email != $replyemail && !in_array($am2_email, $this->mail_not_send()) ){
                         Mail::send('emails.general', ['text' => $text2], function ($message) use($email,$subject,$am2_email, $name) {
                            $message->to($am2_email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Falk')
                            ->subject($subject);
                        });
                    }


                    $text = "Hallo Janine,<br><br> Falk hat deine Vermietungsempfehlung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die Leerstandsfläche $tenant_name freigegeben. Hier kommst du zur Freigabe: ".$url;
                    $email = config('users.falk_email');

                    $replyemail = "j.klausch@fcr-immobilien.de";
                     // $replyemail = "yiicakephp@gmail.com";
                     // $replyemail = "c.wiedemann@fcr-immobilien.de";
                    $subject = "Freigabe $mieter_name für die Leerstandsfläche $tenant_name für das Objekt ".$p->plz_ort.' '.$p->ort;

                    if($custom)
                    {
                        $text = "Hallo Janine,<br><br> Falk hat deine Verlängerung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;
                        $subject = "Freigabe $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;
                    }
                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Falk')
                            ->subject($subject);
                        });

                    }
                }

            }

            if($request->step=="btn1_request")
            {
                $text = "Hallo Janine,<br><br> ".$name." benötigt deine Freigabe für die Vermietungsempfehlung ".$mieter_name." bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die Leerstandsfläche ".$tenant_name.". Hier kommst du zur Freigabe: ".$url;


                $email = "j.klausch@fcr-immobilien.de";
                // $email = "yiicakephp@gmail.com";
                // $email = "c.wiedemann@fcr-immobilien.de";
                $subject = "Freigabe Empfehlung für die Leerstandsfläche ".$tenant_name." für das Objekt ".$p->plz_ort.' '.$p->ort;

                if($custom)
                {
                    $text = "Hallo Janine,<br><br> ".$name." benötigt deine Freigabe für die Verlängerung ".$mieter_name." bei dem Objekt ".$p->plz_ort.' '.$p->ort." Hier kommst du zur Freigabe: ".$url;
                    $subject = "Freigabe Empfehlung für das Objekt ".$p->plz_ort.' '.$p->ort;
                }
                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;



                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemail, $name)
                        ->subject($subject);
                    });
                }

            }

            if($request->step=="btn2_request")
            {
                $text = "Hallo Falk,<br><br> ".$name." benötigt deine Freigabe für die Vermietungsempfehlung ".$mieter_name." bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die Leerstandsfläche ".$tenant_name.". Hier kommst du zur Freigabe: ".$url;



                $email = config('users.falk_email');
                // $email = "yiicakephp@gmail.com";
                // $email = "c.wiedemann@fcr-immobilien.de";
                $subject = "Freigabe Empfehlung für die Leerstandsfläche ".$tenant_name." für das Objekt ".$p->plz_ort.' '.$p->ort;

                if($custom){
                    $text = "Hallo Falk,<br><br> ".$name." benötigt deine Freigabe für die Verlängerung ".$mieter_name." bei dem Objekt ".$p->plz_ort.' '.$p->ort." Hier kommst du zur Freigabe: ".$url;

                    $subject = "Freigabe Empfehlung für die Verlängerung für das Objekt ".$p->plz_ort.' '.$p->ort;
                }
                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;


                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemail, $name)
                        ->subject($subject);
                    });
                }
            }

        }
    }

    public function provisionreleaseprocedure(Request $request)
    {
        $id = $request->property_id;
        if($request->step)
        {
            $url = route('properties.show',['property'=>$id]).'?tab=provision_tab';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $tenant_name = "";

            $mieter_name = "";
            $tenant_id = "";

            $b = Provision::where('id',$request->tenant_id)->first();
            if($b)
            {
                $tenant_id = $b->tenant_id;
                $mieter_name = $b->name;
            }


            $p = Properties::find($id);
            $user = Auth::user();
            $name = $user->name;
            $replyemail = $user->email;

            $am2_email = $am2_name = "";

            if(isset($p->asset_manager) && isset($p->asset_manager->name)){
                $name = $p->asset_manager->name;
                $replyemail = $p->asset_manager->email;
            }

            if(isset($b->asset_manager) && isset($b->asset_manager->name)){
                $name = $b->asset_manager->name;
                $replyemail = $b->asset_manager->email;
            }
            if(in_array($p->id, $this->propertiesids()))
            {
                $p21 = $this->getdata21();
                $name = $p21['name'];
                $replyemail = $p21['email'];
            }

            if(isset($p->asset_manager_two) && isset($p->asset_manager_two->name)){
                $am2_name = $p->asset_manager_two->name;
                $am2_email = $p->asset_manager_two->email;
            }

            $check = EmpfehlungDetail::where('slug',$request->step)->where('provision_id',$request->tenant_id)->first();
            if(!$check || $request->step=='pbtn1_request' || $request->step=='pbtn2_request')
            {
                $modal = new EmpfehlungDetail;
                $modal->property_id = $id;
                $modal->tenant_id = $tenant_id;
                $modal->provision_id = $request->tenant_id;
                $modal->slug = $request->step;
                if($request->comment)
                    $modal->value = $request->comment;
                else
                    $modal->value = "";
                $modal->user_id = Auth::user()->id;
                $modal->save();
            }

            if(!$check)
            {

                if($request->step=="pbtn1")
                {
                    if($replyemail){
                        $text = "Hallo ".$name.",<br><br> Janine hat deine Provision für $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;
                        $email = "j.klausch@fcr-immobilien.de";
                        if($request->comment)
                            $text .= "<br><br>Kommentar: ".$request->comment;

                        $subject = "Freigabe Provision für $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;

                        if( !in_array($replyemail, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                                $message->to($replyemail)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($email, 'Janine')
                                ->subject($subject);
                            });
                        }
                    }

                    if($am2_email && $am2_email != $replyemail){
                        $text = "Hallo ".$am2_name.",<br><br> Janine hat deine Provision für $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;
                        $email = "j.klausch@fcr-immobilien.de";
                        if($request->comment)
                            $text .= "<br><br>Kommentar: ".$request->comment;

                        $subject = "Freigabe Provision für $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;

                        if( !in_array($am2_email, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$am2_email, $name) {
                                $message->to($am2_email)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($email, 'Janine')
                                ->subject($subject);
                            });
                        }
                    }
                }

                if($request->step=="pbtn2")
                {
                    if($replyemail){
                        $text = "Hallo ".$name.",<br><br> Falk hat deine Provision für $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;
                        $email = config('users.falk_email');
                        if($request->comment)
                            $text .= "<br><br>Kommentar: ".$request->comment;

                        $subject = "Freigabe Provision für $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;

                        if( !in_array($replyemail, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                                $message->to($replyemail)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($email, 'Falk')
                                ->subject($subject);
                            });
                        }
                    }

                    if($am2_email && $am2_email != $replyemail){
                        $text = "Hallo ".$am2_name.",<br><br> Falk hat deine Provision für $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;
                        $email = config('users.falk_email');
                        if($request->comment)
                            $text .= "<br><br>Kommentar: ".$request->comment;

                        $subject = "Freigabe Provision für $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;

                        if( !in_array($am2_email, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$am2_email, $name) {
                                $message->to($am2_email)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($email, 'Falk')
                                ->subject($subject);
                            });
                        }
                    }

                    /*
                    $text = "Hallo Janine,<br><br> Falk hat die Provision für $name für $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;
                    $email = config('users.falk_email');
                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $replyemail = "j.klausch@fcr-immobilien.de";

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                        $message->to($replyemail)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($email, 'Falk')
                        ->subject($subject);
                    });*/

                    $text = "Hallo  Ulf Wallisch,<br><br> Falk hat die Provision für $name für $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." freigegeben. Hier kommst du zur Freigabe: ".$url;
                    $email = config('users.falk_email');
                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    //$replyemail = $email = "c.wiedemann@fcr-immobilien.de";
                    $replyemail = "u.wallisch@fcr-immobilien.de";
                     // $replyemail = "yiicakephp@gmail.com";
                     // $replyemail = "c.wiedemann@fcr-immobilien.de";

                    if( !in_array($replyemail, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                            $message->to($replyemail)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($email, 'Falk')
                            ->subject($subject);
                        });
                    }
                }

            }

            if($request->step=="pbtn1_request")
            {
                $text = "Hallo Janine,<br><br> ".$name." benötigt deine Freigabe für die Provision für den Mieter ".$mieter_name." bei dem Objekt ".$p->plz_ort.' '.$p->ort.". Hier kommst du zur Freigabe: ".$url;

                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;


                $email = "j.klausch@fcr-immobilien.de";

                $subject = "Freigabe Provision für den Mieter $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemail, $name)
                        ->subject($subject);
                    });
                }

            }

            if($request->step=="pbtn2_request")
            {
                $text = "Hallo Falk,<br><br> ".$name." benötigt deine Freigabe für die Provision für den Mieter ".$mieter_name." bei dem Objekt ".$p->plz_ort.' '.$p->ort.". Hier kommst du zur Freigabe: ".$url;

                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;


                $email = config('users.falk_email');
                // $email = "yiicakephp@gmail.com";
                // $email = "c.wiedemann@fcr-immobilien.de";
                $subject = "Freigabe Provision für den Mieter $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemail, $name)
                        ->subject($subject);
                    });
                }
            }

        }
        echo "1";
    }

    public function provisionMarkAsNotRelease(Request $request, $property_id, $id){
        $user = Auth::user();
        $provision = Provision::where('id', $id)->first();

        $modal = new EmpfehlungDetail;
        $modal->property_id = $property_id;
        $modal->tenant_id = $provision->tenant_id;
        $modal->provision_id = $id;
        $modal->slug = 'provosion_mark_as_not_release';
        if($request->comment)
            $modal->value = $request->comment;
        $modal->user_id = Auth::user()->id;
        if($modal->save()){

            if($provision->tenant_id){
                $modal_vacant = PropertyVacant::find($provision->tenant_id);
                $modal_vacant->provision_not_release_status = 1;
                $modal_vacant->save();
            }

            $property = DB::table('properties as p')
                        ->selectRaw('p.name_of_property, p.plz_ort, p.ort, u.id as am_id, u.name as am_name, u.email as am_email')
                        ->join('users as u', 'u.id', '=', 'p.asset_m_id')
                        ->where('p.id', $property_id)
                        ->first();

            if($property){

                $email = $property->am_email;
                $name = $property->am_name;
                $replyemail = $user->email;
                $replyname = $user->name;

                $mieter_name = $provision->name;

                $url = route('properties.show',['property'=>$property_id]).'?tab=provision_tab';
                $url = '<a href="'.$url.'">'.$url.'</a>';

                $text = "Hallo ".$name.",<br><br>Falk hat deine Provision für ".$mieter_name." bei dem Objekt ".$property->plz_ort.' '.$property->ort." nicht freigegeben. Hier kommst du zum Intranet: ".$url;
                if($request->comment){
                    $text .= "<br><br>Kommentar: ".$request->comment;
                }

                $subject = "Provision für $mieter_name für das Objekt ".$property->plz_ort.' '.$property->ort;

                if( $email && !in_array($email, $this->mail_not_send()) ){
                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                        $message->to($email, $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemail, $replyname)
                        ->subject($subject);
                    });
                }
            }

            $response = ['status' => true, 'message' => 'Provision mark as not release successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Provision mark as not release fail!'];
        }

        return response()->json($response);

    }

    public function invoicereleaseprocedure(Request $request)
    {

        $idsarr  = explode(',', $request->id);

        foreach ($idsarr as $key => $value) {

            $request->invoice_id = $value;
            if($request->step)
            {
                $invoice = PropertyInvoices::where('id',$request->invoice_id)->first();

                if($request->invoice_asset_manager)
                {
                    $invoice->email = $request->invoice_asset_manager;
                    $invoice->save();
                }

                $id = $invoice->property_id;

                $url = route('properties.show',['property'=>$id]).'?tab=property_invoice';
                $url = '<a href="'.$url.'">'.$url.'</a>';


                $extern_url = route('extern_home').'?property_id='.base64_encode($id);
                $extern_url = '<a href="'.$extern_url.'">'.$extern_url.'</a>';




                $contents = collect(Storage::cloud()->listContents($invoice->file_dirname, false));
                $gDriveFile = $contents->where('type','file')->where('basename', $invoice->file_basename)->first();



                $p = Properties::find($id);
                $user = Auth::user();
                $aname = $name = $user->name;
                $areplyemail = $replyemail = $user->email;
                $areplyemail = "";
                $hvbu_user_name = $hvbu_user_email = "";
                $hvpm_user_name = $hvpm_user_email = "";
                $am2_replyemail = $am2_name = "";

                $role = 1;
                if(isset($invoice->invoice_user) && isset($invoice->invoice_user->name)){
                    $name = $invoice->invoice_user->name;
                    $replyemail = $invoice->invoice_user->email;
                    $role = $invoice->invoice_user->role;
                }
                if(isset($p->asset_manager) && isset($p->asset_manager->name)){
                    $aname = $p->asset_manager->name;
                    $areplyemail = $p->asset_manager->email;
                }
                if(isset($p->asset_manager_two) && isset($p->asset_manager_two->name)){
                    $am2_name = $p->asset_manager_two->name;
                    $am2_replyemail = $p->asset_manager_two->email;
                }
                if(in_array($p->id, $this->propertiesids()))
                {
                    $p21 = $this->getdata21();
                    $aname = $p21['name'];
                    $areplyemail = $p21['email'];
                }

                if($p->hvbu_id)
                {
                    $hvbuuser = User::find($p->hvbu_id);
                    if($hvbuuser)
                    {
                        $hvbu_user_name = $hvbuuser->name;
                        $hvbu_user_email = $hvbuuser->email;
                    }
                }

                if($p->hvpm_id)
                {
                    $hvpmuser = User::find($p->hvpm_id);
                    if($hvpmuser)
                    {
                        $hvpm_user_name = $hvpmuser->name;
                        $hvpm_user_email = $hvpmuser->email;
                    }
                }


                $check = PropertiesMailLog::where('type',$request->step)->where('record_id',$request->invoice_id)->where('tab','invoice')->first();
                if(!$check || $request->step=='request1' || $request->step=='request2') //here request1 means assetmanager and request2 means falk
                {
                    $modal = new PropertiesMailLog;
                    $modal->property_id = $id;
                    $modal->record_id = $request->invoice_id;
                    $modal->type = $request->step;
                    $modal->tab = 'invoice';
                    if($request->comment)
                        $modal->comment = $request->comment;
                    $modal->user_id = Auth::user()->id;
                    $modal->save();
                }

                $selected_user_name = "";
                if($invoice->email=="m.bitter@fcr-immobilien.de")
                    $selected_user_name = "Martin Bitter";
                if($invoice->email=="m.stavenow@fcr-immobilien.de")
                    $selected_user_name = "Michael Stavenow";
                if($invoice->email=="j.klausch@fcr-immobilien.de")
                    $selected_user_name = "Janine Klausch";



                $glink = "https://drive.google.com/drive/u/2/folders/".$invoice->file_basename;
                if($invoice->file_type == "file"){
                    $glink = "https://drive.google.com/file/d/".$invoice->file_basename;
                }
                $glink = '<a href="'.$glink.'">'.$glink.'</a>';
                $file_name = ($invoice->invoice) ? '('.$invoice->invoice.')' : '';


                $emails_array = array();
                $text = "";
                if(!$check)
                {
                    if($request->step=="release1" && is_numeric($invoice->email))
                    {

                        $firstuser = User::find($invoice->email);
                        if($firstuser)
                        {
                            $replyemailaddress = $firstuser->email;
                            $replyname = $firstuser->name;

                            $text = "Hallo ".$name.",<br> deine Rechnung ".$file_name." wurde von $replyname freigegeben. Link zum Intranet intern: ".$url;
                            $text .= "<br>Link zum Intranet extern: ".$extern_url;


                            $subject = "Rechnung vom ".show_date_format($invoice->created_at).' für '.$p->plz_ort.' '.$p->ort." wurde von $replyname freigegeben";
                        }


                    }
                    if($request->step=="release2")
                    {
                        $text = "Hallo ".$name.",<br> deine Rechnung ".$file_name." wurde von Falk freigegeben. Link zum Intranet intern: ".$url;

                        $text .= "<br>Link zum Intranet extern: ".$extern_url;


                        $replyemailaddress = config('users.falk_email');
                        $replyname = 'Falk';

                        $subject = "Rechnung vom ".show_date_format($invoice->created_at).' für '.$p->plz_ort.' '.$p->ort." wurde freigegeben";


                        $invoice->not_release_status = 0;
                        $invoice->save();
                    }
                    $cc_array = [];

                    if($text)
                    {
                        if($areplyemail){
                            $cc_array[] = $areplyemail;
                        }
                        if($am2_replyemail){
                            $cc_array[]= $am2_replyemail;
                        }
                        if($hvbu_user_email){
                            $cc_array[] = $hvbu_user_email;
                        }
                        if($hvpm_user_email){
                            $cc_array[] = $hvpm_user_email;
                        }

                        // $cc_array = array('yiicakephp+1@gmail.com','yiicakephp+2@gmail.com','yiicakephp+1@gmail.com');
                        // $replyemail = 'yiicakephp@gmail.com';

                        if($cc_array)
                            $cc_array = array_unique($cc_array);



                        if($invoice->comment)
                            $text .= "<br><br>Infos zur Rechnung: ".$invoice->comment;

                        if($request->comment)
                            $text .= "<br><br>Kommentar: ".$request->comment;


                        if($cc_array){

                            if( !in_array($replyemail, $this->mail_not_send()) ){

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$replyemail,$replyemailaddress,$replyname,$invoice,$gDriveFile,$cc_array) {
                                    $message->to($replyemail)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->attachData(\Storage::cloud()->get($invoice->file_basename), $invoice->invoice, [
                                        'mime' => $gDriveFile['mimetype']
                                    ])
                                    ->cc($cc_array)
                                    ->replyTo($replyemailaddress, $replyname)
                                    ->subject($subject);
                                });
                            }

                        }
                        else
                        {
                            if( !in_array($replyemail, $this->mail_not_send()) ){

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$replyemail,$replyemailaddress,$replyname,$invoice,$gDriveFile) {
                                    $message->to($replyemail)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->attachData(\Storage::cloud()->get($invoice->file_basename), $invoice->invoice, [
                                        'mime' => $gDriveFile['mimetype']
                                    ])
                                    ->replyTo($replyemailaddress, $replyname)
                                    ->subject($subject);
                                });
                            }
                        }
                    }


                }

                if($request->step=="request1" && $invoice->email && is_numeric($invoice->email))
                {
                    // $text = "Hallo $selected_user_name,<br> $name benötigt deine Freigabe bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die folgende Rechnung: $glink . Hier kannst du die Rechnung freigeben: ".$url;
                    if($invoice->not_release_status!=0)
                    {
                        $invoice->not_release_status = 0;
                        $invoice->save();
                    }

                    $firstuser = User::find($invoice->email);


                    if($firstuser)
                    {
                        $selected_user_name = $firstuser->name;
                        $text = "Hallo $selected_user_name,<br> $name benötigt deine Freigabe bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die folgende Rechnung ".$file_name.": Hier kannst du die Rechnung freigeben: ".$url;

                        if($invoice->comment)
                            $text .= "<br><br>Infos zur Rechnung: ".$invoice->comment;

                        if($request->comment)
                            $text .= "<br><br>Kommentar: ".$request->comment;



                        $email = $firstuser->email;
                        $subject = "Rechnungsfreigabe  ".$p->plz_ort.' '.$p->ort;

                        if( !in_array($email, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$invoice,$gDriveFile) {
                                $message->to($email)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($replyemail, $name)
                                ->subject($subject);
                            });
                        }
                    }
                }

                if($request->step=="request2")
                {
                    if($invoice->not_release_status!=0)
                    {
                        $invoice->not_release_status = 0;
                        $invoice->save();
                    }
                    echo "1"; die;

                    $text = "Hallo Falk,<br> $name benötigt deine Freigabe bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die folgende Rechnung ".$file_name.": Hier kannst du die Rechnung freigeben: ".$url;

                    if($invoice->comment)
                        $text .= "<br><br>Infos zur Rechnung: ".$invoice->comment;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $email = config('users.falk_email');

                    $subject = "Rechnungsfreigabe  ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$invoice,$gDriveFile) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail, $name)
                            ->subject($subject);
                        });
                    }
                    echo "111";
                }

            }
        }
        echo "1";
    }

    public function dealreleaseprocedure(Request $request)
    {

        $idsarr  = explode(',', $request->id);



        foreach ($idsarr as $key => $value) {

            if($request->step=='insurancetab_release')
            {
                $request->detail_id = $value;

                $d = PropertyInsuranceTabDetails::where('id',$value)->first();
                $value = $d->property_insurance_tab_id;
            }

            $request->invoice_id = $value;
            if($request->step)
            {
                $tab_insurance = $invoice = PropertyInsuranceTab::where('id',$request->invoice_id)->first();


                $title = $invoice->title;

                $id = $invoice->property_id;

                $url = route('properties.show',['property'=>$id]).'?tab=property_insurance_tab';
                $url = '<a href="'.$url.'">'.$url.'</a>';



                $p = Properties::find($id);
                $user = Auth::user();
                $aname = $name = $user->name;
                $replyemail = "";

                $pname = $preplyemail = "";
                $p2name = $p2replyemail = "";
                $hvpm_user_name = $hvpm_user_email = "";
                if(isset($p->asset_manager) && isset($p->asset_manager->name)){
                    $pname = $name = $p->asset_manager->name;
                    $preplyemail = $replyemail = $p->asset_manager->email;
                }
                if(isset($p->asset_manager_two) && isset($p->asset_manager_two->name)){
                    $p2name = $name = $p->asset_manager_two->name;
                    $p2replyemail = $replyemail = $p->asset_manager_two->email;
                }

                if($p->hvpm_id)
                {
                    $hvpmuser = User::find($p->hvpm_id);
                    if($hvpmuser)
                    {
                        $hvpm_user_name = $hvpmuser->name;
                        $hvpm_user_email = $hvpmuser->email;
                    }
                }


                $tab = "insurance_tab";
                if($request->step=="insurance_release")
                    $request->detail_id = $request->fk_id;
                else if($request->step=="insurancetab_release")
                {
                    $tab = "insurancetab2";
                }
                else
                    $request->detail_id = $request->am_id;

                if($request->detail_id==0){
                    echo $request->detail_id.'1'; die;
                }
                // echo "hihihi"; die;

                if($tab=='insurancetab2')
                $check = PropertiesMailLog::where('type',$request->step)->where('selected_id',$request->detail_id)->where('tab',$tab)->first();
                else
                $check = PropertiesMailLog::where('type',$request->step)->where('record_id',$request->invoice_id)->where('tab',$tab)->first();
                if(!$check || $request->step=='insurance_request')
                {
                    $modal = new PropertiesMailLog;
                    $modal->property_id = $id;
                    $modal->record_id = $request->invoice_id;
                    $modal->selected_id = $request->detail_id;
                    $modal->type = $request->step;
                    $modal->tab = $tab;
                    if($request->comment)
                        $modal->comment = $request->comment;
                    $modal->user_id = Auth::user()->id;
                    $modal->save();
                }

                $invoice = PropertyInsuranceTabDetails::where('id',$request->detail_id)->first();

                if(isset($invoice->asset_manager) && isset($invoice->asset_manager->name)){
                    $name = $invoice->asset_manager->name;
                    $replyemail = $invoice->asset_manager->email;
                }

                if(in_array($p->id, $this->propertiesids()))
                {
                    $p21 = $this->getdata21();
                    $name = $p21['name'];
                    $replyemail = $p21['email'];
                }
                // continue;


                $glink = "https://drive.google.com/drive/u/2/folders/".$invoice->file_basename;
                if($invoice->file_type == "file"){
                    $glink = "https://drive.google.com/file/d/".$invoice->file_basename;
                }
                $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                $text = "";
                if(!$check)
                {
                    if($request->step=="insurance_release" || $request->step=="insurancetab_release")
                    {

                        $tab_insurance->not_release_status = 0;
                        $tab_insurance->save();

                        // $invoice->not_release = 0;
                        // $invoice->save();

                        $text = "Hallo ".$name.",<br><br> dein Angebot ".$glink." wurde von Falk freigegeben. Link zum Intranet: ".$url;


                        $replyemailaddress = config('users.falk_email');
                        $replyname = 'Falk';

                    }
                    if($text)
                    {

                        $contents = collect(Storage::cloud()->listContents($invoice->file_dirname, false));
                        $gDriveFile = $contents->where('type','file')->where('basename', $invoice->file_basename)->first();

                        if($request->comment)
                            $text .= "<br><br>Kommentar: ".$request->comment;

                        $subject = "Angebot vom ".show_date_format($invoice->created_at).' für '.$p->plz_ort.' '.$p->ort." wurde freigegeben";

                        $cc_array = [];
                        if($preplyemail && $replyemail != $preplyemail){
                            $cc_array[] = $preplyemail;
                        }

                        if($p2replyemail && $p2replyemail != $replyemail && $p2replyemail != $preplyemail){
                            $cc_array[] = $p2replyemail;
                        }

                        if($hvpm_user_email)
                        {
                            $cc_array[] = $hvpm_user_email;
                        }
                        if($cc_array)
                            $cc_array = array_unique($cc_array);

                        if( !in_array($replyemail, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$replyemail,$replyemailaddress,$replyname,$gDriveFile,$invoice,$cc_array) {
                                $message->to($replyemail)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                 ->attachData(\Storage::cloud()->get($invoice->file_basename), $invoice->file_name, [
                                     'mime' => $gDriveFile['mimetype']
                                 ])
                                ->replyTo($replyemailaddress, $replyname)
                                ->subject($subject);

                                if(!empty($cc_array))
                                    $message->cc($cc_array);
                            });
                        }
                    }

                }

                if($request->step=="insurance_request")
                {
                    // $text = "Hallo Falk,<br> $name benötigt deine Freigabe bei dem Objekt ".$p->plz_ort.' '.$p->ort." für das folgende Angebot: $glink . Hier kannst du das Angebot freigeben: ".$url;


                    $update_data = DB::table('property_insurance_tab_details')->where('property_insurance_tab_id', $request->invoice_id)->update(['not_release' => 0]);


                    $others = PropertyInsuranceTabDetails::whereRaw('id!='.$request->detail_id.' and property_insurance_tab_id='.$request->invoice_id)->get();

                    $glink_array = array();

                    foreach ($others as $key => $value) {
                        $glink1 = "https://drive.google.com/drive/u/2/folders/".isset($value->file_basename) ? $value->file_basename : '';
                        if($value->file_type == "file"){
                            $glink1 = "https://drive.google.com/file/d/".isset($value->file_basename) ? $value->file_basename : '';
                        }
                        $glink1 = '<a href="'.$glink1.'">'.$glink1.'</a>';
                        $glink_array[] = $glink1;
                    }

                    $otherlinks = "";
                    if($glink_array)
                        $otherlinks = implode(' , ', $glink_array);

                    $text = "Hallo Falk,<br><br> $name benötigt deine Freigabe bei dem Objekt ".$p->plz_ort.' '.$p->ort." für eines der folgenden Angebote zum Thema $title: AM Empfehlung: $glink <br> weitere Angebote $otherlinks . <br> Hier kannst du das Angebot freigeben: ".$url;


                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $email = config('users.falk_email');
                    $subject = "Angebotsfreigabe  ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$invoice) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail, $name)
                            ->subject($subject);
                        });
                    }
                }

            }
        }
        echo "1";
    }


    public function index() {
        // echo "hhh";
        // pre("1");
        $perPages = 25;
        $prefix = "";
        $search = '1=1';

        if (isset($_REQUEST['q']) && $_REQUEST['q'] != '')
            $search = 'name_of_property like "%' . $_REQUEST['q'] . '%"';


        if (Auth::user()->isAdmin()) {
            // pre("1");
            if (isset($_REQUEST['bank_id']) && $_REQUEST['bank_id'] != '') {

                $prefix = "bank";
                $properties = Properties::where('Ist', $_REQUEST['bank_id'])
                        ->where('soll', 0)
                        ->where('main_property_id', '!=', 0)
                        ->whereRaw($search)
                        ->paginate($perPages);
            } else {
                // echo 'a'; die;
                $properties = Properties::where('masterliste_id', '=', null)
                        ->where('Ist', '!=', 0)
                        ->where('main_property_id', '!=', 0)
                        ->where('soll', 0)
                        ->whereRaw($search)
                        ->groupBy('main_property_id')
                        ->paginate($perPages);
            }
        } else {
            $user_id = Auth::id();
            $search = '(transaction_m_id='.$user_id.' OR seller_id='.$user_id.') ';
            if (isset($_REQUEST['q']) && $_REQUEST['q'] != '')
                $search .= ' and name_of_property like "%' . $_REQUEST['q'] . '%"';

            if (isset($_REQUEST['bank_id']) && $_REQUEST['bank_id'] != '') {
                $prefix = "bank";
                $properties = Properties::where('Ist', $_REQUEST['bank_id'])
                                ->whereRaw($search)
                                ->where('main_property_id', '!=', 0)
                                ->where('soll', 0)->paginate($perPages);
            } else {
                $properties = Properties::where([['masterliste_id', '=', null]])->where('Ist', '!=', 0)
                        ->where('soll', 0)
                        ->whereRaw($search)
                        ->where('main_property_id', '!=', 0)
                        ->groupBy('main_property_id')
                        ->paginate($perPages);
            }
        }
        $banks = Banks::all();

        return view('properties.' . $prefix . 'index', compact('properties', 'banks'));
    }

    public function einkaufsendmail(Request $request) {
        $email = "f.raudies@fcr-immobilien.de";
        // $email = "yiicakephp@gmail.com";
        //$email = "c.wiedemann@fcr-immobilien.de";
        $replyemail = "info@intranet.fcr-immobilien.de";
        $name = "Falk Raudies";

        $id = $request->property_id;

        $p = Properties::find($request->property_id);

        $kaufprice = $this->geteinkauftable($p);

        if ($request->release == 1) {

            $released = PropertiesBuyDetail::where('property_id', $request->property_id)->where('type', 'btn4')->first();
            if(!$released)
            {
                $modal = new PropertiesBuyDetail;
                $modal->property_id = $request->property_id;
                $modal->type = 'btn4';
                $modal->comment = "";
                if($request->comment)
                $modal->comment = $request->comment;
                $modal->user1 = Auth::user()->id;
                $modal->save();
            }
            else{
                echo "-1";
                die;
            }



            $user = Auth::user();
            $check = PropertiesBuy::where('property_id', $request->property_id)->first();
            if ($check)
                $ps_data = $check;
            else
                $ps_data = new PropertiesBuy;
            $ps_data->property_id = $request->property_id;
            $ps_data->released_by = $user->first_name . ' ' . $user->last_name;
            $ps_data->sale_date = date('Y-m-d');
            $ps_data->save();

            echo "1";


            $a = Properties::where('main_property_id', '=', $request->property_id)->where('Ist','!=',0)->first();

            $seller_id = 0;
            if ($a)
                $seller_id = $a->transaction_m_id;

            $name_of_property = $a->plz_ort.' '.$a->ort;

            // if ($ps_data && $ps_data->saller_id) {
            //     $seller_id = $ps_data->saller_id;
            // }

            // if ($ps_data && $ps_data->property_name) {
            //     $name_of_property = $ps_data->property_name;
            // }

            $v_name = "";
            if ($seller_id) {
                $vk = User::where('id', '=', $seller_id)->first();
                if ($vk) {
                    $replyemail = $vk->email;
                    $email = $vk->email;
                    $v_name = $vk->name;
                }
            }

            // $url = route('properties.show', ['property' => $request->property_id]);
            $url = route('properties.show',['property'=>$request->property_id]).'?tab=einkauf_tab';
            $url = '<a href="'.$url.'">'.$url.'</a>';


            if (!empty($v_name)) {
                try {
                    //$email = "c.wiedemann@fcr-immobilien.de";
                    // Mail::send('emails.einkauf_release_mail', ['v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url], function ($m) use ($email, $name,$replyemail,$v_name) {
                    //     $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                    //     $m->to($email, $name)->subject("Freigabe Einkaufsdatenblatt");
                    //     // $m->replyTo($replyemail, $v_name);
                    // });
                    // $email = "c.wiedemann@fcr-immobilien.de";
                    $text = 'Hallo '.$v_name.',<br> Falk hat das Objekt '.$name_of_property.' freigegeben: '.$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $text .= $kaufprice;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$text) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->subject('Freigabe Transaktionsentscheidung ');
                        });
                    }


                    $text = '⚡⚡⚡ Hallo Thorsten,<br> Falk hat das Objekt '.$name_of_property.' freigegeben: '.$url.'⚡⚡⚡';

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;

                    $email = "t.raudies@fcr-immobilien.de";

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$text) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->subject('⚡⚡⚡Freigabe Transaktionsentscheidung  ⚡⚡⚡');
                        });
                    }

                    $text = 'Hallo Andrea,<br> Falk hat das Objekt '.$name_of_property.' freigegeben: '.$url;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;

                    $email = "a.raudies@fcr-immobilien.de";

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$text) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->subject('Freigabe Transaktionsentscheidung ');
                        });
                    }

                    /*$text = 'Hallo Janine,<br> Falk hat das Objekt '.$name_of_property.' freigegeben: '.$url;
                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $text .= $kaufprice;

                    $email = "j.klausch@fcr-immobilien.de";
                    // $email = "c.wiedemann@fcr-immobilien.de";
                    Mail::send('emails.general', ['text' => $text], function ($message) use($email,$text) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->subject('Freigabe Transaktionsentscheidung ');
                    });*/


                } catch (\Exception $e) {

                }
            }


            die;
        }


        $modal = new PropertiesBuyDetail;
        $modal->property_id = $request->property_id;
        $modal->type = 'btn4_request';
        $modal->comment = "";
        if($request->comment)
        $modal->comment = $request->comment;
        $modal->user1 = Auth::user()->id;
        $modal->save();


        $a = Properties::where('main_property_id', '=', $request->property_id)->where('Ist','!=',0)->first();
        $ps_data = PropertiesBuy::where('property_id', $request->property_id)->first();

        $seller_id = 0;
        if ($a)
            $seller_id = $a->transaction_m_id;

        $name_of_property = $a->plz_ort.' '.$a->ort;

        // if ($ps_data && $ps_data->saller_id) {
        //     $seller_id = $ps_data->saller_id;
        // }

        $v_name = "";
        if ($seller_id) {
            $vk = User::where('id', '=', $seller_id)->first();
            if ($vk){
                $replyemail = $vk->email;
                $v_name = $vk->name;
            }
        }

        // $url = route('properties.show', ['property' => $request->property_id]).'?tab=einkauf_tab';

        $url = route('properties.show',['property'=>$request->property_id]).'?tab=einkauf_tab';
        $url = '<a href="'.$url.'">'.$url.'</a>';
        $comment = "";
        if($request->comment)
            $comment = $request->comment;

        $comment .= $kaufprice;

        $subject = "Freigabe Transaktionsentscheidung ".$name_of_property;

        try {

            if( !in_array($email, $this->mail_not_send()) ){

                Mail::send('emails.einkauf_info_mail', ['data' => $a, 'ps_data' => $ps_data, 'v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url,'comment'=>$comment], function ($m) use ($email, $name,$replyemail,$v_name,$subject) {
                    $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                    $m->to($email, $name)->subject($subject);
                    $m->replyTo($replyemail, $v_name);
                });
            }
        } catch (\Exception $e) {

        }
        echo "message is sent";
    }

    public function einkaufsendmail2(Request $request) {
        $email = "f.raudies@fcr-immobilien.de";
        // $email = "yiicakephp@gmail.com";
        // $email = "c.wiedemann@fcr-immobilien.de";
        $name = "Falk Raudies";
        if ($request->release == 1) {
            $user = Auth::user();
            $check = PropertiesBuy::where('property_id', $request->property_id)->first();
            if ($check)
                $ps_data = $check;
            else
                $ps_data = new PropertiesBuy;
            $ps_data->property_id = $request->property_id;
            $ps_data->released_by2 = $user->first_name . ' ' . $user->last_name;
            $ps_data->sale_date2 = date('Y-m-d');
            $ps_data->save();

            echo "1";


            $a = Properties::where('main_property_id', '=', $request->property_id)->where('Ist','!=',0)->first();

            $seller_id = 0;
            if ($a)
                $seller_id = $a->transaction_m_id;

            $name_of_property = $a->name_of_property;

            if ($ps_data && $ps_data->saller_id) {
                $seller_id = $ps_data->saller_id;
            }

            if ($ps_data && $ps_data->property_name) {
                $name_of_property = $ps_data->property_name;
            }

            $v_name = "";
            if ($seller_id) {
                $vk = User::where('id', '=', $seller_id)->first();
                if ($vk) {
                    //$email = $vk->email;
                    $v_name = $vk->name;
                }
            }

            $url = route('properties.show', ['property' => $request->property_id]);
            if (!empty($v_name)) {
                try {
                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.ankauf_release_mail', ['v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url], function ($m) use ($email, $name) {
                            $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                            $m->to($email, $name)->subject("Freigabe Ankauf");
                        });
                    }
                } catch (\Exception $e) {

                }


                $text = 'Hallo Thorsten, Falk hat das Objekt '.$name_of_property.' freigegeben: '.$url;
                $email = "t.raudies@fcr-immobilien.de";

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::raw($text, function ($message) use($email,$text) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->subject('Freigabe Ankauf');
                    });
                }

                $text = 'Hallo Andrea, Falk hat das Objekt '.$name_of_property.' freigegeben: '.$url;
                $email = "a.raudies@fcr-immobilien.de";

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::raw($text, function ($message) use($email,$text) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->subject('Freigabe Ankauf');
                    });
                }

                /*$text = 'Hallo Janine, Falk hat das Objekt '.$name_of_property.' freigegeben: '.$url;
                $email = "j.klausch@fcr-immobilien.de";
                // $email = "c.wiedemann@fcr-immobilien.de";
                Mail::raw($text, function ($message) use($email,$text) {
                    $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->subject('Freigabe Ankauf');
                });*/

            }


            die;
        }

        $a = Properties::where('main_property_id', '=', $request->property_id)->where('Ist','!=',0)->first();



        $ps_data = PropertiesBuy::where('property_id', $request->property_id)->first();

        $seller_id = 0;
        if ($a)
            $seller_id = $a->seller_id;

        $name_of_property = $a->name_of_property;

        if ($ps_data && $ps_data->saller_id) {
            $seller_id = $ps_data->saller_id;
        }

        if ($ps_data && $ps_data->property_name) {
            $name_of_property = $ps_data->property_name;
        }

        $v_name = "";
        if ($seller_id) {
            $vk = User::where('id', '=', $seller_id)->first();
            if ($vk)
                $v_name = $vk->name;
        }

        // $url = route('properties.show', ['property' => $request->property_id])
        $url = route('properties.show', ['property' => $request->property_id]).'?tab=einkauf_tab';



        try {
            if( !in_array($email, $this->mail_not_send()) ){

                Mail::send('emails.ankauf_info_mail', ['data' => $a, 'ps_data' => $ps_data, 'v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url], function ($m) use ($email, $name) {
                    $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                    $m->to($email, $name)->subject("Freigabe Ankauf");
                });
            }
        } catch (\Exception $e) {

        }
        echo "message is sent";
    }

    public function sendmail(Request $request) {
        $email = "f.raudies@fcr-immobilien.de";
        // $email = "c.wiedemann@fcr-immobilien.de";
        $comment = "";
        if($request->comment)
            $comment = $request->comment;


        $p = Properties::find($request->property_id);
        $kaufprice = $this->getverkauftable($p);
        $comment .= $kaufprice;

        // $email = "yiicakephp@gmail.com";

        if($request->release == 1)
            $type = 'btn4';
        else
            $type = 'btn4_request';

        $check = PropertiesSaleDetail::where('property_id', $request->property_id)->where('type', $type)->first();

        if(!$check)
        {
            $modal1 = new PropertiesSaleDetail;
            $modal1->property_id = $request->property_id;
            if($request->release == 1)
                $modal1->type = 'btn4';
            else
                $modal1->type = 'btn4_request';
            if($request->comment)
                $modal1->comment = $request->comment;
            else
                $modal1->comment = "";
            $modal1->user1 = Auth::user()->id;
            $modal1->save();
        }


        $modal = new PropertiesBuyDetail;
        $modal->property_id = $request->property_id;
        $modal->type = $request->step;
        if($request->comment)
            $modal->comment = $request->comment;
        else
            $modal->comment = "";
        $modal->user1 = Auth::user()->id;
        $modal->save();

        $name = "Falk Raudies";
        if ($request->release == 1) {
            $user = Auth::user();
            $check = PropertiesSale::where('property_id', $request->property_id)->first();
            if ($check)
                $ps_data = $check;
            else
                $ps_data = new PropertiesSale;
            $ps_data->property_id = $request->property_id;
            $ps_data->released_by = $user->first_name . ' ' . $user->last_name;
            $ps_data->sale_date = date('Y-m-d');
            $ps_data->save();

            echo "1";




            $a = Properties::where('main_property_id', '=', $request->property_id)->where('Ist','!=',0)->first();

            $seller_id = 0;
            if ($a)
                $seller_id = $a->seller_id;

            // $name_of_property = $a->name_of_property;
             $name_of_property = $a->plz_ort.' '.$a->ort;


            if ($ps_data && $ps_data->saller_id) {
                $seller_id = $ps_data->saller_id;
            }

            // if ($ps_data && $ps_data->property_name) {
            //     $name_of_property = $ps_data->property_name;
            // }

            $v_name = "";
            if ($seller_id) {
                $vk = User::where('id', '=', $seller_id)->first();
                if ($vk) {
                    $v_name = $vk->name;
                    $email = $vk->email;
                }
            }
            $replyemail = "f.raudies@fcr-immobilien.de";

            // $url = route('properties.show', ['property' => $request->property_id]);

            $url = route('properties.show',['property'=>$request->property_id]).'?tab=verkauf_tab';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $subject = "Freigabe Transaktionsentscheidung ".$p->plz_ort.' '.$p->ort;


            $email = "";
            if(isset($p->transaction_manager) && isset($p->transaction_manager->name)){
                $v_name = $p->transaction_manager->name;
                $email = $p->transaction_manager->email;
            }
            // $email = "yiicakephp@gmail.com";
            if ( !empty($email) && !in_array($email, $this->mail_not_send()) ) {
                try {
                    Mail::send('emails.release_mail', ['v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url,'comment'=>$comment], function ($m) use ($email,$v_name, $name,$replyemail,$subject) {
                        $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                        $m->replyTo($replyemail, 'Falk');
                        $m->to($email, $v_name)->subject($subject);
                    });
                } catch (\Exception $e) {

                }
            }


            $email = "t.raudies@fcr-immobilien.de";
            $v_name = 'Thorsten';
            // $email = "yiicakephp@gmail.com";
            // $email = "c.wiedemann@fcr-immobilien.de";
            if ( !empty($v_name) && !in_array($email, $this->mail_not_send()) ) {
                try {
                    Mail::send('emails.release_mail', ['v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url,'comment'=>$comment], function ($m) use ($email,$v_name, $name,$replyemail,$subject) {
                        $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                        $m->replyTo($replyemail, 'Falk');
                        $m->to($email, $v_name)->subject($subject);
                    });
                } catch (\Exception $e) {

                }
            }


            $email = "j.klausch@fcr-immobilien.de";
            $v_name = 'Janine';
            // $email = "yiicakephp@gmail.com";
            // $email = "c.wiedemann@fcr-immobilien.de";
            // $email = "yiicakephp@gmail.com";
            if ( !empty($v_name) && !in_array($email, $this->mail_not_send()) ) {
                try {
                    Mail::send('emails.release_mail', ['v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url,'comment'=>$comment], function ($m) use ($email,$v_name, $name,$replyemail,$subject) {
                        $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                        $m->replyTo($replyemail, 'Falk');
                        $m->to($email, $v_name)->subject($subject);
                    });
                } catch (\Exception $e) {

                }
            }

            $email = "a.raudies@fcr-immobilien.de";
            $v_name = 'Andrea';
            // $email = "yiicakephp@gmail.com";
            // $email = "c.wiedemann@fcr-immobilien.de";
            // $email = "c.wiedemann@fcr-immobilien.de";
            if ( !empty($v_name) && !in_array($email, $this->mail_not_send()) ) {
                try {
                    Mail::send('emails.release_mail', ['v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url,'comment'=>$comment], function ($m) use ($email,$v_name, $name,$replyemail,$subject) {
                        $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                        $m->replyTo($replyemail, 'Falk');
                        $m->to($email, $v_name)->subject($subject);
                    });
                } catch (\Exception $e) {

                }
            }

            $email = "a.lauterbach@fcr-immobilien.de";
            $v_name = 'Alex';
            // $email = "yiicakephp@gmail.com";
            // $email = "c.wiedemann@fcr-immobilien.de";
            // $email = "c.wiedemann@fcr-immobilien.de";
            if (!empty($v_name) && !in_array($email, $this->mail_not_send())) {
                try {
                    Mail::send('emails.release_mail', ['v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url], function ($m) use ($email,$v_name, $name,$replyemail,$subject) {
                        $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                        $m->replyTo($replyemail, 'Falk');
                        $m->to($email, $v_name)->subject($subject);
                    });
                } catch (\Exception $e) {

                }
            }


            die;
        }

        $a = Properties::where('main_property_id', '=', $request->property_id)->where('Ist','!=',0)->first();
        $ps_data = PropertiesSale::where('property_id', $request->property_id)->first();

        $seller_id = 0;
        if ($a)
            $seller_id = $a->seller_id;

        // $name_of_property = $a->name_of_property;
        $name_of_property = $a->plz_ort.' '.$a->ort;

        if ($ps_data && $ps_data->saller_id) {
            $seller_id = $ps_data->saller_id;
        }

        // if ($ps_data && $ps_data->property_name) {
        //     $name_of_property = $ps_data->property_name;
        // }

        $v_name = "";
        if ($seller_id) {
            $vk = User::where('id', '=', $seller_id)->first();
            if ($vk)
                $v_name = $vk->name;
        }


        $url = route('properties.show',['property'=>$request->property_id]).'?tab=verkauf_tab';
        $url = '<a href="'.$url.'">'.$url.'</a>';

        //$email = "c.wiedemann@fcr-immobilien.de";
        //$email = "yiicakephp@gmail.com";

        if( !in_array($email, $this->mail_not_send()) ){
            try {
                Mail::send('emails.info_mail', ['data' => $a, 'ps_data' => $ps_data, 'v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url,'comment'=>$comment], function ($m) use ($email, $name) {
                    $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                    $m->to($email, $name)->subject("Freigabe Verkaufsobjekt");
                });
            } catch (\Exception $e) {

            }
        }
        echo "message is sent";
    }

    /////////// Mietflachen
    public function mietflachen(Request $request) {

        $data_form = ([
            "property_id" => $request->property_id,
            "street" => $request->street,
            "zip" => $request->zip,
            "city" => $request->city,
            "rent" => $request->rent,
            "demand_price" => $request->demand_price,
            "total_area" => $request->total_area,
            "selling_area" => $request->selling_area,
            "renting_area" => $request->renting_area,
            "year_construction" => $request->year_construction
        ]);

        $check_property_id = DB::table('mietflächen')->where('property_id', $request->property_id)->first();

        if ($check_property_id == null) {

            $insert_data = DB::table('mietflächen')->insert($data_form);

            if ($insert_data) {
                return redirect()->back()->with('message', 'Data inserted successfully');
            } else {
                return redirect()->back()->with('message', 'Something went wrong!');
            }
        } else {
            $update_data = DB::table('mietflächen')->where('property_id', $request->property_id)
                    ->update([
                "street" => $request->street,
                "zip" => $request->zip,
                "city" => $request->city,
                "rent" => $request->rent,
                "demand_price" => $request->demand_price,
                "total_area" => $request->total_area,
                "selling_area" => $request->selling_area,
                "renting_area" => $request->renting_area,
                "year_construction" => $request->year_construction
            ]);


            if ($update_data) {
                return redirect()->back()->with('message', 'Data Updated successfully');
            } else {
                return redirect()->back()->with('message', 'Something went wrong!');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('properties.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function comparsion() {
        if (!Auth::user()->isAdmin()) {
            return redirect('/');
        }
        $status = config('properties.status.buy');
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        }

        $type = 'normal';
        if (isset($_GET['type'])) {
            $type = $_GET['type'];
        }
        //TODO: check is_extra field

        if ($type == 'normal') {
            if ($status) {
                $properties = Properties::where('status', $status)->where('is_extra', false)->where('Ist', 0)->where('soll', 0)->get();
            } else {
                $properties = Properties::where('Ist', 0)->where('soll', 0)->get();
            }
        } else if ($type == 'extra') {
            $properties = Properties::where('is_extra', true)->where('Ist', 0)->where('soll', 0)->get();
        } else {
            $properties = Properties::where('Ist', 0)->where('soll', 0)->get();
        }

        $total_purchase = PropertiesService::calculatePurchaseTotal($properties);

        $all_properties = Properties::where('Ist', 0)->where('soll', 0)->get();

        return view('properties.comparsion', compact('properties', 'status', 'total_purchase', 'all_properties', 'type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $user_id = Auth::id();
        var_dump($user_id);
        $input = $request->all();
        $input['name_of_creator'] = User::where('id', '=', $user_id)->first()->name;

        if ($property = PropertiesService::create($input)) {

            $notification_data = [
                'type' => config('notification.type.create_property'),
                'property_id' => $property->id
            ];
            NotificationsService::create($notification_data);
            return redirect()->action('PropertiesController@index')
                            ->with('message', trans('property.properties_controller.created_success'));
        }

        return redirect()->action('PropertiesController@index')
                        ->with('error', trans('property.properties_controller.created_fail'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addNewVacant(Request $request)
    {
        $model = new PropertyVacant;
        $model->property_id = $request->property_id;
        $model->type = 3;
        if($request->has('type') && $request->type)
            $model->mv_vl = $request->type;
        $model->save();

        $modal = new EmpfehlungDetail;
        $modal->property_id = $request->property_id;
        $modal->user_id = Auth::user()->id;
        $modal->tenant_id = $model->id;
        $modal->slug = 'add_vacant';
        $modal->value = $model->id;
        $modal->save();

        if($request->has('type') && $request->type){
            $modal = new EmpfehlungDetail;
            $modal->property_id = $request->property_id;
            $modal->user_id = Auth::user()->id;
            $modal->tenant_id = $model->id;
            $modal->slug = 'mv_vl';
            $modal->value = $request->type;
            $modal->save();
        }


        // $url = route('properties.show',['property'=>$request->property_id]).'?tab=recommended_tab';

        // return redirect($url);
        return redirect()->back();
    }
    public function delete_new_vacant(Request $request)
    {
        $v = PropertyVacant::where('id',$request->id)->first();


        $modal = new EmpfehlungDetail;
        $modal->property_id = $v->property_id;
        $modal->user_id = Auth::user()->id;
        $modal->tenant_id = $request->id;
        $modal->slug = 'delete_vacant';
        $modal->value = $request->id;
        $modal->save();

        // $v->delete();
        echo '1'; die;
    }

    public function export_properties(Request $request) {
        ini_set('max_execution_time', 300);

        // script for update invoice new added button
        $invoiceRes = DB::table('property_invoices as pi')
                ->selectRaw('
                    pi.id,
                    pi.not_release_status,
                    am_status,
                    user_status,
                    falk_status,
                    last_process_type,
                    ( SELECT pml_falk.type FROM properties_mail_logs as pml_falk WHERE pml_falk.tab = "invoice" AND pml_falk.type IN("request2", "release2", "pending_invoice", "notrelease_invoice") AND pml_falk.record_id = pi.id ORDER BY pml_falk.created_at DESC LIMIT 1 ) as falk_type,
                    ( SELECT pml_user.type FROM properties_mail_logs as pml_user WHERE pml_user.tab = "invoice" AND pml_user.type IN("request1", "release1") AND pml_user.record_id = pi.id ORDER BY pml_user.created_at DESC LIMIT 1 ) as user_type,
                    ( SELECT pml_inv.type FROM properties_mail_logs as pml_inv WHERE pml_inv.tab = "invoice" AND pml_inv.record_id = pi.id ORDER BY pml_inv.created_at DESC LIMIT 1 ) as last_type
                ')
                ->groupBy('pi.id')
                ->get();

        if($invoiceRes){
            foreach ($invoiceRes as $key => $invoice) {

                $falk_status = 0;
                if($invoice->falk_type == 'request2'){
                    $falk_status = 1;
                }elseif ($invoice->falk_type == 'release2') {
                    $falk_status = 2;
                }elseif ($invoice->falk_type == 'pending_invoice') {
                    $falk_status = 3;
                }elseif ($invoice->falk_type == 'notrelease_invoice') {
                    $falk_status = 4;
                }

                $user_status = $am_status = 0;
                if($invoice->user_type == 'request1'){
                    $user_status = 1;
                }elseif ($invoice->user_type == 'release1') {
                    $user_status = 2;
                }

                if($invoice->not_release_status == 3){
                    $am_status = 4;
                }elseif ($invoice->not_release_status == 4) {
                    $am_status = 3;
                }

                $last_process_type = '';

                if($invoice->last_type){
                    if( in_array($invoice->last_type, ['request_invoice_am', 'release_invoice_am', 'pending_invoice_am', 'not_release_invoice_am', 'pending_am_invoice', 'reject_invoice']) ){
                        $last_process_type = 'am_status';
                    }elseif ( in_array($invoice->last_type, ['request_invoice_hv', 'release_invoice_hv', 'pending_invoice_hv', 'not_release_invoice_hv']) ) {
                        $last_process_type = 'hv_status';
                    }elseif ( in_array($invoice->last_type, ['request1', 'release1', 'pending_invoice_user', 'not_release_invoice_user']) ) {
                        $last_process_type = 'user_status';
                    }elseif ( in_array($invoice->last_type, ['request2', 'release2', 'pending_invoice', 'notrelease_invoice']) ) {
                        $last_process_type = 'falk_status';
                    }
                }

                if($invoice->am_status == 0 || $invoice->user_status == 0 || $invoice->falk_status == 0 || (!$invoice->last_process_type && $last_process_type)){

                    $modal = PropertyInvoices::find($invoice->id);

                    if($invoice->am_status == 0)
                        $modal->am_status = $am_status;

                    if($invoice->user_status == 0)
                        $modal->user_status = $user_status;

                    if($invoice->falk_status == 0)
                        $modal->falk_status = $falk_status;

                    if($last_process_type)
                        $modal->last_process_type = $last_process_type;

                    $modal->save();
                }

            }
        }

        echo 'success for new added button status update';exit;

        $items = DB::table('tenancy_schedule_items as tsi')
                    ->select('tsi.id', 'tsi.comment3', 'p.asset_m_id', 'u.name')
                    ->join('tenancy_schedules as ts', 'ts.id', '=', 'tsi.tenancy_schedule_id')
                    ->join('properties as p', 'p.id', '=', 'ts.property_id')
                    ->leftjoin('users as u', 'u.id', '=', 'p.asset_m_id')
                    ->whereRaw('(tsi.comment3 IS NOT NULL AND tsi.comment3 != "")')
                    ->get();
        if($items){
            foreach ($items as $item) {
                $modal = new \App\TenancyScheduleComment;
                $modal->item_id = $item->id;
                $modal->comment = $item->comment3;
                $modal->user_name = $item->name;
                $modal->user_id = $item->asset_m_id;
                $modal->created_at = date('Y-m-d H:i:s', strtotime('2020-03-15 08:00:00'));
                $modal->save();
            }
        }
        echo 'success';exit;

        $items = DB::table('tenancy_schedule_items')->select('id', 'comment', 'comment2', 'updated_at')->whereRaw('( (comment IS NOT NULL AND comment != "") OR (comment2 IS NOT NULL AND comment2 != "") )')->get();
        if($items){
            foreach ($items as $item) {
                if($item->comment){
                    $modal = new \App\TenancyScheduleComment;
                    $modal->item_id = $item->id;
                    $modal->comment = $item->comment;
                    $modal->created_at = $item->updated_at;
                    $modal->save();
                }

                if($item->comment2){
                    $modal = new \App\TenancyScheduleComment;
                    $modal->item_id = $item->id;
                    $modal->comment = $item->comment2;
                    $modal->created_at = $item->updated_at;
                    $modal->save();
                }
            }
        }
        echo 'success';exit;

        $contracts = DB::table('property_contracts')->where('comment2', '!=', '')->whereNotNull('comment2')->get();

        if($contracts){
            foreach ($contracts as $key => $value) {
                $modal = new PropertiesComment;
                $modal->property_id = $value->property_id;
                $modal->user_id     = 1;
                $modal->comment     = $value->comment2;
                $modal->record_id   = $value->id;
                $modal->type        = 'property_contracts';
                $modal->created_at  = $value->created_at;
                $modal->updated_at  = $value->updated_at;
                $modal->save();

            }
        }

        echo 'success';exit;

        $default_payer = DB::table('properties_default_payers')->where('comment', '!=', '')->whereNotNull('comment')->get();

        if($default_payer){
            foreach ($default_payer as $key => $value) {
                $modal = new PropertiesComment;
                $modal->property_id = $value->property_id;
                $modal->user_id     = $value->user_id;
                $modal->comment     = $value->comment;
                $modal->record_id   = $value->id;
                $modal->type        = 'properties_default_payers';
                $modal->created_at  = $value->created_at;
                $modal->updated_at  = $value->updated_at;
                $modal->save();

            }
        }

        echo 'success';exit;

        // update property contract comment
        $contracts = DB::table('empfehlung_details')->groupby('tenant_id')->get();
        if($contracts){
            foreach ($contracts as $key => $value) {

                $em = Empfehlung::where('tenant_id',$value->tenant_id)->first();

                if(!$em)
                {
                    $m = new Empfehlung;
                    $m->tenant_id = $value->tenant_id;
                    $m->property_id = $value->property_id;
                    $m->deleted = 0;
                    $m->save();
                }
                else{
                    $em->deleted = 0;
                    $em->save();
                }

            }
        }

        echo 'success';exit;

        // update property invoice comment 2
        $invoice = DB::table('property_invoices')->where('comment2', '!=', '')->whereNotNull('comment2')->groupby('id')->get();
        // pre($invoice->count());//1723

        if($invoice){
            foreach ($invoice as $key => $value) {
                $modal = new PropertiesComment;
                $modal->property_id = $value->property_id;
                $modal->user_id     = $value->user_id;
                $modal->comment     = $value->comment2;
                $modal->record_id   = $value->id;
                $modal->type        = 'property_invoices';
                $modal->created_at  = $value->created_at;
                $modal->updated_at  = $value->updated_at;
                $modal->save();

            }
        }

        echo 'success';exit;

        // update property invoice comment
        $invoice = DB::table('property_invoices')->where('comment', '!=', '')->whereNotNull('comment')->groupby('id')->get();

        if($invoice){
            foreach ($invoice as $key => $value) {
                $modal = new PropertiesComment;
                $modal->property_id = $value->property_id;
                $modal->user_id     = $value->user_id;
                $modal->comment     = $value->comment;
                $modal->record_id   = $value->id;
                $modal->type        = 'property_invoices';
                $modal->created_at  = $value->created_at;
                $modal->updated_at  = $value->updated_at;
                $modal->save();

            }
        }

        echo 'success';exit;

        $liquiplanung = DB::table('properties_custom_fields')->whereIn('slug', ['extra_cost_pm', 'ao_cost_month1', 'ao_cost_month2'])->where('content', '!=', '')->whereNotNull('content')->get();
        if($liquiplanung){
            foreach ($liquiplanung as $key => $value) {

                if($value->slug == 'extra_cost_pm'){
                    $title = 'Außerordentliche Kosten p.m.';
                    $month = '07';
                    $year  = '2020';
                }elseif($value->slug == 'ao_cost_month1'){
                    $title = 'ao Kosten Monat +1';
                    $month = '08';
                    $year  = '2020';
                }else{
                    $title = 'ao Kosten Monat +2';
                    $month = '09';
                    $year  = '2020';
                }

                $modal              = new LiquiplanungAoCosts;
                $modal->property_id = $value->property_id;
                $modal->user_id     = 1;
                $modal->title       = $title;
                $modal->amount      = $value->content;
                $modal->month       = $month;
                $modal->year        = $year;
                $modal->created_at  = $value->created_at;
                $modal->updated_at  = $value->updated_at;
                $modal->save();
            }
        }

        echo 'success';exit();

        $items = TenancyScheduleItem::select('tenancy_schedule_items.*','property_id')->join('tenancy_schedules','tenancy_schedules.id','=','tenancy_schedule_items.tenancy_schedule_id')->get();

        // pre($items);

        foreach ($items as $key => $value) {
            # code...
            if(!$value->rental_space)
                $value->rental_space = $value->vacancy_in_qm;

            $check = TenantMaster::where('item_name',$value->name)->where('area_space',$value->rental_space)->where('property_id',$value->property_id)->first();

            if(!$check)
            {
                $modal =  new TenantMaster;
                $modal->property_id = $value->property_id;
                $modal->item_name = $value->name;
                $modal->item_type = $value->type;
                $modal->area_space = $value->rental_space;
                $modal->save();

                $id = $modal->id;
            }
            else{
                $id = $check->id;
            }
            DB::table('tenancy_schedule_items')->where('id', $value->id)->update(['tenant_master_id' => $id]);

        }
        echo "done";exit;

        $update_date = 1;

        $d = TenancySchedule::where('property_id','!=',0)->get();
        // pre($d);
        foreach($d as $value)
        $tenancy_schedule_data = TenancySchedulesService::get($value->property_id, $update_date);

        die;


        $detail = DB::table('property_insurance_tab_details as pid')->select('pid.*', 'pit.property_id')
                ->join('property_insurance_tabs as pit', 'pit.id', '=', 'pid.property_insurance_tab_id')
                ->whereNotNull('pid.comment')
                ->get();
        if($detail){
            foreach ($detail as $key => $value) {
                $modal = new PropertiesComment;
                $modal->property_id = $value->property_id;
                $modal->user_id = $value->user_id;
                $modal->comment = $value->comment;
                $modal->record_id = $value->id;
                $modal->type = 'property_insurance_tab_details';
                $modal->created_at = $value->created_at;
                $modal->updated_at = $value->updated_at;
                $modal->save();
            }
        }
        echo 'success';exit;

       /* $data = Properties::where('main_property_id',0)->get();
        foreach ($data as $key => $value) {
            TenancySchedulesService::get($value->id,0);
        }*/
        echo 'done';
    }


    public function addBankenContactEmployee(Request $request){
        if($request->ajax()) {
            $rules = array(
                'banken_id' => 'required',
                'vorname' => 'required',
                'nachname' => 'required',
                'telefon' => 'required',
                'email' => 'required|email',
            );

            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

                $modal = new BankenContactEmployee;
                $modal->banken_id = $request->banken_id;
                $modal->vorname = $request->vorname;
                $modal->nachname = $request->nachname;
                $modal->telefon = $request->telefon;
                $modal->email = $request->email;
                if($modal->save()){
                    $result = ['status' => true, 'message' => 'Conatact save successfully.', 'data' => []];
                }else{
                    $result = ['status' => false, 'message' => 'Conatact save fail! Try again.', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);

    }

    public function getbanklist(Request $request) {
        $id = $request->id;
        $data = [];
        $dataArr = PropertiesBanken::join('banken','banken.Kennung','=','banken_id')
        // ->join('banken_contact_employee', 'banken_contact_employee.banken_id', '=', 'banken.Kennung')
        ->where('property_id',$id)->get();

        foreach($dataArr as $row){

            $currntRow = $row->toArray();
            // dd(collect($currntRow)->Firma);
            $data[] = (object)$currntRow;

            $contacts = BankenContactEmployee::where('banken_id', $currntRow['banken_id'])->get();
            foreach($contacts as $chield){
                $currntRow['Vorname'] = $chield->vorname;
                $currntRow['Nachname'] = $chield->nachname;
                $currntRow['Telefon'] = $chield->telefon;
                $currntRow['E_Mail'] = $chield->email;
                $data[] = (object)$currntRow;
            }

        }
        $a = View::make('properties.banklist', array('data' => $data))->render();
        return $a;
    }

    public function getstatusloi(Request $request) {
        $id = $request->id;
        $data = StatusLoi::selectRaw('status_loi.*,users.name, GROUP_CONCAT(status_loi.pdf SEPARATOR ",") as loi_pdf')->join('users', 'users.id', '=', 'status_loi.user_id')->where('property_id', $id)->get()->groupby('user_id');

        $a = View::make('properties.statusloilist', array('data' => $data))->render();
        return $a;
    }

    public function getStatusLoiByUserId(Request $request) {
        $data = StatusLoi::selectRaw('status_loi.*,users.name')->join('users', 'users.id', '=', 'status_loi.user_id')->where('property_id', $request->property_id)
            ->where('user_id', $request->id)->get();

        $a = View::make('properties.transaction_manager_status_loi', array('data' => $data))->render();
        return $a;
    }

    public function show($id) {

        $login_user = Auth::user();

        // $invoice = PropertyInvoices::find(8);
        // // $data = \Storage::cloud()->get($invoice->file_basename);
        // $text = "HELLO";
        // $subject = "INVOICE";
        // $email = "yiicakephp@gmail.com";
        // Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email,$invoice) {
        //               $message->to($email)
        //               ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
        //               ->attachData(\Storage::cloud()->get($invoice->file_basename), $invoice->invoice)
        //               ->subject($subject);
        //           });
        // die;

        // echo $data;
        //return response($data, 200)
        // ->header('ContentType', 'image/jpg')
         //->header('Content-Disposition', "attachment; filename=4.jpg");
        // pre($data);

        // echo "her"; die;
        //Get selecting property tabs

        $tab_types = [
                'properties',
                'expose',
            'tenant-listing', 'swot-template', 'schl-template', 'comment_tab', 'clever-fit-rental-offer', 'planung-berechnung-wohnen', 'tenancy-schedule', 'verkauf_tab', 'einkauf_tab', 'Leerstandsflächen', 'email_template', 'email_template2', 'Anfragen', 'finance', 'dateien','insurance_tab','sheet','budget','recommended_tab','provision_tab','property_invoice','property_management','property_deals','default_payer','property_insurance_tab','quick-sheet','bank-modal','contracts','vermietung','exportverkauf_tab','Anzeige_aufgeben','am_mail','project', 'feature_analysis', 'tenancy-schedule-new'];
        if (isset($_GET['tab'])) {
            $tab = $_GET['tab'];
            if (!in_array($tab, $tab_types)) {
                if($login_user->role == 4){
                    $tab = 'tenancy-schedule';
                }else{
                    $tab = 'properties';
                }
            }
        } else {
            if($login_user->role == 4){
                $tab = 'tenancy-schedule';
            }else{
                $tab = 'properties';
            }
        }

        $update_date = 1;
        if ($tab == "tenancy-schedule")
            $update_date = 0;


        $list = array();

        $properties1 = $properties = Properties::where('id', '=', $id)->where('Ist', 0)->where('soll', 0)->first();
        $users = User::all();
        //echo '<pre>';
        //print_r($properties1);
        //*******************code for Bankers email form
        $bankers_email_info = Properties::where('main_property_id', $id)->first();

        $total_ccc = 0;
        $ankdate1 = $ankdate2 = "";
        if($bankers_email_info)
        {
            $propertiesExtra1s = PropertiesTenants::where('propertyId', $bankers_email_info->id)->get();
            foreach ($propertiesExtra1s as $k => $propertiesExtra1) {
                $total_ccc += $propertiesExtra1->net_rent_p_a;
                if ($k == 0)
                    $ankdate1 = $propertiesExtra1->mv_end;
                if ($k == 1)
                    $ankdate2 = $propertiesExtra1->mv_end;
            }
        }
        Log::info($total_ccc);
        // echo $total_ccc; die;
        $properties->net_rent_pa = $total_ccc;

        $rental_period_until = null;
        if (isset($propertiesExtra1s[0]['tenant']))
            $rental_period_until = $propertiesExtra1s[0]['tenant'] . ' ';

        $properties->the_main_tenant = $rental_period_until;
        $properties->rental_period_until = $ankdate1;
        //echo'<pre>';print_r($propertiesExtra1s);die;
        //properties againts Ist and Soll
        if ($properties->bank_ids) {
            $final_bank_ids = json_decode($properties->bank_ids, true);

            if (count($final_bank_ids) > 0) {



                $index = 0;
                for ($i = 0; $i < count($final_bank_ids); $i++) {

                    $bank_id = $final_bank_ids[$i];
                    if ($i == 0) {
                        $properties1 = Properties::where('Ist', '=', $bank_id)->where('soll', 0)->first();

                        if ($properties1 && !$properties->niedersachsen && $properties1->niedersachsen) {
                            $properties->niedersachsen = $properties1->niedersachsen;
                        }
                    }

                    $ist_sheet = Properties::select('name_of_property', 'id', 'standard_property_status')->where('Ist', '=', $bank_id)->where('main_property_id', $id)->first();
                    $soll_sheet = Properties::select('name_of_property', 'id', 'standard_property_status')->where('soll', '=', $bank_id)->where('main_property_id', $id)->first();
                    $bank = DB::table('banks')->select('name')->where('id', $bank_id)->first();

                    if (!$ist_sheet || !$soll_sheet)
                        continue;





                    if ($bank) {
                        if (isset($list[$bank_id . '_' . $bank->name])) {

                            $list[$bank_id . '_' . $bank->name]['ist']['id'] = $ist_sheet->id;
                            $list[$bank_id . '_' . $bank->name]['ist']['name'] = $ist_sheet->name_of_property;
                            $list[$bank_id . '_' . $bank->name]['ist']['standard_property_status'] = $ist_sheet->standard_property_status;


                            $list[$bank_id . '_' . $bank->name]['soll']['id'] = $soll_sheet->id;
                            $list[$bank_id . '_' . $bank->name]['soll']['name'] = $soll_sheet->name_of_property;
                            $list[$bank_id . '_' . $bank->name]['soll']['standard_property_status'] = $soll_sheet->standard_property_status;
                        } else {

                            $list[$bank_id . '_' . $bank->name]['ist']['id'] = $ist_sheet->id;
                            $list[$bank_id . '_' . $bank->name]['ist']['name'] = $ist_sheet->name_of_property;
                            $list[$bank_id . '_' . $bank->name]['ist']['standard_property_status'] = $ist_sheet->standard_property_status;


                            $list[$bank_id . '_' . $bank->name]['soll']['id'] = $soll_sheet->id;
                            $list[$bank_id . '_' . $bank->name]['soll']['name'] = $soll_sheet->name_of_property;
                            $list[$bank_id . '_' . $bank->name]['soll']['standard_property_status'] = $soll_sheet->standard_property_status;
                        }
                    }
                }
            }

            if (isset($_GET['rashid'])) {

                echo '<pre>';
                print_r($list);
                echo '</pre>';
                die();
            }


            $ist_soll_properties = Properties::select('name_of_property', 'id', 'Ist', 'soll')
                            ->whereIn('Ist', $final_bank_ids)->orWhereIn('soll', $final_bank_ids)->get();
        } else {
            $ist_soll_properties = 0;
        }
        // end properties againts Ist and Soll



        $puser = User::where('id', $properties->user_id)->first();
        $properties->user_name = "";
        if ($puser)
            $properties->user_name = $puser->name;

        $clever_fit_fields = json_decode($properties->clever_fit_fields, true);

        if (!is_array($clever_fit_fields)) {
            $clever_fit_fields = array();
        }
        $clever_fit_fields = PropertiesService::initCleverFitFields($clever_fit_fields);

        $properties->clever_fit_fields = $clever_fit_fields;



        $planung_fields = json_decode($properties->planung_fields, true);

        if (!is_array($planung_fields)) {
            $planung_fields = array();
        }
        $planung_fields = PropertiesService::initPlanungFields($planung_fields);

        $properties->planung_fields = $planung_fields;

        $tenants = Tenants::where('property_id', $id)->get();

        $bank_ids = json_decode($properties->bank_ids);
        $banks = [];

        $bank_all = [];
        $fake_bank = new Banks();
        $fake_bank->name = '';
        $fake_bank->user_id = 0;
        $fake_bank->with_real_ek = 0;
        $fake_bank->from_bond = 0;
        $fake_bank->bank_loan = 0;
        $fake_bank->interest_bank_loan = 0;
        $fake_bank->eradication_bank = 0;
        $fake_bank->eradication_bank = 0;
        $fake_bank->interest_bond = 0;
        if ($bank_ids != null) {
            foreach ($bank_ids as $bank_id) {
                $banks[] = Banks::where('id', '=', $bank_id)->first();
            }
        } else {
            $banks[] = $fake_bank;
        }

        $bank_all = Banks::all();


        $schl_banks = $properties->schl_banks != null ? (array) json_decode($properties->schl_banks) : [];

        $tenancy_schedule_data = TenancySchedulesService::get($id, $update_date);
        $tenant_name = array();
        $tenant_name_id = array();
        $rented_list = array();

         foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule){
             foreach($tenancy_schedule->items as $item){
                if($item->type == config('tenancy_schedule.item_type.business_vacancy') || $item->type == config('tenancy_schedule.item_type.live_vacancy')){
                }else{
                    $rented_list[$item->id]['name'] = $item->name;
                    $rented_list[$item->id]['date'] = ($item->rent_begin && $item->rent_end) ? show_date_format($item->rent_begin).' - '.show_date_format($item->rent_end) : '';
                }
             }
         }



        $properties->masterliste = Masterliste::where('id', $properties->masterliste_id)->first();
        $propertiesExtra1s = PropertiesExtra1::where('propertyId', $id)->get();

        $properties_pdf = DB::table('pdf')->where('property_id', $id)->get();
        PropertiesHelperClass::uploadLocalPdfToGdrive($id);

        $comments = PropertyComments::where('property_id', $id)->orderBy('created_at', 'DESC')->get();
        $countries_ad = DB::table('countries')->get();

        $mietflachen = DB::table('mietflächen')->where('property_id', $id)->get();

        $ads = DB::table('ads')->where('property_id', $id)->first();

        if ($ads) {
            // $previous_states = State::where('country_id', $ads->country_id)->get();
            $previous_cities = City::where('state_id', $ads->state_id)->get();
        } else {
            // $previous_states = State::where('country_id', old('country'))->get();
            $previous_cities = City::where('state_id', old('state'))->get();
        }

        $previous_states = State::where('country_id', 82)->get();






        $insurances = PropertyInsurance::where('property_id', $id)->get();
        $property_service_providers = PropertyServiceProvider::where('property_id', $id)->get();

        $investation = PropertyInvestation::where('property_id', $id)->get();
        $maintenance = PropertyMaintenance::where('property_id', $id)->get();

        $tr_users = User::whereRaw('user_status=1 and (role=2 or second_role=2 or id=1 or id=10)')->get();
        $as_users = User::whereRaw('user_status=1 and (role=4 or second_role=4 or id=1 or id=10)')->get();

        $s_data = PropertiesSaleDetail::where('property_id', $id)->get()->toArray();

        $sale_data = array();
        $sale_data1 = array();

        foreach ($s_data as $key => $value) {
            if(isset($sale_data[$value['type']])){}
            else
            $sale_data[$value['type']] = $value;

            if(in_array($value['type'], array('btn1_request','btn2_request','btn3_request','btn4_request','btn1','btn2','btn3','btn4','btn5_request')))
                $sale_data1[] = $value;

        }

        $mails_logs = PropertiesMailLog::where('property_id', $id)->orderBy('created_at', 'desc')->get()->toArray();
        $mails_log_data = array();
        foreach ($mails_logs as $key => $value) {
            if(isset($mails_log_data[$value['type']])){}
            else
            $mails_log_data[$value['type']] = $value;
        }


        $ps_data = PropertiesSale::where('property_id', $id)->first();


        $pb_data = PropertiesBuy::where('property_id', $id)->first();
        $b_data = PropertiesBuyDetail::where('property_id', $id)->get()->toArray();
        $buy_data = array();
        $buy_data1 = array();
        $buy_data2 = array();
        foreach ($b_data as $key => $value) {
            if(isset($buy_data[$value['type']])){}
            else
            $buy_data[$value['type']] = $value;



            if(in_array($value['type'], array('btn1_request','btn2_request','btn3_request','btn4_request','btn1','btn2','btn3','btn4','btn5_request','btn6_request','btn6','btn5')))
                $buy_data1[] = $value;

            if(in_array($value['type'], array('vbtn1_request','vbtn1')))
                $buy_data2[] = $value;




        }
        #### For google drive files
        $propertyEinkaufFiles = PropertyEinkaufFile::where('property_id', $id)->get()->groupby('property_buy_detail_type')->toArray();

        ##### for api images
        $api_atachment_imgs = DB::table('properties_api_attachments')->where('property_id', $id)->get();
        //echo '<pre>';
        //print_r($properties1);
        //die;
        $verkauf_tab = DB::table('verkauf_tab')->where('property_id', $id)->orderBy('id', 'asc')->get();

        $email_template = DB::table('email_template')->where('property_id', $id)->first();

        if ($email_template && $email_template->email_template_users) {
            $u = User::find($email_template->email_template_users);
            if ($u && $u->signature)
                $email_template->signature = $u->signature;
        }



        $getuploaditemlist = array();
        $url = "https://vermietung.fcr-immobilien.de/getuploaditemlist/" . $id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str1111 = curl_exec($curl);
        curl_close($curl);
        if ($str1111) {
            $getuploaditemlist = json_decode($str1111, true);
        }
        // var_dump(in_array(213, $getuploaditemlist));
        // print_r($getuploaditemlist); die;

        $bank_popup_data = array();
        //if(isset($_GET['awan'])){

        if (Request()->showall) {
            $bank_popup_data = Banks::whereNotNull('contact_email')->where("contact_email", "!=", '')->get();
        } else {
            if (is_numeric($properties->plz_ort)) {

                $postal_code = substr($properties->plz_ort, 0, 2);
                $bank_popup_data = DB::select(" select * from banks where postal_code like '" . $postal_code . "%'  ");
                if (count($bank_popup_data) == 0) {

                    $postal_code = substr($properties->plz_ort, 0, 1);
                    $bank_popup_data = DB::select(" select * from banks where postal_code like '" . $postal_code . "%'  ");
                }
//
            }
        }
        // die();
        // }



        /**
         *
         * PUT Data into Email Template
         * */
        $send_email_template2_sending_info = array();
        $email_template2 = array();
        if (isset($_GET['tab']) && isset($_GET['bank_id'])) {


            $check = DB::table('email_template2')->where('property_id', $id)->where('bank_id', $_GET['bank_id'])->first();

            if (is_null($check)) {
                $this->add_email2_entry($_GET['bank_id'], $properties);
            }

            $send_email_template2_sending_info = email_template2_sending_info::where('property_id', $properties->id)->get();
            $email_template2 = DB::table('email_template2')->where('property_id', $id)->where('bank_id', $_GET['bank_id'])->first();
        }

        $vads = DB::table('vads')->where('property_id', $id)->first();
        $vads_images = array();
        if($ads){
            $vads_images = DB::table('ads_images')->where('ads_id', $ads->id)->get()->toArray();
        }else{
            $vads_images = DB::table('ads_images')->where('ads_id', $vads->id)->get()->toArray();
        }
        if ($vads) {

            // $vprevious_states = State::where('country_id', $vads->country_id)->get();
            $vprevious_cities = City::where('state_id', $vads->state_id)->get();
        } else {
            // $vprevious_states = State::where('country_id', old('country'))->get();
            $vprevious_cities = City::where('state_id', old('state'))->get();
        }
        $vprevious_states = State::where('country_id', 82)->get();


        $exportads = DB::table('export_ads')->where('property_id', $id)->first();
        if ($exportads) {
            // $eprevious_states = State::where('country_id', $exportads->country_id)->get();
            $eprevious_cities = City::where('state_id', $exportads->state_id)->get();
        } else {
            // $eprevious_states = State::where('country_id', old('country'))->get();
            $eprevious_cities = City::where('state_id', old('state'))->get();
        }
        $eprevious_states = State::where('country_id', 82)->get();

        // print_r($exportads); die;

        $sh = DB::table('sheetproperties')->where('property_id', $id)->first();



        $email_template_users = User::where('role', 1)->orWhere('role', 2)->get();

        $ccomments = Comment::where('property_id', $properties->id)->get();

        $contact_queries = DB::table('contact_queries')->where('property_id', $id)->get();

        $propertiescheckd = Properties::where('main_property_id', $id)->where('Ist', '!=', 0)->first();
        $excelFileCellD42 = ($propertiescheckd->gesamt_in_eur) + (
                ($propertiescheckd->real_estate_taxes * $propertiescheckd->gesamt_in_eur) + ($propertiescheckd->estate_agents * $propertiescheckd->gesamt_in_eur) + (($propertiescheckd->Grundbuch * $propertiescheckd->gesamt_in_eur) / 100) + ($propertiescheckd->evaluation * $propertiescheckd->gesamt_in_eur) + ($propertiescheckd->others * $propertiescheckd->gesamt_in_eur) + ($propertiescheckd->buffer * $propertiescheckd->gesamt_in_eur)
                );
        $excelFileCellG59 = 0;
        if ($excelFileCellD42 && $total_ccc) {
            $excelFileCellG59 = round($excelFileCellD42 / $total_ccc, 1);
        }
        // echo $excelFileCellD42; die;

        $excelFileCellD48 = $propertiescheckd->from_bond * $excelFileCellD42;

        $excelValueE7 = $total_ccc;
        $excelValueE10 = $propertiescheckd->maintenance_nk * $propertiescheckd->gesamt_in_eur;
        $excelValueE11 = $propertiescheckd->operating_costs_nk * $total_ccc;
        $excelValueE12 = $propertiescheckd->object_management_nk * $total_ccc;
        $excelValueE16 = $propertiescheckd->depreciation_nk_money;
        $excelValueE14 = $excelValueE7 - $excelValueE10 - $excelValueE11 - $excelValueE12;
        $excelValueE18 = $excelValueE14 - $excelValueE16;

        $excelValueD49 = $propertiescheckd->bank_loan * $excelFileCellD42;
        $excelValueE20 = $excelValueD49 * $propertiescheckd->interest_bank_loan;

        $excelValueE21 = $excelFileCellD48 * $propertiescheckd->interest_bond;

        $excelValueE23 = $excelValueE18 - $excelValueE20 - $excelValueE21;

        $excelValueE25 = $propertiescheckd->tax * $excelValueE23;

        $excelValueE27 = $excelValueE23 - $excelValueE25;

        $excelValueE29 = $excelValueD49 * $propertiescheckd->eradication_bank;

        $excelValueE31 = $excelValueE27 - $excelValueE29 + $excelValueE16;

        $excelValueH47 = $excelValueD49 - $excelValueE29;
        $excelValueI48 = $excelValueH47 * $propertiescheckd->interest_bank_loan;
        $excelValueI49 = ($excelValueE29 + $excelValueE20) - $excelValueI48;

        $excelValueH49 = $excelValueD49 * $propertiescheckd->eradication_bank;

        $excelValueD47 = $propertiescheckd->with_real_ek*$excelFileCellD42;
        $excelValueD48 = $properties->from_bond*$excelFileCellD42;
        $excelValueD49 = $properties->bank_loan*$excelFileCellD42;
        $excelValue50 = $excelValueD47+$excelValueD48+$excelValueD49;
        // echo $properties->gesamt_in_eur;
        // echo $excelFileCellD48; die;
        $userFormData = UserFormData::where('property_id', $id)->get();

        $vacantspaces = VacantSpace::where('property_id', $id)->get();
        $loansMirrorData = PropertyLoansMirror::where('property_id', $id)->first();
        $loan_mirror = PropertiesLoanMirrors::where('property_id', $id)->first();

        // echo $excelValueE7; die;
        // print "<pre>";
        // print_r($buy_data);
        // die;
        // echo $buy_data['btn1']['user1']; die;
        $total_ccc_verkauf = 0;
        foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
            $total_ccc_verkauf = $tenancy_schedule->calculations['total_actual_net_rent'] * 12;


        $lativity = LeasingActivity::where('property_id',$id)->where('deleted',0)->orderBy('type','asc')->get();

        $emp_data = Empfehlung::where('property_id',$id)->where('deleted',0)->orderBy('name','asc')->get();

        foreach ($emp_data as $key => $value) {
            $ractivity[$value->tenant_id][] = $value;
            $value->files = GdriveUploadFiles::where('property_id',$id)->where('parent_id', $value->id)->where('parent_type', 'empfehlung')->get();
            $value->files2 = GdriveUploadFiles::where('property_id',$id)->where('parent_id', $value->id)->where('parent_type', 'empfehlung2')->get();
        }

        $EmpfehlungDetail = EmpfehlungDetail::where('property_id',$id)->get();

        $EmpfehlungDetail_arr = array();
        $provision_release = array();
        $release_arr = array();
        foreach ($EmpfehlungDetail as $key => $value) {
            $EmpfehlungDetail_arr[$value->tenant_id][$value->slug] = $value->value;
            $release_arr[$value->tenant_id][$value->slug]=$value->created_at;
            $provision_release[$value->provision_id][$value->slug]=$value->created_at;
        }




        // echo $total_ccc_verkauf; die;
        // $category = explode(' ',trim('Söhlde Marktstraße 6'));
        // $emails = $this->outlookHttpService->getMailsByCategory($category[0]);
        $emails['value'] = array();

        $workingDir = '/';
        $p_dir = PropertiesDirectory::where('property_id', $id)->where('parent_id', 0)->first();
        if($p_dir){
            $workingDir = $p_dir->dir_path;
        }

        $tenant_name_id2 = array();
        $v_data = PropertyVacant::where('property_id',$properties->id)->get();
        $tenant_name_id_custom  = array();
        $tenant_name_id['name']  = array();
        $tenant_name_id2['name']  = array();
        foreach($v_data as $item){
            if($item->type == config('tenancy_schedule.item_type.business_vacancy') || $item->type == config('tenancy_schedule.item_type.live_vacancy'))
            {
                $tenant_name[] = $item->name;

                $item->files2 = GdriveUploadFiles::where('property_id',$id)->where('parent_id', $item->id)->where('parent_type', 'empfehlung2')->get();

                if($item->tenant_id)
                $tenant_name_id['name'][$item->id] = $item->name;
                else
                $tenant_name_id2['name'][$item->id] = $item->name;

                $tenant_name_id['files'][$item->id] = $item->files2;

                $tenant_name_id_custom[$item->id] = $item->name;

            }
            // if(!$item->tenant_id)
            // $tenant_name_id_custom = $item->name;
        }

        // print_r($tenant_name_id); die;


        $banking_info = BankingRequest::where('property_id',$id)->first();
        $this->updateprovisioninfo($tenant_name_id_custom,$release_arr,$EmpfehlungDetail_arr, $ractivity, $id);
        $provision_info = Provision::where('property_id',$id)->get();
        $attchments = Attachment::where('property_id',$id)->get();


        $contact_person = User::where('id', $properties->transaction_m_id)->first();

          /*$assetsManagerMails = SendMailToAm::leftjoin('users','users.id','send_mail_to_ams.am_id')
                                ->where('send_mail_to_ams.property_id', $id)
                                ->select('send_mail_to_ams.id','send_mail_to_ams.subject','send_mail_to_ams.message','send_mail_to_ams.created_at', 'send_mail_to_ams.type', 'send_mail_to_ams.email as custom_email', 'send_mail_to_ams.property_id', 'send_mail_to_ams.section_id', 'send_mail_to_ams.am_id', 'send_mail_to_ams.mail_type','users.name','users.email')
                                ->orderBy('send_mail_to_ams.id', 'desc')
                                ->get();*/

        $sendEmails = email_template2_sending_info::where('property_id', $id)->get();


        $insurance_tab_title = PropertyInsuranceTab::select('id', 'title', 'property_id',
            DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurance_release' AND record_id = property_insurance_tabs.id AND tab='insurance_tab') THEN 1 ELSE 0 END as release_status"),
            DB::raw("(SELECT not_release FROM property_insurance_tab_details WHERE property_insurance_tab_id = property_insurance_tabs.id AND not_release != 0 LIMIT 1) as not_release"),
            DB::raw("(SELECT properties_mail_logs.created_at FROM properties_mail_logs WHERE (type = 'insurance_release' OR type = 'insurancetab_release') AND record_id = property_insurance_tabs.id AND (tab='insurance_tab' OR tab='insurancetab2') ORDER BY properties_mail_logs.created_at DESC LIMIT 1) as release_date")
            )->where('property_id', $id)->where('deleted', 0)->orderBy('id', 'desc')->orderBy('release_date', 'desc')->get();

        $rent_paid_excel = RentPaidExcelUpload::where('property_id',$id)->orderBy('id','desc')->first();

        // pre($rent_paid_excel);
        $propertyProjects = $this->propertyProjectService->getByPropertyId($id);

        // ****** Banking offers ****/
        $banksFinancingOffers = BankFinancingOffer::where('property_id', $id)->get();


        $propertyAddressesJobs = AddressesJob::where('property_id',$id)->orderBy('id', 'DESC')->with('emailLogs')->get();

        $hvbu_users = User::whereRaw('user_status=1 and (role='.config('auth.role.hvbu').' OR second_role='.config('auth.role.hvbu').')')->get();
        $hvpm_users = User::whereRaw('user_status=1 and (role='.config('auth.role.hvpm').' OR second_role='.config('auth.role.hvpm').')')->get();
        $sp_users = User::whereRaw('user_status=1 and (role='.config('auth.role.service_provider').')')->get();

        $tax_consultant = User::whereRaw('user_status=1 and (role='.config('auth.role.tax_consultant').')')->get();


        $asset_email = "aaa";
        if(isset($properties->asset_manager) && isset($properties->asset_manager->name)){
            $asset_email = $properties->asset_manager->email;
        }
        $visible_array = array(config('users.falk_email'),'a.raudies@fcr-immobilien.de','u.wallisch@fcr-immobilien.de',$asset_email);
        $provision_visible_class = "hidden";

        if(in_array($login_user->email, $visible_array) || $login_user->role=="1")
        $provision_visible_class = "";


        $custom_fields_schl = PropertiesCustomField::where('property_id',$id)->get();
        $custom_fields_schl_array = array();
        foreach ($custom_fields_schl as $key => $value) {
            $custom_fields_schl_array[$value->slug] = $value->content;
        }


        $tenant_master = TenantMaster::where('property_id', $id)->get();

        $aDataTableHeaderHTML = [];
        $ch = curl_init("https://www.euribor-rates.eu");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Safari');
        $html = curl_exec($ch);
        // $my1 = explode("<tbody>", $html);
        $DOM = new \DOMDocument();
        libxml_use_internal_errors(true);
        $DOM->loadHTML($html);
        libxml_use_internal_errors(false);
        $Detail = $DOM->getElementsByTagName('td');
        foreach($Detail as $NodeHeader)
        {
            $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
        }
        $active_users = User::whereRaw('(role < 6 OR role = 7 OR role = 8) and user_deleted = 0 and user_status = 1')->get();


        // pre($verkauf_tab);


        return view('properties.show', compact('active_users','custom_fields_schl_array','propertyAddressesJobs','banksFinancingOffers','emails','excelValue50','excelValueE29', 'excelValueI49', 'excelValueE31', 'excelFileCellD48', 'excelFileCellG59', 'excelValueH49', 'excelFileCellD42', 'total_ccc', 'verkauf_tab', 'properties', 'send_email_template2_sending_info', 'properties1', 'bank_popup_data', 'list', 'id', 'banks', 'tab', 'tenants', 'schl_banks', 'tenancy_schedule_data', 'fake_bank', 'bank_all', 'propertiesExtra1s', 'properties_pdf', 'comments', 'countries_ad', 'mietflachen', 'ads', 'previous_states', 'previous_cities', 'insurances', 'property_service_providers', 'maintenance', 'investation', 'tr_users', 'sale_data', 'ps_data', 'buy_data', 'pb_data', 'as_users', 'ist_soll_properties', 'api_atachment_imgs', 'email_template', 'email_template_users', 'email_template2', 'getuploaditemlist', 'vads', 'vads_images','vprevious_states', 'propertiescheckd', 'vprevious_cities', 'sh', 'bankers_email_info', 'ccomments', 'users', 'exportads', 'eprevious_states', 'eprevious_cities', 'contact_queries', 'userFormData', 'loansMirrorData', 'propertyEinkaufFiles', 'propertyEinkaufDir','buy_data1','buy_data2','total_ccc_verkauf','sale_data1','mails_logs','mails_log_data','tenant_name','lativity','ractivity','EmpfehlungDetail_arr','tenant_name_id','tenant_name_id2','release_arr', 'workingDir','banking_info', 'contact_person','provision_info','provision_release','attchments', 'assetsManagerMails','sendEmails','rented_list','tenant_master', 'insurance_tab_title','propertyProjects','rent_paid_excel','hvbu_users','hvpm_users','sp_users','tax_consultant','provision_visible_class', 'loan_mirror','aDataTableHeaderHTML'));
    }

    public function updateprovisioninfo($tenant_name_id,$release_arr,$EmpfehlungDetail_arr, $ractivity, $property_id){

        foreach($tenant_name_id as $t_id=>$list1):

            if(isset($release_arr[$t_id]['btn2'])):

                $a1 = 0;
                if(isset($EmpfehlungDetail_arr[$t_id]['amount1']) && $EmpfehlungDetail_arr[$t_id]['amount1'])
                    $a1 = $EmpfehlungDetail_arr[$t_id]['amount1'];

                $a2 = 0;
                if(isset($EmpfehlungDetail_arr[$t_id]['amount2']) && $EmpfehlungDetail_arr[$t_id]['amount2'])
                    $a2 = $EmpfehlungDetail_arr[$t_id]['amount2'];

                $a3 = $a1*12;
                /*if(isset($EmpfehlungDetail_arr[$t_id]['annual_revenue']) && $EmpfehlungDetail_arr[$t_id]['annual_revenue'])
                    $a3 = $EmpfehlungDetail_arr[$t_id]['annual_revenue'];*/

                $a4 = $a1*$a2;

                $a5 = 0;
                if(isset($EmpfehlungDetail_arr[$t_id]['amount5']) && $EmpfehlungDetail_arr[$t_id]['amount5'])
                    $a5 = $EmpfehlungDetail_arr[$t_id]['amount5'];

                $a6 = 0;
                if(isset($EmpfehlungDetail_arr[$t_id]['amount6']) && $EmpfehlungDetail_arr[$t_id]['amount6'])
                    $a6 = $EmpfehlungDetail_arr[$t_id]['amount6'];

                $a7 = 0;
                if(isset($EmpfehlungDetail_arr[$t_id]['amount7']) && $EmpfehlungDetail_arr[$t_id]['amount7'])
                    $a7 = $EmpfehlungDetail_arr[$t_id]['amount7'];

                $total = 0;
                if(isset($ractivity[$t_id])){
                    foreach($ractivity[$t_id] as $k=>$list)
                        $total +=$list->amount;
                }

                $nt = $a4-$total-$a5-$a6;

                $p = Provision::where('tenant_id',$t_id)->first();
                if($p){

                    $m =$p;
                    continue;
                }
                else{
                    $m =  new Provision;
                    $m->commision_percent = $a7;
                }

                $tn_name = @$EmpfehlungDetail_arr[$t_id]['tenant_name'];

                if(@$EmpfehlungDetail_arr[$t_id]['tenant_name'] && is_numeric($EmpfehlungDetail_arr[$t_id]['tenant_name'])){
                    $item = TenancyScheduleItem::find($EmpfehlungDetail_arr[$t_id]['tenant_name']);
                    if($item)
                        $tn_name = $item->name;
                }

                $flache = '';
                if(isset($EmpfehlungDetail_arr[$t_id]['rental_space']) && $EmpfehlungDetail_arr[$t_id]['rental_space']){
                    $itm = TenancyScheduleItem::selectRaw('id, name, IFNULL(rental_space, 0) as rental_space, IFNULL(vacancy_in_qm, 0) as vacancy_in_qm')->where('id', $EmpfehlungDetail_arr[$t_id]['rental_space'])->first();
                    if($itm){
                        $vv = ($itm->rental_space) ? $itm->rental_space : $itm->vacancy_in_qm;
                        $flache = $itm->name.' ('.number_format($vv,2,',','.').')';
                    }
                }

                $m->property_id         = $property_id;
                $m->tenant_id           = $t_id;

                $m->name                = $tn_name; //Mieter
                $m->flache              = $flache; //Fläche
                $m->mv_vl               = strtoupper(@$EmpfehlungDetail_arr[$t_id]['mv_vl']); //Art
                $m->cost                = $total; //Kosten Umbau
                $m->einnahmen_pm        = $a1; //Einnahmen p.m.
                $m->laufzeit_in_monate  = $a2; //Laufzeit in Monate
                $m->mietfrei            = $a5; //Mietfrei in €
                $m->jahrl_einnahmen     = $a3; //Jährl. Einnahmen
                $m->rent                = $a4; //Gesamt-einnahmen
                $m->net_income          = $a4-$total-$a5; //Gesamt-einnahmen netto

                if(isset($EmpfehlungDetail_arr[$t_id]['tenant_name']) && $EmpfehlungDetail_arr[$t_id]['tenant_name'] && is_numeric($EmpfehlungDetail_arr[$t_id]['tenant_name'])){
                    $m->mieter_item_id  = $EmpfehlungDetail_arr[$t_id]['tenant_name']; //Item id
                }

                if(isset($EmpfehlungDetail_arr[$t_id]['rental_space']) && $EmpfehlungDetail_arr[$t_id]['rental_space'] && is_numeric($EmpfehlungDetail_arr[$t_id]['rental_space'])){
                    $m->flache_item_id  = $EmpfehlungDetail_arr[$t_id]['rental_space']; //Item id
                }

                $m->save();

            endif;
        endforeach;
    }
    public function lease_list(Request $request)
    {
        $tenant_name = array();
        $v_data = PropertyVacant::where('property_id',$request->property_id)->get();
        foreach($v_data as $item){
            if($item->type == config('tenancy_schedule.item_type.business_vacancy') || $item->type == config('tenancy_schedule.item_type.live_vacancy'))
            {
                $tenant_name[] = $item->name;
                $tenant_name_id[$item->tenant_id] = $item->name;
            }
        }
        $q = "";
        if($request->search){
            $q = $request->search;
            $lativity = LeasingActivity::where('property_id',$request->property_id)->where('deleted',0)->whereRaw('(type LIKE "%' . $q . '%" OR  tenant LIKE "%' . $q . '%" OR  comment_1 LIKE "%' . $q . '%" OR  comment_2 LIKE "%' . $q . '%")')->orderBy($request->column,$request->sort)->get();
        }
        else
        $lativity = LeasingActivity::where('property_id',$request->property_id)->where('deleted',0)->orderBy($request->column,$request->sort)->get();

        $column = $request->column;
        $sort = $request->sort;
        return view('properties.lease_list', compact('lativity','tenant_name','q','column','sort'));
    }
    public function recommended_list(Request $request)
    {
        $tenancy_schedule_data = TenancySchedulesService::get($request->property_id, 0);
        $tenant_name = $rented_list = array();
        foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule){
            foreach($tenancy_schedule->items as $item){
                if($item->type == config('tenancy_schedule.item_type.business_vacancy') || $item->type == config('tenancy_schedule.item_type.live_vacancy'))
                 {
                 }
                 else{

                    $rented_list[$item->id] = $item->name;
                 }

            }
        }
        $v_data = PropertyVacant::where('id',$request->tenant_id)->first();


        $v_data->files2 = GdriveUploadFiles::where('property_id',$request->property_id)->where('parent_id', $request->tenant_id)->where('parent_type', 'empfehlung2')->get();



        $lativity = array();
        $lativity = Empfehlung::where('property_id',$request->property_id)->where('tenant_id',$request->tenant_id)->where('deleted',0)->orderBy('name','asc')->get();
        foreach($lativity as $key => $value){
            $value->files = GdriveUploadFiles::where('property_id',$request->property_id)->where('parent_id', $value->id)->where('parent_type', 'empfehlung')->get();
        }

        $EmpfehlungDetail = EmpfehlungDetail::where('property_id',$request->property_id)->where('tenant_id',$request->tenant_id)->get();

        $EmpfehlungDetail_arr = array();

        foreach ($EmpfehlungDetail as $key => $value) {
            $EmpfehlungDetail_arr[$value->tenant_id][$value->slug] = $value->value;
        }
        $t_id = $request->tenant_id;
        $property_id = $request->property_id;
        return view('properties.recommended_list', compact('lativity','tenant_name','EmpfehlungDetail_arr','t_id','rented_list','v_data','property_id'));
    }



    Public function prepare_number_format($data){
        $data = str_replace('.','',$data);
        $data = str_replace(',','.',$data);
        $data = str_replace('%','',$data);
        $data = str_replace('€','',$data);

        return $data;
    }

    public function prepare_date_format($data)
    {
        if(strtotime(str_replace('/', '-', $data)))
        $data = date('Y-m-d',strtotime(str_replace('/', '-', $data)));
        elseif(strtotime($data))
        $data = date('Y-m-d',strtotime($data));
        else
            $data = "";
        return $data;
    }

    public function create_loans_mirror(Request $request){
        $messages = [
            'required' => 'This field is required.',
        ];
        $validator = Validator::make($request->all(), [
            'property_id' => 'required',
        ], $messages);



        if ($validator->fails()) {
            return redirect('properties/'.$request->property_id.'?tab=darlehensspiegel')
                        ->withErrors($validator)
                        ->withInput();
        }
        $loansMirrorData = PropertyLoansMirror::where('property_id', $request->property_id)->first();
        if(!$loansMirrorData){
            $loansMirrorData = new PropertyLoansMirror;
        }
        $loansMirrorData->property_id = $request->property_id;
        $loansMirrorData->borrower = $request->borrower;
        $loansMirrorData->lender = $request->lender;
        $loansMirrorData->original_loan = 0;
        if($request->original_loan)
        $loansMirrorData->original_loan = $this->prepare_number_format($request->original_loan);
        if($request->payout_date)
        $loansMirrorData->payout_date = save_date_format($request->payout_date);

        $loansMirrorData->early_interest = 0;
        if($request->early_interest)
        $loansMirrorData->early_interest = $this->prepare_number_format($request->early_interest);
        $loansMirrorData->delta = 0;
        if($request->delta)
        $loansMirrorData->delta = $this->prepare_number_format($request->delta);

        $loansMirrorData->delta2 = 0;
        if($request->delta2)
        $loansMirrorData->delta2 = $this->prepare_number_format($request->delta2);

        $loansMirrorData->delta3 = 0;
        if($request->delta3)
        $loansMirrorData->delta3 = $this->prepare_number_format($request->delta3);

        $loansMirrorData->interest_extend = 0;
        if($request->interest_extend)
            $loansMirrorData->interest_extend = $this->prepare_number_format($request->interest_extend);

        $loansMirrorData->loan_service = 0;
        if($request->loan_service)
            $loansMirrorData->loan_service = $this->prepare_number_format($request->loan_service);

        $loansMirrorData->loan_service_month = 0;
        if($request->loan_service_month)
            $loansMirrorData->loan_service_month = $this->prepare_number_format($request->loan_service_month);

        $loansMirrorData->zins = 0;
        if($request->zins)
            $loansMirrorData->zins = $this->prepare_number_format($request->zins);


        $loansMirrorData->calculation_method = $request->calculation_method;
        $loansMirrorData->variable_interest = $request->variable_interest;
        $loansMirrorData->reference_interest = $request->reference_interest;
        $loansMirrorData->check_the_refinance_1 = $request->check_the_refinance_1;
        $loansMirrorData->check_the_refinance_3 = $request->check_the_refinance_3;
        $loansMirrorData->change_limit = 0;
        if($request->change_limit)
        $loansMirrorData->change_limit = $this->prepare_number_format($request->change_limit);
        $loansMirrorData->treatment_at_negative = $request->treatment_at_negative;
        $loansMirrorData->loan = $request->loan;
        if($request->running_time_type == "Fest"){
            $loansMirrorData->running_time = save_date_format($request->running_time);
        }else{
            $loansMirrorData->running_time = $request->running_time;
        }
        $loansMirrorData->running_time_type = $request->running_time_type;
        $loansMirrorData->standby_interest = 0;
        if($request->standby_interest)
        $loansMirrorData->standby_interest = $this->prepare_number_format($request->standby_interest);
        if($request->standby_interest_from)
        $loansMirrorData->standby_interest_from = save_date_format($request->standby_interest_from);
        $loansMirrorData->management_fee = 0;
        if($request->management_fee)
        $loansMirrorData->management_fee = $this->prepare_number_format($request->management_fee);
        $loansMirrorData->loan_costs = 0;
        if($request->loan_costs)
        $loansMirrorData->loan_costs = $this->prepare_number_format($request->loan_costs);
        $loansMirrorData->registry_fees = $this->prepare_number_format($request->registry_fees);
        $loansMirrorData->exit_fee = $this->prepare_number_format($request->exit_fee);
        $loansMirrorData->mortgage = $this->prepare_number_format($request->mortgage);
        $loansMirrorData->mortgage_in_bond = $this->prepare_number_format($request->mortgage_in_bond);
        $loansMirrorData->mortgage_guarantor = $request->mortgage_guarantor;
        $loansMirrorData->mortgage_extent_of_guarantee = $this->prepare_number_format($request->mortgage_extent_of_guarantee);
        $loansMirrorData->is_shareholder_loan = $request->is_shareholder_loan;
        if($request->is_shareholder_loan)
            $loansMirrorData->shareholder_loan = $request->shareholder_loan;
        $loansMirrorData->guarantor = $request->guarantor;
        $loansMirrorData->extent_of_guarantee = $this->prepare_number_format($request->extent_of_guarantee);

        $loansMirrorData->save();
        return redirect('properties/'.$request->property_id.'?tab=darlehensspiegel')->with('success', trans('Data saved successfully'));

    }

    public function addLoanMirror(Request $request, $property_id){
        $messages = [
            'required' => 'This field is required.',
        ];

        $validator = Validator::make($request->all(), [
            // 'property_id' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return redirect('properties/'.$$property_id.'?tab=finance')
                        ->withErrors($validator)
                        ->withInput();
        }

        $post = $request->all();

        $user = Auth::user();

        $numericFields = ['delta', 'delta2', 'delta3', 'zins', 'interest_extend', 'loan_service', 'loan_service_month', 'darlehensbetrag', 'bereitstellungszinsen', 'tilgungssatz', 'anfangszins', 'aktueller_zins', 'höhe_euribor', 'veränderungsgrenze', 'mabgeblicher_euribor', 'aktuelle_restschuld', 'restschuld_per_1', 'restschuld_per_2', 'zinsaufwand', 'tilgung', 'delta4', 'grundschuld', 'höhe_zusatzsicherheit', 'verkehrswert'];
        $dateFields = ['laufzeitende', 'zinsbindungsende','payout_date', 'ab', 'anderung_wirksam_per', 'zinszahlungsbeginn', 'tilgungsbeginn'];

        foreach ($numericFields as $num_field) {
            if(isset($post[$num_field]))
                $post[$num_field] = $this->prepare_number_format($post[$num_field]);
            else
                $post[$num_field] = 0;
        }

        foreach ($dateFields as $date_field) {
            if(isset($post[$date_field]))
                $post[$date_field] = save_date_format($post[$date_field]);
            else
               $post[$date_field] = null;
        }

        $modal = PropertiesLoanMirrors::where('property_id', $property_id)->first();
        if(!$modal){
            $modal = new PropertiesLoanMirrors;
        }
        $modal->property_id = $property_id;
        $modal->user_id = $user->id;
        $modal->delta = $post['delta'];
        $modal->delta2 = $post['delta2'];
        $modal->delta3 = $post['delta3'];
        $modal->zins = $post['zins'];
        $modal->interest_extend = $post['interest_extend'];
        $modal->loan_service = $post['loan_service'];
        $modal->loan_service_month = $post['loan_service_month'];
        $modal->darlehensbetrag = $post['darlehensbetrag'];
        $modal->darlehensnummer = ($post['darlehensnummer']) ? $post['darlehensnummer'] : 0;
        $modal->darlehensart = $post['darlehensart'];
        $modal->laufzeitende = $post['laufzeitende'];
        $modal->zahlungsrythmus = $post['zahlungsrythmus'];
        $modal->bereitstellungszinsen = $post['bereitstellungszinsen'];
        $modal->tilgungssatz = $post['tilgungssatz'];
        $modal->zinsart = $post['zinsart'];
        $modal->anfangszins = $post['anfangszins'];
        $modal->aktueller_zins = $post['aktueller_zins'];
        $modal->zinsberechnungsmethode = $post['zinsberechnungsmethode'];
        $modal->zinsbindungsende = $post['zinsbindungsende'];
        $modal->euribor = $post['euribor'];
        $modal->höhe_euribor = $post['höhe_euribor'];
        $modal->behandlung_negativzins = $post['behandlung_negativzins'];
        $modal->veränderungsgrenze = $post['veränderungsgrenze'];
        $modal->mabgeblicher_euribor = $post['mabgeblicher_euribor'];
        $modal->ermittlungsturnus = $post['ermittlungsturnus'];
        $modal->ermittlungsstichtag = $post['ermittlungsstichtag'];
        $modal->aktuelle_restschuld = $post['aktuelle_restschuld'];
        $modal->restschuld_per_1 = $post['restschuld_per_1'];
        $modal->restschuld_per_2 = $post['restschuld_per_2'];
        $modal->zinsaufwand = $post['zinsaufwand'];
        $modal->tilgung = $post['tilgung'];
        $modal->delta4 = $post['delta4'];
        $modal->grundschuld = $post['grundschuld'];
        $modal->mietabtretung = $post['mietabtretung'];
        $modal->zusatzsicherheit = $post['zusatzsicherheit'];
        $modal->höhe_zusatzsicherheit = $post['höhe_zusatzsicherheit'];

        $modal->lender = $post['lender'];
        $modal->borrower = $post['borrower'];
        $modal->payout_date = $post['payout_date'];
        $modal->verkehrswert = $post['verkehrswert'];

        $modal->ab = $post['ab'];
        $modal->sonstiges = $post['sonstiges'];

        $modal->anderung_wirksam_per = $post['anderung_wirksam_per'];
        $modal->zinszahlungsbeginn = $post['zinszahlungsbeginn'];
        $modal->tilgungsbeginn = $post['tilgungsbeginn'];

        if($modal->save()){
            return redirect('properties/'.$property_id.'?tab=finance')->with('success', trans('Data saved successfully'));
        }else{
            return redirect('properties/'.$property_id.'?tab=finance')->with('error', trans('Data saved fail'));
        }

    }

    public function add_email2_entry($bank_id, $properties) {


        $bank_data = DB::table('banks')->where('id', $bank_id)->first();



        email_template2::updateOrCreate([
            'property_id' => $properties->id,
            'bank_id' => $bank_id
                ], [
            'property_id' => $properties->id,
            'bank_id' => $bank_id,
            'input1' => $bank_data->fullName,
            'input2' => $properties->ort,
            'input3' => $properties->ort,
            'input4' => $properties->ort,
            'input5' => $properties->construction_year,
            'input6' => 'Einkaufszentrum'
                ]
        );
    }

    public function update_pdf_name(Request $request, $id) {
        DB::table('pdf')->where('id', $id)->update(['upload_file_name' => $request->value]);
        return response()->json(['msg' => 'success']);
    }

    // Set as default property from Ist soll
    public function set_as_standard($orignal_prop, $pid) {

        $properties = Properties::where('id', '=', $orignal_prop)->first();
        //properties againts Ist and Soll  remove standard first
        if ($properties->bank_ids) {

            // $final_bank_ids = json_decode($properties->bank_ids,true);
            $ist_soll_properties = Properties::where('main_property_id', $orignal_prop)->update(['standard_property_status' => 0]);
        }
        //DB::connection()->enableQueryLog();
        $prop_set_standard = Properties::where('id', '=', $pid)->update(['standard_property_status' => 1]);
        //dd(DB::getQueryLog());
        if ($prop_set_standard) {
            return response()->json(['msg' => 'success']);
        }
    }

    public function verkauf_new_row(Request $request) {
        $insert_data = ([
            'property_id' => $request->property_id,
            'row_name' => 'empty',
            'tab_value' => 0,
            'new_added' => 1
        ]);

        $if_inserted = DB::table('verkauf_tab')->insert($insert_data);
        if ($if_inserted) {
            return redirect('properties/' . $request->property_id . '?tab=verkauf_tab');
        }
    }

    public function update_verkauf_tab_row(Request $request, $row_id) {
        // change value column


        $request->value = str_replace('.', '', $request->value);
        $request->value = str_replace(',', '.', $request->value);

        if ($request->has('name') && $request->name == 'value_change') {
            DB::table('verkauf_tab')->where('property_id', $request->pk)->where('id', $row_id)->update(['tab_value' => $request->value]);
        } else {
            // change row name
            DB::table('verkauf_tab')->where('property_id', $request->pk)->where('id', $row_id)->update(['row_name' => $request->value]);
        }
    }

    public function update_month_verkauf_tab(Request $request, $p_id) {
        Verkauf_tab::updateOrCreate(
                ['property_id' => $p_id, 'monat_check' => 1], ['monat' => $request->monat, 'monat_check' => 1]
        );
    }

    public function update_row_11_12_name(Request $request) {
        $get_name = DB::table('verkauf_tab')->where('property_id', $request->pk)->where('fixed_rows', $request->name)->first();
        if (is_null($get_name)) {

            $insert_data = ([
                'property_id' => $request->pk,
                'row_name' => $request->value,
                'fixed_rows' => $request->name
            ]);

            DB::table('verkauf_tab')->insert($insert_data);
        } else {

            DB::table('verkauf_tab')->where('property_id', $request->pk)->where('fixed_rows', $request->name)->update(['row_name' => $request->value]);
        }
    }

    // Delete verkauf tab row
    public function delete_verkauf_tab_row($row_id) {
        DB::table('verkauf_tab')->where('id', $row_id)->delete();

        return redirect('properties/' . $_GET['property_id'] . '?tab=verkauf_tab');
    }

    public function deletesheet() {
        $d = Properties::find($_REQUEST['id']);
        if ($d->standard_property_status == 1) {
            echo "You can not delete this sheet as this is set as standard";
            die;
        } else {
            DB::table('properties')->where('id', $_REQUEST['id'])->delete();
            echo "1";
            die;
        }
    }

    public function update_verkauf_tab_row_others(Request $request) {


        if($request->name=="bestandsmonate" || $request->name=="verkaufspreis" || $request->name=="investitionskosten" || $request->name=="VFE (Athora LV)" || $request->name=="zzgl" || $request->name=="uber" || $request->name=="Maklerprovision")
        {
            $request->value = str_replace('.', '', $request->value);
            $request->value = str_replace(',', '.', $request->value);
        }


        $get_name = DB::table('verkauf_tab')->where('property_id', $request->pk)->where('row_name', $request->name)->first();

        // print_r($get_name);
        if (is_null($get_name)) {

            if($request->name=="bestandsmonate_date")
            $insert_data = ([
                'property_id' => $request->pk,
                'row_name' => $request->name,
                'string_text' => $request->value
            ]);
            else
            $insert_data = ([
                'property_id' => $request->pk,
                'row_name' => $request->name,
                'tab_value' => str_replace(',', '', $request->value)
            ]);
            // echo "inserted";
            DB::table('verkauf_tab')->insert($insert_data);
        } else {

            if($request->name=="bestandsmonate_date")
            DB::table('verkauf_tab')->where('property_id', $request->pk)->where('row_name', $request->name)->update(['string_text' => $request->value]);
            else
            DB::table('verkauf_tab')->where('property_id', $request->pk)->where('row_name', $request->name)->update(['tab_value' => (float) str_replace(',', '', $request->value)]);

            if ($request->name == 'ankaufspreis') {
                DB::table('verkauf_tab')->where('property_id', $request->pk)->where('row_name', 'row_12')->update(['tab_value' => (float) str_replace(',', '', $request->value)]);
            }

            if ($request->name == 'row_12') {
                DB::table('verkauf_tab')->where('property_id', $request->pk)->where('row_name', 'ankaufspreis')->update(['tab_value' => (float) str_replace(',', '', $request->value)]);
            }
        }
    }

    public function ajax_verkauf_tab_load(Request $request, $property_id, $g59) {

        $verkauf_tab = DB::table('verkauf_tab')->where('property_id', $property_id)->orderBy('id', 'asc')->get();
        $id = $property_id;
        $G59 = $g59;
        echo view('properties.templates.verkauf_tab_table', compact('verkauf_tab', 'id', 'G59'));
    }

    public function update_verkauf_item(Request $request, $id, $type) {
        $column = $request->pk;
        $modal = PropertiesSaleDetail::where('property_id', $id)->where('type', $type)->first();

        $response = [
            'success' => false,
        ];
        if($type==101 || $type==102 || $type==103){
            // $request->value = date_format(  date_create(str_replace('.', '-', $request->value)) , 'Y-m-d');

            $v = validatedate($request->value);
            if($v)
            {
                $request->value = save_date_format($request->value);
            }
            else{
                $response['msg'] = "Please enter valid date in (dd.mm.yyyy) format";
                return response()->json($response);
            }

        }

        if ($modal) {
            $modal->$column = $request->value;
            $modal->save();
        } else {
            $modal = new PropertiesSaleDetail;
            $modal->property_id = $id;
            $modal->type = $type;
            $modal->$column = $request->value;
            $modal->save();
        }
    }

    public function update_verkauf(Request $request, $prop_id) {
        $c = $request->pk;
        $properties = PropertiesSale::where('property_id', '=', $prop_id)->first();

        if ($properties) {
            $properties->$c = $request->value;
            $properties->save();
        } else {
            $properties = new PropertiesSale;
            $properties->property_id = $prop_id;
            $properties->$c = $request->value;
            $properties->save();
        }
    }

    public function remove_uploaded_file(Request $request) {
        $modal = PropertiesBuyDetail::where('property_id', $request->property_id)->where('type', $request->type)->first();
        if ($modal && $modal->files) {
            $ar = json_decode($modal->files, true);
            if (isset($ar[$request->id]))
                unset($ar[$request->id]);

            $modal->files = json_encode(array_values($ar));
            $modal->save();
        }
        echo 1;
        die;
    }
    public function uploadSchlFile(Request $request)
    {
        $id = $request->property_id;
        $type = $request->type;
        $column = "files";
        $arr['status'] = 0;
        $modal = PropertiesBuyDetail::where('property_id', $id)->where('type', $type)->first();
        if ($modal) {

            $validator = Validator::make($request->all(), [
                'file' => 'mimes:jpeg,jpg,png,gif|required|max:200096',
            ]);
            if ($validator->fails()) {
                $arr['message'] =  $validator->errors()->first();
                return $arr;
            }
            $dir = 'property_images/';



            $extension = $request->file('file')->getClientOriginalExtension();
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);

            if(file_exists($dir.$modal->files))
                unlink($dir.$modal->files);

            $modal->$column = $filename;
            $modal->save();
            $arr['status'] = 1;
            $arr['path'] = asset($dir.$modal->files);

        } else {
            $validator = Validator::make($request->all(), [
                'file' => 'mimes:jpeg,jpg,png,gif|required|max:200096',
            ]);
            if ($validator->fails()) {
                $arr['message'] =  $validator->errors()->first();
                return $arr;
            }


            $extension = $request->file('file')->getClientOriginalExtension();
            $dir = 'property_images/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);

            $modal = new PropertiesBuyDetail;
            $modal->property_id = $id;
            $modal->type = $type;
            $modal->$column = $filename;
            $modal->save();
            $arr['status'] = 1;
            $arr['path'] = asset($dir.$modal->files);
        }
        return $arr;
    }
    public function deleteprovisioninfo(Request $request)
    {
        $modal = Provision::where('id', $request->id)->first();
        if($modal)
        {
            $modal->deleted = 1;
            $modal->save();
        }
        echo 1;
        die;
    }




    public function update_property_schl_by_field2(Request $request,$id)
    {
        $response = [
            'success' => false,
        ];

        $column = $request->pk;
        $modal = PropertiesCustomField::where('property_id', $id)
        ->where('slug',$column)
        ->first();

        if($column=="electricity_provider_price" || $column=="oil_provider_price" || $column=="gas_provider_price" || $column=="heat_provider_price")
        {
            $request->value = str_replace('.', '', $request->value);
            $request->value = str_replace(',', '.', $request->value);
            $request->value = str_replace('%', '', $request->value);
            $request->value = str_replace('€', '', $request->value);
        }

        if($modal)
        {
            $modal->property_id= $id;
            $modal->slug = $request->pk;
            $modal->content = $request->value;
            $modal->save();
        }
        else
        {
            $modal = new PropertiesCustomField;
            $modal->property_id= $id;
            $modal->slug = $request->pk;
            $modal->content = $request->value;
            $modal->save();
        }
        $response = [
            'success' => true,
            'reload' => false,
        ];
        return response()->json($response);
    }

    public function update_bankoffer(Request $request,$id)
    {
        $response = [
            'success' => false,
        ];

        $column = $request->pk;
        $modal = BankFinancingOffer::find($id);

        if($column=="interest_rate" || $column=="tilgung" || $column=="fk_share_percentage" || $column=="fk_share_nominal")
        {
            $request->value = str_replace('.', '', $request->value);
            $request->value = str_replace(',', '.', $request->value);
            $request->value = str_replace('%', '', $request->value);
            $request->value = str_replace('€', '', $request->value);
        }

        $modal->$column = $request->value;
        $modal->save();

        $response = [
            'success' => true,
            'reload' => false,
        ];
        return response()->json($response);
    }

    public function emailsendinginfo(Request $request,$id)
    {
        $response = [
            'success' => false,
        ];

        $column = $request->pk;
        $modal = email_template2_sending_info::find($id);

        if($column=="interest_rate" || $column=="tilgung" || $column=="fk_share_percentage" || $column=="fk_share_nominal")
        {
            $request->value = str_replace('.', '', $request->value);
            $request->value = str_replace(',', '.', $request->value);
            $request->value = str_replace('%', '', $request->value);
            $request->value = str_replace('€', '', $request->value);
        }

        $modal->$column = $request->value;
        $modal->save();

        $response = [
            'success' => true,
            'reload' => false,
        ];
        return response()->json($response);
    }

    public function changeprovisioninfo(Request $request)
    {
        $response = [
            'success' => false,
        ];

        $column = $request->pk;
        $modal = Provision::where('id', $request->id)->first();

        if($column=="rent" || $column=="cost" || $column=="net_income" || $column=="commision_percent" || $column=="einnahmen_pm" || $column=="laufzeit_in_monate" || $column=="mietfrei" || $column=="jahrl_einnahmen")
        {
            $request->value = str_replace('.', '', $request->value);
            $request->value = str_replace(',', '.', $request->value);
            $request->value = str_replace('%', '', $request->value);
            $request->value = str_replace('€', '', $request->value);
        }

        if($modal)
        {
            $modal->property_id= $request->property_id;
            $modal->$column = $request->value;
            $modal->save();
        }
        else
        {
            $modal = new Provision;
            $modal->property_id= $request->property_id;
            $modal->$column = $request->value;
            $modal->save();
        }
        $response = [
            'success' => true,
        ];
        return response()->json($response);
    }
    public function changeempfehlungdetails(Request $request)
    {
        $response = [
            'success' => false,
        ];

        $column = $request->pk;

        $em = Empfehlung::where('tenant_id',$request->tenant_id)->first();
        if(!$em)
        {
            $m = new Empfehlung;
            $m->tenant_id = $request->tenant_id;
            $m->property_id = $request->property_id;
            // $m->deleted = 0;
            $m->save();
        }

        $modal = EmpfehlungDetail::where('property_id', $request->property_id)->where('tenant_id', $request->tenant_id)->where('slug', $column)->first();

        if($column=="amount1" || $column=="amount2" || $column=="amount5" || $column=="amount6" || $column=="amount7" || $column=="annual_revenue")
        {
            $request->value = str_replace('.', '', $request->value);
            $request->value = str_replace(',', '.', $request->value);
            $request->value = str_replace('%', '', $request->value);
            $request->value = str_replace('€', '', $request->value);
        }

        if($modal)
        {
            $modal->value = $request->value;
            $modal->save();
        }
        else
        {
            $modal = new EmpfehlungDetail;
            $modal->property_id = $request->property_id;
            $modal->tenant_id = $request->tenant_id;
            $modal->slug = $column;
            $modal->value = $request->value;
            $modal->save();
        }

        $total = Empfehlung::selectRaw('IFNULL(SUM(amount), 0) as total')->where('property_id',$request->property_id)->where('tenant_id', $request->tenant_id)->where('deleted',0)->first()->total;
        $detail = EmpfehlungDetail::where('property_id',$request->property_id)->get();

        $detail_arr = array();
        if($detail){
            foreach ($detail as $key => $value) {
                $detail_arr[$value->tenant_id][$value->slug] = $value->value;
            }
        }

        $a1 = 0;
        if(isset($detail_arr[$request->tenant_id]['amount1']) && $detail_arr[$request->tenant_id]['amount1'])
          $a1 = $detail_arr[$request->tenant_id]['amount1'];

        $a2 = 0;
        if(isset($detail_arr[$request->tenant_id]['amount2']) && $detail_arr[$request->tenant_id]['amount2'])
          $a2 = $detail_arr[$request->tenant_id]['amount2'];

        $a3 = $a1*12;
        /*if(isset($detail_arr[$request->tenant_id]['annual_revenue']) && $detail_arr[$request->tenant_id]['annual_revenue'])
            $a3 = $detail_arr[$request->tenant_id]['annual_revenue'];*/

        $a4 = $a1*$a2;

        $a5 = 0;
        if(isset($detail_arr[$request->tenant_id]['amount5']) && $detail_arr[$request->tenant_id]['amount5'])
          $a5 = $detail_arr[$request->tenant_id]['amount5'];

        $a6 = 0;
        if(isset($detail_arr[$request->tenant_id]['amount6']) && $detail_arr[$request->tenant_id]['amount6'])
          $a6 = $detail_arr[$request->tenant_id]['amount6'];

        $data = [
            'kosten_umbau' => number_format($total,2,',','.'),
            'laufzeit' => number_format($a2,2,',','.'),
            'mietfrei' => number_format($a5,2,',','.'),
            'jahrl_einnahmen' => number_format($a3,2,',','.'),
            'Gesamteinnahmen' => number_format($a4,2,',','.'),
            'Gesamteinnahmen_netto' => number_format($a4-$total-$a5,2,',','.'),
        ];

        $response = [
            'success' => true,
            'data' => $data
        ];
        return response()->json($response);

    }

    public function getProvisionbutton(Request $request)
    {
        $user = Auth::user();

        $emp_data = Empfehlung::where('property_id',$request->id)->where('deleted',0)->orderBy('name','asc')->get();

        foreach ($emp_data as $key => $value)
            $ractivity[$value->tenant_id][] = $value;

        $rents = DB::table('tenancy_schedule_items as tsi')
                    ->selectRaw('tsi.rent_end, tsi.rent_begin, tsi.name')
                    ->join('tenancy_schedules as ts', 'ts.id', '=', 'tsi.tenancy_schedule_id')
                    ->where('ts.property_id', $request->id)
                    ->whereIn('tsi.type', [1,2])
                    ->get();

        $rents_arr = [];
        if($rents){
            foreach ($rents as $rent) {
                if($rent->name){
                    $rents_arr[$rent->name]['rent_begin'] = $rent->rent_begin;
                    $rents_arr[$rent->name]['rent_end'] = $rent->rent_end;
                }
            }
        }

        $property_id = $request->id;
        $t_list = Provision::select('provisions.*','property_vacants.tenant_id as pvid','property_vacants.provision_not_release_status')
                ->leftJoin('property_vacants','provisions.tenant_id','=','property_vacants.id')
                ->where('provisions.property_id',$request->id)
                ->where('provisions.deleted',0)
                ->groupBy('provisions.id')
                ->get();
        if($t_list){
            foreach ($t_list as $key => $value) {
                $t_list[$key]->rent_begin = (isset($rents_arr[$value->name])) ? $rents_arr[$value->name]['rent_begin'] : '';
                $t_list[$key]->rent_end = (isset($rents_arr[$value->name])) ? $rents_arr[$value->name]['rent_end'] : '';
            }
        }

        $d = EmpfehlungDetail::where('property_id',$request->id)->get();



        $arr = array();
        $arr2 = array();
        foreach ($d as $key => $value) {
            $arr[$value->provision_id][$value->slug][] = $value;
            $arr2[$value->provision_id][$value->slug] = $value->value;
        }
        // pre($arr); die;


        $tenant_arr = array();
        foreach ($t_list as $key => $tid) {

            $tenant_arr[$tid->id] = $tid->name;

           $key = $tid->id;
           $list = array();
           if(isset($arr[$key])){
            $list = $arr[$key];
           }

           $f=1;
           $a['buttons1'][$key] = View::make('properties.getprovisionbutton', compact('user' ,'list','f'))->render();

           $f=2;
           $a['buttons2'][$key] = View::make('properties.getprovisionbutton', compact('user' ,'list','f'))->render();


       }
       $as_users = User::whereRaw('role=4 or second_role=4 or id=1 or id=10')->get();

       $properties = Properties::select('asset_m_id')->where('id',$request->id)->first();

       $t = 1;
       $a['table'] = View::make('properties.provisionTable', compact('t_list','arr2','as_users','properties', 't'))->render();

       $t = 2;
       $a['table1'] = View::make('properties.provisionTable', compact('t_list','arr2','as_users','properties', 't'))->render();

        $d = EmpfehlungDetail::where('property_id',$request->id)->where('slug','like','pbtn%')->orWhere('slug', 'provosion_mark_as_not_release')->orderBy('created_at', 'desc')->get();

        // return $tenant_arr;


        $a['logs'] = View::make('properties.getprovisionlogs', compact('user' ,'d','tenant_arr', 'user'))->render();
        return $a;
    }


    public function getReleasebutton(Request $request)
    {
        $user = Auth::user();
        $d = EmpfehlungDetail::where('tenant_id',$request->id)
        ->whereRaw('slug like "btn%" OR slug like "vacant_not_release"')
        ->get();

        $arr = array();

        foreach ($d as $key => $value) {
            $arr[$value->slug][$key] = $value;
        }
        // pre($arr);



        $a['buttons'] = View::make('properties.getreleasebutton', compact('user' ,'arr'))->render();

        $a['logs'] = View::make('properties.getreleaselogs', compact('user' ,'d'))->render();

        return $a;
    }
    public function update_einkauf_item(Request $request, $id, $type) {
         $response = [
            'success' => false,
        ];

        $column = $request->pk;
        $modal = PropertiesBuyDetail::where('property_id', $id)->where('type', $type)->first();
        if ($type == 402)
        {

            $v = validatedate($request->value);
            if($v)
            {
                $request->value = save_date_format($request->value);
            }
            else{
                $response['msg'] = "Please enter valid date in (dd.mm.yyyy) format";
                return response()->json($response);
            }
        }

        $response['success'] = true;


        if ($modal) {
            $modal->$column = $request->value;
            $modal->save();
        } else {
            $modal = new PropertiesBuyDetail;
            $modal->property_id = $id;
            $modal->type = $type;
            $modal->$column = $request->value;
            $modal->save();
        }

        if ($type == 402 && $request->value) {
            $a = Properties::where('main_property_id', '=', $id)->where('Ist','!=',0)->first();

            $v_name = Auth::user()->name;

            $ps_data = PropertiesBuy::where('property_id', $id)->first();

            if ($a)
                $name_of_property = $a->name_of_property;


            if ($ps_data && $ps_data->property_name) {
                $name_of_property = $ps_data->property_name;
            }
            $name = "Janine";
            $email = "j.klausch@fcr-immobilien.de";
            // $email = "c.wiedemann@fcr-immobilien.de";
            $url = route('properties.show', ['property' => $id]);

            if( !in_array($email, $this->mail_not_send()) ){
                try {
                    Mail::send('emails.einkauf_date_change', ['name' => $name, 'date' => $request->value, 'v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url], function ($m) use ($email, $name) {
                        $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                        $m->to($email, $name)->subject("Neuer BNL Termin");
                    });
                    // echo "sent";
                } catch (\Exception $e) {

                }
            }

            $name = "Andrea";
            $email = "a.raudies@fcr-immobilien.de";
            // $email = "c.wiedemann@fcr-immobilien.de";
            $url = route('properties.show', ['property' => $id]);

            if( !in_array($email, $this->mail_not_send()) ){
                try {
                    Mail::send('emails.einkauf_date_change', ['name' => $name, 'date' => $request->value, 'v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url], function ($m) use ($email, $name) {
                        $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                        $m->to($email, $name)->subject("Neuer BNL Termin");
                    });
                } catch (\Exception $e) {

                }
            }

            $name = "Christian";
            $email = "c.musacchio@fcr-immobilien.de";
            // $email = "c.wiedemann@fcr-immobilien.de";
            $url = route('properties.show', ['property' => $id]);

            if( !in_array($email, $this->mail_not_send()) ){

                try {
                    Mail::send('emails.einkauf_date_change', ['name' => $name, 'date' => $request->value, 'v_name' => $v_name, 'name_of_property' => $name_of_property, 'url' => $url], function ($m) use ($email, $name) {
                        $m->from('info@intranet.fcr-immobilien.de', 'FCR INTRANET');
                        $m->to($email, $name)->subject("Neuer BNL Termin");
                    });
                } catch (\Exception $e) {

                }
            }
        }
        $response['reload'] = false;
        return response()->json($response);
    }

    public function update_einkauf(Request $request, $prop_id) {
        $c = $request->pk;
        $properties = PropertiesBuy::where('property_id', '=', $prop_id)->first();

        if ($properties) {
            $properties->$c = $request->value;
            $properties->save();
        } else {
            $properties = new PropertiesBuy;
            $properties->property_id = $prop_id;
            $properties->$c = $request->value;
            $properties->save();
        }
    }

    public function searchpbank(Request $request) {
        if (isset($_REQUEST['query']['term'])) {
            $rate = DB::table('banken')->where('Firma', 'like', '%' . $_REQUEST['query']['term'] . '%')->limit(20)->get();
            $results = array();
            foreach ($rate as $key => $value) {
                $results[$key]['name'] = $value->Firma;
                $results[$key]['id'] = $value->Kennung;
                $results[$key]['code'] = $value->Firma;
            }
            return $results;
        }
    }

    public function searchcity(Request $request) {
        if (isset($_REQUEST['query']['term'])) {
            $rate = RateCity::where('name', 'like', '%' . $_REQUEST['query']['term'] . '%')->limit(2000)->get();
            $results = array();
            foreach ($rate as $key => $value) {
                $results[$key]['name'] = $value->name;
                $results[$key]['id'] = $value->name;
                $results[$key]['code'] = $value->name;
            }
            return $results;
        }

        $rate = RateCity::where('name', 'like', '%' . $_REQUEST['query'] . '%')->limit(2000)->get();

        $results = array();
        foreach ($rate as $key => $value) {
            $results[]['name'] = $value->name;
        }
        return $results;
    }

    public function searchbank(Request $request) {
        if (isset($_REQUEST['term']))
            $_REQUEST['query'] = $_REQUEST['term'];
        $rate = Banks::where('name', 'like', '%' . $_REQUEST['query'] . '%')->limit(20)->get();

        $results = array();
        foreach ($rate as $key => $value) {
            $results[$key]['name'] = $value->name;
            $results[$key]['id'] = $value->id;
        }
        return $results;
    }
    public function bankexport($id) {
        $properties = Properties::where('id', '=', $id)->first();
        // print_r($properties); die;

        $main_properties = Properties::where('id', $properties->main_property_id)->first();
        if ($properties->kk_idx)
            $kk_idx = number_format($properties->kk_idx, 2, ",", ".");

        $pr_user = User::where('id', $properties->user_id)->first();
        $properties->user_name = "";
        if($pr_user)
        $properties->user_name = $pr_user->name;

        $clever_fit_fields = json_decode($properties->clever_fit_fields, true);

        if (!is_array($clever_fit_fields)) {
            $clever_fit_fields = array();
        }
        $clever_fit_fields = PropertiesService::initCleverFitFields($clever_fit_fields);

        $properties->clever_fit_fields = $clever_fit_fields;



        $planung_fields = json_decode($properties->planung_fields, true);

        if (!is_array($planung_fields)) {
            $planung_fields = array();
        }
        $planung_fields = PropertiesService::initPlanungFields($planung_fields);

        $properties->planung_fields = $planung_fields;

        $tenants = Tenants::where('property_id', $id)->get();

        $bank_ids = json_decode($properties->bank_ids);
        $banks = [];

        $bank_all = [];
        $fake_bank = new Banks();
        $fake_bank->name = '';
        $fake_bank->user_id = 0;
        $fake_bank->with_real_ek = 0;
        $fake_bank->from_bond = 0.2;
        $fake_bank->bank_loan = 0.8;
        $fake_bank->interest_bank_loan = 0.02;
        $fake_bank->eradication_bank = 0.04;
        $fake_bank->interest_bond = 0.0425;
        if ($bank_ids != null) {
            foreach ($bank_ids as $bank_id) {
                $banks[] = Banks::where('id', '=', $bank_id)->first();
            }
        } else {
            $banks[] = $fake_bank;
        }

        $bank_all = Banks::all();

        $comments_new = array();


        $schl_banks = $properties->schl_banks != null ? (array) json_decode($properties->schl_banks) : [];

        $tenancy_schedule_data = TenancySchedulesService::get($id);
        $properties->masterliste = Masterliste::where('id', $properties->masterliste_id)->first();

        $propertiesExtra1s = PropertiesTenants::where('propertyId', $properties->id)->get();


        $tr_users = User::where('role', 2)->orWhere('second_role', 2)->get();
        $as_users = User::where('role', 4)->orWhere('second_role', 4)->get();

        $properties_user = Properties::select('transaction_m_id', 'asset_m_id', 'seller_id')->where('id', '=', $id)->first();


        $pdf = PDF::loadView('pdf.bankexport', compact('properties', 'properties_user', 'id', 'banks', 'tenants', 'schl_banks', 'tenancy_schedule_data', 'fake_bank', 'bank_all', 'propertiesExtra1s', 'comments_new',  'tr_users', 'as_users', 'main_properties', 'e_w', 'kk_idx'))->setPaper('a3', 'landscape');
        return $pdf->stream('bankexport.pdf');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function banksIframe($id, $bank_id, $tab) {


        // $properties = Properties::where($tab,'=',$bank_id)->where('main_property_id',$id)->first();

        $properties = Properties::where('id', '=', $bank_id)->where('main_property_id', $id)->first();

        if ($properties == null) {
            die();
        }

        $e_w = "";
        $kk_idx = 0;



        $main_properties = Properties::where('id', $id)->first();
        if ($properties->kk_idx)
            $kk_idx = number_format($properties->kk_idx, 2, ",", ".");

        // print_r($main_properties); die;

        if ($main_properties && $main_properties->ort) {
            // echo $main_properties->ort;
            $rate = RateCity::where('name', 'like', $main_properties->ort)->first();
            // var_dump($rate);die;
            if ($rate) {
                $e_w = $rate->e_w;
                if (!$kk_idx)
                    $kk_idx = $rate->kk_idx;
                $kk_idx = number_format($kk_idx, 2, ",", ".");
                $properties->population = $e_w;
                $properties->save();
            }
        }
        // print_r($properties); die;
        // echo $properties->user_id;
        $pr_user = User::where('id', $properties->user_id)->first();
        $properties->user_name = "";
        if($pr_user)
        $properties->user_name = $pr_user->name;
        // echo $properties->user_name;

        $clever_fit_fields = json_decode($properties->clever_fit_fields, true);

        if (!is_array($clever_fit_fields)) {
            $clever_fit_fields = array();
        }
        $clever_fit_fields = PropertiesService::initCleverFitFields($clever_fit_fields);

        $properties->clever_fit_fields = $clever_fit_fields;



        $planung_fields = json_decode($properties->planung_fields, true);

        if (!is_array($planung_fields)) {
            $planung_fields = array();
        }
        $planung_fields = PropertiesService::initPlanungFields($planung_fields);

        $properties->planung_fields = $planung_fields;

        $tenants = Tenants::where('property_id', $id)->get();

        $bank_ids = json_decode($properties->bank_ids);
        $banks = [];

        $bank_all = [];
        $fake_bank = new Banks();
        $fake_bank->name = '';
        $fake_bank->user_id = 0;
        $fake_bank->with_real_ek = 0;
        $fake_bank->from_bond = 0.2;
        $fake_bank->bank_loan = 0.8;
        $fake_bank->interest_bank_loan = 0.02;
        $fake_bank->eradication_bank = 0.04;
        $fake_bank->interest_bond = 0.0425;
        if ($bank_ids != null) {
            foreach ($bank_ids as $bank_id) {
                $banks[] = Banks::where('id', '=', $bank_id)->first();
            }
        } else {
            $banks[] = $fake_bank;
        }

        $bank_all = Banks::all();

        $comments_new = array();


        $schl_banks = $properties->schl_banks != null ? (array) json_decode($properties->schl_banks) : [];

        $tenancy_schedule_data = TenancySchedulesService::get($id);
        $properties->masterliste = Masterliste::where('id', $properties->masterliste_id)->first();

        $propertiesExtra1s = PropertiesTenants::where('propertyId', $properties->id)->get();


        $tr_users = User::where('role', 2)->orWhere('second_role', 2)->get();
        $as_users = User::where('role', 4)->orWhere('second_role', 4)->get();

        $properties_user = Properties::select('transaction_m_id', 'asset_m_id', 'seller_id')->where('id', '=', $id)->first();



        // echo $properties->user_name;
        // die;




        return view('properties.showIframe_ist_sole', compact('properties', 'properties_user', 'id', 'banks', 'tenants', 'schl_banks', 'tenancy_schedule_data', 'fake_bank', 'bank_all', 'propertiesExtra1s', 'comments_new', 'tab', 'tr_users', 'as_users', 'main_properties', 'e_w', 'kk_idx'));
    }

    public function update_property_budget(Request $request,$id)
    {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $budget = Budget::where('property_id', '=', $id)->first();

        if(!$budget)
        {
            $budget = new Budget;
            $budget->property_id = $id;
            $budget->save();
        }
        $k = $request->pk;

        $new_value = str_replace('.', '', $request->value);
        $new_value = str_replace(',', '.', $new_value);
        if($k=='net_rent' || $k==='net_rent_empty' || $k=='tax')
            $new_value = $new_value/100;

        if($k=='maintenance' || $k==='operating_costs' || $k=='object_management')
            $new_value = $new_value/100;

        $budget->$k = $new_value;
        $budget->save();

        $response = [
                'success' => true,
                'msg' => '',
            ];
        return response()->json($response);


    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_property_ist_soll(Request $request, $tab, $bank_id) {

        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $properties = Properties::where('id', '=', $tab)->first();

        if (empty($properties)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        // Clone property
        $input = $properties;
        $list_fields_percent = [
            'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
        ];
        foreach ($list_fields_percent as $field) {
            if (isset($input[$field]))
                $input[$field] *= 100;
        }

        if ($request->pk == "datum_lol" && $request->value) {
            $date = $request->value;
            if ($date != date_format(date_create(str_replace('/', '-', $date)), 'd/m/Y')) {
                $response['msg'] = "Please enter date in (dd/mm/yyyy) format";
                return response()->json($response);
            } else {
                $request->value = date_format(date_create(str_replace('/', '-', $date)), 'Y-m-d');
            }
        }


        // New value
        $new_value = str_replace('.', '', $request->value);
        $new_value = str_replace(',', '.', $new_value);
        $input[$request->pk] = $new_value;


        if (PropertiesService::update($input, $properties)) {

            $ist = $properties->Ist;
            $main_property_id = $properties->main_property_id;

            if ($properties->Ist && $request->pk != "maintenance_nk" && $request->pk != "operating_costs_nk" && $request->pk != "object_management_nk" && $request->pk != "depreciation_nk") {
                $properties = Properties::where('soll', '=', $bank_id)->where('main_property_id', $main_property_id)->first();

                if ($properties) {
                    $input = $properties;
                    $list_fields_percent = [
                        'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
                    ];
                    foreach ($list_fields_percent as $field) {
                        if (isset($input[$field]))
                            $input[$field] *= 100;
                    }
                    // New value
                    $new_value1 = str_replace('.', '', $request->value);
                    $new_value1 = str_replace(',', '.', $new_value);
                    $input[$request->pk] = $new_value1;

                    PropertiesService::update($input, $properties);
                }
            }



            $response = [
                'success' => true,
                'ist' => $ist,
                'msg' => '',
                'selecting_tenancy_schedule' => isset($_REQUEST['selecting_tenancy_schedule']) ? $_REQUEST['selecting_tenancy_schedule'] : ''
            ];
        }

        return response()->json($response);
    }

    //Method for get User Form Answers
    public function showResult(Request $request) {
        $result = email_template2_sending_info::join('user_form_data', 'user_form_data.email_id', '=', 'email_template2_sending_info.id')
                        ->where('email_template2_sending_info.email_to_send', $request->email)->get()->toArray();
        echo json_encode($result);
    }

    public function showIframe($id) {

        //Get selecting property tabs->where('Ist',0)->where('soll',0)


        $properties = Properties::where('id', '=', $id)->where('Ist', 0)->where('soll', 0)->first();
        $prsuser = User::where('id', $properties->user_id)->first();
        $properties->user_name = "";
        if($prsuser)
        $properties->user_name = $prsuser->name;

        $clever_fit_fields = json_decode($properties->clever_fit_fields, true);

        if (!is_array($clever_fit_fields)) {
            $clever_fit_fields = array();
        }
        $clever_fit_fields = PropertiesService::initCleverFitFields($clever_fit_fields);

        $properties->clever_fit_fields = $clever_fit_fields;



        $planung_fields = json_decode($properties->planung_fields, true);

        if (!is_array($planung_fields)) {
            $planung_fields = array();
        }
        $planung_fields = PropertiesService::initPlanungFields($planung_fields);

        $properties->planung_fields = $planung_fields;

        $tenants = Tenants::where('property_id', $id)->get();

        $bank_ids = json_decode($properties->bank_ids);
        $banks = [];

        $bank_all = [];
        $fake_bank = new Banks();
        $fake_bank->name = '';
        $fake_bank->user_id = 0;
        $fake_bank->with_real_ek = 0;
        $fake_bank->from_bond = 0.2;
        $fake_bank->bank_loan = 0.8;
        $fake_bank->interest_bank_loan = 0.02;
        $fake_bank->eradication_bank = 0.04;
        $fake_bank->interest_bond = 0.0425;
        if ($bank_ids != null) {
            foreach ($bank_ids as $bank_id) {
                $b = Banks::where('id', '=', $bank_id)->first();
                if ($b)
                    $banks[] = $b;
            }
        }else {
            $banks[] = $fake_bank;
        }

        $bank_all = Banks::all();

        $comments_new = PropertyComments::where('property_id', $id)->where('user_id', Auth::id())->where('status', null)->get()->toArray();

        $tr_users = User::where('role', 2)->orWhere('second_role', 2)->get();
        $as_users = User::where('role', 4)->orWhere('second_role', 4)->get();
        $contact_person = User::where('id', $properties->transaction_m_id)->first();
        if ($contact_person) {
            $properties->contact_person = $contact_person->name;
        }


        $schl_banks = $properties->schl_banks != null ? (array) json_decode($properties->schl_banks) : [];

        $tenancy_schedule_data = TenancySchedulesService::get($id);
        $properties->masterliste = Masterliste::where('id', $properties->masterliste_id)->first();
        $propertiesExtra1s = PropertiesExtra1::where('propertyId', $id)->get();



        $bank_popup_data = array();
        if (is_numeric($properties->plz_ort)) {

            $postal_code = substr($properties->plz_ort, 0, 2);
            $bank_popup_data = DB::select(" select * from banks where postal_code like '" . $postal_code . "%'  ");
            if (count($bank_popup_data) == 0) {

                $postal_code = substr($properties->plz_ort, 0, 1);
                $bank_popup_data = DB::select(" select * from banks where postal_code like '" . $postal_code . "%'  ");
            }
        }

        $send_email_template2_sending_info = email_template2_sending_info::where('property_id', $properties->id)->get();

        $email_template2 = email_template2::where('property_id', $properties->id)->get();


        return view('properties.showIframe', compact('properties', 'id', 'banks', 'send_email_template2_sending_info', 'tenants', 'bank_popup_data', 'schl_banks', 'tenancy_schedule_data', 'fake_bank', 'tr_users', 'as_users', 'bank_all', 'propertiesExtra1s', 'comments_new', 'email_template2'));
    }

    public static function diff360($date1, $date2) {
        //format: Y-m-d
        if (!$date1 || !$date2) {
            return 0;
        }
        $arrDate1 = explode('-', $date1);
        $arrDate2 = explode('-', $date2);
        $years = abs(intval($arrDate1[0]) - intval($arrDate2[0]));
        $months = abs(intval($arrDate1[1]) - intval($arrDate2[1]));
        $days = abs(intval($arrDate1[2]) - intval($arrDate2[2]));
        return $years * 360 + $months * 30 + $days;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $properties = Properties::where('id', '=', $id)->where('Ist', 0)->where('soll', 0)->first();
        return view('properties.edit', compact('properties'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_property(Request $request, $id) {
        $properties = Properties::where('id', '=', $id)->where('Ist', 0)->where('soll', 0)->first();
        $input = $request->all();

        // print_r($input); die;
        if (PropertiesService::update($input, $properties)) {
            $notification_data = [
                'type' => config('notification.type.edit_property'),
                'property_id' => $properties->id
            ];
            NotificationsService::create($notification_data);
            return redirect()->action('PropertiesController@index')
                            ->with('message', trans('property.properties_controller.updated_success'));
        }
        return redirect()->action('PropertiesController@index')
                        ->with('error', trans('property.properties_controller.updated_fail'));
    }

    public function add_property_bank(Request $request) {
        $arr['status'] = 0;
        if (!$request->banken_id) {
            $arr['message'] = "Please select bank";
            return $arr;
        }

        DB::beginTransaction();

        try {
            $tenant = new PropertiesBanken;
            $tenant->banken_id = $request->banken_id;
            $tenant->property_id = $request->property_id;
            $tenant->save();

            DB::commit();
            $arr['status'] = 1;
            $arr['message'] = "added successfully";

            return $arr;
        } catch (\Exception $e) {
            DB::rollback();
            $arr['message'] = $e->getMessage();
            return $arr;
        }
    }

    public function removepropertybanken(Request $request) {
        if ($request->id) {
            $property = PropertiesBanken::find($request->id);
            $property->delete();
        }
        echo "1";
    }

    public function removestatusloi(Request $request) {
        if ($request->id) {
            $property = StatusLoi::find($request->id);
            $property->delete();
        }
        echo "1";
    }

    public function updatestatusloi(Request $request, $id) {
        if ($id) {
            $cl = $request->pk;
            $property = StatusLoi::find($request->id);
            $property->$cl = $request->value;
            $property->save();
        }
    }

    public function updatebanken(Request $request, $id) {
        $arr['success'] = false;




        DB::beginTransaction();
        $cl = $request->pk;
        try {
            $tenant = PropertiesBanken::find($id);
            if ($cl != "comment") {
                $request->value = str_replace('.', '', $request->value);
                $request->value = str_replace(',', '.', $request->value);
            }


            $tenant->$cl = $request->value;
            $tenant->save();

            DB::commit();
            $arr['success'] = true;
            // $arr['msg']= "added successfully";

            return $arr;
        } catch (\Exception $e) {
            DB::rollback();
            $arr['msg'] = $e->getMessage();
            return $arr;
        }
    }

    public function addNewCity(Request $request) {
        $arr['status'] = 0;
        if (!$request->name) {
            $arr['message'] = "Please enter city name";
            return $arr;
        }

        $r = RateCity::where('name', $request->name)->first();
        if ($r) {
            $arr['message'] = "City already exists";
            return $arr;
        }

        DB::beginTransaction();

        try {
            $tenant = new RateCity;
            $tenant->name = $request->name;
            if ($request->e_w) {
                $request->e_w = trim($request->e_w);
                $value = str_replace('.', '', $request->e_w);
                $value = str_replace(',', '.', $value);

                if (!is_numeric($value)) {
                    $arr['message'] = "Please enter proper value for Einwohner";
                    return $arr;
                }

                $tenant->e_w = $value;
            }
            if ($request->kk_idx) {
                $request->kk_idx = trim($request->kk_idx);
                $value = str_replace('.', '', $request->kk_idx);
                $value = str_replace(',', '.', $value);

                if (!is_numeric($value)) {
                    $arr['message'] = "Please enter proper value for Kaufkraftindex";
                    return $arr;
                }
                $tenant->kk_idx = $value;
            }

            $tenant->save();

            DB::commit();
            $arr['status'] = 1;
            $arr['message'] = "added successfully";

            return $arr;
        } catch (\Exception $e) {
            DB::rollback();
            $arr['message'] = $e->getMessage();
            return $arr;
        }
    }

    public function addNewBank(Request $request) {
        $arr['status'] = 0;
        if (!$request->name) {
            $arr['message'] = "Please enter name";
            return $arr;
        }

        DB::beginTransaction();
        $user_id = Auth::id();
        try {
            $tenant = new Banks;
            $tenant->user_id = $user_id;
            $tenant->name = $request->name;
            if ($request->contact_name)
                $tenant->contact_name = $request->contact_name;
            if ($request->contact_phone)
                $tenant->contact_phone = $request->contact_phone;
            if ($request->contact_email)
                $tenant->contact_email = $request->contact_email;
            if ($request->address)
                $tenant->address = $request->address;
            if ($request->notes)
                $tenant->notes = $request->notes;

            $tenant->save();

            DB::commit();
            $arr['status'] = 1;
            $arr['message'] = "added successfully";

            return $arr;
        } catch (\Exception $e) {
            DB::rollback();
            $arr['message'] = $e->getMessage();
            return $arr;
        }
    }

    public function addNewBanken(Request $request) {
        $arr['status'] = 0;
        if (!$request->Firma) {
            $arr['message'] = "Please enter name";
            return $arr;
        }
        if (!$request->E_Mail) {
			$arr['message'] = "Please enter email";
			return $arr;
        }
        foreach ($request->E_Mail as $email)
        {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $arr['message'] = "Invalid email";
                return $arr;
            }
        }




        DB::beginTransaction();
        try {

            $data['name'] = $request->Firma;
            $data['user_id'] = Auth::id();
            $data['salutation'] = $request->Anrede;
            $data['fullName'] = $request->Vorname.' '.$request->Nachname;
            $data['address'] = $request->Strasse;
            $data['city'] = $request->Ort;
            $data['contact_phone'] = $request->Telefon;
            $data['fax'] = $request->Fax;
            $data['contact_email'] = implode(' || ', $request->E_Mail);

            $created_ad = DB::table('banks')->insertGetId($data);

            DB::commit();
            $arr['status'] = 1;
            $arr['message'] = "added successfully";

            return $arr;
        } catch (\Exception $e) {
            DB::rollback();
            $arr['message'] = $e->getMessage();
            return $arr;
        }
    }

    public function getBank($id)
    {
        $bank= Banks::find($id);
        if ($bank->fullName && explode(' ', $bank->fullName))
        {
            $bank['firstName']= explode(' ', $bank->fullName)[0];
            $bank['surname']= explode(' ', $bank->fullName)[1];
        }
        $bank['contact_email']= explode(' || ', $bank->contact_email);
        return response()->json($bank);
    }

    public function updateBank(Request $request)
    {
        $arr['status'] = 0;
        if (!$request->Firma) {
            $arr['message'] = "Please enter name";
            return $arr;
        }
        if (!$request->E_Mail) {
            $arr['message'] = "Please enter email";
            return $arr;
        }
        foreach ($request->E_Mail as $email)
        {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $arr['message'] = "Invalid email";
                return $arr;
            }
        }




        DB::beginTransaction();
        try {
            $data = Banks::find($request->id_bank);
            $data['name'] = $request->Firma;
            $data['user_id'] = Auth::id();
            $data['salutation'] = $request->Anrede;
            $data['fullName'] = $request->Vorname.' '.$request->Nachname;
            $data['address'] = $request->Strasse;
            $data['city'] = $request->Ort;
            $data['contact_phone'] = $request->Telefon;
            $data['fax'] = $request->Fax;
            $data['contact_email'] = implode(' || ', $request->E_Mail);

            $data->save();

            DB::commit();
            $arr['status'] = 1;
            $arr['message'] = "added successfully";

            return $arr;
        } catch (\Exception $e) {
            DB::rollback();
            $arr['message'] = $e->getMessage();
            return $arr;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $property = Properties::find($id);

        DB::table('properties')->where('main_property_id', $id)->delete();

        $property->delete();
        return redirect()->back()
                        ->with('message', trans('property.properties_controller.deleted_success'));
    }

    public function deletePdf(Request $request)
	{
		$properties_pdf = DB::table('pdf')->where('id', $request->id)->first();
		if ($properties_pdf) {
			Storage::cloud()->delete($properties_pdf->file_path);
			$response = [
				'success' => true,
				'msg' => 'File delete successfully.'
			];

			DB::table('pdf')->where('id', $request->id)->delete();
		}
		echo "1";
		die;
	}

    public function updatepropertyuser(Request $request) {
        $input = $request->all();
        $arr['status'] = 1;
        $property = Properties::where('id', $input['property_id'])->first();
        if ($property) {

            $property->seller_id = $input['seller_id'];
            $property->transaction_m_id = $input['transaction_m_id'];
            $property->asset_m_id = $input['asset_m_id'];
            $property->asset_m_id2 = $input['asset_m_id2'];
            $property->construction_manager = $input['construction_m_id'];
            $property->property_status = $input['property_status'];
            $property->save();

            if($input['changed_field'] == 'asset_m_id'){
                $this->addPropertyHistory($input['property_id'], $input['old_asset_m_id'], $input['asset_m_id'], 'asset_m_id');
            }
            if($input['changed_field'] == 'asset_m_id2'){
                $this->addPropertyHistory($input['property_id'], $input['old_asset_m_id2'], $input['asset_m_id2'], 'asset_m_id2');
            }

            // $properties = Properties::where('id', '=', $input['property_id'])->first();


            $ist_soll_properties = Properties::where('main_property_id', $property->id)->update(['seller_id' => $input['seller_id'], 'transaction_m_id' => $input['transaction_m_id'], 'asset_m_id' => $input['asset_m_id'],'asset_m_id2' => $input['asset_m_id2'],'construction_manager'=>$input['construction_m_id'],'property_status'=>$input['property_status']]);

            /* if(preg_replace("/[^0-9\.]/", '', $properties->bank_ids)){
              $bank_ids_ist_soll = ltrim($properties->bank_ids, '[');
              $bank_ids_ist_soll = rtrim($bank_ids_ist_soll, ']');
              $final_bank_ids = explode(',',$bank_ids_ist_soll);

              $ist_soll_properties = Properties::whereIn('Ist',$final_bank_ids)->orWhereIn('soll',$final_bank_ids)
              ->update(['seller_id'=>$input['seller_id'], 'transaction_m_id' => $input['transaction_m_id'], 'asset_m_id' =>$input['asset_m_id']]);

              } */
            //   if($property->property_status == 1){
            //     $property->status = 18;
            // }
              PropertiesHelperClass::changePropertyDirAsStatus($property->id, $property->status);

        }
        // echo "1";
        return $arr;
        die;
    }

    public function set_seller_data() {

        $property = Properties::where('Ist', 0)->where('soll', 0)->get();

        foreach ($property as $prop) {
            $url = 'https://intranet.fcr-immobilien.de/index.php/updatepropertyuser?transaction_m_id=' . $prop->transaction_m_id . '&seller_id=' . $prop->seller_id . '&asset_m_id=' . $prop->asset_m_id . '&property_id=' . $prop->id . ' ';
            echo '<pre>';
            echo '<a href="' . $url . '">' . $url . '</a>';
            echo '</pre>';
        }
    }

    public function removebank(Request $request) {
        $input = $request->all();
        if ($input['property_id'] == 34) {
            echo "1";
            die;
        }


        $property = Properties::where('id', $input['property_id'])->first();
        if ($property && $property->bank_ids) {
            $a = json_decode($property->bank_ids, true);
            $k = array_search($input['bank_id'], $a);

            if (isset($a[$k])) {
                // $properties1 = Properties::where('Ist', $a[$k])->where('main_property_id',$property->id)->first();
                // $properties1->Ist = 34;
                // $properties1->properties_bank_id = 34;
                // $properties1->save();

                Properties::where('main_property_id', $property->id)->where('Ist', $a[$k])->update(['Ist' => 34, 'properties_bank_id' => 34]);

                Properties::where('main_property_id', $property->id)->where('soll', $a[$k])->update(['soll' => 34, 'properties_bank_id' => 34]);


                // $properties2 = Properties::where('soll', $a[$k])->where('main_property_id',$property->id)->first();
                // $properties2->soll = 34;
                // $properties2->save();
            }
            $a[$k] = 34;
            $a = array_values($a);
            $property->bank_ids = json_encode($a);
            $property->save();
        }
        echo "1";
        die;
    }

    public function changeStatus(Request $request) {
        $input = $request->all();
        $arr['status'] = 1;
        if (false && $input['status'] == 11) {
            $ad = Ad::where('property_id', $input['property_id'])->first();
            if ($ad)
                $this->postlisting($ad->id);
        }
        $property = Properties::where('id', $input['property_id'])->where('Ist', 0)->where('soll', 0)->first();

        $c = StatusLoi::where('property_id',$input['property_id'])->get()->count();
        if($input['status']!=$property->status && $c==0 && $input['status']==7)
        {
            $arr['status'] = 0;
            $arr['oldstatus'] = $property->status;
            $arr['message'] = "Status kann nicht geändert werden, da LOI noch nicht verschickt wurde";
            return $arr;
        }

        if($input['status']!=$property->status && $input['status']==10)
        {

            $subject = $property->name_of_property.' Steuerberater festlegen';
            $email = "t.raudies@fcr-immobilien.de";
            $url = route('properties.show',['property'=>$property->id]);
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $text = "Hallo Thorsten, <br><br>bitte bei dem Objekt ".$property->name_of_property." den Steuerberater hinterlegen: ".$url;

            if( !in_array($email, $this->mail_not_send()) ){

                Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject) {
                    $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->subject($subject);
                });
            }

            $text = "Hallo Alex, <br><br>bitte bei dem Objekt ".$property->name_of_property." den Steuerberater hinterlegen: ".$url;

            $email = "a.lauterbach@fcr-immobilien.de";

            if( !in_array($email, $this->mail_not_send()) ){

                Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject) {
                    $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->subject($subject);
                });
            }

        }




        if ($property->status == config('properties.status.duration') && $input['status'] == config('properties.status.sold')) {
            $masterliste = Masterliste::where('id', $property->masterliste_id)->delete();
            $property->masterliste_id = null;
            $property->save();
        }
        if ($input['status'] == config('properties.status.duration')) {
            $masterliste = Masterliste::create(
                            [
                                'object' => $property->name_of_property,
                                'asset_manager' => $property->user_id,
                                'flat_in_qm' => 0,
                                'vacancy' => 0,
                                'vacancy_structurally' => 0,
                                'technical_ff' => 0,
                                'rented_area' => 0,
                                'rent_net_pm' => 0,
                                'avg_rental_fee' => 0,
                                'potential_pm' => 0,
            ]);
            if ($masterliste) {
                $property->masterliste_id = $masterliste->id;
                $property->save();
            }
            MasterlisteService::sync_all_masterliste();
        }

        PropertiesService::updateStatus($property, $input['status']);
        Properties::where('main_property_id', $input['property_id'])->update(['status' => $input['status'],'status_update_date'=>date('Y-m-d')]);
        $propertyId = $input['property_id'];
        $newStatus = $input['status'];

        PropertiesHelperClass::changePropertyDirAsStatus($propertyId, $newStatus);

        $this->addPropertyHistory($request->property_id, $request->oldstatus, $request->status, 'status');

        return $arr;
        die;
    }

    function addPropertyHistory($property_id, $old_value, $new_value, $field_type){
        return $model = PropertyHistory::create([
                'user_id'       => Auth::user()->id,
                'property_id'   => $property_id,
                'old_value'     => $old_value,
                'new_value'     => $new_value,
                'field_type'    => $field_type,
            ]);
    }

    public function update_property_bank(Request $request, $id, $bank_id) {
        // echo $id;
        // echo $bank_id;
        // dd($request->all());

        $property = Properties::where('id', $id)->first();
        if (!is_null($property)) {
            $a = json_decode($property->bank_ids, true);
            $k = array_search($bank_id, $a);
            if (isset($a[$k])) {


                Properties::where('main_property_id', $id)->where('Ist', $bank_id)->update(['Ist' => $request->value, 'properties_bank_id' => $request->value]);

                Properties::where('main_property_id', $id)->where('soll', $bank_id)->update(['soll' => $request->value, 'properties_bank_id' => $request->value]);



                // $properties1 = Properties::where('Ist', $bank_id)->where('main_property_id',$id)->first();
                // $properties2 = Properties::where('soll', $bank_id)->where('main_property_id',$id)->first();

                $a[$k] = $request->value;
                $a = array_values($a);
                // $property->bank_ids = json_encode($a);
                // $property->save();
                // if($properties1)
                // {
                //   $properties1->bank_ids = json_encode($a);
                //   $properties1->Ist = $request->value;
                //   $properties1->properties_bank_id = $request->value;
                //   $properties1->save();
                // }
                // if($properties2)
                // {
                //   $properties2->bank_ids = json_encode($a);
                //   $properties2->soll = $request->value;
                //   $properties1->properties_bank_id = $request->value;
                //   $properties2->save();
                // }

                Properties::whereRaw('main_property_id=' . $id . ' or id=' . $id)->update(['bank_ids' => json_encode($a)]);
                // print_r($properties); die;
                // unset($a[$k]);
            }
        }
        $response = [
            'success' => true,
            'msg' => 'Successfully updated property'
        ];

        return $response;
    }

    public function update_property_bank_fields(Request $request, $prop_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $properties = Properties::where('id', '=', $prop_id)->where('Ist', 0)->where('soll', 0)->first();

        if (empty($properties)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }
        $input = $properties;
        $list_fields_percent = [
            'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
        ];

        $input['with_real_ek'] = $request->with_real_ek;
        $input['from_bond'] = $request->from_bond;
        $input['bank_loan'] = $request->bank_loan;
        $input['interest_bank_loan'] = $request->interest_bank_loan;
        $input['eradication_bank'] = $request->eradication_bank;
        $input['interest_bond'] = $request->interest_bond;
        $input['bank'] = $request->bank;
        foreach ($list_fields_percent as $field) {
            if (isset($input[$field]))
                $input[$field] *= 100;
        }
        if (PropertiesService::update($input, $properties)) {
            $response = [
                'success' => true,
                'msg' => '',
                'selecting_tenancy_schedule' => isset($_REQUEST['selecting_tenancy_schedule']) ? $_REQUEST['selecting_tenancy_schedule'] : ''
            ];
        }

        return response()->json($response);
    }

    public function update_property_by_field(Request $request, $prop_id) {

        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $properties = Properties::where('id', '=', $prop_id)->where('Ist', 0)->where('soll', 0)->first();

        if (empty($properties)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        // Clone property
        $input = $properties;
        $list_fields_percent = [
            'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
        ];
        foreach ($list_fields_percent as $field) {
            if (isset($input[$field]))
                $input[$field] *= 100;
        }
        $new_value = $request->value;

        // New value
        if($request->pk!="strengths" && $request->pk!="weaknesses")
        {
            $new_value = str_replace('.', '', $request->value);
            $new_value = str_replace(',', '.', $new_value);

        }
        $input[$request->pk] = $new_value;

        if (PropertiesService::update($input, $properties)) {
            $response = [
                'success' => true,
                'msg' => '',
                'selecting_tenancy_schedule' => isset($_REQUEST['selecting_tenancy_schedule']) ? $_REQUEST['selecting_tenancy_schedule'] : ''
            ];
        }

        return response()->json($response);
    }

    public function update_property_schl_by_field(Request $request, $prop_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];



        $properties = Properties::where('id', '=', $prop_id)->where('Ist', 0)->where('soll', 0)->first();
        if (empty($properties)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }


        if($request->pk=="hvbu_id" || $request->pk=="hvpm_id" || $request->pk=="service_provider_id")
        {
            $pk = $request->pk;
            $properties->$pk = $request->value;
            $properties->save();
            return 1;
        }

        // Clone property
        $input = $properties;
        if(!$request->value)
            $request->value = NULL;

        // $list_fields_percent = [
        //     'asset_manager',
        //     'ubername_des_objekts'
        // ];
        // foreach ($list_fields_percent as $field) {
        //     if( isset( $input[$field] ) )
        //         $input[$field] *= 100;
        // }
        // New value
        // New value
        $new_value = $request->value;

        $date_list_field = array('haftplicht_laufzeit_from','haftplicht_laufzeit_to','gebaude_laufzeit_to','gebaude_laufzeit_from');

        if($request->value && ($request->pk=="ubername_des_objekts2" || $request->pk=="ubername_des_objekts3" || $request->pk=="ubername_des_objekts" || $request->pk=="hv_kündbar_bis" || $request->pk=="hv_vertrag_abgeschlossen" || $request->pk=="hv_expire_at"))
        {
            $date = $request->value;
            if($date != date_format(  date_create(str_replace('.', '-', $date)) , 'd.m.Y'))
            {
                $response['msg'] = "Please enter date in (dd.mm.yyyy) format";
                return response()->json($response);
            }
            else{
                $new_value = $request->value = date_format(  date_create(str_replace('.', '-', $date)) , 'Y-m-d');
            }
        }
        else if(in_array($request->pk, $date_list_field))
        {
            $v = validatedate($request->value);
            if($v)
            {
                $new_value = $request->value = save_date_format($request->value);
            }
            else{
                $response['msg'] = "Please enter valid date in (dd.mm.yyyy) format";
                return response()->json($response);
            }
        }
        else if ($request->pk != "ort" && $request->pk != "swot_json") {
            $new_value = str_replace('.', '', $request->value);
            $new_value = str_replace(',', '.', $new_value);
        }
        if(!$new_value)
            $new_value = NULL;


        if ($request->pk == "hausmaxx") {

            $url = route('properties.show', ['property' => $prop_id]);

            $property = DB::table('properties')->select('properties.name_of_property', 'properties.id', 'properties.main_property_id')
                            ->where('Ist', '!=', 0)->where('soll', 0)
                            ->where('properties.main_property_id', '=', $prop_id)->first();

            if ($property) {
                $text = 'Hallo Thorsten, für das Objekt ' . $property->name_of_property . ' wurde die Hausverwaltung ' . $request->value . ' eingetragen: ' . $url;


                $email = "t.raudies@fcr-immobilien.de";
                // $email = "c.wiedemann@fcr-immobilien.de";
                // $email = "yiicakephp@gmail.com";

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::raw($text, function ($message) use($email) {
                        $message->to($email)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->subject('Hausverwaltung wurde eingetragen');
                    });
                }
            }
        }


        $reload = true;
        if (in_array($request->pk, array('gewerbe', 'gewerbe1', 'gewerbe2', 'buropraxen', 'buropraxen1', 'buropraxen2', 'wohnungen', 'wohnungen1', 'wohnungen2', 'lager', 'lager1', 'lager2', 'stellplatze', 'stellplatze1', 'stellplatze2', 'sonstiges', 'sonstiges1', 'sonstiges2','hv_kündbar_bis','hv_vertrag_abgeschlossen'))) {
            $reload = false;

            if (!$new_value && $request->pk!="hv_kündbar_bis" &&  $request->pk!="hv_vertrag_abgeschlossen")
                $new_value = 0;
        }

        $n_array = array('Baden-Württemberg' => '5.0',
            'Bayern' => '3.5',
            'Berlin' => '6.0',
            'Brandenburg' => '6.5',
            'Bremen' => '5.0',
            'Hamburg' => '4.5',
            'Hessen' => '6.0',
            'Mecklenburg-Vorpommern' => '6.0',
            'Niedersachsen' => '5.0',
            'Nordrhein-Westfalen' => '6.5',
            'Rheinland-Pfalz' => '5.0',
            'Saarland' => '6.5',
            'Sachsen' => '3.5',
            'Sachsen-Anhalt' => '5.0',
            'Schleswig-Holstein' => '6.5',
            'Thüringen' => '6.5');

        if ($request->pk == 'niedersachsen' && isset($n_array[$new_value])) {
            $input['real_estate_taxes'] = $n_array[$new_value];
            Properties::where('main_property_id', $prop_id)->update(['real_estate_taxes' => $n_array[$new_value] / 100]);
        }








        $input[$request->pk] = $new_value;

        if (PropertiesService::update($input, $properties)) {
            $response = [
                'success' => true,
                'msg' => '',
                'value' => $new_value,
                'reload' => $reload,
                'selecting_tenancy_schedule' => isset($_REQUEST['selecting_tenancy_schedule']) ? $_REQUEST['selecting_tenancy_schedule'] : ''
            ];
        }

        return response()->json($response);
    }

    public function update_versicherung(Request $request)
    {
        if (!isset($request->versicherungen_insurance_id))
        {
            if ($request->versicherungen_type==1)
            {
                Properties::find($request->property_id)->update([
                    'axa' => $request->versicherunge_name,
                    'gebaude_betrag' => $request->versicherunge_betrag ,
                    'gebaude_laufzeit_from' => $request->versicherunge_laufzeit,
                    'gebaude_laufzeit_to' => $request->versicherunge_laufzeit_end,
                    'gebaude_kundigungsfrist' => $request->versicherunge_kündigungsfrist,
                    'gebaude_comment' => $request->versicherunge_kommentar
                ]);
            } else {
                Properties::find($request->property_id)->update([
                    'allianz' => $request->versicherunge_name,
                    'haftplicht_betrag' => $request->versicherunge_betrag ,
                    'haftplicht_laufzeit_from' => $request->versicherunge_laufzeit,
                    'haftplicht_laufzeit_to' => $request->versicherunge_laufzeit_end,
                    'haftplicht_kundigungsfrist' => $request->versicherunge_kündigungsfrist,
                    'haftplicht_comment' => $request->versicherunge_kommentar
                ]);
            }
        } else {
            PropertyInsurance::find($request->versicherungen_insurance_id)->update([
                'name' => $request->versicherunge_name ,
                'type' => $request->versicherungen_type ,
                'amount' => $request->versicherunge_betrag ,
                'date_from' => $request->versicherunge_laufzeit,
                'date_to' => $request->versicherunge_laufzeit_end,
                'kundigungsfrist' => $request->versicherunge_kündigungsfrist,
                'note' => $request->versicherunge_kommentar
            ]);
        }

        return back();
    }

    public function update_versicherung_approved_status(Request $request)
    {
        if ($request->pk=='gebaude_falk_approved')
        {
            Properties::find($request->id)->update([
              'gebaude_falk_approved' => $request->value
            ]);
        }
        if ($request->pk=='haftplicht_falk_approved')
        {
            Properties::find($request->id)->update([
              'haftplicht_falk_approved' => $request->value
            ]);
        }
        if ($request->pk=='gebaude')
        {
            Properties::find($request->id)->update([
              'gebaude_is_approved' => $request->value
            ]);
        }

        if ($request->pk=='haftplicht')
        {
            Properties::find($request->id)->update([
                'haftplicht_is_approved' => $request->value
            ]);
        }

        if ($request->pk=='insurance')
        {
            PropertyInsurance::find($request->id)->update([
                'is_approved' => $request->value
            ]);
        }
    }

    public function updateFalkStatus(Request $request)
    {
        PropertyInsurance::find($request->id)->update([
                'is_checked' => $request->value
            ]);
    }

    public function deletePropertyInsurance(Request $request)
    {
        PropertyInsurance::find($request->id)->delete();
    }
    public function add_versicherung(Request $request)
    {
        PropertyInsurance::create([
            'property_id' => $request->property_id,
            'name' => $request->versicherunge_name ,
            'type' => $request->versicherungen_type ,
            'amount' => $request->versicherunge_betrag ,
            'date_from' => $request->versicherunge_laufzeit,
            'date_to' => $request->versicherunge_laufzeit_end,
            'kundigungsfrist' => $request->versicherunge_kündigungsfrist,
            'note' => $request->versicherunge_kommentar
        ]);

        return back();
    }

    public function create_property_insurance(Request $request) {

        $input['property_id'] = $request->property_id;
        $property = PropertyInsurance::create($input);
        return redirect('properties/' . $request->property_id . '?tab=schl-template');
    }

    public function create_maintenance(Request $request) {

        $input['property_id'] = $request->property_id;
        $property = PropertyMaintenance::create($input);
        return redirect('properties/' . $request->property_id . '?tab=schl-template');
    }

    public function create_investation(Request $request) {

        $input['property_id'] = $request->property_id;
        $property = PropertyInvestation::create($input);
        return redirect('properties/' . $request->property_id . '?tab=schl-template');
    }

    public function delete_property_insurance(Request $request) {
        $property = PropertyInsurance::where('id', '=', $request->id)->first();
        if ($property)
            $property->delete();
        echo "1";
    }

    public function delete_maintenance(Request $request) {
        $property = PropertyMaintenance::where('id', '=', $request->id)->first();
        if ($property)
            $property->delete();
        echo "1";
    }

    public function delete_investation(Request $request) {
        $property = PropertyInvestation::where('id', '=', $request->id)->first();
        if ($property)
            $property->delete();
        echo "1";
    }

    public function update_maintenance_by_field(Request $request, $prop_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $property = PropertyMaintenance::where('id', '=', $prop_id)->first();

        if (empty($property)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        // Clone property
        $input = $property;
        $new_value = str_replace('.', '', $request->value);
        $new_value = str_replace(',', '.', $new_value);
        $input[$request->pk] = $new_value;

        $propertyData = [
            'name' => $input['name'],
            'amount' => $input['amount'],
        ];

        ;

        if ($property->update($propertyData)) {
            $response = [
                'success' => true,
                'msg' => '',
                'reload' => false,
                'selecting_tenancy_schedule' => isset($_REQUEST['selecting_tenancy_schedule']) ? $_REQUEST['selecting_tenancy_schedule'] : ''
            ];
        }

        return response()->json($response);
    }

    public function update_investation_by_field(Request $request, $prop_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $property = PropertyInvestation::where('id', '=', $prop_id)->first();

        if (empty($property)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        // Clone property
        $input = $property;
        $new_value = str_replace('.', '', $request->value);
        $new_value = str_replace(',', '.', $new_value);
        $input[$request->pk] = $new_value;

        $propertyData = [
            'name' => $input['name'],
            'amount' => $input['amount'],
        ];

        ;

        if ($property->update($propertyData)) {
            $response = [
                'success' => true,
                'msg' => '',
                'reload' => false,
                'selecting_tenancy_schedule' => isset($_REQUEST['selecting_tenancy_schedule']) ? $_REQUEST['selecting_tenancy_schedule'] : ''
            ];
        }

        return response()->json($response);
    }

    public function update_property_insurance_by_field(Request $request, $prop_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $property = PropertyInsurance::where('id', '=', $prop_id)->first();

        if (empty($property)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        // Clone property
        $input = $property;
        $new_value = str_replace('.', '', $request->value);
        $new_value = str_replace(',', '.', $new_value);
        $input[$request->pk] = $new_value;

        $propertyData = [
            'name' => $input['name'],
            'amount' => $input['amount'],
            'date_from' => $input['date_from'],
            'date_to' => $input['date_to'],
            'date_to' => $input['date_to'],
            'note' => $input['note'],
            'type' => $input['type']
        ];

        ;

        if ($property->update($propertyData)) {
            $response = [
                'success' => true,
                'msg' => '',
                'reload' => false,
                'selecting_tenancy_schedule' => isset($_REQUEST['selecting_tenancy_schedule']) ? $_REQUEST['selecting_tenancy_schedule'] : ''
            ];
        }

        return response()->json($response);
    }

    public function create_service_provider(Request $request) {

        $input['property_id'] = $request->property_id;
        $property = PropertyServiceProvider::create($input);
        return redirect('properties/' . $request->property_id . '?tab=schl-template');
    }

    public function delete_service_provider(Request $request) {
        $property = PropertyServiceProvider::where('id', '=', $request->id)->first();
        if ($property)
            $property->delete();
        echo "1";
    }

    public function update_property_service_provider_by_field(Request $request, $prop_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $property = PropertyServiceProvider::where('id', '=', $prop_id)->first();

        if (empty($property)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        // Clone property
        $input = $property;
        $new_value = str_replace('.', '', $request->value);
        $new_value = str_replace(',', '.', $new_value);
        $input[$request->pk] = $new_value;

        $propertyData = [
            'company' => $input['company'],
            'amount' => $input['amount'],
            'note' => $input['note'],
            'type' => $input['type']
        ];

        ;

        if ($property->update($propertyData)) {
            $response = [
                'success' => true,
                'reload' => false,
                'msg' => '',
                'selecting_tenancy_schedule' => isset($_REQUEST['selecting_tenancy_schedule']) ? $_REQUEST['selecting_tenancy_schedule'] : ''
            ];
        }

        return response()->json($response);
    }

    public function saveSchlBanks(Request $request, $prop_id) {

        $properties = Properties::where('id', '=', $prop_id)->where('Ist', 0)->where('soll', 0)->first();

        if (empty($properties)) {
            return response()->json(0);
        }

        // Clone property
        $input = $properties;

        $input['schl_banks'] = json_encode($request->schl_banks);
        if (PropertiesService::update($input, $properties)) {
            return response()->json(1);
        }
        return response()->json(0);
    }

    public function update_property_planung_by_field(Request $request, $prop_id) {
//        $response = [
//            'success' => false,
//            'msg' => 'Failed to updated property'
//        ];
//
//        $properties = Properties::where('id', '=', $prop_id)->first();
//
//        if( empty( $properties ) ) {
//            $response['msg'] = 'Not found property';
//            return response()->json($response);
//        }
//
//        // Clone property
//        $input = $properties;
////
//        $input[$request->pk] = $request->value;
//
//        if (PropertiesService::update( $input, $properties )) {
//            $response = [
//                'success' => true,
//                'msg' => ''
//            ];
//        }
//
//        return response()->json($response);

        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $properties = Properties::where('id', '=', $prop_id)->where('Ist', 0)->where('soll', 0)->first();

        if (empty($properties)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }
        $planung_fields = $properties->planung_fields;
        $planung_fields = json_decode($properties->planung_fields, true);
        if (!is_array($planung_fields)) {
            $planung_fields = array();
        }

        // Clone property
        $input = $properties;

        $new_value = str_replace('.', '', $request->value);
        $new_value = str_replace(',', '.', $new_value);
        $planung_fields[$request->pk] = $new_value;


        $input['planung_fields'] = json_encode($planung_fields);
        //        var_dump($clever_fit_fields);die;
        if (PropertiesService::update($input, $properties)) {
            $response = [
                'success' => true,
                'msg' => ''
            ];
        }

        return response()->json($response);
    }

    public function update_property_clever_fit_by_field(Request $request, $prop_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $properties = Properties::where('id', '=', $prop_id)->where('Ist', 0)->where('soll', 0)->first();

        if (empty($properties)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }
        $clever_fit_fields = $properties->clever_fit_fields;
        $clever_fit_fields = json_decode($properties->clever_fit_fields, true);
        if (!is_array($clever_fit_fields)) {
            $clever_fit_fields = array();
        }

        // Clone property
        $input = $properties;

        $new_value = str_replace('.', '', $request->value);
        $new_value = str_replace(',', '.', $new_value);
        $clever_fit_fields[$request->pk] = $new_value;


        $input['clever_fit_fields'] = json_encode($clever_fit_fields);
        //        var_dump($clever_fit_fields);die;
        if (PropertiesService::update($input, $properties)) {
            $response = [
                'success' => true,
                'msg' => ''
            ];
        }

        return response()->json($response);
    }
    public function guestcreateproperties()
    {
        $tr_users = User::whereRaw('role=2 or second_role=2 or id=1 or id=10')->get();
        return view('dashboard.home',compact('tr_users'));
    }
    public function guestcreatepropertiespost(Request $request) {

        $email = "makler@fcr-immobilien.de";

        $attributeNames = array(
            'upload_pdf' => 'PDF',
        );

        $validator = Validator::make($request->all(), [
            'upload_pdf.*' => 'mimes:pdf|max:200096',
        ]);

        // echo $request->file('upload_pdf')->getClientOriginalName(); die;

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->errors()->first())->withInput();
        }

        $user = User::where('email',$email)->first();
        if($user)
        {
            $user_id = $user->id;
        }
        else{
            $user =  new User;
            $user->email = $email;
            $user->name = $email;
            $user->password = $email.''.rand(0,50);
            $user->role = 3;
            $user->save();
            $user_id = $user->id;
        }

        $input = [];
        // Create default value of fields
        $input['user_id'] = $user_id;
        $input['firma'] = $request->firma;
        $input['phone'] = $request->phone;
        $input['email'] = $request->email;
        $input['contact'] = $request->contact;



        $input['name_of_creator'] = "guest";
        $input['net_rent'] = 1;
        $input['net_rent_empty'] = 0;
        $input['maintenance'] = 1;
        $input['operating_costs'] = 1;
        $input['object_management'] = 1;
        $input['tax'] = 17;
        $input['plot_of_land_m2'] = 0;
        $input['construction_year'] = 0;
        $input['building'] = 80;
        $input['plot_of_land'] = 20;
        $input['real_estate_taxes'] = 6.5;
        $input['estate_agents'] = 3;
        $input['Grundbuch'] = 2;
        $input['notary_land_register'] = 0;
        $input['evaluation'] = 0.1;
        $input['others'] = 0;
        $input['buffer'] = 0.1;
        $input['rent'] = 0;
        $input['rent_retail'] = 0;
        $input['rent_office'] = 0;
        $input['rent_industry'] = 0;
        $input['rent_whg'] = 0;
        $input['vacancy'] = 0;
        $input['vacancy_retail'] = 0;
        $input['vacancy_office'] = 0;
        $input['vacancy_industry'] = 0;
        $input['vacancy_whg'] = 0;
        $input['total_commercial_sqm'] = 0;
        $input['wault'] = 0;
        $input['anchor_tenant'] = 0;
        $input['plot'] = 0;
        $input['with_real_ek'] = 0;
        $input['from_bond'] = 20;
        $input['bank_loan'] = 80;
        $input['interest_bank_loan'] = 2;
        $input['eradication_bank'] = 4;
        $input['interest_bond'] = 4.25;
        $input['maintenance_nk'] = 0.25;
        $input['operating_costs_nk'] = 4.5;
        $input['object_management_nk'] = 3;
        $input['depreciation_nk'] = 3;
        $input['property_value'] = 0.7;
        $input['nk_anchor_tenants'] = 0;
        $input['nk_tenant1'] = 0;
        $input['nk_tenant2'] = 0;
        $input['nk_tenant3'] = 0;
        $input['nk_tenant4'] = 0;
        $input['nk_tenant5'] = 0;
        $input['mv_anchor_tenants'] = '';
        $input['mv_tenant1'] = '';
        $input['mv_tenant2'] = '';
        $input['mv_tenant3'] = '';
        $input['mv_tenant4'] = '';
        $input['mv_tenant5'] = '';
        $input['total_purchase_price'] = 1;
        $input['duration_from'] = '';
        $input['land_value'] = 0;
        $input['net_rent_pa'] = 1;
        $input['city_place'] = '';
        $input['WHG_qm_of_WAULT'] = 0;
        $input['WHG_qm_of_anchor_tenant'] = 0;
        $input['WHG_qm_of_plot'] = 0;
        $input['Ref_CF_Salzgitter'] = 253165;
        $input['Ref_GuV_Salzgitter'] = 369451;
        $input['AHK_Salzgitter'] = 6456000;
        $input['name_of_property'] = '';
        $input['status'] = 17;
        $input['bank_ids'] = ($request->select_bank) ? $request->select_bank : '';
        $input['masterliste_id'] = ($request->masterliste_id) ? $request->masterliste_id : null;
        $input['ground_reference_value_in_euro_m2'] = 0;
        $input['offer_from'] = 'einkauf.fcr';
        $input['duration_from_O38'] = date("Y-m-d H:i:s");

        $property_id = PropertiesService::create($input)->id;
        if ($property_id) {
            $data = [
                'property_id'   =>  $property_id,
                'is_new_directory_strucure' => 1
            ];
            PropertySubfields::create($data);
            $this->bake_create_first_entry($property_id);
            // return redirect('properties/' . $property_id);
        }


        // print_r($_POST); die;
        $value1 = $value = $value2 = $value3 = 0;
         $Rental_area_in_m2= 0;
        if ($request->gesamt_in_eur) {
            $value = str_replace('.', '', $request->gesamt_in_eur);
            $value = str_replace(',', '.', $value);
            $value = str_replace('%', '', $value);
            $value = str_replace('€', '', $value);
        }
        if ($request->Rental_area_in_m2) {
            $Rental_area_in_m2 = str_replace('.', '', $request->Rental_area_in_m2);
            $Rental_area_in_m2 = str_replace(',', '.', $Rental_area_in_m2);
        }

        if ($request->land_area) {
            $value4 = str_replace('.', '', $request->land_area);
            $value4 = str_replace(',', '.', $value4);
            $value4 = str_replace('%', '', $value4);
            $value4 = str_replace('€', '', $value4);
        }

        if ($request->maklerpreis) {
            $value3 = str_replace('.', '', $request->maklerpreis);
            $value3 = str_replace(',', '.', $value3);
            $value3 = str_replace('%', '', $value3);
            $value3 = str_replace('€', '', $value3);
        }
        if ($request->net_rent_pa) {
            $value2 = str_replace('.', '', $request->net_rent_pa);
            $value2 = str_replace(',', '.', $value2);
            $value2 = str_replace('%', '', $value2);
            $value2 = str_replace('€', '', $value2);
        }
        if ($request->real_estate_taxes) {
            $value1 = str_replace('.', '', $request->real_estate_taxes);
            $value1 = str_replace(',', '.', $value1);
        }

        $update_data1 = DB::table('properties')->whereRaw('(main_property_id=' . $property_id . ' OR id=' . $property_id.') and lock_status=0')
            ->update([
                "property_status"=>0,
                "name_of_property" => $request->name_of_property,
                "plz_ort" => $request->plz_ort,
                "ort" => $request->ort,
                "strasse" => $request->strasse,
                "hausnummer" => $request->hausnummer,
                "niedersachsen" => $request->niedersachsen,
                "first_name" => $request->first_name,
                "anbieterkontakt" => $request->anbieterkontakt,
                "net_rent_pa" => $value2,
                "Rental_area_in_m2" => $Rental_area_in_m2,
                "maklerpreis" => $value3,
                "plot_of_land_m2" => $value4,
                "property_type" => $request->property_type,
                "notizen" => $request->notizen,
                "real_estate_taxes" => $value1 / 100,
            ]);

        $company = Company::create([
            'name'=> $input['firma'],
            ]);
        $emp = CompanyEmployee::create([
            'company_id' => $company->id,
            'vorname' => $request->anbieterkontakt,
            'phone' => $request->phone,
            'email' => $request->email,
            'category' => $request->contact,
        ]);



            $attributeNames = array(
                'upload_pdf' => 'PDF',
            );

            $validator = Validator::make($request->all(), [
                'upload_pdf.*' => 'mimes:pdf|max:200096',
            ]);

            // echo $request->file('upload_pdf')->getClientOriginalName(); die;

            $validator->setAttributeNames($attributeNames);

            if ($validator->fails()) {
                return redirect()->back()->with('error', $validator->errors()->first())->withInput();
            }
            else{
                $propertyMainDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', 0)->first();
                if(!$propertyMainDir){
                    // return redirect()->back()->with('error', 'Property directory not created in google Drive.');
                    $property = Properties::find($property_id);
                    if(!$property){
                        die('Property does not exist.');
                    }
                    $propertyStatusArr = config('properties.status_with_drive');
                    // if($property->property_status == 1){
                    //     $property->status = 18;
                    // }
                    if(!$property->status){
                        $propertyStatusArr = $propertyStatusArr[15];
                    }else{
                        $propertyStatusArr = $propertyStatusArr[$property->status];
                    }
                    if($propertyStatusArr[1] == "")
                    {
                        die('Property status directory not exist.'. $propertyStatusArr[0] );
                    }
                    $root_dir = $propertyStatusArr[1];
                    $new_folder_name = PropertiesHelperClass::getPropertyDirectoryName($property);

                    if ( ! $newDir = Storage::cloud()->makeDirectory($root_dir.'/'.$new_folder_name) ) {
                        Log::info('Properties Controller: create property directory named('.$new_folder_name.') /n');
                        die('directory create still remain');
                    }
                    $contents = collect(Storage::cloud()->listContents($root_dir, false));
                    $new_property_dir = $contents->where('type', 'dir')->where('name', $new_folder_name)->sortByDesc('timestamp')->values()->first();
                    // sortByDesc because there can multiple folder with same name so we get latest.

                    PropertiesDirectory::create([
                        'property_id'   => $property_id,
                        'parent_id'   => 0,
                        'dir_name'      => $new_folder_name,
                        'dir_basename'  => $new_property_dir['basename'],
                        'dir_path'      => $new_property_dir['path']
                    ]);
                    $propertyMainDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', 0)->first();
                }
                $propertyAnhangeDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Anhange')->first();
                if(!$propertyAnhangeDir){
                    // return redirect()->back()->with('error', 'Property Anhänge directory not created in google Drive.');
                    Storage::cloud()->makeDirectory($propertyMainDir->dir_path.'/Anhange');
                    $contents = collect(Storage::cloud()->listContents($propertyMainDir->dir_path, false));
                    $driveSubDir = $contents->where('type', 'dir')->where('name', 'Anhange')->sortByDesc('timestamp')->values()->first();
                    PropertiesDirectory::create([
                        'property_id'   => $property_id,
                        'parent_id'   => $propertyMainDir->id,
                        'dir_name'      => 'Anhange',
                        'dir_basename'  => $driveSubDir['basename'],
                        'dir_path'      => $driveSubDir['path']
                    ]);
                    $propertyAnhangeDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Anhange')->first();
                }

                if ($request->hasFile('upload_pdf')) {

                    $fileData = [];

                    foreach($request->file('upload_pdf') as $file){

                        $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        $originalExt = $file->getClientOriginalExtension();
                        $folder = $propertyAnhangeDir->dir_path;
                        $fileData = File::get($file);

                        //upload file
                        $file2 =  Storage::cloud()->put($folder.'/'.$originalName.'.'.$originalExt, $fileData);

                        //get google drive id
                        $contents = collect(Storage::cloud()->listContents($folder, false));

                        $file = $contents->where('type','file')->where('filename', $originalName)->where('extension', $originalExt) ->first();

                        $fileData[] = [
                            'user_id' => $user_id,
                            'property_id' => $property_id,
                            'upload_file_name' => $request->file('upload_pdf')->getClientOriginalName(),
                            'file_name' => $file['name'],
                            'file_basename' => $file['basename'],
                            'file_path' => $file['path'],
                            'file_href' => 'https://drive.google.com/file/d/'.$file['basename'],
                            'extension' => $file['extension']
                        ];

                    }

                    $insert_data = DB::table('pdf')->insert($fileData);
                }
            }


            return back()->with('success', " Vielen Dank! Die Daten zu Ihrem Objekt wurden verschickt. Wir werden Ihr Objekt überprüfen und uns bei Ihnen melden.");

    }

    public function create_property_direct(Request $request) {
        $input = [];
        $user_id = Auth::id();

        // Create default value of fields
        $input['name_of_creator'] = User::where('id', '=', $user_id)->first()->name;
        $input['net_rent'] = 1;
        $input['net_rent_empty'] = 0;
        $input['maintenance'] = 1;
        $input['operating_costs'] = 1;
        $input['object_management'] = 1;
        $input['tax'] = 17;
        $input['plot_of_land_m2'] = 0;
        $input['construction_year'] = 0;
        $input['building'] = 80;
        $input['plot_of_land'] = 20;
        $input['real_estate_taxes'] = 6.5;
        $input['estate_agents'] = 3;
        $input['Grundbuch'] = 2;
        $input['notary_land_register'] = 0;
        $input['evaluation'] = 0.1;
        $input['others'] = 0;
        $input['buffer'] = 0.1;
        $input['rent'] = 0;
        $input['rent_retail'] = 0;
        $input['rent_office'] = 0;
        $input['rent_industry'] = 0;
        $input['rent_whg'] = 0;
        $input['vacancy'] = 0;
        $input['vacancy_retail'] = 0;
        $input['vacancy_office'] = 0;
        $input['vacancy_industry'] = 0;
        $input['vacancy_whg'] = 0;
        $input['total_commercial_sqm'] = 0;
        $input['wault'] = 0;
        $input['anchor_tenant'] = 0;
        $input['plot'] = 0;
        $input['with_real_ek'] = 0;
        $input['from_bond'] = 20;
        $input['bank_loan'] = 80;
        $input['interest_bank_loan'] = 2;
        $input['eradication_bank'] = 4;
        $input['interest_bond'] = 4.25;
        $input['maintenance_nk'] = 0.25;
        $input['operating_costs_nk'] = 4.5;
        $input['object_management_nk'] = 3;
        $input['depreciation_nk'] = 3;
        $input['property_value'] = 0.7;
        $input['nk_anchor_tenants'] = 0;
        $input['nk_tenant1'] = 0;
        $input['nk_tenant2'] = 0;
        $input['nk_tenant3'] = 0;
        $input['nk_tenant4'] = 0;
        $input['nk_tenant5'] = 0;
        $input['mv_anchor_tenants'] = '';
        $input['mv_tenant1'] = '';
        $input['mv_tenant2'] = '';
        $input['mv_tenant3'] = '';
        $input['mv_tenant4'] = '';
        $input['mv_tenant5'] = '';
        $input['total_purchase_price'] = 1;
        $input['duration_from'] = '';
        $input['land_value'] = 0;
        $input['net_rent_pa'] = 1;
        $input['city_place'] = '';
        $input['WHG_qm_of_WAULT'] = 0;
        $input['WHG_qm_of_anchor_tenant'] = 0;
        $input['WHG_qm_of_plot'] = 0;
        $input['Ref_CF_Salzgitter'] = 253165;
        $input['Ref_GuV_Salzgitter'] = 369451;
        $input['AHK_Salzgitter'] = 6456000;
        $input['name_of_property'] = '';
        $input['status'] = 15;
        $input['bank_ids'] = ($request->select_bank) ? $request->select_bank : '';
        $input['masterliste_id'] = ($request->masterliste_id) ? $request->masterliste_id : null;
        $input['ground_reference_value_in_euro_m2'] = 0;
        $input['duration_from_O38'] = date("Y-m-d H:i:s");

        // $list_fields_percent = [
        //     'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents',  'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
        //   ];
        //   foreach ($list_fields_percent as $field) {
        //     if( isset( $input[$field] ) )
        //       $input[$field] = $input[$field]/100;
        //   }

        $property_id = PropertiesService::create($input)->id;
        if ($property_id) {
            $data = [
                'property_id'	=>	$property_id,
                'is_new_directory_strucure' => 1
            ];
            PropertySubfields::create($data);
            $this->bake_create_first_entry($property_id);
            return redirect('properties/' . $property_id);
        }

        return redirect()->back();
    }

    // Create bank for first create property
    public function bake_create_first_entry($property_id) {
        $input = $property_id;

        $property_data = Properties::where('id', '=', $property_id)->first();
        $bank_ids = json_decode($property_data->bank_ids) ? json_decode($property_data->bank_ids) : [];

        // if(isset($bank_ids[0])){
        //   $bank_init = Banks::where('id', '=',$bank_ids[0])->first();
        //   $bank_init['with_real_ek']*=100;
        //   $bank_init['from_bond']*=100;
        //   $bank_init['bank_loan']*=100;
        //   $bank_init['interest_bank_loan']*=100;
        //   $bank_init['eradication_bank']*=100;
        //   $bank_init['interest_bond']*=100;
        // }else{
        //   $bank_init =[
        //     'name'=> 'Bank name',
        //     'picture'=>'',
        //     'with_real_ek' => '0',
        //     'from_bond' => 20,
        //     'bank_loan' => 80,
        //     'interest_bank_loan' => 2,
        //     'eradication_bank' => 4,
        //     'interest_bond' => 6
        //   ];
        // }
        // Upload picture
        //if ($bank = BanksService::create($bank_init)) {

        if (!is_null($property_data)) {

            $property = $property_data;
            $list_fields_percent = [
                'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
            ];
            foreach ($list_fields_percent as $field) {
                if (isset($property_data[$field]))
                    $property_data[$field] *= 100;
            }


            //array_push($bank_ids, $bank->id);

            $default_bank = 34;
            array_push($bank_ids, $default_bank);


            $property_data->bank_ids = json_encode($bank_ids);

            //dd($property_data->bank_ids);
            PropertiesService::update($property_data, $property);

            /*
              $Properties_clone = Properties::find($input['property_id']);
              $Properties_New = $Properties_clone->replicate();
              $Properties_New->Ist = $bank->id;
              $Properties_New->for_bank_tab = 'yes';
              $Properties_New->save();

              $Properties_clone = Properties::find($input['property_id']);
              $Properties_New = $Properties_clone->replicate();
              $Properties_New->soll = $bank->id;
              $Properties_New->for_bank_tab = 'yes';
              $Properties_New->save();
             */
            DB::beginTransaction();
            try {
                $original_properties = Properties::find($property_id);

                // $Properties_clone = Properties::find(596);
                $Properties_clone = Properties::find($property_id);
                $Properties_New = $Properties_clone->replicate();
                //$Properties_New->Ist = $bank->id;
                $Properties_New->Ist = $default_bank;
                $Properties_New->main_property_id = $property_id;
                $Properties_New->properties_bank_id = $default_bank;
                $Properties_New->bank_ids = $original_properties->bank_ids;
                if(Auth::user())
                $Properties_New->user_id = Auth::user()->id;
                $Properties_New->for_bank_tab = 'yes';
                $Properties_New->standard_property_status = 1;
                // $Properties_New->status = 15;
                // $Properties_New->quick_status = 1;
                $Properties_New->save();

                // $Properties_clone = Properties::find(596);
                // $Properties_clone = Properties::find($property_id);
                // $Properties_New = $Properties_clone->replicate();
                // //$Properties_New->soll = $bank->id;
                // $Properties_New->soll = $default_bank;
                // $Properties_New->main_property_id = $property_id;
                // $Properties_New->properties_bank_id = $default_bank;
                // $Properties_New->bank_ids = $original_properties->bank_ids;
                // $Properties_New->user_id =Auth::user()->id;
                // $Properties_New->for_bank_tab = 'yes';
                // $Properties_New->save();

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                die($e->getMessage());
            }
        }
        // return  back();
    }

    public function showCharts() {
        $properties = Properties::where('Ist', 0)->where('soll', 0)->get();
        return view('properties.charts', compact('properties'));
    }

    public function changeStatusInHold(Request $request) {
        $data = $request->all();
        $property = Properties::where('id', $data['propertyId'])->where('Ist', 0)->where('soll', 0)
                ->update(['is_in_negotiation' => $data['status']]);
        echo json_encode($property);
        die;
    }

    public function update_property_direct(Request $request) {

        Properties::where('id', $request->property_id)->where('Ist', 0)->where('soll', 0)
                ->update(['bank_ids' => json_encode($request->select_bank_s)]);

        return redirect()->back();
    }

    public function addPropertiesExtra1Row(Request $request) {
        $data = $request->all();

        // echo "here"; die;

        $PropertiesExtra1 = new PropertiesTenants;
        $PropertiesExtra1->propertyId = $data['property_id'];
        $PropertiesExtra1->tenant = '';
        $PropertiesExtra1->net_rent_p_a = 0;
        $PropertiesExtra1->mv_end = '';
        $PropertiesExtra1->save();

        // $PropertiesExtra2 = new PropertiesExtra2;
        // $PropertiesExtra2->propertyId = $data['property_id'];
        // $PropertiesExtra2->tenant = '';
        // $PropertiesExtra2->net_rent_p_a = 0;
        // $PropertiesExtra2->mv_end = '';
        // $PropertiesExtra2 -> save();
        return redirect()->back();
    }

    public function updatePropertiesExtra1(Request $request, $tab, $prop_id) {
        $data = $request->all();
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];
        $new_value = str_replace('.', '', $data['value']);
        $new_value = str_replace(',', '.', $new_value);

        $PropertiesExtra1 = PropertiesTenants::where('id', $prop_id)->update([$data['pk'] => $new_value]);

        if ($PropertiesExtra1) {
            $response = [
                'success' => true,
                'msg' => ''
            ];
        }

        return response()->json($response);
    }

    public function export_to_excel(Request $request) {
        $input = $request->all();


        $input['property_data'] = trim($input['property_data']);
        $input['property_data'] = str_replace(__('property.export'), '', $input['property_data']);
        $input['property_data'] = str_replace('Add Ort', '', $input['property_data']);
        $input['property_data'] = str_replace('Empty', '', $input['property_data']);
        $input['property_data'] = str_replace('X', '', $input['property_data']);
        $input['property_data'] = str_replace('Gesamtexport', '', $input['property_data']);
        $input['property_data'] = str_replace('Bankexport', '', $input['property_data']);
        $input['property_data'] = str_replace('PDF', '', $input['property_data']);
        $input['property_data'] = str_replace('(nicht ändern!)', '', $input['property_data']);

        $input['property_data'] = str_replace('5.) Risikoberechnung', '4.) Risikoberechnung', $input['property_data']);
        $input['property_data'] = str_replace('6.) Lage / Notizen', '5.) Lage / Notizen', $input['property_data']);

        $skipRow = array(64, 65, 66, 67, 68, 69, 70);
        $skipCol = array(7, 8, 9);
        $property_data = json_decode($input['property_data']);
        foreach ($property_data as $row => $row_data) {
            foreach ($row_data as $col => $value) {
                if (in_array($row, array(58, 59, 60, 61, 62)) && in_array($col, $skipCol))
                    $property_data[$row][$col] = '';
                else
                $property_data[$row][$col] = trim($value);
            }
        }

        $input['tenant_data'] = str_replace(__('property.add_row'), '', $input['tenant_data']);
        $input['tenant_data'] = str_replace('X', '', $input['tenant_data']);
        $input['tenant_data'] = str_replace('Empty', '', $input['tenant_data']);




        $tenant_data = json_decode($input['tenant_data']);
        $input['tenant_data'] = str_replace(__('property.add_row'), '', $input['tenant_data']);
        foreach ($tenant_data as $row_data) {
            $row += 1;
            foreach ($row_data as $col => $value) {
                $property_data[$row][$col] = trim($value);
            }
        }

        $input['note_data'] = str_replace('Empty', '', $input['note_data']);
        $note_data = json_decode($input['note_data']);
        foreach ($note_data as $row_data) {
            $row += 1;
            foreach ($row_data as $col => $value) {
                $property_data[$row][$col] = trim($value);
            }
        }

        /*foreach ($property_data as $key1 => $value1) {
            foreach ($value1 as $key2 => $value2) {
                if( $value2 != '' && !strpos($value2, "%") && ( preg_match("[.|,|€]", $value2) === 1 || is_numeric($value2) ) ){

                    if(!preg_match("/\d{2}\.\d{2}\.\d{4}/", $value2)){

                        $value2 = str_replace('.', '', trim($value2));
                        $value2 = str_replace(',', '.', $value2);
                        $value2 = str_replace('€', '', $value2);
                        $property_data[$key1][$key2] = $value2;
                    }

                }
            }
        }*/


        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Property');


        //------------------------
        //Edit sheet "Property"
        //------------------------
        //Set default width of each column
        $sheet->getDefaultColumnDimension()->setWidth(20);

        //Set datatype to cells
        //change table style
        $style_array_th_grey = PropertiesService::get_style_array(['bold', 'border', 'bg-grey']);
        $style_array_th_black = PropertiesService::get_style_array(['bold', 'bg-black']);
        $style_array_bold = PropertiesService::get_style_array(['bold']);
        $style_array_border = PropertiesService::get_style_array(['border']);
        $style_array_yellow = PropertiesService::get_style_array(['bg-yellow']);
        $style_array_red = PropertiesService::get_style_array(['bg-red']);
        $style_array_green = PropertiesService::get_style_array(['bg-green']);
        $style_text_right = PropertiesService::get_style_array(['align-right']);
        $style_text_left = PropertiesService::get_style_array(['align-left']);
        $style_text_center = PropertiesService::get_style_array(['align-center']);
        $style_array_noborder = PropertiesService::get_style_array(['no-border']);

        $sheet->getStyle('L14')->applyFromArray($style_text_left);
        //$sheet->getStyle('C1:K1')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('D1:E1')->applyFromArray($style_array_yellow);
        $sheet->getStyle('C3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('E3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('G3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('I3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('J5')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B7:B8')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B10:B12')->applyFromArray($style_array_yellow);
        // $sheet->getStyle('K11:K12')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K18')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K21')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B25')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K29')->applyFromArray($style_array_yellow);




        // $sheet->getStyle('L29')->applyFromArray($style_array_yellow);
        // $sheet->getStyle('M29')->applyFromArray($style_array_yellow);
        // $sheet->getStyle('N29')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B36:B37')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B38')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('C38')->applyFromArray($style_array_yellow);
        $sheet->getStyle('F36:F42')->applyFromArray($style_array_yellow);
        $sheet->getStyle('J36:J37')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K36:K37')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B47:B49')->applyFromArray($style_array_yellow);
        $sheet->getStyle('F48:F49')->applyFromArray($style_array_yellow);
        $sheet->getStyle('E50:K50')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('F52')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B60:B63')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B67')->applyFromArray($style_array_yellow);
        // $sheet->getStyle('C74')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B7')->applyFromArray($style_array_border);
        $sheet->getStyle('A40:C40')->applyFromArray($style_array_border);
        $sheet->getStyle('A47:C47')->applyFromArray($style_array_border);
        $sheet->getStyle('E47:K47')->applyFromArray($style_array_border);
        $sheet->getStyle('E39:G40')->applyFromArray($style_array_border);
        $sheet->getStyle('I38:K38')->applyFromArray($style_array_border);
        $sheet->getStyle('I40:K40')->applyFromArray($style_array_border);
        $sheet->getStyle('E52:G52')->applyFromArray($style_array_border);
        $sheet->getStyle('E59:F63')->applyFromArray($style_array_border);
        $sheet->getStyle('C72')->applyFromArray($style_array_border);
        // $sheet->getStyle('A74')->applyFromArray($style_array_border);
        $sheet->getStyle('A1:K1')->applyFromArray($style_array_border);
        $sheet->getStyle('A3:K3')->applyFromArray($style_array_border);
        $sheet->getStyle('J5:K5')->applyFromArray($style_array_yellow);
        $sheet->getStyle('L6:M6')->applyFromArray($style_array_border);
        $sheet->getStyle('J7:M7')->applyFromArray($style_array_border);
        $sheet->getStyle('J8:M8')->applyFromArray($style_array_border);
        $sheet->getStyle('J10:K10')->applyFromArray($style_array_border);
        $sheet->getStyle('J11:K11')->applyFromArray($style_array_border);
        $sheet->getStyle('J12:K12')->applyFromArray($style_array_border);
        $sheet->getStyle('J14:K14')->applyFromArray($style_array_border);
        $sheet->getStyle('J16:K16')->applyFromArray($style_array_border);
        $sheet->getStyle('J18:K18')->applyFromArray($style_array_border);
        $sheet->getStyle('J20:K20')->applyFromArray($style_array_border);
        $sheet->getStyle('J21:K21')->applyFromArray($style_array_border);
        $sheet->getStyle('J23:K23')->applyFromArray($style_array_border);
        $sheet->getStyle('J25:K25')->applyFromArray($style_array_border);
        $sheet->getStyle('J27:K27')->applyFromArray($style_array_border);
        $sheet->getStyle('J29:K29')->applyFromArray($style_array_border);
        $sheet->getStyle('J31:K31')->applyFromArray($style_array_border);
        // $sheet->getStyle('L31:L32')->applyFromArray($style_array_border);
        // $sheet->getStyle('J59:J61')->applyFromArray($style_array_border);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);


        $sheet->getStyle('A1:B1')->applyFromArray($style_array_th_black);
        $sheet->getStyle('D5:H5')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A7')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('C7:H7')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A8:H8')->applyFromArray($style_array_border);
        $sheet->getStyle('A10:H12')->applyFromArray($style_array_border);
        $sheet->getStyle('A10:H12')->applyFromArray($style_array_border);
        $sheet->getStyle('A14:H14')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A16:H16')->applyFromArray($style_array_border);
        $sheet->getStyle('A18:H18')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A20:H21')->applyFromArray($style_array_border);
        $sheet->getStyle('A23:H23')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A25:H25')->applyFromArray($style_array_border);
        $sheet->getStyle('A27:H27')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A29:H29')->applyFromArray($style_array_border);
        $sheet->getStyle('A31:H31')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A33:K33')->applyFromArray($style_array_th_black);
        $sheet->getStyle('A35:C35')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A36:C38')->applyFromArray($style_array_border);
        $sheet->getStyle('E35:G35')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('E36:G38')->applyFromArray($style_array_border);
        $sheet->getStyle('I35:K35')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('I36:K37')->applyFromArray($style_array_border);
        $sheet->getStyle('A41:C42')->applyFromArray($style_array_border);
        $sheet->getStyle('A42:C42')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('E41:G42')->applyFromArray($style_array_border);
        $sheet->getStyle('E42:G42')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('I41:K42')->applyFromArray($style_array_border);

        $sheet->getStyle('A44:K44')->applyFromArray($style_array_th_black);
        $sheet->getStyle('A46:C46')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A48:C49')->applyFromArray($style_array_border);
        $sheet->getStyle('E46:K46')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('E48:K50')->applyFromArray($style_array_border);
        // $sheet->getStyle('E53:G53')->applyFromArray($style_array_border);


        $sheet->getStyle('A54:K54')->applyFromArray($style_array_th_black);
        $sheet->getStyle('A56:B57')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('C56')->applyFromArray($style_array_border);
        $sheet->getStyle('C57')->applyFromArray($style_array_border);
        if($request->bank){
            $sheet->getStyle('E56:F56')->applyFromArray($style_array_th_grey);

        }
        else{
            $sheet->getStyle('E56:I56')->applyFromArray($style_array_th_grey);
        }
        $sheet->getStyle('A59:C59')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A60:C63')->applyFromArray($style_array_border);
        $sheet->getStyle('E59:F60')->applyFromArray($style_array_border);
        $sheet->getStyle('E62:F63')->applyFromArray($style_array_border);
        $sheet->getStyle('H59:I63')->applyFromArray($style_array_border);

        $sheet->getStyle('A65:K65')->applyFromArray($style_array_th_black);

        $sheet->getStyle('A67:C68')->applyFromArray($style_array_border);
        $sheet->getStyle('E67:I68')->applyFromArray($style_array_border);


        $sheet->getStyle('A69:I70')->applyFromArray($style_array_border);

        $sheet->getStyle('A69')->applyFromArray($style_array_bold);
        $sheet->getStyle('A70')->applyFromArray($style_array_bold);

        $sheet->getStyle('M7')->applyFromArray($style_text_right);
        $sheet->getStyle('K31')->applyFromArray($style_text_right);
        //merge for colspan rows
        $sheet->mergeCells('D1:E1');
        $sheet->mergeCells('A14:C14');
        $sheet->mergeCells('A16:C16');
        $sheet->mergeCells('A18:C18');
        $sheet->mergeCells('A20:C20');
        $sheet->mergeCells('A21:C21');
        $sheet->mergeCells('A23:C23');
        $sheet->mergeCells('A27:C27');
        $sheet->mergeCells('A29:C29');
        $sheet->mergeCells('A31:C31');
        $sheet->mergeCells('A46:B46');

        $sheet->mergeCells('A32:H32');
        $sheet->mergeCells('E47:F47');
        if(!$request->bank){
            $sheet->mergeCells('F56:I56');
            $sheet->mergeCells('A69:H69');
            $sheet->mergeCells('A56:B56');
            $sheet->mergeCells('A59:C59');
            $sheet->mergeCells('A57:B57');
            $sheet->mergeCells('A68:B68');
            $sheet->mergeCells('C70:H70');
        }



        $sheet->getStyle('A72:K72')->applyFromArray($style_array_th_black);

        $sheet->getStyle('A75')->applyFromArray($style_array_bold);
        // $sheet->getStyle('C74')->applyFromArray($style_array_border);
        $sheet->getStyle('C74')->applyFromArray($style_text_right);
        $sheet->getStyle('A75:F75')->applyFromArray($style_array_th_grey);

        $nnn = PropertiesTenants::where('propertyId', $input['sub_property_id'])->get()->count();
        $n_tenant_rows = $nnn+9;
        foreach ($property_data as $key => $value) {
            if($key>73 && $key<=(74+$n_tenant_rows-1))
            {
                $property_data[$key][2]=$property_data[$key][4];
                $property_data[$key][3]=$property_data[$key][5];
                $property_data[$key][4]=$property_data[$key][6];
                $property_data[$key][5]=$property_data[$key][7];
                $property_data[$key][6]="";
                $property_data[$key][7]="";
            }
        }
        $sheet->fromArray($property_data);

        // Apply cell format number
        /*$sheet->getStyle("D7:H31")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("K10:K31")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("C35:C42")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("G36:G42")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("J36:K38")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("C46:C49")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("G47:K52")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("C56:C63")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("E67:I68")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("I69:I70")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);*/

        // Apply cell format percentage
        /*$sheet->getStyle("B7:B12")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("B25")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("B36:B38")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("F36:F42")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("J40:K42")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("B47:B49")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("F48:F52")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("B60:B63")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("F59:F63")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("B67")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $sheet->getStyle("B70")->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);*/

        if ($n_tenant_rows > 9) {
            // $sheet->getStyle('B76:E'.($n_tenant_rows+71))->applyFromArray($style_array_border);

            $temp = $n_tenant_rows - 9;

            // Apply cell format number
            $sheet->getStyle('F'.(75 + $temp).':F'.($temp + 80))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
            // Apply cell format percentage
            $sheet->getStyle('F'.($temp + 81))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);

            // echo $temp; die;
            // $sheet->getStyle('A75:D'.($temp+75-1))->applyFromArray($style_array_yellow);
            // $sheet->getStyle('F' . (80 + ($n_tenant_rows - 9)))->applyFromArray($style_array_red);
            $sheet->getStyle('A75:A' . ($temp + 75))->applyFromArray($style_array_border);
            // $sheet->getStyle('A' . (82 + $temp) . ':K' . (82 + $temp))->applyFromArray($style_array_th_black);

            $sheet->getStyle('B76:F' . ($temp + 74))->applyFromArray($style_array_border);

            $sheet->getStyle('B' . (75 + $temp) . ':F' . ($temp + 81))->applyFromArray($style_array_border);

            // $sheet->getStyle('B76:E'.($n_tenant_rows+71))->applyFromArray($style_array_border);
            $sheet->getStyle('D76:D' . ($temp + 74))->applyFromArray($style_text_center);
            $sheet->getStyle('D' . (76 + $temp))->applyFromArray($style_text_center);

            $sheet->getStyle('B76:C' . ($temp + 73))->applyFromArray($style_text_right);
            $sheet->getStyle('B76:E' . ($temp + 75))->applyFromArray($style_text_right);


            // $sheet->mergeCells('B' . (75 + $temp) . ':D' . (75 + $temp));
            $sheet->mergeCells('B' . (76 + $nnn) . ':D' . (76 + $nnn));
            $sheet->mergeCells('B' . (77 + $temp) . ':E' . (77 + $temp));
            $sheet->mergeCells('B' . (78 + $temp) . ':E' . (78 + $temp));
            $sheet->mergeCells('B' . (79 + $temp) . ':E' . (79 + $temp));
            $sheet->mergeCells('B' . (80 + $temp) . ':E' . (80 + $temp));
            $sheet->mergeCells('B' . (81 + $temp) . ':E' . (81 + $temp));

            $sheet->getStyle('A76:C' . ($temp + 75))->applyFromArray($style_array_yellow);

            $sheet->getStyle('C' . ($temp + 75+1))->applyFromArray($style_text_right);

            $sheet->getStyle('F76:F' . (73+$n_tenant_rows))->applyFromArray($style_text_right);


            $v = str_replace(array('%', ','), array('', '.'), $property_data[81 + ($n_tenant_rows - 10)][5]);
            // print_r($property_data);
            // echo 81+($n_tenant_rows-10);
            // echo $v; die;
            if ($v < -25)
                $cl111 = $style_array_red;
            else if ($v < 0 && $v >= -25)
                $cl111 = $style_array_yellow;
            else
                $cl111 = $style_array_green;

            // echo 'F'.(81+($n_tenant_rows-9));
            // echo $cl;
            // die;
            // print_r($cl111); die;

            $sheet->getStyle('F' . (81 + ($n_tenant_rows - 9)))->applyFromArray($cl111);
            // $sheet->getStyle('A75:A'.($temp+74))->applyFromArray($style_array_border);
            $sheet->getStyle('A' . (84 + $temp) . ':K' . (84 + $temp))->applyFromArray($style_array_th_black);

            $temp += 1;


            $sheet->getStyle('A' . (85 + $temp))->applyFromArray($style_array_bold);
            $sheet->getStyle('A' . (88 + $temp))->applyFromArray($style_array_bold);
            $sheet->getStyle('A' . (91 + $temp))->applyFromArray($style_array_bold);
            $sheet->getStyle('A' . (94 + $temp))->applyFromArray($style_array_bold);


            // $sheet->getStyle('A'.(85+$temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(88+$temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(91+$temp).':A'.(93+$temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(86+$temp).':'.'I'.(86+$temp))->setRowHeight(40);
            // $sheet->getStyle('A'.(89+$temp).':'.'I'.(89+$temp))->setRowHeight(40);
            // $sheet->getRowDimension('A'.(86+$temp))->setHeight(30);
            // $sheet->getRowDimension('A'.(89+$temp))->setHeight(30);

            $spreadsheet->getActiveSheet()->getRowDimension(86 + $temp)->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension(89 + $temp)->setRowHeight(30);

            $sheet->getStyle('A' . (86 + $temp))->getAlignment()->setWrapText(true);
            $sheet->getStyle('A' . (89 + $temp))->getAlignment()->setWrapText(true);


            $sheet->mergeCells('A' . (85 + $temp) . ':' . 'I' . (85 + $temp));
            $sheet->mergeCells('A' . (88 + $temp) . ':' . 'I' . (88 + $temp));
            $sheet->mergeCells('A' . (86 + $temp) . ':' . 'I' . (86 + $temp));
            $sheet->mergeCells('A' . (89 + $temp) . ':' . 'I' . (89 + $temp));

            $sheet->getStyle('A' . (85 + $temp) . ':' . 'I' . (85 + $temp))->applyFromArray($style_array_border);
            $sheet->getStyle('A' . (86 + $temp) . ':' . 'I' . (86 + $temp))->applyFromArray($style_array_border);
            $sheet->getStyle('A' . (88 + $temp) . ':' . 'I' . (88 + $temp))->applyFromArray($style_array_border);
            $sheet->getStyle('A' . (89 + $temp) . ':' . 'I' . (89 + $temp))->applyFromArray($style_array_border);
            $sheet->getStyle('A' . (91 + $temp) . ':' . 'I' . (92 + $temp))->applyFromArray($style_array_border);
            $sheet->getStyle('A' . (94 + $temp) . ':' . 'I' . (96 + $temp))->applyFromArray($style_array_border);

            $sheet->mergeCells('A' . (91 + $temp) . ':' . 'I' . (91 + $temp));
            $sheet->mergeCells('A' . (92 + $temp) . ':' . 'I' . (92 + $temp));

            $sheet->mergeCells('A' . (94 + $temp) . ':' . 'I' . (94 + $temp));
            $sheet->mergeCells('A' . (95 + $temp) . ':' . 'I' . (95 + $temp));
            $sheet->mergeCells('A' . (96 + $temp) . ':' . 'I' . (96 + $temp));
        }
        else {
            $sheet->getStyle('D77')->applyFromArray($style_text_center);

            $sheet->getStyle('A' . (84) . ':K' . (84))->applyFromArray($style_array_th_black);

            // $sheet->getStyle('A86:A87')->applyFromArray($style_array_border);
            // $sheet->getStyle('A89:A90')->applyFromArray($style_array_border);
            // $sheet->getStyle('A91:A93')->applyFromArray($style_array_border);

            $sheet->getStyle('A86:I87')->applyFromArray($style_array_bold);
            $sheet->getStyle('A89:I90')->applyFromArray($style_array_bold);
            $sheet->getStyle('A92:I93')->applyFromArray($style_array_bold);

            $sheet->getStyle('A95:I97')->applyFromArray($style_array_bold);

            // $sheet->getStyle('A87:I87')->applyFromArray($style_array_border);
            // $sheet->getStyle('A90:I90')->applyFromArray($style_array_border);
            // $sheet->getStyle('A87:I87')->setRowHeight(40);
            // $sheet->getStyle('A90:I90')->setRowHeight(40);


            $spreadsheet->getActiveSheet()->getRowDimension(87)->setRowHeight(30);
            $spreadsheet->getActiveSheet()->getRowDimension(90)->setRowHeight(30);

            $sheet->getStyle('A87')->getAlignment()->setWrapText(true);
            $sheet->getStyle('A90')->getAlignment()->setWrapText(true);


            $sheet->mergeCells('A86:I86');
            $sheet->mergeCells('A87:I87');
            $sheet->mergeCells('A89:I89');
            $sheet->mergeCells('A90:I90');

            $sheet->mergeCells('A92:I92');
            $sheet->mergeCells('A93:I93');

            $sheet->mergeCells('A95:I95');
            $sheet->mergeCells('A96:I97');
            $sheet->mergeCells('A96:I97');

            $v = str_replace(array('%', ','), array('', '.'), $property_data[81 + ($n_tenant_rows - 10)][5]);
            if ($v < -25)
                $cl111 = $style_array_red;
            else if ($v < 0 && $v >= -25)
                $cl111 = $style_array_yellow;
            else
                $cl111 = $style_array_green;

            $sheet->getStyle('F' . (81 + ($n_tenant_rows - 9)))->applyFromArray($cl111);
        }

        $sheet->getStyle('D1')->applyFromArray($style_text_right);
        $sheet->getStyle('G1')->applyFromArray($style_text_right);
        $sheet->getStyle('I1')->applyFromArray($style_text_right);
        $sheet->getStyle('K1')->applyFromArray($style_text_right);
        $sheet->getStyle('E3')->applyFromArray($style_text_right);
        $sheet->getStyle('G3')->applyFromArray($style_text_right);
        $sheet->getStyle('I3')->applyFromArray($style_text_right);
        $sheet->getStyle('K3')->applyFromArray($style_text_right);
        $sheet->getStyle('K7:L8')->applyFromArray($style_text_right);


        $sheet->getStyle('B7:B29')->applyFromArray($style_text_right);
        $sheet->getStyle('B36:B37')->applyFromArray($style_text_right);
        $sheet->getStyle('B47:B50')->applyFromArray($style_text_right);
        $sheet->getStyle('B60:B63')->applyFromArray($style_text_right);
        $sheet->getStyle('B67:B70')->applyFromArray($style_text_right);
        $sheet->getStyle('D7:H31')->applyFromArray($style_text_right);
        $sheet->getStyle('F36:G42')->applyFromArray($style_text_right);
        $sheet->getStyle('C36:C63')->applyFromArray($style_text_right);
        $sheet->getStyle('F47:K52')->applyFromArray($style_text_right);
        $sheet->getStyle('F59:F69')->applyFromArray($style_text_right);
        $sheet->getStyle('K10:K29')->applyFromArray($style_text_right);
        $sheet->getStyle('J36:K42')->applyFromArray($style_text_right);
        $sheet->getStyle('C60:C63')->applyFromArray($style_text_right);
        $sheet->getStyle('I59:I63')->applyFromArray($style_text_right);

        $sheet->getStyle('J40:J42')->applyFromArray($style_text_center);

        $sheet->getStyle('E67:I70')->applyFromArray($style_text_right);

        $sheet->getStyle('E76:E' . (76 + $n_tenant_rows))->applyFromArray($style_text_right);
        $sheet->getStyle('D76:D' . (76 + $n_tenant_rows))->applyFromArray($style_text_right);


        $sheet->mergeCells('J40:K40');
        $sheet->mergeCells('J41:K41');
        $sheet->mergeCells('J42:K42');

        //------------------------
        //Edit sheet "Tenants"
        //------------------------
        // Create a new worksheet called "My Data"
        /* $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'Tenants');

          // Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
          $spreadsheet->addSheet($myWorkSheet, 1);
          $sheet = $spreadsheet->getSheetByName("Tenants");
          $sheet->fromArray($tenant_data);

          //Set default width of each column
          $sheet->getDefaultColumnDimension()->setWidth(20);

          $sheet->getStyle('A3')->applyFromArray($style_array_bold);
          $sheet->getStyle('C2')->applyFromArray($style_array_border);
          $sheet->getStyle('B3:E3')->applyFromArray($style_array_th_grey);
          $n_tenant_rows = count($tenant_data);
          if ($n_tenant_rows > 10) {
          $sheet->getStyle('B4:E'.($n_tenant_rows))->applyFromArray($style_array_border);
          }

          $spreadsheet->setActiveSheetIndex(0); */

          // print_r($sheet); die;

        if($request->bank)
        {
            $sheet->getStyle('H59:I63')->applyFromArray($style_array_noborder);
            $sheet->removeRow(64, 7);
        }


        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $file_name = "Property-" . $input['property_id'] . ".xlsx";
        header('Content-Disposition: attachment; filename=' . $file_name);
        $writer->save("php://output");
    }

    public function export_to_excel_second(Request $request) {

        $input = $request->all();

        $input['property_data'] = trim($input['property_data']);
        $input['property_data'] = str_replace(__('property.export'), '', $input['property_data']);
        $input['property_data'] = str_replace('Add Ort', '', $input['property_data']);
        $input['property_data'] = str_replace('Empty', '', $input['property_data']);
        $input['property_data'] = str_replace('(nicht ändern!)', '', $input['property_data']);

        $input['property_data'] = str_replace('Gesamtexport', '', $input['property_data']);
        $input['property_data'] = str_replace('Bankexport', '', $input['property_data']);

        $property_data = json_decode($input['property_data']);
        $skipRow = array(64, 65, 66, 67, 68, 69, 70);
        $skipCol = array(7, 8, 9);
        foreach ($property_data as $row => $row_data) {

            foreach ($row_data as $col => $value) {

                $property_data[$row][$col] = trim($value);
                if (in_array($row, $skipRow))
                    $property_data[$row][$col] = '';
                if (in_array($row, array(58, 59, 60, 61, 62)) && in_array($col, $skipCol))
                    $property_data[$row][$col] = '';
                if (strpos($value, '5.) Risikoberechnung') !== false) {
                    $property_data[$row][$col] = str_replace('5.) Risikoberechnung', '4.) Risikoberechnung', $value);
                }
            }
        }

        // pre($input);

        $nnn = PropertiesTenants::where('propertyId', $input['sub_property_id'])->get()->count();


        $input['tenant_data'] = str_replace(__('property.add_row'), '', $input['tenant_data']);
        $input['tenant_data'] = str_replace('X', '', $input['tenant_data']);
        $input['tenant_data'] = str_replace('Empty', '', $input['tenant_data']);

        $tenant_data = json_decode($input['tenant_data']);

        foreach ($tenant_data as $row_data) {
            $row += 1;
            foreach ($row_data as $col => $value) {
                $property_data[$row][$col] = trim($value);
            }
        }



        $input['note_data'] = str_replace('Empty', '', $input['note_data']);
        $note_data = json_decode($input['note_data']);
        foreach ($note_data as $k => $row_data) {
            $row += 1;
            foreach ($row_data as $col => $value) {

                if ($k > 3) {
                    $property_data[$row][$col] = "";
                    continue;
                }


                $property_data[$row][$col] = trim($value);
                if (strpos($value, '6.) Lage / Notizen') !== false) {
                    $property_data[$row][$col] = str_replace('6.) Lage / Notizen', '5.) Lage', $value);
                }
            }
        }

        // print "<pre>";
        // print_r($property_data);
        // die;


        $input['tenant_data'] = trim($input['tenant_data']);


        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Property');
        $sheet->fromArray($property_data);

        //------------------------
        //Edit sheet "Property"
        //------------------------
        //Set default width of each column
        $sheet->getDefaultColumnDimension()->setWidth(20);

        //Set datatype to cells
        //change table style
        $style_array_th_grey = PropertiesService::get_style_array(['bold', 'border', 'bg-grey']);
        $style_array_th_black = PropertiesService::get_style_array(['bold', 'bg-black']);
        $style_array_bold = PropertiesService::get_style_array(['bold']);
        $style_array_border = PropertiesService::get_style_array(['border']);
        $style_array_yellow = PropertiesService::get_style_array(['bg-yellow']);
        $style_array_red = PropertiesService::get_style_array(['bg-red']);
        $style_array_green = PropertiesService::get_style_array(['bg-green']);
        $style_text_right = PropertiesService::get_style_array(['align-right']);
        $style_text_left = PropertiesService::get_style_array(['align-left']);
        $style_text_center = PropertiesService::get_style_array(['align-center']);

        $sheet->getStyle('L14')->applyFromArray($style_text_left);


        //$sheet->getStyle('C1:K1')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('D1:E1')->applyFromArray($style_array_yellow);
        $sheet->getStyle('C3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('E3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('G3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('I3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K3')->applyFromArray($style_array_yellow);
        $sheet->getStyle('J5')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B7:B8')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B10:B12')->applyFromArray($style_array_yellow);
        // $sheet->getStyle('K11:K12')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K18')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K21')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B25')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K29')->applyFromArray($style_array_yellow);




        // $sheet->getStyle('L29')->applyFromArray($style_array_yellow);
        // $sheet->getStyle('M29')->applyFromArray($style_array_yellow);
        // $sheet->getStyle('N29')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B36:B37')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B38')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('C38')->applyFromArray($style_array_yellow);
        $sheet->getStyle('F36:F42')->applyFromArray($style_array_yellow);
        $sheet->getStyle('J36:J37')->applyFromArray($style_array_yellow);
        $sheet->getStyle('K36:K37')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B47:B49')->applyFromArray($style_array_yellow);
        $sheet->getStyle('F48:F49')->applyFromArray($style_array_yellow);
        $sheet->getStyle('E50:K50')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('F52')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B60:B63')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('B67')->applyFromArray($style_array_yellow);
        // $sheet->getStyle('C74')->applyFromArray($style_array_yellow);
        $sheet->getStyle('B7')->applyFromArray($style_array_border);
        $sheet->getStyle('A40:C40')->applyFromArray($style_array_border);
        $sheet->getStyle('A47:C47')->applyFromArray($style_array_border);
        $sheet->getStyle('E47:K47')->applyFromArray($style_array_border);
        $sheet->getStyle('E39:G40')->applyFromArray($style_array_border);
        $sheet->getStyle('I38:K38')->applyFromArray($style_array_border);
        $sheet->getStyle('I40:K40')->applyFromArray($style_array_border);
        $sheet->getStyle('E52:G52')->applyFromArray($style_array_border);
        $sheet->getStyle('E59:F63')->applyFromArray($style_array_border);
        $sheet->getStyle('C72')->applyFromArray($style_array_border);
        // $sheet->getStyle('A74')->applyFromArray($style_array_border);
        $sheet->getStyle('A1:K1')->applyFromArray($style_array_border);
        $sheet->getStyle('A3:K3')->applyFromArray($style_array_border);
        $sheet->getStyle('J5:K5')->applyFromArray($style_array_yellow);
        $sheet->getStyle('L6:M6')->applyFromArray($style_array_border);
        $sheet->getStyle('J7:M7')->applyFromArray($style_array_border);
        $sheet->getStyle('J8:M8')->applyFromArray($style_array_border);
        $sheet->getStyle('J10:K10')->applyFromArray($style_array_border);
        $sheet->getStyle('J11:K11')->applyFromArray($style_array_border);
        $sheet->getStyle('J12:K12')->applyFromArray($style_array_border);
        $sheet->getStyle('J14:K14')->applyFromArray($style_array_border);
        $sheet->getStyle('J16:K16')->applyFromArray($style_array_border);
        $sheet->getStyle('J18:K18')->applyFromArray($style_array_border);
        $sheet->getStyle('J20:K20')->applyFromArray($style_array_border);
        $sheet->getStyle('J21:K21')->applyFromArray($style_array_border);
        $sheet->getStyle('J23:K23')->applyFromArray($style_array_border);
        $sheet->getStyle('J25:K25')->applyFromArray($style_array_border);
        $sheet->getStyle('J27:K27')->applyFromArray($style_array_border);
        $sheet->getStyle('J29:K29')->applyFromArray($style_array_border);
        $sheet->getStyle('J31:K31')->applyFromArray($style_array_border);
        // $sheet->getStyle('L31:L32')->applyFromArray($style_array_border);
        // $sheet->getStyle('J59:J61')->applyFromArray($style_array_border);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);
        //$sheet->getStyle('D1:D2')->applyFromArray($style_array_yellow);


        $sheet->getStyle('A1:B1')->applyFromArray($style_array_th_black);
        $sheet->getStyle('D5:H5')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A7')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('C7:H7')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A8:H8')->applyFromArray($style_array_border);
        $sheet->getStyle('A10:H12')->applyFromArray($style_array_border);
        $sheet->getStyle('A10:H12')->applyFromArray($style_array_border);
        $sheet->getStyle('A14:H14')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A16:H16')->applyFromArray($style_array_border);
        $sheet->getStyle('A18:H18')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A20:H21')->applyFromArray($style_array_border);
        $sheet->getStyle('A23:H23')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A25:H25')->applyFromArray($style_array_border);
        $sheet->getStyle('A27:H27')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A29:H29')->applyFromArray($style_array_border);
        $sheet->getStyle('A31:H31')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A33:K33')->applyFromArray($style_array_th_black);
        $sheet->getStyle('A35:C35')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A36:C38')->applyFromArray($style_array_border);
        $sheet->getStyle('E35:G35')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('E36:G38')->applyFromArray($style_array_border);
        $sheet->getStyle('I35:K35')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('I36:K37')->applyFromArray($style_array_border);
        $sheet->getStyle('A41:C42')->applyFromArray($style_array_border);
        $sheet->getStyle('A42:C42')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('E41:G42')->applyFromArray($style_array_border);
        $sheet->getStyle('E42:G42')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('I41:K42')->applyFromArray($style_array_border);

        $sheet->getStyle('A44:K44')->applyFromArray($style_array_th_black);
        $sheet->getStyle('A46:C46')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A48:C49')->applyFromArray($style_array_border);
        $sheet->getStyle('E46:K46')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('E48:K50')->applyFromArray($style_array_border);
        // $sheet->getStyle('E53:G53')->applyFromArray($style_array_border);


        $sheet->getStyle('A54:K54')->applyFromArray($style_array_th_black);
        $sheet->getStyle('A56:B57')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('C56')->applyFromArray($style_array_border);
        $sheet->getStyle('C57')->applyFromArray($style_array_border);
        $sheet->getStyle('E56:F56')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A59:C59')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('A60:C63')->applyFromArray($style_array_border);
        $sheet->getStyle('E59:F60')->applyFromArray($style_array_border);
        $sheet->getStyle('E62:F63')->applyFromArray($style_array_border);
        //$sheet->getStyle('H59:I63')->applyFromArray($style_array_border);
        //$sheet->getStyle('A60:K60')->applyFromArray($style_array_th_black);
        //$sheet->getStyle('A67:C68')->applyFromArray($style_array_border);
        //$sheet->getStyle('E67:I68')->applyFromArray($style_array_border);
        //$sheet->getStyle('A69:I70')->applyFromArray($style_array_border);
        //$sheet->getStyle('A69')->applyFromArray($style_array_bold);
        //$sheet->getStyle('A70')->applyFromArray($style_array_bold);
        $sheet->getStyle('M7')->applyFromArray($style_text_right);
        $sheet->getStyle('K31')->applyFromArray($style_text_right);
        //merge for colspan rows
        $sheet->mergeCells('D1:E1');
        $sheet->mergeCells('A14:C14');
        $sheet->mergeCells('A16:C16');
        $sheet->mergeCells('A18:C18');
        $sheet->mergeCells('A20:C20');
        $sheet->mergeCells('A21:C21');
        $sheet->mergeCells('A23:C23');
        $sheet->mergeCells('A27:C27');
        $sheet->mergeCells('A29:C29');
        $sheet->mergeCells('A31:C31');
        $sheet->mergeCells('A46:B46');

        $sheet->mergeCells('A32:H32');
        $sheet->mergeCells('E47:F47');
        //$sheet->mergeCells('F56:I56');

        $sheet->mergeCells('A56:B56');
        $sheet->mergeCells('A59:C59');
        $sheet->mergeCells('A57:B57');
        //$sheet->mergeCells('A68:B68');
        //$sheet->mergeCells('A69:H69');
        //$sheet->mergeCells('C70:H70');


        $sheet->getStyle('A72:K72')->applyFromArray($style_array_th_black);

        $sheet->getStyle('A75')->applyFromArray($style_array_bold);
        // $sheet->getStyle('C74')->applyFromArray($style_array_border);
        $sheet->getStyle('C74')->applyFromArray($style_text_right);
        $sheet->getStyle('B75:F75')->applyFromArray($style_array_th_grey);
        $n_tenant_rows = $nnn+11;



        if ($n_tenant_rows > 10) {
            // $sheet->getStyle('B76:E'.($n_tenant_rows+71))->applyFromArray($style_array_border);

            $temp = $n_tenant_rows - 10;

            // echo $temp; die;

            $sheet->getStyle('A68:F' . ($temp + 70))->applyFromArray($style_array_border);

            // $sheet->getStyle('B76:F'.($temp+74))->applyFromArray($style_array_border);
            $sheet->getStyle('B' . (76 + $temp) . ':F' . ($temp + 81))->applyFromArray($style_array_border);

            // $sheet->getStyle('B76:E'.($n_tenant_rows+71))->applyFromArray($style_array_border);
            $sheet->getStyle('D76:D' . ($temp + 74))->applyFromArray($style_text_center);
            $sheet->getStyle('D' . (76 + $temp))->applyFromArray($style_text_center);

            $sheet->getStyle('B76:D' . ($temp + 75))->applyFromArray($style_text_right);


            $sheet->mergeCells('B' . (76 + $temp) . ':D' . (76 + $temp));
            $sheet->mergeCells('B' . (77 + $temp) . ':E' . (77 + $temp));
            $sheet->mergeCells('B' . (78 + $temp) . ':E' . (78 + $temp));
            $sheet->mergeCells('B' . (80 + $temp) . ':E' . (80 + $temp));
            $sheet->mergeCells('B' . (81 + $temp) . ':E' . (81 + $temp));

            $sheet->getStyle('A76:C' . ($temp + 75))->applyFromArray($style_array_yellow);


            $sheet->getStyle('A76:F' . ($temp + 75))->applyFromArray($style_array_border);


            $v = str_replace(array('%', ','), array('', '.'), $property_data[81 + ($n_tenant_rows - 11)][5]);
            // print_r($property_data);
            // echo 81+($n_tenant_rows-10);
            // echo 80+($n_tenant_rows-11);
            // echo $v; die;
            if ($v < -25)
                $cl = $style_array_red;
            else if ($v < 0 && $v >= -25)
                $cl = $style_array_yellow;
            else
                $cl = $style_array_green;

            $sheet->getStyle('F' . (81 + ($n_tenant_rows - 10)))->applyFromArray($cl);
            // $sheet->getStyle('A75:A'.($temp+74))->applyFromArray($style_array_border);
            $sheet->getStyle('A' . (83 + $temp) . ':K' . (83 + $temp))->applyFromArray($style_array_th_black);


            $sheet->getStyle('A' . (85 + $temp))->applyFromArray($style_array_bold);
            $sheet->getStyle('A' . (88 + $temp))->applyFromArray($style_array_bold);
            $sheet->getStyle('A' . (91 + $temp))->applyFromArray($style_array_bold);
            $sheet->getStyle('A' . (94 + $temp))->applyFromArray($style_array_bold);


            // $sheet->getStyle('A'.(85+$temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(88+$temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(91+$temp).':A'.(93+$temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(86+$temp).':'.'I'.(86+$temp))->setRowHeight(40);
            // $sheet->getStyle('A'.(89+$temp).':'.'I'.(89+$temp))->setRowHeight(40);
            // $sheet->getRowDimension('A'.(86+$temp))->setHeight(30);
            // $sheet->getRowDimension('A'.(89+$temp))->setHeight(30);

            $spreadsheet->getActiveSheet()->getRowDimension(86 + $temp)->setRowHeight(30);
            // $spreadsheet->getActiveSheet()->getRowDimension(89+$temp)->setRowHeight(30);

            $sheet->getStyle('A' . (86 + $temp))->getAlignment()->setWrapText(true);
            // $sheet->getStyle('A'.(89+$temp))->getAlignment()->setWrapText(true);


            $sheet->mergeCells('A' . (85 + $temp) . ':' . 'I' . (85 + $temp));
            $sheet->mergeCells('A' . (86 + $temp) . ':' . 'I' . (86 + $temp));
            // $sheet->mergeCells('A'.(88+$temp).':'.'I'.(88+$temp));
            // $sheet->mergeCells('A'.(89+$temp).':'.'I'.(89+$temp));

            $sheet->getStyle('A' . (85 + $temp) . ':' . 'I' . (85 + $temp))->applyFromArray($style_array_border);
            $sheet->getStyle('A' . (86 + $temp) . ':' . 'I' . (86 + $temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(88+$temp).':'.'I'.(88+$temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(89+$temp).':'.'I'.(89+$temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(91+$temp).':'.'I'.(92+$temp))->applyFromArray($style_array_border);
            // $sheet->getStyle('A'.(94+$temp).':'.'I'.(96+$temp))->applyFromArray($style_array_border);
            // $sheet->mergeCells('A'.(91+$temp).':'.'I'.(91+$temp));
            // $sheet->mergeCells('A'.(92+$temp).':'.'I'.(92+$temp));
            // $sheet->mergeCells('A'.(94+$temp).':'.'I'.(94+$temp));
            // $sheet->mergeCells('A'.(95+$temp).':'.'I'.(95+$temp));
            // $sheet->mergeCells('A'.(96+$temp).':'.'I'.(96+$temp));
        }
        else {
            $sheet->getStyle('D77')->applyFromArray($style_text_center);

            $sheet->getStyle('A' . (84) . ':K' . (84))->applyFromArray($style_array_th_black);

            // $sheet->getStyle('A86:A87')->applyFromArray($style_array_border);
            // $sheet->getStyle('A89:A90')->applyFromArray($style_array_border);
            // $sheet->getStyle('A91:A93')->applyFromArray($style_array_border);

            $sheet->getStyle('A86:I87')->applyFromArray($style_array_bold);
            // $sheet->getStyle('A89:I90')->applyFromArray($style_array_bold);
            // $sheet->getStyle('A92:I93')->applyFromArray($style_array_bold);
            // $sheet->getStyle('A95:I97')->applyFromArray($style_array_bold);
            // $sheet->getStyle('A87:I87')->applyFromArray($style_array_border);
            // $sheet->getStyle('A90:I90')->applyFromArray($style_array_border);
            // $sheet->getStyle('A87:I87')->setRowHeight(40);
            // $sheet->getStyle('A90:I90')->setRowHeight(40);
            // $spreadsheet->getActiveSheet()->getRowDimension(87)->setRowHeight(30);
            // $spreadsheet->getActiveSheet()->getRowDimension(90)->setRowHeight(30);
            // $sheet->getStyle('A87')->getAlignment()->setWrapText(true);
            // $sheet->getStyle('A90')->getAlignment()->setWrapText(true);
            // $sheet->mergeCells('A86:I86');
            // $sheet->mergeCells('A87:I87');
            // $sheet->mergeCells('A89:I89');
            // $sheet->mergeCells('A90:I90');

            $sheet->mergeCells('A92:I92');
            $sheet->mergeCells('A93:I93');

            $sheet->mergeCells('A95:I95');
            $sheet->mergeCells('A96:I97');
            $sheet->mergeCells('A96:I97');
        }

        $sheet->getStyle('D1')->applyFromArray($style_text_right);
        $sheet->getStyle('G1')->applyFromArray($style_text_right);
        $sheet->getStyle('I1')->applyFromArray($style_text_right);
        $sheet->getStyle('K1')->applyFromArray($style_text_right);
        $sheet->getStyle('E3')->applyFromArray($style_text_right);
        $sheet->getStyle('G3')->applyFromArray($style_text_right);
        $sheet->getStyle('I3')->applyFromArray($style_text_right);
        $sheet->getStyle('K3')->applyFromArray($style_text_right);
        $sheet->getStyle('K7:L8')->applyFromArray($style_text_right);


        $sheet->getStyle('B7:B29')->applyFromArray($style_text_right);
        $sheet->getStyle('B36:B37')->applyFromArray($style_text_right);
        $sheet->getStyle('B47:B50')->applyFromArray($style_text_right);
        $sheet->getStyle('B60:B63')->applyFromArray($style_text_right);
        $sheet->getStyle('B67:B70')->applyFromArray($style_text_right);
        $sheet->getStyle('D7:H31')->applyFromArray($style_text_right);
        $sheet->getStyle('F36:G42')->applyFromArray($style_text_right);
        $sheet->getStyle('C36:C63')->applyFromArray($style_text_right);
        $sheet->getStyle('F47:K52')->applyFromArray($style_text_right);
        $sheet->getStyle('F59:F69')->applyFromArray($style_text_right);
        $sheet->getStyle('K10:K29')->applyFromArray($style_text_right);
        $sheet->getStyle('J36:K42')->applyFromArray($style_text_right);
        $sheet->getStyle('C60:C63')->applyFromArray($style_text_right);
        $sheet->getStyle('I59:I63')->applyFromArray($style_text_right);

        $sheet->getStyle('J40:J42')->applyFromArray($style_text_center);

        $sheet->getStyle('E67:I70')->applyFromArray($style_text_right);

        $sheet->getStyle('E76:F' . (76 + $n_tenant_rows))->applyFromArray($style_text_right);

        // echo 68+$n_tenant_rows-10; die;

        $sheet->getStyle('A68:F' . (68 + $n_tenant_rows - 10))->applyFromArray($style_array_border);

        // echo $n_tenant_rows; die;


        $sheet->mergeCells('J40:K40');
        $sheet->mergeCells('J41:K41');
        $sheet->mergeCells('J42:K42');

        //------------------------
        //Edit sheet "Tenants"
        //------------------------
        // Create a new worksheet called "My Data"
        /* $myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, 'Tenants');

          // Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
          $spreadsheet->addSheet($myWorkSheet, 1);
          $sheet = $spreadsheet->getSheetByName("Tenants");
          $sheet->fromArray($tenant_data);

          //Set default width of each column
          $sheet->getDefaultColumnDimension()->setWidth(20);

          $sheet->getStyle('A3')->applyFromArray($style_array_bold);
          $sheet->getStyle('C2')->applyFromArray($style_array_border);
          $sheet->getStyle('B3:E3')->applyFromArray($style_array_th_grey);
          $n_tenant_rows = count($tenant_data);
          if ($n_tenant_rows > 10) {
          $sheet->getStyle('B4:E'.($n_tenant_rows))->applyFromArray($style_array_border);
          }

          $spreadsheet->setActiveSheetIndex(0); */
        $sheet->removeRow(64, 7);
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $file_name = "Property-" . $input['property_id'] . ".xlsx";
        header('Content-Disposition: attachment; filename=' . $file_name);
        $writer->save("php://output");
    }

    public function findIndexKeyByColumnName($tenancy_data, $columnName) {
        foreach ($tenancy_data as $key => $data) {
            if ($data[0] == $columnName) {
                return $key;
                break;
            }
        }
    }
    public function getNameFromNumber($num) {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
    public function export_tenancy_to_pdf(Request $request) {
        $input = $request->all();
        $property_name = $input['property_name'];
        $s = TenancySchedulesService::get($input['property_id']);

        $pdf = PDF::loadView('pdf.tenant-pdf', compact('s','property_name'))->setPaper('a2', 'landscape');;
        // $pdf->save(storage_path().'_filename.pdf');
        return $pdf->stream('tenant.pdf');


    }

    public function export_tenancy_to_excel(Request $request) {
        $input = $request->all();

        //This array keep count the number of items in each type of this tenancy
        $n_type_by_items = [];
        foreach (config('tenancy_schedule.item_type') as $type) {
            $n_type_by_items[$type] = TenancyScheduleItem::
                    where('tenancy_schedule_id', $input['tenancy_id'])
                    ->where('type', $type)
                    ->get()
                    ->count();
        }
        // print "<pre>";
        // echo $n_type_by_items[config('tenancy_schedule.item_type.business_vacancy')]; die;
        // print_r($input); die;
        //These variable determine which row is the static row, to apply for dynamic data
        $header_index_of_type_live = 4 + $n_type_by_items[config('tenancy_schedule.item_type.live')] + 2;
        $header_index_of_type_live_vacancy = $header_index_of_type_live + $n_type_by_items[config('tenancy_schedule.item_type.live_vacancy')] + 2;
        $header_index_of_type_business = $header_index_of_type_live_vacancy + $n_type_by_items[config('tenancy_schedule.item_type.business')] + 5;
        $header_index_of_type_business_vacancy = $header_index_of_type_business + $n_type_by_items[config('tenancy_schedule.item_type.business_vacancy')] + $n_type_by_items[config('tenancy_schedule.item_type.free_space')] + 2;
        $input['tenancy_data'] = str_replace(__('tenancy_schedule.add'), '', $input['tenancy_data']);
        $input['tenancy_data'] = str_replace(__('forecast.delete'), '', $input['tenancy_data']);
        $input['tenancy_data'] = str_replace(__('forecast.export'), '', $input['tenancy_data']);
        $input['tenancy_data'] = str_replace("Verl.", '', $input['tenancy_data']);
        $input['tenancy_data'] = str_replace("Empty", '', $input['tenancy_data']);
        $input['tenancy_data'] = str_replace("PDF", '', $input['tenancy_data']);
        $tenancy_data = json_decode($input['tenancy_data']);

        foreach ($tenancy_data as $row => $row_data) {
            foreach ($row_data as $col => $value) {
                if($col==20){
                   $value = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $value);
                    $value = nl2br($value);
                    $value = str_replace("<br />", '', $value);
                    $value = str_replace(" ", '', $value);
                }
                if( $value != '' && !strpos($value, "%") && ( preg_match("[.|,|€]", $value) === 1 || is_numeric($value) ) ){
                    if(!preg_match("/\d{2}\.\d{2}\.\d{4}/", $value)){
                        $value = str_replace('.', '', trim($value));
                        $value = str_replace(',', '.', $value);
                        $value = str_replace('€', '', $value);
                    }
                }
                $tenancy_data[$row][$col] = trim($value);
            }
        }


        // print "<pre>";
        // print_r($tenancy_data);die;

        // echo $header_index_of_type_business_vacancy; die;

        $Wohnen_key = $this->findIndexKeyByColumnName($tenancy_data, "Wohnen") + 1;
        $Vermietet_key = $this->findIndexKeyByColumnName($tenancy_data, "Vermietet") + 1;
        $Leerstand_key = $this->findIndexKeyByColumnName($tenancy_data, "Leerstand") + 1;
        $Gewerbe_key = $this->findIndexKeyByColumnName($tenancy_data, "Gewerbe") + 1;
        $Gewerbe_Vermietet_key = $this->findIndexKeyByColumnName($tenancy_data, "Gewerbe Vermietet") + 1;
        $Gewerbe_Leerstand_key = $this->findIndexKeyByColumnName($tenancy_data, "Gewerbe Leerstand") + 1;
        $Technische_FF_Leerstand_strukturell_key = $this->findIndexKeyByColumnName($tenancy_data, "Technische FF & Leerstand strukturell") + 1;
        $IST_NME_key = $this->findIndexKeyByColumnName($tenancy_data, "IST-NME") + 1;
        $Leerstand_bewertet_key = $this->findIndexKeyByColumnName($tenancy_data, "Leerstand bewertet") + 1;
        $Leerstandsquote_key = $this->findIndexKeyByColumnName($tenancy_data, "Leerstandsquote") + 1;


        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Tenancy');
        $sheet->fromArray($tenancy_data);

        //------------------------
        //Edit sheet "Property"
        //------------------------
        //Set default width of each column
        $sheet->getDefaultColumnDimension()->setWidth(20);

        //change table style
        $style_array_th_grey = PropertiesService::get_style_array(['bold', 'border', 'bg-grey']);
        $style_array_th_black = PropertiesService::get_style_array(['bold', 'border', 'bg-black']);
        $style_array_th_blue = PropertiesService::get_style_array(['bold', 'border', 'bg-blue']);
        $style_array_th_green = PropertiesService::get_style_array(['bold', 'border', 'bg-green']);
        $style_array_bold = PropertiesService::get_style_array(['bold']);
        $style_array_border = PropertiesService::get_style_array(['border']);

        $style_array_noborder = PropertiesService::get_style_array(['no-border']);

        $style_array_th_darkgrey = PropertiesService::get_style_array(['bg-darkgrey']);
        $style_text_right = PropertiesService::get_style_array(['align-right']);
        $style_text_center = PropertiesService::get_style_array(['align-center']);

        $style_array_th_pink = PropertiesService::get_style_array(['bold', 'border', 'bg-pink']);
        $style_array_th_yellow = PropertiesService::get_style_array(['bold', 'border', 'bg-yellow']);
        $style_array_font_red = PropertiesService::get_style_array(['font-red']);



        // Set cell amount type
        $sheet->getStyle("G".($Wohnen_key + 2).":Q".($Vermietet_key - 1))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("G".($Vermietet_key).":Q".($Leerstand_key - 1))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("G".($Leerstand_key + 1).":Q".($Gewerbe_key - 1))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("G".($Gewerbe_key + 2).":Q".($Gewerbe_Vermietet_key - 1))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("G".($Gewerbe_Vermietet_key).":Q".($IST_NME_key - 1))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("B".($IST_NME_key+1).":D".($Leerstand_bewertet_key - 2))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("G".($IST_NME_key+1).":G".($Leerstand_bewertet_key - 2))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("B".($Leerstand_bewertet_key+1).":C".($Leerstandsquote_key - 2))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("F".($Leerstand_bewertet_key+1).":G".($Leerstandsquote_key - 4))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("F".($Leerstandsquote_key - 1).":G".($Leerstandsquote_key - 1))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        $sheet->getStyle("E".($Leerstandsquote_key))->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);

        $tenancy_class = json_decode($input['tenancy_class']);

        foreach ($tenancy_class as $row1 => $row2) {
                // $latter = $this->numberToColumnName($row1+1);
                if(strpos($row2, 'item-pastactive') === false){}
                else
                    $sheet->getStyle("A".($row1+1).":AB".($row1+1))->applyFromArray($style_array_th_darkgrey);


        }
        $tenancy_class = json_decode($input['tenancy_td']);
        // print_r($tenancy_class); die;
        foreach ($tenancy_class as $row1 => $row2) {

                foreach ($row2 as $row3 => $row4){
                    $latter = $this->getNameFromNumber($row3);

                    if(strpos($row4, 'text-right') === false){
                        if(strpos($row4, 'text-center') === false){
                        }
                        else
                            $sheet->getStyle($latter.($row1+1))->applyFromArray($style_text_center);
                    }
                    else
                        $sheet->getStyle($latter.($row1+1))->applyFromArray($style_text_right);
                }

        }
        // die;

        $conditional1 = new Conditional();
        $conditional1->setConditionType(Conditional::OPERATOR_CONTAINSTEXT);
        $conditional1->setOperatorType(Conditional::OPERATOR_CONTAINSTEXT);
        $conditional1->addCondition('Vermietet');
        $conditional1->getStyle()->applyFromArray($style_array_th_black);

        $sheet->getStyle('A1')->applyFromArray($style_array_th_black);

        $sheet->getStyle('A3:A4')->applyFromArray($style_array_th_blue);
        $sheet->getStyle('B3:D4')->applyFromArray($style_array_th_pink);
        $sheet->getStyle('E3:O4')->applyFromArray($style_array_th_blue);
        $sheet->getStyle('P3:Q3')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('R3:AA4')->applyFromArray($style_array_th_blue);
        $sheet->getStyle('AB3:AE4')->applyFromArray($style_array_th_grey);

        $sheet->getStyle('D1')->applyFromArray($style_array_font_red);
        $sheet->getStyle('I1')->applyFromArray($style_array_font_red);
        $sheet->getStyle('M1')->applyFromArray($style_array_font_red);


        $sheet->getStyle('AF3:AG3')->applyFromArray($style_array_border);
        $sheet->getStyle('AF4')->applyFromArray($style_array_border);
        $sheet->getStyle('AF5:AG5')->applyFromArray($style_array_border);



        // $sheet->getStyle('S3:T3')->applyFromArray($style_array_th_grey);
        // $sheet->getStyle('U3:V4')->applyFromArray($style_array_th_blue);
        // $sheet->getStyle('W3:AA4')->applyFromArray($style_array_th_grey);
        $sheet->getStyle('J3:M4')->applyFromArray($style_array_border);
        $sheet->getStyle('M3')->applyFromArray($style_array_bold);
        // $sheet->getStyle('Q3:R4')->applyFromArray($style_array_th_blue);
        $sheet->getStyle("A5:AB$header_index_of_type_live")->applyFromArray($style_array_border);
        $sheet->getStyle("A$header_index_of_type_live:R$header_index_of_type_live")->applyFromArray($style_array_th_blue);


        $key = $this->findIndexKeyByColumnName($tenancy_data, "Vermietet");
        // echo $key; die;
        if ($key) {
            $sheet->getStyle("A" . ($key + 2) . ":AB" . ($n_type_by_items[config('tenancy_schedule.item_type.live')]+$key + 2))->applyFromArray($style_array_border);
        }


        $key = $this->findIndexKeyByColumnName($tenancy_data, "Leerstand");
        if ($key) {
            $sheet->getStyle("A" . ($key + 1) . ":AB" . ($key + 1))->applyFromArray($style_array_th_blue);
        }

        $sheet->getStyle("AA3:AB4")->applyFromArray($style_array_border);


        $key = $this->findIndexKeyByColumnName($tenancy_data, "Gewerbe");
        if ($key) {

            // $sheet->getStyle("A" . ($key + 1) . ":R" . ($key + 2))->applyFromArray($style_array_th_green);
            $sheet->getStyle("A" . ($key + 1) . ":A" . ($key + 2))->applyFromArray($style_array_th_green);
            $sheet->getStyle("B" . ($key + 1) . ":D" . ($key + 2))->applyFromArray($style_array_th_pink);
            $sheet->getStyle("E" . ($key + 1) . ":O" . ($key + 2))->applyFromArray($style_array_th_green);
            $sheet->getStyle("P" . ($key + 1) . ":Q" . ($key + 2))->applyFromArray($style_array_th_grey);
            $sheet->getStyle("P" . ($key + 2))->applyFromArray($style_array_th_yellow);
            $sheet->getStyle("Q" . ($key + 2))->applyFromArray($style_array_th_grey);
            $sheet->getStyle("R" . ($key + 1) . ":AA" . ($key + 2))->applyFromArray($style_array_th_green);
            $sheet->getStyle("AB" . ($key + 1) . ":AE" . ($key + 2))->applyFromArray($style_array_th_grey);


            // $sheet->getStyle("S" . ($key + 1) . ":T" . ($key + 2))->applyFromArray($style_array_th_grey);
            // $sheet->getStyle("U" . ($key + 1) . ":V" . ($key + 2))->applyFromArray($style_array_th_green);
            // $sheet->getStyle("W" . ($key + 1) . ":AB" . ($key + 2))->applyFromArray($style_array_th_grey);

            $sheet->getStyle("S" . ($key + 1) . ":T" . ($key + 2))->applyFromArray($style_array_border);


            $sheet->getStyle('A1:N1')->applyFromArray($style_array_border);
            $sheet->getStyle('A33:E33')->applyFromArray($style_array_border);
        }

        $key = $this->findIndexKeyByColumnName($tenancy_data, "Gewerbe Vermietet");
        if ($key) {
            $sheet->getStyle("A" . ($key + 1) . ":AB" . ($key + 1))->applyFromArray($style_array_th_green);
        }
        $key = $this->findIndexKeyByColumnName($tenancy_data, "Gewerbe Leerstand");
        if ($key) {
            $sheet->getStyle("A" . ($key + 1) . ":AB" . ($key + 1))->applyFromArray($style_array_th_green);
        }
        $key = $this->findIndexKeyByColumnName($tenancy_data, "Technische FF & Leerstand strukturell");
        if ($key) {
            $sheet->getStyle("A" . ($key + 1) . ":AB" . ($key + 1))->applyFromArray($style_array_th_green);
        }

        $sheet->getStyle("A" . ($header_index_of_type_live_vacancy + 4) . ":AB" . ($header_index_of_type_business - 1))->applyFromArray($style_array_border);
        $sheet->getStyle("A$header_index_of_type_business:AB$header_index_of_type_business")->applyFromArray($style_array_border);

        $sheet->getStyle("A" . ($header_index_of_type_business + 1) . ":AB$header_index_of_type_business_vacancy")->applyFromArray($style_array_border);
        // $sheet->getStyle("I" . ($header_index_of_type_business_vacancy + 3) . ":I" . ($header_index_of_type_business_vacancy + 4))->applyFromArray($style_array_border);

        $sheet->getStyle("A" . ($header_index_of_type_business_vacancy + 3) . ":AB" . ($header_index_of_type_business_vacancy + 4))->applyFromArray($style_array_border);

        $sheet->getStyle("K" . ($header_index_of_type_business_vacancy + 3) . ":K" . ($header_index_of_type_business_vacancy + 4))->applyFromArray($style_array_border);

        $sheet->getStyle("A" . ($header_index_of_type_business_vacancy + 8) . ":D" . ($header_index_of_type_business_vacancy + 11))->applyFromArray($style_array_border);
        // A23:D23
        $sheet->getStyle("A" . ($header_index_of_type_business_vacancy + 8) . ":D" . ($header_index_of_type_business_vacancy + 8))->applyFromArray($style_array_th_grey);
        $sheet->getStyle("F" . ($header_index_of_type_business_vacancy + 8) . ":G" . ($header_index_of_type_business_vacancy + 8))->applyFromArray($style_array_th_grey);

        $sheet->getStyle("A" . ($header_index_of_type_business_vacancy + 13) . ":C" . ($header_index_of_type_business_vacancy + 13))->applyFromArray($style_array_border);
        $sheet->getStyle("A" . ($header_index_of_type_business_vacancy + 13) . ":C" . ($header_index_of_type_business_vacancy + 13))->applyFromArray($style_array_bold);
        $sheet->getStyle("A" . ($header_index_of_type_business_vacancy + 13) . ":C" . ($header_index_of_type_business_vacancy + 13))->applyFromArray($style_array_th_grey);
        $sheet->getStyle("F" . ($header_index_of_type_business_vacancy + 13) . ":G" . ($header_index_of_type_business_vacancy + 13))->applyFromArray($style_array_th_grey);

        //Leerstand bewertet
        $sheet->getStyle("A" . ($header_index_of_type_business_vacancy + 13) . ":C" . ($header_index_of_type_business_vacancy + 16))->applyFromArray($style_array_border);
        $sheet->getStyle("F" . ($header_index_of_type_business_vacancy + 8) . ":G" . ($header_index_of_type_business_vacancy + 11))->applyFromArray($style_array_border);
        $sheet->getStyle("F" . ($header_index_of_type_business_vacancy + 13) . ":G" . ($header_index_of_type_business_vacancy + 17))->applyFromArray($style_array_border);

        $sheet->getStyle("A" . ($header_index_of_type_business_vacancy + 18) . ":E" . ($header_index_of_type_business_vacancy + 18))->applyFromArray($style_array_border);



        $key = $this->findIndexKeyByColumnName($tenancy_data, "Technische FF & Leerstand strukturell")+5+$n_type_by_items[config('tenancy_schedule.item_type.free_space')]; //IST-NME row


        $sheet->getStyle("A" . ($key) . ":G" . ($key + 10))->applyFromArray($style_array_border);

        $sheet->getStyle("E" . ($key) . ":E" . ($key + 9))->applyFromArray($style_array_noborder);
        $sheet->getStyle("A" . ($key+4) . ":G" . ($key + 4))->applyFromArray($style_array_noborder);
        $sheet->getStyle("A" . ($key+9) . ":D" . ($key + 9))->applyFromArray($style_array_noborder);
        $sheet->getStyle("A" . ($key+8))->applyFromArray($style_array_noborder);
        $sheet->getStyle("A" . ($key+3))->applyFromArray($style_array_noborder);
        $sheet->getStyle("F" . ($key+3))->applyFromArray($style_array_noborder);

        $sheet->getStyle("B" . ($key+1) . ":D" . ($key + 3))->applyFromArray($style_text_right);
        $sheet->getStyle("B" . ($key+6) . ":C" . ($key + 8))->applyFromArray($style_text_right);

        $sheet->getStyle("B" . ($key+3) . ":C" . ($key + 3))->applyFromArray($style_array_th_grey);
        $sheet->getStyle("B" . ($key+8) . ":C" . ($key + 8))->applyFromArray($style_array_th_grey);

        // $style_text_right



        $sheet->getStyle("D" . ($key+5) . ":D" . ($key + 8))->applyFromArray($style_array_noborder);
        $sheet->getStyle("F" . ($key+10) . ":G" . ($key + 10))->applyFromArray($style_array_noborder);
        $sheet->getStyle("F" . ($key+7) . ":G" . ($key + 7))->applyFromArray($style_array_noborder);

        // echo $key;







        $sheet->mergeCells("A" . ($header_index_of_type_business_vacancy + 18) . ":D" . ($header_index_of_type_business_vacancy + 18));

        // $sheet->getStyle("A3")->applyFromArray($style_array_noborder);

        $sheet->getStyle("G" . ($header_index_of_type_business_vacancy + 11))->applyFromArray($style_array_th_grey);



        $sheet->getStyle("F" . ($header_index_of_type_business_vacancy + 16))->applyFromArray($style_array_th_grey);
        $sheet->getStyle("G" . ($header_index_of_type_business_vacancy + 16))->applyFromArray($style_array_th_grey);





//        $sheet->getCellByColumnAndRow()
        //merge for colspan rows


        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $file_name = "Tenancy-" . $input['tenancy_id'] . ".xlsx";
        header('Content-Disposition: attachment; filename=' . $file_name);
        $writer->save("php://output");
    }

    public function property_upload_pdf(Request $request) {

        $attributeNames = array(
            'upload_pdf' => 'PDF',
        );

        $validator = Validator::make($request->all(), [
                    'upload_pdf' => 'required|mimes:pdf|max:200096',
        ]);

        // echo $request->file('upload_pdf')->getClientOriginalName(); die;

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->errors()->first());
        }
        $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
        if(!$propertyMainDir){
            return redirect()->back()->with('error', 'Property directory not created in google Drive.');
        }
        $propertyAnhangeDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Anhange')->first();
        if(!$propertyAnhangeDir){
            return redirect()->back()->with('error', 'Property Anhänge directory not created in google Drive.');
        }

        $file = $request->file('upload_pdf');
        $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $originalExt = $file->getClientOriginalExtension();
        $folder = $propertyAnhangeDir->dir_path;
        $fileData = File::get($file);

        //upload file
        $file2 =  Storage::cloud()->put($folder.'/'.$originalName.'.'.$originalExt, $fileData);

        //get google drive id
        $contents = collect(Storage::cloud()->listContents($folder, false));

        $file = $contents ->where('type','file')->where('filename', $originalName)->where('extension', $originalExt) ->first();




        $data_array = ([
            'user_id' => Auth::id(),
            'property_id' => $request->property_id,
            'upload_file_name' => $request->file('upload_pdf')->getClientOriginalName(),
            'file_name' => $file['name'],
            'file_basename' => $file['basename'],
            'file_path' => $file['path'],
            'file_href' => 'https://drive.google.com/file/d/'.$file['basename'],
            'extension' => $file['extension']
        ]);

        $insert_data = DB::table('pdf')->insert($data_array);
        // if ($insert_data) {
        // return response()->json(array(
        // 'image' => url('/uploads/service_logos').'/'.$filename,
        // 'alert_type' => 'success',
        // 'msg_text' => 'Logo Updated!'
        // ));
        // }
        return redirect('properties/'.$request->property_id.'?tab=expose')->with('message_expose', 'Upload File Successfully');
    }

    function panga() {


        $properties = DB::table('properties')->where('Ist', '!=', 0)->get();
        foreach ($properties as $property) {

            if (!is_array($property->bank_ids) && $property->bank_ids !== NULL) {

                DB::table('properties')->where('id', $property->id)->update(['bank_ids' => '[' . $property->Ist . ']']);
            }
        }
    }

//Method for show user form/////////////
    public function ShowUserForm($token, $step) {
        $res = email_template2_sending_info::where(array("doc_key" => $token))->get()->first();
        if ($res) {
            $data['property_id'] = $res->property_id;
            $data['properties'] = DB::table('properties')
                            ->leftjoin('users', 'users.id', '=', 'properties.transaction_m_id')
                            ->select('properties.id', 'properties.name_of_property', 'properties.plz_ort', 'properties.ort', 'properties.strasse', 'properties.hausnummer', 'properties.construction_year', 'properties.type_of_property', 'properties.ankermieter1', 'properties.Lease_term_up_to', 'properties.plot', 'users.name')
                            ->where('properties.id', $res->property_id)->first();
            $data['questions'] = DB::table('questions')->where('property_id', $res->property_id)->get();
            $data['email_info'] = DB::table('email_template2_sending_info')->where('doc_key', $token)->first();
            $data['token'] = $token;
            $data['slug'] = 'user_form';
            $data['step'] = $step;
            $data['doc_key'] = $token;
            //echo'<pre>';print_r($data['questions']);die;
            switch ($step) {
                case 'a':
                    $data['step_next'] = 'b';
                    break;
                case 'b':
                    $data['step_next'] = 'c';
                    $data['step_pre'] = 'a';
                    break;
                case 'c':
                    $data['step_next'] = 'd';
                    $data['step_pre'] = 'b';
                    break;
                case 'd':
                    if ($data['questions']->count() == 0) {
                        $default_questions = array();
                        //code for insert default Questions
                        $default_questions[0]['property_id'] = $res->property_id;
                        $default_questions[0]['qn_heading'] = 'Ihre Kontaktdaten';
                        $default_questions[0]['questions'] = '{"question":["Institut","Anschrift","Ansprechpartner","Telefon","E-Mail"],"required":["1","1","1","0","0"]}';
                        $default_questions[1]['property_id'] = $res->property_id;
                        $default_questions[1]['qn_heading'] = 'Ihr Finanzierungsangebot';
                        $default_questions[1]['questions'] = '{"question":[{"qn":"Finanzierungstyp","op":["Non-Recourse","Option 2","Keine Angabe","Other"]},"Kreditbetrag (\u20ac)","Variabler Zinssatz (%)","Tilgung (%)","Laufzeit (Jahre)"],"required":["0","1","1","1","1"]}';
                        $default_questions[2]['property_id'] = $res->property_id;
                        $default_questions[2]['qn_heading'] = 'Anmerkungen';
                        $default_questions[2]['questions'] = '{"question":[null],"required":["0"]}';
                        foreach ($default_questions as $default_question) {
                            DB::table('questions')->insert($default_question);
                        }
                        $data['questions'] = DB::table('questions')->where('property_id', $res->property_id)->get();
                    }
                    $data['step_next'] = 'e';
                    $data['step_pre'] = 'c';
                    break;
                default:
                    abort(403, 'Unauthorized action.');
            }

            return view('user_form.form', $data);
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    //Method for save user form/////////////
    public function SaveUserForm(Request $request) {
            Log::info('Route Post called for saveuserForm');
        // $validator = Validator::make($request->all(), [
        //     'institut' => 'required|max:255',
        //     'anschrift' => 'required|max:255',
        //     'ansprechpartner' => 'required|max:255',
        //     'telefon' => 'required|max:255',
        //     'email' => 'required|max:255',
        //     'kreditbetrag' => 'required|max:255',
        //     'variabler_zinssatz' => 'required|max:255',
        //     'tilgung' => 'required|max:255',
        //     'laufzeit' => 'required|max:255'
        // ]);

        // if ($validator->fails()) {
        //     return redirect('/saveuserForm/fail');
        // }
        $user_form_data['email_id'] = $request->get('email_id');
        $user_form_data['user_data'] = json_encode($request->get('user_form_fields'));

        $user_form_data['property_id'] = $request->get('property_id');
        $user_form_data['doc_key'] = $request->get('doc_key');

        $userFormFields = $request->get('user_form_fields');
        $ihreKontaktdaten = $userFormFields['Ihre Kontaktdaten'];
        $finanzierungsangebot = $userFormFields['Ihr Finanzierungsangebot'];
        $anmerkungen = $userFormFields['Anmerkungen'];

        $user_form_data['institut'] = $ihreKontaktdaten['Institut'];
        $user_form_data['anschrift'] = $ihreKontaktdaten['Anschrift'];
        $user_form_data['ansprechpartner'] = $ihreKontaktdaten['Ansprechpartner'];
        $user_form_data['telefon'] = $ihreKontaktdaten['Telefon'];
        $user_form_data['email'] = $ihreKontaktdaten['E-Mail'];
        //$user_form_data['finanzierungstyp'] = $finanzierungsangebot['Finanzierungstyp'];
        $user_form_data['finanzierungstyp'] = " ";
        $user_form_data['kreditbetrag'] = $finanzierungsangebot['Kreditbetrag (€)'];
        $user_form_data['variabler_zinssatz'] = $finanzierungsangebot['Variabler Zinssatz (%)'];
        $user_form_data['tilgung'] = $finanzierungsangebot['Tilgung (%)'];
        $user_form_data['laufzeit'] = $finanzierungsangebot['Laufzeit (Jahre)'];
        $user_form_data['anmerkungen'] = $anmerkungen[0];

        $res = DB::table('user_form_data')->insert($user_form_data);
        if ($res){
            $data['email_info']=DB::table('email_template2_sending_info')->where('doc_key',$request->get('doc_key'))->first();
			$data['user_form_data'] = json_decode($user_form_data['user_data'], true);

            // mail::to($request->get('contact_email'))->send(new UserFormEmail($data));
            mail::to('marvin1heinrich@gmail.com')->send(new UserFormEmail($data));

            return redirect('/saveuserForm/success');
        }
        else
            echo'error';
    }

    /*     * ****** Export object data list of property ***** */

    public function exportDataListObjectOfProperty($id) {
        return Excel::download(new ObjectDataListView($id), 'objekdatenblatt.xlsx');
    }

    public function update_property_Steuerberater(Request $request, $prop_id) {
        $p = $properties = Properties::where('id', '=', $prop_id)->where('Ist', 0)->where('soll', 0)->first();
        $Steuerberater_user = DB::table('users')->select('id', 'name')->where('id', $request->value)->first();
        $Steuerberater_user_name = (isset($Steuerberater_user->name)) ? $Steuerberater_user->name : '';

        $field = $request->pk;

        $properties->$field = $request->value;
        $properties->save();
        $response = ['success' => true];

        $user = Auth::user();
        $name = $user->name;
        $replyemail = $user->email;
        $email = $toname = "";
        $am2_name = $am2_email = "";

        $url = route('properties.show',['property'=>$prop_id]).'?tab=einkauf_tab';
        $url = '<a href="'.$url.'">'.$url.'</a>';

        if(isset($p->asset_manager) && isset($p->asset_manager->name)){
            $toname = $p->asset_manager->name;
            $email = $p->asset_manager->email;
        }

        if(isset($p->asset_manager_two) && isset($p->asset_manager_two->name)){
            $am2_name = $p->asset_manager_two->name;
            $am2_email = $p->asset_manager_two->email;
        }


        $text = "Hallo ".$toname.",<br><br> für das Objekt ".$p->plz_ort.' '.$p->ort." wurde der Steuerberater ".$Steuerberater_user_name." hinterlegt: ".$url;
        $subject = "Steuerberater für das Objekt ".$p->plz_ort.' '.$p->ort;
        if( $email && !in_array($email, $this->mail_not_send()) ){
            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                $message->to($email)
                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                ->replyTo($replyemail, $name)
                ->subject($subject);
            });
        }

        $text = "Hallo ".$am2_name.",<br><br> für das Objekt ".$p->plz_ort.' '.$p->ort." wurde der Steuerberater ".$Steuerberater_user_name." hinterlegt: ".$url;
        $subject = "Steuerberater für das Objekt ".$p->plz_ort.' '.$p->ort;
        if( $am2_email && $am2_email != $email && !in_array($am2_email, $this->mail_not_send()) ){
            Mail::send('emails.general', ['text' => $text], function ($message) use($am2_email,$subject,$replyemail, $name) {
                $message->to($am2_email)
                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                ->replyTo($replyemail, $name)
                ->subject($subject);
            });
        }


        return response()->json($response);
    }

    public function exportPdf($id)
    {
        // echo "her"; die;
        //Get selecting property tabs
        $tab_types = ['properties', 'tenant-listing', 'swot-template', 'schl-template', 'comment_tab', 'clever-fit-rental-offer', 'planung-berechnung-wohnen', 'tenancy-schedule', 'verkauf_tab', 'einkauf_tab', 'Leerstandsflächen', 'email_template', 'email_template2', 'Anfragen'];
        if (isset($_GET['tab'])) {
            $tab = $_GET['tab'];
            if (!in_array($tab, $tab_types)) {
                $tab = 'properties';
            }
        } else {
            $tab = 'properties';
        }

        $update_date = 1;
        if ($tab == "tenancy-schedule")
            $update_date = 0;


        $list = array();

        $properties1 = $properties = Properties::where('id', '=', $id)->where('Ist', 0)->where('soll', 0)->first();
        $users = User::all();
        //echo '<pre>';
        //print_r($properties1);
        //*******************code for Bankers email form
        $bankers_email_info = Properties::where('main_property_id', $id)->first();

        $propertiesExtra1s = PropertiesTenants::where('propertyId', $bankers_email_info->id)->get();
        $total_ccc = 0;
        $ankdate1 = $ankdate2 = "";
        foreach ($propertiesExtra1s as $k => $propertiesExtra1) {
            $total_ccc += $propertiesExtra1->net_rent_p_a;
            if ($k == 0)
                $ankdate1 = $propertiesExtra1->mv_end;
            if ($k == 1)
                $ankdate2 = $propertiesExtra1->mv_end;
        }
        // echo $total_ccc; die;
        $properties->net_rent_pa = $total_ccc;

        $rental_period_until = null;
        if (isset($propertiesExtra1s[0]['tenant']))
            $rental_period_until = $propertiesExtra1s[0]['tenant'] . ' ';

        $properties->the_main_tenant = $rental_period_until;
        $properties->rental_period_until = $ankdate1;
        //echo'<pre>';print_r($propertiesExtra1s);die;
        //properties againts Ist and Soll
        if ($properties->bank_ids) {
            $final_bank_ids = json_decode($properties->bank_ids, true);

            if (count($final_bank_ids) > 0) {



                $index = 0;
                for ($i = 0; $i < count($final_bank_ids); $i++) {

                    $bank_id = $final_bank_ids[$i];
                    if ($i == 0) {
                        $properties1 = Properties::where('Ist', '=', $bank_id)->where('soll', 0)->first();

                        if ($properties1 && !$properties->niedersachsen && $properties1->niedersachsen) {
                            $properties->niedersachsen = $properties1->niedersachsen;
                        }
                    }

                    $ist_sheet = Properties::select('name_of_property', 'id', 'standard_property_status')->where('Ist', '=', $bank_id)->where('main_property_id', $id)->first();
                    $soll_sheet = Properties::select('name_of_property', 'id', 'standard_property_status')->where('soll', '=', $bank_id)->where('main_property_id', $id)->first();
                    $bank = DB::table('banks')->select('name')->where('id', $bank_id)->first();

                    if (!$ist_sheet || !$soll_sheet)
                        continue;





                    if ($bank) {
                        if (isset($list[$bank_id . '_' . $bank->name])) {

                            $list[$bank_id . '_' . $bank->name]['ist']['id'] = $ist_sheet->id;
                            $list[$bank_id . '_' . $bank->name]['ist']['name'] = $ist_sheet->name_of_property;
                            $list[$bank_id . '_' . $bank->name]['ist']['standard_property_status'] = $ist_sheet->standard_property_status;


                            $list[$bank_id . '_' . $bank->name]['soll']['id'] = $soll_sheet->id;
                            $list[$bank_id . '_' . $bank->name]['soll']['name'] = $soll_sheet->name_of_property;
                            $list[$bank_id . '_' . $bank->name]['soll']['standard_property_status'] = $soll_sheet->standard_property_status;
                        } else {

                            $list[$bank_id . '_' . $bank->name]['ist']['id'] = $ist_sheet->id;
                            $list[$bank_id . '_' . $bank->name]['ist']['name'] = $ist_sheet->name_of_property;
                            $list[$bank_id . '_' . $bank->name]['ist']['standard_property_status'] = $ist_sheet->standard_property_status;


                            $list[$bank_id . '_' . $bank->name]['soll']['id'] = $soll_sheet->id;
                            $list[$bank_id . '_' . $bank->name]['soll']['name'] = $soll_sheet->name_of_property;
                            $list[$bank_id . '_' . $bank->name]['soll']['standard_property_status'] = $soll_sheet->standard_property_status;
                        }
                    }
                }
            }

            if (isset($_GET['rashid'])) {

                echo '<pre>';
                print_r($list);
                echo '</pre>';
                die();
            }


            $ist_soll_properties = Properties::select('name_of_property', 'id', 'Ist', 'soll')
                ->whereIn('Ist', $final_bank_ids)->orWhereIn('soll', $final_bank_ids)->get();
        } else {
            $ist_soll_properties = 0;
        }
        // end properties againts Ist and Soll



        $puser = User::where('id', $properties->user_id)->first();
        $properties->user_name = "";
        if ($puser)
            $properties->user_name = $puser->name;

        $clever_fit_fields = json_decode($properties->clever_fit_fields, true);

        if (!is_array($clever_fit_fields)) {
            $clever_fit_fields = array();
        }
        $clever_fit_fields = PropertiesService::initCleverFitFields($clever_fit_fields);

        $properties->clever_fit_fields = $clever_fit_fields;



        $planung_fields = json_decode($properties->planung_fields, true);

        if (!is_array($planung_fields)) {
            $planung_fields = array();
        }
        $planung_fields = PropertiesService::initPlanungFields($planung_fields);

        $properties->planung_fields = $planung_fields;

        $tenants = Tenants::where('property_id', $id)->get();

        $bank_ids = json_decode($properties->bank_ids);
        $banks = [];

        $bank_all = [];
        $fake_bank = new Banks();
        $fake_bank->name = '';
        $fake_bank->user_id = 0;
        $fake_bank->with_real_ek = 0;
        $fake_bank->from_bond = 0;
        $fake_bank->bank_loan = 0;
        $fake_bank->interest_bank_loan = 0;
        $fake_bank->eradication_bank = 0;
        $fake_bank->eradication_bank = 0;
        $fake_bank->interest_bond = 0;
        if ($bank_ids != null) {
            foreach ($bank_ids as $bank_id) {
                $banks[] = Banks::where('id', '=', $bank_id)->first();
            }
        } else {
            $banks[] = $fake_bank;
        }

        $bank_all = Banks::all();


        $schl_banks = $properties->schl_banks != null ? (array) json_decode($properties->schl_banks) : [];

        $tenancy_schedule_data = TenancySchedulesService::get($id, $update_date);
        $properties->masterliste = Masterliste::where('id', $properties->masterliste_id)->first();
        $propertiesExtra1s = PropertiesExtra1::where('propertyId', $id)->get();

        $properties_pdf = DB::table('pdf')->where('property_id', $id)->get();

        $comments = PropertyComments::where('property_id', $id)->get();
        $countries_ad = DB::table('countries')->get();

        $mietflachen = DB::table('mietflächen')->where('property_id', $id)->get();

        $ads = DB::table('ads')->where('property_id', $id)->first();

        if ($ads) {
            $previous_states = State::where('country_id', $ads->country_id)->get();
            $previous_cities = City::where('state_id', $ads->state_id)->get();
        } else {
            $previous_states = State::where('country_id', old('country'))->get();
            $previous_cities = City::where('state_id', old('state'))->get();
        }






        $insurances = PropertyInsurance::where('property_id', $id)->get();
        $property_service_providers = PropertyServiceProvider::where('property_id', $id)->get();

        $investation = PropertyInvestation::where('property_id', $id)->get();
        $maintenance = PropertyMaintenance::where('property_id', $id)->get();

        $tr_users = User::whereRaw('role=2 or second_role=2 or id=1 or id=10')->get();
        $as_users = User::whereRaw('role=4 or second_role=4 or id=1 or id=10')->get();

        $s_data = PropertiesSaleDetail::where('property_id', $id)->get()->toArray();

        $sale_data = array();
        foreach ($s_data as $key => $value) {
            $sale_data[$value['type']] = $value;
        }
        $ps_data = PropertiesSale::where('property_id', $id)->first();


        $pb_data = PropertiesBuy::where('property_id', $id)->first();
        $b_data = PropertiesBuyDetail::where('property_id', $id)->get()->toArray();
        $buy_data = array();
        foreach ($b_data as $key => $value) {
            if(isset($buy_data[$value['type']])){}
            else
            $buy_data[$value['type']] = $value;
        }
        ##### for api images
        $api_atachment_imgs = DB::table('properties_api_attachments')->where('property_id', $id)->get();
        //echo '<pre>';
        //print_r($properties1);
        //die;
        $verkauf_tab = DB::table('verkauf_tab')->where('property_id', $id)->orderBy('id', 'asc')->get();

        $email_template = DB::table('email_template')->where('property_id', $id)->first();

        if ($email_template && $email_template->email_template_users) {
            $u = User::find($email_template->email_template_users);
            if ($u && $u->signature)
                $email_template->signature = $u->signature;
        }



        $getuploaditemlist = array();
        $url = "https://vermietung.fcr-immobilien.de/getuploaditemlist/" . $id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str1111 = curl_exec($curl);
        curl_close($curl);
        if ($str1111) {
            $getuploaditemlist = json_decode($str1111, true);
        }
        // var_dump(in_array(213, $getuploaditemlist));
        // print_r($getuploaditemlist); die;

        $bank_popup_data = array();
        //if(isset($_GET['awan'])){

        if (Request()->showall) {
            $bank_popup_data = Banks::whereNotNull('contact_email')->where("contact_email", "!=", '')->get();
        } else {
            if (is_numeric($properties->plz_ort)) {

                $postal_code = substr($properties->plz_ort, 0, 2);
                $bank_popup_data = DB::select(" select * from banks where postal_code like '" . $postal_code . "%'  ");
                if (count($bank_popup_data) == 0) {

                    $postal_code = substr($properties->plz_ort, 0, 1);
                    $bank_popup_data = DB::select(" select * from banks where postal_code like '" . $postal_code . "%'  ");
                }
//
            }
        }
        // die();
        // }



        /**
         *
         * PUT Data into Email Template
         * */
        $send_email_template2_sending_info = array();
        $email_template2 = array();
        if (isset($_GET['tab']) && isset($_GET['bank_id'])) {


            $check = DB::table('email_template2')->where('property_id', $id)->where('bank_id', $_GET['bank_id'])->first();

            if (is_null($check)) {
                $this->add_email2_entry($_GET['bank_id'], $properties);
            }

            $send_email_template2_sending_info = email_template2_sending_info::where('property_id', $properties->id)->get();
            $email_template2 = DB::table('email_template2')->where('property_id', $id)->where('bank_id', $_GET['bank_id'])->first();
        }

        $vads = DB::table('vads')->where('property_id', $id)->first();
        if ($vads) {
            $vprevious_states = State::where('country_id', $vads->country_id)->get();
            $vprevious_cities = City::where('state_id', $vads->state_id)->get();
        } else {
            $vprevious_states = State::where('country_id', old('country'))->get();
            $vprevious_cities = City::where('state_id', old('state'))->get();
        }


        $exportads = DB::table('export_ads')->where('property_id', $id)->first();
        if ($exportads) {
            $eprevious_states = State::where('country_id', $exportads->country_id)->get();
            $eprevious_cities = City::where('state_id', $exportads->state_id)->get();
        } else {
            $eprevious_states = State::where('country_id', old('country'))->get();
            $eprevious_cities = City::where('state_id', old('state'))->get();
        }

        // print_r($exportads); die;

        $sh = DB::table('sheetproperties')->where('property_id', $id)->first();



        $email_template_users = User::where('role', 1)->orWhere('role', 2)->get();

        $ccomments = Comment::where('property_id', $properties->id)->get();

        $contact_queries = DB::table('contact_queries')->where('property_id', $id)->get();

        $propertiescheckd = Properties::where('main_property_id', $id)->where('Ist', '!=', 0)->first();
        $excelFileCellD42 = ($propertiescheckd->gesamt_in_eur) + (
                ($propertiescheckd->real_estate_taxes * $propertiescheckd->gesamt_in_eur) + ($propertiescheckd->estate_agents * $propertiescheckd->gesamt_in_eur) + (($propertiescheckd->Grundbuch * $propertiescheckd->gesamt_in_eur) / 100) + ($propertiescheckd->evaluation * $propertiescheckd->gesamt_in_eur) + ($propertiescheckd->others * $propertiescheckd->gesamt_in_eur) + ($propertiescheckd->buffer * $propertiescheckd->gesamt_in_eur)
            );
        $excelFileCellG59 = 0;
        if ($excelFileCellD42 && $total_ccc) {
            $excelFileCellG59 = round($excelFileCellD42 / $total_ccc, 1);
        }
        // echo $excelFileCellG59; die;

        $excelFileCellD48 = $propertiescheckd->from_bond * $excelFileCellD42;

        $excelValueE7 = $total_ccc;
        $excelValueE10 = $propertiescheckd->maintenance_nk * $propertiescheckd->gesamt_in_eur;
        $excelValueE11 = $propertiescheckd->operating_costs_nk * $total_ccc;
        $excelValueE12 = $propertiescheckd->object_management_nk * $total_ccc;
        $excelValueE16 = $propertiescheckd->depreciation_nk_money;
        $excelValueE14 = $excelValueE7 - $excelValueE10 - $excelValueE11 - $excelValueE12;
        $excelValueE18 = $excelValueE14 - $excelValueE16;

        $excelValueD49 = $propertiescheckd->bank_loan * $excelFileCellD42;
        $excelValueE20 = $excelValueD49 * $propertiescheckd->interest_bank_loan;

        $excelValueE21 = $excelFileCellD48 * $propertiescheckd->interest_bond;

        $excelValueE23 = $excelValueE18 - $excelValueE20 - $excelValueE21;

        $excelValueE25 = $propertiescheckd->tax * $excelValueE23;

        $excelValueE27 = $excelValueE23 - $excelValueE25;

        $excelValueE29 = $excelValueD49 * $propertiescheckd->eradication_bank;

        $excelValueE31 = $excelValueE27 - $excelValueE29 + $excelValueE16;

        $excelValueH47 = $excelValueD49 - $excelValueE29;
        $excelValueI48 = $excelValueH47 * $propertiescheckd->interest_bank_loan;
        $excelValueI49 = ($excelValueE29 + $excelValueE20) - $excelValueI48;

        $excelValueH49 = $excelValueD49 * $propertiescheckd->eradication_bank;

        $excelValueD47 = $propertiescheckd->with_real_ek*$excelFileCellD42;
        $excelValueD48 = $properties->from_bond*$excelFileCellD42;
        $excelValueD49 = $properties->bank_loan*$excelFileCellD42;
        $excelValue50 = $excelValueD47+$excelValueD48+$excelValueD49;
        // echo $properties->gesamt_in_eur;
        // echo $excelValueH49; die;
        $userFormData = UserFormData::where('property_id', $id)->get();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('pdf.tenant', compact('excelValue50','excelValueE29', 'excelValueI49', 'excelValueE31', 'excelFileCellD48', 'excelFileCellG59', 'excelValueH49', 'excelFileCellD42', 'total_ccc', 'verkauf_tab', 'properties', 'send_email_template2_sending_info', 'properties1', 'bank_popup_data', 'list', 'id', 'banks', 'tab', 'tenants', 'schl_banks', 'tenancy_schedule_data', 'fake_bank', 'bank_all', 'propertiesExtra1s', 'properties_pdf', 'comments', 'countries_ad', 'mietflachen', 'ads', 'previous_states', 'previous_cities', 'insurances', 'property_service_providers', 'maintenance', 'investation', 'tr_users', 'sale_data', 'ps_data', 'buy_data', 'pb_data', 'as_users', 'ist_soll_properties', 'api_atachment_imgs', 'email_template', 'email_template_users', 'email_template2', 'getuploaditemlist', 'vads', 'vprevious_states', 'propertiescheckd', 'vprevious_cities', 'sh', 'bankers_email_info', 'ccomments', 'users', 'exportads', 'eprevious_states', 'eprevious_cities', 'contact_queries', 'userFormData'))->setPaper(array(0,0,2000,1200));
        $pdf->save(storage_path().'_filename.pdf');
        return $pdf->download('tenant.pdf');
    }

public function expose_genrate_pdf(Request $request){

    $data = $request->all();
    \Session::put('pdf_data', $data);

    return "success";

}

public function expose_download_pdf($id){
    // $data  = \Session::get('pdf_data');

    $data = ExportAd::where('property_id',$id)->first()->toArray();

    if($data['images'])
    {
        $data['img'] = explode(',', $data['images']);
    }
    // print "<pre>";
    // print_r($data); die;

    $pdf = PDF::loadView('pdf.index', compact('data'));
    $slug = rand();
    $filename = Auth::user()->id.$slug.'.pdf';


    if(isset($justsave)){
        // this variable not found so it will not call at now.
        $path = public_path().'/properties_pdf/'.$filename;
        $pdf->save($path);
        return Redirect::back();
    }
    return $pdf->download($filename);

}


    public function saveleaseactivity(Request $request)
    {
        DB::table('leasing_activity')->where('property_id', $request->property_id)->update(['deleted'=>1]);

        if(isset($_POST['tenant']) && $_POST['tenant'])
        {
            $names = $_POST['tenant'];
            $arr = array();
            foreach ($names as $key => $value) {

                $d = LeasingActivity::where('property_id', $request->property_id)->where('deleted',1)->first();

                if($d)
                {
                    $d->comment_1 = $_POST['comment_1'][$key];
                    $d->comment_2 = $_POST['comment_2'][$key];
                    $d->tenant = $_POST['tenant'][$key];
                    $d->type = $_POST['type'][$key];
                    $d->deleted = 0;
                    $d->save();
                }
                else{
                    $arr[0]['property_id'] = $request->property_id;
                    $arr[0]['comment_1'] = $_POST['comment_1'][$key];
                    $arr[0]['comment_2'] = $_POST['comment_2'][$key];
                    $arr[0]['tenant'] = $_POST['tenant'][$key];
                    $arr[0]['type'] = $_POST['type'][$key];
                    $arr[0]['created_at'] = date('Y-m-d H:i:s');
                    $arr[0]['updated_at'] = date('Y-m-d H:i:s');
                    $arr[0]['deleted'] = 0;
                    LeasingActivity::insert($arr);
                }

            }

        }
        // DB::table('leasing_activity')->where('property_id', $request->property_id)->where('deleted',1)->delete();
        echo "1";
    }
    public function updatelease(Request $request)
    {
        $d = "";
        if(isset($_POST['data1']) && $_POST['data1'])
        {
            $d = LeasingActivity::where('id', $request->id)->first();
            if($d)
            {
                $d->comment_1 = $_POST['data1'];
                $d->comment_2 = $_POST['data4'];
                $d->tenant = $_POST['data2'];
                $d->type = $_POST['data3'];
                $d->deleted = 0;
                $d->save();
            }
        }


        $tenant_name = array();
        $v_data = PropertyVacant::where('property_id',$request->property_id)->get();
        foreach($v_data as $item){
            if($item->type == config('tenancy_schedule.item_type.business_vacancy') || $item->type == config('tenancy_schedule.item_type.live_vacancy'))
            {
                $tenant_name[] = $item->name;
                $tenant_name_id[$item->tenant_id] = $item->name;
            }
        }
        return view('properties.lease_row', compact('d','tenant_name'));
    }

    public function saveEmpfehlungFile(Request $request){
        $response = [
            'success' => false,
            'msg' => 'File upload fail.'
        ];
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $originalExt = $file->getClientOriginalExtension();
            $uniqueName = $originalName;
            $dirname = $request->dirname;
            $basename = $request->basename;

            $contents = collect(Storage::cloud()->listContents($dirname, false));
            $folder = $contents->where('basename', $basename)->first();
            if($folder['basename'] == ''){
                $response = [
                    'success' => false,
                    'msg' => 'File upload fail.'
                ];
                return response()->json($response);
            }
            $fileData = File::get($file);
            $file2 =  Storage::cloud()->put($folder['path'].'/'.$uniqueName.'.'.$originalExt, $fileData);
            $contents = collect(Storage::cloud()->listContents($folder['path'], false));
            $file = $contents ->where('type','file')->where('filename', $uniqueName)->where('extension', $originalExt) ->first();
            if($file['basename'] == ""){
                $response = [
                    'success' => false,
                    'msg' => 'File upload fail.'
                ];
                return response()->json($response);
            }
            $dirtype = 'empfehlung';
            if($request->dirtype)
                $dirtype = $request->dirtype;

            GdriveUploadFiles::create([
                'property_id' => $request->property_id,
                'parent_id' => $request->empfehlung_id,
                'parent_type' => $dirtype,
                'file_name' => $file['name'],
                'file_basename' => $file['basename'],
                'file_path' => $file['path'],
                'file_href' => 'https://drive.google.com/file/d/'.$file['basename'],
                'extension' => $file['extension']
            ]);
            $response = [
                'success' => true,
                'msg' => 'File upload success.'
            ];

        }else{
            $id = $request->property_id;
            $dirname = $request->dirname;
            $basename = $request->basename;
            $contents = collect(Storage::cloud()->listContents($dirname, false));
            $file = $contents->where('basename', $basename)->first();
            if($file['basename'] == ''){
                $response = [
                    'success' => false,
                    'msg' => 'Fail to add selected file.'
                ];
            }else{
                  $dirtype = 'empfehlung';
            if($request->dirtype)
                $dirtype = $request->dirtype;

                $insertData = [
                    'property_id' => $request->property_id,
                    'parent_id' => $request->empfehlung_id,
                    'parent_type' => $dirtype,
                    'file_name' => $file['name'],
                    'file_basename' => $file['basename'],
                    'file_path' => $file['path'],
                    'file_href' => '',
                    'extension' => ''
                ];
                $responceMessage = '';
                $element = '';
                if($request->dirOrFile == 'file'){
                    $insertData['file_href'] = 'https://drive.google.com/file/d/'.$file['basename'];
                    $insertData['extension'] = $file['extension'];
                    $insertData['file_type'] = 'file';
                    $responceMessage = 'File selection succesfully.';
                    $element = "<a href='https://drive.google.com/file/d/".$file['basename']."'  target='_blank' title='".$file['name']."'><i class='fa ".(config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file') ."' ></i></a>";
                } else{
                    $insertData['file_type'] = 'dir';
                    $clickFunction = 'loadDirectoryFiles(\''.$file["path"].'\');';
                    $element = '<a href="javascript:void(0);" onClick="'.$clickFunction.'"  title="'.$file['name'].'"><i class="fa fa-folder" ></i></a>';
                    $responceMessage = 'Directory selection succesfully.';


                }
                GdriveUploadFiles::create($insertData);
                $response = [
                    'success' => true,
                    'msg' => $responceMessage,
                    'file' => $file,
                    'element' => $element
                ];
            }
        }
        return response()->json($response);
    }

    public function saveMieterlisteFile(Request $request){

        $response = [
            'success' => false,
            'msg' => 'File upload fail.'
        ];

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $originalExt = $file->getClientOriginalExtension();
            $uniqueName = $originalName;
            $dirname = $request->dirname;
            $basename = $request->basename;

            $contents = collect(Storage::cloud()->listContents($dirname, false));
            $folder = $contents->where('basename', $basename)->first();
            if($folder['basename'] == ''){
                $response = [
                    'success' => false,
                    'msg' => 'File upload fail.'
                ];
                return response()->json($response);
            }
            $fileData = File::get($file);
            $file2 =  Storage::cloud()->put($folder['path'].'/'.$uniqueName.'.'.$originalExt, $fileData);
            $contents = collect(Storage::cloud()->listContents($folder['path'], false));
            $file = $contents ->where('type','file')->where('filename', $uniqueName)->where('extension', $originalExt) ->first();
            if($file['basename'] == ""){
                $response = [
                    'success' => false,
                    'msg' => 'File upload fail.'
                ];
                return response()->json($response);
            }
            $dirtype = 'Betriebskostenabrechnung';
            if($request->dirtype)
                $dirtype = $request->dirtype;

            $file_data = [
                'file_name' => $file['name'],
                'file_basename' => $file['basename'],
                'file_dirname' => $file['dirname'],
                'file_type' => $file['type']
            ];

        }else{
            $id = $request->property_id;
            $dirname = $request->dirname;
            $basename = $request->basename;
            $contents = collect(Storage::cloud()->listContents($dirname, false));
            $file = $contents->where('basename', $basename)->first();
            if($file['basename'] == ''){
                $response = [
                    'success' => false,
                    'msg' => 'Fail to add selected file.'
                ];
                return response()->json($response);
            }else{
                $dirtype = 'Betriebskostenabrechnung';
                if($request->dirtype)
                    $dirtype = $request->dirtype;

                $file_data = [
                    'file_name' => $file['name'],
                    'file_basename' => $file['basename'],
                    'file_dirname' => $file['dirname'],
                    'file_type' => $file['type']
                ];
            }
        }

        if($file_data){

                $modal = TenantPayment::where('tenancy_schedule_item_id', $request->item_id)->where('year', $request->year)->first();

                if($modal){
                    $modal->file_name = $file['name'];
                    $modal->file_basename = $request->basename;
                    $modal->file_dirname = $request->dirname;
                    $modal->file_type = $request->dirOrFile;
                    $modal->save();
                    // $modal->update($file_data);
                }else{
                    // $file_data['tenancy_schedule_item_id'] = $request->item_id;
                    // $file_data['year'] = $request->year;

                    $modal = new TenantPayment;
                    $modal->tenancy_schedule_item_id = $request->item_id;
                    $modal->year = $request->year;
                    $modal->file_name = $file['name'];
                    $modal->file_basename = $request->basename;
                    $modal->file_dirname = $request->dirname;
                    $modal->file_type = $request->dirOrFile;
                    $modal->save();

                    // TenantPayment::create($file_data);
                }

                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($request->basename) ? $request->basename : '');
                if($request->dirOrFile == 'file'){
                    $download_path = "https://drive.google.com/file/d/".(isset($request->basename) ? $request->basename : '');

                    $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($request->basename) ? $request->basename : '').'&file_dir='.(isset($request->dirname) ? $request->dirname : '');
                }

                $html = '
                    <a href="'.$download_path.'"  target="_blank" title="'.$file['name'].'"><i class="fa fa-file" ></i></a>

                    <a class="delete_tenant_payment_file" data-url="'. route('delete_tenant_payment_file', ['id' => $modal->id]) .'"><i class="fa fa-trash"></i></a>
                ';

                $response = [
                    'file'=>$file_data,
                    'success' => true,
                    'msg' => 'File upload successfully.',
                    'html' => $html
                ];
        }

        return response()->json($response);
    }


    public function saveractivity(Request $request)
    {
        // DB::table('empfehlung')->where('property_id', $request->property_id)->where('tenant_id', $request->tenant_id)->update(['deleted'=>1]);

        if(isset($_POST['name']) && $_POST['name'])
        {
            $names = $_POST['name'];
            $arr = array();
            foreach ($names as $key => $value) {
                $_POST['amount'][$key] = str_replace('.', '', $_POST['amount'][$key]);
                $_POST['amount'][$key] = str_replace(',', '.', $_POST['amount'][$key]);

                if(isset($request->empfehlung_id[$key]) && $request->empfehlung_id[$key] != 0){
                    $d = Empfehlung::where('property_id', $request->property_id)->where('tenant_id', $request->tenant_id)->where('id',$request->empfehlung_id[$key])->first();
                    if($d)
                    {
                        $d->name = $_POST['name'][$key];
                        $d->amount = $_POST['amount'][$key];
                        $d->save();
                    }else{
                        $arr[0]['property_id'] = $request->property_id;
                        $arr[0]['tenant_id'] = $request->tenant_id;
                        $arr[0]['name'] = $_POST['name'][$key];
                        $arr[0]['amount'] = $_POST['amount'][$key];
                        Empfehlung::insert($arr);
                    }
                }else{
                    $arr[0]['property_id'] = $request->property_id;
                    $arr[0]['tenant_id'] = $request->tenant_id;
                    $arr[0]['name'] = $_POST['name'][$key];
                    $arr[0]['amount'] = $_POST['amount'][$key];
                    Empfehlung::insert($arr);
                }

            }

        }
        // DB::table('empfehlung')->where('property_id', $request->property_id)->where('tenant_id', $request->tenant_id)->where('deleted',1)->delete();
        echo "1";
    }

    public function sendmail_to_am(Request $request){

        if($request->isMethod('post')) {
            $rules = array('subject' => 'required', 'message' => 'required|min:2');
            $inputs = array(
                'subject' => $request->subject,
                'message' => $request->message
            );
            $validator = Validator::make($inputs, $rules);
            if($validator->fails()){
                return redirect()->back()->with('error', $validator->errors()->first());
            }else{
                $type = (isset($request->type) && $request->type != '') ? $request->type : 0;
                $res = $this->sendMailToPropertyUSer($request->property_id, $type, $request->message);

                if($res){
                    return redirect()->back()->with('success', 'Mail send succesfully.');
                }else{
                    return redirect()->back()->with('error', 'Mail send fail!');
                }
            }

        }else{
            return redirect()->back()->with('error', 'Invalid Request!');
        }
    }

    public function sendmail_to_all(Request $request){

        if($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                "property_id"   => "required",
                "message"       => "required|min:5",
                "type.*"       => "required",
            ]);


            if($validator->fails()){
                return redirect()->back()->with('error', $validator->errors()->first());
            }else{
                foreach ($request->type as $type) {
                    $this->sendMailToPropertyUSer($request->property_id, $type, $request->message);
                }
                return redirect()->back()->with('success', 'Mail send succesfully.');
            }

        }else{
            return redirect()->back()->with('error', 'Invalid Request!');
        }
    }

    public function sendMailToPropertyUSer($property_id, $type, $message){
        $property = DB::table('properties as p')->selectRaw('u.id as user_id, u.name, u.email, p.name_of_property');
        if($type == 1){
            $property = $property->join('users as u', 'u.id', '=', 'p.hvbu_id');
        }elseif($type == 2){
            $property = $property->join('users as u', 'u.id', '=', 'p.hvpm_id');
        }elseif($type == 3){
            $property = $property->join('users as u', 'u.id', '=', 'p.transaction_m_id');
        }elseif($type == 7){
            $property = $property->join('users as u', 'u.id', '=', 'p.steuerberater');
        }else{
            $property = $property->join('users as u', 'u.id', '=', 'p.asset_m_id');
        }
        $property = $property->where('p.id', $property_id)->first();

        if($property){

            $userEmail = ($property->email) ? trim($property->email) : '';

            if($userEmail != ''){

                $text = $message;
                $subject = $property->name_of_property;
                $email = $userEmail;
                $reply_email = Auth::user()->email;
                $reply_name = Auth::user()->name;

                if( !in_array($email, $this->mail_not_send()) ){
                    try {
                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$reply_email, $reply_name) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($reply_email, $reply_name)
                            ->subject($subject);
                        });
                    } catch (\Exception $e) {
                        return false;
                    }
                }

                $modal = new SendMailToAm;
                $modal->property_id = $property_id;
                $modal->user_id = Auth::user()->id;
                $modal->am_id = $property->user_id;
                $modal->subject = $subject;
                $modal->message = $message;
                $modal->type = $type;
                $modal->mail_type = 'PROPERTY DETAIL';

                if($modal->save()){
                    return true;
                }
            }
        }

        return false;
    }

    public function addContract(Request $request){


        if($request->ajax()) {

            $id = (isset($request->id)) ? $request->id : '';

            if($id){
                $rules = array('amount' => 'required', 'date' => 'required|date|date_format:d.m.Y');
            }else{
                $rules = array('file_basename' => 'required', 'comment' => 'required', 'amount' => 'required', 'date' => 'required|date|date_format:d.m.Y');
                if($request->hasFile('contracts_gdrive_file_upload')){
                    $rules = array('comment' => 'required', 'amount' => 'required', 'date' => 'required|date|date_format:d.m.Y');
                }
            }

            $user = Auth::user();

            $inputs = array(
                'file_basename' => $request->file_basename,
                'file_dirname' => $request->file_dirname,
                'file_type' => $request->file_type,
                'file_name' => $request->file_name,
                'comment' => trim($request->comment),
                'amount' => trim($request->amount),
                'date' => trim($request->date),
            );
            $validator = Validator::make($inputs, $rules);
            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

                if($id){
                    $modal = PropertyContracts::find($id);
                }else{
                    $modal = new PropertyContracts;
                    $modal->property_id = $request->property_id;
                    $modal->user_id = $user->id;
                    $modal->comment = trim($request->comment);
                }
                $modal->date = date('Y-m-d', strtotime(str_replace(".", "-", $request->date)));
                if($request->termination_date && $request->termination_date != ''){
                    $modal->termination_date = date('Y-m-d', strtotime(str_replace(".", "-", $request->termination_date)));
                }
                if($request->termination_am && $request->termination_am != ''){
                    $modal->termination_am = date('Y-m-d', strtotime(str_replace(".", "-", $request->termination_am)));
                }
                if($request->completion_date && $request->completion_date != ''){
                    $modal->completion_date = date('Y-m-d', strtotime(str_replace(".", "-", $request->completion_date)));
                }
                $amount = 0;
                if($request->amount)
                {
                    $amount = str_replace('.', '', $request->amount);
                    $amount = str_replace(',', '.', $amount);
                }
                $modal->amount = $amount;
                $modal->category = $request->category;
                $modal->is_old = ($request->is_old) ? 1 : 0;

                if(!$id){

                    if($request->hasFile('contracts_gdrive_file_upload')){

                        $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
                        if(!$propertyMainDir){
                            $res = PropertiesHelperClass::createPropertyDirectories($request->property_id);
                            if(!$res['status']){
                                $result = ['status' => false, 'message' => 'Property Verträge directory not created in google Drive.', 'data' => []];
                                return response()->json($result);
                            }
                            $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
                        }
                        $propertyContractDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Verträge')->first();
                        if(!$propertyContractDir){
                            $res = PropertiesHelperClass::createPropertySubDirectories($propertyMainDir, $request->property_id, ['Verträge']);
                            if(!$res['status']){
                                $result = ['status' => false, 'message' => 'Property Verträge directory not created in google Drive.', 'data' => []];
                                return response()->json($result);
                            }
                            $propertyContractDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Verträge')->first();
                        }


                        $file = $request->file('contracts_gdrive_file_upload');
                        $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                        $originalExt = $file->getClientOriginalExtension();
                        $folder = $propertyContractDir->dir_path;
                        $fileData = File::get($file);

                        //upload file
                        $file2 =  Storage::cloud()->put($folder.'/'.$originalName.'.'.$originalExt, $fileData);

                        //get google drive id
                        $contents = collect(Storage::cloud()->listContents($folder, false));
                        $file = $contents->where('type','file')->where('filename', $originalName)->where('extension', $originalExt) ->first();
                        if($file['basename'] == ""){
                            $result = ['status' => false, 'message' => 'File upload fail.', 'data' => []];
                            return response()->json($result);
                        }

                        $modal->invoice = $file['name'];
                        $modal->file_basename = $file['basename'];
                        $modal->file_dirname = $file['dirname'];
                        $modal->file_type = $file['type'];


                    }else{
                        $modal->invoice = $request->file_name;
                        $modal->file_basename = $request->file_basename;
                        $modal->file_dirname = $request->file_dirname;
                        $modal->file_type = $request->file_type;
                    }
                }

                if($modal->save()){

                    // Save comment
                    if($request->comment){
                        $commentModal = new PropertiesComment;
                        $commentModal->property_id = $request->property_id;
                        $commentModal->user_id = $user->id;
                        $commentModal->comment = $request->comment;
                        $commentModal->record_id = $modal->id;
                        $commentModal->type = 'property_contracts';
                        $commentModal->save();
                    }

                    if(!$id && $user->role >= 6){

                        $glink = "https://drive.google.com/drive/u/2/folders/".$modal->file_basename;
                        if($modal->file_type == "file"){
                            $glink = "https://drive.google.com/file/d/".$modal->file_basename;
                        }
                        $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                        $p = Properties::find($request->property_id);

                        $url = route('properties.show',['property'=>$request->property_id]).'?tab=property_invoice';
                        $url = '<a href="'.$url.'">'.$url.'</a>';

                        $subject = "Vertragsfreigabe";
                        $text = 'Ein neuer Vertrag wurde für das Objekt '.$p->plz_ort.' '.$p->ort." hochgeladen und wartet auf Freigabe: ".$url;

                        if($modal->is_old){
                            $subject = "Neuer Altvertrag";
                            $text = 'Ein neuer Altvertrag wurde für das Objekt '.$p->plz_ort.' '.$p->ort." hochgeladen: ".$url;
                        }
                        $text .= "<br><br>Link: ".$glink;

                        if($request->comment)
                            $text .= "<br><br>Kommentar: ".$request->comment;

                        $name = $name2 = $email = $email2 = "";
                        if(isset($p->asset_manager) && $p->asset_manager){
                            $name = $p->asset_manager->name;
                            $email = $p->asset_manager->email;
                        }

                        if(in_array($p->id, $this->propertiesids())){
                            $p21 = $this->getdata21();
                            $name = $p21['name'];
                            $email = $p21['email'];
                        }

                        if(isset($p->asset_manager_two) && $p->asset_manager_two){
                            $name2 = $p->asset_manager_two->name;
                            $email2 = $p->asset_manager_two->email;
                        }

                        if($email && !in_array($email, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject) {
                                $message->to($email)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->subject($subject);
                            });
                        }

                        if( $email2 && $email2 != $email && !in_array($email2, $this->mail_not_send()) ){

                             Mail::send('emails.general', ['text' => $text], function ($message) use($email2,$subject) {
                                $message->to($email2)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->subject($subject);
                            });
                        }
                    }

                    $result = ['status' => true, 'message' => 'Property contract save successfully.', 'data' => []];

                }else{
                    $result = ['status' => false, 'message' => 'Property contract save fail! Try again.', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getContractById($id){
        $res = PropertyContracts::where('id', $id)->first();
        if($res){

            $res->amount = ($res->amount) ? show_number($res->amount, 2) : 0;
            $res->date = ($res->date) ? show_date_format($res->date) : '';
            $res->completion_date = ($res->completion_date) ? show_date_format($res->completion_date) : '';
            $res->termination_date = ($res->termination_date) ? show_date_format($res->termination_date) : '';
            $res->termination_am = ($res->termination_am) ? show_date_format($res->termination_am) : '';

            $result = ['status' => true, 'message' => 'Record found successfully.', 'data' => $res];
        }else{
            $result = ['status' => false, 'message' => 'Record not found!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getContract($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role==9)
            $condition = 'pc.user_id='.$login_user->id;


        $res = DB::table('property_contracts as pc')
                    ->select('pc.*','u.name', 'p.name_of_property')
                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                    ->join('users as u', 'u.id', '=', 'pc.user_id')
                    ->where('pc.property_id', $property_id)
                    ->where('pc.not_release_status', 0)
                    ->where('pc.is_old', 0)
                    ->whereRaw($condition)
                    ->get();

        $data['data'] = [];
        $user = Auth::user();
        $mails_logs = PropertiesMailLog::where('property_id', $property_id)->where('tab','contracts')->get();

        $array = array();
        foreach ($mails_logs as $key => $value) {
            $array[$value->record_id][$value->type] = $value;
        }

        $taburl = route('properties.show',['property' => $property_id]).'?tab=contracts';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        if(!empty($res)){
            foreach ($res as $key => $value) {

                $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                            ->join('users as u', 'u.id', 'pc.user_id')
                            ->where('pc.record_id', $value->id)
                            ->where('pc.type', 'property_contracts')
                            ->orderBy('pc.created_at', 'desc')
                            ->limit(2)->get();
                $latest_comment = '';
                if($comments && count($comments)){
                    $latest_comment .= '<div class="show_property_cmnt_section">';
                    foreach ($comments as $n => $comment) {
                        $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                        $commented_user = $comment->name.''.$company;
                        $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                    }
                    $latest_comment .= '</div>';
                }
                if($latest_comment){
                    $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_contracts" class="load_property_comment_section" data-closest="td">Show More</a>';
                }

                $content = $taburl;

                $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_contracts" data-subject="Offene Verträge: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';


                $subject = 'Offene Verträge: '.$value->name_of_property;

                $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="OFFENE VERTRÄGE" data-section="property_contracts" data-reload="0">Weiterleiten an</button>';

                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }



                $release2 = 'btn btn-primary contract-release-request';

                $release2_type = "contract_request";
                $rbutton_title2 = "Zur Freigabe an Falk senden";

                $email = strtolower($user->email);

                $list = array();
                if(isset($array[$value->id]))
                    $list = $array[$value->id];

                $delete_button = '<button type="button" data-url="'. route('delete_contract', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-contract" data-type="open-contract"><i class="icon-trash"></i></button>';

                $edit_btn = '<button type="button" data-id="'. $value->id .'" data-url="'. route('getContractById', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm edit-contract"><i class="fa fa-edit"></i></button>';


                if($email==config('users.falk_email'))
                {
                    $rbutton_title2 = "Freigeben";
                    $release2 = 'btn btn-primary contract-release-request';
                    $release2_type = "contract_release";

                }
                else{
                    if($list && isset($list['contract_request']))
                    {
                      $release2 =  " btn-success contract-release-request";
                      $rbutton_title2 = "Zur Freigabe gesendet";
                    }
                }

                $r1 = $r2 = "";

                if($list && isset($list['contract_release']))
                {
                  $release2 =  " btn-success";
                  $rbutton_title2 = "Freigegeben";
                  $delete_button = $edit_btn = "";
                  continue;
                }

                ob_start();
                ?><button data-id="<?=$value->id?>" type="button" class="btn <?=$release2?>" data-column="<?=$release2_type?>"> <?=$rbutton_title2?></button>
                <?php


                $btn2 = ob_get_clean();

                $btn_mark_as_not_release = ($email == config('users.falk_email')) ? '<button data-id="'.$value->id.'" type="button" class="btn btn-primary contract-mark-as-not-release">Ablehnen</button>' : '';

                $btn_mark_as_not_release_am = ($email == config('users.falk_email')) ? '<button data-id="'.$value->id.'" type="button" class="btn btn-primary contract-mark-as-not-release-am">Ablehnen AM</button>' : '';

                $btn_mark_as_pending = ($email == config('users.falk_email')) ? '<button data-id="'.$value->id.'" type="button" class="btn btn-primary contract-mark-as-pending">Pending</button>' : '';

                $data['data'][] = [
                    ($key+1),
                    '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    show_number($value->amount,2),
                    show_date_format($value->date),
                    ($value->completion_date != '') ? show_date_format($value->completion_date) : '',
                    ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                    ($value->termination_am != '') ? show_date_format($value->termination_am) : '',
                    /*'<a href="#" class="inline-edit contract-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/contract/'.$value->id).'" >'.$value->comment.'</a>',*/
                    $latest_comment.'<br>'.$comment_button,
                    $value->name,
                    show_datetime_format($value->created_at),
                    $value->category,
                    /*'<a href="#" class="inline-edit contract-comment" data-type="textarea" data-pk="comment2" data-placement="bottom" data-url="'.url('property/update/contract/'.$value->id).'" >'.$value->comment2.'</a>',*/
                    $btn2,
                    $btn_mark_as_not_release_am,
                    $btn_mark_as_not_release,
                    $btn_mark_as_pending,
                    $edit_btn.' '.$delete_button,
                    $mail_button
                ];
            }
        }

        return response()->json($data);
    }

    public function getOldContracts($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role==9)
            $condition = 'pc.user_id='.$login_user->id;


        $res = DB::table('property_contracts as pc')
                    ->select('pc.*','u.name', 'p.name_of_property')
                    ->join('users as u', 'u.id', '=', 'pc.user_id')
                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                    ->where('pc.property_id', $property_id)
                    ->where('pc.not_release_status', 0)
                    ->whereRaw('(pc.is_old=1) and '.$condition)
                    ->get();

        $data['data'] = [];
        $user = Auth::user();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=contracts';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        if(!empty($res)){
            foreach ($res as $key => $value) {

                $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                            ->join('users as u', 'u.id', 'pc.user_id')
                            ->where('pc.record_id', $value->id)
                            ->where('pc.type', 'property_contracts')
                            ->orderBy('pc.created_at', 'desc')
                            ->limit(2)->get();
                $latest_comment = '';
                if($comments && count($comments)){
                    $latest_comment .= '<div class="show_property_cmnt_section">';
                    foreach ($comments as $n => $comment) {
                        $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                        $commented_user = $comment->name.''.$company;
                        $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                    }
                    $latest_comment .= '</div>';
                }
                if($latest_comment){
                    $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_contracts" class="load_property_comment_section" data-closest="td">Show More</a>';
                }

                $content = $taburl;

                $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_contracts" data-subject="Existierende Altverträge: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

                $subject = 'Existierende Altverträge: '.$value->name_of_property;

                $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="EXISTIERENDE ALTVERTRÄGE" data-section="property_contracts" data-reload="0">Weiterleiten an</button>';

                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }

                $delete_button = '<button type="button" data-url="'. route('delete_contract', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-contract" data-type="old_contract"><i class="icon-trash"></i></button>';

                $edit_btn = '<button type="button" data-id="'. $value->id .'" data-url="'. route('getContractById', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm edit-contract"><i class="fa fa-edit"></i></button>';

                $data['data'][] = [
                    ($key+1),
                    '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    show_number($value->amount,2),
                    show_date_format($value->date),
                    ($value->completion_date != '') ? show_date_format($value->completion_date) : '',
                    ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                    ($value->termination_am != '') ? show_date_format($value->termination_am) : '',
                    /*'<a href="#" class="inline-edit contract-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/contract/'.$value->id).'" >'.$value->comment.'</a>',*/
                    $latest_comment.'<br>'.$comment_button,
                    $value->name,
                    show_datetime_format($value->created_at),
                    $value->category,
                    $edit_btn.' '.$delete_button,
                    $mail_button
                ];
            }
        }
        $res = DB::table('property_contracts as pc')
                    ->select('pc.*','u.name', 'p.name_of_property')
                    ->join('users as u', 'u.id', '=', 'pc.user_id')
                    ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pc.id')
                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                    ->where('pc.property_id', $property_id)
                    ->where('pml.type', 'contract_release')
                    ->where('pml.tab', 'contracts')
                    ->whereRaw('pc.date<="'.date('Y-m-d').'" and '.$condition)
                    ->groupby('pc.id')
                    ->get();
        if(!empty($res)){
            foreach ($res as $key => $value) {

                $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                            ->join('users as u', 'u.id', 'pc.user_id')
                            ->where('pc.record_id', $value->id)
                            ->where('pc.type', 'property_contracts')
                            ->orderBy('pc.created_at', 'desc')
                            ->limit(2)->get();
                $latest_comment = '';
                if($comments && count($comments)){
                    $latest_comment .= '<div class="show_property_cmnt_section">';
                    foreach ($comments as $n => $comment) {
                        $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                        $commented_user = $comment->name.''.$company;
                        $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                    }
                    $latest_comment .= '</div>';
                }
                if($latest_comment){
                    $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_contracts" class="load_property_comment_section" data-closest="td">Show More</a>';
                }

                $content = $taburl;

                $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_contracts" data-subject="Existierende Altverträge: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

                $subject = 'Existierende Altverträge: '.$value->name_of_property;

                $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="EXISTIERENDE ALTVERTRÄGE" data-section="property_contracts" data-reload="0">Weiterleiten an</button>';

                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }

                $delete_button = '<button type="button" data-url="'. route('delete_contract', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-contract" data-type="old_contract"><i class="icon-trash"></i></button>';

                $edit_btn = '<button type="button" data-id="'. $value->id .'" data-url="'. route('getContractById', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm edit-contract"><i class="fa fa-edit"></i></button>';

                $data['data'][] = [
                    ($key+1),
                    '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    show_number($value->amount,2),
                    show_date_format($value->date),
                    ($value->completion_date != '') ? show_date_format($value->completion_date) : '',
                    ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                    ($value->termination_am != '') ? show_date_format($value->termination_am) : '',
                    /*'<a href="#" class="inline-edit contract-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/contract/'.$value->id).'" >'.$value->comment.'</a>',*/
                    $latest_comment.'<br>'.$comment_button,
                    $value->name,
                    show_datetime_format($value->created_at),
                    $value->category,
                    $edit_btn.' '.$delete_button,
                    $mail_button
                ];
            }
        }
        return response()->json($data);
    }

    public function getPendingContracts($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role==9)
            $condition = 'pc.user_id='.$login_user->id;


        $res = DB::table('property_contracts as pc')
                    ->select('pc.*','u.name', 'p.name_of_property')
                    ->join('users as u', 'u.id', '=', 'pc.user_id')
                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                    ->where('pc.property_id', $property_id)
                    ->where('pc.not_release_status', 2)
                    ->whereRaw($condition)
                    ->get();

        $user = Auth::user();
        $data['data'] = array();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=contracts';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_contracts')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_contracts" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_contracts" data-subject="Pending Verträge: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Pending Verträge: '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="PENDING VERTRÄGE" data-reload="0" data-section="property_contracts">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $data['data'][] = [
                ($key+1),
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_number($value->amount,2),
                show_date_format($value->date),
                ($value->completion_date != '') ? show_date_format($value->completion_date) : '',
                ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                ($value->termination_am != '') ? show_date_format($value->termination_am) : '',
                /*'<a href="#" class="inline-edit contract-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/contract/'.$value->id).'" >'.$value->comment.'</a>',*/
                $latest_comment.'<br>'.$comment_button,
                $value->name,
                show_datetime_format($value->created_at),
                $value->category,
                // $value->comment2,
                $mail_button
            ];

        }
        return response()->json($data);
    }

    public function getNotReleaseContracts($property_id){
        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role==9)
            $condition = 'pc.user_id='.$login_user->id;


        $res = DB::table('property_contracts as pc')
                    ->select('pc.*','u.name', 'p.name_of_property')
                    ->join('users as u', 'u.id', '=', 'pc.user_id')
                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                    ->where('pc.property_id', $property_id)
                    ->where('pc.not_release_status', 1)
                    ->whereRaw($condition)
                    ->get();

        $user = Auth::user();
        $data['data'] = array();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=contracts';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_contracts')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_contracts" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_contracts" data-subject="Nicht Freigegeben Verträge (Falk): '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Nicht Freigegeben Verträge (Falk): '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="NICHT FREIGEGEBEN VERTRÄGE" data-reload="0" data-section="property_contracts">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $data['data'][] = [
                ($key+1),
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_number($value->amount,2),
                show_date_format($value->date),
                ($value->completion_date != '') ? show_date_format($value->completion_date) : '',
                ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                ($value->termination_am != '') ? show_date_format($value->termination_am) : '',
                /*'<a href="#" class="inline-edit contract-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/contract/'.$value->id).'" >'.$value->comment.'</a>',*/
                $latest_comment.'<br>'.$comment_button,
                $value->name,
                show_datetime_format($value->created_at),
                $value->category,
                // $value->comment2,
                $mail_button
            ];

        }
        return response()->json($data);
    }

    public function getNotReleaseAMContracts($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role==9)
            $condition = 'pc.user_id='.$login_user->id;


        $res = DB::table('property_contracts as pc')
                    ->select('pc.*','u.name', 'p.name_of_property')
                    ->join('users as u', 'u.id', '=', 'pc.user_id')
                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                    ->where('pc.property_id', $property_id)
                    ->where('pc.not_release_status', 3)
                    ->whereRaw($condition)
                    ->get();

        $user = Auth::user();
        $data['data'] = array();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=contracts';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_contracts')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_contracts" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_contracts" data-subject="Nicht Freigegeben Verträge (AM): '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Nicht Freigegeben Verträge (AM): '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="NICHT FREIGEGEBEN VERTRÄGE" data-reload="0" data-section="property_contracts">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $data['data'][] = [
                ($key+1),
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_number($value->amount,2),
                show_date_format($value->date),
                ($value->completion_date != '') ? show_date_format($value->completion_date) : '',
                ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                ($value->termination_am != '') ? show_date_format($value->termination_am) : '',
                /*'<a href="#" class="inline-edit contract-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/contract/'.$value->id).'" >'.$value->comment.'</a>',*/
                $latest_comment.'<br>'.$comment_button,
                $value->name,
                show_datetime_format($value->created_at),
                $value->category,
                // $value->comment2,
                $mail_button
            ];

        }
        return response()->json($data);
    }

    public function getReleaseContracts($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role==9)
            $condition = 'pc.user_id='.$login_user->id;


        $res = DB::table('property_contracts as pc')
                    ->select('pc.*','u.name', 'p.name_of_property')
                    ->join('users as u', 'u.id', '=', 'pc.user_id')
                    ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pc.id')
                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                    ->where('pc.property_id', $property_id)
                    ->where('pml.type', 'contract_release')
                    ->where('pml.tab', 'contracts')
                    ->whereRaw('pc.date>="'.date('Y-m-d').'" and '.$condition)
                    ->groupby('pc.id')
                    ->get();

        $user = Auth::user();
        $data['data'] = array();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=contracts';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_contracts')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_contracts" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_contracts" data-subject="Freigegeben Verträge: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Freigegeben Verträge: '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="FREIGEGEBEN VERTRÄGE" data-reload="0" data-section="property_contracts">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $data['data'][] = [
                ($key+1),
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_number($value->amount,2),
                show_date_format($value->date),
                ($value->completion_date != '') ? show_date_format($value->completion_date) : '',
                ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                ($value->termination_am != '') ? show_date_format($value->termination_am) : '',
                /*'<a href="#" class="inline-edit contract-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/contract/'.$value->id).'" >'.$value->comment.'</a>',*/
                $latest_comment.'<br>'.$comment_button,
                $value->name,
                show_datetime_format($value->created_at),
                $value->category,
                // $value->comment2,
                $mail_button
            ];

        }
        return response()->json($data);
    }

    public function getContractLogs($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role==9)
            $condition = 'property_contracts.user_id='.$login_user->id;


        $mails_logs = PropertiesMailLog::selectRaw('properties_mail_logs.id, properties_mail_logs.user_id, users.name,properties_mail_logs.created_at,properties_mail_logs.comment,type,file_basename,file_type,invoice, termination_date')
        ->join('property_contracts','properties_mail_logs.record_id','=','property_contracts.id')
        ->join('users','users.id','=','properties_mail_logs.user_id')
        ->where('tab','contracts')
        ->where('property_contracts.property_id', $property_id)
        ->where('properties_mail_logs.property_id', $property_id)
        ->whereRaw($condition)
        ->get();

        $user = Auth::user();

        $data['data'] = [];

        if(!empty($mails_logs)){
            foreach ($mails_logs as $key => $value) {

                $selected_user_name = $value->name;
                $rbutton_title1 = "Zur Freigabe an Falk senden";


                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }



               $btn1  = "";
               if($value->type=='contract_request')
               {
                    $btn1 = $rbutton_title1;
                }
               if($value->type=='contract_release')
               {
                    $btn1 = "Freigegeben";
               }
               if($value->type=='contract_notrelease')
               {
                    $btn1 = "Ablehnen";
               }
               if($value->type=='contract_notrelease_am')
               {
                    $btn1 = "Ablehnen AM";
               }
               if($value->type=='contract_pending')
               {
                    $btn1 = "Pending";
               }

               $btn_delete = ($user->email == config('users.falk_email') && $value->user_id == $user->id) ? '<button type="button" data-id="'.$value->id.'" data-tbl="properties_mail_logs" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>' : '';

                $data['data'][] = [
                    $value->name,
                    $btn1,
                    '<a  target="_blank"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    $value->comment,
                    show_datetime_format($value->created_at),
                    ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                    $btn_delete
                ];
            }
        }

        return response()->json($data);
    }

    public function update_contract_by_field(Request $request, $contract_id) {

        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $contract = DB::table('property_contracts as pc')
                        ->selectRaw('u.name as user_name, u.email as user_email, am.name as am_name, am.email as am_email, p.name_of_property, pc.file_basename, pc.file_type')
                        ->join('properties as p', 'p.id', '=', 'pc.property_id')
                        ->join('users as u', 'u.id', '=', 'pc.user_id')
                        ->join('users as am', 'am.id', '=', 'p.asset_m_id')
                        ->where('pc.id', '=', $contract_id)
                        ->first();

        if (!$contract) {
            $response['msg'] = 'Record not found!';
            return response()->json($response);
        }

        $update_data = DB::table('property_contracts')->where('id', $contract_id)->update([$request->pk => $request->value]);
        if($update_data){

            if($request->pk == 'comment2' && $contract->user_email && $request->value){

                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($contract->file_basename) ? $contract->file_basename : '');
                if($contract->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($contract->file_basename) ? $contract->file_basename : '');
                }

                $subject = 'Verträge : '.$contract->name_of_property;
                $text = 'Kommentar: '.$request->value.'<br>Datei: '.$download_path;

                $email = $contract->user_email;
                $cc_email = $contract->am_email;
                $reply_email = config('users.falk_email');

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $cc_email, $reply_email) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, 'Falk')
                        ->cc($cc_email)
                        ->subject($subject);
                    });
                }

            }

            $response = [
                'success' => true,
                'msg' => 'Record update successfully.'
            ];
        }

        return response()->json($response);
    }

    public function deleteContract($id){
        $model = PropertyContracts::find($id);
        // $file= public_path(). "/invoice_upload/".$model->invoice;
        // File::delete($file);
        if($model->delete()){
            $result = ['status' => true, 'message' => 'Contract delete successfully.', 'data' => []];
        }else{
            $result = ['status' => false, 'message' => 'Contract delete fail! Try again.', 'data' => []];
        }
        return response()->json($result);
    }

    public function contractMarkAsNotReleased(Request $request){
        $update_data = DB::table('property_contracts')->where('id', $request->id)->update(['not_release_status' => 1,'comment2' => $request->comment]);
        if($update_data){

            $contract = PropertyContracts::where('id',$request->id)->first();
            $modal = new PropertiesMailLog;
            $modal->property_id = $contract->property_id;
            $modal->record_id = $request->id;
            $modal->type = 'contract_notrelease';
            $modal->tab = 'contracts';
            if($request->comment)
                  $modal->comment = $request->comment;
            $modal->user_id = Auth::user()->id;
            $modal->save();

            $data = DB::table('property_contracts as pc')
                    ->select('pc.*','u.email','u.name','p.name_of_property', 'p.plz_ort', 'p.ort')
                    ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pc.id')
                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                    ->join('users as u', 'u.id', '=', 'pml.user_id')
                    ->where('pc.id',$request->id)
                    ->where('pml.tab','contracts')
                    ->where('pml.type','contract_request')
                    ->groupBy('pml.user_id')
                    ->get();

            if(!empty($data)){
                foreach ($data as $key => $contract) {

                    $glink = "https://drive.google.com/drive/u/2/folders/".$contract->file_basename;
                    if($contract->file_type == "file"){
                        $glink = "https://drive.google.com/file/d/".$contract->file_basename;
                    }
                    $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                    $url = route('properties.show',['property'=>$contract->property_id]).'?tab=contracts';
                    $url = '<a href="'.$url.'">'.$url.'</a>';

                    $text = "Hallo ".$contract->name.",<br> dein Vertrag ".$glink." wurde von Falk nicht freigegeben. Link zum Intranet: ".$url;
                    $subject = "Vertrag vom ".show_date_format($contract->created_at).' für '.$contract->plz_ort.' '.$contract->ort." wurde nicht freigegeben";

                    $replyemailaddress = config('users.falk_email');
                    $replyname = "Falk";
                    $email = $contract->email;
                    // $email = "yiicakephp@gmail.com";
                    if($request->comment)
                      $text .= "<br><br>Kommentar: ".$request->comment;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email,$replyemailaddress,$replyname,$contract) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemailaddress, $replyname)
                            ->subject($subject);
                        });
                    }

                }
            }

            $response = ['status' => true, 'message' => 'Mark as decline successfully.'];

        }else{
            $response = ['status' => false, 'message' => 'Mark as decline fail! Try again.'];
        }

        return response()->json($response);
    }

    public function contractMarkAsNotReleasedAm(Request $request){
        $update_data = DB::table('property_contracts')->where('id', $request->id)->update(['not_release_status' => 3,'comment2' => $request->comment]);
        if($update_data){

            $contract = DB::table('property_contracts as pc')
                        ->select('pc.id', 'pc.file_basename', 'pc.file_type', 'pc.property_id', 'pc.created_at','u.name', 'u.email', 'p.name_of_property')
                        ->join('users as u', 'u.id', '=', 'pc.user_id')
                        ->join('properties as p', 'p.id', '=', 'pc.property_id')
                        ->where('pc.id', $request->id)
                        ->first();
            if($contract){

                // Add mail log
                $modal = new PropertiesMailLog;
                $modal->property_id = $contract->property_id;
                $modal->record_id = $request->id;
                $modal->type = 'contract_notrelease_am';
                $modal->tab = 'contracts';
                if($request->comment)
                      $modal->comment = $request->comment;
                $modal->user_id = Auth::user()->id;
                $modal->save();

                $user = Auth::user();

                $glink = "https://drive.google.com/drive/u/2/folders/".$contract->file_basename;
                if($contract->file_type == "file"){
                    $glink = "https://drive.google.com/file/d/".$contract->file_basename;
                }
                $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                $url = route('properties.show',['property'=>$contract->property_id]).'?tab=contracts';
                $url = '<a href="'.$url.'">'.$url.'</a>';

                $text = "Hallo ".$contract->name.",<br> dein Vertrag ".$glink." wurde von ".$user->name." nicht freigegeben. Link zum Intranet: ".$url;
                if($request->comment){
                    $text .= "<br><br>Kommentar: ".$request->comment;
                }
                $subject = "Vertrag vom ".show_date_format($contract->created_at)." für ".$contract->name_of_property." wurde nicht freigegeben";

                $email = $contract->email;
                $name = $contract->name;
                $replyemailaddress = $user->email;
                $replyname = $user->name;

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email, $name, $replyemailaddress, $replyname, $contract) {
                        $message->to($email, $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemailaddress, $replyname)
                        ->subject($subject);
                    });
                }

                $p = Properties::find($contract->property_id);

                $aname = $asset_email = "";
                $am2_name = $am2_email = "";

                if(isset($p->asset_manager) && isset($p->asset_manager->name)){
                    $aname = $p->asset_manager->name;
                    $asset_email = $p->asset_manager->email;
                }

                if(in_array($p->id, $this->propertiesids())){
                    $p21 = $this->getdata21();
                    $aname = $p21['name'];
                    $asset_email = $p21['email'];
                }

                if(isset($p->asset_manager_two) && isset($p->asset_manager_two->name)){
                    $am2_name = $p->asset_manager_two->name;
                    $am2_email = $p->asset_manager_two->email;
                }

                $text = "Hallo ".$contract->name.",<br> dein Vertrag ".$glink." wurde von ".$user->name." nicht freigegeben. Link zum Intranet: ".$url;
                if($request->comment){
                    $text .= "<br><br>Kommentar: ".$request->comment;
                }
                $subject = "Vertrag vom ".show_date_format($contract->created_at)." für ".$contract->name_of_property." wurde nicht freigegeben";

                if($asset_email && $email != $asset_email && !in_array($asset_email, $this->mail_not_send()) ){//send asset_m_id

                    $email = $asset_email;
                    $name = $aname;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email, $name, $replyemailaddress, $replyname, $contract) {
                        $message->to($email, $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemailaddress, $replyname)
                        ->subject($subject);
                    });
                }

                if($am2_email && $am2_email != $email && $am2_email != $asset_email && !in_array($am2_email, $this->mail_not_send()) ){//send asset_m_id2

                    $email = $am2_email;
                    $name = $am2_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email, $name, $replyemailaddress, $replyname, $contract) {
                        $message->to($email, $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemailaddress, $replyname)
                        ->subject($subject);
                    });
                }

                $response = ['status' => true, 'message' => 'Contract mark as not release for AM successfully.'];

            }else{
               $response = ['status' => false, 'message' => 'Record not found!'];
            }

        }else{
            $response = ['status' => false, 'message' => 'Mark as decline fail! Try again.'];
        }
        return response()->json($response);
    }

    public function contractMarkAsPending(Request $request){
        $update_data = DB::table('property_contracts')->where('id', $request->id)->update(['not_release_status' => 2,'comment2' => $request->comment]);
        if($update_data){

            $contract = PropertyContracts::where('id',$request->id)->first();
            $modal = new PropertiesMailLog;
            $modal->property_id = $contract->property_id;
            $modal->record_id = $request->id;
            $modal->type = 'contract_pending';
            $modal->tab = 'contracts';
            if($request->comment)
                  $modal->comment = $request->comment;
            $modal->user_id = Auth::user()->id;
            $modal->save();

            $data = DB::table('property_contracts as pc')
                    ->select('pc.*','u.email','u.name','p.name_of_property', 'p.plz_ort', 'p.ort')
                    ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pc.id')
                    ->join('properties as p', 'p.id', '=', 'pc.property_id')
                    ->join('users as u', 'u.id', '=', 'pml.user_id')
                    ->where('pc.id',$request->id)
                    ->where('pml.tab','contracts')
                    ->where('pml.type','contract_request')
                    ->groupBy('pml.user_id')
                    ->get();

            if(!empty($data)){
                foreach ($data as $key => $contract) {
                    $url = route('properties.show',['property'=>$contract->property_id]).'?tab=contracts';
                    $url = '<a href="'.$url.'">'.$url.'</a>';

                    $glink = "https://drive.google.com/drive/u/2/folders/".$contract->file_basename;
                    if($contract->file_type == "file"){
                        $glink = "https://drive.google.com/file/d/".$contract->file_basename;
                    }
                    $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                    $text = "Hallo ".$contract->name.",<br> dein Vertrag ".$glink." wurde von Falk noch nicht freigegeben. Link zum Intranet: ".$url;
                    $subject = "Vertrag vom ".show_date_format($contract->created_at).' für '.$contract->plz_ort.' '.$contract->ort." wurde noch nicht freigegeben";

                    $replyemailaddress = config('users.falk_email');
                    $replyname = "Falk";
                    $email = $contract->email;
                    // $email = "yiicakephp@gmail.com";

                    if($contract->comment)
                        $text .= "<br><br>Kommentar: ".$contract->comment;


                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email,$replyemailaddress,$replyname,$contract) {
                              $message->to($email)
                              ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                              ->replyTo($replyemailaddress, $replyname)
                              ->subject($subject);
                        });
                    }

                }
            }

            $response = ['status' => true, 'message' => 'Mark as pending successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Mark as pending fail! Try again.'];
        }

        return response()->json($response);
    }

    public function contractreleaseprocedure(Request $request){

        $idsarr  = explode(',', $request->id);

        foreach ($idsarr as $key => $value) {
            $request->invoice_id = $value;
            if($request->step){
                $invoice = PropertyContracts::where('id',$request->invoice_id)->first();

                $id = $invoice->property_id;

                $url = route('properties.show',['property'=>$id]).'?tab=contracts';
                $url = '<a href="'.$url.'">'.$url.'</a>';


                $p = Properties::find($id);
                $user = Auth::user();
                $aname = $name = $user->name;
                $areplyemail = $replyemail = $user->email;
                $areplyemail = "";

                $a2replyemail = $a2name = '';

                if(isset($invoice->property_contact_user) && isset($invoice->property_contact_user->name)){
                    $name = $invoice->property_contact_user->name;
                    $replyemail = $invoice->property_contact_user->email;
                }
                if(isset($p->asset_manager) && isset($p->asset_manager->name)){
                    $aname = $p->asset_manager->name;
                    $areplyemail = $p->asset_manager->email;
                }
                if(in_array($p->id, $this->propertiesids()))
                {
                    $p21 = $this->getdata21();
                    $aname = $p21['name'];
                    $areplyemail = $p21['email'];
                }
                if(isset($p->asset_manager_two) && isset($p->asset_manager_two->name)){
                    $a2name = $p->asset_manager_two->name;
                    $a2replyemail = $p->asset_manager_two->email;
                }

                $check = PropertiesMailLog::where('type',$request->step)->where('record_id',$request->invoice_id)->where('tab','contracts')->first();
                if(!$check || $request->step == 'contract_request'){
                    $modal = new PropertiesMailLog;
                    $modal->property_id = $id;
                    $modal->record_id = $request->invoice_id;
                    $modal->type = $request->step;
                    $modal->tab = 'contracts';
                    if($request->comment)
                        $modal->comment = $request->comment;
                    $modal->user_id = Auth::user()->id;
                    $modal->save();
                }

                $glink = "https://drive.google.com/drive/u/2/folders/".$invoice->file_basename;
                if($invoice->file_type == "file"){
                    $glink = "https://drive.google.com/file/d/".$invoice->file_basename;
                }
                $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                $text = "";
                if(!$check){
                    if($request->step=="contract_release"){
                        $text = "Hallo ".$name.",<br> dein Vertrag ".$glink." wurde von Falk freigegeben. Link zum Intranet: ".$url;
                        $replyemailaddress = config('users.falk_email');
                        $replyname = 'Falk';
                    }
                    if($text){
                        $contents = collect(Storage::cloud()->listContents($invoice->file_dirname, false));
                        $gDriveFile = $contents->where('type','file')->where('basename', $invoice->file_basename)->first();

                        if($request->comment){
                            $text .= "<br><br>Kommentar: ".$request->comment;
                            $subject = "Vertrag vom ".show_date_format($invoice->created_at).' für '.$p->plz_ort.' '.$p->ort." wurde freigegeben";


                            $invoice->not_release_status = 0;
                            $invoice->save();

                            $cc_array = [];
                            if($areplyemail && $replyemail != $areplyemail){
                               $cc_array[] = $areplyemail;
                            }
                            if($a2replyemail && $a2replyemail != $replyemail && $a2replyemail != $areplyemail){
                                $cc_array[] = $a2replyemail;
                            }

                            if( !in_array($replyemail, $this->mail_not_send()) ){

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$replyemail,$replyemailaddress,$replyname,$invoice,$gDriveFile,$cc_array) {
                                    $message->to($replyemail)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->attachData(\Storage::cloud()->get($invoice->file_basename), $invoice->invoice, [
                                         'mime' => $gDriveFile['mimetype']
                                     ])
                                    ->replyTo($replyemailaddress, $replyname)
                                    ->subject($subject);

                                    if(!empty($cc_array))
                                        $message->cc($cc_array);
                                });
                            }
                        }
                    }
                }

                if($request->step=="contract_request"){

                    $text = "Hallo Falk,<br> ".$name." benötigt deine Freigabe bei dem Objekt ".$p->plz_ort.' '.$p->ort." für den folgenden Vertrag: ".$glink." . Hier kannst du den Vertrag freigeben: ".$url;


                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $email = config('users.falk_email');
                    $subject = "Vertragsfreigabe  ".$p->plz_ort.' '.$p->ort;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name,$invoice) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemail, $name)
                            ->subject($subject);
                        });
                    }
                }
            }
        }
        echo "1";
    }

    public function addPropertyDeal(Request $request){


        if($request->ajax()) {
            $rules = array('file_basename' => 'required', 'comment' => 'required', 'amount' => 'required', 'date' => 'required|date|date_format:d.m.Y');
            if($request->hasFile('property_deal_gdrive_file_upload')){
                $rules = array('comment' => 'required', 'amount' => 'required', 'date' => 'required|date|date_format:d.m.Y');
            }

            $inputs = array(
                'file_basename' => $request->file_basename,
                'file_dirname' => $request->file_dirname,
                'file_type' => $request->file_type,
                'file_name' => $request->file_name,
                'comment' => trim($request->comment),
                'amount' => trim($request->amount),
                'date' => trim($request->date),
            );
            $validator = Validator::make($inputs, $rules);
            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

                $modal = new PropertiesDeals;
                $modal->property_id = $request->property_id;
                $modal->user_id = Auth::user()->id;
                $modal->comment = trim($request->comment);
                $modal->date = date('Y-m-d', strtotime(str_replace(".", "-", $request->date)));
                if($request->termination_date && $request->termination_date != ''){
                    $modal->termination_date = date('Y-m-d', strtotime(str_replace(".", "-", $request->termination_date)));
                }
                $amount = 0;
                if($request->amount)
                {
                    $amount = str_replace('.', '', $request->amount);
                    $amount = str_replace(',', '.', $amount);
                }
                $modal->amount = $amount;

                if($request->hasFile('property_deal_gdrive_file_upload')){

                    $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
                    if(!$propertyMainDir){
                        $res = PropertiesHelperClass::createPropertyDirectories($request->property_id);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
                    }
                    $propertyDealDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Angebote')->first();
                    if(!$propertyDealDir){
                        $res = PropertiesHelperClass::createPropertySubDirectories($propertyMainDir, $request->property_id, ['Angebote']);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property Angebote directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyDealDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Angebote')->first();
                    }


                    $file = $request->file('property_deal_gdrive_file_upload');
                    $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $originalExt = $file->getClientOriginalExtension();
                    $folder = $propertyDealDir->dir_path;
                    $fileData = File::get($file);

                    //upload file
                    $file2 =  Storage::cloud()->put($folder.'/'.$originalName.'.'.$originalExt, $fileData);

                    //get google drive id
                    $contents = collect(Storage::cloud()->listContents($folder, false));
                    $file = $contents->where('type','file')->where('filename', $originalName)->where('extension', $originalExt) ->first();
                    if($file['basename'] == ""){
                        $result = ['status' => false, 'message' => 'File upload fail.', 'data' => []];
                        return response()->json($result);
                    }

                    $modal->invoice = $file['name'];
                    $modal->file_basename = $file['basename'];
                    $modal->file_dirname = $file['dirname'];
                    $modal->file_type = $file['type'];


                }else{
                    $modal->invoice = $request->file_name;
                    $modal->file_basename = $request->file_basename;
                    $modal->file_dirname = $request->file_dirname;
                    $modal->file_type = $request->file_type;
                }

                if($modal->save()){
                    $result = ['status' => true, 'message' => 'Property deal save successfully.', 'data' => []];
                }else{
                    $result = ['status' => false, 'message' => 'Property deal save fail! Try again.', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getPropertyDeals($property_id){

        $res = DB::table('properties_deals as pd')
                    ->select('pd.*','u.name')
                    ->join('users as u', 'u.id', '=', 'pd.user_id')
                    ->where('pd.property_id', $property_id)
                    ->where('pd.not_release_status', 0)
                    ->orderBy('pd.id', 'desc')
                    ->get();

        $data['data'] = [];
        $user = Auth::user();
        $mails_logs = PropertiesMailLog::where('property_id', $property_id)->where('tab','deals')->get();

        $array = array();
        foreach ($mails_logs as $key => $value) {
            $array[$value->record_id][$value->type] = $value;
        }



        if(!empty($res)){
            foreach ($res as $key => $value) {
                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }



                    $release2 = 'btn btn-primary deal-release-request';

                    $release2_type = "deal_request";
                    $rbutton_title2 = "Zur Freigabe an Falk senden";

                    $email = strtolower($user->email);

                    $list = array();
                    if(isset($array[$value->id]))
                        $list = $array[$value->id];

                    $delete_button = '<button type="button" data-url="'. route('delete_property_deal', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-deal"><i class="icon-trash"></i></button>';


                    if($email==config('users.falk_email'))
                    {
                        $rbutton_title2 = "Freigeben";
                        $release2 = 'btn btn-primary deal-release-request';
                        $release2_type = "deal_release";

                    }
                    else{
                        if($list && isset($list['deal_request']))
                        {
                          $release2 =  " btn-success deal-release-request";
                          $rbutton_title2 = "Zur Freigabe gesendet";
                        }
                    }

                    $r1 = $r2 = "";

                    if($list && isset($list['deal_release']))
                    {
                      $release2 =  " btn-success";
                      $rbutton_title2 = "Freigegeben";
                      $delete_button = "";
                    }

                ob_start();
                ?><button data-id="<?=$value->id?>" type="button" class="btn <?=$release2?>" data-column="<?=$release2_type?>"> <?=$rbutton_title2?></button>
                <?php


                $btn2 = ob_get_clean();

                $btn_mark_as_not_release = ($email == config('users.falk_email')) ? '<button data-id="'.$value->id.'" type="button" class="btn btn-primary deal-mark-as-not-release">Ablehnen</button>' : '';

                $btn_mark_as_pending = ($email == config('users.falk_email')) ? '<button data-id="'.$value->id.'" type="button" class="btn btn-primary deal-mark-as-pending">Pending</button>' : '';

                $data['data'][] = [
                    ($key+1),
                    '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download">
        <i class="fa fa-download fa-fw"></i>
    </a>',
                    show_number($value->amount,2),
                    show_date_format($value->date),
                    ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                    '<a href="#" class="inline-edit property-deal-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/deal/'.$value->id).'" >'.$value->comment.'</a>',
                    $value->name,
                    show_datetime_format($value->created_at),
                    '<a href="#" class="inline-edit property-deal-comment" data-type="textarea" data-pk="comment2" data-placement="bottom" data-url="'.url('property/update/deal/'.$value->id).'" >'.$value->comment2.'</a>',
                    $btn2,
                    $btn_mark_as_not_release,
                    $btn_mark_as_pending,
                    $delete_button

                ];
            }
        }

        return response()->json($data);
    }

    public function getPendingDeals($property_id){

        $res = DB::table('properties_deals as pd')
                    ->select('pd.*','u.name')
                    ->join('users as u', 'u.id', '=', 'pd.user_id')
                    ->where('pd.property_id', $property_id)
                    ->where('pd.not_release_status', 2)
                    ->get();

        $user = Auth::user();
        $data['data'] = array();

        foreach ($res as $key => $value) {
            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $data['data'][] = [
                ($key+1),
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download">
        <i class="fa fa-download fa-fw"></i>
    </a>',
                show_number($value->amount,2),
                show_date_format($value->date),
                ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                '<a href="#" class="inline-edit property-deal-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/deal/'.$value->id).'" >'.$value->comment.'</a>',
                $value->name,
                show_datetime_format($value->created_at),
                $value->comment2,
            ];

        }
        return response()->json($data);
    }

    public function getNotReleaseDeals($property_id){

        $res = DB::table('properties_deals as pd')
                    ->select('pd.*','u.name')
                    ->join('users as u', 'u.id', '=', 'pd.user_id')
                    ->where('pd.property_id', $property_id)
                    ->where('pd.not_release_status', 1)
                    ->get();

        $user = Auth::user();
        $data['data'] = array();

        foreach ($res as $key => $value) {
            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $data['data'][] = [
                ($key+1),
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_number($value->amount,2),
                show_date_format($value->date),
                ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                '<a href="#" class="inline-edit property-deal-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/deal/'.$value->id).'" >'.$value->comment.'</a>',
                $value->name,
                show_datetime_format($value->created_at),
                $value->comment2,
            ];

        }
        return response()->json($data);
    }

    public function deletePropertyDeal($id){
        $model = PropertiesDeals::find($id);
        // $file= public_path(). "/invoice_upload/".$model->invoice;
        // File::delete($file);
        if($model->delete()){
            $result = ['status' => true, 'message' => 'Deal delete successfully.', 'data' => []];
        }else{
            $result = ['status' => false, 'message' => 'Deal delete fail! Try again.', 'data' => []];
        }
        return response()->json($result);
    }

    public function dealMarkAsNotReleased(Request $request){
        $update_data = DB::table('property_insurance_tabs')->where('id', $request->id)->update(['not_release_status' => 1,'comment2' => $request->comment]);

        if($update_data){

            $deal = PropertyInsuranceTab::find($request->id);

            $modal = new PropertiesMailLog;
            $modal->property_id = $deal->property_id;
            $modal->record_id = $request->id;
            // $modal->selected_id = $request->detail_id;
            $modal->type = 'insurance_notrelease';
            $modal->tab = 'insurance_tab';
            if($request->comment)
                $modal->comment = $request->comment;
            $modal->user_id = Auth::user()->id;
            $modal->save();

            $data = DB::table('property_insurance_tabs as pd')
                    ->select('pd.*','u.email','u.name','p.name_of_property', 'p.plz_ort', 'p.ort')
                    ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pd.id')
                    ->join('properties as p', 'p.id', '=', 'pd.property_id')
                    ->join('users as u', 'u.id', '=', 'pml.user_id')
                    ->where('pd.id',$request->id)
                    ->where('pml.tab','insurance_tab')
                    ->where('pml.type','insurance_request')
                    ->groupBy('pml.user_id')
                    ->get();

              foreach ($data as $key => $deal) {

                    $deal1 = PropertyInsuranceTabDetails::where('property_insurance_tab_id',$deal->id)->where('asset_manager_status','1')->first();

                    $glink = "https://drive.google.com/drive/u/2/folders/".$deal1->file_basename;
                    if($deal1->file_type == "file"){
                        $glink = "https://drive.google.com/file/d/".$deal1->file_basename;
                    }
                    $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                    $url = route('properties.show',['property'=>$deal->property_id]).'?tab=property_deals';
                    $url = '<a href="'.$url.'">'.$url.'</a>';

                    // $text = "Hallo ".$deal->name.",<br> dein Angebot/Vertrag ".$glink." wurde von Falk nicht freigegeben. Link zum Intranet: ".$url;
                    // $text = "Hallo ".$deal->name.",<br> dein Angebot ".$glink." wurde von Falk nicht freigegeben. Link zum Intranet: ".$url;

                    $text = "Hallo ".$deal->name.",<br> dein Angebot ".$glink." wurde von Falk nicht freigegeben. Link zum Intranet: ".$url;
                    $subject = "Angebot vom ".show_date_format($deal->created_at).' für '.$deal->plz_ort.' '.$deal->ort." wurde nicht freigegeben";

                    $replyemailaddress = config('users.falk_email');
                    $replyname = "Falk";
                    $email = $deal->email;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email,$replyemailaddress,$replyname,$deal) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemailaddress, $replyname)
                            ->subject($subject);
                        });
                    }
              }

            $response = ['status' => true, 'message' => 'Mark as decline successfully.'];

        }else{
            $response = ['status' => false, 'message' => 'Mark as decline fail! Try again.'];
        }

        return response()->json($response);
    }


    public function insuranceMarkAsNotReleased(Request $request){
        // $update_data = DB::table('property_insurance_tab_details')->where('id', $request->id)->update(['not_release' => 1]);

        $update_data = PropertyInsuranceTabDetails::find($request->id);
        if($update_data){

            $deal1 = $update_data;

            $update_data->not_release = 1;
            $update_data->save();

            $deal = PropertyInsuranceTab::find($update_data->property_insurance_tab_id);

            $modal = new PropertiesMailLog;
            $modal->property_id = $deal->property_id;
            $modal->record_id = $update_data->property_insurance_tab_id;
            $modal->selected_id = $request->id;
            $modal->type = 'insurance_notrelease';
            $modal->tab = 'insurance_tab';
            if($request->comment)
                $modal->comment = $request->comment;
            $modal->user_id = Auth::user()->id;
            $modal->save();

            $data = DB::table('property_insurance_tabs as pd')
                    ->select('pd.*','u.email','u.name','p.name_of_property', 'p.plz_ort', 'p.ort')
                    ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pd.id')
                    ->join('properties as p', 'p.id', '=', 'pd.property_id')
                    ->join('users as u', 'u.id', '=', 'pml.user_id')
                    ->where('pd.id',$update_data->property_insurance_tab_id)
                    ->where('pml.tab','insurance_tab')
                    ->where('pml.type','insurance_request')
                    ->groupBy('pml.user_id')
                    ->get();

              foreach ($data as $key => $deal) {

                    // echo 1; die;
                    // $deal1 = PropertyInsuranceTabDetails::where('property_insurance_tab_id',$deal->id)->where('asset_manager_status','1')->first();

                    $glink = "https://drive.google.com/drive/u/2/folders/".$deal1->file_basename;
                    if($deal1->file_type == "file"){
                        $glink = "https://drive.google.com/file/d/".$deal1->file_basename;
                    }
                    $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                    $url = route('properties.show',['property'=>$deal->property_id]).'?tab=property_deals';
                    $url = '<a href="'.$url.'">'.$url.'</a>';

                    // $text = "Hallo ".$deal->name.",<br> dein Angebot/Vertrag ".$glink." wurde von Falk nicht freigegeben. Link zum Intranet: ".$url;
                    // $text = "Hallo ".$deal->name.",<br> dein Angebot ".$glink." wurde von Falk nicht freigegeben. Link zum Intranet: ".$url;

                    $text = "Hallo ".$deal->name.",<br> dein Angebot ".$glink." wurde von Falk nicht freigegeben. Link zum Intranet: ".$url;
                    $subject = "Angebot vom ".show_date_format($deal->created_at).' für '.$deal->plz_ort.' '.$deal->ort." wurde nicht freigegeben";

                    $replyemailaddress = config('users.falk_email');
                    $replyname = "Falk";
                    $email = $deal->email;

                    if( !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email,$replyemailaddress,$replyname,$deal) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->replyTo($replyemailaddress, $replyname)
                            ->subject($subject);
                        });
                    }
              }

            $response = ['status' => true, 'message' => 'Mark as decline successfully.'];

        }else{
            $response = ['status' => false, 'message' => 'Mark as decline fail! Try again.'];
        }

        return response()->json($response);
    }

    public function insuranceMarkAsNotReleasedAM(Request $request){
        $update_data = PropertyInsuranceTabDetails::find($request->id);

        if($update_data){

            $deal1 = $update_data;

            $update_data->not_release = 2;//not release for am
            $update_data->save();

            $deal = PropertyInsuranceTab::find($update_data->property_insurance_tab_id);

            $modal = new PropertiesMailLog;
            $modal->property_id = $deal->property_id;
            $modal->record_id = $update_data->property_insurance_tab_id;
            $modal->selected_id = $request->id;
            $modal->type = 'insurance_notrelease_am';
            $modal->tab = 'insurance_tab';
            if($request->comment)
                $modal->comment = $request->comment;
            $modal->user_id = Auth::user()->id;
            $modal->save();

            $data = DB::table('property_insurance_tab_details as pitd')
                        ->selectRaw('pitd.file_basename, pitd.file_type, pit.property_id, pitd.created_at, u.name, u.email, p.name_of_property, p.plz_ort, p.ort')
                        ->join('property_insurance_tabs as pit', 'pit.id', '=', 'pitd.property_insurance_tab_id')
                        ->join('properties as p', 'p.id', '=', 'pit.property_id')
                        ->join('users as u', 'u.id', '=', 'pitd.user_id')
                        ->where('pitd.id', $request->id)
                        ->first();

            if($data){

                $glink = "https://drive.google.com/drive/u/2/folders/".$data->file_basename;
                if($data->file_type == "file"){
                    $glink = "https://drive.google.com/file/d/".$data->file_basename;
                }
                $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                $url = route('properties.show',['property'=>$data->property_id]).'?tab=property_insurance_tab';
                $url = '<a href="'.$url.'">'.$url.'</a>';

                $text = "Hallo ".$data->name.",<br> dein Angebot ".$glink." wurde von ".Auth::user()->name." nicht freigegeben. Link zum Intranet: ".$url;
                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;

                $subject = "Angebot vom ".show_date_format($data->created_at).' für '.$data->plz_ort.' '.$data->ort." wurde nicht freigegeben";

                $replyemailaddress = config('users.falk_email');
                $replyname = "Falk";
                $email = $data->email;
                $name = $data->name;

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email, $name, $replyemailaddress, $replyname) {
                        $message->to($email, $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemailaddress, $replyname)
                        ->subject($subject);
                    });
                }
            }

            $response = ['status' => true, 'message' => 'Mark as decline AM successfully.'];

        }else{
            $response = ['status' => false, 'message' => 'Mark as decline AM fail! Try again.'];
        }

        return response()->json($response);
    }

    public function insuranceMarkAsPending(Request $request){
        $update_data = PropertyInsuranceTabDetails::find($request->id);

        if($update_data){

            $deal1 = $update_data;

            $update_data->not_release = 3;//pending
            $update_data->save();

            $deal = PropertyInsuranceTab::find($update_data->property_insurance_tab_id);

            $modal = new PropertiesMailLog;
            $modal->property_id = $deal->property_id;
            $modal->record_id = $update_data->property_insurance_tab_id;
            $modal->selected_id = $request->id;
            $modal->type = 'insurance_pending';
            $modal->tab = 'insurance_tab';
            if($request->comment)
                $modal->comment = $request->comment;
            $modal->user_id = Auth::user()->id;
            $modal->save();

            $data = DB::table('property_insurance_tab_details as pitd')
                        ->selectRaw('pitd.file_basename, pitd.file_type, pit.property_id, pitd.created_at, u.name, u.email, am.name as am_name, am.email as am_email, p.name_of_property, p.plz_ort, p.ort')
                        ->join('property_insurance_tabs as pit', 'pit.id', '=', 'pitd.property_insurance_tab_id')
                        ->join('properties as p', 'p.id', '=', 'pit.property_id')
                        ->join('users as u', 'u.id', '=', 'pitd.user_id')
                        ->leftjoin('users as am', 'am.id', '=', 'p.asset_m_id')
                        ->where('pitd.id', $request->id)
                        ->first();

            if($data){

                $glink = "https://drive.google.com/drive/u/2/folders/".$data->file_basename;
                if($data->file_type == "file"){
                    $glink = "https://drive.google.com/file/d/".$data->file_basename;
                }
                $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                $url = route('properties.show',['property'=>$data->property_id]).'?tab=property_insurance_tab';
                $url = '<a href="'.$url.'">'.$url.'</a>';

                $text = "Hallo ".$data->name.",<br> dein Angebot ".$glink." wurde von Falk auf Pending gesetzt. Link zum Intranet: ".$url;
                if($request->comment)
                    $text .= "<br><br>Kommentar: ".$request->comment;

                $subject = "Angebot vom ".show_date_format($data->created_at).' für '.$data->plz_ort.' '.$data->ort." wurde auf Pending gesetzt";

                $replyemailaddress = config('users.falk_email');
                $replyname = "Falk";
                $email = $data->email;
                $name = $data->name;

                // send mail to uploaded user
                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email, $name, $replyemailaddress, $replyname) {
                        $message->to($email, $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemailaddress, $replyname)
                        ->subject($subject);
                    });
                }

                // send mail to asset manager
                if($data->am_email && $data->am_email != $data->email && !in_array($am_email, $this->mail_not_send()) ){

                    $text = "Hallo ".$data->am_name.",<br> dein Angebot ".$glink." wurde von Falk auf Pending gesetzt. Link zum Intranet: ".$url;
                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;

                    $email = $data->am_email;
                    $name = $data->am_name;

                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email, $name, $replyemailaddress, $replyname) {
                        $message->to($email, $name)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($replyemailaddress, $replyname)
                        ->subject($subject);
                    });
                }


            }

            $response = ['status' => true, 'message' => 'Mark as pending successfully.'];

        }else{
            $response = ['status' => false, 'message' => 'Mark as pending fail! Try again.'];
        }

        return response()->json($response);
    }

    public function dealMarkAsPending(Request $request){
        $update_data = DB::table('properties_deals')->where('id', $request->id)->update(['not_release_status' => 2,'comment2' => $request->comment]);
        if($update_data){

            $deal = PropertyInsuranceTab::find($request->id);

            $modal = new PropertiesMailLog;
            $modal->property_id = $deal->property_id;
            $modal->record_id = $request->id;
            // $modal->selected_id = $request->detail_id;
            $modal->type = 'insurance_pending';
            $modal->tab = 'insurance_tab';
            if($request->comment)
                $modal->comment = $request->comment;
            $modal->user_id = Auth::user()->id;
            $modal->save();

            $data = DB::table('properties_deals as pd')
                    ->select('pd.*','u.email','u.name','p.name_of_property', 'p.plz_ort', 'p.ort')
                    ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pd.id')
                    ->join('properties as p', 'p.id', '=', 'pd.property_id')
                    ->join('users as u', 'u.id', '=', 'pml.user_id')
                    ->where('pd.id',$request->id)
                    ->where('pml.tab','deals')
                    ->where('pml.type','deal_request')
                    ->groupBy('pml.user_id')
                    ->get();

            foreach ($data as $key => $deal) {
                $url = route('properties.show',['property'=>$deal->property_id]).'?tab=property_deals';
                $url = '<a href="'.$url.'">'.$url.'</a>';

                $glink = "https://drive.google.com/drive/u/2/folders/".$deal->file_basename;
                if($deal->file_type == "file"){
                    $glink = "https://drive.google.com/file/d/".$deal->file_basename;
                }
                $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                // $text = "Hallo ".$deal->name.",<br> dein Angebot/Vertrag ".$glink." wurde von Falk noch nicht freigegeben. Link zum Intranet: ".$url;

                $text = "Hallo ".$deal->name.",<br> dein Angebot ".$glink." wurde von Falk noch nicht freigegeben. Link zum Intranet: ".$url;

                // $subject = "Angebot/Vertrag vom ".show_date_format($deal->created_at).' für '.$deal->plz_ort.' '.$deal->ort." wurde noch nicht freigegeben";

                $subject = "Angebot vom ".show_date_format($deal->created_at).' für '.$deal->plz_ort.' '.$deal->ort." wurde noch nicht freigegeben";

                $replyemailaddress = config('users.falk_email');
                $replyname = "Falk";
                $email = $deal->email;
                // $email = "yiicakephp@gmail.com";

                // if($request->comment)
                //     $text .= "<br><br>Kommentar: ".$request->comment;

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email,$replyemailaddress,$replyname,$deal) {
                          $message->to($email)
                          ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                          ->replyTo($replyemailaddress, $replyname)
                          ->subject($subject);
                    });
                }

            }

            $response = ['status' => true, 'message' => 'Mark as pending successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Mark as pending fail! Try again.'];
        }

        return response()->json($response);
    }

    public function update_deal_by_field(Request $request, $deal_id) {

        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $property = PropertiesDeals::where('id', '=', $deal_id)->first();

        if (empty($property)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        $update_data = DB::table('properties_deals')->where('id', $deal_id)
                    ->update([$request->pk => $request->value]);
        $response = [
            'success' => true,
            'msg' => '',
            'reload' => false
        ];

        return response()->json($response);
    }

    public function addPropertyDefaultPayer(Request $request){
        // Excel::import(new ExcelImportMieterliste($request,$request->property_id,$request->year,$request->month), Input::file('default_payer_gdrive_file_upload'));
        // die;

        $request->year = date('Y');
        $request->month = date('m');

        if($request->ajax()) {

            $inputs = array(
                'file_basename' => $request->file_basename,
                'file_dirname' => $request->file_dirname,
                'file_type' => $request->file_type,
                'file_name' => $request->file_name,
                'comment' => trim($request->comment),
                'year' => $request->year,
                'month' => $request->month,
            );

                $modal = new PropertiesDefaultPayers;
                $modal->property_id = $request->property_id;
                $modal->user_id = Auth::user()->id;
                $modal->comment = trim($request->comment);
                $modal->date = date('Y-m-d');
                $modal->year = $request->year;
                $modal->month = $request->month;
                if($request->no_opos_status)
                $modal->no_opos_status = 1;

                if($request->hasFile('default_payer_gdrive_file_upload')){

                    $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
                    if(!$propertyMainDir){
                        $res = PropertiesHelperClass::createPropertyDirectories($request->property_id);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
                    }
                    $propertyPayerDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Verzugszahler')->first();
                    if(!$propertyPayerDir){
                        $res = PropertiesHelperClass::createPropertySubDirectories($propertyMainDir, $request->property_id, ['Verzugszahler']);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property Verzugszahler directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyPayerDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Verzugszahler')->first();
                    }


                    $file = $request->file('default_payer_gdrive_file_upload');
                    $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $originalExt = $file->getClientOriginalExtension();
                    $folder = $propertyPayerDir->dir_path;
                    $fileData = File::get($file);

                    //upload file
                    $file2 =  Storage::cloud()->put($folder.'/'.$originalName.'.'.$originalExt, $fileData);

                    //get google drive id
                    $contents = collect(Storage::cloud()->listContents($folder, false));
                    $file = $contents->where('type','file')->where('filename', $originalName)->where('extension', $originalExt) ->first();
                    if($file['basename'] == ""){
                        $result = ['status' => false, 'message' => 'File upload fail.', 'data' => []];
                        return response()->json($result);
                    }

                    $modal->invoice = $file['name'];
                    $modal->file_basename = $file['basename'];
                    $modal->file_dirname = $file['dirname'];
                    $modal->file_type = $file['type'];


                    if ($originalExt == 'xlsx' || $originalExt == 'xls')

                    Excel::import(new ExcelImportMieterliste($request,$request->property_id,$request->year,$request->month), Input::file('default_payer_gdrive_file_upload'));



                }else{
                    $modal->invoice = $request->file_name;
                    $modal->file_basename = $request->file_basename;
                    $modal->file_dirname = $request->file_dirname;
                    $modal->file_type = $request->file_type;
                }


                // $modal1 = new RentPaidExcelUpload;
                // $modal1->property_id = $request->property_id;
                // $modal1->user_id = Auth::user()->id;

                // $no_opos_status = 0;
                // if($request->no_opos_status)
                //     $no_opos_status = 1;

                // $modal1->no_opos_status = $no_opos_status;
                // $modal1->save();




                if($modal->save()){
                    $result = ['status' => true, 'message' => 'Domus excel save successfully.', 'data' => []];
                }else{
                    $result = ['status' => false, 'message' => 'Domus excel save fail! Try again.', 'data' => []];
                }

        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getDefaultPayer($property_id){

        $res = DB::table('properties_default_payers as pdp')
                    ->select('pdp.*','u.name', 'p.name_of_property')
                    ->join('users as u', 'u.id', '=', 'pdp.user_id')
                    ->join('properties as p', 'p.id', '=', 'pdp.property_id')
                    ->where('pdp.property_id', $property_id)
                    ->orderBy('pdp.id', 'desc')
                    ->get();

        $data['data'] = [];

        $taburl = route('properties.show',['property' => $property_id]).'?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        if(!empty($res)){
            foreach ($res as $key => $value) {

                $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                            ->join('users as u', 'u.id', 'pc.user_id')
                            ->where('pc.record_id', $value->id)
                            ->where('pc.type', 'properties_default_payers')
                            ->orderBy('pc.created_at', 'desc')
                            ->limit(2)->get();
                $latest_comment = '';
                if($comments && count($comments)){
                    $latest_comment .= '<div class="show_property_cmnt_section">';
                    foreach ($comments as $n => $comment) {
                        $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                        $commented_user = $comment->name.''.$company;
                        $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                    }
                    $latest_comment .= '</div>';
                }
                if($latest_comment){
                    $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=properties_default_payers" class="load_property_comment_section" data-closest="td">Show More</a>';
                }

                $content = $taburl;

                $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="properties_default_payers" data-subject="OPOS HV: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }
                $data['data'][] = [
                    ($key+1),
                    // $value->year,
                    // sprintf('%02d', $value->month),
                    get_paid_checkbox($value->id,$value->no_opos_status),
                    '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    /*'<a href="#" class="inline-edit default-payer-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/default-payer/'.$value->id).'" >'.$value->comment.'</a>',*/
                    $latest_comment.'<br>'.$comment_button,
                    $value->name,
                    show_datetime_format($value->created_at),
                    // '<a href="#" class="inline-edit default-payer-comment" data-type="textarea" data-pk="comment2" data-placement="bottom" data-url="'.url('property/update/default-payer/'.$value->id).'" >'.$value->comment2.'</a>',
                    '<button type="button" data-url="'. route('delete_default_payer', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-default-payer"><i class="icon-trash"></i></button>'
                ];
            }
        }

        return response()->json($data);
    }

    public function deleteDefaultPayer($id){
        $model = PropertiesDefaultPayers::find($id);
        // $file= public_path(). "/invoice_upload/".$model->invoice;
        // File::delete($file);

        PropertyRentPaids::where('property_id',$model->property_id)
        ->where('upload_month',$model->month)
        ->where('upload_year',$model->year)
        ->delete();

        if($model->delete()){
            $result = ['status' => true, 'message' => 'Default payer delete successfully.', 'data' => []];
        }else{
            $result = ['status' => false, 'message' => 'Default payer delete fail! Try again.', 'data' => []];
        }
        return response()->json($result);
    }

    public function update_default_payer_by_field(Request $request, $default_payer_id) {

        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $property = PropertiesDefaultPayers::where('id', '=', $default_payer_id)->first();

        if (empty($property)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        $update_data = DB::table('properties_default_payers')->where('id', $default_payer_id)
                    ->update([$request->pk => $request->value]);
        $response = [
            'success' => true,
            'msg' => '',
            'reload' => false
        ];

        return response()->json($response);
    }

    public function addPropertyInvoice(Request $request){

        $user = Auth::user();
        if($request->ajax()) {
            $rules = array('file_basename' => 'required');
            if($request->hasFile('property_invoice_gdrive_file_upload')){
                // $rules = array('comment' => 'required');
                $rules = array();
            }

            $inputs = array(
                'file_basename' => $request->file_basename,
                'file_dirname' => $request->file_dirname,
                'file_type' => $request->file_type,
                'file_name' => $request->file_name,
                'comment' => trim($request->comment)
            );
            $validator = Validator::make($inputs, $rules);
            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{
                $is_paid = 0;
                if($request->is_paid)
                    $is_paid = 1;

                $towards_tenant = 0;
                if($request->towards_tenant)
                    $towards_tenant = 1;

                $is_confidential_invoice = 0;
                if($request->is_confidential_invoice)
                    $is_confidential_invoice = 1;


                $modal = new PropertyInvoices;
                $modal->property_id = $request->property_id;
                $modal->user_id = $user->id;
                $modal->comment = trim($request->comment);
                $modal->need_to_pay = $is_paid;
                $modal->towards_tenant = $towards_tenant;
                $modal->is_confidential_invoice = $is_confidential_invoice;
                $amount = 0;
                if($request->amount)
                {
                    $amount = str_replace('.', '', $request->amount);
                    $amount = str_replace(',', '.', $amount);
                }
                $modal->amount = $amount;
                $foldable = 0;
                if($request->foldable)
                {
                    $foldable = str_replace('.', '', $request->foldable);
                    $foldable = str_replace(',', '.', $foldable);
                }
                $modal->foldable = $foldable;


                if($request->date){
                    $modal->date = date('Y-m-d', strtotime(str_replace(".", "-", $request->date)));
                }

                if($request->hasFile('property_invoice_gdrive_file_upload')){

                    $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
                    if(!$propertyMainDir){
                        $result = ['status' => false, 'message' => 'Property directory not created in google Drive.', 'data' => []];
                        return response()->json($result);
                    }
                    $propertyRechnungenDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Rechnungen')->first();
                    if(!$propertyRechnungenDir){
                        $result = ['status' => false, 'message' => 'Property Rechnungen directory not created in google Drive.', 'data' => []];
                        return response()->json($result);
                    }


                    $file = $request->file('property_invoice_gdrive_file_upload');
                    $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $originalExt = $file->getClientOriginalExtension();
                    $folder = $propertyRechnungenDir->dir_path;
                    $fileData = File::get($file);

                    //upload file
                    $file2 =  Storage::cloud()->put($folder.'/'.$originalName.'.'.$originalExt, $fileData);

                    //get google drive id
                    $contents = collect(Storage::cloud()->listContents($folder, false));
                    $file = $contents->where('type','file')->where('filename', $originalName)->where('extension', $originalExt) ->first();
                    if($file['basename'] == ""){
                        $result = ['status' => false, 'message' => 'File upload fail.', 'data' => []];
                        return response()->json($result);
                    }

                    $modal->invoice = $originalName . '.'. $originalExt;
                    $modal->file_basename = $file['basename'];
                    $modal->file_dirname = $file['dirname'];
                    $modal->file_type = $file['type'];


                }else{
                    $modal->invoice = $request->file_name;
                    $modal->file_basename = $request->file_basename;
                    $modal->file_dirname = $request->file_dirname;
                    $modal->file_type = $request->file_type;
                }


                if($modal->save()){

                    // Save comment
                    if($request->comment){
                        $commentModal = new PropertiesComment;
                        $commentModal->property_id = $request->property_id;
                        $commentModal->user_id = Auth::user()->id;
                        $commentModal->comment = $request->comment;
                        $commentModal->record_id = $modal->id;
                        $commentModal->type = 'property_invoices';
                        $commentModal->save();
                    }

                    $p = Properties::find($request->property_id);

                    // Release invoice if login user is AM or Hv users
                    if( ($p->asset_m_id && $user->id == $p->asset_m_id) || ($p->asset_m_id2 && $user->id == $p->asset_m_id2) ){ //Release AM

                        $releaseModal = PropertyInvoices::find($modal->id);
                        $releaseModal->am_status = 2;
                        $releaseModal->last_process_type = 'am_status';

                        if($releaseModal->save()){

                            // Add mail log
                            $mailLogModal = new PropertiesMailLog;
                            $mailLogModal->property_id = $request->property_id;
                            $mailLogModal->record_id = $modal->id;
                            $mailLogModal->type = 'release_invoice_am';
                            $mailLogModal->tab = 'invoice';
                            $mailLogModal->user_id = $user->id;
                            $mailLogModal->save();

                        }

                    }elseif( ($p->hvbu_id && $user->id == $p->hvbu_id) || ($p->hvpm_id && $user->id == $p->hvpm_id) ) { //Release HV

                        $releaseModal = PropertyInvoices::find($modal->id);
                        $releaseModal->hv_status = 2;
                        $releaseModal->last_process_type = 'hv_status';

                        if($releaseModal->save()){

                            // Add mail log
                            $mailLogModal = new PropertiesMailLog;
                            $mailLogModal->property_id = $request->property_id;
                            $mailLogModal->record_id = $modal->id;
                            $mailLogModal->type = 'release_invoice_hv';
                            $mailLogModal->tab = 'invoice';
                            $mailLogModal->user_id = $user->id;
                            $mailLogModal->save();

                        }

                    }


                    $glink = "https://drive.google.com/drive/u/2/folders/".$modal->file_basename;
                    if($modal->file_type == "file"){
                        $glink = "https://drive.google.com/file/d/".$modal->file_basename;
                    }
                    $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                    $url = route('properties.show',['property'=>$request->property_id]).'?tab=property_invoice';
                    $url = '<a href="'.$url.'">'.$url.'</a>';

                    $subject = "Rechnungsfreigabe";
                    $text = 'Eine neue Rechnung wurde für das Objekt '.$p->plz_ort.' '.$p->ort.' '.$user->name.' hochgeladen und wartet auf Freigabe: '.$url;

                    $text .= "<br><br>Link: ".$glink;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $name = $name2 = $email = $email2 = "";
                    if(isset($p->asset_manager) && $p->asset_manager){
                        $name = $p->asset_manager->name;
                        $email = $p->asset_manager->email;
                    }
                    if(in_array($p->id, $this->propertiesids())){
                        $p21 = $this->getdata21();
                        $name = $p21['name'];
                        $email = $p21['email'];
                    }
                    if(isset($p->asset_manager_two) && $p->asset_manager_two){
                        $name2 = $p->asset_manager_two->name;
                        $email2 = $p->asset_manager_two->email;
                    }

                    $hvbu_name = $hvbu_email = '';
                    $hvpm_name = $hvpm_email = '';

                    if($p->hvbu_id){
                        $hvbuuser = User::find($p->hvbu_id);
                        if($hvbuuser)
                        {
                            $hvbu_name = $hvbuuser->name;
                            $hvbu_email = $hvbuuser->email;
                        }
                    }

                    if($p->hvpm_id){
                        $hvpmuser = User::find($p->hvpm_id);
                        if($hvpmuser)
                        {
                            $hvpm_name = $hvpmuser->name;
                            $hvpm_email = $hvpmuser->email;
                        }
                    }

                    if($user->role==7)  // HV BU upload only AM will be notify
                    {
                        if($email && $user->email!=$email && !in_array($email, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject) {
                                $message->to($email)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->subject($subject);
                            });
                        }

                    }
                    else{
                        // Send mail to AM1
                        if($email && $user->email!=$email && !in_array($email, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject) {
                                $message->to($email)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->subject($subject);
                            });
                        }

                        // Send mail to AM2
                        if($email2 && $user->email!=$email2 && $email2 != $email && !in_array($email2, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($email2,$subject) {
                                $message->to($email2)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->subject($subject);
                            });
                        }

                        // Send mail to HV BU
                        if( $hvbu_email && $user->email!=$hvbu_email &&  !in_array($hvbu_email, [$email, $email2]) && !in_array($hvbu_email, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($hvbu_email,$subject) {
                                $message->to($hvbu_email)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->subject($subject);
                            });

                        }

                        // Send mail to HV PM
                        if( $hvpm_email && $user->email!=$hvpm_email &&  !in_array($hvpm_email, [$email, $email2, $hvbu_email]) && !in_array($hvpm_email, $this->mail_not_send()) ){

                            Mail::send('emails.general', ['text' => $text], function ($message) use($hvpm_email,$subject) {
                                $message->to($hvpm_email)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->subject($subject);
                            });

                        }
                    }

                    $result = ['status' => true, 'message' => 'Invoice save successfully.', 'data' => []];
                }else{
                    $result = ['status' => false, 'message' => 'Invoice save fail! Try again.', 'data' => []];
                }

            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getPropertyInvoice($property_id){

        $login_user = Auth::user();
        $condition =  "1=1";

        if($login_user->role==9)
            $condition = 'pi.user_id='.$login_user->id;

        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';

            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
                $condition = ' (pi.user_id = "'.$login_user->id.'")';

        }



        $res = DB::table('property_invoices as pi')
                    ->select('pi.*', 'p.name_of_property','u.name','u1.name as amname')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->leftJoin('users as u1', 'u1.id', '=', 'pi.email')
                    ->where('pi.property_id', $property_id)
                    ->where('pi.deleted', 0)
                    ->whereRaw($condition)
                    ->whereRaw('((pi.am_status = 0 OR pi.am_status = 1 OR pi.am_status = 2) AND (pi.hv_status = 0 OR pi.hv_status = 1 OR pi.hv_status = 2) AND (pi.user_status = 0 OR pi.user_status = 1 OR pi.user_status = 2) AND (pi.falk_status = 0 OR pi.falk_status = 1))')
                    ->groupBy('pi.id')
                    ->get();


        $mails_logs = PropertiesMailLog::where('property_id', $property_id)->where('tab','invoice')->get();

        $array = array();
        foreach ($mails_logs as $key => $value) {
            $array[$value->record_id][$value->type] = $value;
        }
        $user = Auth::user();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $data['data'] = [];

        if(!empty($res)){
            $no = 1;
            foreach ($res as $key => $value) {

                $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                            ->join('users as u', 'u.id', 'pc.user_id')
                            ->where('pc.record_id', $value->id)
                            ->where('pc.type', 'property_invoices')
                            ->orderBy('pc.created_at', 'desc')
                            ->limit(2)->get();
                $latest_comment = '';
                if($comments && count($comments)){
                    $latest_comment .= '<div class="show_property_cmnt_section">';
                    foreach ($comments as $n => $comment) {
                        $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                        $commented_user = $comment->name.''.$company;
                        $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                    }
                    $latest_comment .= '</div>';
                }
                if($latest_comment){
                    $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
                }

                $content = $taburl;

                $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Offene Rechnungen: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

                $subject = 'Offene Rechnungen: '.$value->name_of_property;

                $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="OFFENE RECHNUNGEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

                $dp = '';

                   $selected_user_name = $value->amname;



                    $release1 = 'btn btn-primary invoice-release-request-am';
                    $release2 = 'btn btn-primary invoice-release-request';

                    $release1_type = "request1";
                    $release2_type = "request2";
                    // $rbutton_title1 = "Zur Freigabe an $selected_user_name senden";
                    $rbutton_title1 = "Senden an $selected_user_name";
                    // $rbutton_title2 = "Zur Freigabe an Falk senden";
                    $rbutton_title2 = "Senden";

                    $email = strtolower($user->email);

                    $list = array();
                    if(isset($array[$value->id]))
                        $list = $array[$value->id];

                    $checkbox = "";
                    $btn3 = $btn4  = $btn5 = "";

                    if(!$value->email)
                        $value->email = 0;

                    if($user->id==$value->email)
                    {
                        $release1 =  'btn btn-primary invoice-release-request-am';
                        $rbutton_title1 = "Freigeben";
                        $release1_type = "release1";

                        if($list && isset($list['request2']))
                        {
                          $release2 =  " btn-success invoice-release-request";
                          $rbutton_title2 = "Zur Freigabe gesendet";
                        }
                    }
                    elseif($email==config('users.falk_email'))
                    {
                        $rbutton_title2 = "Freigeben";
                        $release2 = 'btn btn-primary invoice-release-request';
                        $release2_type = "release2";

                        if($list && isset($list['request1']))
                        {
                          $release1 =  "btn-success invoice-release-request-am";
                          $rbutton_title1 = "Zur Freigabe gesendet";
                        }

                        $checkbox = '<input class="invoice-checkbox" type="checkbox" data-id="'.$value->id.'">';

                        ob_start();

                        ?>
                        <button data-id="<?=$value->id?>" type="button" class="btn btn-primary mark-as-not-release">Ablehnen</button>
                        <?php
                        $btn3 = ob_get_clean();
                        ob_start();

                        ?>
                        <button data-id="<?=$value->id?>" type="button" class="btn btn-primary mark-as-pending">Pending</button>
                        <?php
                        $btn4 = ob_get_clean();

                        $btn5 = '<button data-id="'.$value->id.'" type="button" class="btn btn-primary mark-as-pending-am">Pending AM</button>';


                    }
                    else{
                        if($list && isset($list['request1']))
                        {
                          $release1 =  " btn-success invoice-release-request-am";
                          $rbutton_title1 = "Zur Freigabe gesendet";
                        }

                        if($list && isset($list['request2']))
                        {
                          $release2 =  " btn-success invoice-release-request";
                          $rbutton_title2 = "Zur Freigabe gesendet";
                        }
                    }

                    $r1 = $r2 = "";
                    if($list && isset($list['release1']))
                    {
                      $release1 =  " btn-success";
                      $rbutton_title1 = "Freigegeben";
                      $dp = $selected_user_name;
                    }
                    $delete = 1;

                    if($list && isset($list['release2']))
                    {
                      $delete = 0;
                      $release2 =  " btn-success";
                      $rbutton_title2 = "Freigegeben";
                    }
                $btn1 = '';
                $date2 = show_datetime_format($value->created_at);
                if($list && isset($list['request2'])){
                    $request2 = $list['request2'];
                    $date2 = show_datetime_format($request2->created_at);
                }

                if(1)
                {
                    ob_start();
                    ?><button data-user-id="<?=$value->email?>" data-id="<?=$value->id?>" type="button" class="btn <?=$release1?>" data-column="<?=$release1_type?>" data-user-id="<?=$value->email?>"><?=$rbutton_title1?></button>
                    <?php
                    $btn1 = ob_get_clean();
                }
                ob_start();
                ?><button data-id="<?=$value->id?>" type="button" class="btn <?=$release2?>" data-column="<?=$release2_type?>"> <?=$rbutton_title2?></button>
                <?php


                $btn2 = ob_get_clean();

                if($user->role == 6 && $user->email != 'courteaux@hotel-westerburg.de')
                    $btn2 = "";

                $btn_rejecting = '<button data-id="'.$value->id.'" type="button" class="btn btn-primary btn-reject">Ablehnen</button>';
                // $download_path = route('download_property_invoice', ['file_name' => $value->invoice]);
                // $download_path = route('download_property_invoice', ['file_name' => $value->invoice]);
                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }

                $all_button = $this->getInvoiceButton($value, $user);

                $data['data'][] = [
                    '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                    $no,
                    '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    show_date_format($value->date),
                    show_number($value->amount,2),
                    $latest_comment.'<br>'.$comment_button,
                    get_paid_checkbox($value->id,$value->need_to_pay),
                    get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),
                    $value->name,
                    $date2,
                    // $btn1.'<br>'.$selected_user_name,
                    // $btn5,
                    // $btn_rejecting,
                    // $btn2.$checkbox,
                    // $btn3,
                    // $btn4,
                    $all_button['btn_release_am'],
                    $all_button['btn_release_hv'],
                    $all_button['btn_release_usr'],
                    $all_button['btn_release_falk'],
                    ($delete) ? '<button type="button" data-type="open-invoice" data-id="'.$value->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>' : "",
                    $mail_button
                ];
                $no++;
            }
        }

        return response()->json($data);
    }

    public function getInvoiceButton($invoice, $user){

        $am_class1 = 'btn-inv-release-am';
        $hv_class1 = 'btn-inv-release-hv';
        $user_class1 = 'btn-inv-release-usr';
        $falk_class1 = 'btn-inv-release-falk';

        $am_class2 = $am_class3 = $am_class4 = '';
        $hv_class2 = $hv_class3 = $hv_class4 = '';
        $user_class2 = $user_class3 = $user_class4 = '';
        $falk_class2 = $falk_class3 = $falk_class4 = '';

        if( $user->email == config('users.falk_email') || $invoice->email == $user->id ){

            $am_class2 = $am_class3 = $am_class4 = 'btn-inv-release-am';
            $hv_class2 = $hv_class3 = $hv_class4 = 'btn-inv-release-hv';
            $user_class2 = $user_class3 = $user_class4 = 'btn-inv-release-usr';
            $falk_class2 = $falk_class3 = $falk_class4 = 'btn-inv-release-falk';

        }elseif(in_array($user->role, [7,8])){
            $hv_class2 = $hv_class3 = $hv_class4 = 'btn-inv-release-hv';
        }elseif($user->role == 4){
            $am_class2 = $am_class3 = $am_class4 = 'btn-inv-release-am';
        }

        if($user->role >= 6 && !in_array($user->role, [7, 8])){
            $am_class1 = $am_class2 = $am_class3 = $am_class4 = '';
            $hv_class1 = $hv_class2 = $hv_class3 = $hv_class4 = '';
            $user_class1 = $user_class2 = $user_class3 = $user_class4 = '';
            $falk_class1 = $falk_class2 = $falk_class3 = $falk_class4 = '';
        }

        if($invoice->am_status == 2){
            $am_class1 = $am_class2 = $am_class3 = $am_class4 = '';
        }
        if($invoice->hv_status == 2){
            $hv_class1 = $hv_class2 = $hv_class3 = $hv_class4 = '';
        }
        if($invoice->user_status == 2){
            $user_class1 = $user_class2 = $user_class3 = $user_class4 = '';
        }
        if($invoice->falk_status == 2){
            $am_class1 = $am_class2 = $am_class3 = $am_class4 = '';
            $hv_class1 = $hv_class2 = $hv_class3 = $hv_class4 = '';
            $user_class1 = $user_class2 = $user_class3 = $user_class4 = '';
            $falk_class1 = $falk_class2 = $falk_class3 = $falk_class4 = '';
        }

        $selected_user_name = '';
        if($invoice->email && is_numeric($invoice->email) && $invoice->user_status != 0){
            $usr = DB::table('users')->select('name')->where('id', $invoice->email)->first();
            if(isset($usr->name) && $usr->name){
                $selected_user_name = $usr->name;
            }
        }


        $btn_am_req = '<button type="button" data-status="1" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->am_status == 1) ? 'info' : 'default '.$am_class1) .' btn-inv-rel" title="Send"><i class="fa fa-paper-plane"></i></button>';
        $btn_am_rel = '<button type="button" data-status="2" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->am_status == 2) ? 'success' : 'default '.$am_class2) .' btn-inv-rel" title="Release"><i class="fa fa-check-circle"></i></button>';
        $btn_am_pen = '<button type="button" data-status="3" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->am_status == 3) ? 'warning' : 'default '.$am_class3) .' btn-inv-rel" title="Pending"><i class="fa fa-exclamation-triangle"></i></button>';
        $btn_am_not_rel = '<button type="button" data-status="4" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->am_status == 4) ? 'danger' : 'default '.$am_class4) .' btn-inv-rel" title="Not Release"><i class="fa fa-times-circle"></i></button>';

        $btn_release_am = $btn_am_req.' '.$btn_am_rel.' '.$btn_am_pen.' '.$btn_am_not_rel;

        $btn_hv_req = '<button type="button" data-status="1" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->hv_status == 1) ? 'info' : 'default '.$hv_class1) .' btn-inv-rel" title="Send"><i class="fa fa-paper-plane"></i></button>';
        $btn_hv_rel = '<button type="button" data-status="2" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->hv_status == 2) ? 'success' : 'default '.$hv_class2) .' btn-inv-rel" title="Release"><i class="fa fa-check-circle"></i></button>';
        $btn_hv_pen = '<button type="button" data-status="3" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->hv_status == 3) ? 'warning' : 'default '.$hv_class3) .' btn-inv-rel" title="Pending"><i class="fa fa-exclamation-triangle"></i></button>';
        $btn_hv_not_rel = '<button type="button" data-status="4" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->hv_status == 4) ? 'danger' : 'default '.$hv_class4) .' btn-inv-rel" title="Not Release"><i class="fa fa-times-circle"></i></button>';

        $btn_release_hv = $btn_hv_req.' '.$btn_hv_rel.' '.$btn_hv_pen.' '.$btn_hv_not_rel;

        $btn_usr_req = '<button type="button" data-status="1" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->user_status == 1) ? 'info' : 'default '.$user_class1) .' btn-inv-rel" title="Send"><i class="fa fa-paper-plane"></i></button>';
        $btn_usr_rel = '<button type="button" data-status="2" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->user_status == 2) ? 'success' : 'default '.$user_class2) .' btn-inv-rel" title="Release"><i class="fa fa-check-circle"></i></button>';
        $btn_usr_pen = '<button type="button" data-status="3" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->user_status == 3) ? 'warning' : 'default '.$user_class3) .' btn-inv-rel" title="Pending"><i class="fa fa-exclamation-triangle"></i></button>';
        $btn_usr_not_rel = '<button type="button" data-status="4" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->user_status == 4) ? 'danger' : 'default '.$user_class4) .' btn-inv-rel" title="Not Release"><i class="fa fa-times-circle"></i></button>';

        if($selected_user_name){
            $btn_usr_not_rel .= '<br>'.$selected_user_name;
        }

        $btn_release_usr = $btn_usr_req.' '.$btn_usr_rel.' '.$btn_usr_pen.' '.$btn_usr_not_rel;

        $btn_falk_req = '<button type="button" data-status="1" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->falk_status == 1) ? 'info' : 'default '.$falk_class1) .' btn-inv-rel" title="Send"><i class="fa fa-paper-plane"></i></button>';
        $btn_falk_rel = '<button type="button" data-status="2" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->falk_status == 2) ? 'success' : 'default '.$falk_class2) .' btn-inv-rel" title="Release"><i class="fa fa-check-circle"></i></button>';
        $btn_falk_pen = '<button type="button" data-status="3" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->falk_status == 3) ? 'warning' : 'default '.$falk_class3) .' btn-inv-rel" title="Pending"><i class="fa fa-exclamation-triangle"></i></button>';
        $btn_falk_not_rel = '<button type="button" data-status="4" data-id="'.$invoice->id.'" class="btn btn-'. (($invoice->falk_status == 4) ? 'danger' : 'default '.$falk_class4) .' btn-inv-rel" title="Not Release"><i class="fa fa-times-circle"></i></button>';

        $btn_release_falk = $btn_falk_req.' '.$btn_falk_rel.' '.$btn_falk_pen.' '.$btn_falk_not_rel;

        $all_button = [
            'btn_release_am' => $btn_release_am,
            'btn_release_hv' => $btn_release_hv,
            'btn_release_usr' => $btn_release_usr,
            'btn_release_falk' => $btn_release_falk,
        ];

        return $all_button;
    }

    public function getNotReleaseInvoice($property_id)
    {
        //$property_id = $request->id;
        $login_user = Auth::user();
        $condition =  "1=1";

        if($login_user->role==9){
            $condition = 'pi.user_id='.$login_user->id;
        }
        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';

            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
                $condition = ' (pi.user_id = "'.$login_user->id.'")';
        }

        $res = DB::table('property_invoices as pi')
                    ->select('pi.*','u.name','p.name_of_property')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->where('pi.property_id', $property_id)
                    ->where('pi.falk_status', 4)
                    ->where('pi.last_process_type', 'falk_status')
                    ->where('pi.deleted', 0)
                    ->whereRaw($condition)
                    ->get();

        $delete  = 0;
        $user = Auth::user();
        $email = strtolower($user->email);
        // if($email==config('users.falk_email'))
            // $delete  = 1;
        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $data['data'] = array();
        $no = 1;
        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_invoices')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Nicht Freigegeben (Falk): '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Nicht Freigegeben (Falk): '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="NICHT FREIGEGEBEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $all_button = $this->getInvoiceButton($value, $user);

            $data['data'][] = [
                '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                $no,
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_date_format($value->date),
                show_number($value->amount,2),
                $value->name,
                /*'<a href="#" class="inline-edit invoice-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/invoice/'.$value->id).'" >'.$value->comment.'</a>',*/
                $latest_comment.'<br>'.$comment_button,
                get_paid_checkbox($value->id,$value->need_to_pay),
                get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),
                show_date_format($value->created_at),
                // $value->comment2,
                $all_button['btn_release_am'],
                $all_button['btn_release_hv'],
                $all_button['btn_release_usr'],
                $all_button['btn_release_falk'],
                '<button type="button" data-type="not-release-invoice" data-id="'.$value->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>',
                $mail_button
            ];
            $no++;
        }
        return response()->json($data);

        // return $data;
    }

    public function getPendingInvoice($property_id)
    {
        //$property_id = $request->id;
        $login_user = Auth::user();
        $condition =  "1=1";

        if($login_user->role==9){
            $condition = 'pi.user_id='.$login_user->id;
        }
        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';

            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
                $condition = ' (pi.user_id = "'.$login_user->id.'")';
        }

        $res = DB::table('property_invoices as pi')
                    ->select('pi.*','u.name', 'p.name_of_property','u1.name as amname')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->leftJoin('users as u1', 'u1.id', '=', 'pi.email')
                    ->where('pi.property_id', $property_id)
                    ->where('pi.falk_status', 3)
                    ->where('pi.last_process_type', 'falk_status')
                    ->where('pi.deleted', 0)
                    ->whereRaw($condition)
                    ->get();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $user = Auth::user();
        $data['data'] = array();
        $no = 1;
        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_invoices')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Pending Rechnungen: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Pending Rechnungen: '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="PENDING RECHNUNGEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $list = array();
            if(isset($array[$value->id]))
                $list = $array[$value->id];


                $release2_type = "request2";
                // $rbutton_title2 = "Zur Freigabe an Falk senden";
                $rbutton_title2 = "Senden";

                $email = strtolower($user->email);

                $list = array();
                if(isset($array[$value->id]))
                    $list = $array[$value->id];

                    $selected_user_name = $value->amname;

                    $release1 = 'btn btn-primary invoice-release-request-am';
                    $release2 = 'btn btn-primary invoice-release-request';

                    $release1_type = "request1";
                    // $rbutton_title1 = "Zur Freigabe an $selected_user_name senden";
                    $rbutton_title1 = "Senden an $selected_user_name";

                    $email = strtolower($user->email);

                    $list = array();
                    if(isset($array[$value->id]))
                        $list = $array[$value->id];

                    $checkbox = "";
                    $btn3 = $btn4 = "";

                    if(!$value->email)
                        $value->email = 0;


                    if($user->id==$value->email)
                    {
                        $release1 =  'btn btn-primary invoice-release-request-am';
                        $rbutton_title1 = "Freigeben";
                        $release1_type = "release1";

                        if($list && isset($list['request2']))
                        {
                          $release2 =  " btn-success invoice-release-request";
                          $rbutton_title2 = "Zur Freigabe gesendet";
                        }
                    }



              else if($email==config('users.falk_email'))
                {
                    $rbutton_title2 = "Freigeben";
                    $release2 = 'btn btn-primary invoice-release-request';
                    $release2_type = "release2";
                }
                else{

                        if($list && isset($list['request1']))
                        {
                          $release1 =  " btn-success invoice-release-request-am";
                          $rbutton_title1 = "Zur Freigabe gesendet";
                        }

                        if($list && isset($list['request2']))
                        {
                          $release2 =  " btn-success invoice-release-request";
                          $rbutton_title2 = "Zur Freigabe gesendet";
                        }
                    }

                    $r1 = $r2 = "";
                    if($list && isset($list['release1']))
                    {
                      $release1 =  " btn-success";
                      $rbutton_title1 = "Freigegeben";
                      $dp = $selected_user_name;
                    }
                    $delete = 1;

                    if($list && isset($list['release2']))
                    {
                      $delete = 0;
                      $release2 =  " btn-success";
                      $rbutton_title2 = "Freigegeben";
                    }


                    ob_start();
                    ?><button data-user-id="<?=$value->email?>" data-id="<?=$value->id?>" type="button" class="btn <?=$release1?>" data-column="<?=$release1_type?>"><?=$rbutton_title1?></button>
                    <?php
                    $btn1 = ob_get_clean();


                ob_start();
                ?><button data-id="<?=$value->id?>" type="button" class="btn <?=$release2?>" data-column="<?=$release2_type?>"> <?=$rbutton_title2?></button>
                <?php


                $btn2 = ob_get_clean();

            $all_button = $this->getInvoiceButton($value, $user);

            $data['data'][] = [
                '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                $no,
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_date_format($value->date),
                show_number($value->amount,2),
                /*'<a href="#" class="inline-edit invoice-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/invoice/'.$value->id).'" >'.$value->comment.'</a>',*/
                $latest_comment.'<br>'.$comment_button,
                get_paid_checkbox($value->id,$value->need_to_pay),
                get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),
                $value->name,
                show_date_format($value->created_at),
                // $btn1.'<br>'.$selected_user_name,
                // $btn2,
                // $value->comment2,
                $all_button['btn_release_am'],
                $all_button['btn_release_hv'],
                $all_button['btn_release_usr'],
                $all_button['btn_release_falk'],
                '<button type="button" data-type="pending-invoice" data-id="'.$value->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>',
                $mail_button
            ];
            $no++;

        }
        return response()->json($data);
        // return $data;
    }

    public function getPendingAMInvoice($property_id)
    {
        //$property_id = $request->id;
        $login_user = Auth::user();
        $condition =  "1=1";

        if($login_user->role==9){
            $condition = 'pi.user_id='.$login_user->id;
        }
        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';

            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
            $condition = ' (pi.user_id = "'.$login_user->id.'")';
        }

        $res = DB::table('property_invoices as pi')
                    ->select('pi.*','u.name', 'p.name_of_property','u1.name as amname')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->leftJoin('users as u1', 'u1.id', '=', 'pi.email')
                    ->where('pi.property_id', $property_id)
                    ->where('pi.am_status', 3)
                    ->where('pi.last_process_type', 'am_status')
                    ->where('pi.deleted', 0)
                    ->whereRaw($condition)
                    ->get();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $user = Auth::user();
        $data['data'] = array();
        $no = 1;
        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_invoices')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Pending Rechnungen: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Pending Rechnungen: '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="PENDING AM RECHNUNGEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $list = array();
            if(isset($array[$value->id]))
                $list = $array[$value->id];


                $release2_type = "request2";
                // $rbutton_title2 = "Zur Freigabe an Falk senden";
                $rbutton_title2 = "Senden";

                $email = strtolower($user->email);

                $list = array();
                if(isset($array[$value->id]))
                    $list = $array[$value->id];



                    $selected_user_name = $value->amname;




                    $release1 = 'btn btn-primary invoice-release-request-am';
                    $release2 = 'btn btn-primary invoice-release-request';

                    $release1_type = "request1";
                    // $rbutton_title1 = "Zur Freigabe an $selected_user_name senden";
                    $rbutton_title1 = "Senden an $selected_user_name";

                    $email = strtolower($user->email);

                    $list = array();
                    if(isset($array[$value->id]))
                        $list = $array[$value->id];

                    $checkbox = "";
                    $btn3 = $btn4 = "";

                    if(!$value->email)
                        $value->email = 0;


                    if($user->id==$value->email)
                    {
                        $release1 =  'btn btn-primary invoice-release-request-am';
                        $rbutton_title1 = "Freigeben";
                        $release1_type = "release1";

                        if($list && isset($list['request2']))
                        {
                          $release2 =  " btn-success invoice-release-request";
                          $rbutton_title2 = "Zur Freigabe gesendet";
                        }
                    }



              else if($email==config('users.falk_email'))
                {
                    $rbutton_title2 = "Freigeben";
                    $release2 = 'btn btn-primary invoice-release-request';
                    $release2_type = "release2";
                }
                else{

                        if($list && isset($list['request1']))
                        {
                          $release1 =  " btn-success invoice-release-request-am";
                          $rbutton_title1 = "Zur Freigabe gesendet";
                        }

                        if($list && isset($list['request2']))
                        {
                          $release2 =  " btn-success invoice-release-request";
                          $rbutton_title2 = "Zur Freigabe gesendet";
                        }
                    }

                    $r1 = $r2 = "";
                    if($list && isset($list['release1']))
                    {
                      $release1 =  " btn-success";
                      $rbutton_title1 = "Freigegeben";
                      $dp = $selected_user_name;
                    }
                    $delete = 1;

                    if($list && isset($list['release2']))
                    {
                      $delete = 0;
                      $release2 =  " btn-success";
                      $rbutton_title2 = "Freigegeben";
                    }


                    ob_start();
                    ?><button data-user-id="<?=$value->email?>" data-id="<?=$value->id?>" type="button" class="btn <?=$release1?>" data-column="<?=$release1_type?>"><?=$rbutton_title1?></button>
                    <?php
                    $btn1 = ob_get_clean();


                ob_start();
                ?><button data-id="<?=$value->id?>" type="button" class="btn <?=$release2?>" data-column="<?=$release2_type?>"> <?=$rbutton_title2?></button>
                <?php


                $btn2 = ob_get_clean();

            $all_button = $this->getInvoiceButton($value, $user);

            $data['data'][] = [
                '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                $no,
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_date_format($value->date),
                show_number($value->amount,2),
                /*'<a href="#" class="inline-edit invoice-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/invoice/'.$value->id).'" >'.$value->comment.'</a>',*/
                $latest_comment.'<br>'.$comment_button,
                get_paid_checkbox($value->id,$value->need_to_pay),
                get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),
                $value->name,
                show_date_format($value->created_at),
                // $btn1.'<br>'.$selected_user_name,
                // $btn2,
                $all_button['btn_release_am'],
                $all_button['btn_release_hv'],
                $all_button['btn_release_usr'],
                $all_button['btn_release_falk'],
                // $value->comment2,
                '<button type="button" data-type="pending-am-invoice" data-id="'.$value->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>',
                $mail_button
            ];
            $no++;

        }
        return response()->json($data);
        // return $data;
    }

    public function getPendingHVInvoice($property_id){

        $login_user = Auth::user();
        $condition =  "1=1";

        if($login_user->role==9){
            $condition = 'pi.user_id='.$login_user->id;
        }
        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';

            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
            $condition = ' (pi.user_id = "'.$login_user->id.'")';
        }

        /*$field = ($request->has('field')) ? $request->field : '';
        $start_date = ($request->has('start_date') && $request->start_date != '') ? date('Y-m-d', strtotime(str_replace(".", "-", $request->start_date))) : '';
        $end_date = ($request->has('end_date') && $request->end_date != '') ? date('Y-m-d', strtotime(str_replace(".", "-", $request->end_date))) : '';*/

        $res = DB::table('property_invoices as pi')
                    ->select('pi.*','u.name', 'p.name_of_property','u1.name as amname')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->leftJoin('users as u1', 'u1.id', '=', 'pi.email')
                    ->where('pi.property_id', $property_id)
                    ->where('pi.hv_status', 3)
                    ->where('pi.last_process_type', 'hv_status')
                    ->where('pi.deleted', 0)
                    ->whereRaw($condition)
                    ->get();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $user = Auth::user();
        $data['data'] = array();
        $no = 1;

        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_invoices')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();

            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }

            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Pending Rechnungen: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Pending hv Rechnungen: '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="PENDING HV RECHNUNGEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $list = array();
            if(isset($array[$value->id]))
                $list = $array[$value->id];

            $release2_type = "request2";
            // $rbutton_title2 = "Zur Freigabe an Falk senden";
            $rbutton_title2 = "Senden";

            $email = strtolower($user->email);

            $list = array();
            if(isset($array[$value->id]))
                $list = $array[$value->id];

            $selected_user_name = $value->amname;

            $release1 = 'btn btn-primary invoice-release-request-am';
            $release2 = 'btn btn-primary invoice-release-request';

            $release1_type = "request1";
            // $rbutton_title1 = "Zur Freigabe an $selected_user_name senden";
            $rbutton_title1 = "Senden an $selected_user_name";

            $email = strtolower($user->email);

            $list = array();
            if(isset($array[$value->id]))
                $list = $array[$value->id];

            $checkbox = "";
            $btn3 = $btn4 = "";

            if($user->id==$value->email){
                $release1 =  'btn btn-primary invoice-release-request-am';
                $rbutton_title1 = "Freigeben";
                $release1_type = "release1";

                if($list && isset($list['request2'])){
                    $release2 =  " btn-success invoice-release-request";
                    $rbutton_title2 = "Zur Freigabe gesendet";
                }
            }else if($email==config('users.falk_email')){
                $rbutton_title2 = "Freigeben";
                $release2 = 'btn btn-primary invoice-release-request';
                $release2_type = "release2";
            }else{

                if($list && isset($list['request1'])){
                    $release1 =  " btn-success invoice-release-request-am";
                    $rbutton_title1 = "Zur Freigabe gesendet";
                }

                if($list && isset($list['request2'])){
                    $release2 =  " btn-success invoice-release-request";
                    $rbutton_title2 = "Zur Freigabe gesendet";
                }
            }

            $r1 = $r2 = "";
            if($list && isset($list['release1'])){
                $release1 =  " btn-success";
                $rbutton_title1 = "Freigegeben";
                $dp = $selected_user_name;
            }
            $delete = 1;

            if($list && isset($list['release2'])){
                $delete = 0;
                $release2 =  " btn-success";
                $rbutton_title2 = "Freigegeben";
            }

            $btn1 = '<button data-id="'.$value->id.'" type="button" class="btn '.$release1.'" data-column="'.$release1_type.'">'.$rbutton_title1.'</button>';
            $btn2 = '<button data-id="'.$value->id.'" type="button" class="btn '.$release2.'" data-column="'.$release2_type.'">'.$rbutton_title2.'</button>';

            $all_button = $this->getInvoiceButton($value, $user);

            $data['data'][] = [
                '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                $no,
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_date_format($value->date),
                show_number($value->amount,2),
                $latest_comment.'<br>'.$comment_button,
                get_paid_checkbox($value->id,$value->need_to_pay),
                get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),
                $value->name,
                show_date_format($value->created_at),
                // $btn1.'<br>'.$selected_user_name,
                // $btn2,
                $all_button['btn_release_am'],
                $all_button['btn_release_hv'],
                $all_button['btn_release_usr'],
                $all_button['btn_release_falk'],
                '<button type="button" data-type="pending-am-invoice" data-id="'.$value->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>',
                $mail_button
            ];
            $no++;
        }
        return response()->json($data);
    }

    public function getPendingUserInvoice($property_id){

        $login_user = Auth::user();
        $condition =  "1=1";

        if($login_user->role==9){
            $condition = 'pi.user_id='.$login_user->id;
        }
        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';

            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
                $condition = ' (pi.user_id = "'.$login_user->id.'")';
        }

        $res = DB::table('property_invoices as pi')
                    ->select('pi.*','u.name', 'p.name_of_property','u1.name as amname')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->leftJoin('users as u1', 'u1.id', '=', 'pi.email')
                    ->where('pi.property_id', $property_id)
                    ->where('pi.user_status', 3)
                    ->where('pi.last_process_type', 'user_status')
                    ->where('pi.deleted', 0)
                    ->whereRaw($condition)
                    ->get();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $user = Auth::user();
        $data['data'] = array();
        $no = 1;

        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_invoices')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();

            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }

            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Pending Rechnungen: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Pending User Rechnungen: '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="PENDING USER RECHNUNGEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $list = array();
            if(isset($array[$value->id]))
                $list = $array[$value->id];

            $release2_type = "request2";
            // $rbutton_title2 = "Zur Freigabe an Falk senden";
            $rbutton_title2 = "Senden";

            $email = strtolower($user->email);

            $list = array();
            if(isset($array[$value->id]))
                $list = $array[$value->id];

            $selected_user_name = $value->amname;

            $release1 = 'btn btn-primary invoice-release-request-am';
            $release2 = 'btn btn-primary invoice-release-request';

            $release1_type = "request1";
            // $rbutton_title1 = "Zur Freigabe an $selected_user_name senden";
            $rbutton_title1 = "Senden an $selected_user_name";

            $email = strtolower($user->email);

            $list = array();
            if(isset($array[$value->id]))
                $list = $array[$value->id];

            $checkbox = "";
            $btn3 = $btn4 = "";

            if($user->id==$value->email){
                $release1 =  'btn btn-primary invoice-release-request-am';
                $rbutton_title1 = "Freigeben";
                $release1_type = "release1";

                if($list && isset($list['request2'])){
                    $release2 =  " btn-success invoice-release-request";
                    $rbutton_title2 = "Zur Freigabe gesendet";
                }
            }else if($email==config('users.falk_email')){
                $rbutton_title2 = "Freigeben";
                $release2 = 'btn btn-primary invoice-release-request';
                $release2_type = "release2";
            }else{

                if($list && isset($list['request1'])){
                    $release1 =  " btn-success invoice-release-request-am";
                    $rbutton_title1 = "Zur Freigabe gesendet";
                }

                if($list && isset($list['request2'])){
                    $release2 =  " btn-success invoice-release-request";
                    $rbutton_title2 = "Zur Freigabe gesendet";
                }
            }

            $r1 = $r2 = "";
            if($list && isset($list['release1'])){
                $release1 =  " btn-success";
                $rbutton_title1 = "Freigegeben";
                $dp = $selected_user_name;
            }
            $delete = 1;

            if($list && isset($list['release2'])){
                $delete = 0;
                $release2 =  " btn-success";
                $rbutton_title2 = "Freigegeben";
            }

            $btn1 = '<button data-id="'.$value->id.'" type="button" class="btn '.$release1.'" data-column="'.$release1_type.'">'.$rbutton_title1.'</button>';
            $btn2 = '<button data-id="'.$value->id.'" type="button" class="btn '.$release2.'" data-column="'.$release2_type.'">'.$rbutton_title2.'</button>';

            $all_button = $this->getInvoiceButton($value, $user);

            $data['data'][] = [
                '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                $no,
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_date_format($value->date),
                show_number($value->amount,2),
                $latest_comment.'<br>'.$comment_button,
                get_paid_checkbox($value->id,$value->need_to_pay),
                get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),
                $value->name,
                show_date_format($value->created_at),
                // $btn1.'<br>'.$selected_user_name,
                // $btn2,
                $all_button['btn_release_am'],
                $all_button['btn_release_hv'],
                $all_button['btn_release_usr'],
                $all_button['btn_release_falk'],
                '<button type="button" data-type="pending-am-invoice" data-id="'.$value->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>',
                $mail_button
            ];
            $no++;
        }
        return response()->json($data);
    }

    public function getReleasePropertyInvoice(Request $request, $property_id){
        // $res = DB::table('property_invoices')->where('property_id', $property_id)->orderBy('id', 'desc')->get();

        $login_user = Auth::user();
        $condition =  "1=1";

        if($login_user->role==9){
            $condition = 'pi.user_id='.$login_user->id;
        }
        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';

            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
                $condition = ' (pi.user_id = "'.$login_user->id.'")';
        }


        $field = ($request->has('field')) ? $request->field : '';
        $start_date = ($request->has('start_date') && $request->start_date != '') ? date('Y-m-d', strtotime(str_replace(".", "-", $request->start_date))) : '';
        $end_date = ($request->has('end_date') && $request->end_date != '') ? date('Y-m-d', strtotime(str_replace(".", "-", $request->end_date))) : '';

        $res = DB::table('property_invoices as pi')
                    ->select('pi.*','pi.file_dirname','pi.amount', 'pi.invoice','pi.property_id', 'u.name', 'p.name_of_property', 'p.status', 'pi.file_basename', 'pi.file_type','pi.date', 'pi.comment as invoice_comment','pml.created_at','pml.comment','pi.is_paid','pi.need_to_pay')
                    ->join('properties_mail_logs as pml', 'pml.record_id', '=', 'pi.id')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->where('pi.property_id', $property_id)
                    ->where('pml.type','release2')
                    ->where('pi.falk_status', 2)
                    ->where('pi.last_process_type', 'falk_status')
                    ->whereRaw($condition)
                    ->where('pi.deleted', 0);

                    if($start_date != '' && $end_date != ''){
                        if($field == 'rechnungsdatum'){
                            $res->whereRaw(" (pi.date >= '".$start_date."' AND pi.date <= '".$end_date."') ");
                        }elseif($field == 'freigabedatum'){
                            $res->whereRaw(" (DATE_FORMAT(pml.created_at,'%Y-%m-%d') >= '".$start_date."' AND DATE_FORMAT(pml.created_at,'%Y-%m-%d') <= '".$end_date."') ");
                        }
                    }
                    $res = $res->groupBy('pi.id')->get();

        $user = Auth::user();

        $user_email = strtolower($user->email);
        $replyemailaddress = 't.raudies@fcr-immobilien.de';


        $pr_arr = array(3363,3417,3390,3394);

        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $data['data'] = [];

        $no = 1;
        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_invoices')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Rechnungsfreigabe: '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $amount_condition = 0;
            if(!in_array($value->property_id, $pr_arr))
                $amount_condition = 10000;

            if($value->property_id==121)
                $amount_condition = 30000;

            if($value->status!=6)
                $amount_condition = 0;

            if($value->property_id==3471  || $value->property_id==3363 || $value->property_id==3417 || $value->property_id==3394 || $value->property_id==3473 || $value->property_id==183 || $value->property_id==182 || $value->property_id==1197 || $value->property_id==3652 || $value->property_id==1194 || $value->property_id==3912 || $value->property_id==3925 || $value->property_id==4024)
                $amount_condition = 0;


            $cls = "";
            if($user_email==$replyemailaddress && $value->is_paid==0){
                $cls = "mark-as-paid";
                $amount_condition = 0;
            }

            // array(183,182,4267)
            if(($user_email=="j.lux@fcr-immobilien.de" || $user_email == 'n.eschenbach@fcr-immobilien.de') && $value->is_paid==0 && ($value->property_id==183 || $value->property_id==182 || $value->property_id==4267)){
                $cls = "mark-as-paid";
                $amount_condition = 0;
            }

            if($user_email=="gabriel@hotel-westerburg.de" && $value->is_paid==0){
                $cls = "mark-as-paid";
                $amount_condition = 0;
            }
            if($user_email == "courteaux@hotel-westerburg.de" && $value->property_id== 1197){
                $cls = "mark-as-paid";
                $amount_condition = 0;
            }
            if(($user->role==7 || $user->role==8) && $value->is_paid==0)
            {
                $cls = "mark-as-paid";
                $amount_condition = 0;
            }
            if($user->role == 10){
                $cls = "mark-as-paid";
            }



            $btn1 = "";
            if($cls):
                if($value->amount>$amount_condition):
                    ob_start();
            ?>
                    <button class="btn btn-primary btn-transfer-tr <?php echo $cls;?>" data-id="<?=$value->id?>">Offen</button>
            <?php
                    $btn1 = ob_get_clean();
                endif;
                if($value->is_paid==1 && $value->amount>$amount_condition):
                    ob_start();
                ?>
                    <button class="btn btn-success btn-transfer-tr">Erledigt</button>
                <?php
                    $btn1 = ob_get_clean();
                endif;
            else:
                if($value->is_paid==1)
                $btn1 = "<button class='btn btn-success btn-transfer-tr'>Erledigt</button>";
                else if($value->is_paid==0)
                $btn1 = "<button class='btn btn-primary btn-transfer-tr'>Offen</button>";
            endif;

            if(!$value->comment)
                $value->comment = "";

            $subject = 'Rechnungsfreigabe: '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="RECHNUNGSFREIGABEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $all_button = $this->getInvoiceButton($value, $user);

            $data['data'][] = [
                '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                $no,
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download">
                        <i class="fa fa-download fa-fw"></i>
                    </a>',
                show_date_format($value->date),
                show_number($value->amount,2),
                // $value->invoice_comment,
                $latest_comment.'<br>'.$comment_button,
                get_paid_checkbox($value->id,$value->need_to_pay),
                get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),

                $value->name,
                show_datetime_format($value->created_at),
                $value->comment,
                $all_button['btn_release_am'],
                $all_button['btn_release_hv'],
                $all_button['btn_release_usr'],
                $all_button['btn_release_falk'],
                $mail_button,
                //$dp,
                $btn1,
            ];
            $no++;
        }


        return response()->json($data);
    }

    public function deletePropertyInvoice($id){


        $model = PropertyInvoices::find($id);
        # $file= public_path(). "/invoice_upload/".$model->invoice;
        // File::delete($file);
        $model->deleted = 1;

        if($model->save()){

            $maillog = new PropertiesMailLog;
            $maillog->property_id = $model->property_id;
            $maillog->record_id = $id;
            $maillog->type = 'delete_invoice';
            $maillog->tab = 'invoice';
            $maillog->user_id = Auth::user()->id;
            $maillog->save();

            $result = ['status' => true, 'message' => 'Invoice delete successfully.', 'data' => []];
        }else{
            $result = ['status' => false, 'message' => 'Invoice delete fail! Try again.', 'data' => []];
        }
        return response()->json($result);
    }

    public function setPropertyInvoiceEmail($id = null, $email = null){
        $model = PropertyInvoices::find($id);
        $model->email = $email;
        if($model->save()){
            $result = ['status' => true, 'message' => 'Update successfully.', 'data' => []];
        }else{
            $result = ['status' => false, 'message' => 'Update fail! Try again.', 'data' => []];
        }
        return response()->json($result);
    }

    public function getInvoiceLogs($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role==9)
            $condition = 'property_invoices.user_id='.$login_user->id;

        if( !in_array($login_user->email, getConfidentialInvoiceUser()) )
        {
            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
            $condition = ' (property_invoices.user_id = "'.$login_user->id.'")';

        }


        $mails_logs = PropertiesMailLog::selectRaw('properties_mail_logs.id, properties_mail_logs.user_id, users.name,properties_mail_logs.created_at,property_invoices.email,properties_mail_logs.comment,type,file_basename,file_type,invoice, property_invoices.date,u1.name as amname')
        ->leftJoin('property_invoices','properties_mail_logs.record_id','=','property_invoices.id')->join('users','users.id','=','properties_mail_logs.user_id')->where('tab','invoice')
        ->leftJoin('users as u1','u1.id','=','property_invoices.email')->where('tab','invoice')
        ->where('property_invoices.property_id', $property_id)
        ->where('properties_mail_logs.property_id', $property_id)
        ->whereRaw($condition)
        ->orderBy('properties_mail_logs.id','desc')
        ->get();

        $data['data'] = [];

        if(!empty($mails_logs)){
            foreach ($mails_logs as $key => $value) {

                $selected_user_name = $value->amname;

                $rbutton_title1 = "Zur Freigabe an $selected_user_name senden";
                $rbutton_title2 = "Zur Freigabe an Falk senden";


                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }



                $btn1  = "";

                if($value->type=='request1')
                    $btn1 = $rbutton_title1;

                if($value->type=='request2')
                    $btn1 = $rbutton_title2;

                if($value->type=='request_invoice_am')
                    $btn1 = 'Zur Freigabe an AM senden';

                if($value->type=='request_invoice_hv')
                    $btn1 = 'Zur Freigabe an HV senden';


                if($value->type=='release1')
                    $btn1 = "Freigegeben";

                if($value->type=='release2')
                    $btn1 = "Freigegeben Falk";

                if($value->type=='release_invoice_hv')
                    $btn1 = "Freigegeben HV";

                if($value->type=='release_invoice_am')
                    $btn1 = "Freigegeben AM";


                if($value->type=='pending_invoice')
                    $btn1 = "Pending Falk Rechnungen";

                if($value->type=='pending_am_invoice' || $value->type=='pending_invoice_am')
                    $btn1 = "Pending AM Rechnungen";

                if($value->type=='pending_invoice_hv')
                    $btn1 = "Pending HV Rechnungen";

                if($value->type=='pending_invoice_user')
                    $btn1 = "Pending Rechnungen";


                if($value->type=='notrelease_invoice')
                    $btn1 = "Ablehnen Falk";

                if($value->type=='reject_invoice' || $value->type == 'not_release_invoice_am')
                    $btn1 = "Ablehnen AM";

                if($value->type=='not_release_invoice_hv')
                    $btn1 = "Ablehnen HV";

                if($value->type=='not_release_invoice_user')
                    $btn1 = "Ablehnen";

                if($value->type=='delete_invoice')
                    $btn1 = "Delete Rechnungen";


                $btn_delete = ($user->email == config('users.falk_email') && $value->user_id == $user->id) ? '<button type="button" data-id="'.$value->id.'" data-tbl="properties_mail_logs" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>' : '';

                $data['data'][] = [
                    $value->name,
                    $btn1,
                    '<a  target="_blank"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    show_date_format($value->date),
                    $value->comment,
                    show_datetime_format($value->created_at),
                    $btn_delete
                ];
            }
        }

        return response()->json($data);
    }

    public function invoiceMarkAsReject(Request $request){

        $update_data = DB::table('property_invoices')->where('id', $request->id)->update(['not_release_status' => 3, 'comment2' => $request->comment]);
        if($update_data){

            $invoice = DB::table('property_invoices as pi')
                        ->select('pi.id', 'pi.invoice', 'pi.file_basename', 'pi.file_type', 'pi.property_id', 'pi.created_at','u.name', 'u.email', 'p.name_of_property')
                        ->join('users as u', 'u.id', '=', 'pi.user_id')
                        ->join('properties as p', 'p.id', '=', 'pi.property_id')
                        ->where('pi.id', $request->id)
                        ->first();

            // Add mail log
            $modal = new PropertiesMailLog;
            $modal->property_id = $invoice->property_id;
            $modal->record_id = $request->id;
            $modal->type = 'reject_invoice';
            $modal->tab = 'invoice';
            if($request->comment)
                  $modal->comment = $request->comment;
            $modal->user_id = Auth::user()->id;
            $modal->save();

            $user = Auth::user();

            $glink = "https://drive.google.com/drive/u/2/folders/".$invoice->file_basename;
            if($invoice->file_type == "file"){
                $glink = "https://drive.google.com/file/d/".$invoice->file_basename;
            }
            $glink = '<a href="'.$glink.'">'.$glink.'</a>';

            $file_name = ($invoice->invoice) ? '('.$invoice->invoice.')' : '';


            $url = route('properties.show',['property'=>$invoice->property_id]).'?tab=property_invoice';
            $url = '<a href="'.$url.'">'.$url.'</a>';

            $text = "Hallo ".$invoice->name.",<br>deine Rechnung ".$file_name." ".$glink." wurde von ".$user->name." nicht freigegeben. Link zum Intranet: ".$url;
            if($request->comment){
                  $text .= "<br><br>Kommentar: ".$request->comment;
            }

            $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde nicht freigegeben";
            $email = $invoice->email;
            $name = $invoice->name;
            $replyemailaddress = $user->email;
            $replyname = $user->name;

            if( !in_array($email, $this->mail_not_send()) ){

                Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email, $name, $replyemailaddress, $replyname, $invoice) {
                    $message->to($email, $name)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->replyTo($replyemailaddress, $replyname)
                    ->subject($subject);
                });
            }

            $p = Properties::find($invoice->property_id);

            $aname = $asset_email = "";
            $am2_name = $am2_email = "";

            if(isset($p->asset_manager) && isset($p->asset_manager->name)){
                $aname = $p->asset_manager->name;
                $asset_email = $p->asset_manager->email;
            }

            if(in_array($p->id, $this->propertiesids()))
            {
                $p21 = $this->getdata21();
                $aname = $p21['name'];
                $asset_email = $p21['email'];
            }

            if(isset($p->asset_manager_two) && isset($p->asset_manager_two->name)){
                $am2_name = $p->asset_manager_two->name;
                $am2_email = $p->asset_manager_two->email;
            }

            $text = "Hallo ".$invoice->name.",<br> deine Rechnung ".$file_name." ".$glink." wurde von ".$user->name." nicht freigegeben. Link zum Intranet: ".$url;
            if($request->comment){
                  $text .= "<br><br>Kommentar: ".$request->comment;
            }
            $subject = "Rechnung vom ".show_date_format($invoice->created_at)." für ".$invoice->name_of_property." wurde nicht freigegeben";

            if($asset_email && $email != $asset_email && !in_array($asset_email, $this->mail_not_send()) ){//send asset_m_id

                $email = $asset_email;
                $name = $aname;

                Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email, $name, $replyemailaddress, $replyname, $invoice) {
                    $message->to($email, $name)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->replyTo($replyemailaddress, $replyname)
                    ->subject($subject);
                });
            }

            if($am2_email && $am2_email != $email && $am2_email != $asset_email && !in_array($am2_email, $this->mail_not_send()) ){//send asset_m_id2

                $email = $am2_email;
                $name = $am2_name;

                Mail::send('emails.general', ['text' => $text], function ($message) use($subject,$email, $name, $replyemailaddress, $replyname, $invoice) {
                    $message->to($email, $name)
                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                    ->replyTo($replyemailaddress, $replyname)
                    ->subject($subject);
                });
            }

      }
      echo "1"; die;
    }

    public function getNotReleaseInvoiceAM($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";

        if($login_user->role==9){
            $condition = 'pi.user_id='.$login_user->id;
        }
        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';


            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
            $condition = ' (pi.user_id = "'.$login_user->id.'")';
        }


        $res = DB::table('property_invoices as pi')
                    ->select('pi.*','u.name', 'p.name_of_property')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->where('pi.property_id', $property_id)
                    ->where('pi.am_status', 4)
                    ->where('pi.last_process_type', 'am_status')
                    ->whereRaw($condition)
                    ->get();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $data['data'] = array();
        $no = 1;
        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_invoices')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Nicht Freigegeben (AM): '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Nicht Freigegeben (AM): '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="NICHT FREIGEGEBEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $all_button = $this->getInvoiceButton($value, $user);

            $data['data'][] = [
                '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                $no,
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_date_format($value->date),
                show_number($value->amount,2),
                $value->name,
                /*'<a href="#" class="inline-edit invoice-comment" data-type="textarea" data-pk="comment" data-placement="bottom" data-url="'.url('property/update/invoice/'.$value->id).'" >'.$value->comment.'</a>',*/
                $latest_comment.'<br>'.$comment_button,
                get_paid_checkbox($value->id,$value->is_paid),
                get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),
                show_date_format($value->created_at),
                // $value->comment2,
                $all_button['btn_release_am'],
                $all_button['btn_release_hv'],
                $all_button['btn_release_usr'],
                $all_button['btn_release_falk'],
                '<button type="button" data-type="not-release-invoice-am" data-id="'.$value->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>',
                $mail_button
            ];
            $no++;
        }
        return response()->json($data);
    }

    public function getNotReleaseInvoiceHV($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";

        if($login_user->role==9){
            $condition = 'pi.user_id='.$login_user->id;
        }
        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';

            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
                $condition = ' (pi.user_id = "'.$login_user->id.'")';
        }



        $res = DB::table('property_invoices as pi')
                    ->select('pi.*','u.name', 'p.name_of_property')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->where('pi.property_id', $property_id)
                    ->where('pi.hv_status', 4)
                    ->where('pi.last_process_type', 'hv_status')
                    ->whereRaw($condition)
                    ->get();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $data['data'] = array();
        $no = 1;
        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_invoices')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Nicht Freigegeben (HV): '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Nicht Freigegeben (AM): '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="NICHT FREIGEGEBEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $all_button = $this->getInvoiceButton($value, $user);

            $data['data'][] = [
                '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                $no,
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_date_format($value->date),
                show_number($value->amount,2),
                $value->name,
                $latest_comment.'<br>'.$comment_button,
                get_paid_checkbox($value->id,$value->is_paid),
                get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),
                show_date_format($value->created_at),
                $all_button['btn_release_am'],
                $all_button['btn_release_hv'],
                $all_button['btn_release_usr'],
                $all_button['btn_release_falk'],
                '<button type="button" data-type="not-release-invoice-am" data-id="'.$value->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>',
                $mail_button
            ];
            $no++;
        }
        return response()->json($data);
    }

    public function getNotReleaseInvoiceUser($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";

        if($login_user->role==9){
            $condition = 'pi.user_id='.$login_user->id;
        }
        if( !in_array($login_user->email, getConfidentialInvoiceUser()) ){
            $condition .= ' AND (pi.is_confidential_invoice = 0 OR pi.user_id = "'.$login_user->id.'")';

            if(isset($_REQUEST['internal']) && $_REQUEST['internal'])
                $condition = ' (pi.user_id = "'.$login_user->id.'")';
        }

        $res = DB::table('property_invoices as pi')
                    ->select('pi.*','u.name', 'p.name_of_property')
                    ->join('properties as p', 'p.id', '=', 'pi.property_id')
                    ->join('users as u', 'u.id', '=', 'pi.user_id')
                    ->where('pi.property_id', $property_id)
                    ->where('pi.user_status', 4)
                    ->where('pi.last_process_type', 'user_status')
                    ->whereRaw($condition)
                    ->get();

        $taburl = route('properties.show',['property' => $property_id]).'?tab=property_invoice';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $data['data'] = array();
        $no = 1;
        foreach ($res as $key => $value) {

            $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                        ->join('users as u', 'u.id', 'pc.user_id')
                        ->where('pc.record_id', $value->id)
                        ->where('pc.type', 'property_invoices')
                        ->orderBy('pc.created_at', 'desc')
                        ->limit(2)->get();
            $latest_comment = '';
            if($comments && count($comments)){
                $latest_comment .= '<div class="show_property_cmnt_section">';
                foreach ($comments as $n => $comment) {
                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                    $commented_user = $comment->name.''.$company;
                    $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                }
                $latest_comment .= '</div>';
            }
            if($latest_comment){
                $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_comment') .'?property_id='.$property_id.'&record_id='.$value->id.'&type=property_invoices" class="load_property_comment_section" data-closest="td">Show More</a>';
            }

            $content = $taburl;

            $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="'.$value->id.'" data-property-id="'.$property_id.'" data-type="property_invoices" data-subject="Nicht Freigegeben (User): '.$value->name_of_property.'" data-content="'.$content.'">Kommentar</button>';

            $subject = 'Nicht Freigegeben (User): '.$value->name_of_property;

            $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="NICHT FREIGEGEBEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>';

            $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
            $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
            }

            $all_button = $this->getInvoiceButton($value, $user);

            $data['data'][] = [
                '<input type="checkbox" class="row_checkbox" value="'.$value->id.'">',
                $no,
                '<a  target="_blank"  title="'.$value->invoice.'"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                show_date_format($value->date),
                show_number($value->amount,2),
                $value->name,
                $latest_comment.'<br>'.$comment_button,
                get_paid_checkbox($value->id,$value->is_paid),
                get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2),
                show_date_format($value->created_at),
                $all_button['btn_release_am'],
                $all_button['btn_release_hv'],
                $all_button['btn_release_usr'],
                $all_button['btn_release_falk'],
                '<button type="button" data-type="not-release-invoice-am" data-id="'.$value->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>',
                $mail_button
            ];
            $no++;
        }
        return response()->json($data);
    }

    public function getDealLogs($property_id){

        $mails_logs = PropertiesMailLog::selectRaw('users.name,properties_mail_logs.created_at,properties_mail_logs.comment,type,file_basename,file_type,invoice, termination_date')->join('properties_deals','properties_mail_logs.record_id','=','properties_deals.id')->
        join('users','users.id','=','properties_mail_logs.user_id')->where('tab','deals')
        ->where('properties_deals.property_id', $property_id)
        ->where('properties_mail_logs.property_id', $property_id)
        ->orderBy('properties_mail_logs.id','desc')
        ->get();

        // pre($mails_logs);
        $data['data'] = [];

        if(!empty($mails_logs)){
            foreach ($mails_logs as $key => $value) {

                $selected_user_name = $value->name;
                $rbutton_title1 = "Zur Freigabe an Falk senden";


                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }



               $btn1  = "";
               if($value->type=='deal_request')
               {
                    $btn1 = $rbutton_title1;
                }
               if($value->type=='deal_release')
               {
                    $btn1 = "Freigegeben";
               }

                $data['data'][] = [
                    $value->name,
                    $btn1,
                    '<a  target="_blank"  href="'.$download_path.'">'.$value->invoice.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    $value->comment,
                    show_datetime_format($value->created_at),
                    ($value->termination_date != '') ? show_date_format($value->termination_date) : '',
                ];
            }
        }

        return response()->json($data);
    }

    public function update_invoice_by_field(Request $request, $prop_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated property'
        ];

        $property = PropertyInvoices::where('id', '=', $prop_id)->first();

        if (empty($property)) {
            $response['msg'] = 'Not found property';
            return response()->json($response);
        }

        // Clone property
        // $input = (array)$property;
        $update_data = DB::table('property_invoices')->where('id', $prop_id)
                    ->update([
                $request->pk => $request->value]);
        $response = [
                'success' => true,
                'msg' => '',
                'reload' => false
            ];

        return response()->json($response);
    }

    public function updateInsuranceByField(Request $request, $id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated record'
        ];

        $property_field_array = ['gebaude_comment', 'haftplicht_comment', 'gebaude_betrag', 'haftplicht_betrag', 'haftplicht_laufzeit_from', 'haftplicht_laufzeit_to', 'gebaude_laufzeit_from', 'gebaude_laufzeit_to', 'haftplicht_kundigungsfrist', 'gebaude_kundigungsfrist'];
        $property_insurances_field_array = ['note', 'amount', 'date_from', 'date_to', 'kundigungsfrist'];

        if(in_array($request->pk, $property_field_array)){

            $value = $request->value;
            if($request->pk == 'gebaude_betrag' || $request->pk == 'haftplicht_betrag'){
                $amount = str_replace('.', '', $value);
                $amount = str_replace(',', '.', $amount);
                $value = ($amount != '') ? $amount : 0;
            }

            if($request->pk == 'haftplicht_laufzeit_from' || $request->pk == 'haftplicht_laufzeit_to' || $request->pk == 'gebaude_laufzeit_from' || $request->pk == 'gebaude_laufzeit_to'){
                $value = ($value != '') ? date('Y-m-d', strtotime(str_replace(".", "-", $value))) : null;
            }

            $res = DB::table('properties')->where('id', $id)->update([$request->pk => $value]);

        }elseif(in_array($request->pk, $property_insurances_field_array)){

            $value = $request->value;
            if($request->pk == 'amount'){
                $amount = str_replace('.', '', $value);
                $amount = str_replace(',', '.', $amount);
                $value = ($amount != '') ? $amount : 0;
            }

            if($request->pk == 'date_from' || $request->pk == 'date_to'){
                $value = ($value != '') ? date('Y-m-d', strtotime(str_replace(".", "-", $value))) : null;
            }

            $res = DB::table('property_insurances')->where('id', $id)->update([$request->pk => $value]);

        }

        if($res){
           $response = ['success' => true, 'msg' => 'Record update successfully'];
        }

        return response()->json($response);
    }

    public function addPropertyInsuranceTitle(Request $request, $property_id){
        if($request->ajax()) {

            $rules = array('title' => 'required', 'property_id' => 'required');
            $inputs = array('title' => $request->title, 'property_id' => $property_id);
            $validator = Validator::make($inputs, $rules);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{
                $modal = new PropertyInsuranceTab;
                $modal->property_id = $property_id;
                $modal->user_id = Auth::user()->id;
                $modal->title = trim($request->title);
                if($modal->save()){
                    $result = ['status' => true, 'message' => 'Title add successfully.', 'data' => $modal->toArray()];
                }else{
                    $result = ['status' => false, 'message' => 'Title add fail! Try again.', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function addPropertyInsuranceDetail(Request $request, $id = null){
        if($request->ajax()) {

            $rules = array('file_basename' => 'required', 'amount' => 'required');
            if($request->hasFile('insurance_tab_gdrive_file_upload')){
                $rules = array('amount' => 'required');
            }

            $inputs = array(
                'file_basename' => $request->file_basename,
                'file_dirname' => $request->file_dirname,
                'file_type' => $request->file_type,
                'file_name' => $request->file_name,
                'amount' => trim($request->amount)
            );


            $validator = Validator::make($inputs, $rules);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{
                if($id && $id != ''){
                    $modal = PropertyInsuranceTabDetails::find($id);
                }else{
                    $modal = new PropertyInsuranceTabDetails;
                    $modal->property_insurance_tab_id = $request->property_insurance_tab_id;
                    $modal->user_id = Auth::user()->id;
                }
                $amount = 0;
                if($request->amount){
                    $amount = str_replace('.', '', $request->amount);
                    $amount = str_replace(',', '.', $amount);
                }
                $modal->amount = $amount;
                $modal->comment = trim($request->comment);


                if($request->hasFile('insurance_tab_gdrive_file_upload')){

                    $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
                    if(!$propertyMainDir){
                        $res = PropertiesHelperClass::createPropertyDirectories($request->property_id);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
                    }
                    $propertyInsuranceDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Angebote')->first();
                    if(!$propertyInsuranceDir){
                        $res = PropertiesHelperClass::createPropertySubDirectories($propertyMainDir, $request->property_id, ['Angebote']);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property Angebote directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyInsuranceDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Angebote')->first();
                    }


                    $file = $request->file('insurance_tab_gdrive_file_upload');
                    $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $originalExt = $file->getClientOriginalExtension();
                    $folder = $propertyInsuranceDir->dir_path;
                    $fileData = File::get($file);

                    //upload file
                    $file2 =  Storage::cloud()->put($folder.'/'.$originalName.'.'.$originalExt, $fileData);

                    //get google drive id
                    $contents = collect(Storage::cloud()->listContents($folder, false));
                    $file = $contents->where('type','file')->where('filename', $originalName)->where('extension', $originalExt) ->first();
                    if($file['basename'] == ""){
                        $result = ['status' => false, 'message' => 'File upload fail.', 'data' => []];
                        return response()->json($result);
                    }

                    $modal->file_name = $file['name'];
                    $modal->file_basename = $file['basename'];
                    $modal->file_dirname = $file['dirname'];
                    $modal->file_type = $file['type'];


                }else{
                    $modal->file_name = $request->file_name;
                    $modal->file_basename = $request->file_basename;
                    $modal->file_dirname = $request->file_dirname;
                    $modal->file_type = $request->file_type;
                }


                if($modal->save()){

                    // Save comment
                    if($request->comment){
                        $commentModal = new PropertiesComment;
                        $commentModal->property_id = $request->property_id;
                        $commentModal->user_id = Auth::user()->id;
                        $commentModal->comment = $request->comment;
                        $commentModal->record_id = $modal->id;
                        $commentModal->type = 'property_insurance_tab_details';
                        $commentModal->save();
                    }

                    $user = Auth::user();
                    if($user->role >= 6){

                    $glink = "https://drive.google.com/drive/u/2/folders/".$modal->file_basename;
                    if($modal->file_type == "file"){
                        $glink = "https://drive.google.com/file/d/".$modal->file_basename;
                    }
                    $glink = '<a href="'.$glink.'">'.$glink.'</a>';

                    $p = Properties::find($request->property_id);

                    $url = route('properties.show',['property'=>$request->property_id]).'?tab=property_insurance_tab';
                    $url = '<a href="'.$url.'">'.$url.'</a>';

                    $subject = "Angebotsfreigabe";
                    $text = 'Ein neues Angebot wurde für das Objekt '.$p->plz_ort.' '.$p->ort." hochgeladen und wartet auf Freigabe: ".$url;

                    $text .= "<br><br>Link: ".$glink;

                    if($request->comment)
                        $text .= "<br><br>Kommentar: ".$request->comment;


                    $name = $name2 = $email = $email2 = "";
                    if(isset($p->asset_manager) && $p->asset_manager){
                        $name = $p->asset_manager->name;
                        $email = $p->asset_manager->email;
                    }
                    if(in_array($p->id, $this->propertiesids()))
                    {
                        $p21 = $this->getdata21();
                        $name = $p21['name'];
                        $email = $p21['email'];
                    }
                    if(isset($p->asset_manager_two) && $p->asset_manager_two){
                        $name2 = $p->asset_manager_two->name;
                        $email2 = $p->asset_manager_two->email;
                    }

                    if( $email && !in_array($email, $this->mail_not_send()) ){

                        Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject) {
                            $message->to($email)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->subject($subject);
                        });
                    }

                    if( $email2 && $email2 != $email && !in_array($email2, $this->mail_not_send()) ){

                         Mail::send('emails.general', ['text' => $text], function ($message) use($email2,$subject) {
                            $message->to($email2)
                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                            ->subject($subject);
                        });
                    }


                }



                    $result = ['status' => true, 'message' => 'Record add successfully.', 'data' => $modal->toArray()];
                }else{
                    $result = ['status' => false, 'message' => 'Record add fail! Try again.', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getPropertyInsuranceDetail(Request $request, $tab_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role >= 6)
            $condition = 'pid.user_id='.$login_user->id;

        $detail = DB::table('property_insurance_tab_details as pid')
                            ->select('pid.*','u.name as user_name', 'p.name_of_property', 'pit.property_id',
                                DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurancetab_release' AND selected_id = pid.id AND tab='insurancetab2' ) THEN 1 ELSE 0 END as is_release"),
                                DB::raw("(SELECT properties_mail_logs.created_at FROM properties_mail_logs WHERE (type = 'insurance_release' OR type = 'insurancetab_release') AND selected_id = pid.id AND (tab='insurance_tab' OR tab='insurancetab2') ORDER BY properties_mail_logs.created_at DESC LIMIT 1) as release_date")
                            )
                            ->join('property_insurance_tabs as pit', 'pit.id', '=', 'pid.property_insurance_tab_id')
                            ->join('properties as p', 'p.id', '=', 'pit.property_id')
                            ->join('users as u', 'u.id', '=', 'pid.user_id')
                            ->where('property_insurance_tab_id', $tab_id)
                            ->where('pid.deleted', 0)
                            ->whereRaw($condition);
                            if($request->type == 'Von Falk freigegeben'){
                                $detail = $detail->orderBy('release_date', 'DESC');
                            }else{
                                $detail = $detail->orderBy('id', 'DESC');
                            }
                            $detail = $detail->get();
        $res['data'] = [];
        $user = Auth::user();

        if(count($detail) > 0){
            foreach ($detail as $key => $value) {

                $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                            ->join('users as u', 'u.id', 'pc.user_id')
                            ->where('pc.record_id', $value->id)
                            ->where('pc.type', 'property_insurance_tab_details')
                            ->orderBy('pc.created_at', 'desc')
                            ->limit(2)->get();
                $latest_comment = '';
                if($comments){
                    $latest_comment .= '<div class="show_cmnt">';
                    foreach ($comments as $n => $comment) {
                        $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                        $commented_user = $comment->name.''.$company;
                        $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
                    }
                    $latest_comment .= '</div>';
                }
                if($latest_comment){
                    $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_insurance_detail_comment', ['id' => $value->id]) .'" class="load_comment">Show More</a>';
                }

                $subject = $value->name_of_property;

                $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$value->property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="" data-reload="0" data-section="property_insurance_tab_details">Weiterleiten an</button>';

                $delete_btn = '<button type="button" data-url="'. route('delete_property_insurance_detail', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm delete-insurance-tab-detail"><i class="icon-trash"></i></button>';

                $edit_btn = '<button type="button" data-id="'. $value->id .'" data-url="'. route('get_property_insurance_detail_by_id', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm edit-insurance-tab-detail"><i class="fa fa-edit"></i></button>';

                $am_checkbox = '<input data-id='.$value->id.' type="checkbox" class="am_falk_status" data-field="asset_manager_status" data-url="'. route('update_property_insurance_status', ['id' => $value->id]) .'" '. ( ($value->asset_manager_status == 1) ? 'checked' : '' ) .'>';


                $falk_checkbox = "";
                $not_release = "";
                $not_release_am = "";
                $pending = "";
                if(in_array($value->property_insurance_tab_id, array(4,6,7,13,11,14,10,17,24,18,12,25,27,28))){
                    // $falk_checkbox = '<input data-id='.$value->id.' type="checkbox" class="am_falk_status" data-field="falk_status" data-url="'. route('update_property_insurance_status', ['id' => $value->id]) .'" '. ( ($value->falk_status == 1) ? 'checked' : '' ) .'>';



                        $falk_checkbox = '<input data-id='.$value->id.' type="checkbox" class="" data-field="falk_status" data-url="'. route('update_property_insurance_status', ['id' => $value->id]) .'" '. ( ($value->falk_status == 1) ? 'checked' : '' ) .'>';


                }
                else
                {
                    if($user->email==config('users.falk_email'))
                    {
                        $rbutton_title = "Freigeben";
                        $release = 'btn btn-xs btn-primary property-insurance-release-request';
                        $release_type = "insurancetab_release";
                        $falk_checkbox = '<button data-id="'.$value->id.'" type="button" class="btn '.$release.'" data-column="'.$release_type.'">'.$rbutton_title.'</button>';

                        if($value->not_release == 0 && $value->is_release == 0)
                        {
                            $not_release = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-primary btn-not-release-insurance">Ablehnen</button>';
                            $not_release_am = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-primary btn-not-release-am-insurance">Ablehnen AM</button>';
                            $pending = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-primary btn-pending-insurance">Pending</button>';
                        }
                    }

                    if($value->is_release)
                    {
                        $falk_checkbox = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-success">Freigegeben</button>';
                    }



                }

                if($value->is_release)
                {
                    $delete_btn = $edit_btn = "";
                    $am_checkbox = '<input data-id='.$value->id.' type="checkbox" class="" data-field="asset_manager_status" data-url="'. route('update_property_insurance_status', ['id' => $value->id]) .'" '. ( ($value->asset_manager_status == 1) ? 'checked' : '' ) .'>';

                    // $falk_checkbox = '<input data-id='.$value->id.' type="checkbox" class="" data-field="falk_status" data-url="'. route('update_property_insurance_status', ['id' => $value->id]) .'" '. ( ($value->falk_status == 1) ? 'checked' : '' ) .'>';

                }
                $file_basename = '';
                if(isset($value->file_basename))
                    $file_basename = $value->file_basename;


                $download_path = "https://drive.google.com/drive/u/2/folders/".$file_basename;
                $downlod_file_path = '';
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".$file_basename;
                }

                $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-ins-comment" data-id="'.$value->id.'" data-url="'.route('get_property_insurance_detail_comment', ['id' => $value->id]).'">Kommentar</button>';

                $res['data'][] = [
                    ($key+1),
                    '<a  target="_blank"  title="'.$value->file_name.'"  href="'.$download_path.'">'.$value->file_name.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    '<a href="javascript:void(0);" class="inline-edit '. ( ($user->email == config('users.falk_email')) ? 'angebote-comment' : '' ) .'" data-type="text" data-pk="amount" data-placement="bottom" data-inputclass="mask-input-number" data-url="'. route('update_property_insurance_detail_by_field', ['id' => $value->id]) .'" >'.show_number($value->amount,2).'</a>',
                    $latest_comment.'<br>'.$comment_button,
                    $value->user_name,
                    show_datetime_format($value->created_at),
                    show_datetime_format($value->release_date),
                    $not_release_am,
                    '<a href="javascript:void(0);" class="inline-edit '. ( ($user->email == config('users.falk_email')) ? 'angebote-comment' : '' ) .'" data-type="textarea" data-pk="falk_comment" data-placement="bottom" data-url="'. route('update_property_insurance_detail_by_field', ['id' => $value->id]) .'" >'.$value->falk_comment.'</a>',
                    $am_checkbox,
                    $falk_checkbox,
                    $pending,
                    $not_release,
                    $delete_btn,
                    $mail_button
                ];
            }
        }
        return response()->json($res);
    }

    public function updatePropertyInsuranceDetailByField(Request $request, $id) {

        $response = [
            'success' => false,
            'msg' => 'Failed to updated Record!'
        ];

        $detail = DB::table('property_insurance_tab_details as pitd')
                            ->selectRaw('u.email as user_email, u.name as user_name, am.email as am_email, am.name as am_name, pitd.file_type, pitd.file_basename, p.name_of_property, pit.title')
                            ->join('property_insurance_tabs as pit', 'pitd.property_insurance_tab_id', '=', 'pit.id')
                            ->join('properties as p', 'p.id', '=', 'pit.property_id')
                            ->join('users as u', 'u.id', '=', 'pitd.user_id')
                            ->leftjoin('users as am', 'am.id', '=', 'p.asset_m_id')
                            ->where('pitd.id', '=', $id)
                            ->first();

        if (empty($detail)) {
            $response['msg'] = 'Record not found!';
            return response()->json($response);
        }

        $value = $request->value;

        if($request->pk == 'amount' && $value){
            $value = str_replace('.', '', $value);
            $value = str_replace(',', '.', $value);
        }

        $update_data = DB::table('property_insurance_tab_details')->where('id', $id)->update([$request->pk => $request->value]);
        if($update_data){

            if($request->pk == 'falk_comment' && $detail->user_email && $request->value){

                $download_path = "https://drive.google.com/drive/u/2/folders/".$detail->file_basename;
                if($detail->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".$detail->file_basename;
                }

                $subject = 'Angebot : '.$detail->name_of_property.' '.$detail->title;
                $text = 'Kommentar: '.$request->value.'<br>Datei: '.$download_path;

                $email = $detail->user_email;
                $cc_email = $detail->am_email;
                $reply_email = config('users.falk_email');

                if( !in_array($email, $this->mail_not_send()) ){

                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $cc_email, $reply_email) {
                        $message->to($email)
                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                        ->replyTo($reply_email, 'Falk')
                        ->cc($cc_email)
                        ->subject($subject);
                    });
                }
            }

            $response = [
                'success' => true,
                'msg' => 'Record update successfully'
            ];
        }
        return response()->json($response);
    }

    public function getPropertyInsuranceDetailById($id){
        $res = PropertyInsuranceTabDetails::select('id', 'amount', 'comment')->where('id', $id)->first();
        if($res->amount){
            $res->amount = show_number($res->amount,2);
        }
        return response()->json($res);
    }

    public function deletePropertyInsuranceDetail($id){
        $model = PropertyInsuranceTabDetails::find($id);
        $model->deleted = 1;

        $modal_mail_log = new PropertiesMailLog;
        $modal_mail_log->property_id = $model->tab_title->property_id;
        $modal_mail_log->record_id = $model->tab_title->id;
        $modal_mail_log->selected_id = $id;
        $modal_mail_log->type = 'delete_insurance_tab_details';
        $modal_mail_log->tab = 'insurance_tab';
        $modal_mail_log->user_id = Auth::user()->id;
        $modal_mail_log->save();

        if($model->save()){
            $result = ['status' => true, 'message' => 'Record delete successfully.', 'data' => []];
        }else{
            $result = ['status' => false, 'message' => 'Record delete fail! Try again.', 'data' => []];
        }
        return response()->json($result);
    }

    public function deletePropertyInsuranceTitle($id){
        $check = PropertiesMailLog::where('type','insurance_release')->where('record_id',$id)->where('tab','insurance_tab')->first();
        if(!$check){

            $model = PropertyInsuranceTab::find($id);
            $model->deleted = 1;

            $modal_mail_log = new PropertiesMailLog;
            $modal_mail_log->property_id = $model->property_id;
            $modal_mail_log->record_id = $id;
            $modal_mail_log->type = 'delete_insurance_tab';
            $modal_mail_log->tab = 'insurance_tab';
            $modal_mail_log->user_id = Auth::user()->id;
            $modal_mail_log->save();

            if($model->save()){
                $result = ['status' => true, 'message' => 'Record delete successfully.', 'data' => []];
            }else{
                $result = ['status' => false, 'message' => 'Record delete fail! Try again.', 'data' => []];
            }
        }else{
            $result = ['status' => false, 'message' => 'Record delete fail!', 'data' => []];
        }
        return response()->json($result);
    }

    public function get_angebot_button(Request $request)
    {
        $data = PropertyInsuranceTab::select('id', 'title')->where('property_id', $request->id)->get();
        $user = Auth::user();

        $logs = PropertiesMailLog::where('property_id',$request->id)->where('tab','insurance_tab')->get();
        $array = array();
        foreach ($logs as $key => $value) {
            $array[$value->record_id][$value->type] = 1;
        }
        // pre($array);
        // $json['array'] = $array;
        $json['btn'] = array();
        foreach ($data as $key => $value) {

            $release = 'btn btn-xs btn-primary property-insurance-release-request';
            $release_type = "insurance_request";
            $rbutton_title = "Zur Freigabe an Falk senden";

            $email = strtolower($user->email);

            $list = array();
            if(isset($array[$value->id]))
                $list = $array[$value->id];
            if($email==config('users.falk_email'))
            {
                $rbutton_title = "Freigeben";
                $release = 'btn btn-xs btn-primary property-insurance-release-request';
                $release_type = "insurance_release";
            }
            else{
                if($list && isset($list['insurance_request']))
                {
                  $release =  " btn-xs btn-success property-insurance-release-request";
                  $rbutton_title = "Zur Freigabe gesendet";
                }
            }
            if($list && isset($list['insurance_release']))
            {
              $release =  " btn-success btn-xs";
              $rbutton_title = "Freigegeben";
            }
            $btn = "";
            ob_start();
            ?><button data-id="<?=$value->id?>" type="button" class="btn <?=$release?>" data-column="<?=$release_type?>"><?=$rbutton_title?></button>
            <?php
            if($email==config('users.falk_email')): ?>
            <button data-id="<?=$value->id?>" type="button" class="btn btn-xs btn-primary deal-mark-as-not-release">Ablehnen</button>
            <?php
            endif;

            $btn = ob_get_clean();

            $json['btn'][$value->id] = $btn;
        }
        return $json;
    }

    public function updatePropertyInsuranceStatus(Request $request, $id){

        $res = PropertyInsuranceTabDetails::where('id', $id)->update([$request->field => $request->status]);
        if($res){
            $result = ['status' => true, 'message' => 'Status update successfully.', 'data' => []];
        }else{
            $result = ['status' => false, 'message' => 'Status update fail!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getPropertyInsuranceDetailLog($property_id){

        $login_user = $user = Auth::user();

        $condition =  "1=1";
        if($login_user->role==9)
            $condition = 'pitd.user_id='.$login_user->id;


        $mails_logs = DB::table('properties_mail_logs as pml')
                        ->selectRaw('pml.id, pml.user_id, u.name, pml.created_at, pit.title, pml.comment, type, file_basename, file_type, file_name,file_dirname')
                        ->Join('property_insurance_tabs as pit','pml.record_id','=','pit.id')
                        ->leftjoin('property_insurance_tab_details as pitd','pml.selected_id','=','pitd.id')
                        ->join('users as u','u.id','=','pml.user_id')
                        ->whereRaw('(tab="insurance_tab" OR tab="insurancetab2")')
                        ->where('pit.property_id', $property_id)
                        ->where('pml.property_id', $property_id)
                        ->whereRaw($condition)
                        ->orderBy('pml.id','desc')
                        ->get();

        $data['data'] = [];

        $user = Auth::user();

        if(!empty($mails_logs)){
            foreach ($mails_logs as $key => $value) {

                $download_path = "https://drive.google.com/drive/u/2/folders/". (isset($value->file_basename) ? $value->file_basename : '');
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }

                $btn1  = "";
                if($value->type=='insurance_request'){
                    $btn1 = "Zur Freigabe an Falk senden";
                }
                if($value->type=='insurance_release' || $value->type=='insurancetab_release'){
                    $btn1 = "Freigegeben";
                }
                if($value->type=='delete_insurance_tab' || $value->type=='delete_insurance_tab_details'){
                    $btn1 = "Delete";
                }
                if($value->type=='insurance_notrelease'){
                    $btn1 = "Ablehnen";
                }
                if($value->type=='insurance_notrelease_am'){
                    $btn1 = "Ablehnen AM";
                }
                if($value->type=='insurance_pending'){
                    $btn1 = "Pending";
                }

                $btn_delete = ($user->email == config('users.falk_email') && $value->user_id == $user->id) ? '<button type="button" data-id="'.$value->id.'" data-tbl="properties_mail_logs" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>' : '';

                $data['data'][] = [
                    ($key+1),
                    $value->title,
                    $value->name,
                    $btn1,
                    '<a  target="_blank"  href="'.$download_path.'">'.$value->file_name.'</a> <a target="_blank" href="'.$downlod_file_path.'" title="Download"><i class="fa fa-download fa-fw"></i></a>',
                    $value->comment,
                    show_datetime_format($value->created_at),
                    $btn_delete
                ];
            }
        }

        return response()->json($data);
    }


    public function addNote(Request $request)
    {
        email_template2_sending_info::where('id',$request->banken_id)->first()->update([
                'notizen' => $request->note
        ]);
    }

    public function deleteBankEmailLog($id)
    {
        email_template2_sending_info::where('id',$id)->delete();

        return response()->json(200);
    }

    public function importRentPaid(Request $request, $property_id){

        // echo $request->no_opos_status;die;
        // pre($request->file('excel'));
        if ($request->hasFile('excel')) {


                $modal = new RentPaidExcelUpload;
                $modal->property_id = $property_id;
                $modal->user_id = Auth::user()->id;

                $no_opos_status = 0;
                if($request->no_opos_status)
                    $no_opos_status = 1;

                $modal->no_opos_status = $no_opos_status;


                $propertyMainDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', 0)->first();
                    if(!$propertyMainDir){
                        $res = PropertiesHelperClass::createPropertyDirectories($property_id);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyMainDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', 0)->first();
                    }
                    $propertyPayerDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Verzugszahler')->first();
                    if(!$propertyPayerDir){
                        $res = PropertiesHelperClass::createPropertySubDirectories($propertyMainDir, $property_id, ['Verzugszahler']);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property Verzugszahler directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyPayerDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Verzugszahler')->first();
                    }


                    $file = $request->file('excel');
                    $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $originalExt = $file->getClientOriginalExtension();
                    $folder = $propertyPayerDir->dir_path;
                    $fileData = File::get($file);

                    //upload file
                    $file2 =  Storage::cloud()->put($folder.'/'.$originalName.'.'.$originalExt, $fileData);

                    //get google drive id
                    $contents = collect(Storage::cloud()->listContents($folder, false));
                    $file = $contents->where('type','file')->where('filename', $originalName)->where('extension', $originalExt) ->first();
                    if($file['basename'] == ""){
                        $result = ['status' => false, 'message' => 'File upload fail.', 'data' => []];
                        return response()->json($result);
                    }

                    $modal->file_name = $file['name'];
                    $modal->file_basename = $file['basename'];
                    $modal->file_dirname = $file['dirname'];
                    $modal->file_type = $file['type'];
                    $modal->save();



            $path = $request->file('excel')->getRealPath();

             $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($path);

            /**  Create a new Reader of the type that has been identified  **/
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

            /**  Load $inputFileName to a Spreadsheet Object  **/
            $spreadsheet = $reader->load($path);

            /**  Convert Spreadsheet Object to an Array for ease of use  **/
            $data = $spreadsheet->getActiveSheet()->toArray();

            if(!empty($data)){

                $head = $data[6];

                foreach ($data as $row => $row_value) {
                    if($row > 6){
                        if(trim($row_value[1]) != ''){
                            $this->addOrUpdateRentPaidData($row_value, $property_id, $head);
                        }
                    }
                }

                return redirect('properties/'.$property_id.'?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid')->with('success', 'File import successfully.');

            }else{
                return redirect('properties/'.$property_id.'?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid')->with('error', 'Excel data not found!');
            }

        }else{
            return redirect('properties/'.$property_id.'?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid')->with('error', 'File not found!');
        }
    }


    public function addOrUpdateRentPaidData($row, $property_id, $head){

        // clear column data
        foreach ($row as $col => $col_value) {
            if(in_array($col, [2,3,4,6,7,10,11,12,13,14,15,16,17,18,19,20,21])){
                $col_value = str_replace("€", "", $col_value);
                $col_value = str_replace(",", "", $col_value);
            }
            $row[$col] = trim($col_value);
        }
        // pre($row);

        $existParent = PropertyRentPaids::whereRaw("property_id = '".$property_id."' AND mieter = '".$row[1]."'")->first();
        if($existParent){
            $modal = $existParent;
        }else{
            $modal              = new PropertyRentPaids;
            $modal->property_id = $property_id;
            $modal->user_id     = Auth::user()->id;
        }
        $modal->nr              = $row[0];
        $modal->mieter          = $row[1];
        $modal->soll            = $row[2];
        $modal->ist             = $row[3];
        $modal->diff            = $row[4];
        $modal->info            = $row[5];
        $modal->mkz_aktuell     = $row[6];
        $modal->vorschlag_mkz   = $row[7];
        $modal->bemerkung_ok    = $row[8];
        $modal->bemerkung_fcr   = $row[9];
        if($modal->save()){
            if(isset($modal->id) && $modal->id != ''){
                foreach ($row as $col => $col_value) {
                    if($col >= 10 && $col <= 21 && $col_value != ''){

                        $arr = explode("-", $head[$col]);

                        $month = (isset($arr[0])) ? Carbon::createFromFormat('M', $arr[0])->format('m') : '';
                        $year = (isset($arr[1])) ? Carbon::createFromFormat('y', $arr[1])->format('Y') : '';

                        $existChild = PropertyRentPaidDetails::whereRaw("property_rent_paid_id = '".$modal->id."' AND month = '".$month."' AND year = '".$year."'")->first();
                        if($existChild){
                            $modal_child = $existChild;
                        }else{
                            $modal_child                        = new PropertyRentPaidDetails;
                            $modal_child->property_rent_paid_id = $modal->id;
                            $modal_child->month                 = $month;
                            $modal_child->year                  = $year;
                            $modal_child->year                  = $year;
                        }
                        $modal_child->amount = ($col_value != '' && is_numeric($col_value)) ? $col_value : 0;
                        $modal_child->save();
                    }
                }
            }
        }
        return true;
    }


    public function getRentPaidData(Request $request,$property_id){
        $sol = '';
        $ist = '';
        $diff = '';
        $upload= '';

        $properties = Properties::find($property_id);

        if($request->summery)
        {
            $data = PropertyRentPaids::selectRaw('id,mieter,SUM(ist) as ist,SUM(soll) as soll,SUM(diff) as diff')->where('property_id', $property_id);
            if($request['month']){
                $data = $data->where('upload_month',$request['month']);
            }
            $data = $data->groupBy('mieter')->get();

            $html = View::make('properties.templates.rend-paid-table-detail', ['data' => $data,'properties'=>$properties])->render();
            return $html;

        }

        if($request['month']){
            $data = PropertyRentPaids::where('property_id', $property_id)->where('upload_month',$request['month'])->orderBy('id', 'DESC')->get();
            $sol = PropertyRentPaids::where('property_id', $property_id)->where('upload_month',$request['month'])->get()->sum('soll');
            $ist = PropertyRentPaids::where('property_id', $property_id)->where('upload_month',$request['month'])->get()->sum('ist');
            $diff = PropertyRentPaids::where('property_id', $property_id)->where('upload_month',$request['month'])->get()->sum('diff');
        }else{

            if($request->rmonth)
            {
                // echo "here";
                $data = PropertyRentPaids::where('property_id', $property_id)->orderBy('id', 'DESC')->where('upload_month',$request->rmonth)->where('upload_year',$request->ryear)->get();
                $sol = PropertyRentPaids::where('property_id', $property_id)->where('upload_month',$request->rmonth)->where('upload_year',$request->ryear)->get()->sum('soll');
                $ist = PropertyRentPaids::where('property_id', $property_id)->where('upload_month',$request->rmonth)->where('upload_year',$request->ryear)->get()->sum('ist');
                $diff = PropertyRentPaids::where('property_id', $property_id)->where('upload_month',$request->rmonth)->where('upload_year',$request->ryear)->get()->sum('diff');
            }
            else{
                $data = PropertyRentPaids::where('property_id', $property_id)->orderBy('id', 'DESC')->get();
                $sol = PropertyRentPaids::where('property_id', $property_id)->get()->sum('soll');
                $ist = PropertyRentPaids::where('property_id', $property_id)->get()->sum('ist');
                $diff = PropertyRentPaids::where('property_id', $property_id)->get()->sum('diff');
            }
            $upload = RentPaidExcelUpload::selectRaw('rent_paid_excel_uploads.*, users.name as user_name')->join('users', 'users.id', '=', 'rent_paid_excel_uploads.user_id')->where('rent_paid_excel_uploads.property_id', $property_id)->get();
        }

        $html = View::make('properties.templates.rend-paid-table', ['data' => $data,'sol'=>$sol,'ist'=>$ist,'diff'=>$diff, 'upload' => $upload,'properties'=>$properties])->render();
        return $html;
    }

    public function getRentPaidMonthwiseData($id){
        $i = PropertyRentPaids::find($id);
        $data = PropertyRentPaids::selectRaw('*')->where('mieter','LIKE', '%'.$i->mieter.'%')->where('property_id', $i->property_id)->get();
        // pre($data);
        $html = View::make('properties.templates.rend-paid-table-detail2', ['data' => $data])->render();
        return $html;
    }


    public function deleteRentPaidData($property_id){


        $rent = PropertyRentPaids::where('property_id', $property_id);

        foreach($rent->get() as $rentdetail) {
            $rentdetail->rent_paid_details()->delete();
        }

        $rent->delete();

        RentPaidExcelUpload::where('property_id', $property_id)->delete();

        return redirect('properties/'.$property_id.'?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid')->with('success', 'All information delete successfully');
    }

    public function vacantUploadingStatuse(Request $request, $vacant_id){
        $exist = VacantUploadingStatus::where('vacant_id', $vacant_id)->where('type', $request->type)->first();

        if($exist)
        {
            $m = $exist;
        }
        else
            $m = new VacantUploadingStatus;

        $f = $request->field;
        if($f == 'comment'){
           $m->updated_date = date('Y-m-d H:i:s');
        }
        $m->vacant_id = $vacant_id;
        $m->type = $request->type;
        $m->$f = $request->value;
        $m->save();


        $response = ['status' => true, 'message' => 'Record save successfully.', 'data' => ['date' => show_date_format(date('Y-m-d'))]];


        return response()->json($response);
    }

    public function addRentalActivityComment(Request $request, $vacant_id){
        $modal = new VacantUploadingStatus;
        $modal->vacant_id = $vacant_id;
        $modal->type = $request->type;
        $modal->comment = trim($request->comment);
        if($modal->save()){
            $response = ['status' => true, 'message' => 'Comment added successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Comment added fail!'];
        }
        return response()->json($response);
    }

    public function getRentalActivityComment(Request $request){
        $vacant_id = $request->id;

        $data = VacantUploadingStatus::select('id', 'comment', 'created_at')->where('vacant_id', $vacant_id)->where('type', 8)->get();

        $res['data'] = [];

        if($data){
            foreach ($data as $key => $value) {
                $res['data'][] = [
                    $value->comment,
                    show_datetime_format($value->created_at),
                ];
            }
        }
        return response()->json($res);
    }

    public function bankFinancingOfferImport(Request $request)
    {

        Excel::import(new BankFinancingOfferExport($request), Input::file('pic'));
        return 'done';
    }

    public function vacantmarkednotrelease(Request $request)
    {
        $tenant = PropertyVacant::find($request->tenant_id);
        $tenant->not_release_status = 1;
        $tenant->save();

        $id = $tenant->property_id;

        $url = route('properties.show',['property'=>$id]).'?tab=recommended_tab';
        $url = '<a href="'.$url.'">'.$url.'</a>';

        $custom = 0;
        $tenant_name = "";
        if($tenant){
            if(!$tenant->tenant_id)
                $custom = 1;
            $tenant_name = $tenant->name;
        }

        $mieter_name = "";

        $b = EmpfehlungDetail::where('slug','tenant_name')->where('tenant_id',$request->tenant_id)->first();
        if($b)
        {
            $mieter_name = $b->value;
            if(is_numeric($b->value) && $custom)
            {
                $i = TenancyScheduleItem::where('id',$b->value)->first();
                if($i)
                    $mieter_name = $i->name;
            }
        }

        $p = Properties::find($id);
        $user = Auth::user();
        $name = "";
        $email = "";

        $modal = new EmpfehlungDetail;
        $modal->property_id = $id;
        $modal->tenant_id = $request->tenant_id;
        $modal->slug = 'vacant_not_release';
        if($request->comment)
            $modal->value = $request->comment;
        $modal->user_id = Auth::user()->id;
        $modal->save();

        if(isset($p->asset_manager) && isset($p->asset_manager->name)){
            $name = $p->asset_manager->name;
            $email = $p->asset_manager->email;
        }
        if(in_array($p->id, $this->propertiesids()))
        {
            $p21 = $this->getdata21();
            $name = $p21['name'];
            $email = $p21['email'];
        }

        $text = "Hallo ".$name.",<br><br> Falk hat deine Vermietungsempfehlung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die Leerstandsfläche $tenant_name nicht freigegeben. Hier kommst du zur Empfehlung: ".$url;

        $subject = "Keine Freigabe $mieter_name für die Leerstandsfläche $tenant_name für das Objekt ".$p->plz_ort.' '.$p->ort;

        if($custom)
        {
            $text = "Hallo ".$name.",<br><br> Falk hat deine Verlängerung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." nicht freigegeben. Hier kommst du zur Verlängerung: ".$url;
            $subject = "Keine Freigabe $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;
        }

        if($request->comment)
            $text .= "<br><br>Kommentar: ".$request->comment;

        // echo $text; die;

        if( $email && !in_array($email, $this->mail_not_send()) )
        {
            $replyemail = config('users.falk_email');
            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                $message->to($email)
                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                ->replyTo($replyemail, 'Falk')
                ->subject($subject);
            });
        }
        echo "1";
    }

    public function vacantMarkAsPending(Request $request, $vacant_id){

        $tenant = PropertyVacant::find($vacant_id);
        $tenant->not_release_status = 2;
        $tenant->save();

        $id = $tenant->property_id;

        $url = route('properties.show',['property'=>$id]).'?tab=recommended_tab';
        $url = '<a href="'.$url.'">'.$url.'</a>';

        $custom = 0;
        $tenant_name = "";
        if($tenant){
            if(!$tenant->tenant_id)
                $custom = 1;
            $tenant_name = $tenant->name;
        }

        $mieter_name = "";

        $b = EmpfehlungDetail::where('slug','tenant_name')->where('tenant_id',$vacant_id)->first();
        if($b)
        {
            $mieter_name = $b->value;
            if(is_numeric($b->value) && $custom)
            {
                $i = TenancyScheduleItem::where('id',$b->value)->first();
                if($i)
                    $mieter_name = $i->name;
            }
        }

        $p = Properties::find($id);
        $user = Auth::user();
        $name = "";
        $email = "";

        $modal = new EmpfehlungDetail;
        $modal->property_id = $id;
        $modal->tenant_id = $vacant_id;
        $modal->slug = 'vacant_pending';
        if($request->comment)
            $modal->value = $request->comment;
        $modal->user_id =$user->id;
        $modal->save();

        if(isset($p->asset_manager) && isset($p->asset_manager->name)){
            $name = $p->asset_manager->name;
            $email = $p->asset_manager->email;
        }
        if(in_array($p->id, $this->propertiesids()))
        {
            $p21 = $this->getdata21();
            $name = $p21['name'];
            $email = $p21['email'];
        }

        $text = "Hallo ".$name.",<br><br> Falk hat deine Vermietungsempfehlung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." für die Fläche $tenant_name noch nicht freigegeben. Hier kommst du zur Empfehlung: ".$url;

        $subject = "Noch keine Freigabe $mieter_name für die Fläche $tenant_name für das Objekt ".$p->plz_ort.' '.$p->ort;

        if($custom){

            $text = "Hallo ".$name.",<br><br> Falk hat deine Verlängerung $mieter_name bei dem Objekt ".$p->plz_ort.' '.$p->ort." noch nicht freigegeben. Hier kommst du zur Verlängerung: ".$url;

            $subject = "Noch keine Freigabe $mieter_name für das Objekt ".$p->plz_ort.' '.$p->ort;
        }

        if($request->comment)
            $text .= "<br><br>Kommentar: ".$request->comment;

        if( $email && !in_array($email, $this->mail_not_send()) )
        {
            $replyemail = config('users.falk_email');
            Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject,$replyemail, $name) {
                $message->to($email)
                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                ->replyTo($replyemail, 'Falk')
                ->subject($subject);
            });
        }
        echo "1";
    }


    public function excelImportMieterliste(Request $request,$propertyId)
    {

        if ($request->hasFile('excel')) {

                $property_id = $propertyId;
                $modal = new RentPaidExcelUpload;
                $modal->property_id = $property_id;
                $modal->user_id = Auth::user()->id;

                $no_opos_status = 0;
                if($request->no_opos_status)
                    $no_opos_status = 1;

                $modal->no_opos_status = $no_opos_status;


                $propertyMainDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', 0)->first();
                    if(!$propertyMainDir){
                        $res = PropertiesHelperClass::createPropertyDirectories($property_id);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyMainDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', 0)->first();
                    }
                    $propertyPayerDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Verzugszahler')->first();
                    if(!$propertyPayerDir){
                        $res = PropertiesHelperClass::createPropertySubDirectories($propertyMainDir, $property_id, ['Verzugszahler']);
                        if(!$res['status']){
                            $result = ['status' => false, 'message' => 'Property Verzugszahler directory not created in google Drive.', 'data' => []];
                            return response()->json($result);
                        }
                        $propertyPayerDir = PropertiesDirectory::where('property_id', $property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Verzugszahler')->first();
                    }


                    $file = $request->file('excel');
                    $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $originalExt = $file->getClientOriginalExtension();
                    $folder = $propertyPayerDir->dir_path;
                    $fileData = File::get($file);

                    //upload file
                    $file2 =  Storage::cloud()->put($folder.'/'.$originalName.'.'.$originalExt, $fileData);

                    //get google drive id
                    $contents = collect(Storage::cloud()->listContents($folder, false));
                    $file = $contents->where('type','file')->where('filename', $originalName)->where('extension', $originalExt) ->first();
                    if($file['basename'] == ""){
                        $result = ['status' => false, 'message' => 'File upload fail.', 'data' => []];
                        return response()->json($result);
                    }

                    $modal->file_name = $file['name'];
                    $modal->file_basename = $file['basename'];
                    $modal->file_dirname = $file['dirname'];
                    $modal->file_type = $file['type'];
                    $modal->save();



                    $modal = new PropertiesDefaultPayers;
                    $modal->property_id = $property_id;
                    $modal->user_id = Auth::user()->id;
                    // $modal->comment = trim($request->comment);
                    $modal->date = date('Y-m-d');
                    $modal->year = $request->year;
                    $modal->month = $request->month;
                    if($request->no_opos_status)
                    $modal->no_opos_status = 1;

                    $modal->invoice = $file['name'];
                    $modal->file_basename = $file['basename'];
                    $modal->file_dirname = $file['dirname'];
                    $modal->file_type = $file['type'];
                    $modal->save();

                    Excel::import(new ExcelImportMieterliste($request,$propertyId,$request->year,$request->month), Input::file('default_payer_gdrive_file_upload'));
        }
        else{

            $property_id = $propertyId;
            $modal = new RentPaidExcelUpload;
            $modal->property_id = $property_id;
            $modal->user_id = Auth::user()->id;

            $no_opos_status = 0;
            if($request->no_opos_status)
                $no_opos_status = 1;
            $modal->no_opos_status = $no_opos_status;
            $modal->save();


            $modal = new PropertiesDefaultPayers;
            $modal->property_id = $property_id;
            $modal->user_id = Auth::user()->id;
            // $modal->comment = trim($request->comment);
            $modal->date = date('Y-m-d');
            $modal->year = $request->year;
            $modal->month = $request->month;
            if($request->no_opos_status)
            $modal->no_opos_status = 1;
            $modal->save();
        }
        return redirect()->back();
//        return redirect('properties/'.$propertyId.'?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid')->with('success', 'File import successfully.');
    }

    public function addRecommendedComment(Request $request){
        if($request->ajax()) {

            $validator = Validator::make($request->all(), [
                "tenant_id"   => "required",
                "comment"       => "required|min:5",
            ]);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

                $user = Auth::user();

                $modal = new EmpfehlungComments;
                $modal->user_id = $user->id;
                $modal->tenant_id = $request->tenant_id;
                $modal->comment = trim($request->comment);
                if($modal->save()){

                    // $res = DB::table('empfehlung_comments as ec')->selectRaw('ec.id, ec.user_id, ec.tenant_id, ec.comment, ec.created_at, u.name as user_name')->join('users as u', 'u.id', '=', 'ec.user_id')->where('ec.id', $modal->id)->first();
                    // $res->created_at = show_datetime_format($res->created_at);

                    $email_not_send_array = $this->mail_not_send();

                    $data = DB::table('property_vacants as pv')
                            ->selectRaw('pv.property_id, p.name_of_property, u.name, u.email, u2.name as name2, u2.email as email2')
                            ->join('properties as p', 'p.id', '=', 'pv.property_id')
                            ->leftjoin('users as u', 'u.id', '=', 'p.asset_m_id')
                            ->leftjoin('users as u2', 'u2.id', '=', 'p.asset_m_id2')
                            ->where('pv.id', $request->tenant_id)
                            ->first();

                    if($data){

                        $subject = 'Neuer Kommentar zu Vermietung: '.$data->name_of_property;
                        $text = '<br/>Kommentar: '.$request->comment;

                        $taburl = route('properties.show',['property' => $data->property_id]).'?tab=recommended_tab';
                        $taburl = '<a href="'.$taburl.'">'.$taburl.'</a>';

                        $text .= '<br><br>'.$taburl;

                        if($user->email == config('users.falk_email')){//send mail to am

                            $email = $data->email;
                            $name = $data->name;
                            $replyemail = config('users.falk_email');
                            $replyname = 'Falk';

                            if( $email && !in_array($email, $email_not_send_array) ){

                                $email_not_send_array[] = $email;

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });
                            }

                            if( $data->email2 && !in_array($data->email2, $email_not_send_array) ){

                                $email_not_send_array[] = $data->email2;

                                $email = $data->email2;
                                $name = $data->name2;

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });
                            }

                        }else{//send mail to falk

                            $is_send_mail_falk = false;

                            $is_request = DB::table('empfehlung_details')->where(['tenant_id' => $request->tenant_id, 'slug' => 'btn2_request'])->get()->count();
                            if($is_request > 0){
                                $is_send_mail_falk = true;
                            }

                            if($is_send_mail_falk){

                                $email = config('users.falk_email');
                                $name = 'Falk';
                                $replyemail = $data->email;
                                $replyname = $data->name;

                                if( $email && !in_array($email, $email_not_send_array) ){

                                    $email_not_send_array[] = $email;

                                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                        $message->to($email, $name)
                                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                        ->replyTo($replyemail, $replyname)
                                        ->subject($subject);
                                    });
                                }
                            }

                        }

                        // send mail to custom users
                        $custom_mail = ($request->comment) ? fetch_mails_by_string($request->comment) : [];
                        if(!empty($custom_mail)){

                            $custom_mail = array_unique($custom_mail);

                            if($user->email == config('users.falk_email')){
                                $replyemail = config('users.falk_email');
                                $replyname = 'Falk';
                            }else{
                                $replyemail = $data->email;
                                $replyname = $data->name;
                            }

                            Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $custom_mail, $replyemail, $replyname) {
                                $message->to($custom_mail)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($replyemail, $replyname)
                                ->subject($subject);
                            });
                        }

                        // send mail to uploaded user
                        $detail = DB::table('empfehlung_details as d')->select('u.email', 'u.name')->join('users as u', 'u.id', '=', 'd.user_id')->where('d.tenant_id', $request->tenant_id)->where('d.slug', 'add_vacant')->first();
                        if( isset($detail->email) && $detail->email && !in_array($detail->email, $email_not_send_array) ){

                            $email_not_send_array[] = $detail->email;

                            $email = $detail->email;
                            $name = $detail->name;
                            $replyemail = $user->email;
                            $replyname = $user->name;

                            Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                $message->to($email, $name)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($replyemail, $replyname)
                                ->subject($subject);
                            });
                        }
                    }

                    $result = ['status' => true, 'message' => 'Comment add successfully.', 'data' => []];
                }else{
                    $result = ['status' => false, 'message' => 'Comment add fail! Try again.', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);

    }

    public function getRecommendedComment($id, $limit = ''){
        $user = Auth::user();

        $res = DB::table('empfehlung_comments as ec')
            ->selectRaw('ec.id, ec.user_id, ec.tenant_id, ec.comment, ec.created_at, u.name as user_name, u.role, u.company')
            ->join('users as u', 'u.id', '=', 'ec.user_id')
            ->where('ec.tenant_id', $id);
        if($limit != '' && $limit != 0 && $limit != -1){
            $res = $res->limit($limit);
        }
        $res = $res->orderBy('ec.created_at', 'desc')->get();

        if($res){
            foreach ($res as $key => $value) {
                $res[$key]->comment = nl2br($value->comment);
                $res[$key]->created_at = show_datetime_format($value->created_at);
                $res[$key]->login_id = $user->id;
            }
        }

        return response()->json($res);
    }

    public function deleteRecommendedComment($id){
        $model = EmpfehlungComments::find($id);
        if($model->delete()){
            $response = ['status' => true, 'message' => 'Comment delete successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Comment delete fail!'];
        }
        return response()->json($response);
    }
    public function saveNotesOfPropertyRentPaid(Request $request)
    {
        PropertyRentPaids::where('id',$request->property_rent_paids_id)->first()->update([
            'bemerkung_hv' => $request->bemerkung_hv,
            'bemerkung_fcr' => $request->bemerkung_fcr
        ]);

        return redirect()->back();
    }

    public function addProvisionComment(Request $request){
        if($request->ajax()) {

            $validator = Validator::make($request->all(), [
                "provision_id"   => "required",
                "comment"       => "required",
            ]);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

                $user = Auth::user();

                $modal = new ProvisionComments;
                $modal->user_id = $user->id;
                $modal->provision_id = $request->provision_id;
                $modal->comment = trim($request->comment);
                if($modal->save()){

                    $p = DB::table('provisions as pv')->selectRaw('p.id, p.name_of_property, u.name, u.email, u2.name as name2, u2.email as email2')->join('properties as p', 'p.id', '=', 'pv.property_id')->leftjoin('users as u', 'u.id', '=', 'p.asset_m_id')->leftjoin('users as u2', 'u2.id', '=', 'p.asset_m_id2')->where('pv.id', $request->provision_id)->first();

                    if($p){

                        $email_not_send_array = $this->mail_not_send();

                        $subject = 'Provision: '.$p->name_of_property;
                        $text = '<br/>Kommentar '.$user->name.': '.$request->comment;

                        $taburl = route('properties.show',['property' => $p->id]).'?tab=provision_tab';
                        $taburl = '<a href="'.$taburl.'">'.$taburl.'</a>';

                        $text .= '<br><br>'.$taburl;

                        $text .= '<br><br>Bitte antworte direkt im Kommentarfeld im Intranet auf diesen Kommentar.';

                        if($user->email == config('users.falk_email')){//send mail to am

                            $email = $p->email;
                            $name = $p->name;
                            $replyemail = config('users.falk_email');
                            $replyname = 'Falk';

                            if($email && !in_array($email, $email_not_send_array) ){

                                $email_not_send_array[] = $email;

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });
                            }

                            if( $p->email2 && !in_array($p->email2, $email_not_send_array) ){

                                $email_not_send_array[] = $p->email2;

                                $email = $p->email2;
                                $name = $p->name2;
                                $replyemail = config('users.falk_email');
                                $replyname = 'Falk';


                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });
                            }

                        }else{//send mail to falk

                            $is_send_mail_falk = false;

                            $request = DB::table('empfehlung_details')->where(['slug' => 'pbtn2_request', 'provision_id' => $request->provision_id])->get()->count();
                            if($request > 0){
                                $is_send_mail_falk = true;
                            }

                            if($is_send_mail_falk){

                                $email = config('users.falk_email');
                                $name = 'Falk';
                                $replyemail = $p->email;
                                $replyname = $p->name;

                                if( $email && !in_array($email, $email_not_send_array) ){

                                    $email_not_send_array[] = $email;

                                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                        $message->to($email, $name)
                                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                        ->replyTo($replyemail, $replyname)
                                        ->subject($subject);
                                    });
                                }
                            }

                        }

                    }

                    $result = ['status' => true, 'message' => 'Comment add successfully.', 'data' => $modal];
                }else{
                    $result = ['status' => false, 'message' => 'Comment add fail! Try again.', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getProvisionComment($id, $limit = ''){
        $res = DB::table('provision_comments as pc')
            ->selectRaw('pc.id, pc.user_id, pc.provision_id, pc.comment, pc.created_at, u.name as user_name, u.role, u.company')
            ->join('users as u', 'u.id', '=', 'pc.user_id')
            ->where('pc.provision_id', $id);
        if($limit != '' && $limit != 0 && $limit != -1){
            $res = $res->limit($limit);
        }
        $res = $res->orderBy('pc.id', 'desc')->get();

        if($res){
            foreach ($res as $key => $value) {
                $res[$key]->created_at = show_datetime_format($value->created_at);
                $res[$key]->comment = nl2br($value->comment);
            }
        }
        return response()->json($res);
    }

    public function deleteProvisionComment($id){
        $model = ProvisionComments::find($id);
        if($model->delete()){
            $response = ['status' => true, 'message' => 'Comment delete successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Comment delete fail!'];
        }
        return response()->json($response);
    }


    public function getRecommendedTable($property_id){

        $user = Auth::user();

        $tenancy_schedule_data = TenancySchedulesService::get($property_id, 1);
        $rented_list = array();
        $area = [];
        if($tenancy_schedule_data['tenancy_schedules']){
            foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule){
                foreach($tenancy_schedule->items as $item){
                    if($item->type == config('tenancy_schedule.item_type.business_vacancy') || $item->type == config('tenancy_schedule.item_type.live_vacancy')){
                    }else{
                        $rented_list[$item->id]['name'] = $item->name;
                        $rented_list[$item->id]['date'] = ($item->rent_begin && $item->rent_end) ? show_date_format($item->rent_begin).' - '.show_date_format($item->rent_end) : '';
                    }
                    if($item->name){
                        $vv = ($item->rental_space) ? $item->rental_space : $item->vacancy_in_qm;
                        if(!$vv)
                            $vv = 0;

                        $area[$item->id] = [
                            'area' => $item->name.' ('.number_format($vv,2,',','.').')',
                            'name' => $item->name,
                            'size' => $vv,
                        ];
                    }
                }
            }
        }

        $emp_data = Empfehlung::where('property_id',$property_id)->where('deleted',0)->orderBy('name','asc')->get();
        $ractivity = [];
        if($emp_data){
            foreach ($emp_data as $key => $value) {
                $ractivity[$value->tenant_id][] = $value;
            }
        }

        $detail = EmpfehlungDetail::where('property_id',$property_id)->get();

        $detail_arr = array();
        $detail_date_arr = array();
        if($detail){
            foreach ($detail as $key => $value) {
                $detail_arr[$value->tenant_id][$value->slug] = $value->value;
                $detail_date_arr[$value->tenant_id][$value->slug] = $value->created_at;
            }
        }


        $data = DB::table('property_vacants as pv')
                    ->selectRaw('pv.id, pv.type, pv.name, pv.tenant_id, pv.mv_vl, pv.not_release_status, pv.created_at, CASE WHEN EXISTS (SELECT ed.id FROM empfehlung_details as ed WHERE ed.tenant_id = pv.id AND ed.slug = "btn2") THEN 1 ELSE 0 END as is_btn2, (CASE WHEN EXISTS (SELECT id FROM empfehlung_details WHERE slug = "delete_vacant" AND tenant_id = pv.id) THEN 1 ELSE 0 END) as is_deleted')
                    ->where('pv.property_id', $property_id)
                    ->havingRaw('is_deleted != 1')
                    ->get();

        if($data){
            foreach ($data as $key => $element) {
                $data[$key]->files = GdriveUploadFiles::where('property_id',$property_id)->where('parent_id', $element->id)->where('parent_type', 'empfehlung2')->get();
            }
        }

        $log = DB::table('empfehlung_details as ed')->selectRaw('ed.id, ed.slug, ed.value, ed.created_at, u.name, ed.user_id')
            ->leftjoin('users as u', 'u.id', '=', 'ed.user_id')
            ->where('ed.property_id', $property_id)
            ->whereIn('ed.slug', ['delete_vacant', 'btn2', 'btn2_request', 'vacant_pending', 'vacant_not_release'])
            ->orderBy('ed.created_at', 'desc')->get();

        //open vacant
        $res['table1'] = View::make('properties.templates.recommended_table', ['table' => '1', 'data' => $data, 'rented_list' => $rented_list, 'detail_arr' => $detail_arr, 'ractivity' => $ractivity, 'user' => $user, 'detail_date_arr' => $detail_date_arr, 'property_id' => $property_id, 'area' => $area])->render();

        //release vacant
        $res['table2'] = View::make('properties.templates.recommended_table', ['table' => '2', 'data' => $data, 'rented_list' => $rented_list, 'detail_arr' => $detail_arr, 'ractivity' => $ractivity, 'user' => $user, 'detail_date_arr' => $detail_date_arr, 'property_id' => $property_id, 'area' => $area])->render();

        //pending vacant
        $res['table3'] = View::make('properties.templates.recommended_table', ['table' => '3', 'data' => $data, 'rented_list' => $rented_list, 'detail_arr' => $detail_arr, 'ractivity' => $ractivity, 'user' => $user, 'detail_date_arr' => $detail_date_arr, 'property_id' => $property_id, 'area' => $area])->render();

        //not release vacant
        $res['table4'] = View::make('properties.templates.recommended_table', ['table' => '4', 'data' => $data, 'rented_list' => $rented_list, 'detail_arr' => $detail_arr, 'ractivity' => $ractivity, 'user' => $user, 'detail_date_arr' => $detail_date_arr, 'property_id' => $property_id, 'area' => $area])->render();

        //release log
        $res['table5'] = View::make('properties.templates.recommended_table', ['table' => '5', 'log' => $log, 'property_id' => $property_id, 'user' => $user])->render();
        return response()->json($res);
    }

    public function getKostenUmbau($property_id, $tenant_id){
        $data = Empfehlung::where('property_id',$property_id)->where('tenant_id',$tenant_id)->where('deleted',0)->get();
        return View::make('properties.templates.kosten_umbau', ['data' => $data, 'property_id' => $property_id, 'tenant_id' => $tenant_id])->render();
    }

    public function getBankFinanceOffers(Request $request)
    {
        $data = BankFinancingOffer::where('property_id', $request->id)->get();
        $a = View::make('properties.bank_finanzierungsanfragen', array('data' => $data, 'type' => 'Angebote'))->render();
        return $a;
    }


    public function getAllBankMailLogsOfProperty(Request $request)
    {
        $data = email_template2_sending_info::where('property_id', $request->id)->get();
        $a = View::make('properties.bank_finanzierungsanfragen', array('data' => $data, 'type' => 'Anfragen'))->render();
        return $a;
    }

    public function getProperty(Request $request)
    {
        return Properties::where("name_of_property","LIKE","%{$request['query']['term']}%")->groupBy('name_of_property')->get();
    }

    public function deleteTenantPaymentFile($id){
        $modal = TenantPayment::find($id);
        $modal->file_name = null;
        $modal->file_basename = null;
        $modal->file_dirname = null;
        $modal->file_type = null;
        if($modal->save()){
            $response = ['status' => true, 'message' => 'file delete successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'file delete fail!'];
        }
        return response()->json($response);
    }


    public function getEmailTemplateAndFinanceOffers(Request $request)
    {
        $sendEmails = email_template2_sending_info::where('property_id',$request->id)->get();
        $banksFinancingOffers = BankFinancingOffer::where('property_id',$request->id)->get();
        $a = View::make('properties.finanzierungsanfragen-und-angebote', array('sendEmails' => $sendEmails, 'banksFinancingOffers' => $banksFinancingOffers))->render();
        return $a;
    }

    public  function deletePropertyRentPaid($id)
    {
        PropertyRentPaids::where('property_id',$id)->delete();
    }

    public  function updateCheckboxKeinOpos(Request $request)
    {
        Log::info($request->all());
        RentPaidExcelUpload::updateOrCreate(
            ['property_id'=>$request['id']],
            ['no_opos_status' => $request['value'],
              'property_id'=>$request['id'],

            ]);

    }

    public function update_tenant_master(Request $request, $id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated field'
        ];

        $TenantMaster = TenantMaster::where('id', $id)->first();
        if (!$TenantMaster) {
            $response['msg'] = 'Record not found!';
            return response()->json($response);
        }

        $field = $request->pk;
        $value = $request->value;

        $TenantMaster->$field = $value;
        if($TenantMaster->save()){
            $response = [
                'success' => true,
                'msg' => 'Field update successfully.'
            ];
        }
        return response()->json($response);
    }

    public function saveTenantMaster(Request $request, $id, $item_id = ''){

        $modal = TenantMaster::find($id);
        $modal->item_name = $request->item_name;
        $modal->item_type = $request->item_type;
        $modal->area_space = make_amount($request->area_space);

        if($modal->save()){

            if($item_id){
                $submodal = TenancyScheduleItem::find($item_id);
            }else{
                $submodal = new TenancyScheduleItem;
            }
            $submodal->tenant_closed    = (isset($request->tenant_closed)) ? $request->tenant_closed : null;
            $submodal->rent_payment     = (isset($request->rent_payment)) ? $request->rent_payment : null;
            $submodal->comment3         = (isset($request->comment3)) ? $request->comment3 : null;
            $submodal->rent_begin       = (isset($request->rent_begin) && $request->rent_begin) ? save_date_format($request->rent_begin) : null;
            $submodal->rent_end         = (isset($request->rent_end) && $request->rent_end) ? save_date_format($request->rent_end) : null;
            $submodal->rental_space     = (isset($request->rental_space)) ? make_amount($request->rental_space) : 0;
            $submodal->actual_net_rent  = (isset($request->actual_net_rent)) ? make_amount($request->actual_net_rent) : 0;
            $submodal->nk_netto         = (isset($request->nk_netto)) ? make_amount($request->nk_netto) : 0;
            $submodal->is_new           = (isset($request->is_new) && $request->is_new == 1) ? 1 : 0;
            $submodal->opos_mieter          = (isset($request->opos_mieter)) ? $request->opos_mieter : null;
            $submodal->asset_manager_id = (isset($request->asset_manager_id)) ? $request->asset_manager_id : null;
            $submodal->asset_manager_id2= (isset($request->asset_manager_id2)) ? $request->asset_manager_id2 : null;
            $submodal->assesment_date   = (isset($request->assesment_date) && $request->assesment_date) ? save_date_format($request->assesment_date) : null;
            $submodal->termination_date = (isset($request->termination_date) && $request->termination_date) ? save_date_format($request->termination_date) : null;
            $submodal->selected_option  = (isset($request->selected_option)) ? $request->selected_option : null;
            $submodal->options          = (isset($request->options)) ? $request->options : null;
            $submodal->use              = (isset($request->use)) ? $request->use : null;
            $submodal->category         = isset($request->category) ? $request->category : null;
            $submodal->warning_date1    = (isset($request->warning_date1) && $request->warning_date1) ? save_date_format($request->warning_date1) : null;
            $submodal->warning_price1   = (isset($request->warning_price1) && $request->warning_price1 == 1) ? 1 : 0;
            $submodal->warning_date2    = (isset($request->warning_date2) && $request->warning_date2) ? save_date_format($request->warning_date2) : null;
            $submodal->warning_price2   = (isset($request->warning_price2) && $request->warning_price2 == 1) ? 1 : 0;
            $submodal->warning_date3    = (isset($request->warning_date3) && $request->warning_date3) ? save_date_format($request->warning_date3) : null;
            $submodal->warning_price3   = (isset($request->warning_price3)) ? make_amount($request->warning_price3) : 0;
            $submodal->indexierung      = (isset($request->indexierung)) ? $request->indexierung : null;
            $submodal->kaution          = (isset($request->kaution)) ? $request->kaution : null;
            $submodal->vacancy_in_qm    = (isset($request->vacancy_in_qm)) ? make_amount($request->vacancy_in_qm) : 0;
            $submodal->vacancy_in_eur   = (isset($request->vacancy_in_eur)) ? make_amount($request->vacancy_in_eur) : 0;
            $submodal->actual_net_rent2 = (isset($request->actual_net_rent2)) ? make_amount($request->actual_net_rent2) : 0;
            $submodal->vacant_since     = (isset($request->vacant_since) && $request->vacant_since) ? save_date_format($request->vacant_since) : null;
            $submodal->vacancy_on_purcahse  = (isset($request->vacancy_on_purcahse) && $request->vacancy_on_purcahse == 1) ? 1 : 0;

            $submodal->save();

            // Save tenant payments
            if(isset($request->bka_date) && !empty($request->bka_date)){

                $year_arr = [0 => '2018', 1 => '2019', 2=> '2020'];

                for ($i=0; $i <= 2; $i++) {

                    $tenant_payments = TenantPayment::where('tenancy_schedule_item_id', $item_id)->where('year',$year_arr[$i])->first();
                    if(!$tenant_payments){
                        $tenant_payments = new TenantPayment;
                        $tenant_payments->tenancy_schedule_item_id = $item_id;
                        $tenant_payments->year = $year_arr[$i];
                    }
                    $tenant_payments->bka_date = (isset($request->bka_date[$i]) && $request->bka_date[$i]) ? save_date_format($request->bka_date[$i]) : null;
                    $tenant_payments->amount = (isset($request->bka_amount[$i])) ? make_amount($request->bka_amount[$i]) : 0;
                    $tenant_payments->payment_date = (isset($request->bka_payment_date[$i]) && $request->bka_payment_date[$i]) ? save_date_format($request->bka_payment_date[$i]) : null;
                    $tenant_payments->save();
                }

            }


            return redirect()->route('iframe_tenant_master', ['id' => $id])->with('success', 'Tenant update Successfully.');
        }else{
            return redirect()->route('iframe_tenant_master', ['id' => $id])->with('error', 'Tenant update fail!');
        }
    }

    public function getPropertyInsuranceDetailComment(Request $request, $id, $limit = ''){
        $user = Auth::user();

        $res = DB::table('properties_comments as pc')
                ->selectRaw('pc.id, pc.user_id, pc.record_id, pc.comment, pc.created_at, u.name as user_name, u.role, u.company')
                ->join('users as u', 'u.id', '=', 'pc.user_id')
                ->where('record_id', $id)
                ->where('type', $request->type);
                if($limit != '' && $limit != 0 && $limit != -1){
                    $res = $res->limit($limit);
                }
                $res = $res->orderBy('pc.created_at', 'desc')->get();

        if($res){
            foreach ($res as $key => $value) {
                $res[$key]->comment = nl2br($value->comment);
                $res[$key]->created_at = show_datetime_format($value->created_at);
                $res[$key]->login_id = $user->id;
            }
        }

        return response()->json($res);
    }

    public function addPropertyInsuranceComment(Request $request){
        if($request->ajax()) {

            $validator = Validator::make($request->all(), [
                "record_id"     => "required",
                "comment"       => "required|min:5",
                "property_id"   => "required",
                "type"          => "required",
            ]);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

                $user = Auth::user();

                $modal = new PropertiesComment;
                $modal->property_id = $request->property_id;
                $modal->user_id = $user->id;
                $modal->comment = trim($request->comment);
                $modal->record_id = $request->record_id;
                $modal->type = $request->type;
                if($modal->save()){

                    $email_not_send_array = $this->mail_not_send();

                    $subject = $text = '';

                    if($request->type == 'property_insurance_tab_details'){

                        $data = DB::table('property_insurance_tab_details as pitd')
                                ->selectRaw('pitd.id, pitd.property_insurance_tab_id, pitd.file_basename, pitd.file_type, pitd.file_name, p.name_of_property, u.name, u.email, u2.name as name2, u2.email as email2, u3.email as upload_user_email, u3.name as upload_user_name')
                                ->join('property_insurance_tabs as pit', 'pit.id', '=', 'pitd.property_insurance_tab_id')
                                ->join('properties as p', 'p.id', '=', 'pit.property_id')
                                ->leftjoin('users as u', 'u.id', '=', 'p.asset_m_id')
                                ->leftjoin('users as u2', 'u2.id', '=', 'p.asset_m_id2')
                                ->join('users as u3', 'u3.id', '=', 'pitd.user_id')
                                ->where('pitd.id', $request->record_id)
                                ->first();

                        if($data){

                            $download_path = "https://drive.google.com/drive/u/2/folders/".$data->file_basename;
                            if($data->file_type == "file"){
                                $download_path = "https://drive.google.com/file/d/".$data->file_basename;
                            }

                            $subject = 'Neuer Kommentar zu Angebot: '.$data->name_of_property;
                            $text = 'Angebot: <a title="'.$data->file_name.'" href="'.$download_path.'" target="_blank">'.$data->file_name.'</a>';
                            if($request->comment){
                                $text .= '<br/><br/>Kommentar '.$user->name.': '.$request->comment;
                            }

                            $taburl = route('properties.show',['property' => $request->property_id]).'?tab=property_insurance_tab';
                            $taburl = '<a href="'.$taburl.'">'.$taburl.'</a>';

                            $text .= '<br><br>'.$taburl;

                            $text .= '<br><br>Bitte antworte direkt im Kommentarfeld im Intranet auf diesen Kommentar.';

                            if($user->email == config('users.falk_email')){//send mail to am

                                $email = $data->email;
                                $name = $data->name;
                                $replyemail = config('users.falk_email');
                                $replyname = 'Falk';

                                if( $email && !in_array($email, $email_not_send_array) ){

                                    $email_not_send_array[] = $email;

                                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                        $message->to($email, $name)
                                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                        ->replyTo($replyemail, $replyname)
                                        ->subject($subject);
                                    });
                                }

                                if( $data->email2 && !in_array($data->email2, $email_not_send_array) ){

                                    $email_not_send_array[] = $data->email2;

                                    $email = $data->email2;
                                    $name = $data->name2;
                                    $replyemail = config('users.falk_email');
                                    $replyname = 'Falk';

                                    if($email){
                                        Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                            $message->to($email, $name)
                                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                            ->replyTo($replyemail, $replyname)
                                            ->subject($subject);
                                        });
                                    }

                                }

                            }else{//send mail to falk

                                // $is_request = DB::table('properties_mail_logs')->where(['type' => 'insurance_request', 'selected_id' => $request->record_id])->get()->count();
                                $is_request = DB::table('properties_mail_logs')->where(['type' => 'insurance_request', 'record_id' => $data->property_insurance_tab_id])->get()->count();

                                if($is_request > 0){

                                    $email = config('users.falk_email');
                                    $name = 'Falk';
                                    $replyemail = $data->email;
                                    $replyname = $data->name;

                                    if( $email && !in_array($email, $email_not_send_array) ){
                                        Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                            $message->to($email, $name)
                                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                            ->replyTo($replyemail, $replyname)
                                            ->subject($subject);
                                        });
                                    }
                                }

                            }

                            // send mail to uploaded user
                            if( $data->upload_user_email && !in_array($data->upload_user_email, $email_not_send_array) ){

                                $email_not_send_array[] = $data->upload_user_email;

                                $email = $data->upload_user_email;
                                $name = $data->upload_user_name;
                                $replyemail = $user->email;
                                $replyname = $user->name;

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });

                            }

                        }
                    }elseif($request->type == 'property_insurance_tabs') {

                        $data = DB::table('property_insurance_tabs as pit')
                                    ->selectRaw('pit.id, pit.title, p.name_of_property, p.hausmaxx, u.name, u.email, u2.name as name2, u2.email as email2, u3.email as upload_user_email, u3.name as upload_user_name')
                                    ->join('properties as p', 'p.id', '=', 'pit.property_id')
                                    ->leftjoin('users as u', 'u.id', '=', 'p.asset_m_id')
                                    ->leftjoin('users as u2', 'u2.id', '=', 'p.asset_m_id2')
                                    ->join('users as u3', 'u3.id', '=', 'pit.user_id')
                                    ->where('pit.id', $request->record_id)
                                    ->first();
                        if($data){

                            $subject = 'Neuer Kommentar zu Angebot: '.$data->name_of_property.' / '.$data->hausmaxx.' / '.$data->title;
                            $text = '<br/>Kommentar '.$user->name.': '.$request->comment;

                            $taburl = route('properties.show',['property' => $request->property_id]).'?tab=property_insurance_tab';
                            $taburl = '<a href="'.$taburl.'">'.$taburl.'</a>';

                            $text .= '<br><br>'.$taburl;

                            $text .= '<br><br>Bitte antworte direkt im Kommentarfeld im Intranet auf diesen Kommentar.';

                            if($user->email == config('users.falk_email')){//send mail to am

                                $email = $data->email;
                                $name = $data->name;
                                $replyemail = config('users.falk_email');
                                $replyname = 'Falk';

                                if($email && !in_array($email, $email_not_send_array) ){

                                    $email_not_send_array[] = $email;

                                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                        $message->to($email, $name)
                                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                        ->replyTo($replyemail, $replyname)
                                        ->subject($subject);
                                    });
                                }

                                if( $data->email2 && !in_array($data->email2, $email_not_send_array) ){

                                    $email_not_send_array[] = $data->email2;

                                    $email = $data->email2;
                                    $name = $data->name2;
                                    $replyemail = config('users.falk_email');
                                    $replyname = 'Falk';

                                    if($email){
                                        Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                            $message->to($email, $name)
                                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                            ->replyTo($replyemail, $replyname)
                                            ->subject($subject);
                                        });
                                    }

                                }

                            }else{//send mail to falk

                                $is_request = DB::table('properties_mail_logs')->where(['type' => 'insurance_request', 'record_id' => $request->record_id])->get()->count();

                                if($is_request > 0){

                                    $email = config('users.falk_email');
                                    $name = 'Falk';
                                    $replyemail = $data->email;
                                    $replyname = $data->name;

                                    if( $email && !in_array($email, $email_not_send_array) ){

                                        $email_not_send_array[] = $email;

                                        Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                            $message->to($email, $name)
                                            ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                            ->replyTo($replyemail, $replyname)
                                            ->subject($subject);
                                        });
                                    }
                                }

                            }

                            // send mail to uploaded user
                            if( $data->upload_user_email && !in_array($data->upload_user_email, $email_not_send_array) ){

                                $email_not_send_array[] = $data->upload_user_email;

                                $email = $data->upload_user_email;
                                $name = $data->upload_user_name;
                                $replyemail = $user->email;
                                $replyname = $user->name;

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });

                            }

                        }
                    }

                    if($subject && $text){

                        $replyemail = $user->email;
                        $replyname = $user->name;

                        // send mail to custom users
                        $custom_mail = ($request->comment) ? fetch_mails_by_string($request->comment) : [];
                        if(!empty($custom_mail)){

                            $custom_mail = array_unique($custom_mail);

                            Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $custom_mail, $replyemail, $replyname) {
                                $message->to($custom_mail)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($replyemail, $replyname)
                                ->subject($subject);
                            });
                        }

                        /*----------SEND MAIL TO HV USERS MAIL - START------------*/
                        $propertyRes = DB::table('properties as p')
                                        ->selectRaw('hvbu.name as hvbu_name, hvbu.email as hvbu_email, hvpm.name as hvpm_name, hvpm.email as hvpm_email')
                                        ->leftjoin('users as hvbu', 'p.hvbu_id', '=', 'hvbu.id')
                                        ->leftjoin('users as hvpm', 'p.hvpm_id', '=', 'hvpm.id')
                                        ->where('p.id', $request->property_id)
                                        ->first();
                        if($propertyRes){
                            if( $propertyRes->hvbu_email && !in_array($propertyRes->hvbu_email, $email_not_send_array) ){

                                $email_not_send_array[] = $propertyRes->hvbu_email;

                                $email = $propertyRes->hvbu_email;
                                $name = $propertyRes->hvbu_name;

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });
                            }

                            if( $propertyRes->hvpm_email && !in_array($propertyRes->hvpm_email, $email_not_send_array) ){

                                $email_not_send_array[] = $propertyRes->hvpm_email;

                                $email = $propertyRes->hvpm_email;
                                $name = $propertyRes->hvpm_name;

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });
                            }
                        }
                        /*----------SEND MAIL TO HV USERS MAIL - END------------*/
                    }

                    $result = ['status' => true, 'message' => 'Comment add successfully.', 'data' => ['comment' => $modal->comment, 'name' => $user->name, 'created_at' => show_datetime_format($modal->created_at)]];
                }else{
                    $result = ['status' => false, 'message' => 'Comment add fail! Try again.', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);

    }

    public function deleteInsuranceDetailComment($id){
        $model = PropertiesComment::find($id);
        if($model->delete()){
            $response = ['status' => true, 'message' => 'Comment delete successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Comment delete fail!'];
        }
        return response()->json($response);
    }

    public function iframeTenantMaster($id){

        $workingDir = '/';

        $tenant_master = TenantMaster::where('id', $id)->first();
        if($tenant_master){
            $item = TenancyScheduleItem::selectRaw('tenancy_schedule_items.*, tenancy_schedules.business_date')
                        ->join('tenancy_schedules', 'tenancy_schedules.id', '=', 'tenancy_schedule_items.tenancy_schedule_id')
                        ->where('tenancy_schedule_items.tenant_master_id', $tenant_master->id)
                        ->orderBy('tenancy_schedule_items.id', 'desc')
                        ->first();
            if($item){
                if($item->termination_date && $item->termination_date != "0000-00-00"  && $item->termination_date > date('Y-m-d')){
                    $item->date_diff_average = TenancySchedulesService::diff360($item->termination_date, $item->business_date)/365;
                }
                else{
                    $item->date_diff_average = TenancySchedulesService::diff360($item->rent_end, $item->business_date)/365;
                }

                if($item->type == config('tenancy_schedule.item_type.business')){
                    $item->remaining_time_in_eur = $item->actual_net_rent * $item->date_diff_average * 12;
                }

                $tp = TenantPayment::where('tenancy_schedule_item_id',$item->id)->get();

                foreach ($tp as $key => $tp_row) {
                    if($tp_row->year==2018)
                    {
                        $item->bka_date_2018 = $tp_row->bka_date;
                        $item->payment_date_2018 = $tp_row->payment_date;
                        $item->amount_2018 = $tp_row->amount;
                        $item->tenant_payments_2018 = $tp_row;

                    }
                    if($tp_row->year==2019)
                    {
                        $item->bka_date_2019 = $tp_row->bka_date;
                        $item->payment_date_2019 = $tp_row->payment_date;
                        $item->amount_2019 = $tp_row->amount;
                        $item->tenant_payments_2019 = $tp_row;

                    }
                    if($tp_row->year==2020)
                    {
                        $item->bka_date_2020 = $tp_row->bka_date;
                        $item->payment_date_2020 = $tp_row->payment_date;
                        $item->amount_2020 = $tp_row->amount;
                        $item->tenant_payments_2020 = $tp_row;
                    }
                }

                if(!$item->opos_mieter){

                    $rentdata = PropertyRentPaids::where('property_id',$tenant_master->property_id)->groupBy('mieter')->get();

                    $rent_number_array = array();
                    foreach ($rentdata as $key => $value) {
                        $rent_number_array[] = trim($value->mieter);
                    }

                    foreach ($rent_number_array as $key => $value) {

                        if ($value && $item->name && strpos(str_replace(' ', '', $value), str_replace(' ', '',$item->name)) !== false){
                            $rent_n = explode(' ', $value);
                            if(isset($rent_n[0]))
                            $item->opos_mieter = $rent_n[0];
                            break;
                        }
                    }
                }
            }

            $tenant_master->item = $item;

            $p_dir = PropertiesDirectory::where('property_id', $tenant_master->property_id)->where('parent_id', 0)->first();
            if($p_dir){
                $workingDir = $p_dir->dir_path;
            }
        }

        $as_users = User::whereRaw('user_status=1 and (role=4 or second_role=4 or id=1 or id=10)')->get();

        $td1 = array('Nein','Ja');
        $td2 = array(1=>'1 (keine Probleme)',2=>'2 (evtl. Probleme)',3=>'3 (Mieter zahlt nicht)');
        $category_list = array("Nicht zugeordnet","Lebensmittel","Fashion","Hotel","Baumarkt","Drogerie","Sonderposten","Elektro / Spielwaren","Dienstleistung","Gewerbe","Gastro / Café","Logistik","Wohnen","Sonstiges");

        $type = (isset($tenant_master->item_type)) ? $tenant_master->item_type : '';

        return view('properties.templates.iframe_tenant_master', ['tenant_master' => $tenant_master, 'td1' => $td1, 'td2' => $td2, 'type' => $type, 'as_users' => $as_users, 'category_list' => $category_list, 'workingDir' => $workingDir]);
    }

    public function selectMieterlisteFile(Request $request, $property_id){

        $id = $request->id;
        $dirname = $request->dirname;
        $basename = $request->basename;
        $type = $request->type;

        $contents = collect(Storage::cloud()->listContents($dirname, false));
        $file = $contents->where('type','file')->where('basename', $basename)->first();

        if($file['basename'] == ''){
            $response = [
                'success' => false,
                'msg' => 'Fail to add selected file.'
            ];
        }else{

            $file_column_type = 0;
            if($request->file_column_type)
                $file_column_type = $request->file_column_type;

            TenancyScheduleItemFile::create([
                'property_id' => $property_id,
                'tenancy_schedule_item_id' => $id,
                'file_name' => $file['name'],
                'file_basename' => $file['basename'],
                'file_path' => $file['path'],
                'file_href' => 'https://drive.google.com/file/d/'.$file['basename'],
                'extension' => $file['extension'],
                'file_column_type' => $file_column_type,
                'type'=>"file"
            ]);

            $response = [
                'success' => true,
                'msg' => 'File selection succesfully.',
                'file' => $file,
                'element' => "<a href='https://drive.google.com/file/d/".$file['basename']."'  target='_blank' title='".$file['name']."'><i class='fa ".(config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file') ."' ></i></a>"
            ];

        }
        return response()->json($response);
    }
    public function selectMieterlisteDir(Request $request, $property_id){

        $id = $request->id;
        $dirname = $request->dirname;
        $basename = $request->basename;
        $type = $request->type;

        $contents = collect(Storage::cloud()->listContents($dirname, false));
        $file = $contents->where('type','dir')->where('basename', $basename)->first();

        if($file['basename'] == ''){
            $response = [
                'success' => false,
                'msg' => 'Fail to add selected file.'
            ];
        }else{
            TenancyScheduleItemFile::create([
                'property_id' => $property_id,
                'tenancy_schedule_item_id' => $id,
                'file_name' => $file['name'],
                'file_basename' => $file['basename'],
                'file_path' => $file['path'],
                'file_href' => '',
                'extension' => '',
                'type'=>"dir"
            ]);

            $clickFunction = 'loadDirectoryFiles(\''.$file["path"].'\');';
            $response = [
                'success' => true,
                'msg' => 'Directory selection succesfully.',
                'file' => $file,
                'element' => '<a href="javascript:void(0);" onClick="'.$clickFunction.'"  title="'.$file['name'].'"><i class="fa fa-folder" ></i></a>'
            ];
        }
        return response()->json($response);
    }

    public function addProvisionFile(Request $request, $property_id){

        $id = $request->id;
        $dirname = $request->dirname;
        $basename = $request->basename;
        $type = $request->type;

        $contents = collect(Storage::cloud()->listContents($dirname, false));
        $file = $contents->where('type','file')->where('basename', $basename)->first();

        if($file['basename'] == ''){
            $response = [
                'success' => false,
                'msg' => 'Fail to add selected file.'
            ];
        }else{

            $modal = Provision::find($id);
            $modal->file_name       = $file['name'];
            $modal->file_basename   = $file['basename'];
            $modal->file_path       = $file['path'];
            $modal->file_href       = 'https://drive.google.com/file/d/'.$file['basename'];
            $modal->file_extension  = $file['extension'];
            $modal->file_type       = "file";
            $modal->save();

            $response = [
                'success' => true,
                'msg' => 'File added succesfully.',
                'file' => $file,
                'element' => "<a href='https://drive.google.com/file/d/".$file['basename']."'  target='_blank' title='".$file['name']."'><i class='fa ".(config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file') ."' ></i></a>"
            ];

        }
        return response()->json($response);
    }
    public function addProvisionDir(Request $request, $property_id){

        $id = $request->id;
        $dirname = $request->dirname;
        $basename = $request->basename;
        $type = $request->type;

        $contents = collect(Storage::cloud()->listContents($dirname, false));
        $file = $contents->where('type','dir')->where('basename', $basename)->first();

        if($file['basename'] == ''){
            $response = [
                'success' => false,
                'msg' => 'Fail to add selected file.'
            ];
        }else{

            $modal = Provision::find($id);
            $modal->file_name       = $file['name'];
            $modal->file_basename   = $file['basename'];
            $modal->file_path       = $file['path'];
            $modal->file_href       = '';
            $modal->file_extension  = '';
            $modal->file_type       = "dir";
            $modal->save();

            $clickFunction = 'loadDirectoryFiles(\''.$file["path"].'\');';
            $response = [
                'success' => true,
                'msg' => 'Directory added succesfully.',
                'file' => $file,
                'element' => '<a href="javascript:void(0);" onClick="'.$clickFunction.'"  title="'.$file['name'].'"><i class="fa fa-folder" ></i></a>'
            ];
        }
        return response()->json($response);
    }

    public function addAoCost(Request $request, $property_id){
        if($request->ajax()) {

            $validator = Validator::make($request->all(), [
                "amount"    => "required",
                "month"     => "required",
                "year"      => "required",
                "type"      => "required",
            ]);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

                $user = Auth::user();


                $modal              = new LiquiplanungAoCosts;
                $modal->property_id = $property_id;
                $modal->user_id     = $user->id;
                $modal->month       = $request->month;
                $modal->year        = $request->year;
                $modal->title       = $request->title;
                $modal->amount      = make_amount($request->amount);
                $modal->type        = $request->type;
                $modal->frequency        = $request->frequency;


                if($modal->save()){
                    $result = ['status' => true, 'message' => 'Record save successfully.', 'data' => []];
                }else{
                    $result = ['status' => false, 'message' => 'Record save fail!', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getAoCost(Request $request, $property_id){
        $type = ($request->type == 1) ? 1 : 0;
        $month = (isset($request->month)) ? $request->month : '';
        $year = (isset($request->year)) ? $request->year : '';

        $res['data'] = [];
        $costs = LiquiplanungAoCosts::where('property_id', $property_id)->where('type', $type);
                    if($month && $year){
                        $costs = $costs->where('month', $month)->where('year', $year);
                    }
                    $costs = $costs->orderBy('id', 'desc')->get();
        if($costs){
            foreach ($costs as $key => $value) {

                $f = 'einmalig';
                if($value->frequency=="weekly")
                $f = 'wöchentlich';
                else if($value->frequency=="monthly")
                $f = 'monatlich';
                else if($value->frequency=="quarterly")
                $f = 'vierteljährlich';
                else if($value->frequency=="yearly")
                $f = 'jährlich';

                $delete_btn = '<button type="button" data-url="'.route('delete_ao_cost', ['id' => $value->id]).'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-ao_cost" data-type="'.$type.'"><i class="icon-trash"></i></button>';

                $res['data'][] = [
                    $f,
                    $value->title,
                    show_number($value->amount, 2),
                    $value->month,
                    $value->year,
                    $delete_btn
                ];
            }
        }
        return response()->json($res);
    }

    public function deleteAoCost($id){

        $model = LiquiplanungAoCosts::find($id);

        if($model->delete()){
            $response = ['status' => true, 'message' => 'Record delete successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Record delete fail!'];
        }
        return response()->json($response);
    }

    public function deleteLog(Request $request){
        $id     = $request->id;
        $table  = $request->tbl;

        if($table && $id){
            $modal = DB::table($table)->where('id', $id)->delete();
            if($modal){
                $result = ['status' => true, 'message' => 'Record delete successfully.'];
            }else{
                $result = ['status' => false, 'message' => 'Record delete fail!'];
            }
        }else{
            $result = ['status' => false, 'message' => 'Record delete fail!'];
        }
        return response()->json($result);
    }

    public function getTenancyItemDetail(Request $request){
        $id = $request->id;
        $res = ['status' => false, 'data' => []];

        if($id){
            $item = TenancyScheduleItem::selectRaw('id, name, rent_begin, rent_end, rental_space, vacancy_in_qm')->where('id', $id)->first();
            if($item){
                $item->date = ($item->rent_begin && $item->rent_end) ? show_date_format($item->rent_begin).' - '.show_date_format($item->rent_end) : '';
                $space = ($item->rental_space) ? $item->rental_space : $item->vacancy_in_qm;
                if(!$space)
                    $space = 0;
                $item->space = number_format($space,2,',','.');

                $item->files = "";

                foreach($item->files()->get() as $file):
                    if($file['file_column_type']==1):
                    if($file['type']=='file') :
                        $ext = config('filemanager.file_icon_array.' . $file['file_extension']);
                    $string = '<a href="'.$file['file_href'].'"   target="_blank">
                    <i  class="fa '. $ext.' fa-file " ></i></a>';
                    else:
                        $string = '<a href="javascript:void(0);"  onClick="loadDirectoryFiles('. $file['file_path'].');"  ><i class="fa fa-folder" ></i></a>';
                    endif;
                    $item->files .= $string;
                    endif;
                endforeach;


                ?>

                <?php


                $res = ['status' => true, 'data' => $item];
            }
        }
        return response()->json($res);
    }

    public function updateLiquiplanungData(Request $request, $property_id){
        $field = $request->field;
        $value = $request->value;

        $numeric_fields = ['object_effort_pm', 'effort_pm', 'current_balance', 'share_deposite'];

        if(in_array($field, $numeric_fields)){
            $value = make_amount($value);
        }
        if($field == 'not_consider_in_liquid'){
            $value = ($value) ? 1 : 0;
        }

        $modal = PropertiesCustomField::where('property_id', $property_id)->where('slug', $field)->first();
        if(!$modal){
            $modal = new PropertiesCustomField;
            $modal->property_id = $property_id;
        }
        $modal->slug = $field;
        $modal->content = $value;
        if($modal->save()){
            $res = ['status' => true, 'message' => "Field update successfully"];
        }else{
            $res = ['status' => false, 'message' => "Field update fail"];
        }
        return response()->json($res);
    }

    public function getAmLog($property_id){
        $res = DB::table('property_histories as ph')
                ->selectRaw('ph.id, ph.created_at, ph.field_type, new_am.name as new_am')
                ->leftjoin('users as new_am', 'new_am.id', '=', 'ph.new_value')
                ->where('ph.property_id', $property_id)
                ->where('ph.field_type', 'asset_m_id')
                ->orderBy('ph.created_at', 'desc')
                ->get();

        $data['data'] = [];
        if($res){
            foreach ($res as $key => $value) {
                $files_html = $this->getAmLogFiles($property_id, $value->id, 'am-log', true);
                $data['data'][] = [
                    $value->new_am,
                    show_date_format($value->created_at),
                    '<a href="javascript:void(0);" class="btn-am-log-upload" data-property-id="'.$property_id.'" data-id="'.$value->id.'" data-type="am-log" data-file_column_type="0"><i class="fa fa-link"></i></a> '.$files_html
                ];
            }
        }

        $res = DB::table('property_histories as ph')
                ->selectRaw('ph.id, ph.created_at, ph.field_type, new_am.name as new_am')
                ->leftjoin('users as new_am', 'new_am.id', '=', 'ph.old_value')
                ->where('ph.property_id', $property_id)
                ->where('ph.field_type', 'asset_m_id')
                ->first();

        $properties = Properties::where('id', $property_id)->first();

        if($properties){
            if($res){
                $files_html = $this->getAmLogFiles($property_id, null, 'am-1', true);
                $data['data'][] = [
                    $res->new_am,
                    ($properties->ubername_des_objekts2) ? show_date_format($properties->ubername_des_objekts2) : '',
                    '<a href="javascript:void(0);" class="btn-am-log-upload" data-property-id="'.$property_id.'" data-id="" data-type="am-1" data-file_column_type="0"><i class="fa fa-link"></i></a> '.$files_html
                ];
            }elseif(isset($properties->asset_manager->name) && $properties->asset_manager->name){
                $files_html = $this->getAmLogFiles($property_id, null, 'am-1', true);
                $data['data'][] = [
                    $properties->asset_manager->name,
                    ($properties->ubername_des_objekts2) ? show_date_format($properties->ubername_des_objekts2) : '',
                    '<a href="javascript:void(0);" class="btn-am-log-upload" data-property-id="'.$property_id.'" data-id="" data-type="am-1" data-file_column_type="0"><i class="fa fa-link"></i></a> '.$files_html
                ];
            }
            if($properties->asset_manager3){
                $files_html = $this->getAmLogFiles($property_id, null, 'am-2', true);
                $data['data'][] = [
                    $properties->asset_manager3,
                    ($properties->ubername_des_objekts3) ? show_date_format($properties->ubername_des_objekts3) : '',
                    '<a href="javascript:void(0);" class="btn-am-log-upload" data-property-id="'.$property_id.'" data-id="" data-type="am-2" data-file_column_type="0"><i class="fa fa-link"></i></a> '.$files_html
                ];
            }
            if($properties->asset_manager2){
                $files_html = $this->getAmLogFiles($property_id, null, 'am-3', true);
                $data['data'][] = [
                    $properties->asset_manager2,
                    ($properties->ubername_des_objekts) ? show_date_format($properties->ubername_des_objekts) : '',
                    '<a href="javascript:void(0);" class="btn-am-log-upload" data-property-id="'.$property_id.'" data-id="" data-type="am-3" data-file_column_type="0"><i class="fa fa-link"></i></a> '.$files_html
                ];
            }
        }

        return response()->json($data);
    }

    public function getAmLogFiles($property_id = null, $parent_id = null, $parent_type = null, $is_html = false){
        $res = GdriveUploadFiles::select('id', 'file_name', 'file_basename', 'file_path', 'file_href', 'extension', 'file_type', 'created_at');
        if($property_id){
            $res = $res->where('property_id', $property_id);
        }
        if($parent_id){
            $res = $res->where('parent_id', $parent_id);
        }
        if($parent_type){
            $res = $res->where('parent_type', $parent_type);
        }
        $res = $res->get();

        if($is_html){
            $html = "";
            if($res){
                foreach ($res as $file) {
                    if($file->file_type == 'file'){
                        $html .= " <a href='https://drive.google.com/file/d/".$file->file_basename."'  target='_blank' title='".$file->file_name."'><i class='fa ".(config('filemanager.file_icon_array.' . $file->extension) ?: 'fa-file') ."' ></i></a>";
                    }else{
                        $clickFunction = 'loadDirectoryFiles(\''.$file->file_path.'\');';
                        $html .= " <a href='javascript:void(0);'' onClick='".$clickFunction."' title='".$file->file_name."'><i class='fa fa-folder'></i></a>";
                    }
                }
            }
            return $html;
        }else{
            return $res;
        }
    }

    public function selectAmLogFile(Request $request){
        $id             = $request->id;
        $type           = $request->type;
        $property_id    = $request->property_id;

        $dirname        = $request->dirname;
        $basename       = $request->basename;

        $contents = collect(Storage::cloud()->listContents($dirname, false));
        $file = $contents ->where('type','file')->where('basename', $basename)->first();
        if($file['basename'] == ''){
            $response = [
                'status' => false,
                'message' => 'Fail to add selected file.'
            ];
        }else{

            $modal = new GdriveUploadFiles;
            $modal->property_id = $property_id;
            if($id){
                $modal->parent_id = $id;
            }
            $modal->parent_type = $type;
            $modal->file_name = $file['name'];
            $modal->file_basename = $file['basename'];
            $modal->file_path = $file['path'];
            $modal->file_href = 'https://drive.google.com/file/d/'.$file['basename'];
            $modal->extension = $file['extension'];
            $modal->file_type = 'file';
            $modal->save();

            $response = [
                'status' => true,
                'message' => 'File selection succesfully.',
                'file' => $file,
                'element' => "<a href='https://drive.google.com/file/d/".$file['basename']."'  target='_blank' title='".$file['name']."'><i class='fa ".(config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file') ."' ></i></a>"
            ];
        }
        return response()->json($response);
    }

    public function selectAmLogDir(Request $request){
        $id             = $request->id;
        $type           = $request->type;
        $property_id    = $request->property_id;

        $dirname        = $request->dirname;
        $basename       = $request->basename;

        $contents = collect(Storage::cloud()->listContents($dirname, false));
        $file = $contents->where('basename', $basename)->first();
        if($file['basename'] == ''){
            $response = [
                'status' => false,
                'message' => 'Fail to add selected file.'
            ];
        }else{

            $modal = new GdriveUploadFiles;
            $modal->property_id = $property_id;
            if($id){
                $modal->parent_id = $id;
            }
            $modal->parent_type = $type;
            $modal->file_name = $file['name'];
            $modal->file_basename = $file['basename'];
            $modal->file_path = $file['path'];
            $modal->file_type = 'dir';
            $modal->save();

            $clickFunction = 'loadDirectoryFiles(\''.$file["path"].'\');';
            $response = [
                'status' => true,
                'message' => 'Directory selection succesfully.',
                'file' => $file,
                'element' => '<a href="javascript:void(0);" onClick="'.$clickFunction.'"  title="'.$file['name'].'"><i class="fa fa-folder" ></i></a>'
            ];
        }

        return response()->json($response);
    }

    Public function updatePropertyCustomField(Request $request, $property_id){
        $field = $request->field;
        $value = $request->value;

        if($field == 'annuitatendarlehen_aktuelle_restschuld'){
            $value = make_amount($value);
        }

        $modal = PropertiesCustomField::where('slug', $field)->where('property_id', $property_id)->first();
        if(!$modal){
            $modal = new PropertiesCustomField;
            $modal->property_id = $property_id;
            $modal->slug = $field;
        }
        $modal->content = $value;
        if($modal->save()){
            $response = ['status' => true, 'message' => 'Field update successfully'];
        }else{
            $response = ['status' => fali, 'message' => 'Field update fail'];
        }

        return response()->json($response);
    }

    public function getReleasedAngebote(){

        $user = Auth::user();

        $insurance_tab = PropertyInsuranceTab::select('property_insurance_tabs.id', 'property_insurance_tabs.title', 'property_insurance_tabs.property_id', 'p.name_of_property',
            DB::raw("CASE WHEN EXISTS (SELECT id FROM properties_mail_logs WHERE (type = 'insurance_release' OR type = 'insurancetab_release') AND (tab='insurance_tab' OR tab='insurancetab2') AND record_id = property_insurance_tabs.id) THEN 1 ELSE 0 END as release_status"),
            DB::raw("(SELECT properties_mail_logs.created_at FROM properties_mail_logs WHERE (type = 'insurance_release' OR type = 'insurancetab_release') AND record_id = property_insurance_tabs.id AND (tab='insurance_tab' OR tab='insurancetab2') ORDER BY properties_mail_logs.created_at DESC LIMIT 1) as release_date")
            )
        ->join('properties as p', 'p.id', '=', 'property_insurance_tabs.property_id')
        ->where('property_insurance_tabs.deleted', 0);
        if($user->role = 7){
            $insurance_tab = $insurance_tab->where('p.hvbu_id', $user->id);
        }elseif($user->role = 8){
            $insurance_tab = $insurance_tab->where('p.hvpm_id', $user->id);
        }
        $insurance_tab = $insurance_tab->havingRaw('release_status = 1')->orderBy('property_insurance_tabs.id', 'desc')->orderBy('release_date', 'desc')->get();

        return View::make('properties.templates.released_angebote', ['insurance_tab' => $insurance_tab, 'user' => $user])->render();
    }

}
