<?php

namespace App\Http\Controllers;

use App\Models\Tenants;
use App\Services\TenantsService;
use Illuminate\Http\Request;

class TenantsController extends Controller
{
    public function createTenant(Request $request){
        $response = [
            'success' => false,
            'msg' => 'Failed to updated tenant'
        ];
        $input = $request->all();
        if (TenantsService::create($input)) {
            $response = [
                'success' => true,
                'msg' => ''
            ];
        }

        echo response()->json($response);
        die;

    }
    public function updateTenantByField(Request $request, $tenant_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated tenant'
        ];

        $tenant = Tenants::where('id', '=', $tenant_id)->first();

        if( empty( $tenant ) ) {
            $response['msg'] = 'Not found tenant';
            return response()->json($tenant);
        }

        // Clone tenant
        $input = $tenant;

        // New value
        $input[$request->pk] = $request->value;

        if (TenantsService::update( $input, $tenant )) {
            $response = [
                'success' => true,
                'msg' => ''
            ];
        }

        return response()->json($response);
    }

}
