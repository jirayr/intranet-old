<?php
    namespace App\Http\Controllers;
    use Illuminate\Http\Request;
    use Auth;
    use App\Models\Properties;
    use App\Services\PropertiesService;
    use Symfony\Component\HttpKernel\Profiler\Profile;
    class GoogleMapController extends Controller
    {
            /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $user_id =  Auth::id();
            $properties = Properties::where('user_id','=',$user_id)->get();
            return view('google-map.index', compact('properties'));
        }
    }