<?php

namespace App\Http\Controllers;

use App\Models\PropertyManagement;
use App\Models\TenancySchedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use DB;

class PropertyManagementController extends Controller
{

    public function add(Request $request)
    {
        if ($request->add) {
            $property              = new PropertyManagement;
            $property->property_id = $request->id;
            $property->save();
        } else if ($request->delete) {

            $property = PropertyManagement::find($request->id);
            if ($property) {
                $property->delete();
            }
            $request->id = $request->property_id;
        }

        $TenancySchedule       = TenancySchedule::select('text_json', 'property_id')->where('property_id', $request->id)->first();
        $total_actual_net_rent = 0;
        if ($TenancySchedule && $TenancySchedule->text_json) {
            $jsonarray = json_decode($TenancySchedule->text_json, true);
        }

        if (isset($jsonarray['total_actual_net_rent'])) {
            $total_actual_net_rent = $jsonarray['total_actual_net_rent'];
        }

        $data = PropertyManagement::select('property_managements.*',DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'release_management' AND properties_mail_logs.record_id = property_managements.id) THEN 1 ELSE 0 END as is_release"))->where('property_id', $request->id)->get();

        $a['table'] = View::make('property_management.table', compact('total_actual_net_rent', 'data'))->render();

        return $a;

    }

    public function update_row(Request $request)
    {
        $row = PropertyManagement::where('id', $request->id)->first();

        if ($row) {
            $pk = $request->pk;
            if ($request->pk == 'percent') {
                $request->value = str_replace(',', '.', $request->value);
            }

            $row->$pk = $request->value;
            $row->save();

        }

        $TenancySchedule       = TenancySchedule::select('text_json', 'property_id')->where('property_id', $request->property_id)->first();
        $total_actual_net_rent = 0;
        if ($TenancySchedule && $TenancySchedule->text_json) {
            $jsonarray = json_decode($TenancySchedule->text_json, true);
        }

        if (isset($jsonarray['total_actual_net_rent'])) {
            $total_actual_net_rent = $jsonarray['total_actual_net_rent'];
        }

        $data = PropertyManagement::where('property_id', $request->property_id)->get();

        $a['table'] = View::make('property_management.table', compact('total_actual_net_rent', 'data'))->render();

        return $a;

    }
}
