<?php

namespace App\Http\Controllers;

use App\Models\ModulePermission;
use App\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Storage;
use File;
use App\Models\PropertiesDirectory;
use App\Models\PropertyInsuranceTab;
use App\Models\Directory_Permission;
use App\Models\Properties;
use App\Helpers\PropertiesHelperClass;
use App\Services\TenancySchedulesService;
use Illuminate\Support\Facades\Log;
use Validator;
use Mail;
use DB;

use Illuminate\Http\Request;

class ExternalUserController extends Controller
{

    public function invoiceobjects($propertyId)
    {
        $internal = 1;

        $user = Auth::user();
        if($user->role >= 6 || $user->second_role >= 6){
            return redirect('/extern_user');
        }
        $propertyStatusArr = config('properties.status_with_drive');
        // For Objekte Bestand
        $propertyStatusArr = $propertyStatusArr[6];
        if($propertyStatusArr[1] == "")
        {
            die('Property status directory not exist.'. $propertyStatusArr[0] );
        }
        $root_dir = $propertyStatusArr[1];

        $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) {
                    $join->on('properties.id', '=', 'properties_directories.property_id')
                        ->whereIn('properties.status', array(6,11,13))
                        ->where('properties_directories.parent_id', '=', 0);
                })->get();
            

        $permission = ModulePermission::where('user_id', $user->id)->get();
        // dd($propertiesDirectory->toArray());
        // $permission = Directory_Permission::where('user_id', $user->id)->get();
        return view('external_user.invoice-status-dir-list', compact('propertiesDirectory',  'permission', 'user','internal','propertyId'));
    }
    public function index()
    {
        $internal = 0;
        $user = Auth::user();
        $propertyStatusArr = config('properties.status_with_drive');
        // For Objekte Bestand
        $propertyStatusArr = $propertyStatusArr[6];
        if($propertyStatusArr[1] == "")
        {
            die('Property status directory not exist.'. $propertyStatusArr[0] );
        }
        $root_dir = $propertyStatusArr[1];

        if(strpos($user->email, 'ok-hv.de') !== false)
        {
            // echo "here"; die;
            $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) use ($user){
                $join->on('properties.id', '=', 'properties_directories.property_id')
                    ->whereRaw('properties.hausmaxx="OK"')
                    ->where('properties_directories.parent_id', '=', 0);
            })->get();   
        }
        else if($user->role==config('auth.role.tax_consultant'))
        {
            $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) use ($user){
                $join->on('properties.id', '=', 'properties_directories.property_id')
                    ->whereRaw('(properties.steuerberater = "'.$user->id.'" OR properties.steuerberater1 = "'.$user->id.'")')
                    ->where('properties_directories.parent_id', '=', 0);
            })->get();
        }
        else if($user->role ==config('auth.role.service_provider'))
        {
            $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) use ($user){
                $join->on('properties.id', '=', 'properties_directories.property_id')
                    ->whereRaw('(properties.service_provider_id='.$user->id.' OR properties.service_provider_id1='.$user->id.' OR properties.service_provider_id2='.$user->id.' OR properties.service_provider_id3='.$user->id.' OR properties.service_provider_id4='.$user->id.')')
                    ->where('properties_directories.parent_id', '=', 0);
            })->get();
        }
        elseif($user->role ==config('auth.role.hvbu') || $user->role ==config('auth.role.hvpm') || $user->second_role ==config('auth.role.hvbu') || $user->second_role ==config('auth.role.hvpm'))
        {
            $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) use ($user){
                $join->on('properties.id', '=', 'properties_directories.property_id')
                    ->whereRaw('(properties.hvbu_id='.$user->id.' OR properties.hvpm_id='.$user->id.' OR properties.hausmaxx="'.$user->company.'")')
                    ->where('properties_directories.parent_id', '=', 0);
            })->get();
        }
        elseif($user->email == "courteaux@hotel-westerburg.de"){
            $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) use ($user){
                $join->on('properties.id', '=', 'properties_directories.property_id')
                    ->whereIn('properties.id', array(1197))
                    ->where('properties_directories.parent_id', '=', 0);
                    // ->where('properties.hausmaxx', '=', $user->company);
            })->get();
        }
        elseif($user->email == "jana.schilling@ok-hv.de"){
            $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) use ($user){
                $join->on('properties.id', '=', 'properties_directories.property_id')
                    ->whereIn('properties.status', array(6,11,13,8))
                    ->where('properties_directories.parent_id', '=', 0)
                    ->where('properties.hausmaxx', '=', $user->company);
            })->get();
        }
        elseif ($user->company)
        {
            $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) use ($user){
                $join->on('properties.id', '=', 'properties_directories.property_id')
                    ->whereIn('properties.status', array(6,11,13))
                    ->where('properties_directories.parent_id', '=', 0)
                    ->where('properties.hausmaxx', '=', $user->company);
            })->get();
        } else
            {
                $propertiesDirectory = PropertiesDirectory::join('properties', function ($join) {
                    $join->on('properties.id', '=', 'properties_directories.property_id')
                        ->whereIn('properties.status', array(6,11,13))
                        ->where('properties_directories.parent_id', '=', 0);
                })->get();
            }

        $permission = ModulePermission::where('user_id', $user->id)->get();
        // dd($propertiesDirectory->toArray());
        // $permission = Directory_Permission::where('user_id', $user->id)->get();
        return view('external_user.status-dir-list', compact('propertiesDirectory',  'permission', 'user','internal'));
    }
    public function showProperty(Request $request, $id)
    {
        $properties = Properties::where('id', '=', $id)->where('Ist', 0)->where('soll', 0)->first();
        $workingDir = '/';
        $p_dir = PropertiesDirectory::where('property_id', $id)->where('parent_id', 0)->first();
        if($p_dir){
            $workingDir = $p_dir->dir_path;            
        }
        $user = Auth::user();
        $insurance_tab_title = array();

        if($user->role !=config('auth.role.tax_consultant')){
            /*$insurance_tab_title = PropertyInsuranceTab::select('id', 'title',DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurance_release' AND record_id = property_insurance_tabs.id AND tab='insurance_tab') THEN 1 ELSE 0 END as release_status"))->where('property_id', $id)->where('deleted', 0)->get();*/
            $insurance_tab_title = PropertyInsuranceTab::select('id', 'title', 'property_id',
            DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurance_release' AND record_id = property_insurance_tabs.id AND tab='insurance_tab') THEN 1 ELSE 0 END as release_status"),
            DB::raw("(SELECT not_release FROM property_insurance_tab_details WHERE property_insurance_tab_id = property_insurance_tabs.id AND not_release != 0 LIMIT 1) as not_release"))->where('property_id', $id)->where('deleted', 0)->get();
        }

        

        $active_users = User::whereRaw('role<6 and user_deleted=0 and user_status=1')->get();

        // return view('properties.show', compact('emails','excelValue50','excelValueE29', 'excelValueI49', 'excelValueE31', 'excelFileCellD48', 'excelFileCellG59', 'excelValueH49', 'excelFileCellD42', 'total_ccc', 'verkauf_tab', 'properties', 'send_email_template2_sending_info', 'properties1', 'bank_popup_data', 'list', 'id', 'banks', 'tab', 'tenants', 'schl_banks', 'tenancy_schedule_data', 'fake_bank', 'bank_all', 'propertiesExtra1s', 'properties_pdf', 'comments', 'countries_ad', 'mietflachen', 'ads', 'previous_states', 'previous_cities', 'insurances', 'property_service_providers', 'maintenance', 'investation', 'tr_users', 'sale_data', 'ps_data', 'buy_data', 'pb_data', 'as_users', 'ist_soll_properties', 'api_atachment_imgs', 'email_template', 'email_template_users', 'email_template2', 'getuploaditemlist', 'vads', 'vads_images','vprevious_states', 'propertiescheckd', 'vprevious_cities', 'sh', 'bankers_email_info', 'ccomments', 'users', 'exportads', 'eprevious_states', 'eprevious_cities', 'contact_queries', 'userFormData', 'loansMirrorData', 'propertyEinkaufFiles', 'propertyEinkaufDir','buy_data1','buy_data2','total_ccc_verkauf','sale_data1','mails_logs','mails_log_data','tenant_name','lativity','ractivity','EmpfehlungDetail_arr','tenant_name_id','tenant_name_id2','release_arr', 'workingDir','banking_info', 'contact_person','provision_info','provision_release','attchments', 'assetsManagerMails','sendEmails','rented_list'));

        $hvbu_users = User::whereRaw('user_status=1 and (role='.config('auth.role.hvbu').' OR second_role='.config('auth.role.hvbu').')')->get();
        $hvpm_users = User::whereRaw('user_status=1 and (role='.config('auth.role.hvpm').' OR second_role='.config('auth.role.hvpm').')')->get();

        // pre($hvbu_users);
        $update_date = 0;

        $tenancy_schedule_data = TenancySchedulesService::get($id, $update_date);
        
        

        $hidesidebar = true;
        $internal = $request->internal;
        if($internal)
            return view('external_user.invoiceproperties', compact('id', 'hidesidebar', 'workingDir', 'properties','user','insurance_tab_title', 'active_users','internal','hvbu_users','hvpm_users','tenancy_schedule_data'));
        else
            return view('external_user.properties', compact('id', 'hidesidebar', 'workingDir', 'properties','user','insurance_tab_title', 'active_users','internal','hvbu_users','hvpm_users','tenancy_schedule_data'));
    }

    public function dashboard(){
        
        $user = Auth::user();

        if(!in_array($user->role, [7,8])){
            return redirect('/');
        }

        $active_users = User::whereRaw('(role < 6 OR role = 7 OR role = 8) and user_deleted = 0 and user_status = 1')->get();

        return view('external_user.dashboard', ['user' => $user, 'active_users' => $active_users]);
    }
}
