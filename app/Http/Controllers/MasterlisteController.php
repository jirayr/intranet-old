<?php

namespace App\Http\Controllers;

use App\Models\TenancySchedule;
use App\Models\TenancyScheduleItem;
use App\Models\Masterliste;
use App\Models\Properties;
use App\Services\TenancySchedulesService;
use App\Services\MasterlisteService;
use App\User;
use App\Models\Banks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MasterlisteController extends Controller
{
    public function index(){
		$users = User::all();
		$users_data="{";
		foreach($users as $index=>$user){
			
			$users_data .= "'" . $user->id . "':'" . $user->name . "'";
			if($index != count($users) - 1){
				$users_data .= ",";

			}else{
			$users_data.="}";
			}
		}
        $tenancy_schedules = TenancySchedule::all();
         $masterlistes = Masterliste::all();
         // print "<pre>";
         // print_r($masterlistes); die;
		 foreach($masterlistes as $key=>$masterliste){
			 // if($masterliste->asset_manager){
				// $masterliste->asset_manager_name = User::where('id', $masterliste->asset_manager)->first()->name;
			 // }
			 
              $pr = Properties::where('masterliste_id', $masterliste->id)->where('status',6)->first();

              if(!$pr){
                unset($masterlistes[$key]);
                continue;
              }
                


			 $masterliste->property_id = ($pr) ? $pr->id :null;
            $masterliste->property = $pr;

            if($pr && isset($pr->asset_manager))
            {
                $masterliste->asset_manager_name = $pr->asset_manager->name;
            }

           


            $tenancy_schedule_data = TenancySchedulesService::get($masterliste->property_id);
            $masterliste->tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : null;
		 }
        foreach ($tenancy_schedules as $tenancy_schedule){
            $u = User::where('id', $tenancy_schedule->creator_id)->first();
            if($u)
                $tenancy_schedule->creator = $u->name;
            else
                $tenancy_schedule->creator = "";
            $tenancy_schedule->items = TenancyScheduleItem::where('tenancy_schedule_id', $tenancy_schedule->id)->get();

            $total_live_rental_space = 0;
            $total_business_rental_space = 0;
            $total_live_actual_net_rent = 0;
            $total_business_actual_net_rent = 0;

            foreach ($tenancy_schedule->items as $item){

                $remaining_time_in_eur = $item->remaining_time * $item->actual_net_rent * 12;
                $item->remaining_time_in_eur = $remaining_time_in_eur;

                if ($item->type == config('tenancy_schedule.item_type.live')){
                    $total_live_rental_space += $item->rental_space;
                    $total_live_actual_net_rent += $item->actual_net_rent;
                }

                if ($item->type == config('tenancy_schedule.item_type.business')){
                    $total_business_rental_space += $item->rental_space;
                    $total_business_actual_net_rent += $item->actual_net_rent;
                }

            }
            $tenancy_schedule->calculations = [
                'total_live_rental_space' => $total_live_rental_space,
                'total_live_actual_net_rent' => $total_live_actual_net_rent,
                'total_business_rental_space' => $total_business_rental_space,
                'total_business_actual_net_rent' => $total_business_actual_net_rent,
                'total_rental_space' => $total_live_rental_space + $total_business_rental_space,
                'total_actual_net_rent' => $total_live_actual_net_rent + $total_business_actual_net_rent

            ];
        }

        if (isset($_GET['selecting_tenancy_schedule'])){
            $selecting_tenancy_schedule = $_GET['selecting_tenancy_schedule'];
        } else {
            if (count($tenancy_schedules) > 0){
                $selecting_tenancy_schedule = $tenancy_schedules[0]->id;
            }
            else $selecting_tenancy_schedule = -1;

        }
		
        // print_r($tenancy_schedules);
        // die();	
			

            // print "<pre>";
         // print_r($masterlistes); die;
		
		$banks = Banks::all();
        return view('tenancy_schedules.index',
            compact('tenancy_schedules', 'selecting_tenancy_schedule', 'masterlistes','users_data','banks'));
    }

    public function show(){
		$users = User::all();
		$users_data="{";
		foreach($users as $index=>$user){
			
			$users_data .= "'" . $user->id . "':'" . $user->name . "'";
			if($index != count($users) - 1){
				$users_data .= ",";
				
			}else{
			$users_data.="}";
			}
		}
        if(Auth::user()->isAdmin()){
            $masterlistes = Masterliste::all();
        }else{
            $user_id = Auth::user()->getAuthIdentifier();
            $masterlistes = Masterliste::get();
        }

        $luser = Auth::user();
        $admin = $luser->isAdmin();
        

        foreach($masterlistes as $key=>$masterliste){
             if($masterliste->asset_manager){
                $masterliste->asset_manager_name = User::where('id', $masterliste->asset_manager)->first()->name;
             }
             
              // $pr = Properties::where('masterliste_id', $masterliste->id)->first();

              $pr = Properties::where('masterliste_id', $masterliste->id)->where('status',6)->first();

              if(!$pr){
                unset($masterlistes[$key]);
                continue;
              }


             $masterliste->property_id = ($pr) ? $pr->id :null;
            $masterliste->property = $pr;

            if($pr && isset($pr->asset_manager))
            {
                $masterliste->asset_manager_name = $pr->asset_manager->name;
            }

            if(!$admin && $pr &&  $luser->id != $pr->asset_m_id)
            {
                unset($masterlistes[$key]);
                continue;
            }

            $tenancy_schedule_data = TenancySchedulesService::get($masterliste->property_id);
            $masterliste->tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : null;
         }
         $banks = Banks::all();
        return view('tenancy_schedules.show',compact('masterlistes','users_data','banks'));
    }

	public function createMasterliste(Request $request){
        $response = [
            'success' => false,
            'msg' => 'Failed to updated tenant'
        ];
        $input = $request->all();
	
        if (Masterliste::create($input)) {
            $response = [
                'success' => true,
                'msg' => ''
            ];
        }

        echo response()->json($response);
        die;

    }
	
	public function updateMasterlisteByField(Request $request, $masterliste_id) {
        $response = [
            'success' => false,
            'msg' => 'Failed to updated tenant'
        ];

        $masterliste = Masterliste::where('id', '=', $masterliste_id)->first();

        if( empty( $masterliste ) ) {
            $response['msg'] = 'Not found tenant';
            return response()->json($masterliste);
        }

        // Clone masterliste
        $input = $masterliste;

        if($request->pk=="asset_manager")
        {
            $p = Properties::where('masterliste_id', $masterliste->id)->where('Ist', '!=', 0)->first();
            if($p)
            {
                $p->asset_m_id = $request->value;
                $p->save();
            }

        }

        // New value	
        $input[$request->pk] = $request->value;
		$input = MasterlisteService::calculate_masterliste($input);
        if (MasterlisteService::update( $input, $masterliste )) {
            $response = [
                'success' => true,
                'msg' => ''
            ];
        }

        return response()->json($response);
    }
	public function delete(Request $request)
	{
		$response = [
            'success' => false,
            'msg' => 'Failed to delete masterliste'
        ];
		$input = $request->all();
		$masterliste_id = isset( $input['id'] ) ? $input['id'] : 0;
		$masterliste = Masterliste::where('id', '=', $masterliste_id)->first();
		if( MasterlisteService::delete( $masterliste ) ) {
			$response = [
                'success' => true,
                'msg' => ''
            ];
		}
		echo response()->json($response);
        die;
	}



}
