<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Feedback;
use App\Services\FeedbackService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    //
	public function index(){
		$perPages = 25;
		if (Auth::user()->isAdmin()){
			
			$feedbacks = Feedback::paginate($perPages);
		}else{
			$feedbacks = Feedback::where('user_id',Auth::user()->id)->paginate($perPages);
			
		}
		// die;
		
		foreach($feedbacks as $k=>$feedback){

			$u = User::where('id', $feedback->user_id)->first();
			if($u)
				$feedback->user_name = $u->name;
			else
				$feedback->user_name = "";
			
			
		}
		return view('feedback.index',['feedbacks'=>$feedbacks]);
		echo "123";	
	}
	
	public function add(Request $request){
		$input = $request->all();
		FeedbackService::create( $input);
		return  back();
	}
}
