<?php

namespace App\Http\Controllers;

use App\Models\Masterliste;
use App\Models\Properties;
use App\Models\TenancySchedule;
use App\Models\TenancyScheduleItem;
use App\Services\TenancySchedulesService;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Auth;

class AssetManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $input  = $request->all();
        $status = 6;
        $user = Auth::user();

        // echo $status; die;

        // print_r($input); die;
        // $user = sizeof(User::all());

        if ($input && isset($input['type'])) {
            // die;
            if ($input['type'] == "r1" || $input['type'] == "v1") {

                $status1 = array(10, 12);
                $query   = Properties::selectRaw('users.name as name,asset_m_id,properties.id,name_of_property')
                    ->leftjoin('users', 'users.id', '=', 'asset_m_id')
                    ->whereIn('status', $status1)
                    ->where('Ist', 0)->where('soll', 0);
            } else {
                $query = Properties::selectRaw('users.name as name,asset_m_id,properties.id,name_of_property')->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
                    ->join('users', 'users.id', '=', 'asset_m_id')
                    ->where('status', $status)
                    ->where('Ist', 0)->where('soll', 0);
            }

            if ($input['asset_manager']) {
                $query->where('asset_m_id', $input['asset_manager']);
            }

            if ($input['property_id']) {
                $query->where('properties.id', $input['property_id']);
            }

            $data = $query->get();

            // echo count($data); die;

            $tenant_data = array();
            foreach ($data as $key => $value) {

                if ($input['type'] == "r1" || $input['type'] == "v1") {
                    $tenant_data[$key]['name']   = $value->name_of_property;
                    $tenant_data[$key]['id']     = $value->id;
                    $tenant_data[$key]['tenant'] = TenancySchedulesService::get($value->id);
                } elseif ($value->asset_m_id) {
                    $tenant_data[$key]['name']   = $value->name_of_property;
                    $tenant_data[$key]['id']     = $value->id;
                    $tenant_data[$key]['tenant'] = TenancySchedulesService::get($value->id);
                }
            }
            // print "<pre>";
            // print_r($tenant_data);
            // die;

            if ($input['type'] == "r" || $input['type'] == "r1") {
                $a = View::make('assetmanagement.rents', compact('tenant_data', 'input'))->render();
            } else {
                $a = View::make('assetmanagement.vacants', compact('tenant_data', 'input'))->render();
            }

            return $a;

        }

        $TenancySchedule = TenancySchedule::select('text_json', 'property_id')->groupBy('property_id')->get();

        $TenancySchedulearray = array();
        foreach ($TenancySchedule as $key => $value) {
            $TenancySchedulearray[$value->property_id] = json_decode($value->text_json, true);
        }
        // print "<pre>";
        // print_r($TenancySchedulearray); die;

        if ($input && isset($input['id'])) {
            $id = $input['id'];

            $analytics_array = array();
            if ($id == -1) {
                $status1   = array(10, 12);
                $analytics = Properties::selectRaw('transaction_m_id,users.name as name,asset_m_id,properties.id,name_of_property,purchase_date')
                    ->leftjoin('users', 'users.id', '=', 'asset_m_id')
                    ->whereIn('status', $status1)
                    ->where('Ist', 0)->where('soll', 0)->get();

                // print_r(count($analytics)); die;
            } else {

                $analytics = Properties::selectRaw('users.name as name,asset_m_id,properties.id,name_of_property')->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
            ->join('users', 'users.id', '=', 'asset_m_id')
            ->where('status', $status)
            ->where('Ist', 0)->where('soll', 0)->where('asset_m_id', $id)->get();


                // $analytics = Properties::selectRaw('users.name as name,asset_m_id,properties.id,name_of_property')->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
                    // ->join('users', 'users.id', '=', 'asset_m_id')
                    // ->where('status', $status)
                    // ->where('Ist', 0)->where('soll', 0)->where('asset_m_id', $id)->get();
            }
            // pre($analytics);
            $key = 0;
            foreach ($analytics as $value) {

                $property_id = $value->id;

                $analytics_array[$key]['purchase_date']      = "";
                $purchase_date_prs = Properties::select('purchase_date')->whereRaw('main_property_id='.$property_id)->first();
                if($purchase_date_prs)
                $analytics_array[$key]['purchase_date']      = $purchase_date_prs->purchase_date;

                $analytics_array[$key]['asset_manager']      = $value->asset_m_id;
                $analytics_array[$key]['asset_manager_name'] = $value->name;

                $analytics_array[$key]['transaction_m_id'] = $value->transaction_m_id;

                $analytics_array[$key]['id'] = $value->id;
                $asset_name_array[$key]      = $value->name;

                $analytics_array[$key]['flat_in_qm']  = 0;
                $analytics_array[$key]['rented_area'] = 0;
                $analytics_array[$key]['vacancy']     = 0;
                $analytics_array[$key]['rent_net_pm'] = 0;

                $analytics_array[$key]['potential_pm'] = 0;
                $analytics_array[$key]['object']       = "";

                $analytics_array[$key]['rents']   = 0;
                $analytics_array[$key]['vacants'] = 0;

                $analytics_array[$key]['object'] = $value->name_of_property;

                if (!isset($TenancySchedulearray[$property_id])) {
                    $key++;

                    continue;
                }

                $json_array = $TenancySchedulearray[$property_id];

                $analytics_array[$key]['flat_in_qm']  = $json_array['mi9'] + $json_array['mi10'];
                $analytics_array[$key]['rented_area'] = $json_array['total_live_rental_space'] + $json_array['total_business_rental_space'];
                $analytics_array[$key]['vacancy']     = $json_array['total_live_vacancy_in_qm'] + $json_array['total_business_vacancy_in_qm'];
                $analytics_array[$key]['rent_net_pm'] = $json_array['total_actual_net_rent'];

                $analytics_array[$key]['potential_pm'] = $json_array['potenzial_eur_jahr'];

                $analytics_array[$key]['rents']   = $json_array['businesscount'] + $json_array['livecount'];
                $analytics_array[$key]['vacants'] = $json_array['vbusinesscount'] + $json_array['vlivecount'];

                $key++;

            }
            $tenancy_items = TenancySchedule::leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->where('type', 3)->orWhere('type', '=', 4)->get();

            $tenancy_items_2 = TenancySchedule::leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->whereIn('type', [1, 2])->where('status', '=', 1)->orderBy('rent_end', 'asc')->get();

            $masterlistes = Masterliste::selectRaw('masterliste.*,properties.id as property_id')->join('properties', 'masterliste.id', '=', 'properties.masterliste_id')
                ->where('Ist', 0)->where('soll', 0)
                ->get();

            // print_r($masterlistes); die;

            $master_array           = array();
            $diffff                 = array();
            $summ_vermietet_percent = $summ_leerstand_percent = 0;
            $sum_rented_area        = $sum_flat_in_qm        = 0;
            $summ_vermietet_m2      = $summ_leerstand_m2      = 0;
            foreach ($masterlistes as $key => $masterliste) {
                $master_array[$masterliste->id] = $masterliste->asset_manager;

                if (!isset($TenancySchedulearray[$masterliste->property_id])) {
                    continue;
                }

                $diffff[$masterliste->id]['amount'] = $TenancySchedulearray[$masterliste->property_id]['potenzial_eur_monat'];

                $diffff[$masterliste->id]['property_id'] = $masterliste->property_id;

                $rented_area = isset($masterliste->rented_area) ? $masterliste->rented_area : 0;
                $summ_vermietet_m2 += $rented_area;
                if (isset($masterliste->flat_in_qm)) {
                    $sum_rented_area += $masterliste->rented_area;
                    $sum_flat_in_qm += $masterliste->flat_in_qm;
                }
                $vacancy = isset($masterliste->vacancy) ? $masterliste->vacancy : 0;
                $summ_leerstand_m2 += $vacancy;

            }

            if ($sum_flat_in_qm != 0) {
                $summ_vermietet_percent = $sum_rented_area / $sum_flat_in_qm * 100;
                $summ_leerstand_percent = $summ_leerstand_m2 / $sum_flat_in_qm * 100;
            }

            $pa_array = $pa_array2 = array();
            foreach ($tenancy_items as $key => $tenancy_item) {
                if ($tenancy_item->tenancy_schedule_id) {
                    $properties = Properties::where('Ist', 0)->where('soll', 0)->where('id', $tenancy_item->property_id)->first();
                    if ($properties != null && $properties->masterliste_id != null) {
                        $tenancy_item->object_name = $properties->name_of_property;
                        // $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                        if (isset($master_array[$properties->masterliste_id])) {

                            // $asset_manager = $master_array[$properties->masterliste_id];
                            $asset_manager = $properties->asset_m_id;

                            $tenancy_item->creator_name = "";
                            if (isset($asset_name_array[$asset_manager])) {
                                $tenancy_item->creator_name = $asset_name_array[$asset_manager];
                            }

                            if (isset($pa_array[$asset_manager])) {
                                $pa_array[$asset_manager] += $tenancy_item->actual_net_rent;
                            } else {
                                $pa_array[$asset_manager] = $tenancy_item->actual_net_rent;
                            }

                            if (isset($pa_array2[$tenancy_item->property_id])) {
                                $pa_array2[$tenancy_item->property_id] += $tenancy_item->actual_net_rent;
                            } else {
                                $pa_array2[$tenancy_item->property_id] = $tenancy_item->actual_net_rent;
                            }

                        } else {
                            unset($tenancy_items[$key]);
                        }
                        if ($tenancy_item->type == 4) {
                            $tenancy_item->type = 'Gewerbe';
                        } elseif ($tenancy_item->type == 3) {
                            $tenancy_item->type = 'Wohnen';
                        }

                    } else {
                        unset($tenancy_items[$key]);
                    }

                }
            }

            foreach ($tenancy_items_2 as $key => $tenancy_item) {

                if (($tenancy_item->rent_begin != '' && $tenancy_item->rent_begin > date('Y-m-d')) || ($tenancy_item->rent_end != '' && $tenancy_item->rent_end < date('Y-m-d'))) {
                    continue;
                }

                if ($tenancy_item->tenancy_schedule_id && $tenancy_item->actual_net_rent && $tenancy_item->rental_space) {
                    $properties = Properties::where('Ist', 0)->where('soll', 0)->where('id', $tenancy_item->property_id)->first();
                    if ($properties != null && $properties->masterliste_id != null) {
                        $tenancy_item->object_name = $properties->name_of_property;
                        // $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                        if (isset($master_array[$properties->masterliste_id])) {

                            // $asset_manager = $master_array[$properties->masterliste_id];

                            $asset_manager = $properties->asset_m_id;

                            $tenancy_item->creator_name = "";
                            if (isset($asset_name_array[$asset_manager])) {
                                $tenancy_item->creator_name = $asset_name_array[$asset_manager];
                            }

                            if (isset($pa_array[$asset_manager])) {
                                $pa_array[$asset_manager] += $tenancy_item->actual_net_rent;
                            } else {
                                $pa_array[$asset_manager] = $tenancy_item->actual_net_rent;
                            }

                            if (isset($pa_array2[$tenancy_item->property_id])) {
                                $pa_array2[$tenancy_item->property_id] += $tenancy_item->actual_net_rent;
                            } else {
                                $pa_array2[$tenancy_item->property_id] = $tenancy_item->actual_net_rent;
                            }

                        } else {
                            unset($tenancy_items_2[$key]);
                        }
                        if ($tenancy_item->type == 2) {
                            $tenancy_item->type = 'Gewerbe';
                        } elseif ($tenancy_item->type == 1) {
                            $tenancy_item->type = 'Wohnen';
                        }

                    } else {
                        unset($tenancy_items_2[$key]);
                    }
                }
            }

            // print_r($analytics_array); die;
            if ($id == -1) {
                $AssetManager = DB::table('users')->select('id', 'name')->whereRaw('role = 4 OR second_role = 4')->get();

                $a = View::make('assetmanagement.properties_dashboard', compact('analytics_array', 'pa_array2', 'pa_array', 'id', 'AssetManager'))->render();
            } else {
                $a = View::make('assetmanagement.properties', compact('analytics_array', 'pa_array2', 'pa_array', 'id'))->render();
            }

            return $a;

        }

        $analytics = Properties::selectRaw('users.name as name,asset_m_id,count(properties.id) as id,group_concat(masterliste_id) as masterliste_id,group_concat(properties.id) as pid')->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
            ->join('users', 'users.id', '=', 'asset_m_id')
            ->where('status', $status)
            ->where('Ist', 0)->where('soll', 0)->groupBy('asset_m_id')->get();


        // pre($analytics);

        $o = Properties::select('id')->whereIn('status', array(6))->where('Ist', 0)->where('soll', 0)->get();

        $wv = $p_count = 0;

        $er = array();

        foreach ($o as $key => $value) {

            if (!isset($TenancySchedulearray[$value->id])) {
                continue;
            }

            $json_array = $TenancySchedulearray[$value->id];

            $p_count = $p_count + 1;
            $temp    = 0;
            if (isset($json_array['wault'])) {
                $temp = $json_array['wault'];
            }

            // else
            // $temp = ($json_array['total_business_actual_net_rent']!=0) ? number_format(($json_array['total_remaining_time_in_eur'])/(12 * $json_array['total_business_actual_net_rent']),1) : 0 ;
            $wv += $temp;

            $er[$value->id] = $temp;
        }

        $avg_wault = 0;
        if ($p_count) {
            $avg_wault = $wv / $p_count;
        }
        // $text = json_encode($er).'p-count-'.$p_count.' and wv='.$wv.' and avg_wault='.$avg_wault;
        // $subject = "assets";
        // $email = "yiicakephp@gmail.com";
        // if($email){
        //     Mail::send('emails.general', ['text' => $text], function ($message) use($email,$subject) {
        //         $message->to($email)
        //         ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
        //         ->subject($subject);
        //     });
        // }

        // print "<pre>";
        // print_r($er);
        // print_r($er);

        // print "<pre>";
        // print_r($analytics_array);
        // die;

        $analytics_array  = array();
        $asset_name_array = array();
        $property_array   = array();
        foreach ($analytics as $key => $value) {

            $analytics_array[$value->asset_m_id]['asset_manager']      = $value->asset_m_id;
            $analytics_array[$value->asset_m_id]['asset_manager_name'] = $value->name;
            $analytics_array[$value->asset_m_id]['count']              = 0;

            $asset_name_array[$value->asset_m_id] = $value->name;

            $analytics_array[$value->asset_m_id]['flat_in_qm']   = 0;
            $analytics_array[$value->asset_m_id]['rented_area']  = 0;
            $analytics_array[$value->asset_m_id]['vacancy']      = 0;
            $analytics_array[$value->asset_m_id]['rent_net_pm']  = 0;
            $analytics_array[$value->asset_m_id]['potential_pm'] = 0;
            $analytics_array[$value->asset_m_id]['object']       = '';
            $analytics_array[$value->asset_m_id]['rents']        = 0;
            $analytics_array[$value->asset_m_id]['vacants']      = 0;

            // $m_list = Masterliste::whereIn('id',explode(',', $value->masterliste_id))->get();

            $prids = explode(',', $value->pid);

            foreach ($prids as $key => $property_id) {

                $property_array[] = $property_id;

                $analytics_array[$value->asset_m_id]['count'] += 1;
                if (!isset($TenancySchedulearray[$property_id])) {
                    continue;
                }

                $json_array = $TenancySchedulearray[$property_id];

                // print_r($json_array); die;

                $analytics_array[$value->asset_m_id]['flat_in_qm'] += $json_array['mi9'] + $json_array['mi10'];
                $analytics_array[$value->asset_m_id]['rented_area'] += $json_array['total_live_rental_space'] + $json_array['total_business_rental_space'];
                $analytics_array[$value->asset_m_id]['vacancy'] += $json_array['total_live_vacancy_in_qm'] + $json_array['total_business_vacancy_in_qm'];
                $analytics_array[$value->asset_m_id]['rent_net_pm'] += $json_array['total_actual_net_rent'];

                $analytics_array[$value->asset_m_id]['potential_pm'] += $json_array['potenzial_eur_jahr'];
                $analytics_array[$value->asset_m_id]['object'] .= "";

                $analytics_array[$value->asset_m_id]['rents'] += $json_array['businesscount'] + $json_array['livecount'];
                $analytics_array[$value->asset_m_id]['vacants'] += $json_array['vbusinesscount'] + $json_array['vlivecount'];
            }
        }

        $tenancy_items = TenancySchedule::leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->where('type', 3)->orWhere('type', '=', 4)->get();

        //$tenancy_items_2 = TenancySchedule::leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->whereIn('type', [1, 2])->where('status', '=', 1)->orderBy('rent_end', 'asc')->get();
        $tenancy_items_2 = TenancySchedule::selectRaw('tenancy_schedule_items.rent_begin,tenancy_schedule_items.rent_end,tenancy_schedule_items.tenancy_schedule_id,tenancy_schedule_items.actual_net_rent,tenancy_schedule_items.rental_space,tenancy_schedules.property_id,tenancy_schedule_items.type,tenancy_schedule_items.id,tenancy_schedule_items.name,tenancy_schedule_items.comment,(SELECT tsc.comment FROM tenancy_schedule_comments tsc WHERE tsc.item_id = tenancy_schedule_items.id ORDER BY tsc.id DESC LIMIT 1) as comment1')
                            ->leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')
                            ->whereIn('type', [1, 2])
                            ->where('status', '=', 1)
                            ->orderBy('rent_end', 'asc')
                            ->get();


        $masterlistes = Masterliste::selectRaw('masterliste.*,properties.id as property_id')->join('properties', 'masterliste.id', '=', 'properties.masterliste_id')
            ->where('Ist', 0)->where('soll', 0)
            ->get();

        // print_r($masterlistes); die;

        $master_array           = array();
        $diffff                 = array();
        $summ_vermietet_percent = $summ_leerstand_percent = 0;
        $sum_rented_area        = $sum_flat_in_qm        = 0;
        $summ_vermietet_m2      = $summ_leerstand_m2      = 0;
        foreach ($masterlistes as $key => $masterliste) {
            $master_array[$masterliste->id] = $masterliste->asset_manager;

            if (!isset($TenancySchedulearray[$masterliste->property_id])) {
                continue;
            }

            $diffff[$masterliste->id]['amount'] = $TenancySchedulearray[$masterliste->property_id]['potenzial_eur_monat'];

            $diffff[$masterliste->id]['property_id'] = $masterliste->property_id;

            $rented_area = isset($masterliste->rented_area) ? $masterliste->rented_area : 0;
            $summ_vermietet_m2 += $rented_area;
            if (isset($masterliste->flat_in_qm)) {
                $sum_rented_area += $masterliste->rented_area;
                $sum_flat_in_qm += $masterliste->flat_in_qm;
            }
            $vacancy = isset($masterliste->vacancy) ? $masterliste->vacancy : 0;
            $summ_leerstand_m2 += $vacancy;

        }

        if ($sum_flat_in_qm != 0) {
            $summ_vermietet_percent = $sum_rented_area / $sum_flat_in_qm * 100;
            $summ_leerstand_percent = $summ_leerstand_m2 / $sum_flat_in_qm * 100;
        }

        $pa_array = $pa_array2 = array();
        $data     = TenancyScheduleItem::selectRaw('rent_begin,rent_end,asset_m_id,actual_net_rent,rental_space')->join('tenancy_schedules', 'tenancy_schedules.id', '=', 'tenancy_schedule_items.tenancy_schedule_id')
            ->join('properties', 'properties.id', '=', 'tenancy_schedules.property_id')
            ->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
            ->join('users', 'users.id', '=', 'asset_m_id')
            ->whereRaw('(type=2 or type=1) and  properties.status=6')
            ->groupBy('tenancy_schedule_items.id')
            ->get()->toArray();

        foreach ($data as $key => $value) {

            if (($value['rent_begin'] != '' && $value['rent_begin'] > date('Y-m-d')) || ($value['rent_end'] != '' && $value['rent_end'] < date('Y-m-d'))) {
                continue;
            }

            if ($value['actual_net_rent'] && $value['rental_space']) {
                if (isset($pa_array[$value['asset_m_id']])) {
                    $pa_array[$value['asset_m_id']] += $value['actual_net_rent'];
                } else {
                    $pa_array[$value['asset_m_id']] = $value['actual_net_rent'];
                }
            }
        }

        foreach ($tenancy_items as $key => $tenancy_item) {
            if ($tenancy_item->tenancy_schedule_id) {
                $properties = Properties::where('Ist', 0)->where('soll', 0)->where('id', $tenancy_item->property_id)->first();
                if ($properties != null && $properties->masterliste_id != null) {
                    $tenancy_item->object_name = $properties->name_of_property;
                    // $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                    if (isset($master_array[$properties->masterliste_id])) {

                        $asset_manager = $master_array[$properties->masterliste_id];

                        $asset_manager = $properties->asset_m_id;

                        $tenancy_item->creator_name = "";
                        if (isset($asset_name_array[$asset_manager])) {
                            $tenancy_item->creator_name = $asset_name_array[$asset_manager];
                        }

                        //if(isset($pa_array[$asset_manager]))
                        //$pa_array[$asset_manager] += $tenancy_item->actual_net_rent;
                        //else
                        //$pa_array[$asset_manager] = $tenancy_item->actual_net_rent;

                        if (isset($pa_array2[$tenancy_item->property_id])) {
                            $pa_array2[$tenancy_item->property_id] += $tenancy_item->actual_net_rent;
                        } else {
                            $pa_array2[$tenancy_item->property_id] = $tenancy_item->actual_net_rent;
                        }

                    } else {
                        unset($tenancy_items[$key]);
                    }
                    if ($tenancy_item->type == 4) {
                        $tenancy_item->type = 'Gewerbe';
                    } elseif ($tenancy_item->type == 3) {
                        $tenancy_item->type = 'Wohnen';
                    }

                } else {
                    unset($tenancy_items[$key]);
                }

            }
        }

        foreach ($tenancy_items_2 as $key => $tenancy_item) {
            if (($tenancy_item->rent_begin != '' && $tenancy_item->rent_begin > date('Y-m-d')) || ($tenancy_item->rent_end != '' && $tenancy_item->rent_end < date('Y-m-d'))) {
                continue;
            }

            if ($tenancy_item->tenancy_schedule_id && $tenancy_item->actual_net_rent && $tenancy_item->rental_space) {
                $properties = Properties::where('Ist', 0)->where('soll', 0)->where('id', $tenancy_item->property_id)->first();
                if ($properties != null && $properties->masterliste_id != null) {
                    $tenancy_item->object_name = $properties->name_of_property;
                    // $masterliste  = Masterliste::where('id', $properties->masterliste_id)->first();
                    if (isset($master_array[$properties->masterliste_id])) {

                        $asset_manager = $properties->asset_m_id;

                        $tenancy_item->creator_name = "";
                        if (isset($asset_name_array[$asset_manager])) {
                            $tenancy_item->creator_name = $asset_name_array[$asset_manager];
                        }

                        //if(isset($pa_array[$asset_manager]))
                        //$pa_array[$asset_manager] += $tenancy_item->actual_net_rent;
                        //else
                        //$pa_array[$asset_manager] = $tenancy_item->actual_net_rent;

                        if (isset($pa_array2[$tenancy_item->property_id])) {
                            $pa_array2[$tenancy_item->property_id] += $tenancy_item->actual_net_rent;
                        } else {
                            $pa_array2[$tenancy_item->property_id] = $tenancy_item->actual_net_rent;
                        }

                    } else {
                        unset($tenancy_items_2[$key]);
                    }
                    if ($tenancy_item->type == 2) {
                        $tenancy_item->type = 'Gewerbe';
                    } elseif ($tenancy_item->type == 1) {
                        $tenancy_item->type = 'Wohnen';
                    }

                } else {
                    unset($tenancy_items_2[$key]);
                }
            }
        }
        $is_asset_manager = isset($input['asset_manager']);

        if (isset($input['ajax']) && $input['ajax']) {
            $a = View::make('assetmanagement.dashboard-properties', compact('user', 'analytics_array', 'is_asset_manager', 'pa_array', 'pa_array2', 'tenancy_items_2', 'tenancy_items', 'masterlistes', 'diffff', 'summ_leerstand_m2', 'summ_vermietet_m2', 'summ_vermietet_percent', 'summ_leerstand_percent', 'avg_wault'))->render();
            return $a;
        }

        return view('assetmanagement.index', compact('user', 'analytics_array', 'is_asset_manager', 'pa_array', 'pa_array2', 'tenancy_items_2', 'tenancy_items', 'masterlistes', 'diffff', 'summ_leerstand_m2', 'summ_vermietet_m2', 'summ_vermietet_percent', 'summ_leerstand_percent', 'avg_wault'));

    }

    public function assetManagementOptimized(Request $request){
        $input  = $request->all();
        $status = 6;
        $user = Auth::user();

        if ($input && isset($input['type'])) {
            if ($input['type'] == "r1" || $input['type'] == "v1") {
                $status1 = array(10, 12);
                $query   = Properties::selectRaw('users.name as name,asset_m_id,properties.id,name_of_property')
                    ->leftjoin('users', 'users.id', '=', 'asset_m_id')
                    ->whereIn('status', $status1)
                    ->where('Ist', 0)->where('soll', 0);
            } else {
                $query = Properties::selectRaw('users.name as name,asset_m_id,properties.id,name_of_property')->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
                    ->join('users', 'users.id', '=', 'asset_m_id')
                    ->where('status', $status)
                    ->where('Ist', 0)->where('soll', 0);
            }

            if ($input['asset_manager']) {
                $query->where('asset_m_id', $input['asset_manager']);
            }

            if ($input['property_id']) {
                $query->where('properties.id', $input['property_id']);
            }

            $data = $query->get();

            $tenant_data = array();
            foreach ($data as $key => $value) {
                if ($input['type'] == "r1" || $input['type'] == "v1") {
                    $tenant_data[$key]['name']   = $value->name_of_property;
                    $tenant_data[$key]['id']     = $value->id;
                    $tenant_data[$key]['tenant'] = TenancySchedulesService::get($value->id);
                } elseif ($value->asset_m_id) {
                    $tenant_data[$key]['name']   = $value->name_of_property;
                    $tenant_data[$key]['id']     = $value->id;
                    $tenant_data[$key]['tenant'] = TenancySchedulesService::get($value->id);
                }
            }

            if ($input['type'] == "r" || $input['type'] == "r1") {
                $a = View::make('assetmanagement.rents', compact('tenant_data', 'input'))->render();
            } else {
                $a = View::make('assetmanagement.vacants', compact('tenant_data', 'input'))->render();
            }

            return $a;
        }

        $TenancySchedule = TenancySchedule::select('text_json', 'property_id')->groupBy('property_id')->get();

        $TenancySchedulearray = array();
        foreach ($TenancySchedule as $key => $value) {
            $TenancySchedulearray[$value->property_id] = json_decode($value->text_json, true);
        }

        if ($input && isset($input['id'])) {
            $id = $input['id'];
            
            $analytics_array = array();
            if ($id == -1) {
                $status1   = array(10, 12);
                $analytics = Properties::selectRaw('properties.transaction_m_id,users.name as name,properties.asset_m_id,properties.id,properties.name_of_property,properties.purchase_date, p.purchase_date as purchase_date2')
                    ->leftjoin('users', 'users.id', '=', 'asset_m_id')
                    ->leftjoin('properties as p', 'p.main_property_id', 'properties.id')
                    ->whereIn('properties.status', $status1)
                    ->where('properties.Ist', 0)->where('properties.soll', 0)->groupBy('properties.id')->get();
            } else {
                $analytics = Properties::selectRaw('users.name as name, properties.asset_m_id,properties.id, properties.name_of_property, p.purchase_date as purchase_date2')->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
                    ->join('users', 'users.id', '=', 'asset_m_id')
                    ->leftjoin('properties as p', 'p.main_property_id', 'properties.id')
                    ->where('properties.status', $status)
                    ->where('properties.Ist', 0)->where('properties.soll', 0)->where('properties.asset_m_id', $id)->groupBy('properties.id')->get();
            }

            $key = 0;
            foreach ($analytics as $value) {
                $property_id = $value->id;
                $analytics_array[$key]['purchase_date']      = "";

                // $purchase_date_prs = Properties::select('purchase_date')->whereRaw('main_property_id='.$property_id)->first();

                if($value->purchase_date2)
                    $analytics_array[$key]['purchase_date']      = $value->purchase_date2;

                $analytics_array[$key]['asset_manager']      = $value->asset_m_id;
                $analytics_array[$key]['asset_manager_name'] = $value->name;
                $analytics_array[$key]['transaction_m_id'] = $value->transaction_m_id;
                $analytics_array[$key]['id'] = $value->id;
                $asset_name_array[$key]      = $value->name;
                $analytics_array[$key]['flat_in_qm']  = 0;
                $analytics_array[$key]['rented_area'] = 0;
                $analytics_array[$key]['vacancy']     = 0;
                $analytics_array[$key]['rent_net_pm'] = 0;
                $analytics_array[$key]['potential_pm'] = 0;
                $analytics_array[$key]['object']       = "";
                $analytics_array[$key]['rents']   = 0;
                $analytics_array[$key]['vacants'] = 0;
                $analytics_array[$key]['object'] = $value->name_of_property;

                if (!isset($TenancySchedulearray[$property_id])) {
                    $key++;
                    continue;
                }

                $json_array = $TenancySchedulearray[$property_id];

                $analytics_array[$key]['flat_in_qm']  = $json_array['mi9'] + $json_array['mi10'];
                $analytics_array[$key]['rented_area'] = $json_array['total_live_rental_space'] + $json_array['total_business_rental_space'];
                $analytics_array[$key]['vacancy']     = $json_array['total_live_vacancy_in_qm'] + $json_array['total_business_vacancy_in_qm'];
                $analytics_array[$key]['rent_net_pm'] = $json_array['total_actual_net_rent'];
                $analytics_array[$key]['potential_pm'] = $json_array['potenzial_eur_jahr'];
                $analytics_array[$key]['rents']   = $json_array['businesscount'] + $json_array['livecount'];
                $analytics_array[$key]['vacants'] = $json_array['vbusinesscount'] + $json_array['vlivecount'];

                $key++;
            }
            $tenancy_items = TenancySchedule::leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->where('type', 3)->orWhere('type', '=', 4)->get();

            $tenancy_items_2 = TenancySchedule::leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->whereIn('type', [1, 2])->where('status', '=', 1)->orderBy('rent_end', 'asc')->get();

            $masterlistes = Masterliste::selectRaw('masterliste.*,properties.id as property_id')->join('properties', 'masterliste.id', '=', 'properties.masterliste_id')
                ->where('Ist', 0)->where('soll', 0)
                ->get();

            $master_array           = array();
            $diffff                 = array();
            $summ_vermietet_percent = $summ_leerstand_percent = 0;
            $sum_rented_area        = $sum_flat_in_qm        = 0;
            $summ_vermietet_m2      = $summ_leerstand_m2      = 0;

            foreach ($masterlistes as $key => $masterliste) {
                $master_array[$masterliste->id] = $masterliste->asset_manager;

                if (!isset($TenancySchedulearray[$masterliste->property_id])) {
                    continue;
                }

                $diffff[$masterliste->id]['amount'] = $TenancySchedulearray[$masterliste->property_id]['potenzial_eur_monat'];
                $diffff[$masterliste->id]['property_id'] = $masterliste->property_id;
                $rented_area = isset($masterliste->rented_area) ? $masterliste->rented_area : 0;
                $summ_vermietet_m2 += $rented_area;

                if (isset($masterliste->flat_in_qm)) {
                    $sum_rented_area += $masterliste->rented_area;
                    $sum_flat_in_qm += $masterliste->flat_in_qm;
                }

                $vacancy = isset($masterliste->vacancy) ? $masterliste->vacancy : 0;
                $summ_leerstand_m2 += $vacancy;
            }

            if ($sum_flat_in_qm != 0) {
                $summ_vermietet_percent = $sum_rented_area / $sum_flat_in_qm * 100;
                $summ_leerstand_percent = $summ_leerstand_m2 / $sum_flat_in_qm * 100;
            }

            $pa_array = $pa_array2 = array();
            foreach ($tenancy_items as $key => $tenancy_item) {

                if ($tenancy_item->tenancy_schedule_id) {

                    $properties = Properties::where('Ist', 0)->where('soll', 0)->where('id', $tenancy_item->property_id)->first();

                    if ($properties != null && $properties->masterliste_id != null) {

                        $tenancy_item->object_name = $properties->name_of_property;

                        if (isset($master_array[$properties->masterliste_id])) {

                            $asset_manager = $properties->asset_m_id;
                            $tenancy_item->creator_name = "";

                            if (isset($asset_name_array[$asset_manager])) {
                                $tenancy_item->creator_name = $asset_name_array[$asset_manager];
                            }

                            if (isset($pa_array[$asset_manager])) {
                                $pa_array[$asset_manager] += $tenancy_item->actual_net_rent;
                            } else {
                                $pa_array[$asset_manager] = $tenancy_item->actual_net_rent;
                            }

                            if (isset($pa_array2[$tenancy_item->property_id])) {
                                $pa_array2[$tenancy_item->property_id] += $tenancy_item->actual_net_rent;
                            } else {
                                $pa_array2[$tenancy_item->property_id] = $tenancy_item->actual_net_rent;
                            }

                        } else {
                            unset($tenancy_items[$key]);
                        }

                        if ($tenancy_item->type == 4) {
                            $tenancy_item->type = 'Gewerbe';
                        } elseif ($tenancy_item->type == 3) {
                            $tenancy_item->type = 'Wohnen';
                        }

                    } else {
                        unset($tenancy_items[$key]);
                    }

                }
            }

            foreach ($tenancy_items_2 as $key => $tenancy_item) {

                if (($tenancy_item->rent_begin != '' && $tenancy_item->rent_begin > date('Y-m-d')) || ($tenancy_item->rent_end != '' && $tenancy_item->rent_end < date('Y-m-d'))) {
                    continue;
                }

                if ($tenancy_item->tenancy_schedule_id && $tenancy_item->actual_net_rent && $tenancy_item->rental_space) {

                    $properties = Properties::where('Ist', 0)->where('soll', 0)->where('id', $tenancy_item->property_id)->first();

                    if ($properties != null && $properties->masterliste_id != null) {

                        $tenancy_item->object_name = $properties->name_of_property;
                        
                        if (isset($master_array[$properties->masterliste_id])) {

                            $asset_manager = $properties->asset_m_id;
                            $tenancy_item->creator_name = "";

                            if (isset($asset_name_array[$asset_manager])) {
                                $tenancy_item->creator_name = $asset_name_array[$asset_manager];
                            }

                            if (isset($pa_array[$asset_manager])) {
                                $pa_array[$asset_manager] += $tenancy_item->actual_net_rent;
                            } else {
                                $pa_array[$asset_manager] = $tenancy_item->actual_net_rent;
                            }

                            if (isset($pa_array2[$tenancy_item->property_id])) {
                                $pa_array2[$tenancy_item->property_id] += $tenancy_item->actual_net_rent;
                            } else {
                                $pa_array2[$tenancy_item->property_id] = $tenancy_item->actual_net_rent;
                            }

                        } else {
                            unset($tenancy_items_2[$key]);
                        }

                        if ($tenancy_item->type == 2) {
                            $tenancy_item->type = 'Gewerbe';
                        } elseif ($tenancy_item->type == 1) {
                            $tenancy_item->type = 'Wohnen';
                        }

                    } else {
                        unset($tenancy_items_2[$key]);
                    }
                }
            }
            
            if ($id == -1) {
                $AssetManager = DB::table('users')->select('id', 'name')->whereRaw('role = 4 OR second_role = 4')->get();
                $a = View::make('assetmanagement.properties_dashboard', compact('analytics_array', 'pa_array2', 'pa_array', 'id', 'AssetManager'))->render();
            } else {
                $a = View::make('assetmanagement.properties', compact('analytics_array', 'pa_array2', 'pa_array', 'id'))->render();
            }

            return $a;
        }

        $analytics = Properties::selectRaw('users.name as name,asset_m_id,count(properties.id) as id,group_concat(masterliste_id) as masterliste_id,group_concat(properties.id) as pid')->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
            ->join('users', 'users.id', '=', 'asset_m_id')
            ->where('status', $status)
            ->where('Ist', 0)->where('soll', 0)->groupBy('asset_m_id')->get();

        $o = Properties::select('id')->whereIn('status', array(6))->where('Ist', 0)->where('soll', 0)->get();

        $wv = $p_count = 0;
        $er = array();

        if($o){
            foreach ($o as $key => $value) {

                if (!isset($TenancySchedulearray[$value->id])) {
                    continue;
                }

                $json_array = $TenancySchedulearray[$value->id];

                $p_count = $p_count + 1;
                $temp    = 0;
                if (isset($json_array['wault'])) {
                    $temp = $json_array['wault'];
                }
                $wv += $temp;
                $er[$value->id] = $temp;
            }
        }

        $avg_wault = 0;
        if ($p_count) {
            $avg_wault = $wv / $p_count;
        }

        $analytics_array  = array();
        $asset_name_array = array();
        $property_array   = array();

        foreach ($analytics as $key => $value) {
            $analytics_array[$value->asset_m_id]['asset_manager']      = $value->asset_m_id;
            $analytics_array[$value->asset_m_id]['asset_manager_name'] = $value->name;
            $analytics_array[$value->asset_m_id]['count']              = 0;

            $asset_name_array[$value->asset_m_id] = $value->name;

            $analytics_array[$value->asset_m_id]['flat_in_qm']   = 0;
            $analytics_array[$value->asset_m_id]['rented_area']  = 0;
            $analytics_array[$value->asset_m_id]['vacancy']      = 0;
            $analytics_array[$value->asset_m_id]['rent_net_pm']  = 0;
            $analytics_array[$value->asset_m_id]['potential_pm'] = 0;
            $analytics_array[$value->asset_m_id]['object']       = '';
            $analytics_array[$value->asset_m_id]['rents']        = 0;
            $analytics_array[$value->asset_m_id]['vacants']      = 0;

            $prids = explode(',', $value->pid);

            foreach ($prids as $key => $property_id) {
                $property_array[] = $property_id;

                $analytics_array[$value->asset_m_id]['count'] += 1;
                if (!isset($TenancySchedulearray[$property_id])) {
                    continue;
                }

                $json_array = $TenancySchedulearray[$property_id];

                $analytics_array[$value->asset_m_id]['flat_in_qm'] += $json_array['mi9'] + $json_array['mi10'];
                $analytics_array[$value->asset_m_id]['rented_area'] += $json_array['total_live_rental_space'] + $json_array['total_business_rental_space'];
                $analytics_array[$value->asset_m_id]['vacancy'] += $json_array['total_live_vacancy_in_qm'] + $json_array['total_business_vacancy_in_qm'];
                $analytics_array[$value->asset_m_id]['rent_net_pm'] += $json_array['total_actual_net_rent'];

                $analytics_array[$value->asset_m_id]['potential_pm'] += $json_array['potenzial_eur_jahr'];
                $analytics_array[$value->asset_m_id]['object'] .= "";

                $analytics_array[$value->asset_m_id]['rents'] += $json_array['businesscount'] + $json_array['livecount'];
                $analytics_array[$value->asset_m_id]['vacants'] += $json_array['vbusinesscount'] + $json_array['vlivecount'];
            }
        }

        $masterlistes = Masterliste::selectRaw('masterliste.*,properties.id as property_id')->join('properties', 'masterliste.id', '=', 'properties.masterliste_id')
            ->where('Ist', 0)->where('soll', 0)
            ->get();

        $master_array           = array();
        $diffff                 = array();
        $summ_vermietet_percent = $summ_leerstand_percent = 0;
        $sum_rented_area        = $sum_flat_in_qm        = 0;
        $summ_vermietet_m2      = $summ_leerstand_m2      = 0;

        foreach ($masterlistes as $key => $masterliste) {
            $master_array[$masterliste->id] = $masterliste->asset_manager;

            if (!isset($TenancySchedulearray[$masterliste->property_id])) {
                continue;
            }

            $diffff[$masterliste->id]['amount'] = $TenancySchedulearray[$masterliste->property_id]['potenzial_eur_monat'];
            $diffff[$masterliste->id]['property_id'] = $masterliste->property_id;
            $rented_area = isset($masterliste->rented_area) ? $masterliste->rented_area : 0;

            $summ_vermietet_m2 += $rented_area;
            if (isset($masterliste->flat_in_qm)) {
                $sum_rented_area += $masterliste->rented_area;
                $sum_flat_in_qm += $masterliste->flat_in_qm;
            }

            $vacancy = isset($masterliste->vacancy) ? $masterliste->vacancy : 0;
            $summ_leerstand_m2 += $vacancy;
        }

        if ($sum_flat_in_qm != 0) {
            $summ_vermietet_percent = $sum_rented_area / $sum_flat_in_qm * 100;
            $summ_leerstand_percent = $summ_leerstand_m2 / $sum_flat_in_qm * 100;
        }

        $pa_array = $pa_array2 = array();

        $data = TenancyScheduleItem::selectRaw('rent_begin,rent_end,asset_m_id,actual_net_rent,rental_space')->join('tenancy_schedules', 'tenancy_schedules.id', '=', 'tenancy_schedule_items.tenancy_schedule_id')
            ->join('properties', 'properties.id', '=', 'tenancy_schedules.property_id')
            ->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
            ->join('users', 'users.id', '=', 'asset_m_id')
            ->whereRaw('(type=2 or type=1) and  properties.status=6')
            ->groupBy('tenancy_schedule_items.id')
            ->get()->toArray();

        foreach ($data as $key => $value) {

            if (($value['rent_begin'] != '' && $value['rent_begin'] > date('Y-m-d')) || ($value['rent_end'] != '' && $value['rent_end'] < date('Y-m-d'))) {
                continue;
            }

            if ($value['actual_net_rent'] && $value['rental_space']) {
                if (isset($pa_array[$value['asset_m_id']])) {
                    $pa_array[$value['asset_m_id']] += $value['actual_net_rent'];
                } else {
                    $pa_array[$value['asset_m_id']] = $value['actual_net_rent'];
                }
            }
        }
        

        $tenancy_items = TenancyScheduleItem::selectRaw('tenancy_schedule_items.id, properties.name_of_property as object_name, users.name as creator_name, (CASE WHEN tenancy_schedule_items.type = 3 THEN "Wohnen" WHEN tenancy_schedule_items.type = 4 THEN "Gewerbe" ELSE tenancy_schedule_items.type END) as type, tenancy_schedules.property_id, IFNULL(tenancy_schedule_items.actual_net_rent, 0) as actual_net_rent, tenancy_schedule_items.name, tenancy_schedule_items.vacancy_in_qm, tenancy_schedule_items.vacancy_in_eur, tenancy_schedule_items.comment')
                            ->join('tenancy_schedules', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')
                            ->join('properties', function($join){
                                    $join->on('properties.id', '=', 'tenancy_schedules.property_id')
                                    ->where('properties.Ist', 0)
                                    ->where('properties.soll', 0);
                            })
                            ->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
                            ->leftjoin('users', 'users.id', '=', 'properties.asset_m_id')
                            ->whereIn('tenancy_schedule_items.type', [3, 4])
                            ->whereNotNull('masterliste.asset_manager')
                            ->orderBy('tenancy_schedule_items.id')
                            ->get();

        if($tenancy_items){
            foreach ($tenancy_items as $key => $tenancy_item) {
                if (isset($pa_array2[$tenancy_item->property_id])) {
                    $pa_array2[$tenancy_item->property_id] += $tenancy_item->actual_net_rent;
                } else {
                    $pa_array2[$tenancy_item->property_id] = $tenancy_item->actual_net_rent;
                }
            }
        }

        $tenancy_items_2 = TenancySchedule::selectRaw('tenancy_schedule_items.rent_begin,tenancy_schedule_items.rent_end,tenancy_schedule_items.tenancy_schedule_id,tenancy_schedule_items.actual_net_rent,tenancy_schedule_items.rental_space,tenancy_schedules.property_id,(CASE WHEN tenancy_schedule_items.type = 1 THEN "Wohnen" WHEN tenancy_schedule_items.type = 2 THEN "Gewerbe" ELSE tenancy_schedule_items.type END) as type,tenancy_schedule_items.id,tenancy_schedule_items.name,tenancy_schedule_items.comment,(SELECT tsc.comment FROM tenancy_schedule_comments tsc WHERE tsc.item_id = tenancy_schedule_items.id ORDER BY tsc.id DESC LIMIT 1) as comment1, properties.masterliste_id, properties.asset_m_id, properties.name_of_property as object_name')
                            ->leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')
                            ->leftjoin('properties', function($join){
                                    $join->on('properties.id', '=', 'tenancy_schedules.property_id')
                                    ->where('properties.Ist', 0)
                                    ->where('properties.soll', 0)
                                    ->whereNotNull('properties.masterliste_id');
                            })
                            ->whereIn('tenancy_schedule_items.type', [1, 2])
                            ->where('tenancy_schedule_items.status', '=', 1)
                            ->orderBy('tenancy_schedule_items.rent_end', 'asc')
                            ->get();
        if($tenancy_items_2){
            foreach ($tenancy_items_2 as $key => $tenancy_item) {

                if (($tenancy_item->rent_begin != '' && $tenancy_item->rent_begin > date('Y-m-d')) || ($tenancy_item->rent_end != '' && $tenancy_item->rent_end < date('Y-m-d'))) {
                    continue;
                }

                if ($tenancy_item->tenancy_schedule_id && $tenancy_item->actual_net_rent && $tenancy_item->rental_space) {

                    if (isset($master_array[$tenancy_item->masterliste_id])) {

                        $asset_manager = $tenancy_item->asset_m_id;
                        $tenancy_item->creator_name = "";

                        if (isset($asset_name_array[$asset_manager])) {
                            $tenancy_item->creator_name = $asset_name_array[$asset_manager];
                        }

                        if (isset($pa_array2[$tenancy_item->property_id])) {
                            $pa_array2[$tenancy_item->property_id] += $tenancy_item->actual_net_rent;
                        } else {
                            $pa_array2[$tenancy_item->property_id] = $tenancy_item->actual_net_rent;
                        }

                    } else {
                        unset($tenancy_items_2[$key]);
                    }
                }
            }
        }

        $is_asset_manager = isset($input['asset_manager']);

        if (isset($input['ajax']) && $input['ajax']) {
            $a = View::make('assetmanagement.dashboard-properties', compact('user', 'analytics_array', 'is_asset_manager', 'pa_array', 'pa_array2', 'tenancy_items_2', 'tenancy_items', 'masterlistes', 'diffff', 'summ_leerstand_m2', 'summ_vermietet_m2', 'summ_vermietet_percent', 'summ_leerstand_percent', 'avg_wault'))->render();
            return $a;
        }

        return view('assetmanagement.asset-management-optimized', compact('user', 'analytics_array', 'is_asset_manager', 'pa_array', 'pa_array2', 'tenancy_items_2', 'tenancy_items', 'masterlistes', 'diffff', 'summ_leerstand_m2', 'summ_vermietet_m2', 'summ_vermietet_percent', 'summ_leerstand_percent', 'avg_wault'));

    }

    public function index1(Request $request)
    {
        $input           = $request->all();
        $status          = config('properties.status.buy');
        $user            = sizeof(User::all());
        $Revenues        = Properties::where('Ist', 0)->where('soll', 0)->where('status', $status)->get();
        $sum_Revenue     = 0;
        $properties      = Properties::where('Ist', 0)->where('soll', 0)->get();
        $masterlistes    = Masterliste::all();
        $tenancy_items   = TenancyScheduleItem::where('type', 3)->orWhere('type', '=', 4)->get();
        $tenancy_items_2 = TenancyScheduleItem::whereIn('type', [1, 2])->where('status', '=', 1)->orderBy('rent_end', 'asc')->get();

        if (isset($input['asset_manager'])) {
            $analytics = Properties::selectRaw('asset_m_id,count(properties.id) as id,group_concat(masterliste_id) as masterliste_id')->
                join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')->where('asset_m_id', $input['asset_manager'])
                ->where('Ist', 0)->where('soll', 0)->groupBy('asset_m_id')->get();
        } else {
            $analytics = Properties::selectRaw('asset_m_id,count(properties.id) as id,group_concat(masterliste_id) as masterliste_id')->
                join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
                ->where('Ist', 0)->where('soll', 0)->groupBy('asset_m_id')->get();
        }

        // print "<pre>";
        // print($analytics); die;

        $analytics_array = array();

        $diffff = array();
        foreach ($masterlistes as $key => $masterliste) {
            $m_property               = $masterliste->property;
            $masterliste->property_id = ($m_property)
            ? $m_property->id : null;

            if (!$masterliste->property_id) {
                continue;
            }

            $tenancy_schedule_data         = TenancySchedulesService::get($masterliste->property_id);
            $masterliste->tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : new TenancySchedule();

            $diffff[$masterliste->id]['amount'] = $masterliste->tenancy_schedule->calculations['potenzial_eur_monat'];

            $diffff[$masterliste->id]['property_id'] = $masterliste->property_id;

        }
        // print_r($diffff); die;

        foreach ($analytics as $key => $value) {
            $user = User::find($value->asset_m_id);

            if (!$user) {
                continue;
            }

            if (!isset($input['asset_manager'])):
                $analytics_array[$value->asset_m_id]['flat_in_qm']   = 0;
                $analytics_array[$value->asset_m_id]['rented_area']  = 0;
                $analytics_array[$value->asset_m_id]['vacancy']      = 0;
                $analytics_array[$value->asset_m_id]['rent_net_pm']  = 0;
                $analytics_array[$value->asset_m_id]['potential_pm'] = 0;
                $analytics_array[$value->asset_m_id]['object']       = '';
            endif;

            $m_list = Masterliste::whereIn('id', explode(',', $value->masterliste_id))->get();
            $count  = 0;
            foreach ($m_list as $index => $masterliste_2) {

                // $masterliste->asset_manager = $value->asset_m_id;
                if (isset($input['asset_manager'])):
                    $masterliste = $masterliste_2;

                    $masterliste->asset_manager_name = $user->name;
                    $m_property                      = $masterliste->property;
                    $masterliste->property_id        = ($m_property)
                    ? $m_property->id : null;
                    $tenancy_schedule_data                         = TenancySchedulesService::get($masterliste->property_id);
                    $masterliste->tenancy_schedule                 = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : new TenancySchedule();
                    $analytics_array[$index]['count']              = 1;
                    $analytics_array[$index]['asset_manager_name'] = $masterliste['asset_manager_name'];
                    // working
                    $analytics_array[$index]['flat_in_qm']    = $masterliste['flat_in_qm'];
                    $analytics_array[$index]['rented_area']   = $masterliste['rented_area'];
                    $analytics_array[$index]['vacancy']       = $masterliste['vacancy'];
                    $analytics_array[$index]['rent_net_pm']   = $masterliste['rent_net_pm'];
                    $analytics_array[$index]['potential_pm']  = $masterliste->tenancy_schedule->calculations['potenzial_eur_jahr'];
                    $analytics_array[$index]['asset_manager'] = $value->asset_m_id;

                    $ist = Properties::where('Ist', '!=', 0)->where('soll', 0)->where('main_property_id', $masterliste->property_id)->first();
                    if ($ist) {
                        $analytics_array[$index]['object'] = $ist['name_of_property'];
                    } else {
                        $analytics_array[$index]['object'] = $masterliste['object'];
                    }

                    $query = str_replace('.', '', $masterliste['object']);

                    $properties_exe_2 = $properties = Properties::where('Ist', 0)->where('soll', 0)->where('id', $masterliste->property_id)->first();

                    if (isset($properties->id)) {
                        $analytics_array[$index]['id'] = $properties->id;
                    }
                    if (isset($properties_exe_2->id)) {
                        $analytics_array[$index]['id'] = $properties_exe_2->id;
                    } else {
                        $analytics_array[$index]['id'] = 0;
                    } else :
                    $m_property                 = $masterliste_2->property;
                    $masterliste_2->property_id = ($m_property)
                    ? $m_property->id : null;
                    $tenancy_schedule_data           = TenancySchedulesService::get($masterliste_2->property_id);
                    $masterliste_2->tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : new TenancySchedule();

                    $count++;
                    $analytics_array[$value->asset_m_id]['asset_manager'] = $value->asset_m_id;
                    $analytics_array[$value->asset_m_id]['id']            = $masterliste_2->id;

                    $analytics_array[$value->asset_m_id]['count']              = $count;
                    $analytics_array[$value->asset_m_id]['asset_manager_name'] = $user->name;

                    $analytics_array[$value->asset_m_id]['flat_in_qm'] += $masterliste_2->tenancy_schedule->calculations['mi9'] + $masterliste_2->tenancy_schedule->calculations['mi10'];
                    $analytics_array[$value->asset_m_id]['rented_area'] += $masterliste_2->tenancy_schedule->calculations['total_live_rental_space'] + $masterliste_2->tenancy_schedule->calculations['total_business_rental_space'];
                    $analytics_array[$value->asset_m_id]['vacancy'] += $masterliste_2->tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $masterliste_2->tenancy_schedule->calculations['total_business_vacancy_in_qm'];
                    $analytics_array[$value->asset_m_id]['rent_net_pm'] += $masterliste_2->tenancy_schedule->calculations['total_actual_net_rent'];

                    $analytics_array[$value->asset_m_id]['potential_pm'] += $masterliste_2->tenancy_schedule->calculations['potenzial_eur_jahr'];

                    $analytics_array[$value->asset_m_id]['object'] .= (isset($masterliste_2->object) ? $masterliste_2->object . '<br/> ' : ' ');
                endif;
            }
        }

        // print "<pre>";
        // print_r($analytics_array); die;

        $masterlistes_new   = array();
        $masterlistes_new_1 = array();
        $masterlistes_new_2 = array();

        $summ_vermietet_m2               = 0;
        $summ_vermietet_percent          = 0;
        $summ_leerstand_m2               = 0;
        $summ_leerstand_percent          = 0;
        $summ_differenz_miete_in_euro    = 0;
        $summ_differenz_miete_pa_in_euro = 0;

        $graph_data                                     = [];
        $graph_data['sum_vermietet_m2_this_month']      = 0;
        $graph_data['sum_vermietet_m2_last_month']      = 0;
        $graph_data['sum_vermietet_m2_next_6_month']    = 0;
        $graph_data['sum_leerstand_m2_this_month']      = 0;
        $graph_data['sum_leerstand_m2_last_month']      = 0;
        $graph_data['sum_leerstand_m2_next_6_month']    = 0;
        $graph_data['sum_differenz_miete_this_month']   = 0;
        $graph_data['sum_differenz_miete_last_month']   = 0;
        $graph_data['sum_differenz_miete_next_6_month'] = 0;

        $thisMonthStart = mktime(0, 0, 0, date("n"), 1);
        $thisMonthEnd   = mktime(23, 59, 59, date("n"), date("t"));

        $nextMonthStart = mktime(0, 0, 0, date("n", strtotime("+1 month")), 1);
        $next6MonthEnd  = mktime(23, 59, 59, date("n", strtotime("+6 month")), date("t", strtotime("+6 month")));

        $lastMonthStart = mktime(0, 0, 0, date("n", strtotime("-1 month")), 1);
        $lastMonthEnd   = mktime(23, 59, 59, date("n", strtotime("-1 month")), date("t", strtotime("-1 month")));

        $sum_rented_area = 0;
        $sum_flat_in_qm  = 0;

        foreach ($masterlistes as $masterliste) {

            //Get graph data
            $prt = Properties::where('Ist', 0)->where('soll', 0)->where('masterliste_id', $masterliste->id)->first();
            if ($prt) {

                $pre_total_live_rental_space1        = 0;
                $pre_total_live_rental_space2        = 0;
                $pre_total_live_rental_space3        = 0;
                $pre_total_live_actual_net_rent1     = 0;
                $pre_total_live_actual_net_rent2     = 0;
                $pre_total_live_actual_net_rent3     = 0;
                $pre_total_business_rental_space1    = 0;
                $pre_total_business_rental_space2    = 0;
                $pre_total_business_rental_space3    = 0;
                $pre_total_business_actual_net_rent1 = 0;
                $pre_total_business_actual_net_rent2 = 0;
                $pre_total_business_actual_net_rent3 = 0;

                $p_id = $prt->id;
                $t_id = TenancySchedule::where('property_id', $p_id)->first();
                if (isset($t_id->id)) {

                    $_t_id = $t_id->id;
                }
                $t_items = TenancyScheduleItem::where('tenancy_schedule_id', $_t_id)->get();

                foreach ($t_items as $t_item) {

                    $t_item_date      = date_parse_from_format("Y-m-d", $t_item->rent_end);
                    $t_item_timestamp = mktime(
                        $t_item_date['hour'],
                        $t_item_date['minute'],
                        $t_item_date['second'],
                        $t_item_date['month'],
                        $t_item_date['day'],
                        $t_item_date['year']
                    );
                    if ($t_item_timestamp >= $lastMonthStart && $t_item_timestamp <= $lastMonthEnd) {
                        switch ($t_item->type) {
                            case config('tenancy_schedule.item_type.live'):
                                $pre_total_live_rental_space1 += $t_item->rental_space;
                                $pre_total_live_actual_net_rent1 += $t_item->actual_net_rent;
                                break;
                            case config('tenancy_schedule.item_type.business'):
                                $pre_total_business_rental_space1 += $t_item->rental_space;
                                $pre_total_business_actual_net_rent1 += $t_item->actual_net_rent;
                                break;
                        }

                    }
                    if ($t_item_timestamp >= $thisMonthStart && $t_item_timestamp <= $thisMonthEnd) {
                        switch ($t_item->type) {
                            case config('tenancy_schedule.item_type.live'):
                                $pre_total_live_rental_space2 += $t_item->rental_space;
                                $pre_total_live_actual_net_rent2 += $t_item->actual_net_rent;
                                break;
                            case config('tenancy_schedule.item_type.business'):
                                $pre_total_business_rental_space2 += $t_item->rental_space;
                                $pre_total_business_actual_net_rent2 += $t_item->actual_net_rent;
                                break;
                        }
                    }
                    if ($t_item_timestamp >= $nextMonthStart && $t_item_timestamp <= $next6MonthEnd) {
                        switch ($t_item->type) {
                            case config('tenancy_schedule.item_type.live'):
                                $pre_total_live_rental_space3 += $t_item->rental_space;
                                $pre_total_live_actual_net_rent3 += $t_item->actual_net_rent;
                                break;
                            case config('tenancy_schedule.item_type.business'):
                                $pre_total_business_rental_space3 += $t_item->rental_space;
                                $pre_total_business_actual_net_rent3 += $t_item->actual_net_rent;
                                break;
                        }
                    }

                }

                $total_live_vacancy_in_eur1 = 0;
                $total_live_vacancy_in_eur2 = 0;
                $total_live_vacancy_in_eur3 = 0;

                $total_business_vacancy_in_eur1 = 0;
                $total_business_vacancy_in_eur2 = 0;
                $total_business_vacancy_in_eur3 = 0;

                $total_live_vacancy_in_qm = 0;

                foreach ($t_items as $t_item) {
                    switch ($t_item->type) {
                        case config('tenancy_schedule.item_type.live_vacancy'):
                            if ($pre_total_live_rental_space1 != 0) {
                                $cust_condition1 = $pre_total_live_actual_net_rent1 / $pre_total_live_rental_space1;
                            } else {
                                $cust_condition1 = 0;
                            }
                            $total_live_vacancy_in_eur1 += $t_item->vacancy_in_qm * $cust_condition1 * 0.8;

                            if ($pre_total_live_rental_space2 != 0) {
                                $cust_condition2 = $pre_total_live_actual_net_rent2 / $pre_total_live_rental_space2;
                            } else {
                                $cust_condition2 = 0;
                            }
                            $total_live_vacancy_in_eur2 += $t_item->vacancy_in_qm * $cust_condition2 * 0.8;

                            if ($pre_total_live_rental_space3 != 0) {
                                $cust_condition3 = $pre_total_live_actual_net_rent3 / $pre_total_live_rental_space3;
                            } else {
                                $cust_condition3 = 0;
                            }
                            $total_live_vacancy_in_eur3 += $t_item->vacancy_in_qm * $cust_condition3 * 0.8;

                            $total_live_vacancy_in_qm += $t_item->vacancy_in_qm;

                            break;
                        case config('tenancy_schedule.item_type.business_vacancy'):
                            if ($pre_total_business_rental_space1 != 0) {
                                $cust_condition1 = $pre_total_business_actual_net_rent1 / $pre_total_business_rental_space1;
                            } else {
                                $cust_condition1 = 0;
                            }
                            $total_business_vacancy_in_eur1 += $t_item->vacancy_in_qm * $cust_condition1 * 0.8;

                            if ($pre_total_business_rental_space2 != 0) {
                                $cust_condition2 = $pre_total_business_actual_net_rent2 / $pre_total_business_rental_space2;
                            } else {
                                $cust_condition2 = 0;
                            }
                            $total_business_vacancy_in_eur2 += $t_item->vacancy_in_qm * $cust_condition2 * 0.8;

                            if ($pre_total_business_rental_space3 != 0) {
                                $cust_condition3 = $pre_total_business_actual_net_rent3 / $pre_total_business_rental_space3;
                            } else {
                                $cust_condition3 = 0;
                            }
                            $total_business_vacancy_in_eur3 += $t_item->vacancy_in_qm * $cust_condition3 * 0.8;
                            break;
                    }
                }

                $graph_data['sum_vermietet_m2_last_month'] += $pre_total_live_rental_space1 + $pre_total_business_rental_space1;
                $graph_data['sum_vermietet_m2_this_month'] += $pre_total_live_rental_space2 + $pre_total_business_rental_space2;
                $graph_data['sum_vermietet_m2_next_6_month'] += $pre_total_live_rental_space3 + $pre_total_business_rental_space3;

                $graph_data['sum_leerstand_m2_last_month'] += $total_live_vacancy_in_qm;
                $graph_data['sum_leerstand_m2_this_month'] += $total_live_vacancy_in_qm;
                $graph_data['sum_leerstand_m2_next_6_month'] += $total_live_vacancy_in_qm;

                $graph_data['sum_differenz_miete_last_month'] += ($total_live_vacancy_in_eur1 + $total_business_vacancy_in_eur1) * 12;
                $graph_data['sum_differenz_miete_this_month'] += ($total_live_vacancy_in_eur2 + $total_business_vacancy_in_eur2) * 12;
                $graph_data['sum_differenz_miete_next_6_month'] += ($total_live_vacancy_in_eur3 + $total_business_vacancy_in_eur3) * 12;

            }

            foreach ($graph_data as $key => $value) {
                $graph_data[$key] = round($value, 2);
            }

            $count = 0;
            if ($masterliste->asset_manager) {
                $masterliste->asset_manager_name = $masterliste->user->name;
            }

            $masterlistes_new[$masterliste->asset_manager]['flat_in_qm']   = 0;
            $masterlistes_new[$masterliste->asset_manager]['rented_area']  = 0;
            $masterlistes_new[$masterliste->asset_manager]['vacancy']      = 0;
            $masterlistes_new[$masterliste->asset_manager]['rent_net_pm']  = 0;
            $masterlistes_new[$masterliste->asset_manager]['potential_pm'] = 0;
            $masterlistes_new[$masterliste->asset_manager]['object']       = '';

            foreach ($masterlistes as $masterliste_2) {

                if ($masterliste->asset_manager == $masterliste_2->asset_manager) {
                    $m_property                 = $masterliste_2->property;
                    $masterliste_2->property_id = ($m_property)
                    ? $m_property->id : null;
                    $tenancy_schedule_data           = TenancySchedulesService::get($masterliste_2->property_id);
                    $masterliste_2->tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : new TenancySchedule();

                    $count++;
                    $masterlistes_new[$masterliste->asset_manager]['asset_manager'] = $masterliste_2->asset_manager;
                    $masterlistes_new[$masterliste->asset_manager]['id']            = $masterliste_2->id;

                    $masterlistes_new[$masterliste->asset_manager]['count']              = $count;
                    $masterlistes_new[$masterliste->asset_manager]['asset_manager_name'] = $masterliste_2->asset_manager_name;

                    $masterlistes_new[$masterliste->asset_manager]['flat_in_qm'] += $masterliste_2->tenancy_schedule->calculations['mi9'] + $masterliste_2->tenancy_schedule->calculations['mi10'];
                    $masterlistes_new[$masterliste->asset_manager]['rented_area'] += $masterliste_2->tenancy_schedule->calculations['total_live_rental_space'] + $masterliste_2->tenancy_schedule->calculations['total_business_rental_space'];
                    $masterlistes_new[$masterliste->asset_manager]['vacancy'] += $masterliste_2->tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $masterliste_2->tenancy_schedule->calculations['total_business_vacancy_in_qm'];
                    $masterlistes_new[$masterliste->asset_manager]['rent_net_pm'] += $masterliste_2->tenancy_schedule->calculations['total_actual_net_rent'];

                    $masterlistes_new[$masterliste->asset_manager]['potential_pm'] += $masterliste_2->tenancy_schedule->calculations['potenzial_eur_jahr'];

                    $masterlistes_new[$masterliste->asset_manager]['object'] .= (isset($masterliste_2->object) ? $masterliste_2->object . '<br/> ' : ' ');
                }
            }

            $rented_area = isset($masterliste->rented_area) ? $masterliste->rented_area : 0;
            $summ_vermietet_m2 += $rented_area;
            if (isset($masterliste->flat_in_qm)) {
                $sum_rented_area += $masterliste->rented_area;
                $sum_flat_in_qm += $masterliste->flat_in_qm;
            }

            $vacancy = isset($masterliste->vacancy) ? $masterliste->vacancy : 0;
            $summ_leerstand_m2 += $vacancy;

            $potential_pm = isset($masterliste->potential_pm) ? $masterliste->potential_pm : 0;
//            $summ_differenz_miete_in_euro += $potential_pm;
            $summ_differenz_miete_pa_in_euro += $potential_pm * 12;
        }
        if ($sum_flat_in_qm != 0) {
            $summ_vermietet_percent = $sum_rented_area / $sum_flat_in_qm * 100;
            $summ_leerstand_percent = $summ_leerstand_m2 / $sum_flat_in_qm * 100;
        }

        foreach ($masterlistes_new as $masterliste) {

            $asset_manager_name_check = substr($masterliste['asset_manager_name'], 0, 11);
            if ($asset_manager_name_check != 'Entwicklung') {

                $masterlistes_new_1[$masterliste["asset_manager"]]['count'] = 0;

                $masterlistes_new_1[$masterliste["asset_manager"]]['asset_manager_name'] = '';
                $masterlistes_new_1[$masterliste["asset_manager"]]['flat_in_qm']         = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['rented_area']        = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['vacancy']            = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['rent_net_pm']        = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['potential_pm']       = 0;
                $masterlistes_new_1[$masterliste["asset_manager"]]['object']             = '';

                $masterlistes_new_1[$masterliste["asset_manager"]]['asset_manager']      = $masterliste["asset_manager"];
                $masterlistes_new_1[$masterliste["asset_manager"]]['count']              = $masterliste['count'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['asset_manager_name'] = $masterliste['asset_manager_name'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['flat_in_qm'] += $masterliste['flat_in_qm'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['rented_area'] += $masterliste['rented_area'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['vacancy'] += $masterliste['vacancy'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['rent_net_pm'] += $masterliste['rent_net_pm'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['potential_pm'] += $masterliste['potential_pm'];
                $masterlistes_new_1[$masterliste["asset_manager"]]['object'] .= $masterliste['object'];
            } else {
                $masterlistes_new_2[$masterliste["asset_manager"]]['count']              = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['asset_manager_name'] = '';
                $masterlistes_new_2[$masterliste["asset_manager"]]['flat_in_qm']         = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['rented_area']        = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['vacancy']            = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['rent_net_pm']        = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['potential_pm']       = 0;
                $masterlistes_new_2[$masterliste["asset_manager"]]['object']             = '';

                $masterlistes_new_2[$masterliste["asset_manager"]]['count']              = $masterliste['count'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['asset_manager_name'] = $masterliste['asset_manager_name'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['flat_in_qm'] += $masterliste['flat_in_qm'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['rented_area'] += $masterliste['rented_area'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['vacancy'] += $masterliste['vacancy'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['rent_net_pm'] += $masterliste['rent_net_pm'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['potential_pm'] += $masterliste['potential_pm'];
                $masterlistes_new_2[$masterliste["asset_manager"]]['object'] .= $masterliste['object'];
            }
            $summ_differenz_miete_in_euro += $masterliste['potential_pm'];
        }
        foreach ($Revenues as $Revenue) {
            $sum_Revenue = $sum_Revenue + $Revenue['total_purchase_price'];
        }
//        $summ_leerstand_percent /= count($masterlistes);
        //         $summ_vermietet_percent /= count($masterlistes);

        if (isset($input['asset_manager'])) {
            $masterlistes_new_1 = array();
            $masterlistes       = Masterliste::where('asset_manager', '=', $input['asset_manager'])->get();
            foreach ($masterlistes as $index => $masterliste) {

                $masterliste->asset_manager_name = User::where('id', $masterliste->asset_manager)->first()->name;
                $m_property                      = $masterliste->property;
                $masterliste->property_id        = ($m_property)
                ? $m_property->id : null;
                $tenancy_schedule_data                            = TenancySchedulesService::get($masterliste->property_id);
                $masterliste->tenancy_schedule                    = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : new TenancySchedule();
                $masterlistes_new_1[$index]['count']              = 1;
                $masterlistes_new_1[$index]['asset_manager_name'] = $masterliste['asset_manager_name'];
                // working
                $masterlistes_new_1[$index]['flat_in_qm']    = $masterliste['flat_in_qm'];
                $masterlistes_new_1[$index]['rented_area']   = $masterliste['rented_area'];
                $masterlistes_new_1[$index]['vacancy']       = $masterliste['vacancy'];
                $masterlistes_new_1[$index]['rent_net_pm']   = $masterliste['rent_net_pm'];
                $masterlistes_new_1[$index]['potential_pm']  = $masterliste->tenancy_schedule->calculations['potenzial_eur_jahr'];
                $masterlistes_new_1[$index]['asset_manager'] = $masterliste['asset_manager'];
                $masterlistes_new_1[$index]['object']        = $masterliste['object'];

                $query = str_replace('.', '', $masterliste['object']);

                //DB::enableQueryLog();

                // $properties = Properties::where('Ist',0)->where('soll',0)->where('name_of_property', 'like', '%' . $query . '%' )->first();
                // $properties_exe_2 = Properties::where('Ist',0)->where('soll',0)->where('name_of_property', 'like', '%' . $masterliste['object'] . '%' )->first();

                $properties_exe_2 = $properties = Properties::where('Ist', 0)->where('soll', 0)->where('id', $masterliste->property_id)->first();
                // $properties_exe_2 = Properties::where('Ist',0)->where('soll',0)->where('id', 'like', '%' . $masterliste['object'] . '%' )->first();

                //$query = DB::getQueryLog();

                if (isset($properties->id)) {
                    $masterlistes_new_1[$index]['id'] = $properties->id;
                }
                if (isset($properties_exe_2->id)) {
                    $masterlistes_new_1[$index]['id'] = $properties_exe_2->id;
                } else {
                    $masterlistes_new_1[$index]['id'] = 0;

                }

            }
        }

        $pa_array = $pa_array2 = array();
        foreach ($tenancy_items as $key => $tenancy_item) {
            if ($tenancy_item->tenancy_schedule_id) {
                $tenancy_schedule = TenancySchedule::where('id', $tenancy_item->tenancy_schedule_id)->first();
                $properties       = Properties::where('Ist', 0)->where('soll', 0)->where('id', $tenancy_schedule->property_id)->first();
                if ($properties != null && $properties->masterliste_id != null) {
                    $tenancy_item->object_name = $properties->name_of_property;
                    $masterliste               = Masterliste::where('id', $properties->masterliste_id)->first();
                    if ($masterliste != null) {

                        $tenancy_item->creator_name = "";
                        if ($masterliste->asset_manager) {
                            $tenancy_item->creator_name = User::where('id', $masterliste->asset_manager)->first()->name;
                        }

                        if (isset($pa_array[$masterliste->asset_manager])) {
                            $pa_array[$masterliste->asset_manager] += $tenancy_item->actual_net_rent;
                        } else {
                            $pa_array[$masterliste->asset_manager] = $tenancy_item->actual_net_rent;
                        }

                        if (isset($pa_array2[$tenancy_schedule->property_id])) {
                            $pa_array2[$tenancy_schedule->property_id] += $tenancy_item->actual_net_rent;
                        } else {
                            $pa_array2[$tenancy_schedule->property_id] = $tenancy_item->actual_net_rent;
                        }

                    } else {
                        unset($tenancy_items[$key]);
                    }
                    if ($tenancy_item->type == 4) {
                        $tenancy_item->type = 'Gewerbe';
                    } elseif ($tenancy_item->type == 3) {
                        $tenancy_item->type = 'Wohnen';
                    }

                } else {
                    unset($tenancy_items[$key]);
                }

            }
        }

        foreach ($tenancy_items_2 as $key => $tenancy_item) {

            if (($tenancy_item->rent_begin != '' && $tenancy_item->rent_begin > date('Y-m-d')) || ($tenancy_item->rent_end != '' && $tenancy_item->rent_end < date('Y-m-d'))) {
                continue;
            }

            if ($tenancy_item->tenancy_schedule_id && $tenancy_item->actual_net_rent && $tenancy_item->rental_space) {
                $tenancy_schedule = TenancySchedule::where('id', $tenancy_item->tenancy_schedule_id)->first();
                $properties       = Properties::where('Ist', 0)->where('soll', 0)->where('id', $tenancy_schedule->property_id)->first();
                if ($properties != null && $properties->masterliste_id != null) {
                    $tenancy_item->object_name = $properties->name_of_property;
                    $masterliste               = Masterliste::where('id', $properties->masterliste_id)->first();
                    if ($masterliste != null) {
                        $tenancy_item->creator_name = "";
                        if ($masterliste->asset_manager) {
                            $tenancy_item->creator_name = User::where('id', $masterliste->asset_manager)->first()->name;
                        }

                        if (isset($pa_array[$masterliste->asset_manager])) {
                            $pa_array[$masterliste->asset_manager] += $tenancy_item->actual_net_rent;
                        } else {
                            $pa_array[$masterliste->asset_manager] = $tenancy_item->actual_net_rent;
                        }

                        if (isset($pa_array2[$tenancy_schedule->property_id])) {
                            $pa_array2[$tenancy_schedule->property_id] += $tenancy_item->actual_net_rent;
                        } else {
                            $pa_array2[$tenancy_schedule->property_id] = $tenancy_item->actual_net_rent;
                        }

                    } else {
                        unset($tenancy_items_2[$key]);
                    }
                    if ($tenancy_item->type == 2) {
                        $tenancy_item->type = 'Gewerbe';
                    } elseif ($tenancy_item->type == 1) {
                        $tenancy_item->type = 'Wohnen';
                    }

                } else {
                    unset($tenancy_items_2[$key]);
                }
            }
        }
        // print "<pre>";
        // print_r($masterlistes_new_1); die;
        $is_asset_manager = isset($input['asset_manager']);
        return view('assetmanagement.index', compact('user', 'Revenues', 'sum_Revenue', 'properties', 'masterlistes', 'masterlistes_new_1', 'masterlistes_new_2', 'summ_vermietet_m2', 'summ_vermietet_percent', 'summ_leerstand_m2', 'summ_leerstand_percent', 'summ_differenz_miete_in_euro', 'pa_array', 'pa_array2', 'summ_differenz_miete_pa_in_euro', 'is_asset_manager', 'tenancy_items', 'tenancy_items_2', 'graph_data', 'analytics_array', 'diffff'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function setAssestManagerOnProperty($property_id = null, $am_id = null)
    {
        $model = Properties::whereRaw('id=' . $property_id . ' OR main_property_id=' . $property_id)
            ->update(['asset_m_id' => $am_id]);

        if ($model) {
            $result = ['status' => true, 'message' => 'AM Update successfully.'];
        } else {
            $result = ['status' => false, 'message' => 'AM Update fail! Try again.'];
        }
        return response()->json($result);
    }

    public function getRentCompare(Request $request)
    {
        // $a_date = date('Y-m-d');
        $month = $request->month;
        if($request->month){
            if(strlen($request->month)==1)
                    $request->month = '0'.$request->month;
            $a_date = date('Y').'-'.$request->month.'-01';
        }
        for ($i=0; $i < 4 ; $i++) { 
            $end_date[] = date("Y-m-t", strtotime($a_date));
            $a_date = date('Y-m',strtotime($a_date.' -1 years')).'-01';
        }

        $min_date = $end_date[3];
        $max_date = $end_date[0];
        // pre($end_date);
       
        
        
        $rent_array3 = $rent_array2 = $rent_array = array();


        foreach($end_date as $key=>$list)
        {


        $rental_agreements = TenancySchedule::selectRaw('asset_manager_id,users.name as am_name, tenancy_schedules.property_id, properties.name_of_property, tenancy_schedule_items.id, tenancy_schedule_items.name, tenancy_schedule_items.comment,tenancy_schedule_items.rent_end, tenancy_schedule_items.rental_space, tenancy_schedule_items.actual_net_rent, tenancy_schedule_items.type,rent_begin,rent_end,asset_m_id')
                                                    ->join('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')
                                                    ->join('properties', 'tenancy_schedules.property_id', '=', 'properties.id')
                                                    ->join('masterliste', 'masterliste.id', '=', 'properties.masterliste_id')
                                                    ->join('users', 'tenancy_schedule_items.asset_manager_id', '=', 'users.id')
                                                    ->where('Ist', 0)->where('soll', 0)
                                                    
                                                    ->whereIn('type',[1,2])
                                                    ->where('properties.status',6)
                                                    ->where('tenancy_schedule_items.status',1)
                                                    ->where('rent_end', '>=', $list)
                                                    ->where('rent_begin', '<=', $list)
                                                    ->whereNotNull('rent_end');
            $rental_agreements = $rental_agreements->orderBy('rent_end','asc')->get();

            // echo $list;
            $year = date('Y',strtotime($end_date[$key]));
            
            foreach ($rental_agreements as  $value) {

                if($request->hide_status && $value->rent_begin<=$min_date && $value->rent_end>=$max_date)
                    continue;

                if(isset($rent_array[$value->am_name][$year]))
                $rent_array[$value->am_name][$year] += $value->actual_net_rent;
                else
                $rent_array[$value->am_name][$year] = $value->actual_net_rent;

                $rent_array2[$value->am_name] = $value->asset_manager_id;
                // $rent_array[$value->asset_m_id][$year]['name'] = $value->am_name;
                if(isset($rent_array3[$value->am_name][$year]))
                    $rent_array3[$value->am_name][$year] +=1;
                else
                    $rent_array3[$value->am_name][$year] =1;
            }


        }
        // pre($rent_array3);
        // echo "<br>";
        return view('assetmanagement.rent_compare', compact('rent_array','month','rent_array2','rent_array3'));                    
    }
    
    public function getRentCompareList(Request $request)
    {
        
        if($request->month){
            if(strlen($request->month)==1)
                    $request->month = '0'.$request->month;
        }
        $month = $request->month;

        if($request->month){
            $a_date = date('Y').'-'.$request->month.'-01';
        }
        for ($i=0; $i < 4 ; $i++) { 
            $end_date[] = date("Y-m-t", strtotime($a_date));
            $a_date = date('Y-m',strtotime($a_date.' -1 years')).'-01';
        }

        $min_date = $end_date[3];
        $max_date = $end_date[0];
        
        $end_date = array();

        $year = $request->year;
        $user_id = $request->user_id;
        $a_date = $year.'-'.$month.'-01';

        $end_date[0] = date("Y-m-t", strtotime($a_date));
        // pre($end_date); die;

        foreach($end_date as $key=>$list)
        {


        $rental_agreements = TenancySchedule::selectRaw('users.name as am_name, tenancy_schedules.property_id, properties.name_of_property, tenancy_schedule_items.id, tenancy_schedule_items.name, tenancy_schedule_items.comment,tenancy_schedule_items.rent_end, tenancy_schedule_items.rental_space, tenancy_schedule_items.actual_net_rent, tenancy_schedule_items.type,rent_begin,rent_end')
                                                    ->leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')
                                                    ->join('properties', 'tenancy_schedules.property_id', '=', 'properties.id')
                                                    ->join('users', 'tenancy_schedule_items.asset_manager_id', '=', 'users.id')
                                                    ->where('Ist', 0)->where('soll', 0)
                                                    ->whereIn('type',[1,2])
                                                    ->where('tenancy_schedule_items.status',1)
                                                    ->where('properties.status',6)
                                                    ->where('rent_end', '>=', $list)
                                                    ->where('rent_begin', '<=', $list)
                                                    ->where('asset_manager_id',$user_id);
                                                    
        $rental_agreements = $rental_agreements->orderBy('rent_end','asc')->get();


        }


        foreach ($rental_agreements as $key => $value) {
            if($request->hide_status && $value->rent_begin<=$min_date && $value->rent_end>=$max_date)
                    unset($rental_agreements[$key]);

        }

        // pre($rental_agreements);
        // echo "<br>";
        return view('assetmanagement.rent_compare_list', compact('rental_agreements'));
    }
}
