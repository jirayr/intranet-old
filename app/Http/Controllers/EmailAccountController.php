<?php

namespace App\Http\Controllers;

use App\Repositories\EmailAccountRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EmailAccountController extends Controller
{
    protected $emailAccountRepository;

    public function __construct(EmailAccountRepository $emailAccountRepository)
    {
        $this->emailAccountRepository = $emailAccountRepository;
    }

    public function store(Request $request)
    {

        return json_encode($this->emailAccountRepository->store($request->all()));
    }

    public function getAccounts()
    {
        return $this->emailAccountRepository->get();
    }

    public function edit($id)
    {
        return $this->emailAccountRepository->edit($id);
    }

    public function update($id,Request $request)
    {
         return json_encode($this->emailAccountRepository->update($id,$request->all()));
    }

    public function delete($id)
    {
        return json_encode($this->emailAccountRepository->delete($id));
    }
}
