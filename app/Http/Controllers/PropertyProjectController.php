<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Services\PropertyProjectService;
use Illuminate\Http\Request;

class PropertyProjectController extends Controller
{
    protected  $propertyProjectService;

    public function __construct(PropertyProjectService $propertyProjectService)
    {
        $this->propertyProjectService = $propertyProjectService;
    }


    public function create()
    {
        return view('properties.project.add');
    }

    public function store(Request $request)
    {
        $this->propertyProjectService->store($request);
    }

    public function edit($pId,$id)
    {
        return  $this->propertyProjectService->find($id);
    }

    public function update($pId,$id,Request $request)
    {
        $this->propertyProjectService->update($id,$request);

    }

    public function delete($pId,$id)
    {
        $this->propertyProjectService->delete($id);
    }

    public function getEmails($propertyId,$projectId,$emailType)
    {
        $employees = $this->propertyProjectService->find($projectId)->employees->pluck('email');
        $data = Email::whereIn('email_sent_from',$employees)->with(['emailMapping'=> function($query)use($emailType) {
                $query->where('email_type_id',$emailType)->get();}
            ])->orderBy('date_of_email','DESC')->get();
        return view('properties.project.emails',compact('data','emailType'));
    }


}
