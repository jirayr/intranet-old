<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Models\Properties;
use App\Models\Banks;
use App\Models\Notifications;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Services\PropertiesService;
use App\Models\Masterliste;
use App\Models\TenancySchedule;
use App\Services\TenancySchedulesService;
use App\Models\TenancyScheduleItem;
use App\Models\PropertyComments;
use DB;
use Illuminate\Support\Facades\View;
use Response;
use App\Profit_loss_graph;
use Carbon\CarbonPeriod;

class ApiController extends Controller
{
    public function oldest($status = ''){

        if($status == ''){ die(); }

        $data = array();
        $immo_upload = DB::table('immo_upload')
            ->where('status', $status)
            ->orderBy('immo_upload.id', 'asc')->first();

        if(!is_null($immo_upload)){

            $data = json_decode(json_encode($immo_upload), TRUE);
            $images_data = array();
            if(!is_null($data['images'])){

                $images = explode( '|', $data['images']);
                for ($i=0; $i < count($images); $i++){

                    $images_data[] = 'https://intranet.fcr-immobilien.de/property_images/'.$images[$i];
                }
            }

            $data['images'] = $images_data;

        }

        echo json_encode($data);
    }

    public function update($id = 0, $status = ''){

        if(!is_numeric($id) || $id == 0){ die('provide id for update data!'); }
        if($status == ''){ die(); }


        DB::table('immo_upload')->where('id', $id)->update(['status' => $status]);
        echo 'data updated!';


    }




}