<?php

namespace App\Http\Controllers;
use App\Models\PropertyComments;
use App\Services\PropertyCommentsService;
use Illuminate\Http\Request;
use Auth;
use App\Models\Masterliste;
use App\User;
use Validator;


class PropertyCommentsController extends Controller
{
	
	public function updateCommentDate(Request $request)
    {
		 try {

            $name = $request['name'];
            $value = $request['value'];
            $update_comment = PropertyComments::find($request['pk']);

            $update_comment->$name = $value;

            $update_comment->save();
            return $update_comment;

        } catch (\Exception $e) {

            return null;
        }		
    }
	
	
	public function show($id)
    {
        $property_id = $id;
        $masterlistes = Masterliste::all();
        foreach($masterlistes as $masterliste){
            if($masterliste->asset_manager){
                $masterliste->asset_manager_name = User::where('id', $masterliste->asset_manager)->first()->name;
            }
        }

        $comments = PropertyComments::where('property_id',$property_id)->get();
        return view('property_comments.show',compact('property_id','masterlistes', 'comments'));
    }
	
	public function _show()
    {
		$property_id= -1;
        $masterlistes = Masterliste::all();
        foreach($masterlistes as $masterliste){
            if($masterliste->asset_manager){
                $masterliste->asset_manager_name = User::where('id', $masterliste->asset_manager)->first()->name;
            }
        }

        $comments = PropertyComments::where('property_id',$property_id)->get();
        return view('property_comments.show',compact('property_id','masterlistes', 'comments'));
    }

    public function create(Request $request){
        $input = $request->all();

        $input['comment'] = $input['comment_text'];
        unset($input['comment_text']);

        $input['pdf_file'] = "";
        if($request->file('pdf'))
        {

            $attributeNames = array(
            'pdf' => 'PDF',
            );

            $validator = Validator::make($request->all(),
            [
            'pdf' => 'required|mimes:pdf|max:4096',
            ]);

            // echo $request->file('upload_pdf')->getClientOriginalName(); die;

            $validator->setAttributeNames($attributeNames);

            if ($validator->fails()){
            return redirect()->back()->with('error', $validator->errors()->first());   
            }


            $extension = $request->file('pdf')->getClientOriginalExtension();
            $dir = 'pdf_upload/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pdf')->move($dir, $filename);
            $input['pdf_file'] = $filename;
        }


        PropertyCommentsService::create($input);
        // return  back();

        return redirect('properties/'.$request->property_id.'?tab=comment_tab');
    }

    public function edit(Request $request,$id){
        $update_comment=PropertyComments::where('id','=',$id);
        $input = $request->all();
        if($input['value']=='DELETE'){
            PropertyCommentsService::delete($update_comment);
        }
        else{
            PropertyCommentsService::update($input['value'],$update_comment);
        }

        return  back();
    }

  public function edit_comment_show_iframe(Request $request,$id){
       
      
      $flight = PropertyComments::updateOrCreate(
     ['property_id' => $id, 'user_id' => Auth::id()],
     [ $request->pk => $request->value]
     );


        //return  back();
    }

 


    public function update_comment(Request $request){
        $input = $request->all();
        $update_comment=PropertyComments::where('id','=',$input['comment_id_modal']);
        PropertyCommentsService::update_comment($input['comment_edit_modal'],$update_comment);
        return  back();
    }

}
