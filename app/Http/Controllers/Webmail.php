<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Mail;

class Webmail extends Controller
{
    
    public function index()
    {

        $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        if(!is_null($user_data)){
          
        $mailbox = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX', $user_data->web_email, $user_data->web_pass);
 
        try {
            $mailbox->imap('check');
   
            return view('webmail.index',compact('mailbox'));
        }
        catch (\Exception $e) {
             return redirect('webmail-login');
        }

         }else
            {
                return redirect('webmail-login');
            }
 
    }
 


public function webmail_login_post(Request $request){
        
        $mailbox = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX', $request->email, $request->password);
       
        try {
            $check = $mailbox->imap('check');
             
            $update_create = DB::table('webmail')->where('user_id',Auth::id())->first();

            if(!is_null($update_create)){

                DB::table('webmail')->where('user_id',Auth::id())->update([
                    'web_email' => $request->email,
                    'web_pass' => $request->password
                ]);
            }else{
                DB::table('webmail')->insert([
                    'user_id' => Auth::id(),
                    'web_email' => $request->email,
                    'web_pass' => $request->password
                ]);
            }
                
          return redirect('webmail'); 
        }
        catch (\Exception $e) {
              
        return redirect('webmail-login')->with('error','Email or Password is incorrect');
        }
}

// Inbox  read email by id
public function read($id){
 
      $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        if(!is_null($user_data)){

        $path_to_save = '';
        if (!is_dir(public_path().'/email_attachments/'.$user_data->web_email)) {
             $path_to_save = mkdir(public_path().'/email_attachments/'.$user_data->web_email, 0777, true);
        }

        $path_attachment = public_path().'/email_attachments/'.$user_data->web_email;

        $mailbox = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX', $user_data->web_email, $user_data->web_pass, $path_attachment);
 
         try {
            $mailbox->imap('check');
 
                $head = $mailbox->getMailHeader($id);
                $markAsSeen = true;
                $mail = $mailbox->getMail($id, $markAsSeen);

                return view('webmail.read',compact('head','mail'));
                 
            }
        catch (\Exception $e) {
             return redirect('webmail-login');
        }

         }else
            {
                return redirect('webmail-login');
            }
        }
 
// Sent  read email by id
public function sent_read($id){
 
      $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        if(!is_null($user_data)){

        $path_to_save = '';
        if (!is_dir(public_path().'/email_attachments/'.$user_data->web_email)) {
             $path_to_save = mkdir(public_path().'/email_attachments/'.$user_data->web_email, 0777, true);
        }

        $path_attachment = public_path().'/email_attachments/'.$user_data->web_email;

        $mailbox = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Sent', $user_data->web_email, $user_data->web_pass, $path_attachment);
 
         try {
            $mailbox->imap('check');
 
                $head = $mailbox->getMailHeader($id);
                $markAsSeen = true;
                $mail = $mailbox->getMail($id, $markAsSeen);

                return view('webmail.sent_read',compact('head','mail'));
                 
            }
        catch (\Exception $e) {
             return redirect('webmail-login');
        }

         }else
            {
                return redirect('webmail-login');
            }
        }


// Spam read email by id
public function spam_read($id){
 
      $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        if(!is_null($user_data)){

        $path_to_save = '';
        if (!is_dir(public_path().'/email_attachments/'.$user_data->web_email)) {
             $path_to_save = mkdir(public_path().'/email_attachments/'.$user_data->web_email, 0777, true);
        }

        $path_attachment = public_path().'/email_attachments/'.$user_data->web_email;

        $mailbox = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Junk', $user_data->web_email, $user_data->web_pass, $path_attachment);
 
         try {
            $mailbox->imap('check');
 
                $head = $mailbox->getMailHeader($id);
                $markAsSeen = true;
                $mail = $mailbox->getMail($id, $markAsSeen);

                return view('webmail.spam_read',compact('head','mail'));
                 
            }
        catch (\Exception $e) {
             return redirect('webmail-login');
        }

         }else
            {
                return redirect('webmail-login');
            }
        }
 
// Trash read email by id
public function trash_read($id){
 
      $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        if(!is_null($user_data)){

        $path_to_save = '';
        if (!is_dir(public_path().'/email_attachments/'.$user_data->web_email)) {
             $path_to_save = mkdir(public_path().'/email_attachments/'.$user_data->web_email, 0777, true);
        }

        $path_attachment = public_path().'/email_attachments/'.$user_data->web_email;

        $mailbox = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Trash', $user_data->web_email, $user_data->web_pass, $path_attachment);
 
         try {
            $mailbox->imap('check');
 
                $head = $mailbox->getMailHeader($id);
                $markAsSeen = true;
                $mail = $mailbox->getMail($id, $markAsSeen);

                return view('webmail.trash_read',compact('head','mail'));
                 
            }
        catch (\Exception $e) {
             return redirect('webmail-login');
        }

         }else
            {
                return redirect('webmail-login');
            }
        }


public function reply(Request $request){
  //dd($request->filename);
        $body_message = $request->body_message;
        $to = $request->replyto;
        $subject = $request->subject;
        $from = $request->from_address;
    
        $path = array();
        $ext = array();
        $filename = array();
        if($request->hasFile('filename')) 
        {
             foreach($request->filename as $file)
            {
              $path[] =  $file->getRealPath();
              $ext[] = $file->getClientOriginalExtension();
              $filename[] = $file->getClientOriginalName();
            }
        }
    
        //'info@intran.fcr-immobilien.de'
    try{  
         Mail::send('emails.webmail', ['body_message' => $body_message], function ($message) use ($path,$ext,$filename,$to,$subject,$from,$request)
        {

            $message->from($from,'FCR INTRANET');

            $message->to($to);
  
            $size = sizeOf($path); //get the count of number of attachments 

            for ($i=0; $i < $size; $i++) {
                $message->attach($path[$i],[
                    'as' => $filename[$i].'.'.$ext[$i] 
                ]);
            }
     
            $message->subject($subject);

        },true);

          return redirect()->back()->with('message','Email sent successfully!');

 }catch (\Exception $e){
         return $e->getMessage();
        // return redirect()->back()->with('error','Email not sent!');
  }
}



    public function delete($id){
    
        $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        $server = '{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}';
        $connection = imap_open($server, $user_data->web_email, $user_data->web_pass);
         //$mailboxes = imap_list($connection, $server, '*');
        // var_dump($mailboxes);
         imap_mail_move($connection, $id, 'INBOX.Trash',CP_UID);
         return redirect('webmail')->with('message','Email deleted successfully!');
    
    } 


    public function delete_sent($id){
    
        $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        $server = '{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Sent';
        $connection = imap_open($server, $user_data->web_email, $user_data->web_pass);
        imap_mail_move($connection, $id, 'INBOX.Trash',CP_UID);
        return redirect('webmail-sent')->with('message','Email deleted successfully!');
 
    } 

    public function delete_spam($id){
    
        $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        $server = '{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Junk';
        $connection = imap_open($server, $user_data->web_email, $user_data->web_pass);
        //$mailboxes = imap_list($connection, $server, '*');
        //var_dump($mailboxes);
        imap_mail_move($connection, $id, 'INBOX.Trash',CP_UID);
        return redirect('webmail-spam')->with('message','Email deleted successfully!');
 
    } 
 

 
    public function permanent_delete($id){
    $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
    $mbox = imap_open("{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Trash", $user_data->web_email, $user_data->web_pass);
      
    imap_delete($mbox, $id,FT_UID);
  
     return redirect('webmail-trash')->with('message','Email permanent deleted!');
    }


    public function empty_trash(){
   
        $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
       
        $mbox = imap_open("{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Trash", $user_data->web_email, $user_data->web_pass);
       
        imap_delete($mbox,'1:*');   
        imap_expunge($mbox);
   
        return redirect('webmail-trash')->with('message','Trash is empty!');
 
    }

 
    public function trash(){
    
        $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        if(!is_null($user_data)){
          
        $Trash = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Trash', $user_data->web_email, $user_data->web_pass);
 
        try {
            $Trash->imap('check');
   
            return view('webmail.trash',compact('Trash'));
        }
        catch (\Exception $e) {
             return redirect('webmail-login');
        }

         }else
            {
                return redirect('webmail-login');
            }
    }

 

    public function sent(){
    
        $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        if(!is_null($user_data)){
          
        $sent = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Sent', $user_data->web_email, $user_data->web_pass);
 
        try {
            $sent->imap('check');
   
            return view('webmail.sent',compact('sent'));
        }
        catch (\Exception $e) {
             return redirect('webmail-login');
        }

         }else
            {
                return redirect('webmail-login');
            }
    }


    public function spam(){
    
        $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
        if(!is_null($user_data)){
          
        $spam = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX.Junk', $user_data->web_email, $user_data->web_pass);
 
        try {
            $spam->imap('check');
   
            return view('webmail.spam',compact('spam'));
        }
        catch (\Exception $e) {
             return redirect('webmail-login');
        }

         }else
            {
                return redirect('webmail-login');
            }
    }

 






}
