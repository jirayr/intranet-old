<?php

namespace App\Http\Controllers;

use App\Models\BankFinancingOffer;
use App\Services\property\BankFinancingOfferService;
use Illuminate\Http\Request;

class BankFinancingOffersController extends Controller
{
   protected $bankFinancingOfferService;
    public function __construct(BankFinancingOfferService $bankFinancingOfferService)
    {
        $this->bankFinancingOfferService = $bankFinancingOfferService;
    }

    public function index()
    {

    }

    public function store(Request $request)
    {
        $this->bankFinancingOfferService->store($request->all());
        return back();
    }

    public function edit($id)
    {
        return $this->bankFinancingOfferService->edit($id);
    }

    public function update(Request $request , $id)
    {
        $this->bankFinancingOfferService->update($request->all(),$id);
    }

    public function delete($id)
    {
        $this->bankFinancingOfferService->delete($id);
    }
}
