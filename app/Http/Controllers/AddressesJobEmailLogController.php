<?php

namespace App\Http\Controllers;

use App\AddressesJob;
use App\Services\AddressesJobEmailLogService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class AddressesJobEmailLogController extends Controller
{
    protected  $addressesJobEmailLogService;

    public function __construct(AddressesJobEmailLogService $addressesJobEmailLogService)
    {
        $this->addressesJobEmailLogService = $addressesJobEmailLogService;
    }


    public function index()
    {
        return view('adressen.email-logs');
    }

    public function create(Request $request)
    {
       $logs = $this->addressesJobEmailLogService->getAll();
       return Datatables::of($logs)->make(true);
    }

    public function emailLog(Request $request,$id)
    {
        $propertyAddressesJobs = AddressesJob::where('property_id',$id)->orderBy('id', 'DESC')->with('emailLogs')->first();
        return view('adressen.property-email-logs',compact('propertyAddressesJobs'));
    }

}
