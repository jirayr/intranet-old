<?php

namespace App\Http\Controllers;

use App\Account;
use App\BankTransaction;
use App\Imports\AccountsFileImport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class BankTransactionController extends Controller
{

    public function index()
    {
        $bankTransactions = BankTransaction::all();
        return view('banks.bank_transactions',compact('bankTransactions'));
    }


    public function AccountsImport(Request $request)
    {
        Excel::import(new AccountsFileImport($request), Input::file('pic'));
        return 'done';
    }

    public function getAccountBankTransactions($AccountId)
    {
        $bankTransactions = BankTransaction::where('account_id',$AccountId)->orderBy('date_of_transaction','DESC')->get();
        return view('banks.bank_transactions',compact('bankTransactions'));
    }
}
