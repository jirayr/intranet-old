<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\PropertiesComment;
use Auth;



class PropertiesCommentController  extends Controller
{
	public function saveNewComment(Request $request)
	{
		$m = new PropertiesComment;
		$m->property_id = $request->property_id;
		$m->user_id = Auth::user()->id;
		$m->comment = $request->comment;
        $m->type = 'properties';
		$m->save();
	}
	public function getComments(Request $request)
    {
        $data = PropertiesComment::selectRaw('properties_comments.*,users.name')->where('property_id',$request->id)->leftJoin('users','users.id','=','properties_comments.user_id')->orderBy('properties_comments.created_at','desc')
        ->where('type','properties')
        ->limit($request->show)->get();
        foreach ($data as $key => $value) {
        	$value->f_date = show_datetime_format($value->created_at);
        }
        return $data;
    }
    public function deleteComment($id)
    {
    	PropertiesComment::find($id)->delete();
        echo "1"; die;
    }
}
?>