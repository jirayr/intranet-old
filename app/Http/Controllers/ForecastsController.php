<?php

namespace App\Http\Controllers;

use App\Models\Forecast;
use App\Models\ForecastKind;
use App\Services\ForecastKindsService;
use App\Services\ForecastsService;
use Illuminate\Http\Request;

class ForecastsController extends Controller
{
    public function index(){
        $forecasts = Forecast::all();

        foreach ($forecasts as $forecast) {
            $forecast->forecast_kinds = ForecastKind::where('forecast_id', $forecast->id)->get();
        }

        //Get selecting forecast
        if (isset($_GET['selecting-forecast'])){
            $selecting_forecast = $_GET['selecting-forecast'];
        } else {
            if (count($forecasts) > 0){
                $selecting_forecast = $forecasts[0]->id;
            }
            else $selecting_forecast = -1;

        }

        return view('forecasts.index', compact('forecasts', 'selecting_forecast'));

    }

    public function store(Request $request){

        $forecastData = [
            'name' => 'New Forecast',
            'missing_info' => 'Fehlende Information',
            'todo'  => 'Todo'
        ];

        $forecast = Forecast::create($forecastData);


        return redirect("/forecasts?selecting-forecast=$forecast->id");
    }

    public function update_by_field(Request $request, $forecast_id){
        $input = $request->all();
        $response = [
            'success' => false,
            'msg' => 'Failed to updated forecast'
        ];

        $forecast = Forecast::where('id', '=', $forecast_id)->first();

        // Clone forecast
        $input = $forecast;

        if( empty( $forecast ) ) {
            $response['msg'] = 'Not found forecast';
            return response()->json($response);
        }

        // New value
        $input[$request->pk] = $request->value;

        if (ForecastsService::update($forecast, $input)) {
            $response = [
                'success' => true,
                'msg' => '',
                'selecting_forecast' => $forecast_id
            ];
        }

        return response()->json($response);
    }

    public function delete($id){
        ForecastKindsService::delete($id);
        Forecast::find($id)->delete();

        return redirect(route('forecast.index'));
    }


}
