<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use App\Models\Directory_Permission;
use App\Models\PropertiesDirectory;
use App\Models\Properties;
use App\Models\PropertyInvoices;

class MainFileManagerController extends Controller
{
    //
	public function index(){
        return view('file-manager.main.index');
    }
    public function listDirSidebar(Request $request){
        $dir = '/';

        $recursive = false; // Get subdirectories also?
        $contents = Storage::cloud()->listContents($dir, $recursive);
        foreach($contents as $subKey => $subArray){
            if($subArray['basename'] == '1Rm4D6kuh_IC8dKJPjUnN4_Cnl0zSIIdI'){
                unset($contents[$subKey]);
            }
        }
        $contents = collect($contents)->where('type', '=', 'dir');
        $contents = array_values(array_sort($contents, function ($value) {
			return $value['name'];
		}));
        $user = Auth::user();
        $permission = Directory_Permission::where('user_id', $user->id)->get();
        
        return view('file-manager.tree', compact('contents', 'user', 'permission'));
    }
    public function upload(Request $request){
        $driverId = "";
        if ($request->hasFile('upload')) {
            $file = $request->file('upload');
            $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $originalExt = $file->getClientOriginalExtension();
            $uniqueName = $originalName;
            $foldername = $request->working_dir;
            $driverId = $this->uploadToGoogleDrive($uniqueName,$file,$foldername,$originalExt);
        }
        if($driverId == ''){
            $response = [
                'success' => false,
                'msg' => 'Fail upload fail.'
            ];
        }else{
            $response = [
                'success' => true,
                'msg' => 'File upload succesfully.',
                'file_id' => $driverId
            ];
        }
        return response()->json($response);
    }

    public function uploadToGoogleDrive($unique_name, $file_data, $upload_folder, $extension)
    {
        $root_dir = '/';
        $recursive = false;
        $contents = collect(Storage::cloud()->listContents($root_dir, $recursive));

        //get unique name dir
        $contents = collect(Storage::cloud()->listContents($upload_folder, $recursive));
        $app_dir = $contents->where('type', 'dir')->where('filename', $unique_name)->first();

        $upload_file_name = $unique_name;
        $fileData = File::get($file_data);
        //upload file
        $file2 =  Storage::cloud()->put($upload_folder.'/'.$upload_file_name.'.'.$extension, $fileData);

        //get google drive id
        $contents = collect(Storage::cloud()->listContents($upload_folder, $recursive));

        $file = $contents ->where('type','file')->where('filename', $upload_file_name)->where('extension', $extension) ->first();

        $google_drive_id = $file["basename"];
        //return id if file upload
        return $google_drive_id;
    }

    public function newfolder(Request $request){
        $root_dir = $request->working_dir;
        $new_folder_name = $request->name;
        $recursive = false;
        $contents = collect(Storage::cloud()->listContents($root_dir, $recursive));
        //get app dir
        $app_parent_dir = $contents->where('type', 'dir')->where('filename', $new_folder_name)->first();

        //check already exist Folder
        if ( ! $app_parent_dir) {
            //Create Folder
            if ( ! Storage::cloud()->makeDirectory($root_dir.'/'.$new_folder_name) ) {
                return '';
            }
            //get ipaz dir
            $contents = collect(Storage::cloud()->listContents($root_dir, $recursive));
            $app_parent_dir = $contents->where('type', 'dir')->where('filename', $new_folder_name)->first();
        }

        if($app_parent_dir == ''){
            $response ='Folder create fail.';
        }else{
            $response = 'Folder create succesfully.';
        }
        return response()->json($response);
    }

    public function listMain(Request $request){
        $dir = '/';
        if($request->has('working_dir') && $request->working_dir != ""){
            $dir = $request->working_dir;
        }
        $recursive = false; // Get subdirectories also?
        $contents = Storage::cloud()->listContents($dir, $recursive);
        foreach($contents as $subKey => $subArray){
            if($subArray['basename'] == '1Rm4D6kuh_IC8dKJPjUnN4_Cnl0zSIIdI'){
                unset($contents[$subKey]);
            }
        }
        if ($request->has('sort_type') && $request->sort_type == 'alphabetic' && $request->order == 'asc') {

			$contents = array_values(array_sort($contents, function ($value) {
				return $value['name'];
			}));
		}

		if ($request->has('sort_type') && $request->sort_type == 'alphabetic' && $request->order == 'desc') {
			$contents = array_values(array_reverse(array_sort($contents, function ($value) {
				return $value['name'];
			})));
        }
        $user = Auth::user();
        $permission = Directory_Permission::where('user_id', $user->id)->get();

        $allUsers = User::all();


        if($request->has('show_list') && $request->show_list == 0){
            return [
                'html' => (string) view('file-manager.grid-view', compact('contents', 'user', 'permission', 'allUsers')),
                'working_dir' => $dir,
            ];
        }
        return [
            'html' => (string) view('file-manager.list-view', compact('contents', 'user', 'permission', 'allUsers')),
            'working_dir' => $dir,
        ];
    }

    public function download(Request $request){
        if($request->has('file') && $request->file != ""){
            $parentDir = "/";
            if($request->has('file_dir') && $request->file_dir != ""){
                $parentDir = $request->file_dir;
            }
            $contents = collect(Storage::cloud()->listContents($parentDir, false));
            $file = $contents->where('basename', '=', $request->file )->first();
            $rawData = Storage::cloud()->get($file['path']);
            $filename = $file['name'];
            return response($rawData, 200)
            ->header('ContentType', $file['mimetype'])
            ->header('Content-Disposition', "attachment; filename=$filename");
        }
        else{
            return false;
        }

    }

    public function downloadInvoices(Request $request)
    {
        if($request->has('file') && $request->file != ""){
            $parentDir = "/";
            if($request->has('file_dir') && $request->file_dir != ""){
                $parentDir = $request->file_dir;
            }
            $contents = collect(Storage::cloud()->listContents($parentDir, true));
            $file = $contents->where('basename', '=', $request->file )->first();
            $rawData = Storage::cloud()->get($file['path']);
            $filename = $file['name'];
            return response($rawData, 200)
                ->header('ContentType', $file['mimetype'])
                ->header('Content-Disposition', "attachment; filename=$filename");
        }
        else{
            return false;
        }
    }

    public function showDriveOnMainPage()
    {
        return view('file-manager.main.main_index');
    }

    public function deleteDir(Request $request){
        if($request->has('delete_path')){
            Storage::cloud()->delete($request->delete_path);
            $response = [
                'success' => true,
                'msg' => 'Directory delete successfully.'
            ];
        }else{
            $response = [
                'success' => flase,
                'msg' => 'Directory delete fail.'
            ];
        }
    }
    public function deleteFile(Request $request){
        if($request->has('delete_path')){
            Storage::cloud()->delete($request->delete_path);
            $response = [
                'success' => true,
                'msg' => 'File delete successfully.'
            ];
        }else{
            $response = [
                'success' => flase,
                'msg' => 'File delete fail.'
            ];
        }
    }
    public function renameFile(Request $request){
        if($request->has('path') && $request->has('dirname') && $request->has('new_name') && $request->has('extension')){
            Storage::cloud()->move($request->path, $request->dirname.'/'.$request->new_name.".".$request->extension );
            $response = [
                'success' => true,
                'msg' => 'File rename successfully.'
            ];
        }else{
            $response = [
                'success' => flase,
                'msg' => 'File rename fail.'
            ];
        }
    }
    public function renameDir(Request $request){
        if($request->has('path') && $request->has('dirname') && $request->has('new_name')){
            Storage::cloud()->move($request->path, $request->dirname.'/'.$request->new_name );
            $response = [
                'success' => true,
                'msg' => 'Directory rename successfully.'
            ];
        }else{
            $response = [
                'success' => flase,
                'msg' => 'Directory rename fail.'
            ];
        }
    }

    public function createNewFolderForAllProperties(Request $request)
    {
        $recursive = false;
        $newFolderName='Betriebskostenabrechnung';
        /****** Statuses Folder Path for Properties *****/
        $propertyStatusFolders =['1zMNTbdOWshFanmP1vo2da_rV1uLzMPHH'];

       foreach ($propertyStatusFolders as $propertyStatusFolder)
       {
           /***** All properties folder of status *******/
           $contents = Storage::cloud()->listContents($propertyStatusFolder, $recursive);
           $propertiesFolders = collect($contents)->where('type', '=', 'dir');
           $propertiesFolders = array_values(array_sort($propertiesFolders, function ($value) {
               return $value['name'];
           }));

           /********** Create New Folder In each Property ******/
           foreach ($propertiesFolders as $propertyFolder)
           {
               $contents = collect(Storage::cloud()->listContents($propertyFolder['path'], $recursive));
               //get app dir
               $app_parent_dir = $contents->where('type', 'dir')->where('filename', $newFolderName)->first();

               //check already exist Folder
               if ( ! $app_parent_dir) {
                   //Create Folder
                   if ( ! Storage::cloud()->makeDirectory($propertyFolder['path'].'/'.$newFolderName) ) {
                       return '';
                   }
               }
           }
           /********** Create New Folder End ******/
       }
    }

    public function downloadInvoiceszip(Request $request)
    {
        $i = array(0);
        if($request->id)
        $i = explode(',', $request->id);

        $p = PropertyInvoices::select('property_invoices.*','name_of_property')->join('properties as p', 'p.id', '=', 'property_invoices.property_id')
            ->whereIn('property_invoices.id',$i)->get();


        // pre($p);

        $zip_name = 'invoice.zip';
        // echo ; die;
        $array['path']  =array();

        foreach ($p as $key => $invoice) {
            $contents = collect(Storage::cloud()->listContents($invoice->file_dirname, false));
            $gDriveFile = $contents->where('type','file')->where('basename', $invoice->file_basename)->first();

            // pre($gDriveFile);
            if(!$gDriveFile)
                continue;
            $rawData = Storage::cloud()->get($gDriveFile['path']);
            
            $fname = $gDriveFile['name'];
            $fpath = public_path()."/ad_files_upload/".$fname;
            $array['path'][] = $fpath;
            $array['name'][] = $fname; 

            $fp = fopen($fpath, "w");
            fwrite($fp, $rawData);
            fclose($fp);

            $zip_name = $invoice->name_of_property.'.zip';
            // pre($rawData);
        }
        // pre($array);
        if(!$array['path'])
        {
            return redirect()->back();
        }

        $zipcreated = public_path()."/ad_files_upload/".$zip_name;


        $zip = new \ZipArchive;
        $zip->open($zipcreated, \ZipArchive::CREATE);
        foreach ($array['path'] as $key => $value) {
            $zip->addFile($value,'invoices/'.$array['name'][$key]);
        }
        $zip->close();

        foreach ($array['path'] as $key => $value) {
             unlink($value);;
        }

        $filename = $zipcreated;
         header('Content-Type: application/zip');
         header('Content-Disposition: attachment; filename="'.basename($filename).'"');
         header('Content-Length: ' . filesize($filename));

         flush();
         readfile($filename);
         // delete file
         unlink($filename);


    }

}
