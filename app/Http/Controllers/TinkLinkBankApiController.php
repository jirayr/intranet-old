<?php

namespace App\Http\Controllers;

use App\Services\TinkLinkApiService;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class TinkLinkBankApiController extends Controller
{
    protected $client;
    protected $accessTokenRepository;
    protected $tinkLinkService;
    public function __construct(TinkLinkApiService $tinkLinkApiService)
    {
        $this->client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),));
        $this->tinkLinkService = $tinkLinkApiService;
    }

    public function index()
    {
        return view('tink_link_bank.account');
    }


    public function getAccessToken(Request $request)
    {
        $this->tinkLinkService->getAccessToken($request->all());
        return redirect(url('/show-accounts'));

    }


    public function getAccounts()
    {
      $accounts = $this->tinkLinkService->getAccounts();
      return view('tink_link_bank.account',compact('accounts'));
    }
}
