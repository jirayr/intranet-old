<?php

namespace App\Http\Controllers;
use App\Models\Properties;
use App\Models\PropertiesBuyDetail;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use App\BankAccountUpload;
use App\Imports\BankDataImport;
use Illuminate\Http\Request;

class BankAccountUploadController extends Controller
{
    public function BankFileUpload(Request $request)
    {
        Excel::import(new BankDataImport,Input::file('bank_file'));
        return redirect('/show-accounts');
    }

    public function showBankData()
    {
        $bankData = BankAccountUpload::orderBy('updated_at','DESC')->get();
        $IbanData = PropertiesBuyDetail::where('type',1003)->get();
        foreach ($IbanData as $data)
        {
            BankAccountUpload::where('iban','like', '%' . $data->comment . '%')->update([
                'property_id'=>$data->property_id
            ]);

        }

        return view('tink_link_bank.account',compact('bankData'));

    }

    public function updatePropertyInAccount(Request $request)
    {
        BankAccountUpload::where('id',$request->id)->first()->update([
            'property_id' => $request->property_id
        ]);
    }

    public function deleteBankAccount(Request $request)
    {
        BankAccountUpload::where('id',$request->id)->first()->delete();
    }

    public function create()
    {
        return view('tink_link_bank.create');
    }

    public function store(Request $request)
    {
        BankAccountUpload::create([
            'account_designation' =>$request->account_designation,
            'iban'=> $request->iban,
            'account_owner'=>$request->account_owner,
            'date_of_booking_balance'=> $request->date_of_booking_balance,
            'account_category'=> $request->account_category,
            'book_balance'=> $request->book_balance,
            'currency'=> $request->currency
        ]);

        return redirect('/show-accounts');
    }
}
