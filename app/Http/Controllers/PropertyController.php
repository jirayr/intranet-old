<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Properties;
use App\User;
use App\Models\Banks;
use App\State;
use App\City;
use App\Models\PropertySubfields;
use App\Models\PropertiesCustomField;
use App\Models\PropertiesDirectory;
use App\Services\TenancySchedulesService;
use App\RentPaidExcelUpload;
use App\Models\PropertyMaintenance;
use App\Models\PropertyInvestation;
use App\Models\PropertyServiceProvider;
use App\Models\PropertyInsurance;
use App\email_template2_sending_info;
use App\Models\BankFinancingOffer;
use App\Models\BankingRequest;
use App\Models\Attachment;
use App\Models\PropertiesTenants;
use App\Models\PropertiesBuyDetail;
use App\Models\PropertyComments;
use App\Models\Comment;
use App\Models\LeasingActivity;
use App\Models\PropertyVacant;
use App\AddressesJob;
use App\Models\GdriveUploadFiles;
use App\Models\EmpfehlungDetail;
use App\Models\Empfehlung;
use App\Models\PropertyInsuranceTab;
use App\Models\PropertiesBuy;
use App\Models\PropertiesSale;
use App\Models\PropertiesSaleDetail;
use App\Models\PropertyLoansMirror;
use App\Models\PropertiesMailLog;
use App\Models\SendMailToAm;
use App\Services\PropertyProjectService;
use App\Models\PropertiesExtra1;

class PropertyController extends Controller
{

    protected $propertyProjectService;

    public function __construct(PropertyProjectService $propertyProjectService)
    {
        $this->propertyProjectService = $propertyProjectService;
    }

    public function show(Request $request, $id)
    {

        $active_tab = [
            'properties',
            'quick-sheet',
            'sheet',
            'budget',
            'tenancy-schedule',
            'swot-template',
            'schl-template',
            'expose',
            'bank-modal',
            'comment_tab',
            'recommended_tab',
            'provision_tab',
            'property_invoice',
            'property_insurance_tab',
            'contracts',
            'default_payer',
            'einkauf_tab',
            'vermietung',
            'verkauf_tab',
            'exportverkauf_tab',
            'Anzeige_aufgeben',
            'Anfragen',
            'Leerstandsflächen',
            'email_template',
            'finance',
            'darlehensspiegel',
            'dateien',
            'insurance_tab',
            'property_management',
            'am_mail',
            'project',
        ];

        $tab = ($request->tab && in_array($request->tab, $active_tab)) ? $request->tab : 'properties';

        if ($tab == 'properties') {
            return view('property.kalkulation', $this->kalkulation($id));
        } elseif ($tab == 'quick-sheet') {
            return view('property.quick-sheet', $this->quickSheet($id));
        } elseif ($tab == 'sheet') {
            return view('property.sheet', $this->sheet($id));
        } elseif ($tab == 'budget') {
            return view('property.budget', $this->budget($id));
        } elseif ($tab == 'tenancy-schedule') {
            return view('property.mieterliste', $this->mieterliste($id));
        } elseif ($tab == 'swot-template') {
            return view('property.swot-template', $this->swotTemplate($id));
        } elseif ($tab == 'schl-template') {
            return view('property.schl-template', $this->sclTemplate($id));
        } elseif ($tab == 'expose') {
            return view('property.expose', $this->expose($id));
        } elseif ($tab == 'bank-modal') {
            $showall = ($request->showall) ? $request->showall : 0;
            return view('property.bank-modal', $this->bankModal($id, $showall));
        } elseif ($tab == 'comment_tab') {
            return view('property.comment_tab', $this->commentTab($id));
        } elseif ($tab == 'recommended_tab') {
            return view('property.recommended_tab', $this->recommendedTab($id));
        } elseif ($tab == 'provision_tab') {
            return view('property.provision_tab', $this->provisionTab($id));
        } elseif ($tab == 'property_invoice') {
            return view('property.property_invoice', $this->propertyInvoice($id));
        } elseif ($tab == 'property_insurance_tab') {
            return view('property.property_insurance_tab', $this->propertyInsuranceTab($id));
        } elseif ($tab == 'contracts') {
            return view('property.contracts', $this->contracts($id));
        } elseif ($tab == 'default_payer') {
            return view('property.default_payer', $this->defaultPayer($id));
        } elseif ($tab == 'einkauf_tab') {
            return view('property.einkauf_tab', $this->einkaufTab($id));
        } elseif ($tab == 'vermietung') {
            return view('property.vermietung', $this->vermietung($id));
        } elseif ($tab == 'verkauf_tab') {
            return view('property.verkauf_tab', $this->verkaufTab($id));
        } elseif ($tab == 'exportverkauf_tab') {
            return view('property.exportverkauf_tab', $this->exportverkaufTab($id));
        } elseif ($tab == 'Anzeige_aufgeben') {
            return view('property.anzeige_aufgeben', $this->anzeigeAufgeben($id));
        } elseif ($tab == 'Anfragen') {
            return view('property.anfragen', $this->anfragen($id));
        } elseif ($tab == 'Leerstandsflächen') {
            return view('property.leerstandsflächen', $this->leerstandsflächen($id));
        } elseif ($tab == 'email_template') {
            return view('property.email_template', $this->emailTemplate($id));
        } elseif ($tab == 'finance') {
            return view('property.finance', $this->finance($id));
        } elseif ($tab == 'darlehensspiegel') {
            return view('property.darlehensspiegel', $this->darlehensspiegel($id));
        } elseif ($tab == 'dateien') {
            return view('property.dateien', $this->dateien($id));
        } elseif ($tab == 'insurance_tab') {
            return view('property.insurance_tab', $this->insuranceTab($id));
        } elseif ($tab == 'property_management') {
            return view('property.property_management', $this->propertyManagement($id));
        } elseif ($tab == 'am_mail') {
            return view('property.am_mail', $this->amMail($id));
        } elseif ($tab == 'project') {
            return view('property.project', $this->project($id));
        }
    }

    public function kalkulation($id)
    {
        $properties = $this->getProperty($id);
        $tr_users = User::whereRaw('role=2 or second_role=2 or id=1 or id=10')->get();
        $as_users = User::whereRaw('role=4 or second_role=4 or id=1 or id=10')->get();

        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        return ['id' => $id, 'properties' => $properties, 'tr_users' => $tr_users, 'as_users' => $as_users, 'banks' => $banks];
    }

    public function quickSheet($id)
    {
        $properties = $this->getProperty($id);
        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        $tr_users = User::whereRaw('role=2 or second_role=2 or id=1 or id=10')->get();
        $properties_pdf = DB::table('pdf')->where('property_id', $id)->get();

        return ['id' => $id, 'properties' => $properties, 'banks' => $banks, 'tr_users' => $tr_users, 'properties_pdf' => $properties_pdf];
    }

    public function sheet($id)
    {
        $properties = $this->getProperty($id);
        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);
        $bank_all = Banks::all();

        return ['id' => $id, 'properties' => $properties, 'banks' => $banks, 'bank_all' => $bank_all];
    }

    public function budget($id)
    {
        $properties = $this->getProperty($id);
        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);
        return ['id' => $id, 'properties' => $properties, 'banks' => $banks];
    }

    public function mieterliste($id)
    {
        $properties = $this->getProperty($id);

        $tenancy_schedule_data = TenancySchedulesService::get($id, 0);

        $propertiescheckd = Properties::where('main_property_id', $id)->where('Ist', '!=', 0)->first();

        $as_users = User::whereRaw('role=4 or second_role=4 or id=1 or id=10')->get();

        $rent_paid_excel = RentPaidExcelUpload::where('property_id', $id)->orderBy('id', 'desc')->first();

        $workingDir = $this->getPropertyDirectory($id);

        return ['id' => $id, 'properties' => $properties, 'tenancy_schedule_data' => $tenancy_schedule_data, 'propertiescheckd' => $propertiescheckd, 'as_users' => $as_users, 'rent_paid_excel' => $rent_paid_excel, 'workingDir' => $workingDir];
    }

    public function swotTemplate($id)
    {
        $properties = $this->getProperty($id);
        $rateSwotValArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        return ['id' => $id, 'properties' => $properties, 'rateSwotValArray' => $rateSwotValArray];
    }

    public function sclTemplate($id)
    {

        $properties = $this->getProperty($id);

        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        $tenancy_schedule_data = TenancySchedulesService::get($id, 1);

        $hvbu_users = User::whereRaw('role=' . config('auth.role.hvbu') . ' OR second_role=' . config('auth.role.hvbu'))->get();

        $hvpm_users = User::whereRaw('role=' . config('auth.role.hvpm') . ' OR second_role=' . config('auth.role.hvpm'))->get();

        $sp_users = User::whereRaw('role=' . config('auth.role.service_provider'))->get();

        $tax_consultant = User::whereRaw('user_status=1 and (role=' . config('auth.role.tax_consultant') . ')')->get();

        $maintenance = PropertyMaintenance::where('property_id', $id)->get();

        $investation = PropertyInvestation::where('property_id', $id)->get();

        $property_service_providers = PropertyServiceProvider::where('property_id', $id)->get();

        $n_array = [
            'Baden-Württemberg' => '5.0',
            'Bayern' => '3.5',
            'Berlin' => '6.0',
            'Brandenburg' => '6.5',
            'Bremen' => '5.0',
            'Hamburg' => '4.5',
            'Hessen' => '6.0',
            'Mecklenburg-Vorpommern' => '6.0',
            'Niedersachsen' => '5.0',
            'Nordrhein-Westfalen' => '6.5',
            'Rheinland-Pfalz' => '5.0',
            'Saarland' => '6.5',
            'Sachsen' => '3.5',
            'Sachsen-Anhalt' => '5.0',
            'Schleswig-Holstein' => '6.5',
            'Thüringen' => '6.5'
        ];

        $fake_bank = $this->getFakeBank();

        $schl_banks = $properties->schl_banks != null ? (array)json_decode($properties->schl_banks) : [];

        $insurances = PropertyInsurance::where('property_id', $id)->get();

        $buy_data = [];
        $b_data = PropertiesBuyDetail::where('property_id', $id)->get()->toArray();
        foreach ($b_data as $key => $value) {
            if (isset($buy_data[$value['type']])) {
            } else
                $buy_data[$value['type']] = $value;
        }

        return ['id' => $id, 'properties' => $properties, 'n_array' => $n_array, 'tenancy_schedule_data' => $tenancy_schedule_data, 'hvbu_users' => $hvbu_users, 'hvpm_users' => $hvpm_users, 'sp_users' => $sp_users, 'maintenance' => $maintenance, 'investation' => $investation, 'property_service_providers' => $property_service_providers, 'banks' => $banks, 'fake_bank' => $fake_bank, 'schl_banks' => $schl_banks, 'insurances' => $insurances, 'buy_data' => $buy_data, 'tax_consultant' => $tax_consultant];
    }

    public function expose($id)
    {
        $properties = $this->getProperty($id);
        $properties_pdf = DB::table('pdf')->where('property_id', $id)->get();

        return ['id' => $id, 'properties' => $properties, 'properties_pdf' => $properties_pdf];
    }

    public function bankModal($id, $showall = 0)
    {
        $properties = $this->getProperty($id);

        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        $sendEmails = email_template2_sending_info::where('property_id', $id)->get();

        $banksFinancingOffers = BankFinancingOffer::where('property_id', $id)->get();

        $email_template = DB::table('email_template as et')->selectRaw('et.*, u.signature')->leftjoin('users as u', 'u.id', '=', 'et.email_template_users')->where('et.property_id', $id)->first();

        $banking_info = BankingRequest::where('property_id', $id)->first();

        $tr_users = User::where('role', 2)->orWhere('second_role', 2)->get();

        $bank_popup_data = array();

        if ($showall) {
            $bank_popup_data = Banks::whereNotNull('contact_email')->where("contact_email", "!=", '')->get();
        } else {
            if (is_numeric($properties->plz_ort)) {

                $postal_code = substr($properties->plz_ort, 0, 2);
                $bank_popup_data = DB::select(" select * from banks where postal_code like '" . $postal_code . "%'  ");
                if (count($bank_popup_data) == 0) {

                    $postal_code = substr($properties->plz_ort, 0, 1);
                    $bank_popup_data = DB::select(" select * from banks where postal_code like '" . $postal_code . "%'  ");
                }
            }
        }

        $attchments = Attachment::where('property_id', $id)->get();

        $bankers_email_info = Properties::where('main_property_id', $id)->first();

        $ankdate1 = null;
        if ($bankers_email_info) {
            $propertiesExtra1s = PropertiesTenants::where('propertyId', $bankers_email_info->id)->get();
            foreach ($propertiesExtra1s as $k => $propertiesExtra1) {
                // $total_ccc += $propertiesExtra1->net_rent_p_a;
                if ($k == 0)
                    $ankdate1 = $propertiesExtra1->mv_end;
                if ($k == 1)
                    $ankdate2 = $propertiesExtra1->mv_end;
            }
        }

        $rental_period_until = null;
        if (isset($propertiesExtra1s[0]['tenant']))
            $rental_period_until = $propertiesExtra1s[0]['tenant'] . ' ';

        $properties->the_main_tenant = $rental_period_until;
        $properties->rental_period_until = $ankdate1;

        return ['id' => $id, 'properties' => $properties, 'sendEmails' => $sendEmails, 'banksFinancingOffers' => $banksFinancingOffers, 'banks' => $banks, 'email_template' => $email_template, 'banking_info' => $banking_info, 'tr_users' => $tr_users, 'bank_popup_data' => $bank_popup_data, 'attchments' => $attchments, 'bankers_email_info' => $bankers_email_info];
    }

    public function commentTab($id)
    {
        $properties = $this->getProperty($id);

        $comments = PropertyComments::where('property_id', $id)->orderBy('created_at', 'DESC')->get();
        $ccomments = Comment::where('property_id', $properties->id)->get();
        $lativity = LeasingActivity::where('property_id', $id)->where('deleted', 0)->orderBy('type', 'asc')->get();
        $propertyAddressesJobs = AddressesJob::where('property_id', $id)->orderBy('id', 'DESC')->get();

        $v_data = PropertyVacant::where('property_id', $id)->get();
        $tenant_name = [];
        if ($v_data) {
            foreach ($v_data as $item) {
                if ($item->type == config('tenancy_schedule.item_type.business_vacancy') || $item->type == config('tenancy_schedule.item_type.live_vacancy')) {
                    $tenant_name[] = $item->name;
                }
            }
        }

        return ['id' => $id, 'properties' => $properties, 'comments' => $comments, 'ccomments' => $ccomments, 'tenant_name' => $tenant_name, 'propertyAddressesJobs' => $propertyAddressesJobs, 'lativity' => $lativity];
    }

    public function recommendedTab($id)
    {

        $properties = $this->getProperty($id);

        $emp_data = Empfehlung::where('property_id', $id)->where('deleted', 0)->orderBy('name', 'asc')->get();
        $ractivity = [];
        if ($emp_data) {
            foreach ($emp_data as $key => $value) {
                $ractivity[$value->tenant_id][] = $value;
            }
        }

        $EmpfehlungDetail = EmpfehlungDetail::where('property_id', $id)->get();

        $EmpfehlungDetail_arr = $release_arr = array();//used
        if ($EmpfehlungDetail) {
            foreach ($EmpfehlungDetail as $key => $value) {
                $EmpfehlungDetail_arr[$value->tenant_id][$value->slug] = $value->value;
                $release_arr[$value->tenant_id][$value->slug] = $value->created_at;
            }
        }

        $tenancy_schedule_data = TenancySchedulesService::get($id, 1);
        $tenant_name_id = array();//used
        $rented_list = array();//used

        if (isset($tenancy_schedule_data['tenancy_schedules']) && $tenancy_schedule_data['tenancy_schedules']) {
            foreach ($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule) {
                foreach ($tenancy_schedule->items as $item) {
                    if ($item->type == config('tenancy_schedule.item_type.business_vacancy') || $item->type == config('tenancy_schedule.item_type.live_vacancy')) {
                    } else {
                        $rented_list[$item->id]['name'] = $item->name;
                        $rented_list[$item->id]['date'] = ($item->rent_begin && $item->rent_end) ? show_date_format($item->rent_begin) . ' - ' . show_date_format($item->rent_end) : '';
                    }
                }
            }
        }

        $v_data = PropertyVacant::where('property_id', $properties->id)->get();
        $tenant_name_id2 = array();//used
        $tenant_name_id['name'] = array();//used
        $tenant_name_id2['name'] = array();//used

        if ($v_data) {
            foreach ($v_data as $item) {
                if ($item->type == config('tenancy_schedule.item_type.business_vacancy') || $item->type == config('tenancy_schedule.item_type.live_vacancy')) {

                    $item->files2 = GdriveUploadFiles::where('property_id', $id)->where('parent_id', $item->id)->where('parent_type', 'empfehlung2')->get();

                    if ($item->tenant_id)
                        $tenant_name_id['name'][$item->id] = $item->name;
                    else
                        $tenant_name_id2['name'][$item->id] = $item->name;

                    $tenant_name_id['files'][$item->id] = $item->files2;

                }
            }
        }

        $workingDir = $this->getPropertyDirectory($id);

        return ['id' => $id, 'properties' => $properties, 'tenant_name_id2' => $tenant_name_id2, 'rented_list' => $rented_list, 'EmpfehlungDetail_arr' => $EmpfehlungDetail_arr, 'release_arr' => $release_arr, 'tenant_name_id' => $tenant_name_id, 'ractivity' => $ractivity, 'workingDir' => $workingDir];
    }

    public function provisionTab($id)
    {

        $login_user = Auth::user();

        $properties = $this->getProperty($id);

        $asset_email = "aaa";
        if (isset($properties->asset_manager) && isset($properties->asset_manager->name)) {
            $asset_email = $properties->asset_manager->email;
        }

        $visible_array = array(config('users.falk_email'), 'a.raudies@fcr-immobilien.de', 'u.wallisch@fcr-immobilien.de', $asset_email);
        $provision_visible_class = "hidden";

        if (in_array($login_user->email, $visible_array) || $login_user->role == "1")
            $provision_visible_class = "";

        return ['id' => $id, 'properties' => $properties, 'provision_visible_class' => $provision_visible_class];
    }

    public function propertyInvoice($id)
    {
        $properties = $this->getProperty($id);

        $workingDir = $this->getPropertyDirectory($id);

        return ['id' => $id, 'properties' => $properties, 'workingDir' => $workingDir];
    }

    public function propertyInsuranceTab($id)
    {

        $properties = $this->getProperty($id);

        $workingDir = $this->getPropertyDirectory($id);

        $insurance_tab_title = PropertyInsuranceTab::select('id', 'title', DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurance_release' AND record_id = property_insurance_tabs.id AND tab='insurance_tab') THEN 1 ELSE 0 END as release_status"))->where('property_id', $id)->where('deleted', 0)->get();

        return ['id' => $id, 'properties' => $properties, 'insurance_tab_title' => $insurance_tab_title, 'workingDir' => $workingDir];
    }

    public function contracts($id)
    {

        $properties = $this->getProperty($id);

        $workingDir = $this->getPropertyDirectory($id);

        return ['id' => $id, 'properties' => $properties, 'workingDir' => $workingDir];
    }

    public function defaultPayer($id)
    {

        $properties = $this->getProperty($id);

        $workingDir = $this->getPropertyDirectory($id);

        return ['id' => $id, 'properties' => $properties, 'workingDir' => $workingDir];
    }

    public function einkaufTab($id)
    {

        $properties = $this->getProperty($id);

        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        $properties1 = Properties::where('id', '=', $id)->where('Ist', 0)->where('soll', 0)->first();

        $propertiescheckd = Properties::where('main_property_id', $id)->where('Ist', '!=', 0)->first();

        $workingDir = $this->getPropertyDirectory($id);

        $pb_data = PropertiesBuy::where('property_id', $id)->first();

        $tr_users = User::where('role', 2)->orWhere('second_role', 2)->get();
        $as_users = User::whereRaw('role=4 or second_role=4 or id=1 or id=10')->get();

        $propertiesExtra1s = PropertiesExtra1::where('propertyId', $id)->get();

        $tenancy_schedule_data = TenancySchedulesService::get($id, 1);

        $b_data = PropertiesBuyDetail::where('property_id', $id)->get()->toArray();
        $buy_data = array();
        $buy_data1 = array();
        $buy_data2 = array();
        foreach ($b_data as $key => $value) {
            if (isset($buy_data[$value['type']])) {
            } else
                $buy_data[$value['type']] = $value;

            if (in_array($value['type'], array('btn1_request', 'btn2_request', 'btn3_request', 'btn4_request', 'btn1', 'btn2', 'btn3', 'btn4', 'btn5_request', 'btn6_request', 'btn6', 'btn5')))
                $buy_data1[] = $value;

            if (in_array($value['type'], array('vbtn1_request', 'vbtn1')))
                $buy_data2[] = $value;

        }

        return ['id' => $id, 'properties' => $properties, 'workingDir' => $workingDir, 'pb_data' => $pb_data, 'tr_users' => $tr_users, 'tenancy_schedule_data' => $tenancy_schedule_data, 'propertiesExtra1s' => $propertiesExtra1s, 'properties1' => $properties1, 'tab' => 'einkauf_tab', 'propertiescheckd' => $propertiescheckd, 'as_users' => $as_users, 'buy_data1' => $buy_data1, 'propertyEinkaufDir' => null, 'banks' => $banks, 'buy_data' => $buy_data];
    }

    public function vermietung($id)
    {

        $properties = $this->getProperty($id);

        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        $tenancy_schedule_data = TenancySchedulesService::get($id, 1);

        $ads = DB::table('ads')->where('property_id', $id)->first();

        $vads = DB::table('vads')->where('property_id', $id)->first();
        $vads_images = array();
        if ($ads) {
            $vads_images = DB::table('ads_images')->where('ads_id', $ads->id)->get()->toArray();
        }

        $vprevious_states = State::where('country_id', 82)->get();

        $pb_data = PropertiesBuy::where('property_id', $id)->first();

        $getuploaditemlist = array();
        $url = "https://vermietung.fcr-immobilien.de/getuploaditemlist/" . $id;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str1111 = curl_exec($curl);
        curl_close($curl);
        if ($str1111) {
            $getuploaditemlist = json_decode($str1111, true);
        }

        return ['id' => $id, 'properties' => $properties, 'tenancy_schedule_data' => $tenancy_schedule_data, 'vads' => $vads, 'banks' => $banks, 'vads_images' => $vads_images, 'vprevious_states' => $vprevious_states, 'pb_data' => $pb_data, 'getuploaditemlist' => $getuploaditemlist, 'ads' => $ads];
    }

    public function verkaufTab($id)
    {

        $properties = $this->getProperty($id);

        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        $ps_data = PropertiesSale::where('property_id', $id)->first();

        $s_data = PropertiesSaleDetail::where('property_id', $id)->get()->toArray();

        $sale_data = array();
        $sale_data1 = array();

        foreach ($s_data as $key => $value) {
            if (isset($sale_data[$value['type']])) {
            } else
                $sale_data[$value['type']] = $value;

            if (in_array($value['type'], array('btn1_request', 'btn2_request', 'btn3_request', 'btn4_request', 'btn1', 'btn2', 'btn3', 'btn4', 'btn5_request')))
                $sale_data1[] = $value;

        }

        $b_data = PropertiesBuyDetail::where('property_id', $id)->get()->toArray();
        $buy_data2 = array();
        foreach ($b_data as $key => $value) {
            if (in_array($value['type'], array('vbtn1_request', 'vbtn1')))
                $buy_data2[] = $value;
        }

        $propertiescheckd = Properties::where('main_property_id', $id)->where('Ist', '!=', 0)->first();
        $excelFileCellD42 = ($propertiescheckd->gesamt_in_eur) + (
                ($propertiescheckd->real_estate_taxes * $propertiescheckd->gesamt_in_eur) + ($propertiescheckd->estate_agents * $propertiescheckd->gesamt_in_eur) + (($propertiescheckd->Grundbuch * $propertiescheckd->gesamt_in_eur) / 100) + ($propertiescheckd->evaluation * $propertiescheckd->gesamt_in_eur) + ($propertiescheckd->others * $propertiescheckd->gesamt_in_eur) + ($propertiescheckd->buffer * $propertiescheckd->gesamt_in_eur)
            );
        $excelFileCellD48 = $propertiescheckd->from_bond * $excelFileCellD42;

        $tr_users = User::whereRaw('role=2 or second_role=2 or id=1 or id=10')->get();

        $verkauf_tab = DB::table('verkauf_tab')->where('property_id', $id)->orderBy('id', 'asc')->get();

        $tenancy_schedule_data = TenancySchedulesService::get($id, 1);

        $total_ccc_verkauf = 0;
        foreach ($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
            $total_ccc_verkauf = $tenancy_schedule->calculations['total_actual_net_rent'] * 12;


        return ['id' => $id, 'properties' => $properties, 'banks' => $banks, 'ps_data' => $ps_data, 'sale_data' => $sale_data, 'tr_users' => $tr_users, 'sale_data1' => $sale_data1, 'buy_data2' => $buy_data2, 'excelFileCellD42' => $excelFileCellD42, 'verkauf_tab' => $verkauf_tab, 'propertiescheckd' => $propertiescheckd, 'total_ccc_verkauf' => $total_ccc_verkauf, 'excelFileCellD48' => $excelFileCellD48, 'tenancy_schedule_data' => $tenancy_schedule_data];
    }

    public function exportverkaufTab($id)
    {

        $properties = $this->getProperty($id);

        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        $exportads = DB::table('export_ads')->where('property_id', $id)->first();
        if ($exportads) {
            $eprevious_states = State::where('country_id', $exportads->country_id)->get();
            $eprevious_cities = City::where('state_id', $exportads->state_id)->get();
        } else {
            $eprevious_states = State::where('country_id', old('country'))->get();
            $eprevious_cities = City::where('state_id', old('state'))->get();
        }

        $tenancy_schedule_data = TenancySchedulesService::get($id, 1);

        return ['id' => $id, 'properties' => $properties, 'exportads' => $exportads, 'eprevious_states' => $eprevious_states, 'eprevious_cities' => $eprevious_cities, 'banks' => $banks, 'tenancy_schedule_data' => $tenancy_schedule_data];
    }

    public function anzeigeAufgeben($id)
    {

        $properties = $this->getProperty($id);

        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        $ads = DB::table('ads')->where('property_id', $id)->first();

        $tenancy_schedule_data = TenancySchedulesService::get($id, 1);

        $previous_states = State::where('country_id', 82)->get();

        return ['id' => $id, 'properties' => $properties, 'ads' => $ads, 'banks' => $banks, 'tenancy_schedule_data' => $tenancy_schedule_data, 'previous_states' => $previous_states];
    }

    public function anfragen($id)
    {

        $properties = $this->getProperty($id);

        $contact_queries = DB::table('contact_queries')->where('property_id', $id)->get();

        return ['id' => $id, 'properties' => $properties, 'contact_queries' => $contact_queries];
    }

    public function leerstandsflächen($id)
    {
        $properties = $this->getProperty($id);

        $pb_data = PropertiesBuy::where('property_id', $id)->first();

        $tenancy_schedule_data = TenancySchedulesService::get($id, 1);

        return ['id' => $id, 'properties' => $properties, 'pb_data' => $pb_data, 'tenancy_schedule_data' => $tenancy_schedule_data];
    }

    public function emailTemplate($id)
    {

        $properties = $this->getProperty($id);

        $bank_ids = json_decode($properties->bank_ids);
        $banks = $this->getBanks($bank_ids);

        $email_template_users = User::where('role', 1)->orWhere('role', 2)->get();

        $email_template = DB::table('email_template as et')->selectRaw('et.*, u.signature')->leftjoin('users as u', 'u.id', '=', 'et.email_template_users')->where('et.property_id', $id)->first();

        $contact_person = User::where('id', $properties->transaction_m_id)->first();

        return ['id' => $id, 'properties' => $properties, 'email_template' => $email_template, 'banks' => $banks, 'email_template_users' => $email_template_users, 'contact_person' => $contact_person];
    }

    public function finance($id)
    {

        $properties = $this->getProperty($id);

        $loansMirrorData = PropertyLoansMirror::where('property_id', $id)->first();

        $tenancy_schedule_data = TenancySchedulesService::get($id, 0);

        $custom_fields_schl = PropertiesCustomField::where('property_id', $id)->get();
        $custom_fields_schl_array = array();
        foreach ($custom_fields_schl as $key => $value) {
            $custom_fields_schl_array[$value->slug] = $value->content;
        }

        return ['id' => $id, 'properties' => $properties, 'loansMirrorData' => $loansMirrorData, 'tenancy_schedule_data' => $tenancy_schedule_data, 'custom_fields_schl_array' => $custom_fields_schl_array];
    }

    public function darlehensspiegel($id)
    {

        $properties = $this->getProperty($id);

        $loansMirrorData = PropertyLoansMirror::where('property_id', $id)->first();

        return ['id' => $id, 'properties' => $properties, 'loansMirrorData' => $loansMirrorData];
    }

    public function dateien($id)
    {
        $properties = $this->getProperty($id);

        return ['id' => $id, 'properties' => $properties];
    }

    public function insuranceTab($id)
    {

        $properties = $this->getProperty($id);

        $mails_logs = PropertiesMailLog::where('property_id', $id)->get()->toArray();
        $mails_log_data = array();
        foreach ($mails_logs as $key => $value) {
            if (isset($mails_log_data[$value['type']])) {
            } else
                $mails_log_data[$value['type']] = $value;
        }

        $insurances = PropertyInsurance::where('property_id', $id)->get();

        return ['id' => $id, 'properties' => $properties, 'mails_logs' => $mails_logs, 'mails_log_data' => $mails_log_data, 'insurances' => $insurances];
    }

    public function propertyManagement($id)
    {

        $properties = $this->getProperty($id);

        $mails_logs = PropertiesMailLog::where('property_id', $id)->get()->toArray();
        $mails_log_data = array();
        foreach ($mails_logs as $key => $value) {
            if (isset($mails_log_data[$value['type']])) {
            } else
                $mails_log_data[$value['type']] = $value;
        }

        return ['id' => $id, 'properties' => $properties, 'mails_logs' => $mails_logs, 'mails_log_data' => $mails_log_data];
    }

    public function amMail($id)
    {

        $properties = $this->getProperty($id);

        $assetsManagerMails = SendMailToAm::join('users', 'users.id', 'send_mail_to_ams.user_id')
            ->where('send_mail_to_ams.property_id', $id)
            ->select('send_mail_to_ams.id', 'send_mail_to_ams.subject', 'send_mail_to_ams.message', 'send_mail_to_ams.created_at', 'send_mail_to_ams.type', 'users.name', 'users.email')
            ->orderBy('send_mail_to_ams.id', 'desc')
            ->get();

        return ['id' => $id, 'properties' => $properties, 'assetsManagerMails' => $assetsManagerMails];
    }

    public function project($id)
    {

        $properties = $this->getProperty($id);

        $propertyProjects = $this->propertyProjectService->getByPropertyId($id);

        return ['id' => $id, 'properties' => $properties, 'propertyProjects' => $propertyProjects];
    }

    public function getProperty($id)
    {
        return Properties::where('id', '=', $id)->where('Ist', 0)->where('soll', 0)->first();
    }

    public function getBanks($bank_ids = null)
    {
        $banks = [];

        if ($bank_ids != null) {
            $banks = Banks::whereIn('id', $bank_ids)->get();
        } else {
            $banks[] = $this->getFakeBank();
        }

        return $banks;
    }

    public function getFakeBank()
    {
        $fake_bank = new Banks();
        $fake_bank->name = '';
        $fake_bank->user_id = 0;
        $fake_bank->with_real_ek = 0;
        $fake_bank->from_bond = 0;
        $fake_bank->bank_loan = 0;
        $fake_bank->interest_bank_loan = 0;
        $fake_bank->eradication_bank = 0;
        $fake_bank->eradication_bank = 0;
        $fake_bank->interest_bond = 0;
        return $fake_bank;
    }

    public function getPropertyDirectory($id)
    {
        $workingDir = '/';
        $p_dir = PropertiesDirectory::select('dir_path')->where('property_id', $id)->where('parent_id', 0)->first();
        if ($p_dir) {
            $workingDir = $p_dir->dir_path;
        }
        return $workingDir;
    }

    public function property_upload_pdf(Request $request)
    {

        $attributeNames = array(
            'upload_pdf' => 'PDF',
        );

        $validator = Validator::make($request->all(), [
            'upload_pdf' => 'required|mimes:pdf|max:200096',
        ]);

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->errors()->first());
        }
        $propertyMainDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', 0)->first();
        if (!$propertyMainDir) {
            return redirect()->back()->with('error', 'Property directory not created in google Drive.');
        }
        $propertyAnhangeDir = PropertiesDirectory::where('property_id', $request->property_id)->where('parent_id', $propertyMainDir->id)->where('dir_name', 'LIKE', 'Anhange')->first();
        if (!$propertyAnhangeDir) {
            return redirect()->back()->with('error', 'Property Anhänge directory not created in google Drive.');
        }

        $file = $request->file('upload_pdf');
        $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $originalExt = $file->getClientOriginalExtension();
        $folder = $propertyAnhangeDir->dir_path;
        $fileData = File::get($file);

        //upload file
        $file2 = Storage::cloud()->put($folder . '/' . $originalName . '.' . $originalExt, $fileData);

        //get google drive id
        $contents = collect(Storage::cloud()->listContents($folder, false));

        $file = $contents->where('type', 'file')->where('filename', $originalName)->where('extension', $originalExt)->first();

        $data_array = ([
            'user_id' => Auth::id(),
            'property_id' => $request->property_id,
            'upload_file_name' => $request->file('upload_pdf')->getClientOriginalName(),
            'file_name' => $file['name'],
            'file_basename' => $file['basename'],
            'file_path' => $file['path'],
            'file_href' => 'https://drive.google.com/file/d/' . $file['basename'],
            'extension' => $file['extension']
        ]);

        $insert_data = DB::table('pdf')->insert($data_array);

        return redirect('property/' . $request->property_id . '?tab=quick-sheet')->with('message_expose', 'Upload File Successfully');
    }

    public function saveGSheet(Request $request)
    {

        $value1 = $value = $value2 = $value3 = $Rental_area_in_m2 = 0;
        if ($request->gesamt_in_eur) {
            $value = str_replace('.', '', $request->gesamt_in_eur);
            $value = str_replace(',', '.', $value);
            $value = str_replace('%', '', $value);
            $value = str_replace('€', '', $value);
        }
        if ($request->Rental_area_in_m2) {
            $Rental_area_in_m2 = str_replace('.', '', $request->Rental_area_in_m2);
            $Rental_area_in_m2 = str_replace(',', '.', $Rental_area_in_m2);
        }
        if ($request->maklerpreis) {
            $value3 = str_replace('.', '', $request->maklerpreis);
            $value3 = str_replace(',', '.', $value3);
            $value3 = str_replace('%', '', $value3);
            $value3 = str_replace('€', '', $value3);
        }
        if ($request->net_rent_pa) {
            $value2 = str_replace('.', '', $request->net_rent_pa);
            $value2 = str_replace(',', '.', $value2);
            $value2 = str_replace('%', '', $value2);
            $value2 = str_replace('€', '', $value2);
        }
        if ($request->real_estate_taxes) {
            $value1 = str_replace('.', '', $request->real_estate_taxes);
            $value1 = str_replace(',', '.', $value1);
        }

        $properties_subfields = PropertySubfields::where('property_id', $request->main_property_id)->first();
        if (!$properties_subfields) {
            $properties_subfields = new PropertySubfields();
            $properties_subfields->property_id = $request->main_property_id;
            $properties_subfields->is_new_directory_strucure = 0;
        }
        $properties_subfields->location_latitude = $request->location_latitude;
        $properties_subfields->location_longitude = $request->location_longitude;
        $properties_subfields->save();


        $update_data1 = DB::table('properties')->whereRaw('(main_property_id=' . $request->main_property_id . ' OR id=' . $request->main_property_id . ') and lock_status=0')
            ->update([
                "name_of_property" => $request->name_of_property,
                "plz_ort" => $request->plz_ort,
                "ort" => $request->ort,
                "strasse" => $request->strasse,
                "hausnummer" => $request->hausnummer,
                "niedersachsen" => $request->niedersachsen,
                "anbieterkontakt" => $request->anbieterkontakt,
                "firma" => $request->firma,
                "first_name" => $request->first_name,
                "net_rent_pa" => $value2,
                "maklerpreis" => $value3,
                "transaction_m_id" => $request->transaction_m_id,
                "investor_name" => $request->investor_name,
                "property_type" => $request->property_type,
                "notizen" => $request->notizen,
                "offer_from" => $request->offer_from,
                "Rental_area_in_m2" => $Rental_area_in_m2,
                "real_estate_taxes" => $value1 / 100,
            ]);

        $update_data1 = DB::table('properties')->whereRaw('Ist!=0 and lock_status=0 and  (main_property_id=' . $request->main_property_id . ' OR id=' . $request->main_property_id . ')')
            ->update([
                "gesamt_in_eur" => $value,
            ]);

        $s = 0;
        if ($request->niedersachsen) {
            $state = State::where('state_name', $request->niedersachsen)->first();
            if ($state)
                $s = $state->id;

        }


        $update_data = DB::table('vads')->whereRaw('property_id=' . $request->main_property_id)
            ->update([
                "type" => $request->property_type,
                "state_id" => $s
            ]);
        $update_data = DB::table('ads')->whereRaw('property_id=' . $request->main_property_id)
            ->update([
                "type" => $request->property_type,
                "state_id" => $s
            ]);
        $update_data = DB::table('export_ads')->whereRaw('property_id=' . $request->main_property_id)
            ->update([
                "type" => $request->property_type,
                "state_id" => $s
            ]);
        return redirect('property/' . $request->main_property_id . '/?tab=quick-sheet');
    }


    public function save_planning_data(Request $request)
    {

        if (!isset($_POST['not_consider_in_liquid']))
            $_POST['not_consider_in_liquid'] = 0;

        // pre($_POST);
        foreach ($_POST as $column => $value) {

            if ($column == "average_cost_pm" || $column == "extra_cost_pm" || $column == "current_balance" || $column == 'object_effort_pm' || $column == 'effort_pm' || $column == 'ao_cost_month1' || $column == 'ao_cost_month2' || $column == "not_consider_in_liquid" || $column == "share_deposite") {

                $check = PropertiesCustomField::where('property_id', $request->property_id)
                    ->where('slug', $column)
                    ->first();

                $value = str_replace('.', '', $value);
                $value = str_replace(',', '.', $value);
                $value = str_replace('%', '', $value);
                $value = str_replace('€', '', $value);

                if ($check)
                    $modal = $check;
                else
                    $modal = new PropertiesCustomField;

                if ($column == "not_consider_in_liquid") {
                    $v1 = 0;
                    if (isset($_POST['not_consider_in_liquid']) && $_POST['not_consider_in_liquid'])
                        $v1 = 1;

                    $value = $v1;
                }

                $modal->property_id = $request->property_id;
                $modal->slug = $column;
                $modal->content = $value;
                $modal->save();
            }
        }
        return redirect('properties/' . $request->property_id . '/?tab=finance');
    }

}
