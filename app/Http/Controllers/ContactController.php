<?php

namespace App\Http\Controllers;

use App\Models\Properties;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Models\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $str = "1=1";
        if(isset($_REQUEST['q']) && $_REQUEST['q']){
            $q = trim($_REQUEST['q']);
            $str = "Firma like '%".$q."%' or Vorname like '%".$q."%' or Nachname like '%".$q."%'";
        }
        
    	$perPages = 25;
    	$contacts = Contact::orderBy('id','desc')->whereRaw($str)->paginate($perPages);
        return view('contacts.index',compact('contacts'));
    }
    public function view($id) {
        $contact = Contact::where('id', '=', $id)->first();
        $type = 'view';
        if( empty( $contact ) )
            return redirect('contacts');
        return view( 'contacts.view', compact( 'type', 'contact' ) );
    }
    public function detail() {
        $id = $_REQUEST['id'];
        $contact = Contact::where('id', '=', $id)->first()->toArray();
        return $contact;
    }
    public function delete(Request $request)
    {   
        $input = $request->all();
        $bank_id = isset( $input['contact_id'] ) ? $input['contact_id'] : 0;
        $bank = Contact::where('id', '=', $input['contact_id'])->delete();
        return redirect('contacts');
    }

    public function addNewContact(Request $request)
    {
        $arr['status']=0;
        if(!$request->Firma)
        {
          $arr['message']= "Please enter Firma";
          return $arr;
        }
        
        if(!$request->Vorname)
        {
          $arr['message']= "Please enter Vorname";
          return $arr;
        }
        if(!$request->Nachname)
        {
          $arr['message']= "Please enter Nachname";
          return $arr;
        }




        DB::beginTransaction();
        $user_id        =  Auth::id();
        try {

            $data['Firma'] = $request->Firma;
            $data['Anrede'] = $request->Anrede;
            $data['Vorname'] = $request->Vorname;
            $data['Nachname'] = $request->Nachname;
            $data['Strasse'] = $request->Strasse;
            $data['PLZ'] = $request->PLZ;
            $data['Ort'] = $request->Ort;
            $data['Ortsteil'] = $request->Ortsteil;
            $data['Telefon'] = $request->Telefon;
            $data['Fax'] = $request->Fax;
            $data['E_Mail'] = $request->E_Mail;
            $data['Internet'] = $request->Internet;
            $data['Notiz'] = $request->Notiz;
            $data['Stichwort'] = $request->Stichwort;
            $data['Stichwort2'] = $request->Stichwort2;

            if($request->id)
            {
                $exportad = Contact::where('id',$request->id)->first();
                $exportad->Firma = $request->Firma;
                $exportad->Anrede = $request->Anrede;
                $exportad->Vorname = $request->Vorname;
                $exportad->Nachname = $request->Nachname;
                $exportad->Strasse = $request->Strasse;
                $exportad->PLZ = $request->PLZ;
                $exportad->Ort = $request->Ort;
                $exportad->Ortsteil = $request->Ortsteil;
                $exportad->Telefon = $request->Telefon;
                $exportad->Fax = $request->Fax;
                $exportad->E_Mail = $request->E_Mail;
                $exportad->Internet = $request->Internet;
                $exportad->Notiz = $request->Notiz;
                $exportad->Stichwort = $request->Stichwort;
                $exportad->Stichwort2 = $request->Stichwort2;
                $exportad->save();
                // print_r($exportad); die;
                // $updated_ad = $exportad->update($data);
                $arr['message']= "updated successfully";

            }
            else{
                $created_ad = DB::table('contacts')->insertGetId($data);
                $arr['message']= "added successfully";
            }

            DB::commit();
            $arr['status']=1;
            

            return $arr;
        } catch (\Exception $e) {

            DB::rollback();
            // print_r($e); die;
            // $arr['all']= $e;
            $arr['message']= $e->getMessage();
            return $arr;
        }

    }
    
}
