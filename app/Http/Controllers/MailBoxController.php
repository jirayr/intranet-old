<?php

namespace App\Http\Controllers;

use App\EmailAccount;
use App\Models\Email;
use App\Models\EmailMapping;
use App\Models\EmailType;
use App\Services\EmailMappingService;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Webklex\IMAP\Client;
ini_set('memory_limit','-1');

class MailBoxController extends Controller
{
    protected $email;
    protected $emailType;
    protected $emailMappingService;
    protected $emailAccount;

    public function __construct(Email $email, EmailMappingService $emailMappingService, EmailType $emailType, EmailAccount $emailAccount)
    {
        $this->email = $email;
        $this->emailMappingService = $emailMappingService;
        $this->emailType = $emailType;
        $this->emailAccount = $emailAccount;

    }

    public function index()
    {
        return view('mailbox.index');

    }
//    public function getReceivedEmails(Request $request)
//    {
//        if (isset($request->password) && isset($request->email)){
//            $request->session()->put('email', $request->email);
//            $request->session()->put('password', $request->password);
//
//        }
//        Log::info($request->session()->get('email'));
//        $oClient = new Client([
//            'host'          => 'imap.fcr-immobilien.de',
//            'port'          => 993,
//            'encryption'    => 'ssl',
//            'validate_cert' => false,
//            'username'      =>  $request->session()->get('email'),
//            'password'      =>  $request->session()->get('password'),
//            'protocol'      => 'imap'
//        ]);
//
//        $oClient->connect();
//        $aFolder = $oClient->getFolder('INBOX');
//
//
//       $messages = $aFolder->query()->get()->paginate(5) ;
//       $sentEmails = $this->getSentEmails($request);
//        dd($sentEmails);
//        return view('mailbox.index',compact('messages','sentEmails'));
//
//    }
//
//
//
//    public function getSentEmails($request)
//    {
//        $oClient = new Client([
//            'host'          => 'imap.fcr-immobilien.de',
//            'port'          => 993,
//            'encryption'    => 'ssl',
//            'validate_cert' => false,
//            'username'      =>  $request->session()->get('email'),
//            'password'      =>  $request->session()->get('password'),
//            'protocol'      => 'imap'
//        ]);
//
//        $oClient->connect();
//        $aFolder = $oClient->getFolder('Inbox.Sent');
//       return $sentEmails = $aFolder->query()->get()->paginate(5);
//
//    }

    public function testView()
    {
        return view('mailbox.main');

    }


    public function syncEmails()
    {
        $accounts = EmailAccount::all();
        foreach ($accounts as $account)
        {
            $emailType = $this->emailType->all();
            foreach ($emailType as $type)
            {
                $this->storeEmail($type,$account);
            }
        }
    }

     public function storeEmail($type,$account)
     {
         $client = new Client([
             'host'          => 'imap.fcr-immobilien.de',
             'port'          => 993,
             'encryption'    => 'ssl',
             'validate_cert' => false,
             'username'      => $account->email,
             'password'      => $account->password,
             'protocol'      => 'imap'
         ]);
         $client->connect();
         $mailBoxFolder = $client->getFolder($type->type);
         $messages = $mailBoxFolder->query()->setFetchAttachment(false)->get();

         try {

             foreach ($messages as $message)
             {
                 $ids =[];

                 $createdEmail = $this->email->updateOrCreate(
                     ['message_id' => $message->message_id,],
                     ['message_id' => $message->message_id,
                         'subject' => $message->subject,
                         'body' => isset($message->bodies['text'])?$message->bodies['text']->content:'',
                         'bcc' => '',
                         'cc' => '',
                         'email_sent_from' => $message->sender[0]->mail,
                         'date_of_email' => strval($message->date)]);

                 $ids = ['email_id' => $createdEmail->id,'account_id'=> $account->id,'email_type_id'=>$type->id];
                 $this->storeEmailMapping($ids);

             }

         } catch (QueryException $e) {
             $errorCode = $e->errorInfo[1];
             if($errorCode == 1062){
                 return 'Duplicate Entry';
             }
         }


     }


     public function storeEmailMapping($ids)
     {
         EmailMapping::updateOrCreate(
             ['email_id'=>$ids['email_id']],
             ['email_id' => $ids['email_id'],
              'account_id'=> $ids['account_id'],
              'email_type_id'=>$ids['email_type_id']
             ]);
     }


     public function getEmailsByAccountAndEmailType($accountId,$emailTypeId)
     {
       return $this->emailMappingService->getEmailsByAccountAndEmailType($accountId,$emailTypeId);
     }

     public function getAllEmails($emailTypeId)
     {
       return $this->emailMappingService->getAllEmails($emailTypeId);
     }

     public function emailSearch(Request $request)
     {
         return $this->emailMappingService->emailSearch($request->all());
     }


    public function validateAccount(Request $request)
    {
        $account = $this->emailAccount->where('email','LIKE','%'.$request->email.'%')->first();
        if(($account && $account->id == $request->id) || !$account )
        {
            try {
                $oClient = new Client([
                    'host'          => 'imap.fcr-immobilien.de',
                    'port'          => 993,
                    'encryption'    => 'ssl',
                    'validate_cert' => false,
                    'username'      =>  $request->email,
                    'password'      =>  $request->password,
                    'protocol'      => 'imap'
                ]);

                $oClient->connect();

                return  json_encode('connected');
            } catch (\Exception $exception) {
                return  json_encode('Connection Failed ! Check Username or Password');
            }
        }

        return json_encode('Account already exists with this email!');

    }



}
