<?php

namespace App\Http\Controllers;
use App\Models\TenancyScheduleItemFile;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use App\Models\PropertiesDirectory;
use App\Models\Directory_Permission;
use App\Models\Properties;
use App\Helpers\PropertiesHelperClass;
use Illuminate\Support\Facades\Log;
use Validator;
use Mail;



use App\Models\PropertyEinkaufFile;
use App\Models\GdriveUploadFiles;
use DB;



class FileManagerController extends Controller
{
    //
	public function index(Request $request, $propertyId ){

        Log::useDailyFiles(storage_path().'/logs/Gdrive_'.$propertyId.'.log');
        Log::info('///////************'.date("Y-m-d H:i:s").'************///////');
        Log::info('File manager Controller /n/n/n');
        $chield_dir = $request->route('chield_dir');

        $property = Properties::find($propertyId);
        if(!$property){
            die('Property does not exist.');
        }
        if($chield_dir){
            $p_dir = PropertiesDirectory::where('property_id', $propertyId)->where('dir_name', $chield_dir)->first();
            if($p_dir){
                $working_dir = $p_dir->dir_path;
                return view('file-manager.index', compact('propertyId', 'working_dir'));
            }else{
                $propertyMainDir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
                $res = PropertiesHelperClass::createPropertySubDirectories($propertyMainDir, $propertyId, [$chield_dir]);
                if(!$res['status']){
                    die('Property requested directory not exist.'  );
                }
                $p_dir = PropertiesDirectory::where('property_id', $propertyId)->where('dir_name', $chield_dir)->first();
                if($p_dir){
                    $working_dir = $p_dir->dir_path;
                    return view('file-manager.index', compact('propertyId', 'working_dir'));
                }
                else{
                    die('Property requested directory not exist.'  );
                }
            }

        }

        // dd($property);
        $propertyStatusArr = config('properties.status_with_drive');
        // if($property->property_status == 1){
        //     $property->status = 18;
        // }
        if(!$property->status){
            $propertyStatusArr = $propertyStatusArr[15];
        }else{
            $propertyStatusArr = $propertyStatusArr[$property->status];
        }
        if($propertyStatusArr[1] == "")
        {
            die('Property status directory not exist.'. $propertyStatusArr[0] );
        }
        $root_dir = $propertyStatusArr[1];

        $p_dir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
        if(!$p_dir){
            $new_folder_name = PropertiesHelperClass::getPropertyDirectoryName($property);

            if ( ! $newDir = Storage::cloud()->makeDirectory($root_dir.'/'.$new_folder_name) ) {
                Log::info('File manager Controller: create property directory named('.$new_folder_name.') /n');
                die('directory create still remain');
            }
            $contents = collect(Storage::cloud()->listContents($root_dir, false));
            $new_property_dir = $contents->where('type', 'dir')->where('name', $new_folder_name)->sortByDesc('timestamp')->values()->first();
            // sortByDesc because there can multiple folder with same name so we get latest.


            PropertiesDirectory::create([
                'property_id'   => $propertyId,
                'parent_id'   => 0,
                'dir_name'      => $new_folder_name,
                'dir_basename'  => $new_property_dir['basename'],
                'dir_path'      => $new_property_dir['path']
            ]);
            $p_dir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
        }else{
            if(is_null($p_dir->dir_path)) {
                die('directory create still remain');
            }
            $contents = collect(Storage::cloud()->listContents($root_dir, false));
            $propertyDir = $contents->where('type', 'dir')->where('basename', $p_dir->dir_basename)->first();
            if(!$propertyDir){
                $dirAsStatus = PropertiesHelperClass::changePropertyDirAsStatus($propertyId, $property->status);
				if (!$dirAsStatus) {
                    die('Directory does not exist.');
				}
                Log::info('File manager Controller: Property#'.$propertyId.' directory moved as new status('.$property->status.'). /n');
                $p_dir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
            }
            // check new name concept match with current directory name.
            $new_folder_name = PropertiesHelperClass::getPropertyDirectoryName($property);
            if($p_dir->dir_name != $new_folder_name){
                $temp_root_dir = explode('/', $p_dir->dir_path);
                array_pop($temp_root_dir);
                $temp_root_dir = implode('/', $temp_root_dir);
                Storage::cloud()->move($p_dir->dir_path, $temp_root_dir.'/'.$new_folder_name);
                $p_dir->dir_name = $new_folder_name;
                $p_dir->save();
                Log::info('File manager Controller: Property#'.$propertyId.' change directory name to ('.$new_folder_name.'). /n');
            }

        }
        PropertiesHelperClass::createPropertySubDirectories($p_dir, $propertyId);

        return view('file-manager.index', compact('propertyId'));
    }
    public function externUserDrive(Request $request){

    }
    public function listDirSidebar(Request $request, $propertyId ){
        $dir = '/';
        $working_dir = $request->working_dir;
        if($working_dir && $working_dir != ""){
            $dir = $working_dir;
        }else{
            $p_dir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
            if(!$p_dir){
                return false;
            }
            $dir = $p_dir->dir_path;
        }
        $recursive = false; // Get subdirectories also?
        $contents = collect(Storage::cloud()->listContents($dir, $recursive))->where('type', '=', 'dir');
        $contents = array_values(array_sort($contents, function ($value) {
			return $value['name'];
        }));
        $user = Auth::user();
        $permission = Directory_Permission::where('user_id', $user->id)->get();

        return view('file-manager.tree', compact('contents', 'user', 'permission'));
    }
    public function upload(Request $request){
        $driverId = "";
        if ($request->hasFile('upload')) {
            $file = $request->file('upload');
            $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $originalExt = $file->getClientOriginalExtension();
            $uniqueName = $originalName;
            $foldername = $request->working_dir;
            $driverId = $this->uploadToGoogleDrive($uniqueName,$file,$foldername,$originalExt);
        }
        if($driverId == ''){
            $response = [
                'success' => false,
                'msg' => 'Fail upload fail.'
            ];
        }else{
            $response = [
                'success' => true,
                'msg' => 'File upload succesfully.',
                'file_id' => $driverId
            ];
        }
        return response()->json($response);
    }

    public function uploadToGoogleDrive($unique_name, $file_data, $upload_folder, $extension)
    {
        $recursive = false;
        $upload_file_name = $unique_name;
        $fileData = File::get($file_data);
        //upload file
        $file2 =  Storage::cloud()->put($upload_folder.'/'.$upload_file_name.'.'.$extension, $fileData);

        //get google drive id
        $contents = collect(Storage::cloud()->listContents($upload_folder, $recursive));

        $file = $contents ->where('type','file')->where('filename', $upload_file_name)->where('extension', $extension) ->first();

        $google_drive_id = $file["basename"];
        //return id if file upload
        return $google_drive_id;
    }

    public function newfolder(Request $request){
        $root_dir = $request->working_dir;
        $new_folder_name = $request->name;
        $recursive = false;
        $contents = collect(Storage::cloud()->listContents($root_dir, $recursive));
        //get app dir
        $app_parent_dir = $contents->where('type', 'dir')->where('filename', $new_folder_name)->first();

        //check already exist Folder
        if ( ! $app_parent_dir) {
            //Create Folder
            if ( ! Storage::cloud()->makeDirectory($root_dir.'/'.$new_folder_name) ) {
                return '';
            }
            //get ipaz dir
            $contents = collect(Storage::cloud()->listContents($root_dir, $recursive));
            $app_parent_dir = $contents->where('type', 'dir')->where('filename', $new_folder_name)->first();
        }

        if($app_parent_dir == ''){
            $response ='Folder create fail.';
        }else{
            $response = 'Folder create succesfully.';
        }
        return response()->json($response);
    }

    public function listForSelectGdriveFolder(Request $request){
        $dir = '/';
        if($request->has('working_dir') && $request->working_dir != ""){
            $dir = $request->working_dir;
        }
        $recursive = false; // Get subdirectories also?
        $contents = Storage::cloud()->listContents($dir, $recursive);
        $contents = array_values(array_sort($contents, function ($value) {
			return $value['name'];
        }));
        $user = Auth::user();
        $permission = Directory_Permission::where('user_id', $user->id)->get();
        return [
            'html' => (string) view('file-manager.list-for-select-gdrive-folder', compact('contents', 'user', 'permission')),
            'working_dir' => $dir,
        ];
    }
    public function listForSelectGdriveFile(Request $request){
        $dir = '/';
        if($request->has('working_dir') && $request->working_dir != ""){
            $dir = $request->working_dir;
        }
        $recursive = false; // Get subdirectories also?
        $contents = Storage::cloud()->listContents($dir, $recursive);
        $contents = array_values(array_sort($contents, function ($value) {
			return $value['timestamp'];
        }));
        $contents = array_reverse($contents);

        /***** If we need to access active tab folder of property ****/
        if ($request->has('current_active_tab') && $request->current_active_tab != "")
        {
            $contents=$this->selectTheFolderOfActiveTabOfProperty($contents, $request->current_active_tab);
        }

        $user = Auth::user();
        $permission = Directory_Permission::where('user_id', $user->id)->get();
        return [
            'html' => (string) view('file-manager.list-for-select-gdrive-file', compact('contents', 'user', 'permission')),
            'working_dir' => $dir,
        ];
    }
    public function listForSelect(Request $request, $propertyId){
        $dir = '/';
        if($request->has('working_dir') && $request->working_dir != ""){
            $dir = $request->working_dir;
        }else{
            $p_dir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
            if(!$p_dir){
                return false;
            }
            $dir = $p_dir->dir_path;
        }
        $recursive = false; // Get subdirectories also?
        $contents = Storage::cloud()->listContents($dir, $recursive);
        $contents = array_values(array_sort($contents, function ($value) {
			return $value['name'];
        }));
        $user = Auth::user();
        $permission = Directory_Permission::where('user_id', $user->id)->get();
        return [
            'html' => (string) view('file-manager.select-view', compact('contents', 'user', 'permission')),
            'working_dir' => $dir,
        ];
    }
    public function listMain(Request $request, $propertyId ){
        $dir = '/';
        if($request->has('working_dir') && $request->working_dir != ""){
            $dir = $request->working_dir;
        }else{
            $p_dir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
            if(!$p_dir){
                return false;
            }
            $dir = $p_dir->dir_path;
        }
        $recursive = false; // Get subdirectories also?
        $contents = Storage::cloud()->listContents($dir, $recursive);
        if ($request->has('sort_type') && $request->sort_type == 'alphabetic' && $request->order == 'asc') {

			$contents = array_values(array_sort($contents, function ($value) {
				return $value['name'];
			}));
		}

		if ($request->has('sort_type') && $request->sort_type == 'alphabetic' && $request->order == 'desc') {
			$contents = array_values(array_reverse(array_sort($contents, function ($value) {
				return $value['name'];
			})));
        }
        $user = Auth::user();
        $permission = Directory_Permission::where('user_id', $user->id)->get();
        $allUsers = User::all();

        if($request->has('show_list') && $request->show_list == 0){
            return [
                'html' => (string) view('file-manager.grid-view', compact('contents', 'user', 'permission', 'allUsers')),
                'working_dir' => $dir,
            ];
        }
        return [
            'html' => (string) view('file-manager.list-view', compact('contents', 'user', 'permission', 'allUsers')),
            'working_dir' => $dir,
        ];
    }

    public function download(Request $request, $propertyId ){
        if($request->has('file') && $request->file != ""){
            $parentDir = "/";
            if($request->has('file_dir') && $request->file_dir != ""){
                $parentDir = $request->file_dir;
            }

                $contents = collect(Storage::cloud()->listContents($parentDir, false));
                $file = $contents->where('basename', '=', $request->file )->first();
                $rawData = Storage::cloud()->get($file['path']);
                $filename = $file['name'];
                return response($rawData, 200)
                    ->header('ContentType', $file['mimetype'])
                    ->header('Content-Disposition', "attachment; filename=$filename");
        }
        else{
            return false;
        }

    }
    public function deleteDir(Request $request){
        if($request->has('delete_path')){
            Storage::cloud()->delete($request->delete_path);
            $response = [
                'success' => true,
                'msg' => 'Directory delete successfully.'
            ];
        }else{
            $response = [
                'success' => flase,
                'msg' => 'Directory delete fail.'
            ];
        }
    }
    public function deleteFile(Request $request){
        if($request->has('delete_path')){
            Storage::cloud()->delete($request->delete_path);
            $response = [
                'success' => true,
                'msg' => 'File delete successfully.'
            ];
        }else{
            $response = [
                'success' => flase,
                'msg' => 'File delete fail.'
            ];
        }
    }
    public function renameFile(Request $request){
        if($request->has('path') && $request->has('dirname') && $request->has('new_name') && $request->has('extension')){
            Storage::cloud()->move($request->path, $request->dirname.'/'.$request->new_name.".".$request->extension );
            $response = [
                'success' => true,
                'msg' => 'File rename successfully.'
            ];
        }else{
            $response = [
                'success' => flase,
                'msg' => 'File rename fail.'
            ];
        }
    }
    public function renameDir(Request $request){
        if($request->has('path') && $request->has('dirname') && $request->has('new_name')){
            Storage::cloud()->move($request->path, $request->dirname.'/'.$request->new_name );
            $response = [
                'success' => true,
                'msg' => 'Directory rename successfully.'
            ];
        }else{
            $response = [
                'success' => flase,
                'msg' => 'Directory rename fail.'
            ];
        }
    }
    public function showFile(Request $request, $fileId){
        $url = 'https://drive.google.com/file/d/'.$fileId.'/preview';
        return view('file-manager.show-file', compact('url'));
    }

    public function listFilesForeDelete(Request $request)
	{
        if($request->type == "einkauf"){
            $files = PropertyEinkaufFile::where('property_id', $request->property_id)->where('property_buy_detail_type', $request->id)->get()->toArray();

            foreach($files as &$value){
                $value['file_type'] = $value['type'];
            }

            $objData = [
                'property_id' => $request->property_id,
                'parent_id' => $request->id,
                'type' => "einkauf",
                '_token' => csrf_token()
            ];
            // return view('file-manager.delete-file', compact('files'));
            return [
                'html' => (string) view('file-manager.delete-file', compact('files', 'objData')),
                'status' => true
            ];
        }else if($request->type == "empfehlung"){
            $files = GdriveUploadFiles::where('property_id', $request->property_id)->where('parent_id', $request->id)->where('parent_type', "empfehlung")->get()->toArray();
            $objData = [
                'property_id' => $request->property_id,
                'parent_id' => $request->id,
                'type' => "empfehlung",
                '_token' => csrf_token()
            ];
            // return view('file-manager.delete-file', compact('files'));
            return [
                'html' => (string) view('file-manager.delete-file', compact('files', 'objData')),
                'status' => true
            ];
        }else if($request->type == "mieterliste"){
            $files = TenancyScheduleItemFile::where('tenancy_schedule_item_id',  $request->id)->get()->toArray();
            foreach($files as &$value){
                $value['file_type'] = $value['type'];
            }

            $objData = [
                'property_id' => $request->property_id,
                'parent_id' => $request->id,
                'type' => "mieterliste",
                '_token' => csrf_token()
            ];
            // return view('file-manager.delete-file', compact('files'));
            return [
                'html' => (string) view('file-manager.delete-file', compact('files', 'objData')),
                'status' => true
            ];
        }
        else{
            return [
                'html' => (string) view('file-manager.delete-file', compact('files')),
                'status' => false
            ];
        }
    }
    public function deleteGdriveFile(Request $request)
	{
        if($request->type == "einkauf"){
            $properties_pdf = DB::table('property_einkauf_file')->where('id', $request->id)->first();
            if ($properties_pdf) {
                DB::table('property_einkauf_file')->where('id', $request->id)->delete();
            }
        }else if($request->type == "empfehlung"){
            GdriveUploadFiles::where('id', $request->id)->delete();
        }else if($request->type == "mieterliste"){
            TenancyScheduleItemFile::where('id', $request->id)->delete();
        }

		echo "1";
		die;
	}
    public function viewFiles(Request $request)
	{
		$dir = '/';
		if ($request->has('working_dir') && $request->working_dir != "") {
			$dir = $request->working_dir;
		}
		$recursive = false; // Get subdirectories also?
        $propertyEinkaufFiles = Storage::cloud()->listContents($dir, $recursive);
        // dd($propertyEinkaufFiles);
		return view('file-manager.view-file', compact('propertyEinkaufFiles'));
    }

    public function removeDuplicateSubDirectory(Request $request, $propertyId){
        echo PropertiesHelperClass::deleteDuplicatePropertyDirName($propertyId);
    }

    public function checkPropertyDirectoryName(Request $request, $propertyId){
        $property = Properties::find($propertyId);

        if(!$property){
            die('Property does not exist.');
        }
        $new_folder_name = PropertiesHelperClass::getPropertyDirectoryName($property);
        echo "Property name=".$new_folder_name;
        die();
    }

    public function directory_Permission(Request $request)
	{

        /*
        // $static_role = [2, 3, 4,5];
        $auth_roles = config('auth.role');
        unset($auth_roles['admin']);
        $static_role = array_values($auth_roles);
        // dd($static_role);

		$req_data = $request->user_role;
        $res = array_diff($static_role, $req_data);
        Directory_Permission::where('basename', $request->basename)->delete();
		foreach ($res as $role) {
			Directory_Permission::create(['basename' => $request->basename, 'user_role' => $role]);
		}
		return response()->json(array(
			'success' => true,
			'data' => "Saved"
        ));
        */

        // New impliment for user wise
        $res = $request->user_role;
        if(!is_array($res)){
            return response()->json(array(
                'success' => false,
                'data' => "Not saved"
            ));
        }
        $old_blocked_user = Directory_Permission::where('basename', $request->basename)->pluck('user_id')->toArray();
        $allUsers = User::pluck('id')->toArray();
        $new_block_user = [];
        $remove_block_user = [];
        foreach($allUsers as $userId){
            if(in_array($userId, $res))
            {
                if(in_array($userId, $old_blocked_user)){
                    $remove_block_user[] = $userId;
                }
            }else{
                if(!in_array($userId, $old_blocked_user)){
                    $new_block_user[] = [
                        'basename' => $request->basename,
                        'user_role' => 0,
                        'user_id' => $userId
                    ];
                }
            }
        }
        Directory_Permission::where('basename', $request->basename)->whereIn('user_id', $remove_block_user)->delete();
        Directory_Permission::insert($new_block_user);
        return response()->json(array(
            'success' => false,
            'data' => "Not saved"
        ));


	}

	public function get_Permission(Request $request)
	{
        $auth_roles = config('auth.role');
        unset($auth_roles['admin']);
        $static_role = array_values($auth_roles);
		$data =	Directory_Permission::where('basename', $request->id)->get();

		return response()->json(array(
			'success' => true,
			'data' => $data,
			'static_role' => $static_role,
		));
    }

    public function sendGdriveFileAttachmentMail(Request $request){
        $user = Auth::user();
        $rules = array(
            'file_basename' => 'required',
            'filename' => 'required',
            'mime_type' => 'required',
            'to_mail' => 'required|email',
            'subject' => 'required',
            );
        $inputs = $request->all();
        $inputs['from'] = $user->email;
        $inputs['from_name'] = $user->name;
        $validator = Validator::make($inputs, $rules);
        if($validator->fails()){
            $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            return response()->json($result);
        }
        // dd($inputs);

        if( !in_array($inputs['to_mail'], $this->mail_not_send()) ){

            Mail::raw($inputs['message'], function($mail) use ($inputs) {
                $mail->subject($inputs['subject'])->to($inputs['to_mail']);
                $mail->from($inputs['from'], $inputs['from_name']);
                $mail->replyTo($inputs['from'], $inputs['from_name']);
                $mail->sender($inputs['from'], $inputs['from_name']);
                $mail->attachData(\Storage::cloud()->get($inputs['file_basename']), $inputs['filename'], [
                                'mime' => $inputs['mime_type'],
                            ]);
             });
        }

         $result = ['status' => true, 'message' => 'Mail is sent successfully.', 'data' => []];
        return response()->json($result);

    }

    public function get_User_Permission(Request $request)
	{
     	$data =	Directory_Permission::where('basename', $request->id)->get();

		return response()->json(array(
			'success' => true,
			'data' => $data
		));
    }

    public function selectTheFolderOfActiveTabOfProperty($contents, $currentActiveTabName)
    {
        $tabContents = [];
        foreach($contents as $item)
        {
            if ($item['type'] == 'dir' &&  $item['name']==$currentActiveTabName)
            {
                $tabContents = Storage::cloud()->listContents($item['path'], false);
                $tabContents = array_values(array_sort($tabContents, function ($value) {
                    return $value['name'];
                }));
            }
        }

        return $tabContents;
    }

}
