<?php

namespace App\Http\Controllers;

use App\Models\Forecast;
use App\Models\ForecastKind;
use App\Services\ForecastsService;
use Illuminate\Http\Request;

class ForecastKindsController extends Controller
{
    public function store(Request $request, $forecast_id){

        $input = $request->all();

        $forecastKindData = [
            'forecast_id' => $forecast_id,
            'kind' => '',
            'description' => '',
            'status' => '',
            'notary' => date("Y-m-d"),
            'bnl' => date("Y-m-d"),
            'total_purchase_price' => 0,
            'fk_or_sale' => 0,
            'reflux' => 0,
            'note' => '',
            'type' => $input['type']
        ];

        $forecast_kind = ForecastKind::create($forecastKindData);
        $forecast = Forecast::find($forecast_id);


        return redirect("/forecasts?selecting-forecast=$forecast->id");
    }

    public function update_by_field(Request $request, $forecast_kind_id){
        $input = $request->all();
        $response = [
            'success' => false,
            'msg' => 'Failed to updated forecast kind'
        ];

        $forecast_kind = ForecastKind::where('id', '=', $forecast_kind_id)->first();

        // Clone forecast_kind
        $input = $forecast_kind;

        if( empty( $forecast_kind ) ) {
            $response['msg'] = 'Not found forecast kind';
            return response()->json($response);
        }

        // New value
        $input[$request->pk] = $request->value;

        if (ForecastsService::update($input, $forecast_kind))
        {
            $response = [
                'success' => true,
                'msg' => '',
                'selecting_forecast' => $forecast_kind->forecast_id
            ];
        }

        return response()->json($response);
    }
}
