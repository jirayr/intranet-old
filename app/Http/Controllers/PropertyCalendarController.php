<?php

namespace App\Http\Controllers;
use App\Models\PropertyCalendar;
use App\Models\Masterliste;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Models\Properties;
use App\Services\PropertiesService;
use App\Services\PropertyCalendarService;
use Symfony\Component\HttpKernel\Profiler\Profile;


class PropertyCalendarController extends Controller
{
            /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function show($id)
    {
        //
		$PropertyId = $id;
		$PropertyCalendars = PropertyCalendar::where('property_id','=',$id)->get();
		$Masterlistes = Masterliste::all();
		foreach($Masterlistes as $masterliste){
			 if($masterliste->asset_manager){
				$masterliste->asset_manager_name = User::where('id', $masterliste->asset_manager)->first()->name;
			 }
		 }
		foreach ($PropertyCalendars as $Calendar){
			//Add 1 day to events.end to fix bug in PE-32
            $Calendar->end = date('Y-m-d',strtotime($Calendar->end . ' +1 day'));
        }
		return view('property_calendar.show',compact('PropertyCalendars','PropertyId','Masterlistes'));
    }
   
    public function create(Request $request){
        $input = $request->all();
        if($input['start']>$input['end']&&isset($input['end'])){
            return "End date should be after or equal the Start date";
        }
        else{
			PropertyCalendarService::create( $input);
			return  back();
        }
    }
    public function edit(Request $request){
        $input = $request->all();
        $PropertyCalendar=PropertyCalendar::where('id','=',$input['id']);
        if(isset($input['delete'])){
			PropertyCalendarService::delete($PropertyCalendar);
        }
		else{
          
            PropertyCalendarService::update( $input,$PropertyCalendar);
        }  
        return  back();
    }
}
