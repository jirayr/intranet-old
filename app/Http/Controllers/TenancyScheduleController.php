<?php

namespace App\Http\Controllers;

use App\Models\TenancySchedule;
use App\Models\TenancyScheduleItem;
use App\Models\Properties;
use App\Models\PropertyVacant;
use App\Models\TenantPayment;
use App\Models\Masterliste;
use App\Models\PropertyRentPaids;
use App\Services\TenancySchedulesService;
use App\Services\PropertiesService;
use App\TenancyScheduleComment;
use App\User;
use App\Profit_loss_graph;
use App\Models\TenancyExtension;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use DB;
use Validator;

class TenancyScheduleController extends Controller
{
    public function index(){
        $tenancy_schedules = TenancySchedule::all();
        foreach ($tenancy_schedules as $tenancy_schedule){
            $tenancy_schedule->creator = User::where('id', $tenancy_schedule->creator_id)->first()->name;
            $tenancy_schedule->items = TenancyScheduleItem::where('tenancy_schedule_id', $tenancy_schedule->id)->get();

            $total_live_rental_space = 0;
            $total_business_rental_space = 0;
            $total_live_actual_net_rent = 0;
            $total_business_actual_net_rent = 0;

            $total_live_vacancy_in_qm = 0;
            $total_live_vacancy_in_eur = 0;
            $total_business_vacancy_in_qm = 0;
            $total_business_vacancy_in_eur = 0;

            foreach ($tenancy_schedule->items as $item){

                $remaining_time_in_eur = $item->remaining_time * $item->actual_net_rent * 12;
                $item->remaining_time_in_eur = $remaining_time_in_eur;

                if ($item->type == config('tenancy_schedule.item_type.live')){
                    $total_live_rental_space += $item->rental_space;
                    $total_live_actual_net_rent += $item->actual_net_rent;
                }

                if ($item->type == config('tenancy_schedule.item_type.live_vacancy')){
                    $total_live_vacancy_in_qm += $item->vacancy_in_qm;
                    $total_live_vacancy_in_eur += $item->vacancy_in_eur;
                }

                if ($item->type == config('tenancy_schedule.item_type.business')){
                    $total_business_rental_space += $item->rental_space;
                    $total_business_actual_net_rent += $item->actual_net_rent;
                }

                if ($item->type == config('tenancy_schedule.item_type.business_vacancy')){
                    $total_business_vacancy_in_qm += $item->vacancy_in_qm;
                    $total_business_vacancy_in_eur += $item->vacancy_in_eur;
                }

            }
            $tenancy_schedule->calculations = [
                'total_live_rental_space' => $total_live_rental_space,
                'total_live_actual_net_rent' => $total_live_actual_net_rent,
                'total_business_rental_space' => $total_business_rental_space,
                'total_business_actual_net_rent' => $total_business_actual_net_rent,
                'total_rental_space' => $total_live_rental_space + $total_business_rental_space,
                'total_actual_net_rent' => $total_live_actual_net_rent + $total_business_actual_net_rent,
                'total_live_vacancy_in_qm' => $total_live_vacancy_in_qm,
                'total_live_vacancy_in_eur' => $total_live_vacancy_in_eur ,
                'total_business_vacancy_in_qm' => $total_business_vacancy_in_qm,
                'total_business_vacancy_in_eur' => $total_business_vacancy_in_eur
            ];

            if( $tenancy_schedule->property_id ) {
                $tenancy_schedule->property = Properties::where('property_id', $tenancy_schedule->property_id)->first();
            }

            if( isset( $tenancy_schedule->property ) && $tenancy_schedule->property != null ) {
                $tenancy_schedule->masterliste = Masterliste::where('id', $tenancy_schedule->property->masterliste_id )->first();
            }
        }

        if (isset($_GET['selecting_tenancy_schedule'])){
            $selecting_tenancy_schedule = $_GET['selecting_tenancy_schedule'];
        } else {
            if (count($tenancy_schedules) > 0){
                $selecting_tenancy_schedule = $tenancy_schedules[0]->id;
            }
            else $selecting_tenancy_schedule = -1;

        }


        return view('tenancy_schedules.index',
            compact('tenancy_schedules', 'selecting_tenancy_schedule'));
    }
    public function delete_tenant(Request $request){

        TenancySchedule::find($request->id)->delete();
        echo "1"; die;
    }

    public function create(Request $request){
        $response = [
            'success' => false,
            'msg' => __('tenancy_schedule.messages.create_fail')
        ];

        $input_data = $request->all();
        $property_id = $input_data['property_id'];
      

        $input = [
            'object' => 'New Tenancy Schedule',
            'creator_id' => Auth::id(),
            'property_id' => $property_id
        ];



        if ($tenancy_schedule = TenancySchedulesService::create($input)) {
            $response = [
                'success' => true,
                'msg' => ''
            ];

            

        }
        return redirect(url("properties/$property_id?tab=tenancy-schedule&selecting_tenancy_schedule=".$tenancy_schedule->id));

    }

    public function update_by_field(Request $request, $id){
        $input = $request->all();
        $response = [
            'success' => false,
            'msg' => __('tenancy_schedule.messages.update_fail')
        ];

        $tenancy_schedule = TenancySchedule::where('id', '=', $id)->first();
        $property_id = $tenancy_schedule->property_id;

        // Clone forecast
        $input = $tenancy_schedule;

        if( empty( $tenancy_schedule ) ) {
            $response['msg'] = __('tenancy_schedule.messages.not_found');
            return response()->json($response);
        }





        // New value
        $input[$request->pk] = $request->value;

        if (TenancySchedulesService::update($tenancy_schedule, $input)) {
            $response = [
                'success' => true,
                'msg' => '',
                'selecting_tenancy_schedule' => $id
            ];
        }

        return response()->json($response);
    }

    public function delete($id){
//        ForecastKindsService::delete($id);
        $property_id = TenancySchedule::find($id)->property_id;
        TenancySchedule::find($id)->delete();

        return redirect(url("properties/$property_id?tab=tenancy-schedule"));

    }
    public function deleteItem($id,$itemId){
        $item=TenancyScheduleItem::find($itemId);
        $property_id = TenancySchedule::find($id)->property_id;
        if($item)
            $item->delete();

        // delete entry from graph table
        Profit_loss_graph::where('schedule_item_id',$itemId)->delete();
   

        return redirect(url("properties/$property_id?tab=tenancy-schedule"));
    }

    public function createItem(Request $request){
        $input_data = $request->all();
      
        $response = [
            'success' => false,
            'msg' => __('tenancy_schedule.messages.create_item_fail')
        ];
       
        $property_id = $input_data['property_id'];
        $properties = Properties::find($property_id);
        $asset_magager_id = 0;
        if($properties)
            $asset_magager_id = $properties->asset_m_id;

            $common_item_id = 1;
 
            $i = TenancyScheduleItem::max('common_item_id');
            if($i)
                $common_item_id = $i+1;


        
            $input = [
            'tenancy_schedule_id' => $input_data['tenancy_schedule_id'],
            'type' => $input_data['type'],
            'common_item_id' => $common_item_id,
            'asset_manager_id'=>$asset_magager_id
            ];  
     

        if ($tenancy_schedule_item = TenancySchedulesService::createItem($input)) {
            $response = [
                'success' => true,
                'msg' => ''
            ];

            // if($input_data['type']== config('tenancy_schedule.item_type.business_vacancy') || $input_data['type'] == config('tenancy_schedule.item_type.live_vacancy')){

                $new = new PropertyVacant;
                $new->type = $tenancy_schedule_item->type;
                $new->property_id = $property_id;
                $new->tenant_id = $tenancy_schedule_item->id;
                $new->save();    
            // }
            

            Profit_loss_graph::create([
               
                'schedule_item_id'=>$tenancy_schedule_item->id
            ]);

        }
        return redirect(url("properties/$property_id?tab=tenancy-schedule&selecting_tenancy_schedule=".$tenancy_schedule_item->tenancy_schedule_id));
    }
    
    public function updatePayment(Request $request,$id,$year)
    {
        $response = [
            'success' => false,
        ];

        $column = $request->pk;
        $modal = TenantPayment::where('tenancy_schedule_item_id', $id)
        ->where('year',$year)
        ->first();

        if($column=="amount")
        {
            $request->value = str_replace('.', '', $request->value);
            $request->value = str_replace(',', '.', $request->value);
            $request->value = str_replace('%', '', $request->value);
            $request->value = str_replace('€', '', $request->value);
        }

        $date_list_field = array('bka_date','payment_date');
        if(in_array($request->pk, $date_list_field))
        {
            $v = validatedate($request->value);
            if($v)
            {
                $request->value = save_date_format($request->value);
            }
            else{
                $response['msg'] = "Please enter valid date in (dd.mm.yyyy) format";
                return response()->json($response);
            }
        }

        if($modal)
        {
            $modal->$column = $request->value;
            $modal->save();
        }
        else
        {
            $modal = new TenantPayment;
            $modal->tenancy_schedule_item_id= $id;
            $modal->year = $year;
            $modal->$column = $request->value;
            $modal->save();
        }
        $response = [
            'success' => true,
            'reload' => false,
        ];
        return response()->json($response);
    }

    public function update_item_by_field(Request $request, $id){
        $input = $request->all();
        $response = [
            'success' => false,
            'msg' => __('tenancy_schedule.messages.update_item_fail')
        ];

        $tenancy_schedule_item = TenancyScheduleItem::where('id', '=', $id)->first();

        if($request->pk=="opos_mieter" && $request->value)
        {
            $property = TenancySchedule::selectRaw('property_id')->where('tenancy_schedules.id', '=', $tenancy_schedule_item->tenancy_schedule_id)->first();
            if($property)
            {
                $rentdata = PropertyRentPaids::where('property_id',$property->property_id)->where('mieter','like','%'.$request->value.'%')->first();
                if(!$rentdata)
                {
                    $response['msg'] = "Invalid Value";
                    return response()->json($response);
                }

            }
        }

        
        // Clone forecast
        $input = $tenancy_schedule_item;

        if($request->pk!="comment" &&$tenancy_schedule_item->rent_end && $tenancy_schedule_item->rent_end < date('Y-m-d'))
        {
            $response['msg'] = "Item is inactive. Can not update";
            return response()->json($response);
        }

        if( empty( $tenancy_schedule_item ) ) {
            $response['msg'] = __('tenancy_schedule.messages.item_not_found');
            return response()->json($response);
        }

        /*if(($request->pk=="rent_begin" || $request->pk=="assesment_date" || $request->pk=="rent_end" || $request->pk=="termination_date") && $request->value)
        {
            $date = $request->value;
            if($date != date_format(  date_create(str_replace('/', '-', $date)) , 'd/m/Y'))
            {
                $response['msg'] = "Please enter date in (dd/mm/yyyy) format";
                return response()->json($response);
            }
            else{
                $request->value = date_format(  date_create(str_replace('/', '-', $date)) , 'Y-m-d');   
            }
        }*/

        $date_list_field = array('rent_begin','assesment_date','rent_end','termination_date', 'vacant_since', 'is_upload_date','termination_by');
        if(in_array($request->pk, $date_list_field))
        {
            $v = validatedate($request->value);
            if($v)
            {
                $request->value = save_date_format($request->value);
            }
            else{
                $response['msg'] = "Please enter valid date in (dd.mm.yyyy) format";
                return response()->json($response);
            }
        }



        
        if($request->pk=="nk_netto" || $request->pk=="actual_net_rent")
        {
            $request->value = str_replace('.','',$request->value);
            $request->value = str_replace(',','.',$request->value);    
        }

        $property = TenancySchedule::selectRaw('name_of_property,main_property_id,ads.id')->leftjoin('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')
        ->leftjoin('properties', 'properties.main_property_id', 'tenancy_schedules.property_id')
        ->leftJoin('ads','properties.main_property_id','=','ads.property_id')
        ->where('tenancy_schedule_items.id', '=', $id)->first();

        $url = "";
        $name_of_property = "";
        $adid = 0;
        if($property)
        {
            $name_of_property = $property->name_of_property;
            $adid = $property->id;
            $url = route('properties.show',['property'=>$property->main_property_id]);
        }

        $name_array = array('assesment_date'=>'Abschluss MV','rent_begin'=>'Mietbeginn','termination_date'=>'Sonderkündigunsrecht','rent_end'=>'Mietende','options'=>'Optionen','use'=>'Nutzung','rental_space'=>'Mietfläche/m²','actual_net_rent'=>'IST-Nettokaltmiete p.m.','nk_netto'=>'NK netto p.m.','indexierung'=>'Indexierung','warning_date1'=>'Mahnung','warning_date2'=>'Mahnung','warning_date3'=>'Mahnung','warning_price3'=>'Mahnung','kaution'=>'Kaution','vacancy_in_qm'=>'Leerstand in qm','actual_net_rent2'=>'bewertet AM','comment'=>'Kommentar');
        $change_str = "";
        if(isset($name_array[$request->pk]))
            $change_str = $name_array[$request->pk];


        // $text = 'Änderungen in der Mieterliste für das Obkjekt: '.$name_of_property.'  im Intranet. Es wurde '.$change_str.' von '.Auth::user()->name.' geändert. Es müssen eventuell entsprechende Änderungen im Verkaufsportal vorgenommen werden. '.$url;

        $text = 'Änderungen in der Mieterliste für das Obkjekt: '.$name_of_property.'  im Intranet. Es wurde bei dem Mieter '.$tenancy_schedule_item->name.' das Feld '.$change_str.' von '.Auth::user()->name.' in '.$request->value.' geändert.. Es müssen eventuell entsprechende Änderungen im Verkaufsportal vorgenommen werden. '.$url;

        $email = array('a.raudies@fcr-immobilien.de');
        
        $url = "https://verkauf.fcr-immobilien.de/getuploadlist";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $str = curl_exec($curl);
        curl_close($curl); 
        $ids = array(); 
        if($str)
        {
            $ids = json_decode($str,true);
        }



        // New value
        $input[$request->pk] = $request->value;
        $this->calculate_item($tenancy_schedule_item);

        if (TenancySchedulesService::updateItem($tenancy_schedule_item, $input)) {

            // $email = "c.wiedemann@challenges.de";
            if($request->pk=='name')
                PropertyVacant::where('tenant_id', $id)->update(['name' =>$request->value]);

            if(in_array($adid, $ids) && !in_array($email, $this->mail_not_send()) )
            {
                Mail::raw($text, function ($message) use($email) {
                            $message->to($email)
                    ->from('no-reply@intranet.fcr-immobilien.de')
                    ->subject('Änderungen Mieterliste');
                }); 

            }
            

            $response = [
                'success' => true,
                'msg' => '',
                'selecting_tenancy_schedule' => $tenancy_schedule_item->tenancy_schedule_id
            ];

        /*========================================================
        =  Update or create new entry for profit and loss graph  =
        =========================================================*/
            $t_schedule_item = TenancyScheduleItem::where('id', '=', $id)->first();
            $profit_loss_graph = Profit_loss_graph::where('schedule_item_id', '=', $id)->orderBy('id','desc')->first();

            if(!$profit_loss_graph)
            {
                Profit_loss_graph::create([
                    'schedule_item_id'=>$id
                ]);

                $profit_loss_graph = Profit_loss_graph::where('schedule_item_id', '=', $id)->orderBy('id','desc')->first();
            }

            $item_rent_begin = $t_schedule_item->rent_begin;
            $item_rent_end   = $t_schedule_item->rent_end;
            $item_net_rent   = $t_schedule_item->actual_net_rent;
            
            $graph_rent_begin  = $profit_loss_graph->rent_begin;
            $graph_rent_end    = $profit_loss_graph->rent_end;
            $graph_net_rent    = $profit_loss_graph->actual_net_rent;

            if (strtotime($item_rent_begin) >= strtotime($item_rent_end) && $graph_rent_end != null ){

                Profit_loss_graph::create([
                    'rent_begin'       => $item_rent_begin,
                    'rent_end'         => $item_rent_end,
                    'actual_net_rent'  => $item_net_rent,
                    'schedule_item_id' => $id
                ]);
 
            }elseif($item_rent_begin != $graph_rent_begin || $item_rent_end == $graph_rent_end || $item_net_rent == $graph_net_rent){
                 Profit_loss_graph::where('id',$profit_loss_graph->id)->update([
                    'rent_begin'      =>$item_rent_begin,
                    'rent_end'        => $item_rent_end,
                    'actual_net_rent' =>$item_net_rent
                ]);
            }
 
        /*===============  End profit and loss graph  ============*/


        }

        return response()->json($response);
    }

    public function change_date(Request $request){
        $input = $request->all();
        $response = [
            'success' => false,
            'msg' => __('tenancy_schedule.messages.update_item_fail')
        ];

        $tenancy_schedule_item = TenancyScheduleItem::where('id', '=', $request->id)->first();

        // Clone forecast
        $input = $tenancy_schedule_item;

        if( empty( $tenancy_schedule_item ) ) {
            $response['msg'] = __('tenancy_schedule.messages.item_not_found');
            return response()->json($response);
        }

        if(!$request->new_start_date)
        {
            $response['msg'] = "Bitte Beginn der Verlängerung eintragen";
            return response()->json($response);
        }
        if(!$request->new_date)
        {
            $response['msg'] = "Bitte Mietende eintragen";
            return response()->json($response);
        }
        $date = $request->new_start_date;
        // if($date != date('d/m/Y', strtotime(str_replace('/', '-', $date))))

        if($date != date_format(  date_create(str_replace('/', '-', $date)) , 'd/m/Y'))

        {
            $response['msg'] = "Ungültiges Beginn der Verlängerung eintragen (dd/mm/yyyy)";
            return response()->json($response);
        }
        $date = $request->new_date;
        // if($date != date('d/m/Y', strtotime(str_replace('/', '-', $date))))
        if($date != date_format(  date_create(str_replace('/', '-', $date)) , 'd/m/Y'))
        {
            $response['msg'] = "Ungültiges Mietende eintragen (dd/mm/yyyy)";
            return response()->json($response);
        }

        

        // New value
        //$input['rent_end'] = date('Y-m-d',strtotime($request->new_date));

        $input['status'] = 0;
        $input1['new_start_date'] = date_format(  date_create(str_replace('/', '-', $request->new_start_date)) , 'Y-m-d');

        $input1['new_date'] = date_format(  date_create(str_replace('/', '-', $request->new_date)) , 'Y-m-d');

        // $response['input1'] = $input1;
        // return response()->json($response);

        // $input1['new_date']     = date('Y-m-d',strtotime($request->new_date));
        
        

        if (TenancySchedulesService::updateItem($tenancy_schedule_item, $input)) {

            $item_data = [
                'name' => $tenancy_schedule_item['name'],
                'type' => $tenancy_schedule_item['type'],
                'tenancy_schedule_id' => $tenancy_schedule_item['tenancy_schedule_id'],
                'asset_manager_id' => $tenancy_schedule_item['asset_manager_id'],
                'rent_begin' => $input1['new_start_date'],
                'rent_end' => $input1['new_date'],
                'termination_date' => $tenancy_schedule_item['termination_date'],
                'options' => $tenancy_schedule_item['options'],
                'use' => $tenancy_schedule_item['use'],
                'rental_space' => $tenancy_schedule_item['rental_space'],
                'actual_net_rent' => $tenancy_schedule_item['actual_net_rent'],
                'vacancy_in_qm' => $tenancy_schedule_item['vacancy_in_qm'],
                'vacancy_in_eur' => $tenancy_schedule_item['vacancy_in_eur'],
                'remaining_time' => $tenancy_schedule_item['remaining_time'],
                'is_new' => 0,
                'status' => 1,
                'comment' => $tenancy_schedule_item['comment'],
                'comment2' => $tenancy_schedule_item['comment2'],
                'common_item_id' => $tenancy_schedule_item['common_item_id'],

            ];
            $item = TenancySchedulesService::createItem($item_data);

            // print_r($item);

            if($tenancy_schedule_item['type']== config('tenancy_schedule.item_type.business_vacancy') || $tenancy_schedule_item['type'] == config('tenancy_schedule.item_type.live_vacancy')){


                $s = TenancySchedule::find($tenancy_schedule_item->tenancy_schedule_id);

                $new = new PropertyVacant;
                $new->type = $tenancy_schedule_item['type'];
                if($s)
                $new->property_id = $s->property_id;
                $new->tenant_id = $item->id;
                $new->save();    
            }



            if($item)
            {
                $input1['tenancy_schedule_item_id']     = $item->id;
                TenancyExtension::create($input1);    
            }
            

            $response = [
                'success' => true,
                'msg' => '',
                'selecting_tenancy_schedule' => $tenancy_schedule_item->tenancy_schedule_id
            ];
        }

        return response()->json($response);
    }

    public function calculate_item($tenancy_schedule_item){
//        $tenancy_schedule_item->vacancy_in_eur = $tenancy_schedule_item->vacancy_in_qm * 5;
    }

    public function  storeComments(Request $request){
        $tenancyScheduleComment = new TenancyScheduleComment();
        $tenancyScheduleComment->create([
            'user_name' => Auth()->user()->name,
            'user_id' => Auth()->user()->id,
            'item_id'=> $request->item_id,
            'comment'=> $request->comment,
        ]);
        return redirect()->back();
    }

    public function  storeComment2(Request $request){
        $tenancyScheduleComment = new TenancyScheduleComment();
        $tenancyScheduleComment->create([
            'user_name' => Auth()->user()->name,
            'user_id' => Auth()->user()->id,
            'item_id'=> $request->item_id,
            'external_comment'=> $request->external_comment
        ]);
        return redirect()->back();
    }

    public function getComments(Request $request)
    {
        return TenancyScheduleComment::where('item_id',$request->id)->orderBy('created_at','desc')->get();
    }

    public function deleteComment($id)
    {
        TenancyScheduleComment::find($id)->delete();
        return redirect()->back();
    }

    public function find(Request $request)
    {
        return TenancyScheduleItem::where('id',$request->id)->first();
    }

    public function addItemComment(Request $request){
        if($request->ajax()) {
            $validator = Validator::make($request->all(), [
                "comment"       => "required|min:5",
                "item_id"   => "required",
                "property_id"   => "required",
            ]);

            if($validator->fails()){
                $result = ['status' => false, 'message' => $validator->errors()->first(), 'data' => []];
            }else{

                $user = Auth::user();

                $modal = new TenancyScheduleComment;
                $modal->item_id = $request->item_id;
                if($request->type == 1){
                    $modal->external_comment = $request->comment;
                }else{
                    $modal->comment = $request->comment;
                }
                $modal->user_name = $user->name;
                $modal->user_id = $user->id;

                if( $modal->save() ){

                    if($request->subject){

                        $email_not_send_array = $this->mail_not_send();

                        $p = DB::table('properties as p')->selectRaw('p.id, u.name, u.email, u2.name as name2, u2.email as email2')
                                ->leftjoin('users as u', 'u.id', '=', 'p.asset_m_id')
                                ->leftjoin('users as u2', 'u2.id', '=', 'p.asset_m_id2')
                                ->where('p.id', $request->property_id)->first();

                        $subject = $request->subject;
                        $text = '';
                        if($request->comment){
                            $text = 'Kommentar '.$user->name.': '.$request->comment;
                        }
                        if($request->content){
                            $text .= '<br><br>'.$request->content;
                        }

                        $text .= '<br><br>Bitte antworte direkt im Kommentarfeld im Intranet auf diesen Kommentar.';
                        
                        if($user->email == config('users.falk_email')){//send mail to am

                            $email = $p->email;
                            $name = $p->name;
                            $replyemail = config('users.falk_email');
                            $replyname = 'Falk';

                            if($email && !in_array($email, $email_not_send_array) ){

                                $email_not_send_array[] = $email;

                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });
                            }

                            if( $p->email2 && !in_array($p->email2, $email_not_send_array) ){

                                $email_not_send_array[] = $p->email2;

                                $email = $p->email2;
                                $name = $p->name2;
                                $replyemail = config('users.falk_email');
                                $replyname = 'Falk';

                        
                                Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                    $message->to($email, $name)
                                    ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                    ->replyTo($replyemail, $replyname)
                                    ->subject($subject);
                                });
                            }           

                        }else{//send mail to falk

                                $email = config('users.falk_email');
                                $name = 'Falk';
                                $replyemail = $p->email;
                                $replyname = $p->name;

                                if( $email && !in_array($email, $email_not_send_array) ){

                                    $email_not_send_array[] = $email;

                                    Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                        $message->to($email, $name)
                                        ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                        ->replyTo($replyemail, $replyname)
                                        ->subject($subject);
                                    });
                                }        

                        }

                        // send mail to custom users
                        $custom_mail = ($request->comment) ? fetch_mails_by_string($request->comment) : [];
                        if(!empty($custom_mail)){

                            $custom_mail = array_unique($custom_mail);
                            $replyemail = $user->email;
                            $replyname = $user->name;

                            Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $custom_mail, $replyemail, $replyname) {
                                $message->to($custom_mail)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($replyemail, $replyname)
                                ->subject($subject);
                            });
                        }

                        // send mail to uploaded user
                        $userRes = DB::table('tenancy_schedule_items as tsi')
                                ->select('u.email', 'u.name')
                                ->join('tenancy_schedules as ts', 'ts.id', '=', 'tsi.tenancy_schedule_id')
                                ->join('users as u', 'u.id', '=', 'ts.creator_id')
                                ->where('tsi.id', $request->item_id)
                                ->first();
                        if( isset($userRes->email) && $userRes->email && !in_array($userRes->email, $email_not_send_array) ){

                            $email_not_send_array[] = $userRes->email;

                            $email = $userRes->email;
                            $name = $userRes->name;
                            $replyemail = $user->email;
                            $replyname = $user->name;

                            Mail::send('emails.general', ['text' => $text], function ($message) use($subject, $email, $name, $replyemail, $replyname) {
                                $message->to($email, $name)
                                ->from('no-reply@intranet.fcr-immobilien.de', 'FCR INTRANET')
                                ->replyTo($replyemail, $replyname)
                                ->subject($subject);
                            });

                        }

                    }

                    $result = ['status' => true, 'message' => 'Comment add successfully.', 'data' => []];
                }else{
                    $result = ['status' => false, 'message' => 'Comment add fail! Try again.', 'data' => []];
                }
            }
        }else{
            $result = ['status' => false, 'message' => 'Invalid request!', 'data' => []];
        }
        return response()->json($result);
    }

    public function getItemComment(Request $request){
        $user = Auth::user();

        $res = DB::table('tenancy_schedule_comments as tc')
                ->selectRaw('tc.id, tc.user_id, IFNULL(tc.user_name, "") as user_name, tc.comment, tc.external_comment, tc.created_at, IFNULL(u.name, "") as name, u.role, u.company')
                ->leftjoin('users as u', 'u.id', '=', 'tc.user_id');

                if($request->item_id){
                    $res = $res->where('item_id', $request->item_id);
                }
                if($request->type == 1){
                    $res = $res->whereNotNull('tc.external_comment')->where('tc.external_comment', '!=', '');
                }else{
                    $res = $res->whereNotNull('tc.comment')->where('tc.comment', '!=', '');
                }
                if($request->limit != '' && $request->limit != 0 && $request->limit != -1){
                    $res = $res->limit($request->limit);
                }
                $res = $res->orderBy('tc.created_at', 'desc')->get();

        if($res){
            foreach ($res as $key => $value) {
                $res[$key]->comment = nl2br($value->comment);
                $res[$key]->date = show_date_format($value->created_at);
                $res[$key]->created_at = show_datetime_format($value->created_at);
                $res[$key]->login_id = $user->id;
            }
        }

        return response()->json($res);
    }

    public function deleteItemComment(Request $request){
        $model = TenancyScheduleComment::find($request->id);
        if($model->delete()){
            $response = ['status' => true, 'message' => 'Comment delete successfully.'];
        }else{
            $response = ['status' => false, 'message' => 'Comment delete fail!'];
        }
        return response()->json($response);
    }

}
