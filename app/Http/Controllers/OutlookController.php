<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use GuzzleHttp\Client;


class OutlookController extends Controller
{

 
public function mail()
{
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }

  $tokenCache = new \App\TokenStore\TokenCache;

  $graph = new Graph();
  $graph->setAccessToken($tokenCache->getAccessToken());

  $user = $graph->createRequest('GET', '/me')
                ->setReturnType(Model\User::class)
                ->execute();

  $messageQueryParams = array (
    // Only return Subject, ReceivedDateTime, and From fields
    "\$select" => "subject,receivedDateTime,from,body",
    // Sort by ReceivedDateTime, newest first
    "\$orderby" => "receivedDateTime DESC",
    // Return at most 10 results
    "\$top" => "10"
  );

  $getMessagesUrl = '/me/mailfolders/inbox/messages?'.http_build_query($messageQueryParams);
  $messages = $graph->createRequest('GET', $getMessagesUrl)
                    ->setReturnType(Model\Message::class)
                    ->execute();

  return view('outlook/mail', array(
    'username' => $user->getDisplayName(),
    'messages' => $messages
  ));
}


// public function calendar()
// {
//   if (session_status() == PHP_SESSION_NONE) {
//     session_start();
//   }

//   $tokenCache = new \App\TokenStore\TokenCache;

//   $graph = new Graph();
//   $graph->setAccessToken($tokenCache->getAccessToken());

//   $user = $graph->createRequest('GET', '/me')
//                 ->setReturnType(Model\User::class)
//                 ->execute();

//   $eventsQueryParams = array (
//     // // Only return Subject, Start, and End fields
//     "\$select" => "subject,start,end",
//     // Sort by Start, oldest first
//     "\$orderby" => "Start/DateTime",
//     // Return at most 10 results
//     "\$top" => "10"
//   );

//   $getEventsUrl = '/me/events?'.http_build_query($eventsQueryParams);
//   $events = $graph->createRequest('GET', $getEventsUrl)
//                   ->setReturnType(Model\Event::class)
//                   ->execute();

//   return view('outlook/calendar', array(
//     'username' => $user->getDisplayName(),
//     'events' => $events
//   ));
// }


public function contacts()
{
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }

  $tokenCache = new \App\TokenStore\TokenCache;

  $graph = new Graph();
  $graph->setAccessToken($tokenCache->getAccessToken());

  $user = $graph->createRequest('GET', '/me')
                ->setReturnType(Model\User::class)
                ->execute();

  $contactsQueryParams = array (
    // // Only return givenName, surname, and emailAddresses fields
    "\$select" => "givenName,surname,emailAddresses",
    // Sort by given name
    "\$orderby" => "givenName ASC",
    // Return at most 10 results
    "\$top" => "10"
  );

  $getContactsUrl = '/me/contacts?'.http_build_query($contactsQueryParams);
  $contacts = $graph->createRequest('GET', $getContactsUrl)
                    ->setReturnType(Model\Contact::class)
                    ->execute();

  return view('outlook/contacts', array(
    'username' => $user->getDisplayName(),
    'contacts' => $contacts
  ));
}


 public function read_message($id){

  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }

  $tokenCache = new \App\TokenStore\TokenCache;

  $graph = new Graph();
  $graph->setAccessToken($tokenCache->getAccessToken());

  $user = $graph->createRequest('GET', '/me')
                ->setReturnType(Model\User::class)
                ->execute();

  $messageQueryParams = array (
    // Only return Subject, ReceivedDateTime, and From fields
    "\$select" => "subject,receivedDateTime,from,body",
    // Sort by ReceivedDateTime, newest first
    "\$orderby" => "receivedDateTime DESC",
    // Return at most 10 results
    "\$top" => "10"
  );

  $getMessagesUrl = '/me/mailfolders/inbox/messages/'.$id.'/?'.http_build_query($messageQueryParams);
  $messages['messages'] = $graph->createRequest('GET', $getMessagesUrl)
                    ->setReturnType(Model\Message::class)
                    ->execute();

   return view('outlook/message',$messages);
 } 


 public function reply_message($id){

  if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }
 echo $_SESSION['access_token'];
    $_SESSION['refresh_token'];
    $_SESSION['token_expires'];
 
die();
  $tokenCache = new \App\TokenStore\TokenCache;

  $graph = new Graph();
  $graph->setAccessToken($tokenCache->getAccessToken());

  $user = $graph->createRequest('POST', '/me')
                ->setReturnType(Model\User::class)
                ->execute();
 
	$messageQueryParams = json_encode(['Message' => 'Comment']);
   
    $getMessagesUrl = '/me/messages/'.$id.'/reply?';
    $messages['messages'] = $graph->createRequest('POST', $getMessagesUrl)
                    ->setReturnType(Model\Message::class)
                    ->attachBody($messageQueryParams)
                    ->execute();


                    dd($messages);
   //return view('outlook/message',$messages);
 } 



 public function forward_message(){
 

 if (session_status() == PHP_SESSION_NONE) {
    session_start();
  }

  $tokenCache = new \App\TokenStore\TokenCache;

  $graph = new Graph();
  $graph->setAccessToken($tokenCache->getAccessToken());

  $user = $graph->createRequest('POST', '/me')
                ->setReturnType(Model\User::class)
                ->execute();

  $messageQueryParams = array (
	 "Comment" => "this is final test"
  );

  $getMessagesUrl = '/me/messages/'.$id.'/forward?'.http_build_query($messageQueryParams);
  $messages['messages'] = $graph->createRequest('POST', $getMessagesUrl)
                    ->setReturnType(Model\Message::class)
                    ->execute();
	
dd($messages);
  
//  $response_send = {
//   "Comment": "FYI",
//   "ToRecipients": [
//     {
//       "EmailAddress": {
//         "Address": "club.akash@gmail.com"
//       }
//     },
//     {
//       "EmailAddress": {
//         "Address": "garthf@a830edad9050849NDA1.onmicrosoft.com"
//       }
//     }
//   ]
// };


 }




}