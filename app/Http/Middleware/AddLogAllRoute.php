<?php

namespace App\Http\Middleware;

use Closure;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class AddLogAllRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->path() !=  'get-new-notification'){
            Log::info('Route Log: =>');
            Log::info('URL: '.$request->path());
            Log::info('method: '.$request->method());
            Log::info('params: ');
            Log::info(print_r($request->all(), true));
        }
        if($request->path() !=  'change-password' && $request->path() !=  'logout'){
            $user = Auth::user();
            if($user && $user->password_change_date){
                $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->password_change_date);
                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
                $diff_in_days = $to->diffInDays($from);
                if($diff_in_days > 30){
                     return redirect('/change-password');
                }
            }
        }

        $user = Auth::user();
            if($request->path() !=  'logout' && $user && $user->last_login_time !=Session::get('last_login_time') && $user->id != 2 && $user->id != 1)
            {
                return redirect('/logout');
            }

        // Session::get('last_login_time');
        // die;
        return $next($request);
    }
}
