<?php

namespace App\Http\Middleware;

use Closure;
use Auth;


class Guest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        return $next($request);
        // foreach(Auth::User()->role as $role){

        //     #### is_admin
        //     if($role->title == 'admin'){

        //         return $next($request);
        //     }
        // }  
        
        // return response('Unauthorized.', 401);
    

    }
}