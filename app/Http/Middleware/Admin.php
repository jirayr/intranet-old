<?php

namespace App\Http\Middleware;

use Closure;
use Auth;


class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::user()->role);
        if ( Auth::check() && Auth::user()->role != 6 )
        {
            return $next($request);
        }

        return redirect('/extern_user');
    }
}
