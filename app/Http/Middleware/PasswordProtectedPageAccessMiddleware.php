<?php

namespace App\Http\Middleware;

use Closure;

class PasswordProtectedPageAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->session()->has('granted_protected_page_access'))
        {
            return $next($request);
        } else {

            return redirect('/password_for_protected_page_access');
        }

    }
}
