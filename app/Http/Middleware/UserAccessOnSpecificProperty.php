<?php

namespace App\Http\Middleware;

use App\PropertyPermission;
use Closure;
use Illuminate\Support\Facades\Auth;

class UserAccessOnSpecificProperty
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if(in_array($user->role, array(1,5)))       //admin and finance user can see all properties
        {
            return $next($request);
        }
        if(in_array($user->second_role, array(1,5)))
        {
            return $next($request);
        }
        // echo "here"; die;
            

        $propertyPermissions = PropertyPermission::where('property_id', $request->route('property'))->get()->pluck('user_id')->toArray();

        if ($propertyPermissions){
            if (in_array($user->id, $propertyPermissions))
            {
                return $next($request);
            } else {
                if($user->role<=5)
                {
                    // echo "here"; die;
                    return redirect('/invoiceobjects/'.$request->route('property'));    
                }
                return redirect('/');
            }
        }

        return $next($request);
    }

}
