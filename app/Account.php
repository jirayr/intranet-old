<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['name_of_property','account_owner','bank_name','iban','contact_person','balance','last_updated_at'];

}
