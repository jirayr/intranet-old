<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['company_id','employee_id','zip','street','city','part_of_city'];

}
