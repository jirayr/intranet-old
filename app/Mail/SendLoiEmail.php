<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendLoiEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->data['cc'])
        {
             $this->cc( $this->data['cc']);
        }
        if($this->data['bcc'])
        {
             $this->bcc( $this->data['bcc']);
        }

        return $this
            ->from($this->data['sender_email'], $this->data['from_name'])
            ->replyTo($this->data['reply_email'], $this->data['from_name'])
            ->attach($this->data['attachment'])
            ->subject($this->data['subject'])
            ->view('mails.loi_email_template')->with([
                'data' => $this->data
            ]);
    }
}
