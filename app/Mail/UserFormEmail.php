<?php
 
namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class UserFormEmail extends Mailable
{
    use Queueable, SerializesModels;
     
    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $data;
    public $subject='Neues Finanzierungsangebot';
    public $attachment=null;
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data=null)
    {
        $this->data = $data;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build($params=array())
    {
        $this->from(env('MAIL_USERNAME', 'bhanwar@itradicals.com'))
				->subject($this->subject)
				->view('mails.user_form_detail')
				->with(
				  [
						'testVarOne' => '1',
						'testVarTwo' => '2',
				  ]);
		
		return $this;
    }
}