<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class DemoEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $demo;
    public $subject='FCR Immobilien AG_Finanzierungsanfrage';
    public $attachment=null;
    public $attachment_name=null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($demo=null)
    {
        $this->demo = $demo;
        // $this->subject = $demo->subject;
        $this->attachment = $demo->attachment;
//        $this->attachment_name = $demo->attachment_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build($params=array())
    {
        // $this->from(env('MAIL_USERNAME', 'bhanwar@itradicals.com'))
        $this->from($this->demo->email_from)
            ->bcc(Auth::user()->email)
            ->replyTo($this->demo->email_from, $this->demo->email_from_name)
            ->subject($this->demo->subject)
            ->view('mails.demo')
            //->text('mails.demo_plain')
            ->with(
                [
                    'testVarOne' => '1',
                    'testVarTwo' => '2',
                ]);
        if($this->attachment)
        {
            $this->attach(public_path('/email_templates2_attachments/').$this->demo->doc_key."/".$this->attachment, [
                'as' => $this->attachment_name,
            ]);
        }
//          $filePostFix = 0;
//          foreach($this->demo->pdfFiles as $list){
//            $filename = "Exposé";
//            if(count($this->demo->pdfFiles) > 1){
//              $filePostFix = $filePostFix+1;
//              $filename = $filename."_".$filePostFix;
//            }
//            $filename = $filename.".pdf";
//            // $this->attach($list['file_path'], ['as' => $filename]);
//            $this->attachData(\Storage::cloud()->get($list['file_basename']), $filename, [
//              'mime' => 'application/pdf',
//            ]);
//          }
//          $this->attach(public_path('/email_templates2_attachments/').$this->demo->doc_key."/".$this->demo->auto_attachment_name);

        if($this->demo->is_file_attached == 1){

            if ($this->demo->is_expose == 1 && $this->demo->property_id!=2945)
            {
                $this->attach(public_path('/Expose.pdf'));
            } elseif($this->demo->is_expose == 1) {
                $this->attach(public_path('/Expose_Bitterfeld.pdf'));
            }

            $this->attach(public_path('/email_templates2_attachments/').'FCR_Immobilien_AG.zip');
            if (isset($this->demo->pdf) && $this->demo->property_id!=2945 && $this->demo->property_id!=2943)
            {
                $this->attach(public_path('/Planungsspiegel.pdf'));
            } elseif(isset($this->demo->pdf) && $this->demo->property_id==2945) {
                $this->attach(public_path('/Finanzierungsplan_FCR_Bitterfeld_Gmbh_&_Co_KG.pdf'));
            } elseif(isset($this->demo->pdf) && $this->demo->property_id==2943) {
                $this->attach(public_path('/Finanzierungsplan_Plauen_200528.pdf'));
            }

        }


        if(isset($this->demo->attchments) && $this->demo->attchments)
        {
            foreach ($this->demo->attchments as $key => $value) {
                $this->attach(public_path('/email_templates2_attachments/'.$this->demo->doc_key.'/').$value['file_basename'],[
                        'as' =>$value['filename']
                    ]
                );
            }
        }

        return $this;
    }
}
