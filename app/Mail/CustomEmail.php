<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@fcr-immobilien.de')
        ->attach(public_path('/').'Expose_Westerburg_Final_Stand_08.11.2019.pdf')
        ->subject('Anfrage FCR Immobilien AG')
        ->cc('s.precht@fcr-immobilien.de')
        ->view('mails.csv_user_email')
        ->with(['data'=>'
         Sehr geehrte Damen und Herren, wir sind auf Ihre Klinik aufmerksam geworden. Deshalb wollten wir Ihnen unser Hotel und Spezialimmobilie das Wasserschloss Westerburg im Harz zum Kauf anbieten. Wir könnten uns vorstellen, dass Sie sich dort eine Klinik einrichten könnten. Bitte entnehmen Sie dem Anhang zu dieser E-Mail unser Objektexposé. Sofern Sie Rückfragen zum Objekt haben sollten, stehen wir Ihnen gerne zur Verfügung. Wir würden uns über eine Antwort aus Ihrem Hause freuen.
        ']);
    }
}
