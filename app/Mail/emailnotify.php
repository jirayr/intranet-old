<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class emailnotify extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emailArray)
    {
        $this->emailArray = $emailArray;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (isset($this->emailArray['attachment'])){

            return $this->from(Auth::user()->email)

                ->attach($this->emailArray['attachment'],['as' => $this->emailArray['attachment']->getClientOriginalName()])
                ->subject($this->emailArray['subject'])
                ->view('mails.csv_user_email')
                ->with(['data'=>$this->emailArray]);

        }else{
            return $this->from(Auth::user()->email)

                ->subject($this->emailArray['subject'])
                ->view('mails.csv_user_email')
                ->with(['data'=>$this->emailArray]);
        }

    }
}
