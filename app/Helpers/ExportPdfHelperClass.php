<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 1/20/2020
 * Time: 5:52 PM
 */

namespace App\Helpers;


use App\Models\Banks;
use App\Models\Masterliste;
use App\Models\Properties;
use App\Models\PropertiesTenants;
use App\Models\Tenants;
use App\Services\PropertiesService;
use App\Services\TenancySchedulesService;
use App\User;
use Illuminate\Support\Facades\Storage;
use PDF;
class ExportPdfHelperClass
{
    public static function bankexport($id,$sheetPropId) {
        $properties = Properties::where('id', '=', $id)->first();
        // print_r($properties); die;

        $main_properties = Properties::where('id',$sheetPropId)->where('main_property_id', $id)->first();
         if ($properties->kk_idx)
            $kk_idx = number_format($properties->kk_idx, 2, ",", ".");

        $pr_user = User::where('id', $properties->user_id)->first();
        $properties->user_name = "";
        if($pr_user)
            $properties->user_name = $pr_user->name;

        $clever_fit_fields = json_decode($properties->clever_fit_fields, true);

        if (!is_array($clever_fit_fields)) {
            $clever_fit_fields = array();
        }
        $clever_fit_fields = PropertiesService::initCleverFitFields($clever_fit_fields);

        $properties->clever_fit_fields = $clever_fit_fields;



        $planung_fields = json_decode($properties->planung_fields, true);

        if (!is_array($planung_fields)) {
            $planung_fields = array();
        }
        $planung_fields = PropertiesService::initPlanungFields($planung_fields);

        $properties->planung_fields = $planung_fields;

        $tenants = Tenants::where('property_id', $id)->get();

        $bank_ids = json_decode($properties->bank_ids);
        $banks = [];

        $bank_all = [];
        $fake_bank = new Banks();
        $fake_bank->name = '';
        $fake_bank->user_id = 0;
        $fake_bank->with_real_ek = 0;
        $fake_bank->from_bond = 0.2;
        $fake_bank->bank_loan = 0.8;
        $fake_bank->interest_bank_loan = 0.02;
        $fake_bank->eradication_bank = 0.04;
        $fake_bank->interest_bond = 0.06;
        if ($bank_ids != null) {
            foreach ($bank_ids as $bank_id) {
                $banks[] = Banks::where('id', '=', $bank_id)->first();
            }
        } else {
            $banks[] = $fake_bank;
        }

        $bank_all = Banks::all();

        $comments_new = array();

        if($main_properties)
            $properties = $main_properties;


        $schl_banks = $properties->schl_banks != null ? (array) json_decode($properties->schl_banks) : [];

        $tenancy_schedule_data = TenancySchedulesService::get($id);
        $properties->masterliste = Masterliste::where('id', $properties->masterliste_id)->first();

        $propertiesExtra1s = PropertiesTenants::where('propertyId', $properties->id)->get();


        $tr_users = User::where('role', 2)->orWhere('second_role', 2)->get();
        $as_users = User::where('role', 4)->orWhere('second_role', 4)->get();

        $properties_user = Properties::select('transaction_m_id', 'asset_m_id', 'seller_id')->where('id', '=', $id)->first();


        $pdf = PDF::loadView('pdf.bankexport', compact('properties', 'properties_user', 'id', 'banks', 'tenants', 'schl_banks', 'tenancy_schedule_data', 'fake_bank', 'bank_all', 'propertiesExtra1s', 'comments_new',  'tr_users', 'as_users', 'main_properties', 'e_w', 'kk_idx'))->setPaper('a4', 'landscape');
        file_put_contents(public_path('Planungsspiegel.pdf'), $pdf->Output());
 //        return $pdf->stream('bankexport.pdf');
    }
}
