<?php
namespace App\Helpers;

use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


use App\Models\PropertiesDirectory;
use App\Models\PropertySubfields;
use App\User;
use App\Models\PDF;
use App\Models\Properties;



class PropertiesHelperClass
{

	public static function getPropertyDirectoryName($property){
		$tr_managerStr = "";
		$tr_manager = User::find($property->transaction_m_id);
		if($tr_manager){
			$words = explode(" ", $tr_manager->name);
			foreach ($words as $w) {
				$tr_managerStr .= $w[0];
			}
		}
		if ($property->plz_ort != null || $property->ort != null || $property->strasse != null || $property->hausnummer !== null || $tr_managerStr != "") {
			$new_folder_name = $property->plz_ort . " " . $property->ort . " " . $property->strasse . " " . $property->hausnummer . " (" . $tr_managerStr . ")";
		} else {
			$new_folder_name = "Keine Angabe #" . $property->id;
		}		
		if(str_replace(' ','',$new_folder_name) == "()")
		{
			$new_folder_name = "Keine Angabe #" . $property->id;
		}
		$new_folder_name = preg_replace('/[\/;?:@=&]/', '-', $new_folder_name); 
		return $new_folder_name;           
            
    }
    
    public static function createPropertyDirectories($propertyId){
        $property = Properties::find($propertyId);
        if(!$property){
            $result = ['status' => false, 'message' => 'Property does not exist.', 'data' => []];
            return $result;
        }

        $propertyStatusArr = config('properties.status_with_drive');                            
        if(!$property->status){
            $propertyStatusArr = $propertyStatusArr[15];
        }else{
            $propertyStatusArr = $propertyStatusArr[$property->status];
        }
        if($propertyStatusArr[1] == "")
        {
            $result = ['status' => false, 'message' => 'Property status directory not exist.'. $propertyStatusArr[0], 'data' => []];
            return $result;
        }
        $root_dir = $propertyStatusArr[1];
        $new_folder_name = PropertiesHelperClass::getPropertyDirectoryName($property);

        if ( ! $newDir = Storage::cloud()->makeDirectory($root_dir.'/'.$new_folder_name) ) {
            $result = ['status' => false, 'message' => 'Property directory not created.', 'data' => []];
            return $result;
        }
        $contents = collect(Storage::cloud()->listContents($root_dir, false));
        $new_property_dir = $contents->where('type', 'dir')->where('name', $new_folder_name)->sortByDesc('timestamp')->values()->first();
        // sortByDesc because there can multiple folder with same name so we get latest.


        PropertiesDirectory::create([
            'property_id'   => $propertyId,
            'parent_id'   => 0,
            'dir_name'      => $new_folder_name,
            'dir_basename'  => $new_property_dir['basename'],
            'dir_path'      => $new_property_dir['path']
        ]);
        $result = ['status' => true, 'message' => 'Property directory created successfually.', 'data' => []];
        return $result;
    }

	public static function createPropertySubDirectories($p_dir, $propertyId, $propertySubDirList = array()){
        if(empty($propertySubDirList)){
            $propertySubField = PropertySubfields::where('property_id', $propertyId)->first();
                    
            $propertySubDirList = config('properties.property_child_directories');
            $propertyRenameDirList = config('properties.property_rename_child_directories');
            if($propertySubField && $propertySubField->is_new_directory_strucure){
                $propertyRenameDirList = config('properties.property_rename_child_directories_new_2020');
                $propertySubDirList = config('properties.property_child_directories_new_2020');
            }
            PropertiesHelperClass::changePropertyDirName($propertyId, $propertyRenameDirList);
        }

		$contents = collect(Storage::cloud()->listContents($p_dir->dir_path, false));

        foreach($propertySubDirList as $dir){
            $temp = PropertiesDirectory::where('property_id', $propertyId)
                                ->where('parent_id', $p_dir->id)
                                ->where('dir_name', 'LIKE', $dir)->first();
            if(!$temp){
                $driveSubDir = $contents->where('type', 'dir')->where('name', $dir)->first();
                if(!$driveSubDir){
                    Storage::cloud()->makeDirectory($p_dir->dir_path.'/'.$dir);

                    $contents = collect(Storage::cloud()->listContents($p_dir->dir_path, false));
                    $driveSubDir = $contents->where('type', 'dir')->where('name', $dir)->sortByDesc('timestamp')->values()->first();
                }
                PropertiesDirectory::create([
                    'property_id'   => $propertyId,
                    'parent_id'   => $p_dir->id,
                    'dir_name'      => $dir,
                    'dir_basename'  => $driveSubDir['basename'],
                    'dir_path'      => $driveSubDir['path']
                ]);
            }

        }
        $result = ['status' => true, 'message' => 'Property sub directory created successfually.', 'data' => []];
        return $result;
	}

    public static function changePropertyDirAsStatus($propertyId, $newStatus){
        $property = Properties::find($propertyId);
        $p_dir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
		if ($p_dir->dir_path) {
			$contents = collect(Storage::cloud()->listContents($p_dir->dir_path, false));
			if ($contents) {
				$propertyStatusArr = config('properties.status_with_drive');
				if($property->property_status == 1){
					$newStatus = 18;
				}
				$propertyStatusArr = $propertyStatusArr[$newStatus];
				if ($propertyStatusArr[1] == "") {
					die('Property status directory not exist.' . $propertyStatusArr[0]);
				}
				$root_dir = $propertyStatusArr[1];
				Storage::cloud()->move($p_dir->dir_path, $root_dir . '/' . $p_dir->dir_name);
				$contents = collect(Storage::cloud()->listContents($root_dir, false));
				$new_property_dir = $contents->where('type', 'dir')->where('name', $p_dir->dir_name)->sortByDesc('timestamp')->values()->first();
				if($new_property_dir['basename']){
					$new_property_dir = $contents->where('type', 'dir')->where('name', $p_dir->dir_name)->sortByDesc('timestamp')->values()->first();
					$p_dir->dir_basename  = $new_property_dir['basename'];
					$p_dir->dir_path      = $new_property_dir['path'];
					$p_dir->save();
					DB::update('update `properties_directories` set `dir_path` = CONCAT("' . $p_dir->dir_path . '", "/", dir_basename) where `parent_id` = ' . $p_dir->id . '');
				}
				return true;
			}else{
				return false;
			}
		} else {
			return false;
		}
	}
	public static function deleteDuplicatePropertyDirName($propertyId)
	{
		$propertyMainDir = PropertiesDirectory::select('id', 'dir_name', DB::raw('group_concat(dir_path) as dirpath'), DB::raw('COUNT(dir_name) as dirname'))->where('property_id', $propertyId)->groupby('dir_name')->having('dirname', '>', 1)->get();
		$deleted = 0;
		// dd($propertyMainDir);
		foreach ($propertyMainDir as $key => $directoryName) {
			$str = explode(',', $directoryName->dirpath);

			$last = count($str);
			foreach (explode(',', $directoryName->dirpath) as $path) {

				$contents = collect(Storage::cloud()->listContents($path, false));
				$new_property_dir = $contents->where('type', 'dir')->where('name', $directoryName->dir_name)->sortByDesc('timestamp')->values()->first();
				if ($new_property_dir == null) {
					if ($last > 1) {
						Storage::cloud()->delete($path);
						PropertiesDirectory::where('id', $directoryName->id)->delete();
						$deleted++;
					}
				}
				$last--;
			}
		}
		return $deleted . 'Duplicate Directory Deleted in Google Drive';
	}
    public static function uploadLocalPdfToGdrive($propertyId){
        $propertyMainDir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
        if(!$propertyMainDir){
            return false;
        }
        $propertyAnhangeDir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', $propertyMainDir->id)->where('dir_name' , 'LIKE','Anhange')->first();
        if(!$propertyAnhangeDir){
            return false;
        }
        $path = $propertyAnhangeDir->dir_path;
        
        $properties_pdf = PDF::where('property_id', $propertyId)->get();
        foreach($properties_pdf as $pdf){
            if(is_null($pdf->file_basename)){
                $dir = 'pdf_upload';
                if (!file_exists($dir . '/' . $pdf->file_name)){
                 continue;   
                }

                $fileData = File::get($dir . '/' . $pdf->file_name);
                //upload file
                $file2 =  Storage::cloud()->put($path.'/'.$pdf->file_name, $fileData);

                //get file detail from google drive
                $contents = collect(Storage::cloud()->listContents($path, false));

                $file = $contents ->where('type','file')->where('name', $pdf->file_name)->sortByDesc('timestamp')->values()->first();

                $pdf->file_basename = $file['basename'];
                $pdf->file_path = $file['path'];
                $pdf->file_href = 'https://drive.google.com/file/d/'.$file['basename'];
                $pdf->extension = $file['extension'];
                $pdf->save();

                unlink($dir . '/' . $pdf->file_name);
            }
        }
    }
    
    
    public static function uploadLocalImagesToGdrive($propertyId)
	{
		$propertyMainDir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
		if (!$propertyMainDir) {
			return false;
		}
		$propertyAnhangeDir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', $propertyMainDir->id)->where('dir_name', 'LIKE', 'Ads')->first();
		if (!$propertyAnhangeDir) {
			return false;
		}
		$path = $propertyAnhangeDir->dir_path;
		//ads images moved local folder to gdir
		$ads_images = DB::table('ads')->where('property_id', $propertyId)->get();

		foreach ($ads_images as $image) {
			if ($image->images != null) {

				$arr = explode(',', $image->images);

				foreach ($arr as $img) {

					$dir = 'ad_files_upload';
					if (!file_exists(public_path() . '/' . $dir . '/' . $img)) {
						continue;
					}
					$fileData = File::get(public_path() . '/' . $dir . '/' . $img);
					//upload file
					$file2 =  Storage::cloud()->put($path . '/' . $img, $fileData);

					//get file detail from google drive
					$contents = collect(Storage::cloud()->listContents($path, false));

					$file = $contents->where('type', 'file')->where('name', $img)->sortByDesc('timestamp')->values()->first();


					$adsimages = [
						'ads_id'         => $image->id,
						'file_name' => $file['name'],
						'file_basename' => $file['basename'],
						'file_path' => $file['path'],
						'file_href' => 'https://drive.google.com/file/d/' . $file['basename'],
						'extension' => $file['extension'],
						"property_id"	=>	$image->property_id,
						"type"	=>	"ads",

					];
					DB::table('ads_images')->insert($adsimages);
					//delete images from db


					unlink(public_path() . '/' . $dir . '/' . $img);
				}
				$deleteimage = [
					'images'	=>	""
				];
				DB::table('ads')->where('id', $image->id)->update($deleteimage);
			}
		}

		//vads images moved local folder to gdir
		$vads_images = DB::table('vads')->where('property_id', $propertyId)->get();
		foreach ($vads_images as $image) {
			if ($image->images != null) {

				$arr = explode(',', $image->images);

				foreach ($arr as $img) {

					$dir = 'ad_files_upload';
					if (!file_exists(public_path() . '/' . $dir . '/' . $img)) {
						continue;
					}
					$fileData = File::get(public_path() . '/' . $dir . '/' . $img);
					//upload file
					$file2 =  Storage::cloud()->put($path . '/' . $img, $fileData);

					//get file detail from google drive
					$contents = collect(Storage::cloud()->listContents($path, false));

					$file = $contents->where('type', 'file')->where('name', $img)->sortByDesc('timestamp')->values()->first();


					$adsimages = [
						'ads_id'         => $image->id,
						'file_name' => $file['name'],
						'file_basename' => $file['basename'],
						'file_path' => $file['path'],
						'file_href' => 'https://drive.google.com/file/d/' . $file['basename'],
						'extension' => $file['extension'],
						"property_id"	=>	$image->property_id,
						"type"	=>	"vads",

					];

					DB::table('ads_images')->insert($adsimages);

					//delete images from db


					unlink(public_path() . '/' . $dir . '/' . $img);
				}
				$deleteimage = [
					'images'	=>	""
				];
				DB::table('vads')->where('id', $image->id)->update($deleteimage);
			}
		}
	}

	public static function changePropertyDirName($propertyId, $propertyRenameDirList)
	{
		$propertyMainDir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', 0)->first();
		if (!$propertyMainDir) {
			return false;
		}

		
		foreach ($propertyRenameDirList as $key => $directoryNewName) {

			$propertySubDir = PropertiesDirectory::where('property_id', $propertyId)->where('parent_id', $propertyMainDir->id)->where('dir_name', 'LIKE', $key)->first();
			if (!$propertySubDir) {
				continue;
			}
			$path = $propertySubDir->dir_path;
			Storage::cloud()->move($path, $directoryNewName);

			$contents = collect(Storage::cloud()->listContents($propertyMainDir->dir_path, false));
			$new_property_dir = $contents->where('type', 'dir')->where('name', $directoryNewName)->sortByDesc('timestamp')->values()->first();
			$propertySubDir->dir_name = $directoryNewName;
			$propertySubDir->dir_basename  = $new_property_dir['basename'];
			$propertySubDir->dir_path      = $new_property_dir['path'];
			$propertySubDir->save();
		}
		return 'Directory was renamed in Google Drive';
	}
}