<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyProject extends Model
{
    protected $fillable = ['name','property_id','user_id','description','note','date'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function employees()
    {
        return $this->belongsToMany(CompanyEmployee::class,'project_employees');
    }

    public function userEmployees()
    {
        return $this->belongsToMany(User::class,'project_employees');
    }

}
