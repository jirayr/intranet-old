<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccountUpload extends Model
{
    protected $guarded = [];


    public function property(){
        return $this->belongsTo('App\Models\Properties', 'property_id');
    }
}
