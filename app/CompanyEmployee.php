<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyEmployee extends Model
{
    protected $fillable = ['company_id','category','anrede','vorname','nachname','phone','fax','email','internet','notes','last_contact','keyword','responsibility','kennung'];

    public function employeeAddress()
    {
        return $this->hasOne('App\Address', 'employee_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }
}
