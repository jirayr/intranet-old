<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankTransaction extends Model
{
    protected  $fillable = ['account_id','amount','date_of_transaction','remaining_balance','uploaded_file_name','bank_transaction_type','end_balance_complete'];

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }


}
