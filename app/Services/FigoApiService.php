<?php
/**
 * Created by PhpStorm.
 * User: aliraza
 * Date: 11/08/2019
 * Time: 7:12 PM
 */

namespace App\Services;



use App\Repositories\AccessTokenRepository;
use GuzzleHttp\Client;
use Illuminate\Contracts\Logging\Log;

class FigoApiService
{
    protected $client;
    protected $accessTokenRepository;

    public function __construct(AccessTokenRepository $accessTokenRepository)
    {
        $this->client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),));
        $this->accessTokenRepository = $accessTokenRepository;
    }

    public function authenticate()
    {
        $encoded_Auth_Key = base64_encode(env('FIGO_CLIENT_ID') . ':' . env('FIGO_CLIENT_SECRET'));
        $response = $this->client->request(
            'POST',
            Constants::FIGO_API_URL . '/auth/token',
            [
                'headers' => [
                    'Authorization' => 'Basic ' . $encoded_Auth_Key,
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'grant_type' => 'password',
                    'username' => env('FIGO_API_USERNAME'),
                    'password' => env('FIGO_API_PASSWORD'),
                    'scope' => 'accounts=ro balance=ro transactions=ro offline'
                ]
            ]
        );
        $resultToken = json_decode($response->getBody(), true);
        $this->accessTokenRepository->updateByName('figo', $resultToken['access_token']);
        return $resultToken['access_token'];
    }

    public function getAllBankAccounts()
    {
        $accessToken = $this->accessTokenRepository->findByApiName('figo')->token;

        try {
            \Illuminate\Support\Facades\Log::info('In try '. $accessToken);
            $body = array(
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$accessToken,
                ]);
            $response = $this->client->request(
                'GET',
                Constants::FIGO_API_URL . '/rest/accounts', $body
            );

            return json_decode((string)$response->getBody(), true);
        } catch (\Exception $exception) {
            /*** To regenerate the access token ****/
            $this->authenticate();
            return $this->getAllBankAccounts();
        }

    }
}