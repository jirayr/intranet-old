<?php
namespace App\Services;

use App\Models\ExportHistory;
use App\User;
use DB;
use Auth;

class ExportHistoryService
{
	public static function create($section_type, $file_name) {
        DB::beginTransaction();

        try {

            $model = ExportHistory::create([
            	'user_id' 		=> Auth::user()->id,
            	'section_type' 	=> $section_type,
            	'file_name' 	=> $file_name,
            ]);

            DB::commit();

            return $model;
        } catch (\Exception $e) {
            DB::rollback();
            die($e->getMessage());
            return false;
        }
    }
}

?>