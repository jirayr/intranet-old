<?php

namespace App\Services;
use App\Models\Events;
use DB;
use Auth;
class EventsService
{


	public static function create($input) {
		DB::beginTransaction();
		try {		
			$event = Events::create( [
				'user_id' =>  Auth::id(),
				'name' => $input['name'] ,
				'description' => $input['description'] ,
				'start' =>  date('Y-m-d',strtotime($input['start'])),
				'end' => date('Y-m-d',strtotime($input['end'])) ,
			] );
			            
			DB::commit();

			return $event;
		} catch (\Exception $e) {
			DB::rollback();
			return($e->getMessage());
		}
	}

	public static function update( $input, $event) {
		DB::beginTransaction();
		try {
			$eventData = [
				'user_id' =>  Auth::id(),
				'name' => $input['name'] ,
				'description' => $input['description'] ,
				'start' =>  date('Y-m-d',strtotime($input['start'])),
				'end' =>date('Y-m-d',strtotime($input['end'])) ,
			];
			$event->update($eventData);
			DB::commit();
			return $event;
		} catch ( \Exception $e ) {
			var_dump( $e);
			DB::rollback();
			return false;
		}
    }
    public static function delete($event, $mes = null)
	{
		DB::beginTransaction();
		try {
			$event->delete();
			DB::commit();
			return true;
		} catch (\Exception $e) {
            DB::rollback();
            
			return false;
		}
	}

}

?>
