<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 5/1/2020
 * Time: 11:31 AM
 */

namespace App\Services;


use App\ProjectEmployee;
use App\Repositories\PropertyProjectRepository;

class PropertyProjectService
{
    protected  $propertyProjectRepository;

    public function __construct(PropertyProjectRepository $propertyProjectRepository)
    {
        $this->propertyProjectRepository = $propertyProjectRepository;
    }

    public function store($data)
    {
        $data['user_id']= Auth()->user()->id;
        $project = $this->propertyProjectRepository->store($data);
        foreach ( $data->employee_id as $eId)
        {
            ProjectEmployee::create([
                'property_project_id' => $project->id,
                'company_employee_id' => $eId,
            ]);
        }
        foreach ($data->employee_user as $uId)
        {
            ProjectEmployee::create([
                'property_project_id' => $project->id,
                'user_id' => $uId,
            ]);
        }



    }

    public function all()
    {
      return  $this->propertyProjectRepository->all();
    }

    public function getByPropertyId($id)
    {
        return  $this->propertyProjectRepository->getByPropertyId($id);
    }

    public function find($id)
    {
        return  $this->propertyProjectRepository->find($id);
    }

    public function update($id,$request)
    {
        $this->propertyProjectRepository->update($id,$request);
    }

    public function delete($id)
    {
        $this->propertyProjectRepository->delete($id);
    }






}
