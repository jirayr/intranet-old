<?php

namespace App\Services;


use App\Models\Masterliste;
use App\Models\Properties;
use DB;

class MasterlisteService
{

    public static function create($input) {
        DB::beginTransaction();

        try {
            $masterliste = Masterliste::create($input);
            //$tenant = TenantsService::calculate($tenant, $property);
            $masterliste->save();

            DB::commit();

            return $masterliste;
        } catch (\Exception $e) {
            DB::rollback();
            die($e->getMessage());
            return false;
        }
    }

    public static function update( $input, $masterliste) {
        DB::beginTransaction();
        try {
			 $masterlisteData = [
                'object' => $input['object'],
                'asset_manager' => $input['asset_manager'],
                'flat_in_qm' => $input['flat_in_qm'],
                'vacancy' => $input['vacancy'],
                'vacancy_structurally' => $input['vacancy_structurally'],
                'technical_ff' => $input['technical_ff'],
                'rented_area' => $input['rented_area'],
                'rent_net_pm' => $input['rent_net_pm'],
                'avg_rental_fee' => $input['avg_rental_fee'],
                'potential_pm' => $input['potential_pm'],
                'testneu' => $input['testneu']
            ];
            $masterliste->update($masterlisteData);
			$masterliste->save();
            DB::commit();

            return $masterliste;
        } catch ( \Exception $e ) {
            DB::rollback();
            return false;
        }
    }

    public static function calculate(Tenants $tenant, Properties $property){
        $wault = ((strtotime($tenant['mv_end']) - strtotime($property['duration_from']))/ 86400)/365;

        $rental_income = $tenant['nk_pa'] * $wault;

        $tenant['wault'] = $wault;
        $tenant['rental_income'] = $rental_income;

        return $tenant;

    }
	public static function delete( $masterliste ) {
		DB::beginTransaction();
		try {
			$masterliste->delete();
			
			DB::commit();

			return true;
		} catch ( \Exception $e ) {
			DB::rollback();

			return false;
		}
	}

    public static function calculate_masterliste($masterliste){
        $property_id = (Properties::where('masterliste_id', $masterliste->id)->first())
            ? Properties::where('masterliste_id', $masterliste->id)->first()->id
            :null;
        $tenancy_schedule_data = TenancySchedulesService::get($property_id);
        $tenancy_schedule = isset($tenancy_schedule_data['tenancy_schedules'][0]) ? $tenancy_schedule_data['tenancy_schedules'][0] : null;

        if($tenancy_schedule){
            $masterliste['flat_in_qm']=$tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10'];
            $masterliste['rented_area']=$tenancy_schedule->calculations['total_live_rental_space'] + $tenancy_schedule->calculations['total_business_rental_space'];
            $masterliste['vacancy'] = $tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $tenancy_schedule->calculations['total_business_vacancy_in_qm'];
            $masterliste['avg_rental_fee'] = (($tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10']) != 0)? ($tenancy_schedule->calculations['total_actual_net_rent'])/($tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10']) : 0;
            $masterliste['potential_pm'] = ($tenancy_schedule->calculations['total_business_vacancy_in_qm'] * $masterliste->avg_rental_fee * 12) + ($tenancy_schedule->calculations['total_live_vacancy_in_qm']*$masterliste->avg_rental_fee*12);
        }

        return $masterliste;
    }

    public static function sync_all_masterliste(){
        $masterlistes = Masterliste::all();
        foreach($masterlistes as $masterliste){

            $input = $masterliste;
            $input = MasterlisteService::calculate_masterliste($input);
            MasterlisteService::update( $input, $masterliste );
        }
    }
}