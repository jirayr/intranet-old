<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 2/7/2020
 * Time: 4:35 PM
 */

namespace App\Services;


use App\Repositories\OutlookAccessTokenRepository;
use Carbon\Carbon;
use GuzzleHttp\Client;

class OutlookHTTPService
{
    protected $outlookAccessTokenRepository;
    public function __construct(OutlookAccessTokenRepository $outlookAccessTokenRepository)
    {
        $this->client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),));
        $this->outlookAccessTokenRepository = $outlookAccessTokenRepository;
    }

    public function authenticate($code)
    {
        $response = $this->client->request(
            'POST',
            'https://login.microsoftonline.com/common/oauth2/v2.0/token',
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Host' => 'login.microsoftonline.com'
                ],
                'form_params' => [
                    'client_id' => 'ad2b7d19-160a-4703-ae2d-51e28232e849',
                    'client_secret' => 'BSzvye@AuZR5@UW7@MFL19MUSdwCoA:=',
                    'code' => $code,
                    'grant_type' => 'authorization_code'
                ]
            ]
        );
        $resultToken = json_decode($response->getBody(), true);
       return $this->createOrUpdateToken($resultToken);
    }

    public function createOrUpdateToken($resultToken)
    {
        $this->outlookAccessTokenRepository->ifExist()?
        $this->outlookAccessTokenRepository->update($resultToken):
        $this->outlookAccessTokenRepository->store($resultToken);
        return redirect(url('/dashboard'));
    }

    public function isValidToken()
    {
        if ((Carbon::now())->greaterThanOrEqualTo(Carbon::parse($this->outlookAccessTokenRepository->getToken()->validity)))
        {
            $this->refreshToken();
        }
    }

    public function getCategories()
    {
        $this->isValidToken();
        $response = $this->client->request(
            'GET',
            'https://graph.microsoft.com/beta/me/outlook/masterCategories',
            [
                'headers' => [
                    'Authorization' => 'Bearer '.$this->outlookAccessTokenRepository->getToken()->access_token,
                    'Content-Type' => 'application/json'
                    ],

            ]
        );
        return json_decode($response->getBody(), true);
    }

    public function getMailsByCategory($category)
    {
        $this->isValidToken();
        $response = $this->client->request(
            'GET',
            'https://graph.microsoft.com/v1.0/me/messages?$search='.$category,
            [
                'headers' => [
                    'Authorization' => 'Bearer '.$this->outlookAccessTokenRepository->getToken()->access_token,
                    'Content-Type' => 'application/json'
                ],
            ]
        );

        return json_decode($response->getBody(), true);
    }

    public function redirectToAuthFlow()
    {
          return redirect()->away('https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=ad2b7d19-160a-4703-ae2d-51e28232e849&redirect_uri='.url('/getCode').'&response_type=code&scope=offline_access MailboxSettings.ReadWrite');
    }


    public function refreshToken()
    {
        $response = $this->client->request(
            'POST',
            'https://login.microsoftonline.com/common/oauth2/v2.0/token',
            [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Host' => 'login.microsoftonline.com'
                ],
                'form_params' => [
                    'client_id' => 'ad2b7d19-160a-4703-ae2d-51e28232e849',
                    'client_secret' => 'BSzvye@AuZR5@UW7@MFL19MUSdwCoA:=',
                    'refresh_token' => $this->outlookAccessTokenRepository->getToken()->access_token->refresh_token,
                    'grant_type' => 'refresh_token'
                ]
            ]
        );
        $resultToken = json_decode($response->getBody(), true);
        return $this->createOrUpdateToken($resultToken);
    }

}