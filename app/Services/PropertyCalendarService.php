<?php

namespace App\Services;
use App\Models\PropertyCalendar;
use DB;
use Auth;
class PropertyCalendarService
{


	public static function create($input) {
		DB::beginTransaction();
		try {		
			$PropertyCalendar = PropertyCalendar::create( [
				'user_id' =>  Auth::id(),
				'property_id' => $input['property_id'] ,
				'description' => $input['description'] ,
				'start' =>  date('Y-m-d',strtotime($input['start'])),
				'end' => date('Y-m-d',strtotime($input['end'])) ,
			] );
			            
			DB::commit();
			return $PropertyCalendar;
		} catch (\Exception $e) {
			DB::rollback();
			return($e->getMessage());
		}
	}

	public static function update( $input, $PropertyCalendar) {
		DB::beginTransaction();
		try {
			$calendarData = [
				'user_id' =>  Auth::id(),
				'property_id' => $input['property_id'] ,
				'description' => $input['description'] ,
				'start' =>  date('Y-m-d',strtotime($input['start'])),
				'end' => date('Y-m-d',strtotime($input['end'])) ,
			];
			$PropertyCalendar->update($calendarData);
			DB::commit();
			return $PropertyCalendar;
		} catch ( \Exception $e ) {
			var_dump( $e);
			DB::rollback();
			return false;
		}
    }
    public static function delete($PropertyCalendar, $mes = null)
	{
		DB::beginTransaction();
		try {
			$PropertyCalendar->delete();
			DB::commit();
			return true;
		} catch (\Exception $e) {
            DB::rollback();
            
			return false;
		}
	}

}

?>
