<?php
/**
 * Created by PhpStorm.
 * User: aliraza
 * Date: 11/08/2019
 * Time: 7:12 PM
 */

namespace App\Services;



use App\Repositories\AccessTokenRepository;
use GuzzleHttp\Client;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Support\Carbon;
use GuzzleHttp\RequestOptions;

class FinApiService
{
    protected $client;
    protected $accessTokenRepository;

    public function __construct(AccessTokenRepository $accessTokenRepository)
    {
        $this->client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),));
        $this->accessTokenRepository = $accessTokenRepository;
    }

    public function authenticate()
    {
        $response = $this->client->request(
            'POST',
            Constants::FIN_API_URL,
            [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => env('FIN_CLIENT_ID'),
                    'client_secret' => env('FIN_CLIENT_SECRET'),
                    'username' =>'user123',
                    'password' =>'user123',
                ]
            ]
        );
        $resultToken = json_decode($response->getBody(), true);
        $this->createOrUpdateToken($resultToken);
    }

    public function createOrUpdateToken($resultToken)
    {
        $this->accessTokenRepository->ifExist()?
        $this->accessTokenRepository->update($resultToken):
        $this->accessTokenRepository->store($resultToken);
    }

    public function isValidToken()
    {
        if ((Carbon::now())->greaterThanOrEqualTo(Carbon::parse($this->accessTokenRepository->getToken()->validity)))
        {
            $this->authenticate();
        }
    }

    public function refreshAccessToken()
    {
        $response = $this->client->request(
            'POST',
            Constants::FIN_API_URL,
            [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'client_id' => env('FIN_CLIENT_ID'),
                    'client_secret' => env('FIN_CLIENT_SECRET'),
                    'refresh_token' => $this->accessTokenRepository->getToken()->refresh_token,
                ]
            ]
        );
        $resultToken = json_decode($response->getBody(), true);
        $this->createOrUpdateToken($resultToken);
    }

    public function getAllBanks()
    {
        $this->isValidToken();
        $response = $this->client->request(
            'GET',
            Constants::FIN_API_ALL_BANKS_URL,
            [
                'headers' => [
                    'Authorization' => 'Bearer '.$this->accessTokenRepository->getToken()->access_token,
                ],
            ]
        );
        $resultToken = json_decode($response->getBody(), true);
        return $resultToken;
    }

    public function getAllBankConnections()
    {
        $this->isValidToken();
        $response = $this->client->request(
            'GET',
            Constants::GET_ALL_BANK_CONNECTION_URL,
            [
                'headers' => [
                    'Authorization' => 'Bearer '.$this->accessTokenRepository->getToken()->access_token,
                ],
            ]
        );
        return json_decode($response->getBody(), true);
    }


    public function importBankConnection($data)
    {
        $this->isValidToken();
        try {
            $response = $this->client->request(
                'POST',
                Constants::IMPORT_BANK_CONNECTION_URL,
                [
                    'headers' => [
                        'Authorization' => 'Bearer '.$this->accessTokenRepository->getToken()->access_token,
                    ],

                    RequestOptions::JSON  => [
                        'bankId' => $data->bankId,
                        'name' => $data->connectionName
                    ]
                ]
            );

            $response = theMethodMayThrowException();
            return json_decode($response->getBody(), true);
        } catch (\GuzzleHttp\Exception\RequestException $e) {

            if ($e->hasResponse())
            {
                $response = $e->getResponse();
                if ($response->getStatusCode() == '451')
                {
                    $header = $response->getHeaders();
                    return redirect($header['Location'][0].'?redirectUrl='.url('/bank-connection'));

                }elseif($response->getStatusCode() == '422')

                    {
                        $response = json_decode($response->getBody(), true);
                        return redirect('/bank-connection')->with('error',$response['errors'][0]['message']);
                    }
            }
        }
    }


    public function getBank($bankId)
    {
        $this->isValidToken();
        $response = $this->client->request(
            'GET',
            Constants::GET_SINGLE_BANK.$bankId,
            [
                'headers' => [
                    'Authorization' => 'Bearer '.$this->accessTokenRepository->getToken()->access_token,
                ],

            ]
        );
        return json_decode($response->getBody(), true);
    }


    public function getAccount($accountIds)
    {
        $this->isValidToken();
        $response = $this->client->request(
            'GET',
            Constants::GET_BANK_ACCOUNT.$accountIds,
            [
                'headers' => [
                    'Authorization' => 'Bearer '.$this->accessTokenRepository->getToken()->access_token,
                ],

            ]
        );
        return json_decode($response->getBody(), true);
    }


}