<?php

namespace App\Services;


use App\Models\Notifications;
use App\Models\NotificationStatuses;
use App\Models\Properties;
use App\User;
use Illuminate\Support\Facades\Auth;
use DB;

class NotificationsService
{
    public static function getAllNotifications(){
        $results = [];
//        $notifications = Notifications::all();

        $user = Auth::user();
        if($user)
        {

        $notifications = Notifications::where('created_user_id' , '!=', Auth::user()->id)->limit(20)->get();
        $seen_notification_ids = NotificationStatuses::where('user_id', Auth::id())->pluck('notification_id')->toArray();
        foreach ($notifications as $notification){

            $property = Properties::where('id', $notification->property_id)->first();
            if(!isset($property))
                return [];
            $notification->property_name = $property->name_of_property;

            $property_status = $property->status;
            switch ($property_status) {
                case config('properties.status.buy'): //1
                    $property_status = 'Buy';
                    break;
                case config('properties.status.hold'): //2
                    $property_status = 'Hold';
                    break;
                case config('properties.status.decline'): //3
                    $property_status = 'Decline';
                    break;
                default:
                    break;

            }

            $created_user = User::where('id', $notification->created_user_id)->first();
            $notification->created_user_name = $created_user->name;
            $notification->created_user_avatar = config('upload.avatar_path') . $created_user->image;

            switch ($notification->type) {
                case config('notification.type.create_property'): //1
                    $notification->content = "User $notification->created_user_name added property $notification->property_name";
                    break;
                case config('notification.type.edit_property'): //2
                    $notification->content = "User $notification->created_user_name edited property $notification->property_name";
                    break;
                case config('notification.type.change_property_status'): //3
                    $notification->content = "Admin changed status of property $notification->property_name to $property_status";
                    break;
                default:
                    break;
            }

            $result = [
                'id'        => $notification->id,
                'created_user_id' => $notification->created_user_id,
                'created_at' => date('g:i A', strtotime($notification->created_at)),
                'updated_at' => $notification->updated_at,
                'property_id' => $notification->property_id,
                'property_name' => $notification->property_name,
                'status' => $notification->status,
                'created_user_name' => $notification->created_user_name,
                'created_user_avatar' => $notification->created_user_avatar,
                'content' => $notification->content,
                'type' => $notification->type
            ];

            if (in_array($notification->id, $seen_notification_ids)) {
                $result['is_seen'] = true;
            } else {
                $result['is_seen'] = false;
            }

            array_push($results, $result);
        }
        }
        return array_reverse($results);
    }

    public static function create($input) {
        DB::beginTransaction();
        try {
            $notification = Notifications::create( [
                'created_user_id' =>  Auth::id(),
                'type' => $input['type'] ,
                'property_id' => $input['property_id'] ,
            ] );

            DB::commit();

            return $notification;
        } catch (\Exception $e) {
            DB::rollback();
            return($e->getMessage());
        }
    }

    public static function seenNotification($notification_id){
        DB::beginTransaction();
        try {
            $event = NotificationStatuses::create( [
                'notification_id' =>  $notification_id,
                'user_id' => Auth::id(),
                'status' => null
            ] );

            DB::commit();

            return $event;
        } catch (\Exception $e) {
            DB::rollback();
            return($e->getMessage());
        }
    }

    public static function countNewNotifications(){
        $notifications = NotificationsService::getAllNotifications();
        $result = 0;
        foreach ($notifications as $notification){
            if ($notification['is_seen'] == false){
                $result++;
            }
        }
        return $result;
    }
}