<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 3/31/2020
 * Time: 5:45 PM
 */

namespace App\Services;


use App\Repositories\EmailMappingRepository;

class EmailMappingService
{
    protected $emailMappingRepository;

    public function __construct(EmailMappingRepository $emailMappingRepository)
    {
        $this->emailMappingRepository = $emailMappingRepository;
    }


    public function getEmailsByAccountAndEmailType($accountId,$emailTypeId)
    {
      return  $this->emailMappingRepository->getEmailsByAccountAndEmailType($accountId,$emailTypeId);
    }

    public function getAllEmails($emailTypeId)
    {
     return $this->emailMappingRepository->getAllEmails($emailTypeId);
    }

    public function emailSearch($data)
    {
      return $this->emailMappingRepository->emailSearch($data);
    }

}