<?php

namespace App\Services;
use App\User;
use DB;

class UserService
{


	public static function create($input) {
		DB::beginTransaction();

		try {
			$user = User::create( [
				'name' => $input['name'],
				'email' => $input['email'],
				'phone' => $input['phone'],
				'mobile' => $input['mobile'],
				'company' => $input['company'],
				'role' => config('auth.role.user'),
				'password' => bcrypt($input['password']),
				'signature'=> $input['signature'],
				'image' => 'no-avatar.png',
				'user_status' => (isset($input['user_status'])) ? $input['user_status'] : 1
			] );

			DB::commit();

			return $user;
		} catch (\Exception $e) {
			DB::rollback();
			die($e->getMessage());
			return false;
		}
	}

	public static function update( $input, $user ) {
		DB::beginTransaction();
		
		try {
			if($input['password'])
			{
				$userData = [
					'name' => $input['name'],
					'email' => $input['email'],
					'password' => bcrypt($input['password']),
					'role' => $input['role'],
					'company' => $input['company'],
				
					'phone' => $input['phone'],
					'mobile' => $input['mobile'],
					'signature'=> $input['signature'],
					'second_role' => $input['second_role'],
					'user_status' => (isset($input['user_status'])) ? $input['user_status'] : 1,
				];	
			}
			else{
				$userData = [
					'name' => $input['name'],
					'email' => $input['email'],
					'role' => $input['role'],
					'company' => $input['company'],
				
					'phone' => $input['phone'],
					'mobile' => $input['mobile'],
					'signature'=> $input['signature'],
					'second_role' => $input['second_role'],
					'user_status' => (isset($input['user_status'])) ? $input['user_status'] : 1,
				];
			}
			

			$user->update($userData);

			DB::commit();

			return $user;
		} catch ( \Exception $e ) {
						
			DB::rollback();
				
			return false;
		}
	}


	public static function delete($user, $mes = null)
	{
		DB::beginTransaction();
		try {
			// $user->delete();
			$user->update(['user_deleted' => 1]);
			
			DB::commit();

			return true;
		} catch (\Exception $e) {
			DB::rollback();

			return false;
		}
	}


	public static function updateProfile( $input, $user ) {
		DB::beginTransaction();
		try {
			$userData = [
				'name' => $input['name'],
				'email' => $input['email'],
				'image' => $input['image'],
				'first_name' => $input['first_name'],
				'last_name' => $input['last_name'],
				'birthday' => $input['birthday'],
				'phone' => $input['phone'],
				'mobile' => $input['mobile'],
				'address' => $input['address'],
				'gender' => $input['gender'],
				'website' => $input['website'],
				'postal_code' => $input['postal_code'],
//				'signature'=> $input['signature'],
			];

			$user->update($userData);

			DB::commit();

			return $user;
		} catch ( \Exception $e ) {
			DB::rollback();

			return false;
		}
	}


}

?>
