<?php

namespace App\Services;

use DB;
class ForecastsService
{
    public static function update( $input, $forecast) {
        DB::beginTransaction();
        try {
            $forecast_data = [
                'name' => $input['name'],
                'missing_info' => $input['missing_info'],
                'todo'  => $input['todo'],
                'credit_fcr' => $input['credit_fcr'],
                'credit_spv' => $input['credit_spv'],
                'possible_transfer' => $input['possible_transfer'],
                'buffer' => $input['buffer'],
                'available_1' => $input['available_1'],
                'available_2' => $input['available_2'],
                'available_3' => $input['available_3'],
                'available_4' => $input['available_4'],
                'available_5' => $input['available_5'],
                'available_6' => $input['available_6'],
                'available_7' => $input['available_7'],
                'available_8' => $input['available_8'],
                'available_9' => $input['available_9'],
                'available_10' => $input['available_10'],
                'available_11' => $input['available_11'],
                'available_12' => $input['available_12'],
            ];

            $forecast->update($forecast_data);

            $forecast->save();

            DB::commit();

            return $forecast;
        } catch ( \Exception $e ) {
            DB::rollback();

            return false;
        }
    }
}