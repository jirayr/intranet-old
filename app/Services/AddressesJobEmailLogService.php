<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 4/22/2020
 * Time: 12:09 PM
 */

namespace App\Services;


use App\AddressesJobEmailLog;
use App\Repositories\AddressesJobEmailLogRepository;

class AddressesJobEmailLogService
{
    protected $addressesJobEmailLogRepository;

    public function __construct(AddressesJobEmailLogRepository $addressesJobEmailLogRepository)
    {
        $this->addressesJobEmailLogRepository = $addressesJobEmailLogRepository;
    }

    public function store($emails,$addressObj)
    {
        foreach ($emails as $email)
        {
            $this->addressesJobEmailLogRepository->store($email,$addressObj);
        }
    }

    public function getAll()
    {
       return $this->addressesJobEmailLogRepository->get();
    }

    public function find($addressesJobId)
    {
        return $this->addressesJobEmailLogRepository->getByAddressesJobId($addressesJobId);
    }

    public function getAllMails()
    {
        return $this->addressesJobEmailLogRepository->getAllMails();
    }
    public function getByPropertyId()
    {
        return $this->addressesJobEmailLogRepository->getByPropertyId();
    }



}
