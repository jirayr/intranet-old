<?php


namespace App\Services;
use App\Models\Properties;
use App\Models\Tenants;
use DB;
use Auth;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

error_reporting(E_ALL ^ E_NOTICE);

class PropertiesService
{


	public static function create($input) {
		DB::beginTransaction();
		
		try {
			$property = Properties::create( [
				'user_id' =>  isset($input['user_id']) ? $input['user_id'] : Auth::id(),
				'transaction_m_id' =>  isset($input['transaction_m_id']) ? $input['transaction_m_id'] : "",
				'asset_m_id' =>  isset($input['asset_m_id']) ? $input['asset_m_id'] : "",
				'seller_id' =>  isset($input['seller_id']) ? $input['seller_id'] : "",

				'firma' =>  isset($input['firma']) ? $input['firma'] : "",
				'offer_from' =>  isset($input['offer_from']) ? $input['offer_from'] : "",
				'email' =>  isset($input['email']) ? $input['email'] : "",
				'phone' =>  isset($input['phone']) ? $input['phone'] : "",
				'contact' =>  isset($input['contact']) ? $input['contact'] : "",
				
				'net_rent' => $input['net_rent'] / 100,
				'net_rent_empty' => $input['net_rent_empty'] / 100,
				'maintenance' => $input['maintenance'] / 100,
				'operating_costs' => $input['operating_costs'] / 100,
				'object_management' => $input['object_management'] / 100,
				'tax' => $input['tax'] / 100,
				'plot_of_land_m2' => $input['plot_of_land_m2'],
				'construction_year' => $input['construction_year'],
				'building' => $input['building'] / 100,
				'plot_of_land' => $input['plot_of_land'] / 100,
				'real_estate_taxes' => $input['real_estate_taxes'] / 100,
				'estate_agents' => $input['estate_agents'] / 100,
				'notary_land_register' => $input['notary_land_register'] / 100,
				'evaluation' => $input['evaluation'] / 100,
				'others' => $input['others'] / 100,
				'buffer' => $input['buffer'] / 100,
				'rent' => $input['rent'],
				'rent_retail' => $input['rent_retail'],
				'rent_office' => $input['rent_office'],
				'rent_industry' => $input['rent_industry'],
				'rent_whg' => $input['rent_whg'],
				'vacancy' => $input['vacancy'],
				'vacancy_retail' => $input['vacancy_retail'],
				'vacancy_office' => $input['vacancy_office'],
				'vacancy_industry' => $input['vacancy_industry'],
				'vacancy_whg' => $input['vacancy_whg'],
				'total_commercial_sqm' => $input['total_commercial_sqm'],
				'wault' => $input['wault'],
				'anchor_tenant' => $input['anchor_tenant'],
				'plot' => $input['plot'],
				'with_real_ek' => $input['with_real_ek'] / 100,
				'from_bond' => $input['from_bond'] / 100,
				'bank_loan' => $input['bank_loan'] / 100,
				'interest_bank_loan' => $input['interest_bank_loan'] / 100,
				'eradication_bank' => $input['eradication_bank'] / 100,
				'interest_bond' => $input['interest_bond'] / 100,
				'maintenance_nk' => $input['maintenance_nk'] / 100,
				'operating_costs_nk' => $input['operating_costs_nk'] / 100,
				'object_management_nk' => $input['object_management_nk'] / 100,
				'depreciation_nk' => $input['depreciation_nk'] / 100,
				'property_value' => $input['property_value'] / 100,
				'nk_anchor_tenants' => $input['nk_anchor_tenants'],
				'nk_tenant1' => $input['nk_tenant1'],
				'nk_tenant2' => $input['nk_tenant2'],
				'nk_tenant3' => $input['nk_tenant3'],
				'nk_tenant4' => $input['nk_tenant4'],
				'nk_tenant5' => $input['nk_tenant5'],
				'mv_anchor_tenants' => date('Y-m-d H:i:s', strtotime($input['mv_anchor_tenants'])),
				'mv_tenant1' => date('Y-m-d H:i:s', strtotime($input['mv_tenant1'])),
				'mv_tenant2' => date('Y-m-d H:i:s', strtotime($input['mv_tenant2'])),
				'mv_tenant3' => date('Y-m-d H:i:s', strtotime($input['mv_tenant3'])),
				'mv_tenant4' => date('Y-m-d H:i:s', strtotime($input['mv_tenant4'])),
				'mv_tenant5' => date('Y-m-d H:i:s', strtotime($input['mv_tenant5'])),
				'status_update_date' => date('Y-m-d'),
				'total_purchase_price' => $input['total_purchase_price'],
				'duration_from' => date('Y-m-d H:i:s', strtotime($input['duration_from'])),
				'land_value'=>$input['land_value'],
				'net_rent_pa'=>$input['net_rent_pa'],
				'city_place'=>$input['city_place'],
				'WHG_qm_of_WAULT'=>$input['WHG_qm_of_WAULT'],
				'WHG_qm_of_anchor_tenant'=>$input['WHG_qm_of_anchor_tenant'],
				'WHG_qm_of_plot'=>$input['WHG_qm_of_plot'],
				'name_of_property'=>$input['name_of_property'],
				'name_of_creator'=>$input['name_of_creator'],
				'status' => (isset($input['status'])) ? $input['status'] : 15,
				'quick_status' => 1,
				'bank_ids'=>json_encode($input['bank_ids']),
				'masterliste_id'=>$input['masterliste_id'],
				'ground_reference_value_in_euro_m2'=>$input['ground_reference_value_in_euro_m2'],
				'duration_from_O38'=>$input['duration_from_O38'],
				'Grundbuch'=>$input['Grundbuch'],
				'Ref_CF_Salzgitter'=>$input['Ref_CF_Salzgitter'],
				'Ref_GuV_Salzgitter'=>$input['Ref_GuV_Salzgitter'],
				'AHK_Salzgitter'=>$input['AHK_Salzgitter']
			] );
			
			$property = PropertiesService::caculate($property);
			$property->save();
			
			DB::commit();

			return $property;
		} catch (\Exception $e) {
			DB::rollback();
			die($e->getMessage());
			return false;
		}
	}

	public static function update( $input, $property) {
		DB::beginTransaction();
		//try {
			$propertyData = [
				'user_id' =>  isset($input['user_id']) ? $input['user_id'] : Auth::id(),
				'transaction_m_id' =>  isset($input['transaction_m_id']) ? $input['transaction_m_id'] : "",
				'asset_m_id' =>  isset($input['asset_m_id']) ? $input['asset_m_id'] : "",
				'seller_id' =>  isset($input['seller_id']) ? $input['seller_id'] : "",
				'type_of_property' =>  isset($input['type_of_property']) ? $input['type_of_property'] : "",
				'net_rent' => $input['net_rent'] / 100,
				'net_rent_empty' => $input['net_rent_empty'] / 100,
				'maintenance' => $input['maintenance'] / 100,
				'operating_costs' => $input['operating_costs'] / 100,
				'object_management' => $input['object_management'] / 100,
				'tax' => $input['tax'] / 100,
				'plot_of_land_m2' => $input['plot_of_land_m2'],
				'construction_year' => $input['construction_year'],
				'building' => $input['building'] / 100,
				'plot_of_land' => $input['plot_of_land'] / 100,
				'real_estate_taxes' => $input['real_estate_taxes'] / 100,
				'estate_agents' => $input['estate_agents'] / 100,
				'notary_land_register' => $input['notary_land_register'] / 100,
				'evaluation' => $input['evaluation'] / 100,
				'others' => $input['others'] / 100,
				'buffer' => $input['buffer'] / 100,
				'rent' => $input['rent'],
                'rent_retail' => $input['rent_retail'],
                'rent_office' => $input['rent_office'],
                'rent_industry' => $input['rent_industry'],
				'rent_whg' => $input['rent_whg'],
				'vacancy' => $input['vacancy'],
                'vacancy_retail' => $input['vacancy_retail'],
                'vacancy_office' => $input['vacancy_office'],
                'vacancy_industry' => $input['vacancy_industry'],
				'vacancy_whg' => $input['vacancy_whg'],
				'total_commercial_sqm' => $input['total_commercial_sqm'],
				'wault' => $input['wault'],
				'anchor_tenant' => $input['anchor_tenant'],
				'plot' => $input['plot'],
				'with_real_ek' => $input['with_real_ek'] / 100,
				'from_bond' => $input['from_bond'] / 100,
				'bank_loan' => $input['bank_loan'] / 100,
				'interest_bank_loan' => $input['interest_bank_loan'] / 100,
				'eradication_bank' => $input['eradication_bank'] / 100,
				'interest_bond' => $input['interest_bond'] / 100,
				'maintenance_nk' => $input['maintenance_nk'] / 100,
				'operating_costs_nk' => $input['operating_costs_nk'] / 100,
				'object_management_nk' => $input['object_management_nk'] / 100,
				'depreciation_nk' => $input['depreciation_nk'] / 100,
				'property_value' => $input['property_value'] / 100,
				'nk_anchor_tenants' => $input['nk_anchor_tenants'],
				'nk_tenant1' => $input['nk_tenant1'],
				'nk_tenant2' => $input['nk_tenant2'],
				'nk_tenant3' => $input['nk_tenant3'],
				'nk_tenant4' => $input['nk_tenant4'],
				'nk_tenant5' => $input['nk_tenant5'],
                'nk_pm_anchor_tenants' => $input['nk_pm_anchor_tenants'],
                'nk_pm_tenant1' => $input['nk_pm_tenant1'],
                'nk_pm_tenant2' => $input['nk_pm_tenant2'],
                'nk_pm_tenant3' => $input['nk_pm_tenant3'],
                'nk_pm_tenant4' => $input['nk_pm_tenant4'],
                'nk_pm_tenant5' => $input['nk_pm_tenant5'],
                'mv_begin_anchor_tenants' => date('Y-m-d H:i:s', strtotime($input['mv_begin_anchor_tenants'])),
                'mv_begin_tenant1' => date('Y-m-d H:i:s', strtotime($input['mv_begin_tenant1'])),
                'mv_begin_tenant2' => date('Y-m-d H:i:s', strtotime($input['mv_begin_tenant2'])),
                'mv_begin_tenant3' => date('Y-m-d H:i:s', strtotime($input['mv_begin_tenant3'])),
                'mv_begin_tenant4' => date('Y-m-d H:i:s', strtotime($input['mv_begin_tenant4'])),
                'mv_begin_tenant5' => date('Y-m-d H:i:s', strtotime($input['mv_begin_tenant5'])),
				'mv_anchor_tenants' => date('Y-m-d H:i:s', strtotime($input['mv_anchor_tenants'])),
				'mv_tenant1' => date('Y-m-d H:i:s', strtotime($input['mv_tenant1'])),
				'mv_tenant2' => date('Y-m-d H:i:s', strtotime($input['mv_tenant2'])),
				'mv_tenant3' => date('Y-m-d H:i:s', strtotime($input['mv_tenant3'])),
				'mv_tenant4' => date('Y-m-d H:i:s', strtotime($input['mv_tenant4'])),
				'mv_tenant5' => date('Y-m-d H:i:s', strtotime($input['mv_tenant5'])),
				'total_purchase_price' => $input['total_purchase_price'],
				'duration_from' => date('Y-m-d H:i:s', strtotime($input['duration_from'])),
				'land_value'=>$input['land_value'],
				'net_rent_pa'=>$input['net_rent_pa'],
				'city_place'=>$input['city_place'],
				'WHG_qm_of_WAULT'=>$input['WHG_qm_of_WAULT'],
				'WHG_qm_of_anchor_tenant'=>$input['WHG_qm_of_anchor_tenant'],
				'WHG_qm_of_plot'=>$input['WHG_qm_of_plot'],
				'duration_from_O38' => date('Y-m-d H:i:s', strtotime($input['duration_from_O38'])),
				'Ref_CF_Salzgitter'=>$input['Ref_CF_Salzgitter'],
				'Ref_GuV_Salzgitter'=>$input['Ref_GuV_Salzgitter'],
				'AHK_Salzgitter'=>$input['AHK_Salzgitter'],

				'gewerbe'=>$input['gewerbe'],
				'gewerbe1'=>$input['gewerbe1'],
				'gewerbe2'=>$input['gewerbe2'],
				'buropraxen'=>$input['buropraxen'],
				'buropraxen1'=>$input['buropraxen1'],
				'buropraxen2'=>$input['buropraxen2'],
				'wohnungen'=>$input['wohnungen'],
				'wohnungen1'=>$input['wohnungen1'],
				'wohnungen2'=>$input['wohnungen2'],
				'lager'=>$input['lager'],
				'lager1'=>$input['lager1'],
				'lager2'=>$input['lager2'],
				'stellplatze'=>$input['stellplatze'],
				'stellplatze1'=>$input['stellplatze1'],
				'stellplatze2'=>$input['stellplatze2'],
				'sonstiges'=>$input['sonstiges'],
				'sonstige_flache'=>$input['sonstige_flache'],
				'plz_ort'=>$input['plz_ort'],
				'ort'=>$input['ort'],
				'sonstiges2'=>$input['sonstiges1'],
				'sonstiges2'=>$input['sonstiges2'],
				'population_edited'=>$input['population_edited'],
				'kk_idx'=>$input['kk_idx'],
				'ankermieter_option1'=>$input['ankermieter_option1'],
				'ankermieter_option2'=>$input['ankermieter_option2'],
				'construction_year_note'=>$input['construction_year_note'],
				'purchase_date2'=>$input['purchase_date2'],
				'purchase_date'=>$input['purchase_date'],
				'miete_text'=>$input['miete_text'],
				'plot_of_land_m2_text'=>$input['plot_of_land_m2_text'],
				'miscellaneous_title'=>$input['miscellaneous_title'],
				'buffer_title'=>$input['buffer_title'],
				'gastronomie'=>$input['gastronomie'],
				'gastronomie1'=>$input['gastronomie1'],
				'gastronomie2'=>$input['gastronomie2'],
				'datum_lol'=>$input['datum_lol'],
				'sheet_title'=>$input['sheet_title'],
				'portfolio'=>$input['portfolio'],
				'swot_json'=>$input['swot_json'],
				'hv_kündbar_bis'=>$input['hv_kündbar_bis'],
				'hv_vertrag_abgeschlossen'=>$input['hv_vertrag_abgeschlossen'],
				

			];



			$property->update($propertyData);
			
			$property = PropertiesService::caculate($property);
			$property->save();

            //update all tenants
            $property_id = $property->id;
            $tenants = Tenants::where('property_id', $property_id)->get();
            if (count($tenants) > 0){
                foreach ($tenants as $tenant){
                	$tenant = TenantsService::calculate($tenant, $property);
                	$tenant->save();

				}
			}
			
			DB::commit();

			return $property;
//		} catch ( \Exception $e ) {
//
//			echo $e->getMessage();
//			DB::rollback();
//
//			return false;
//		}
	}


//	public static function delete($user, $mes = null)
//	{
//		DB::beginTransaction();
//		try {
//			$user->delete();
//			DB::commit();
//
//			return true;
//		} catch (\Exception $e) {
//			DB::rollback();
//
//			return false;
//		}
//	}

	public static function caculate(Properties $property){
		$total_nbpac = $property['total_purchase_price'] + $property['total_purchase_price']* ($property['buffer'] + 
																							$property['others'] + 
																							$property['evaluation'] + 
																							$property['notary_land_register'] + 
																							$property['estate_agents'] +
																							$property['real_estate_taxes']);
				$bank_loan_money = $property['bank_loan']* $total_nbpac;
				

				
				$financing_structure_sum = $property['with_real_ek'] + $property['from_bond'] + $property['bank_loan']; //C50
				
				$net_rent_increase_year2 = $property['net_rent_pa'] + ($property['net_rent_pa']* $property['net_rent']);
				$net_rent_increase_year3 = $net_rent_increase_year2 + ($net_rent_increase_year2* $property['net_rent']);
				$net_rent_increase_year4 = $net_rent_increase_year3 + ($net_rent_increase_year3* $property['net_rent']);
				$net_rent_increase_year5 = $net_rent_increase_year4 + ($net_rent_increase_year4* $property['net_rent']);
				
				$maintenance_increase_year1 = $property['maintenance_nk']* $property['gesamt_in_eur'];
				$maintenance_increase_year2 = $maintenance_increase_year1 + $maintenance_increase_year1 * $property['maintenance'];				
				$maintenance_increase_year3 = $maintenance_increase_year2 + $maintenance_increase_year2 * $property['maintenance'];																
				$maintenance_increase_year4 = $maintenance_increase_year3 + $maintenance_increase_year3 * $property['maintenance'];
				$maintenance_increase_year5 = $maintenance_increase_year4 + $maintenance_increase_year4 * $property['maintenance'];	
				
				$operating_cost_increase_year1 = $property['operating_costs_nk']* $property['net_rent_pa'];					
				$operating_cost_increase_year2 = $operating_cost_increase_year1 + $operating_cost_increase_year1 * $property['operating_costs'];				
				$operating_cost_increase_year3 = $operating_cost_increase_year2 + $operating_cost_increase_year2 * $property['operating_costs'];																
				$operating_cost_increase_year4 = $operating_cost_increase_year3 + $operating_cost_increase_year3 * $property['operating_costs'];
				$operating_cost_increase_year5 = $operating_cost_increase_year4 + $operating_cost_increase_year4 * $property['operating_costs'];		
				
				$property_management_increase_year1 = $property['object_management_nk']* $property['net_rent_pa'];
				$property_management_increase_year2 = $property_management_increase_year1 + $property_management_increase_year1 * $property['object_management'];				
				$property_management_increase_year3 = $property_management_increase_year2 + $property_management_increase_year2 * $property['object_management'];																
				$property_management_increase_year4 = $property_management_increase_year3 + $property_management_increase_year3 * $property['object_management'];
				$property_management_increase_year5 = $property_management_increase_year4 + $property_management_increase_year4 * $property['object_management'];	
				
				$ebitda_year_1 = $property['net_rent_pa'] - $maintenance_increase_year1 - $operating_cost_increase_year1 - $property_management_increase_year1;
				$ebitda_year_2 = $net_rent_increase_year2 - $maintenance_increase_year2 - $operating_cost_increase_year2 - $property_management_increase_year2;
				$ebitda_year_3 = $net_rent_increase_year3 - $maintenance_increase_year3 - $operating_cost_increase_year3 - $property_management_increase_year3;
				$ebitda_year_4 = $net_rent_increase_year4 - $maintenance_increase_year4 - $operating_cost_increase_year4 - $property_management_increase_year4;
				$ebitda_year_5 = $net_rent_increase_year5 - $maintenance_increase_year5 - $operating_cost_increase_year5 - $property_management_increase_year5;

				$depreciation_year_1 = $property['depreciation_nk'] * $property['building'] * $property['gesamt_in_eur'];
				$depreciation_year_2 = $depreciation_year_1;
				$depreciation_year_3 = $depreciation_year_2;
				$depreciation_year_4 = $depreciation_year_3;
				$depreciation_year_5 = $depreciation_year_4;
				
				$ebit_year_1 = $ebitda_year_1 - $depreciation_year_1;
				$ebit_year_2 = $ebitda_year_2 - $depreciation_year_2;
				$ebit_year_3 = $ebitda_year_3 - $depreciation_year_3;
				$ebit_year_4 = $ebitda_year_4 - $depreciation_year_4;
				$ebit_year_5 = $ebitda_year_5 - $depreciation_year_5;
				
				$eradication_bank_end_of_year_1 = $property['eradication_bank']* $bank_loan_money;
				$value_end_of_year_1 = $bank_loan_money - $eradication_bank_end_of_year_1;
				$interest_bank_loan_end_of_year_1 = $property['interest_bank_loan']* $bank_loan_money;
				$sum_end_year_1 = $interest_bank_loan_end_of_year_1 + $eradication_bank_end_of_year_1;	
				
				$interest_bank_loan_end_of_year_2 = $property['interest_bank_loan']*$value_end_of_year_1;
				$eradication_bank_end_of_year_2 = $sum_end_year_1 - $interest_bank_loan_end_of_year_2;
				$sum_end_year_2 = $interest_bank_loan_end_of_year_2 + $eradication_bank_end_of_year_2;
				$value_end_of_year_2 = $value_end_of_year_1 - $eradication_bank_end_of_year_2;
				
				
				$interest_bank_loan_end_of_year_3 = $property['interest_bank_loan']* $value_end_of_year_2;
				$eradication_bank_end_of_year_3 = $sum_end_year_2 - $interest_bank_loan_end_of_year_3;
				$sum_end_year_3 = $interest_bank_loan_end_of_year_3 + $eradication_bank_end_of_year_3;
				$value_end_of_year_3 = $value_end_of_year_2 - $eradication_bank_end_of_year_3;
				
				$interest_bank_loan_end_of_year_4 = $property['interest_bank_loan']* $value_end_of_year_3;
				$eradication_bank_end_of_year_4 = $sum_end_year_3 - $interest_bank_loan_end_of_year_4;
				$sum_end_year_4 = $interest_bank_loan_end_of_year_4 + $eradication_bank_end_of_year_4;
				$value_end_of_year_4 = $value_end_of_year_3 - $eradication_bank_end_of_year_4;
				
				$interest_bank_loan_end_of_year_5 = $property['interest_bank_loan']*$value_end_of_year_4;
				$eradication_bank_end_of_year_5 = $sum_end_year_3 - $interest_bank_loan_end_of_year_5;
				$sum_end_year_5 = $interest_bank_loan_end_of_year_5 + $eradication_bank_end_of_year_5;
				$value_end_of_year_5 = $value_end_of_year_4 - $eradication_bank_end_of_year_5;

				$from_bond_money = $property['from_bond'] * $total_nbpac;
				$interest_bond_from_bond = $property['interest_bond'] * $from_bond_money;
				
				$Interest_bond_year1 = $interest_bond_from_bond;
				$Interest_bond_year2 = $Interest_bond_year1;
				$Interest_bond_year3 = $Interest_bond_year2;
				$Interest_bond_year4 = $Interest_bond_year3;
				$Interest_bond_year5 = $Interest_bond_year4;
				
				$EBT_year_1 = $ebit_year_1 - $interest_bank_loan_end_of_year_1 - $Interest_bond_year1;
				$EBT_year_2 = $ebit_year_2 - $interest_bank_loan_end_of_year_2 - $Interest_bond_year2;
				$EBT_year_3 = $ebit_year_3 - $interest_bank_loan_end_of_year_3 - $Interest_bond_year3;
				$EBT_year_4 = $ebit_year_4 - $interest_bank_loan_end_of_year_4 - $Interest_bond_year4;
				$EBT_year_5 = $ebit_year_5 - $interest_bank_loan_end_of_year_5 - $Interest_bond_year5;
				
				$Tax_year_1 = $EBT_year_1 * $property['tax'];
				$Tax_year_2 = $EBT_year_2 * $property['tax'];
				$Tax_year_3 = $EBT_year_3 * $property['tax'];
				$Tax_year_4 = $EBT_year_4 * $property['tax'];
				$Tax_year_5 = $EBT_year_5 * $property['tax'];

				$EAT_year_1 = $EBT_year_1 - $Tax_year_1;
				$EAT_year_2 = $EBT_year_2 - $Tax_year_2;
				$EAT_year_3 = $EBT_year_3 - $Tax_year_3;
				$EAT_year_4 = $EBT_year_4 - $Tax_year_4;
				$EAT_year_5 = $EBT_year_5 - $Tax_year_5;

				$Cash_flow_after_taxes_year_1 = $EAT_year_1 - $eradication_bank_end_of_year_1 + $depreciation_year_1;
				$Cash_flow_after_taxes_year_2 = $EAT_year_2 - $eradication_bank_end_of_year_2 + $depreciation_year_2;
				$Cash_flow_after_taxes_year_3 = $EAT_year_3 - $eradication_bank_end_of_year_3 + $depreciation_year_3;
				$Cash_flow_after_taxes_year_4 = $EAT_year_4 - $eradication_bank_end_of_year_4 + $depreciation_year_4;
				$Cash_flow_after_taxes_year_5 = $EAT_year_5 - $eradication_bank_end_of_year_5 + $depreciation_year_5;


				if(is_null($property['net_rent_pa'])){ $property['net_rent_pa'] = 0;  }

				if($property['net_rent_pa'] != 0 ){
					$gross_yield = $property['net_rent_pa'] / $total_nbpac;

				}
				else{
					$gross_yield =  $total_nbpac;

				}
				$Gross_desperate = 0;
				if($property['net_rent_pa'])
				$Gross_desperate = $total_nbpac / $property['net_rent_pa'];

				$EK_Rendite_CF = $EK_Rendite_GuV = 0;
				if($from_bond_money)
				{
					$EK_Rendite_CF = ($Cash_flow_after_taxes_year_1 + $Tax_year_1)/$from_bond_money;
					$EK_Rendite_GuV = $EBT_year_1/$from_bond_money;	
				}
				
				$Increase_p_a_year1 = $property['gesamt_in_eur'] +( $property['property_value'] * $property['gesamt_in_eur']);
				$Increase_p_a_year2 = $Increase_p_a_year1 +( $property['property_value'] * $Increase_p_a_year1);
				$Increase_p_a_year3 = $Increase_p_a_year2 +( $property['property_value'] * $Increase_p_a_year2);
				$Increase_p_a_year4 = $Increase_p_a_year3 +( $property['property_value'] * $Increase_p_a_year3);
				$Increase_p_a_year5 = $Increase_p_a_year4 +( $property['property_value'] * $Increase_p_a_year4);
				
				$end_of_year_1 = $value_end_of_year_1;
				$end_of_year_2 = $value_end_of_year_2;
				$end_of_year_3 = $value_end_of_year_3;
				$end_of_year_4 = $value_end_of_year_4;
				$end_of_year_5 = $value_end_of_year_5;
				
				$from_sales_proceeds = $Increase_p_a_year5 - $end_of_year_5;
				$from_revaluation = $Increase_p_a_year5 * 0.8 -$end_of_year_5;
				$anchor_tenants = $property['anchor_tenant'];

				$K38 = $property['rent_retail'] + $property['vacancy_retail'];
				$L38 = $property['rent_office'] + $property['vacancy_office'];
				$M38 = $property['rent_industry'] + $property['vacancy_industry'];
				$N38 = $property['rent_whg'] + $property['vacancy_whg'];

				//$Rental_area_in_m2 = $K38 + $L38 + $M38 + $N38;
				if (( $property['rent_retail'] + $property['rent_office'] + $property['rent_industry'] + $property['rent_whg'] ) == 0){
                    $Rent_euro_m2 = 0;
				}
				else {
                    $Rent_euro_m2 = $property['net_rent_pa'] /( $property['rent_retail'] + $property['rent_office'] + $property['rent_industry'] + $property['rent_whg'] )/12;
				}
				if (($K38 + $L38) == 0){
                    $KP_€_m2_p_NF = 0;
                    $occupancy_rate = 0;
				}
				else {
                    $KP_€_m2_p_NF = $property['total_purchase_price']/($K38 + $L38);
                    $occupancy_rate =($property['rent_retail']+ $property['rent_office'])/($K38 + $L38);
				}
				
				$plots = $property['plot'];

				$Factor_net = 0;
				if($property['net_rent_pa'])
				$Factor_net  = $property['total_purchase_price'] / $property['net_rent_pa'];
				$Lease_term_up_to = $property['mv_anchor_tenants'];
				$Land_value = $property['land_value'];
				$Total_Land_value = $property['plot_of_land_m2'];

				$WAULT__of_anchor_tenants = ((strtotime($property['mv_anchor_tenants']) - strtotime($property['duration_from']))/ 86400)/365;
				$WAULT__of_tenant1= ((strtotime($property['mv_tenant1']) - strtotime($property['duration_from']))/ 86400)/365;
				$WAULT__of_tenant2= ((strtotime($property['mv_tenant2']) - strtotime($property['duration_from']))/ 86400)/365;
				$WAULT__of_tenant3= ((strtotime($property['mv_tenant3']) - strtotime($property['duration_from']))/ 86400)/365;
				$WAULT__of_tenant4= ((strtotime($property['mv_tenant4']) - strtotime($property['duration_from']))/ 86400)/365;
				$WAULT__of_tenant5= ((strtotime($property['mv_tenant5']) - strtotime($property['duration_from']))/ 86400)/365;

				$Rental_income_of_anchor_tenants = $property['nk_anchor_tenants'] *$WAULT__of_anchor_tenants;
				$Rental_income_of_tenant1 =  $property['nk_tenant1']* $WAULT__of_tenant1;
				$Rental_income_of_tenant2 = $property['nk_tenant2']* $WAULT__of_tenant2;
				$Rental_income_of_tenant3 = $property['nk_tenant3']* $WAULT__of_tenant3;
				$Rental_income_of_tenant4 = $property['nk_tenant4']* $WAULT__of_tenant4;
				$Rental_income_of_tenant5 = $property['nk_tenant5']* $WAULT__of_tenant5;
				
				$Total_of_Rental_income = $Rental_income_of_anchor_tenants + $Rental_income_of_tenant1+ $Rental_income_of_tenant2+ $Rental_income_of_tenant3+ $Rental_income_of_tenant4+ $Rental_income_of_tenant5;
				$Total_of_WAULT = 0;
				if($property['net_rent_pa'])
				$Total_of_WAULT = $Total_of_Rental_income/$property['net_rent_pa'];
				$Value_totally = $Total_of_Rental_income + $Total_Land_value;
				$non_recoverable_ancillary_costs_and_maintenance_via_the_WAULT = 0;	
				$risk_value = -$total_nbpac + $Value_totally -$non_recoverable_ancillary_costs_and_maintenance_via_the_WAULT;
				$risk_value_percent = 0;
				if($total_nbpac)
				$risk_value_percent = $risk_value / $total_nbpac;
				$building_eur = $property['building']* $property['total_purchase_price'];
				$plot_of_land_eur = $property['plot_of_land']* $property['total_purchase_price'];// D37
				$net_buy_price = $property['total_purchase_price'];
				$additional_cost = $property['total_purchase_price']* ($property['buffer'] + 
																	$property['others'] + 
																	$property['evaluation'] + 
																	$property['notary_land_register'] + 
																	$property['estate_agents'] +
																	$property['real_estate_taxes']);
				$with_real_ek_money = $property['with_real_ek'] * $total_nbpac;
				$net_rent_pa = $property['net_rent_pa'];
				$maintenance_nk_money = $property['maintenance_nk']* $property['total_purchase_price'];
				//$operating_costs_nk_money = $property['operating_costs_nk']* $property['net_rent_pa'];
				//$object_management_nk_money = $property['object_management_nk']* $property['net_rent_pa'];
				$depreciation_nk_money = $property['depreciation_nk'] * $property['building'] * $property['gesamt_in_eur'];
				$net_rent_increase_year1 = $property['net_rent_pa'];

				$property['building_eur'] = $building_eur;
				$property['plot_of_land_eur'] = $plot_of_land_eur;
				$property['net_buy_price'] = $net_buy_price;
				$property['additional_cost']= $additional_cost;
				$property['total_nbpac'] = $total_nbpac;
				$property['with_real_ek_money'] = $with_real_ek_money;
				$property['from_bond_money'] = $from_bond_money;
				$property['bank_loan_money'] = $bank_loan_money;
				$property['net_rent_pa'] = $net_rent_pa;
				$property['maintenance_nk_money'] = $maintenance_nk_money;
				//$property['operating_costs_nk_money'] = $operating_costs_nk_money;
				//$property['object_management_nk_money'] = $object_management_nk_money;
				$property['depreciation_nk_money'] = $depreciation_nk_money;
				$property['net_rent_increase_year1'] = $net_rent_increase_year1;
				$property['financing_structure_sum'] = $financing_structure_sum;
				$property['net_rent_increase_year2'] = $net_rent_increase_year2;
				$property['net_rent_increase_year3'] =  $net_rent_increase_year3;
				$property['net_rent_increase_year4'] = $net_rent_increase_year4;
				$property['net_rent_increase_year5'] = $net_rent_increase_year5;
				$property['maintenance_increase_year1'] = $maintenance_increase_year1;
				$property['maintenance_increase_year2'] = $maintenance_increase_year2;
				$property['maintenance_increase_year3'] = $maintenance_increase_year3;
				$property['maintenance_increase_year4'] = $maintenance_increase_year4;
				$property['maintenance_increase_year5'] = $maintenance_increase_year5;
				$property['operating_cost_increase_year1'] = $operating_cost_increase_year1;
				$property['operating_cost_increase_year2'] = $operating_cost_increase_year2;
				$property['operating_cost_increase_year3'] = $operating_cost_increase_year3;
				$property['operating_cost_increase_year4'] = $operating_cost_increase_year4;
				$property['operating_cost_increase_year5'] = $operating_cost_increase_year5;
				$property['property_management_increase_year1'] = $property_management_increase_year1;
				$property['property_management_increase_year2'] = $property_management_increase_year2;
				$property['property_management_increase_year3'] = $property_management_increase_year3;
				$property['property_management_increase_year4'] = $property_management_increase_year4;
				$property['property_management_increase_year5'] = $property_management_increase_year5;
				$property['ebitda_year_1'] = $ebitda_year_1;
				$property['ebitda_year_2'] = $ebitda_year_2;
				$property['ebitda_year_3'] = $ebitda_year_3;
				$property['ebitda_year_4'] = $ebitda_year_4;
				$property['ebitda_year_5'] = $ebitda_year_5;
				$property['depreciation_year_1'] = $depreciation_year_1;
				$property['depreciation_year_2'] = $depreciation_year_2;
				$property['depreciation_year_3'] = $depreciation_year_3;
				$property['depreciation_year_4'] = $depreciation_year_4;
				$property['depreciation_year_5'] = $depreciation_year_5;
				$property['ebit_year_1'] = $ebit_year_1;
				$property['ebit_year_2'] = $ebit_year_2;
				$property['ebit_year_3'] = $ebit_year_3;
				$property['ebit_year_4'] = $ebit_year_4;
				$property['ebit_year_5'] = $ebit_year_5;
				$property['Interest_bank_loan_year_1'] = $interest_bank_loan_end_of_year_1;
				$property['Interest_bank_loan_year_2'] = $interest_bank_loan_end_of_year_2;
				$property['Interest_bank_loan_year_3'] = $interest_bank_loan_end_of_year_3;
				$property['Interest_bank_loan_year_4'] = $interest_bank_loan_end_of_year_4;
				$property['Interest_bank_loan_year_5'] = $interest_bank_loan_end_of_year_5;
				$property['Interest_bond_year1'] = $Interest_bond_year1;
				$property['Interest_bond_year2'] = $Interest_bond_year2;
				$property['Interest_bond_year3'] = $Interest_bond_year3;
				$property['Interest_bond_year4'] = $Interest_bond_year4;
				$property['Interest_bond_year5'] = $Interest_bond_year5;
				$property['EBT_year_1'] = $EBT_year_1;
				$property['EBT_year_2'] = $EBT_year_2;
				$property['EBT_year_3'] = $EBT_year_3;
				$property['EBT_year_4'] = $EBT_year_4;
				$property['EBT_year_5'] = $EBT_year_5;
				$property['Tax_year_1'] = $Tax_year_1;
				$property['Tax_year_2'] = $Tax_year_2;
				$property['Tax_year_3'] = $Tax_year_3;
				$property['Tax_year_4'] = $Tax_year_4;
				$property['Tax_year_5'] = $Tax_year_5;
				$property['EAT_year_1'] = $EAT_year_1;
				$property['EAT_year_2'] = $EAT_year_2;
				$property['EAT_year_3'] = $EAT_year_3;
				$property['EAT_year_4'] = $EAT_year_4;
				$property['EAT_year_5'] = $EAT_year_5;
				$property['Eradication_bank_year1'] = $eradication_bank_end_of_year_1;
				$property['Eradication_bank_year2'] = $eradication_bank_end_of_year_2;
				$property['Eradication_bank_year3'] = $eradication_bank_end_of_year_3;
				$property['Eradication_bank_year4'] = $eradication_bank_end_of_year_4;
				$property['Eradication_bank_year5'] = $eradication_bank_end_of_year_5;
				$property['Cash_flow_after_taxes_year_1'] = $Cash_flow_after_taxes_year_1;
				$property['Cash_flow_after_taxes_year_2'] = $Cash_flow_after_taxes_year_2;
				$property['Cash_flow_after_taxes_year_3'] = $Cash_flow_after_taxes_year_3;
				$property['Cash_flow_after_taxes_year_4'] = $Cash_flow_after_taxes_year_4;
				$property['Cash_flow_after_taxes_year_5'] = $Cash_flow_after_taxes_year_5;
				$property['real_estate_taxes_in_EUR'] = $property['real_estate_taxes']* $property['total_purchase_price'];
				$property['estate_agents_in_EUR'] = $property['estate_agents'] * $property['total_purchase_price'];
				$property['notary_land_register_in_EUR'] = $property['notary_land_register'] * $property['total_purchase_price'];
				$property['Evaluation_in_EUR'] = $property['evaluation'] * $property['total_purchase_price'];
				$property['Grundbuch_in_EUR'] = $property['Grundbuch'] * $property['total_purchase_price'];
				$property['Others_in_EUR'] = $property['others'] * $property['total_purchase_price'];
				$property['Buffer_in_EUR'] = $property['buffer'] * $property['total_purchase_price'];
				
				$property['value_end_of_year_1'] = $value_end_of_year_1;
				$property['value_end_of_year_2'] = $value_end_of_year_2;
				$property['value_end_of_year_3'] = $value_end_of_year_3;
				$property['value_end_of_year_4'] = $value_end_of_year_4;
				$property['value_end_of_year_5'] = $value_end_of_year_5;
				$property['interest_bank_loan_end_of_year_1'] = $interest_bank_loan_end_of_year_1;
				$property['interest_bank_loan_end_of_year_2'] = $interest_bank_loan_end_of_year_2;
				$property['interest_bank_loan_end_of_year_3'] = $interest_bank_loan_end_of_year_3;
				$property['interest_bank_loan_end_of_year_4'] = $interest_bank_loan_end_of_year_4;
				$property['interest_bank_loan_end_of_year_5'] = $interest_bank_loan_end_of_year_5;
				$property['eradication_bank_end_of_year_1'] = $eradication_bank_end_of_year_1;
				$property['eradication_bank_end_of_year_2'] = $eradication_bank_end_of_year_2;
				$property['eradication_bank_end_of_year_3'] = $eradication_bank_end_of_year_3;
				$property['eradication_bank_end_of_year_4'] = $eradication_bank_end_of_year_4;
				$property['eradication_bank_end_of_year_5'] = $eradication_bank_end_of_year_5;
				$property['sum_end_year_1'] =$sum_end_year_1;
				$property['sum_end_year_2'] =$sum_end_year_2;
				$property['sum_end_year_3'] =$sum_end_year_3;
				$property['sum_end_year_4'] =$sum_end_year_4;
				$property['sum_end_year_5'] =$sum_end_year_5;
				$property['interest_bond_from_bond'] = $interest_bond_from_bond;
				$property['gross_yield'] = $gross_yield;
				$property['Gross_desperate'] = $Gross_desperate;
				$property['EK_Rendite_CF'] = $EK_Rendite_CF;
				$property['EK_Rendite_GuV'] = $EK_Rendite_GuV;
				$property['Increase_p_a_year1'] = $Increase_p_a_year1;
				$property['Increase_p_a_year2'] = $Increase_p_a_year2;
				$property['Increase_p_a_year3'] = $Increase_p_a_year3;
				$property['Increase_p_a_year4'] = $Increase_p_a_year4;
				$property['Increase_p_a_year5'] = $Increase_p_a_year5;
				$property['end_of_year_1'] = $end_of_year_1;
				$property['end_of_year_2'] = $end_of_year_2;
				$property['end_of_year_3'] = $end_of_year_3;
				$property['end_of_year_4'] = $end_of_year_4;
				$property['end_of_year_5'] = $end_of_year_5;
				$property['from_sales_proceeds'] = $from_sales_proceeds;
				$property['from_revaluation'] = $from_revaluation;
				$property['anchor_tenants'] = $anchor_tenants;
				//$property['Rental_area_in_m2'] = $Rental_area_in_m2;
				$property['Rent_euro_m2'] = $Rent_euro_m2;
				$property['KP_€_m2_p_NF'] = $KP_€_m2_p_NF;
				$property['plots'] = $plots;
				$property['occupancy_rate'] = $occupancy_rate;
				$property['Factor_net'] = $Factor_net;
				$property['Lease_term_up_to'] = $Lease_term_up_to;
				$property['land_value'] = $Land_value;
				$property['WAULT__of_anchor_tenants'] = $WAULT__of_anchor_tenants;
				$property['WAULT__of_tenant1'] = $WAULT__of_tenant1;
				$property['WAULT__of_tenant2'] = $WAULT__of_tenant2;
				$property['WAULT__of_tenant3'] = $WAULT__of_tenant3;
				$property['WAULT__of_tenant4'] = $WAULT__of_tenant4;
				$property['WAULT__of_tenant5'] = $WAULT__of_tenant5;
				$property['Rental_income_of_anchor_tenants'] = $Rental_income_of_anchor_tenants;
				$property['Rental_income_of_tenant1'] = $Rental_income_of_tenant1;
				$property['Rental_income_of_tenant2'] = $Rental_income_of_tenant2;
				$property['Rental_income_of_tenant3'] = $Rental_income_of_tenant3;
				$property['Rental_income_of_tenant4'] = $Rental_income_of_tenant4;
				$property['Rental_income_of_tenant5'] = $Rental_income_of_tenant5;
				$property['Total_of_WAULT'] = $Total_of_WAULT;
				$property['Total_of_Rental_income'] = $Total_of_Rental_income;
				$property['Total_Land_value'] = $Total_Land_value;
				$property['Value_totally'] = $Value_totally;
				$property['non_recoverable_ancillary_costs_and_maintenance_via_the_WAULT'] = $non_recoverable_ancillary_costs_and_maintenance_via_the_WAULT;
				$property['risk_value'] = $risk_value;
				$property['risk_value_percent'] = $risk_value_percent;
		
		return $property;
	}

	public static function calculatePurchaseTotal($properties){
        $sum_purchase_price = 0;
        $sum_purchase_price_99 = 0;
        $sum_purchase_price_75 = 0;

        $sum_purchase_price_total = 0;
        $sum_purchase_price_total_99 = 0;
        $sum_purchase_price_total_75 = 0;

        $sum_rental_fee = 0;
        $sum_rental_fee_99 = 0;
        $sum_rental_fee_75 = 0;

        foreach ($properties as $property){
        	if (abs($property->risk_value_percent) <= 0.75){
                $sum_purchase_price += $property->building_eur + $property->plot_of_land;
                $sum_purchase_price_total += $property->total_purchase_price;
                $sum_rental_fee += $property->rent;
			}
			else if (abs($property->risk_value_percent) > 0.99){
                $sum_purchase_price_99 += $property->building_eur + $property->plot_of_land;
                $sum_purchase_price_total_99 += $property->total_purchase_price;
                $sum_rental_fee_99 += $property->rent;

            }
			else if (abs($property->risk_value_percent) > 0.75 && abs($property->risk_value_percent) <= 0.99){
                $sum_purchase_price_75 += $property->building_eur + $property->plot_of_land;
                $sum_purchase_price_total_75 += $property->total_purchase_price;
                $sum_rental_fee_75 += $property->rent;
            }
		}

		$total_purchase_price = $sum_purchase_price + $sum_purchase_price_99 + $sum_purchase_price_75;
        $total_rental_fee = $sum_rental_fee + $sum_rental_fee_99 + $sum_rental_fee_75;


		$result = [
			'sum_purchase_price' 			=> $sum_purchase_price,
            'sum_purchase_price_99' 		=> $sum_purchase_price_99,
            'sum_purchase_price_75' 		=> $sum_purchase_price_75,

			'sum_purchase_price_total'		=> $sum_purchase_price_total,
            'sum_purchase_price_total_99'	=> $sum_purchase_price_total_99,
            'sum_purchase_price_total_75'	=> $sum_purchase_price_total_75,

			'sum_rental_fee'				=> $sum_rental_fee,
            'sum_rental_fee_99'				=> $sum_rental_fee_99,
            'sum_rental_fee_75'				=> $sum_rental_fee_75,

			'total_purchase_price'			=> $total_purchase_price,
			'total_rental_fee'				=> $total_rental_fee

		];

		return $result;
	}

    public static function updateStatus($property, $status) {
        DB::beginTransaction();
        try {
            $propertyData = [
                'status'	=> $status,
                'status_update_date'=>date('Y-m-d')
            ];

            $property->update($propertyData);

            $property->save();

            DB::commit();

            return $property;
        } catch ( \Exception $e ) {
            DB::rollback();

            return false;
        }
    }

    public static function initPlanungFields($fields){
		$field_names=array('planung_total_area',
            'nutzflache_flur',
            'zimmerwohnung_1',
            'zimmerwohnung_2',
            'zimmerwohnung_3',
            'zimmerwohnung_4',
            'zimmerwohnung_5',
            'zimmerwohnung_6',
            'zimmerwohnung_7',
            'zimmerwohnung_8',
            'zimmerwohnung_9',
            'zimmerwohnung_10',
            'zimmerwohnung_11',
            'zimmerwohnung_12',
            'zimmerwohnung_13',
            'insgesamt_pm_1',
            'insgesamt_pm_2',
            'insgesamt_pm_3',
            'pro_wohnung_1',
            'pro_wohnung_2',
            'pro_wohnung_3',
            'insgesamt_pm_4',
            'insgesamt_pm_5',
            'verkauf_zu_faktor_1',
            'verkauf_zu_faktor_2',
            'verkauf_zu_faktor_3',
            'verkauf_zu_faktor_4',
            'verkauf_zu_faktor_5',
            'moglicher_verkaufspreis_1',
            'moglicher_verkaufspreis_2',
            'moglicher_verkaufspreis_3',
            'moglicher_verkaufspreis_4',
            'moglicher_verkaufspreis_5',
            'einkaufspreis_buchwert_1',
            'einkaufspreis_buchwert_2',
            'einkaufspreis_buchwert_3',
            'einkaufspreis_buchwert_4',
            'einkaufspreis_buchwert_5',
            'invest_inkl_ek_ruckzahlung_1',
            'invest_inkl_ek_ruckzahlung_2',
            'invest_inkl_ek_ruckzahlung_3',
            'invest_inkl_ek_ruckzahlung_4',
            'invest_inkl_ek_ruckzahlung_5',
            'delta_1',
            'delta_2',
            'delta_3',
            'delta_4',
            'delta_5');
        foreach ($field_names as $field_name){
            if(!isset($fields[$field_name])){
                $fields[$field_name]='';
            }
        }
        return $fields;
    }
    public static function initCleverFitFields($fields){
		$field_names=array('entkernung_erdgeschoss',
            'jahr_nettokaltmiete_1',
            'jahr_em_nkm_1',
            'jahr_nkm_pa_1',
            'nettokaltmiete_1',
            'em_nkm_1',
            'nkm_pa_1',
            'jahr_nettokaltmiete_2',
            'jahr_em_nkm_2',
            'jahr_nkm_pa_2',
            'nettokaltmiete_2',
            'em_nkm_2',
            'nkm_pa_2',
            'jahr_nettokaltmiete_3',
            'jahr_em_nkm_3',
            'jahr_nkm_pa_3',
            'nettokaltmiete_3',
            'em_nkm_3',
            'nkm_pa_3',
            'schliebung_der_galerie',
            'jahr_nettokaltmiete_4',
            'jahr_em_nkm_4',
            'jahr_nkm_pa_4',
            'nettokaltmiete_4',
            'em_nkm_4',
            'nkm_pa_4',
            'ausbaukostenzuschuss_clever_fit',
            'jahr_nettokaltmiete_5',
            'jahr_em_nkm_5',
            'jahr_nkm_pa_5',
            'nettokaltmiete_5',
            'em_nkm_5',
            'nkm_pa_5',
            'neuinstallation_heizung_elektro_luftung',
            'jahr_nettokaltmiete_6',
            'jahr_em_nkm_6',
            'jahr_nkm_pa_6',
            'entsorgung',
            'jahr_nettokaltmiete_7',
            'jahr_em_nkm_7',
            'jahr_nkm_pa_7',
            'jahr_nettokaltmiete_8',
            'jahr_em_nkm_8',
            'jahr_nkm_pa_8',
            'nettokaltmiete_8',
            'em_nkm_8',
            'nkm_pa_8',
            'jahr_nettokaltmiete_9',
            'jahr_em_nkm_9',
            'jahr_nkm_pa_9',
            'jahr_nettokaltmiete_10',
            'jahr_em_nkm_10',
            'jahr_nkm_pa_10',
            'calculation_invest_total',
            'jahr_nettokaltmiete_11',
            'jahr_em_nkm_11',
            'jahr_nkm_pa_11',
            'jahr_nettokaltmiete_12',
            'jahr_em_nkm_12',
            'jahr_nkm_pa_12',
            'jahr_nettokaltmiete_13',
            'jahr_em_nkm_13',
            'jahr_nkm_pa_13',
            'jahr_nettokaltmiete_14',
            'jahr_em_nkm_14',
            'jahr_nkm_pa_14',
            'jahr_nettokaltmiete_15',
            'jahr_em_nkm_15',
            'jahr_nkm_pa_15',
            'mittelwert_1',
            'mittelwert_2',
            'summe_1',
            'moglicher_verkaufspreis_1',
            'moglicher_verkaufspreis_2',
            'moglicher_verkaufspreis_3',
            'moglicher_verkaufspreis_4',
            'moglicher_verkaufspreis_5',
            'einkaufspreis_buchwert_1',
            'einkaufspreis_buchwert_2',
            'einkaufspreis_buchwert_3',
            'einkaufspreis_buchwert_4',
            'einkaufspreis_buchwert_5',
            'delta_1',
            'delta_2',
            'delta_3',
            'delta_4',
            'delta_5',
            'invest_inkl_ekruckzahlung_1',
            'invest_inkl_ekruckzahlung_2',
            'invest_inkl_ekruckzahlung_3',
            'invest_inkl_ekruckzahlung_4',
            'invest_inkl_ekruckzahlung_5',
            'clever_fit_total_1',
            'clever_fit_total_2',
            'clever_fit_total_3',
            'clever_fit_total_4',
            'clever_fit_total_5',
            'clever_fit_total_6',
            'clever_fit_total_7',
            'tedi_total_1',
            'tedi_total_2',
            'tedi_total_3',
            'tedi_total_4',
            'tedi_total_5',
            'tedi_total_6',
            'tedi_total_7',
            'tedi_total_8',
            'gewerbe_vermietet_1',
            'gewerbe_vermietet_2',
            'pot_kellerflachen_f_wohnungen_1',
            'pot_kellerflachen_f_wohnungen_2',
            'pot_wohnungen_1',
            'pot_wohnungen_2',
            'gewerbe_leerstand_1',
            'gewerbe_leerstand_2',
            'gewerbe_leerstand_3',
            'gewerbe_leerstand_4');
		foreach ($field_names as $field_name){
			if(!isset($fields[$field_name])){
                $fields[$field_name]='';
			}
		}
		return $fields;

	}


	static public function get_style_array($keys){
		$style_array = [];
		foreach ($keys as $key){
			if ($key == 'no-border'){
				$style_array['borders'] = [
					'allBorders' => [
						'borderStyle' => Border::BORDER_NONE ,
					]
				];

			}
			if ($key == 'bold'){
				$style_array['font']['bold'] = true;
			}
			if ($key == 'align-left'){
				$style_array['alignment'] = [
					'horizontal' => Alignment::HORIZONTAL_LEFT,
				];

			}
			if ($key == 'align-center'){
				$style_array['alignment'] = [
					'horizontal' => Alignment::HORIZONTAL_CENTER,
				];
			}
			if ($key == 'align-right'){
				$style_array['alignment'] = [
					'horizontal' => Alignment::HORIZONTAL_RIGHT,
				];
			}
			if ($key == 'border'){
				$style_array['borders'] = [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THIN,
						'color' => [
							'rgb' => '000000'
						]
					]
				];
			}
			if ($key == 'thickBorder'){
				$style_array['borders'] = [
					'allBorders' => [
						'borderStyle' => Border::BORDER_THICK,
						'color' => [
							'rgb' => '000000'
						]
					]
				];
			}
			if ($key == 'bg-darkgrey'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = 'a0a0a0';
			}

			if ($key == 'bg-grey'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = 'C0C0C0';
			}
			if ($key == 'bg-yellow'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = 'FFFE9F';
			}
			if ($key == 'bg-black'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = '000000';
				$style_array['font']['color']['rgb'] = 'FFFFFF';
			}
			if ($key == 'bg-blue'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = '9DCEF9';
			}
			if ($key == 'bg-green'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = '9BC838';
			}
			if ($key == 'light-green'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = 'ABEBC6';
			}
			if ($key == 'light-grey'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = 'CDCDCD';
			}
			if ($key == 'bg-red'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = 'FEC7CE';
			}
			if ($key == 'bg-pink'){
				$style_array['fill']['fillType'] = Fill::FILL_SOLID;
				$style_array['fill']['color']['rgb'] = 'FEC7CE';
			}
			if ($key == 'font-red'){
				$style_array['font']['color']['rgb'] = 'f53132';
			}
			if ($key == 'font-green'){
				$style_array['font']['color']['rgb'] = '008000';
			}

		}

		return $style_array;
	}
}

?>
