<?php

namespace App\Services;


use App\Models\Properties;
use App\Models\Tenants;
use DB;

class TenantsService
{

    public static function create($input) {
        DB::beginTransaction();

        try {
            $tenantData = [
                'nk_pm' => $input['nk_pm'],
                'nk_pa' => $input['nk_pa'],
                'mv_begin' => $input['mv_begin'],
                'mv_end' => date('Y-m-d H:i:s', strtotime($input['mv_end'])),
                'property_id' => $input['property_id']
            ];
            $tenant = Tenants::create($input);

            $property = Properties::find($tenant->property_id);
            $tenant = TenantsService::calculate($tenant, $property);
            $tenant->save();

            DB::commit();

            return $tenant;
        } catch (\Exception $e) {
            DB::rollback();
            die($e->getMessage());
            return false;
        }
    }

    public static function update( $input, $tenant) {
        DB::beginTransaction();
        try {
            $tenantData = [
                'nk_pm' => $input['nk_pm'],
                'nk_pa' => $input['nk_pa'],
                'mv_begin' => $input['mv_begin'],
                'mv_end' => $input['mv_end'],
                'property_id' => $input['property_id']
            ];

            $tenant->update($tenantData);

            $property = Properties::find($tenant->property_id);
            $tenant = TenantsService::calculate($tenant, $property);
            $tenant->save();

            DB::commit();

            return $tenant;
        } catch ( \Exception $e ) {
            DB::rollback();

            return false;
        }
    }

    public static function calculate(Tenants $tenant, Properties $property){
        $wault = ((strtotime($tenant['mv_end']) - strtotime($property['duration_from']))/ 86400)/365;

        $rental_income = $tenant['nk_pa'] * $wault;

        $tenant['wault'] = $wault;
        $tenant['rental_income'] = $rental_income;

        return $tenant;

    }
}