<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 5/6/2020
 * Time: 11:47 AM
 */

namespace App\Services\property;


use App\Repositories\Property\BankFinancingOfferRepository;

class BankFinancingOfferService
{
    protected $bankFinancingOfferRepository;
    public function __construct(BankFinancingOfferRepository $bankFinancingOfferRepository)
    {
        $this->bankFinancingOfferRepository = $bankFinancingOfferRepository;
    }

    public function store($data)
    {
        $this->bankFinancingOfferRepository->store($data);
    }


    public function edit($id)
    {
        return $this->bankFinancingOfferRepository->find($id);
    }

    public function update($data,$id)
    {
        $this->bankFinancingOfferRepository->update($data,$id);
    }

    public function delete($id)
    {
        $this->bankFinancingOfferRepository->delete($id);
    }
}
