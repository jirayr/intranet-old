<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 6/25/2020
 * Time: 1:57 PM
 */

namespace App\Services;


use App\Repositories\AccessTokenRepository;
use App\Services\Constants;
use Carbon\Carbon;
use GuzzleHttp\Client;

class TinkLinkApiService
{
    protected $client;
    protected $accessTokenRepository;

    public function __construct(AccessTokenRepository $accessTokenRepository)
    {
        $this->client = new Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false,),));
        $this->accessTokenRepository = $accessTokenRepository;
    }

    public function isValidToken()
    {
        if ((Carbon::now())->greaterThanOrEqualTo(Carbon::parse($this->accessTokenRepository->getToken()->validity)))
        {
            $this->refreshAccessToken();
        }
    }

    public function createOrUpdateToken($resultToken)
    {
        $this->accessTokenRepository->ifExist()?
            $this->accessTokenRepository->update($resultToken):
            $this->accessTokenRepository->store($resultToken);
    }

    public function getAccessToken($data)
    {
        $response = $this->client->request(
            'POST',
            Constants::GET_ACCESS_TOKEN_TINK_LINK,
            [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'client_id' => 'd91eb6a4d5a44ae1883c46a1cd8d3711',
                    'client_secret' => '698881596a4a486fb22a6e2c95b2483e',
                    'code' =>$data['code'],
                ]
            ]
        );
        $resultToken = json_decode($response->getBody(), true);
        $this->createOrUpdateToken($resultToken);
    }

    public function refreshAccessToken()
    {
        $response = $this->client->request(
            'POST',
            Constants::GET_ACCESS_TOKEN_TINK_LINK,
            [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'client_id' => 'd91eb6a4d5a44ae1883c46a1cd8d3711',
                    'client_secret' => '698881596a4a486fb22a6e2c95b2483e',
                    'refresh_token' => $this->accessTokenRepository->getToken()->refresh_token,
                ]
            ]
        );
        $resultToken = json_decode($response->getBody(), true);
        $this->createOrUpdateToken($resultToken);
    }

    public function getAccounts()
    {
        if ($this->accessTokenRepository->getToken())
        {
            $this->isValidToken();
            try {
                $response = $this->client->request(
                    'GET',
                    Constants::GET_ACCOUNT_LIST
                    ,
                    [
                        'headers' => [
                            'Authorization' => 'Bearer '.$this->accessTokenRepository->getToken()->access_token,
                        ],
                    ]
                );
                return json_decode($response->getBody(), true);

            } catch (\Exception $exception) {

            }
        }
    }
}
