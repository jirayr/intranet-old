<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 11/27/18
 * Time: 3:23 PM
 */

namespace App\Services;

use App\Models\TenancySchedule;
use App\Models\TenancyScheduleItem;
use App\Models\TenantPayment;
use App\Models\Properties;
use App\Models\Masterliste;
use App\Models\PropertyRentPaids;
use App\Models\TenancyExtension;
use App\User;
use DateTime;
use DB;

class TenancySchedulesService
{
    public static function get($property_id,$update_date=0){
        
        $rentdata = PropertyRentPaids::where('property_id',$property_id)->groupBy('mieter')->get();

        $rent_number_array = array();
        foreach ($rentdata as $key => $value) {
            $rent_number_array[] = trim($value->mieter);
        }
        
        $tenancy_schedules = TenancySchedule::where('property_id', $property_id)->get();
        
        foreach ($tenancy_schedules as $tenancy_schedule){

            if($update_date==1  && $tenancy_schedule->business_date != date('Y-m-d'))
            {
                $tenancy_schedule->business_date = date('Y-m-d');
                $tenancy_schedule->save();
            }


            $cr = User::where('id', $tenancy_schedule->creator_id)->first();
            $tenancy_schedule->creator = "";
            if($cr)
            $tenancy_schedule->creator = $cr->name;
            $tenancy_schedule->items = TenancyScheduleItem::where('tenancy_schedule_id', $tenancy_schedule->id)->get();

            $pre_total_live_rental_space = 0;
            $pre_total_live_actual_net_rent=0;
            $pre_total_business_rental_space=0;
            $pre_total_business_actual_net_rent=0;
            // pre($tenancy_schedule->items);

            foreach ($tenancy_schedule->items as $item) {
                //if($item->status==0)
                //    continue;

                $item->bka_date_2017 = '';
                $item->bka_date_2018 = '';
                $item->bka_date_2019 = '';
                $item->bka_date_2020 = '';

                $item->payment_date_2017 = '';
                $item->payment_date_2018 = '';
                $item->payment_date_2019 = '';
                $item->payment_date_2020 = '';


                $item->amount_2017 = 0;
                $item->amount_2018 = 0;
                $item->amount_2019 = 0;
                $item->amount_2020 = 0;

                if(!$item->opos_mieter)
                {
                    foreach ($rent_number_array as $key => $value) {

                        if ($value && $item->name && strpos(str_replace(' ', '', $value), str_replace(' ', '',$item->name)) !== false)
                        {
                            $rent_n = explode(' ', $value);
                            if(isset($rent_n[0]))
                            $item->opos_mieter = $rent_n[0];
                            break;                        
                        }
                    }
                }

                $tp = TenantPayment::where('tenancy_schedule_item_id',$item->id)->get();

                foreach ($tp as $key => $tp_row) {
                    
                    if($tp_row->year==2017)
                    {
                        $item->bka_date_2017 = $tp_row->bka_date;
                        $item->payment_date_2017 = $tp_row->payment_date;
                        $item->amount_2017 = $tp_row->amount;
                        $item->tenant_payments_2017 = $tp_row;

                    }
                    if($tp_row->year==2018)
                    {
                        $item->bka_date_2018 = $tp_row->bka_date;
                        $item->payment_date_2018 = $tp_row->payment_date;
                        $item->amount_2018 = $tp_row->amount;
                        $item->tenant_payments_2018 = $tp_row;

                    }
                    if($tp_row->year==2019)
                    {
                        $item->bka_date_2019 = $tp_row->bka_date;
                        $item->payment_date_2019 = $tp_row->payment_date;
                        $item->amount_2019 = $tp_row->amount;
                        $item->tenant_payments_2019 = $tp_row;
                        
                    }
                    if($tp_row->year==2020)
                    {
                        $item->bka_date_2020 = $tp_row->bka_date;
                        $item->payment_date_2020 = $tp_row->payment_date;
                        $item->amount_2020 = $tp_row->amount;
                        $item->tenant_payments_2020 = $tp_row;
                    }
                }



                if(($item->rent_begin != '' && $item->rent_begin > date('Y-m-d')) ||
                   ($item->rent_end != '' && $item->rent_end < date('Y-m-d')))
                    continue;

                if ($item->type == config('tenancy_schedule.item_type.live')){
                    $pre_total_live_rental_space += $item->rental_space;
                    $pre_total_live_actual_net_rent += $item->actual_net_rent;
                }

                if ($item->type == config('tenancy_schedule.item_type.business')){
                    $pre_total_business_rental_space += $item->rental_space;
                    $pre_total_business_actual_net_rent += $item->actual_net_rent;
                }
            }
            $total_live_rental_space = 0;
            $total_business_rental_space = 0;
            $total_live_actual_net_rent = 0;
            $total_live_nk_netto = 0;
            $total_business_actual_net_rent = 0;
            $total_business_nk_netto = 0;

            $total_live_vacancy_in_qm = 0;
            $total_live_vacancy_in_eur = 0;
            $total_business_vacancy_in_qm = 0;
            $total_business_vacancy_in_eur = 0;
            $total_remaining_time_in_eur = 0;
            $business_total_avg_rent =0;
            $live_total_avg_rent = 0;
            $businesscount = $livecount = 0;
            $vbusinesscount = $vlivecount = 0;
            $total_remaining_time_in_eur_array = array();
            $sum_total_amount = $sum_actual_net_rent = 0;
            $vsum_total_amount = 0; 

            foreach ($tenancy_schedule->items as $item){

                //if($item->status==0)
                //    continue;

                if($item->termination_date && $item->termination_date!="0000-00-00"  && $item->termination_date>date('Y-m-d'))
                    $item->date_diff_average = self::diff360($item->termination_date, $tenancy_schedule->business_date)/365;
                else
                    $item->date_diff_average = self::diff360($item->rent_end, $tenancy_schedule->business_date)/365;


                $item->verkauf_date_diff = self::diff360($item->rent_end, $tenancy_schedule->business_date)/365;


                if ($item->type == config('tenancy_schedule.item_type.business')){
                    $item->remaining_time_in_eur = $item->actual_net_rent * $item->date_diff_average * 12;

                    $item->vremaining_time_in_eur = $item->actual_net_rent * $item->verkauf_date_diff * 12;



                    if( $item->status && $item->rent_end > date('Y-m-d') && $item->rent_end && substr($item->rent_end,0,4)!="2099")
                    {
                            $sum_total_amount += $item->remaining_time_in_eur;
                            $vsum_total_amount += $item->vremaining_time_in_eur;
                    }

                    if($item->status && $item->rent_end > date('Y-m-d') && $item->rent_end && substr($item->rent_end,0,4)!="2099")
                    $sum_actual_net_rent += $item->actual_net_rent;

                }


                
                if(($item->rent_begin != '' && $item->rent_begin > date('Y-m-d')) ||
                   ($item->rent_end != '' && $item->rent_end < date('Y-m-d')))
                    continue;

                
                if($item->type == config('tenancy_schedule.item_type.business') && $item->rental_space && $item->actual_net_rent){
                    $businesscount = $businesscount+1;
                    $business_total_avg_rent += ($item->actual_net_rent/$item->rental_space);

                    

                }
                if($item->type == config('tenancy_schedule.item_type.live') && $item->rental_space && $item->actual_net_rent){
                    $livecount = $livecount+1;
                    $live_total_avg_rent += ($item->actual_net_rent/$item->rental_space);
                }






                

                if ($item->type == config('tenancy_schedule.item_type.live')){
                    $total_live_rental_space += $item->rental_space;
                    $total_live_actual_net_rent += $item->actual_net_rent;
                    $total_live_nk_netto += $item->nk_netto;
                }
                

                if ($item->type == config('tenancy_schedule.item_type.live_vacancy')){
                    if( $pre_total_live_rental_space != 0 ){

                        $cust_condition  = $pre_total_live_actual_net_rent / $pre_total_live_rental_space;
                    }else{
                        $cust_condition = 0;
                    }
                    $item->vacancy_in_eur = $item->vacancy_in_qm*$cust_condition*0.8;

                     if(!$item->vacancy_in_eur)
                    $item->vacancy_in_eur = $item->actual_net_rent2;

                    
                    if($item->vacancy_in_eur!=$item->calculated_vacancy_in_eur)
                    TenancyScheduleItem::where('id',$item->id)->update(['calculated_vacancy_in_eur'=>$item->vacancy_in_eur]);

                    $total_live_vacancy_in_qm += $item->vacancy_in_qm;
                    $total_live_vacancy_in_eur += $item->vacancy_in_eur;

                    $vlivecount = $vlivecount+1;

                }
                if ($item->type == config('tenancy_schedule.item_type.business')){

                    $total_business_rental_space += $item->rental_space;
                    // if($item->rent_end && substr($item->rent_end,0,4)!="2099")
                    if(true)
                    {   
                        $total_business_actual_net_rent += $item->actual_net_rent;
                        $item->remaining_time_in_eur = $item->actual_net_rent * $item->date_diff_average * 12;

                        // if(isset($total_remaining_time_in_eur_array[$item->common_item_id]) &&  $total_remaining_time_in_eur_array[$item->common_item_id] < $item->remaining_time_in_eur)
                        //      $total_remaining_time_in_eur_array[$item->common_item_id] = $item->remaining_time_in_eur;
                        
                        //  if(!isset($total_remaining_time_in_eur_array[$item->common_item_id])){
                        //      $total_remaining_time_in_eur_array[$item->common_item_id] = $item->remaining_time_in_eur;
                        //  }



                         $total_remaining_time_in_eur += $item->remaining_time_in_eur;

                         


                        /*if($item->is_new)
                            $total_remaining_time_in_eur_array[$item->id] = $item->remaining_time_in_eur;*/


                        // if($item->status)
                        // $total_remaining_time_in_eur += $item->remaining_time_in_eur;
                    }





                    $total_business_nk_netto += $item->nk_netto;
                    
                }

                if ($item->type == config('tenancy_schedule.item_type.business_vacancy')){
                    if( $pre_total_business_rental_space != 0 ){

                        $cust_condition  = $pre_total_business_actual_net_rent / $pre_total_business_rental_space;
                    }else{
                        $cust_condition = 0;
                    }
                    $item->vacancy_in_eur = $item->vacancy_in_qm*$cust_condition*0.8;
                    if(!$item->vacancy_in_eur)
                    $item->vacancy_in_eur = $item->actual_net_rent2;
                    $total_business_vacancy_in_qm += $item->vacancy_in_qm;
                    $total_business_vacancy_in_eur += $item->vacancy_in_eur;

                    $vbusinesscount = $vbusinesscount + 1;
                    if($item->vacancy_in_eur!=$item->calculated_vacancy_in_eur)
                    TenancyScheduleItem::where('id',$item->id)->update(['calculated_vacancy_in_eur'=>$item->vacancy_in_eur]);


                }

            }

            // foreach ($total_remaining_time_in_eur_array as  $amount) {
            //     $total_remaining_time_in_eur +=$amount;
            // }


            if( $tenancy_schedule->property_id ) {
                $tenancy_schedule->property = Properties::where('id', $tenancy_schedule->property_id)->first();
            }

            if( isset( $tenancy_schedule->property ) && $tenancy_schedule->property != null ) {
                $tenancy_schedule->masterliste = Masterliste::where('id', $tenancy_schedule->property->masterliste_id )->first();
            }

            // if($businessc)
            // {
            //     $business_total_avg_rent = $business_total_avg_rent/$businessc;
            // }
            // if($livec)
            // {
            //     $live_total_avg_rent = $live_total_avg_rent/$livec;
            // }
            // pre($tenancy_schedule->items);
            
            $wault = 0;
            if($sum_actual_net_rent && $sum_total_amount)
                $wault = round($sum_total_amount/(12*$sum_actual_net_rent),1);


            $verkauf_wault = 0;
            if($sum_actual_net_rent && $vsum_total_amount)
                $verkauf_wault = round($vsum_total_amount/(12*$sum_actual_net_rent),1);


            if($total_live_rental_space)
                $live_total_avg_rent = $total_live_actual_net_rent/$total_live_rental_space;


            $arr = [
                'sum_total_amount'=>$sum_total_amount,
                'sum_actual_net_rent'=>$sum_actual_net_rent,
                'total_live_rental_space' => $total_live_rental_space,
                'total_live_actual_net_rent' => $total_live_actual_net_rent,
                'total_live_nk_netto' => $total_live_nk_netto,
                'total_business_rental_space' => $total_business_rental_space,
                'business_total_avg_rent'=>$business_total_avg_rent,
                'live_total_avg_rent'=>$live_total_avg_rent,
                'total_business_actual_net_rent' => $total_business_actual_net_rent,
                'total_business_nk_netto' => $total_business_nk_netto,
                
                'total_rental_space' => $total_live_rental_space + $total_live_vacancy_in_qm + $total_business_rental_space + $total_business_vacancy_in_qm,
                'total_actual_net_rent' => $total_live_actual_net_rent + $total_business_actual_net_rent,
                'total_live_vacancy_in_qm' => $total_live_vacancy_in_qm,
                'total_live_vacancy_in_eur' => $total_live_vacancy_in_eur ,
                'total_business_vacancy_in_qm' => $total_business_vacancy_in_qm,
                'total_business_vacancy_in_eur' => $total_business_vacancy_in_eur,
                'potenzial_eur_monat' => $total_live_vacancy_in_eur + $total_business_vacancy_in_eur,
                'potenzial_eur_jahr' => ($total_live_vacancy_in_eur + $total_business_vacancy_in_eur) *12,
                'mi9' => $total_business_rental_space + $total_business_vacancy_in_qm,
                'mi10' => $total_live_rental_space + $total_live_vacancy_in_qm,
                'total_remaining_time_in_eur' => $total_remaining_time_in_eur,

                'businesscount'=>$businesscount,
                'vbusinesscount'=>$vbusinesscount,
                'livecount'=>$livecount,
                'vlivecount'=>$vlivecount,
                'wault'=>$wault,
                'verkauf_wault'=>$verkauf_wault,

            ];

            $tenancy_schedule->calculations = $arr;
            // if($update_date==1  && $tenancy_schedule->business_date != date('Y-m-d'))
            // {
            TenancySchedule::where('id',$tenancy_schedule->id)->update(['text_json'=>json_encode($arr),'wault'=>$wault]);
            // }

        }

        if (isset($_GET['selecting_tenancy_schedule'])){
            $selecting_tenancy_schedule = $_GET['selecting_tenancy_schedule'];
        } else {
            if (count($tenancy_schedules) > 0){
                $selecting_tenancy_schedule = $tenancy_schedules[0]->id;
            }
            else $selecting_tenancy_schedule = -1;

        }

        return [
            'tenancy_schedules' => $tenancy_schedules,
            'selecting_tenancy_schedule' => $selecting_tenancy_schedule
        ];
    }

    public static function diff360($date1, $date2) {
        //format: Y-m-d
        if (!$date1 || !$date2){
            return 0;
        }

        $date1 = date_create($date1);
        $date2 = date_create($date2);

        $diff = date_diff($date1,$date2);

        return $diff->days;
    }

    public static function create($input) {
        DB::beginTransaction();

        try {
            $tenant = TenancySchedule::create($input);

            $tenant->save();

            DB::commit();

            return $tenant;
        } catch (\Exception $e) {
            DB::rollback();
            die($e->getMessage());
            return false;
        }
    }

    public static function createItem($input){
        // print_r($input); die;
        DB::beginTransaction();

        try {
            $item = TenancyScheduleItem::create($input);
            // print_r($item); die;
            // $item->asset_manager_id=$input['asset_manager_id'];
            $item->save();

            DB::commit();

            return $item;
        } catch (\Exception $e) {
            DB::rollback();
            die($e->getMessage());
            return false;
        }
    }

    public static function update($tenancy_schedule, $input) {
        DB::beginTransaction();
        try {
            $tenancy_schedule_data = [
                'name' => $input['name'],
                'object' => $input['object'],
                'creator_id' => $input['creator_id']
            ];

            $tenancy_schedule->update($tenancy_schedule_data);

            

//          $property = Properties::find($tenant->property_id);
//          $tenant = TenantsService::calculate($tenant, $property);
            $tenancy_schedule->save();

            DB::commit();

            return $tenancy_schedule;
        } catch ( \Exception $e ) {
            DB::rollback();

            return false;
        }
    }

    public static function updateItem($item, $input) {
        DB::beginTransaction();
        try {
            $item_data = [
                'name' => $input['name'],
                'type' => $input['type'],
                'asset_manager_id' => $input['asset_manager_id'],
                'tenancy_schedule_id' => $input['tenancy_schedule_id'],
                'rent_begin' => $input['rent_begin'],
                'rent_end' => $input['rent_end'],
                'termination_date' => $input['termination_date'],
                'options' => $input['options'],
                'options1' => $input['options1'],
                'options2' => $input['options2'],
                'selected_option' => $input['selected_option'],
                'use' => $input['use'],
                'rental_space' => $input['rental_space'],
                'actual_net_rent' => $input['actual_net_rent'],
                'actual_net_rent2' => $input['actual_net_rent2'],
                'vacancy_in_qm' => $input['vacancy_in_qm'],
                'vacancy_in_eur' => $input['vacancy_in_eur'],
                'remaining_time' => $input['remaining_time'],
                'is_new' => $input['is_new'],
                'status' => $input['status'],
                'comment' => $input['comment'],
                'indexierung' => $input['indexierung'],
                'nk_netto' => $input['nk_netto'],

                'warning_date1' => $input['warning_date1'],
                'warning_date2' => $input['warning_date2'],
                'warning_date3' => $input['warning_date3'],
                'warning_price1' => $input['warning_price1'],
                'warning_price2' => $input['warning_price2'],
                'warning_price3' => $input['warning_price3'],
                'assesment_date' => $input['assesment_date'],
                'kaution' => $input['kaution'],
                'vacancy_on_purcahse' => $input['vacancy_on_purcahse'],
                'opos_mieter' => $input['opos_mieter'],
                
                

            ];

            $item->update($item_data);

//            $property = Properties::find($tenant->property_id);
//            $tenant = TenantsService::calculate($tenant, $property);
            $item->save();

            DB::commit();

            return $item;
        } catch ( \Exception $e ) {
            DB::rollback();

            return false;
        }
    }

}