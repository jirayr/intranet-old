<?php
	namespace App\Services;
	use App\Models\Banks;
	use DB;
	use Auth;
	class BanksService
	{
		public static function create($input) {
			DB::beginTransaction();
			
			try {
				$bank = Banks::create( [
					'user_id' 	=> Auth::id(),
					'picture' 	=> $input['picture'],
					'name' 		=> $input['name'],
					'contact_name' 	=> $input['contact_name'],
					'contact_phone' => $input['contact_phone'],
					'contact_email' => $input['contact_email'],
					'address' 	=> $input['address'],
					'location_latitude' 	=> $input['location_latitude'],
					'location_longitude' 	=> $input['location_longitude'],
//					'with_real_ek' => $input['with_real_ek'] / 100,
//					'from_bond' => $input['from_bond'] / 100,
//					'bank_loan' => $input['bank_loan'] / 100,
//					'interest_bank_loan' => $input['interest_bank_loan'] / 100,
//					'eradication_bank' => $input['eradication_bank'] / 100,
//					'interest_bond' => $input['interest_bond'] / 100,
				] );
				
				$bank->save();
				
				DB::commit();

				return $bank;
			} catch (\Exception $e) {
				DB::rollback();
				return false;
			}
		}

		public static function update( $input, $bank) {
			DB::beginTransaction();
			try {
				$bankData = [
					'user_id' 	=> Auth::id(),
					'picture' 	=> $input['picture'],
					'name' 		=> $input['name'],
					'contact_name' 	=> $input['contact_name'],
					'contact_phone' => $input['contact_phone'],
					'contact_email' => $input['contact_email'],
					'address' => $input['address'],
					'location_latitude' => $input['location_latitude'],
					'location_longitude' => $input['location_longitude'],
					'notes' => $input['notes'],
					// 'with_real_ek' => $input['with_real_ek'] / 100,
					// 'from_bond' => $input['from_bond'] / 100,
					// 'bank_loan' => $input['bank_loan'] / 100,
					// 'interest_bank_loan' => $input['interest_bank_loan'] / 100,
					// 'eradication_bank' => $input['eradication_bank'] / 100,
					// 'interest_bond' => $input['interest_bond'] / 100,				
				];
				// dd($bankData);
				
				$bank->update($bankData);
				$bank->save();
				
				DB::commit();

				return $bank;
			} catch ( \Exception $e ) {
				DB::rollback();

				return false;
			}
		}

		public static function delete( $bank ) {
			DB::beginTransaction();
			try {
				$bank->delete();
				
				DB::commit();

				return true;
			} catch ( \Exception $e ) {
				DB::rollback();

				return false;
			}
		}
	}

