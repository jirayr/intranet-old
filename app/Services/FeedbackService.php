<?php

namespace App\Services;
use App\Models\Feedback;
use DB;
use Auth;
class FeedbackService
{


	public static function create($input) {
		DB::beginTransaction();
		
		try {		
			$feedback = Feedback::create( [
				'user_id' =>  Auth::id(),
				'message'=>$input['message']
			] );
			
			DB::commit();

			return $feedback;
		} catch (\Exception $e) {
			DB::rollback();
			die($e->getMessage());
			return false;
		}
	}

	public static function update( $input, $feedback) {
		DB::beginTransaction();
		try {
			$feedbackData = [
				'user_id' =>  Auth::id(),
				'message'=>$input['message']
			];
			$feedback->update($feedbackData);
			DB::commit();
			return $feedback;
		} catch ( \Exception $e ) {
			var_dump( $e);
			DB::rollback();
			return false;
		}
    }
    public static function delete($feedback, $mes = null)
	{
		DB::beginTransaction();
		try {
			$feedback->delete();
			DB::commit();
			return true;
		} catch (\Exception $e) {
            DB::rollback();
            
			return false;
		}
	}

}

?>
