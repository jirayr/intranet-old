<?php

namespace App\Services;
use App\Models\PropertyComments;
use DB;
use Auth;
class PropertyCommentsService
{

	public static function create($input) {
		DB::beginTransaction();
		try {
            $comment = PropertyComments::create( [
				'user_id' =>  Auth::id(),
				'property_id' => $input['property_id'] ,
				'comment' => $input['comment'],
				'pdf_file' => $input['pdf_file'],
				'status' => 'PENDING'
			] );
			DB::commit();
			return $comment;
		} catch (\Exception $e) {
			var_dump($e->getMessage());die;
			DB::rollback();
			return($e->getMessage());
		}
	}

	public static function update( $input, $comment) {
		DB::beginTransaction();
		try {
			$update_data = [
                'status' => $input
			];
            $comment->update($update_data);
			DB::commit();
			return $comment;
		} catch ( \Exception $e ) {
			var_dump( $e);
			DB::rollback();
			return false;
		}
    }
    public static function delete($comment, $mes = null)
	{
		DB::beginTransaction();
		try {
            $comment->delete();
			DB::commit();
			return true;
		} catch (\Exception $e) {
            DB::rollback();
            
			return false;
		}
	}
    public static function update_comment( $input , $comment) {
        DB::beginTransaction();
        try {
            $update_data = [
				'comment' => $input
            ];
            $comment->update($update_data);
            DB::commit();
            return $comment;
        } catch ( \Exception $e ) {
            var_dump( $e);
            DB::rollback();
            return false;
        }
    }
}

?>
