<?php
/**
 * Created by PhpStorm.
 * User: aliraza
 * Date: 11/08/2019
 * Time: 7:33 PM
 */

namespace App\Services;


class Constants
{
    const  FIGO_API_URL = 'https://api.figo.me';
    const  FIN_API_URL = 'https://docs.finapi.io/oauth/token';
    const  FIN_API_ALL_BANKS_URL = 'https://docs.finapi.io/api/v1/banks';
    const  IMPORT_BANK_CONNECTION_URL = 'https://docs.finapi.io/api/v1/bankConnections/import';
    const  GET_ALL_BANK_CONNECTION_URL = 'https://docs.finapi.io/api/v1/bankConnections';
    const  GET_SINGLE_BANK = 'https://docs.finapi.io/api/v1/banks/';
    const  GET_BANK_ACCOUNT = 'https://docs.finapi.io/api/v1/accounts/';
    const  GET_ACCESS_TOKEN_TINK_LINK = 'https://api.tink.com/api/v1/oauth/token';
    const  GET_ACCOUNT_LIST = 'https://api.tink.com/api/v1/accounts/list';
}
