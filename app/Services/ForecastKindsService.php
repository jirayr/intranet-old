<?php

namespace App\Services;


use App\Models\ForecastKind;
use DB;

class ForecastKindsService
{
    public static function update( $input, $forecast_kind) {
        DB::beginTransaction();
        try {
            $forecast_kind_data = [
                'kind' => $input['kind'],
                'description' => $input['description'],
                'status' => $input['status'],
                'notary' => $input['notary'],
                'bnl' => $input['bnl'],
                'total_purchase_price' => $input['total_purchase_price'],
                'fk_or_sale' => $input['fk_or_sale'],
                'reflux' => $input['reflux'],
                'note' => $input['note']
            ];

            $forecast_kind->update($forecast_kind_data);

            $forecast_kind->save();

            DB::commit();

            return $forecast_kind;
        } catch ( \Exception $e ) {
            DB::rollback();

            return false;
        }
    }

    public static function delete($forecast_id) {
        DB::beginTransaction();
        try {
            $forecast_kinds = ForecastKind::where('forecast_id', $forecast_id);

            $forecast_kinds->delete();

            DB::commit();

            return true;
        } catch ( \Exception $e ) {
            DB::rollback();

            return false;
        }
    }
}