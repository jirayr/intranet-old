<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 4/21/2020
 * Time: 1:36 PM
 */

namespace App\Services;


use App\EmailConfig;

class EmailConfigService
{
    protected $emailConfig ;

    public function __construct(EmailConfig $emailConfig)
    {
        $this->emailConfig = $emailConfig;
    }


    public function store($request)
    {
            if (is_null ($request->email )){
                $request->email = [];
            }
        if ($this->emailConfig->exists())
        {
            $this->emailConfig->first()->update([
                'email'=> implode($request->email,',')

            ]);
        }else
            $this->emailConfig->create([
                'email'=> implode($request->email,',')
            ]);
    }

    public function find()
    {
        if ($this->emailConfig->first()){
            return explode(',',$this->emailConfig->first()->email);
        };

    }
}
