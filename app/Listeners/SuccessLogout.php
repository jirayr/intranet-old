<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\LoginLogs;

class SuccessLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        if ($event->user->id!=2)
        {
            $currentLog = LoginLogs::where('user_id', $event->user->id)->where('is_logout', 0)->orderBy('id', 'desc')->first();
            if($currentLog){
                $currentLog->is_logout = 1;
                $currentLog->save();
            }
        }

    }
}
