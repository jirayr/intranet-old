<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use App\Models\LoginLogs;

class SuccessLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        if ($event->user->id!=2)
        {
            $modal              = new LoginLogs;
            $modal->user_id     = $event->user->id;
            $modal->ip_address  = $this->request->ip();
            $modal->agent       = $this->request->header('User-Agent');
            $modal->is_login    = 1;
            $modal->save();
        }
    }
}
