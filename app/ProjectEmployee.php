<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectEmployee extends Model
{
    public $fillable = [
        'company_employee_id',
        'property_project_id',
        'user_id',
    ];
}
