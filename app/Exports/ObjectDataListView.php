<?php

namespace App\Exports;

use App\Models\Properties;
use App\Models\PropertiesBuyDetail;
use App\Models\PropertyInsurance;
use App\Models\PropertyInvestation;
use App\Models\PropertyMaintenance;
use App\Models\PropertyServiceProvider;
use App\Services\PropertiesService;
use App\Services\TenancySchedulesService;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class ObjectDataListView implements FromView , ShouldAutoSize,WithEvents
{

    protected  $id;
    public function __construct($id)
    {
        $this->id =  $id;
    }

    public function view(): View
    {
        $properties = Properties::where('id','=',$this->id)->where('Ist',0)->where('soll',0)->first();
        $tenancy_schedule_data = TenancySchedulesService::get($this->id,1);
        $maintenance = PropertyMaintenance::where('property_id',$this->id)->get();
        $investation = PropertyInvestation::where('property_id',$this->id)->get();
        $insurances = PropertyInsurance::where('property_id',$this->id)->get();
        $property_service_providers = PropertyServiceProvider::where('property_id',$this->id)->get();

        $b_data = PropertiesBuyDetail::where('property_id', $this->id)->get()->toArray();
        $buy_data = array();
        foreach ($b_data as $key => $value) {
            if(isset($buy_data[$value['type']])){}
            else
            $buy_data[$value['type']] = $value;
        }

        $set = false;
        return view('properties.templates.schl', [
            'properties' => $properties,
            'tenancy_schedule_data' => $tenancy_schedule_data,
            'maintenance' => $maintenance,
            'investation' => $investation,
            'insurances' => $insurances,
            'property_service_providers' => $property_service_providers,
            'banks' => [],
            'export'=>true,
            'id' => $this->id,
            'buy_data'=>$buy_data,
            'isShowUnWantedFields' => false
        ]);
    }


    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event)
            {
                $event->sheet->getDelegate()->getStyle('A1:B8')->applyFromArray(PropertiesService::get_style_array(['border']));
                $event->sheet->getDelegate()->getStyle('A10:B10')->applyFromArray(PropertiesService::get_style_array(['border']));
                $event->sheet->getDelegate()->getStyle('A12:B12')->applyFromArray(PropertiesService::get_style_array(['border']));
                $event->sheet->getDelegate()->getStyle('A14:B21')->applyFromArray(PropertiesService::get_style_array(['border']));
                $event->sheet->getDelegate()->getStyle('A28:E30')->applyFromArray(PropertiesService::get_style_array(['border']));
                $event->sheet->getDelegate()->getStyle('E2:I5')->applyFromArray(PropertiesService::get_style_array(['border']));
                $event->sheet->getDelegate()->getStyle('E6:I14')->applyFromArray( PropertiesService::get_style_array(['border']));
                $event->sheet->getDelegate()->getStyle('E16:I24')->applyFromArray( PropertiesService::get_style_array(['border']));
                $event->sheet->getDelegate()->getStyle('A1:A8')->applyFromArray( PropertiesService::get_style_array(['bold']));
                $event->sheet->getDelegate()->getStyle('A10')->applyFromArray( PropertiesService::get_style_array(['bold']));
                $event->sheet->getDelegate()->getStyle('A12')->applyFromArray( PropertiesService::get_style_array(['bold']));
                $event->sheet->getDelegate()->getStyle('A14:A21')->applyFromArray( PropertiesService::get_style_array(['bold']));
                $event->sheet->getDelegate()->getStyle('E2:E5')->applyFromArray( PropertiesService::get_style_array(['bold']));
                $event->sheet->getDelegate()->getStyle('E6:I6')->applyFromArray( PropertiesService::get_style_array(['bold', 'border', 'bg-grey']));
                $event->sheet->getDelegate()->getStyle('F17:I17')->applyFromArray( PropertiesService::get_style_array(['bold', 'border', 'bg-grey']));
                $event->sheet->getDelegate()->getStyle('A14:B14')->applyFromArray( PropertiesService::get_style_array(['bold', 'border', 'bg-grey']));
            }
        ];
    }
}