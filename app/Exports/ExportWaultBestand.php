<?php

namespace App\Exports;

use App\Models\Properties;
use App\Models\TenancySchedule;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportWaultBestand implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return TenancySchedule::join('properties', 'tenancy_schedules.property_id', '=', 'properties.main_property_id')->where('properties.main_property_id','!=',0)->where('properties.status',6)->groupBy('properties.main_property_id')
            ->select('properties.main_property_id','properties.name_of_property','tenancy_schedules.wault')->get();
    }

    public function headings(): array{
        return ['Main Property Id','Objekt','Wault'];
    }
}
