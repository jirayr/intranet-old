<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Concerns\WithHeadings;


class ExcelExport implements FromArray, WithHeadings // this was FromQuery before
{
    use Exportable, SerializesModels;

    private $data;
    private $header;
    public function __construct($header,$data)
    {
        $this->data = $data;     //Inject data
        $this->header = $header;
    }

    public function array(): array // this was query() before
    {
        return $this->data;
    }

    public function headings(): array{
        return $this->header;
    }
}