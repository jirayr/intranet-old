<?php

namespace App;

use App\Models\Banks;
use App\Models\Properties;
use Illuminate\Database\Eloquent\Model;

class email_template2_sending_info extends Model
{
    protected $table = 'email_template2_sending_info';
    protected $fillable = array('property_id','message', 'subject', 'email_from', 'attachment','contact_person','object','construction_year','address','type','the_main_tenant','rental_period_until','parking','email_to_send','financing','equity_share','leverage','variable_interest_rate','repayment','doc_key', 'role','notizen');
    protected $guarded = [];

    public function getBank($email)
    {
        return Banks::where('contact_email','LIKE','%'.$email.'%')->first();
    }

    public function property()
    {
        return $this->belongsTo(Properties::class, 'property_id');
    }

}
