<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable =['name','address_id'];

    public function companyAddress()
    {
        return $this->hasOne('App\Address', 'company_id');
    }
}

