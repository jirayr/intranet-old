<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressesJob extends Model
{
    protected $fillable =['result','status','keyword','limit_max','type','is_status_checked','property_id'];

    public function emailLogs()
    {
        return $this->hasMany('App\AddressesJobEmailLog','addresses_job_id');
    }
}
