<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Properties;
use App\User;
use App\Models\PropertiesDirectory;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Helpers\PropertiesHelperClass;


class CreatePropertyDirectory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'property:createDirectory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create property directory in Google Drive.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::useDailyFiles(storage_path().'/logs/Gdrive_cron.log');
        Log::info('///////************'.date("Y-m-d H:i:s").'************///////');
        $property = Properties::doesnthave('directory')
                ->where('masterliste_id', '=', null)
                ->where('Ist', '!=', 0)
                ->where('main_property_id', '!=', 0)
                ->where('soll', 0)
                ->first();
        if(!$property){
            Log::info('No new property without directory in Google Drive.');
            $this->info('No new property without directory in Google Drive.');
            exit;
        }       
        
        // if($property->property_status == 1){
        //     $property->status = 18;
        // }

        $propertyStatusArr = config('properties.status_with_drive');             
        if(!$property->status){
            $propertyStatusArr = $propertyStatusArr[15];
        }else{
            $propertyStatusArr = $propertyStatusArr[$property->status];
        }
        if($propertyStatusArr[1] == "")
        {
            Log::info('Property status directory not exist.'. $propertyStatusArr[0]);
            $this->info('Property status directory not exist.'. $propertyStatusArr[0] );
            exit;
        }

        $root_dir = $propertyStatusArr[1];
        $temp_root_dir = explode('/', $root_dir);
        array_pop($temp_root_dir);
        $temp_root_dir = implode('/', $temp_root_dir); 
        $contents = collect(Storage::cloud()->listContents($temp_root_dir, false));
        $root_dir = $contents->where('type', 'dir')->where('path', $root_dir)->first();
        if(!$root_dir){
            Log::info('Property status directory not exist in google drive.'. $propertyStatusArr[0]);
            $this->info('Property status directory not exist in google drive.'. $propertyStatusArr[0]);
            exit;
        }
        $root_dir = $root_dir['path'];

        $new_folder_name = PropertiesHelperClass::getPropertyDirectoryName($property); 

        if ( ! $newDir = Storage::cloud()->makeDirectory($root_dir.'/'.$new_folder_name) ) {                    
            Log::info('directory create still remain for property#'. $property->id);
            $this->info('directory create still remain for property#'. $property->id);
            exit;
        }

        $contents = collect(Storage::cloud()->listContents($root_dir, false));
        $new_property_dir = $contents->where('type', 'dir')->where('name', $new_folder_name)->sortByDesc('timestamp')->values()->first();
        // sortByDesc because there can multiple folder with same name so we get latest.
        PropertiesDirectory::create([
            'property_id'   => $property->id,
            'parent_id'   => 0,
            'dir_name'      => $new_folder_name,
            'dir_basename'  => $new_property_dir['basename'],
            'dir_path'      => $new_property_dir['path']
        ]);
        $p_dir = PropertiesDirectory::where('property_id', $property->id)->where('parent_id', 0)->first();

        // Creating sub directories.
        PropertiesHelperClass::createPropertySubDirectories($p_dir, $property->id);

        
        Log::info('Property directories created successfully. for property#'. $property->id);
        $this->info('Property directories created successfully. for property#'. $property->id);


        Log::info('///////************ End ************///////');
    }
}
