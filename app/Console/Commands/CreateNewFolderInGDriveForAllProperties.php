<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CreateNewFolderInGDriveForAllProperties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:gdrive_folder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $recursive = false;
        $newFolderName='Betriebskostenabrechnung';
        /****** Statuses Folder Path for Properties *****/
        $propertyStatusFolders =['1oRpMk2M1ackZCalarP8lLvIiTNxeKonG'];

        foreach ($propertyStatusFolders as $propertyStatusFolder)
        {
            /***** All properties folder of status *******/
            $contents = Storage::cloud()->listContents($propertyStatusFolder, $recursive);
            $propertiesFolders = collect($contents)->where('type', '=', 'dir');
            $propertiesFolders = array_values(array_sort($propertiesFolders, function ($value) {
                return $value['name'];
            }));

            /********** Create New Folder In each Property ******/
            foreach ($propertiesFolders as $propertyFolder)
            {
                $contents = collect(Storage::cloud()->listContents($propertyFolder['path'], $recursive));
                //get app dir
                $app_parent_dir = $contents->where('type', 'dir')->where('filename', $newFolderName)->first();

                //check already exist Folder
                if ( ! $app_parent_dir) {
                    //Create Folder
                    Storage::cloud()->makeDirectory($propertyFolder['path'].'/'.$newFolderName) ;
                }
            }
            /********** Create New Folder End ******/
        }
    }
}
