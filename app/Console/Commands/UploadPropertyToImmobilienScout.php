<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Properties;
use App\Models\UploadImmobilienScout;
use Log;

class UploadPropertyToImmobilienScout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'property:upload {--propertyId= : Property id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for upload property add at ImmobilienScout24.de via API.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $propertyId = $this->option('propertyId', "");        
        $property = '';
        if($propertyId == "") {
            $property = Properties::doesnthave('uploadState')->first();
        }else{
            $property = Properties::find($propertyId);
        }
        if(!$property){
            $this->info( "Invalid value given, it should be property id");
            return false;
        }
        Log::useFiles(storage_path().'/logs/immobilienScout24.log');
        // Log::info('propertyId to upload is '.$property->id);
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/realestate",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                'authorization: OAuth oauth_consumer_key="Intranet FCRKey",oauth_token="2ae96bba-e08b-4baa-b625-54cb56bbc24d",
                oauth_timestamp="1571043425",
                oauth_nonce="1BZJ2A",
                oauth_version="1.0",
                oauth_signature_method="HMAC-SHA1",
                oauth_signature="9xZH8z1jwUcafQMTsXOBHTZeLq8%3D",
                "cache-control: no-cache",
                "content-type: application/xml'
            ),
            CURLOPT_POSTFIELDS => "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><common:realtorContactDetail xmlns:common=\"http://rest.immobilienscout24.de/schema/common/1.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" \r\nxmlns:ns4=\"http://rest.immobilienscout24.de/schema/customer/1.0\" xmlns:ns5=\"http://rest.immobilienscout24.de/schema/user/1.0\" >
            <externalId>123</externalId>
            <title>This is test object by API</title>
            <address>
                <street></street>
            </address>
            </common:realtorContactDetail>",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            Log::info('ups something went wrong! '.print_r($err, true));

            return redirect()->back();
        } else {



            $xml = new \SimpleXMLElement($response);
            $response = json_encode($xml, JSON_PRETTY_PRINT);
            $response_massage = json_decode($response, TRUE);

            Log::info($response_massage['message']['message'] .$property->id);



        }


    }
}
