<?php

namespace App\Console\Commands;

use App\Account;
use App\BankTransaction;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class InsertBankTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:insertBankTransactions';
    protected $account;
    protected $bankTransaction;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get the latest file from g drive and insert and update the bank transactions in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Account $account, BankTransaction $bankTransaction)
    {
        parent::__construct();
        $this->account = $account;
        $this->bankTransaction = $bankTransaction;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->store($this->mapFileData());
    }
    public function mapFileData()
    {
        $fileRawData = $this->getTheLatestFileFromGDrive();
        $arrayOfGroupedTransactions = $this->structurizeTheDataAccordingToTransactionsPerAccount($fileRawData);
        $insertDataArray = array();
        $innerArray =  array();
        foreach ($arrayOfGroupedTransactions as $fileRecords)
        {
            if ($fileRecords)
                foreach ($fileRecords as $key => $fileData)
                {
                    if (strpos($fileData, ':61:') !== false)
                    {
                        $transactionData =  strtok($fileData, ',');
                        $insertDataArray[] = $innerArray;
                        $innerArray['uploaded_file_name'] = $fileRawData['fileName'] ;
                        $innerArray['amount'] = substr($transactionData, 15);
                        $innerArray['date_of_transaction'] =  substr($transactionData, 4,6);
                        $innerArray['bank_transaction_type'] = substr($transactionData, 14,1);
                    }

                    // value of end balance
                    if (strpos($fileData, ':62M:') !== false ||strpos($fileData, ':62F:') !== false)
                    {
                        $innerArray['end_balance_complete'] = $fileData;
                        $innerArray['remaining_balance'] =  substr(strtok($fileData, ','), 15);
                    }

                    // value of account number
                    if (strpos($fileData, ':25:') !== false)
                    {
                        $innerArray['account_number'] = substr($fileData, strpos($fileData, "/") + 1);
                    }
                }
        }
        return $insertDataArray;
    }

    public function store($data)
    {
        if(!$this->bankTransaction->where('uploaded_file_name','LIKE','%'.$data[2]['uploaded_file_name'].'%')->first())
        {

            $account = '';
            foreach ($data as $arrayData)
            {
                $account = $this->account->where('iban','LIKE', '%'.wordwrap( $arrayData['account_number'], strlen($arrayData['account_number'])-2,' ',true).'%')->first();
                if ($account)
                {
                    $lastUpdatedDate =strtotime($account->last_updated_at);
                    $transactionDate =strtotime($arrayData['date_of_transaction']);


                    if (!$account->last_updated_at)
                    {
                        $this->updateAccount($account,$arrayData);
                        $this->createBankTransaction($account,$arrayData);
                    }
                    elseif ($lastUpdatedDate <= $transactionDate)
                    {
                        $this->updateAccount($account,$arrayData);
                        $this->createBankTransaction($account,$arrayData);
                    }
                }else{
                    $this->createBankTransaction($account,$arrayData);
                }
            }
        }


        return 'Done';
    }

    public function getTheLatestFileFromGDrive()
    {

        $collection = collect(Storage::cloud()->listContents('1jmEub4s6hqUhgpXVN3UTODcfmKhAv768/1oZg3SVHr3b4Mh1fwFaOC1xsvpfhbMmQD'));
        $contents = array_values(array_sort($collection, function ($value)
        {
            return $value['timestamp'];
        }));
        $contents = array_reverse($contents);
        $fileName = $contents[0]['filename'];
        $fileRawData = Storage::cloud()->get('1jmEub4s6hqUhgpXVN3UTODcfmKhAv768/1oZg3SVHr3b4Mh1fwFaOC1xsvpfhbMmQD/1qUFUnfxm5V_2Ap_FMJJdgTe-NrOYnHj0');
        return array(
            'fileData'=>$fileRawData,
            'fileName'=>$fileName
        );

    }

    public function structurizeTheDataAccordingToTransactionsPerAccount($fileRawData)
    {
        $remove = "\r\n";
        $split = explode($remove, $fileRawData['fileData']);
        $recordArray[] = null;
        foreach ($split as $string)
        {
            array_push($recordArray,$string);
        }
        $arrayOfGroupedTransactions = array();
        $counter = 0 ;
        // Grouping the same transactions
        foreach ($recordArray as $key => $value)
        {
            if (strpos($value, ":20:STARTUMS") !== false)
            {
                $counter++;
                unset($recordArray[$key]);
            }else{
                $arrayOfGroupedTransactions[$counter][] = $value;
            }
        }

        return $arrayOfGroupedTransactions;
    }

    public function updateAccount($account,$arrayData)
    {
        $account->update([
            'balance' => $arrayData['remaining_balance'],
            'last_updated_at' => $arrayData['date_of_transaction']
        ]);
    }

    public function createBankTransaction($account,$arrayData)
    {
        $this->bankTransaction->create([
            'account_id' => isset($account->id)?$account->id:'',
            'amount'=> isset($arrayData['amount'])?$arrayData['amount']:'',
            'date_of_transaction'=> isset($arrayData['date_of_transaction'])?$arrayData['date_of_transaction']:'',
            'remaining_balance'=> isset($arrayData['remaining_balance'])?$arrayData['remaining_balance']:'',
            'uploaded_file_name'=> isset($arrayData['uploaded_file_name'])?$arrayData['uploaded_file_name']:'',
            'bank_transaction_type'=> isset($arrayData['bank_transaction_type'])?$arrayData['bank_transaction_type']:'',
            'end_balance_complete'=> isset($arrayData['end_balance_complete'])?$arrayData['end_balance_complete']:''
        ]);

    }
}
