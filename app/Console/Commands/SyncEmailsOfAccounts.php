<?php

namespace App\Console\Commands;

use App\EmailAccount;
use App\Models\Email;
use App\Models\EmailMapping;
use App\Models\EmailType;
use Illuminate\Database\QueryException;
use Webklex\IMAP\Client;

use Illuminate\Console\Command;

class SyncEmailsOfAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:syncEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $email;
    protected $emailType;
    protected $emailMapping;

    public function __construct(Email $email, EmailMapping $emailMapping, EmailType $emailType )
    {
        parent::__construct();
        $this->email = $email;
        $this->emailMapping = $emailMapping;
        $this->emailType = $emailType;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->syncEmails();
    }


    public function syncEmails()
    {
        $accounts = EmailAccount::all();
        foreach ($accounts as $account)
        {
            $emailType = $this->emailType->all();
            foreach ($emailType as $type)
            {
                $this->storeEmail($type,$account);
            }
        }
    }

    public function storeEmail($type,$account)
    {
        $client = new Client([
            'host'          => 'imap.fcr-immobilien.de',
            'port'          => 993,
            'encryption'    => 'ssl',
            'validate_cert' => false,
            'username'      => $account->email,
            'password'      => $account->password,
            'protocol'      => 'imap'
        ]);
        $client->connect();
        $mailBoxFolder = $client->getFolder($type->type);
        $messages = $mailBoxFolder->query()->setFetchAttachment(false)->get();
        try {

            foreach ($messages as $message)
            {
                $ids =[];

                $createdEmail = $this->email->updateOrCreate(
                    ['message_id' => $message->message_id,],
                    ['message_id' => $message->message_id,
                        'subject' => $message->subject,
                        'body' => isset($message->bodies['text'])?$message->bodies['text']->content:'',
                        'bcc' => '',
                        'cc' => '',
                        'email_sent_from' => $message->sender[0]->mail,
                        'date_of_email' => strval($message->date)]);

                $ids = ['email_id' => $createdEmail->id,'account_id'=> $account->id,'email_type_id'=>$type->id];
                $this->storeEmailMapping($ids);

            }

        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062){
                return 'Duplicate Entry';
            }
        }

    }

    public function storeEmailMapping($ids)
    {
        $this->emailMapping->updateOrCreate(
            ['email_id'=>$ids['email_id']],
            ['email_id' => $ids['email_id'],
                'account_id'=> $ids['account_id'],
                'email_type_id'=>$ids['email_type_id']
            ]);
    }
}
