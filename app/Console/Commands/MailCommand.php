<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use App\Repository\AirbnbApi;
use App\user_airbnb_profile;
use App\Setting;
use App\User;
use DB;
use App\listing;
use App\thread_user;
use App\threads;
use App\post;
use App\reservation_statuses;
use Illuminate\Support\Carbon;



class MailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will use to send email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $date = \Carbon\Carbon::now()->addDays(7)->format('Y-m-d');


        $properties = DB::table('properties')->select('users.email', 'properties.exklusivität_bis', 'u.name')
            ->join('users as u', 'u.id', '=', 'properties.transaction_m_id')
            ->join('users', 'users.id', '=', 'properties.user_id')->where('properties.exklusivität_bis', '=', $date)
            ->get();

        foreach ($properties as $property){

            $text = "Ende Exklusivität für das Objekt ".$property->name_of_property." von ".$property->name."  in einer Woche.";
            Mail::raw($text, function ($message) {
                $message->to($property->email)
                    ->from('no-reply@intranet.fcr-immobilien.de')
                    ->bcc('a.raudies@fcr-immobilien.de','Andrea Raudies')
                    ->subject('Ablauf Exklusivität');
            });
        }




    }
 





}

