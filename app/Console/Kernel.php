<?php

namespace App\Console;

use App\Console\Commands\CreateNewFolderInGDriveForAllProperties;
use App\Console\Commands\LogoutAllUsers;
use App\Console\Commands\SyncEmailsOfAccounts;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CreatePropertyDirectory::class,
        SyncEmailsOfAccounts::class,
        CreateNewFolderInGDriveForAllProperties::class,
        LogoutAllUsers::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->command('property:createDirectory')
//                 ->hourly();
//        $schedule->command('command:syncEmail')->everyFiveMinutes();
        $schedule->command('command:logoutUsers')
                 ->timezone('Europe/Berlin')->dailyAt('19:00');
        $schedule->command('command:insertBankTransactions')
            ->timezone('Europe/Berlin')->cron('0 */2 * * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
