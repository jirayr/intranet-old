<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Profit_loss_graph extends Model
{
     protected $table = 'profit_loss_graph';
     protected $fillable = array('rent_begin', 'rent_end', 'actual_net_rent', 'schedule_item_id');

     public $timestamps = false;

}

                 