<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenancyScheduleComment extends Model
{
    protected $fillable = ['item_id','comment','user_name','external_comment'];


}

