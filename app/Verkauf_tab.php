<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Verkauf_tab extends Model
{
     protected $table = 'verkauf_tab';
     protected $fillable = array('property_id', 'tab_value', 'row_name', 'new_added', 'fixed_rows', 'monat','monat_check');

     public $timestamps = false;

}

                 