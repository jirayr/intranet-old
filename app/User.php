<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'id','name', 'email','role', 'password','password_change_date','created_at','updated_at',
		'image','first_name','last_name','birthday','phone','mobile','address','gender',
		'website','postal_code','second_role','signature','company', 'user_deleted', 'user_status','last_login_time'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin(){
        return $this->role === config('auth.role.admin');
    }
    public function isGuest(){
        return $this->role === config('auth.role.guest');
    }
    /**
     * Get the comments for the blog user.
     */
    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }
    public function getshortname($array=array())
    {
        $name = explode(" ", $this->name);
        $s = "";
        if(isset($name[0]))
            $s .= $name[0][0];
        if(isset($name[1]))
            $s .= $name[1][0];

        if($array && $s && isset($array[$s]))
        {
            if(isset($name[1][1]))
            $s .= $name[1][1];
        }

        return $s;
    }

    public function properties()
    {
        return $this->hasMany('App\PropertyPermission','user_id');
    }
}
