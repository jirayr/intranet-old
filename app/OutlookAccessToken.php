<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutlookAccessToken extends Model
{
    protected $fillable = ['access_token', 'refresh_token','expires_in','validity'];
}
