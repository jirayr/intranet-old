<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 5/1/2020
 * Time: 11:32 AM
 */

namespace App\Repositories;


use App\ProjectEmployee;
use App\PropertyProject;

class PropertyProjectRepository
{
    protected  $propertyProject;

    public function __construct(PropertyProject $propertyProject)
    {
        $this->propertyProject = $propertyProject;
    }

    public function store($data)
    {
        return $this->propertyProject->create($data->all());
    }

    public function all()
    {
       return $this->propertyProject->all()->orderBy('created_at','desc');
    }

    public function getByPropertyId($id)
    {
        return $this->propertyProject->where('property_id',$id)->get();
    }

    public function find($id)
    {
        return $this->propertyProject->where('id',$id)->with('employees','userEmployees')->first();
    }

    public function update($id,$request)
    {
        if (!isset($request->date)){
            unset($request['date']);

        }
        $this->propertyProject->find($id)->update($request->all());
        ProjectEmployee::where('property_project_id',$id)->delete();

        foreach ( $request->employee_id as $eId)
        {
            ProjectEmployee::create([
                'property_project_id' => $id,
                'company_employee_id' => $eId
            ]);
        }
        foreach ($request->employee_user as $uId)
        {
            ProjectEmployee::create([
                'property_project_id' => $id,
                'user_id' => $uId,
            ]);
        }

    }

    public function delete($id)
    {
        ProjectEmployee::where('property_project_id',$id)->delete();
        $this->propertyProject->where('id',$id)->with('employees')->delete();
    }


}
