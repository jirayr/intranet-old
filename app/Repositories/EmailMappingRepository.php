<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 3/31/2020
 * Time: 5:46 PM
 */

namespace App\Repositories;


use App\Models\EmailMapping;

class EmailMappingRepository
{
    protected $emailMapping;

    public function __construct(EmailMapping $emailMapping)
    {
        $this->emailMapping = $emailMapping;
    }


    public function getEmailsByAccountAndEmailType($accountId,$emailTypeId)
    {
       return $this->emailMapping->where('account_id',$accountId)
           ->where('email_type_id',$emailTypeId)
           ->orderBy('created_at','DESC')
           ->with(['emailAccount','emails'=> function($query) {
               $query->orderBy('date_of_email','DESC');}
           ])->paginate(25);
    }

    public function getAllEmails($emailTypeId)
    {
      return $this->emailMapping->where('email_type_id',$emailTypeId)
          ->orderBy('created_at','DESC')
          ->with(['emailAccount','emails'=> function($query) {
              $query->orderBy('date_of_email','DESC');
          }
          ])->paginate(25);
    }

    public function emailSearch($data)
    {
      return $this->emailMapping->where('email_type_id',$data['type'])
          ->orderBy('created_at','DESC')
          ->with(['emails'=> function($query)use($data) {
          $query->where('email_sent_from','LIKE','%'.$data['search'].'%')
          ->orWhere('subject','LIKE','%'.$data['search'].'%')
          ->orWhere('body','LIKE','%'.$data['search'].'%')
          ->orderBy('date_of_email','DESC')
          ->get();}
          ])->paginate(10000);
    }

    public function getEmailsByDomainAndEmailType($domain,$emailType)
    {
          return $this->emailMapping->where('email_type_id',$emailType)
            ->with(['emails'=> function($query)use($domain) {
                $query->where('email_sent_from','LIKE','%'.$domain.'%')
                ->get();}
            ])->get();
    }


}
