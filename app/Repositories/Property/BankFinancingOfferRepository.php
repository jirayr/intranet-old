<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 5/6/2020
 * Time: 11:48 AM
 */

namespace App\Repositories\Property;


use App\Models\BankFinancingOffer;

class BankFinancingOfferRepository
{
    protected $bankFinancingOffer;

    public function __construct(BankFinancingOffer $bankFinancingOffer)
    {
        $this->bankFinancingOffer = $bankFinancingOffer;
    }

    public function store($data)
    {
        $this->bankFinancingOffer->create($data);
    }

    public function find($id)
    {
        return $this->bankFinancingOffer->where('id',$id)->with('bank')->first();
    }

    public function update($data , $id)
    {
        $this->bankFinancingOffer->find($id)->update($data);
    }

    public function delete($id)
    {
        $this->bankFinancingOffer->find($id)->delete();
    }
}
