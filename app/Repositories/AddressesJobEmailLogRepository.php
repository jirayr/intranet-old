<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 4/22/2020
 * Time: 12:36 PM
 */

namespace App\Repositories;


use App\AddressesJobEmailLog;
class AddressesJobEmailLogRepository
{
    protected $addressesJobEmailLog;

    public function __construct(AddressesJobEmailLog $addressesJobEmailLog)
    {
        $this->addressesJobEmailLog = $addressesJobEmailLog;
    }

    public function store($email,$addressObj)
    {
        $this->addressesJobEmailLog->create([
            'email'=> $email,
            'addresses_job_id'=> $addressObj->id,
            'user_id' => Auth()->user()->id
        ]);
    }

    public function get()
    {
       return $this->addressesJobEmailLog->with('creator','addressesJob')->get();
    }

    public function getByAddressesJobId($addressesJobId)
    {
        return $this->addressesJobEmailLog->where('addresses_job_id',$addressesJobId)->pluck('email')->toArray();
    }

    public function getAllMails()
    {
        return $this->addressesJobEmailLog->pluck('email')->toArray();
    }

}
