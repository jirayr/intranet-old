<?php

namespace App\Repositories;


use App\OutlookAccessToken;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: aliraza
 * Date: 11/08/2019
 * Time: 7:20 PM
 */
class OutlookAccessTokenRepository
{
    protected  $accessToken;

    public function __construct(OutlookAccessToken $accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function getToken()
    {
        return $this->accessToken->first();
    }

    public function updateByName($name, $token)
    {
        return $this->accessToken->where('api_name', $name)->update([
            'token' => $token
        ]);
    }

    public function ifExist()
    {
       return $this->accessToken->exists();
    }

    public function store($data)
    {
        $config['access_token'] = $data['access_token'];
        $config['expires_in'] = $data['expires_in'];
        $config['refresh_token'] = $data['refresh_token'];
        $config['validity'] = Carbon::now()->addMinute(50);
        $this->accessToken->create($config);
    }

    public function update($accessToken)
    {
        $token['access_token'] = $accessToken['access_token'];
        $token['expires_in'] = $accessToken['expires_in'];
        $token['refresh_token'] = $accessToken['refresh_token'];
        $token['validity'] =  Carbon::now()->addMinute(50);
        $this->accessToken->first()->update($token);
    }
}