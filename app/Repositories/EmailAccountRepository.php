<?php
/**
 * Created by PhpStorm.
 * User: PAKISTAN COMPUTER
 * Date: 3/29/2020
 * Time: 3:45 PM
 */

namespace App\Repositories;


use App\EmailAccount;
use Illuminate\Support\Facades\DB;

class EmailAccountRepository
{
    protected $emailAccount;
    public function __construct(EmailAccount $emailAccount)
    {
        $this->emailAccount = $emailAccount;
    }

    public function store($data)
    {
        foreach ($data as $dat)
        {
            $obj =  $this->emailAccount->create([
                'email' => $dat['email'],
                'password'=> $dat['password'],
                'name'=> $dat['name'],

            ]);
        }
       return $obj;
    }

    public function get()
    {
        return $this->emailAccount->all();
    }

    public function edit($id)
    {
        return $this->emailAccount->find($id);
    }

    public function update($id,$data)
    {
        return  $this->emailAccount->find($id)->update([
            'email' => $data['email'],
            'password'=> $data['password'],
            'name'=> $data['name'],


        ]);
    }


    public function delete($id)
    {
        return  $this->emailAccount->find($id)->delete();
    }
}
