<?php
function diff360($date1, $date2) {
    //format: Y-m-d
    if (!$date1 || !$date2){
        return 0;
    }

    $date1 = date_create(str_replace('/', '-',$date1));
    $date2 = date_create(str_replace('/', '-',$date2));

    $diff = date_diff($date1,$date2);

    return $diff->days;
}
function get_paid_checkbox($id,$is_paid)
{
    if($is_paid)
    return "<input class='ck' type='checkbox' checked>";
    else
        return "";
}
function date_formatting($v)
{
    $v  = str_replace('/', '.', $v);
    return $v;
}
function user_short_name($string)
{
    if($string){
        $arr = explode(" ", $string);
        if(count($arr)>1)
            return substr($arr[0], 0,1).$arr[1];
        else
            return $string;
    }
    return $string;
}
function pre($v)
{
    print "<pre>";
    print_r($v); die;
}
function show_date_format($date)
{
    if(!$date)
        return "";

    if($date=='0000-00-00')
        return "";       
    if (strpos($date, '.') !== false)
    {
        return $date;    
    } 
    if (strpos($date, '-') !== false || strpos($date, '/') !== false) {
        return date_format(date_create($date) , 'd.m.Y');
    }
    return $date;
}
function show_datetime_format($date,$format="d.m.Y H:i:s")
{
    if(!$date)
        return "";
    $timezone='Europe/Berlin';
    $userTimezone = new DateTimeZone($timezone);
    $gmtTimezone = new DateTimeZone('UTC');
    $myDateTime = new DateTime($date, $gmtTimezone);
    $offset = $userTimezone->getOffset($myDateTime);

    return date($format, strtotime($date)+$offset);
}
function save_date_format($date)
{
    if(!$date)
        return "";
    return date_format(date_create(str_replace('.', '-', $date)) , 'Y-m-d');
}

function validatedate($date)
{
    if($date=="")
        return 1;
    
    if(strlen($date)!=10)
        return 0;

    if(date_create(str_replace('.', '-', $date)) && $date == date_format(  date_create(str_replace('.', '-', $date)) , 'd.m.Y'))
    {
        return 1;
    }
    return 0;
}
function pr($arr = []){
    echo '<pre/>';print_r($arr);exit;
}

function show_number($number,$decimal=0)
{
    if(is_numeric($number))
    {
        return number_format($number, $decimal,",",".");
    }
    return $number;
}

function is_monday(){
    
    $res = false;

    $time = strtotime('first day of this month');

    $currentDate = date('Y-m-d');

    $firstDay = date('Y-m-d', $time);
    $firstMonday = date('Y-m-d', strtotime('First monday of this month'));
    $secondMonday = date('Y-m-d', strtotime('Second monday of this month'));
    
    $first = date('D', $time);
    $second = date('D', strtotime("+1 day", $time));
    $third = date('D', strtotime("+2 day", $time));
    $fourth = date('D', strtotime("+3 day", $time));
    
    if(in_array('Mon', [$first, $second, $third, $fourth])){
        if($currentDate >= $firstDay && $currentDate <= $secondMonday){
            $res = true;
        }
    }else{
        if($currentDate >= $firstDay && $currentDate <= $firstMonday){
            $res = true;
        }
    }
    
    return $res;
}

function getWeekMonSun($weekOffset) {
    $dt = new DateTime();
    $dt->setIsoDate($dt->format('o'), $dt->format('W') + $weekOffset);
    return array(
        'Mon' => $dt->format('Y-m-d'),
        'Sun' => $dt->modify('+6 day')->format('Y-m-d'),
    );
}

function isLocalhost($whitelist = ['127.0.0.1', '::1']) {
    return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
}

function make_amount($amount = 0){
    if($amount){
        $amount = str_replace('.','',$amount);
        $amount = str_replace(',','.',$amount);
        $amount = str_replace('%','',$amount);
        $amount = str_replace('€','',$amount);
        return $amount;
    }else{
        return 0;
    }
}

/*function weekOfMonth($when = null) {
    if ($when === null) $when = time();
    $week = date('W', $when); // note that ISO weeks start on Monday
    $firstWeekOfMonth = date('W', strtotime(date('Y-m-01', $when)));
    return 1 + ($week < $firstWeekOfMonth ? $week : $week - $firstWeekOfMonth);
}*/

function weekOfMonth($when = null) {
    $date = ($when) ? $when : date('y-m-d');
    $firstOfMonth = date("Y-m-01", strtotime($date));
    return intval(date("W", strtotime($date))) - intval(date("W", strtotime($firstOfMonth)));
}

function calculateAusgaben($value){
    $amount = 0;
    $value = ($value) ? $value : 0;
    $week = weekOfMonth();
    // echo $week;die;
    $weekPer = [0=>100,1 => 100, 2 => 75, 3 => 50, 4 => 25];
    if(in_array($week, [0,1,2,3,4])){
        $amount = ( $weekPer[$week] / 100 ) * $value;
    }
    return $amount;
}

function get_property_status($status){
    $arr = [
        config('properties.status.externquicksheet')    => __('property.externquicksheet'),
        config('properties.status.quicksheet')          => __('property.quicksheet'),
        config('properties.status.in_purchase')         => __('property.in_purchase'),
        config('properties.status.offer')               => __('property.offer'),
        config('properties.status.exclusive')           => __('property.exclusive'),
        config('properties.status.liquiplanung')        => __('property.liquiplanung'),
        config('properties.status.exclusivity')         => __('property.exclusivity'),
        config('properties.status.certified')           => __('property.certified'),
        config('properties.status.duration')            => __('property.duration'),
        config('properties.status.hold')                => __('property.hold'),
        config('properties.status.sold')                => __('property.sold'),
        config('properties.status.declined')            => __('property.declined'),
        config('properties.status.lost')                => __('property.lost'),
    ];

    return (isset($arr[$status])) ? $arr[$status] : $status;
}

function get_property_status_value($status){
    $arr = [
        __('property.externquicksheet') => config('properties.status.externquicksheet'),
        __('property.quicksheet')       => config('properties.status.quicksheet'),
        __('property.in_purchase')      => config('properties.status.in_purchase'),
        __('property.offer')            => config('properties.status.offer'),
        __('property.exclusive')        => config('properties.status.exclusive'),
        __('property.liquiplanung')     => config('properties.status.liquiplanung'),
        __('property.exclusivity')      => config('properties.status.exclusivity'),
        __('property.certified')        => config('properties.status.certified'),
        __('property.duration')         => config('properties.status.duration'),
        __('property.hold')             => config('properties.status.hold'),
        __('property.sold')             => config('properties.status.sold'),
        __('property.declined')         => config('properties.status.declined'),
        __('property.lost')             => config('properties.status.lost'),
    ];

    return (isset($arr[$status])) ? $arr[$status] : false;
}
function get2lettersname($name)
{
    $name = explode(" ", $name);
    $s = "";
    if(isset($name[0][0]))
        $s .= $name[0][0];
    if(isset($name[1][0]))
        $s .= $name[1][0];

    return $s;
}

function getConfidentialInvoiceUser(){
    $arr = [config('users.falk_email'), 't.raudies@fcr-immobilien.de'];
    return $arr;
}

function fetch_mails_by_string($text){
    $str = '/([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+/i';
    preg_match_all($str, $text, $out);
    return isset($out[0]) ? $out[0] : array();
}
function getobjekttypearray()
{
    return array('shopping_mall'=>'Einkaufszentrum','retail_center'=>'Fachmarktzentrum','specialists'=>'Fachmarkt','retail_shop'=>'Einzelhandel','office'=>'Büro','logistik'=>'Logistik','commercial_space'=>'Kommerzielle Fläche','land'=>'Grundstück','apartment'=>'Wohnung','condos'=>'Eigentumswohnung','house'=>'Haus','living_and_business'=>'Wohn- und Geschäftshaus','villa'=>'Villa','hotel'=>'Hotel','nursing_home'=>'Pflegeheim', 'multifamily_house'=>'Mehrfamilienhaus','dormitory'=> 'Studentenwohnheim','other'=>'Sonstiges');
}
function falk_dashboard_ids()
{
    return [1,4,84];
}
?>