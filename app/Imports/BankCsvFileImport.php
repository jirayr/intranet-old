<?php

namespace App\Imports;

use App\Models\Banks;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BankCsvFileImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row)
        {
            Banks::create([
                'keyword'=> $row[0],
                'name'=> $row[1],
                'address'=> $row[2],
                'city'=> $row[4],
                'district'=> $row[5],
                'postal_code'=> $row[7],
                'contact_phone'=> $row[8],
                'internet'=> $row[9],
                'location_latitude'=> $row[10],
                'location_longitude'=> $row[11],
                'contact_email'=> $row[22],

            ]);
        }
    }
}
