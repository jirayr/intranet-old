<?php

namespace App\Imports;

use App\Models\PropertyRentPaids;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;

class ExcelImportMieterliste implements ToCollection
{
    private $data;
    private $propertyId;

    public function __construct($data, $propertyId)
    {
        $this->data = $data;     //Inject data
        $this->propertyId = $propertyId;     //Inject data
    }


    public function collection(Collection $collection)
    {
//        if (strpos( $this->data->file('excel')->getClientOriginalName(),'120_opmieteigdetail.xls')!== false ){

            // pre($collection);

    $month = date('m');
    $year = date('Y');

    PropertyRentPaids::where('property_id',$this->propertyId)->delete();

            // print "<pre>";

            $type = '';
            $mieter = '';
            $isSv = false;
            $date = '';
            $BuchungsText = '';
            foreach ($collection as $key => $row)
            {
                if ($row[1] == 'SV') {
                    $type = 'sv';
                    $isSv = true;
                    $date = $row[0];
                    $BuchungsText = $row[2];
                }elseif (strstr($row[1],'MSoll'))
                {
                    $isSv = true;
                    $type = 'Msoll';
                    $date = $row[0];
                    $BuchungsText = $row[2];

                }
                elseif ($row[1] == 'MZhlg'){
                    $isSv = true;
                    $type = 'Msoll';
                    $date = $row[0];
                    $BuchungsText = $row[2];

                }

                


                if(strstr( $row[0], 'Mieter/Eigentümer' )){
                    $mieter = str_replace("Mieter/Eigentümer", "",$row[0]);
                }
                
                if ($isSv)
                {
                    // print_r($row);

                    if ($row[1] != 'SV' && $row[1] != 'JournalArt' && $row[1] != 'MSoll' && $row[1] != 'MZhlg')
                    {
                        if ($row[1] != null && ($row[4] || $row[8] || $row[10]))
                        {
                          if($row[5])
                          {
                           $row[5] =  \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[5]);
                          }

                          // $c = PropertyRentPaids::where('property_id',$this->propertyId)->where('upload_month',$month)->where('upload_year',$year)->where('mieter',$mieter)->first();
                          $c = 0;

                          $data[] = [
                            'property_id'=>$this->propertyId ,
                            'user_id' =>Auth()->user()->id,
                            'mieter'=>$mieter,
                            'sv'=>$row[1],
                            'soll'=>$row[4],
                            'ist'=>$row[8],
                            'dta'=>$row[9],
                            'diff'=>$row[10],
                            'zahlungsdatum'=>$row[5],
                            'type'=> $type,
                            'datum'=> $date,
                            'upload_month' => $month,
                            'upload_year' => $year,
                            'buchungs_text'=> $BuchungsText
                        ];



                          if(false && $c)
                          {
                            $c->user_id =Auth()->user()->id;
                            $c->sv=$row[1];
                            $c->soll=$row[4];
                            $c->ist=$row[8];
                            $c->dta=$row[9];
                            $c->diff=$row[10];
                            $c->zahlungsdatum=$row[5];
                            $c->type= $type;
                            $c->datum= $date;
                            $c->save();
                          }
                          else

                          PropertyRentPaids::create([
                            'property_id'=>$this->propertyId ,
                            'user_id' =>Auth()->user()->id,
                            'mieter'=>$mieter,
                            'sv'=>$row[1],
                            'soll'=>$row[4],
                            'ist'=>$row[8],
                            'dta'=>$row[9],
                            'diff'=>$row[10],
                            'zahlungsdatum'=>$row[5],
                            'type'=> $type,
                            'datum'=> $date,
                            'upload_month' => $month,
                            'upload_year' => $year,
                            'buchungs_text'=> $BuchungsText
                        ]);
                        }
                    }
                }
            }
//        }else {
//
//            $this->storeDataOfFileKopieVon1117($collection);
//        }

        // pre($data);

    }

    public function storeDataOfFileKopieVon1117($collection){

        $type = '';
        $mieter = '';
        $isSv = false;
        $date = '';
        foreach ($collection as $key => $row)
        {

            if ($row[2] == 'SV') {
                $isSv = true;
                $type = 'sv';
                $date = $row[0];
            }elseif ($row[2] == 'MSoll'){
                $isSv = true;
                $type = 'Msoll';
                $date = $row[0];
            }

            if(strstr( $row[0], 'Mieter/Eigentümer' ))
            {
                $mieter = str_replace("Mieter/Eigentümer", "",$row[0]);
            }

             if ($isSv)
            {

                if ($row[2] != 'SV' && $row[2] != 'JournalArt' && $row[2] != 'MSoll' && $row[2] != 'MZhlg')
                {

                    if ($row[2] != null)
                    {
                        PropertyRentPaids::create([
                            'property_id'=>$this->propertyId ,
                            'user_id' =>Auth()->user()->id,
                            'mieter'=>$mieter,
                            'sv'=>$row[2],
                            'soll'=>$row[7],
                            'ist'=>$row[9],
                            'dta'=>$row[10],
                            'diff'=>$row[11],
                            'zahlungsdatum'=>$row[8],
                            'type'=>$type,
                            'datum'=>$date,
                            'upload_month' => $this->data['upload_month']

                        ]);
                    }
                }
            }
        }



        }

}