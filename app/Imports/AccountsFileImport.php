<?php

namespace App\Imports;

use App\Account;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class AccountsFileImport implements ToCollection
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;     //Inject data
    }


    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $key => $account)
        {
            if ($key>=2)
            Account::create([
                'name_of_property' => $account[1],
                 'account_owner' => $account[2],
                 'bank_name' => $account[3],
                 'iban' => $account[5],
                 'contact_person' => $account[9]
            ]);
        }
    }
}
