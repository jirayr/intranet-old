<?php

namespace App\Imports;

use App\Address;
use App\Company;
use App\CompanyEmployee;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
ini_set('max_execution_time', 180); //3 minutes

class ExcelFileImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {

        foreach ($collection as $row)
        {
           $enteredCompany = Company::where('name','like',  '%' . $row[1] .'%')->first();
           if(!$enteredCompany)
           {
               $company = Company::create([
                   'name' => $row[1],
               ]);

               $employee = CompanyEmployee::create([
                   'company_id' => $company->id,
                   'anrede' => $row[2],
                   'vorname' => $row[3],
                   'nachname' => $row[4],
                   'phone' => $row[9],
                   'fax' => $row[10],
                   'email' => $row[11],
                   'internet' => $row[12],
                   'notes' => $row[13],
                   'last_contact' => $row[14],
                   'keyword' => $row[15],
                   'kennung' => $row[0],

               ]);

               Address::create([
                   'company_id' => $company->id ,
                   'employee_id' => $employee->id ,
                   'zip' => $row[6],
                   'street' => $row[5],
                   'city' => $row[7],
                   'part_of_city' => $row[8],
               ]);
           }else{

               $employee = CompanyEmployee::create([
                   'company_id' => $enteredCompany->id,
                   'anrede' => $row[2],
                   'vorname' => $row[3],
                   'nachname' => $row[4],
                   'phone' => $row[9],
                   'fax' => $row[10],
                   'email' => $row[11],
                   'internet' => $row[12],
                   'notes' => $row[13],
                   'last_contact' => $row[14],
                   'keyword' => $row[15],
                   'kennung' => $row[0],

               ]);

               Address::create([
                   'company_id' => $enteredCompany->id ,
                   'employee_id' => $employee->id ,
                   'zip' => $row[6],
                   'street' => $row[5],
                   'city' => $row[7],
                   'part_of_city' => $row[8],
               ]);

           }

        }
      }
}
