<?php

namespace App\Imports;

use App\email_template2_sending_info;
use App\Models\Banks;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BankFinancingOfferExport implements ToCollection
{
    /**
    * @param Collection $collection
    */

    private $data;
    public function __construct($data)
    {
        $this->data = $data;     //Inject data
    }

    public function collection(Collection $collection)
    {
        foreach ($collection  as $key => $row)
        {
            if ($key >= 3)

                if ($row[2])
                email_template2_sending_info::create([

                    'property_id'=>$this->data['property_id'],
                    'email_to_send' =>$row[5],
                    'contact_person'=>$row[3],
                    'address'=>$row[2],
                    'notizen'=>$row[6]
                ]);
        }
    }
}
