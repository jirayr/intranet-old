<?php

namespace App\Imports;

use App\BankAccountUpload;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;
use NumberFormatter;

class BankDataImport implements ToCollection
{
    protected $data ;

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection  as $key => $row)
        {
            if ($key >= 3)
            {
                $mergeData = $row[0].','.$row[1];
                $data = explode(';',$mergeData);
                if (isset($data[4]) && $data[4] != '')
                {
                    BankAccountUpload::updateOrCreate(
                        ['account_designation'=>$data[0]],[
                        'account_designation' => $data[0],
                        'iban' => $data[1],
                        'account_owner' => $data[2],
                        'date_of_booking_balance' => isset($data[4])?$data[4]:'',
                        'book_balance' =>  isset($data[6])?$data[6]:'',
                        'currency' => isset($data[7])?$data[7]:'',
                        'account_category' => isset($data[8])?$data[8]:'',
                    ]);
                }
            }









        }
    }
}
