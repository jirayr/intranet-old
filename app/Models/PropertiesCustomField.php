<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertiesCustomField extends Model
{
    protected $guarded = [];
    protected $table = "properties_custom_fields";
	public $timestamps = true;

}
