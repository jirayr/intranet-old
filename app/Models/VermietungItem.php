<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VermietungItem extends Model
{
    protected $table = "vermietung_items";
}
