<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacantUploadingStatus extends Model
{
    protected $guarded = [];
    protected $table = "vacant_uploading_statuses";
	public $timestamps = true;
}
