<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Provision extends Model
{
	protected $table = "provisions";
	public $timestamps = true;
	protected $guarded = [];

	public function asset_manager()
	{
		return $this->belongsTo('App\User', 'asset_m_id');
		# code...
	}
}
