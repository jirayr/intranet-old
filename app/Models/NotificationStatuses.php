<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationStatuses extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'notification_id',
        'user_id',
        'status'
    ];

    protected $table = "notification_statuses";
}
