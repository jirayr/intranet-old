<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertiesDeals extends Model
{
    protected $guarded = [];
    protected $table = "properties_deals";
	public $timestamps = true;


	public function property_deal_user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
}
