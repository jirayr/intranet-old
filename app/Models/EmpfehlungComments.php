<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpfehlungComments extends Model
{
    protected $guarded = [];
    protected $table = "empfehlung_comments";
	public $timestamps = true;
}
