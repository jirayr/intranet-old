<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyContracts extends Model
{
    protected $guarded = [];
    protected $table = "property_contracts";
	public $timestamps = true;


	public function property_contact_user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

}
