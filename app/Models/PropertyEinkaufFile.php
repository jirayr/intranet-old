<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyEinkaufFile extends Model
{
	protected $fillable = [
		'id',
		'property_id', 
		'property_buy_detail_type', 
		'file_name', 
		'file_basename',
		'file_path',
		'file_href',
		'extension',
		'type'
	];
	protected $table = "property_einkauf_file";
	public $timestamps = true;

}
