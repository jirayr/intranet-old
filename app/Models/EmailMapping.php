<?php

namespace App\Models;

use App\EmailAccount;
use Illuminate\Database\Eloquent\Model;

class EmailMapping extends Model
{
    protected $connection = 'mysql2';
    protected $fillable = ['email_id','account_id','email_type_id','is_read','is_starred'];

    public function emails()
    {
       return $this->belongsTo(Email::class,'email_id');
    }

    public function emailAccount()
    {
        return $this->belongsTo(EmailAccount::class,'account_id');
    }
}
