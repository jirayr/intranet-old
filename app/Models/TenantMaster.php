<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenantMaster extends Model
{
    protected $primaryKey = 'id';

    protected $table = "tenant_master";
    public $timestamps = true;


    public function scheduleItem()
	{
		return $this->belongsTo('App\Models\TenancyScheduleItem', 'tenant_master_id');
		# code...
	}

}
