<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LiquiplanungAoCosts extends Model
{
    protected $guarded = [];
    protected $table = "liquiplanung_ao_costs";
	public $timestamps = true;


	public function user(){
		return $this->belongsTo('App\User', 'user_id');
	}

	public function property(){
		return $this->belongsTo('App\Models\Properties', 'property_id');
	}
}
