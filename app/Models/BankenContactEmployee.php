<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankenContactEmployee extends Model
{
	protected $fillable = [
		'id',
		'banken_id',
        'vorname',
        'nachname',
        'telefon',
        'email',
	];
	protected $table = "banken_contact_employee";
	public $timestamps = true;
}
