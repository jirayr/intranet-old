<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForecastKind extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'forecast_id',
        'kind',
        'description',
        'status',
        'notary',
        'bnl',
        'total_purchase_price',
        'fk_or_sale',
        'reflux',
        'note',
        'type'
    ];

    protected $table = "forecast_kinds";

    public $timestamps = false;
}
