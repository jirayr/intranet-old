<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
	protected $fillable = [
		'id',
		'user_id', // User Creator
		'name', // Bank name
		'picture',
		
		// all information of 2.) Finanzierungsstruktur
		'with_real_ek',
		'from_bond',
		'bank_loan',
		'interest_bank_loan',
		'eradication_bank',
		'interest_bond',
		'contact_name',
		'contact_phone',
		'contact_email',
		'address',
		'location_latitude',
		'location_longitude',
		'notes',
        'keyword',
        'name',
        'city',
        'district',
        'postal_code',
        'internet',




	];
	protected $table = "banks";
	public $timestamps = false;

}
