<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertiesTenants extends Model
{
	protected $fillable = [
		'id',
		'propertyId',
		'tenant',
		'net_rent_p_a', 
		'mv_end', 
	];
	protected $table = "properties_tenants";
	public $timestamps = false;
}
