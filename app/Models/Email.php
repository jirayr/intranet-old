<?php

namespace App\Models;

use App\EmailAccount;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $connection = 'mysql2';

    protected $fillable = ['message_id','subject','','body','bcc','cc','email_sent_from','date_of_email'];

    public function emailMapping()
    {
        return $this->hasOne(EmailMapping::class);
    }


}
