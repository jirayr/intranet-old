<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProvisionComments extends Model
{
   	protected $guarded = [];
    protected $table = "provision_comments";
	public $timestamps = true;
}
