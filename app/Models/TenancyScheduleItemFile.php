<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenancyScheduleItemFile extends Model
{
    protected  $table = 'tenancy_schedule_item_files';

    protected $fillable = [  'property_id',  'tenancy_schedule_item_id', 'file_name', 'file_basename', 'file_path', 'file_href', 'extension', 'type'];
}
