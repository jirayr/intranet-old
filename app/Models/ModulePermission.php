<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModulePermission extends Model
{
    protected $fillable = [
		'id',
		'user_id',
		'permission'
	];
	protected $table = "module_permissions";
	public $timestamps = true;
}
