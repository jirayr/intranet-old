<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyCalendar extends Model
{
    //
	 protected $fillable = [
        'id',
        'description',
        'start',
        'end',
		'user_id',
		'property_id'
    ];
	public $timestamps = false;
}
