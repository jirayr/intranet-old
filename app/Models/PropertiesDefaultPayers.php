<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertiesDefaultPayers extends Model
{
    protected $guarded = [];
    protected $table = "properties_default_payers";
	public $timestamps = true;


	public function default_payer_user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
}
