<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tenants extends Model
{
    protected $fillable = [
        'id',
        'nk_pm',
        'nk_pa',
        'mv_begin',
        'mv_end',
        'wault',
        'rental_income',
        'property_id'
    ];
    protected $table = "tenants";
    public $timestamps = false;
}
