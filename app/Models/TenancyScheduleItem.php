<?php

namespace App\Models;

use App\TenancyScheduleComment;
use Illuminate\Database\Eloquent\Model;

class TenancyScheduleItem extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'type',
        'name',
        'tenancy_schedule_id',
        'asset_manager_id',
        'rent_begin',
        'rent_end',
        'use',
        'rental_space',
        'actual_net_rent',
        'vacancy_in_qm',
        'vacancy_in_eur',
        'remaining_time',
        'remaining_time_in_eur',
        'comment','indexierung','comment2',
        'options','options1','options2','selected_option','assesment_date','kaution','vacancy_on_purcahse','common_item_id'

    ];
    protected $table = "tenancy_schedule_items";
    // public $timestamps = false;

    public function singleComment()
    {
        return TenancyScheduleComment::where('item_id',$this->id)->where('comment', '!=', '')->latest()->first();
    }
    public function extComment()
    {
        return TenancyScheduleComment::where('item_id',$this->id)->where('external_comment', '!=', '')->latest()->first();
    }
    public function latestComment($type = 0)
    {
        $field = ($type == 0) ? 'tenancy_schedule_comments.comment' : 'tenancy_schedule_comments.external_comment';
        return TenancyScheduleComment::selectRaw('tenancy_schedule_comments.*, users.name, users.role, users.company')->leftjoin('users', 'users.id', '=', 'tenancy_schedule_comments.user_id')->where('tenancy_schedule_comments.item_id',$this->id)->where($field, '!=', '')->orderBy('tenancy_schedule_comments.created_at', 'desc')->limit(2)->get();
    }
    public function getUploadStatus()
    {
        $d = VacantUploadingStatus::where('vacant_id',$this->id)->get();
        $arr = array();
        foreach ($d as $key => $value) {
            $arr[$value->type] = $value;
        }
        return $arr;
    }

    public function vacant_uploading_statuses(){
        return $this->hasMany('App\Models\VacantUploadingStatus', 'vacant_id');
    }

    public function files()
    {
        return $this->hasMany(TenancyScheduleItemFile::class, 'tenancy_schedule_item_id');
    }
}
