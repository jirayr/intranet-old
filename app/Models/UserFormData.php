<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFormData extends Model
{
	protected $table = "user_form_data";
	
	protected $fillable = [
        'id',
        'email_id',
		'user_data',
		'property_id',
		'doc_key',
        'institut',
        'anschrift',
        'ansprechpartner',
        'telefon',
        'email',
        'finanzierungstyp',
        'kreditbetrag',
        'variabler_zinssatz',
        'tilgung',
        'laufzeit',
        'anmerkungen'
	];
	
	public $timestamps = true;
}
