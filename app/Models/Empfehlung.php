<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Empfehlung extends Model
{
	protected $table = "empfehlung";
	public $timestamps = true;
}
