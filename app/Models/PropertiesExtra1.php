<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertiesExtra1 extends Model
{
	protected $fillable = [
		'id',
		'propertyId',
		'tenant',
		'net_rent_p_a', 
		'mv_end', 
	];
	protected $table = "properties_extra1";
	public $timestamps = false;
}
