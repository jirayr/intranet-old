<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExportHistory extends Model
{
    protected $guarded = [];
    protected $table = "export_histories";
	public $timestamps = true;


	public function user(){
		return $this->belongsTo('App\User', 'user_id');
	}
}
