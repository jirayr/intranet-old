<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
	protected $fillable = [
		'id',
		'name',
		'description',
		'start',
		'end',
		'user_id',
	];
	protected $table = "events";
	public $timestamps = false;
}
