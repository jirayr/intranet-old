<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertySubfields extends Model
{
	protected $fillable = [
		'id',
		'property_id',
		'is_new_directory_strucure',
		'location_latitude',
		'location_longitude',
	];
	protected $table = "property_subfields";
	public $timestamps = true;
}
