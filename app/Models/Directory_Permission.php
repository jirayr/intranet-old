<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Directory_Permission extends Model
{
	protected $fillable = [
		'id',
		'basename',
		'user_role'
	];
	protected $table = "directory__permissions";
	public $timestamps = true;
}
