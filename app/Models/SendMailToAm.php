<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SendMailToAm extends Model
{
    protected $fillable = [
        'id',
        'property_id',
        'user_id',
        'am_id',
        'subject',
        'message'
    ];
    protected $table = "send_mail_to_ams";
    public $timestamps = true;
}
