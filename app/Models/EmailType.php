<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailType extends Model
{
    protected $connection = 'mysql2';
    protected $fillable = ['type'];
}
