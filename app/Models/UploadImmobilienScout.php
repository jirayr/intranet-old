<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UploadImmobilienScout extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'property_id',
        'uploaded'
    ];

    protected $table = "upload_immobilienscout";
}
