<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginLogs extends Model
{
    protected $guarded = [];
    protected $table = "login_logs";
	public $timestamps = true;
}
