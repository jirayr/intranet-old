<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertiesDirectory extends Model
{
	protected $fillable = [
		'id',
		'property_id', 
		'parent_id', 
		'dir_name', 
		'dir_basename',
		'dir_path',
	];
	protected $table = "properties_directories";
	public $timestamps = true;

}
