<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class LeasingActivity extends Model
{
	protected $table = "leasing_activity";
	public $timestamps = true;
}
