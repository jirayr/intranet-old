<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyLoansMirror extends Model
{
    //

    protected $table = "property_loans_mirror";
	protected $fillable = [
        'id',
		'borrower',
        'lender',
        'original_loan',
        'payout_date',
        'early_interest',
        'calculation_method',
        'variable_interest',
        'reference_interest',
        'check_the_refinance_1',
        'check_the_refinance_3',
        'change_limit',
        'treatment_at_negative',
        'loan',
        'running_time',
        'running_time_type',
        'standby_interest',
        'standby_interest_from',
        'management_fee',
        'loan_costs',
        'registry_fees',
        'exit_fee',
        'mortgage',
        'mortgage_in_bond',
        'mortgage_guarantor',
        'mortgage_extent_of_guarantee',
        'is_shareholder_loan',
        'shareholder_loan',
        'guarantor',
        'extent_of_guarantee'
    ];
	 public $timestamps = true;
}
