<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyRentPaids extends Model
{
    protected $guarded = [];
    protected $table = "property_rent_paids";
	public $timestamps = true;

	public function rent_paid_details(){
		return $this->hasMany('App\Models\PropertyRentPaidDetails', 'property_rent_paid_id');
	}

	public function user(){
        return $this->belongsTo('App\User', 'user_id');

    }
}
