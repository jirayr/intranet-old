<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacantSpace extends Model
{
    protected $table = "vacant_spaces";
    public $timestamps = false;
}
