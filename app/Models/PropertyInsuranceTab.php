<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyInsuranceTab extends Model
{
    protected $guarded = [];
    protected $table = "property_insurance_tabs";
	public $timestamps = true;
}
