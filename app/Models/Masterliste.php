<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Masterliste extends Model
{
    protected $fillable = [
        'id',
        'object',
        'asset_manager',
        'flat_in_qm',
        'vacancy',
        'vacancy_structurally',
        'technical_ff',
        'rented_area',
        'rent_net_pm',
        'avg_rental_fee',
        'potential_pm'
    ];
    protected $table = "masterliste";
    public $timestamps = false;

    public function property()
    {
        return $this->hasOne('App\Models\Properties', 'masterliste_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User','asset_manager');
    }
}
