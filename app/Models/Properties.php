<?php

namespace App\Models;

use App\email_template2_sending_info;
use Illuminate\Database\Eloquent\Model;
use DB;



class Properties extends Model
{


	protected $fillable = [
		'id',
		'firma','phone','email',
        'axa',
        'gebaude_betrag',
        'gebaude_laufzeit_from',
        'gebaude_laufzeit_to',
        'gebaude_kundigungsfrist',
        'gebaude_is_approved',
        'allianz',
        'haftplicht_betrag',
        'haftplicht_laufzeit_from',
        'haftplicht_laufzeit_to',
        'haftplicht_kundigungsfrist',
        'haftplicht_is_approved',
        'gebaude_comment',
        'haftplicht_comment',
		'user_id',
		'city_place',
        'address', //Street and number
        'type_of_property',
		'net_rent',
		'net_rent_empty',
		'maintenance',
		'operating_costs',
		'object_management',
		'tax',
		'plot_of_land_m2',
		'plot_of_land',
		'construction_year',
		'land_value',
		'building',
		'plot',
		'real_estate_taxes',
		'estate_agents',
		'notary_land_register',
		'evaluation',
		'others',
		'buffer',
		'rent',
		'rent_retail',
		'rent_office',
		'rent_industry',
		'rent_whg',
		'vacancy',
		'vacancy_retail',
		'vacancy_office',
		'vacancy_industry',
		'vacancy_whg',
		'total_commercial_sqm', //k38
		'wault',
		'anchor_tenant',
		'with_real_ek',
		'from_bond',
		'bank_loan',
		'interest_bank_loan',
		'eradication_bank',
		'interest_bond',
		'maintenance_nk',
		'operating_costs_nk',
		'object_management_nk',
		'depreciation_nk',
		'property_value',
		'duration_from',
		'nk_anchor_tenants',
		'nk_tenant1',
		'nk_tenant2',
		'nk_tenant3',
		'nk_tenant4',
		'nk_tenant5',
        'nk_pm_anchor_tenants',
        'nk_pm_tenant1',
        'nk_pm_tenant2',
        'nk_pm_tenant3',
        'nk_pm_tenant4',
        'nk_pm_tenant5',
        'mv_begin_anchor_tenants',
        'mv_begin_tenant1',
        'mv_begin_tenant2',
        'mv_begin_tenant3',
        'mv_begin_tenant4',
        'mv_begin_tenant5',
		'mv_anchor_tenants',
		'mv_tenant1',
		'mv_tenant2',
		'mv_tenant3',
		'mv_tenant4',
		'mv_tenant5',
		'buy_recommendation',
		'location',
		'status',
		'WHG_qm_of_WAULT',
		'WHG_qm_of_anchor_tenant',
		'WHG_qm_of_plot',
		'Key_figures',//G56
		'total_purchase_price',//D38
		'financing_structure_sum', //C50
		'building_eur',
		'plot_of_land_eur',
		'net_buy_price',
		'additional_cost',
		'total_nbpac',
		'with_real_ek_money',
		'from_bond_money',
		'bank_loan_money',
		'net_rent_pa',
		'maintenance_nk_money',
		'operating_costs_nk_money',
		'object_management_nk_money',
		'depreciation_nk_money',
		'net_rent_increase_year1',
		'net_rent_increase_year2',
		'net_rent_increase_year3',
		'net_rent_increase_year4',
		'net_rent_increase_year5',
		'name_of_property',
		'name_of_creator',
		'maintenance_increase_year1',
		'maintenance_increase_year2',
		'maintenance_increase_year3',
		'maintenance_increase_year4',
		'maintenance_increase_year5',
		'operating_cost_increase_year1',
		'operating_cost_increase_year2',
		'operating_cost_increase_year3',
		'operating_cost_increase_year4',
		'operating_cost_increase_year5',
		'property_management_increase_year1',
		'property_management_increase_year2',
		'property_management_increase_year3',
		'property_management_increase_year4',
		'property_management_increase_year5',
		'ebitda_year_1',
		'ebitda_year_2',
		'ebitda_year_3',
		'ebitda_year_4',
		'ebitda_year_5',
		'depreciation_year_1',
		'depreciation_year_2',
		'depreciation_year_3',
		'depreciation_year_4',
		'depreciation_year_5',
		'ebit_year_1',
		'ebit_year_2',
		'ebit_year_3',
		'ebit_year_4',
		'ebit_year_5',
		'Interest_bank_loan_year_1',
		'Interest_bank_loan_year_2',
		'Interest_bank_loan_year_3',
		'Interest_bank_loan_year_4',
		'Interest_bank_loan_year_5',
		'Interest_bond_year1',
		'Interest_bond_year2',
		'Interest_bond_year3',
		'Interest_bond_year4',
		'Interest_bond_year5',
		'EBT_year_1',
		'EBT_year_2',
		'EBT_year_3',
		'EBT_year_4',
		'EBT_year_5',
		'Tax_year_1',
		'Tax_year_2',
		'Tax_year_3',
		'Tax_year_4',
		'Tax_year_5',
		'EAT_year_1',
		'EAT_year_2',
		'EAT_year_3',
		'EAT_year_4',
		'EAT_year_5',
		'Eradication_bank_year1',
		'Eradication_bank_year2',
		'Eradication_bank_year3',
		'Eradication_bank_year4',
		'Eradication_bank_year5',
		'Cash_flow_after_taxes_year_1',
		'Cash_flow_after_taxes_year_2',
		'Cash_flow_after_taxes_year_3',
		'Cash_flow_after_taxes_year_4',
		'Cash_flow_after_taxes_year_5',
		'real_estate_taxes_in_EUR',
		'estate_agents_in_EUR',
		'notary_land_register_in_EUR',
		'Evaluation_in_EUR',
		'Grundbuch_in_EUR',
		'Grundbuch',
		'Others_in_EUR',
		'Buffer_in_EUR',
		'Depreciation_year_2',
		'Depreciation_year_3',
		'Depreciation_year_4',
		'Depreciation_year_5',
		'EBIT_year_1',
		'EBIT_year_2',
		'EBIT_year_3',
		'EBIT_year_4',
		'EBIT_year_5',
		'Interest_bank_loan_year_1',
		'Interest_bank_loan_year_2',
		'Interest_bank_loan_year_3',
		'Interest_bank_loan_year_4',
		'Interest_bank_loan_year_5',
		'Interest_bond_year1',
		'Interest_bond_year2',
		'Interest_bond_year3',
		'Interest_bond_year4',
		'Interest_bond_year5',
		'EBT_year_1',
		'EBT_year_2',
		'EBT_year_3',
		'EBT_year_4',
		'EBT_year_5',
		'Tax_year_1',
		'Tax_year_2',
		'Tax_year_3',
		'Tax_year_4',
		'Tax_year_5',
		'EAT_year_1',
		'EAT_year_2',
		'EAT_year_3',
		'EAT_year_4',
		'EAT_year_5',
		'Eradication_bank_year1',
		'Eradication_bank_year2',
		'Eradication_bank_year3',
		'Eradication_bank_year4',
		'Eradication_bank_year5',
		'Cash_flow_after_taxes_year_1',
		'Cash_flow_after_taxes_year_2',
		'Cash_flow_after_taxes_year_3',
		'Cash_flow_after_taxes_year_4',
		'Cash_flow_after_taxes_year_5',
		// 'real_estate_taxes_in_EUR',
		// 'estate_agents_in_EUR',
		// 'notary_land_register_in_EUR',
		// 'Evaluation_in_EUR',
		// 'Others_in_EUR',
		// 'Buffer_in_EUR',
		'value_end_of_year_1',
		'value_end_of_year_2',
		'value_end_of_year_3',
		'value_end_of_year_4',
		'value_end_of_year_5',
		'interest_bank_loan_end_of_year_1',
		'interest_bank_loan_end_of_year_2',
		'interest_bank_loan_end_of_year_3',
		'interest_bank_loan_end_of_year_4',
		'interest_bank_loan_end_of_year_5',
		'eradication_bank_end_of_year_1',
		'eradication_bank_end_of_year_2',
		'eradication_bank_end_of_year_3',
		'eradication_bank_end_of_year_4',
		'eradication_bank_end_of_year_5',
		'sum_end_year_1',
		'sum_end_year_2',
		'sum_end_year_3',
		'sum_end_year_4',
		'sum_end_year_5',
		'interest_bond_from_bond',
		'gross_yield',
		'Gross_desperate',
		'EK_Rendite_CF',
		'EK_Rendite_GuV',
		'Increase_p_a_year1',
		'Increase_p_a_year2',
		'Increase_p_a_year3',
		'Increase_p_a_year4',
		'Increase_p_a_year5',
		'end_of_year_1',
		'end_of_year_2',
		'end_of_year_3',
		'end_of_year_4',
		'end_of_year_5',
		'from_sales_proceeds',
		'from_revaluation',
		'anchor_tenants',
		'Rental_area_in_m2',
		'Rent_euro_m2',
		'KP_€_m2_p_NF',
		'plots',
		'occupancy_rate',
		'Factor_net',
		'City_/_place',
		'Lease_term_up_to',
		//'land_value',
		//'duration_from',
		'WAULT__of_anchor_tenants',
		'WAULT__of_tenant1',
		'WAULT__of_tenant2',
		'WAULT__of_tenant3',
		'WAULT__of_tenant4',
		'WAULT__of_tenant5',
		'Rental_income_of_anchor_tenants',
		'Rental_income_of_tenant1',
		'Rental_income_of_tenant2',
		'Rental_income_of_tenant3',
		'Rental_income_of_tenant4',
		'Rental_income_of_tenant5',
		'Total_of_WAULT',
		'Total_of_Rental_income',
		'Total_Land_value',
		'Value_totally',
		'non-recoverable_ancillary_costs_and_maintenance_via_the_WAULT',
		'risk_value',
		'risk_value_percent',
		'bank',
		'bank_ids',
        'is_in_negotiation',
        'strengths',
        'weaknesses',
        'opportunities',
        'threats',
		'masterliste_id',
		'duration_from_O38',
		'ground_reference_value_in_euro_m2',
		'Ref_CF_Salzgitter',
		'Ref_GuV_Salzgitter',
		'AHK_Salzgitter',
		'population',
		'population_edited',
		'kk_idx',
		'sonstige_flache',
		'position',
		'construction_year_note',
		'purchase_date','purchase_date2',
		'miscellaneous_title','buffer_title','miete_text','plot_of_land_m2_text',
		'gastronomie','gastronomie1','gastronomie2','properties_bank_id','sheet_title','portfolio','swot_json','lock_status','notizen',
        'steuerberater','property_status','investor_name','status_update_date','gebaude_falk_approved','haftplicht_falk_approved','offer_from','contact','market_value','hvbu_id','hvpm_id','is_not_an_existing'

	];
	protected $table = "properties";
	public $timestamps = true;


	public function uploadState(){
		return $this->hasOne('App\Models\UploadImmobilienScout', 'property_id');
	}
	public function schedule()
	{
		return $this->belongsTo('App\Models\TenancySchedule', 'property_id');
		# code...
	}

	public function creator()
	{
		return $this->belongsTo('App\User', 'user_id');
		# code...
	}

	public function transaction_manager()
	{
		return $this->belongsTo('App\User', 'transaction_m_id');
		# code...
	}
	public function asset_manager()
	{
		return $this->belongsTo('App\User', 'asset_m_id');
		# code...
	}

	public function asset_manager_two()
	{
		return $this->belongsTo('App\User', 'asset_m_id2');
	}

	public function seller()
	{
		return $this->belongsTo('App\User', 'seller_id');
		# code...
	}

	public function directory()
    {
        return $this->hasMany(PropertiesDirectory::class, 'property_id', 'main_property_id');
    }

	public function calculate_rent($properties)
	{
		$amount = 0;
		if($properties)
		{
			$arr = explode(',', $properties);
			$c= TenancySchedule::join('tenancy_schedule_items', 'tenancy_schedule_items.tenancy_schedule_id', 'tenancy_schedules.id')->selectRaw('SUM(tenancy_schedule_items.actual_net_rent) as actual_net_rent')->whereIn('tenancy_schedules.property_id',$arr)->get();
			if(isset($c[0]['actual_net_rent']) && $c[0]['actual_net_rent'])
				$amount = $c[0]['actual_net_rent'];
		}
		return $amount;
	}

	public function getIstProperty($property)
	{
		$response = array();
		if($property->bank_ids && explode(',' , $property->bank_ids)){
            $bank_ids = explode(',' , $property->bank_ids);
            if(isset($bank_ids[0])){
                $response  =  DB::table('properties')->where('properties.Ist', $bank_ids[0])->first();
            }
        }
        return $response;
	}





	public function format_property($id){

	    $response = array();
        $property =  DB::table('properties')->where('properties.id', $id)->first();
        if(!is_null($property)){

            $response['property_id'] = $property->id;
            if(explode(',' , $property->bank_ids)){

                $bank_ids = explode(',' , $property->bank_ids);
                if(isset($bank_ids[0])){

                    $response['property']  =  DB::table('properties')->where('properties.Ist', $bank_ids[0])->first();
                }

            }
        }

        return $response;

	}

    public function insurances($property,$type)
    {
       return DB::table('property_insurances')->where('property_id', $property->id)->where('type',$type)->count();
    }

    public function status_relation()
    {
        return $this->belongsTo(PropertyStatus::class, 'status');
    }

    public function banking_offers()
    {
        return $this->hasMany(BankFinancingOffer::class, 'property_id', 'main_property_id');
    }

    public function propertyBankEmailLogs()
    {
        return $this->hasMany(email_template2_sending_info::class, 'property_id', 'main_property_id');
    }


    public static function getSumOfSolRentPaid($id, $month = '', $year = '')
    {
    	$d = '1=1';

        if($year){
            $d .= ' and upload_year='.$year;
        }
        if($month){
            $d .= ' and upload_month='.$month;
        }

        // return PropertyRentPaids::where('property_id',$id)->where('Diff','>',0)->get()->sum('soll');
        return PropertyRentPaids::where('property_id',$id)->whereRaw($d)->get()->sum('soll');
    }

    public static function getSumOfIstRentPaid($id, $month = '', $year = '')
    {	
    	$d = '1=1';

        if($year){
            $d .= ' and upload_year='.$year;
        }
        if($month){
            $d .= ' and upload_month='.$month;
        }
        // return PropertyRentPaids::where('property_id',$id)->where('Diff','>',0)->get()->sum('ist');
        return PropertyRentPaids::where('property_id',$id)->whereRaw($d)->get()->sum('ist');
    }

    public static  function getSumOfDiffRentPaid($id, $month = '', $year = '')
    {
    	$d = '1=1';

        if($year){
            $d .= ' and upload_year='.$year;
        }
        if($month){
            $d .= ' and upload_month='.$month;
        }
        // return PropertyRentPaids::where('property_id',$id)->where('Diff','>',0)->get()->sum('Diff');
        return PropertyRentPaids::where('property_id',$id)->whereRaw($d)->get()->sum('diff');
    }

}
