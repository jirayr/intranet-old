<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenantPayment extends Model
{
    protected $primaryKey = 'id';
    protected $table = "tenant_payments";
}
