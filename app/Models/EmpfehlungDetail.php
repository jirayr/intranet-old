<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class EmpfehlungDetail extends Model
{
	protected $table = "empfehlung_details";
	public $timestamps = true;
}
