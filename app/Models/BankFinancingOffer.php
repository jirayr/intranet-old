<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankFinancingOffer extends Model
{
    protected $fillable = ['bank_id', 'telefonnotiz', 'interest_rate', 'fk_share_percentage', 'fk_share_nominal','tilgung','property_id'];

    public function bank()
    {
        return $this->belongsTo(Banks::class, 'bank_id');
    }

    public function property()
    {
        return $this->belongsTo(Properties::class, 'property_id');
    }
}
