<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenancySchedule extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'creator_id',
        'object',
        'property_id'
    ];
    protected $table = "tenancy_schedules";
    public $timestamps = true;



    public function scheduleItem()
	{
		return $this->belongsTo('App\Models\TenancyScheduleItem', 'tenancy_schedule_id');
		# code...
	}

}
