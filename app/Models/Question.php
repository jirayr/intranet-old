<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	protected $fillable = [
		'id',
		'question',
		'createdBy',
		'createdOn',
	];
	protected $table = "questions";
	protected $primaryKey = 'id';
	public $timestamps = false;

}
