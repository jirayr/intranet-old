<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Budget extends Model
{
	protected $table = "budgets";
	public $timestamps = false;
}
