<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GdriveUploadFiles extends Model
{
	protected $fillable = [
		'id',
		'property_id', 
		'parent_id', 
		'parent_type', 
		'file_name', 
		'file_basename',
		'file_path',
		'file_href',
		'extension',
		'file_type'
	];
	protected $table = "gdrive_upload_files";
	public $timestamps = true;

}
