<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyAccountBalance extends Model
{
    protected $guarded = [];
    protected $table = "property_account_balance";
	public $timestamps = true;

}
