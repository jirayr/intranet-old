<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusLoi extends Model
{
	protected $table = "status_loi";
	public $timestamps = false;

    public function property()
    {
        return $this->belongsTo(Properties::class, 'property_id', 'main_property_id');
    }
}
