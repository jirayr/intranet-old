<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyInsuranceTabDetails extends Model
{
    protected $guarded = [];
    protected $table = "property_insurance_tab_details";
	public $timestamps = true;

	public function asset_manager(){
		return $this->belongsTo('App\User', 'user_id');
	}

	public function tab_title(){
		return $this->belongsTo('App\Models\PropertyInsuranceTab', 'property_insurance_tab_id');
	}
}
