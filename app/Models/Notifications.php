<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'created_user_id',
        'property_id',
        'type'
    ];

    protected $table = "notifications";
}
