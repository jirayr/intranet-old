<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyInvoices extends Model
{
    protected $guarded = [];
    protected $table = "property_invoices";
	public $timestamps = true;


	public function invoice_user()
	{
		return $this->belongsTo('App\User', 'user_id');
		# code...
	}
}
