<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyHistory extends Model
{
    protected $guarded = [];
    protected $table = "property_histories";
	public $timestamps = true;


	public function user(){
		return $this->belongsTo('App\User', 'user_id');
	}

	public function property(){
		return $this->belongsTo('App\Models\Properties', 'property_id');
	}
}
