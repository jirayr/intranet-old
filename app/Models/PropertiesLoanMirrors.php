<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertiesLoanMirrors extends Model
{
    protected $guarded = [];
    protected $table = "properties_loan_mirrors";
	public $timestamps = true;
}
