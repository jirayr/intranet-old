<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Forecast extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'name',
        'missing_info',
        'todo',
        'credit_fcr',
        'credit_spv',
        'possible_transfer',
        'buffer',
        'available_1',
        'available_2',
        'available_3',
        'available_4',
        'available_5',
        'available_6',
        'available_7',
        'available_8',
        'available_9',
        'available_10',
        'available_11',
        'available_12'
    ];

    protected $table = "forecasts";

    public $timestamps = false;
}
