<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyRentPaidDetails extends Model
{
    protected $guarded = [];
    protected $table = "property_rent_paid_details";
	public $timestamps = true;
}
