<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressesJobEmailLog extends Model
{
    protected $fillable = ['user_id','addresses_job_id','email'];


    public function addressesJob()
    {
            return $this->belongsTo(AddressesJob::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
