<?php

namespace App;

use App\Models\Properties;
use Illuminate\Database\Eloquent\Model;

class PropertyPermission extends Model
{
    protected $fillable = ['property_id','user_id'];

    public function users($id)
    {
        return User::where('id',$id)->first();
    }

    public function property()
    {
        return $this->belongsTo(Properties::class, 'property_id');
    }
}
