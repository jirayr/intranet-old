<?php
return [
    'title' => 'Kalendar',

    'create_event' => 'Event erstellen',
    'add_event' => 'Event hinzufügen',
    'event_name' => 'Event Name',
    'event_description' => 'Event Beschreibung',
    'start_date'    => 'Start Tag',
    'end_date'  => 'End Tag',

    'edit_event' => 'Event bearbeiten',
    'change_event_name' => 'Event Name ändern',
    'close' => 'Schliessen',
    'save' => 'Speichern',
    'delete' => 'Löschen',
    'error_create' => 'Das Enddatum sollte nach oder gleich dem Startdatum liegen',
    'error_edit' => 'Das Enddatum sollte nach oder gleich dem Startdatum liegen   ',
];
