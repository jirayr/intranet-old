<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwörter müssen mindestens sechs Zeichen lang sein.',
    'reset' => 'Dein Passwort wurde zurückgesetzt!',
    'sent' => 'Du hast eine E-Mail mit einem Passwort-Reset-Link erhalten!',
    'token' => 'Dieser Passwort-Reset-Token ist ungültig.',
    'user' => "Kein Nutzer mit dieser E-mail gefunden.",

];
