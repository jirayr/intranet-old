<?php

return [

    'title' => 'FCR Intranet',
	'transactionmanagement'=>'Transaction',
    'dashboard_title' => 'FCR Intranet',
    'assetmanagmenet' =>'Assetmanagement',
    'my_profile'    => 'Mein Profil',
    'inbox' => 'Inbox',
    'change_password'   => 'Passwort ändern',
    'logout'    => 'Logout',
    'users'     => 'Nutzer',
    'user_activities' => 'Nutzer Aktivitäten',
    'properties' => 'Immobilien',
    'properties_comparison' => 'Immobilienvergleich',
    'google_map'    => 'Karte',
    'calendar'  => 'Kalender',
	'feedback'	=> 'Feedback',
	'action_planning' => 'Aktionsplanung',

    'header' => [
        'you_have'   => 'Du hast',
        'new_messages' => 'neue Nachrichten',
        'see_all_notifications' => 'Alle Benachrichtigungen anzeigen',

    ],

    'side_menu' => [
		'immo_ai' => 'Immo KI',
        'upload_new_listing' => 'Neues Inserat hochladen',
        'select_banks' => 'Finanzierung auswählen',
        'choose_banks_for_property' => 'Bitte Finanzierung für das Inserat wählen',
		'you_need_to_add_bank' => 'Es muss eine Finanzierung ausgwählt werden',
		'click_here_to_add_a_bank'=>'Hier clicken um eine Bank hinzuzufügen',
		'close' => 'Schliessen',
		'create'=>'Erstellen',
        'skip'=>'überspringen'
    ]

];
