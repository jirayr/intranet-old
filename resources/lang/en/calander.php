<?php

return [
    'Jan' => 'Jan',
    'Feb' => 'Feb',
    'Mar' => 'Mär',
    'Apr' => 'Apr',
    'May' => 'Mai',
    'Jun' => 'Jun',
    'Jul' => 'Jul',
    'Aug' => 'Aug',
    'Sep' => 'Sept',
    'Oct' => 'Okt',
    'Nov' => 'Nov',
    'Dec' => 'Dez',
];
