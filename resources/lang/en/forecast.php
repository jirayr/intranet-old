<?php
return [
    'add' => 'Hinzufügen',
    'forecast_al_ls' => 'Forecast',
    'delete' => 'Löschen',
    'add_forecast' => "Neuer Forecast",
    'export' => "Export",

    'field' => [
        'kind' => 'Art',
        'description' => 'Beschreibung',
        'status' => 'Status',
        'notary' => 'Notar',
        'bnl' => 'BNL',
        'total_purchase_price' => 'Kaufpreis Gesamt',
        'missing_information' => 'Fehlende Information',
        'name' => 'Name',
        'todo' => 'Todo',
        'fk' => 'FK',
        'sale' => 'Verkauf',
        'reflux' => 'Liqui reflux',
        'note' => 'Note',
        'type' => 'Typ',
        'credit_fcr' => 'Guthaben   FCR',
        'credit_spv' => 'Guthaben SPV',
        'possible_transfer' => 'Mögliche Überweisung',
        'buffer' => 'Puffer',
        'available' => 'Verfügbar'
    ]
];