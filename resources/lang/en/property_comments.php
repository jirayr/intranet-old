<?php
return [
    'select_object' => 'Objekt wählen:',
	'comments' => '',
	'confirm' => 'Bestätigen',
	'decline' => 'Ablehnen',
	'edit' => 'Bearbeiten',
	'clear' => 'Löschen',
	'release' => 'FREIGEBEN',
	'edit_comment' => 'Kommentar bearbeiten',
	'close' => 'Schließen',
	'save_changes' => 'Änderungen speichern',
	'comment' => 'Maßnahmen',
	'leave_a_comment' => 'Kommentieren',
];
