<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Zu viele Login versuche bitte versuche wieder in  :sekunden sekunden.',

    'login' => [
        'sign_in_to_admin'  => 'Als Admin registrieren',
        'enter_your_detail_below' => 'Hier Daten eingeben',
        'email_address' => 'Email Addresse',
        'password' => 'Passwort',
        'remember_me' => 'eingeloggt bleiben',
        'login' => 'Log In',
        'recover_password'  => 'Passwort wiederherstellen',
        'enter_email'   => 'E-mail eingeben',
        'reset' => 'Reset',
		'email' => 'Email'
    ],
	'register' => [
		'name' => 'Name',
		'email_address' => 'Email Addresse',
		'password' => 'Passwort',
		'confirm_password' => 'Passwort bestätigen',
		'register' => 'Registrieren',
		
	],
	'email_address' => 'Email Addresse',
	'reset_password' => 'Passwort zurücksetzen',
	'send_password_reset_link' => 'Passwort-Reset-Link senden',
	
];
