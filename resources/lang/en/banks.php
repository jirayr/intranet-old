<?php

return [


    'index' => [
        'manage_banks'   => 'BANKEN VERWALTEN',
        'picture' => 'Bild',
        'bank_name'   => 'Bankname',
        'user_creator' => 'Ersteller',
        'actions'   => 'AKTIONEN',
    ],

    'add_edit' => [
        'banks' => 'BANKEN ',
        'picture' => 'BILD',
        'description_picture' => 'BILD WÄHLEN',
        'name' => 'Name',
        'With_real_ek' => 'Mit echtem EK',
        'from_bond' => 'Von Anleihe',
        'bank_loan' => 'Bankkredit',
        'interest_bank_loan' => 'Zinsen Bankkredit',
        'eradication_bank' => 'Tilgung Bank',
        'interest_bond' => 'Zinsen Anleihe',
        'cancel' => 'Schliessen',
        'submit' => 'Speichern',
    ]

];