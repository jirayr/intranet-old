<?php
return [
    'add' => 'Hinzufügen',
    'title' => 'Mieterliste',
    'delete' => 'Löschen',
    'add_tenancy_schedule' => "Mieterliste hinzufügen",

    'messages' => [
        'create_fail' => 'Failed to created tenancy schedule',
        'update_fail' => 'Failed to updated tenancy schedule',
        'create_item_fail' => 'Failed to created tenancy schedule item',
        'update_item_fail' => 'Failed to updated tenancy schedule item',
        'not_found' => 'Not found tenancy schedule',
        'item_not_found' => 'Not found tenancy schedule item'
    ],

    'field' => [

    ],
];