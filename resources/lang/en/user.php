<?php
return [
    'activity' => [
        'name' => 'NAME',
        'email' => 'EMAIL',
        'number_of_properties' => 'ANZAHL Immobilien',
        'role' => 'ROLLE',
        'day' => 'Tag',
        'week' => 'Woche',
        'month' => 'Monat',
        'actions' => 'AKTIONEN'
    ],

    'user_activities' => 'NUTZER AKTIVITÄTEN',
    'manage_users' => 'NUTZER VERWALTEN',
    'edit_user' => 'NUTZER BEARBEITEN',
    'edit_profile' => 'PROFIL BEARBEITEN',
    'create_user' => 'NUTZER ERSTELLEN',
    'change_password' => 'PASSWORT ÄNDERN',
    'cancel' => 'Schliessen',
    'submit' => 'Einreichen',
    'enter' => 'Eingeben',
    'example' => 'Beispiel',

    'field' => [
        'name' => 'Name',
        'email' => 'Email',
        'password' => 'Passwort',
        'first_name' => 'Vorname',
        'last_name' => 'Nachname',
        'birthday' => 'Geburtstag',
        'profile_picture' => 'Profilbild',
        'phone' => 'Telefon',
        'company' => 'Company',
        'address' => 'Addresse',
        'gender' => 'Geschlecht',
        'website' => 'Website',
        'postal_code' => 'PLZ',
    ],

    'password' => [
        'current_password' => 'Aktuelles Passwort',
        'new_password' => 'Neues Passwort',
        'confirm_new_password' => 'Neues Passwort bestätigen',
        'current_password_hint' => 'Aktuelles Passwort eingeben..',
        'new_password_hint' => 'Neues Passwort eingeben..',
        'confirm_new_password_hint' => 'Neues Passwort erneut eingeben..',

    ],

    'message' => [
        'change_avatar' => 'Bitte Foto auswählen'
    ],

    'gender' => [
        'unknown' => 'Unbekannt',
        'male' => 'Männlich',
        'female' => 'Weiblich'
    ],
    'role' => [
        'admin' => 'Admin',
        'user' => 'Transaction Manager',
        'manager' => 'Asset Manager',
        'finance' => 'Finance',
    ],
    'confirm' => 'Bist du sicher?',
    'user_controller' =>[
        'email_already' => 'This email has already been used!',
        'created_success' => 'User created successfully!',
        'created_fail' => 'Failed to create user!',
        'updated_success' => 'User updated successfully!',
        'updated_fail' => 'Failed to update user!',
        'deleted_success' => 'User deleted successfully!',
        'deleted_fail' => 'Failed to delete user!',
        'password_changed_success' => 'Password changed successfully!',
        'not_matched' => 'Two new passwords are not matched!',
        'not_true' => 'Current password is not true!',
    ],
];
