<?php
return [
    'add' => 'Hinzufügen',
    'title' => 'Masterliste',
    'listing' => 'Meine Objekte',
    'delete' => 'Löschen',
    'add_tenancy_schedule' => "Objekt hinzufügen",
	'property_info'=>'Property Info',
    'messages' => [
        'create_fail' => 'Failed to created tenancy schedule',
        'update_fail' => 'Failed to updated tenancy schedule',
        'create_item_fail' => 'Failed to created tenancy schedule item',
        'update_item_fail' => 'Failed to updated tenancy schedule item',
        'not_found' => 'Not found tenancy schedule',
        'item_not_found' => 'Not found tenancy schedule item'
    ],

    'field' => [

    ],
];