<?php
return [
    'title' => 'Kalendar',

    'create_calendar ' => 'Kalendar erstellen',
    'add_calendar' => 'Kalendar hinzufügen',
	 'calendar_description' => 'Kalendar Beschreibung',
    'summary_text' => 'Summary Text',
    'start_date'    => 'Start Tag',
    'end_date'  => 'End Tag',

    'edit_calendar' => 'Kalendar bearbeiten',
    'create_calendar' => 'Kalendar bearbeiten',
    'close' => 'Schliessen',
    'save' => 'Speichern',
    'delete' => 'Löschen',
    'error_create' => 'Das Enddatum sollte nach oder gleich dem Startdatum liegen',
    'error_edit' => 'Das Enddatum sollte nach oder gleich dem Startdatum liegen   ',
];
