<div class="table-responsive">
	<div class="property-table">
    <table class="table table-striped property-management-table">
        <thead>
        <tr>
            <th>Name</th>
            <th class="text-right">IST Netto Miete p.m.</th>
            <th class="text-right">Prozent(%)</th>
            <th class="text-right"> Fee</th>
            <th>Kommentar</th>
            <th>AM-Empfehlung</th>
            <th>Falk</th>
            <th>Ablehnen</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $user = Auth::user();
        ?>
        @foreach($data as $key=>$row)
        <tr>
            <td><input data-column="name" class="change-property-manage" type="text" data-id="{{$row->id}}" value="{{$row->name}}"></td>
            <td class="text-right">{{ show_number($total_actual_net_rent, 2) }}</td>
            <td class="text-right"><input style="width: 100px;" data-column="percent" class="change-property-manage mask-input-number text-right" type="text" data-id="{{$row->id}}" value="{{show_number($row->percent,2)}}"></td>
            <td class="text-right">@if($row->percent) {{show_number($total_actual_net_rent*$row->percent/100,2)}}@endif</td>
            <td><input style="width: 300px;" data-column="comment" class="change-property-manage" type="text" data-id="{{$row->id}}" value="{{$row->comment}}"></td>
            <td class="text-center"><input data-column="release_status1" type="checkbox" class="change-property-manage-status" data-id="{{$row->id}}" @if($row->release_status1) checked @endif ></td>
            <td class="text-center">
                <?php 
                $falk_checkbox = $not_release = "";
                if($row->property_id != 1743 && $user->email==config('users.falk_email'))
                {
                    $rbutton_title = "Freigeben";
                    $release = 'btn btn-xs btn-primary pmanagement-request-button';
                    $release_type = "release_management";
                    $falk_checkbox = '<button data-id="'.$row->id.'" type="button" class="btn '.$release.'" data-column="'.$release_type.'">'.$rbutton_title.'</button>';

                    if($row->not_release==0 && $row->is_release==0)
                    {
                        $not_release = '<button data-id="'.$row->id.'" type="button" class="btn btn-xs btn-primary btn-propert-management-not-release">Ablehnen</button>';   
                    }
                }
                if($row->property_id != 1743 && $row->is_release==1)
                {
                   $falk_checkbox = '<button type="button" class="btn btn-xs btn-success">Freigegeben</button>';;
 
                }

                ?>
                {!! $falk_checkbox !!}
                <!-- <input data-column="release_status2" type="checkbox" class="change-property-manage-status" data-id="{{$row->id}}" @if($row->release_status2) checked @endif > -->
            </td>
            <td>
                {!! $not_release !!}
            </td>
            <td>@if($row->release_status2==0)<button type="button" class="btn-danger btn-xs" onclick='delete_property_management("{{$row->id}}")'><i class="fa fa-times"></i></button>@endif</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
