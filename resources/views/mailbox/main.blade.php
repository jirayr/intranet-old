@extends('layouts.admin')
@section('content')
    <style>
        .container-fluid{
            background-color: white !important;
        }
    </style>
    <head>

         {{--<link href="{{ asset('css/app.css') }}" type="text/css" rel="stylesheet" />--}}
        <meta name="csrf-token" value="{{ csrf_token() }}" />
    </head>

    <div id="app">
    </div>
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
@endsection