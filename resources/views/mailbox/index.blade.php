@extends('layouts.admin')
@section('content')
    <form  method="get" action="{{url('/mail-box/search')}}">
        <div class="row">

            <div class="form-group col-md-4" >
                <label for="email">Email:</label>
                <input required type="email" class="form-control" name="email" maxlength="105"  placeholder="Enter Email" id="email">
            </div>
            <div class="form-group col-md-4">
                <label for="email">Password:</label>
                <input required type="text" class="form-control" name="password"   placeholder="Enter Password">
            </div>
            <div class="form-group col-md-4">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <label for="email">&nbsp;</label><div></div>
                <button type="submit" class=" btn btn-primary">Search</button>
            </div>

        </div>

    </form>

    <div class="card-header">
        <div class="row">
            <div class="col-md-10">
                <h4 class="card-title">INBOX MAILS</h4>
            </div>
        </div>
    </div>

    <div class="col-sm-12 table-responsive white-box">
        <table style="table-layout:fixed" class="table table-striped" id="inquiries">
            <thead>
            <tr>
                <th>Subject</th>
                <th>To</th>
                <th>From</th>
                <th>Body</th>
            </tr>
            </thead>
            <tbody>
            @isset($messages)
                    @foreach($messages as $message)
                        <tr>
                        <td>{{$message->subject}}</td>
                        <td>{{$message->to[0]->mail}}</td>
                        <td>{{$message->from[0]->mail}}</td>
                        <td>{!! $message->bodies['text']->content !!}</td>
                        </tr>
                @endforeach
            @endisset
            </tbody>
        </table>
        @isset($messages)
        {{$messages->links()}}
        @endisset

    </div>

    <div class="card-header">
        <div class="row">
            <div class="col-md-10">
                <h4 class="card-title">SENT EMAILS</h4>
            </div>
        </div>
    </div>

    <div class="col-sm-12 table-responsive white-box">
        <table style="table-layout:fixed" class="table table-striped" id="inquiries">
            <thead>
            <tr>
                <th>Subject</th>
                <th>To</th>
                <th>From</th>
                <th>Body</th>
            </tr>
            </thead>
            <tbody>
            @isset($sentEmails)
                @foreach($sentEmails as $sentEmail)
                    <tr>
                        <td>{{$sentEmail->subject}}</td>
                        <td>{{$sentEmail->to[0]->mail}}</td>
                        <td>{{$sentEmail->from[0]->host}}</td>
                        <td>{!! $sentEmail->bodies['text']->content !!}</td>
                    </tr>
                @endforeach
            @endisset
            </tbody>
        </table>
        @isset($sentEmails)
            {{$sentEmails->links()}}
        @endisset
    </div>

@endsection