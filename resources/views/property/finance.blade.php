@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<style type="text/css">
		.mask-number-input{
			text-align: right;
		}
	</style>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="finance" class="tab-pane fade in active">
  		
  		<ul class="nav nav-tabs">
	        <li class="active"><a data-toggle="tab" href="#darlehensspiegel">Darlehensspiegel</a></li>
	        <li><a data-toggle="tab" href="#liquiplanung">Liquiplanung</a></li>
	    </ul>

	    <div class="tab-content">
	      	<div id="darlehensspiegel" class="tab-pane fade in active">
	          	@include('properties.templates.darlehensspiegel')
	      	</div>
	      	<div id="liquiplanung" class="tab-pane fade">
	          	@include('properties.templates.liquiplanung')
	      	</div>
	    </div>

  	</div>
</div>

@endsection

@section('js')
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script type="text/javascript">
  		$(document).ready(function(){
		    $(".is_shareholder_radio").change(function(){
		        if($('#is_shareholder_loan_yes').is(':checked')){
		            $('#shareholder_loan').show();
		        }else{
		            $('#shareholder_loan').hide();
		        }
		    });

		    $('#running_time_type').change(function(){
		        if($(this).val()  == "Fest"){
		            $('#running_time').mask('00/00/0000');
		            $('#running_time').attr('placeholder', 'DD/MM/YYYY');
		        }else{
		            $('#running_time').unmask();
		            $('#running_time').attr('placeholder', '');
		        }

		    });
		});
  	</script>
@endsection