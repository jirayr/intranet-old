@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="bank-modal" class="tab-pane fade in active">

  		<?php
         	$bank_array = array();
         	$j = $ist = 0;
         	foreach($banks as $key => $bank){
            	$bank_array[] = $bank->id;
         	}

         	array_unique($bank_array);
         	$str = implode(',', $bank_array);
         
         	$properties_banks = DB::table('properties')->whereRaw('main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('standard_property_status','desc')->get();
         	if($properties_banks){
         		foreach ($properties_banks as $property_sheet) {
         			if($property_sheet->lock_status == 0){
         				if($property_sheet->Ist){
         					if($ist==0){
                				$GLOBALS['ist_sheet'] = $property_sheet;
            				}
            				$ist = 1;
         				}
         			}else{
         				$GLOBALS['release_sheet'] = $property_sheet;
         			}
         		}
         	}
        ?>

        <?php
			$bank_array = array();
		    foreach($banks as $key => $bank){
		        $bank_array[] = $bank->id;
		    }

		    array_unique($bank_array);
		    $str = implode(',', $bank_array);

			$properties_banks = DB::table('properties')->select('sheet_title','properties.id','standard_property_status','Ist','soll','banks.name','properties_bank_id')->join('banks','banks.id','=','properties.properties_bank_id')->whereRaw('lock_status=0 and main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('properties_bank_id')->get();
		?>

		<div class="">
			<div class="modal-dialog modal-lg hidden" style="width: 100%">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Finanzierungsanfragen</h4>
					</div>
					<div class="modal-body">
						<div class="row ">
							<div class="col-sm-12 table-responsive">
								{{--<table class="table table-striped  data-table" id="user-form-data">--}}
								{{--<thead>--}}
								{{--<tr>--}}
								{{--<th>Email Send To</th>--}}
								{{--<th>Email From</th>--}}
								{{--<th>Subject</th>--}}
								{{--<th>Contact Person</th>--}}
								{{--<th>Role</th>--}}
								{{--<th>Message</th>--}}
								{{--</tr>--}}
								{{--</thead>--}}
								{{--<tbody>--}}
								{{--@if($sendEmails)--}}
								{{--@foreach($sendEmails as $list)--}}
								{{--<tr>--}}
								{{--<td>{{$list->email_to_send}}</td>--}}
								{{--<td>{{$list->email_from}}</td>--}}
								{{--<td>{{$list->subject}}</td>--}}
								{{--<td>{{$list->contact_person}}</td>--}}
								{{--<td>{{$list->role}}</td>--}}
								{{--<td>{!! $list->message !!}</td>--}}
								{{--</tr>--}}
								{{--@endforeach--}}
								{{--@endif--}}
								{{--</tbody>--}}
								{{--</table>--}}

								<table class="table table-striped  hidden" id="user-form-data">
									<thead>
										<tr>
											<th>Bank</th>
											<th>Ort</th>
											<th>Ansprechpartner</th>
											<th>Datum</th>
											<th>Uhrzeit</th>
											<!--<th>Telefon</th>
											<th>Kontakt</th>
											<th>Email</th>
											<th>Message</th>-->
											<th>Notizen</th>
											<th>Aktion</th>
										</tr>
									</thead>
									<tbody>
										@if($sendEmails)
											@foreach($sendEmails as $list)
												<tr>
													<td>{{$list->getBank($list->email_to_send)->name}}</td>
													<td>{{isset($list->getBank($list->email_to_send)->address) ? $list->getBank($list->email_to_send)->address : ''}}</td>
													<td>
														{{ isset($list->getBank($list->email_to_send)->firstName) ?  $list->getBank($list->email_to_send)->firstName.' '.$list->getBank($list->email_to_send)->surname: ''}}
													</td>
													<td>{{\Carbon\Carbon::parse($list->created_at)->format('d.m.Y')}}</td>
													<td>{{\Carbon\Carbon::parse($list->created_at)->format('H:i')}}</td>
												<!--<td>{{$list->getBank($list->email_to_send)->contact_phone}}</td>
													<td>{{$list->getBank($list->email_to_send)->contact_name}}</td>
													<td>{{$list->email_to_send}}</td>
													<td>{!! $list->message !!}</td>-->
													<td id="note-text">{{ $list->notizen }}</td>
													<td><a href="" data-id="{{$list->id}}" data-note="{{$list->notizen}}" id="csvEmail" data-toggle="modal"  class="email btn-sm btn-info">Notizen</a></td>
												</tr>
											@endforeach
										@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="">
			<div class="modal-dialog modal-lg" style="width: 100%">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<div class="col-md-9">
							<h4 class="modal-title">Finanzierungsanfragen und -angebote	</h4>
						</div>
						<div class="col-md-3">
							<a class="btn btn-primary FinanzierungsangeboteCreate">Erstellen</a>
							<a href="{{route('export_bank_data',$id)}}" class="btn btn-success">Export</a>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-12 table-responsive">
								<table class="table table-striped" id="listing-banks">
									<thead>
										<tr>
											<th>Auto. angefragt</th>
											<th>Bank</th>
											<th>Ort</th>
											<th>Ansprechpartner</th>
											<!--<th>Telefon</th>
			                                <th>Email</th>-->
											<th>Notizen</th>
											<th>Zinsatz</th>
											<th>Tilgung</th>
											<th>FK-Anteil proz.</th>
											<th>FK-Anteil nominal</th>
											<th>Datum</th>
											<th>Uhrzeit</th>
											<th>Aktion</th>
											<!-- <th></th> -->
										</tr>
									</thead>
									<tbody>

										@if($sendEmails)
											@foreach($sendEmails as $list)
												<tr>
													<td>{!! get_paid_checkbox(1,1) !!}</td>
													<td>{{$list->getBank($list->email_to_send)->name}}</td>
													<td>
														{{isset($list->getBank($list->email_to_send)->address) ? $list->getBank($list->email_to_send)->address : ''}}
												    	{{isset($list->getBank($list->email_to_send)->city) ? $list->getBank($list->email_to_send)->city : ''}}
													</td>
													<td>
														{{ isset($list->getBank($list->email_to_send)->firstName) ?  $list->getBank($list->email_to_send)->firstName.' '.$list->getBank($list->email_to_send)->surname: ''}}
													</td>
													<td>
														<a href="#" class="inline-edit" data-type="text" data-pk="notizen" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" >
														@if($list->notizen)
															{{ $list->notizen }}
														@else
															k.A.
														@endif
														</a>
													</td>
													<td>
														<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
															@if($list->interest_rate)
																{{ show_number($list->interest_rate,2) }}
															@else
																0
															@endif
														</a>
													</td>
													<td>
														<a href="#" class="inline-edit" data-type="text" data-pk="tilgung" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
															@if($list->tilgung)
																{{ show_number($list->tilgung,2) }}
															@else
																0
															@endif
														</a>
													</td>
													<td>
														<a href="#" class="inline-edit" data-type="text" data-pk="fk_share_percentage" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
															@if($list->fk_share_percentage)
																{{ show_number($list->fk_share_percentage,2) }}
															@else
																0
															@endif
														</a>
													</td>
													<td>
														<a href="#" class="inline-edit" data-type="text" data-pk="fk_share_nominal" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
															@if($list->fk_share_nominal)
																{{ show_number($list->fk_share_nominal,2) }}
															@else
																0
															@endif
														</a>
													</td>
													<td>{{show_datetime_format($list->created_at,'d.m.Y')}}</td>
													<td>{{show_datetime_format($list->created_at,'H:i')}}</td>
													<!--<td>{{$list->getBank($list->email_to_send)->contact_phone}}</td>
													<td>{{$list->getBank($list->email_to_send)->contact_name}}</td>
													<td>{{$list->email_to_send}}</td>
													<td>{!! $list->message !!}</td>-->
													<td>
														<!-- <a href="" data-id="{{$list->id}}" data-note="{{$list->notizen}}" id="csvEmail" data-toggle="modal"  class="email btn-sm btn-info">Notizen</a> -->
														<a style="margin-left: 10px" href="javascript:void(0)" onclick="deleteBankEmailLog({{$list->id}})" class="btn-sm btn-danger">Löschen</a>
													</td>
												</tr>
											@endforeach
										@endif

										@foreach($banksFinancingOffers as $banksFinancingOffer)
											<tr>
												<td></td>
												<td>
													<a href="javascript:void(0)" data-id="{{$banksFinancingOffer->id}}" class=" FinanzierungsangeboteEdit">{{$banksFinancingOffer->bank->name}}</a>
												</td>
												<td>{{$banksFinancingOffer->bank->address}}</td>
												<td>{{$banksFinancingOffer->bank->contact_name}} <br>
													{{$banksFinancingOffer->bank->contact_phone}}<br>{{$banksFinancingOffer->bank->contact_email}}
												</td>
												<!-- <td>{{$banksFinancingOffer->telefonnotiz}}</td> -->
												<td>
													<a href="#" class="inline-edit" data-type="textarea" data-pk="telefonnotiz" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-title="">{{$banksFinancingOffer->telefonnotiz}}</a>
												</td>
												<td>
													<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->interest_rate,2) }}</a>
												</td>
												<td>
													<a href="#" class="inline-edit" data-type="text" data-pk="tilgung" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->tilgung,2) }}</a>
												</td>
												<td>
													<a href="#" class="inline-edit" data-type="text" data-pk="fk_share_percentage" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->fk_share_percentage,2) }}</a>
												</td>
												<td>
													<a href="#" class="inline-edit" data-type="text" data-pk="fk_share_nominal" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->fk_share_nominal,2) }}</a>
												</td>
												<td>{{show_datetime_format($banksFinancingOffer->created_at,'d.m.Y')}}</td>
												<td>{{show_datetime_format($banksFinancingOffer->created_at,'H:i')}}</td>
												<td>
													<!-- <a type="button" data-id="{{$banksFinancingOffer->id}}" class="btn-sm btn-info FinanzierungsangeboteEdit">Bearbeiten</a> -->
													<a type="button" data-id="{{$banksFinancingOffer->id}}"  class="btn-sm btn-danger delete-bank-finance-offer">Löschen</a>
												</td>
											</tr>
										@endforeach

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="">
			<div class="modal-dialog modal-lg" style="width: 100%">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">BANKEN</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="panel">
								<form id="sendemailtobankers" action="{{ route('sendemailtobankers') }}" method="post" enctype='multipart/form-data'>
									<div class="row">
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
										<input type="hidden" name="property_id" value="{{ $id }}">
										<input type="hidden" name="bank_id" value="" id="bank_id">
										<input type="hidden" name="bank_email" value="" id="bank-email">
										<input type="hidden" name="property_data" id="property-data-second" value="">
										<input type="hidden" name="tenant_data" id="tenant-data-second" value="">
										<input type="hidden" name="note_data" id="note-data-second" value="">
										<input type="hidden" name="isTestEmail" id="isTestEmail" value="0">
										<input type="hidden" name="sheet_prop_id" id="sheet_prop_id" value="">

										<?php

											$main_property_id = $properties->id;
											if(isset($GLOBALS['ist_sheet']) && $GLOBALS['ist_sheet']){
												$the_main_tenant = $properties->the_main_tenant;
												$rental_period_until = $properties->rental_period_until;

												$properties = $GLOBALS['ist_sheet'];
												$properties->the_main_tenant = $the_main_tenant;
												$properties->rental_period_until = $rental_period_until;

												$main_property_id = $properties->main_property_id;
											}

											$manager = "";
											$manager_id = $properties->transaction_m_id;
											$subject = "Finanzierungsanfrage FCR Immobilien AG";
											$email_content = "";
											$sheet_selected = "";
											if(isset($email_template) && ($email_template->transaction))
												$manager = $email_template->transaction;

											if($banking_info){
												$manager = $banking_info->role;
												$manager_id = $banking_info->contact_person;
												$subject= $banking_info->title;
												$email_content= $banking_info->email_content;
												$sheet_selected= $banking_info->iframe_name;
											}
										?>

										<div class="col-sm-8 col-md-8 col-md-offset-2">
											<!-------Section for email------------------>
											<div class="form-group col-md-12 no-padding">
												<div class="col-sm-6">
													Titel Transactionmanagement
													<select class="form-control" name="role" id="mySelect" >
														<option @if($manager == 'Transaction Manager') selected @endif value="Transaction Manager">Transaction Manager</option>
														<option @if($manager == 'Transaction Management') selected @endif value="Transaction Management">Transaction Management</option>
														<option @if($manager == 'FCR Immobilien AG') selected @endif  value="FCR Immobilien AG">FCR Immobilien AG</option>
														<option @if($manager == 'Director Transaction Management') selected  @endif value="Director Transaction Management">Director Transaction Management</option>
														<option @if($manager == 'Head of Transaction Management') selected  @endif value="Head of Transaction Management">Head of Transaction Management</option>
													</select>
												</div>

												<div class="col-sm-6 cpl-md-6">
													<label>{{__('user_form.Ansprechpartner')}}</label>
													<select name="contact_person" class="form-control required" id="contact_person" placeholder="{{__('user_form.Ansprechpartner')}}">
														<option value="">{{__('dashboard.transaction_manager')}}</option>
														@foreach($tr_users as $list1)
															@if($manager_id==$list1->id)
																<option selected="selected" value="{{$list1->name}}">{{$list1->name}}</option>
															@elseif($manager_id==$list1->name)
																<option selected="selected" value="{{$list1->name}}">{{$list1->name}}</option>
															@else
																<option value="{{$list1->name}}">{{$list1->name}}</option>
															@endif
														@endforeach
													</select>
												</div>
												<div class="col-sm-12 col-md-12">
													<label>&nbsp;</label>
													<input type="text" placeholder="Betreff" class="form-control required" name="subject" value="{{$subject}}" required="required" id="subject">
												</div>
											</div>

											{{-- <div class="col-sm-12">
												<textarea rows="10" class="form-control required" name="message" placeholder="Nachricht" id="message"></textarea>
											</div> --}}

											<div class="col-sm-12">
												<textarea  class="form-control required" name="message" placeholder="Nachricht" id="mail_message" style="min-height: 200px;">
													@if($email_content)
														{!! $email_content !!}
													@else
														Sehr geehrte Damen und Herren,
														<br>
														<br>wir würden gerne eine Finanzierung für das Objekt in {{$properties->plz_ort}} {{$properties->ort}}, {{$properties->strasse}} {{$properties->hausnummer}} bei Ihnen anfragen.
														<br><br>

														Das Gebäude wurde im Jahr {{$properties->construction_year}} erbaut und befindet sich daher in einem guten und gepflegten Zustand. Den Kunden stehen ca. {{ number_format($properties->plot,0,',','.') }} Parkplätze zur Verfügung. Das Nahversorgungszentrum ist am Standort etabliert und erfährt eine hohe Nachfrage bei der umliegenden Wohnbevölkerung.<br>
														<br>
														Für den Objekterwerb stellen wir uns eine Non-Recourse Finanzierung vor. Wir werden für den Erwerb der Immobilien eine Objektgesellschaft (GmbH) gründen. Dabei würden wir 20% Eigenkapital mitbringen und einen Fremdkapitalanteil i. H. v. 80% bei Ihnen erbeten. Wir könnten uns einen variablen Zinssatz von 1% und eine Tilgung von 3,5% vorstellen, sind aber offen für Gegenvorschläge Ihrerseits.
														<br><br>Damit Sie zunächst einen ersten Überblick über unsere Gesellschaft und unser Vorhaben erhalten, übersende ich Ihnen in der Anlage nachfolgende Unterlagen der FCR Immobilien AG sowie die zugehörigen Objektunterlagen:<br>
														<ul>
															<li>Jahresabschlüsse 2019</li>
															<li>Geschäftsbericht 2019</li>
															<li>Unternehmenspräsentation 2019</li>
															<li>Objektexposé, Planungsspiegel</li>
														</ul>
														<br>
														Falls Sie weitere Informationen oder Unterlagen benötigen, stehe ich Ihnen gerne zur Verfügung.<br>
														<br>
														Es würde mich freuen, wenn Ihnen das Objekt zusagen würde und wir dazu nächste Woche Rücksprache halten könnten.<br><br>
													@endif
												</textarea>
											</div>

											<div class="col-sm-12">
												<br><br>
												<label>Anbei automatisch mitgesendet</label>
												<ul>
													<li>Jahresabschlüsse 2019</li>
													<li>Geschäftsbericht 2019</li>
													<li>Unternehmenspräsentation 2019</li>
													<li>Objektexposé, Planungsspiegel</li>
												</ul>
												<br>
											</div>

											<div class="col-sm-12">
												<br><br><label>Anhang hochladen</label><br>
												<input type="file" name="email_attachment" class="email-file-attachments">

												<ul class="uploadeds">
													@foreach($attchments as $value)
														<li>
															<a href="{{asset('email_templates2_attachments/'.$main_property_id.'/'.$value->file)}}">{{$value->name}}</a><a href='javascript:void(0)'><i data-id="{{$value->id}}" class='fa fa-times delete-attachment'></i></a>
														</li>
													@endforeach
												</ul>

												<label>Automatische Dateien anhängen</label>
												<input type="checkbox" checked id="isFileAttached" name="is_file_attached" value="1" />
											</div>

											<!-------Section for Objektdaten data------------------>
											<div class="col-sm-12">
												<br><br>
												<label>Kalkulation wählen</label>
											</div>

											<div class="form-group col-md-12 no-padding">
												<div class="col-sm-6 col-md-6">
													<select name="bank_sheet" class="form-control required" id="bank-sheet" placeholder="Kalkulation wählen">
														<option value="">Kalkulation wählen (.PDF Anhang)</option>
														@foreach($properties_banks as $properties_bank)
															@php
																$tempType = ($properties_bank->Ist)? "Ist" : "soll";
																$tempValue ="bank-".$tempType."-iframe".$properties_bank->id;
															@endphp
															<option @if($sheet_selected==$tempValue) selected @endif data-id="{{$properties_bank->id}}" value="{{$tempValue}}"> {{$properties_bank->sheet_title}} {{ $tempType }}
															</option>
														@endforeach
													</select>
												</div>
											</div>

											<div class="col-sm-12">
												<br><br>
											</div>
											<div class="form-group col-md-12 no-padding hidden">
												<div class="col-sm-6 col-md-6">
													<label>{{__('user_form.Objekt')}}</label>
													<input type="text" placeholder="{{__('user_form.Objekt')}}" class="form-control required" name="object_info"  value="{{($bankers_email_info->name_of_property) ? $bankers_email_info->name_of_property : ''}}" title="{{__('user_form.Objekt')}}" id="object">
												</div>
												<div class="col-sm-6 col-md-6">
													<label>{{__('user_form.Baujahr')}}</label>
													<input type="text" placeholder="{{__('user_form.Baujahr')}}" class="form-control required" name="construction_year"  value="{{$bankers_email_info->construction_year}}" title="{{__('user_form.Baujahr')}}" id="construction_year">
												</div>
											</div>

											<div class="form-group col-md-12 no-padding hidden">
												@php
													$address=$properties->plz_ort.' '.$properties->ort.' '.$properties->strasse.' '.$properties->hausnummer;
												@endphp
												<div class="col-sm-6 col-md-6">
													<label>{{__('user_form.Adresse')}}</label>
													<input type="text" placeholder="{{__('user_form.Adresse')}}" class="form-control required" name="address" value="{{($address) ? $address : ''}}" id="address">
												</div>
											</div>

											<div class="form-group col-md-12 no-padding hidden">
												<div class="col-sm-6 col-md-6">
													<label>{{__('user_form.Typ')}}</label>
													<input type="text" placeholder="{{__('user_form.Typ')}}" class="form-control" name="type" value="{{($properties->type_of_property) ? $properties->type_of_property : ''}}" id="type">
												</div>
												<div class="col-sm-6 col-md-6">
													<label>{{__('user_form.Hauptmieter')}}</label>
													<input type="text" placeholder="{{__('user_form.Hauptmieter')}}" class="form-control " name="the_main_tenant" value="{{($properties->the_main_tenant) ? $properties->the_main_tenant : ''}}" id="the_main_tenant">
												</div>
											</div>

											<div class="form-group col-md-12 no-padding hidden">
												<div class="col-sm-6">
													<label>{{__('user_form.Mietlaufzeit bis')}}</label>
													<input type="text" placeholder="{{__('user_form.Mietlaufzeit bis')}}" class="form-control" name="rental_period_until" value="{{($properties->rental_period_until) ? $properties->rental_period_until : ''}}" id="rental_period_until">
												</div>
												<div class="col-sm-6">
													<label>{{__('user_form.Parkplätze')}}</label>
													<input type="text" placeholder="{{__('user_form.Parkplätze')}}" class="form-control " name="parking" value="{{number_format($properties->plots,0,",",".")}}" id="parking">
												</div>
											</div>
											<!-------Section ends for financing data------------------>

											<!-------Section for financing data------------------>
											<div class="col-sm-12 hidden">
												<br><br>
												<label>{{__('user_form.Finanzierung')}}</label>
											</div>
											<div class="form-group col-md-12 no-padding hidden">
												<div class="col-sm-6 col-md-6">
													<input type="text" placeholder="{{__('user_form.Finanzierungsart')}}" class="form-control " name="financing"  value="" title="{{__('user_form.Finanzierungsart')}}" id="financing">
												</div>

												<div class="col-sm-6 col-md-6">
													<input type="text" placeholder="{{__('user_form.Eigenkapitalanteil')}}" class="form-control " name="equity_share" value="" id="equity_share">
												</div>
											</div>

											<div class="form-group col-md-12 no-padding hidden">
												<div class="col-sm-6 col-md-6">
													<input type="text" placeholder="{{__('user_form.Fremdkapitalanteil')}}" class="form-control " name="leverage"  value="" title="{{__('Fremdkapitalanteil')}}" id="leverage">
												</div>

												<div class="col-sm-6 col-md-6">
													<input type="text" placeholder="{{__('user_form.Variabler Zinssatz')}}" class="form-control " name="variable_interest_rate" value="" id="variable_interest_rate">
												</div>
											</div>

											<div class="form-group col-md-12 no-padding hidden">
												<div class="col-sm-6">
													<input type="text" placeholder="{{__('user_form.Tilgung')}}" class="form-control " name="repayment" value="" id="repayment">
												</div>
											</div>
											<div class="row">
												<div class="col-sm-6 col-md-6">
													<label>&nbsp;</label>
													<input type="email" placeholder="Test Mail eingeben" class="form-control" name="testEmail" value=""  id="testEmail">
												</div>
												<div style="margin-top: 4%" class="col-sm-6 col-md-6 ">
													<button type="button" class="btn btn-primary send_test_email" id="testEmailButton">Testmail senden</button>
												</div>
											</div>
											<br>

											<div class="form-group col-md-12 no-padding">
												<div class="col-sm-6">
													<button type="button" class="btn btn-primary save-template-info">Speichern</button>
												</div>
											</div>
											<!-------Section ends for financing data------------------>
										</div>
									</div>

									<div style="float: right">
										<label for="">Umkreissuche:</label>
										<input required min="1" type="text" name="radius" id="radius" placeholder="Radius eingeben">
										<input type="hidden" name="p_id" id="p_id" value="{{ $properties->id }}">
										<a id="radiusSearch" class="btn btn-success ">Suche</a>
									</div>

									<div class="table-responsive" style="margin-top: 4%">
										<a  data-property="{{ $properties->id }}" href="{{route('show_property',['id'=>$properties->main_property_id ])}}?tab=bank-modal&showall=1"  class="btn btn-primary">{{__('user_form.showall')}}</a>
										<button  data-property="{{ $properties->id }}"  class="btn btn-primary all_green_btn" type="button" style="float:right">Alle Anfragen</button>
										<a href="javascript:void(0)" class="btn btn-success pull-right new-banken" style="margin-right: 5px;">Neue Bank hinzufügen</a>
										<br/><br/>
										<table class="table table-hover manage-u-table color-bordered-table purple-bordered-table data-table">
											<thead>
												<tr>
													<th style="width: 70px;" class="text-center">#</th>
													<th>{{__('banks.index.bank_name')}}</th>
													<th>Ansprechpartner</th>
													<th>{{__('user_form.Result')}}</th>
													<th>E-mail</th>
													<th>{{__('banks.index.actions')}}</th>
												</tr>
											</thead>
											<tbody class="table-body">
												@if(isset($bank_popup_data) && count($bank_popup_data) > 0)
													@foreach($bank_popup_data as $data)
														<tr class="table-row">
															<td>
															<!--{{ $loop->index + 1  }}-->
																<div class="col-md-4 col-md-offset-4 no-padding">
																	<input type="checkbox" name="bank_ids[]" value="{{$data->id}}" class="form-control bank_ids"/>
																</div>
															</td>
															<td><a href="javascrip:void(0)">{!!  $data->name  !!}</a></td>
															<td>{!! str_replace("||","<br>",$data->fullName) !!} {{-- {!! $data->surname  !!} --}}</td>
															<td>
																<button class="btn btn-success show-result-list" type="button" data-email="{{$data->contact_email}}">
																	<i class='fa fa-eye'></i>
																</button>
															</td>
															<td>
																@if(count(explode('||', $data->contact_email))>1)
																	@foreach(explode('||', $data->contact_email) as $email)
																		<input type="radio" name="selected_email" id="bank-email{{ $data->id }}" onclick="setBankMailField('{{str_replace(' ', '', $email)}}',{{$data->id}})"> {{$email}}
																		<br>
																	@endforeach
																@else
																	{!! str_replace("","<br>",$data->contact_email) !!}
																@endif
															</td>
															<td>
																@php
																	$record = DB::table('email_template2')->where('bank_id', $data->id)->where('property_id', $properties->id )->first();
			                                                        if(!is_null($record) && $record->email_status == 1)
			                                                        {
																@endphp
																<button type="button" class="btn btn-success btn-outline btn-circle btn-lg m-r-5">
																	<i class="mdi mdi-check"></i>
																</button>
																@php
																	}
																	else{
																@endphp

																<a class="btn btn-primary btn-sm send_email_to_banks"  selected-mail-{{ $data->id }}="" id="bank-id{{ $data->id }}" data-bank="{{ $data->id }}" data-property="{{ $properties->id }}" >E-Mail senden</a>
																@php
																	}
																@endphp
															</td>
														</tr>
													@endforeach
												@endif
											</tbody>
										</table>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" name="" value="" id="selected_bank_mail">
	
		<!-- Modal -->
		<div class="modal fade" id="result-list" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">{{__('user_form.Result')}}</h4>
					</div>
					<div class="modal-body">
						<table class="color-bordered-table purple-bordered-table table table-hover" id="result-list-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>Email</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="result-modal" role="dialog">
			<div class="modal-dialog modal-lg">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">{{__('user_form.Result')}}</h4>
					</div>
					<div class="modal-body">
						<table class="color-bordered-table purple-bordered-table table table-hover" id="result-table">
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>

		<div class=" modal fade" role="dialog" id="emailConfigModel">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Notizen </h4>
					</div>
					<form  class="config-form" id="config-form"   action="{{ url('/sendEmail') }}" method="POST" enctype="multipart/form-data">
						<div class="modal-body">
							<input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="banken_id"  class="form-control" id="banken_id" value="" >
							<label>Note :</label>
							<textarea name="note" id="noteText" class="form-control" required ></textarea>
							<span id="subject-field"   style="visibility:hidden;color: red;">This field is Required!</span>
							<br>
						</div>
						<div class="modal-footer">
							<span id="config-loader" style="visibility:hidden"><i class="fa fa-spin fa-spinner"></i></span>
							<button id="save-config" onsubmit="sendEmail()" class="btn btn-primary" >Save</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class=" modal fade" role="dialog" id="FinanzierungsangeboteCreate">
			<div class="modal-dialog modal-lg">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="title">Finanzierungsangebote</h4>
					</div>
					<div class="modal-body">
						<form id="finanzierungsanfragen-form" class="finanzierungsanfragen-form" method="post" action="">

							<div class="row">
								<div class="form-group col-md-6">
									<label for="email">Bank:</label>
									<select  required class="form-control bank" name="bank_id"  style="width: 100%"></select>
								</div>
								<div class="form-group col-md-6 hide-custom" >
									<label for="email">Telefonnotiz:</label>
									<input required type="text" class="form-control" name="telefonnotiz"  placeholder="Enter Telefonnotiz" id="telefonnotiz">
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-6 hide-custom">
									<label for="email">Zinsatz:</label>
									<input type='text' id='interest_rate' name="interest_rate" class="form-control" >
								</div>
								<div class="form-group col-md-6 hide-custom">
									<label for="email">Tilgung:</label>
									<input class="form-control" name="tilgung" id="tilgung" type="text">
								</div>
							</div>

							<div class="row hide-custom">
								<div class="form-group col-md-6">
									<label for="email">FK-Anteil proz:</label>
									<input class="form-control" name="fk_share_percentage" id="fk_share_percentage" type="text">
								</div>
								<div class="form-group col-md-6">
									<label for="email">FK-Anteil nominal:</label>
									<input class="form-control" name="fk_share_nominal" id="fk_share_nominal" type="text">
								</div>
							</div>

							<input type="hidden"  id="bankFinanceId" value="">
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<input type="hidden" name="property_id" id="propId" value="<?php echo Request::segment(2); ?>">
							<button type="submit"  id="btn-save" class="btn btn-primary">Speichern</button>

						</form>
					</div>
				</div>
			</div>
		</div>

		<div class=" modal fade" role="dialog" id="nue-banken">
		   <div class="modal-dialog">
		      <!-- Modal content-->
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		            <h4 class="modal-title">Neue Bank erstellen</h4>
		         </div>
		         <div class="modal-body">
		            <form id="add-new-banken">
		               <input type="hidden" name="_token" value="{{csrf_token()}}">
		               <label>Firma</label>
		               <input type="text" name="Firma" class="form-control">
		               <br>
		               <label>Anrede</label>
		               <input type="text" name="Anrede" class="form-control">
		               <br>
		               <label>Vorname</label>
		               <input type="text" name="Vorname" class="form-control">
		               <br>
		               <label>Nachname</label>
		               <input type="text" name="Nachname" class="form-control">
		               <br>
		               <label>Strasse</label>
		               <input type="text" name="Strasse" class="form-control">
		               <br>
		               <label>Ort</label>
		               <input type="text" name="Ort" class="form-control">
		               <br>
		               <label>Telefon</label>
		               <input type="text" name="Telefon" class="form-control">
		               <br>
		               <label>Mobil</label>
		               <input type="text" name="Fax" class="form-control">
		               <br>
		               <label>E-Mail</label>
		               <input type="text" name="E_Mail" class="form-control">
		               <br>
		            </form>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-primary save-new-bank" >Speichern</button>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		         </div>
		      </div>
		   </div>
		</div>

  	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var _token 							= '{{ csrf_token() }}';
		var url_radius_search				= '{{ url('banks/radius-search') }}';
		var url_savenote 					= '{{ url('/savenote') }}';
		var url_delete_bank_email_log 		= '{{ url('/delete_bank_email_log') }}';
		var url_bank_financing_offer 		= '{{ url('/bank-financing-offer') }}';
		var url_bank_financing_offer_update = '{{ url('/bank-financing-offer/update') }}';
		var url_bank_financing_offer_delete = '{{ url('/bank-financing-offer/delete') }}';
		var url_get_banks 					= '{{ url('/get-banks') }}';
		var url_sendemailtobankers 			= '{{ route('sendemailtobankers') }}';
		var url_add_new_banken  			= '{{ route('add_new_banken') }}';
		var url_showresult 					= '{{ route('showresult') }}';
	</script>
	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  	<script src="{{ asset('js/custom-datatable.js') }}"></script>
  	<script src="{{asset('js/property/bank-modal.js')}}"></script>
@endsection














