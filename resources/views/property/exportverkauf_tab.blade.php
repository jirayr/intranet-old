@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<style type="text/css">
		.btn-outline {
		    color: inherit;
		    background-color: transparent;
		    transition: all .5s;
		}

		.btn-primary.btn-outline {
		    color: #428bca;
		}

		.btn-success.btn-outline {
		    color: #5cb85c;
		}

		.btn-info.btn-outline {
		    color: #5bc0de;
		}

		.btn-warning.btn-outline {
		    color: #f0ad4e;
		}

		.btn-danger.btn-outline {
		    color: #d9534f;
		}

		.btn-primary.btn-outline:hover,
		.btn-success.btn-outline:hover,
		.btn-info.btn-outline:hover,
		.btn-warning.btn-outline:hover,
		.btn-danger.btn-outline:hover {
		    color: #fff;
		}

		.chat {
		    margin: 0;
		    padding: 0;
		    list-style: none;
		}

		.chat li {
		    margin-bottom: 10px;
		    padding-bottom: 5px;
		    border-bottom: 1px dotted #999;
		}

		.chat li.left .chat-body {
		    margin-left: 60px;
		}

		.chat li.right .chat-body {
		    margin-right: 60px;
		}

		.chat li .chat-body p {
		    margin: 0;
		}

		.panel .slidedown .glyphicon,
		.chat .glyphicon {
		    margin-right: 5px;
		}

		.chat-panel .panel-body {
		    height: 350px;
		    overflow-y: scroll;
		}

		.login-panel {
		    margin-top: 25%;
		}

		.flot-chart {
		    display: block;
		    height: 400px;
		}

		.flot-chart-content {
		    width: 100%;
		    height: 100%;
		}

		.btn-circle {
		    width: 30px;
		    height: 30px;
		    padding: 6px 0;
		    border-radius: 15px;
		    text-align: center;
		    font-size: 12px;
		    line-height: 1.428571429;
		}

		.btn-circle.btn-lg {
		    width: 50px;
		    height: 50px;
		    padding: 10px 16px;
		    border-radius: 25px;
		    font-size: 18px;
		    line-height: 1.33;
		}

		.btn-circle.btn-xl {
		    width: 70px;
		    height: 70px;
		    padding: 10px 16px;
		    border-radius: 35px;
		    font-size: 24px;
		    line-height: 1.33;
		}

		.show-grid [class^=col-] {
		    padding-top: 10px;
		    padding-bottom: 10px;
		    border: 1px solid #ddd;
		    background-color: #eee!important;
		}

		.show-grid {
		    margin: 15px 0;
		}

		.huge {
		    font-size: 20px;
		}

		.panel-green {
		    border-color: #5cb85c;
		}

		.panel-green .panel-heading {
		    border-color: #5cb85c;
		    color: #fff;
		    background-color: #5cb85c;
		}

		.panel-green a {
		    color: #5cb85c;
		}

		.panel-green a:hover {
		    color: #3d8b3d;
		}

		.panel-red {
		    border-color: #d9534f;
		}

		.panel-red .panel-heading {
		    border-color: #d9534f;
		    color: #fff;
		    background-color: #d9534f;
		}

		.panel-red a {
		    color: #d9534f;
		}

		.panel-red a:hover {
		    color: #b52b27;
		}

		.panel-yellow {
		    border-color: #f0ad4e;
		}

		.panel-yellow .panel-heading {
		    border-color: #f0ad4e;
		    color: #fff;
		    background-color: #f0ad4e;
		}

		.panel-yellow a {
		    color: #f0ad4e;
		}

		.panel-yellow a:hover {
		    color: #df8a13;
		}
		/**Customized **/
		.select2-container .select2-choice {
		    background-image: none;
		    height: 30px;
		    line-height: 30px;
		}
		#toast-container > div{
		    opacity: 1 !important;
		}
		.file-upload-wrap label{
		    border: 1px solid #dddddd;
		    float: left;
		    text-align: center;
		    height: 150px;
		    width: 150px;
		    padding: 30px 0;
		    cursor: pointer;
		    margin: 5px;
		}
		.file-upload-wrap i{
		    font-size: 50px;
		    cursor: pointer;
		}
		.creating-ads-img-wrap{
		    border: 1px solid #dddddd;
		    float: left;
		    height: 150px;
		    width: 150px;
		    padding: 5px;
		    margin: 5px;
		    position: relative;
		}
		.img-action-wrap {
		    bottom: 0;
		    position: absolute;
		}
		.img-action-wrap a{
		    font-size: 16px;
		    margin: 3px;
		}

		.img-action-wrap a.imgDeleteBtn{
		    color: #ff0000;
		}
		.img-action-wrap a.imgFeatureBtn{
		    color: #d58512;
		}
		label{
		    font-weight: 400;
		}

		.agent-feature-btn{
		    color: #d58512;
		}

		/** Google map */
		.controls {
		    margin-top: 10px;
		    border: 1px solid transparent;
		    border-radius: 2px 0 0 2px;
		    box-sizing: border-box;
		    -moz-box-sizing: border-box;
		    height: 32px;
		    outline: none;
		    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
		}

		#pac-input {
		    background-color: #fff;
		    font-family: Roboto;
		    font-size: 15px;
		    font-weight: 300;
		    margin-left: 12px;
		    padding: 0 11px 0 13px;
		    text-overflow: ellipsis;
		    width: 300px;
		}

		#pac-input:focus {
		    border-color: #4d90fe;
		}

		.pac-container {
		    font-family: Roboto;
		}

		#type-selector {
		    color: #fff;
		    background-color: #4d90fe;
		    padding: 5px 11px 0px 11px;
		}

		#type-selector label {
		    font-family: Roboto;
		    font-size: 13px;
		    font-weight: 300;
		}
		#target {
		    width: 345px;
		}
		#dvMap {
		    height: 100%;
		}
		.mask-number-input{
			text-align: right;
		}
	</style>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="exportverkauf_tab" class="tab-pane fade in active">

  		<?php
	     	$bank_array = array();
	     	$j = $ist = 0;
	     	foreach($banks as $key => $bank){
	        	$bank_array[] = $bank->id;
	     	}

	     	array_unique($bank_array);
	     	$str = implode(',', $bank_array);
	     
	     	$properties_banks = DB::table('properties')->whereRaw('main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('standard_property_status','desc')->get();
	     	if($properties_banks){
	     		foreach ($properties_banks as $property_sheet) {
	     			if($property_sheet->lock_status == 0){
	     				if($property_sheet->Ist){
	     					if($ist==0){
	            				$GLOBALS['ist_sheet'] = $property_sheet;
	        				}
	        				$ist = 1;
	     				}
	     			}else{
	     				$GLOBALS['release_sheet'] = $property_sheet;
	     			}
	     		}
	     	}

	     	$vermietet_total = $leerstand_total  = 0;
	    ?>
  	
  		<div id="tenancy-schedule">
    		<h1>Exposé</h1>
            <div class="row">
                <div class="col-md-10 col-xs-12">

                    <form action="{{ url('ad_pdf') }}" method="post"  class="form-horizontal" accept-charset="UTF-8" enctype="multipart/form-data" id="pdfadsPostForm">
                        {{ csrf_field() }}

                    	<legend>Anzeigeninfo</legend>
                      		
                  		<input type="hidden" name="property_id" value="{{ $id }}">
                  		<input type="hidden" name="vermietung" value="0">
                        
                        <div class="form-group {{ $errors->has('ad_title')? 'has-error':'' }}">
                            <label for="ad_title" class="col-sm-4 control-label">Objektname</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="ad_title" value="@if(isset($exportads)){{ $exportads->title }}@else{{ old('ad_title') }} @endif" name="ad_title" placeholder="Objektname">
                                {!! $errors->has('ad_title')? '<p class="help-block">'.$errors->first('ad_title').'</p>':'' !!}
                                <p class="text-info">70-100 Charaktere sind super für einen Titel</p>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('ad_description')? 'has-error':'' }}">
                            <label for="ad_description" class="col-sm-4 control-label">Anzeigenbeschreibung</label>
                            <div class="col-sm-8">
                                <textarea name="ad_description" class="form-control" rows="8">@if(isset($exportads)){{ $exportads->description }}@else{{ old('ad_description') }} @endif</textarea>
                                {!! $errors->has('ad_description')? '<p class="help-block">'.$errors->first('ad_description').'</p>':'' !!}
                                <p class="text-info">Eine Beschreibung bietet Ihren Benutzern Details zum Produkt</p>
                            </div>
                        </div>

                        <?php
                        	$type = $properties->property_type;
                        	if(old('type'))
                            	$type = old('type');
                        	if(isset($exportads))
	                            $type = $exportads->type;
                        ?>

                        <div class="form-group required {{ $errors->has('type')? 'has-error':'' }}">
                            <label class="col-md-4 control-label">Objekttyp </label>
                            <div class="col-md-8">
                                <?php
                                    $a = array('shopping_mall'=>'Einkaufszentrum', 'retail_center'=>'Fachmarktzentrum','specialists'=>'Fachmarkt', 'retail_shop'=>'Einzelhandel','office'=>'Büro','logistik'=>'Logistik','commercial_space'=>'Kommerzielle Fläche','land'=>'Grundstück','apartment'=>'Wohnung','condos'=>'Eigentumswohnung','house'=>'Haus','living_and_business'=>'Wohn- und Geschäftshaus','villa'=>'Villa','hotel'=>'Hotel','nursing_home'=>'Pflegeheim','multifamily_house'=>'Mehrfamilienhaus');
                                    
                                ?>
                                @foreach($a as $k=>$list)
	                                <label for="type_{{$k}}" class="radio-inline">
	                                    <input type="radio" value="{{$k}}" id="type_{{$k}}" name="type" {{ $type == $k? 'checked="checked"' : '' }}>
	                                   {{$list}} 
	                                </label>
                                @endforeach
                                {!! $errors->has('type')? '<p class="help-block">'.$errors->first('type').'</p>':'' !!}
                            </div>
                        </div>

                    	<?php
                    		$purpose = old('purpose');
                    		if(isset($exportads))
                        		$purpose = $exportads->purpose;
                    	?>

                        <div class="form-group hidden {{ $errors->has('purpose')? 'has-error':'' }}">
                            <label for="purpose" class="col-sm-4 control-label">Zweck</label>
                            <div class="col-sm-8">
                                <select class="form-control  " name="purpose" id="purpose">
                                    <option value="sale"  {{ $purpose == 'sale' ? 'selected':'' }}>Zu verkaufen</option>
                                    <option value="rent" {{ $purpose == 'rent' ? 'selected':'' }}  >Vermietung</option>
                                    <option value="sold" {{ $purpose == 'sold' ? 'selected':'' }}  >Verkauft</option>
                                    <option value="exclusive" {{ $purpose == 'exclusive' ? 'selected':'' }}  >Exklusivität</option>
                                </select>
                                {!! $errors->has('purpose')? '<p class="help-block">'.$errors->first('purpose').'</p>':'' !!}
                            </div>
                        </div>


                        <?php
                        	if(isset($exportads) && $exportads->price)
                            	$m_price =$pppp = $exportads->price;
                        	else
                            	$m_price =$pppp = $properties->verkaufspreis;
                        ?>

                        <div class="form-group  {{ $errors->has('price')? 'has-error':'' }}">
                            <label for="price" class="col-md-4 control-label">Preis</label>
                            <div class="col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon">EUR{{-- get_option('currency_sign') --}}</span>
                                    <input type="text" step="any" placeholder="Ehem. 15000" class="form-control mask-number-input" name="price" id="price" value="{{$pppp}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div>
                                    <label>
                                         <input type="checkbox" value="1" name="negotiable" id="negotiable" @if(isset($exportads) && $exportads->is_negotiable == 1) checked @endif >
                                       Verhandelbar
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-offset-4">
                                {!! $errors->has('price')? '<p class="help-block">'.$errors->first('price').'</p>':'' !!}
                                <p class="text-info">Einen guten Preis wählen.</p>
                            </div>
                        </div>

                        <div class="form-group hidden">
                            <label class="col-sm-4 control-label">Preis pro Einheit:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">EUR{{-- get_option('currency_sign') --}}</span>
                                    <input type="text" step="any" class="form-control mask-number-input" placeholder="Preis pro Einheit" value="@if(isset($exportads)){{$exportads->price_per_unit}}@else{{ old('price_per_unit') }}@endif" name="price_per_unit" id="price_per_unit">
                                </div>
                                <span class="help-inline">&nbsp;</span>
                            </div>
                            <?php
                                $unit_type = old('price_unit');
                                if(isset($exportads))
                                    $unit_type = $exportads->unit_type;
                            ?>
                            <div class="col-sm-4">
                                <select class="form-control select2NoSearch" name="price_unit">
                                    <option value="sqft" {{ $unit_type == 'sqft' ? 'selected':'' }}>Quadratfuß</option>
                                    <option value="sqmeter" {{ $unit_type == 'sqmeter' ? 'selected':'' }}>Quadratmeter</option>
                                    <option value="acre" {{ $unit_type == 'acre' ? 'selected':'' }}>Ar</option>
                                    <option value="hector" {{ $unit_type == 'hector' ? 'selected':'' }}>Hektar</option>
                                </select>
                                <span class="help-inline">&nbsp;</span>
                            </div>
                        </div>

                    	<legend>Objektdetails</legend>

                    	<?php

                        	$ist_sheet = "";
                        	if(isset($GLOBALS['ist_sheet']) && $GLOBALS['ist_sheet'])
                        		$ist_sheet = $GLOBALS['ist_sheet'];

                        	$pppp2 = $properties->plot_of_land_m2;

                        	if($ist_sheet)
                            	$pppp2 = $ist_sheet->plot_of_land_m2;

                        	$pppp2 = number_format($pppp2, 2,",",".");

                        	$plot = "";
                        	if($ist_sheet)
                            	$plot = number_format($ist_sheet->plot, 2,",",".");
                        ?>


                    	<div class="form-group {{ $errors->has('square_unit_space')? 'has-error':'' }}">
                        	<label for="square_unit_space" class="col-sm-4 control-label">Grundstücksfläche in m²</label>
                        	<div class="col-sm-8">
                            	<input type="text" readonly="readonly" step="any" class="form-control" id="square_unit_space" value="{{$pppp2}}" name="square_unit_space" placeholder="Grundstücksfläche in m²">
                            	{!! $errors->has('square_unit_space')? '<p class="help-block">'.$errors->first('square_unit_space').'</p>':'' !!}
                            	<!-- <p class="help-block">@lang('app.square_unit_space_help_text') </p> -->
                        	</div>
                    	</div>

                    	<?php
                    		$a = $b = $c = $d = array();
                    		$selection_array_total = array();

                    		$v2 = $v3 = $v4 = "";

                    		$w =0;
                    		$an_rent1 = $an_rent = 0;

                    		$sum_actual_net_rent = $sum_total_amount = 0;


	                        foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule){


	                            $v2 = number_format($tenancy_schedule->calculations['total_rental_space'], 2,",",".");
	                            $v3 = number_format($tenancy_schedule->calculations['mi10'], 2,",",".");
	                            $v4 = number_format($tenancy_schedule->calculations['mi9'], 2,",",".");

	                            foreach($tenancy_schedule->items as $item){

	                                if($item->type == config('tenancy_schedule.item_type.business') && $item->rent_end > date('Y-m-d') && $item->rent_end && substr($item->rent_end,0,4)!="2099")
                                    {
                                            $sum_actual_net_rent += $item->actual_net_rent;
                                            $sum_total_amount += $item->remaining_time_in_eur;
                                    }
	                                if($item->status && $item->use && $item->rental_space)
	                                {
	                                    if(isset($selection_array_total[$item->use]))
	                                        $selection_array_total[$item->use] += $item->rental_space; 
	                                    else
	                                        $selection_array_total[$item->use] = $item->rental_space; 

	                                }
	                                if($item->status && $item->use && $item->vacancy_in_qm)
	                                {
	                                    if(isset($selection_array_total[$item->use]))
	                                        $selection_array_total[$item->use] += $item->vacancy_in_qm; 
	                                    else
	                                        $selection_array_total[$item->use] = $item->vacancy_in_qm; 
	                                }

	                                if($item->type == config('tenancy_schedule.item_type.business'))
	                                {
	                                    $a[$item->actual_net_rent] = $item->name;
	                                    $b[$item->actual_net_rent] = $item->rental_space;
	                                    $c[$item->actual_net_rent] = $item->rent_begin;
	                                    $d[$item->actual_net_rent] = $item->rent_end;


	                                }
	                            }

	                            if($sum_actual_net_rent && $sum_total_amount)
	                                $w = number_format($sum_total_amount/(12*$sum_actual_net_rent),1);
	                            else
	                                $w = ($tenancy_schedule->calculations['total_business_actual_net_rent']!=0)?number_format(($tenancy_schedule->calculations['total_remaining_time_in_eur'])/(12 * $tenancy_schedule->calculations['total_business_actual_net_rent']),1) : 0 ;


	                            $an_rent1 = $tenancy_schedule->calculations['total_actual_net_rent'] * 12;
	                            $an_rent = number_format($tenancy_schedule->calculations['total_actual_net_rent'] * 12,0,",",".");
	                        }

	                        krsort($a);
	                        krsort($b);
	                        krsort($c);
	                        krsort($d);

	                        $a  = array_values($a);
	                        $b  = array_values($b);
	                        $c  = array_values($c);
	                        $d  = array_values($d);


	                        $kmkmkm = 0;
	                        foreach ($selection_array_total as $key => $value) {
	                            if(in_array($key, array('Einzelhandel','Büro/Praxen','Wohnungen','Gastronomie','Lager','Stellplätze','Sonstiges')))
	                            $kmkmkm += $value;
	                        }
	                        $kmkmkm = $v2;
	                        
	                    ?>


                        <div class="form-group {{ $errors->has('rentable_area')? 'has-error':'' }}">
                            <label for="rentable_area" class="col-sm-4 control-label">Vermietbare Fläche in m²</label>
                            <div class="col-sm-8">
                                <input readonly="readonly" type="text" step="any" class="form-control mask-number-input" id="rentable_area" value="{{$kmkmkm}}" name="rentable_area" placeholder="Vermietbare Fläche in m²">
                                {!! $errors->has('rentable_area')? '<p class="help-block">'.$errors->first('rentable_area').'</p>':'' !!}
                            </div>
                        </div>

                    	<?php
                    		$lp = $cs = "";
                    		foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule){
                        		$lp = $tenancy_schedule->calculations['mi10'];
                        		$cs = $tenancy_schedule->calculations['mi9'];
                    		}

                        	$lp = $v3;
                        	$cs = $v4;
                    	?>

                        <div class="form-group {{ $errors->has('living_space')? 'has-error':'' }}">
                            <label for="living_space" class="col-sm-4 control-label">Wohnfläche in m²</label>
                            <div class="col-sm-8">
                                <input readonly="readonly" type="text" step="any" class="form-control mask-number-input" id="living_space" value="{{$lp}}" name="living_space" placeholder="Wohnfläche in m²">
                                {!! $errors->has('living_space')? '<p class="help-block">'.$errors->first('living_space').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('commercial_space_area')? 'has-error':'' }}">
                            <label for="commercial_space_area" class="col-sm-4 control-label">Gewerbefläche in m²</label>
                            <div class="col-sm-8">
                                <input readonly="readonly" type="text" step="any" class="form-control mask-number-input" id="commercial_space_area" value="{{$cs}}" name="commercial_space" placeholder="Gewerbefläche in m²">
                                {!! $errors->has('commercial_space')? '<p class="help-block">'.$errors->first('commercial_space').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('current_state')? 'has-error':'' }}">
                            <label for="current_state" class="col-sm-4 control-label">Zustand</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="current_state" value="@if(isset($exportads)){{$exportads->current_state}}@else{{ old('current_state') }}@endif" name="current_state" placeholder="Zustand">
                                {!! $errors->has('current_state')? '<p class="help-block">'.$errors->first('current_state').'</p>':'' !!}
                            </div>
                        </div>

                        <?php
	                        // $construction_year = "";
	                        // if(isset($exportads) && $exportads->const_year)
	                        //     $construction_year = $exportads->const_year;
	                        // else{
	                            if($ist_sheet)
	                            $construction_year = $ist_sheet->construction_year;
	                            else
	                            $construction_year = $properties->construction_year;
	                        // }
                    	?>

                        <div class="form-group {{ $errors->has('const_year')? 'has-error':'' }}">
                            <label for="const_year" class="col-sm-4 control-label">Baujahr</label>
                            <div class="col-sm-8">
                                <input type="number"  readonly="readonly"  class="form-control" id="const_year" value="{{$construction_year}}" name="const_year" placeholder="Baujahr">
                                {!! $errors->has('const_year')? '<p class="help-block">'.$errors->first('const_year').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('stellplatze')? 'has-error':'' }}">
                            <label for="stellplatze" class="col-sm-4 control-label">Stellplätze</label>
                            <div class="col-sm-8">
                                <input type="text" readonly="readonly" step="any" class="form-control" id="stellplatze" value="{{$plot}}" name="stellplatze" placeholder="Stellplätze">
                                {!! $errors->has('stellplatze')? '<p class="help-block">'.$errors->first('stellplatze').'</p>':'' !!}
                            </div>
                        </div>
                    

                        <div class="form-group {{ $errors->has('heating')? 'has-error':'' }}">
                            <label for="heating" class="col-sm-4 control-label">Heizungsart</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="heating" value="@if(isset($exportads)){{$exportads->heating}}@else{{ old('heating') }}@endif" name="heating" placeholder="Heizungsart">
                                {!! $errors->has('heating')? '<p class="help-block">'.$errors->first('heating').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('energy_available')? 'has-error':'' }}">
                            <label for="energy_available" class="col-sm-4 control-label">Energieausweis liegt vor</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="energy_available" value="@if(isset($exportads)){{$exportads->energy_available}}@else{{ old('energy_available') }}@endif" name="energy_available" placeholder="Energieausweis liegt vor">
                                {!! $errors->has('energy_available')? '<p class="help-block">'.$errors->first('energy_available').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('energy_value')? 'has-error':'' }}">
                            <label for="energy_value" class="col-sm-4 control-label">Energiekennwert</label>
                            <div class="col-sm-8">
                                <input type="text" step="any" class="form-control mask-number-input" id="energy_value" value="@if(isset($exportads)){{$exportads->energy_value}}@else{{ old('energy_value') }}@endif" name="energy_value" placeholder="Energiekennwert">
                                {!! $errors->has('energy_value')? '<p class="help-block">'.$errors->first('energy_value').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('energy_valid')? 'has-error':'' }}">
                            <label for="energy_valid" class="col-sm-4 control-label">Energieausweis gültig bis</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="energy_valid" value="@if(isset($exportads)){{$exportads->energy_valid}}@else{{ old('energy_valid') }}@endif" name="energy_valid" placeholder="Energieausweis gültig bis">
                                {!! $errors->has('energy_valid')? '<p class="help-block">'.$errors->first('energy_valid').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('provision')? 'has-error':'' }}">
                            <label for="provision" class="col-sm-4 control-label">Provision</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="provision" value="@if(isset($exportads)){{$exportads->provision}}@else{{ old('provision') }}@endif" name="provision" placeholder="Provision">
                                {!! $errors->has('provision')? '<p class="help-block">'.$errors->first('provision').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('annual_rent')? 'has-error':'' }}">
                            <label for="annual_rent" class="col-sm-4 control-label">Jahresnettomiete in €</label>
                            <div class="col-sm-8">
                                <input readonly="readonly" type="text" step="any" class="form-control" id="provision" value="{{$an_rent}}" name="annual_rent" placeholder="Jahresnettomiete in €">
                                {!! $errors->has('annual_rent')? '<p class="help-block">'.$errors->first('annual_rent').'</p>':'' !!}
                            </div>
                        </div>

                    	<?php
                    		$Ankermieter = "";
                    		if(isset($a[0]))
                        		$Ankermieter = $a[0];

                    		// if(isset($exportads) && $exportads->tenants)
                        		// $Ankermieter = $exportads->tenants;
                    	?>

                        <div class="form-group {{ $errors->has('tenants')? 'has-error':'' }}">
                            <label for="tenants" class="col-sm-4 control-label">Ankermieter</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="provision" value="{{$Ankermieter}}" name="tenants" placeholder="Ankermieter">
                                {!! $errors->has('tenants')? '<p class="help-block">'.$errors->first('tenants').'</p>':'' !!}
                            </div>
                        </div>

                        <?php
                        	$mtn = 0;
                        	if($m_price)
                            	$mtn = 100*$an_rent1/$m_price;

                        	$mtn  = number_format((float)$mtn, 2, '.', '');
                        ?>

                        <div class="form-group {{ $errors->has('equity')? 'has-error':'' }}">
                            <label for="equity" class="col-sm-4 control-label">Mietrendite</label>
                            <div class="col-sm-8">
                                <input readonly="readonly" type="text" step="any" class="form-control mask-number-input" id="equity" value="{{$mtn}}" name="equity" placeholder="Mietrendite">
                                {!! $errors->has('equity')? '<p class="help-block">'.$errors->first('equity').'</p>':'' !!}
                            </div>
                        </div>

                    	<?php
				            $vvv = 0;
				            $v1 = 0;
				            $pm_total = 0;
				            $pa_total = 0;

				            $pm_total1 = 0;
				            $pa_total1 = 0;

				            // $w = "";
				        ?>
        
                    	<?php
                    		foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule):
            
                				$t = $tenancy_schedule->calculations['total_actual_net_rent'];
                				$pm_total += $t;
                				$pa_total +=$t*12;

                				$pm_total1 += $tenancy_schedule->calculations['potenzial_eur_monat'];
                				$pa_total1 += $tenancy_schedule->calculations['potenzial_eur_jahr'];

                				if( ($tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10'] ) != 0)
                      			{

                        			$v1 = number_format(($tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $tenancy_schedule->calculations['total_business_vacancy_in_qm']) / ($tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10']) * 100,2,",",".");
                        			$vvv =  number_format(100 - (($tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $tenancy_schedule->calculations['total_business_vacancy_in_qm']) / ($tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10']) * 100),2,",",".").'%';
                      			}
                            

                				$vermietet_total += $tenancy_schedule->calculations['total_live_rental_space'];
                				$vermietet_total += $tenancy_schedule->calculations['total_business_rental_space'];

                				$leerstand_total += $tenancy_schedule->calculations['total_live_vacancy_in_qm'];
                				$leerstand_total += $tenancy_schedule->calculations['total_business_vacancy_in_qm'];

            
            					// $w = ($tenancy_schedule->calculations['total_business_actual_net_rent']!=0)?number_format(($tenancy_schedule->calculations['total_remaining_time_in_eur'])/(12 * $tenancy_schedule->calculations['total_business_actual_net_rent']),1) : 0 ;
            				endforeach;

                			// if(isset($exportads) && $exportads->wault)
                        	// $w = $exportads->wault;
                    	?>

                        <div class="form-group {{ $errors->has('wault')? 'has-error':'' }}">
                            <label for="wault" class="col-sm-4 control-label">WAULT</label>
                            <div class="col-sm-8">
                                <input readonly="readonly" type="text" step="any" class="form-control" id="wault" value="{{$w}}" name="wault" placeholder="WAULT">
                                {!! $errors->has('wault')? '<p class="help-block">'.$errors->first('wault').'</p>':'' !!}
                            </div>
                        </div>

                    	<legend class="hidden">Ausstattungsmerkmale</legend>
                    
                    	@php 
                    		$saved_amenities = array();
                    		if(isset($exportads))
                    			$saved_amenities = (array) unserialize($exportads->amenities); 
                    	@endphp

                        <div class="form-group type_checkbox hidden">
                            <div class="col-sm-12">
                               {{--  //@if($categories->count() > 0)
                                    //@foreach($categories as $category) --}}
                                        <label> <input type="checkbox" value="123" name="amenities[123]" @if(in_array(123 ,$saved_amenities)) checked="checked" @endif>Leerstände</label>
                                        <label> <input type="checkbox" value="124" name="amenities[124]" @if(in_array(124 ,$saved_amenities)) checked="checked" @endif> Grundstücksfläche qm </label>
                              {{--  @endforeach
                                @endif   --}}
                            </div>
                        </div>

                   

                    	<legend>Bild</legend>

                        <div class="form-group {{ $errors->has('images')? 'has-error':'' }}">
                            <div class="col-sm-12 export-medias">

                                @if(isset($exportads) && $exportads->images)
                                  	<div id="sortablelist" class="list-group mb-4 mt-3">
                                    	<?php
                                    		$imageid = 0;
                                    		$arr_titel = array();
                                    		$arr = explode(',', $exportads->images);
                                    	?>
                                    	@if(isset($exportads) && $exportads->image_title)
                                    		<?php $imageid = $exportads->image_title; ?>
                                    	@endif

                                    	@foreach($arr as $key=>$img)
                                        	<div  class="list-group-item creating-ads-img-wrap media-common-class">
                                            	<img src="{{ asset('ad_files_upload/'.$img) }}" class="img-responsive" />
                                            		<input class="dr-image" type="hidden" name="img[]" value="{{$img}}">
                                            		@if($key==$imageid)
                                            			<a href="javascript:;" class="exportsetasfavourite" data-key="{{$exportads->id}}" data-id="{{ $key }}"><i class="fa fa-star"></i></a>
                                            		@else
                                            			<a href="javascript:;" class="exportsetasfavourite"  data-key="{{$exportads->id}}" data-id="{{ $key }}"><i class="fa fa-star-o"></i></a>
                                            		@endif
                                            		<div class="img-action-wrap" id="{{ $key }}">
                                                		<a href="javascript:;" class="imgDeleteBtn"><i class="fa fa-trash-o"></i> </a>
                                            		</div>
                                        	</div>
                                    	@endforeach
                                    </div>
                                @endif
        
                                <div class="file-upload-wrap">
                                  <div id="image_preview3" style="float: left;"></div>
                                    <label for="images3">
                                        <input type="file" name="images[]" id="images3" multiple onchange="preview_image3();" style="display: none;" />
                                        <i class="fa fa-cloud-upload"></i>
                                        <p>Bild hochladen...</p>
                                        <div class="progress" style="display: none;"></div>
                                    </label>
                                </div>

                                {!! $errors->has('images')? '<p class="help-block">'.$errors->first('images').'</p>':'' !!}

                            </div>
                        </div>

                    	<legend>location Info</legend>

                        <div class="form-group  {{ $errors->has('country')? 'has-error':'' }}">
                            <label for="category_name" class="col-sm-4 control-label">Land</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="country" value="82">
                                Germany
                                {{-- <select class="form-control select2 " name="country" id="excountry">
                                    <option value="">Ein Land auswählen</option>


                                    @foreach($countries_ad as $country) 
                                        <option value="{{ $country->id }}" @if($exportads && $exportads->country_id==$country->id)selected @endif>{{ $country->country_name }}</option>
                                      @endforeach   
                                </select> --}}
                                {!! $errors->has('country')? '<p class="help-block">'.$errors->first('country').'</p>':'' !!}
                            </div>
                        </div>

                        <?php
                        	$street = $postcode = $city = $istate = "";
                        	$ist_sheet = "";
                        	if(isset($GLOBALS['ist_sheet']) && $GLOBALS['ist_sheet'])
                          		$ist_sheet = $GLOBALS['ist_sheet'];


                        	if($ist_sheet){
                            	$street = $ist_sheet->strasse.' '.$ist_sheet->hausnummer;
                            	$postcode = $ist_sheet->plz_ort;
                            	$city = $ist_sheet->ort;
                            	$istate = $ist_sheet->niedersachsen;
                        	}
                        ?>

                        <div class="form-group  {{ $errors->has('state')? 'has-error':'' }}">
                            <label for="category_name" class="col-sm-4 control-label">Bundesland</label>
                            <div class="col-sm-8">
                                <select class="form-control select2" id="excountry_to_state" readonly name="state">
                                    @foreach($eprevious_states as $state)
                                    	<option value="{{ $state->id }}" {{ $istate == $state->state_name ? 'selected' :'' }}>{{ $state->state_name }}</option>
                                    @endforeach
                                </select>
                                <p class="text-info">
                                    <span id="state_loader" style="display: none;"><i class="fa fa-spin fa-spinner"></i> </span>
                                </p>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('city')? 'has-error':'' }}">
                            <label for="wault" class="col-sm-4 control-label">Stadt</label>
                            <div class="col-sm-8">
                                <input type="text"  class="form-control" id="city" value="{{$city}}" readonly name="ad_city_name" placeholder="Stadt">
                                {!! $errors->has('city')? '<p class="help-block">'.$errors->first('city').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('street')? 'has-error':'' }}">
                            <label for="street" class="col-sm-4 control-label">Straße</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="street" value="{{$street}}" readonly name="street" placeholder="Straße">
                                {!! $errors->has('street')? '<p class="help-block">'.$errors->first('street').'</p>':'' !!}
                                <!-- <p class="text-info"> @lang('app.great_title_info')</p> -->
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('postcode')? 'has-error':'' }}">
                            <label for="postcode" class="col-sm-4 control-label">Postleitzahl</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="postcode" value="{{$postcode}}" readonly name="postcode" placeholder="Postleitzahl">
                                {!! $errors->has('postcode')? '<p class="help-block">'.$errors->first('postcode').'</p>':'' !!}
                                <!-- <p class="text-info"> @lang('app.great_title_info')</p> -->
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('location')? 'has-error':'' }}">
                            <label for="location" class="col-sm-4 control-label">Lage</label>
                            <div class="col-sm-8">
                                <textarea  class="form-control" id="location" rows="8" name="location" placeholder="Lage">@if(isset($exportads)){{$exportads->location}}@else{{ old('location') }}@endif</textarea>
                                {!! $errors->has('location')? '<p class="help-block">'.$errors->first('postcode').'</p>':'' !!}
                                <!-- <p class="text-info"> @lang('app.great_title_info')</p> -->
                            </div>
                        </div>

                        <?php

                        	$users = DB::table('users')->get();
                        	$contact_id = 0;
                        	if(isset($exportads))
                            	$contact_id = $exportads->user_id;

                        ?>

                        <div class="form-group {{ $errors->has('user_id')? 'has-error':'' }}">
                            <label for="user_id" class="col-sm-4 control-label">Kontakt</label>
                            <div class="col-sm-8">
                                <select class="form-control " name="user_id" id="user_id">
                                    @foreach($users as $user)
                                    	<option value="{{$user->id}}"  {{ $user->id == $contact_id ? 'selected':'' }}>{{$user->name}}</option>
                                    @endforeach
                                </select>
                                {!! $errors->has('user_id')? '<p class="help-block">'.$errors->first('user_id').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="form-group hidden {{ $errors->has('total_area')? 'has-error':'' }}">
                            <label for="postcode" class="col-sm-4 control-label">Gesamtfläche m²</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="total_area" name="total_area" value="@if(isset($exportads)){{$exportads->total_area}}@else{{ old('total_area') }}@endif" placeholder="Gesamtfläche m²">
                                {!! $errors->has('total_area')? '<p class="help-block">'.$errors->first('total_area').'</p>':'' !!}
                                <!-- <p class="text-info"> @lang('app.great_title_info')</p> -->
                            </div>
                        </div>
                   

                        <div class="form-group hidden {{ $errors->has('number_of_parking')? 'has-error':'' }}">
                            <label for="postcode" class="col-sm-4 control-label">Anzahl Stellplätze</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="number_of_parking" name="number_of_parking" placeholder="Anzahl Stellplätze" value="@if(isset($exportads)){{$exportads->number_of_parking}}@else{{ old('number_of_parking') }}@endif">
                                {!! $errors->has('number_of_parking')? '<p class="help-block">'.$errors->first('number_of_parking').'</p>':'' !!}
                                <!-- <p class="text-info"> @lang('app.great_title_info')</p> -->
                            </div>
                        </div>


                        <div class="form-group {{ $errors->has('note')? 'has-error':'' }}">
                            <label for="note" class="col-sm-4 control-label">HINWEIS</label>
                            <div class="col-sm-8">
                                <textarea  class="form-control" id="note" rows="8" name="note" placeholder="HINWEIS">@if(isset($exportads)){{$exportads->note}}@else{{ old('note') }}@endif</textarea>
                                {!! $errors->has('note')? '<p class="help-block">'.$errors->first('note').'</p>':'' !!}
                                <!-- <p class="text-info"> @lang('app.great_title_info')</p> -->
                            </div>
                        </div>


                        <div class="form-group {{ $errors->has('domestic_equipments')? 'has-error':'' }}">
                            <label for="domestic_equipments" class="col-sm-4 control-label">AUSSTATTUNG</label>
                            <div class="col-sm-8">
                                <textarea  class="form-control" id="domestic_equipments" rows="8" name="domestic_equipments" placeholder="AUSSTATTUNG">@if(isset($exportads)){{$exportads->domestic_equipments}}@else{{ old('domestic_equipments') }}@endif</textarea>
                                {!! $errors->has('domestic_equipments')? '<p class="help-block">'.$errors->first('domestic_equipments').'</p>':'' !!}
                                <!-- <p class="text-info"> @lang('app.great_title_info')</p> -->
                            </div>
                        </div>

                    	<div class="form-group {{ $errors->has('sonstiges')? 'has-error':'' }}">
                            <label for="sonstiges" class="col-sm-4 control-label">Sonstiges</label>
                            <div class="col-sm-8">
                                    <textarea  class="form-control" id="sonstiges" rows="8" name="sonstiges" placeholder="Sonstiges">@if(isset($exportads)){{$exportads->sonstiges}}@else{{ old('sonstiges') }}@endif</textarea>
                                    {!! $errors->has('sonstiges')? '<p class="help-block">'.$errors->first('sonstiges').'</p>':'' !!}
                                    <!-- <p class="text-info"> @lang('app.great_title_info')</p> -->
                            </div>
                    	</div>

                    	@if(isset($exportads))
                    		<input type="hidden" class="form-control" id="ex-latitude" value="{{ $exportads->latitude }}" name="latitude" placeholder="@lang('app.latitude')">
                    		<input type="hidden" class="form-control" id="ex-longitude" value="{{$exportads->longitude }}" name="longitude" placeholder="@lang('app.longitude')">
                    	@else
                    		<input type="hidden" class="form-control" id="ex-latitude" value="{{ old('latitude') }}" name="latitude" placeholder="@lang('app.latitude')">
                    		<input type="hidden" class="form-control" id="ex-longitude" value="{{ old('longitude') }}" name="longitude" placeholder="@lang('app.longitude')">
                    	@endif

                    	<legend class="hidden">Transaction Manager</legend>

                        <div class="hidden form-group {{ $errors->has('seller_name')? 'has-error':'' }}">
                            <label for="ad_title" class="col-sm-4 control-label">Agentenname</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="seller_name" value="@if(isset($exportads)){{$exportads->seller_name}}@endif" name="seller_name" placeholder="Agentenname">
                                {!! $errors->has('seller_name')? '<p class="help-block">'.$errors->first('seller_name').'</p>':'' !!}
                            </div>
                        </div>

                        <div class="hidden form-group {{ $errors->has('seller_email')? 'has-error':'' }}">
                            <label for="ad_title" class="col-sm-4 control-label">Agenten-E-Mail</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="seller_email" value="@if(isset($exportads)){{$exportads->seller_email}}@endif" name="seller_email" placeholder="Agenten-E-Mail">
                                {!! $errors->has('seller_email')? '<p class="help-block">'.$errors->first('seller_email').'</p>':'' !!}
                            </div>
                        </div>

                    	<input type="hidden" name="seller_phone" value="">

                    	<div class="form-group">
                        	<div class="col-sm-offset-4 col-sm-8">
                            	<button type="submit" class="btn btn-primary">Speichern</button>
                            	<button type="submit" class="btn btn-success" name="download" value="1">Expose erstellen</button>
                        	</div>
                    	</div>
                    {{ Form::close() }}

                    <a href="{{ url('/download_property_pdf/'.$id) }}" id="download_property_pdf" style="opacity: 0;"></a>

                </div>
            </div>
  		</div>

  	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var _token             			= '{{ csrf_token() }}';
		var url_uploadfiles 			= '{{ route('uploadfiles') }}';
		var url_delete_ads_images		= '{{ route('delete_ads_images') }}';
		var url_set_ad_image_favourite 	= '{{ route('set_ad_image_favourite') }}';
		var url_update_image_order 		= '{{route('update_image_order')}}';
	</script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script type="text/javascript" src="{{asset('js/Sortable.js')}}"></script>
  	<script src="{{asset('js/property/exportverkauf_tab.js')}}"></script>
@endsection