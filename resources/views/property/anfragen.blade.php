@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="Anfragen" class="tab-pane fade in active">
  		
  		<div class="row">
			<div class="col-sm-12 table-responsive white-box">
				<table class="table table-striped" id="inquiries">
					<thead>
						<tr>
							<th>Name</th>
							<th>E-Mail</th>
							<th>Telefonnummer</th>
							<th>Nachricht</th>
						</tr>
					</thead>
					<tbody>
						@if ($contact_queries)
						
							@foreach($contact_queries as $list)
								<tr>
									<td>{{$list->name}}</td>
									<td>{{$list->email}}</td>
									<td>{{$list->phone}}</td>
									<td>{{$list->message}}</td>
								</tr>
							@endforeach

						@endif
					</tbody>
				</table>
			</div>
		</div>

  	</div>
</div>

@endsection

@section('js')
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script type="text/javascript">
  		$('#inquiries').DataTable();
  	</script>
@endsection