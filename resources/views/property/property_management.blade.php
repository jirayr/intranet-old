@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="property_management" class="tab-pane fade in active">

		<div class="">
			<div class="modal-dialog modal-lg" style="width: 100%">
			    <div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Hausverwaltung</h4>
					</div>
					<div class="modal-body">
						<div class="row ">

							<button style="margin-left: 2%" type="button" class="btn btn-success btn-xs" onclick="add_property_management(1)">Hinzufügen</button>

							@if(Auth::user()->email!=config('users.falk_email'))

								<button class="btn btn-primary btn-xs @if(isset($mails_log_data['management'])) btn-success @else btn-primary @endif @if(!isset($mails_log_data['release_management'])) management-request-button @endif" data-id="management">
									@if(!isset($mails_log_data['release_management']))  
										Zur Freigabe an Falk senden 
									@else 
										Freigegeben 
									@endif
								</button>

							@else

								{{-- <button class="btn  btn-xs   @if(!isset($mails_log_data['release_management'])) btn-primary management-request-button @else btn-success  @endif" data-id="release_management">@if(!isset($mails_log_data['release_management'])) Freigeben @else Freigegeben @endif </button> --}}

							@endif

							<div class="management-table-list">
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>	

		<div class="tenancy-schedules" style="display: block;">
			<h3 style="margin-top: 40px;">Verlauf</h3>
			<div class="clearfix"></div>
		  	<table class="forecast-table" >
		    	<thead>
		      		<tr>
		        		<th class="border bg-gray">Name</th>
		        		<th class="border bg-gray">Button</th>
		        		<th class="border bg-gray">Kommentar</th>
		        		<th class="border bg-gray">Datum</th>
		      		</tr>
		    	</thead>
		    	<tbody>
		      		@foreach($mails_logs as $k=>$list)
		      
		      			<?php
		      				$tname = "";
		      				$u = DB::table('users')->where('id', $list['user_id'])->first();
		      				if($u)
		        				$tname = $u->name;

		      				$rbutton_title = "";
					      	if($list['type']=="management")
					      		$rbutton_title = "Zur Freigabe an Falk gesendet";
					      	if($list['type']=="release_management")
					        	$rbutton_title = "Freigegeben";
					      	if($list['type']=="not_release_property_management")
					        	$rbutton_title = "Ablehnen";

		      			?>

		      			@if($rbutton_title)
			      			<tr>
			        			<td class="border">{{$tname}}</td>
			        			<td class="border">{{$rbutton_title}}</td>
			        			<td class="border">{{$list['comment']}}</td>
			        			<td class="border">{{show_datetime_format($list['created_at'])}}</td>
			      			</tr>
		      			@endif
		      		@endforeach
		    	</tbody>
		  	</table>
		</div>

		<div class="modal fade" id="management-modal-request" role="dialog">
		   <div class="modal-dialog">
		      <!-- Modal content-->
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		         </div>
		         <div class="modal-body">
		            <label>Kommentar</label>
		            <textarea class="form-control management-comment" name="message"></textarea>
		            <br>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-primary management-comment-submit" data-dismiss="modal" >Senden</button>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		         </div>
		      </div>
		   </div>
		</div>

		<div class="modal fade" id="property-management-not-release-modal" role="dialog">
		   <div class="modal-dialog">
		      <!-- Modal content-->
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		         </div>
		         <form action="{{ route('propertyManagementNotRelease') }}" id="property-management-not-release-form">
		            <div class="modal-body">
		               <label>Kommentar</label>
		               <textarea class="form-control" name="message" required></textarea>
		               <br>
		            </div>
		            <div class="modal-footer">
		               <button type="submit" class="btn btn-primary">Senden</button>
		               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		            </div>
		         </form>
		      </div>
		   </div>
		</div>

		<div class="modal fade" id="pmanagement-modal-request" role="dialog">
		   <div class="modal-dialog">
		      <!-- Modal content-->
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		         </div>
		         <div class="modal-body">
		            <label>Kommentar</label>
		            <textarea class="form-control pmanagement-comment" name="message"></textarea>
		            <br>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-primary pmanagement-comment-submit" data-dismiss="modal" >Senden</button>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		         </div>
		      </div>
		   </div>
		</div>

  	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var _token 								= '{{ csrf_token() }}';
		var url_property_management_add 		= '{{ url('property_management/add') }}';
		var url_property_addmaillog 			= '{{ url('property/addmaillog') }}';
		var url_property_management_update 		= '{{ route('property_management.update') }}';
	</script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{ asset('js/custom-datatable.js') }}"></script>
  	<script src="{{asset('js/property/property_management.js')}}"></script>
@endsection