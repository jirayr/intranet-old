@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="budget" class="tab-pane fade in active">

  		<h3>Budget</h3>

  		<?php
         	$bank_array = array();
         	$j = $ist = 0;
         	foreach($banks as $key => $bank){
            	$bank_array[] = $bank->id;
         	}

         	array_unique($bank_array);
         	$str = implode(',', $bank_array);
         
         	$properties_banks = DB::table('properties')->whereRaw('main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('standard_property_status','desc')->get();
         	if($properties_banks){
         		foreach ($properties_banks as $property_sheet) {
         			if($property_sheet->lock_status == 0){
         				if($property_sheet->Ist){
         					if($ist==0){
                				$GLOBALS['ist_sheet'] = $property_sheet;
            				}
            				$ist = 1;
         				}
         			}else{
         				$GLOBALS['release_sheet'] = $property_sheet;
         			}
         		}
         	}
        ?>	

		<?php
			$budgetproperties = "";

			if(isset($GLOBALS['release_sheet']) && $GLOBALS['release_sheet']){
				$budgetproperties = $GLOBALS['release_sheet'];
			}else if(isset($GLOBALS['ist_sheet']) && $GLOBALS['ist_sheet']){
 				$budgetproperties = $GLOBALS['ist_sheet'];

 				// $budgetproperties->net_rent = $budgetproperties->net_rent/100;
		 		// $budgetproperties->tax = $budgetproperties->tax/100;

			 	// $budgetproperties->maintenance = $budgetproperties->maintenance/100;
			 	// $budgetproperties->operating_costs = $budgetproperties->operating_costs/100;
			 	// $budgetproperties->operating_costs_nk = $budgetproperties->operating_costs_nk/100;
			 	// $budgetproperties->object_management = $budgetproperties->object_management/100;
			 	// $budgetproperties->object_management_nk = $budgetproperties->object_management_nk/100;


	 			/* $list_fields_percent = ['net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'];

	    		foreach ($list_fields_percent as $jjj) {
	    			$budgetproperties->$jjj = $budgetproperties->$jjj/100;
	    		}*/
			}


			$budgetpropertiesExtra1s = array();
			if($budgetproperties){
				$budgetpropertiesExtra1s =  DB::table('properties_tenants')->whereRaw('propertyId='.$budgetproperties->id)->get();	
			}

			$total_ccc = 0;
			$ankdate1 = $ankdate2  = "";
		?>

		@if(isset($budgetpropertiesExtra1s) && $budgetpropertiesExtra1s)
			@foreach($budgetpropertiesExtra1s as $k=>$budgetpropertiesExtra1)
				<?php
					if($budgetpropertiesExtra1->is_current_net)
						$total_ccc += $budgetpropertiesExtra1->net_rent_p_a; 

					if($k==0)
						$ankdate1 = $budgetpropertiesExtra1->mv_end;
					if($k==1)
						$ankdate2 = $budgetpropertiesExtra1->mv_end;

				?>
			@endforeach
		@endif

		<?php

			$budget =  DB::table('budgets')->whereRaw('property_id='.$budgetproperties->main_property_id)->first();

			$bank_interest_year_1 = $bank_interest_year_2 = $bank_interest_year_3 = $bank_interest_year_4 = $bank_interest_year_5 = $interest_rate_bond_year_1 = $interest_rate_bond_year_2 = $interest_rate_bond_year_3 = $interest_rate_bond_year_4 = $interest_rate_bond_year_5 = $ebt_year_1 = $ebt_year_2 = $ebt_year_3 = $ebt_year_4 = $ebt_year_5 = $tax_year_1 = $tax_year_2 = $tax_year_3 = $tax_year_4 = $tax_year_5 = $eat_year_1 = $eat_year_2 = $eat_year_3 = $eat_year_4 = $eat_year_5 = $redemption_bank_year_1 = $redemption_bank_year_2 = $redemption_bank_year_3 = $redemption_bank_year_4 = $redemption_bank_year_5 = $cashflow_year_1 = $cashflow_year_2 = $cashflow_year_3 = $cashflow_year_4 = $cashflow_year_5 = 0;

			$depreciation_nk_money_year1  = $depreciation_nk_money_year2 = $depreciation_nk_money_year3 = $depreciation_nk_money_year4 = $depreciation_nk_money_year5 = 0;

			if($budget){

				if($budget->net_rent)
					$budgetproperties->net_rent = $budget->net_rent;

				if($budget->net_rent_empty)
					$budgetproperties->net_rent_empty = $budget->net_rent_empty;

				if($budget->object_management)
					$budgetproperties->object_management = $budget->object_management;

				if($budget->tax)
					$budgetproperties->tax = $budget->tax;

				if($budget->ebit_year_1)
				      $ebit_year_1 = $budget->ebit_year_1;

				if($budget->ebit_year_2)
				      $ebit_year_2 = $budget->ebit_year_2;

				if($budget->ebit_year_3)
				    $ebit_year_3 = $budget->ebit_year_3;

				if($budget->ebit_year_4)
				    $ebit_year_4 = $budget->ebit_year_4;

				if($budget->ebit_year_5)
				    $ebit_year_5 = $budget->ebit_year_5;

				if($budget->bank_interest_year_1)
					$bank_interest_year_1 = $budget->bank_interest_year_1;

				if($budget->bank_interest_year_2)
					$bank_interest_year_2 = $budget->bank_interest_year_2;

				if($budget->bank_interest_year_3)
					$bank_interest_year_3 = $budget->bank_interest_year_3;

				if($budget->bank_interest_year_4)
					$bank_interest_year_4 = $budget->bank_interest_year_4;

				if($budget->bank_interest_year_5)
					$bank_interest_year_5 = $budget->bank_interest_year_5;

				if($budget->interest_rate_bond_year_1)
				    $interest_rate_bond_year_1 = $budget->interest_rate_bond_year_1;

				if($budget->interest_rate_bond_year_2)
				    $interest_rate_bond_year_2 = $budget->interest_rate_bond_year_2;

				if($budget->interest_rate_bond_year_3)
				    $interest_rate_bond_year_3 = $budget->interest_rate_bond_year_3;

				if($budget->interest_rate_bond_year_4)
				    $interest_rate_bond_year_4 = $budget->interest_rate_bond_year_4;

				if($budget->interest_rate_bond_year_5)
				      $interest_rate_bond_year_5 = $budget->interest_rate_bond_year_5;

				if($budget->ebt_year_1)
					$ebt_year_1 = $budget->ebt_year_1;

				if($budget->ebt_year_2)
				    $ebt_year_2 = $budget->ebt_year_2;

				if($budget->ebt_year_3)
				    $ebt_year_3 = $budget->ebt_year_3;

				if($budget->ebt_year_4)
				    $ebt_year_4 = $budget->ebt_year_4;

				if($budget->ebt_year_5)
				    $ebt_year_5 = $budget->ebt_year_5;

				 if($budget->tax_year_1)
				    $tax_year_1 = $budget->tax_year_1;

				if($budget->tax_year_2)
				    $tax_year_2 = $budget->tax_year_2;

				if($budget->tax_year_3)
				    $tax_year_3 = $budget->tax_year_3;

				if($budget->tax_year_4)
				    $tax_year_4 = $budget->tax_year_4;

				if($budget->tax_year_5)
				    $tax_year_5 = $budget->tax_year_5;

				if($budget->eat_year_1)
				    $eat_year_1 = $budget->eat_year_1;

				if($budget->eat_year_2)
				    $eat_year_2 = $budget->eat_year_2;

				if($budget->eat_year_3)
				    $eat_year_3 = $budget->eat_year_3;

				if($budget->eat_year_4)
				    $eat_year_4 = $budget->eat_year_4;

				if($budget->eat_year_5)
				      $eat_year_5 = $budget->eat_year_5;

				if($budget->redemption_bank_year_1)
				    $redemption_bank_year_1 = $budget->redemption_bank_year_1;

				if($budget->redemption_bank_year_2)
				    $redemption_bank_year_2 = $budget->redemption_bank_year_2;

				if($budget->redemption_bank_year_3)
				    $redemption_bank_year_3 = $budget->redemption_bank_year_3;

				if($budget->redemption_bank_year_4)
				    $redemption_bank_year_4 = $budget->redemption_bank_year_4;

				if($budget->redemption_bank_year_5)
				    $redemption_bank_year_5 = $budget->redemption_bank_year_5;

				if($budget->cashflow_year_1)
				    $cashflow_year_1 = $budget->cashflow_year_1;

				if($budget->cashflow_year_2)
				    $cashflow_year_2 = $budget->cashflow_year_2;

				if($budget->cashflow_year_3)
				    $cashflow_year_3 = $budget->cashflow_year_3;

				if($budget->cashflow_year_4)
				    $cashflow_year_4 = $budget->cashflow_year_4;

				if($budget->cashflow_year_5)
				    $cashflow_year_5 = $budget->cashflow_year_5;

				if($budget->depreciation_nk_money_year1)
				    $depreciation_nk_money_year1 = $budget->depreciation_nk_money_year1;

				if($budget->depreciation_nk_money_year2)
				    $depreciation_nk_money_year2 = $budget->depreciation_nk_money_year2;

				if($budget->depreciation_nk_money_year3)
				    $depreciation_nk_money_year3 = $budget->depreciation_nk_money_year3;

				if($budget->depreciation_nk_money_year4)
				    $depreciation_nk_money_year4 = $budget->depreciation_nk_money_year4;

				if($budget->depreciation_nk_money_year5)
				    $depreciation_nk_money_year5 = $budget->depreciation_nk_money_year5;

			}

		?>

		<?php
		
			$fixbudgetproperties = DB::table('properties')->whereRaw('id='.$budgetproperties->id)->first();	

			if($total_ccc){
				$budgetproperties->net_rent_pa = $total_ccc;
				$fixtotal_ccc = $total_ccc;
				$fixbudgetproperties->net_rent_pa = $total_ccc;
			}else{
				$total_ccc = $budgetproperties->net_rent_pa;
				$fixtotal_ccc = $budgetproperties->net_rent_pa;
			}

			$fixbudgetproperties->net_rent_increase_year1 = $fixtotal_ccc;
			$fixbudgetproperties->net_rent_increase_year2 = $fixbudgetproperties->net_rent_increase_year1 + $fixbudgetproperties->net_rent_increase_year1*$fixbudgetproperties->net_rent;
			$fixbudgetproperties->net_rent_increase_year3 = $fixbudgetproperties->net_rent_increase_year2 + $fixbudgetproperties->net_rent_increase_year2*$fixbudgetproperties->net_rent;
			$fixbudgetproperties->net_rent_increase_year4 = $fixbudgetproperties->net_rent_increase_year3 + $fixbudgetproperties->net_rent_increase_year3*$fixbudgetproperties->net_rent;
			$fixbudgetproperties->net_rent_increase_year5 = $fixbudgetproperties->net_rent_increase_year4 + $fixbudgetproperties->net_rent_increase_year4*$fixbudgetproperties->net_rent;


			$fixbudgetproperties->operating_cost_increase_year1 = $fixbudgetproperties->net_rent_increase_year1*$fixbudgetproperties->operating_costs_nk;

			$fixbudgetproperties->operating_cost_increase_year2 = $fixbudgetproperties->operating_cost_increase_year1 + $fixbudgetproperties->operating_cost_increase_year1*$fixbudgetproperties->operating_costs;

			$fixbudgetproperties->operating_cost_increase_year3 = $fixbudgetproperties->operating_cost_increase_year2 + $fixbudgetproperties->operating_cost_increase_year2*$fixbudgetproperties->operating_costs;

			$fixbudgetproperties->operating_cost_increase_year4 = $fixbudgetproperties->operating_cost_increase_year3 + $fixbudgetproperties->operating_cost_increase_year3*$fixbudgetproperties->operating_costs;
			$fixbudgetproperties->operating_cost_increase_year5 = $fixbudgetproperties->operating_cost_increase_year4 + $fixbudgetproperties->operating_cost_increase_year4*$fixbudgetproperties->operating_costs;


			$fixbudgetproperties->property_management_increase_year1 = $fixbudgetproperties->net_rent_increase_year1*$fixbudgetproperties->object_management_nk;

			$fixbudgetproperties->property_management_increase_year2 =$fixbudgetproperties->property_management_increase_year1 +  $fixbudgetproperties->property_management_increase_year1*$fixbudgetproperties->object_management;

			$fixbudgetproperties->property_management_increase_year3 =$fixbudgetproperties->property_management_increase_year2 +  $fixbudgetproperties->property_management_increase_year2*$fixbudgetproperties->object_management;

			$fixbudgetproperties->property_management_increase_year4 =$fixbudgetproperties->property_management_increase_year3 +  $fixbudgetproperties->property_management_increase_year3*$fixbudgetproperties->object_management;

			$fixbudgetproperties->property_management_increase_year5 =$fixbudgetproperties->property_management_increase_year4 +  $fixbudgetproperties->property_management_increase_year4*$fixbudgetproperties->object_management;


			$fixbudgetproperties->ebitda_year_1 = $fixtotal_ccc - $fixbudgetproperties->maintenance_increase_year1 - $fixbudgetproperties->operating_cost_increase_year1 - $fixbudgetproperties->property_management_increase_year1;


			$fixbudgetproperties->ebitda_year_2 = $fixbudgetproperties->net_rent_increase_year2 - $fixbudgetproperties->maintenance_increase_year2 - $fixbudgetproperties->operating_cost_increase_year2 - $fixbudgetproperties->property_management_increase_year2;
			$fixbudgetproperties->ebitda_year_3 = $fixbudgetproperties->net_rent_increase_year3 - $fixbudgetproperties->maintenance_increase_year3 - $fixbudgetproperties->operating_cost_increase_year3 - $fixbudgetproperties->property_management_increase_year3;
			$fixbudgetproperties->ebitda_year_4 = $fixbudgetproperties->net_rent_increase_year4 - $fixbudgetproperties->maintenance_increase_year4 - $fixbudgetproperties->operating_cost_increase_year4 - $fixbudgetproperties->property_management_increase_year4;
			$fixbudgetproperties->ebitda_year_5 = $fixbudgetproperties->net_rent_increase_year5 - $fixbudgetproperties->maintenance_increase_year5 - $fixbudgetproperties->operating_cost_increase_year5 - $fixbudgetproperties->property_management_increase_year5;

			$fixbudgetproperties->ebit_year_1= $fixbudgetproperties->ebitda_year_1 - $fixbudgetproperties->depreciation_nk_money;
			$fixbudgetproperties->ebit_year_2= $fixbudgetproperties->ebitda_year_2 - $fixbudgetproperties->depreciation_nk_money;
			$fixbudgetproperties->ebit_year_3= $fixbudgetproperties->ebitda_year_3 - $fixbudgetproperties->depreciation_nk_money;
			$fixbudgetproperties->ebit_year_4= $fixbudgetproperties->ebitda_year_4 - $fixbudgetproperties->depreciation_nk_money;
			$fixbudgetproperties->ebit_year_5= $fixbudgetproperties->ebitda_year_5 - $fixbudgetproperties->depreciation_nk_money;

			$fixpcomments_new = DB::table('property_comments')->where('property_id',$fixbudgetproperties->id)->whereNotNull('Ankermieter_note')->where('status',null)->first();

		?>
	 
		{{-- @foreach ($fixbanks as $fixkey => $fixbank) --}}
        <?php

			$fixD42 = $fixbudgetproperties->gesamt_in_eur 
				+ ($fixbudgetproperties->real_estate_taxes * $fixbudgetproperties->gesamt_in_eur) 
				+ ($fixbudgetproperties->estate_agents * $fixbudgetproperties->gesamt_in_eur) 
				+ (($fixbudgetproperties->Grundbuch * $fixbudgetproperties->gesamt_in_eur)/100) 
				+ ($fixbudgetproperties->evaluation * $fixbudgetproperties->gesamt_in_eur) 
				+ ($fixbudgetproperties->others * $fixbudgetproperties->gesamt_in_eur) 
				+ ($fixbudgetproperties->buffer * $fixbudgetproperties->gesamt_in_eur);
			$fixD46 = $fixD42;

			$fixD47 = $fixbudgetproperties->with_real_ek * $fixD46;	//C47*D46
			$fixD48 = $fixbudgetproperties->from_bond * $fixD46;	//C48*D46
			
			$fixD49 = $fixbudgetproperties->bank_loan * $fixD46;

			$fixD50 = $fixD47 + $fixD48 +  $fixD49;
	
			
            if(isset($fixbanks[0]) && $fixbanks[0] == ''){
                $fixbank_check = 0;
                $fixbank = $fixfake_bank;
            }else{
                $fixbank_check = 1;
			}

            $fixH47 = $fixD49 - $fixD49 * $fixbudgetproperties->eradication_bank;
            $fixH48 = $fixD49 * $fixbudgetproperties->interest_bank_loan;    //G48*D49
            $fixH49 = $fixD49 * $fixbudgetproperties->eradication_bank;
            $fixH50 = $fixD49 * $fixbudgetproperties->interest_bank_loan + $fixD49*$fixbudgetproperties->eradication_bank;
			$fixH52 = $fixD48 * $fixbudgetproperties->interest_bond;
           
            $fixI48 = $fixH47 * $fixbudgetproperties->interest_bank_loan;
            $fixI49 = $fixH50 - $fixI48;
            $fixI47 = $fixH47 - $fixI49;
            $fixI50 = $fixI48 + $fixI49;

            $fixJ48 = $fixI47 * $fixbudgetproperties->interest_bank_loan;
            $fixJ49 = $fixH50 - $fixJ48;
            $fixJ47 = $fixI47 - $fixJ49;
            $fixJ50 = $fixJ48 + $fixJ49;

            $fixK48 = $fixJ47 * $fixbudgetproperties->interest_bank_loan;
            $fixK49 = $fixI50 - $fixK48;
            $fixK47 = $fixJ47 - $fixK49;
            $fixK50 = $fixK48 + $fixK49;

            $fixL48 = $fixK47 * $fixbudgetproperties->interest_bank_loan;
            $fixL49 = $fixJ50 - $fixL48;
            $fixL47 = $fixK47 - $fixL49;
            $fixL50 = $fixL48 + $fixL49;


            $fixE18 = ($fixbudgetproperties->net_rent_increase_year1
			-$fixbudgetproperties->maintenance_increase_year1
			-$fixbudgetproperties->operating_cost_increase_year1
			-$fixbudgetproperties->property_management_increase_year1)
			-$fixbudgetproperties->depreciation_nk_money;
			
			$fixF18 = ($fixbudgetproperties->net_rent_increase_year2
			-$fixbudgetproperties->maintenance_increase_year2
			-$fixbudgetproperties->operating_cost_increase_year2
			-$fixbudgetproperties->property_management_increase_year2)
			-$fixbudgetproperties->depreciation_nk_money;

			$fixG18 = ($fixbudgetproperties->net_rent_increase_year3
			-$fixbudgetproperties->maintenance_increase_year3
			-$fixbudgetproperties->operating_cost_increase_year3
			-$fixbudgetproperties->property_management_increase_year3)
			-$fixbudgetproperties->depreciation_nk_money;

			$fixH18 = 
			($fixbudgetproperties->net_rent_increase_year4
			-$fixbudgetproperties->maintenance_increase_year4
			-$fixbudgetproperties->operating_cost_increase_year4
			-$fixbudgetproperties->property_management_increase_year4)
			-$fixbudgetproperties->depreciation_nk_money;

			$fixI18 = ($fixbudgetproperties->net_rent_increase_year5
			-$fixbudgetproperties->maintenance_increase_year5
			-$fixbudgetproperties->operating_cost_increase_year5
			-$fixbudgetproperties->property_management_increase_year5)
			-$fixbudgetproperties->depreciation_nk_money;

			$fixE23 = $fixE18- $fixH48 -$fixH52;
			$fixF23 = $fixF18-($fixH52)-($fixI48);
			$fixG23 = $fixG18-($fixH52)-($fixJ48);
			$fixH23 = $fixH18 - ($fixH52)-($fixK48);
			$fixI23 = $fixI18-($fixH52)-($fixL48);
			$fixE31 = ($fixE23 - ($fixbudgetproperties->tax * $fixE23)) - $fixH49 + $fixbudgetproperties->depreciation_nk_money;
			
			$fixI16= $fixbudgetproperties->depreciation_nk_money;
			$fixI27 =$fixI23 - ($fixbudgetproperties->tax * $fixI23);
			$fixI29 = $fixL49;					
			
			
			$fixI31 = $fixI27-$fixI29+$fixI16;

			$fixG58 = ($fixD42 == 0) ? 0: $fixbudgetproperties->net_rent_pa/$fixD42;
			$fixG59 = ($fixbudgetproperties->net_rent_pa == 0) ? 0 : $fixD42/ $fixbudgetproperties->net_rent_pa;
			$fixE25 = $fixbudgetproperties->tax * $fixE23;
			$fixG61 = ($fixD48 == 0) ? 0 : $fixE31/$fixD48;
			$fixG62 = ($fixD48 == 0) ? 0 : $fixE23/$fixD48;
			
			

			$fixL14 = ($fixbudgetproperties->rent_retail + $fixbudgetproperties->rent_whg == 0) ? 0 : ($fixbudgetproperties->net_rent_pa / ($fixbudgetproperties->rent_retail + $fixbudgetproperties->rent_whg) / 12);
			$fixL16 = ($fixbudgetproperties->rent_retail + $fixbudgetproperties->vacancy_retail + $fixbudgetproperties->rent_whg + $fixbudgetproperties->vacancy_whg == 0) ? 0 : 
			($fixbudgetproperties->gesamt_in_eur)/($fixbudgetproperties->rent_retail + $fixbudgetproperties->vacancy_retail + $fixbudgetproperties->rent_whg + $fixbudgetproperties->vacancy_whg);
			$fixL20 = ($fixbudgetproperties->rent_retail + $fixbudgetproperties->vacancy_retail + $fixbudgetproperties->rent_whg + $fixbudgetproperties->vacancy_whg == 0) ? 0 : 
			($fixbudgetproperties->rent_retail + $fixbudgetproperties->rent_whg)/($fixbudgetproperties->rent_retail + $fixbudgetproperties->vacancy_retail + $fixbudgetproperties->rent_whg + $fixbudgetproperties->vacancy_whg);
			
			$fixL25 = ($fixbudgetproperties->net_rent_pa == 0) ? 0 : $fixbudgetproperties->gesamt_in_eur / $fixbudgetproperties->net_rent_pa;

			
			$fixL11 = $fixbudgetproperties->plot_of_land_m2;
			$fixP31 = $fixbudgetproperties->ground_reference_value_in_euro_m2;
			$fixP43 = $fixL11* $fixP31;
			$fixD59 = $fixbudgetproperties->gesamt_in_eur*($fixbudgetproperties->maintenance_nk);
			$fixD60 = $fixbudgetproperties->operating_cost_increase_year1;

			$fixtotal_ccc = $total_ccc;

			$net_rent_increase_year1 = $total_ccc;


			$budgetproperties->net_rent_increase_year1 = $total_ccc;

			$budgetproperties->net_rent_increase_year1 = 0;
			$budgetproperties->net_rent_increase_year2 = 0;
			$budgetproperties->net_rent_increase_year3 = 0;
			$budgetproperties->net_rent_increase_year4 = 0;
			$budgetproperties->net_rent_increase_year5 = 0;

			if($budget && $budget->net_rent_increase_year1){
				$budgetproperties->net_rent_increase_year1 = $budget->net_rent_increase_year1;
			}

			$maintenance_increase_year1 =  $budgetproperties->maintenance_increase_year1;

			$budgetproperties->maintenance_increase_year1 = 0;
			$budgetproperties->maintenance_increase_year2 = 0;
			$budgetproperties->maintenance_increase_year3 = 0;
			$budgetproperties->maintenance_increase_year4 = 0;
			$budgetproperties->maintenance_increase_year5 = 0;

			if($budget && $budget->maintenance_increase_year1)
				$budgetproperties->maintenance_increase_year1 = $budget->maintenance_increase_year1;
			if($budget && $budget->maintenance_increase_year2)
				$budgetproperties->maintenance_increase_year2 = $budget->maintenance_increase_year2;
			if($budget && $budget->maintenance_increase_year3)
				$budgetproperties->maintenance_increase_year3 = $budget->maintenance_increase_year3;
			if($budget && $budget->maintenance_increase_year4)
				$budgetproperties->maintenance_increase_year4 = $budget->maintenance_increase_year4;
			if($budget && $budget->maintenance_increase_year5)
				$budgetproperties->maintenance_increase_year5 = $budget->maintenance_increase_year5;


		

			if($budget && $budget->net_rent_increase_year2)
				$budgetproperties->net_rent_increase_year2 = $budget->net_rent_increase_year2;
			
			
			if($budget && $budget->net_rent_increase_year3)
				$budgetproperties->net_rent_increase_year3 = $budget->net_rent_increase_year3;

			
			if($budget && $budget->net_rent_increase_year4)
				$budgetproperties->net_rent_increase_year4 = $budget->net_rent_increase_year4;

			
			if($budget && $budget->net_rent_increase_year5)
				$budgetproperties->net_rent_increase_year5 = $budget->net_rent_increase_year5;


			$operating_cost_increase_year1 = $net_rent_increase_year1*$budgetproperties->operating_costs_nk;
			$budgetproperties->operating_cost_increase_year1 = 0;
			$budgetproperties->operating_cost_increase_year2 = 0;
			$budgetproperties->operating_cost_increase_year3 = 0;
			$budgetproperties->operating_cost_increase_year4 = 0;
			$budgetproperties->operating_cost_increase_year5 = 0;

			if($budget && $budget->operating_cost_increase_year1)
				$budgetproperties->operating_cost_increase_year1 = $budget->operating_cost_increase_year1;


			if($budget && $budget->operating_cost_increase_year2)
				$budgetproperties->operating_cost_increase_year2 = $budget->operating_cost_increase_year2;



			if($budget && $budget->operating_cost_increase_year3)
				$budgetproperties->operating_cost_increase_year3 = $budget->operating_cost_increase_year3;



			if($budget && $budget->operating_cost_increase_year4)
				$budgetproperties->operating_cost_increase_year4 = $budget->operating_cost_increase_year4;


			if($budget && $budget->operating_cost_increase_year5)
				$budgetproperties->operating_cost_increase_year5 = $budget->operating_cost_increase_year5;


			$budgetproperties->property_management_increase_year1 = $budgetproperties->net_rent_increase_year1*$budgetproperties->object_management_nk;
			$property_management_increase_year1 = $net_rent_increase_year1*$budgetproperties->object_management_nk;


			$budgetproperties->property_management_increase_year1 =0;

			$budgetproperties->property_management_increase_year2 =0;

			$budgetproperties->property_management_increase_year3 =0;

			$budgetproperties->property_management_increase_year4 =0;

			$budgetproperties->property_management_increase_year5 =0;

			if($budget && $budget->property_management_increase_year1)
				$budgetproperties->property_management_increase_year1 = $budget->property_management_increase_year1;

			if($budget && $budget->property_management_increase_year2)
				$budgetproperties->property_management_increase_year2 = $budget->property_management_increase_year2;
			if($budget && $budget->property_management_increase_year3)
				$budgetproperties->property_management_increase_year3 = $budget->property_management_increase_year3;
			if($budget && $budget->property_management_increase_year4)
				$budgetproperties->property_management_increase_year4 = $budget->property_management_increase_year4;
			if($budget && $budget->property_management_increase_year5)
				$budgetproperties->property_management_increase_year5 = $budget->property_management_increase_year5;

			if($budget){
				$budgetproperties->net_rent_empty_increase_year1 = $budget->net_rent_empty_increase_year1;
				$budgetproperties->net_rent_empty_increase_year2 = $budget->net_rent_empty_increase_year2;
				$budgetproperties->net_rent_empty_increase_year3 = $budget->net_rent_empty_increase_year3;
				$budgetproperties->net_rent_empty_increase_year4 = $budget->net_rent_empty_increase_year4;
				$budgetproperties->net_rent_empty_increase_year5 = $budget->net_rent_empty_increase_year5;
			}

		

		
			$ebitda_year_1 = $total_ccc - $maintenance_increase_year1 - $operating_cost_increase_year1 - $property_management_increase_year1;

			$budgetproperties->ebitda_year_1 = $budgetproperties->net_rent_increase_year1 - $budgetproperties->maintenance_increase_year1 - $budgetproperties->operating_cost_increase_year1 - $budgetproperties->property_management_increase_year1;

			$budgetproperties->ebitda_year_2 = $budgetproperties->net_rent_increase_year2 - $budgetproperties->maintenance_increase_year2 - $budgetproperties->operating_cost_increase_year2 - $budgetproperties->property_management_increase_year2;

			$budgetproperties->ebitda_year_3 = $budgetproperties->net_rent_increase_year3 - $budgetproperties->maintenance_increase_year3 - $budgetproperties->operating_cost_increase_year3 - $budgetproperties->property_management_increase_year3;
			$budgetproperties->ebitda_year_4 = $budgetproperties->net_rent_increase_year4 - $budgetproperties->maintenance_increase_year4 - $budgetproperties->operating_cost_increase_year4 - $budgetproperties->property_management_increase_year4;
			$budgetproperties->ebitda_year_5 = $budgetproperties->net_rent_increase_year5 - $budgetproperties->maintenance_increase_year5 - $budgetproperties->operating_cost_increase_year5 - $budgetproperties->property_management_increase_year5;

			if($budget && $budget->ebitda_year_1)
				$budgetproperties->ebitda_year_1 = $budget->ebitda_year_1;
			if($budget && $budget->ebitda_year_2)
				$budgetproperties->ebitda_year_2 = $budget->ebitda_year_2;
			if($budget && $budget->ebitda_year_3)
				$budgetproperties->ebitda_year_3 = $budget->ebitda_year_3;
			if($budget && $budget->ebitda_year_4)
				$budgetproperties->ebitda_year_4 = $budget->ebitda_year_4;
			if($budget && $budget->ebitda_year_5)
				$budgetproperties->ebitda_year_5 = $budget->ebitda_year_5;

			 
			 $ebit_year_1= $ebitda_year_1 - $budgetproperties->depreciation_nk_money;

			 if(isset($budget->ebit_year_1) && $budget->ebit_year_1)
				$ebit_year_1 = $budget->ebit_year_1;
			 
					
			$budgetproperties->ebit_year_1= $budgetproperties->ebitda_year_1 - $depreciation_nk_money_year1;
			$budgetproperties->ebit_year_2= $budgetproperties->ebitda_year_2 - $depreciation_nk_money_year2;
			$budgetproperties->ebit_year_3= $budgetproperties->ebitda_year_3 - $depreciation_nk_money_year3;
			$budgetproperties->ebit_year_4= $budgetproperties->ebitda_year_4 - $depreciation_nk_money_year4;
			$budgetproperties->ebit_year_5= $budgetproperties->ebitda_year_5 - $depreciation_nk_money_year5;





			$D42 = $budgetproperties->gesamt_in_eur 
				+ ($budgetproperties->real_estate_taxes * $budgetproperties->gesamt_in_eur) 
				+ ($budgetproperties->estate_agents * $budgetproperties->gesamt_in_eur) 
				+ (($budgetproperties->Grundbuch * $budgetproperties->gesamt_in_eur)/100) 
				+ ($budgetproperties->evaluation * $budgetproperties->gesamt_in_eur) 
				+ ($budgetproperties->others * $budgetproperties->gesamt_in_eur) 
				+ ($budgetproperties->buffer * $budgetproperties->gesamt_in_eur);
			$D42 = 0;
			$D46 = $D42;

			$D47 = $budgetproperties->with_real_ek * $D46;	//C47*D46
			$D48 = $budgetproperties->from_bond * $D46;	//C48*D46

			$D49 = $budgetproperties->bank_loan * $D46;


			$D50 = $D47 + $D48 +  $D49;


			if($banks[0] == ''){
			    $bank_check = 0;
			    $bank = $fake_bank;
			}else{
			    $bank_check = 1;
			}

			$H47 = $D49 - $D49 * $budgetproperties->eradication_bank;
			$H48 = $D49 * $budgetproperties->interest_bank_loan;    //G48*D49
			$H49 = $D49 * $budgetproperties->eradication_bank;
			$H50 = $D49 * $budgetproperties->interest_bank_loan + $D49*$budgetproperties->eradication_bank;
			$H52 = $D48 * $budgetproperties->interest_bond;

			$I48 = $H47 * $budgetproperties->interest_bank_loan;
			$I49 = $H50 - $I48;
			$I47 = $H47 - $I49;
			$I50 = $I48 + $I49;

			$J48 = $I47 * $budgetproperties->interest_bank_loan;
			$J49 = $H50 - $J48;
			$J47 = $I47 - $J49;
			$J50 = $J48 + $J49;

			$K48 = $J47 * $budgetproperties->interest_bank_loan;
			$K49 = $I50 - $K48;
			$K47 = $J47 - $K49;
			$K50 = $K48 + $K49;

			$L48 = $K47 * $budgetproperties->interest_bank_loan;
			$L49 = $J50 - $L48;
			$L47 = $K47 - $L49;
			$L50 = $L48 + $L49;

			if(!isset($budgetproperties->depreciation_nk_money_year1) || !$budgetproperties->depreciation_nk_money_year1){
				$budgetproperties->depreciation_nk_money_year1 = 0;
			}
			if(!isset($budgetproperties->depreciation_nk_money_year2) || !$budgetproperties->depreciation_nk_money_year2){
				$budgetproperties->depreciation_nk_money_year2 = 0;
			}
			if(!isset($budgetproperties->depreciation_nk_money_year3) || !$budgetproperties->depreciation_nk_money_year3){
				$budgetproperties->depreciation_nk_money_year3 = 0;
			}
			if(!isset($budgetproperties->depreciation_nk_money_year4) || !$budgetproperties->depreciation_nk_money_year4){
				$budgetproperties->depreciation_nk_money_year4 = 0;
			}
			if(!isset($budgetproperties->depreciation_nk_money_year5) || !$budgetproperties->depreciation_nk_money_year5){
				$budgetproperties->depreciation_nk_money_year5 = 0;
			}

			$E18 = ($budgetproperties->net_rent_increase_year1
			-$budgetproperties->maintenance_increase_year1
			-$budgetproperties->operating_cost_increase_year1
			-$budgetproperties->property_management_increase_year1)
			-$budgetproperties->depreciation_nk_money_year1;

			$F18 = ($budgetproperties->net_rent_increase_year2
			-$budgetproperties->maintenance_increase_year2
			-$budgetproperties->operating_cost_increase_year2
			-$budgetproperties->property_management_increase_year2)
			-$budgetproperties->depreciation_nk_money_year2;

			$G18 = ($budgetproperties->net_rent_increase_year3
			-$budgetproperties->maintenance_increase_year3
			-$budgetproperties->operating_cost_increase_year3
			-$budgetproperties->property_management_increase_year3)
			-$budgetproperties->depreciation_nk_money_year3;

			$H18 = 
			($budgetproperties->net_rent_increase_year4
			-$budgetproperties->maintenance_increase_year4
			-$budgetproperties->operating_cost_increase_year4
			-$budgetproperties->property_management_increase_year4)
			-$budgetproperties->depreciation_nk_money_year4;

			$I18 = ($budgetproperties->net_rent_increase_year5
			-$budgetproperties->maintenance_increase_year5
			-$budgetproperties->operating_cost_increase_year5
			-$budgetproperties->property_management_increase_year5)
			-$budgetproperties->depreciation_nk_money_year5;

			$E23 = $E18- $H48 -$H52;
			$F23 = $F18-($H52)-($I48);
			$G23 = $G18-($H52)-($J48);
			$H23 = $H18 - ($H52)-($K48);
			$I23 = $I18-($H52)-($L48);

			$E31 = ($E23 - ($budgetproperties->tax * $E23)) - $H49 + $budgetproperties->depreciation_nk_money;

			$I16= $budgetproperties->depreciation_nk_money_year5;
			$I27 =$I23 - ($budgetproperties->tax * $I23);
			$I29 = $L49;					


			$I31 = $I27-$I29+$I16;

			$G58 = ($D42 == 0) ? 0: $budgetproperties->net_rent_pa/$D42;
			$G59 = ($budgetproperties->net_rent_pa == 0) ? 0 : $D42/ $budgetproperties->net_rent_pa;
			$E25 = $budgetproperties->tax * $E23;
			$G61 = ($D48 == 0) ? 0 : $E31/$D48;
			$G62 = ($D48 == 0) ? 0 : $E23/$D48;



			$L14 = ($budgetproperties->rent_retail + $budgetproperties->rent_whg == 0) ? 0 : ($budgetproperties->net_rent_pa / ($budgetproperties->rent_retail + $budgetproperties->rent_whg) / 12);
			$L16 = ($budgetproperties->rent_retail + $budgetproperties->vacancy_retail + $budgetproperties->rent_whg + $budgetproperties->vacancy_whg == 0) ? 0 : 
			($budgetproperties->gesamt_in_eur)/($budgetproperties->rent_retail + $budgetproperties->vacancy_retail + $budgetproperties->rent_whg + $budgetproperties->vacancy_whg);
			$L20 = ($budgetproperties->rent_retail + $budgetproperties->vacancy_retail + $budgetproperties->rent_whg + $budgetproperties->vacancy_whg == 0) ? 0 : 
			($budgetproperties->rent_retail + $budgetproperties->rent_whg)/($budgetproperties->rent_retail + $budgetproperties->vacancy_retail + $budgetproperties->rent_whg + $budgetproperties->vacancy_whg);

			$L25 = ($budgetproperties->net_rent_pa == 0) ? 0 : $budgetproperties->gesamt_in_eur / $budgetproperties->net_rent_pa;


			$L11 = $budgetproperties->plot_of_land_m2;
			$P31 = $budgetproperties->ground_reference_value_in_euro_m2;
			$P43 = $L11* $P31;
			$D59 = $budgetproperties->gesamt_in_eur*($budgetproperties->maintenance_nk);
			$D60 = $budgetproperties->operating_cost_increase_year1;

		?>

		<div class="property-details">
			<table class="property-table einkauf-prop-table budget-table">
				<tbody>
					<tr>
						<td colspan="3"></td>
						<th class="border bg-gray text-center">Stand Ankauf</th>
						<th class="border bg-gray text-center">Jahr 1</th>
						<th class="border bg-gray text-center">Jahr 2</th>
						<th class="border bg-gray text-center">Jahr 3</th>
						<th class="border bg-gray text-center">Jahr 4</th>
						<th class="border bg-gray text-center">Jahr 5</th>
						<td></td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="">&nbsp;</td>
						
					</tr>

					<tr>
						<th class="border bg-gray">Netto Miete (IST) p.a.</th>
						<td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" data-title="{{__('property.field.net_rent')}} (%)">{{number_format($budgetproperties->net_rent*100,2,",",".")}}</a><span>%</span></td>
						<th class="border bg-gray">Steigerung p.a.</th>
						<th class="border bg-gray text-right">{{number_format($fixbudgetproperties->net_rent_increase_year1,2,",",".")}}</th>
						<th class="border bg-gray text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_increase_year1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->net_rent_increase_year1,2,",",".")}}</a></th>
						<th class="border bg-gray text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_increase_year2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->net_rent_increase_year2,2,",",".")}}</a></th>
						<th class="border bg-gray text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_increase_year3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->net_rent_increase_year3,2,",",".")}}</a></th>
						<th class="border bg-gray text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_increase_year4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->net_rent_increase_year4,2,",",".")}}</a></th>
						<th class="border bg-gray text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_increase_year5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->net_rent_increase_year5,2,",",".")}}</a></th>
					</tr>

					<tr>
						<td class="border">Netto Miete (Soll) Leerst.</td>
						<td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_empty" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" data-title="{{__('property.field.net_rent_empty')}}(%)">{{number_format($fixbudgetproperties->net_rent_empty*100,2,",",".")}}</a>%</td>
						<td class="border">Steigerung p.a.</td>
						<td class="border text-right">{{number_format(0,2,",",".")}}</td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_empty_increase_year1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format(( (isset($budgetproperties->net_rent_empty_increase_year1)) ? $budgetproperties->net_rent_empty_increase_year1 : 0 ),2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_empty_increase_year2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format(( (isset($budgetproperties->net_rent_empty_increase_year2)) ? $budgetproperties->net_rent_empty_increase_year2 : 0 ),2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_empty_increase_year3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format(( (isset($budgetproperties->net_rent_empty_increase_year3)) ? $budgetproperties->net_rent_empty_increase_year3 : 0 ),2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_empty_increase_year4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format(( (isset($budgetproperties->net_rent_empty_increase_year4)) ? $budgetproperties->net_rent_empty_increase_year4 : 0 ),2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_empty_increase_year5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format(( (isset($budgetproperties->net_rent_empty_increase_year5)) ? $budgetproperties->net_rent_empty_increase_year5 : 0 ),2,",",".")}}</a></td>
						<td>&nbsp;</td>
						
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td class="border">Instandhaltung nichtumlfähig</td>
						<td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" data-title="{{__('property.field.maintenance')}} (%)">{{number_format($budgetproperties->maintenance*100,2,",",".")}}</a>%</td>
						<td class="border">Steigerung p.a.</td>
						<td class="border text-right">{{number_format($fixbudgetproperties->maintenance_increase_year1,2,",",".")}}</td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance_increase_year1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->maintenance_increase_year1,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance_increase_year2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->maintenance_increase_year2,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance_increase_year3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->maintenance_increase_year3,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance_increase_year4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->maintenance_increase_year4,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance_increase_year5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->maintenance_increase_year5,2,",",".")}}</a></td>

					</tr>

					<tr>
						<td class="border">Betriebsk. nicht umlfähig</td>
						<td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="operating_costs" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" data-title="{{__('property.field.operating_costs')}} (%)">{{number_format($budgetproperties->operating_costs*100,2,",",".")}}</a>%</td>
						<td class="border">Steigerung p.a.</td>
						<td class="border text-right">{{number_format($fixbudgetproperties->operating_cost_increase_year1,2,",",".")}}</td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="operating_cost_increase_year1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->operating_cost_increase_year1,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="operating_cost_increase_year2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->operating_cost_increase_year2,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="operating_cost_increase_year3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->operating_cost_increase_year3,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="operating_cost_increase_year4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->operating_cost_increase_year4,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="operating_cost_increase_year5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->operating_cost_increase_year5,2,",",".")}}</a></td>
					</tr>

					<tr>
						<td class="border">Objektverwalt. nichtumlfähig</td>
						<td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="object_management" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" data-title="{{__('property.field.object_management')}} (%)">{{number_format($budgetproperties->object_management*100,2,",",".")}}</a>%
						</td>
						<td class="border">Steigerung p.a.</td>
						<td class="border text-right">{{number_format($fixbudgetproperties->property_management_increase_year1,2,",",".")}}</td>

						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="property_management_increase_year1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->property_management_increase_year1,2,",",".")}}</a></td>

						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="property_management_increase_year2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->property_management_increase_year2,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="property_management_increase_year3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->property_management_increase_year3,2,",",".")}}</a></td>
						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="property_management_increase_year4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->property_management_increase_year4,2,",",".")}}</a></td>

						<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="property_management_increase_year5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->property_management_increase_year5,2,",",".")}}</a></td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<th colspan="3" class="border bg-gray">EBITDA</th>
						<th class="border bg-gray text-right">{{number_format($fixbudgetproperties->ebitda_year_1,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebitda_year_1,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebitda_year_2,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebitda_year_3,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebitda_year_4,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebitda_year_5,2,",",".")}}</th>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td colspan="3" class="border">Abschreibung</td>
						<td class="border text-right">{{number_format($fixbudgetproperties->depreciation_nk_money,2,",",".")}}</td>
						<?php $budgetproperties->depreciation_nk_money =  0; ?>
						<td class="border text-right">
							@if($depreciation_nk_money_year1)
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($depreciation_nk_money_year1,2,",",".")}}</a>
							@else
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->depreciation_nk_money,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($depreciation_nk_money_year2)
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($depreciation_nk_money_year2,2,",",".")}}</a>
							@else
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->depreciation_nk_money,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($depreciation_nk_money_year3)
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($depreciation_nk_money_year3,2,",",".")}}</a>
							@else
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->depreciation_nk_money,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($depreciation_nk_money_year4)
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($depreciation_nk_money_year4,2,",",".")}}</a>
							@else
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->depreciation_nk_money,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($depreciation_nk_money_year5)
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($depreciation_nk_money_year5,2,",",".")}}</a>
							@else
								<a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk_money_year5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($budgetproperties->depreciation_nk_money,2,",",".")}}</a>
							@endif
						</td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<th colspan="3" class="border bg-gray">EBIT</th>
						<th class="border bg-gray text-right">{{number_format($fixbudgetproperties->ebit_year_1,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebit_year_1,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebit_year_2,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebit_year_3,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebit_year_4,2,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($budgetproperties->ebit_year_5,2,",",".")}}</th>
						<th>&nbsp;</th>
					</tr>

					<?php
						$EBT1 = $budgetproperties->ebit_year_1 - $bank_interest_year_1 - $interest_rate_bond_year_1;
						$EBT2 = $budgetproperties->ebit_year_2 - $bank_interest_year_2 - $interest_rate_bond_year_2;
						$EBT3 = $budgetproperties->ebit_year_3 - $bank_interest_year_3 - $interest_rate_bond_year_3;
						$EBT4 = $budgetproperties->ebit_year_4 - $bank_interest_year_4 - $interest_rate_bond_year_4;
						$EBT5 = $budgetproperties->ebit_year_5 - $bank_interest_year_5 - $interest_rate_bond_year_5;
					?>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td colspan="3" class="border">Zins Bankkredit</td>
						<td class="border text-right">{{number_format($fixH48,2,",",".")}}</td>
						<?php $H48 = $I48 = $J48 = $K48 = $L48= 0; ?>
						<td class="border text-right">
							@if($bank_interest_year_1)
                                <a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($bank_interest_year_1,2,",",".")}}</a>
                            @else
                                <a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >
								{{number_format($H48,2,",",".")}}
							@endif
						</td>
						<td class="border text-right">
							@if($bank_interest_year_2)
                             	<a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($bank_interest_year_2,2,",",".")}}</a>
                            @else
                                <a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($I48,2,",",".")}}
                            @endif
                       	</td>
						<td class="border text-right">
							@if($bank_interest_year_3)
                            	<a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($bank_interest_year_3,2,",",".")}}</a>
                          	@else
                          		<a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($J48,2,",",".")}}@endif
                        </td>
						<td class="border text-right">
							@if($bank_interest_year_4)
                            	<a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($bank_interest_year_4,2,",",".")}}</a>
                            @else
                            	<a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($K48,2,",",".")}}
                            @endif
                       	</td>
						<td class="border text-right">
							@if($bank_interest_year_5)
                            	<a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($bank_interest_year_5,2,",",".")}}</a>
                            @else
                             	<a href="#" class="inline-edit" data-type="text" data-pk="bank_interest_year_5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >
								{{number_format($L48,2,",",".")}}@endif
						</td>
					</tr>
						
					<tr>
						<th colspan="3" class="border">Zins Anleihe</th>
						<td class="border text-right">{{number_format($fixH52,2,",",".")}}</td>
						<?php $H52 = 0; ?>
						<td class="border text-right">
							@if($interest_rate_bond_year_1)
                            	<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($interest_rate_bond_year_1,2,",",".")}}</a>
                            @else
                           		<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($H52,2,",",".")}}
                           	@endif
                        </td>
						<td class="border text-right">
							@if($interest_rate_bond_year_2)
                            	<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($interest_rate_bond_year_2,2,",",".")}}</a>
                            @else
                            	<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($H52,2,",",".")}}
                            @endif
                        </td>
						<td class="border text-right">
							@if($interest_rate_bond_year_3) 
								<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($interest_rate_bond_year_3,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($H52,2,",",".")}}
							@endif
						</td>
						<td class="border text-right">
							@if($interest_rate_bond_year_4) 
								<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($interest_rate_bond_year_4,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($H52,2,",",".")}}
							@endif
						</td>
						<td class="border text-right">
							@if($interest_rate_bond_year_5) 
								<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($interest_rate_bond_year_5,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="interest_rate_bond_year_5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($H52,2,",",".")}}
							@endif
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<th colspan="3" class="border bg-gray">EBT</th>
						<th class="border text-right bg-gray">{{number_format($fixE18 - $fixH48 - $fixH52 ,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EBT1,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EBT2,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EBT3,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EBT4,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EBT5,2,",",".")}}</th>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td class="border">Steuern</td>
						<td class="border text-right bg-yellow">
							<a href="#" class="inline-edit" data-type="text" data-pk="tax" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" data-title="{{__('property.field.Tax')}} (%)">{{number_format($budgetproperties->tax*100,2,",",".")}}</a>%
						</td>
						<td class="border"></td>
						<td class="border text-right">{{number_format($fixE25,2,",",".")}}</td>
						<?php $E25 = 0;?>
						<td class="border text-right">
							@if($tax_year_1) 
								<a href="#" class="inline-edit" data-type="text" data-pk="tax_year_1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($tax_year_1,2,",",".")}}</a> 
							@else <a href="#" class="inline-edit" data-type="text" data-pk="tax_year_1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($E25,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($tax_year_2) 
								<a href="#" class="inline-edit" data-type="text" data-pk="tax_year_2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($tax_year_2,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="tax_year_2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format(0,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($tax_year_3) 
								<a href="#" class="inline-edit" data-type="text" data-pk="tax_year_3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($tax_year_3,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="tax_year_3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format(0,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($tax_year_4) 
								<a href="#" class="inline-edit" data-type="text" data-pk="tax_year_4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($tax_year_4,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="tax_year_4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format(0,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($tax_year_5) 
								<a href="#" class="inline-edit" data-type="text" data-pk="tax_year_5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($tax_year_5,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="tax_year_5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format(0,2,",",".")}}</a>
							@endif
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<?php
						$EAT1 = $EBT1 - $tax_year_1;
						$EAT2 = $EBT2 - $tax_year_2;
						$EAT3 = $EBT3 - $tax_year_3;
						$EAT4 = $EBT4 - $tax_year_4;
						$EAT5 = $EBT5 - $tax_year_5;

						$CF1 = $EAT1 - $redemption_bank_year_1 +  $depreciation_nk_money_year1;
						$CF2 = $EAT2 - $redemption_bank_year_2 +  $depreciation_nk_money_year2;
						$CF3 = $EAT3 - $redemption_bank_year_3 +  $depreciation_nk_money_year3;
						$CF4 = $EAT4 - $redemption_bank_year_4 +  $depreciation_nk_money_year4;
						$CF5 = $EAT5 - $redemption_bank_year_5 +  $depreciation_nk_money_year5;
					?>

					<tr>
						<th colspan="3" class="border bg-gray">EAT</th>
						<th class="border text-right bg-gray">{{number_format($fixE23 - ($budgetproperties->tax * $fixE23),2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EAT1,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EAT2,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EAT3,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EAT4,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($EAT5,2,",",".")}}</th>
					</tr>

					{{--TODO: start from here--}}
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td colspan="3" class="border">Tilgung Bank</td>
						<td class="border text-right">{{number_format($fixH49,2,",",".")}}</td>
						<?php $H49 = $I49 = $J49 =  $K49 = $L49 = 0; ?>
						<td class="border text-right">
							@if($redemption_bank_year_1) 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($redemption_bank_year_1,2,",",".")}}</a>
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_1" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($H49,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($redemption_bank_year_2) 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($redemption_bank_year_2,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_2" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($I49,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($redemption_bank_year_3) 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($redemption_bank_year_3,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_3" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($J49,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($redemption_bank_year_4) 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($redemption_bank_year_4,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_4" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($K49,2,",",".")}}</a>
							@endif
						</td>
						<td class="border text-right">
							@if($redemption_bank_year_5) 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($redemption_bank_year_5,2,",",".")}}</a> 
							@else 
								<a href="#" class="inline-edit" data-type="text" data-pk="redemption_bank_year_5" data-url="{{url('property/update_property_budget/'.$budgetproperties->main_property_id) }}" >{{number_format($L49,2,",",".")}}</a>
							@endif
						</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<th colspan="3" class="border bg-gray">Cashflow (nach Steuern)</th>
						<th class="border text-right bg-gray">
							{{number_format(($fixE23 - ($budgetproperties->tax * $fixE23)) - $fixH49 + $fixbudgetproperties->depreciation_nk_money,2,",",".")}} 
						</th>
						<th class="border text-right bg-gray">{{number_format($CF1,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($CF2,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($CF3,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($CF4,2,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($CF5 ,2,",",".")}}</th>
					</tr>

				</tbody>
			</table>
		</div>

  	</div>
</div>

@endsection

@section('js')
	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{asset('js/property/budget.js')}}"></script>
@endsection