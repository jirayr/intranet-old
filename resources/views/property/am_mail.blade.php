@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
@endsection

@section('content')

@include('property.partial.tab')

{{-- <div class="tab-content">
  	<div id="am_mail" class="tab-pane fade in active">
  		
  		<ul class="nav nav-tabs">
		  	<li class="active"><a data-toggle="tab" href="#mail_an_am">Mail an AM</a></li>
		  	<li><a data-toggle="tab" href="#mail_a_hv_bu">Mail an HV BU</a></li>
		    <li><a data-toggle="tab" href="#mail_a_hv_pm">Mail an HV PM</a></li>
		    <li><a data-toggle="tab" href="#mail_an_ek">Mail an EK</a></li>
		  	<li><a data-toggle="tab" href="#mail_an_user">Mail an User</a></li>
		</ul>

		<div class="tab-content">
		  	<div id="mail_an_am" class="tab-pane fade in active">
		    	@include('properties.templates.am_mail_logs')
		  	</div>
		  	<div id="mail_a_hv_bu" class="tab-pane fade">
		    	@include('properties.templates.hv_bu_mail_logs')
		  	</div>
		  	<div id="mail_a_hv_pm" class="tab-pane fade">
		    	@include('properties.templates.hv_pm_mail_logs')
		  	</div>
		    <div id="mail_an_ek" class="tab-pane fade">
		      @include('properties.templates.ek_mail_logs')
		    </div>
		    <div id="mail_an_user" class="tab-pane fade">
		      @include('properties.templates.user_mail_logs')
		    </div>
		</div>

  	</div>
</div> --}}

<div class="col-sm-12 table-responsive white-box">
  <table class="table table-striped" id="am_mail_table">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Nachricht</th>
        <th>Datum/Uhrzeit</th>
      </tr>
    </thead>
    <tbody>
      @if(!empty($assetsManagerMails))
        @php
          $count = 1;
        @endphp
        @foreach ($assetsManagerMails as $key => $value)
            <tr>
              <td>{{ $count }}</td>
              <td>
                  
                  @if($value->type == 5)

                    <?php 
                      $email_arr = [];
                      if($value->custom_email){
                        $arr = explode(",", $value->custom_email);
                        $arr = array_unique($arr);
                        $email_arr = $arr;
                      }
                    ?>
                    @if(!empty($email_arr))
                      {{ implode(", ", $email_arr) }}
                    @endif

                  @elseif($value->type == 6)

                    {{ $value->name }}{{ ($value->custom_email) ? ', '.$value->custom_email : '' }}

                  @else

                    {{ $value->name }}

                  @endif

              </td>
              <td><?php echo $value->message; ?></td>
              <td>{{ show_datetime_format($value->created_at) }}</td>
            </tr>
            @php
              $count++;
            @endphp
        @endforeach
      @endif
    </tbody>
  </table>
</div>

@endsection

@section('js')
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{ asset('js/custom-datatable.js') }}"></script>
  	<script type="text/javascript">
  		var columns = [
	        null,
	        null,
	        null, 
	        {"type": "new-date-time"}
	    ];
	    makeDatatable($('#am_mail_table'), columns, 'desc', 3);
	    // makeDatatable($('#hv_bu_mail_table'), columns, 'desc', 3);
	    // makeDatatable($('#hv_pm_mail_table'), columns, 'desc', 3);
	    // makeDatatable($('#ek_mail_table'), columns, 'desc', 3);
	    // makeDatatable($('#user_mail_table'), columns, 'desc', 3);
  	</script>
@endsection