@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
	<style type="text/css">
      .change-in-user{
        width: 80px !important;
      }
      
    </style>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="Leerstandsflächen" class="tab-pane fade in active">
  		
  		<div id="tenancy-schedule">
    		<h1>Leerstandsflächen</h1>
			
			<?php
				$name = $properties->name_of_property;
				$seller_id = $properties->seller_id;
				$ireleased_by = "";
				$isale_date = "";
				$ireleased_by2 = "";
				$isale_date2 = "";
				$street = "";
				$postcode = "";
				$city = "";

				if($pb_data && $pb_data->property_name)
				  	$name = $pb_data->property_name;

				$seller_id = $properties->transaction_m_id;
				if($pb_data && $pb_data->saller_id)
				  	$seller_id = $pb_data->saller_id;

				$street = $properties->strasse;
				if($pb_data && $pb_data->street)
				  	$street = $pb_data->street;

				if($pb_data && $pb_data->city)
				  	$city = $pb_data->city;

				if($pb_data && $pb_data->postcode)
				  	$postcode = $pb_data->postcode;

				if($pb_data && $pb_data->released_by)
				  	$ireleased_by = $pb_data->released_by;

				if($pb_data && $pb_data->sale_date)
				  	$isale_date = $pb_data->sale_date;


				if($pb_data && $pb_data->released_by2)
				  	$ireleased_by2 = $pb_data->released_by2;

				if($pb_data && $pb_data->sale_date2)
				  	$isale_date2 = $pb_data->sale_date2;

			?>

 
			<input type="hidden" class="property_id" value="<?php echo $id?>">
			<div class="row tenancy-schedules lbuy-sheet" style="margin-top: 20px;">
			  	<div class="col-sm-12">
				 	@php 
						$check = false;
				 	@endphp

					@if(isset($tenancy_schedule_data))
						@foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
							@foreach($tenancy_schedule->items as $item)
								@if($item->type == 3 || $item->type == 4)
									@php
										$check = true;
									@endphp

	    							<h3 class="title-tag">
	  
		           						@php

		            						$immo_upload =  DB::table('immo_upload')->where('property_id', $item->id)->first();
		            
							           		if(isset($immo_upload->property_id)){
							              		if($immo_upload->status == 'Active'){
							                  		$class ="color:green;";
							              		}else{
							                  		$class ="color:yellow;";
							              		}
							           		}else{
							                	$class ="color:red;";
							           		}
							          	@endphp

		        						<i class="fa fa-circle" style="{{$class}}" aria-hidden="true"></i>

									    @if(is_null($item->name))
									      	empty
									    @else
									    	{{ $item->name }}  
									    @endif

		        						<a style="margin-left: 10px;" href="javascript:void(0)" data-class="sheetcustom-{{ $item->id }} " class="btn btn-info show-links pull-right">Vermietungsaktivitäten  <i class="fa fa-angle-down"></i></a>

		        						<a href="javascript:void(0)" data-class="sheet-{{ $item->id }} " class="btn btn-info show-links pull-right">IS Upload <i class="fa fa-angle-down"></i></a>

	    							</h3>

									<div class="sheet-{{ $item->id }}  hidden"> 
									  	@include('properties.templates.Immobilie')
									</div>


	  								<div class="customvacancy sheetcustom-{{ $item->id }}  hidden" data-url="{{ route('vacant_uploading_statuse', ['vacant_id' => $item->id]) }}"> 
	  									@include('properties.templates.customvacancy')
	  								</div>

	    							<hr>
								@endif
							@endforeach
						@endforeach  
					@else
						Keine Daten auf dieser Registerkarte verfügbar
					@endif   
	  

					@if($check ==  false)
						<h3>Keine Leerstandsflächen gefunden.</h3>
					@endif
  				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="irelease-property" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title"></h4>
		      </div>
		      <div class="modal-body">
		        <h4>Möchtest du dieses Objekt wirklich freigeben?</h4>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-primary irelease-the-property" data-dismiss="modal" >Ja</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		      </div>
		    </div>

		  </div>
		</div>

		<div class="modal fade" id="irelease-property2" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title"></h4>
		      </div>
		      <div class="modal-body">
		        <h4>Möchtest du dieses Objekt wirklich freigeben?</h4>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-primary irelease-the-property2" data-dismiss="modal" >Ja</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		      </div>
		    </div>

		  </div>
		</div>

  	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var _token  							= '{{ csrf_token() }}';
		var url_einkauf_item_update 			= '{{ url('einkauf-item/update') }}';
		var url_uploadfiles           			= '{{ route('uploadfiles') }}';
		var url_get_rental_activity_comment   	= '{{ route('get_rental_activity_comment') }}';
	</script>
	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{asset('js/property/leerstandsflächen.js')}}"></script>
@endsection