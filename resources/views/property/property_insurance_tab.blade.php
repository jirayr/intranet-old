@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="property_insurance_tab" class="tab-pane fade in active">
  		
  		<div class="row">
			<div class="col-md-12">
				<span id="insurance_tab_msg"></span>
			</div>

			<div class="col-md-12">
				<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add_insurance_tab_title_modal">Neue</button>
			</div>

		</div>

		<div id="insurance-table-content">
			<h3>Offene Angebote</h3>
			@if (isset($insurance_tab_title) && count($insurance_tab_title) > 0)
				@foreach ($insurance_tab_title as $key => $value)
					<?php
						$value->details = DB::table('property_insurance_tab_details')->where('property_insurance_tab_id', $value->id)->where('deleted',0)->get()->count();

						$detail = DB::table('property_insurance_tab_details as pid')
	                            	->select('pid.id',
	                                	DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurancetab_release' AND selected_id = pid.id AND tab='insurancetab2' ) THEN 1 ELSE 0 END as is_release")
	                            	)
	                            	->where('property_insurance_tab_id', $value->id)
	                            	->where('pid.deleted', 0)
	                            	->havingRaw('is_release=1')
	                            	->orderBy('id', 'DESC')
	                            	->get();
	                	$value->release2 = count($detail);
					?>

					@if( ( $value->release_status==0 && $value->release2!=$value->details ) || $value->details==0)
						<div class="row main_card" style="margin-top: -30px;">
							<div class="col-md-12">
								<div class="modal-dialog modal-lg" style="width: 100%">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title">{{ $value->title }}</h4>
										</div>
										<div class="modal-body sec-{{ $value->id }}">
											<div class="row ">
												<div class="col-md-1">
													<button type="button" class="btn btn-success btn-xs btn-insurance-tab-detail uptetsntse" data-tabid="{{ $value->id }}">Hinzufügen</button>
													
												</div>
												<div class="col-md-2 ins-btn-{{ $value->id }}"></div>
												<div class="col-md-1">
													<button type="button" class="btn btn btn-xs btn-primary delete-title" data-url="{{ route('delete_property_insurance_title', ['id' => $value->id]) }}">Delete</button>
												</div>
												<div class="col-md-12 table-responsive">
													<table class="table table-striped tbl-insurance-tab-detail" data-tabid="{{ $value->id }}" id="insurance-table-{{ $value->id }}">
														<thead>
															<tr>
																<th>#</th>
																<th>Datei</th>
																<th>Betrag</th>
																<th>Kommentar</th>
																<th>User</th>
																<th>Datum</th>
																<th>Falk Kommantare</th>
																<th>Empfehlung</th>
																<th>Freigeben</th>
																<th>Ablehnen</th>
																<th>Action</th>
															</tr>
														</thead>
														<tbody>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endif
				@endforeach
			@endif
		</div>

		<h3>Von Falk freigegeben</h3>
		@if (isset($insurance_tab_title) && count($insurance_tab_title) > 0)
			@foreach ($insurance_tab_title as $key => $value)
				@if( $value->details && ( $value->details==$value->release2 || ($value->release_status==1) ) )
					<div class="row" style="margin-top: -30px;">
						<div class="col-md-12">
							<div class="modal-dialog modal-lg" style="width: 100%">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title">{{ $value->title }}</h4>
									</div>
									<div class="modal-body">
										<div class="row ">
											<div class="col-md-3"></div>
											<div class="col-md-12 table-responsive">
												<table class="table table-striped tbl-insurance-tab-detail" data-tabid="{{ $value->id }}" id="insurance-table-{{ $value->id }}">
													<thead>
														<tr>
															<th>#</th>
															<th>Datei</th>
															<th>Betrag</th>
															<th>Kommentar</th>
															<th>User</th>
															<th>Datum</th>
															<th>Falk Kommantare</th>
															<th>Empfehlung</th>
															<th>Freigeben</th>
															<th>Ablehnen</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
			@endforeach
		@endif

		<h3>Verlauf</h3>
		<div class="row" style="margin-top: -30px;">
			<div class="col-md-12">
				<div class="modal-dialog modal-lg" style="width: 100%">
					<div class="modal-content">
						{{-- <div class="modal-header">
							<h4 class="modal-title">Title</h4>
						</div> --}}
						<div class="modal-body">
							<div class="row ">
								<div class="col-md-12 table-responsive">
									<table class="table table-striped" id="insurance-tab-log">
										<thead>
											<tr>
												<th>#</th>
												<th>Title</th>
												<th>Name</th>
												<th>Button</th>
												<th>Datei</th>
												<th>Kommentar</th>
												<th>Datum</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="insurance-tab-hidden-html" style="display: none;">
			<div class="row main_card">
				<div class="col-md-12">
					<div class="modal-dialog modal-lg" style="width: 100%">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">##TITLE##</h4>
							</div>
							<div class="modal-body sec-##ID##">
								<div class="row ">

									<div class="col-md-1">
										<button type="button" class="btn btn-success btn-xs btn-insurance-tab-detail uptetsntse" data-tabid="##ID##">Hinzufügen</button>
									</div>
									<div class="col-md-2 ins-btn-##ID##"></div>
									<div class="col-md-1">
										<button type="button" class="btn btn btn-xs btn-primary delete-title" data-url="##TITLE-DELETE-URL##">Delete</button>
									</div>
									<div class="col-md-12 table-responsive">
										<table class="table table-striped tbl-insurance-tab-detail hidden-table" data-tabid="##ID##" id="insurance-table-##ID##">
											<thead>
												<tr>
													<th>#</th>
													<th>Datei</th>
													<th>Betrag</th>
													<th>Kommentar</th>
													<th>User</th>
													<th>Datum</th>
													<th>Falk Kommantare</th>
													<th>Empfehlung</th>
													<th>Falk</th>
													<th>Ablehnen</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class=" modal fade" role="dialog" id="add_insurance_tab_title_modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<form method="post" id="form_add_insurance_tab_title" action="{{ route('add_property_insurance_title', ['property_id' => $properties->id]) }}">
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<span id="add_insurance_tab_title_msg"></span>
								</div>
								<div class="col-md-12">
									<label>Titel</label>
									<input type="text" name="title" class="form-control" required="">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" >Speichern</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
						</div>
					</form>
				</div>

			</div>
		</div>

		<div class=" modal fade" role="dialog" id="add_insurance_tab_detail_modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"></h4>
					</div>
					<form method="post" id="form_add_insurance_tab_detail" action="">
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12">
									<span id="add_insurance_tab_detail_msg"></span>
								</div>
								<input type="hidden" id="property_insurance_tab_id" name="property_insurance_tab_id">

								<div class="col-md-12">
									<input type="hidden" name="file_basename" id="insurance_tab_gdrive_file_basename" value="">
									<input type="hidden" name="file_dirname" id="insurance_tab_gdrive_file_dirname" value="">
									<input type="hidden" name="file_type" id="insurance_tab_gdrive_file_type" value="">
									<input type="hidden" name="file_name" id="insurance_tab_gdrive_file_name" value="">
									<input type="hidden" name="current_tab_name" id="current_tab_name_insurance_tab" value="Angebote">

									<label>Datei</label> <span id="insurance_tab_gdrive_file_name_span"> </span>
									<a href="javascript:void(0);" class="link-button-gdrive-insurance-tab btn btn-info">Datei auswählen</a>


				                    <a class="insurance-tab-upload-file-icon btn btn-info" href="javascript:void(0);">Hochladen</a>
				                    <input type="file" name="insurance_tab_gdrive_file_upload" class="hide insurance-tab-gdrive-upload-file-control" id="insurance_tab_gdrive_file_upload" >
									<br><br>
								</div>

								{{-- <div class="col-md-12">
									<label>Name</label>
									<input type="text" name="name" class="form-control" required="">
								</div> --}}
								<div class="col-md-12">
									<label>Betrag</label>
									<input type="text" name="amount" class="form-control mask-input-number" required="">
								</div>
								<div class="col-md-12">
									<label>Kommentar</label>
									<textarea name="comment" class="form-control"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" >Speichern</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
						</div>
					</form>
				</div>

			</div>
		</div>

		<div class="modal fade" id="property-insurance-release-property-modal" role="dialog">
		   <div class="modal-dialog">
		      <!-- Modal content-->
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		         </div>
		         <div class="modal-body">
		            <label>Kommentar</label>
		            <textarea class="form-control property-insurance-comment" name="message"></textarea>
		            <br>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-primary property-insurance-submit" data-dismiss="modal" >Senden</button>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		         </div>
		      </div>
		   </div>
		</div>

		<div class="modal fade" id="deal-not-release-modal" role="dialog">
		   <div class="modal-dialog">
		      <!-- Modal content-->
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		         </div>
		         <div class="modal-body">
		            <label>Kommentar</label>
		            <textarea class="form-control deal-not-release-comment" name="message"></textarea>
		            <br>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-primary deal-not-release-submit" data-dismiss="modal" >Senden</button>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		         </div>
		      </div>
		   </div>
		</div>

		<div class="modal fade" id="insurance-not-release-modal" role="dialog">
		   <div class="modal-dialog">
		      <!-- Modal content-->
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		         </div>
		         <div class="modal-body">
		            <label>Kommentar</label>
		            <textarea class="form-control insurance-not-release-comment" name="message"></textarea>
		            <br>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-primary insurance-not-release-submit" data-dismiss="modal" >Senden</button>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		         </div>
		      </div>
		   </div>
		</div>

		@include('property.partial.comman_modal')

  	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var _token                					= '{{ csrf_token() }}';
		var workingDir 								= '{{ $workingDir }}';
		var current_property_id 					= '{{ $properties->id }}';
		var is_falk									= {{ ( Auth::user()->email == config('users.falk_email') ) ? 1 : 0 }};

		var lfm_route    		  					= '{{ url("file-manager") }}';
		var url_get_property_insurance_detail 		= '{{ route("get_property_insurance_detail", ["tab_id" => ":tab_id"]) }}';
		var url_add_property_insurance_detail 		= '{{ route('add_property_insurance_detail') }}';
		var url_get_property_insurance_detail_log	= '{{ route('get_property_insurance_detail_log', ['property_id' => $properties->id]) }}';
		var url_delete_property_insurance_title 	= '{{ route("delete_property_insurance_title", ["id" => ":id"]) }}';
		var url_get_angebot_button 					= '{{ route('get_angebot_button') }}';
		var url_property_dealreleaseprocedure 		= '{{ url('property/dealreleaseprocedure') }}';
		var url_deal_mark_as_notrelease 			= '{{ route('deal_mark_as_notrelease') }}';
		var url_insurance_mark_as_notrelease  		= '{{ route('insurance_mark_as_notrelease') }}';
	</script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
  	<script src="{{ asset('file-manager/js/bootbox.min.js') }}"></script>
	<script src="{{ asset('file-manager/js/script.js') }}"></script>
	<script src="{{ asset('js/custom-datatable.js') }}"></script>
  	<script src="{{asset('js/property/property_insurance_tab.js')}}"></script>
@endsection