@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="swot-template" class="tab-pane fade in active">

	  	<div class="white-box table-responsive" style="height: 600px; overflow: auto;">

			<table class="table" style="width: 100%">
				<tbody>
					<tr>
						<th>Stärken (Strengths)</th>
						<th>Schwächen (Weaknesses)</th>
					</tr>
					<tr style="text-align: center; font-size: 24px; height: 40vh;">
						<td style="width: 50%">
							<a href="#" class="inline-edit" data-type="textarea" data-pk="strengths" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.strengths')}}">{{ $properties->strengths }}</a>
						</td>
						<td style="width: 50%">
							<a href="#" class="inline-edit" data-type="textarea" data-pk="weaknesses" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.weaknesses')}}">{{ $properties->weaknesses }}</a>
						</td>
					</tr>
					<tr>
						<th>Chancen (Opportunities)</th>
						<th>Risiken (Threats)</th>
					</tr>
					<tr style="text-align: center; font-size: 24px; height: 40vh;">
						<td style="width: 50%">
							<a href="#" class="inline-edit" data-type="textarea" data-pk="opportunities" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.opportunities')}}">{{ $properties->opportunities }}</a>
						</td>
						<td style="width: 50%">
							<a href="#" class="inline-edit" data-type="textarea" data-pk="threats" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.threats')}}">{{ $properties->threats }}</a>
						</td>
					</tr>
				</tbody>
			</table>

			<div class="row">
				<div class="swot_ranking col-md-8">
					<form id="swat_form">
						
						<?php
							$b = $b1 = $b2 = $b3 = $b4 = $b5 = 0;
							if($properties->swot_json){
								$nnna = json_decode($properties->swot_json,true);
								if(isset($nnna[0]))
									$b = $nnna[0];

								if(isset($nnna[1]))
									$b1 = $nnna[1];

								if(isset($nnna[2]))
									$b2= $nnna[2];

								if(isset($nnna[3]))
									$b3= $nnna[3];

								if(isset($nnna[4]))
									$b4= $nnna[4];

								if(isset($nnna[5]))
									$b5= $nnna[5];
							}
						?>

						<table class="table table-striped">
							<tr>
								<td>
									<label>Lage : </label>
									<select id="rate_box_1" name="location" class="">
										<option value="0">-- Choose a value --</option>
										<?php foreach($rateSwotValArray as $val): ?>
											<option @if($val==$b)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<label>Mietermix : </label>
									<select id="rate_box_2" name="tenant" class="">
										<option value="0">-- Choose a value --</option>
										<?php foreach($rateSwotValArray as $val): ?>
											<option @if($val==$b1)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<label>Mieterbonität : </label>
									<select id="rate_box_3" name="tenant_credit" class="">
										<option value="0">-- Choose a value --</option>
										<?php foreach($rateSwotValArray as $val): ?>
												<option @if($val==$b2)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<label>Finanzierbarkeit : </label>
									<select id="rate_box_4" name="affordability" class="">
										<option value="0">-- Choose a value --</option>
										<?php foreach($rateSwotValArray as $val): ?>
											<option @if($val==$b3)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<label>Bausubstanz innen : </label>
									<select id="rate_box_5" name="building_substance_inside" class="">
										<option value="0">-- Choose a value --</option>
										<?php foreach($rateSwotValArray as $val): ?>
											<option @if($val==$b4)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<label>Bausubstanz außen : </label>
									<select id="rate_box_6" name="building_substance_outside" class="">
										<option value="0">-- Choose a value --</option>
										<?php foreach($rateSwotValArray as $val): ?>
											<option @if($val==$b5)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									<label>Summe : </label>
									<?php $bsum = $b +$b1 +$b2 +$b3 +$b4 +$b5; ?>
									<input type="text" readonly value="{{$bsum}}" id="total_rate" name="total_rate" />
								</td>
							</tr>
						</table>

						<p>*10 ist am besten</p>
					</form>
				</div>
			</div>
		</div>
						
	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var _token                      = '{{ csrf_token() }}';
		var url_property_update_schl_1	= '{{url('property/update/schl') }}';
	</script>
	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{asset('js/property/swot-template.js')}}"></script>
@endsection