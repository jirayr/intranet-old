@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  <div id="property_invoice" class="tab-pane fade in active">
  	
  	<div class="row">
		<div class="col-md-12">
			<span id="property_invoice_msg"></span>
		</div>
	</div>

	<div class="row white-box">

		<div class="col-md-12">
			<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add_property_invoice_modal">Neue Rechnung</button>
			@if(Auth::user()->email==config('users.falk_email'))
				<button type="button" class="btn btn-primary multiple-invoice-release-request pull-right">Markierte Rechnungen freigeben</button>
			@endif
		</div>

		<br/><br/>

		<div class="col-md-12 main_div" style="margin-bottom: 20px;">

			<div class="row">
				<div class="col-md-12">
					<h3>Offene Rechnungen</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 table-responsive">
					<table class="table table-striped" id="add_property_invoice_table" style="width: 100%;padding-left: 0px;">
						<thead>
							<tr>
								<th>
									<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
									<input type="checkbox" class="row_checkall">
								</th>
								<th>#</th>
								<th>Rechnung</th>
								<th>Re. D.</th>
								<th>Re. Bet.</th>
								<th>Kommentar</th>
								<th>Abbuch.</th>
								<th>Umlegb.</th>
								<th>User</th>
								<th>Datum</th>
								<!-- <th>User</th> -->
								<!-- <th>Freigabe AM/Bau</th> -->
								<th>Ablehnen AM</th>
								<th>Freigabe Falk</th>
								<th>Ablehnen</th>
								<th>Pending</th>
								<th>Aktion</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-md-12 main_div">

			<div class="row">
				<div class="col-md-12">
					<h3>Freigegebene Rechnungen</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-md-2">
					<select class="form-control" id="invoice-filter-field">
						<option value="rechnungsdatum" selected>Re. D.</option>
						<option value="freigabedatum">Freigabedatum</option>
					</select>
				</div>
				<div class="col-md-2">
					<input type="text" id="invoice-filter-start-date" class="form-control mask-input-new-date"  placeholder="DD.MM.YYYY">
				</div>
				<div class="col-md-2">
					<input type="text" id="invoice-filter-end-date" class="form-control mask-input-new-date" placeholder="DD.MM.YYYY">
				</div>
				<div class="col-md-6">
					<button type="button" class="btn btn-success" id="btn-invoice-filter">Filter</button>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 table-responsive">
					<table class="table table-striped" id="add_property_invoice_table2" style="width: 100%;padding-left: 0px;">
						<thead>
							<tr>
								<th>
									<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
									<input type="checkbox" class="row_checkall">
								</th>
								<th>#</th>
								<th>Rechnung</th>
								<th>Re. D.</th>
								<th>Re. Bet.</th>
								<th>Kommentar Rechnung</th>
								<th>Abbuch.</th>
								<th>Umlegb.</th>
								<th>User</th>
								<th>Freigabedatum</th>
								<th>Kommentar Falk</th>
								<th>Überweisung TR</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-md-12 main_div">

			<div class="row">
				<div class="col-md-12">
					<h3>Pending Rechnungen</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 table-responsive">
					<table class="table table-striped" id="add_property_invoice_table4" style="width: 100%;padding-left: 0px;">
						<thead>
							<tr>
								<th>
									<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
									<input type="checkbox" class="row_checkall">
								</th>
								<th>#</th>
								<th>Rechnung</th>
								<th>Re. D.</th>
								<th>Re. Bet.</th>
								<th>Kommentar Rechnung</th>
								<th>Abbuch.</th>
								<th>Umlegb.</th>
								<th>User</th>
								<th>Freigabedatum</th>
								<th>Freigabe Falk</th>
								<th>Kommentar Falk</th>
								<th>Aktion</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-md-12 main_div">

			<div class="row">
				<div class="col-md-12">
					<h3>Nicht freigegeben (AM)</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 table-responsive">
					<table class="table table-striped" id="table_rejected_invoice" style="width: 100%;padding-left: 0px;">
						<thead>
							<tr>
								<th>
									<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
									<input type="checkbox" class="row_checkall">
								</th>
								<th>#</th>
								<th>Rechnung</th>
								<th>Re. D.</th>
								<th>Re. Bet.</th>
								<th>User</th>
								<th>Kommentar</th>
								<th>Abbuch.</th>
								<th>Umlegb.</th>
								<th>Datum</th>
								<th>Kommentar AM</th>
								<th>Aktion</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-md-12 main_div">

			<div class="row">
				<div class="col-md-12">
					<h3>Nicht Freigegeben (Falk)</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 table-responsive">
					<table class="table table-striped" id="add_property_invoice_table3" style="width: 100%;padding-left: 0px;">
						<thead>
							<tr>
								<th>
									<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
									<input type="checkbox" class="row_checkall">
								</th>
								<th>#</th>
								<th>Rechnung</th>
								<th>Re. D.</th>
								<th>Re. Bet.</th>
								<th>User</th>
								<th>Kommentar</th>
								<th>Abbuch.</th>
								<th>Umlegb.</th>
								<th>Datum</th>
								<th>Kommentar Falk</th>
								<th>Aktion</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>

		</div>

		<div class="col-md-12" style="margin-top: 20px;">

			<h3>Verlauf</h3>
			<div class="table-responsive">
				<table class="table table-striped" id="invoice_mail_table">
			    	<thead>
			      		<tr>
					        <th class="">Name</th>
					        <th class="">Button</th>
					        <th>Invoice</th>
					        <th>Re. D.</th>
					        <th class="">Kommentar</th>
					        <th class="">Datum</th>
			      		</tr>
			    	</thead>
			    	<tbody>
			    	</tbody>
			  	</table>
			</div>
		</div>

	</div>



	<div class=" modal fade" role="dialog" id="add_property_invoice_modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Neue Rechnung</h4>
				</div>
				<form method="post" enctype="multipart/form-data" id="add_property_invoice_form">
					<div class="modal-body">

						<span id="add_property_invoice_msg"></span>

						<input type="hidden" name="_token" value="{{csrf_token()}}">
						<input type="hidden" name="property_id" value="{{$id}}">
						<input type="hidden" name="file_basename" id="property_invoice_gdrive_file_basename" value="">
						<input type="hidden" name="file_dirname" id="property_invoice_gdrive_file_dirname" value="">
						<input type="hidden" name="file_type" id="property_invoice_gdrive_file_type" value="">
						<input type="hidden" name="file_name" id="property_invoice_gdrive_file_name" value="">
						<input type="hidden" name="current_tab_name" id="current_tab_nam_invoice" value="Rechnungen">

						<label>Rechnung</label> <span id="property_invoice_gdrive_file_name_span"> </span>
						<a href="javascript:void(0);" class="link-button-gdrive-invoice btn btn-info">Datei auswählen</a>


	                    <a class="property_invoice-upload-file-icon btn btn-info" href="javascript:void(0);">Hochladen</a>
	                    <input type="file" name="property_invoice_gdrive_file_upload" class="hide property_invoice-gdrive-upload-file-control" id="property_invoice_gdrive_file_upload" >
						<br>
						<br>

						<label>Re. D.</label>
						<input type="text" name="date" class="form-control mask-input-new-date" placeholder="DD.MM.YYYY">
						<br>

						<label>Re. Bet.</label>
						<input type="text" name="amount" class="form-control mask-input-number">
						<br>
						
						<label>Kommentar</label>
						<textarea class="form-control" name="comment" ></textarea>
						<br>

						<input type='checkbox' class="" name="is_paid" >
						<label>wird automatisch abgebucht</label>
						<br>

						<input type='checkbox' style="float: left;margin-right: 3px;margin-top:12px;" class="" name="towards_tenant" >
						<label style="float: left;padding-right: 10px;margin-top:8px;">auf Mieter zu</label>	
						<input type="text" name="foldable" class="form-control mask-input-number" style="float: left;padding-right: 10px;width: 100px;">
						<label style="float: left;padding-right: 10px;margin-top:8px;">% umlegbar</label>
						
						<br>
						

					</div>
					<div class="modal-footer" style="margin-top: 15px;">
						<button type="submit" class="btn btn-primary">Speichern</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
					</div>
				</form>
			</div>

		</div>
	</div>

	<div class="modal fade" id="invoice-reject-modal" role="dialog">
	   <div class="modal-dialog">
	      <!-- Modal content-->
	      <div class="modal-content">
	         <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	         </div>
	         <div class="modal-body">
	            <label>Kommentar</label>
	            <textarea class="form-control invoice-reject-comment" name="message"></textarea>
	            <br>
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn btn-primary invoice-reject-submit" data-dismiss="modal" >Senden</button>
	            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="modal fade" id="multiple-invoice-release-property-modal" role="dialog">
	   <div class="modal-dialog">
	      <!-- Modal content-->
	      <div class="modal-content">
	         <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	         </div>
	         <div class="modal-body">
	            <label>Kommentar</label>
	            <textarea class="form-control multiple-invoice-release-comment" name="message"></textarea>
	            <br>
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn btn-primary multiple-invoice-release-submit" data-dismiss="modal" >Senden</button>
	            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="modal fade" id="invoice-release-property-modal" role="dialog">
	   <div class="modal-dialog">
	      <!-- Modal content-->
	      <div class="modal-content">
	         <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	         </div>
	         <div class="modal-body">
	            <p class="i-message hidden">Möchten Sie die Rechnung wirklich an Falk zur Freigabe senden?</p>
	            <label class="i-message2">Kommentar</label>
	            <textarea class="form-control invoice-release-comment i-message2" name="message"></textarea>
	            <br>
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn btn-primary invoice-release-submit" data-dismiss="modal" >Senden</button>
	            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="modal fade" id="not-release-modal" role="dialog">
	   <div class="modal-dialog">
	      <!-- Modal content-->
	      <div class="modal-content">
	         <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	         </div>
	         <div class="modal-body">
	            <label>Kommentar</label>
	            <textarea class="form-control invoice-not-release-comment" name="message"></textarea>
	            <br>
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn btn-primary invoice-not-release-submit" data-dismiss="modal" >Senden</button>
	            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="modal fade" id="pending-modal" role="dialog">
	   <div class="modal-dialog">
	      <!-- Modal content-->
	      <div class="modal-content">
	         <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	         </div>
	         <div class="modal-body">
	            <label>Kommentar</label>
	            <textarea class="form-control invoice-pending-comment" name="message"></textarea>
	            <br>
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn btn-primary invoice-pending-submit" data-dismiss="modal" >Senden</button>
	            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
	         </div>
	      </div>
	   </div>
	</div>

	@include('property.partial.comman_modal')

  </div>
</div>

@endsection

@section('js')

	<script type="text/javascript">
		var _token                				= '{{ csrf_token() }}';
		var workingDir 							= '{{ $workingDir }}';
		var lfm_route    		  				= '{{ url("file-manager") }}';
		var url_get_property_invoice 			= '{{ route('get_property_invoice', ['property_id' => $properties->id]) }}';
		var url_get_pending_invoice 			= '{{ route('get_pending_invoice', ['id' => $properties->id]) }}';
		var url_get_not_released_invoice 		= '{{ route('get_not_released_invoice', ['id' => $properties->id]) }}';
		var url_get_not_released_invoice_am 	= '{{ route('get_not_released_invoice_am', ['id' => $properties->id]) }}';
		var url_get_release_property_invoice 	= '{{ route('get_release_property_invoice', ['property_id' => $properties->id]) }}';
		var url_get_invoice_logs 				= '{{ route('get_invoice_logs', ['property_id' => $properties->id]) }}';
		var url_add_property_invoice 			= '{{ route('add_property_invoice') }}';
		var url_property_invoicemarkreject 		= '{{ url('property/invoicemarkreject') }}';
		var url_property_invoicereleaseprocedure = '{{ url('property/invoicereleaseprocedure') }}';
		var url_property_invoicenotrelease 		= '{{ url('property/invoicenotrelease') }}';
		var url_property_invoicemarkpending 	= '{{ url('property/invoicemarkpending') }}';
		var url_delete_property_invoice 		= '{{ route("delete_property_invoice", ":id") }}';
		var url_makeAsPaid 						= '{{ route('makeAsPaid') }}';
	</script>

	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{ asset('file-manager/js/bootbox.min.js') }}"></script>
	<script src="{{ asset('file-manager/js/script.js') }}"></script>
  	<script src="{{ asset('js/custom-datatable.js') }}"></script>
  	<script src="{{asset('js/property/property_invoice.js')}}"></script>
@endsection