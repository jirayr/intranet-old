<div class="modal fade" id="select-gdrive-file-model" role="dialog" style="z-index: 9999;">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <input type='hidden' name='workingDir' id='workingDir'>
            <input type='hidden' name='select-gdrive-file-type' id='select-gdrive-file-type'>
            <input type='hidden' name='select-gdrive-file-data' id='select-gdrive-file-data'>
            <input type='hidden' name='select-gdrive-file-callback' id='select-gdrive-file-callback'>
            <a href="javascript:;" class="navbar-brand clickable hide" id="gdrive-file-previous-button">
            <i class="fa fa-arrow-left"></i>
            <span class="hidden-xs">{{ trans('lfm.nav-back') }}</span>
            </a>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <h4>Select any file to link. </h4>
            <div id="select-gdrive-file-content"></div>
         </div>
      </div>
      <!-- Modal content-->
   </div>
</div>

<div class="modal fade" id="viewFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Files</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <input type="hidden" id="file_id" value="0">
            <div id="view-gdrive-file-content"></div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="deleteFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete File</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <input type="hidden" id="file_id" value="0">
            <div id="delete-gdrive-file-content"></div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>