@section('p_title')

  {{ $properties->name_of_property }}

  <?php

      if ($properties->status == config('properties.status.in_purchase')):
        $status = __('property.in_purchase');
      endif;

      if ($properties->status == config('properties.status.offer')):
        $status = __('property.offer');
      endif;

      if ($properties->status == config('properties.status.exclusive')): 
        $status = __('property.exclusive');
      endif;

      if ($properties->status == config('properties.status.exclusivity')): 
        $status = __('property.exclusivity');
      endif;

      if ($properties->status == config('properties.status.certified')): 
        $status = __('property.certified');
      endif;

      if ($properties->status == config('properties.status.duration')): 
        $status = __('property.duration');
      endif;

      if ($properties->status == config('properties.status.sold')): 
        $status = __('property.sold');
      endif;

      if ($properties->status == config('properties.status.declined')): 
        $status = __('property.declined');
      endif;

      if ($properties->status == config('properties.status.lost')): 
        $status = __('property.lost');
      endif;

      if ($properties->status == config('properties.status.hold')): 
        $status = __('property.hold');
      endif;

      if ($properties->status == config('properties.status.quicksheet')): 
        $status = __('property.quicksheet');
      endif;

      if ($properties->status == config('properties.status.liquiplanung')): 
        $status = __('property.liquiplanung');
      endif;

      if ($properties->status == config('properties.status.externquicksheet')): 
        $status = __('property.externquicksheet');
      endif;
  ?>

  @if(isset($properties->asset_manager))
    (AM: {{$properties->asset_manager->name}}, Status: {{$status}})
  @else
    (Status: {{$status}})
  @endif

  <button type="button" id="mail-an-am" data-type="0" class="btn btn-primary btn-sm"></i> Mail an AM</button>
  <button type="button" id="mail-an-hv-bu" data-type="1" class="btn btn-primary btn-sm"></i> Mail an HV BU</button>
  <button type="button" id="mail-an-hv-pm" data-type="2" class="btn btn-primary btn-sm"></i> Mail an HV PM</button>
  <button type="button" id="mail-an-ek" data-type="3" class="btn btn-primary btn-sm"></i> Mail an EK</button>
  <button type="button" id="mail-an-sb" data-type="7" class="btn btn-primary btn-sm"></i> Mail an SB</button>
  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mail_all_modal"></i> Mails an... </button>

@endsection

<div class=" modal fade" role="dialog" id="mail_an_am_modal">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mail an AM</h4>
      </div>
      <form action="{{ route('sendmail_to_am') }}" method="POST">
        <div class="modal-body">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="property_id" value="{{$properties->id}}">
          {{-- <input type="hidden" name="am_id" id="am_id" value="{{$properties->asset_m_id}}"> --}}
          <input type="hidden" name="subject" value="{{ $properties->name_of_property }}">
          <input type="hidden" name="type" id="mail_type" value="0">

          <label>Nachricht</label>
          <textarea class="form-control" name="message" required></textarea>
          <br>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >Senden</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class=" modal fade" role="dialog" id="mail_all_modal">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mail</h4>
      </div>
      <form action="{{ route('sendmail_to_all') }}" method="POST">
        <div class="modal-body">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          <input type="hidden" name="property_id" value="{{$properties->id}}">

          <label>Nachricht</label>
          <textarea class="form-control" name="message" required></textarea>
          <br>

          <label class="checkbox-inline"><input type="checkbox" name="type[]" value="0">AM</label>
          <label class="checkbox-inline"><input type="checkbox" name="type[]" value="1">HV BU</label>
          <label class="checkbox-inline"><input type="checkbox" name="type[]" value="2">HV PM</label>
          <label class="checkbox-inline"><input type="checkbox" name="type[]" value="3">EK</label>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" >Senden</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
        </div>
      </form>
    </div>
  </div>
</div>

@php
	$active_tab = ['properties', 'quick-sheet', 'sheet', 'budget', 'tenancy-schedule', 'swot-template', 'schl-template', 'expose', 'bank-modal', 'comment_tab', 'recommended_tab', 'provision_tab', 'property_invoice', 'property_insurance_tab', 'contracts', 'default_payer', 'einkauf_tab', 'vermietung', 'verkauf_tab', 'exportverkauf_tab', 'Anzeige_aufgeben', 'Anfragen', 'Leerstandsflächen', 'email_template', 'finance','darlehensspiegel', 'dateien', 'insurance_tab', 'property_management', 'am_mail', 'project'];
	$tab = (isset($_GET['tab']) && in_array($_GET['tab'], $active_tab)) ? $_GET['tab'] : 'properties';
@endphp

<input type="hidden" id="path-properties-show" value="{{ route('show_property', ['id' => $id]) }}">
<input type="hidden" id="main-properties-path" value="{{ route('show_property', ['id' => $id]) }}">
<input type="hidden" id="selected_property_id" value="{{ $id }}">
<input type="hidden" id="select_a_brand" value="{{ trans('app.select_a_brand') }}">
	
<ul class="nav nav-tabs" id="property_tab" data-url="{{ route('show_property', ['id' => $id]) }}">
  <li class="{{ ($tab == 'properties') ? 'active' : '' }}" data-tab="properties"><a data-toggle="tab" href="#properties">Kalkulation</a></li>
  <li class="{{ ($tab == 'quick-sheet') ? 'active' : '' }}" data-tab="quick-sheet"><a data-toggle="tab" href="#quick-sheet">Quick Sheet</a></li>
  <li class="{{ ($tab == 'sheet') ? 'active' : '' }}" data-tab="sheet"><a data-toggle="tab" href="#sheet">Sheet</a></li>
  <li class="{{ ($tab == 'budget') ? 'active' : '' }}" data-tab="budget"><a data-toggle="tab" href="#budget">Budget</a></li>
  <li class="{{ ($tab == 'tenancy-schedule') ? 'active' : '' }}" data-tab="tenancy-schedule"><a data-toggle="tab" href="#tenancy-schedule">Mieterliste</a></li>
  <li class="{{ ($tab == 'swot-template') ? 'active' : '' }}" data-tab="swot-template"><a data-toggle="tab" href="#swot-template">SWOT Template</a></li>

  <li class="{{ ($tab == 'schl-template') ? 'active' : '' }}" data-tab="schl-template"><a data-toggle="tab" href="#schl-template">Objektdatenblatt</a></li>
  <li class="{{ ($tab == 'expose') ? 'active' : '' }}" data-tab="expose"><a data-toggle="tab" href="#expose">Anhänge</a></li>
  <li class="{{ ($tab == 'bank-modal') ? 'active' : '' }}" data-tab="bank-modal"><a data-toggle="tab" href="#bank-modal">Bankenanfrage</a></li>
  <li class="{{ ($tab == 'comment_tab') ? 'active' : '' }}" data-tab="comment_tab"><a data-toggle="tab" href="#comment_tab">Maßnahmen</a></li>
  <li class="{{ ($tab == 'recommended_tab') ? 'active' : '' }}" data-tab="recommended_tab"><a data-toggle="tab" href="#recommended_tab">Empfehlung</a></li>
  <li class="{{ ($tab == 'provision_tab') ? 'active' : '' }}" data-tab="provision_tab"><a data-toggle="tab" href="#provision_tab">Provision</a></li>
  <li class="{{ ($tab == 'property_invoice') ? 'active' : '' }}" data-tab="property_invoice"><a data-toggle="tab" href="#property_invoice">Rechnungen</a></li>
  <li class="{{ ($tab == 'property_insurance_tab') ? 'active' : '' }}" data-tab="property_insurance_tab"><a data-toggle="tab" href="#property_insurance_tab">Angebote</a></li>
  <li class="{{ ($tab == 'contracts') ? 'active' : '' }}" data-tab="contracts"><a data-toggle="tab" href="#contracts">Verträge</a></li>
  {{-- <li class="{{ ($tab == 'default_payer') ? 'active' : '' }}" data-tab="default_payer"><a data-toggle="tab" href="#default_payer">Offene Posten</a></li> --}}
  <li class="{{ ($tab == 'einkauf_tab') ? 'active' : '' }}" data-tab="einkauf_tab"><a data-toggle="tab" href="#einkauf_tab">Einkauf</a></li>
  <li class="{{ ($tab == 'vermietung') ? 'active' : '' }}" data-tab="vermietung"><a data-toggle="tab" href="#vermietung">Vermietungsportal</a></li>
  <li class="{{ ($tab == 'verkauf_tab') ? 'active' : '' }}" data-tab="verkauf_tab"><a data-toggle="tab" href="#verkauf_tab">Verkauf</a></li>
  <li class="{{ ($tab == 'exportverkauf_tab') ? 'active' : '' }}" data-tab="exportverkauf_tab"><a data-toggle="tab" href="#exportverkauf_tab">Exposé</a></li>
  <li class="{{ ($tab == 'Anzeige_aufgeben') ? 'active' : '' }}" data-tab="Anzeige_aufgeben"><a data-toggle="tab" href="#Anzeige_aufgeben">Verkaufsportal</a></li>
  <li class="{{ ($tab == 'Anfragen') ? 'active' : '' }}" data-tab="Anfragen"><a data-toggle="tab" href="#Anfragen">Anfragen</a></li>
  <li class="{{ ($tab == 'Leerstandsflächen') ? 'active' : '' }}" data-tab="Leerstandsflächen"><a data-toggle="tab" href="#Leerstandsflächen">Leerstandsflächen</a></li>
  <li class="{{ ($tab == 'email_template') ? 'active' : '' }}" data-tab="email_template"><a data-toggle="tab" href="#email_template">LOI</a></li>
  {{-- <li class="{{ ($tab == 'darlehensspiegel') ? 'active' : '' }}" data-tab="darlehensspiegel"><a data-toggle="tab" href="#darlehensspiegel">Darlehensspiegel</a></li> --}}
  @if(Auth::user()->role==1 || Auth::user()->role==5 || Auth::user()->id==$properties->asset_m_id)
    <li class="{{ ($tab == 'finance') ? 'active' : '' }}" data-tab="finance"><a data-toggle="tab" href="#finance">Finance</a></li>
  @endif
  <li class="{{ ($tab == 'dateien') ? 'active' : '' }}" data-tab="dateien"><a data-toggle="tab" href="#dateien">Dateien</a></li>
  <li class="{{ ($tab == 'insurance_tab') ? 'active' : '' }}" data-tab="insurance_tab"><a data-toggle="tab" href="#insurance_tab">Versicherungen</a></li>
  <li class="{{ ($tab == 'property_management') ? 'active' : '' }}" data-tab="property_management"><a data-toggle="tab" href="#property_management">Hausverwaltung</a></li>
  <li class="{{ ($tab == 'am_mail') ? 'active' : '' }}" data-tab="am_mail"><a data-toggle="tab" href="#am_mail">Mails</a></li>
  <li class="{{ ($tab == 'project') ? 'active' : '' }}" data-tab="project"><a data-toggle="tab" href="#project">Project</a></li>

</ul>