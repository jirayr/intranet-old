@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="project" class="tab-pane fade in active">
  		
  		<div class="modal-dialog modal-lg" style="width:100%">
			<div class="modal-content">
				<div class="modal-header">
					<div>
						<div class="col-md-10">
							<h4 class="modal-title">Projekte</h4>
						</div>
						<div class="col-md-2">
							<a class="btn btn-primary pojectCreate">Bearbeiten</a>
						</div>
					</div>

				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12 table-responsive white-box">
							<table class="table table-striped" id="project_table">
								<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Erstellt von </th>
									<th>Verknüpft</th>
									<th>Beschreibung</th>
									<th>Notizen</th>
									<th>Datum</th>
									<th>Aktion</th>
								</tr>
								</thead>
								<tbody>
								@if(!empty($propertyProjects))
									@foreach ($propertyProjects as $key => $propertyProject)
										<tr>
											<td>{{ ($key+1) }}</td>
											<td>{{ $propertyProject->name }}</td>
											<td>{{ $propertyProject->user->name }}</td>
											<td>
												@foreach ($propertyProject->employees as $employee)
													<span class="badge">{{ $employee->vorname}}{{ $employee->nachname}}</span>
												@endforeach
												@foreach ($propertyProject->userEmployees as $employee)
													<span class="badge">{{ $employee->name}}</span>
												@endforeach
											</td>
											<td>{{ $propertyProject->description }}</td>
											<td>{{ $propertyProject->note}}</td>
											<td>{{$propertyProject->date }}</td>
											<td>
												<a type="button" data-id="{{$propertyProject->id}}" class="btn-sm btn-info edit">Bearbeiten</a>
												<a type="button" data-id="{{$propertyProject->id}}"  class="btn-sm btn-danger delete">Löschen</a>
												<a type="button" href="{{url('properties/'.Request::segment(2).'/project/'.$propertyProject->id.'/email/type/'.'1')}}" class="btn-sm btn-primary"> View Emails</a>
											</td>
										</tr>
									@endforeach
								@endif
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div class=" modal fade" role="dialog" id="projectCreate">
			<div class="modal-dialog modal-lg">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title" id="title"> Projekt erstellen</h4>
					</div>
					<div class="modal-body">
						<form id="projectForm" class="projectform" method="post" action="">
							<div class="row">
								<div class="form-group col-md-6" >
									<label for="email">Name:</label>
									<input required type="text" class="form-control" name="name" maxlength="105"  placeholder="Name eingeben" id="name">
								</div>
								<div class="form-group col-md-6">
									<label for="email">Mitarbeiter:</label>
									<select  required class="form-control search" name="employee_id[]"  style="width: 100%"></select>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-6">
									<label for="email">Datum:</label>
									<input type='text' id='datetimepicker1' name="date" class="form-control" />
								</div>
								<div class="form-group col-md-6">
									<label for="email">Nutzer:</label>
									<select  required class="form-control user-search" name="employee_user[]"  style="width: 100%"></select>
								</div>
							</div>

							<div class="row">
								<div class="form-group col-md-6">
									<label for="email">Beschreibung:</label>
									<textarea name="description" class="form-control" id="description" cols="30" rows="10" placeholder="Beschreibung eingeben"></textarea>
								</div>
								<div class="form-group col-md-6">
									<label for="email">Notiz:</label>
									<textarea name="note" class="form-control" id="notes" cols="30" rows="10" placeholder="Notiz eingeben"></textarea>
								</div>
							</div>
							<input type="hidden" name="property_id" id="propId" value="<?php echo Request::segment(2); ?>">
							<input type="hidden"  id="projectId" value="">
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<button type="submit"  id="btn-save" class="btn btn-primary">Speichern</button>
						</form>
					</div>
				</div>
			</div>
		</div>

  	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var _token 					= '{{ csrf_token() }}';
		var url_project_edit 		= '{{url('properties/'.Request::segment(2).'/project/edit')}}';
		var url_project_delete    	= '{{url('properties/'.Request::segment(2).'/project/delete')}}';
		var url_autocompleteSearchUserByName = '{{route("autocompleteSearchUserByName")}}';
		var url_autocompleteByName 	= '{{route("autocompleteByName")}}';
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{ asset('js/custom-datatable.js') }}"></script>
  	<script src="{{asset('js/property/project.js')}}"></script>
@endsection