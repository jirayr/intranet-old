@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="provision_tab" class="tab-pane fade in active">
  	
  		<div class="col-md-12 col-lg-12 col-sm-12 {{$provision_visible_class}}">
			<button type="button" class="add-new-provision btn btn-primary">Add New</button>
            <div class="provision_info-table" style="margin-top: 20px;"></div>
            <div class="provision-release-logs"></div>
        </div>

        <div class="modal fade" id="provision_comment_modal" role="dialog">
		   	<div class="modal-dialog" style="width: 50%;">
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		         </div>
		         <div class="modal-body">
		            <form id="provision_comment_form" action="{{ route('add_provision_comment') }}">
		               <div class="row">
		                  <div class="col-md-12">
		                     <label>Kommentar</label>
		                     <textarea class="form-control" id="provision_comment" name="comment" required></textarea>
		                  </div>
		               </div>
		               <br/>
		               <div class="row">
		                  <div class="col-md-12 text-center">
		                     <button type="submit" class="btn btn-primary">Posten</button>
		                  </div>
		               </div>
		            </form>

		            <br>

		            <div class="row">
		               <div class="col-md-12">
		                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
		                    <table class="forecast-table logtable" id="provision_comments_table">
		                      <thead>
		                        <tr>
		                          <th class="bg-light-gray">Name</th>
		                          <th class="bg-light-gray">Kommentar</th>
		                          <th class="bg-light-gray">Datum</th>
		                          <th class="bg-light-gray">Action</th>
		                        </tr>
		                      </thead>
		                      <tbody></tbody>
		                      <tfoot>
		                        <tr>
		                          <td colspan="4">
		                            <button type="button" class="btn-sm btn" id="provision_comments_limit">Show More</button>
		                          </td>
		                        </tr>
		                      </tfoot>
		                    </table>
		                  </div>
		               </div>
		            </div>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		         </div>
		      </div>
		   </div>
		</div>

		<div class="modal fade" id="provision-release-property-modal" role="dialog">
		   <div class="modal-dialog">
		      <!-- Modal content-->
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		         </div>
		         <div class="modal-body">
		            <label>Kommentar</label>
		            <textarea class="form-control provision-release-comment" name="message"></textarea>
		            <br>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-primary provision-release-submit" data-dismiss="modal" >Senden</button>
		            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		         </div>
		      </div>
		   </div>
		</div>

		<div class="modal fade" id="provision-not-release-modal" role="dialog">
		   <div class="modal-dialog">
		      <!-- Modal content-->
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal">&times;</button>
		         </div>
		         <form action="{{ route('provision_mark_as_not_release', ['property_id' => $id]) }}" id="provision-not-release-form">
		            <div class="modal-body">
		               <label>Kommentar</label>
		               <textarea class="form-control" name="message" required></textarea>
		               <br>
		            </div>
		            <div class="modal-footer">
		               <button type="submit" class="btn btn-primary">Senden</button>
		               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
		            </div>
		         </form>
		      </div>
		   </div>
		</div>

  	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var _token                					= '{{ csrf_token() }}';
		var url_getProvisionbutton 					= '{{ route('getProvisionbutton') }}';
		var url_delete_provision_comment			= '{{ route('delete_provision_comment', ['id' => ':id']) }}';
		var url_property_provisionreleaseprocedure 	= '{{ url('property/provisionreleaseprocedure') }}';
		var url_changeprovisioninfo 				= '{{ route('changeprovisioninfo') }}';
		var url_deleteprovisioninfo 				= '{{ route('deleteprovisioninfo') }}';
	</script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{ asset('js/custom-datatable.js') }}"></script>
  	<script src="{{asset('js/property/provision_tab.js')}}"></script>
@endsection