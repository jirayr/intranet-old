@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<style type="text/css">
	  .tenant-list .form-control{
	    padding: 7px 5px !important;
	    font-size: 13px !important;
	  }
	  .mask-number-input, .mask-number-input-negetive{
	  	text-align: right;
	  }
	</style>
	<script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  	<div id="sheet" class="tab-pane fade in active">

  		<div id="tenancy-schedule">

		   	<ul class="nav nav-tabs">
		      	
		      	<?php
		         	$bank_array = array();
		         	$j = $ist = 0;
		         	foreach($banks as $key => $bank){
		            	$bank_array[] = $bank->id;
		         	}

		         	array_unique($bank_array);
		         	$str = implode(',', $bank_array);
		         
		         	$properties_banks = DB::table('properties')->whereRaw('main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('standard_property_status','desc')->get();
		        ?>

		      	@foreach($properties_banks as $property_sheet)
		      		@if($property_sheet->lock_status==0)
		      			<li class="{{($j == 0)?'in active':''}}">

		         			<a data-toggle="tab" href="#sheet-{{$property_sheet->id}}">

			         			@if($property_sheet->Ist)
			         				<?php $t = "Ist-";?>
			         			@else
			         				<?php $t = "Soll-";?>
			         			@endif

			         			@if($property_sheet->sheet_title)
			         				{{$property_sheet->sheet_title}}
		         				@else
			         				Title
			         			@endif

			         			@if($property_sheet->Ist)
			         				@if($ist==1)
			         					<span data-id="{{$property_sheet->id}}" onclick="deletesheet({{$property_sheet->id}})" class="remove-sheet">X</span>
			         				@endif
			         				<?php
			            				if($ist==0){
			                				$GLOBALS['ist_sheet'] = $property_sheet;
			            				}
			            				$ist = 1;
			            			?>
			         			@else
			         				<span data-id="{{$property_sheet->id}}" onclick="deletesheet({{$property_sheet->id}})" class="remove-sheet">X</span>
			         			@endif
		         			</a>
		      			</li>

		      			@php
		      				$j = 1;
		      			@endphp
		      		@else
		      			<?php
		        			$GLOBALS['release_sheet'] = $property_sheet;
		      			?>
		      		@endif
		      	@endforeach

		   	</ul>

		   	<div class="tab-content">

		      	<?php $j=0; $ist = 0; ?>

		      	@foreach($properties_banks as $property_sheet)

			      	<?php

			        	if($property_sheet->lock_status)
			          		continue;

			         	$list_fields_percent = [
			             'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
			         	];

			         	foreach ($list_fields_percent as $field) {
			             	$property_sheet->$field *= 100;
			         	}

			         	$property_sheet->plz_ort = $properties->plz_ort;
			         	$property_sheet->ort = $properties->ort;
			         	$property_sheet->strasse = $properties->strasse;
			         	$property_sheet->hausnummer = $properties->hausnummer;
			         	$property_sheet->niedersachsen = $properties->niedersachsen;

			         	$comments_new = DB::table('property_comments')->where('property_id',$property_sheet->id)->whereNotNull('Ankermieter_note')->where('status',null)->first();
			         
			        ?>

			      	<div class="sheet-class tab-pane bank-tab @if($j==0) fade active in @endif" id="sheet-{{$property_sheet->id}}">

			         	<div class="row">
			            	<a class="btn btn-success create-new-soll"  href="{{route('create_soll',array($property_sheet->main_property_id,$property_sheet->id))}}" >+ Add New Sheet</a>
			            	<legend>Allgemeine Objektinformationen</legend>

			            	@php
			            		$j = 1;
			            		$sheet_property = $property_sheet;
			            		$id = $property_sheet->id;
			            	@endphp

			            	<div style="width: 100%"  class="col-md-10 col-xs-12">

			              		<form action="{{ url('importtenant') }}" method="post" id="importform{{ $id }}" enctype="multipart/form-data"></form>

		               			<form action="{{route('save_sheet')}}" method="post" class="form-horizontal sheetForm" accept-charset="UTF-8" enctype="multipart/form-data">
		                  			{{ csrf_field() }}
		                  			<input type="hidden" name="property_id" value="{{ $id }}">
		                  			<input type="hidden" name="main_property_id" value="{{ $property_sheet->main_property_id }}">
		                  			{{--First Row--}}

		                  			@if($ist==0 && $property_sheet->Ist)
		                  				<input type="hidden" name="global_sheet" value="1">
		                  				<?php $ist=1; ?>
		                  			@else
		                  				<input type="hidden" name="global_sheet" value="0">
		                  			@endif

			                  		<div class="form-group m-form__group row ">

			                     		<div class="sheet-section-1">

			                        		<div class="col-lg-2">
			                           			<label style="font-size: 13px">Name der Kalkulation:</label>
			                           			<input type="text" class="form-control" id="sheet_title" value="@if(isset($sheet_property)){{ $sheet_property->sheet_title }}@endif" name="sheet_title">
			                        		</div>

			                         		<div class="col-lg-3">
			                           			<label class="">Finanzierung:</label>
					                           	<select class="form-control bank-search " name="properties_bank_id" style="width: 100%; height: 100%">
					                              	@foreach($bank_all as $bank)
					                              		<option value="{{$bank->id}}" @if($sheet_property->properties_bank_id == $bank->id) selected @endif>{{$bank->name}}</option>
					                              	@endforeach
					                           	</select>
			                        		</div>

			                        		<div class="clearfix" style="margin-bottom:10px;"></div>

					                        <div class="col-lg-2">
					                           	<label>PLZ:</label>
					                           	@if(isset($sheet_property))
					                           		<br>{{ $sheet_property->plz_ort }}
					                           	@endif
					                           	<input type="hidden" class="form-control" id="plz_ort" value="@if(isset($sheet_property)){{ $sheet_property->plz_ort }}@endif" name="plz_ort">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label class="">Ort:</label>
					                           	@if(isset($sheet_property) && $sheet_property->ort)
					                           		<br><?php echo $sheet_property->ort;?>
					                           	@endif
					                           	<input type="hidden" class="form-control" id="ort" value="@if(isset($sheet_property)){{ $sheet_property->ort }}@endif" name="ort">
					                        </div>

			                        		<div class="col-lg-2">
			                           			<label class="">Bundesland: </label><br>
			                           			
			                           			<?php
					                              	$stats = array('Baden-Württemberg','Bayern','Berlin','Brandenburg','Bremen','Hamburg','Hessen','Mecklenburg-Vorpommern','Niedersachsen','Nordrhein-Westfalen','Rheinland-Pfalz','Saarland','Sachsen','Sachsen-Anhalt','Schleswig-Holstein','Thüringen');

			                              			$n_array = array('Baden-Württemberg'=>'5,0','Bayern'=>'3,5','Berlin'=>'6,0','Brandenburg'=>'6,5','Bremen'=>'5,0','Hamburg'=>'4,5','Hessen'=>'6,0','Mecklenburg-Vorpommern'=>'6,0','Niedersachsen'=>'5,0','Nordrhein-Westfalen'=>'6,5','Rheinland-Pfalz'=>'5,0','Sachsen'=>'3,5','Sachsen-Anhalt'=>'5,0','Schleswig-Holstein'=>'6,5','Thüringen'=>'6,5');
			                              		?>

			                           			@if(isset($sheet_property) && $sheet_property->niedersachsen)
			                           				{{ $sheet_property->niedersachsen }}
			                           			@endif
			                           			<input type="hidden" class="form-control" id="real_estate_taxes" value="{{$sheet_property->niedersachsen}}" name="niedersachsen">
			                        		</div>

					                        <div class="col-lg-2">
					                           	<label>Strasse:</label><br>
					                           	@if(isset($sheet_property))
					                           		{{ $sheet_property->strasse }}
					                           	@endif
					                           	<input type="hidden" class="form-control" id="strasse" value="@if(isset($sheet_property)){{ $sheet_property->strasse }}@endif" name="strasse">
					                        </div>

					                        <div class="col-lg-2">
					                          	<label>Hausnummer:</label><br>
					                           	@if(isset($sheet_property))
					                           		{{ $sheet_property->hausnummer }}
					                           	@endif
					                           	<input type="hidden" class="form-control" id="hausnummer" value="@if(isset($sheet_property)){{ $sheet_property->hausnummer }}@endif" name="hausnummer">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label class="">Objekt:</label><br>
					                           	{{ $sheet_property->name_of_property }}
					                           	<input type="hidden" class="form-control" id="name_of_property" value="@if(isset($sheet_property)){{ $sheet_property->name_of_property }}@endif" name="name_of_property">
					                        </div>

			                        		<div class="clearfix" style="margin-bottom:10px;"></div>

					                        <div class="col-lg-2">
					                           <label class="">Objekttyp:</label>
					                           	<?php
					                              	$a = getobjekttypearray();
					                            ?>
					                           	@foreach($a as $k=>$list)
					                           		@if(isset($sheet_property) && $sheet_property->property_type==$k)
					                           			<br>{{ $list }}
					                           		@endif
					                           	@endforeach
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Maklerpreis:</label><br>
					                           	@if(isset($sheet_property))
					                           		{{ number_format($sheet_property->maklerpreis,2,',','.') }}
					                           	@endif
					                           	<input type="hidden" class="form-control mask-number-input" id="maklerpreis" value="@if(isset($sheet_property)){{ number_format($sheet_property->maklerpreis,2,',','.') }}@endif" name="maklerpreis">
					                        </div>

					                        <div class="clearfix" style="margin-bottom:10px;"></div>

					                        <div class="col-lg-2">
					                           	<label>Baujahr:</label>
					                           	<input type="text" class="form-control bg-yellow" id="construction_year" value="@if(isset($sheet_property)){{ $sheet_property->construction_year }}@endif" name="construction_year">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>&nbsp;</label>
					                           	<input type="text" class="form-control" id="construction_year_note" value="@if(isset($sheet_property)){{ $sheet_property->construction_year_note }}@endif" name="construction_year_note" placeholder="Notizen">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Grundstück in m²:</label>
					                           	<input type="text" class="form-control mask-number-input bg-yellow" id="plot_of_land_m2" value="@if(isset($sheet_property)){{ number_format($sheet_property->plot_of_land_m2,2,',','.') }}@endif" name="plot_of_land_m2">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>&nbsp;</label>
					                           	<input type="text" class="form-control" id="plot_of_land_m2_text" value="@if(isset($sheet_property)){{ $sheet_property->plot_of_land_m2_text }}@endif" name="plot_of_land_m2_text" placeholder="Notizen">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Stellplätze:</label>
					                           	<input type="text" class="form-control mask-number-input bg-yellow" id="plot" value="@if(isset($sheet_property)){{ number_format($sheet_property->plot,2,',','.') }}@endif" name="plot">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Notizen zur Miete (€/m²):</label>
					                           	<input type="text" class="form-control mask-number-input" id="miete_text" value="@if(isset($sheet_property)){{ $sheet_property->miete_text }}@endif" name="miete_text">
					                        </div>

					                        <div class="clearfix" style="margin-bottom:20px;"></div>

					                        <div class="col-lg-2">
					                           	<label>Gewerbefläche vermietet</label>
					                           	<input type="text" class="form-control bg-yellow mask-number-input" id="rent_retail" value="@if(isset($sheet_property)){{ number_format($sheet_property->rent_retail,2,',','.') }}@endif" name="rent_retail">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Gewerbefläche Leerstand</label>
					                           	<input type="text" class="bg-yellow form-control mask-number-input" id="vacancy_retail" value="@if(isset($sheet_property)){{ number_format($sheet_property->vacancy_retail,2,',','.') }}@endif" name="vacancy_retail">
					                        </div>

					                        <div class="col-lg-2">
					                           	<input style="margin-top: -13px;width: 65%;float: left;" type="text" class="form-control"  id="buffer_title" value="@if(isset($sheet_property) && $sheet_property->sonstige_flache){{ $sheet_property->sonstige_flache }}  @endif" name="sonstige_flache" placeholder="Sonstige Fläche">
					                           	<label style="float: right;">Vermietet</label>
					                           	<input type="text" class="form-control mask-number-input" id="rent_whg" value="@if(isset($sheet_property)){{ number_format($sheet_property->rent_whg,2,',','.') }}@endif" name="rent_whg">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Leerstand</label>
					                           	<input type="text" class="form-control mask-number-input" id="vacancy_whg" value="@if(isset($sheet_property)){{ number_format($sheet_property->vacancy_whg,2,',','.') }}@endif" name="vacancy_whg">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Bodenrichtwert in €/m²:</label>
					                           	<input type="text" class="form-control mask-number-input bg-yellow" id="ground_reference_value_in_euro_m2" value="@if(isset($sheet_property)){{ number_format($sheet_property->ground_reference_value_in_euro_m2,2,',','.') }}@endif" name="ground_reference_value_in_euro_m2">
					                        </div>

					                        <div class="col-lg-2">
					                           <label>Exklusivität bis:</label>
					                           <input type="text" class="form-control mask-date-input-new" id="exklusivität_bis" value="@if(isset($sheet_property) && $sheet_property->exklusivität_bis && $sheet_property){{date('d.m.Y',strtotime($sheet_property->exklusivität_bis))}}@endif" name="exklusivität_bis" placeholder="DD.MM.YYYY">
					                        </div>

					                        <div class="clearfix" style="margin-bottom:25px;"></div>

					                        <div class="col-lg-2">
					                           	<label>Netto Miete (IST) p.a. Steigerung p.a. in %:</label><br>
					                           	<input type="text" class="form-control mask-number-input" id="net_rent" value="@if(isset($sheet_property)){{ number_format($sheet_property->net_rent,2,',','.') }}@endif" name="net_rent">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label> Netto Miete (Soll) Steigerung p.a. in %:</label>
					                           	<br><br>
					                           	<input type="text" class="form-control mask-number-input" id="net_rent_empty" value="@if(isset($sheet_property)){{ number_format($sheet_property->net_rent_empty,2,',','.') }}@endif" name="net_rent_empty">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Instandhaltung nichtumlfähig Steigerung p.a. in %:</label>
					                           	<br>
					                           	<input type="text" class="form-control mask-number-input" id="maintenance" value="@if(isset($sheet_property)){{ number_format($sheet_property->maintenance,2,',','.') }}@endif" name="maintenance">
					                        </div>

					                        <div class="col-lg-2">
					                           <label>Betriebsk. nicht umlfähig Steigerung p.a. in %:</label>
					                           <br>
					                           <input type="text" class="form-control mask-number-input" id="operating_costs" value="@if(isset($sheet_property)){{ number_format($sheet_property->operating_costs,2,',','.') }}@endif" name="operating_costs">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Objektverwalt. nichtumlfähig Steigerung p.a. in %:</label>
					                           	<br>
					                           	<input type="text" class="form-control mask-number-input" id="object_management" value="@if(isset($sheet_property)){{ number_format($sheet_property->object_management,2,',','.') }}@endif" name="object_management">
					                        </div>

					                        <div class="col-lg-2">
					                           	<label>Steuern in %:</label>
					                           	<br><br>
					                           	<?php $sheet_property->tax = 17; ?>
					                           	<input readonly="readonly" type="text" class="form-control mask-number-input" id="tax" value="@if(isset($sheet_property)){{ number_format($sheet_property->tax,2,',','.') }}@endif" name="tax">
					                        </div>

					                        <div class="clearfix"></div>

			                     		</div>

			                    		{{--Eighth Row--}}

			                     		<legend>1.) Erwerb- und Erwerbsnebenkosten</legend>
			                     		<!-- <p style="font-weight: bold;">Kaufpreis</p> -->

			                     		{{--Nineth Row--}}

			                     		<div class="col-lg-2">
			                        		<label  style="font-weight: bold;color: red;">Kaufpreis:</label>
			                     		</div>

			                     		<div class="col-lg-2">

			                        		@if(isset($sheet_property) && $sheet_property->Ist)
			                        			{{ number_format($sheet_property->gesamt_in_eur,2,',','.') }}
			                        		@endif

			                        		<input type="@if($sheet_property->Ist){{'hidden'}}@else{{'text'}}@endif" class="form-control mask-number-input" id="gesamt_in_eur" value="@if(isset($sheet_property)){{ number_format($sheet_property->gesamt_in_eur,2,',','.') }}@endif" name="gesamt_in_eur">

			                     		</div>

			                     		<div class="clearfix" style="margin-bottom: 10px;"></div>

					                    <div class="col-lg-2">
					                    	<label>Gebäude in %:</label>
					                    </div>

					                    <div class="col-lg-1">
					                    	<input type="text" class="form-control mask-number-input purchase_price" id="building" value="@if(isset($sheet_property)){{ number_format($sheet_property->building,2,',','.') }}@endif" name="building">
					                    </div>

					                    <div class="col-lg-2">
					                        <input type="text" class="form-control mask-number-input purchase_price" id="building_amount" value="@if(isset($sheet_property)){{ number_format( ($sheet_property->building * $sheet_property->gesamt_in_eur / 100) ,2,',','.') }}@endif">
					                    </div>

					                    <div class="clearfix" style="margin-bottom: 10px;"></div>

					                    <div class="col-lg-2">
					                        <label>Grundstück in %:</label>
					                    </div>

					                    <div class="col-lg-1">
					                        <input type="text" class="form-control mask-number-input purchase_price" id="plot_of_land" value="@if(isset($sheet_property)){{ number_format($sheet_property->plot_of_land,2,',','.') }}@endif" name="plot_of_land">
					                    </div>

					                    <div class="col-lg-2">
					                    	<input type="text" class="form-control mask-number-input purchase_price" id="plot_of_land_amount" value="@if(isset($sheet_property)){{ number_format( ($sheet_property->plot_of_land * $sheet_property->gesamt_in_eur / 100) ,2,',','.') }}@endif">
					                    </div>

			                  		</div>

			                  		<p style="font-weight: bold;">Nebenkosten</p>

			                  		{{--Tenth Row--}}

					                <div class="form-group m-form__group row">

					                    <div class="col-lg-2">
					                        <label>GrunderwerbsSt. in %:</label>
					                    </div>

					                    <div class="col-lg-1">
					                        <input type="text" class="form-control real_estate_taxes mask-number-input" id="real_estate_taxes" value="@if(isset($sheet_property)){{ number_format($sheet_property->real_estate_taxes,2,',','.') }}@endif" name="real_estate_taxes">
					                    </div>

					                    <div class="clearfix" style="margin-bottom: 10px;"></div>

					                    <div class="col-lg-2">
					                    	<label>Makler in %:</label>
					                    </div>

					                    <div class="col-lg-1">
				                        	<input type="text" class="form-control mask-number-input bg-yellow" id="estate_agents" value="@if(isset($sheet_property)){{ number_format($sheet_property->estate_agents,2,',','.') }}@endif" name="estate_agents">
					                    </div>

					                    <div class="clearfix" style="margin-bottom: 10px;"></div>

					                    <div class="col-lg-2">
					                        <label >Notar/Grundbuch in %:</label>
					                    </div>

					                    <div class="col-lg-1">
					                        <input type="text" class="form-control mask-number-input bg-yellow" id="Grundbuch" value="@if(isset($sheet_property)){{ number_format($sheet_property->Grundbuch,2,',','.') }}@endif" name="Grundbuch">
					                    </div>

					                    <div class="clearfix" style="margin-bottom: 10px;"></div>

					                    <div class="col-lg-2">
					                        <label>Due Dilligence in %:</label>
					                    </div>

					                    <div class="col-lg-1">
					                        <input type="text" class="form-control mask-input-number-six-decimal bg-yellow" id="evaluation" value="@if(isset($sheet_property)){{ number_format($sheet_property->evaluation,6,',','.') }}@endif" name="evaluation">
					                    </div>

					                    <div class="clearfix" style="margin-bottom: 10px;"></div>

					                    <div class="col-lg-2">
					                        <input type="text" style="float: left;
					                           width: 77%;" class="form-control bg-yellow" id="miscellaneous_title" value="@if(isset($sheet_property) && $sheet_property->miscellaneous_title){{ $sheet_property->miscellaneous_title }} @else{{'Sonstiges'}}@endif" name="miscellaneous_title">
					                        <label style="margin-top: 6px;
					                           margin-left: 2px;">in %:</label>
					                    </div>

					                    <div class="col-lg-1">
					                        <input type="text" class="form-control mask-input-number-six-decimal bg-yellow" id="others" value="@if(isset($sheet_property)){{ number_format($sheet_property->others,6,',','.') }}@endif" name="others">
					                    </div>

					                    <div class="clearfix" style="margin-bottom: 10px;"></div>

					                    <div class="col-lg-2">
					                        <input type="text" style="float: left;
					                           width: 77%;" class="form-control bg-yellow"  id="buffer_title" value="@if(isset($sheet_property) && $sheet_property->buffer_title){{ $sheet_property->buffer_title }} @else{{'Puffer'}}@endif" name="buffer_title">
					                        <label style="margin-top: 6px;
					                           margin-left: 2px;">in %:</label>
					                    </div>

					                    <div class="col-lg-1">
					                        <input type="text" class="form-control mask-input-number-six-decimal bg-yellow" id="buffer" value="@if(isset($sheet_property)){{ number_format($sheet_property->buffer,6,',','.') }}@endif" name="buffer">
					                    </div>

					                </div>

			                  		{{--Eleventh Row--}}

			                  		<p style="font-weight: bold;">Fläche in m²</p>

			                  		{{--Forteen  Row--}}
			                  		<?php
					                    $D42 = $D46 = $D48 = $D49 = 0;

					                    if($sheet_property){

					                      $D42 = $sheet_property->gesamt_in_eur 
					                          + (($sheet_property->real_estate_taxes/100) * $sheet_property->gesamt_in_eur) 
					                          + (($sheet_property->estate_agents/100) * $sheet_property->gesamt_in_eur) 
					                          + (($sheet_property->Grundbuch * $sheet_property->gesamt_in_eur)/100) 
					                          + (($sheet_property->evaluation/100) * $sheet_property->gesamt_in_eur) 
					                          + (($sheet_property->others/100) * $sheet_property->gesamt_in_eur) 
					                          + (($sheet_property->buffer/100) * $sheet_property->gesamt_in_eur);

					                      $D46 = $D42;

					                      //$D47 = $sheet_property->with_real_ek * $D46;  //C47*D46
					                      $D48 = $sheet_property->from_bond * $D46; //C48*D46
					                      
					                      $D49 = $sheet_property->bank_loan * $D46;
					                    }
					                ?>
			                  		<legend>2.) Finanzierungsstruktur</legend>

					                <div class="form-group m-form__group sheet-section-2">
					                    <div class="col-lg-6">
					                        <div class="row">

					                            <input type="hidden" class="total_purchase_value" value="{{ show_number($D42,2) }}">

					                           <div class="col-lg-4">
					                              <label style="text-align: center">Eigenmittel in %</label>
					                           </div>
					                           <div class="col-lg-4">
					                              <input type="text" class="form-control bg-yellow mask-input-number-ten-decimal from_bond s_purchase_price" id="from_bond{{$property_sheet->id}}" value="@if(isset($sheet_property)){{ number_format($sheet_property->from_bond,10,',','.') }}@endif" name="from_bond">
					                           </div>
					                           <div class="col-lg-4">
					                              <input type="text" class="form-control bg-yellow mask-number-input from_bond_amount s_purchase_price" id="from_bond_amount{{$property_sheet->id}}" value="{{ ($D48) ? number_format($D48/100,2,',','.') : 0 }}">
					                           </div>

					                           <div class="col-lg-4">
					                              <label>Bankkredit in %</label>
					                           </div>
					                           <div class="col-lg-4">
					                              <input type="text" class="form-control bg-yellow mask-input-number-ten-decimal bank_loan s_purchase_price" id="bank_loan{{$property_sheet->id}}" value="@if(isset($sheet_property)){{ number_format($sheet_property->bank_loan,10,',','.') }}@endif" name="bank_loan">
					                           </div>
					                           <div class="col-lg-4">
					                              <input type="text" class="form-control bg-yellow mask-number-input bank_loan_amount s_purchase_price" id="bank_loan_amount{{$property_sheet->id}}" value="{{ ($D49) ? number_format($D49/100,2,',','.') : 0 }}">
					                           </div>

					                           <div class="col-lg-4">
					                              <label>Eigenmittelverzinsung in %</label>
					                           </div>
					                           <div class="col-lg-4">
					                              <input type="text" class="form-control  mask-number-input" id="interest_bond" value="@if(isset($sheet_property)){{ number_format($sheet_property->interest_bond,2,',','.') }}@endif" name="interest_bond">
					                           </div>

					                        </div>
					                    </div>
					                    <div class="col-lg-3">
					                        <div class="row">
					                           	<div class="col-lg-8">
					                              	<label for="">Zins Bankkredit in %</label>
					                           	</div>
					                           	<div class="col-lg-4">
					                              	<input type="text" class="form-control bg-yellow mask-number-input" id="interest_bank_loan" value="@if(isset($sheet_property)){{ number_format($sheet_property->interest_bank_loan,2,',','.') }}@endif" name="interest_bank_loan">
					                           	</div>
					                           	<div class="col-lg-8">
					                              	<label style="text-align: center">Tilgung Bank in %</label>
					                           	</div>
					                           	<div class="col-lg-4">
					                              	<input type="text" class="bg-yellow form-control mask-number-input" id="eradication_bank" value="@if(isset($sheet_property)){{ number_format($sheet_property->eradication_bank,2,',','.') }}@endif" name="eradication_bank">
					                           	</div>
					                        </div>
					                    </div>
					                </div>

			                  		{{--Sixteen row--}}

			                  		<legend>3.) Betrieb und Verw.</legend>

			                  		<p style="font-weight: bold;">Nichtumlagefähige NK</p>

			                  		{{--Seventeen row--}}

					                <div class="form-group m-form__group row sheet-section-2">
					                    <div class="col-lg-2">
					                        <label for="">Instandhaltung nichtumlfähig in %</label>
					                    </div>
					                    <div class="col-lg-1">
					                        <input type="text" class="bg-yellow form-control mask-input-number-six-decimal" id="maintenance_nk" value="@if(isset($sheet_property)){{ number_format($sheet_property->maintenance_nk,6,',','.') }}@endif" name="maintenance_nk">
					                    </div>
					                    <div class="clearfix" style="margin-bottom: 10px;"></div>
					                    <div class="col-lg-2">
					                        <label>Betriebsk. nicht umlfähig in %</label>
					                    </div>
					                    <div class="col-lg-1">
					                        <input type="text" class="bg-yellow form-control mask-input-number-six-decimal" id="operating_costs_nk" value="@if(isset($sheet_property)){{ number_format($sheet_property->operating_costs_nk,6,',','.') }}@endif" name="operating_costs_nk">
					                    </div>
					                    <div class="clearfix" style="margin-bottom: 10px;"></div>
					                    <div class="col-lg-2">
					                        <label>Objektverwalt. nichtumlfähig in %</label>
					                    </div>
					                    <div class="col-lg-1">
					                        <input type="text" class="bg-yellow form-control mask-number-input" id="object_management_nk" value="@if(isset($sheet_property)){{ number_format($sheet_property->object_management_nk,2,',','.') }}@endif" name="object_management_nk">
					                    </div>
					                    <div class="clearfix" style="margin-bottom: 10px;"></div>
					                    <div class="col-lg-2">
					                        <label for="">Abschreibung in %</label>
					                    </div>
					                    <div class="col-lg-1">
					                        <input type="text" class="form-control mask-number-input" id="depreciation_nk" value="@if(isset($sheet_property)){{ number_format($sheet_property->depreciation_nk,2,',','.') }}@endif" name="depreciation_nk">
					                    </div>
					                </div>

			                  		<legend>4.) Exit / Rückzahlung Anleihe</legend>

				                  	<div class="form-group">
				                     	<label for="property_value" class="col-sm-4 control-label">Wertenwicklung Immobilie in %</label>
				                     	<div class="col-sm-4">
				                        	<input type="text" class="form-control mask-number-input" id="property_value" value="@if(isset($sheet_property)){{ number_format($sheet_property->property_value,2,',','.') }}@endif" name="property_value">
				                     	</div>
				                  	</div>

			                  		<legend>5.) Mieterliste</legend>

				                  	<div class="form-group hidden">
				                     	<label for="duration_from_O38" class="col-sm-4 control-label">Laufzeit ab</label>
				                     	<div class="col-sm-4">
				                        	<input type="text" class="form-control mask-date-input-new" id="duration_from_O38" value="@if(isset($sheet_property) && $sheet_property->duration_from_O38 && $sheet_property){{date('d.m.Y',strtotime($sheet_property->duration_from_O38))}}@endif" name="duration_from_O38" placeholder="DD.MM.YYYY">
				                     	</div>
				                  	</div>

                  					<input type="hidden" name="dynamic_list" class="dynamic_list">
				                  	<input type="hidden" name="rent_list" class="rent_list">
				                  	<input type="hidden" name="ank_1_list" class="ank_1_list">
				                  	<input type="hidden" name="ank_2_list" class="ank_2_list">

					                <div class="form-group">
					                    <div class="col-sm-12">
					                        <table class="table">
					                          	<thead>
					                            	<tr>
					                              		<th></th>
					                              		<th width="4%">Ank-1</th>
					                              		<th width="4%">Ank-2</th>
					                              		<th></th>
					                              		<th>Netto Miete</th>
					                              		<th></th>
					                              		<th>MV Ende</th>
					                              		<th>Laufzeit ab</th>
					                              		<th></th>
					                              		<th>Fläche in m²</th>
					                              		<th>Netto Miete p.m</th>
					                              		<th>€/m²</th>
					                              		<th>Wault</th>
					                              		<th></th>
					                            	</tr>
					                          	</thead>
					                          	<tbody class="sheet-tenant-list">
						                            @if(isset($sheet_property))

						                              <?php 

						                                $tr = DB::table('properties_tenants')->whereRaw('propertyId='.$sheet_property->id)->get(); 

						                                $total = $total1 = $total2 = $total_ccc = 0;

						                                foreach($tr as $k=>$propertiesExtra1){
						                                  if($propertiesExtra1->type==config('tenancy_schedule.item_type.live_vacancy') || $propertiesExtra1->type==config('tenancy_schedule.item_type.business_vacancy'))
						                                    continue;


						                                  if($propertiesExtra1->is_current_net)
						                                    $total_ccc += $propertiesExtra1->net_rent_p_a; 

						                                }

						                                if($total_ccc){
						                                  $sheet_property->net_rent_pa = $total_ccc;
						                                }
						                                else{
						                                  $total_ccc = $sheet_property->net_rent_pa;
						                                }  

						                              ?>

						                              @if($tr->count() > 0)
						                                @foreach($tr as $key=>$list)
						                                  <?php
						                                    if($list->is_dynamic_date)
						                                      $list->mv_end2 = date('d.m.Y');
						                                  ?>
						                                  <?php

						                                    if( strpos($sheet_property->duration_from_O38, "unbefristet") !== false){
						                                      $value = 1;
						                                    }else{
						                                      $sheet_property->duration_from_O38 = $list->mv_end2;

						                                      $date1=date_create(str_replace('/', '-', $list->mv_end));
						                                      $date2=date_create(str_replace('/', '-', $list->mv_end2));

						                                      $value = 0;
						                                      if($date1 && $date2){
						                                        $diff=date_diff($date1,$date2);
						                                        $value =  $diff->format("%a")/ 365;
						                                      }

						                                      // $value = ((strtotime(str_replace('/', '-', $list->mv_end)) -  strtotime(str_replace('/', '-', $properties->duration_from_O38))) / 86400) / 365;
						                                    }

						                                    if (strpos($list->mv_end, '2099') !== false)
						                                      $value = 0.5;

						                                    $Q53 = $value*$list->net_rent_p_a;
						                                    $P53 = ($sheet_property->net_rent_pa == 0) ? 0 : ($Q53 / $sheet_property->net_rent_pa);

						                                    if($list->mv_end)
						                                      $list->mv_end = show_date_format(str_replace('/', '.', $list->mv_end));
						                                  ?>

						                                  <tr>
						                                    <td>
						                                      <select class="form-control" name="type[]">
						                                        <option @if($list->type==config('tenancy_schedule.item_type.business')) selected @endif  value="{{config('tenancy_schedule.item_type.business')}}">Gewerbe Vermietet</option>
						                                        <option @if($list->type==config('tenancy_schedule.item_type.live')) selected @endif value="{{config('tenancy_schedule.item_type.live')}}">Wohnen Vermietet</option>
						                                        <option @if($list->type==config('tenancy_schedule.item_type.live_vacancy')) selected @endif  value="{{config('tenancy_schedule.item_type.live_vacancy')}}"> Wohnen Leerstand</option>
						                                        <option @if($list->type==config('tenancy_schedule.item_type.business_vacancy')) selected @endif  value="{{config('tenancy_schedule.item_type.business_vacancy')}}">Gewerbe Leerstand</option>
						                                        <option @if($list->type==config('tenancy_schedule.item_type.free_space')) selected @endif  value="{{config('tenancy_schedule.item_type.free_space')}}">Gewerbe Technische FF & Leerstand strukturell</option>
						                                      </select>
						                                    </td>
						                                    <td>
						                                      <input type="checkbox" name="ank_1[]" class="ank_1" style="margin-top: 12px;" {{ ($list->ank_1 == 1) ? 'checked' : '' }} >
						                                    </td>
						                                    <td>
						                                      <input type="checkbox" name="ank_2[]" class="ank_2" style="margin-top: 12px;" {{ ($list->ank_2 == 1) ? 'checked' : '' }} >
						                                    </td>
						                                    <td>
						                                      <input type="text" class="form-control"  value="{{$list->tenant}}" name="tenant[]" placeholder="Mieter">
						                                    </td>
						                                    <?php
						                                      if($list->net_rent_p_a)
						                                        $total += $list->net_rent_p_a;
						                             
						                                      if($list->flache)
						                                        $total1 += $list->flache;
						                                    ?>
						                                    <td>
						                                      <input type="text" class="form-control mask-number-input-negetive l-date"  value="@if($list->net_rent_p_a){{number_format($list->net_rent_p_a,2,',','.')}}@endif" name="net_rent_p_a[]" placeholder="Netto Kaltmiete p.a.">
						                                    </td>
						                                    <td>
						                                      <input type="checkbox" class="is_current_net-checkbox" @if($list->is_current_net) checked @endif>
						                                    </td>
						                                    <td>
						                                      <input type="text" class="form-control mask-date-input-new"  value="{{$list->mv_end}}" name="mv_end[]"  placeholder="DD.MM.YYYY">
						                                    </td>
						                                    <?php
						                                      if($list->is_dynamic_date)
						                                          $list->mv_end2 = date('d.m.Y');
						                                    ?>
						                                    <td>
						                                      <input type="text" class="form-control mask-date-input-new l-date"  value="{{$list->mv_end2}}" name="mv_end2[]"  placeholder="DD.MM.YYYY">
						                                    </td>
						                                    <td>    
						                                      <input type="checkbox" name="is_dynamic_date[]" class="date-change-checkbox" @if($list->is_dynamic_date) checked @endif>
						                                    </td>
						                                    <td>
						                                      <input type="text" class="mask-number-input form-control"  value="@if($list->flache){{number_format($list->flache,2,',','.')}}@endif" name="flache[]" placeholder="Fläche in m²">
						                                    </td>
						                                    <td class="text-right">
						                                      @if($list->net_rent_p_a)
						                                        {{number_format($list->net_rent_p_a/12,2,',','.')}}
						                                      @endif
						                                    </td>
						                                    <td class="text-right">
						                                      @if($list->flache)
						                                        {{number_format($list->net_rent_p_a/(12*$list->flache),2,',','.')}}
						                                      @endif
						                                    </td>
						                                    <td class="text-right">
						                                      {{-- <span>{{show_number($P53,1)}}</span> --}}
						                                      <span>{{show_number($value,2)}}</span>
						                                    </td>
						                                    <td>
						                                      <input type="text" class="form-control note-input"  value="@if(isset($list->mietvertrag_text)){{$list->mietvertrag_text}}@endif" name="mietvertrag_text[]" placeholder="Notizen">
						                                    </td>
						                                    <td>
						                                      @if($key==0)
						                                        <button type="button" class="btn-success btn-sm add-more-tenant">+</button>
						                                      @else
						                                        <button type="button" class="btn-danger btn-sm remove-ctenant">-</button>
						                                      @endif
						                                    </td>
						                                  </tr>

						                                @endforeach
						                              @else
						                                <tr>
						                                  <td>
						                                    <select class="form-control" name="type[]">
						                                       <option value="{{config('tenancy_schedule.item_type.business')}}">Gewerbe Vermietet</option>
						                                       <option value="{{config('tenancy_schedule.item_type.live')}}">Wohnen Vermietet</option>
						                                       <option value="{{config('tenancy_schedule.item_type.live_vacancy')}}">Wohnen Leerstand</option>
						                                       <option value="{{config('tenancy_schedule.item_type.business_vacancy')}}">Gewerbe Leerstand</option>
						                                       <option value="{{config('tenancy_schedule.item_type.free_space')}}">Gewerbe Technische FF & Leerstand strukturell</option>
						                                    </select>
						                                  </td>
						                                  <td>
						                                    <input type="checkbox" name="ank_1[]" class="ank_1" style="margin-top: 12px;">
						                                  </td>
						                                  <td>
						                                    <input type="checkbox" name="ank_2[]" class="ank_2" style="margin-top: 12px;">
						                                  </td>
						                                  <td>
						                                      <input type="text" class="form-control"  value="" name="tenant[]" placeholder="Mieter">
						                                  </td>
						                                  <td>
						                                      <input type="text" class="form-control mask-number-input-negetive l-date"  value="" name="net_rent_p_a[]" placeholder="Netto Kaltmiete p.a.">
						                                  </td>
						                                  <td>
						                                      <input type="checkbox" class="is_current_net-checkbox" checked>
						                                  </td>
						                                  <td>
						                                      <input type="text" class="form-control mask-date-input-new"  value="" name="mv_end[]" placeholder="DD.MM.YYYY">
						                                  </td>
						                                  <td>
						                                      <input type="text" class="form-control mask-date-input-new l-date"  value="{{date('d.m.Y')}}" name="mv_end2[]" placeholder="DD.MM.YYYY">
						                                  </td>
						                                  <td>
						                                   
						                                      <input type="checkbox" name="is_dynamic_date[]" class="date-change-checkbox" checked>
						                                  </td>
						                                  <td>
						                                      <input type="text" class="form-control mask-number-input" name="flache[]" placeholder="Fläche in m²">
						                                  </td>
						                                  <td></td>
						                                  <td></td>
						                                  <td></td>
						                                  <td>
						                                      <input type="text" class="form-control note-input"  value="" name="mietvertrag_text[]" placeholder="Notizen">
						                                  </td>
						                                  <td>
						                                      <button type="button" class="btn-success btn-sm add-more-tenant">+</button>
						                                  </td>
						                                </tr>
						                              @endif
						                            @endif
					                          	</tbody>
					                          	<tfoot>
					                            	<tr>
					                              		<td colspan="4"></td>
					                              		<td  class="text-right">{{show_number($total,2)}}</td>
					                              		<td colspan="4"></td>
					                              		<td class="text-right">{{show_number($total1,2)}}</td>
					                              		<td class="text-right">{{show_number($total/12,2)}}</td>
					                              		<td colspan="4"></td>
					                            	</tr>
					                          	</tfoot>
				                        	</table>
					                    </div>
					                </div>

			                  		{{--Eighteen row--}}

				                  	<div class="form-group m-form__group row">
				                     	<div class="col-lg-4">
				                        	<label for="">Daten in die Mieterliste kopieren?</label>
				                        	<input type="checkbox" name="copy_data" value="1">
				                     	</div>
				                     	<div class="col-lg-4">
				                        	<label>Ankermieter1  Optionen</label>
				                        	<input type="text" class="form-control" id="ankermieter_option1" value="@if(isset($sheet_property)){{ $sheet_property->ankermieter_option1 }}@endif" name="ankermieter_option1">
				                     	</div>
				                     	<div class="col-lg-4">
				                        	<label>Ankermieter2  Optionen</label>
				                        	<input type="text" class="form-control" id="ankermieter_option2" value="@if(isset($sheet_property)){{ $sheet_property->ankermieter_option2 }}@endif" name="ankermieter_option2">
				                     	</div>
				                  	</div>

		                  			<input type="hidden" name="_token" value="{{ csrf_token() }}" form="importform{{ $id }}">
		                  			<input type="hidden" name="property_id" value="{{ $id }}" form="importform{{ $id }}">
			                  		<input type="hidden" name="main_property_id" value="{{ $property_sheet->main_property_id }}" form="importform{{ $id }}">
			                  		<input type="file" name="select_file" form="importform{{ $id }}">
			                  		<button style="margin-top: 10px;margin-bottom: 10px;" type="submit" class="btn btn-primary" form="importform{{ $id }}">Mieterliste hochladen</button>


			                  		<legend>6.) Lage / Notizen</legend>

			                  		{{--Eighteen row--}}

			                  		<div class="form-group m-form__group row">
				                     	<div class="col-lg-2">
				                        	<label for="">Lage</label>
				                        	<input type="text" class="form-control" id="position" value="@if(isset($sheet_property)){{ $sheet_property->position }}@endif" name="position">
				                     	</div>
				                     	<div class="col-lg-2">
				                        	<label>Notizen</label>
				                        	<input type="text" class="form-control" id="Ankermieter_note" value="@if($comments_new){{ $comments_new->Ankermieter_note }}@endif" name="Ankermieter_note">
				                     	</div>
				                     	<div class="col-lg-2">
				                        	<label>Datum LOI</label>
				                        	@php
				                        		$datum_lol_value = '';
				                        		if(isset($sheet_property) && $sheet_property->datum_lol && $sheet_property){
				                           			$datum_lol_value = date('d.m.Y',strtotime($sheet_property->datum_lol));
				                        		}else{
				                           			if(isset($email_template)){
				                              			$datum_lol_value =  \Carbon\Carbon::parse($email_template->date)->format('d.m.Y');
				                           			}
				                        		}
				                        	@endphp
				                        	<input type="text" class="form-control mask-date-input-new" id="datum_lol" value="{{$datum_lol_value}}" name="datum_lol" >
				                     	</div>
					                    <div class="col-sm-2">
					                        <label for="purchase_date">Tag der Beurkundung</label>
					                        <input type="text" class="form-control mask-date-input-new" id="purchase_date" value="@if(isset($sheet_property) && $sheet_property->purchase_date && $sheet_property){{date('d.m.Y',strtotime($sheet_property->purchase_date))}}@endif" name="purchase_date" placeholder="DD.MM.YYYY">
					                    </div>
					                    <div class="col-sm-2 hidden">
					                        <label>&nbsp;</label>
					                        <input type="text" class="form-control" id="purchase_date2" value="@if(isset($sheet_property)){{ $sheet_property->purchase_date2 }}@endif" name="purchase_date2">
					                    </div>
				                  	</div>

			                  		<div class="form-group">
			                     		<div class="col-sm-offset-4 col-sm-8">
		                        			<button type="submit" class="btn btn-primary save-calculation-sheet">Speichern</button>
			                     		</div>
			                  		</div>

			               		</form>
			            	</div>
			         	</div>

			         	<script>
				            $( "#from_bond{{$property_sheet->id}}" ).keyup(function() {
				                var form_bond_value = $( "#from_bond{{$property_sheet->id}}" ).val();
				                form_bond_value = parseFloat(form_bond_value.replace('.','').replace(',','.'));
				                var value=100-form_bond_value;
				                value=value.toFixed(10);
				                value = value.replace('.',',');
				                $( "#bank_loan{{$property_sheet->id}}" ).val(value);
				            });
				            
				            $( "#bank_loan{{$property_sheet->id}}" ).keyup(function() {
				                var form_bond_value = $( "#bank_loan{{$property_sheet->id}}" ).val();
				                form_bond_value = parseFloat(form_bond_value.replace('.','').replace(',','.'));
				                var value=100-form_bond_value;
				                value=value.toFixed(10);
				                value = value.replace('.',',');
				                $( "#from_bond{{$property_sheet->id}}" ).val(value);
				            });
			         	</script>

			        </div>

		    	@endforeach

		    </div>

		   	<table class="hidden">
  				<tbody class="tenant">
  					<tr>
                        <td>
                          <select class="form-control" name="type[]">
                             <option value="{{config('tenancy_schedule.item_type.business')}}">Gewerbe Vermietet</option>
                             <option value="{{config('tenancy_schedule.item_type.live')}}">Wohnen Vermietet</option>
                             <option value="{{config('tenancy_schedule.item_type.live_vacancy')}}">Wohnen Leerstand</option>
                             <option value="{{config('tenancy_schedule.item_type.business_vacancy')}}">Gewerbe Leerstand</option>
                             <option value="{{config('tenancy_schedule.item_type.free_space')}}">Gewerbe Technische FF & Leerstand strukturell</option>
                          </select>
                       	</td>
                        <td>
                            <input type="checkbox" name="ank_1[]" class="ank_1" style="margin-top: 12px;">
                        </td>
                        <td>
                          	<input type="checkbox" name="ank_2[]" class="ank_2" style="margin-top: 12px;">
                        </td>
                       	<td>
                          	<input type="text" class="form-control"  value="" name="tenant[]" placeholder="Mieter">
                       	</td>
                        <td>
                           	<input type="text" class="form-control mask-number-input-negetive l-date"  value="" name="net_rent_p_a[]" placeholder="Netto Kaltmiete p.a.">
                        </td>
                        <td>
                           	<input type="checkbox" class="is_current_net-checkbox" checked>
                        </td>
                        <td>
                           	<input type="text" class="form-control mask-date-input-new"  value="" name="mv_end[]" placeholder="DD.MM.YYYY">
                        </td>
                        <td>
                           	<input type="text" class="form-control mask-date-input-new l-date"  value="{{date('d.m.Y')}}" name="mv_end2[]" placeholder="DD.MM.YYYY">
                        </td>
                        <td>
                           
                           	<input type="checkbox" name="is_dynamic_date[]" class="date-change-checkbox" checked>
                        </td>
                        <td>
                           	<input type="text" class="form-control mask-number-input" name="flache[]" placeholder="Fläche in m²">
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="text" class="form-control note-input"  value="" name="mietvertrag_text[]" placeholder="Notizen">
                        </td>
                        <td>  
            				<button type="button" class="btn-danger btn-sm remove-ctenant">-</button>
                        </td>
         			</tr>
   				</tbody>
     		</table>

		</div>

  	</div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var url_searchbank = '{{ route("searchbank") }}';
	</script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{asset('js/property/sheet.js')}}"></script>
@endsection