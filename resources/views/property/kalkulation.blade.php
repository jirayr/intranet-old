@extends('layouts.admin')

@section('css')
  <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  <div id="properties" class="tab-pane fade in active">
    
    <div class="row form-inline">
      <div class="col-md-12">

        <div class="form-group">
            <label> EK</label>
            <select name="transaction_m_id" class="change-p-user change_transaction_m_id form-control">
              <option value="">{{__('dashboard.transaction_manager')}}</option>
              @foreach($tr_users as $tr_user)
                <option value="{{$tr_user->id}}" {{ ($properties->transaction_m_id == $tr_user->id) ? 'selected' : '' }} >{{$tr_user->name}}</option>
              @endforeach
            </select>
        </div>

        <div class="form-group">
          <label> AM 1</label>
          <select name="asset_m_id" class="change-p-user change_asset_m_id form-control">
            <option value="">{{__('dashboard.asset_manager')}}</option>
            @foreach($as_users as $as_user)
              <option value="{{$as_user->id}}" {{ ($properties->asset_m_id == $as_user->id) ? 'selected' : '' }} >{{$as_user->name}}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label> AM 2</label>
          <select name="asset_m_id2" class="change-p-user change_asset_m_id2 form-control">
            <option value="">{{__('dashboard.asset_manager')}}</option>
            @foreach($as_users as $as_user)
              <option value="{{$as_user->id}}" {{ ($properties->asset_m_id2 == $as_user->id) ? 'selected' : '' }}>{{$as_user->name}}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label> BL</label>
          <select name="construction_manager" class="change-p-user construction_manager form-control">
            <option value="">{{__('dashboard.asset_manager')}}</option>
            @foreach($as_users as $as_user)
              <option value="{{$as_user->id}}" {{ ($properties->construction_manager == $as_user->id) ? 'selected' : '' }}>{{$as_user->name}}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label> VK</label>
          <select name="seller_id" class="change-p-user change_seller_id form-control">
            <option value="">{{__('dashboard.seller')}}</option>
            @foreach($tr_users as $tr_user)
              <option value="{{$tr_user->id}}"  {{ ($properties->seller_id == $tr_user->id) ? 'selected' : '' }}>{{$tr_user->name}}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <label> Art</label>
          <select name="property_status" class="change-p-user change_property_status form-control">
            <option value="0" {{ ($properties->property_status==0) ? 'selected' : '' }}>intern</option>
            <option value="1" {{ ($properties->property_status==1) ? 'selected' : '' }}>extern</option>
          </select>
        </div>

      </div>
    </div>
    <br>
    <div class="row form-inline">
      <div class="col-md-12">

        <div class="form-group">
          <label>Status</label>
          <select class="property-status form-control" data-property-id="{{$properties->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
            <option value="{{config('properties.status.externquicksheet')}}" @if($properties->status == config('properties.status.externquicksheet')) {!!"selected" !!} @endif>{{__('property.externquicksheet')}}</option>
            <option value="{{config('properties.status.quicksheet')}}" @if($properties->status == config('properties.status.quicksheet')) {!!"selected" !!} @endif>{{__('property.quicksheet')}}</option>
            <option value="{{config('properties.status.in_purchase')}}" @if($properties->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
            <option value="{{config('properties.status.offer')}}" @if($properties->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
            <option value="{{config('properties.status.exclusive')}}" @if($properties->status == config('properties.status.exclusive')) {!!"selected" !!} @endif>{{__('property.exclusive')}}</option>
            <option value="{{config('properties.status.liquiplanung')}}" @if($properties->status == config('properties.status.liquiplanung')) {!!"selected" !!} @endif>{{__('property.liquiplanung')}}</option>
            <option value="{{config('properties.status.exclusivity')}}" @if($properties->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
            <option value="{{config('properties.status.certified')}}" @if($properties->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
            <option value="{{config('properties.status.duration')}}" @if($properties->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
            <option value="{{config('properties.status.hold')}}" @if($properties->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
            <option value="{{config('properties.status.sold')}}" @if($properties->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
            <option value="{{config('properties.status.declined')}}" @if($properties->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
            <option value="{{config('properties.status.lost')}}" @if($properties->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
          </select>
        </div>

        <div class="form-group">
          <select class="set_as_standard form-control">
          <option value="">Als Standard gesetzt</option>
          <?php

            $bank_array = array();
            foreach($banks as $key => $bank)
            {
                $bank_array[] = $bank->id;
            }

            array_unique($bank_array);
            $str = implode(',', $bank_array);

            $properties_banks = DB::table('properties')->select('sheet_title','properties.id','standard_property_status','Ist','soll','banks.name','properties_bank_id')->join('banks','banks.id','=','properties.properties_bank_id')->whereRaw('lock_status=0 and main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('properties_bank_id')->get();
          ?>
          <?php
            $ar = array();
            foreach ($properties_banks as $key1 => $bank1):
          ?>
          @if(!isset($ar[$bank1->properties_bank_id]))
            <optgroup label="{{$bank1->name}}">
              <?php $ar[$bank1->properties_bank_id] = 1; ?>
              @if($key1>0)
            </optgroup>
              @endif
          @endif
          @if($bank1->Ist)
            <option @if($bank1->standard_property_status == 1) selected @endif value="{{ $bank1->id }}">Ist<?php if($bank1->sheet_title)echo "-".$bank1->sheet_title?></option>
          @else
            <option @if($bank1->standard_property_status == 1) selected @endif value="{{ $bank1->id }}">Soll<?php if($bank1->sheet_title) echo "-".$bank1->sheet_title?></option>
          @endif
          <?php endforeach; ?>
          @if($ar)
            </optgroup>
          @endif
          </select>
        </div>

        <div style="float: right;">
          <button type="button" class="btn btn-primary" id="property-zoom-out"><i class="fa fa-search-minus"></i></button>
          <button class="btn btn-default" disabled><span id="property-zoom-value"></span>&nbsp;%</button>
          <button type="button" class="btn btn-primary" id="property-zoom-in"><i class="fa fa-search-plus"></i></button>
        </div>

      </div>
    </div>

    <br/>

    <iframe src="{{route('properties.showIframe', $id)}}" id="propertyIframeParent" width="100%"  style="border:none;min-height: 2800px; "  scrolling="auto"></iframe>

  </div>
</div>

@endsection

@section('js')
  <script type="text/javascript">
    var _token                          = '{{ csrf_token() }}';
    var url_updatepropertyuser          = '{{ route('updatepropertyuser') }}';
    var url_properties_change_status    = '{{ route('properties.change_status') }}';
    var url_property_set_as_standard    = '{{ url('property/set_as_standard') }}/{{ $id }}';
  </script>

  <script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
  <script src="{{asset('js/property/custom.js')}}"></script>
  <script src="{{asset('js/property/kalkulation.js')}}"></script>
@endsection