@extends('layouts.admin')

@section('css')
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
@endsection

@section('content')

@include('property.partial.tab')

<div class="tab-content">
  <div id="default_payer" class="tab-pane fade in active">
  		
  		<div class="row">
			<div class="col-md-12">
				<span id="default_payer_msg"></span>
			</div>
		</div>

		<div class="row white-box">
			<div class="col-md-12">
				<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add_default_payer_modal">
					Neue Excel
				</button>
			</div>
			<br/><br/>
			<div class="col-md-12" style="margin-bottom: 20px;">
				<h3>Offene Posten</h3>
				<table class="table table-striped" id="default_payer_table" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>#</th>
							<th>Jahr</th>
							<th>Monat</th>
							<th>Keine OPOS</th>
							<th>Excel</th>
							<th>Kommentar</th>
							<th>User</th>
							<th>Datum</th>
							<th>Kommentar Falk</th>
							<th>Aktion</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>

		<div class=" modal fade" role="dialog" id="add_default_payer_modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Neue Excel</h4>
					</div>
					<form method="post" enctype="multipart/form-data" id="add_default_payer_form">
						<div class="modal-body">

							<span id="add_default_payer_msg"></span>

							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="hidden" name="property_id" value="{{$properties->id}}">
							<input type="hidden" name="file_basename" id="default_payer_gdrive_file_basename" value="">
							<input type="hidden" name="file_dirname" id="default_payer_gdrive_file_dirname" value="">
							<input type="hidden" name="file_type" id="default_payer_gdrive_file_type" value="">
							<input type="hidden" name="file_name" id="default_payer_gdrive_file_name" value="">
							<input type="hidden" name="current_tab_name" id="current_tab_name_payer" value="Verzugszahler">

							<label>Excel</label> <span id="default_payer_gdrive_file_name_span"> </span>
							<a href="javascript:void(0);" class="link-button-gdrive-default-payer btn btn-info">Datei auswählen</a>


		                    <a class="default-payer-upload-file-icon btn btn-info" href="javascript:void(0);">Hochladen</a>
		                    <input type="file" name="default_payer_gdrive_file_upload" class="hide default-payer-gdrive-upload-file-control" id="default_payer_gdrive_file_upload" >
							<br>

							<label>Jahr</label>
							<select name="year" class="form-control">
								@for ($i = date('Y'); $i < date('Y', strtotime('+5 year')); $i++)
									<option value="{{ $i }}" {{ (date('Y') == $i) ? 'selected' : '' }} >{{ $i }}</option>
								@endfor
							</select>
							<br>

							<label>Monat</label>
							<select name="month" class="form-control">
								@for ($i = 1; $i <= 12; $i++)
									<option value="{{ $i }}" {{ (date('m') == $i) ? 'selected' : '' }} >{{ sprintf('%02d', $i) }}</option>
								@endfor
							</select>
							<br>
							
							<label>Kommentar</label>
							<textarea class="form-control" name="comment"></textarea>
							<br>

							<input type='checkbox' class="" name="no_opos_status" >
							<label>Keine OPOS</label>
							<br>


						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" >Speichern</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
						</div>
					</form>
				</div>

			</div>
		</div>

		@include('property.partial.comman_modal')

  </div>
</div>

@endsection

@section('js')
	<script type="text/javascript">
		var _token                				= '{{ csrf_token() }}';
		var workingDir 							= '{{$workingDir}}';
		var lfm_route    		  				= '{{ url("file-manager") }}';
		var url_get_default_payer 				= '{{ route('get_default_payer', ['property_id' => $properties->id]) }}';
		var url_add_default_payer 				= '{{ route('add_default_payer') }}';
	</script>
  	<script src="{{asset('js/property/custom.js')}}"></script>
  	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
  	<script src="{{ asset('file-manager/js/bootbox.min.js') }}"></script>
	<script src="{{ asset('file-manager/js/script.js') }}"></script>
  	<script src="{{asset('js/property/default_payer.js')}}"></script>
@endsection