<html>
<style type="text/css">
@page { margin: 0px; }
body { margin: 0px;
    font-family: arial,sans-serif !important;
}

.table-border-bottom td{
    padding: 2px;
    border-bottom: 2px solid #cdcdcd;
    font-size: 13px;
}
</style>
<body>

    <?php

    $a = getobjekttypearray();

    $page = 2;
    if(isset($data['img']) && count($data['img'])>1)
    $page = 3;
    if(isset($data['img']) && count($data['img'])>3)
        $page = 4;
    if(isset($data['img']) && count($data['img'])>5)
        $page = 5;
    if(isset($data['img']) && count($data['img'])>7)
        $page = 6;


    $table_arr = array();
    $table_arr2 = array();


                if($data['type'] && isset($a[$data['type']])){
                    $table_arr['name'][] = 'Objektart:';
                    $table_arr['value'][] = $a[$data['type']];
                }


                if($data['street'] && isset($data['street'])){
                    $table_arr['name'][] = 'Straße:';
                    $table_arr['value'][] = $data['street'];
                }

                if($data['postcode'] && isset($data['postcode'])){
                    $table_arr['name'][] = 'Ort:';
                    $table_arr['value'][] = $data['postcode'].' '.$data['ad_city_name'];
                }
                if($data['price'] && isset($data['price'])){
                    $table_arr['name'][] = 'Kaufpreis in €:';
                    $table_arr['value'][] = number_format($data['price'],2,",",".");
                }
                if($data['square_unit_space'] && isset($data['square_unit_space'])){
                    $table_arr['name'][] = 'Grundstücksfläche in m²:';
                    $table_arr['value'][] = number_format($data['square_unit_space'],2,",",".");
                }
                if($data['rentable_area'] && isset($data['rentable_area'])){
                    $table_arr['name'][] = 'Vermietbare Fläche in m²:';
                    $table_arr['value'][] = number_format($data['rentable_area'],2,",",".");
                }
                if($data['commercial_space'] && isset($data['commercial_space'])){
                    $table_arr['name'][] = 'Gewerbefläche in m²:';
                    $table_arr['value'][] = number_format($data['commercial_space'],2,",",".");
                }
                if($data['living_space'] && $data['living_space']>0){
                    $table_arr['name'][] = 'Wohnfläche in m²:';
                    $table_arr['value'][] = number_format($data['living_space'],2,",",".");
                }
                if($data['total_area'] && isset($data['total_area'])){
                    $table_arr['name'][] = 'Gesamtfläche m²:';
                    $table_arr['value'][] = number_format($data['total_area'],2,",",".");
                }
                if($data['current_state'] && isset($data['current_state'])){
                    $table_arr['name'][] = 'Zustand:';
                    $table_arr['value'][] = $data['current_state'];
                }
                if($data['const_year'] && isset($data['const_year'])){
                    $table_arr['name'][] = 'Baujahr:';
                    $table_arr['value'][] = $data['const_year'];
                }


                if($data['stellplatze']>0 && isset($data['stellplatze'])){
                    $table_arr2['name'][] = 'Stellplätze:';
                    $table_arr2['value'][] = number_format($data['stellplatze'],2,",",".");
                }
                if($data['heating'] && isset($data['heating'])){
                    $table_arr2['name'][] = 'Heizungsart:';
                    $table_arr2['value'][] = $data['heating'];
                }
                if($data['energy_available'] && isset($data['energy_available'])){
                    $table_arr2['name'][] = 'Energieausweis liegt vor:';
                    $table_arr2['value'][] = $data['energy_available'];
                }
                if($data['energy_value'] && isset($data['energy_value'])){
                    $table_arr2['name'][] = 'Energiekennwert:';
                    $table_arr2['value'][] = number_format($data['energy_value'],2,",",".");
                }

                if($data['energy_valid'] && isset($data['energy_valid'])){
                    $table_arr2['name'][] = 'Energieausweis gültig bis:';
                    $table_arr2['value'][] = $data['energy_valid'];
                }

                if($data['provision'] && isset($data['provision'])){
                    $table_arr2['name'][] = 'Provision:';
                    $table_arr2['value'][] = $data['provision'];
                }


                if($data['annual_rent'] && isset($data['annual_rent'])){
                    $table_arr2['name'][] = 'Jahresnettomiete in €:';
                    $table_arr2['value'][] = number_format($data['annual_rent'],2,",",".");
                }

                if($data['tenants'] && isset($data['tenants'])){
                    $table_arr2['name'][] = 'Ankermieter:';
                    $table_arr2['value'][] = $data['tenants'];
                }
                if($data['equity'] && isset($data['equity'])){
                    $table_arr2['name'][] = 'Mietrendite in %:';
                    $table_arr2['value'][] = number_format($data['equity'],2,",",".");
                }
                if($data['wault'] && isset($data['wault'])){
                    $table_arr2['name'][] = 'WAULT:';
                    $table_arr2['value'][] = number_format($data['wault'],2,",",".");
                }


    ?>

    <div id="" style="width: 100%; position: relative;">
        <div id="" style="padding: 25px 25px 0 25px;">
        <div style="width: 400px; margin: auto;">
            <img src="{{ getcwd() }}/img/logo.png" width="400">
        </div>
        <div style="margin-top: 40px; min-height: 280px;">
            @if(isset($data['img'][0]) && $data['img'][0] != '')
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][0]}}" style="width: 530px; float: left; object-fit: cover;">
            @else
                <img src="{{ getcwd() }}/pdf_images/not-found.png" style="width: 530px; float: left;object-fit: cover;">
            @endif
            @if(isset($data['img'][1]) && $data['img'][1] != '')
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][1]}}" style="width: 180px; margin: 0 0 20px 30px; object-fit: cover;">
            @endif
            
            
            @if(isset($data['img'][2]) && $data['img'][2] != '')
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][2]}}" style="width: 180px; margin: 0 0 20px 30px; object-fit: cover;">
            @endif

            @if(isset($data['img'][3]) && $data['img'][3] != '')
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][3]}}" style="width: 180px; margin: 0 0 20px 30px; object-fit: cover;">
            @endif
        </div>

        
        <div style="margin-top: 40px;clear: both;">

        <p>
            @if($data['type'] && isset($a[$data['type']]))
            {{$a[$data['type']]}}
            @endif

            @if($data)
            | {{$data['postcode']}} {{$data['ad_city_name']}}
            @endif
        </p>

        <p style="font-size: 24px;">
             {{ $data['title'] }} </p>

        {{-- <p style="width: 530px; font-size: 14px;">
        @if(isset($data['type']) && $data['type'] != '') {{ $data['type'] }} @endif @if(isset($data['ad_city_name']) && $data['ad_city_name'] != '')│ {{ $data['ad_city_name'] }} @endif</p> --}}
            
        </div>
        <?php
        $c = 0;
        if(isset($table_arr['name']))
        $c = count($table_arr);
        if(isset($table_arr2['name']) && $c<count($table_arr2['name']))
            $c = count($table_arr2['name']);
        ?><div style="margin-top: 20px; ">
        <table class="table-border-bottom" cellspacing="0">
            @for($i=0;$i<$c;$i++)
            <tr style="width: 100%;">
                <td  style="width: 160px;">@if(isset($table_arr['name'][$i])) {{$table_arr['name'][$i]}} @endif</td>
                <td style="width: 160px;text-align: right;">@if(isset($table_arr['value'][$i])) {{$table_arr['value'][$i]}} @endif</td>
                <td style="width: 60px; border-bottom: none;">&nbsp;</td>
                <td style="width: 160px;">@if(isset($table_arr2['name'][$i])) {{$table_arr2['name'][$i]}} @endif</td>
                <td style="width: 160px;text-align: right;">@if(isset($table_arr2['value'][$i])) {{$table_arr2['value'][$i]}} @endif</td>
            </tr>
            @endfor
        </table>
 
            
        </div>
        </div>
        <br>
        <div class="clearfix"></div>

        <div style="padding: 25px; bottom: 130px; position: absolute;">
        <p style="font-size: 13px; margin: 10px 0 0; width: 730px">Alle Angaben nach bestem Wissen. Irrtum und Zwischenverkauf vorbehalten. Dieses Exposé ist eine Vorinformation, als Rechtsgrundlage gilt allein der notariell abgeschlossene Kaufvertrag.</p>
        <p style="text-align: right; padding-right: 80px; margin: 0 0 0;">© FCR Immobilien AG</p>
        </div>

        <div style="height: 100px; position: absolute; bottom: 0; width: 100%; background: #dddddd; padding: 25px;">
            
        
            <div style="width: 350px; float: left; font-size: 13px;">
                <p>FCR Immobilien AG</p>
                <p style="font-size: 13px;">Paul-Heyse-Str. 28 | 80336 München<br>
                Tel.: +49 89 413 2496 00 | Fax: +49 89 413 2496 99<br>
                info@fcr-immobilien.de | www.fcr-immobilien.de</p>
            </div>
            <div style="width: 330px; float: right;">
                <h1 style="color: white; font-size: 90px; margin: -10px 0 0; font-weight: normal;">EXPOSE</h1>
            </div>
        </div>
        
    </div>
    <div style="page-break-before: always;"></div>

    <div id="page_2" style="margin-top: 50px; width: 800px; position: relative;">
        <div id="page2_main">

            <div style="width: 350px; float: right;margin-right: 100px;">
            <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>
            
            
            @if(isset($data['note']) && $data['note'] != '')
            <div style="clear: both;">&nbsp;</div>
            <div style="float:left;width: 200px;"><p style="text-align: right;">HINWEIS<p></div>
            <div style="float:left;width: 580px;">
            <p style="text-align: left;font-size: 14px;padding-left: 50px;">
                    {!! nl2br($data['note']) !!} 
                </p>
            </div>
            @endif 

            @if(isset($data['description']) && $data['description'] != '')
            <div style="clear: both;">&nbsp;</div>
            <div style="float:left;width: 200px;"><p style="text-align: right;">OBJEKTBESCHREIBUNG<p></div>
            <div style="float:left;width: 580px;">
            <p style="text-align: left;font-size: 14px;padding-left: 50px;">
                    {!! nl2br($data['description']) !!} 
                </p>
            </div>
            @endif 

            @if(isset($data['location']) && $data['location'] != '')
            <div style="clear: both;">&nbsp;</div>

            <div style="float:left;width: 200px;"><p style="text-align: right;">LAGE<p></div>
            <div style="float:left;width: 580px;">
            <p style="text-align: left;font-size: 14px;padding-left: 50px;">
                    {!! $data['location'] !!} 
                   
                </p>
            </div>
            @endif 

            @if(isset($data['domestic_equipments']) && $data['domestic_equipments'] != '')
            <div style="clear: both;">&nbsp;</div>
            <div style="float:left;width: 200px;"><p style="text-align: right;">AUSSTATTUNG<p></div>
            <div style="float:left;width: 580px;">
            <p style="text-align: left;font-size: 14px;padding-left: 50px;">
                    {!! nl2br($data['domestic_equipments']) !!} 
                </p>
            </div>
            @endif 

            <?php
            $u = "";
            if(isset($data['user_id']) && $data['user_id'] != '')
            {
                $u = \DB::table('users')->where('id', $data['user_id'])->first();
            }
            ?>


            @if($u && $u->name)
            <div style="clear: both;">&nbsp;</div>
            <div style="float:left;width: 200px;"><p style="text-align: right;">Kontakt<p></div>
            <div style="float:left;width: 580px;">
                <p style="text-align: left;font-size: 14px;padding-left: 50px;">
                    {!! $u->name !!} 
                </p>
                <p style="text-align: left;font-size: 14px;padding-left: 50px;">
                        {!! $u->email !!}
                </p>
                <p style="text-align: left;font-size: 14px;padding-left: 50px;">
                    {!! $u->phone !!}
                </p>
            </div>
            @endif
            @if(isset($data['sonstiges']) && $data['sonstiges'] != '')
            <div style="clear: both;">&nbsp;</div>
            <div style="float:left;width: 200px;"><p style="text-align: right;">Sonstiges<p></div>
            <div style="float:left;width: 580px;">
            <p style="text-align: left;font-size: 14px;padding-left: 50px;">
                    {!! nl2br($data['sonstiges']) !!}
                </p>
            </div>
            @endif



            <div style="clear: both;">&nbsp;</div>
            

            
            <div style="width: 500px; padding: 25px 0 25px 25px; font-size: 13px; position: absolute; right: 30px;">
                <div style="height: 100px; position: absolute; bottom: -30px; width: 100%;  padding: 25px;">
                        <div style="width: 480px; border-top: 1px dotted;"></div>
                        <div style="width: 310px; float: left;">
                            <p>
                                <strong>FCR Immobilien AG</strong><br>
                                Tel.: +49 89 413 2496 00 | Fax: +49 89 413 2496 99<br>
                                info@fcr-immobilien.de | www.fcr-immobilien.de
                            </p>
                        </div>
                        <div style="width: 190px; float: right; text-align: right;">
                            <p>
                                <br>
                                <strong>Seite 2/{{$page}}</strong><br>
                                {{date('d')}}. {{__('dashboard.'.date('F',strtotime('2000-'.date('m').'-01')))}} {{date('Y')}}
                            </p>
                        </div>

                </div>
                
            </div>
            
            
        </div>
    </div>
    @if(isset($data['img'][1]) && $data['img'][1] != '')
    <div style="page-break-before: always;"></div>


    <div id="page_2" style="margin-top: 100px; position: relative; width: 800px;">
        <div id="page2_main">

            <div style="width: 350px; float: right;margin-right: 100px;">
            <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>

            <div style="clear: both;">&nbsp;</div>

            <div style="width: 370px; position: absolute; right: 50px; padding: 10px 0 25px 25px;">
                

            @if(isset($data['img'][1]) && $data['img'][1] != '')
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][1]}}" style="width: 400px; object-fit: cover; position: absolute; margin: 50px auto; left: -50;">
            @endif
            </div>
            <div style="width: 370px; position: absolute; right: 50px; padding: 10px 0 25px 25px;margin-top:10px;">
            @if(isset($data['img'][2]) && $data['img'][2] != '')
                
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][2]}}" style="width: 400px; object-fit: cover; position: absolute; margin: 420px auto 30px; left: -50;">
            @endif

                
            
            <div style="width: 500px; padding: 25px 0 25px 25px; font-size: 13px; position: absolute; right: 30px;">
                <div style="height: 100px; position: absolute; bottom: -30px; width: 100%;  padding: 25px;">
                        <div style="width: 480px; border-top: 1px dotted;"></div>
                        <div style="width: 310px; float: left;">
                            <p>
                                <strong>FCR Immobilien AG</strong><br>
                                Tel.: +49 89 413 2496 00 | Fax: +49 89 413 2496 99<br>
                                info@fcr-immobilien.de | www.fcr-immobilien.de
                            </p>
                        </div>
                        <div style="width: 190px; float: right; text-align: right;">
                            <p>
                                <br>
                                <strong>Seite 3/{{$page}}</strong><br>
                                {{date('d')}}. {{__('dashboard.'.date('F',strtotime('2000-'.date('m').'-01')))}} {{date('Y')}}
                            </p>
                        </div>

                </div>
                
            </div>
                
                
        
            </div>
            
    
        </div>
    </div>
    @endif

    @if(isset($data['img'][3]) && $data['img'][3] != '')

    <div style="page-break-before: always;"></div>
    <div id="page_2" style="margin-top: 100px; position: relative; width: 800px;">
        <div id="page2_main">

            <div style="width: 350px; float: right;margin-right: 100px;">
            <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>

            <div style="clear: both;">&nbsp;</div>

            <div style="width: 470px; position: absolute; right: 50px; padding: 10px 0 25px 25px;">
                

            @if(isset($data['img'][3]) && $data['img'][3] != '')
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][3]}}" style="width: 560px; height: 340px; object-fit: cover; position: absolute; margin: 50px auto; left: -100;">
            @endif

            @if(isset($data['img'][4]) && $data['img'][4] != '')
                
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][4]}}" style="width: 560px; height: 340px; object-fit: cover; position: absolute; margin: 420px auto 30px; left: -100;">
            @endif

                
            
            <div style="width: 500px; padding: 25px 0 25px 25px; font-size: 13px; position: absolute; right: 30px;">
                <div style="height: 100px; position: absolute; bottom: -30px; width: 100%;  padding: 25px;">
                        <div style="width: 480px; border-top: 1px dotted;"></div>
                        <div style="width: 310px; float: left;">
                            <p>
                                <strong>FCR Immobilien AG</strong><br>
                                Tel.: +49 89 413 2496 00 | Fax: +49 89 413 2496 99<br>
                                info@fcr-immobilien.de | www.fcr-immobilien.de
                            </p>
                        </div>
                        <div style="width: 190px; float: right; text-align: right;">
                            <p>
                                <br>
                                <strong>Seite 4/{{$page}}</strong><br>
                                {{date('d')}}. {{__('dashboard.'.date('F',strtotime('2000-'.date('m').'-01')))}} {{date('Y')}}
                            </p>
                        </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    @endif



    @if(isset($data['img'][5]) && $data['img'][5] != '')

    <div style="page-break-before: always;"></div>
    <div id="page_2" style="margin-top: 100px; position: relative; width: 800px;">
        <div id="page2_main">

            <div style="width: 350px; float: right;margin-right: 100px;">
            <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>
            <div style="clear: both;">&nbsp;</div>

            <div style="width: 470px; position: absolute; right: 50px; padding: 10px 0 25px 25px;">
                

            @if(isset($data['img'][5]) && $data['img'][5] != '')
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][5]}}" style="width: 560px; height: 340px; object-fit: cover; position: absolute; margin: 50px auto; left: -100;">
            @endif

            @if(isset($data['img'][6]) && $data['img'][6] != '')
                
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][6]}}" style="width: 560px; height: 340px; object-fit: cover; position: absolute; margin: 420px auto 30px; left: -100;">
            @endif

                
            
            <div style="width: 500px; padding: 25px 0 25px 25px; font-size: 13px; position: absolute; right: 30px;">
                <div style="height: 100px; position: absolute; bottom: -30px; width: 100%;  padding: 25px;">
                        <div style="width: 480px; border-top: 1px dotted;"></div>
                        <div style="width: 310px; float: left;">
                            <p>
                                <strong>FCR Immobilien AG</strong><br>
                                Tel.: +49 89 413 2496 00 | Fax: +49 89 413 2496 99<br>
                                info@fcr-immobilien.de | www.fcr-immobilien.de
                            </p>
                        </div>
                        <div style="width: 190px; float: right; text-align: right;">
                            <p>
                                <br>
                                <strong>Seite 5/{{$page}}</strong><br>
                                {{date('d')}}. {{__('dashboard.'.date('F',strtotime('2000-'.date('m').'-01')))}} {{date('Y')}}
                            </p>
                        </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    @endif


    @if(isset($data['img'][7]) && $data['img'][7] != '')

    <div style="page-break-before: always;"></div>
    <div id="page_2" style="margin-top: 100px; position: relative; width: 800px;">
        <div id="page2_main">

            <div style="width: 350px; float: right;margin-right: 100px;">
            <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>

            <div style="clear: both;">&nbsp;</div>

            <div style="width: 470px; position: absolute; right: 50px; padding: 10px 0 25px 25px;">
                

            @if(isset($data['img'][7]) && $data['img'][7] != '')
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][7]}}" style="width: 560px; height: 340px; object-fit: cover; position: absolute; margin: 50px auto; left: -100;">
            @endif

            @if(isset($data['img'][8]) && $data['img'][8] != '')
                
                <img src="{{ getcwd() }}/ad_files_upload/{{$data['img'][8]}}" style="width: 560px; height: 340px; object-fit: cover; position: absolute; margin: 420px auto 30px; left: -100;">
            @endif

                
            
            <div style="width: 500px; padding: 25px 0 25px 25px; font-size: 13px; position: absolute; right: 30px;">
                <div style="height: 100px; position: absolute; bottom: -30px; width: 100%;  padding: 25px;">
                        <div style="width: 480px; border-top: 1px dotted;"></div>
                        <div style="width: 310px; float: left;">
                            <p>
                                <strong>FCR Immobilien AG</strong><br>
                                Tel.: +49 89 413 2496 00 | Fax: +49 89 413 2496 99<br>
                                info@fcr-immobilien.de | www.fcr-immobilien.de
                            </p>
                        </div>
                        <div style="width: 190px; float: right; text-align: right;">
                            <p>
                                <br>
                                <strong>Seite 6/{{$page}}</strong><br>
                                {{date('d')}}. {{__('dashboard.'.date('F',strtotime('2000-'.date('m').'-01')))}} {{date('Y')}}
                            </p>
                        </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    @endif

     
    {{-- <div style="page-break-before: always;"></div>

    <div id="page_4" style="margin-top: 50px; width: 800px; position: relative;">
        <div id="page4_main">
            <div style="width: 200px;position: absolute; left: 0; background: #dddddd; padding: 25px;">
                <div style="height: 650px;">
                    <p style="position: absolute; top:80px; right: 15px;"><p>
                </div>
                <div style="height: 100px;">
                    <p style="transform-origin: 0 0;transform: rotate(-90deg); float: right;margin-right: -140px;margin-top: 330px;">© FCR Immobilien AG</p>
                </div>
                                        
            </div>
            <div style="width: 500px; position: relative; height: 700px; float: right; margin-right: 25px; padding: 10px 0 25px 25px;">
                <div style="width: 250px; margin: auto;">
                <img src="{{ getcwd() }}/img/logo.png" width="250">
                </div>
                <p style="margin-top: 40px; font-size: 13px;">@if(isset($data['type']) && $data['type'] != '') {{ $data['type'] }} @endif @if(isset($data['ad_city_name']) && $data['ad_city_name'] != '')│ {{ $data['ad_city_name'] }} @endif</p>
                <p style="font-size: 24px;">@if(isset($data['ad_title']) && $data['ad_title'] != '') {{ $data['ad_title'] }} @endif</p>
                <p style="border-bottom: 1px dotted; font-size: 13px; padding-bottom: 15px;">@if(isset($data['ad_title']) && $data['ad_title'] != '') {{ $data['ad_title'] }} @endif</p>
                <div style="position: absolute; top: 950px; width: 490px font-size: 13px; right: 35px;">
                <div style="width: 480px; border-top: 1px dotted;"></div>
                <div style="width: 250px; float: left;">
                    <p style="font-size: 13px;">
                        <strong>FCR Immobilien AG</strong><br>
                        Tel.: +49 89 413 2496 00 | Fax: +49 89 413 2496 99<br>
                        info@fcr-immobilien.de | www.fcr-immobilien.de
                    </p>
                </div>
                <div style="width: 200px; float: right; text-align: right;">
                    <p style="font-size: 13px;">
                        <strong></strong><br>
                        <br>
                        
                    </p>
                </div>
                </div>
                
            </div>
            
        </div>
    </div> 
    --}}

    

    
</body>

</html>

<script src="{{ public_path() }}/js/sha512.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="{{ public_path() }}/domjs/jspdf.min.js "></script>
<script type="text/javascript" src="{{ public_path() }}/domjs/jquery-git.js"></script>
