<style type="text/css">
.pr-table{
    font-size: 12px;
    font-family: arial,sans-serif;
    text-align: center;
}

h1,h2,h3,p{
    font-family: arial,sans-serif;
}

.pr-table thead th{
    background: #7A8C8E;
    color: white;
    font-weight: bold;
    padding: 5px;
}
.pr-table tbody td{
    padding: 3px;
    padding-top: 5px;
    padding-bottom: 5px;
}


.ads-table thead th{
    background: black;
    color: white;
    font-weight: bold;
    padding: 8px;
}
.ads-table tbody td{
    font-size: 12px;
    padding: 2px;

}
.text-bold{
    font-weight: bold;
}
.text-right{
    text-align: right;
}
.text-center{
    text-align: center;
}
.ads-table tbody tr:nth-child(even) {background-color: #f2f2f2;}
.pr-table tbody tr:nth-child(even) {background-color: #D7DBDB;}
.pr-table tbody tr:nth-child(odd) {background-color: #ECEEEE;}
</style>
<h3 class="text-center">Transaction Meeting-Auszug Intranet</h3>
<p class="text-right">Datum: {{date('d.m.Y')}}</p>



<h2>Einkauf</h2>
<h3 class="box-title">{{__('dashboard.status')}}: {{__('property.exclusive')}}</h3>

<div class="pr-table">
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>#</th>
                <th>PLZ</th>
                <th>Ort</th>
                <th>TM</th>
                <th>Liqui Freig.</th>
                <th>EK Preis</th>
                <th>{{__('dashboard.total_purchase_price')}}</th>
                <th>{{__('dashboard.gross_return')}}</th>
                <th>D. Exkl.</th>
            </tr>
        </thead>
        <tbody>
            <?php $sum1 = $sum2 = $sum3 = 0;?>
        	@foreach($array[14] as $property)
        	<?php


                            if(!isset($property->bank_ids)){  continue; }


                            $propertiesExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();
                            $total_ccc = 0;

                            $neitoo = 0;

                            ?>
                            @foreach($propertiesExtra1s as $propertiesExtra1)
                                <?php
                                if($propertiesExtra1->is_current_net)
                                $total_ccc += $propertiesExtra1->net_rent_p_a;
                                ?>
                            @endforeach
                            <?php 
                            if($total_ccc)
                                $property->net_rent_pa = $total_ccc;
                            else
                                $total_ccc = $property->net_rent_pa;


                            $property->net_rent_increase_year1 = $total_ccc;
                    $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1*$property->net_rent;
                    $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2*$property->net_rent;
                    $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3*$property->net_rent;
                    $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4*$property->net_rent;


                    $property->operating_cost_increase_year1 = $property->net_rent_increase_year1*$property->operating_costs_nk;

                    $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1*$property->operating_costs;

                    // echo $property->operating_cost_increase_year1;
                    // echo "";
                    // echo $property->operating_costs_nk;



                    $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2*$property->operating_costs;

                    $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3*$property->operating_costs;
                    $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4*$property->operating_costs;


                    $property->property_management_increase_year1 = $property->net_rent_increase_year1*$property->object_management_nk;

                    $property->property_management_increase_year2 =$property->property_management_increase_year1 +  $property->property_management_increase_year1*$property->object_management;

                    $property->property_management_increase_year3 =$property->property_management_increase_year2 +  $property->property_management_increase_year2*$property->object_management;

                    $property->property_management_increase_year4 =$property->property_management_increase_year3 +  $property->property_management_increase_year3*$property->object_management;

                    $property->property_management_increase_year5 =$property->property_management_increase_year4 +  $property->property_management_increase_year4*$property->object_management;


                    $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                    $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                    $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                    $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                    $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                     $property->ebit_year_1= $property->ebitda_year_1 - $property->depreciation_nk_money;
                     $property->ebit_year_2= $property->ebitda_year_2 - $property->depreciation_nk_money;
                     $property->ebit_year_3= $property->ebitda_year_3 - $property->depreciation_nk_money;
                     $property->ebit_year_4= $property->ebitda_year_4 - $property->depreciation_nk_money;
                     $property->ebit_year_5= $property->ebitda_year_5 - $property->depreciation_nk_money;




                            /*if(json_decode($property->bank_ids,true)){
                            $bank_ids = json_decode($property->bank_ids,true);
                            if($bank_ids != null) {
                                foreach ($banks as $b) {
                                    if(isset($bank_ids[0]) && $b->id == $bank_ids[0]){
                                        $bank = $b;
                                        break;
                                    }
                                }
                            }
                            }*/



                            $D42 = $property->gesamt_in_eur
                                + ($property->real_estate_taxes * $property->gesamt_in_eur)
                                + ($property->estate_agents * $property->gesamt_in_eur)
                                + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                                + ($property->evaluation * $property->gesamt_in_eur)
                                + ($property->others * $property->gesamt_in_eur)
                                + ($property->buffer * $property->gesamt_in_eur);


                            $L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                            $price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

                            // if(isset($bank)){
                                $E18 = ($property->net_rent_increase_year1
                                        -$property->maintenance_increase_year1
                                        -$property->operating_cost_increase_year1
                                        -$property->property_management_increase_year1)
                                    -$property->depreciation_nk_money;

                                $D49 = $property->bank_loan * $D42;
                                $D48 = $property->from_bond * $D42;
                                $H48 = $D49 * $property->interest_bank_loan;
                                $H49 = $D49 * $property->eradication_bank;
                                $H52 = $D48 * $property->interest_bond;

                                $E23 = $E18- $H48 -$H52;
                                $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                                $G61 = ($D48 == 0) ? 0 : $E31/$D48;
                                $G62 = ($D48 == 0) ? 0 : $E23/$D48;
                            // }
                            $G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;

                            if(!isset($G61)){
                                $G61 = 0;
                            }
                            ?>
                        <tr>
                                <td class="text-center">{{$property->property_actual_id}}</td>

                                <td class="" >{{$property->plz_ort}}</td>
                                <td class="" >{{$property->ort}} </td>
                                <td>@php
                                        $user =  DB::table('properties')->join('users','users.id','=','properties.transaction_m_id')->where('properties.id', $property->property_actual_id)->first();
                                         if($user){
                                             echo user_short_name($user->name);
                                         }
                                    @endphp</td>
                                <td>@php
                                        $check =  DB::table('properties_buy_details')->where('property_id', $property->property_actual_id)->where('type','btn2')->first();
                                         if($check){
                                             echo "Ja";
                                         }
                                         else{
                                         echo "Nein";
                                        }
                                    @endphp</td>
                                
                                <?php
                                    $sum1 += $property->gesamt_in_eur;
                                    $sum2 += $D42;
                                ?>
                                <td class="text-right">{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>

                                <td class="text-right">{{number_format($D42,0,",",".")}}€</td>
                                <td class="text-right">{{number_format(($G58 *100),2,",",".")}}%</td>

                                
                                <td class="text-right">@if($property->exklusivität_bis && strtotime(str_replace('/','-',$property->exklusivität_bis))){{date_formatting(date('d/m/Y',strtotime(str_replace('/','-',$property->exklusivität_bis))))}}@endif</td>
                            </tr>

        	@endforeach
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right">{{number_format(($sum1),0,",",".")}}€</th>
                <th class="text-right">{{number_format(($sum2),0,",",".")}}€</th>
                <th></th>
                <th></th>
            </tr>
        </tbody>
    </table>
</div>




<h3 class="box-title">{{__('dashboard.status')}}: {{__('property.liquiplanung')}}</h3>

<div class="pr-table">
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>#</th>
                <th>PLZ</th>
                <th>Ort</th>
                <th>TM</th>
                <th>Liqui Freig.</th>
                <th>EK Preis</th>
                <th>{{__('dashboard.total_purchase_price')}}</th>
                <th>{{__('dashboard.gross_return')}}</th>
                <th>D. Exkl.</th>
            </tr>
        </thead>
        <tbody>
            <?php
                            $sum1 = 0;
                            $sum2 = 0;
                        ?>
        	@foreach($array[16] as $property)
        	<?php


                            if(!isset($property->bank_ids)){  continue; }


                            $propertiesExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();
                            $total_ccc = 0;

                            $neitoo = 0;

                            ?>
                            @foreach($propertiesExtra1s as $propertiesExtra1)
                                <?php
                                if($propertiesExtra1->is_current_net)
                                $total_ccc += $propertiesExtra1->net_rent_p_a;
                                ?>
                            @endforeach
                            <?php 
                            if($total_ccc)
                                $property->net_rent_pa = $total_ccc;
                            else
                                $total_ccc = $property->net_rent_pa;


                            $property->net_rent_increase_year1 = $total_ccc;
                    $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1*$property->net_rent;
                    $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2*$property->net_rent;
                    $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3*$property->net_rent;
                    $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4*$property->net_rent;


                    $property->operating_cost_increase_year1 = $property->net_rent_increase_year1*$property->operating_costs_nk;

                    $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1*$property->operating_costs;

                    // echo $property->operating_cost_increase_year1;
                    // echo "";
                    // echo $property->operating_costs_nk;



                    $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2*$property->operating_costs;

                    $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3*$property->operating_costs;
                    $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4*$property->operating_costs;


                    $property->property_management_increase_year1 = $property->net_rent_increase_year1*$property->object_management_nk;

                    $property->property_management_increase_year2 =$property->property_management_increase_year1 +  $property->property_management_increase_year1*$property->object_management;

                    $property->property_management_increase_year3 =$property->property_management_increase_year2 +  $property->property_management_increase_year2*$property->object_management;

                    $property->property_management_increase_year4 =$property->property_management_increase_year3 +  $property->property_management_increase_year3*$property->object_management;

                    $property->property_management_increase_year5 =$property->property_management_increase_year4 +  $property->property_management_increase_year4*$property->object_management;


                    $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                    $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                    $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                    $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                    $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                     $property->ebit_year_1= $property->ebitda_year_1 - $property->depreciation_nk_money;
                     $property->ebit_year_2= $property->ebitda_year_2 - $property->depreciation_nk_money;
                     $property->ebit_year_3= $property->ebitda_year_3 - $property->depreciation_nk_money;
                     $property->ebit_year_4= $property->ebitda_year_4 - $property->depreciation_nk_money;
                     $property->ebit_year_5= $property->ebitda_year_5 - $property->depreciation_nk_money;




                            /*if(json_decode($property->bank_ids,true)){
                            $bank_ids = json_decode($property->bank_ids,true);
                            if($bank_ids != null) {
                                foreach ($banks as $b) {
                                    if(isset($bank_ids[0]) && $b->id == $bank_ids[0]){
                                        $bank = $b;
                                        break;
                                    }
                                }
                            }
                            }*/



                            $D42 = $property->gesamt_in_eur
                                + ($property->real_estate_taxes * $property->gesamt_in_eur)
                                + ($property->estate_agents * $property->gesamt_in_eur)
                                + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                                + ($property->evaluation * $property->gesamt_in_eur)
                                + ($property->others * $property->gesamt_in_eur)
                                + ($property->buffer * $property->gesamt_in_eur);


                            $L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                            $price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

                            // if(isset($bank)){
                                $E18 = ($property->net_rent_increase_year1
                                        -$property->maintenance_increase_year1
                                        -$property->operating_cost_increase_year1
                                        -$property->property_management_increase_year1)
                                    -$property->depreciation_nk_money;

                                $D49 = $property->bank_loan * $D42;
                                $D48 = $property->from_bond * $D42;
                                $H48 = $D49 * $property->interest_bank_loan;
                                $H49 = $D49 * $property->eradication_bank;
                                $H52 = $D48 * $property->interest_bond;

                                $E23 = $E18- $H48 -$H52;
                                $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                                $G61 = ($D48 == 0) ? 0 : $E31/$D48;
                                $G62 = ($D48 == 0) ? 0 : $E23/$D48;
                            // }
                            $G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;

                            if(!isset($G61)){
                                $G61 = 0;
                            }
                            ?>

                        <?php
                            $sum1 += $property->gesamt_in_eur;
                            $sum2 += $D42;
                        ?>


                        <tr>
                                <td class="text-center">{{$property->property_actual_id}}</td>

                                <td class="" >{{$property->plz_ort}}</td>
                                <td class="" >{{$property->ort}}</td>
                                <td>@php
                                        $user =  DB::table('properties')->join('users','users.id','=','properties.transaction_m_id')->where('properties.id', $property->property_actual_id)->first();
                                         if($user){
                                             echo user_short_name($user->name);
                                         }
                                    @endphp</td>
                                <td>@php
                                        $check =  DB::table('properties_buy_details')->where('property_id', $property->property_actual_id)->where('type','btn2')->first();
                                         if($check){
                                             echo "Ja";
                                         }
                                         else{
                                         echo "Nein";
                                        }
                                    @endphp</td>
                                <td class="text-right">{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>

                                <td class="text-right">{{number_format($D42,0,",",".")}}€</td>
                                <td class="text-right">{{number_format(($G58 *100),2,",",".")}}%</td>

                                <td class="text-right">@if($property->exklusivität_bis && strtotime(str_replace('/','-',$property->exklusivität_bis)))
                                    {{date_formatting(date('d/m/Y',strtotime(str_replace('/','-',$property->exklusivität_bis))))}}
                                    @endif
                                </td>
                                    
                            </tr>

        	@endforeach

            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right">{{number_format(($sum1),0,",",".")}}€</th>
                <th class="text-right">{{number_format(($sum2),0,",",".")}}€</th>
                <th></th>
                <th></th>
            </tr>
        </tbody>
    </table>
</div>




<h3 class="box-title">{{__('dashboard.status')}}: {{__('property.exclusivity')}}</h3>

<div class="pr-table">
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>#</th>
                <th>PLZ</th>
                <th>Ort</th>
                <th>TM</th>
                <th>AM</th>
                <th>EK Preis</th>
                <th>{{__('dashboard.total_purchase_price')}}</th>
                <th>{{__('dashboard.gross_return')}}</th>
                <th>Notartermin</th>
            </tr>
        </thead>
        <tbody>
            <?php
                            $sum1 = 0;
                            $sum2 = 0;
                        ?>
        	@foreach($array[10] as $property)
        	<?php


                            if(!isset($property->bank_ids)){  continue; }


                            $propertiesExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();
                            $total_ccc = 0;

                            $neitoo = 0;

                            ?>
                            @foreach($propertiesExtra1s as $propertiesExtra1)
                                <?php
                                if($propertiesExtra1->is_current_net)
                                $total_ccc += $propertiesExtra1->net_rent_p_a;
                                ?>
                            @endforeach
                            <?php 
                            if($total_ccc)
                                $property->net_rent_pa = $total_ccc;
                            else
                                $total_ccc = $property->net_rent_pa;


                            $property->net_rent_increase_year1 = $total_ccc;
                    $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1*$property->net_rent;
                    $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2*$property->net_rent;
                    $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3*$property->net_rent;
                    $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4*$property->net_rent;


                    $property->operating_cost_increase_year1 = $property->net_rent_increase_year1*$property->operating_costs_nk;

                    $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1*$property->operating_costs;

                    // echo $property->operating_cost_increase_year1;
                    // echo "";
                    // echo $property->operating_costs_nk;



                    $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2*$property->operating_costs;

                    $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3*$property->operating_costs;
                    $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4*$property->operating_costs;


                    $property->property_management_increase_year1 = $property->net_rent_increase_year1*$property->object_management_nk;

                    $property->property_management_increase_year2 =$property->property_management_increase_year1 +  $property->property_management_increase_year1*$property->object_management;

                    $property->property_management_increase_year3 =$property->property_management_increase_year2 +  $property->property_management_increase_year2*$property->object_management;

                    $property->property_management_increase_year4 =$property->property_management_increase_year3 +  $property->property_management_increase_year3*$property->object_management;

                    $property->property_management_increase_year5 =$property->property_management_increase_year4 +  $property->property_management_increase_year4*$property->object_management;


                    $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                    $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                    $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                    $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                    $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                     $property->ebit_year_1= $property->ebitda_year_1 - $property->depreciation_nk_money;
                     $property->ebit_year_2= $property->ebitda_year_2 - $property->depreciation_nk_money;
                     $property->ebit_year_3= $property->ebitda_year_3 - $property->depreciation_nk_money;
                     $property->ebit_year_4= $property->ebitda_year_4 - $property->depreciation_nk_money;
                     $property->ebit_year_5= $property->ebitda_year_5 - $property->depreciation_nk_money;




                            /*if(json_decode($property->bank_ids,true)){
                            $bank_ids = json_decode($property->bank_ids,true);
                            if($bank_ids != null) {
                                foreach ($banks as $b) {
                                    if(isset($bank_ids[0]) && $b->id == $bank_ids[0]){
                                        $bank = $b;
                                        break;
                                    }
                                }
                            }
                            }*/



                            $D42 = $property->gesamt_in_eur
                                + ($property->real_estate_taxes * $property->gesamt_in_eur)
                                + ($property->estate_agents * $property->gesamt_in_eur)
                                + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                                + ($property->evaluation * $property->gesamt_in_eur)
                                + ($property->others * $property->gesamt_in_eur)
                                + ($property->buffer * $property->gesamt_in_eur);


                            $L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                            $price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

                            // if(isset($bank)){
                                $E18 = ($property->net_rent_increase_year1
                                        -$property->maintenance_increase_year1
                                        -$property->operating_cost_increase_year1
                                        -$property->property_management_increase_year1)
                                    -$property->depreciation_nk_money;

                                $D49 = $property->bank_loan * $D42;
                                $D48 = $property->from_bond * $D42;
                                $H48 = $D49 * $property->interest_bank_loan;
                                $H49 = $D49 * $property->eradication_bank;
                                $H52 = $D48 * $property->interest_bond;

                                $E23 = $E18- $H48 -$H52;
                                $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                                $G61 = ($D48 == 0) ? 0 : $E31/$D48;
                                $G62 = ($D48 == 0) ? 0 : $E23/$D48;
                            // }
                            $G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;

                            if(!isset($G61)){
                                $G61 = 0;
                            }
                            ?>
                            <?php
                                $sum1 += $property->gesamt_in_eur;
                                $sum2 += $D42;
                            ?>
                        <tr>
                                <td class="text-center">{{$property->property_actual_id}}</td>

                                <td class="" >{{$property->plz_ort}}</td>
                                <td class="" >{{$property->ort}}</td>
                                <td>@php
                                        $user =  DB::table('properties')->join('users','users.id','=','properties.transaction_m_id')->where('properties.id', $property->property_actual_id)->first();
                                         if($user){
                                             echo user_short_name($user->name);
                                         }
                                    @endphp</td>
                                <td>@php
                                    $user =  DB::table('properties')->join('users','users.id','=','properties.asset_m_id')->where('properties.id', $property->property_actual_id)->first();

                                        if($user){
                                            echo user_short_name($user->name);
                                        }
                                    @endphp</td>
                                <td class="text-right">{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>

                                <td class="text-right">{{number_format($D42,0,",",".")}}€</td>
                                <td class="text-right">{{number_format(($G58 *100),2,",",".")}}%</td>

                                <td class="text-right">@if($property->purchase_date && $property->purchase_date!="0000-00-00"){{date_formatting(date('d/m/Y',strtotime($property->purchase_date)))}}
                                @endif</td>
                            </tr>

        	@endforeach
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right">{{number_format(($sum1),0,",",".")}}€</th>
                <th class="text-right">{{number_format(($sum2),0,",",".")}}€</th>
                <th></th>
                <th></th>
            </tr>
        </tbody>
    </table>
</div>


<h3 class="box-title">{{__('dashboard.status')}}: {{__('property.certified')}}</h3>

<div class="pr-table">
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>#</th>
                <th>PLZ</th>
                <th>Ort</th>
                <th>TM</th>
                <th>AM</th>
                <th>EK Preis</th>
                <th>{{__('dashboard.total_purchase_price')}}</th>
                <th>{{__('dashboard.gross_return')}}</th>
                <th>D. Exkl.</th>
            </tr>
        </thead>
        <tbody>
            <?php $sum1 = $sum2 = 0;?>
        	@foreach($array[12] as $property)
        	<?php


                            if(!isset($property->bank_ids)){  continue; }


                            $propertiesExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();
                            $total_ccc = 0;

                            $neitoo = 0;

                            ?>
                            @foreach($propertiesExtra1s as $propertiesExtra1)
                                <?php
                                if($propertiesExtra1->is_current_net)
                                $total_ccc += $propertiesExtra1->net_rent_p_a;
                                ?>
                            @endforeach
                            <?php 
                            if($total_ccc)
                                $property->net_rent_pa = $total_ccc;
                            else
                                $total_ccc = $property->net_rent_pa;


                            $property->net_rent_increase_year1 = $total_ccc;
                    $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1*$property->net_rent;
                    $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2*$property->net_rent;
                    $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3*$property->net_rent;
                    $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4*$property->net_rent;


                    $property->operating_cost_increase_year1 = $property->net_rent_increase_year1*$property->operating_costs_nk;

                    $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1*$property->operating_costs;

                    // echo $property->operating_cost_increase_year1;
                    // echo "";
                    // echo $property->operating_costs_nk;



                    $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2*$property->operating_costs;

                    $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3*$property->operating_costs;
                    $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4*$property->operating_costs;


                    $property->property_management_increase_year1 = $property->net_rent_increase_year1*$property->object_management_nk;

                    $property->property_management_increase_year2 =$property->property_management_increase_year1 +  $property->property_management_increase_year1*$property->object_management;

                    $property->property_management_increase_year3 =$property->property_management_increase_year2 +  $property->property_management_increase_year2*$property->object_management;

                    $property->property_management_increase_year4 =$property->property_management_increase_year3 +  $property->property_management_increase_year3*$property->object_management;

                    $property->property_management_increase_year5 =$property->property_management_increase_year4 +  $property->property_management_increase_year4*$property->object_management;


                    $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                    $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                    $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                    $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                    $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                     $property->ebit_year_1= $property->ebitda_year_1 - $property->depreciation_nk_money;
                     $property->ebit_year_2= $property->ebitda_year_2 - $property->depreciation_nk_money;
                     $property->ebit_year_3= $property->ebitda_year_3 - $property->depreciation_nk_money;
                     $property->ebit_year_4= $property->ebitda_year_4 - $property->depreciation_nk_money;
                     $property->ebit_year_5= $property->ebitda_year_5 - $property->depreciation_nk_money;




                            /*if(json_decode($property->bank_ids,true)){
                            $bank_ids = json_decode($property->bank_ids,true);
                            if($bank_ids != null) {
                                foreach ($banks as $b) {
                                    if(isset($bank_ids[0]) && $b->id == $bank_ids[0]){
                                        $bank = $b;
                                        break;
                                    }
                                }
                            }
                            }*/



                            $D42 = $property->gesamt_in_eur
                                + ($property->real_estate_taxes * $property->gesamt_in_eur)
                                + ($property->estate_agents * $property->gesamt_in_eur)
                                + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                                + ($property->evaluation * $property->gesamt_in_eur)
                                + ($property->others * $property->gesamt_in_eur)
                                + ($property->buffer * $property->gesamt_in_eur);


                            $L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                            $price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

                            // if(isset($bank)){
                                $E18 = ($property->net_rent_increase_year1
                                        -$property->maintenance_increase_year1
                                        -$property->operating_cost_increase_year1
                                        -$property->property_management_increase_year1)
                                    -$property->depreciation_nk_money;

                                $D49 = $property->bank_loan * $D42;
                                $D48 = $property->from_bond * $D42;
                                $H48 = $D49 * $property->interest_bank_loan;
                                $H49 = $D49 * $property->eradication_bank;
                                $H52 = $D48 * $property->interest_bond;

                                $E23 = $E18- $H48 -$H52;
                                $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                                $G61 = ($D48 == 0) ? 0 : $E31/$D48;
                                $G62 = ($D48 == 0) ? 0 : $E23/$D48;
                            // }
                            $G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;

                            if(!isset($G61)){
                                $G61 = 0;
                            }
                            ?>
                            <?php
                                $sum1 += $property->gesamt_in_eur;
                                $sum2 += $D42;
                            ?>
                        <tr>
                                <td class="text-center">{{$property->property_actual_id}}</td>

                                <td class="" >{{$property->plz_ort}}</td>
                                <td class="" >{{$property->ort}}</td>
                                <td>@php
                                        $user =  DB::table('properties')->join('users','users.id','=','properties.transaction_m_id')->where('properties.id', $property->property_actual_id)->first();
                                         if($user){
                                             echo user_short_name($user->name);
                                         }
                                    @endphp</td>
                                <td>@php
                                    $user =  DB::table('properties')->join('users','users.id','=','properties.asset_m_id')->where('properties.id', $property->property_actual_id)->first();

                                        if($user){
                                            echo user_short_name($user->name);
                                        }
                                    @endphp</td>
                                <td class="text-right">{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>

                                <td class="text-right">{{number_format($D42,0,",",".")}}€</td>
                                <td class="text-right">{{number_format(($G58 *100),2,",",".")}}%</td>

                                <td class="text-right">@if($property->exklusivität_bis && strtotime(str_replace('/','-',$property->exklusivität_bis))){{date_formatting(date('d/m/Y',strtotime(str_replace('/','-',$property->exklusivität_bis))))}}@endif</td>
                                    
                            </tr>

        	@endforeach
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right">{{number_format(($sum1),0,",",".")}}€</th>
                <th class="text-right">{{number_format(($sum2),0,",",".")}}€</th>
                <th></th>
                <th></th>
            </tr>
        </tbody>
    </table>
</div>
<br>
<h2>Verkauf</h2>
<h3 class="box-title">{{__('dashboard.status')}}: Exklusivität (Bestand)</h3>

<div class="pr-table">
    <table style="width: 100%;">
        <thead>
            <tr>
                <th>#</th>
                <!-- <th>PLZ</th> -->
                <th>Ort</th>
                <th>Exkl. bis</th>
                <th>Notar</th>
                <th>BNL</th>
                <th>Verkäufer</th>
                <th>AM</th>
                <th>EK Preis</th>
                <th>{{__('dashboard.total_purchase_price')}}</th>
                <th>Verkaufspreis</th>
            </tr>
        </thead>
        <tbody>
            <?php $sum1 = $sum2 = $sum3 = 0;?>
        	@foreach($array[6] as $property)
        	<?php


                            if(!isset($property->bank_ids)){  continue; }


                            $propertiesExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();
                            $total_ccc = 0;

                            $neitoo = 0;

                            ?>
                            @foreach($propertiesExtra1s as $propertiesExtra1)
                                <?php
                                if($propertiesExtra1->is_current_net)
                                $total_ccc += $propertiesExtra1->net_rent_p_a;
                                ?>
                            @endforeach
                            <?php 
                            if($total_ccc)
                                $property->net_rent_pa = $total_ccc;
                            else
                                $total_ccc = $property->net_rent_pa;


                            $property->net_rent_increase_year1 = $total_ccc;
                    $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1*$property->net_rent;
                    $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2*$property->net_rent;
                    $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3*$property->net_rent;
                    $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4*$property->net_rent;


                    $property->operating_cost_increase_year1 = $property->net_rent_increase_year1*$property->operating_costs_nk;

                    $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1*$property->operating_costs;

                    // echo $property->operating_cost_increase_year1;
                    // echo "";
                    // echo $property->operating_costs_nk;



                    $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2*$property->operating_costs;

                    $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3*$property->operating_costs;
                    $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4*$property->operating_costs;


                    $property->property_management_increase_year1 = $property->net_rent_increase_year1*$property->object_management_nk;

                    $property->property_management_increase_year2 =$property->property_management_increase_year1 +  $property->property_management_increase_year1*$property->object_management;

                    $property->property_management_increase_year3 =$property->property_management_increase_year2 +  $property->property_management_increase_year2*$property->object_management;

                    $property->property_management_increase_year4 =$property->property_management_increase_year3 +  $property->property_management_increase_year3*$property->object_management;

                    $property->property_management_increase_year5 =$property->property_management_increase_year4 +  $property->property_management_increase_year4*$property->object_management;


                    $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                    $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                    $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                    $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                    $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                     $property->ebit_year_1= $property->ebitda_year_1 - $property->depreciation_nk_money;
                     $property->ebit_year_2= $property->ebitda_year_2 - $property->depreciation_nk_money;
                     $property->ebit_year_3= $property->ebitda_year_3 - $property->depreciation_nk_money;
                     $property->ebit_year_4= $property->ebitda_year_4 - $property->depreciation_nk_money;
                     $property->ebit_year_5= $property->ebitda_year_5 - $property->depreciation_nk_money;




                            /*if(json_decode($property->bank_ids,true)){
                            $bank_ids = json_decode($property->bank_ids,true);
                            if($bank_ids != null) {
                                foreach ($banks as $b) {
                                    if(isset($bank_ids[0]) && $b->id == $bank_ids[0]){
                                        $bank = $b;
                                        break;
                                    }
                                }
                            }
                            }*/



                            $D42 = $property->gesamt_in_eur
                                + ($property->real_estate_taxes * $property->gesamt_in_eur)
                                + ($property->estate_agents * $property->gesamt_in_eur)
                                + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                                + ($property->evaluation * $property->gesamt_in_eur)
                                + ($property->others * $property->gesamt_in_eur)
                                + ($property->buffer * $property->gesamt_in_eur);


                            $L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                            $price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

                            // if(isset($bank)){
                                $E18 = ($property->net_rent_increase_year1
                                        -$property->maintenance_increase_year1
                                        -$property->operating_cost_increase_year1
                                        -$property->property_management_increase_year1)
                                    -$property->depreciation_nk_money;

                                $D49 = $property->bank_loan * $D42;
                                $D48 = $property->from_bond * $D42;
                                $H48 = $D49 * $property->interest_bank_loan;
                                $H49 = $D49 * $property->eradication_bank;
                                $H52 = $D48 * $property->interest_bond;

                                $E23 = $E18- $H48 -$H52;
                                $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                                $G61 = ($D48 == 0) ? 0 : $E31/$D48;
                                $G62 = ($D48 == 0) ? 0 : $E23/$D48;
                            // }
                            $G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;

                            if(!isset($G61)){
                                $G61 = 0;
                            }
                            ?>
                        <tr>
                                <td class="text-center">{{$property->property_actual_id}}</td>

                                <!-- <td class="" >{{$property->plz_ort}}</td> -->
                                <td class="" >{{$property->ort}}</td>

                                <?php
                                    $d['101'] = $d['102'] = $d['103'] = "";
                                    $dates =  DB::table('properties_sale_details')->whereRaw('(type=101 OR type=102 OR type=103) and property_id='.$property->property_actual_id)->get();

                                    foreach ($dates as $key => $value) {
                                        if($value->comment)
                                        $d[$value->type] = date('d.m.Y',strtotime($value->comment));
                                    }

                                    ?>
                                    <td>{{$d['103']}}</td>
                                    <td>{{$d['101']}}</td>
                                    <td>{{$d['102']}}</td>
                                <td>@php
                                        $user =  DB::table('properties')->join('users','users.id','=','properties.seller_id')->where('properties.id', $property->property_actual_id)->first();
                                         if($user){
                                             echo user_short_name($user->name);
                                         }
                                    @endphp</td>
                                <td>@php
                                    $user =  DB::table('properties')->join('users','users.id','=','properties.asset_m_id')->where('properties.id', $property->property_actual_id)->first();

                                        if($user){
                                            echo user_short_name($user->name);
                                        }
                                    @endphp</td>
                                <td class="text-right">{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>

                                <td class="text-right">{{number_format($D42,0,",",".")}}€</td>
                                <td class="text-right">{{show_number($property->preisverk)}}€</td>

                                
                                    
                            </tr>
                            <?php
                                $sum1 += $property->gesamt_in_eur;
                                $sum2 += $D42;
                                $sum3 += $property->preisverk;
                            ?>

        	@endforeach

            <tr>
                <th></th>
                <!-- <th></th> -->
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right">{{number_format(($sum1),0,",",".")}}€</th>
                <th class="text-right">{{number_format(($sum2),0,",",".")}}€</th>
                <th class="text-right">{{number_format(($sum3),0,",",".")}}€</th>
            </tr>
        </tbody>
    </table>
</div>