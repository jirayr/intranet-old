<style type="text/css">
<style type="text/css">
   @page { margin: 0px; }
   body { margin: 0px;
   font-family: arial,sans-serif !important;
   font-size: 9px;
   }
   .text-left {
   text-align: left !important;
   }
   .text-right {
   text-align: right !important;
   padding-right: 3px;
   }
   .text-center {
   text-align: center !important;
   }
   .color-red {
   color: #f53132 !important;
   }
   .bg-brown {
   background-color: #333333;
   color: #ffffff;
   }
   .bg-light-blue {
   background-color: #9BCDFB;
   }
   .bg-gray {
   background-color: #c0c0c0;
   color: #333333;
   }
   .bg-light-green {
   background-color: rgb(226,239,219);
   color: #333333;
   }
   .bg-yellow {
   background-color: #ffff99;
   color: #333333;
   }
   .bg-orange {
   background-color: rgb(253, 191, 45);
   color: #333333;
   }
   .bg-red {
   background-color: #FEC7CE;
   color: #333333;
   }
   .bg-blue {
   background-color: rgb(181, 199, 230);
   color: #333333;
   }
   .bg-green{
   background-color: #9ACA27;
   color: #333333;
   }
   .border-none {
   border: 0 !important;
   }
   .border {
   border: 1px solid #090909;
   }
   .border-right {
   border-right: 1px solid #090909;
   }
   .border-no-right {
   /*border: 2px solid #090909;*/
   border-right: 0px;
   border: 1px solid #090909;
   }
   .border-bottom {
   /*border-bottom: 2px solid #090909;*/
   border-bottom: 1px solid #090909;
   }
   .border-no-bottom {
   /*border: 2px solid #090909;*/
   border: 1px solid #090909;
   border-bottom: 0px;
   }
   .border-no-left {
   /*border: 2px solid #090909;*/
   border: 1px solid #090909;
   border-left: 0px;
   }
   .border-no-top {
   border-top: 0px;
   border: 1px solid #090909;
   }
   a{
   color: black;
   text-decoration: none;
   cursor: default;
   padding: 0 !important;
   margin:0 !important;
   }
   table th,
   table td{
   padding: 2px;
   /*width: 8% !important;*/
   }
</style>
<body>
   <?php
      $total_ccc = 0;
      $ankdate1 = $ankdate2  = "";
      ?>
   @foreach($propertiesExtra1s as $k=>$propertiesExtra1)
   <?php
      if($propertiesExtra1->is_current_net)
      $total_ccc += $propertiesExtra1->net_rent_p_a;

      if($k==0)
      $ankdate1 = $propertiesExtra1->mv_end;
      if($k==1)
      $ankdate2 = $propertiesExtra1->mv_end;




      ?>
   @endforeach
   <?php
      if($total_ccc){
          $properties->net_rent_pa = $total_ccc;
      }
      else{
          $total_ccc = $properties->net_rent_pa;
      }
      // $total_ccc = 754283;
      $properties->net_rent_increase_year1 = $total_ccc;
      $properties->net_rent_increase_year2 = $properties->net_rent_increase_year1 + $properties->net_rent_increase_year1*$properties->net_rent;
      $properties->net_rent_increase_year3 = $properties->net_rent_increase_year2 + $properties->net_rent_increase_year2*$properties->net_rent;
      $properties->net_rent_increase_year4 = $properties->net_rent_increase_year3 + $properties->net_rent_increase_year3*$properties->net_rent;
      $properties->net_rent_increase_year5 = $properties->net_rent_increase_year4 + $properties->net_rent_increase_year4*$properties->net_rent;


      $properties->operating_cost_increase_year1 = $properties->net_rent_increase_year1*$properties->operating_costs_nk;

      $properties->operating_cost_increase_year2 = $properties->operating_cost_increase_year1 + $properties->operating_cost_increase_year1*$properties->operating_costs;

      // echo $properties->operating_cost_increase_year1;
      // echo "<br>";
      // echo $properties->operating_costs_nk;



      $properties->operating_cost_increase_year3 = $properties->operating_cost_increase_year2 + $properties->operating_cost_increase_year2*$properties->operating_costs;

      $properties->operating_cost_increase_year4 = $properties->operating_cost_increase_year3 + $properties->operating_cost_increase_year3*$properties->operating_costs;
      $properties->operating_cost_increase_year5 = $properties->operating_cost_increase_year4 + $properties->operating_cost_increase_year4*$properties->operating_costs;


      $properties->property_management_increase_year1 = $properties->net_rent_increase_year1*$properties->object_management_nk;

      $properties->property_management_increase_year2 =$properties->property_management_increase_year1 +  $properties->property_management_increase_year1*$properties->object_management;

      $properties->property_management_increase_year3 =$properties->property_management_increase_year2 +  $properties->property_management_increase_year2*$properties->object_management;

      $properties->property_management_increase_year4 =$properties->property_management_increase_year3 +  $properties->property_management_increase_year3*$properties->object_management;

      $properties->property_management_increase_year5 =$properties->property_management_increase_year4 +  $properties->property_management_increase_year4*$properties->object_management;


      $properties->ebitda_year_1 = $total_ccc - $properties->maintenance_increase_year1 - $properties->operating_cost_increase_year1 - $properties->property_management_increase_year1;


      //echo $total_ccc.'/'.$properties->maintenance_increase_year1.'/'.$properties->operating_cost_increase_year1.'/'.$properties->property_management_increase_year1;



      $properties->ebitda_year_2 = $properties->net_rent_increase_year2 - $properties->maintenance_increase_year2 - $properties->operating_cost_increase_year2 - $properties->property_management_increase_year2;
      $properties->ebitda_year_3 = $properties->net_rent_increase_year3 - $properties->maintenance_increase_year3 - $properties->operating_cost_increase_year3 - $properties->property_management_increase_year3;
      $properties->ebitda_year_4 = $properties->net_rent_increase_year4 - $properties->maintenance_increase_year4 - $properties->operating_cost_increase_year4 - $properties->property_management_increase_year4;
      $properties->ebitda_year_5 = $properties->net_rent_increase_year5 - $properties->maintenance_increase_year5 - $properties->operating_cost_increase_year5 - $properties->property_management_increase_year5;

       $properties->ebit_year_1= $properties->ebitda_year_1 - $properties->depreciation_nk_money;
       $properties->ebit_year_2= $properties->ebitda_year_2 - $properties->depreciation_nk_money;
       $properties->ebit_year_3= $properties->ebitda_year_3 - $properties->depreciation_nk_money;
       $properties->ebit_year_4= $properties->ebitda_year_4 - $properties->depreciation_nk_money;
       $properties->ebit_year_5= $properties->ebitda_year_5 - $properties->depreciation_nk_money;


       $pcomments_new = DB::table('property_comments')->where('property_id',$properties->id)->whereNotNull('Ankermieter_note')->where('status',null)->first();




      ?>
   {{-- @foreach ($banks as $key => $bank) --}}
   <?php
      $D42 = $properties->gesamt_in_eur
          + ($properties->real_estate_taxes * $properties->gesamt_in_eur)
          + ($properties->estate_agents * $properties->gesamt_in_eur)
          + (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
          + ($properties->evaluation * $properties->gesamt_in_eur)
          + ($properties->others * $properties->gesamt_in_eur)
          + ($properties->buffer * $properties->gesamt_in_eur);
      $D46 = $D42;

      $D47 = $properties->with_real_ek * $D46;    //C47*D46
      $D48 = $properties->from_bond * $D46;   //C48*D46

      $D49 = $properties->bank_loan * $D46;


      $D50 = $D47 + $D48 +  $D49;


      if($banks[0] == ''){
          $bank_check = 0;
          $bank = $fake_bank;
      }else{
          $bank_check = 1;
      }

      $H47 = $D49 - $D49 * $properties->eradication_bank;
      $H48 = $D49 * $properties->interest_bank_loan;    //G48*D49
      $H49 = $D49 * $properties->eradication_bank;
      $H50 = $D49 * $properties->interest_bank_loan + $D49*$properties->eradication_bank;
      $H52 = $D48 * $properties->interest_bond;

      $I48 = $H47 * $properties->interest_bank_loan;
      $I49 = $H50 - $I48;
      $I47 = $H47 - $I49;
      $I50 = $I48 + $I49;

      $J48 = $I47 * $properties->interest_bank_loan;
      $J49 = $H50 - $J48;
      $J47 = $I47 - $J49;
      $J50 = $J48 + $J49;

      $K48 = $J47 * $properties->interest_bank_loan;
      $K49 = $I50 - $K48;
      $K47 = $J47 - $K49;
      $K50 = $K48 + $K49;

      $L48 = $K47 * $properties->interest_bank_loan;
      $L49 = $J50 - $L48;
      $L47 = $K47 - $L49;
      $L50 = $L48 + $L49;


      $E18 = ($properties->net_rent_increase_year1
      -$properties->maintenance_increase_year1
      -$properties->operating_cost_increase_year1
      -$properties->property_management_increase_year1)
      -$properties->depreciation_nk_money;

      $F18 = ($properties->net_rent_increase_year2
      -$properties->maintenance_increase_year2
      -$properties->operating_cost_increase_year2
      -$properties->property_management_increase_year2)
      -$properties->depreciation_nk_money;

      $G18 = ($properties->net_rent_increase_year3
      -$properties->maintenance_increase_year3
      -$properties->operating_cost_increase_year3
      -$properties->property_management_increase_year3)
      -$properties->depreciation_nk_money;

      $H18 =
      ($properties->net_rent_increase_year4
      -$properties->maintenance_increase_year4
      -$properties->operating_cost_increase_year4
      -$properties->property_management_increase_year4)
      -$properties->depreciation_nk_money;

      $I18 = ($properties->net_rent_increase_year5
      -$properties->maintenance_increase_year5
      -$properties->operating_cost_increase_year5
      -$properties->property_management_increase_year5)
      -$properties->depreciation_nk_money;

      $E23 = $E18- $H48 -$H52;
      $F23 = $F18-($H52)-($I48);
      $G23 = $G18-($H52)-($J48);
      $H23 = $H18 - ($H52)-($K48);
      $I23 = $I18-($H52)-($L48);
      $E31 = ($E23 - ($properties->tax * $E23)) - $H49 + $properties->depreciation_nk_money;

      $I16= $properties->depreciation_nk_money;
      $I27 =$I23 - ($properties->tax * $I23);
      $I29 = $L49;


      $I31 = $I27-$I29+$I16;

      $G58 = ($D42 == 0) ? 0: $properties->net_rent_pa/$D42;
      $G59 = ($properties->net_rent_pa == 0) ? 0 : $D42/ $properties->net_rent_pa;
      $E25 = $properties->tax * $E23;
      $G61 = ($D48 == 0) ? 0 : $E31/$D48;
      $G62 = ($D48 == 0) ? 0 : $E23/$D48;



      $L14 = ($properties->rent_retail + $properties->rent_whg == 0) ? 0 : ($properties->net_rent_pa / ($properties->rent_retail + $properties->rent_whg) / 12);
      $L16 = ($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg == 0) ? 0 :
      ($properties->gesamt_in_eur)/($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg);
      $L20 = ($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg == 0) ? 0 :
      ($properties->rent_retail + $properties->rent_whg)/($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg);

      $L25 = ($properties->net_rent_pa == 0) ? 0 : $properties->gesamt_in_eur / $properties->net_rent_pa;


      $L11 = $properties->plot_of_land_m2;
      $P31 = $properties->ground_reference_value_in_euro_m2;
      $P43 = $L11* $P31;
      $D59 = $properties->gesamt_in_eur*($properties->maintenance_nk);
      $D60 = $properties->operating_cost_increase_year1;


      if($properties->Ist && $D42)
      {

          $row_11 = \App\Verkauf_tab::where('row_name','row_12')->where('property_id',$properties->main_property_id)->first();
          if(!$row_11)
          {
              $row_11 = new \App\Verkauf_tab;
              $row_11->property_id = $properties->main_property_id;
              $row_11->tab_value = $D42;
              $row_11->row_name = 'row_12';
              $row_11->save();
          }
      }

      // echo $D42;

      ?>
   <table class="pr-zoom-in-out-table" cellspacing="0" cellpadding="0">
      <tbody>
         <tr class="">
            <th class="bg-brown border-right" colspan="2">Kalkulations- und Planungsspiegel</th>
            <th class="color-red border-no-right">Objekt</th>
            <th colspan="2" class="border-no-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="name_of_property" data-placement="bottom" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.name_of_property')}}">{{ $properties->name_of_property }}</a></th>
            <th style="width: 9%;" class="color-red border-no-right">{{__('property.date_of_last_update')}}</th>
            <th style="width: 9%;" class="text-right border-no-right">{{date('d.m.Y', strtotime($properties->updated_at))}}</th>
            <th style="width: 9%;" class="color-red border-no-right">{{__('property.date_of_creation')}}</th>
            <th style="width: 9%;" class="text-right border-no-right">{{date('d.m.Y', strtotime($properties->created_at))}}</th>
            <th class="color-red border-no-right">{{__('property.creator_username')}}</th>
            <th class="border border-right">{{$properties->user_name}}</th>
            <th></th>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td style="width: 8%;">&nbsp;</td>
            <td style="width: 8%;">&nbsp;</td>
            <td style="width: 9%;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th class="border text-center">Adresse</th>
            <!--<th class="border">PLZ</th>
               <td class="border text-center bg-yellow">
                   <a href="#" class="inline-edit" data-type="text" data-pk="plz_ort" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="PLZ">{{ $properties->plz_ort }}</a></td>
               <th class="border">Ort</th>
               <td class="border text-center bg-yellow">
                   <a href="#" class="inline-edit" data-type="text" data-pk="ort" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Ort">{{ $properties->ort }}</a>
               </td>
               <th class="border">Strasse</th>
               <td class="border text-center bg-yellow">
                   <a href="#" class="inline-edit" data-type="text" data-pk="strasse" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Strasse">{{ $properties->strasse }}</a>
               </td>-->
            <th class="border">PLZ</th>
            <td class="border text-center bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="plz_ort" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="PLZ">{{ $main_properties->plz_ort }}</a></td>
            <th class="border">
               Ort
               <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#add-city" class="add_ort">Add Ort</a> -->
            </th>
            <td class="border text-center bg-yellow"><a href="#" class="city-remote" data-type="select2" data-pk="ort" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="Ort">{{ $main_properties->ort }}</a>
            </td>
            <th class="border">Strasse</th>
            <td class="border text-center bg-yellow">
               <a href="#" class="inline-edit" data-type="text" data-pk="strasse" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="Strasse">{{ $main_properties->strasse }}</a>
            </td>
            <th class="border">Hausnummer</th>
            <td class="border text-center bg-yellow">
               <a href="#" class="inline-edit" data-type="text" data-pk="hausnummer" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="Hausnummer">{{ $main_properties->hausnummer }}</a>
            </td>
            <th class="border">Bundesland</th>
            <td class="border text-center bg-yellow border-right">
               <a href="#" class="inline-edit-niedersachsen" data-type="select" data-pk="niedersachsen" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="Bundesland">{{ $main_properties->niedersachsen }}</a>
            </td>
            <td></td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td colspan="3"></td>
            <th class="border-no-right bg-gray text-center">Jahr 1</th>
            <th class="border-no-right bg-gray text-center">Jahr 2</th>
            <th class="border-no-right bg-gray text-center">Jahr 3</th>
            <th class="border-no-right bg-gray text-center">Jahr 4</th>
            <th class="border bg-gray text-center">Jahr 5</th>
            <td></td>
            <th colspan="2" class="bg-yellow">Gelbe Felder = Eingabefelder</th>
            <!-- <th colspan="2" class="border text-center">Bemerkungen</th> -->
            <td></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="">&nbsp;</td>
            <td class=""></td>
            <td></td>
            <td colspan="3">
            </td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th class="border-no-right bg-gray">Netto Miete (IST) p.a.</th>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.net_rent')}} (%)">{{number_format($properties->net_rent*100,2,",",".")}}</a><span>%</span></td>
            <th class="border-no-right bg-gray">Steigerung p.a.</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year1,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year2,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year3,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year4,2,",",".")}}</th>
            <th class="border bg-gray text-right">{{number_format($properties->net_rent_increase_year5,2,",",".")}}</td>
            <td></td>
            <td>Ankermieter 1</td>
            <td colspan="2" class="text-right">
               @if(isset($propertiesExtra1s[0]['tenant']))
               {{$propertiesExtra1s[0]['tenant']}}
               @endif
               <!-- <a href="#" class="inline-edit" data-type="text" data-pk="ankermieter4" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Ankermieter">{{ $properties->ankermieter4 }}</a> -->
            </td>
            <td >

            </td>
            <td  >
               <!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Mieter_comment" data-pk="Mieter_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mieter_comment'] }}@endif</a> -->
            </td>
            {{--
            <td colspan="4" class="border">
               <a href="#" class="inline-edit" data-type="textarea" data-name="Mieter_note" data-pk="Mieter_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mieter_note'] }}@endif
               </a>
            </td>
            --}}
            <td>&nbsp;</td>
            {{--
            <td>{{number_format($properties->anchor_tenants,2)}}</td>
            --}}
         </tr>
         <tr>
            <td class="border-no-right">Netto Miete (Soll) Leerst.</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_empty" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.net_rent_empty')}}(%)">{{number_format($properties->net_rent_empty*100,2,",",".")}}</a>%</td>
            {{--
            <td class="border-no-right bg-yellow text-right"><a href="#" data-toggle="modal" data-target="#myModal" > Mietflächen</a></td>
            --}}
            <td class="border-no-right">Steigerung p.a.</td>
            <td class="border-no-right text-right">{{number_format(0,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format(0,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format(0,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format(0,2,",",".")}}</td>
            <td class="border text-right">{{number_format(0,2,",",".")}}</td>
            <td></td>
            <td>Ankermieter 2</td>
            <td colspan="2" class="text-right">
               <!-- <a href="#" class="inline-edit" data-type="text" data-pk="mieter" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Mieter">{{ $properties->mieter }}</a> -->
               @if(isset($propertiesExtra1s[1]['tenant']))
               {{$propertiesExtra1s[1]['tenant']}}
               @endif
            </td>
            <td class=""></td>
            <td>&nbsp;</td>
            {{--
            <td colspan="4" class="border">
               <a href="#" class="inline-edit" data-type="textarea" data-name="Mietflache_note" data-pk="Mietflache_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mietflache_note'] }}@endif</a>
            </td>
            --}}
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" >
               <!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Grundstuck_comment" data-pk="Grundstuck_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Grundstuck_comment'] }}@endif</a> -->
            </td>
            {{--
            <td colspan="4" class="border">
               <a href="#" class="inline-edit" data-type="textarea" data-name="Grundstuck_note" data-pk="Grundstuck_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Grundstuck_note'] }}@endif</a>
            </td>
            --}}
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td class="border-no-right">Instandhaltung nichtumlfähig</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.maintenance')}} (%)">{{number_format($properties->maintenance*100,2,",",".")}}</a>%</td>
            <td class="border-no-right">Steigerung p.a.</td>
            <td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year1,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year2,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year3,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year4,2,",",".")}}</td>
            <td class="border text-right">{{number_format($properties->maintenance_increase_year5,2,",",".")}}</td>
            <td></td>
            <td>Mietfläche in m²</td>
            <td colspan="2" class="text-right">
               <!-- {{number_format($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg, 2,",",".")}} -->
               <!-- @foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
                  {{number_format($tenancy_schedule->calculations['total_rental_space'], 2,",",".")}}
                  @endforeach -->
               {{number_format($properties->vacancy_retail+$properties->rent_retail+$properties->vacancy_whg+$properties->rent_whg,2,",",".")}}
            </td>
            <td colspan="2" >
               <!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Baujahr_comment" data-pk="Baujahr_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Baujahr_comment'] }}@endif</a> -->
            </td>
            {{--
            <td colspan="4" class="border">
               <a href="#" class="inline-edit" data-type="textarea" data-name="Baujahr_note" data-pk="Baujahr_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Baujahr_note'] }}@endif</a>
            </td>
            --}}
         </tr>
         <tr>
            <td class="border-no-right">Betriebsk. nicht umlfähig</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="operating_costs" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.operating_costs')}} (%)">{{number_format($properties->operating_costs*100,2,",",".")}}</a>%</td>
            <td class="border-no-right">Steigerung p.a.</td>
            <td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year1,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year2,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year3,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year4,2,",",".")}}</td>
            <td class="border text-right">{{number_format($properties->operating_cost_increase_year5,2,",",".")}}</td>
            <td></td>
            <td >Kaufpreis p. m² MF</td>
            <td colspan="2" class="text-right">{{number_format($L16,2,",",".")}}&nbsp;€</td>
            {{--
            <td colspan="4" class="border">
               <a href="#" class="inline-edit" data-type="textarea" data-name="Miete_note" data-pk="Miete_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Miete_note'] }}@endif</a>
            </td>
            --}}
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td class="border-no-right">Objektverwalt. nichtumlfähig</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="object_management" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.object_management')}} (%)">{{number_format($properties->object_management*100,2,",",".")}}</a>%</td>
            <td class="border-no-right">Steigerung p.a.</td>
            <td class="border-no-right text-right">{{number_format($properties->property_management_increase_year1,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->property_management_increase_year2,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->property_management_increase_year3,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->property_management_increase_year4,2,",",".")}}</td>
            <td class="border text-right">{{number_format($properties->property_management_increase_year5,2,",",".")}}</td>
            <td></td>
            <td>Miete (€/m²)</td>
            <td colspan="2" class="text-right">
               {{number_format($L14,2,",",".")}}&nbsp;€
            </td>
            <td><a href="#" class="inline-edit" data-type="text" data-pk="miete_text" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="">{{$properties->miete_text}}</a></td>
            <td >
               <!-- <a href="#" class="inline-edit" data-type="textarea" data-name="KP_comment" data-pk="KP_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['KP_comment'] }}@endif</a> -->
            </td>
            {{--
            <td colspan="4" class="border">
               <a href="#" class="inline-edit" data-type="textarea" data-name="KP_note" data-pk="KP_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['KP_note'] }}@endif</a>
            </td>
            --}}
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2" >
               <!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Stellplatze_comment" data-pk="Stellplatze_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Stellplatze_comment'] }}@endif</a> -->
            </td>
            {{--
            <td colspan="4" class="border">
               <a href="#" class="inline-edit" data-type="textarea" data-name="Stellplatze_note" data-pk="Stellplatze_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Stellplatze_note'] }}@endif</a>
            </td>
            --}}
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="3" class="border-no-right bg-gray">EBITDA</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_1,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_2,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_3,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_4,2,",",".")}}</th>
            <th class="border bg-gray text-right">{{number_format($properties->ebitda_year_5,2,",",".")}}</th>
            <td></td>
            <td>Baujahr</td>
            <td class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="construction_year" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.construction_year')}}">{{$properties->construction_year}}</a></td>
            <td class=""><a href="#" class="inline-edit" data-type="text" data-pk="construction_year_note" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.construction_year')}}">{{$properties->construction_year_note}}</a></td>
            <td colspan="2" >
               <!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Vermietungsstand_comment" data-pk="Vermietungsstand_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Vermietungsstand_comment'] }}@endif</a> -->
            </td>
            {{--
            <td colspan="4" class="border">
               <a href="#" class="inline-edit" data-type="textarea" data-name="Vermietungsstand_note" data-pk="Vermietungsstand_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Vermietungsstand_note'] }}@endif</a>
            </td>
            --}}
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            {{--
            <td colspan="4" class="border">
               <a href="#" class="inline-edit" data-type="textarea" data-name="Maklerpreis_note" data-pk="Maklerpreis_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Maklerpreis_note'] }}@endif</a>
            </td>
            --}}
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td colspan="3" class="border-no-right">Abschreibung</td>
            <td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
            <td class="border text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
            <td></td>
            <td>Grundstück in m²</td>
            <td colspan="2" class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="plot_of_land_m2" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.plot_of_land_m2')}}">{{number_format($properties->plot_of_land_m2,2,",",".")}}</a></td>
            <td><a href="#" class="inline-edit" data-type="text" data-pk="plot_of_land_m2_text" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="">{{$properties->plot_of_land_m2_text}}</a></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="3" class="border-no-right bg-gray">EBIT</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_1,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_2,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_3,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_4,2,",",".")}}</th>
            <th class="border bg-gray text-right">{{number_format($properties->ebit_year_5,2,",",".")}}</th>
            <td></td>
            <td>Bodenrichtwert in €/m²</td>
            <td colspan="2" class="text-right bg-yellow">
               <a href="#" class="inline-edit" data-type="text" data-pk="ground_reference_value_in_euro_m2" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.ground_reference_value_in_euro_m2')}}">{{number_format($properties->ground_reference_value_in_euro_m2,2,",",".")}}</a>
               €
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td colspan="3" class="border-no-right">Zins Bankkredit</td>
            <td class="border-no-right text-right">{{number_format($H48,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($I48,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($J48,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($K48,2,",",".")}}</td>
            <td class="border text-right">{{number_format($L48,2,",",".")}}</td>
            <td></td>
            <td>Grundstückswert</td>
            <td colspan="2" class="text-right">{{number_format($P43,2,",",".")}} €</td>
            <!-- <td>Vermietungsstand</td>
               <td class="text-right">
                   {{number_format($L20 * 100, 1,",",".")}}%
               </td> -->
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="3" class="border-no-right">Eigenmittelverzinsung</th>
            <td class="border-no-right text-right">{{number_format($H52,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($H52,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($H52,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($H52,2,",",".")}}</td>
            <td class="border text-right">{{number_format($H52,2,",",".")}}</td>
            <td></td>
            <td >Stellplätze</td>
            <td colspan="2" class="text-right bg-yellow">
               <a href="#" class="inline-edit" data-type="text" data-pk="plot" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Stellplätze">{{number_format($properties->plots,0,",",".")}}</a>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="3" class="border-no-right bg-gray">EBT</th>
            <th class="border-no-right text-right bg-gray">{{number_format($E18
               - $H48
               -$H52,2,",",".")}}
            </th>
            <th class="border-no-right text-right bg-gray">{{number_format($F18
               -$H52
               -$I48,2,",",".")}}
            </th>
            <th class="border-no-right text-right bg-gray">{{number_format($G18
               -$H52-
               $J48,2,",",".")}}
            </th>
            <th class="border-no-right text-right bg-gray">{{number_format($H18
               -$H52
               -$K48,2,",",".")}}
            </th>
            <th class="border text-right bg-gray">{{number_format($I18
               -$H52
               -$L48,2,",",".")}}
            </th>
            <td>&nbsp;</td>
            <td>Einwohner</td>
            <td colspan="2" class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="population_edited" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.population')}}">
               <?php
                  if($properties->population_edited)
                      $properties->population = $properties->population_edited;
                  ?>
               {{number_format($properties->population,2,",",".")}}
               </a>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td class="border-no-right">Steuern</td>
            <td class="border-no-right text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="tax" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.Tax')}} (%)">{{number_format($properties->tax*100,2,",",".")}}</a>%</td>
            <td class="border-no-right"></td>
            <td class="border-no-right text-right">{{number_format($E25,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->tax * $F23,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->tax * $G23,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($properties->tax * $H23,2,",",".")}}</td>
            <td class="border text-right">{{number_format($properties->tax * $I23,2,",",".")}}</td>
            <td></td>
            <td>Kaufkraftindex</td>
            <td colspan="2" class="text-right">
               <a href="#" class="inline-edit" data-type="text" data-pk="kk_idx" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Kaufkraftindex">
               {{$kk_idx}}
               </a>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="3" class="border-no-right bg-gray">EAT</th>
            <th class="border-no-right text-right bg-gray">{{number_format($E23 - ($properties->tax * $E23),2,",",".")}}</th>
            <th class="border-no-right text-right bg-gray">{{number_format($F23 - ($properties->tax * $F23),2,",",".")}}</th>
            <th class="border-no-right text-right bg-gray">{{number_format($G23 - ($properties->tax * $G23),2,",",".")}}</th>
            <th class="border-no-right text-right bg-gray">{{number_format($H23 - ($properties->tax * $H23),2,",",".")}}</th>
            <th class="border text-right bg-gray">{{number_format($I23 - ($properties->tax * $I23),2,",",".")}}</th>
            <td></td>
            <td>Exklusivität bis</td>
            <td colspan="2" class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-format="yyyy-mm-dd" data-pk="exklusivität_bis" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Exklusivität bis">
               @if($properties->exklusivität_bis)
               {{$properties->exklusivität_bis}}
               @endif</a>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         {{--TODO: start from here--}}
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td colspan="3" class="border-no-right">Tilgung Bank</td>
            <td class="border-no-right text-right">{{number_format($H49,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($I49,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($J49,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($K49,2,",",".")}}</td>
            <td class="border text-right">{{number_format($L49,2,",",".")}}</td>
            <td></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>

         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="3" class="border-no-right bg-gray">Cashflow (nach Steuern)</th>
            <th class="border-no-right text-right bg-gray">{{number_format(($E23 - ($properties->tax * $E23)) - $H49 + $properties->depreciation_nk_money,2,",",".")}} </th>
            <th class="border-no-right text-right bg-gray">{{number_format(($F23 - ($properties->tax * $F23)) - $I49 + $properties->depreciation_nk_money,2,",",".")}}</th>
            <th class="border-no-right text-right bg-gray">{{number_format(($G23 - ($properties->tax * $G23)) - $J49 + $properties->depreciation_nk_money,2,",",".")}}</th>
            <th class="border-no-right text-right bg-gray">{{number_format(($H23 - ($properties->tax * $H23)) - $K49 + $properties->depreciation_nk_money,2,",",".")}}</th>
            <th class="border text-right bg-gray">{{number_format($I31 ,2,",",".")}}</th>
            <td></td>

            <!-- <th class="border text-center">{{__('property.field.address')}}</th>
               <td class="border text-center"><a href="#" class="inline-edit" data-type="text" data-pk="address" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.address')}}">{{$properties->address}}</a></td> -->
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="11" class="border bg-brown">1.) Erwerb- und Erwerbsnebenkosten</th>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <th class="text-center"></th>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class=" text-center"></td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th class="border-no-right bg-gray">Kaufpreis</th>
            <th class="border-no-right bg-gray text-center">Anteil</th>
            <th class="border bg-gray text-center">in EUR</th>
            <td></td>
            <th class="border-no-right bg-gray">Nebenkosten</th>
            <th class="border-no-right bg-gray text-center">Anteil</th>
            <th class="border bg-gray text-center">in EUR</th>
            <td></td>
            <th class="border-no-right bg-gray">Objektinfos</th>
            <th class="border-no-right bg-gray text-center">Gewerbe Fläche</th>
            <th class="border bg-gray text-center"><a href="#" class="inline-edit" data-type="text" data-pk="sonstige_flache" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="">{{$properties->sonstige_flache}}</a> Fläche</th>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td class="border-no-right">Gebäude</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="building" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.building')}} (%)">{{number_format($properties->building*100,2,",",".")}}</a>%</td>
            <td class="border text-right">
               {{ number_format ( $properties->building * $properties->gesamt_in_eur, 2,",","." ) }}
            </td>
            <td></td>
            <td class="border-no-right">GrunderwerbsSt.</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="real_estate_taxes" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.real_estate_taxes')}} (%)">{{number_format($properties->real_estate_taxes*100,2,",",".")}}</a>%</td>
            <td class="border text-right">
               {{number_format($properties->real_estate_taxes * $properties->gesamt_in_eur,2,",",".")}}
            </td>
            <td></td>
            <td class="border-no-right">Vermietet</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="rent_retail" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.rent_retail')}}">{{number_format($properties->rent_retail,2,",",".")}}</a></td>
            <td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="rent_whg" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.rent_whg')}}">{{number_format($properties->rent_whg,2,",",".")}}</a></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td class="border-no-right">Grundstück</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="plot_of_land" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.plot_of_land')}} (%)">{{number_format($properties->plot_of_land*100,2,",",".")}}</a>%</td>
            <td class="border text-right">
               {{ number_format ( $properties->plot_of_land * $properties->gesamt_in_eur, 2 ,",",".") }}
            </td>
            <td></td>
            <td class="border-no-right">Makler</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="estate_agents" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.estate_agents')}} (%)">{{number_format($properties->estate_agents*100,2,",",".")}}</a>%</td>
            <td class="border text-right">
               {{number_format($properties->estate_agents * $properties->gesamt_in_eur,2,",",".")}}
            </td>
            <td></td>
            <td class="border-no-right">Leerstand</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="vacancy_retail" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.vacancy_retail')}}">{{number_format($properties->vacancy_retail,2,",",".")}}</a></td>
            <td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="vacancy_whg" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.vacancy_whg')}}">{{number_format($properties->vacancy_whg,2,",",".")}}</a></td>
            <td></td>
         </tr>
         <tr>
            <th class="border-no-right">Gesamt</th>
            <td class="border bg-gray text-right"></td>
            <th class="border bg-gray text-right bg-yellow">
               <a href="#" class="inline-edit" data-type="text" data-pk="gesamt_in_eur" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}">{{number_format($properties->gesamt_in_eur,2,",",".")}}</a>
            </th>
            <td></td>
            <td class="border-no-right">Notar/Grundbuch</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="Grundbuch" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.Grundbuch')}} (%)">{{number_format($properties->Grundbuch,2,",",".")}}</a>%</td>
            <td class="border text-right">
               {{number_format(($properties->Grundbuch * $properties->gesamt_in_eur)/100,2,",",".")}}
            </td>
            <!--G38*D38-->
            <td>&nbsp;</td>
            <th class="border">Gesamt</th>
            <th class="border text-right">
               {{number_format($properties->vacancy_retail+$properties->rent_retail,2,",",".")}}
            </th>
            <th class="border text-right">{{number_format($properties->vacancy_whg+$properties->rent_whg,2,",",".")}}
            </th>
            <td></td>
         </tr>
         <tr>
            <td colspan="3"></td>
            <td></td>
            <td class="border-no-right">Due Dilligence</td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="evaluation" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.evaluation')}} (%)">{{number_format($properties->evaluation*100,2,",",".")}}</a>%</td>
            <td class="border text-right">
               {{number_format($properties->evaluation * $properties->gesamt_in_eur,2,",",".")}}
            </td>
            <td colspan="4"></td>
         </tr>
         <tr>
            <td class="border-no-right">Gesamtkaufpreis</td>
            <td class="border-no-right">Netto KP</td>
            <td class="border text-right">
               {{number_format($properties->gesamt_in_eur,2,",",".")}}
            </td>
            <td></td>
            <td class="border-no-right">
               <a href="#" class="inline-edit" data-type="text" data-pk="miscellaneous_title" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.others')}}">
               @if($properties->miscellaneous_title)
               {{$properties->miscellaneous_title}}
               @else
               {{'Sonstiges'}}
               @endif
               </a>
            </td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="others" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.others')}} (%)">{{number_format($properties->others*100,2,",",".")}}</a>%</td>
            <td class="border text-right">
               {{number_format($properties->others * $properties->gesamt_in_eur,2,",",".")}}
            </td>
            <td></td>
            <?php
               //declare values
               $Q53 = 0;
               ?>
            @foreach($propertiesExtra1s as $propertiesExtra1)
            <?php
               if($propertiesExtra1->is_dynamic_date)
                   $propertiesExtra1->mv_end2 = date('d.m.Y');

               ?>
            <?php
               if( strpos($properties->duration_from_O38, "unbefristet") !== false){
                   $value = 1;
               }else{
                   $properties->duration_from_O38 = $propertiesExtra1->mv_end2;

                   $date1=date_create(str_replace('/', '-', $propertiesExtra1->mv_end));
                   $date2=date_create(str_replace('/', '-', $propertiesExtra1->mv_end2));

                   $diff=date_diff($date1,$date2);

                   $value =  $diff->format("%a")/ 365;

                   // $value = ((strtotime(str_replace('/', '-', $propertiesExtra1->mv_end)) -  strtotime(str_replace('/', '-', $properties->duration_from_O38))) / 86400) / 365;
               }

               if (strpos($propertiesExtra1->mv_end, '2099') !== false)
                   $value = 0.5;

               $Q53 += $value*$propertiesExtra1->net_rent_p_a;

               ?>
            @endforeach
            <?php
               $P53 = ($properties->net_rent_pa == 0) ? 0 : ($Q53 / $properties->net_rent_pa) ;
               ?>
            <td class="border-no-right">WAULT</td>
            <td colspan="2" class="border  text-center wault-amount">{{number_format($P53,2,",",".")}}</td>
            <td></td>
         </tr>
         <tr>
            <td class="border-no-right"></td>
            <td class="border-no-right">AHK</td>
            <td class="border text-right">
               {{
               number_format( ($properties->real_estate_taxes * $properties->gesamt_in_eur)
               + ($properties->estate_agents * $properties->gesamt_in_eur)
               + (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
               + ($properties->evaluation * $properties->gesamt_in_eur)
               + ($properties->others * $properties->gesamt_in_eur)
               + ($properties->buffer * $properties->gesamt_in_eur) , 2 ,",",".")
               }}
            </td>
            <td></td>
            <td class="border-no-right">
               <a href="#" class="inline-edit" data-type="text" data-pk="buffer_title" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.buffer')}}">
               @if($properties->buffer_title)
               {{$properties->buffer_title}}
               @else
               {{'Puffer'}}
               @endif
               </a>
            </td>
            <td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="buffer" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.buffer')}} (%)">{{number_format($properties->buffer*100,2,",",".")}}</a>%</td>
            <td class="border text-right">
               {{number_format($properties->buffer * $properties->gesamt_in_eur,2,",",".")}}
            </td>
            <td></td>
            <td class="border-no-right">Leerstandsquote</td>
            <td colspan="2" class="border  text-center">{{number_format(100-($L20 * 100), 2,",",".")}}%</td>
            <td></td>
         </tr>
         <tr>
            <th colspan="2" class="border-no-right bg-gray">Gesamtkaufpreis</th>
            <th class="border bg-gray text-right">
               {{
               number_format( $D42 , 2 ,",",".")
               }}
            </th>
            <td></td>
            <th class="border-no-right bg-gray">Gesamt</th>
            <th class="border-no-right bg-gray text-right">{{number_format(($properties->real_estate_taxes+$properties->estate_agents+$properties->Grundbuch/100+$properties->evaluation+$properties->others+$properties->buffer)*100,2,",",".")}}%</th>
            <th class="border bg-gray text-right">
               {{
               number_format( ($properties->real_estate_taxes * $properties->gesamt_in_eur)
               + ($properties->estate_agents * $properties->gesamt_in_eur)
               + (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
               + ($properties->evaluation * $properties->gesamt_in_eur)
               + ($properties->others * $properties->gesamt_in_eur)
               + ($properties->buffer * $properties->gesamt_in_eur) , 2 ,",",".")
               }}
            </th>
            <td></td>
            <td class="border-no-right">Vermietungsstand</td>
            <td colspan="2" class="border text-center">
               {{number_format($L20 * 100, 2,",",".")}}%
               <!-- {{number_format((100-$properties->Leerstandsquote),1,",",".")}} -->
            </td>
            <td></td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
         </tr>
         <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
         </tr>
         <tr>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="11" class="border bg-brown">2.) Finanzierungsstruktur</th>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th class="border-no-right bg-gray">Gesamtkaufpreis</th>
            <th class="border-no-right bg-gray"></th>
            <th class="border bg-gray text-right">
               {{
               number_format( $D42 , 2 ,",",".")
               }}
            </th>
            <td></td>
            <th colspan="2" class="border-no-right bg-gray">Finanz.Kosten Tilgung (p.a.)</th>
            <th class="border-no-right bg-gray">End of year 1</th>
            <th class="border-no-right bg-gray">End of year 2</th>
            <th class="border-no-right bg-gray">End of year 3</th>
            <th class="border-no-right bg-gray">End of year 4</th>
            <th class="border bg-gray">End of year 5</th>
            <td></td>
         </tr>
         <tr>
            <!-- <td class="border-no-right">mit echtem EK</td>
               <td class="border-no-right bg-yellow text-right">
                   <?php if($properties->id != NULL){?>
                   <a href="#" class="inline-edit" data-type="text" data-pk="with_real_ek" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.with_real_ek')}}">{{number_format($properties->with_real_ek*100,2,",",".")}}</a>%
                   <?php }else{?>
                   {{number_format($properties->with_real_ek*100,2,",",".")}}%
                   <?php }?>
               </td>
               <td class="border text-right">
                   {{number_format($D47,2,",",".")}}
               </td>-->
            <td class="border-no-right">Eigenmittel</td>
            <td class="border-no-right bg-yellow text-right">
               <?php if($properties->id != NULL){?>
               <a href="#" class="inline-edit" data-type="text" data-pk="from_bond" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.from_bond')}}">{{number_format($properties->from_bond*100,2,",",".")}}</a>%
               <?php }else{?>
               {{number_format($properties->from_bond*100,2,",",".")}}%
               <?php }?>
            </td>
            <td class="border text-right" readonly>
               {{
               number_format($D48,2,",",".")
               }}
            </td>
            <td></td>
            <td colspan="2" class="border-no-right">Valuta</td>
            <td class="border-no-right text-right">{{number_format($H47,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($I47,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($J47,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($K47,2,",",".")}}</td>
            <td class="border text-right">{{number_format($L47,2,",",".")}}</td>
            <td></td>
         </tr>
         <tr>
            <td class="border-no-right">Bankkredit</td>
            <td class="border-no-right bg-yellow text-right">
               <?php if($properties->id != NULL){?>
               <a href="#" class="inline-edit" data-type="text" data-pk="bank_loan" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.bank_loan')}}">{{number_format($properties->bank_loan*100,2,",",".")}}</a>%
               <?php }else{?>
               {{number_format($properties->bank_loan*100,2,",",".")}}%
               <?php }?>
            </td>
            <td class="border text-right">
               {{
               number_format($D49,2,",",".")
               }}
            </td>
            <td></td>
            <td class="border-no-right">Zins Bankkredit</td>
            <td class="border-no-right bg-yellow text-right">
               <a href="#" class="inline-edit" data-type="text" data-pk="interest_bank_loan" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.interest_bank_loan')}}">{{number_format($properties->interest_bank_loan*100,2,",",".")}}</a>%
            </td>
            <td class="border-no-right text-right">{{number_format($H48,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($I48,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($J48,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($K48,2,",",".")}}</td>
            <td class="border text-right">{{number_format($L48,2,",",".")}}</td>
            <td></td>
         </tr>
         <tr>
            <th class="border-no-right">Summe</th>
            <th class="border-no-right bg-yellow text-right">
               {{number_format(($properties->with_real_ek + $properties->from_bond + $properties->bank_loan)*100,2,",",".")}}%
            </th>
            <th class="border text-right">
               {{number_format($D50,2,",",".")}}
            </th>
            <td></td>
            <td class="border-no-right">Tilgung Bank</td>
            <td class="border-no-right bg-yellow text-right">
               <a href="#" class="inline-edit" data-type="text" data-pk="eradication_bank" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.eradication_bank')}}">{{number_format($properties->eradication_bank*100,2,",",".")}}</a>%
            </td>
            <td class="border-no-right text-right">{{number_format($H49,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($I49,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($J49,2,",",".")}}</td>
            <td class="border-no-right text-right">{{number_format($K49,2,",",".")}}</td>
            <td class="border text-right">{{number_format($L49,2,",",".")}}</td>
            <td></td>
         </tr>
         <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <th class="border-no-right bg-gray">Annuität</th>
            <th class="border-no-right bg-gray text-right">
               {{number_format(($properties->interest_bank_loan + $properties->eradication_bank)*100,2,",",".")}}%
            </th>
            <th class="border-no-right bg-gray text-right">{{number_format($H50,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($I50,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($J50,2,",",".")}}</th>
            <th class="border-no-right bg-gray text-right">{{number_format($K50,2,",",".")}}</th>
            <th class="border bg-gray text-right">{{number_format($L50,2,",",".")}}</th>
            <td></td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td colspan="4"></td>
            <td class="border-no-right">Eigenmittelverzinsung</td>
            <td class="border-no-right bg-yellow text-right">
               <a href="#" class="inline-edit" data-type="text" data-pk="interest_bond" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.interest_bond')}}">{{number_format($properties->interest_bond*100,2,",",".")}}</a>%
            </td>
            <td class="border text-right">{{number_format($H52,2,",",".")}}</td>
            <td colspan="4"></td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="11" class="border bg-brown">3.) Betrieb und Verw.</th>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <th colspan="2" class="border-no-right bg-gray">Netto Miete (IST) p.a.</th>
            <th class="border  text-right">
               {{number_format($properties->net_rent_pa,2,",",".")}}
            </th>
            <td></td>
            <th class="border-no-right bg-gray" colspan="2">Kennzahlen</th>
            <td colspan="3" class="" style="border-left: 0px;"></td>
            <td colspan="2"></td>
            <td>&nbsp;</td>
         </tr>
         <!-- inline-edit-use -->
         <tr>
            <th colspan="2" class="border-no-right"  style="background: #eaeaea;font-style: italic;">Netto Miete (IST) p.m.</th>
            <th class="border  text-right">
               {{number_format($properties->net_rent_pa/12,2,",",".")}}
            </th>
            <td></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <tr>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
         </tr>
         <tr>
            <th colspan="2" class="border bg-gray">Nichtumlagefähige NK</th>
            <th class="border bg-gray"></th>
            <td></td>
            <td class="border-no-right">Faktor (netto)</td>
            <td class="border text-right">{{number_format($L25,2,",",".")}}</td>
            <td></td>
            <td class=""></td>
            <th class="  text-right"></th>
            <td colspan="2"></td>
         </tr>
         <tr>
            <td class="border-no-right text-right">Instandhaltung nichtumlfähig</td>
            <td class="border-no-right text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance_nk" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.maintenance_nk')}} (%)">{{number_format($properties->maintenance_nk*100,2,",",".")}}</a>%</td>
            <td class="border text-right">
               {{ $properties->maintenance_increase_year1 = number_format($properties->gesamt_in_eur
               *($properties->maintenance_nk),2,",",".")
               }}
            </td>
            <td></td>
            <td class="border-no-right asdad ">BruttoRendite</td>
            <td class="border text-right">
               <!-- {{ $D42 }} {{ $properties->net_rent_pa }}  -->
               <!-- <br/>  -->
               {{number_format(($G58 *100),2,",",".")}}%
            </td>
            <td></td>
            <td class=""></td>
            <th class="text-right"></th>
            <td colspan="2"></td>
         </tr>
         <tr>
            <td class="border text-right">Betriebsk. nicht umlfähig</td>
            <td class="border text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="operating_costs_nk" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.operating_costs_nk')}} (%)">{{number_format($properties->operating_costs_nk*100,2,",",".")}}</a>%</td>
            <td class="border text-right">{{number_format($properties->operating_cost_increase_year1,2,",",".")}}</td>
            <td colspan="1"></td>
            <td class="border-no-right">BruttoVervielält.</td>
            <td class="border text-right">{{number_format(($G59),2,",",".")}}</td>
            <td colspan="1"></td>
            <td class=""></td>
            <th class=""></th>
            <td colspan="2"></td>
         </tr>
         <tr>
            <td class="border text-right">Objektverwalt. nichtumlfähig</td>
            <td class="border text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="object_management_nk" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.object_management_nk')}} (%)">{{number_format($properties->object_management_nk*100,2,",",".")}}</a>%</td>
            <td class="border text-right ">{{number_format($properties->property_management_increase_year1,2,",",".")}}</td>
            <td></td>
            <td class="border-no-right">EK-Rendite/CF</td>
            <td class="border text-right">{{number_format(($G61 * 100),2,",",".")}}%</td>
            <!--=E31/D48--->
            <td></td>
            <th class=""></th>
            <th class="">
               <?php
                  //J58*(D42/J60)
                  if($properties->AHK_Salzgitter != 0){
                      $cf_faktor = ($properties->Ref_CF_Salzgitter*
                              (($properties->gesamt_in_eur
                                              + ($properties->real_estate_taxes * $properties->gesamt_in_eur)
                                              + ($properties->estate_agents * $properties->gesamt_in_eur)
                                              + (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
                                              + ($properties->evaluation * $properties->gesamt_in_eur)
                                              + ($properties->others * $properties->gesamt_in_eur)
                                              + ($properties->buffer * $properties->gesamt_in_eur))
                                      /$properties->AHK_Salzgitter));
                  }

                  ?>
            </th>
            <!--(E31+E25)/(J58*(D42/J60))-->
            <td colspan="2"></td>
         </tr>
         <tr>
            <td class="border text-right">Abschreibung</td>
            <td class="border text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.depreciation_nk')}} (%)">{{number_format($properties->depreciation_nk*100,2,",",".")}}</a>%</td>
            <td class="border text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
            <td></td>
            <td class="border-no-right">EK-Rendite/GuV/EBT</td>
            <td class="border text-right">
               {{number_format(($G62 * 100),2,",",".")}}%
            <td></td>
            <th class=""></th>
            <th class="">
               <?php
                  //J59*(D42/J60)
                  if($properties->AHK_Salzgitter != 0){
                      $guv_faktor = (($properties->Ref_GuV_Salzgitter)* (($properties->gesamt_in_eur
                                              + ($properties->real_estate_taxes * $properties->gesamt_in_eur)
                                              + ($properties->estate_agents * $properties->gesamt_in_eur)
                                              + (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
                                              + ($properties->evaluation * $properties->gesamt_in_eur)
                                              + ($properties->others * $properties->gesamt_in_eur)
                                              + ($properties->buffer * $properties->gesamt_in_eur))
                                      /($properties->AHK_Salzgitter)));
                  }

                  ?>
            </th>
            <!--E23/(J59*(D42/J60))-->
            <td colspan="2"></td>
         </tr>

      </tbody>
   </table>
</body>
