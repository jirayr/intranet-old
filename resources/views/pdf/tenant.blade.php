@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <style type="text/css">
        .text-center{
            text-align: center;
        }
    </style>

@endsection
<div id="tenancy-schedule">
    <div>
        <?php
            $leerstand_total = 0;
            $vermietet_total = 0;
        ?>
        <?php
        $sum_actual_net_rent = $sum_total_amount = 0;
        $wault = 0;
        ?>

        @foreach($s['tenancy_schedules'] as $key => $tenancy_schedule)
            <div id="tenancy-schedule-{{$tenancy_schedule->id}}">
                    <table style=" border-collapse: collapse;">
                        <tbody>

                        <?php
                        $csum = $csum1 = $csum2 = $csum3 = 0;
                        ?>
                         <?php
                        $sum_actual_net_rent2 = 0;
                        $sum_actual_net_rent2 +=0;
                        ?>

                        <tr>
                            <th></th>
                            <th></th>
                            <th colspan="2" style="text-align: center">Laufzeit</th>
                            <th style="text-align: center">Fläche</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align: center">Miete</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align: center"></th>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="border-right: 2px solid black ; border-bottom: 2px solid black">Gewerbe</td>
                            <td style="border-bottom: 2px solid black">Mietbeginn&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="border-right: 2px solid black; border-bottom: 2px solid black">Mietende</td>
                            <td style="border-right: 2px solid black; border-bottom: 2px solid black">Fläche in m²</td>
                            <td style="border-bottom: 2px solid black">Mietzins p.m.&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="border-bottom: 2px solid black">NK-Vorausz. p.m.&nbsp;&nbsp;&nbsp;&nbsp; </td>
                            <td style="border-bottom: 2px solid black"> Gesamt Netto p.m.&nbsp;&nbsp;&nbsp;&nbsp; </td>
                            <td style="border-bottom: 2px solid black">19% MwSt.&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="border-bottom: 2px solid black">Gesamt Brutto p.m.&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="border-right: 2px solid black; border-bottom: 2px solid black">Gesamt Brutto p.a. </td>
                            <td style="border-right: 2px solid black; border-bottom: 2px solid black">Mietzins</td>
                            <td style="border-bottom: 2px solid black">Optionen &nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="border-right: 2px solid black; border-bottom: 2px solid black">auszuüben</td>
                            <td style="border-right: 2px solid black; border-bottom: 2px solid black">Kaution</td>
                            <td style="border-right: 2px solid black; border-bottom: 2px solid black">Indexierung</td>
                            <td style="border-right: 2px solid black; border-bottom: 2px solid black">Parkplätze</td>

                            <td>&nbsp;</td>
                            <td>&nbsp;</td>

                        </tr>
                        <?php
                        $csum = $csum1 = $csum2 = $csum3 = 0;
                        ?>
                        @foreach($tenancy_schedule->items as $item)
                            @if($item->type == config('tenancy_schedule.item_type.business'))
                                <tr style="border-right: 2px solid black" class="@if($item->rent_begin > date('Y-m-d')) item-futureactive @elseif($item->rent_end < date('Y-m-d')) item-pastactive @endif">
                                    <td></td>
                                    <td style="border-right: 2px solid black; border-bottom: 2px solid black;">
                                            {{ $item->name }}
                                    </td>
                                    <td style="border-bottom: 2px solid black;"><span data-type="text" data-pk="rent_begin" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-inputclass="mask-input-date" data-title="DD/MM/JJJJ" data-value="@if($item->rent_begin){{date_format(  date_create(str_replace('/', '-', $item->rent_begin)) , 'd/m/Y')}}@endif">{{ $item->rent_begin }}</span></td>
                                    <td style="border-right: 2px solid black;border-bottom: 2px solid black; "><span data-type="text" data-pk="rent_end" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD/MM/JJJJ" data-inputclass="mask-input-date" data-value="@if($item->rent_end){{date_format(  date_create(str_replace('/', '-', $item->rent_end)) , 'd/m/Y')}}@endif">{{ $item->rent_end }}</span></td>
                                    <td style="border-right: 2px solid black; border-bottom: 2px solid black; "><span data-type="number" data-step="0.01" data-pk="rental_space" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->rental_space, 2,",",".") }}</span>m²</td>
                                    <td style="border-bottom: 2px solid black; "><span data-type="text" data-step="0.01" data-pk="actual_net_rent" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->actual_net_rent, 2,",",".") }}</span>&nbsp;€</td>
                                    <td style="border-bottom: 2px solid black; "><span data-type="text" data-step="0.01" data-pk="nk_netto" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->nk_netto, 2,",",".") }}</span>&nbsp;€</td>
                                    <?php
                                    $natto =$item->actual_net_rent+$item->nk_netto;
                                    $tn = ($item->actual_net_rent+$item->nk_netto)*19/100;

                                    $csum += $natto;
                                    $csum1 += $tn;
                                    $csum2 += ($natto+$tn);
                                    $csum3 += ($natto+$tn)*12;

                                    ?>
                                    <td style="border-bottom: 2px solid black; ">{{ number_format($natto, 2,",",".") }}&nbsp;€</td>
                                    <td style="border-bottom: 2px solid black; ">{{ number_format($tn, 2,",",".") }}&nbsp;€</td>
                                    <td style="border-bottom: 2px solid black; ">{{ number_format($natto+$tn, 2,",",".") }}&nbsp;€</td>
                                    <td style="border-right: 2px solid black;border-bottom: 2px solid black; ">{{ number_format(($natto+$tn)*12, 2,",",".") }}&nbsp;€</td>
                                    <td class="text-center" style="border-right: 2px solid black;border-bottom: 2px solid black; ">
                                        @if($item->rental_space && $item->actual_net_rent)
                                            {{ number_format($item->actual_net_rent/$item->rental_space, 2,",",".") }}
                                        @else
                                            {{ number_format(0, 2,",",".") }}

                                        @endif

                                        <?php
                                        if($item->rent_end && substr($item->rent_end,0,4)!="2099")
                                        {
                                            $sum_actual_net_rent += $item->actual_net_rent;
                                            $sum_total_amount += $item->remaining_time_in_eur;
                                        }
                                        ?>
                                    </td>
                                    <td class="text-center" style="border-bottom: 2px solid black; ">
                                        <span data-type="text" data-pk="options" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->options }}</span>
                                        <input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="0" @if($item->selected_option==0) checked @endif >
                                        <input type="checkbox"  data-id="{{ $item->id }}" value="1" @if($item->selected_option==1) checked @endif >
                                        <input type="checkbox" data-id="{{ $item->id }}" value="2" @if($item->selected_option==2) checked @endif >
                                    </td>
                                    <td style="border-right: 2px solid black;border-bottom: 2px solid black; ">{{ $item->comment }}</td>
                                    <td style="border-right: 2px solid black;border-bottom: 2px solid black; "><span  data-type="text" data-pk="kaution" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->kaution }}</span></td>
                                    <td style="border-right: 2px solid black;border-bottom: 2px solid black; "><span  data-type="text" data-pk="indexierung" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->indexierung }}</span></td>
                                    <td style="border-right: 2px solid black;border-bottom: 2px solid black; "></td>

                            @endif
                        @endforeach
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td><br><br><br></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="3" style="border: 2px solid black">Nettokaltmiete p.a.</td>
                                    <td style="border: 2px solid black">{{number_format($tenancy_schedule->calculations['total_actual_net_rent'] * 12, 2,",",".")}}€</td>
                                    <td></td>

                                </tr>
                        <?php
                        $sum_actual_net_rent2 = 0;
                        ?>

                        </tbody>

                    </table>

            </div>
            @break
        @endforeach


    </div>

</div>



@section('js')
    @parent
    {{--<script>--}}
        {{--jQuery(document).ready(function($) {--}}
            {{--$('.checkbox-is-new').on('click', function () {--}}
                {{--var id = $(this).attr('data-id');--}}
                {{--v = 0;--}}
                {{--if($(this).is(':checked'))--}}
                    {{--v = 1;--}}

                {{--$.ajax({--}}
                    {{--type : 'POST',--}}
                    {{--url : "{{url('tenancy-schedule-items/update') }}/"+id,--}}
                    {{--data : { pk : 'is_new', value:v,  _token : '{{ csrf_token() }}' },--}}
                    {{--success : function (data) {--}}
                        {{--// generate_option_from_json(data, 'country_to_state');--}}
                    {{--}--}}
                {{--});--}}
            {{--});--}}
            {{--$('.checkbox-is-vacant').on('click', function () {--}}
                {{--var id = $(this).attr('data-id');--}}
                {{--v = 0;--}}
                {{--if($(this).is(':checked'))--}}
                    {{--v = 1;--}}

                {{--$.ajax({--}}
                    {{--type : 'POST',--}}
                    {{--url : "{{url('tenancy-schedule-items/update') }}/"+id,--}}
                    {{--data : { pk : 'vacancy_on_purcahse', value:v,  _token : '{{ csrf_token() }}' },--}}
                    {{--success : function (data) {--}}
                        {{--// generate_option_from_json(data, 'country_to_state');--}}
                    {{--}--}}
                {{--});--}}
            {{--});--}}

            {{--$('.change-asset-user').on('change', function () {--}}
                {{--var id = $(this).attr('data-id');--}}
                {{--v = $(this).val();--}}
                {{--$.ajax({--}}
                    {{--type : 'POST',--}}
                    {{--url : "{{url('tenancy-schedule-items/update') }}/"+id,--}}
                    {{--data : { pk : 'asset_manager_id', value:v,  _token : '{{ csrf_token() }}' },--}}
                    {{--success : function (data) {--}}
                        {{--// generate_option_from_json(data, 'country_to_state');--}}
                    {{--}--}}
                {{--});--}}
            {{--});--}}

            {{--$('.option-check').on('click', function () {--}}
                {{--var id = $(this).attr('data-id');--}}
                {{--v = -1;--}}
                {{--if($(this).is(':checked'))--}}
                    {{--v = $(this).val();--}}

                {{--$.ajax({--}}
                    {{--type : 'POST',--}}
                    {{--url : "{{url('tenancy-schedule-items/update') }}/"+id,--}}
                    {{--data : { pk : 'selected_option', value:v,  _token : '{{ csrf_token() }}' },--}}
                    {{--success : function (data) {--}}
                        {{--// generate_option_from_json(data, 'country_to_state');--}}
                    {{--}--}}
                {{--});--}}

            {{--});--}}


            {{--$('.warning-price-check').on('click', function () {--}}
                {{--var id = $(this).attr('data-id');--}}
                {{--v = 'NULL';--}}
                {{--if($(this).is(':checked'))--}}
                    {{--v = $(this).val();--}}

                {{--var pk = $(this).attr('data-pk');--}}

                {{--$.ajax({--}}
                    {{--type : 'POST',--}}
                    {{--url : "{{url('tenancy-schedule-items/update') }}/"+id,--}}
                    {{--data : { pk : pk, value:v,  _token : '{{ csrf_token() }}' },--}}
                    {{--success : function (data) {--}}
                        {{--// generate_option_from_json(data, 'country_to_state');--}}
                    {{--}--}}
                {{--});--}}

            {{--});--}}

            {{----}}



            {{--$('.btn-export-tenancy-to-excel').on('click', function () {--}}
                {{--var tenancyScheduleID = $(this).data('tenancy-id');--}}
                {{--$('input#tenancy-id').val(tenancyScheduleID);--}}
                {{--var selector = 'div#tenancy-schedule-' + tenancyScheduleID + ' table';--}}
                {{--var tenancyTableArray = getTableData(selector);--}}

                {{--$('input#tenancy-data').val(JSON.stringify(tenancyTableArray));--}}
                {{--// console.log(JSON.stringify(tenancyTableArray));--}}
                {{--tenancyTableArray = getTableClass(selector);--}}

                {{--$('input#tenancy-class').val(JSON.stringify(tenancyTableArray));--}}

                {{--tenancyTableArray = getTableTdClass(selector);--}}
                {{--$('input#tenancy-td').val(JSON.stringify(tenancyTableArray));--}}

                {{----}}
                {{----}}

                {{--$('#export-tenancy-to-excel-form').submit();--}}
            {{--});--}}


            {{--var tenancySchedulesTable = $('.tenancy-schedules table');--}}
            {{--if (isNaN($.cookie('tenancy-schedule-zoom-value'))){--}}
                {{--$.removeCookie('tenancy-schedule-zoom-value');--}}
            {{--}--}}

            {{--var zoomValue = 1;--}}
            {{--if ($.cookie('tenancy-schedule-zoom-value')){--}}
                {{--zoomValue = parseFloat(parseFloat($.cookie('tenancy-schedule-zoom-value')).toFixed(1));--}}
            {{--}--}}
            {{--else {--}}
                {{--$.cookie('tenancy-schedule-zoom-value', 1);--}}
            {{--}--}}

            {{--$('span#tenancy-schedule-zoom-value').text((zoomValue * 100).toFixed(0));--}}
            {{--tenancySchedulesTable.css({--}}
                {{--'zoom'              : zoomValue,--}}
                {{--'-webkit-transform' : 'scale(' + zoomValue + ')',--}}
                {{--'-moz-transform'    : 'scale(' + zoomValue + ')',--}}
                {{--'-ms-transform'     : 'scale(' + zoomValue + ')',--}}
                {{--'-o-transform'      : 'scale(' + zoomValue + ')',--}}
                {{--'transform'         : 'scale(' + zoomValue + ')',--}}
                {{--'transform-origin'	: 'top left'--}}
            {{--});--}}

            {{--$('#tenacy-schedule-zoom-in').on('click', function () {--}}
                {{--zoomValue = parseFloat(parseFloat($.cookie('tenancy-schedule-zoom-value')).toFixed(1));--}}
                {{--if (zoomValue < 2.0) {--}}
                    {{--zoomValue += 0.1;--}}
                    {{--$.cookie('tenancy-schedule-zoom-value', zoomValue);--}}
                {{--}--}}
                {{--$('span#tenancy-schedule-zoom-value').text((zoomValue * 100).toFixed(0));--}}
                {{--tenancySchedulesTable.css({--}}
                    {{--'zoom'              : zoomValue,--}}
                    {{--'-webkit-transform' : 'scale(' + zoomValue + ')',--}}
                    {{--'-moz-transform'    : 'scale(' + zoomValue + ')',--}}
                    {{--'-ms-transform'     : 'scale(' + zoomValue + ')',--}}
                    {{--'-o-transform'      : 'scale(' + zoomValue + ')',--}}
                    {{--'transform'         : 'scale(' + zoomValue + ')',--}}
                    {{--'transform-origin'	: 'top left'--}}
                {{--});--}}
            {{--});--}}

            {{--$('#tenacy-schedule-zoom-out').on('click', function () {--}}
                {{--zoomValue = parseFloat(parseFloat($.cookie('tenancy-schedule-zoom-value')).toFixed(1));--}}
                {{--if (zoomValue >= 0.6) {--}}
                    {{--zoomValue -= 0.1;--}}
                    {{--$.cookie('tenancy-schedule-zoom-value', zoomValue);--}}
                {{--}--}}
                {{--$('span#tenancy-schedule-zoom-value').text((zoomValue * 100).toFixed(0));--}}
                {{--tenancySchedulesTable.css({--}}
                    {{--'zoom'              : zoomValue,--}}
                    {{--'-webkit-transform' : 'scale(' + zoomValue + ')',--}}
                    {{--'-moz-transform'    : 'scale(' + zoomValue + ')',--}}
                    {{--'-ms-transform'     : 'scale(' + zoomValue + ')',--}}
                    {{--'-o-transform'      : 'scale(' + zoomValue + ')',--}}
                    {{--'transform'         : 'scale(' + zoomValue + ')',--}}
                    {{--'transform-origin'	: 'top left'--}}
                {{--});--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection
