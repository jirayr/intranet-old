<style type="text/css">
    @page { margin: 15px; }
    body { margin: 15px; }
    .pr-table{
        font-size: 12px;
        text-align: center;
    }
    .pr-table thead th{
        background: #7A8C8E;
        color: white;
        font-weight: bold;
        padding: 5px;
    }

    .ads-table thead th{
        background: black;
        color: white;
        font-weight: bold;
        padding: 8px;
    }
    .ads-table tbody td{
        font-size: 12px;
        padding: 2px;
    }
    .text-bold{
        font-weight: bold;
    }
    .text-right{
        text-align: right;
    }
    .text-center{
        text-align: center;
    }
    .ads-table tbody tr:nth-child(even) {background-color: #f2f2f2;}
    .pr-table tbody tr:nth-child(even) {background-color: #D7DBDB;}
    .pr-table tbody tr:nth-child(odd) {background-color: #ECEEEE;}
    .contact p{
        font-size: 13px;
        color: #4a4d50;
    }
    body{
        font-family: arial,sans-serif;
    }
</style>
<title>FCR Intranet</title>
<body>
    <div>

        <div style="float: right;padding-right: 35px;">
            <img src="{{ getcwd() }}/img/logo.png" width="350">
        </div>

        <div style="clear: both;"></div>
        
        <h4>LEERSTÄNDE (€)</h4>

        <div class="pr-table">
            <table style="width: 100%;">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Asset Manager</th>
                        <th>Objekt</th>
                        <th>Leerstand</th>
                        <th>Fläche (m2)</th>
                        <th>Leerstand (€)</th>
                        <th>Typ</th>
                        <th>Kommentare</th>
                    </tr>
                </thead>
                <tbody>
                    @if($data)
                        @foreach ($data as $tenancy_item)
                            <?php 
                                $tenancy_item->vacancy_in_eur = $tenancy_item->calculated_vacancy_in_eur;
                                $comment = ($tenancy_item->comment) ? $tenancy_item->comment : $tenancy_item->comment1;
                            ?>
                            <tr>
                                <td>{{$tenancy_item->id}}</td>
                                <td>
                                    {{ $tenancy_item->creator_name }}
                                </td>
                                <td>
                                    {{$tenancy_item->object_name}}
                                </td>
                                <td>{{$tenancy_item->name}}</td>
                                <td class="text-right">{{$tenancy_item->vacancy_in_qm ? number_format($tenancy_item->vacancy_in_qm,2,",",".") : 0}}</td>
                                <td class="text-right">{{$tenancy_item->vacancy_in_eur ? number_format($tenancy_item->vacancy_in_eur,2,",",".") : 0}}</td>
                                <td>{{$tenancy_item->type}}</td>
                                <td>{!! $comment !!}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>

    </div>
</body>
            
    