<style type="text/css">
@page { margin: 15px; }
body { margin: 15px; }
.pr-table{
    font-size: 12px;
    text-align: center;
}

.pr-table thead th{
    background: #7A8C8E;
    color: white;
    font-weight: bold;
    padding: 5px;
}

.ads-table thead th{
    background: black;
    color: white;
    font-weight: bold;
    padding: 8px;
}
.ads-table tbody td{
    font-size: 12px;
    padding: 2px;
}
.text-bold{
    font-weight: bold;
}
.text-right{
    text-align: right;
}
.text-center{
    text-align: center;
}
.ads-table tbody tr:nth-child(even) {background-color: #f2f2f2;}
.pr-table tbody tr:nth-child(even) {background-color: #D7DBDB;}
.pr-table tbody tr:nth-child(odd) {background-color: #ECEEEE;}
.contact p{
    font-size: 13px;
    color: #4a4d50;
}
body{
    font-family: arial,sans-serif;
}
</style>
<body>

    <!-- <div style="page-break-before: always;"></div> -->
        <?php
        $front = "";
        if(isset($ads2['images']) && $ads2['images'])
        {
            $f_arr = explode(',', $ads2['images']);
            if(isset($f_arr[$ads2['fav_image']]))
            $front = $f_arr[$ads2['fav_image']];
            else
            $front = $f_arr[0];
        }

        ?>
        <div id="page2_main">
            <div style="float: left;font-weight: bold;padding-left: 35px;">
                <h2 style="color: #253848;">Investmentproposal - Factsheet <br>
                {{count($ads)}} Fachmärkte, Einkaufszentren und Einzelhandelsimmobilien</h2>
            </div>
            <div style="float: right;padding-right: 35px;">
                <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>
            <div style="clear: both;"></div>
            <div style="float: left;margin-top: 100px;padding-left: 35px;padding-right: 35px;">
                @if($front && file_exists(getcwd().'/ad_files_upload/'.$front))
                <img src="{{ getcwd() }}/ad_files_upload/{{$front}}" height="530" style="width: 100%;">
                @endif
            </div>
            <h3 class="text-center" style="color: #595959;">Streng vertraulich/ confidential</h3>
        </div>

        <div style="page-break-before: always;"></div>

        <div>
            <div style="float: left;font-weight: bold;padding-left: 35px;">
                <h2 style="color: #253848;">Investmentproposal - Factsheet <br>
                {{count($ads)}} Fachmärkte, Einkaufszentren und Einzelhandelsimmobilien</h2>
            </div>
            <div style="float: right;padding-right: 35px;">
                <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>
            <div style="clear: both;"></div>

            <div class="ads-table">
                <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th style="width: 3%;">Nr.</th>
                            <th style="width: 35%;">Standort</th>
                            <th style="width: 35%;">Objekt</th>
                            <th style="width: 10%;">Miete p.a.</th>
                            <th style="width: 12%;">Kaufpreis EURO</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                        $k=0;
                        ?>
                        <?php
                        $t1 = 0;
                        $t2 = 0;
                        ?>
                        @foreach($ads as $list)
                        <?php
                        $t1 += $list->annual_rent;
                        $t2 += $list->price;
                        if($list->type=="hotel")
                            continue;
                        ?>
                        <tr>
                            <td class="text-bold">{{$k+1}}<?php
                            $k++;
                            ?></td>
                            <td class="text-bold">{{$list->postcode}} {{$list->ad_city_name}}, {{$list->street}},  {{$list->state_name}}</td>
                            <td class="text-bold ">{{$list->title}}</td>
                            
                            <td class="text-right">{{number_format($list->annual_rent,2,",",".")}} €</td>
                            <td class="text-bold text-right">{{number_format($list->price,2,",",".")}} €</td>
                        </tr>
                        @endforeach

                        <tr><td colspan="5">&nbsp;</td></tr>

                        @foreach($ads as $list)
                        <?php
                        //$t1 += $list->annual_rent;
                        //$t2 += $list->price;
                        if($list->type!="hotel")
                            continue;
                        ?>
                        <tr>
                            <td class="text-bold">{{$k+1}}<?php
                            $k++;
                            ?></td>
                            <td class="text-bold">{{$list->postcode}} {{$list->ad_city_name}}, {{$list->street}},  {{$list->state_name}}</td>
                            <td class="text-bold ">{{$list->title}}</td>
                            
                            <td class="text-right">{{number_format($list->annual_rent,2,",",".")}} €</td>
                            <td class="text-bold text-right">{{number_format($list->price,2,",",".")}} €</td>
                        </tr>
                        @endforeach
                        

                        <tr>
                            <td></td>
                            <td class="text-bold">Jahresnettomiete:</td>
                            <td></td>
                            <td class="text-bold text-right">{{number_format($t1,2,",",".")}} €</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-bold">Angebotspreis gesamt:</td>
                            <td></td>
                            <td></td>
                            <td class="text-bold text-right">{{number_format($t2,2,",",".")}} €</td>
                        </tr>




                    </tbody>
                </table>
            </div>
        </div>


        <div style="page-break-before: always;"></div>

        <div>
            <div style="float: left;font-weight: bold;padding-left: 35px;">
                <h2 style="color: #253848;">Standorte und Wirtschaftliche Daten <br>
                 {{count($prs)}} Fachmärkte, Einkaufszentren und Einzelhandelsimmobilien</h2>
            </div>
            <div style="float: right;padding-right: 35px;">
                <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>
            <div style="clear: both;"></div>

            <div class="pr-table">
                <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th style="width: 3%;">Nr.</th>
                            <th style="width: 5%;">PLZ</th>
                            <th style="width: 8%;">Standort</th>
                            <th style="width: 8%;">Typ</th>
                            <th style="width: 10%;" class="text-right">Grundstück m²</th>
                            <th style="width: 8%;" class="text-right">MF m²</th>
                            <!-- <th class="text-right">Zustand</th> -->
                            <!-- <th>Gewerbe m²</th> -->
                            <th style="width: 5%;" class="text-right">BJahr</th>
                            <!-- <th class="text-right">Heizung</th> -->
                            <th style="width: 8%;" class="text-right">JNM EURO</th>
                            <th style="width: 20%;" class="text-right">Ankermieter</th>
                            <th style="width: 5%;" class="text-right">Rendite</th>
                            <th style="width: 5%;" class="text-right">WALT</th>
                            <th class="text-right">Kaufpreis EURO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $t1 = 0;
                        $t2 = 0;
                        $k=0;
                        ?>
                        @foreach($prs as $list)
                        <?php
                        $t1 += $list->annual_rent;
                        $t2 += $list->price;
                        if($list->property_type=="Hotel")
                            continue;
                        ?>
                        <tr>
                            <td class="text-bold">{{$k+1}}<?php
                            $k++;
                            ?></td>
                            <td >{{$list->plz_ort}}</td>
                            <td >{{$list->ad_city_name}}</td>
                            <td >{{$list->property_type}}</td>
                            <td class="text-right">{{number_format($list->plot_of_land_m2,2,",",".")}} m²</td>
                            <td class=" text-right">@if($mf_array[$list->main_property_id]) {{number_format($mf_array[$list->main_property_id],2,",",".")}} m² @else {{number_format($list->Rental_area_in_m2,2,",",".")}} m² @endif
                            </td>
                            <td class="text-right">{{$list->construction_year}}</td>
                            <!-- <td class="text-right">{{$list->heating}}</td> -->
                            <td class="text-bold text-right">{{number_format($list->annual_rent,2,",",".")}}</td>
                            <td class="text-right">{{$list->tenants}}</td>

                            <td class="text-right">{{number_format($list->equity,2,",",".")}}%</td>
                            <td class="text-right">{{number_format($list->wault,1,",",".")}}</td>
                            <td class="text-right">{{number_format($list->price,2,",",".")}}</td>
                        </tr>
                        @endforeach

                        <tr><td colspan="12">&nbsp;</td></tr>

                        @foreach($prs as $list)
                        <?php
                        //$t1 += $list->annual_rent;
                        //$t2 += $list->price;
                        if($list->property_type!="Hotel")
                            continue;

                        ?>
                        <tr>
                            <td class="text-bold">{{$k+1}}<?php
                            $k++;
                            ?></td>
                            <td >{{$list->plz_ort}}</td>
                            <td >{{$list->ad_city_name}}</td>
                            <td >{{$list->property_type}}</td>
                            <td class="text-right">{{number_format($list->plot_of_land_m2,2,",",".")}} m²</td>
                            <td class=" text-right">@if($mf_array[$list->main_property_id]) {{number_format($mf_array[$list->main_property_id],2,",",".")}} m² @else {{number_format($list->Rental_area_in_m2,2,",",".")}} m² @endif
                            </td>
                            <td class="text-right">{{$list->construction_year}}</td>
                            <!-- <td class="text-right">{{$list->heating}}</td> -->
                            <td class="text-bold text-right">{{number_format($list->annual_rent,2,",",".")}}</td>
                            <td class="text-right">{{$list->tenants}}</td>

                            <td class="text-right">{{number_format($list->equity,2,",",".")}}%</td>
                            <td class="text-right">{{number_format($list->wault,1,",",".")}}</td>
                            <td class="text-right">{{number_format($list->price,2,",",".")}}</td>
                        </tr>
                        @endforeach
                       

                    </tbody>
                </table>
            </div>
        </div>

        <div style="page-break-before: always;"></div>

        <div>
            <div style="float: left;font-weight: bold;padding-left: 35px;">
                <h2 style="color: #253848;">Foto – Übersicht mit Objektnummer  <br></h2>
            </div>
            <div style="float: right;padding-right: 35px;">
                <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>
            <div style="clear: both;"></div>
        <div style="float: left;margin-top: 100px;padding-left: 35px;padding-right: 35px;">
            
        <?php
        $city = array();
        $index = 0;
        ?>
        @foreach($ads as $k=>$list)
        <?php
        $front = "";
        if(isset($list['images']) && $list['images'])
        {
            $f_arr = explode(',', $list['images']);
            if(isset($f_arr[$list['fav_image']]))
            $front = $f_arr[$list['fav_image']];
            else
            $front = $f_arr[0];

            //$front = "topa.jpg";
        }

        ?>
        @if($front && file_exists(getcwd().'/ad_files_upload/'.$front))
        <?php
        $city[$index+1] = $list->ad_city_name;
        $index +=1;
        ?>
        @endif
        @endforeach

        <?php
        $top = '100px';
        $index = 0;
        $page = 0;
        $t=0;
        $city_ar = array_values(array_slice($city, 0, 18));
        ?>

        @foreach($ads as $k=>$list)
        <?php

        $front = "";
        if(isset($list['images']) && $list['images'])
        {
            $f_arr = explode(',', $list['images']);
            if(isset($f_arr[$list['fav_image']]))
            $front = $f_arr[$list['fav_image']];
            else
            $front = $f_arr[0];

            //$front = "topa.jpg";
        }

        ?>
        @if($front && file_exists(getcwd().'/ad_files_upload/'.$front))
        <?php
        if($index && $index%18==0 ){
            // $index = 0;

            $top = '120px';
            
            echo '<br>';
            

        }

        if($index && ($index%14==0 || (count($ads)-1)==$k)){



             ?>

            <div style="float: right;width: 18%;height: 315px;border: 1px solid gray;font-size: 14px;padding-left: 10px;">
            @foreach($city_ar as $l=>$li)
            {{++$t}}. {{$li}}<br>
            @endforeach
            </div>
            <?php
            echo '<br>';
            $page = $page+1;
            $city_ar = array_values(array_slice($city, ($page)*18, 18));
            

        }
        ?>
        <img src="{{ getcwd() }}/ad_files_upload/{{$front}}" width="18%" height="150px" style="margin-right: 5px;margin-top: 10px;" />
        <span style="color:#fff;padding: 10px;background: #525659;position: absolute;top:{{$top}};left:-10px;border-radius: 5px">&nbsp;{{++$index}}&nbsp;</span>
        
        @endif
        @endforeach


        




        </div>
        </div>


        <div style="page-break-before: always;"></div>

        <div class="contact">
            <div style="float: left;font-weight: bold;padding-left: 35px;">
                <h2 style="color: #253848;">KONTAKT</h2>
                <h4>DAS TEAM DER FCR IMMOBILIEN AG</h4>
            </div>
            <div style="float: right;padding-right: 35px;">
                <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>
            <div style="clear: both;"></div>

            <div style="float: left;width: 17%;margin-right: 60px;margin-left:70px;">
            <img src="{{ getcwd() }}/img/ar.jpeg" width="100%" height="180px"   />
            <p>Andrea Raudies<br>
            +49 89 413 2496 12<br>
            +49 151 418 06513<br>
            a.raudies@fcr-immobilien.de</p>
            </div>

            <div style="float: left;width: 17%;margin-right: 60px;">
            <img src="{{ getcwd() }}/img/sp.jpeg" width="100%" height="180px"   />
            <p>Sebastian Precht<br>
            +49 89 413 2496 22<br>
            +49 173 568 6738<br>
            s.precht@fcr-immobilien.de</p>
            </div>

            <div style="float: left;width: 17%;margin-right: 60px;">

            <img src="{{ getcwd() }}/img/mr.jpeg" width="100%"  height="180px"  />
            <p>Matthias Riedel<br>
+49 89 413 2496 29<br>
+49 151 1164 0584<br>
m.riedel@fcr-immobilien.de</p>
            </div>

            {{-- <div style="float: left;width: 17%;margin-right: 60px;">
            <img src="{{ getcwd() }}/img/ws.jpg" width="100%" height="180px"  />
            <p>Walid Sharif<br>
+49 89 413 2496 21<br>
+49 162 134 6279<br>
w.sharif@fcr-immobilien.de</p>
            </div>--}}

            



            <div style="float: left;width: 17%;margin-right: 60px;">
            <img src="{{ getcwd() }}/img/ks.jpeg" width="100%"  height="180px"  />
            <p>Kay Schinkel<br>
+49 89 413 2496 26<br>
+49 172 132 9338<br>
k.schinkel@fcr-immobilien.de</p>
            </div>

            <div style="clear: both;"></div>


            <div style="float: left;width: 17%;margin-right: 60px;margin-left: 70px;">
            <img src="{{ getcwd() }}/img/ab.jpeg" width="100%" height="180px"   />
            <p>Anton Bernhardt<br>
+49 89 413 2496 25<br>
+49 172 105 1753<br>
a.bernhardt@fcr-immobilien.de</p>
            </div>
            <div style="float: left;width: 17%;margin-right: 60px;">
            <img src="{{ getcwd() }}/img/js.jpeg" width="100%" height="180px"   />
            <p>Joschka Saffran<br>
+49 89 413 2496 24<br>
+49 162 719 8302<br>
j.saffran@fcr-immobilien.de</p>
            </div>

            <div style="float: left;width: 17%;margin-right: 60px;">
            &nbsp;</div>


            <div style="float: left;margin-right: 60px; bottom: 70px;position: absolute;">
            <p>FCR Immobilien AG<br>
Paul-Heyse-Straße 28<br>
80336 München<br>
www.fcr-immobilien.de<br>
Phone +49 89 413 2496 00<br>
Fax +49 89 413 2496 99<br>
E-Mail info@fcr-immobilien.de</p>
            </div>


            


        </div>
</body>
            
    