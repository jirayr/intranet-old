@php 
require_once(public_path().'/dompdf/autoload.inc.php');
require_once(public_path().'/dompdf/lib/html5lib/Parser.php');
require_once(public_path().'/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php');
require_once(public_path().'/dompdf/lib/php-svg-lib/src/autoload.php');
require_once(public_path().'/dompdf/src/Autoloader.php');
use Dompdf\Dompdf;
ob_start();
function euro_price($number)
{
    return  $number;
}
function format_number($number){
    $n = number_format($number, 2, ',', '.');    
    return $n;
}

function themeqx_price_ng($ad){
    $price = $ad->price;
    $purpose = ($ad->purpose == 'rent') ? ' /month':'';

    $ng = '';
    $show_price = '';
    if ($price > 0){
        $show_price = euro_price($price);
    }

    return $show_price.$ng.$purpose;
}
@endphp
 <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
 @import url("https://fonts.googleapis.com/css?family=Poppins&display=swap");

@font-face {
  font-family: PoppinsBold;
  src: url({{ public_path().'/fonts/Poppins-Bold.ttf' }});
}

@font-face {
  font-family: PoppinsSemiBold;
  src: url({{ public_path().'/fonts/Poppins-SemiBold.ttf' }});
}

@font-face {
  font-family: PoppinsRegular;
  src: url({{ public_path().'/fonts/Poppins-Regular.ttf' }});
}

@page { margin: 0px; }
body { margin: 0px; font-family: "Poppins"!important;}

h1,span {
font-family: "PoppinsBold"!important;
}

</style>
</head>
 
<body style="padding: 20px 0; margin: 0;  width: 100%; ">


<div style="position:relative;">
<img style="position:absolute;top:-125px; width:9.05in;height:12.82in;" src="{{ getcwd() }}/pdf_images/ci_1.png" />
<div style="position:absolute;top:9.5in;left:5.2in;width:3in;margin: 0 auto; line-height:0.8;">
    <p style=" font-size:14px;color:#ffffff; margin: 0; font-family: 'PoppinsBold'!important; ">
    Kontakt
</p>

<p style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; margin: 0;">
@if(isset($user->firma)) {{ $user->firma }} @endif
</p>

<p style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; margin: 0;">
@if(isset($user->name)) {{ $user->name }} @endif
</p>

<a href="mailto:{{ $user->email }}" style="text-decoration: none;">
    <p style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; margin: 0;">E-Mail: @if(isset($user->email)) {{ $user->email }} @endif</p>
</a>

<a href="mailto:test@trash-mail.comMobil" style="text-decoration: none;">
    <p style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; margin: 0;">Mobil: @if(isset($user->mobile)) {{ $user->mobile }} @endif</p>
</a>

<p style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; margin: 0;">
Telefon: @if(isset($user->phone)) {{ $user->phone }} @endif
</p>

<p style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; margin: 0;">
Adresse: @if(isset($user->address)) {{ $user->address }} @endif
</p>

<p style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; margin: 0;"> @if(isset($user->website )) {{ $user->website }} @endif</p>
 
<p style="font-style:normal;font-weight:normal;font-size:11pt;color:#ffffff; margin: 0;"></p><br/></SPAN></div>
<img style="position:absolute;top:-20px;left:1.5in;width:7.73in;height:4.50in" src="{{ getcwd() }}/pdf_images/ri_1.png" />
<img style="position:absolute;top:-20px;left:1.5in;width:7.61in;height:4.28in" src="{{ getcwd() }}/pdf_images/ci_2.png" />
 
<img style="position:absolute;top:15px;left:5.2in;" width="300px" src="{{ getcwd() }}/img/export_logo.png" />

<div style="position:absolute;top:4.04in;left:0.7in;width:6.04in; line-height: 30px;"> 
    <span style="font-size:30pt;color:#ffffff; line-height: 30px;">@if(isset($ad->title)){{ $ad->title }} @endif</span>
</div>
<br>
</div>

<div style="page-break-before: always;"></div>

<div style="position:relative;">
 
<img style="position:absolute;top:-20px;left:0;width:9.02in;height:12.76in" src="{{ getcwd() }}/pdf_images/vi_1.png" />
<img style="position:absolute;top:8in;left:0;width:9.02in;height:4.50in" src="{{ getcwd() }}/pdf_images/ci_6.png" />
<div style="position:absolute;top:520px;left:20px;width:750px;"><span style="font-style:normal;font-size:16pt;color:#ffffff">@if(isset($ad->title)){{ $ad->title }} @endif</span></SPAN><br/></div>

<div style="position:absolute;top:600px;left:20px; width:750px;"><span style="font-style:normal;font-size:14pt;color:#ffffff">Preis: @if($ad->price) {{ number_format($ad->price,2,',','.') }} @endif </span><span style="font-style:normal;font-size:14pt;color:#ffffff"></span><br/></SPAN></div>
<div style="position:absolute;top:600px;right:20px;">
@if($ad->equity>0)           
    <span style="font-style:normal;font-size:14pt;color:#ffffff">{{ format_number($ad->equity) }}% Mietrendite</span>
@endif
</div>
<div style="position:absolute;top:645px;left:20px;"><span style="font-style:normal;font-size:14pt;color:#ffffff">Lage: @if($ad->street) {{$ad->street}}, @endif @if($ad->postcode) {{$ad->postcode}}, @endif @if(isset($city->city_name)) {{ $city->city_name }}, @endif @if(isset($state->state_name)) {{ $state->state_name }}, @endif @if(isset($country->country_name)) {{ $country->country_name }} @endif
    </span></SPAN><br/></div>

<?php
$objarr = getobjekttypearray();
?>

<div style="position:absolute;top:690px;left:20px;"><span style="font-style:normal;font-size:14pt;color:#ffffff">Objekttyp: @if($ad->type) {{$objarr[$ad->type]}} @endif
    </span></SPAN><br/></div>

{{--<div style="position:absolute;top:690px;right:20px;">
    <span style="font-style:normal;font-size:14pt;color:#ffffff">Ankermieter:
@if($ad->tenants)           
     {{$ad->tenants}}
@endif
</span>
</div>--}}

{{--<div style="position:absolute;top:735px;left:20px;"><span style="font-style:normal;font-size:14pt;color:#ffffff">Adresse: @if($ad->address) {{ $ad->address }} @endif
    </span></SPAN><br/></div>--}}

<?php
$j = 0;
?>
@if(isset($ad_images[$j]) && file_exists(getcwd().'/ad_files_upload/'.$ad_images[$j]))
<img style="position:absolute;top:20px;left:20px;width:750px;height:500px; object-fit:cover;" src="{{ getcwd().'/ad_files_upload/'.$ad_images[$j] }}" />
@endif


<img style="position:absolute;top:745px;left:20px;width:750px;height:3.5in" src="{{ getcwd() }}/pdf_images/ci_7.png" />
<img style="position:absolute;top:745px;left:20px;width:750px;height:0.49in" src="{{ getcwd() }}/pdf_images/ci_8.png" />
<div style="position:absolute;top:735px;left:300px;width:2.46in;"><span style="font-style:normal;font-size:21pt;color:#005496">Beschreibung</span><span style="font-style:normal;font-size:21pt;color:#005496"></span><br/></SPAN></div>
<div style="position:absolute;top:770px;left:40px;width:655px;"><span style="font-style:normal;font-size:12pt;color:#ffffff"></span><br/></SPAN><span style="font-style:normal;font-weight:normal;font-size:10pt;color:#ffffff">
	@if(isset($ad->description))
    {!! substr($ad->description,0,700) !!}
    @endif
</span>
</SPAN><br/></div>
</div>
<div style="page-break-before: always;"></div>

<div style="position:relative;">
<img style="position:absolute;top:0;left:0;width:730px;height:13.5in" src="{{ getcwd() }}/pdf_images/vi_2.png" />
<img style="position:absolute;top:-100px;left:0;width:800px; height: 1370px;" src="{{ getcwd() }}/pdf_images/back-3.jpg" />
<img style="position:absolute;top: 0;left: 20px;width: width:730px;height:0.49in" src="{{ getcwd() }}/pdf_images/ci_10.png" />
<div style="position: absolute; top: -20px;left: 300px;"><h1 style="font-style:normal;font-size:21pt;color: white;">Objektdetails</h1></div>
<div style="height: 750px; width: 375px; position: absolute; top: 40px;left: 20px; border: 1px solid #005496;">
    @if($ad->square_unit_space>0)
    <p style="color: #005496;  margin: 0 0 0 10px;   border-bottom: 1px solid #005496;">Grundstücksfläche in m²</p>
    @endif
    @if($ad->rentable_area>0)
    <p style="color: #005496;  margin:0 0 0 10px;   border-bottom: 1px solid #005496;">Vermietbare Fläche in m²</p>
    @endif
    @if($ad->living_space>0)
    <p style="color: #005496;  margin: 0 0 0 10px;   border-bottom: 1px solid #005496;">Wohnfläche in m²</p>
    @endif
    @if($ad->commercial_space>0)
    <p style="color: #005496;  margin: 0 0 0 10px;   border-bottom: 1px solid #005496;">Gewerbefläche in m²</p>
    @endif
    @if($ad->const_year)
    <p style="color: #005496;  margin: 0 0 0 10px;   border-bottom: 1px solid #005496;">Baujahr</p>
    @endif
    @if($ad->current_state)
    <p style="color: #005496;  margin: 0 0 0 10px;   border-bottom: 1px solid #005496;">Zustand</p>
    @endif
    @if($ad->heating)             
    <p style="color: #005496;  margin: 0 0 0 10px; border-bottom: 1px solid #005496;">Heizungsart</p>
    @endif
    @if($ad->energy_available)
      <p style="color: #005496;  margin: 0 0 0 10px;   border-bottom: 1px solid #005496;">Energieausweis liegt vor</p>
        @endif
        @if($ad->energy_value>0)
        <p style="color: #005496;  margin: 0 0 0 10px;   border-bottom: 1px solid #005496;">Energiekennwert</p>
        @endif
        @if($ad->energy_valid)
        <p style="color: #005496;  margin:  0 0 0 10px;   border-bottom: 1px solid #005496;">Energieausweis gültig bis</p>
        @endif
    @if($ad->tenants)
    <p style="color: #005496;  margin: 0 0 0 10px; border-bottom: 1px solid #005496;">Ankermieter</p>
    @endif
    @if($ad->annual_rent>0)
    <p style="color: #005496;  margin: 0 0 0 10px; border-bottom: 1px solid #005496;">Jahresnettomiete in €</p>
    @endif
    @if($ad->equity>0)
    <p style="color: #005496;  margin:0 0 0 10px;  border-bottom: 1px solid #005496;">Mietrendite in %</p>
    @endif
    @if($ad->wault>0)
    <p style="color: #005496;  margin:0 0 0 10px;  border-bottom: 1px solid #005496;">WAULT</p>
    @endif

    @if($ad->location)
    <p style="color: #005496;  margin:0 0 0 10px; ">Lage</p>
    @endif
    

</div>

    
<div style="height: 750px; width: 375px; position: absolute; top: 40px;left: 395px; border: 1px solid #005496; ">
    @if($ad->square_unit_space>0)
    <p style="color: #005496; margin: 0 0 0 10px;   border-bottom: 1px solid #005496;">{!! format_number($ad->square_unit_space) !!} </p>
    @endif
    @if($ad->rentable_area>0)
    <p style="color: #005496; margin: 0 0 0 10px;  border-bottom: 1px solid #005496;">{!! format_number($ad->rentable_area) !!}</p>
    @endif
    @if($ad->living_space>0)
    <p style="color: #005496; margin:  0 0 0 10px;  border-bottom: 1px solid #005496;">{!! format_number($ad->living_space) !!}</p>
    @endif
    @if($ad->commercial_space>0)
    <p style="color: #005496; margin:0 0 0 10px;   border-bottom: 1px solid #005496;">{!! format_number($ad->commercial_space) !!}</p>
    @endif
    @if($ad->const_year)
    <p style="color: #005496; margin:0 0 0 10px;  border-bottom: 1px solid #005496;">{!! ($ad->const_year) !!}</p>
    @endif
    @if($ad->current_state)
    <p style="color: #005496; margin:0 0 0 10px;  border-bottom: 1px solid #005496;">{!! ($ad->current_state) !!}</p>
    @endif
    @if($ad->heating)
    <p style="color: #005496; margin: 0 0 0 10px;   border-bottom: 1px solid #005496;">{!! ($ad->heating) !!}</p>               
    @endif
    @if($ad->energy_available)
    <p style="color: #005496; margin:0 0 0 10px;   border-bottom: 1px solid #005496;">{!! ($ad->energy_available) !!}</p>
    @endif
    @if($ad->energy_value>0)
    <p style="color: #005496; margin:0 0 0 10px; border-bottom: 1px solid #005496;">{!! format_number($ad->energy_value) !!}</p>
    @endif
    @if($ad->energy_valid)
    <p style="color: #005496; margin:0 0 0 10px;  border-bottom: 1px solid #005496;">{!! ($ad->energy_valid) !!}</p>
    @endif

    @if($ad->tenants)
    <p style="color: #005496; margin:0 0 0 10px;  border-bottom: 1px solid #005496;">{!! ($ad->tenants) !!}</p>
    @endif

    @if($ad->annual_rent>0)
    <p style="color: #005496; margin:0 0 0 10px;   border-bottom: 1px solid #005496;">{!! format_number($ad->annual_rent) !!}</p>
    @endif
    @if($ad->equity>0)
    <p style="color: #005496; margin: 0 0 0 10px;  border-bottom: 1px solid #005496;">{!! format_number($ad->equity) !!}</p>
    @endif
    @if($ad->wault>0)
    <p style="color: #005496; margin:0 0 0 10px;  border-bottom: 1px solid #005496;">{!! format_number($ad->wault) !!}</p>
    @endif 
       @if($ad->location)
    <p style="color: #005496; margin: 0 0 0 8px;  width: 360px; text-align: justify;text-justify: inter-word;">
        {!! substr($ad->location,0,450) !!}</p>
        @endif
    
    
</div>
<?php
$j = 1;
?>

<div style="top:820px;position:absolute; width:750px; left: 20px;">
@if(isset($ad_images[$j]) && file_exists(getcwd().'/ad_files_upload/'.$ad_images[$j]))
<img style="width:370px;height:270px; object-fit:cover; float: left; margin-right: 5px;" src="{{ getcwd().'/ad_files_upload/'.$ad_images[$j] }}"/>
<?php
$j = 2;
?>

@endif

@if(isset($ad_images[$j]) && file_exists(getcwd().'/ad_files_upload/'.$ad_images[$j]))
<img style="width:370px;height:270px; object-fit:cover; float: left; margin-left: 5px;" src="{{ getcwd().'/ad_files_upload/'.$ad_images[$j] }}" />
@endif
</div>

</div>

<div style="page-break-before: always;"></div>


<div style="position:relative;">
 <img style="position:absolute;top:-20px;left:0;width:800px;height:12.82in; z-index:-2;" src="{{ getcwd() }}/pdf_images/ci_11.png">
<div style=" text-align: center; margin-top:350px; z-index:999;">
	
    <img src="{{ getcwd() }}/img/export_logo.png" style="margin-bottom: 30px;" width="200px">
	
    <div style="text-align: center; ">
        <span style="font-style:normal;font-size:14px;color:#ffffff">Kontakt</span>
        <span style="font-style:normal;font-size:14px;color:#ffffff"></span><br>
    </div>
	 
        <div style="text-align: center;">
        	@if(isset($user->firma))
            <div style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; width:100%; margin-top:-10px;">{{ $user->firma }}</div> 
            @endif
			@if(isset($user->name))
            <div style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; width:100%; margin-top:-10px;">{{ $user->name }}</div> 
            @endif
			@if(isset($user->email))
            <div style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; width:100%; margin-top:-10px;">E-Mail: {{ $user->email }}</div> 
            @endif
     		@if(isset($user->mobile))
			<div style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; width:100%; margin-top:-10px;">Mobil: {{ $user->mobile }}</div> 
			@endif
			@if(isset($user->phone))
			<div style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; width:100%; margin-top:-10px;">Telefon: {{ $user->phone }}</div> 
			@endif
			@if(isset($user->address))
			<div style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; width:100%; margin-top:-10px;">Adresse: {{ $user->address }}</div> 
			@endif
			@if(isset($user->website))
			<div style="font-style:normal;font-weight:normal;font-size:14px;color:#ffffff; width:100%; margin-top:-10px;">{{ $user->website }}</div>
			@endif
        </div>
 
</div>
</div>
</body>
 </html>
@php
 
  $html = ob_get_contents();
  ob_end_clean();

  $dompdf = new Dompdf();
  $dompdf->setPaper('A4', 'portrait');
  $dompdf->load_html($html,'UTF-8'); 
  $dompdf->render();
  $dompdf->stream($ad->slug."pdf",array('Attachment'=>0));
  //$generete_name = uniqid().'.pdf';
  //session(['pdfname' => $generete_name]);
  //$output = $dompdf->output();
 
  //file_put_contents(public_path().'/properties_pdf/'.$user->id.$slug.'.pdf', $output ); 

@endphp

<!-- <script src="{{ public_path() }}/js/sha512.js" type="text/javascript" charset="utf-8"></script> -->
<!-- <script type="text/javascript" src="{{asset('domjs/jspdf.min.js') }}"></script> -->
<!-- <script type="text/javascript" src="{{ asset('domjs/jquery-git.js') }}"></script> -->

 

