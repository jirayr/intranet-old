<style type="text/css">
    @page { margin: 15px; }
    body { margin: 15px; }
    .pr-table{
        font-size: 12px;
        text-align: center;
    }
    .pr-table thead th{
        background: #7A8C8E;
        color: white;
        font-weight: bold;
        padding: 5px;
    }

    .ads-table thead th{
        background: black;
        color: white;
        font-weight: bold;
        padding: 8px;
    }
    .ads-table tbody td{
        font-size: 12px;
        padding: 2px;
    }
    .text-bold{
        font-weight: bold;
    }
    .text-right{
        text-align: right;
    }
    .text-center{
        text-align: center;
    }
    .ads-table tbody tr:nth-child(even) {background-color: #f2f2f2;}
    .pr-table tbody tr:nth-child(even) {background-color: #D7DBDB;}
    .pr-table tbody tr:nth-child(odd) {background-color: #ECEEEE;}
    .contact p{
        font-size: 13px;
        color: #4a4d50;
    }
    body{
        font-family: arial,sans-serif;
    }
</style>
<title>FCR Intranet</title>
<body>
        <div>
            {{-- <div style="float: left;font-weight: bold;padding-left: 35px;">
                <h2 style="color: #253848;">Standorte und Wirtschaftliche Daten <br>
                 {{count($prs)}} Fachmärkte, Einkaufszentren und Einzelhandelsimmobilien</h2>
            </div> --}}
            <div style="float: right;padding-right: 35px;">
                <img src="{{ getcwd() }}/img/logo.png" width="350">
            </div>
            <div style="clear: both;"></div>

            <div class="pr-table">
                <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Objekt</th>
                            <!-- <th>Status</th> -->
                            <!-- <th>Miete p.a.</th> -->
                            <th>PLZ</th>
                            <th>Ort</th>
                            <th>Bundesland</th>
                            <th>Strasse</th>
                            <th>Hausnummer</th>
                            <th>Netto Miete (IST) p.a.</th>
                            <th>Verkehrswert</th>
                            <!-- <th>Kaufpreis</th> -->
                            <th>Mietrendite</th>
                            <th>Objekttyp</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($data)
                            @foreach ($data as $element)
                                <tr>
                                    <td>{{ $element->id }}</td>
                                    <td>{{ $element->name_of_property }}</td>
                                    {{--<td>
                                        @if($element->status == config('properties.status.in_purchase'))
                                            {{__('property.in_purchase')}}
                                        @elseif($element->status == config('properties.status.offer'))
                                            {{__('property.offer')}}
                                        @elseif($element->status == config('properties.status.exclusive'))
                                            {{__('property.exclusive')}}
                                        @elseif($element->status == config('properties.status.exclusivity'))
                                            {{__('property.exclusivity')}}
                                        @elseif($element->status == config('properties.status.certified'))
                                            {{__('property.certified')}}
                                        @elseif($element->status == config('properties.status.duration'))
                                            {{__('property.duration')}}
                                        @elseif($element->status == config('properties.status.sold'))
                                            {{__('property.sold')}}
                                        @elseif($element->status == config('properties.status.declined'))
                                            {{__('property.declined')}}
                                        @elseif($element->status == config('properties.status.lost'))
                                            {{__('property.lost')}}
                                        @elseif($element->status == config('properties.status.hold'))
                                            {{__('property.hold')}}
                                        @elseif($element->status == config('properties.status.quicksheet'))
                                            {{__('property.quicksheet')}}
                                        @elseif($element->status == config('properties.status.liquiplanung'))
                                            {{__('property.liquiplanung')}}
                                        @endif
                                    </td>--}}
                                    <td>{{ $element->plz_ort }}</td>
                                    <td>{{ $element->ort }}</td>
                                    <td>{{ $element->niedersachsen }}</td>
                                    <td>{{ $element->strasse }}</td>
                                    <td>{{ $element->hausnummer }}</td>
                                    <?php
                                    $n = 0;
                                    ?>
                                    @if($element->total_actual_net_rent)
                                    <?php
                                    $n = $element->total_actual_net_rent;
                                    ?>
                                    <td class="text-right">{{ show_number($element->total_actual_net_rent, 2) }}</td>
                                    @else
                                    <?php
                                    $n = $element->net_rent_pa;
                                    ?>
                                    <td class="text-right">{{ show_number($element->net_rent_pa, 2) }}</td>
                                    @endif
                                    <td class="text-right">{{ show_number($element->verkehrswert, 2) }}</td>
                                    <td class="text-right">
                                        @if($element->verkehrswert && $n)
                                        {{ show_number((100*$n)/$element->verkehrswert, 2) }}
                                        @else
                                        0,00
                                        @endif
                                    </td>
                                    <!-- <td class="text-right">{{ show_number($element->gesamt_in_eur, 2) }}</td> -->
                                    <td>
                                        @php
                                            $a = array('shopping_mall'=>'Einkaufszentrum', 'retail_center'=>'Fachmarktzentrum','specialists'=>'Fachmarkt', 'retail_shop'=>'Einzelhandel','office'=>'Büro','logistik'=>'Logistik','commercial_space'=>'Kommerzielle Fläche','land'=>'Grundstück','apartment'=>'Wohnung','condos'=>'Eigentumswohnung','house'=>'Haus','living_and_business'=>'Wohn- und Geschäftshaus','villa'=>'Villa','hotel'=>'Hotel','nursing_home'=>'Pflegeheim', 'multifamily_house'=>'Mehrfamilienhaus');
                                        @endphp
                                        {{ (isset($a[$element->property_type])) ? $a[$element->property_type] : '' }}
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
</body>
            
    