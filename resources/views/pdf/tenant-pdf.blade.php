<style type="text/css">
	.text-center{
		text-align: center;
	}
	.text-right{
		text-align: right;
	}
	th{
		font-size:19px;
		text-align: center;
	}
	td{
		font-size:21px;
	}
	.border-right{
		border-right: 1px solid black;
	}
	.no-border{
		border: 0px !important;
	}
	.item-pastactive {
	    background-color: #a0a0a0 !important;
	}
	.item-futureactive {
	    background-color: #cccaca !important;
	}
	body{
		font-family: arial,sans-serif;
	}
</style>
<h1>Mieterliste (Stand: {{ show_date_format(date('Y-m-d')) }})</h1>
<h1>{{$property_name}}</h1>
<table border="1" style="border-collapse: collapse;width: 100%;">
	<thead>
		<tr>
			<th style="width: 15%"></th>
			<th style="font-size: 25px;" colspan="2" class="border-right">Laufzeit</th>
			<th style="font-size: 25px;">Fläche</th>
			<th colspan="7" style="font-size: 25px;" class="border-right">Miete</th>
			<th class="border-right"></th>
			<th class="border-right" style="font-size: 25px;"></th>
			<th class="border-right"></th>
			<th class="border-right"></th>
			<th class="border-right"></th>
			<!-- <th class="border-right"></th> -->
		</tr>
		<tr>
			<th style="width: 10%" class="border-right">Mieter</th>
			<th>Mietbeginn</th>
			<th class="border-right">Mietende</th>
			<th class="border-right">Fläche in m²</th>
			<th>Mietzins p.m.</th>
			<th>Mietzins p.a.</th>
			<th>NK-Vorausz. p.m.</th>
			<th>Gesamt Netto p.m.</th>
			<th>19% MwSt.</th>
			<th>Gesamt Brutto p.m.</th>
			<th class="border-right">Gesamt Brutto p.a.</th>
			<th>Mietzins</th>
			<th>Option</th>
			<!-- <th class="border-right">auszuüben</th> -->
			<th class="border-right">Kaution</th>
			<th class="border-right">Indexierung</th>
			<th class="border-right">Kommentar</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$a = 0;
		?>
		@foreach($s['tenancy_schedules'] as $key => $tenancy_schedule)
			@foreach($tenancy_schedule->items as $item)

				@if( $item->type == 1 && ( $item->rent_end == '' || date('Y-m-d', strtotime($item->rent_end)) >= date('Y-m-d') ) )
					<tr class="@if($item->rent_begin > date('Y-m-d')) item-futureactive @elseif($item->rent_end && $item->rent_end < date('Y-m-d')) item-pastactive @endif">
						<td>{{$item->name}}</td>
						<?php
							$a = $tenancy_schedule->calculations['total_actual_net_rent']*12;
							// $a += $item->actual_net_rent*12;
							if($item->rent_begin != "")
							{
								$date=date_create($item->rent_begin);
								$item->rent_begin = date_format($date,"d.m.Y");

							}
							if($item->rent_end != "")
							{
								$date=date_create($item->rent_end);
								$item->rent_end = date_format($date,"d.m.Y");
							}
						?>
						<td class="text-right">{{$item->rent_begin}}</td>
						<td class="text-right">{{$item->rent_end}}</td>
						<td class="text-right">{{number_format($item->rental_space, 2,",",".") }} m²</td>
						<?php
			                $natto =$item->actual_net_rent+$item->nk_netto; 
			                $tn = ($item->actual_net_rent+$item->nk_netto)*19/100;

			                // $csum += $natto;
			                // $csum1 += $tn;
			                // $csum2 += ($natto+$tn);
			                // $csum3 += ($natto+$tn)*12;

			            ?>
						<td class="text-right">{{ number_format($item->actual_net_rent, 2,",",".") }}&nbsp;€</td>
						<td class="text-right">{{ number_format($item->actual_net_rent*12, 2,",",".") }}&nbsp;€</td>
						<td class="text-right">{{ number_format($item->nk_netto, 2,",",".") }}&nbsp;€</td>

						<td class="text-right">{{ number_format($natto, 2,",",".") }}&nbsp;€</td>
						<td class="text-right">{{ number_format($tn, 2,",",".") }}&nbsp;€</td>
						<td class="border text-right">{{ number_format($natto+$tn, 2,",",".") }}&nbsp;€</td>

			            <td class="border text-right">{{ number_format(($natto+$tn)*12, 2,",",".") }}&nbsp;€</td>
						<td class="border text-center">
							@if($item->rental_space && $item->actual_net_rent)
	                			{{ number_format($item->actual_net_rent/$item->rental_space, 2,",",".") }}
	                		@else
	                			{{ number_format(0, 2,",",".") }}

	                		@endif
						</td>
						<td class="border text-center">{{$item->options}}</td>
						<!-- <td>{{$item->comment}}</td> -->
						<td>{{$item->kaution}}</td>
						<td>{{$item->indexierung}}</td>
						<td>{{$item->extComment() ? $item->extComment()->external_comment : $item->comment2}}</td>
					</tr>
				@endif
			@endforeach
		@endforeach
		<?php $count = 0;?>

		@foreach($s['tenancy_schedules'] as $key => $tenancy_schedule)
			@foreach($tenancy_schedule->items as $item)

				<?php
				if(($item->type == config('tenancy_schedule.item_type.live_vacancy') || $item->type == config('tenancy_schedule.item_type.business_vacancy')))
					$count++;
				?>

				@if( $item->type == 2 && ( $item->rent_end == '' || date('Y-m-d', strtotime($item->rent_end)) >= date('Y-m-d') ) )
					<tr class="@if($item->rent_begin > date('Y-m-d')) item-futureactive @elseif($item->rent_end && $item->rent_end < date('Y-m-d')) item-pastactive @endif">
						<td>{{$item->name}}</td>
						<?php
						$a = $tenancy_schedule->calculations['total_actual_net_rent']*12;
						// $a += $item->actual_net_rent*12;
						if($item->rent_begin != "")
						{
							$date=date_create($item->rent_begin);
							$item->rent_begin = date_format($date,"d.m.Y");

						}
						if($item->rent_end != "")
						{
							$date=date_create($item->rent_end);
							$item->rent_end = date_format($date,"d.m.Y");
						}
						?>

						<td class="text-right">{{$item->rent_begin}}</td>
						<td class="text-right">{{$item->rent_end}}</td>
						<td class="text-right">{{number_format($item->rental_space, 2,",",".") }} m²</td>
						<?php
			                $natto =$item->actual_net_rent+$item->nk_netto; 
			                $tn = ($item->actual_net_rent+$item->nk_netto)*19/100;

			                // $csum += $natto;
			                // $csum1 += $tn;
			                // $csum2 += ($natto+$tn);
			                // $csum3 += ($natto+$tn)*12;

			                ?>
						<td class="text-right">{{ number_format($item->actual_net_rent, 2,",",".") }}&nbsp;€</td>
						<td class="text-right">{{ number_format($item->actual_net_rent*12, 2,",",".") }}&nbsp;€</td>
						<td class="text-right">{{ number_format($item->nk_netto, 2,",",".") }}&nbsp;€</td>

						<td class="text-right">{{ number_format($natto, 2,",",".") }}&nbsp;€</td>
						<td class="text-right">{{ number_format($tn, 2,",",".") }}&nbsp;€</td>
						<td class="border text-right">{{ number_format($natto+$tn, 2,",",".") }}&nbsp;€</td>

			            <td class="border text-right">{{ number_format(($natto+$tn)*12, 2,",",".") }}&nbsp;€</td>
						<td class="border text-center">
							@if($item->rental_space && $item->actual_net_rent)
			                {{ number_format($item->actual_net_rent/$item->rental_space, 2,",",".") }}
			                @else
			                {{ number_format(0, 2,",",".") }}

			                @endif

						</td>
						<td class="border text-center">{{$item->options}}</td>
						<!-- <td>{{$item->comment}}</td> -->
						<td>{{$item->kaution}}</td>
						<td>{{$item->indexierung}}</td>
						<td>{{$item->extComment() ? $item->extComment()->external_comment : $item->comment2}}</td>

					</tr>
				@endif
			@endforeach
		@endforeach

	</tbody>
</table>

<div class="text-center" style="margin-left:130px;float:left;border: 1px solid;width: 15%;margin-top: 15px;font-size: 25px;"><strong>Nettokaltmiete</strong></div>
<div class="text-center" style="float:left;border: 1px solid;width: 10%;font-size: 25px;"><strong>{{ number_format($a, 2,",",".") }}&nbsp;€</strong></div>

@if($count > 0)

	<br>
	<h1>Leerstandsflächen</h1>

	<table border="1" style="border-collapse: collapse;width: 40%;">
		<thead>
			<tr>
				<th style="font-size: 25px;" class="border-right">Leerstand</th>
				<th style="font-size: 25px;">Mietfläche</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$a = 0;
			?>
			@foreach($s['tenancy_schedules'] as $key => $tenancy_schedule)
				@foreach($tenancy_schedule->items as $item)
					@if (($item->type == config('tenancy_schedule.item_type.live_vacancy') || $item->type == config('tenancy_schedule.item_type.business_vacancy'))):
						<tr>
							<td>{{$item->name}}</td>
							<td class="text-right">{{ number_format($item->vacancy_in_qm, 2,",",".") }} m²</td>
						</tr>
					@endif
				@endforeach
			@endforeach
		</tbody>
	</table>

@endif