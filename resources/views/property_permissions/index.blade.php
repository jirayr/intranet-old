@extends('layouts.admin')

@section('content')

	<div class="row">
		<div class="col-md-12">
			@include('partials.flash')
		</div>
	</div>

    <div class="row">

        {!! Form::open(['action' => ['PropertyPermissionController@store'], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
	        {!! Form::token() !!}
	        <div class="col-md-12 col-lg-12 col-sm-12">
	            <div class="panel">
	                <div class="panel-heading">Create Property Permission</div>
	                <div class="white-box">

						<div class="row form-group">
				    		<div class="col-md-6">
							    <label>User</label>
							    <select class="form-control" name="user_id" required>
							    	<option value="">Select User</option>
							    	@if(isset($users) && $users)
							    		@foreach ($users as $u)
							    			<option value="{{ $u->id }}" {{ (old('user_id') == $u->id) ? 'selected' : '' }} >{{ $u->name }}</option>
							    		@endforeach
							    	@endif
							    </select>
							</div>
							<div class="col-md-6">
							    <label>Property Id</label>
							   	<input type="text" class="form-control" name="property_id" value="{{ old('property_id') }}" required>
							</div>
				    	</div>

				    	<div class="row form-group">
	                        <div class="col-md-12 text-center">
	                            <button type="submit" name="submit" class="btn btn-primary btn-rounded">Submit</button>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
     	{!! Form::close() !!}

    </div>

@endsection
