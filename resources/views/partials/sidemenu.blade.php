<?php
        $current_user = Auth::user();
		$banks = App\Models\Banks::all();
?>
<div class="navbar-default sidebar @if(isset($hidesidebar) && $hidesidebar) hide @endif" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3>
                {{--<span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> --}}
                <span class="hide-menu"><img src="{{ asset('img/logo.png') }}" alt="" style="width: 100%">
</span></h3> </div>
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div><img src="{{(config('upload.avatar_path').$current_user->image)}}" alt="user-img" class="img-circle"></div>
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <?php echo $current_user->last_name?
                        ($current_user->first_name . ' ' . $current_user->last_name) : $current_user->name ?>
                    <span class="caret"></span></a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="{{route('edit-profile',['id'=>Auth::user()->id])}}"><i class="ti-user"></i> {{__('app.my_profile')}}</a></li>
                    <li><a href="#"><i class="ti-email"></i> {{__('app.inbox')}}</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{route('change-password')}}"><i class="ti-lock"></i> {{__('app.change_password')}}</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{route('logout')}}"><i class="fa fa-power-off"></i> {{__('app.logout')}}</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav" id="side-menu">
        @if($current_user->role >= 6)
            @if(in_array($current_user->role, [7,8]))
                <li> 
                    <a href="{{ url('extern_user/dashboard') }}" class="waves-effect"><i class="mdi mdi-view-dashboard fa-fw"></i> <span class="hide-menu">Dashboard</span></a>
                </li>
            @endif
            <li> <a href="{{ url('extern_user') }}" class="waves-effect"><i class="mdi mdi-folder fa-fw"></i> <span class="hide-menu">FCR Drive</span></a></li>
        @else

            @if($current_user->email == "test@test.de")
                <li> <a href="https://fcrimmo.com/?" style="margin:0 17px !important" type="button" class="new_property_btn btn-success" ><span class="glyphicon-plus"></span> {{__('app.side_menu.immo_ai')}}</a></li>
            @else
                <!--<li> <a href="{{route('create_property_direct')}}" class="new_property_btn btn-success"><span class="glyphicon-plus"></span> {{__('app.side_menu.upload_new_listing')}}</a></li>-->
                @if (!Auth::user()->isGuest())

                    <li>
                        <form method="POST" action="{{route('create_property_direct')}}">
                            {!! csrf_field() !!}
                            <button style="margin:0 17px !important" type="submit" id="select_bank_btn" class="new_property_btn btn-success"><span class="glyphicon-plus"></span> {{__('app.side_menu.upload_new_listing')}}</button>
                        </form>
                    </li>
                @endif
            @endif
            {{-- 
            @if($current_user->role == 1)
                <li> <a href="{{ url('module_permission') }}" class="waves-effect"><i class="mdi mdi-lock fa-fw"></i> <span class="hide-menu">Module Permission</span></a></li>
            @endif
            --}}

            @if ($current_user->email == config('users.falk_email') ||  $current_user->email == "a.raudies@fcr-immobilien.de" ||  $current_user->email == "mrv@admin.com" ||  $current_user->email == "a.lauterbach@fcr-immobilien.de" ||  $current_user->email == "l.schaut@fcr-immobilien.de" ||  $current_user->email == "c.wiedemann@fcr-immobilien.de" ||  $current_user->email == "t.raudies@fcr-immobilien.de" || strtolower($current_user->email)=="u.wallisch@fcr-immobilien.de" || strtolower($current_user->email)=="m.heinrich@fcr-immobilien.de" || strtolower($current_user->email)=="yiicakephp@gmail.com" || strtolower($current_user->email)=="m.sedlmeier@fcr-immobilien.de" || $current_user->role==5)
                <li> <a href="{{route('dashboard')}}" class="waves-effect"><i class="mdi mdi-view-dashboard fa-fw"></i> <span class="hide-menu">Dashboard</span></a></li>
                <li> <a href="http://ui.immowin24.de/auth/login" class="waves-effect"><i class="mdi mdi-av-timer fa-fw"></i> <span class="hide-menu">Immo KI</span></a></li>
            @endif

            @if ($current_user->email == "j.klausch@fcr-immobilien.de" || $current_user->role == 4)
                <li> <a href="{{route('dashboard')}}" class="waves-effect"><i class="mdi mdi-view-dashboard fa-fw"></i> <span class="hide-menu">Dashboard</span></a></li>
            @endif

            @if ($current_user->role == 2)
                <li> <a href="{{route('dashboard')}}" class="waves-effect"><i class="mdi mdi-view-dashboard fa-fw"></i> <span class="hide-menu">Dashboard</span></a></li>
            @endif

            @if (($current_user->role==2 || $current_user->role==4) && ($current_user->second_role==2 || $current_user->second_role==4))
                <li> <a href="{{route('dashboard2')}}" class="waves-effect"><i class="mdi mdi-view-dashboard fa-fw"></i> <span class="hide-menu">Dashboard2</span></a></li>
            @endif


                <li> <a href="{{route('index')}}" class="waves-effect"><i class="mdi mdi-av-timer fa-fw"></i> <span class="hide-menu">{{__('app.transactionmanagement')}}</span></a>
                </li>

                <li> <a href="{{route('external')}}" class="waves-effect"><i class="mdi mdi-av-timer fa-fw"></i> <span class="hide-menu">Externe Objekte</span></a>
                </li>

            
            @if (!Auth::user()->isGuest())
                <li> <a href="{{route('assetmanagement')}}" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> {{__('app.assetmanagmenet')}} </span></a>
                </li>
            @endif

            <li> <a href="{{ url('trello') }}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">Trello</span></a></li>
            <li> <a href="{{ url('fcr_drive') }}" class="waves-effect"><i class="mdi mdi-folder fa-fw"></i> <span class="hide-menu">FCR Drive</span></a></li>
            <li> <a href="{{ url('verkaufsportal') }}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">Verkaufsportal</span></a></li>
            <li> <a href="{{ url('vermietungsportal') }}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">Vermietungsportal</span></a></li>
            <li> <a href="{{ url('tawk') }}" class="waves-effect"><i class="mdi-facebook-messenger mdi fa-fw"></i> <span class="hide-menu">Tawk</span></a></li>


            @if (!Auth::user()->isGuest())

            <li> <a href="{{route('masterliste.index')}}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">{{__('masterliste.title')}}</span></a></li>
            <li> <a href="{{route('properties.index')}}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">{{__('app.properties')}}</span></a></li>
	    <li> <a href="{{route('banks')}}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">Banken</span></a></li>
	    <li> <a href="{{route('bankAccountsBalance')}}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">Bankkonten</span></a></li>

        <li> <a href="{{route('contacts')}}" class="waves-effect"><i class="ti-user fa-fw"></i> <span class="hide-menu">Kontakte</span></a>
            </li>
         <li>
             <a href="{{route('company')}}" class="waves-effect"><i class="ti-user fa-fw"></i> <span class="hide-menu">Adressen</span></a>
         </li>

            @endif






            @if (!Auth::user()->isGuest())


            <li class="devider"></li>

            <li> <a href="{{route('forecast.index')}}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">{{__('forecast.forecast_al_ls')}}</span></a></li>
            <li> <a href="{{route('masterliste.listing')}}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">{{__('masterliste.listing')}}</span></a></li>
            <li class="devider"></li>
            @endif

            @if (Auth::user()->isAdmin())

            <li> <a href="{{route('users.index')}}" class="waves-effect"><i  class="ti-user fa-fw"></i> <span class="hide-menu">{{__('app.users')}}</span></a> </li>
            <li> <a href="{{route('heatmap')}}" class="waves-effect"><i class="mdi mdi-google-maps fa-fw"></i> <span class="hide-menu">Heatmap</span></a></li>

            <!-- <li><a href="{{route('users.add_api_contacts')}}" class="waves-effect"><i  class="ti-user fa-fw"></i> <span class="hide-menu">Contact</span></a></li>
            -->
            @endif
            @if (!Auth::user()->isGuest())

            <li class="devider"></li>
            <li> <a href="{{route('calendar.index')}}" class="waves-effect"><i class="mdi mdi-calendar-check fa-fw"></i> <span class="hide-menu">{{__('app.calendar')}}</span></a></li>
            <li class="devider"></li>

			<li><a href="{{route('feedback')}}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">{{__('app.feedback')}}</span></a></li>
			<li> <a href="{{url('adressen')}}" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">Adressen Scrape</span></a></li>

            @if(strtolower($current_user->email)==config('users.falk_email') || strtolower($current_user->email)=="c.wiedemann@fcr-immobilien.de" || strtolower($current_user->email)=="l.schaut@fcr-immobilien.de" || strtolower($current_user->email)=="m.heinrich@fcr-immobilien.de" || strtolower($current_user->email)=="a.raudies@fcr-immobilien.de" || strtolower($current_user->email)=="yiicakephp@gmail.com")
                <li><a href="{{route('login_history')}}" class="waves-effect"><i class="mdi mdi-history fa-fw"></i> <span class="hide-menu">Login-Verlauf</span></a></li>
            @endif

            <li><a href="{{route('logout')}}" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">{{__('app.logout')}}</span></a></li>
            <li class="devider"></li>
            @endif

        @endif
        </ul>

    </div>
</div>


<div class="modal fade" id="nobank_Modal" tabindex="-1" role="dialog" aria-labelledby="nobank_Modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">{{__('app.side_menu.you_need_to_add_bank')}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <a href="{{ url('banks/add') }}">{{__('app.side_menu.click_here_to_add_a_bank')}}</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('app.side_menu.close')}}</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="selectbank_Modal" tabindex="-1" role="dialog" aria-labelledby="selectbank_Modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form method="POST" action="{{route('create_property_direct')}}">
		  {!! csrf_field() !!}
		  <div class="modal-header">
			<h4 class="modal-title" id="selectbank_Label">{{__('app.side_menu.select_banks')}}</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<h5>{{__('app.side_menu.choose_banks_for_property')}}:</h5>

            <select id="select_bank" name="select_bank[]" class="select2" multiple="multiple">
                @foreach($banks as $bank)
                <option value="{{$bank->id}}">{{$bank->name}}</option>
                @endforeach
            </select>
			<!-- @foreach($banks as $bank)
			<label>{{$bank->name}}</label> <input class="select_bank" type="checkbox" name="select_bank[]" value="{{$bank->id}}"/>&nbsp &nbsp
			@endforeach -->
			<input type="hidden" id="masterliste_id" name="masterliste_id" value="">
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('app.side_menu.close')}}</button>
            <button  id="skip" type="submit" class="btn btn-primary">{{__('app.side_menu.skip')}}</button>
			<button  id="selectbank_create_btn" type="submit" class="btn btn-primary" disabled>{{__('app.side_menu.create')}}</button>
          </div>

		</form>
    </div>
  </div>
</div>
