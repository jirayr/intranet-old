<?php
$current_user = Auth::user();
$notifications = \App\Services\NotificationsService::getAllNotifications();
$nNewNotifications = \App\Services\NotificationsService::countNewNotifications();
?>

<nav class="navbar navbar-default navbar-static-top m-b-0 @if(isset($hidesidebar) && $hidesidebar) hide @endif">
    <div class="navbar-header">
        <div class="top-left-part">
            <!-- Logo -->
            <a class="logo" href="index.html">
                <!-- Logo icon image, you can use font-icon also --><b>
                    <!--This is dark logo icon--><img src="{{ asset('assets/plugins/images/admin-logo.png') }}" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="{{ asset('assets/plugins/images/admin-logo-dark.png') }}" alt="home" class="light-logo hidden" />
                </b>
                <!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!--This is dark logo text--><img src="{{ asset('assets/plugins/images/admin-text.png') }}" alt="home" class="dark-logo" /><!--This is light logo text--><img src="{{ asset('assets/plugins/images/admin-text-dark.png') }}" alt="home" class="light-logo" />
                     </span> </a>
        </div>
        <!-- /Logo -->
        <!-- Search input and Toggle icon -->
        <ul class="nav navbar-top-links navbar-left toogle-menu-custom">
            <li><a href="javascript:void(0)" class="open-close waves-effect waves-light"><i class="ti-menu"></i></a></li>
            <li>
                <a href="{{ url('webmail') }}" class="waves-effect waves-light">
                <img src="{{ asset('img/outlook.png') }}" width="22" />
                </a></li>
            
            </li>
            <li class="dropdown">
                <a id="notification" onclick="seenNotification()" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="mdi mdi-gmail"></i>
                    <div class="notify">

                        @if($nNewNotifications > 0)
                            <span class="heartbit new-notification"></span>
                            <span class="point new-notification"></span>
                        @endif
                    </div>
                </a>
                <ul class="dropdown-menu mailbox animated bounceInDown" style="overflow-y: scroll; max-height: 500px; width: 380px">
                    <li>
                        <div class="drop-title">{{__('app.header.you_have')}} <span id="notification-number">{{$nNewNotifications}}</span> {{__('app.header.new_messages')}}</div>
                    </li>
                    <li>
                        <div class="message-center">
                            @foreach($notifications as $notification)
                                <a data-id="{{$notification['id']}}" href="{{route('properties.show', $notification['property_id'])}}" @if($notification['is_seen'] == false) class="new" @endif>
                                    <div class="user-img"> <img src="{{$notification['created_user_avatar']}}" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                    <div class="mail-contnet">
                                        <h5>{{$notification['created_user_name']}}</h5>
                                        <span class="mail-desc">{{$notification['content']}}</span>
                                        <span class="time">{{$notification['created_at']}}</span>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </li>
                    {{--<li>--}}
                    {{--<a class="text-center" href="javascript:void(0);"> <strong>{{__('app.header.see_all_notifications')}}</strong> <i class="fa fa-angle-right"></i> </a>--}}
                    {{--</li>--}}
                </ul>
                <!-- /.dropdown-messages -->
            </li>
            <li>
                <a href="https://trello.com" class="waves-effect waves-light" target="_blank">
                    <i class="mdi mdi-trello"></i>
                </a>
            </li>
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">

            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{(config('upload.avatar_path').$current_user->image)}}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">
                        <?php echo $current_user->first_name? $current_user->first_name : $current_user->name ?>
                    </b><span class="caret"></span> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-img"><img src="{{(config('upload.avatar_path').$current_user->image)}}" alt="user" /></div>
                            <div class="u-text">
                                <h4><?php echo $current_user->last_name?
                                        ($current_user->first_name . ' ' . $current_user->last_name) : $current_user->name ?></h4>
                            </div>
                        </div>
                    </li>
                    <li role="separator" class="divider"></li>

                    <li><a href="{{route('edit-profile',['id'=>Auth::user()->id])}}"><i class="ti-user"></i> {{__('app.my_profile')}}</a></li>
                    <li><a href="#"><i class="ti-email"></i> {{__('app.inbox')}}</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{route('change-password')}}"><i class="ti-lock"></i> {{__('app.change_password')}}</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{route('logout')}}"><i class="fa fa-power-off"></i> {{__('app.logout')}}</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>