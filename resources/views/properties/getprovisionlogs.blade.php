<h3 style="margin-top: 40px;">Verlauf</h3>
<table class="forecast-table" >
    <thead>
      <tr>
        <th class="border bg-gray">Name</th>
        <th class="border bg-gray">Mieter</th>
        <th class="border bg-gray">Button</th>
        <th class="border bg-gray">Kommentar</th>
        <th class="border bg-gray">Datum</th>
        <th class="border bg-gray">Aktion</th>
      </tr>
    </thead>
    <tbody>
      @foreach($d as $k=>$list)

      <?php
      $tname = "";
      $u = DB::table('users')->where('id', $list->user_id)->first();
      if($u)
        $tname = $u->name;
      
      if($list->slug=="pbtn1_request")
        $rbutton_title = "1) Zur Freigabe an Janine senden";
      else if($list->slug=="pbtn2_request")
        $rbutton_title = "2) Zur Freigabe an Falk senden";
      else if($list->slug=="provosion_mark_as_not_release")
        $rbutton_title = "Ablehnen";
      else{
        $number = str_replace('pbtn', '', $list->slug);
        $rbutton_title = $number.") Freigeben";
      }


      ?>
      <tr>
        <td class="border">{{$tname}}</td>
        <td class="border">{{@$tenant_arr[$list->provision_id]}}</td>
        <td class="border">{{$rbutton_title}}</td>
        <td class="border">{{$list->value}}</td>
        <td class="border">{{date('d.m.Y H:i:s',strtotime($list['created_at']))}}</td>
        <td class="border">
          @if($user->email == config('users.falk_email') && $list->user_id == $user->id)
            <button type="button" data-id="{{ $list->id }}" data-tbl="empfehlung_details" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>