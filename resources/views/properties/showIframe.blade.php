<!DOCTYPE html>
<html lang="en" style="overflow-y: hidden;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/plugins/images/favicon.png') }}">
    <title>{{__('app.title')}}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Menu CSS -->
    <link href="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css') }}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{ asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <!-- morris CSS -->
    <link href="{{ asset('assets/plugins/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{ asset('assets/plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="{{ asset('assets/plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet" />
	<!--jquery ui datepicker-->
	<link href="{{ asset('assets/plugins/bower_components/x-editable/dist/jquery-editable/jquery-ui-datepicker/css/redmond/jquery-ui-1.10.3.custom.min.css') }}" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('assets/css/colors/default.css') }}" id="theme" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2.css" rel="stylesheet" />
    <link href="/css/custom-style.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <style type="text/css">
        .remove-sheet,
    	.remove-bank{
    		color: red;
    		font-weight: bold;
    		cursor: pointer;
    	}
        .remove-sheet{
            margin-left: 10px;
        }
        
        .cst > li.active > a:focus,
        .cst > li.active > a:hover,
        .cst > li.active > a{
    		border-color: #cdcdcd #cdcdcd transparent;
    		border-width: 2px;
    		font-size: 18px;
    	}
    	.cst>li.green-bg  > a:focus,
        .cst>li.green-bg  > a:hover,
        .cst>li.green-bg  > a{
    		background : #9ACA27 !important;
    		/*color: white !important;*/
    		color: black;
    		font-weight: bold;
    	}
    </style>
    <style type="text/css">
    .select2-container {
    	width: 100px;
    }
    .select2-container .select2-choice{
    	display: block;
	    height: 26px;
	    padding: 0 0 0 8px;
	    overflow: hidden;
	    position: relative;
	    border: 1px solid #aaa !important;
	    white-space: nowrap;
	    line-height: 26px;
	    color: #444;
	    text-decoration: none;
	    border-radius: 4px;
	    background-clip: padding-box;
	    -webkit-touch-callout: none;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    background-color: #fff;
	    background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0, #eee), color-stop(0.5, #fff));
	    background-image: -webkit-linear-gradient(center bottom, #eee 0%, #fff 50%);
    }

    .select2-dropdown-open .select2-choice {
	    border-bottom-color: transparent;
	    -webkit-box-shadow: 0 1px 0 #fff inset;
	    box-shadow: 0 1px 0 #fff inset;
	    border-bottom-left-radius: 0;
	    border-bottom-right-radius: 0;
	    background-color: #eee !important;
	    background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0, #fff), color-stop(0.5, #eee));
	    background-image: -webkit-linear-gradient(center bottom, #fff 0%, #eee 50%);
	    background-image: -moz-linear-gradient(center bottom, #fff 0%, #eee 50%);
	    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#eeeeee', endColorstr='#ffffff', GradientType=0);
	    background-image: linear-gradient(to top, #fff 0%, #eee 50%);
	}
	.select2-drop{
		min-width: 180px !important;
		margin-top: 5px;
	}

	#LoadingImage {background: rgba(255,255,255,0.5); width: 100%; height: 100%; position: fixed; z-index: 99999999;}
    #LoadingImage img { left: 50%; margin-left: -16px; top: 10%; margin-top: -16px; position: absolute;}
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!--<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">-->

    <![endif]-->
</head>
<body style="height: 100%">

<div id="LoadingImage" style="display: none;">
    <img src="{{ asset('message-loader.gif') }}" />
</div>

<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->


    <?php
    $bank_array = array();
    ?>
	<input type="hidden" id="path-properties-show" value="{{route('properties.showIframe', $id)}}">
		 <ul class="nav nav-tabs">
		@foreach($banks as $key => $bank)
        <?php
        $bank_array[] = $bank->id;
        ?>
        @if($key==0)
		@if(isset($_GET['bank_id']))
			<li class="{{($bank->id == $_GET['bank_id'])?'in active':''}}">
                <a data-toggle="tab" href="#bank-{{$bank->id}}">{{$bank->name}}</a>
            </li>
		@else
			<li class="{{($key == 0)?'in active':''}}">



                <a data-toggle="tab" href="#bank-{{$bank->id}}"style="cursor: pointer;"  onclick="">Kalkulation
                	<!-- <span data-id="{{$bank->id}}" onclick="deletebank({{$bank->id}})" class="remove-bank">X</span> -->
                </a>

            </li>

		@endif
        @endif





			@endforeach
			<li class=" ">
				<div style="float: left;" class="hidden">
				<form action="{{route("bank-direct-create")}}" method="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="property_id" value="{{$id}}">
					<button type="submit" class="btn btn-success btn-sm" style="margin-bottom: -20px">{{__('property.create_bank')}}</button>
				</form>
				</div>

			</li>

		</ul>


		<div class="tab-content" style="height: 10000px">

		@foreach ($banks as $key => $bank)
            <?php

            if($key)
                continue;

            array_unique($bank_array);

            $str = implode(',', $bank_array);

            // echo $str;

			$properties_banks = DB::table('properties')->whereRaw('main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('lock_status','desc')->orderBy('standard_property_status','desc')->get();

			// print_r($properties_banks); die;

			if(!$properties_banks)
				continue;



		   ?>

			<div id="bank-{{$bank->id}}" class="bank-container tab-pane fade {{($key == 0)?'in active':''}} " data-bank-id="{{$bank->id}}">
			@if($bank->name !='')
            <!-- <h3 class="bank-name-editable" >Finanzierung:  -->

            <!-- <a href="#" class="bank-remote" data-type="select2" data-pk="bank_id" data-url="{{url('property/update/bank/'.$id.'/'.$bank->id) }}" data-title="Banken">{{$bank->name}}</a> -->

            <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#add-popup-bank" class="btn btn-primary btn-sm add_bank">Bank hinzufügen</a> -->

            <!-- </h3> -->
			@endif
		<?php
        $bbbbbb = 0;
        ?>
		 <ul class="nav nav-tabs cst">
		 	@foreach ($properties_banks as $key1 => $bank1)
		 	@if($bank1->Ist)

            <?php
            if($bbbbbb==0)
                $bbbbbb = $bank1->id;

            $clll = "";
            if($bank1->standard_property_status)
            	$clll = 'green-bg';



            ?>

			<li id="sheet-nav-{{$bank1->id}}" style="{{$clll}}" class="{{$clll}} @if($key1==0) active @endif">
                <a data-toggle="tab" style=""  href="#bank-Ist-{{$bank1->id}}" ><?php echo "Ist";
                 if($bank1->sheet_title) echo '-'.$bank1->sheet_title?>


                </a>
            </li>
            @else
            <?php
            if(!$bank1->sheet_title)
                $bank1->sheet_title = "Title";
            ?>
			<li id="sheet-nav-{{$bank1->id}}" class="@if($key1==0) active @endif">
                <a data-toggle="tab"  href="#bank-Soll-{{$bank1->id}}" ><?php if($bank1->sheet_title) echo $bank1->sheet_title?>

                </a>
            </li>
            @endif
            @endforeach
            <!-- <a class="btn btn-success create-new-soll"  href="{{route('create_soll',array($properties->id,$bbbbbb))}}" >+ Add New</a> -->
         </ul>
         <div class="tab-content">
			 	@foreach ($properties_banks as $key1 => $bank1)
		 		 @if($bank1->Ist)
		 		<div id="bank-Ist-{{$bank1->id}}" class="tab-pane bank-tab @if($key1==0) fade active in @endif"  >
			      <iframe src="{{ url('banksIframe')}}/{{ $id }}/{{ $bank1->id }}/Ist" id="bank-Ist-iframe{{$bank1->id}}" width="100%"  style="border:none; min-height: 10000px;"></iframe>
				</div>
				@else

				<div id="bank-Soll-{{$bank1->id}}" class="tab-pane bank-tab @if($key1==0) fade active in @endif" >
				  <iframe src="{{ url('banksIframe')}}/{{ $id }}/{{ $bank1->id }}/soll" id="bank-soll-iframe{{$bank1->id}}" width="100%"  style="border:none; min-height: 10000px;"></iframe>
				</div>
				@endif
				@endforeach
		</div>

		</div>
		@endforeach

	</div>

	<div class=" modal fade" role="dialog" id="add-popup-bank">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add Banken</h4>
	      </div>
	      <div class="modal-body">
	      		<form id="add-new-bank-form">
	      			<input type="hidden" name="_token" value="{{csrf_token()}}">
	      			<label>Name</label>
			        <input type="text" name="name" class="form-control">
			        <br>
			        <label>Ansprechpartner</label>
			        <input type="text" name="contact_name" class="form-control">
			        <br>
			        <label>Telefonnummer</label>
			        <input type="text" name="contact_phone" class="form-control">
			        <br>
			        <label>Contact Email</label>
			        <input type="text" name="contact_email" class="form-control">
			        <br>
			        <label>Adresse</label>
			        <input type="text" name="address" class="form-control">
			        <br>
			        <label>Notizen</label>
			        <input type="text" name="notes" class="form-control">

	      		</form>

	      </div>
	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary save-new-bank" >Speichern</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
	      </div>
	    </div>

	  </div>
	</div>


	<script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--slimscroll JavaScript -->
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset('assets/js/waves.js') }}"></script>
<!--Counter js -->
<script src="{{ asset('assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
<!-- chartist chart -->
<script src="{{ asset('assets/plugins/bower_components/chartist-js/dist/chartist.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
<!-- Sparkline chart JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{ asset('assets/js/custom.min.js') }}"></script>
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="{{ asset('assets/js/dashboard1.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>
<!--Style Switcher -->
<script src="{{ asset('assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
<!--jquery ui datepicker-->
<script src="{{ asset('assets/plugins/bower_components/x-editable/dist/jquery-editable/jquery-ui-datepicker/js/jquery-ui-1.10.3.custom.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2.js"></script>

<!-- Calendar JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/calendar/dist/fullcalendar.js') }}"></script>

<script src="{{ asset('assets/plugins/bower_components/calendar/dist/jquery.fullcalendar.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/calendar/dist/cal-init.js') }}"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script type="text/javascript">

$.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    }
});
		function deletebank(id)
		{
			if ( confirm("Are you sure want to delete this?")){
				pid = $('#select_property_id').val();
				$this = $(this);
		        $.ajax({
		            url : '{{ route('removebank') }}',
		            data : { bank_id : id,property_id:pid,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                location.reload();
		            }
		        });
		    }
		}



        jQuery(document).ready(function($){

















        	$('.save-new-bank').on('click', function () {
                $.ajax({
                    url: "{{route('add_new_bank')}}",
                    type: "post",
                    data : $('#add-new-bank-form').serialize(),
                    success: function (response) {
                    	alert(response.message);
                    	if(response.status==1){
                    		$('#add-popup-bank').modal('hide');
                    		$('#add-new-bank-form')[0].reset();
                    	}

                    }
                });

            });


            // jQuery('.preview_bank').on('click', function(){
            //
            //     var url = $(this).attr('data-href');
            //     window.location = url;
            // });

			// jQuery('.all_green_btn').on('click', function () {
			// 	jQuery('td button').each(function () {
			// 		jQuery(this).removeClass('btn-primary');
			// 		jQuery(this).addClass('btn btn-success btn-outline btn-circle btn-lg m-r-5');
			// 		jQuery(this).html('<i class="mdi mdi-check"></i>');
			// 	});
			// });

        	$.fn.editable.defaults.params = function (params) {
		        params._token = $("meta[name=token]").attr("content");
		        return params;
		    };

            $.fn.editable.defaults.ajaxOptions = {type: "POST"};
			 $('.bank-name-editable').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        var path = $('#path-properties-show').val();
						var bank_id = $('.bank-container.tab-pane.fade.in.active').data("bank-id");
                        location.reload();
					}
                }
            });

			 $('.bank-remote').editable({
			        select2: {
			            placeholder: 'Banken',
			            allowClear: true,
			            minimumInputLength: 3,
			            id: function (item) {
			                return item.id;
			            },
			            ajax: {
			                url: '{{route("searchbank")}}',
			                dataType: 'json',
			                data: function (term, page) {
			                    return { query: term };
			                },
			                results: function (data, page) {
			                    return { results: data };
			                }
			            },
			            formatResult: function (item) {
			                return item.name;
			            },
			            formatSelection: function (item) {
			                return item.name;
			            },

			        },
			        success: function(response, newValue) {
			                location.reload();
			        }
			    });







			// $('.bank-name-editable').find('a.inline-edit').editable({
			// 	success: function(response, newValue) {
			// 		if( response.success === false )
			// 			return response.msg;
			// 		else{
			// 			var path = $('#path-properties-show').val();
			// 			var bank_id = $('.bank-container.tab-pane.fade.in.active').data("bank-id");
			// 			location.reload();
			// 		}
			// 	}
			// });


		});
	</script>
