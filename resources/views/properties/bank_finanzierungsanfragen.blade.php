<div class="col-sm-12 table-responsive">
    <table class="table table-striped" id="list-banks">
        <thead>
        <tr>
            <th>Bank</th>
            <th>Ort</th>
            <th>Ansprechpartner</th>
            <!--<th>Telefon</th>
            <th>Email</th>-->
            <th>Telefonnotiz</th>
            @if($type != 'Anfragen')
            <th>Zinsatz</th>
            <th>Tilgung</th>
            <th>FK-Anteil proz.</th>
            <th>FK-Anteil nominal</th>
            @endif
            @if($type == 'Angebote')
            <th></th>
            @endif
            <th>Datum</th>
            <th>Uhrzeit</th>
        </tr>
        </thead>
        <tbody>
        @if($type == 'Anfragen')
            @foreach($data as $list)
                <tr>
                    <td>@if(isset($list->getBank($list->email_to_send)->name))
                            {{$list->getBank($list->email_to_send)->name}}
                        @endif</td>
                    <td>{{$list->address}}</td>
                    <td>{{$list->contact_person}}</td>
                    <td>{{ $list->notizen }}</td>
                 
                    <td>{{show_datetime_format($list->created_at,'d.m.Y')}}</td>
                    <td>{{show_datetime_format($list->created_at,'H:i')}}</td>
                </tr>
            @endforeach
        @elseif($type == 'Angebote')
            @foreach($data as $banksFinancingOffer)
                <tr>

                    <td>{{$banksFinancingOffer->bank->name}}</td>
                    <td>{{$banksFinancingOffer->bank->address}}</td>
                    <td>{{$banksFinancingOffer->bank->contact_name}}</td>
                <!--<td>{{$banksFinancingOffer->bank->contact_phone}}</td>
								<td>{{$banksFinancingOffer->bank->contact_email}}</td>-->
                    <td>{{$banksFinancingOffer->telefonnotiz}}</td>
                    <td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="interest_rate" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->interest_rate,2) }}</a>
                    </td>
                    <td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="tilgung" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->tilgung,2) }}</a>
                    </td>
                    <td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_percentage" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->fk_share_percentage,2) }}</a>
                    </td>
                    <td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_nominal" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->fk_share_nominal,2) }}</a>
                    </td>

                <!-- <td>{{$banksFinancingOffer->tilgung}}</td> -->
                <!-- <td>{{$banksFinancingOffer->fk_share_percentage}}</td> -->
                <!-- <td>{{$banksFinancingOffer->fk_share_nominal}}</td> -->
                    <td></td>
                    <td>{{show_datetime_format($banksFinancingOffer->created_at,'d.m.Y')}}</td>
                    <td>{{show_datetime_format($banksFinancingOffer->created_at,'H:i')}}</td>

                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>