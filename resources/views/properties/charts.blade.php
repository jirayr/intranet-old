@extends('layouts.admin') 

@section('css')
    <!-- Styles -->
    <link href="{{ asset('css/property-comparison.css') }}" rel="stylesheet">
@endsection

@section('content')
	<div class="white-box">
		<h3>{{ __('property.multiple_properties') }}</h3>
		
		<label for="multiple-select">{{ __('property.choose_property') }}</label>
        <select id="multiple-select" class="form-control select2" multiple="multiple">
            @foreach ( $properties as $property )
                <option value="{{$property->id}}">{{$property->name_of_property}}</option>
            @endforeach
        </select>
		
		<label for="multiple-attribute"> {{ __('property.choose_attribute') }}</label>
		<select id="multiple-attribute" class="form-control select2" multiple="multiple">
            <option value="net_rent">{{ __('property.field.net_rent') }}</option>
            <option value="net_rent_empty">{{ __('property.field.net_rent_empty') }}</option>
            <option value="maintenance">{{ __('property.field.maintenance') }}</option>
            <option value="operating_costs">{{ __('property.field.operating_costs') }}</option>
            <option value="object_management">{{ __('property.field.object_management') }}</option>
            <option value="tax">{{ __('property.field.tax') }}</option>
            <option value="plot_of_land_m2">{{ __('property.field.plot_of_land_m2') }}</option>
            <option value="plot_of_land">{{ __('property.field.plot_of_land') }}</option>
            <option value="construction_year">{{ __('property.field.construction_year') }}</option>
            <option value="land_value">{{ __('property.field.land_value') }}</option>
            <option value="building">{{ __('property.field.building') }}</option>
            <option value="plot">{{ __('property.field.plot') }}</option>
            <option value="real_estate_taxes">{{ __('property.field.real_estate_taxes') }}</option>
            <option value="estate_agents">{{ __('property.field.estate_agents') }}</option>
            <option value="notary_land_register">{{ __('property.field.notary_land_register') }}</option>
            <option value="evaluation">{{ __('property.field.evaluation') }}</option>
            <option value="others">{{ __('property.field.others') }}</option>
            <option value="buffer">{{ __('property.field.buffer') }}</option>
            <option value="rent">{{ __('property.field.rent') }}</option>
            <option value="rent_whg">{{ __('property.field.rent_whg') }}</option>
            <option value="vacancy">{{ __('property.field.vacancy') }}</option>
            <option value="vacancy_whg">{{ __('property.field.vacancy_whg') }}</option>
            <option value="wault">{{ __('property.field.wault') }}</option>
            <option value="anchor_tenant">{{ __('property.field.anchor_tenant') }}</option>
            <option value="with_real_ek">{{ __('property.field.with_real_ek') }}</option>
            <option value="from_bond">{{ __('property.field.from_bond') }}</option>
            <option value="bank_loan">{{ __('property.field.bank_loan') }}</option>
            <option value="interest_bank_loan">{{ __('property.field.interest_bank_loan') }}</option>
            <option value="eradication_bank">{{ __('property.field.eradication_bank') }}</option>
            <option value="interest_bond">{{ __('property.field.interest_bond') }}</option>
            <option value="maintenance_nk">{{ __('property.field.maintenance_nk') }}</option>
            <option value="operating_costs_nk">{{ __('property.field.operating_costs_nk') }}</option>
            <option value="object_management_nk">{{ __('property.field.object_management_nk') }}</option>
            <option value="depreciation_nk">{{ __('property.field.depreciation_nk') }}</option>
            <option value="property_value">{{ __('property.field.property_value') }}</option>
            <option value="duration_from">{{ __('property.field.duration_from') }}</option>
            <option value="nk_anchor_tenants">{{ __('property.field.nk_anchor_tenants') }}</option>
            <option value="nk_tenant1">{{ __('property.field.nk_tenant1') }}</option>
            <option value="nk_tenant2">{{ __('property.field.nk_tenant2') }}</option>
            <option value="nk_tenant3">{{ __('property.field.nk_tenant3') }}</option>
            <option value="nk_tenant4">{{ __('property.field.nk_tenant4') }}</option>
            <option value="nk_tenant5">{{ __('property.field.nk_tenant5') }}</option>
            <option value="mv_anchor_tenants">{{ __('property.field.mv_anchor_tenants') }}</option>
            <option value="mv_tenant1">{{ __('property.field.mv_tenant1') }}</option>
            <option value="mv_tenant2">{{ __('property.field.mv_tenant2') }}</option>
            <option value="mv_tenant3">{{ __('property.field.mv_tenant3') }}</option>
            <option value="mv_tenant4">{{ __('property.field.mv_tenant4') }}</option>
            <option value="mv_tenant5">{{ __('property.field.mv_tenant5') }}</option>
            <option value="buy_recommendation">{{ __('property.field.buy_recommendation') }}</option>
            <option value="status">{{ __('property.field.status') }}</option>
          <option value="total_purchase_price">{{ __('property.field.total_purchase_price') }}</option> //D38
			<option value="maintenance_nk_money">{{ __('property.field.maintenance_nk_money') }}</option> //D59
            <option value="total_commercial_sqm">{{ __('property.field.total_commercial_sqm') }}</option> //K38
			<option value="financing_structure_sum">{{ __('property.field.financing_structure_sum') }}</option> //C50
			<option value="building_eur">{{ __('property.field.building_eur') }}</option> //D36
			<option value="plot_of_land_eur">{{ __('property.field.plot_of_land_eur') }}</option> //D37
			<option value="net_buy_price">{{ __('property.field.net_buy_price') }}</option> //D40
			<option value="additional_cost">{{ __('property.field.additional_cost') }}</option> //D41
			<option value="total_nbpac">{{ __('property.field.total_nbpac') }}</option> //D42
			<option value="with_real_ek_money">{{ __('property.field.with_real_ek_money') }}</option> //D47
			<option value="from_bond_money">{{ __('property.field.from_bond_money') }}</option> //D48
			<option value="bank_loan_money">{{ __('property.field.bank_loan_money') }}</option> //D49
			<option value="net_rent_pa">{{ __('property.field.net_rent_pa') }}</option> //D56
			
			<option value="operating_costs_nk_money">{{ __('property.field.operating_costs_nk_money') }}</option> //D60
			<option value="object_management_nk_money">{{ __('property.field.object_management_nk_money') }}</option> //D61
			<option value="depreciation_nk_money">{{ __('property.field.depreciation_nk_money') }}</option> //D62
			<option value="net_rent_increase_year1">{{ __('property.field.net_rent_increase_year1') }}</option> //E7
			<option value="net_rent_increase_year2">{{ __('property.field.net_rent_increase_year2') }}</option> //F7
			<option value="net_rent_increase_year3">{{ __('property.field.net_rent_increase_year3') }}</option> //G7
			<option value="net_rent_increase_year4">{{ __('property.field.net_rent_increase_year4') }}</option> //H7
			<option value="net_rent_increase_year5">{{ __('property.field.net_rent_increase_year5') }}</option> //I7
			<option value="maintenance_increase_year1">{{ __('property.field.maintenance_increase_year1') }}</option> //E10
			<option value="maintenance_increase_year2">{{ __('property.field.maintenance_increase_year2') }}</option> //F10
			<option value="maintenance_increase_year3">{{ __('property.field.maintenance_increase_year3') }}</option> //G10
			<option value="maintenance_increase_year4">{{ __('property.field.maintenance_increase_year4') }}</option> //H10
			<option value="maintenance_increase_year5">{{ __('property.field.maintenance_increase_year5') }}</option> //I10
			<option value="operating_cost_increase_year1">{{ __('property.field.operating_cost_increase_year1') }}</option> //E11
			<option value="operating_cost_increase_year2">{{ __('property.field.operating_cost_increase_year2') }}</option> //F11
			<option value="operating_cost_increase_year3">{{ __('property.field.operating_cost_increase_year3') }}</option> //G11
			<option value="operating_cost_increase_year4">{{ __('property.field.operating_cost_increase_year4') }}</option> //H11
			<option value="operating_cost_increase_year5">{{ __('property.field.operating_cost_increase_year5') }}</option> //I11
			<option value="property_management_increase_year1">{{ __('property.field.property_management_increase_year1') }}</option> //E12
			<option value="property_management_increase_year2">{{ __('property.field.property_management_increase_year2') }}</option> //F12
			<option value="property_management_increase_year3">{{ __('property.field.property_management_increase_year3') }}</option> //G12	
			<option value="property_management_increase_year4">{{ __('property.field.property_management_increase_year4') }}</option> //H12
			<option value="property_management_increase_year5">{{ __('property.field.property_management_increase_year5') }}</option> //I12
			<option value="ebitda_year_1">{{ __('property.field.ebitda_year_1') }}</option> //E14
			<option value="ebitda_year_2">{{ __('property.field.ebitda_year_2') }}</option> //F14
			<option value="ebitda_year_3">{{ __('property.field.ebitda_year_3') }}</option> //G14
			<option value="ebitda_year_4">{{ __('property.field.ebitda_year_4') }}</option> //H14
			<option value="ebitda_year_5">{{ __('property.field.ebitda_year_5') }}</option> //I14
			<option value="depreciation_year_1">{{ __('property.field.depreciation_year_1') }}</option> //E16
			<option value="depreciation_year_2">{{ __('property.field.depreciation_year_2') }}</option> //F16
			<option value="depreciation_year_3">{{ __('property.field.depreciation_year_3') }}</option> //G16
			<option value="depreciation_year_4">{{ __('property.field.depreciation_year_4') }}</option> //H16
			<option value="depreciation_year_5">{{ __('property.field.depreciation_year_5') }}</option> //I16
			<option value="ebit_year_1">{{ __('property.field.ebit_year_1') }}</option> //E18
			<option value="ebit_year_2">{{ __('property.field.ebit_year_2') }}</option> //F18
			<option value="ebit_year_3">{{ __('property.field.ebit_year_3') }}</option> //G18
			<option value="ebit_year_4">{{ __('property.field.ebit_year_4') }}</option> //H18
			<option value="ebit_year_5">{{ __('property.field.ebit_year_5') }}</option> //I18
			<option value="Interest_bank_loan_year_1">{{ __('property.field.Interest_bank_loan_year_1') }}</option> //E20
			<option value="Interest_bank_loan_year_2">{{ __('property.field.Interest_bank_loan_year_2') }}</option> //F20
			<option value="Interest_bank_loan_year_3">{{ __('property.field.Interest_bank_loan_year_3') }}</option> //G20
			<option value="Interest_bank_loan_year_4">{{ __('property.field.Interest_bank_loan_year_4') }}</option> //H20
			<option value="Interest_bank_loan_year_5">{{ __('property.field.Interest_bank_loan_year_5') }}</option> //I20
			<option value="Interest_bond_year1">{{ __('property.field.Interest_bond_year1') }}</option> //E21
			<option value="Interest_bond_year2">{{ __('property.field.Interest_bond_year2') }}</option> //F21
			<option value="Interest_bond_year3">{{ __('property.field.Interest_bond_year3') }}</option> //G21
			<option value="Interest_bond_year4">{{ __('property.field.Interest_bond_year4') }}</option> //H21
			<option value="Interest_bond_year5">{{ __('property.field.Interest_bond_year5') }}</option> //I21
			<option value="EBT_year_1">{{ __('property.field.EBT_year_1') }}</option> //E23
			<option value="EBT_year_2">{{ __('property.field.EBT_year_2') }}</option> //F23
			<option value="EBT_year_3">{{ __('property.field.EBT_year_3') }}</option> //G23
			<option value="EBT_year_4">{{ __('property.field.EBT_year_4') }}</option> //H23
			<option value="EBT_year_5">{{ __('property.field.EBT_year_5') }}</option> //I23
			<option value="Tax_year_1">{{ __('property.field.Tax_year_1') }}</option> //E25
			<option value="Tax_year_2">{{ __('property.field.Tax_year_2') }}</option> //F25
			<option value="Tax_year_3">{{ __('property.field.Tax_year_3') }}</option> //G25
			<option value="Tax_year_4">{{ __('property.field.Tax_year_4') }}</option> //H25
			<option value="Tax_year_5">{{ __('property.field.Tax_year_5') }}</option> //I25
			<option value="EAT_year_1">{{ __('property.field.EAT_year_1') }}</option> //E27
			<option value="EAT_year_2">{{ __('property.field.EAT_year_2') }}</option> //F27
			<option value="EAT_year_3">{{ __('property.field.EAT_year_3') }}</option> //G27
			<option value="EAT_year_4">{{ __('property.field.EAT_year_4') }}</option> //H27
			<option value="EAT_year_5">{{ __('property.field.EAT_year_5') }}</option> //I27
			<option value="Eradication_bank_year1">{{ __('property.field.Eradication_bank_year1') }}</option> //E29
			<option value="Eradication_bank_year2">{{ __('property.field.Eradication_bank_year2') }}</option> //F29
			<option value="Eradication_bank_year3">{{ __('property.field.Eradication_bank_year3') }}</option> //G29
			<option value="Eradication_bank_year4">{{ __('property.field.Eradication_bank_year4') }}</option> //H29
			<option value="Eradication_bank_year5">{{ __('property.field.Eradication_bank_year5') }}</option> //I29
			<option value="Cash_flow_after_taxes_year_1">{{ __('property.field.Cash_flow_after_taxes_year_1') }}</option> //E31
			<option value="Cash_flow_after_taxes_year_2">{{ __('property.field.Cash_flow_after_taxes_year_2') }}</option> //F31
			<option value="Cash_flow_after_taxes_year_3">{{ __('property.field.Cash_flow_after_taxes_year_3') }}</option> //H31
			<option value="Cash_flow_after_taxes_year_4">{{ __('property.field.Cash_flow_after_taxes_year_4') }}</option> //G31
			<option value="Cash_flow_after_taxes_year_5">{{ __('property.field.Cash_flow_after_taxes_year_5') }}</option> //I31
			<option value="real_estate_taxes_in_EUR">{{ __('property.field.real_estate_taxes_in_EUR') }}</option> //H36
			<option value="estate_agents_in_EUR">{{ __('property.field.estate_agents_in_EUR') }}</option> //H37
			<option value="notary_land_register_in_EUR">{{ __('property.field.notary_land_register_in_EUR')}}</option> //H38
			<option value="Grundbuch_in_EUR">{{ __('property.field.Grundbuch_in_EUR') }}</option> //H38
			<option value="Evaluation_in_EUR">{{ __('property.field.Evaluation_in_EUR') }}</option> //H39
			<option value="Others_in_EUR">{{ __('property.field.Others_in_EUR') }}</option> //H40
			<option value="Buffer_in_EUR">{{ __('property.field.Buffer_in_EUR') }}</option> //H41
			<option value="value_end_of_year_1">{{ __('property.field.value_end_of_year_1') }}</option> //H47
			<option value="value_end_of_year_2">{{ __('property.field.value_end_of_year_2') }}</option> //I47
			<option value="value_end_of_year_3">{{ __('property.field.value_end_of_year_3') }}</option> //J47
			<option value="value_end_of_year_4">{{ __('property.field.value_end_of_year_4') }}</option> //K47
			<option value="value_end_of_year_5">{{ __('property.field.value_end_of_year_5') }}</option> //L47
			<option value="interest_bank_loan_end_of_year_1">{{ __('property.field.interest_bank_loan_end_of_year_1') }}</option> //H48
			<option value="interest_bank_loan_end_of_year_2">{{ __('property.field.interest_bank_loan_end_of_year_2') }}</option> //I48
			<option value="interest_bank_loan_end_of_year_3">{{ __('property.field.interest_bank_loan_end_of_year_3') }}</option> //J48
			<option value="interest_bank_loan_end_of_year_4">{{ __('property.field.interest_bank_loan_end_of_year_4') }}</option> //K48
			<option value="interest_bank_loan_end_of_year_5">{{ __('property.field.interest_bank_loan_end_of_year_5') }}</option> //L48
			<option value="eradication_bank_end_of_year_1">{{ __('property.field.eradication_bank_end_of_year_1') }}</option> //H49
			<option value="eradication_bank_end_of_year_2">{{ __('property.field.eradication_bank_end_of_year_2') }}</option> //I49
			<option value="eradication_bank_end_of_year_3">{{ __('property.field.eradication_bank_end_of_year_3') }}</option> //J49
			<option value="eradication_bank_end_of_year_4">{{ __('property.field.eradication_bank_end_of_year_4') }}</option> //K49
			<option value="eradication_bank_end_of_year_5">{{ __('property.field.eradication_bank_end_of_year_5') }}</option> //L49
			<option value="sum_end_year_1">{{ __('property.field.sum_end_year_1') }}</option> //H50
			<option value="sum_end_year_2">{{ __('property.field.sum_end_year_2') }}</option> //I50
			<option value="sum_end_year_3">{{ __('property.field.sum_end_year_3') }}</option> //J50
			<option value="sum_end_year_4">{{ __('property.field.sum_end_year_4') }}</option> //K50
			<option value="sum_end_year_5">{{ __('property.field.sum_end_year_5') }}</option> //L50
			<option value="interest_bond_from_bond">{{ __('property.field.interest_bond_from_bond') }}</option> //H52
			<option value="gross_yield">{{ __('property.field.gross_yield') }}</option> //G58
			<option value="Gross_desperate">{{ __('property.field.Gross_desperate') }}</option> //G59
			<option value="EK_Rendite_CF">{{ __('property.field.EK_Rendite_CF') }}</option> //G61
			<option value="EK_Rendite_GuV">{{ __('property.field.EK_Rendite_GuV') }}</option> //G62
			<option value="Increase_p_a_year1">{{ __('property.field.Increase_p_a_year1') }}</option> //F66
			<option value="Increase_p_a_year2">{{ __('property.field.Increase_p_a_year2') }}</option> //G66
			<option value="Increase_p_a_year3">{{ __('property.field.Increase_p_a_year3') }}</option> //H66
			<option value="Increase_p_a_year4">{{ __('property.field.Increase_p_a_year4') }}</option> //I66
			<option value="Increase_p_a_year5">{{ __('property.field.Increase_p_a_year5') }}</option> //J66
			<option value="end_of_year_1">{{ __('property.field.end_of_year_1') }}</option> //F67
			<option value="end_of_year_2">{{ __('property.field.end_of_year_2') }}</option> //G67
			<option value="end_of_year_3">{{ __('property.field.end_of_year_3') }}</option> //H67
			<option value="end_of_year_4">{{ __('property.field.end_of_year_4') }}</option> //I67
			<option value="end_of_year_5">{{ __('property.field.end_of_year_5') }}</option> //J67
			<option value="from_sales_proceeds">{{ __('property.field.from_sales_proceeds') }}</option> //J69
			<option value="from_revaluation">{{ __('property.field.from_revaluation') }}</option> //J70
			<option value="anchor_tenants">{{ __('property.field.anchor_tenants') }}</option> //L7
			<option value="Rental_area_in_m2">{{ __('property.field.Rental_area_in_m2') }}</option> //L10
			<option value="Rent_euro_m2">{{ __('property.field.Rent_euro_m2') }}</option> //L14
			<option value="KP_€_m2_p_NF">{{ __('property.field.KP_€_m2_p_NF') }}</option> //L16
			<option value="plots">{{ __('property.field.plots') }}</option> //L18
			<option value="occupancy_rate">{{ __('property.field.occupancy_rate') }}</option> //L20
			<option value="Factor_net">{{ __('property.field.Factor_net') }}</option> //K25
			<option value="WAULT__of_anchor_tenants">{{ __('property.field.WAULT__of_anchor_tenants') }}</option> //P40
			<option value="WAULT__of_tenant1">{{ __('property.field.WAULT__of_tenant1') }}</option> //P41
			<option value="WAULT__of_tenant2">{{ __('property.field.WAULT__of_tenant2') }}</option> //P42
			<option value="WAULT__of_tenant3">{{ __('property.field.WAULT__of_tenant3') }}</option> //P43
			<option value="WAULT__of_tenant4">{{ __('property.field.WAULT__of_tenant4') }}</option> //P44
			<option value="WAULT__of_tenant5">{{ __('property.field.WAULT__of_tenant5') }}</option> //P45
			<option value="Rental_income_of_anchor_tenants">{{ __('property.field.Rental_income_of_anchor_tenants') }}</option> //Q40
			<option value="Rental_income_of_tenant1">{{ __('property.field.Rental_income_of_tenant1') }}</option> //Q41
			<option value="Rental_income_of_tenant2">{{ __('property.field.Rental_income_of_tenant2') }}</option> //Q42
			<option value="Rental_income_of_tenant3">{{ __('property.field.Rental_income_of_tenant3') }}</option> //Q43
			<option value="Rental_income_of_tenant4">{{ __('property.field.Rental_income_of_tenant4') }}</option> //Q44
			<option value="Rental_income_of_tenant5">{{ __('property.field.Rental_income_of_tenant5') }}</option> //Q45
			<option value="Total_of_WAULT">{{ __('property.field.Total_of_WAULT') }}</option> //P46
			<option value="Total_of_Rental_income">{{ __('property.field.Total_of_Rental_income') }}</option> //Q46
			<option value="Total_Land_value">{{ __('property.field.Total_Land_value') }}</option> //Q47
			<option value="Value_totally">{{ __('property.field.Value_totally') }}</option> //Q48
			<option value="non_recoverable_ancillary_costs_and_maintenance_via_the_WAULT">{{ __('property.field.non_recoverable_ancillary_costs_and_maintenance_via_the_WAULT') }}</option> //Q49
			<option value="risk_value">{{ __('property.field.risk_value') }}</option> //Q50
			<option value="risk_value_percent">{{ __('property.field.risk_value_percent') }}</option> //Q51
			<option value="WHG_qm_of_WAULT">{{ __('property.field.WHG_qm_of_WAULT') }}</option> //L40
			<option value="WHG_qm_of_anchor_tenant">{{ __('property.field.WHG_qm_of_anchor_tenant') }}</option> //L41
			<option value="WHG_qm_of_plot">{{ __('property.field.WHG_qm_of_plot') }}</option> //L42

          
        </select>
		<div id="multiple-properties"></div>
	</div>
@endsection

@section('js')
    <script type="text/javascript">
        var data = <?php echo json_encode($properties); ?>;
        var data_build = {};
        for (var i in data) {
            data_build[data[i].id] = data[i];
        }
        var single_chart;
        var multiple_chart;
        document.addEventListener("DOMContentLoaded", function(event) { 
         

            // Multiple properties
            multiple_chart = Morris.Line({
                element: 'multiple-properties',
                data: null,
                xkey: ['duration_from'],
                ykeys: ['purchase_price', 'total_purchase_price','rent'],
                labels: ['<?php echo __('property.purchase_price') ?>', '<?php echo __('property.purchase_price_total') ?>', '<?php echo __('property.rental_fee') ?>'],
                parseTime        : false
            });
        });
        $(document).ready(function(){
            $('#multiple-select').on('change', function(){
                var property_ids = $(this).val();
                property_ids = property_ids == null ? [] : property_ids;
                var properties = [];
                if( property_ids.length > 0 ) {
                    $.each(property_ids, function(i, v){
                        if( typeof data_build[v] != 'undefined' ) {
                            properties.push({
                                duration_from: data_build[v].duration_from,
                                purchase_price: data_build[v].total_purchase_price,
                                total_purchase_price: data_build[v].total_nbpac,
                                rent: data_build[v].rent
                            });
                        }
                    });

                    if( properties.length > 0 ) {
                        multiple_chart.setData(properties);

                    } else {
                        multiple_chart.setData(null);
                    }
                } else {
                    multiple_chart.setData(null);
                }
            });
			$('#multiple-attribute').on('change', function(){
                var property_ids = $('#multiple-select').val();
                var attribute = $(this).val();
                property_ids = property_ids == null ? [] : property_ids;
				attribute =  attribute == null ? ['total_purchase_price', 'total_nbpac','rent'] : attribute;
				labels =  ['<?php echo __('property.purchase_price') ?>', '<?php echo __('property.purchase_price_total') ?>', '<?php echo __('property.rental_fee') ?>'];
                var properties = [];

				multiple_chart.options.ykeys = attribute ;
				if($(this).val() != null){
				multiple_chart.options.labels = attribute ;
				}else{
					multiple_chart.options.labels = labels;
					
				}
                if( property_ids.length > 0 ) {
                    $.each(property_ids, function(i, v){
						var property =[];
						property['duration_from'] = data_build[v].duration_from;
						$.each(attribute, function(i2, v2){
							if( typeof data_build[v] != 'undefined' ) {
								property[v2] = data_build[v][v2];							
							}
						});
						properties.push(property);
                    });

                    if( properties.length > 0 ) {
                        multiple_chart.setData(properties);

                    } else {
                        multiple_chart.setData(null);
                    }
                } else {
                    multiple_chart.setData(null);
                }
            });
        });
    </script>
@endsection