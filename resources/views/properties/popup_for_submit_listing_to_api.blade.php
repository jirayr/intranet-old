<!-- Modal -->
<div class="myImmoModal modal fade" role="dialog">
    <div class="modal-dialog">
        <form id="immoscout_form" method="post" action="upload_listing_to_api">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Immoscout24 Upload</h4>
            </div>
            <div class="modal-body">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <label>Title</label>
                    <input type="text" name="title" class="form-control" placeholder="Title" required>

                    <h4>Address</h4>

                    <label>{{-- Eckdaten --}} postcode</label>
                     <input type="number" name="postcode" placeholder="typr here" class="form-control" required>
                     <label>City</label>
                     <input type="text" name="city" placeholder="typr here" class="form-control" required>
                    <label>Street</label>
                     <input type="text" name="street" placeholder="typr here" class="form-control" required>
                    <label>House Number</label>
                     <input type="text" name="houseNumber" placeholder="typr here" class="form-control" required>


                    <label>Show Address</label>
                    <select name="showAddress" class="form-control" required>
                        <option value="volvo">---Select---</option>
                        <option value="true">Yes</option>
                        <option value="false">No</option>
                    </select>

                     <label>Objektart:</label>
                    <select name="commercializationType" class="form-control" required>
                        <option value="BUY">BUY</option>
                        <option value="RENT">RENT</option>
                        <option value="LEASE">LEASE</option>
                        <option value="LEASEHOLD">LEASEHOLD</option>
                        <option value="COMPULSORY_AUCTION">COMPULSORY AUCTION</option>
                        <option value="RENT_AND_BUY">RENT AND BUY</option>

                    </select><br>

                    <label>Utilization TradeSite</label>
                    <input type="text" required name="utilizationTradeSite" placeholder="type here" class="form-control">

                    <h4>Price</h4>

                    <label>Value</label>
                    <input type="text" name="value" placeholder="type here" class="form-control" required>

                    <label>Currency</label>
                    <select name="currency" class="form-control" required>
                        <option value="">---Select---</option>
                        <option value="EUR">EUR</option>
                        <option value="DOLLAR">DOLLAR</option>
                    </select>

                    <label>Marketing Type</label>
                    <input type="text" name="marketingType" placeholder="type here" class="form-control" required>

                    <label>Plot Area</label>
                    <input type="text" name="plotArea" placeholder="type here" class="form-control" required>

                    <h4>Courtage</h4>

                    <label>Has Courtage</label>
                    <select name="hasCourtage" class="form-control" required>
                        <option value="volvo">---Select---</option>
                        <option value="YES">Yes</option>
                        <option value="NO">no</option>
                    </select>

                    <label>Courtage</label>
                    <input type="text" name="courtage" placeholder="type here" class="form-control">

        


            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" >Upload</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>

            </form>
        </div>

    </div>
</div>