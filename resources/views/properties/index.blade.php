@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">{{__('property.manage_property')}}</div>

                <form action="{{route('properties.index')}}" method="get">
                    <div class="col-sm-4" style="    margin-left: 10px;">
                    <input type="text" name="q" class="form-control" value="{{@$_REQUEST['q']}}">
                    </div>
                    <div class="col-sm-4">
                    <button type="submit" class="btn btn-success">Search</button>
                    </div>
                </form>

                <div class="clearfix"></div>

 
                <div class="table-responsive">
                    <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th style="width: 70px;" class="text-center">#</th>
                            <th>{{__('property.name_of_property')}}</th>
                            <th>{{__('property.listing.creator_username')}}</th>
                            <th>{{__('property.listing.net_rent')}}</th>
                            <th>{{__('property.listing.net_rent_empty')}}</th>
                            <th>{{__('property.listing.maintenance')}}</th>
                            <th>{{__('property.listing.tax')}}</th>
                            <th>{{__('property.listing.object_management')}}</th>
                            
                            <th>{{__('property.listing.status')}}</th>
                            <th>{{__('property.listing.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($properties as $property)

                        <?php
                        $name_of_property = $property->name_of_property;
                        if(false && $property->bank_ids ){
                            $bank_ids = json_decode($property->bank_ids,true);
                            if(isset($bank_ids[0])){
                                $response  =  DB::table('properties')->where('properties.Ist', $bank_ids[0])->where('main_property_id',$property->id)->first();
                                if($response)
                                    $name_of_property = $response->name_of_property;

                            }
                        }

                        ?>
                        <tr>
                                <td class="text-center"><br>{{$property->main_property_id}}</td>
                                <td><br>{{$name_of_property}}</td>
                                <td><br>{{$property->name_of_creator}}</td>
                                <td class="number-right"><br>{{number_format($property->net_rent,2,",",".")}}</td>
                                <td class="number-right"><br>{{number_format($property->net_rent_empty,2,",",".")}}</td>
                                <td class="number-right"><br>{{number_format($property->maintenance,2,",",".")}}</td>
                                <td class="number-right"><br>{{number_format($property->tax,2,",",".")}}</td>
                                <td class="number-right"><br>{{$property->object_management}}</td>
                                <td>
				    <select class="form-control property-status" data-property-id="{{$property->main_property_id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
                                    <option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
                                        <option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>

                                        <option value="{{config('properties.status.exclusive')}}" @if($property->status == config('properties.status.exclusive')) {!!"selected" !!} @endif>{{__('property.exclusive')}}</option>

                                        <option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>

                                        <option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>

                                        <option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>

                                        <option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>




                                        <option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>

                                        <option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>

                                        <option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>


                                        <option value="{{config('properties.status.quicksheet')}}" @if($property->status == config('properties.status.quicksheet')) {!!"selected" !!} @endif>{{__('property.quicksheet')}}</option>
					
				        <option value="{{config('properties.status.liquiplanung')}}" @if($property->status == config('properties.status.liquiplanung')) {!!"selected" !!} @endif>{{__('property.liquiplanung')}}</option>


                                    </select>
                                </td>
                                <td>

                                    {!! Form::open(['action' => ['PropertiesController@destroy', $property->main_property_id],
                                    'method' => 'DELETE']) !!}

                                    @php
                                        $show_url = (Auth::user()->role == 4) ? route('properties.show',['property'=>$property->main_property_id, 'tab' => 'tenancy-schedule']) : route('properties.show',['property'=>$property->main_property_id]);
                                    @endphp

                                    <button onclick="location.href='{{ $show_url }}'"
                                            type="button"
                                            class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                                class="ti-eye"></i></button>
                                    <button onclick="location.href='{{ $show_url }}'"
                                            type="button"
                                            class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                                class="ti-pencil-alt"></i></button>
                                    <button type="submit" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"
                                            onclick="return confirm('{{__("property.confirm")}}')">
                                        <i class="icon-trash"></i></button>

                                    {!! Form::close() !!}

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper" style="margin-left: 20px">
                        <div class="row pull-left">
                                {{$properties->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="Modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Choose a Bank</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label>{{__('property.select_a_bank')}}: </label>
		<select id="bank">
		@foreach ($banks as $bank)
			<option value="{{$bank->id}}" data-with_real_ek="{{$bank->with_real_ek}}" data-from_bond="{{$bank->from_bond}}"
			data-bank_loan="{{$bank->bank_loan}}" data-interest_bank_loan="{{$bank->interest_bank_loan}}" 
			data-eradication_bank="{{$bank->eradication_bank}}" data-interest_bond="{{$bank->interest_bond}}"
			>{{$bank->name}}</option>
		@endforeach
		</select>
      </div>
      <div class="modal-footer">
        <button id="modal_save_btn" type="button" class="btn btn-primary">{{__('property.save_changes')}}</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="property-status-confirm-password-modal" role="dialog" data-keyboard="false" data-backdrop="static"style="z-index: 9999">
     <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title">Bitte trage das Passwort ein</h4>
             </div>

             <form id="property-status-confirm-password-form" action="{{ route('confirm_password') }}" autocomplete="off">
                 <div class="modal-body">
                     <div class="row">
                         <div class="col-md-12">
                             <span id="property-status-confirm-password-error"></span>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-md-12">
                             <label>Passwort eintragen</label>
                             <input type="password" name="password" class="form-control" autocomplete="off" required>
                             <input type="hidden" name="check_login_password" value="1">
                         </div>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button type="submit" class="btn btn-primary">Bestätigen</button>
                     <button type="button" class="btn btn-default cancel-confirm-password" data-dismiss="modal">Abbrechen</button>
                 </div>
             </form>

         </div>
     </div>
 </div>

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var property_status;
            var old_val = $('.property-status').val();

            $('body').on('change', '.property-status', function(){
                property_status = $(this);
                if(old_val == '6'){
                    $('#property-status-confirm-password-modal').modal('show');
                    $('#property-status-confirm-password-modal').find('input[name="password"]').val('').focus();
                }else{
                    changePropertyStatus(property_status);
                }
            });

            $('body').on('click', '.cancel-confirm-password', function(){
                $('.property-status').val(old_val);
            });

            $('#property-status-confirm-password-form').on('submit', (function(e) {
                e.preventDefault();

                var $this = $(this);
                var formData = new FormData($(this)[0]);
                var url = $(this).attr('action');

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    cache: false,
                    beforeSend: function() {
                        $($this).find('button[type="submit"]').prop('disabled', true);
                    },
                    success: function(result) {
                        $($this).find('button[type="submit"]').prop('disabled', false);
                        if (result.status) {
                            $('#property-status-confirm-password-modal').modal('hide');
                            changePropertyStatus(property_status);
                        }else{
                          showFlash($('#property-status-confirm-password-error'), result.message, 'error');
                        }
                    },
                    error: function(error) {
                        $($this).find('button[type="submit"]').prop('disabled', false);
                        showFlash($('#property-status-confirm-password-error'), 'Somthing want wrong!', 'error');
                    }
                });
            }));

            var propertyId;
            function changePropertyStatus(obj){
                if ($(obj).val() == "1") {
                    $('#Modal').modal('show');
                }
                obj.prop("disabled", true);
                propertyId = $(obj).data('property-id');
                var status = $(obj).val();
                var notificationType = $(obj).data('notification-type');
                var data = {
                    _token: '<?php echo csrf_token() ?>',
                    status: status,
                    oldstatus: old_val,
                    property_id: propertyId
                };
                
                $.ajax({
                    type: 'POST',
                    url: '{{route('properties.change_status')}}',
                    data: data,
                    success: function(data) {
                        if (data.status == 0) {
                            alert(data.message);
                            obj.prop("disabled", false);
                            $('.property-status').val(data.oldstatus)
                        } else {
                            newNotification(propertyId, 3);
                            // location.reload();
                        }
                    }
                });
            }
		
			/*var propertyId;
            $('.property-status').on('change', function () {
				if($(this).val()=="1"){
					$('#Modal').modal('show');
				}
                selection = $('.property-status');
                selection.prop( "disabled", true );
                propertyId = $(this).data('property-id');
                var status = $(this).val();
                var notificationType = $(this).data('notification-type');
                
                var data = {
                    _token : '<?php echo csrf_token() ?>',
                    status : status,
                    property_id : propertyId
                };
                $.ajax({
                    type:'POST',
                    url:'{{route('properties.change_status')}}',
                    data: data,
                    success:function(data){
                        newNotification(propertyId, 3);
                    }
                });
            });*/
			$('#modal_save_btn').on('click', function () {
			    $.ajax({
					method: "POST",
					url: "{{url('/property/select_bank')}}" + "/" + propertyId,
					data: { 
						with_real_ek: $('#bank option:selected').data("with_real_ek"),
						from_bond: $('#bank option:selected').data("from_bond"),
						bank_loan: $('#bank option:selected').data("bank_loan"),
						interest_bank_loan: $('#bank option:selected').data("interest_bank_loan"),
						eradication_bank: $('#bank option:selected').data("eradication_bank"),
						interest_bond: $('#bank option:selected').data("interest_bond"),
						bank: $('#bank option:selected').val()
						}
					})
					.success(function( response, newValue) {
					   if( response.success === false )
                            return response.msg;
                        else
                            $('#Modal').modal('hide');
					   console.log(propertyId);
				    });
		        })
            });
        
		
    </script>
@endsection
