<div class="col-sm-12 table-responsive">
<table class="table table-striped" id="list-statusloi">
	<thead>
	<tr>
		<th>Transaction Manager</th>
        <th>Amount</th>
        <th>Last sent</th>
        <th>LOI PDF</th>
	</tr>
	</thead>
	<tbody>
		<?php $s = 0; ?>
		@foreach($data as $lists)
			@foreach($lists as $list)
		<tr>
			<td><a href="javascript:void(0)" onclick="loadAllStatusLoiByUser({{$list->user_id}})">{{$list->name}}</a></td>
			<td>{{$lists->count()}}</td>
			<td>{{$list->date}}</td>
			<td>
				<?php
	                $pdf_arr = [];
	                if($list->loi_pdf){
	                    $pdf_arr = explode(",", $list->loi_pdf);
	                }
	            ?>
	            @if(!empty($pdf_arr))
	                @foreach ($pdf_arr as $pdf)
	                    <a href="{{ asset($pdf) }}" target="_blank">{{ $pdf }} </a><br>
	                @endforeach
	            @endif
			</td>

			<?php
			$s +=$lists->count();
			?>
		</tr>
			@break
			@endforeach
		@endforeach
	</tbody>
</table>
<input type="hidden" class="list-statusloi-sum" value="{{$s}}">
</div>