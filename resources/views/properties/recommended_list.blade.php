<div class="col-md-6 col-lg-6 col-sm-12">
  <?php
  if($v_data->tenant_id && !isset($EmpfehlungDetail_arr[$t_id]['mv_vl']))
      $EmpfehlungDetail_arr[$t_id]['mv_vl'] = "mv";
  if(!$v_data->tenant_id && !isset($EmpfehlungDetail_arr[$t_id]['mv_vl']))
      $EmpfehlungDetail_arr[$t_id]['mv_vl'] = "vl";
  
                
  ?>
  @if(!$v_data->tenant_id)
                
  <select style="margin-left: 8px;width: 77%;" class="form-control change-comment-recommended" data-column="tenant_name" data-id="{{$t_id}}">
                    <option value="">Select</option>
                    @foreach($rented_list as $kid=>$rrow)
                    <option value="{{$kid}}" @if(@$EmpfehlungDetail_arr[$t_id]['tenant_name']==$kid) selected @endif>{{$rrow}}</option>
                    @endforeach
                  </select>
@else
<input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="tenant_name" placeholder="Mieter" value="{{@$EmpfehlungDetail_arr[$t_id]['tenant_name']}}" style="margin-left: 8px;width: 77%;" >
 @endif   <br>
    
    <select style="margin-left: 8px;width: 77%;" class="form-control change-comment-recommended" data-column="mv_vl" data-id="{{$t_id}}">
      <option value="">Select</option>
      <option value="mv" @if(@$EmpfehlungDetail_arr[$t_id]['mv_vl']=="mv") selected @endif>Mietvertrag</option>
      <option value="vl" @if(@$EmpfehlungDetail_arr[$t_id]['mv_vl']=="vl") selected @endif>Verlängerung</option>
    </select>
    <br>
  </div>
<div class="clearfix"></div>

<div class="col-md-1">
                    <strong>Anhänge</strong>
                    </div>
<div class="col-md-3 col-lg-3 col-sm-12">
  
                  <div class="uploaded-files uploaded-files-empfehlung2 rowt-{{$t_id}}" style="float:left">

                                <a href="javascript:void(0);" class="link-button-empfehlung2"  id="empfehlung_file_link_{{$t_id}}" data-empfehlung_id="{{$t_id}}" data-property_id="{{$property_id}}" data-tenant_id="{{$t_id}}" >
                                  <i class="fa fa-link"></i>
                                </a>
                                @if( isset($v_data->files2) )
                                  @foreach($v_data->files2 as $file)
                                  @if($file['file_type'] == 'file')
                                    <a href="{{ $file['file_href'] }}"  target="_blank" title="{{ $file['file_name'] }}">
                                    <i extension="filemanager.file_icon_array.{{$file['extension']}}" class="fa {{ config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file' }}" ></i>
                                    </a>
                                  @else
                                    <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  id="empfehlung-gdrive-link-{{ $file['id'] }}"  ><i class="fa fa-folder" ></i></a>
                                  @endif
                                  @endforeach
                                @endif 
                            </div>
                            <a class="listFilesForDelete" data-property_id="{{$property_id}}" data-id="{{$t_id}}" data-type="empfehlung2"><i class="fa fa-trash iconstyle"></i></a>
                  </div>
<div class="clearfix"></div>
<div class="col-md-6 col-lg-6 col-sm-12">
<h4>Kosten Umbau</h4>
<table class="table">
  <tbody class="recommended-list">
    <?php
    $total = 0;
    ?>
    @if($lativity->count())
    @foreach($lativity as $k=>$list)
    <?php
    $total +=$list->amount;
    ?>
    <tr>
    <td>
    <input type="text" name="name[]" class="form-control" value="{{$list->name}}" placeholder="z.B. Küche">
    <input type="hidden" name="empfehlung_id[]" value="{{$list->id}}" >
    </td>
    <td>
    <input type="text" name="amount[]" class="form-control mask-number-input-nodecimal" value="{{number_format($list->amount,2,',','.')}}">
    </td>
    <td>
          {{--<a class="empfehlung-upload-file-icon" href="javascript:void(0);"> <i class="fa fa-upload"></i></a>
          <input type="file" name="empfehlung_file_{{$list->id}}" class="hide empfehlung-upload-file-control" id="empfehlung_file_{{$list->id}}" data-empfehlung_id="{{$list->id}}" data-property_id="{{$property_id}}" data-tenant_id="{{$t_id}}" >
          --}}
          <div class="uploaded-files uploaded-files-empfehlung row-{{$list->id}}" style="float:left">

              <a href="javascript:void(0);" class="link-button-empfehlung"  id="empfehlung_file_link_{{$list->id}}" data-empfehlung_id="{{$list->id}}" data-property_id="{{$property_id}}" data-tenant_id="{{$t_id}}" >
                <i class="fa fa-link"></i>
              </a>
              @if( isset($list->files) )
                @foreach($list->files as $file)
                @if($file['file_type'] == 'file')
                  <a href="{{ $file['file_href'] }}"  target="_blank" title="{{ $file['file_name'] }}">
                  <i extension="filemanager.file_icon_array.{{$file['extension']}}" class="fa {{ config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file' }}" ></i>
                  </a>
                @else
                  <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  id="empfehlung-gdrive-link-{{ $file['id'] }}"  ><i class="fa fa-folder" ></i></a>
                @endif
                @endforeach
              @endif 
          </div>
          <a class="listFilesForDelete" data-property_id="{{$property_id}}" data-id="{{$list->id}}" data-type="empfehlung"><i class="fa fa-trash iconstyle"></i></a>
    </td>
    <td>
        @if($k==0)
        <button type="button" class="btn-success btn-sm add-row-rec">+</button>
        @else
        <button type="button" class="btn-danger btn-sm remove-mieter">-</button>
        @endif
    </td>
    </tr>
    @endforeach
    @else
    <tr>
    <td>
    <input type="text" name="name[]" class="form-control" placeholder="z.B. Küche">
    <input type="hidden" name="empfehlung_id[]" value="0" >
    </td>
    <td>
    <input type="text" name="amount[]" class="form-control mask-number-input-nodecimal">
    </td>
    <td>
      <button type="button" class="btn-success btn-sm add-row-rec">+</button>
    </td>
    </tr>
    @endif
    <tr>
        <th>Gesamt</th>
        <th class="pl-20">{{number_format($total,2,',','.')}}</th>
        <td>&nbsp;</td>
    </tr>


  </tbody>
</table>
</div>
                  <div class="col-md-6 col-lg-6 col-sm-12">
                    <!-- <label>Kommentar</label> -->
                    <textarea class="form-control hidden change-comment-recommended" rows="15" data-id="{{$t_id}}" data-column="comment"></textarea>
                  </div>
                  <?php
                    $a1 = 0;
                    if(isset($EmpfehlungDetail_arr[$t_id]['amount1']) && $EmpfehlungDetail_arr[$t_id]['amount1'])
                    $a1 = $EmpfehlungDetail_arr[$t_id]['amount1'];

                    $a2 = 0;
                    if(isset($EmpfehlungDetail_arr[$t_id]['amount2']) && $EmpfehlungDetail_arr[$t_id]['amount2'])
                    $a2 = $EmpfehlungDetail_arr[$t_id]['amount2'];

                    $a3 = $a1*12;
                    $a4 = $a3*$a2;

                    $a5 = 0;
                    if(isset($EmpfehlungDetail_arr[$t_id]['amount5']) && $EmpfehlungDetail_arr[$t_id]['amount5'])
                    $a5 = $EmpfehlungDetail_arr[$t_id]['amount5'];

                    $a6 = 0;
                    if(isset($EmpfehlungDetail_arr[$t_id]['amount6']) && $EmpfehlungDetail_arr[$t_id]['amount6'])
                    $a6 = $EmpfehlungDetail_arr[$t_id]['amount6'];
                    ?>
                  <div class="clearfix"></div>
                  <div class="col-md-6 col-lg-6 col-sm-12">
                    <h4>Einnahmen</h4>
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>
                            <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="title1" value="{{@$EmpfehlungDetail_arr[$t_id]['title1']}}">
                          </td>
                          <td>
                            <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount1" value="{{number_format($a1,2,',','.')}}"> pro Monat
                          </td>
                          <td><button type="button" class="btn-danger btn-sm hidden">-</button></td>
                        </tr>
                        <tr>
                          <td>
                            Laufzeit in Jahren
                          </td>
                          <td>
                            <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount2" value="{{number_format($a2,2,',','.')}}">
                          </td>
                          <td><button type="button" class="btn-danger btn-sm hidden">-</button></td>
                        </tr>
                        <tr>
                          <td>
                            Mieteinnahmen p.a. 
                          </td>
                          <td class="pl-20">
                            {{number_format($a3,2,',','.')}}
                          </td>
                          <td><button type="button" class="btn-danger btn-sm hidden">-</button></td>
                        </tr>
                        <tr>
                          <th>
                            Einnahmen Gesamt 
                          </th>
                          <th class="pl-20">
                            {{number_format($a4,2,',','.')}}
                          </th>
                          <td><button type="button" class="btn-danger btn-sm hidden">-</button></td>
                        </tr>
                      </tbody>
                    </table>
                    <h4>Netto Einnahmen</h4>
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>&nbsp;</td>
                          <th class="pl-20">
                            {{number_format($a4-$total-$a5-$a6,2,',','.')}}
                          </th>
                          <td><button type="button" class="btn-danger btn-sm hidden">-</button></td>
                        </tr>
                      </tbody>
                    </table>
                </div>

                <br/>

                  <div class="col-md-6 col-lg-6 col-sm-12 recommendation_comments_section">
                    <div class="row">
                      <div class="col-md-12">
                          <button type="button" class="btn btn-primary btn-add-recommended-comment" data-tenant-id="{{ $t_id }}">Neuer Kommentar</button>
                      </div>

                      <div class="col-md-12">
                        <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;"  >
                          <table class="forecast-table logtable recommendation_comments_table" id="recommendation_comments_table_{{ $t_id }}" data-id="{{ $t_id }}">
                            <thead>
                              <tr>
                                <th class="bg-light-gray">Name</th>
                                <th class="bg-light-gray">Kommentar</th>
                                <th class="bg-light-gray">Datum</th>
                                <th class="bg-light-gray">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                              <tr>
                                <td colspan="4">
                                  <button type="button" class="btn-sm btn recommendation_comments_limit">Show More</button>
                                </td>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                  <div class="col-md-6 col-lg-6 col-sm-12" style="margin-top: 15px;">
                  <h4>Tenant Incentive</h4>
                  <table class="table">
                    <tbody>
                      <tr>
                        <td>
                          <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="title2" value="{{@$EmpfehlungDetail_arr[$t_id]['title2']}}">
                        </td>
                        <td>
                          <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount5" value="{{number_format($a5,2,',','.')}}">
                        </td>
                        <td><button type="button" class="btn-danger btn-sm hidden">-</button></td>
                      </tr>
                      <tr>
                        <td>
                          <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="title3" value="{{@$EmpfehlungDetail_arr[$t_id]['title3']}}">
                        </td>
                        <td>
                          <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount6" value="{{number_format($a6,2,',','.')}}">
                        </td>
                        <td><button type="button" class="btn-danger btn-sm hidden">-</button></td>
                      </tr>
                    </tbody>
                  </table>
                  </div>
                  
