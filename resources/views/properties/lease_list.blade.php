<table class="table table-striped dataTable">
  <thead>
    <tr class="head-title-sort">
        <th class="@if($column=='comment_1'){{'sorting_'.$sort}}@else{{'sorting'}} @endif" data-column="comment_1">Pot. Mieter</th>
        <th class="@if($column=='tenant'){{'sorting_'.$sort}}@else{{'sorting'}}@endif" style="width: 150px;" data-column="tenant">Fläche</th>
        <th class="@if($column=='type'){{'sorting_'.$sort}}@else{{'sorting'}}@endif" style="width: 150px;" data-column="type">Status</th>
        <th class="@if($column=='comment_2'){{'sorting_'.$sort}}@else{{'sorting'}}@endif" data-column="comment_2">Kommentare</th>
        <th class="@if($column=='updated_at'){{'sorting_'.$sort}}@else{{'sorting'}}@endif" data-column="updated_at">Date</th>
        <th></th>
    </tr>
  </thead>
  <tbody class="miter-list">
    @if($lativity->count())
    @foreach($lativity as $key=>$list)


      <?php
      $check = 0;
      if($list->tenant=='Sonstige Fläche')
        $check = 1;

      foreach($tenant_name as $list1){
        if($list->tenant==$list1){
          $check = 1;
        }
      }

      ?>

      <tr>
        <td><input type="text" name="comment_1[]" class="form-control" value="{{$list->comment_1}}"></td>
        <td>
          @if($check)
          @if($list->type=='Zusage')
          {{$list->tenant}}
          <input type="hidden" name="tenant[]" class="form-control" value="{{$list->tenant}}">
          @else
          <select class="form-control" name="tenant[]">    
            <option @if($list->tenant=='Sonstige Fläche') selected @endif value="Sonstige Fläche">Sonstige Fläche</option>
            @foreach($tenant_name as $list1)
            <option @if($list->tenant==$list1) selected @endif value="{{$list1}}">{{$list1}}</option>
            @endforeach
            </select>
            @endif
            @else
            {{$list->tenant}}
            <input type="hidden" name="tenant[]" class="form-control" value="{{$list->tenant}}">
            @endif
        </td>
        <?php
        $clss = "";
        if($list->type=='Zusage')
          $clss = "select-bg-green";
        if($list->type=='Mietvertrag ausgetauscht')
          $clss = "select-bg-yellow";
        if($list->type=='Absage')
          $clss = "select-bg-red";

        ?>
        <td>
            <select class="form-control lease-type {{$clss}}" name="type[]">
    <option @if($list->type=='Zusage') selected @endif value="Zusage">Zusage</option>
    <option @if($list->type=='Mietvertrag ausgetauscht') selected @endif value="Mietvertrag ausgetauscht">Mietvertrag ausgetauscht </option>
    <option @if($list->type=='Besichtigung') selected @endif value="Besichtigung">Besichtigung</option>
    <option @if($list->type=='Expose verschickt') selected @endif value="Expose verschickt">Expose verschickt</option>
    <option @if($list->type=='angefragt') selected @endif value="angefragt">angefragt</option>
    <option @if($list->type=='vorgemerkt') selected @endif value="vorgemerkt">vorgemerkt</option>
    <option @if($list->type=='Absage') selected @endif value="Absage">Absage</option>
    <option @if($list->type=='In Prüfung') selected @endif value="In Prüfung">In Prüfung</option>
</select>
        </td>
        <td>
          <textarea name="comment_2[]" class="form-control">{{$list->comment_2}}</textarea>
        </td>
        <td>{{date('d.m.Y',strtotime($list->updated_at))}}</td>
        <td>
            @if(!$q)
            @if($key==0)
              <button type="button" class="btn-success btn-sm add-more-mieter">+</button>
              @else
              <button type="button" class="btn-danger btn-sm remove-mieter">-</button>
              @endif
            @endif

              @if($q)
              <button type="button" class="btn-primary btn-sm save-row-mieter" data-id="{{$list->id}}">Speichern</button>
              @endif

        </td>
          
      </tr>
      @endforeach
      @else
      <tr>
          <td>
              <input type="text" name="comment_1[]" class="form-control" value="">
          </td>
          <td>
              <select class="form-control" name="tenant[]">
                <option value="Sonstige Fläche">Sonstige Fläche</option>
                @foreach($tenant_name as $list)
                <option value="{{$list}}">{{$list}}</option>
                @endforeach
            </select>
          </td>
          <td>
              <select class="form-control lease-type" name="type[]">
                    <option value="Zusage">Zusage</option>
                    <option value="Mietvertrag ausgetauscht">Mietvertrag ausgetauscht </option>
                    <option value="Besichtigung">Besichtigung</option>
                    <option value="Expose verschickt">Expose verschickt</option>
                    <option value="angefragt">angefragt</option>
                    <option value="vorgemerkt">vorgemerkt</option>
                    <option value="Absage">Absage</option>
                    <option value="In Prüfung">In Prüfung</option>
                </select>
          </td>
          <td>
              <textarea name="comment_2[]" class="form-control"></textarea>
          </td>
          <td></td>
          <td>
              @if(!$q)
              <button type="button" class="btn-success btn-sm add-more-mieter">+</button>
              @endif
          </td>
      </tr>
      @endif
  </tbody>
</table>