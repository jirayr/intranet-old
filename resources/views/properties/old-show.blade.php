<?php
//echo "<pre>";
//print_r($properties)
$rateSwotValArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
?>
@extends('layouts.admin')
@section('p_title')
{{ $properties->name_of_property }}
<?php
if ($properties->status == config('properties.status.in_purchase')):
    $status = __('property.in_purchase');
endif;

if ($properties->status == config('properties.status.offer')):
    $status = __('property.offer');
endif;

if ($properties->status == config('properties.status.exclusive')): $status = __('property.exclusive');
endif;

if ($properties->status == config('properties.status.exclusivity')): $status = __('property.exclusivity');
endif;

if ($properties->status == config('properties.status.certified')): $status = __('property.certified');
endif;

if ($properties->status == config('properties.status.duration')): $status = __('property.duration');
endif;

if ($properties->status == config('properties.status.sold')): $status = __('property.sold');
endif;

if ($properties->status == config('properties.status.declined')): $status = __('property.declined');
endif;

if ($properties->status == config('properties.status.lost')): $status = __('property.lost');
endif;

if ($properties->status == config('properties.status.hold')): $status = __('property.hold');
endif;

if ($properties->status == config('properties.status.quicksheet')): $status = __('property.quicksheet');
endif;

if ($properties->status == config('properties.status.liquiplanung')): $status = __('property.liquiplanung');
endif;

if ($properties->status == config('properties.status.externquicksheet')): $status = __('property.externquicksheet');
endif;

?>
@if(isset($properties->asset_manager))
(AM: {{$properties->asset_manager->name}}, Status: {{$status}})
@else
(Status: {{$status}})
@endif
<button type="button" id="mail-an-am" data-toggle="modal" data-target="#mail_an_am_modal" class="btn btn-primary btn-sm"></i> Mail an AM</button>
@endsection
@section('css')
<script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<!-- Styles -->
<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
<link rel="stylesheet"  href="{{ asset('css/tagsinput-revisited.css') }}" />
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection
@section('content')
<div class=" modal fade" role="dialog" id="send_email_modal">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">E-Mail senden</h4>
			</div>
			<form action="{{ route('save_status_loi') }}" method="post">
				<div class="modal-body">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$properties->id}}">
					<label>Receiver</label>
					<input id="receiver"  type="text"  name="receiver" class="form-control" required>
					<br>
					@php
					$tm_email_value = "";
					if(isset($email_template)){
					$tm_email_value = $email_template->email;
					}else{
					if($contact_person){
					$tm_email_value = $contact_person->email;
					}
					}
					@endphp
					<label>CC</label>
					<input type="text" id="cc" name="cc" class="form-control" value="{{$tm_email_value}}">
					<br>
					<label>BCC</label>
					<input type="text" id="bcc" name="bcc" class="form-control">
					<br>
					<label>Subject</label>
					<input type="text" name="subject" class="form-control" required>
					<br>
					<label>Message</label>
					<textarea class="form-control" name="message" required></textarea>
					<br>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Send</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class=" modal fade" role="dialog" id="load_all_status_loi_by_user">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div id="load_all_status_loi_by_user_content">
			</div>
		</div>
	</div>
</div>
<div class=" modal fade" role="dialog" id="mail_an_am_modal">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Mail an AM</h4>
			</div>
			<form action="{{ route('sendmail_to_am') }}" method="POST">
				<div class="modal-body">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$properties->id}}">
					<input type="hidden" name="am_id" value="{{$properties->asset_m_id}}">
					<input type="hidden" name="subject" value="{{ ($properties->plz_ort) ? $properties->plz_ort : '' }} - {{ ($properties->ort) ? $properties->ort : '' }}">
					<label>Nachricht</label>
					<textarea class="form-control" name="message" required></textarea>
					<br>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Senden</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	CKEDITOR.replace( 'message' );
</script>
<?php //var_dump($this->api_url);  ?>
@if(Session::has('massage'))
<p class="alert alert-success">{{ Session::get('massage') }}</p>
@endif
@if(Session::has('error'))
<p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif
@if(Session::has('success'))
<p class="alert alert-success">{{ Session::get('success') }}</p>
@endif
<ul class="nav nav-tabs">
	<li @if($tab == 'properties') class="active" @endif><a data-toggle="tab" href="#properties-tab">{{__('property.properties')}}</a></li>
	<li @if($tab == 'quick-sheet') class="active" @endif><a data-toggle="tab" href="#quick-sheet">Quick Sheet</a></li>
	<li @if($tab == 'sheet') class="active" @endif><a data-toggle="tab" href="#sheet">Sheet</a></li>
	<li @if($tab == 'budget') class="active" @endif><a data-toggle="tab" href="#budget">Budget</a></li>
	<li @if($tab == 'tenancy-schedule') class="active" @endif><a data-toggle="tab" href="#tenancy-schedule">Mieterliste</a></li>
	<li @if($tab == 'swot-template') class="active" @endif><a data-toggle="tab" href="#swot-template">{{__('property.SWOT_template')}}</a></li>
	<li @if($tab == 'schl-template') class="active" @endif><a data-toggle="tab" href="#schl-template">Objektdatenblatt</a></li>
	{{--	<li @if($tab == 'clever-fit-rental-offer') class="active" @endif><a data-toggle="tab" href="#clever-fit-rental-offer">Clever Fit Mietangebot</a></li>--}}
	<!-- <li @if($tab == 'planung-berechnung-wohnen') class="active" @endif><a data-toggle="tab" href="#planung-berechnung-wohnen">Planung Berechnung Wohnen</a></li> -->
	<li {{-- @if($tab == 'planung-berechnung-wohnen')  class="active" @endif --}}><a data-toggle="tab" href="#expose">Anhänge</a></li>
	<li @if($tab == 'bank-modal')  class="" @endif><a data-toggle="tab" href="#bank-modal">{{__('user_form.Bankenanfrage')}}</a></li>
	<li  @if($tab == 'comment_tab')  class="active" @endif  ><a data-toggle="tab" href="#comment_tab">Maßnahmen</a></li>
	<li  @if($tab == 'recommended_tab')  class="active" @endif  ><a data-toggle="tab" href="#recommended_tab">Empfehlung</a></li>
	<li  @if($tab == 'provision_tab')  class="active" @endif  ><a data-toggle="tab" href="#provision_tab">Provision</a></li>
	<li  @if($tab == 'property_invoice')  class="active" @endif  ><a data-toggle="tab" href="#property_invoice">Rechnungsfreigaben</a></li>
	<li @if($tab == 'einkauf_tab')  class="active" @endif><a data-toggle="tab" href="#einkauf_tab">Einkauf</a></li>
	<li @if($tab == 'vermietung')  class="active" @endif><a data-toggle="tab" href="#vermietung">Vermietungsportal
	</a></li>
	<li @if($tab == 'verkauf_tab')  class="active" @endif><a data-toggle="tab" href="#verkauf_tab">Verkauf</a></li>
	<li @if($tab == 'exportverkauf_tab')  class="active" @endif><a data-toggle="tab" href="#exportverkauf_tab">Exposé</a></li>
	<li @if($tab == 'Anzeige_aufgeben')  class="active" @endif><a data-toggle="tab" href="#Anzeige_aufgeben">Verkaufsportal
	</a></li>
	<li @if($tab == 'Anfragen')  class="active" @endif><a data-toggle="tab" href="#Anfragen">Anfragen</a></li>
	<!---<li @if($tab == 'Mietflächen')  class="active" @endif><a data-toggle="tab" href="#Mietflächen">Mietflächen</a></li> -->
	<li @if($tab == 'Leerstandsflächen')  class="active" @endif><a data-toggle="tab" href="#Leerstandsflächen">Leerstandsflächen</a></li>
	<li @if($tab == 'email_template')  class="active" @endif><a data-toggle="tab" href="#email_template">LOI</a></li>
	<!--- <li @if($tab == 'email_template2')  class="active" @endif><a data-toggle="tab" href="#email_template2">email template 2</a></li>
		-->
	<!-- <li @if($tab == 'Immobilie')  class="active" @endif><a data-toggle="tab" href="#Immobilie">Laden Sie eine Immobilie hoch
		</a></li> -->
	<li @if($tab == 'darlehensspiegel')  class="active" @endif><a data-toggle="tab" href="#darlehensspiegel">Darlehensspiegel</a></li>
	<li @if($tab == 'dateien')  class="active" @endif><a data-toggle="tab" href="#dateien">Dateien
	</a></li>
	<li  @if($tab == 'insurance_tab')  class="active" @endif  ><a data-toggle="tab" href="#insurance_tab">Versicherungen</a></li>
	<li  @if($tab == 'property_management')  class="active" @endif  ><a data-toggle="tab" href="#property_management">Hausverwaltung</a></li>
	<li  @if($tab == 'am_mail')  class="active" @endif  ><a data-toggle="tab" href="#am_mail">Mails an AM</a></li>
	{{--@if(Request::segment(2) == '85')--}}
	{{--<li  @if($tab == 'outlook-email')  class="active" @endif  ><a data-toggle="tab" href="#outlook-email">Email</a></li>--}}
	{{--@endif--}}
</ul>
<div class="tab-content">
	<input type="hidden" id="path-properties-show" value="{{route('properties.show', $id)}}">
	<input type="hidden" id="main-properties-path" value="{{route('properties.show', $id)}}">
	<div id="properties-tab" class="tab-pane fade {{($tab == 'properties') ? 'in active':''}}">
		<input type="hidden" id="selected_property_id" value="{{ $id }}">
		<td>
			<?php
				// print_r($properties);
				?>
			<!-- {{$properties->transaction_m_id}} -->
			<label>Einkäufer</label>
			<select name="transaction_m_id" class="change-p-user change_transaction_m_id">
				<option value="">{{__('dashboard.transaction_manager')}}</option>
				@foreach($tr_users as $list1)
				@if($properties && $properties->transaction_m_id==$list1->id)
				<option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
				@else
				<option value="{{$list1->id}}">{{$list1->name}}</option>
				@endif
				@endforeach
			</select>
			<label>Assetmanager</label>
			<select name="asset_m_id" class="change-p-user change_asset_m_id">
				<option value="">{{__('dashboard.asset_manager')}}</option>
				@foreach($as_users as $list1)
				@if($properties && $properties->asset_m_id==$list1->id)
				<option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
				@else
				<option value="{{$list1->id}}">{{$list1->name}}</option>
				@endif
				@endforeach
			</select>
			<label>Bauleiter</label>
			<select name="construction_manager" class="change-p-user construction_manager">
				<option value="">{{__('dashboard.asset_manager')}}</option>
				@foreach($as_users as $list1)
				@if($properties && $properties->construction_manager==$list1->id)
				<option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
				@else
				<option value="{{$list1->id}}">{{$list1->name}}</option>
				@endif
				@endforeach
			</select>
			<label>Verkäufer</label>
			<select name="seller_id" class="change-p-user change_seller_id">
				<option value="">{{__('dashboard.seller')}}</option>
				@foreach($tr_users as $list1)
				@if($properties && $properties->seller_id==$list1->id)
				<option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
				@else
				<option value="{{$list1->id}}">{{$list1->name}}</option>
				@endif
				@endforeach
			</select>
			<label>Art</label>
			<select name="property_status" class="change-p-user change_property_status">
			<option value="0" @if($properties->property_status==0) selected @endif>intern</option>
			<option value="1" @if($properties->property_status==1) selected @endif>extern</option>
			</select>
			<!-- {{$properties->status}} -->
			<label>Status</label>
			<select class="property-status" data-property-id="{{$properties->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
			<option value="{{config('properties.status.externquicksheet')}}" @if($properties->status == config('properties.status.externquicksheet')) {!!"selected" !!} @endif>{{__('property.externquicksheet')}}</option>
			<option value="{{config('properties.status.quicksheet')}}" @if($properties->status == config('properties.status.quicksheet')) {!!"selected" !!} @endif>{{__('property.quicksheet')}}</option>
			<option value="{{config('properties.status.in_purchase')}}" @if($properties->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
			<option value="{{config('properties.status.offer')}}" @if($properties->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
			<option value="{{config('properties.status.exclusive')}}" @if($properties->status == config('properties.status.exclusive')) {!!"selected" !!} @endif>{{__('property.exclusive')}}</option>
			<option value="{{config('properties.status.liquiplanung')}}" @if($properties->status == config('properties.status.liquiplanung')) {!!"selected" !!} @endif>{{__('property.liquiplanung')}}</option>
			<option value="{{config('properties.status.exclusivity')}}" @if($properties->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
			<option value="{{config('properties.status.certified')}}" @if($properties->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
			<option value="{{config('properties.status.duration')}}" @if($properties->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
			<option value="{{config('properties.status.hold')}}" @if($properties->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
			<option value="{{config('properties.status.sold')}}" @if($properties->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
			<option value="{{config('properties.status.declined')}}" @if($properties->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
			<option value="{{config('properties.status.lost')}}" @if($properties->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
			</select>
			<div class="btn-group" style="float: right;">
				<select class="set_as_standard" >
					<option value="">Als Standard gesetzt</option>
					<?php
						$bank_array = array();
						      foreach($banks as $key => $bank)
						      {
						          $bank_array[] = $bank->id;
						      }
						
						      array_unique($bank_array);
						      $str = implode(',', $bank_array);
						
						      $properties_banks = DB::table('properties')->select('sheet_title','properties.id','standard_property_status','Ist','soll','banks.name','properties_bank_id')->join('banks','banks.id','=','properties.properties_bank_id')->whereRaw('lock_status=0 and main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('properties_bank_id')->get();
						
						
						
						
						
						?>
					<?php
						$ar = array();
						foreach ($properties_banks as $key1 => $bank1):
						
						?>
					@if(!isset($ar[$bank1->properties_bank_id]))
					<optgroup label="{{$bank1->name}}">
						<?php
							$ar[$bank1->properties_bank_id] = 1;
							?>
						@if($key1>0)
					</optgroup>
					@endif
					@endif
					@if($bank1->Ist)
					<option @if($bank1->standard_property_status == 1) selected @endif value="{{ $bank1->id }}">Ist<?php if($bank1->sheet_title)echo "-".$bank1->sheet_title?>
					</option>
					@else
					<option @if($bank1->standard_property_status == 1) selected @endif value="{{ $bank1->id }}">Soll<?php if($bank1->sheet_title) echo "-".$bank1->sheet_title?>
					</option>
					@endif
					<?php endforeach; ?>
					@if($ar)
					</optgroup>
					@endif
				</select>
				<button type="button" class="btn btn-primary" id="property-zoom-out"><i class="fa fa-search-minus"></i></button>
				<button class="btn btn-default" disabled><span id="property-zoom-value"></span>&nbsp;%</button>
				<button type="button" class="btn btn-primary" id="property-zoom-in"><i class="fa fa-search-plus"></i></button>
			</div>
			<iframe src="{{route('properties.showIframe', $id)}}" id="propertyIframeParent" width="100%"  style="border:none;min-height: 2800px; "  scrolling="auto"></iframe>
			@foreach ($banks as $key => $bank)
			<?php
				if($bank == ''){
					$bank_check = 0;
					$bank = $fake_bank;
				}else{
					$bank_check = 1;
				}
				
				?>
			@endforeach
	</div>
	<div id="tenancy-schedule" class="tab-pane fade {{($tab == 'tenancy-schedule') ? 'in active':''}}">
	@include('properties.templates.tenancy-schedule')
	</div>
	<div id="Anfragen" class="tab-pane fade {{($tab == 'Anfragen') ? 'in active':''}}">
	@include('properties.templates.Anfragen')
	</div>
	<div id="tenant-listing" class="tab-pane fade {{($tab == 'tenant-listing') ? 'in active':''}}">
	<div class="white-box table-responsive" style="height: 600px; overflow: auto;">
	<table class="table">
	<tbody>
	<tr>
	<td colspan="4" class="bg-white"></td>
	<th>Laufzeit ab</th>
	<td colspan="2" class="bg-white"></td>
	</tr>
	<tr>
	<td colspan="4" class="bg-white"></td>
	<td>
	<a href="#" data-placement="bottom" class="inline-edit" data-type="date" data-pk="duration_from" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.duration_from')}}">{{$properties->duration_from}}</a>
	</td>
	<td colspan="2" class="bg-white"></td>
	</tr>
	<tr>
	<th><strong>Mieter</strong></th>
	<th>Netto Kaltmiete p.m.</th>
	<th>Netto Kaltmiete p.a.</th>
	<th>MV Beginn</th>
	<th>MV Ende</th>
	<th>WAULT</th>
	<th>Mieterlöse</th>
	</tr>
	<tr>
	<td>Ankermieter</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_pm_anchor_tenants" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_pm')}}">{{ number_format($properties->nk_pm_anchor_tenants,0,",",".") }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_anchor_tenants" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_pa')}}">{{ number_format($properties->nk_anchor_tenants,0,",",".") }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_begin_anchor_tenants" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.mv_begin')}}">{{ $properties->mv_begin_anchor_tenants }}</a>
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_anchor_tenants" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.mv_end')}}">{{ $properties->mv_anchor_tenants }}</a>
	</td>
	<td>{{number_format($properties->WAULT__of_anchor_tenants, 1,",",".")}}</td>
	<td>{{number_format($properties->Rental_income_of_anchor_tenants, 2,",",".")}}&nbsp;€</td>
	</tr>
	@for($i = 1; $i <= 5; $i++)
	<tr>
	<td>Mieter {{$i}}</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_pm_tenant{{$i}}" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_pm')}}">{{ number_format($properties["nk_pm_tenant{$i}"],0,",",".") }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_tenant{{$i}}" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_pa')}}">{{ number_format($properties["nk_tenant{$i}"],0,",",".") }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_begin_tenant{{$i}}" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.mv_begin')}}">{{ $properties["mv_begin_tenant{$i}"] }}</a>
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_tenant{{$i}}" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.mv_end')}}">{{ $properties["mv_tenant{$i}"] }}</a>
	</td>
	<td>{{number_format($properties["WAULT__of_tenant{$i}"], 1,",",".")}}</td>
	<td>{{number_format($properties["Rental_income_of_tenant{$i}"], 2,",",".")}}&nbsp;€</td>
	</tr>
	@endfor
	@foreach($tenants as $key => $tenant)
	<tr>
	<td>Mieter {{ $key + 6 }}</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_pm" data-url="{{route('tenants.update', $tenant->id) }}" data-title="{{__('property.field.net_rent_pm')}}">{{ $tenant->nk_pm }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_pa" data-url="{{route('tenants.update', $tenant->id) }}" data-title="{{__('property.field.net_rent_pa')}}">{{ $tenant->nk_pa }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_begin" data-url="{{route('tenants.update', $tenant->id) }}" data-title="{{__('property.field.mv_begin')}}">{{ $tenant->mv_begin }}</a>
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_end" data-url="{{route('tenants.update', $tenant->id) }}" data-title="{{__('property.field.mv_end')}}">{{ $tenant->mv_end }}</a>
	</td>
	<td>{{number_format($tenant->wault, 1,",",".")}}</td>
	<td>{{number_format($tenant->rental_income, 2,",",".")}}&nbsp;€</td>
	</tr>
	@endforeach
	<tr>
	<td>
	<button type="button" class="btn btn-success" id="new-tenant" data-property-id="{{$id}}">Add Tenant</button>
	</td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	</tr>
	</tbody>
	</table>
	</div>
	</div>
	<div id="swot-template" class="tab-pane fade {{($tab == 'swot-template') ? 'in active':''}}">
	<div class="white-box table-responsive" style="height: 600px; overflow: auto;">
	<table class="table" style="width: 100%">
	<tbody>
	<tr>
	<th>Stärken (Strengths)</th>
	<th>Schwächen (Weaknesses)</th>
	</tr>
	<tr style="text-align: center; font-size: 24px; height: 40vh;">
	<td style="width: 50%">
	<a href="#" class="inline-edit" data-type="textarea" data-pk="strengths" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.strengths')}}">{{ $properties->strengths }}</a>
	</td>
	<td style="width: 50%">
	<a href="#" class="inline-edit" data-type="textarea" data-pk="weaknesses" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.weaknesses')}}">{{ $properties->weaknesses }}</a>
	</td>
	</tr>
	<tr>
	<th>Chancen (Opportunities)</th>
	<th>Risiken (Threats)</th>
	</tr>
	<tr style="text-align: center; font-size: 24px; height: 40vh;">
	<td style="width: 50%">
	<a href="#" class="inline-edit" data-type="textarea" data-pk="opportunities" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.opportunities')}}">{{ $properties->opportunities }}</a>
	</td>
	<td style="width: 50%">
	<a href="#" class="inline-edit" data-type="textarea" data-pk="threats" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.threats')}}">{{ $properties->threats }}</a>
	</td>
	</tr>
	</tbody>
	</table>
	<div class="row">
	<div class="swot_ranking col-md-8">
	<form id="swat_form">
	<?php
		$b = $b1 = $b2 = $b3 = $b4 = $b5 = 0;
		if($properties->swot_json)
		{
			// echo $properties->swot_json;
			$nnna = json_decode($properties->swot_json,true);
			if(isset($nnna[0]))
				$b = $nnna[0];
		
			if(isset($nnna[1]))
				$b1 = $nnna[1];
		
			if(isset($nnna[2]))
				$b2= $nnna[2];
		
			if(isset($nnna[3]))
				$b3= $nnna[3];
		
			if(isset($nnna[4]))
				$b4= $nnna[4];
		
			if(isset($nnna[5]))
				$b5= $nnna[5];
		}
		
		?>
	<table class="table table-striped">
	<tr>
	<td>
	<label>Lage : </label>
	<select id="rate_box_1" name="location" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Mietermix : </label>
	<select id="rate_box_2" name="tenant" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b1)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Mieterbonität : </label>
	<select id="rate_box_3" name="tenant_credit" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b2)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Finanzierbarkeit : </label>
	<select id="rate_box_4" name="affordability" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b3)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Bausubstanz innen : </label>
	<select id="rate_box_5" name="building_substance_inside" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b4)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Bausubstanz außen : </label>
	<select id="rate_box_6" name="building_substance_outside" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b5)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Summe : </label>
	<?php
		$bsum = $b +$b1 +$b2 +$b3 +$b4 +$b5;
		?>
	<input type="text" readonly value="{{$bsum}}" id="total_rate" name="total_rate" />
	</td>
	</tr>
	</table>
	<p>*10 ist am besten</p>
	</form>
	</div>
	</div>
	</div>
	</div>
	<?php
		$leerstand_total = 0;
		$vermietet_total = 0;
		$GLOBALS['ist_sheet'] = "";
		$GLOBALS['release_sheet'] = "";
		
		?>
	<div id="quick-sheet" class="tab-pane fade {{($tab == 'quick-sheet') ? 'in active':''}}">
	@include('properties.templates.quicksheet')
	</div>
	<div id="sheet" class="tab-pane fade {{($tab == 'sheet') ? 'in active':''}}">
	@include('properties.templates.sheet')
	</div>
	<div id="budget" class="tab-pane fade {{($tab == 'budget') ? 'in active':''}}">
	@include('properties.templates.budget')
	</div>
	<div id="darlehensspiegel" class="tab-pane fade {{($tab == 'darlehensspiegel') ? 'in active':''}}">
	@include('properties.templates.darlehensspiegel')
	</div>
	<div id="insurance_tab" class="tab-pane fade {{($tab == 'insurance_tab') ? 'in active':''}}">
	@include('properties.templates.insurance_tab')
	</div>
	{{--<div id="outlook-email" class="tab-pane fade {{($tab == 'outlook-email') ? 'in active':''}}">--}}
	{{--@include('properties.email')--}}
	{{--</div>--}}
	<div id="dateien" class="tab-pane fade {{($tab == 'dateien') ? 'in active':''}}">
	@include('properties.templates.dateien')
	</div>
	<div id="schl-template" class="tab-pane fade {{($tab == 'schl-template') ? 'in active':''}}">
	@include('properties.templates.schl')
	</div>
	<div id="clever-fit-rental-offer" class="tab-pane fade {{($tab == 'clever-fit-rental-offer') ? 'in active':''}}">
	@include('properties.templates.clever-fit')
	</div>
	<div id="planung-berechnung-wohnen" class="tab-pane fade {{($tab == 'planung-berechnung-wohnen') ? 'in active':''}}">
	@include('properties.templates.planung-berechnung-wohnen')
	</div>
	<div id="expose" class="tab-pane fade {{($tab == 'expose') ? 'in active':''}}">
	@include('properties.templates.expose')
	</div>
	<div id="comment_tab" class="tab-pane fade {{($tab == 'comment_tab') ? 'in active':''}}">
	@include('properties.templates.comments_tab')
	</div>
	<div id="recommended_tab" class="tab-pane fade {{($tab == 'recommended_tab') ? 'in active':''}}">
	@include('properties.templates.recommended_tab')
	</div>
	<div id="provision_tab" class="tab-pane fade {{($tab == 'provision_tab') ? 'in active':''}}">
	@include('properties.templates.provision_tab')
	</div>
	<div id="property_invoice" class="tab-pane fade {{($tab == 'property_invoice') ? 'in active':''}}">
	@include('properties.templates.property_invoice')
	</div>
	<div id="einkauf_tab" class="tab-pane fade {{($tab == 'einkauf_tab') ? 'in active':''}}">
	@include('properties.templates.einkauf_tab')
	</div>
	<div id="verkauf_tab" class="tab-pane fade {{($tab == 'verkauf_tab') ? 'in active':''}}">
	@include('properties.templates.verkauf_tab')
	</div>
	<div id="exportverkauf_tab" class="tab-pane fade {{($tab == 'exportverkauf_tab') ? 'in active':''}}">
	@include('properties.templates.exportverkauf_tab')
	</div>
	<div id="Anzeige_aufgeben" class="tab-pane fade {{($tab == 'Anzeige_aufgeben') ? 'in active':''}}">
	@include('properties.templates.Anzeige_aufgeben')
	</div>
	<div id="vermietung" class="tab-pane fade {{($tab == 'vermietung') ? 'in active':''}}">
	@include('properties.templates.vermietung')
	</div>
	<div id="Immobilie" class="tab-pane fade {{($tab == 'Immobilie') ? 'in active':''}}">
	@include('properties.templates.Immobilie')
	</div>
	<div  class="uploadAttachement tab-pane fade {{($tab == 'uploadAttachement') ? 'in active':''}}">
	@include('properties.templates.upload_attachment_immobilie')
	</div>
	<div id="Mietflächen" class="tab-pane fade {{($tab == 'Mietflächen') ? 'in active':''}}">
	@include('properties.templates.Mietflächen')
	</div>
	<div id="Leerstandsflächen" class="tab-pane fade {{($tab == 'Leerstandsflächen') ? 'in active':''}}">
	@include('properties.templates.Leerstandsflächen')
	</div>
	<div id="email_template" class="tab-pane fade {{($tab == 'email_template') ? 'in active':''}}">
	@include('properties.templates.email_template')
	</div>
	<div id="email_template2" class="tab-pane fade {{($tab == 'email_template2') ? 'in active':''}}">
	@include('properties.templates.email_template2')
	</div>
	<div id="bank-modal" class="tab-pane fade {{($tab == 'bank-modal') ? 'in active':''}}">
	@include('properties.templates.bank-modal')
	</div>
	<div id="property_management" class="tab-pane fade {{($tab == 'property_management') ? 'in active':''}}">
	@include('properties.templates.property_management')
	</div>
	<div id="am_mail" class="tab-pane fade {{($tab == 'am_mail') ? 'in active':''}}">
	@include('properties.templates.am_mail_logs')
	</div>
	<iframe id="txtArea1" style="display:none"></iframe>
</div>
<div class="modal fade" id="vacant-release-property-modal" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<label>Kommentar</label>
<textarea class="form-control vacant-release-comment" name="message"></textarea>
<br>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary vacant-release-submit" data-dismiss="modal" >Senden</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>
<div class="modal fade" id="provision-release-property-modal" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<label>Kommentar</label>
<textarea class="form-control provision-release-comment" name="message"></textarea>
<br>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary provision-release-submit" data-dismiss="modal" >Senden</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>
<div class="modal fade" id="invoice-release-property-modal" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<label>Kommentar</label>
<textarea class="form-control invoice-release-comment" name="message"></textarea>
<br>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary invoice-release-submit" data-dismiss="modal" >Senden</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>


<div class="modal fade" id="not-release-modal" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<label>Kommentar</label>
<textarea class="form-control invoice-not-release-comment" name="message"></textarea>
<br>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary invoice-not-release-submit" data-dismiss="modal" >Senden</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>

<div class=" modal fade" role="dialog" id="add-property-bank-modal">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add Banken</h4>
</div>
<div class="modal-body">
<form id="add-p-bank">
<input type="hidden" name="_token" value="{{csrf_token()}}">
<input type="hidden" name="property_id" value="{{$id}}">
<label>Banken</label>
<select class="form-control property-bank" name="banken_id"></select>
<br>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary save-pbank" >Speichern</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>
<div class=" modal fade" role="dialog" id="nue-banken">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Neue Bank erstellen</h4>
</div>
<div class="modal-body">
<form id="add-new-banken">
<input type="hidden" name="_token" value="{{csrf_token()}}">
<label>Firma</label>
<input type="text" name="Firma" class="form-control">
<br>
<label>Anrede</label>
<input type="text" name="Anrede" class="form-control">
<br>
<label>Vorname</label>
<input type="text" name="Vorname" class="form-control">
<br>
<label>Nachname</label>
<input type="text" name="Nachname" class="form-control">
<br>
<label>Strasse</label>
<input type="text" name="Strasse" class="form-control">
<br>
<label>Ort</label>
<input type="text" name="Ort" class="form-control">
<br>
<label>Telefon</label>
<input type="text" name="Telefon" class="form-control">
<br>
<label>Mobil</label>
<input type="text" name="Fax" class="form-control">
<br>
<label>E-Mail</label>
<input type="text" name="E_Mail" class="form-control">
<br>
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary save-new-bank" >Speichern</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>
<div class=" modal fade" role="dialog" id="add-city">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Add Ort</h4>
</div>
<div class="modal-body">
<form id="add-ort">
<input type="hidden" name="_token" value="{{csrf_token()}}">
<label>Name</label>
<input type="text" name="name" class="form-control">
<br>
<label>Einwohner</label>
<input type="text" name="e_w" class="form-control">
<br>
<label>Kaufkraftindex</label>
<input type="text" name="kk_idx" class="form-control">
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary save-city" >Speichern</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>
{{--
<!-- Modal -->
<div class="myImmoModal modal fade" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Immoscout24 Upload</h4>
</div>
<div class="modal-body">
<form id="immoscout_form">
<label>Eckdaten</label>
<input type="text" name="" placeholder="PLZ" class="form-control">
<input type="text" name="" placeholder="Ort" class="form-control">
<input type="text" name="" placeholder="Straße" class="form-control">
<input type="text" name="" placeholder="Nr." class="form-control">
<label>Objektart</label>
<select name="objektart">
<option value="volvo">Austellungsfläche</option>
<option value="saab">Einkaufszentrum</option>
<option value="fiat">Factory Outlet</option>
<option value="audi">Kaufhaus</option>
<option value="audi">Kiosk</option>
<option value="audi">Laden</option>
<option value="audi">SB-Markt</option>
<option value="audi">Verkaufsfläche</option>
<option value="audi">Verkaufshalle</option>
</select><br>
<input type="text" name="" placeholder="Verkaufsfläche (m²)" class="form-control">
<input type="text" name="" placeholder="Gesamtfläche (m²)" class="form-control">
<input type="text" name="" placeholder="VK-fläche teilbar ab (m²)" class="form-control">
<input type="text" name="" placeholder="Mietpreis (€/m²)" class="form-control"><br>
<label>Kosten</label>
<input type="text" name="" placeholder="Nebenkosten (€)" class="form-control">
<input type="text" name="" placeholder="Kaution (€)" class="form-control"><br>
<label>Bilder und Dokumente</label>
<form action="{{ url('property_upload_pdf') }}" method="post" enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="property_id" id="select_property_id" value="{{ $id }}">
<div class="form-group">
<label>Dokument hochladen</label>
<input type="file" name="upload_pdf">
</div>
<input type="submit" name="" value="Hochladen" class="btn btn-success">
</form><br>
<form action="{{ url('property_upload_pdf') }}" method="post" enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="property_id" value="{{ $id }}">
<div class="form-group">
<label>Bilder hochladen</label>
<input type="file" name="upload_pdf">
</div><br>
<input type="submit" name="" value="Hochladen" class="btn btn-success">
</form><br>
<label>Ausstattung</label><br>
<label>Lageart</label>
<select name="lageart">
<option value="volvo">A-Lage</option>
<option value="saab">B-Lage</option>
<option value="fiat">Einkaufszentrum</option>
<option value="audi">Keine Angabe</option>
</select><br>
<input type="text" name="" placeholder="Etagenzahl" class="form-control"><br>
<label>Bausubstanz und Energieausweis</label><br>
<label>Objektzustand</label>
<select name="lageart">
<option value="volvo">Erstbezug</option>
<option value="saab">Neuwertig</option>
<option value="fiat">Gepflegt</option>
<option value="audi">Modernisiert</option>
</select>
<input type="text" name="" placeholder="Letzte Modernisierung" class="form-control">
<input type="text" name="" placeholder="Bauhjahr des Gebäudes" class="form-control">  <br>
<label>Beschreibung</label>
<input type="text" name="" placeholder="Überschrift" class="form-control">
<input type="text" name="" placeholder="Beschreibung" class="form-control">
<input type="text" name="" placeholder="Ausstattung" class="form-control">
<input type="text" name="" placeholder="Lage" class="form-control">
<input type="text" name="" placeholder="Sonstiges" class="form-control"><br>
<label>Kontaktdaten</label>
<input type="text" name="" placeholder="Vorname" class="form-control">
<input type="text" name="" placeholder="Nachname" class="form-control">
<input type="text" name="" placeholder="Telefonnummer" class="form-control">
<input type="text" name="" placeholder="E-Mail" class="form-control">
</form>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal" >Upload</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>
--}}
<div class="modal fade" id="insurance-modal-request" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<label>Kommentar</label>
<textarea class="form-control insurance-comment" name="message"></textarea>
<br>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary insurance-comment-submit" data-dismiss="modal" >Senden</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>
<div class="modal fade" id="management-modal-request" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<label>Kommentar</label>
<textarea class="form-control management-comment" name="message"></textarea>
<br>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-primary management-comment-submit" data-dismiss="modal" >Senden</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
</div>
</div>
</div>
</div>
<!-- Modal-->
<div class="modal fade" id="select-gdrive-folder-model" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<input type='hidden' name='select-gdrive-folder-type' id='select-gdrive-folder-type'>
<input type='hidden' name='select-gdrive-folder-data' id='select-gdrive-folder-data'>
<input type='hidden' name='select-gdrive-folder-callback' id='select-gdrive-folder-callback'>
<a href="javascript:;" class="navbar-brand clickable hide" id="gdrive-folder-previous-button">
<i class="fa fa-arrow-left"></i>
<span class="hidden-xs">{{ trans('lfm.nav-back') }}</span>
</a>
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body">
<h4 id="select-gdrive-folder-message">Select folder where to upload. </h4>
<div id="select-gdrive-folder-content">
</div>
</div>
</div><!-- Modal content-->
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="select-gdrive-file-model" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<input type='hidden' name='workingDir' id='workingDir'>
<input type='hidden' name='select-gdrive-file-type' id='select-gdrive-file-type'>
<input type='hidden' name='select-gdrive-file-data' id='select-gdrive-file-data'>
<input type='hidden' name='select-gdrive-file-callback' id='select-gdrive-file-callback'>
<a href="javascript:;" class="navbar-brand clickable hide" id="gdrive-file-previous-button">
<i class="fa fa-arrow-left"></i>
<span class="hidden-xs">{{ trans('lfm.nav-back') }}</span>
</a>
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title"></h4>
</div>
<div class="modal-body">
<h4>Select any file to link. </h4>
<div id="select-gdrive-file-content">
</div>
</div>
</div><!-- Modal content-->
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="viewFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Files</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<input type="hidden" id="file_id" value="0">
<div id="view-gdrive-file-content">
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="deleteFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Delete File</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<input type="hidden" id="file_id" value="0">
<div id="delete-gdrive-file-content">
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<!-- Modal -->
<div class=" modal fade" role="dialog" id="add_new_property_comment">
<div class="modal-dialog modal-dialog-centered " >
<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Kommentare</h4>
</div>
<form action="{{ url('properties-comment/save') }}" method="post" id="my-comment-form">
<div class="modal-body">
<input type="hidden" name="_token" value="{{csrf_token()}}">
<input type="hidden"  name="property_id" value="{{$properties->id}}">
<label>Kommentare</label>
<textarea class="form-control p-comment" name="comment"></textarea>
<div class="modal-footer">
<button type="submit" class="save-p-comment btn btn-primary" >Posten</button>
</div>
<br>
</div>
</form>
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="add_banken_contact_employee_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel">Add Bank contact</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<form action="{{ url('/addBankenContactEmployee') }}" method="post" id="add_banken_contact_employee_form">
<div class="modal-body">
<div class="modal-body">
<span id="add_banken_contact_employee_msg"></span>
<input type="hidden" name="_token" value="{{csrf_token()}}">
<input type="hidden" id="add_banken_contact_employee_banken_id"  name="banken_id" value="">
<div class="form-group">                                
<label>Vorname</label>
<input class="form-control" name="vorname" />                                
</div>
<div class="form-group">                                
<label>Nachname</label>
<input class="form-control" name="nachname" />                                
</div>
<div class="form-group">                                
<label>Telefon</label>
<input class="form-control" name="telefon" />                                
</div>
<div class="form-group">                                
<label>E-mail</label>
<input class="form-control" name="email" />                                
</div>                            
</div>                        
</div>
<div class="modal-footer">
<button type="submit" class="btn btn-primary" >Posten</button>
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</form>
</div>
</div>
</div>
<!-- Modal -->
@include('properties.popup_for_submit_listing_to_api')
@endsection

@section('js')

	<script>
		var route_prefix  	= '{{ url("file-manager") }}';
		var lfm_route    	= '{{ url("file-manager") }}';
		var lang 			= {!! json_encode(trans('lfm')) !!};
		var workingDir 		= "{{$workingDir}}";
		var _token 			= '{{ csrf_token() }}';
		var id 				= '{{ $id }}';
		var user_signature 	= '{{ url('/').'/img/giphy.gif' }}';
		var request_showall	= '{{ Request()->showall }}';
		var workingDir 		= '{{$workingDir}}';

		/*-------------------URL START------------------*/

		var url_property_saveEmpfehlungFile 	= '{{ url("property/saveEmpfehlungFile") }}';
		var url_saveEmpfehlungFile 				= '{{ route("saveEmpfehlungFile") }}';
		var url_property_addmaillog 			= '{{ url('property/addmaillog') }}';
		var url_properties_comment_get_comments = '{{ url('properties-comment/get-comments') }}';
		var url_properties_comment_save 		= '{{ url('properties-comment/save') }}';
		var url_getReleasebutton 				= '{{ route('getReleasebutton') }}';
		var url_getProvisionbutton 				= '{{ route('getProvisionbutton') }}';
		var url_changeempfehlungdetails 		= '{{ route('changeempfehlungdetails') }}';
		var url_changeprovisioninfo 			= '{{ route('changeprovisioninfo') }}';
		var url_saveractivity 					= '{{ route('saveractivity') }}';
		var url_recommended_list 				= '{{ route('recommended_list') }}';
		var url_saveleaseactivity 				= '{{ route('saveleaseactivity') }}';
		var url_lease_list 						= '{{ route('lease_list') }}';
		var url_updatelease 					= '{{ route('updatelease') }}';
		var url_save_new_post 					= '{{ route('save_new_post') }}';
		var url_add_new_vacant 					= '{{ route('add_new_vacant') }}';
		var url_delete_comment_file 			= '{{ route('delete_comment_file') }}';
		var url_savebankinfoformail 			= '{{ route('savebankinfoformail') }}';
		var url_save_vacant_space 				= '{{ route('save_vacant_space') }}';
		var url_save_vacant_data 				= '{{ route('save_vacant_data') }}';
		var url_properties_change_status 		= '{{ route('properties.change_status') }}';
		var url_remove_uploaded_file 			= '{{ route('remove_uploaded_file') }}';
		var url_remove_vacant_media 			= '{{ route('remove_vacant_media') }}';
		var url_set_vacant_media_favourite		= '{{ route('set_vacant_media_favourite') }}';
		var url_set_ad_image_favourite 			= '{{ route('set_ad_image_favourite') }}';
		var url_set_vacantitem_favourite 		= '{{ route('set_vacantitem_favourite') }}';
		var url_set_export_media_favourite 		= '{{ route('set_export_media_favourite') }}';
		var url_tenants_store 					= '{{ route('tenants.store') }}';
		var url_change_comment					= '{{ route('change-comment') }}?';
		var url_uploadfiles 					= '{{ route('uploadfiles') }}';
		var url_delete_ads_images 				= '{{ route('delete_ads_images') }}';
		var url_delete_ads_pdf 					= '{{ route('delete_ads_pdf') }}';
		var url_delete_property_pdf 			= '{{ route('delete_property_pdf') }}';
		var url_delete_property_insurance		= '{{ route('delete_property_insurance') }}';
		var url_delete_service_provider			= '{{ route('delete_service_provider') }}';
		var url_delete_maintenance				= '{{ route('delete_maintenance') }}';
		var url_delete_investation				= '{{ route('delete_investation') }}';
		var url_change_date						= '{{ route('change_date') }}';
		var url_property_set_as_standard 		= '{{ url('property/set_as_standard') }}/{{ $id }}';
		var url_delete_tenant 					= '{{ route('delete_tenant') }}';
		var url_properties_upload_listing_to_api = '{{ url('/properties/upload_listing_to_api') }}';
		var url_properties_upload_attachment_images = '{{ url('/properties/upload_attachment_images') }}';
		var url_email_template_users 			= '{{ url('email_template_users') }}';
		var url_updatepropertyuser 				= '{{ route('updatepropertyuser') }}';
		var url_properties_api_imag_del 		= '{{ url('/properties/api_imag_del') }}';
		var url_add_new_city 					= '{{ route('add_new_city') }}';
		var url_add_property_bank 				= '{{ route('add_property_bank') }}';
		var url_add_new_banken  				= '{{ route('add_new_banken') }}';
		var url_searchpbank						= '{{ route("searchpbank") }}';
		var url_searchcity 						= '{{ route("searchcity") }}';
		var url_searchbank 						= '{{ route("searchbank") }}';
		var url_property_update_steuerberater 	= '{{ url('property/update/steuerberater') }}/{{ $id }}';
		var url_property_update_schl 			= '{{ url('property/update/schl') }}/{{ $id }}';
		var url_getbanklist 					= '{{ route('getbanklist') }}';
		var url_getstatusloi 					= '{{ route('getstatusloi') }}';
		var url_property_management_add 		= '{{ url('property_management/add') }}';
		var url_property_management_update 		= '{{ route('property_management.update') }}';
		var url_getStatusLoiByUserId 			= '{{ route('getStatusLoiByUserId') }}';
		var url_schl_file 						= '{{ route('schl-file') }}';
		var url_upload_bank_file 				= '{{ route('upload-bank-file') }}';
		var url_uploadattachments 				= '{{ route('uploadattachments') }}';
		var url_remove_attachment_file 			= '{{ route('remove_attachment_file') }}';
		var url_removepropertybanken 			= '{{ route('removepropertybanken') }}';
		var url_removestatusloi 				= '{{ route('removestatusloi') }}';
		var url_statusloi_update 				= '{{ url('statusloi/update') }}';
		var url_update_versicherung_approved_status = '{{ url('/update_versicherung_approved_status') }}';
		var url_update_falk__status 			= '{{ url('/update_falk__status') }}';
		var url_delete_Property_insurance1 		= '{{ url('/delete_Property_insurance') }}';
		var url_sendemailtobankers 				= '{{ route('sendemailtobankers') }}';
		var url_property_update_schl_1 			= '{{url('property/update/schl') }}';
		var url_showresult 						= '{{ route('showresult') }}';
		var url_deletesheet			 			= '{{ route('deletesheet') }}';
		var url_genarate_property_pdf 			= '{{ url('/genarate_property_pdf') }}';
		var url_property_vacantreleaseprocedure = '{{ url('property/vacantreleaseprocedure') }}';
		var url_property_provisionreleaseprocedure = '{{ url('property/provisionreleaseprocedure') }}';
		var url_property_invoicereleaseprocedure = '{{ url('property/invoicereleaseprocedure') }}';
		var url_get_property_invoice 			= '{{ route('get_property_invoice', ['property_id' => $properties->id]) }}';
		var url_get_release_property_invoice 	= '{{ route('get_release_property_invoice', ['property_id' => $properties->id]) }}';
		var url_get_invoice_logs 				= '{{ route('get_invoice_logs', ['property_id' => $properties->id]) }}';
		var url_makeAsPaid 						= '{{ route('makeAsPaid') }}';
		var url_add_property_invoice 			= '{{ route('add_property_invoice') }}';
		var url_addBankenContactEmployee 		= '{{ route('addBankenContactEmployee') }}';
		var url_delete_property_invoice 		= '{{ route("delete_property_invoice", ":id") }}';
		var url_set_property_invoice_email 		= '{{ route("set_property_invoice_email", ["id" => ":id", "email" => ":email"]) }}';


		/*-------------------URL END------------------*/
	</script>

	{{-- <script src="{{ asset('js/bootstrap-editable.min.js') }}"></script> --}}
	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
	<script src="{{ asset('js/custom-datatable.js') }}"></script>
	<script src="{{ asset('js/tagsinput-revisited.js') }}"></script>
	<script src="{{ asset('js/ckeditor.js') }}"></script>
	<script type='text/javascript' src='//maps.googleapis.com/maps/api/js?libraries=places&#038;key=AIzaSyA5o1eFEZtJOyhgsDdJp0IHMwMIIi8LPgw&#038;callback'></script>
	<script src="{{ asset('file-manager/js/bootbox.min.js') }}"></script>
	<script src="{{ asset('file-manager/js/script.js') }}"></script>

	{{-- Page js --}}
	<script src="{{asset('js/property-details.js')}}"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key={{config('env.GOOGLEMAP_API_KEY')}}
	&libraries=places&callback=initAutocomplete" async defer></script>

	<script>
		function showResult(result) {
		
		    document.getElementById('latitude').value = result.geometry.location.lat();
		    document.getElementById('longitude').value = result.geometry.location.lng();
		}

		function getLatitudeLongitude(callback, address) {
	        // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
	        address = address || 'Ferrol, Galicia, Spain';
	        // Initialize the Geocoder
	        geocoder = new google.maps.Geocoder();
	        if (geocoder) {
	            geocoder.geocode({
	                'address': address
	            }, function (results, status) {
	                if (status == google.maps.GeocoderStatus.OK) {
	                    callback(results[0]);
	                }
	            });
	        }
	    }
		
		
		function generate_option_from_json(jsonData, fromLoad){
		    //Load Category Json Data To Brand Select
		    if (fromLoad === 'category_to_brand'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected> <?php echo trans('app.select_a_brand') ?> </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
		            }
		            $('#brand_select').html(option);
		            $('#brand_select').select2();
		        }else {
		            $('#brand_select').html('');
		            $('#brand_select').select2();
		        }
		        $('#brand_loader').hide('slow');
		    }else if(fromLoad === 'country_to_state'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Bundesland auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
		            }
		            $('#state_select').html(option);
		            //$('#state_select').select2();
		        }else {
		            $('#state_select').html('');
		           // $('#state_select').select2();
		        }
		        $('#state_loader').hide('slow');
		
		    }else if(fromLoad === 'vcountry_to_state'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Bundesland auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
		            }
		            $('#vstate_select').html(option);
		            //$('#state_select').select2();
		        }else {
		            $('#vstate_select').html('');
		           // $('#state_select').select2();
		        }
		        $('#state_loader').hide('slow');
		
		    }
		    else if(fromLoad === 'excountry_to_state'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Bundesland auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
		            }
		            $('#excountry_to_state').html(option);
		            //$('#state_select').select2();
		        }else {
		            $('#excountry_to_state').html('');
		           // $('#state_select').select2();
		        }
		        $('#state_loader').hide('slow');
		
		    }else if(fromLoad === 'state_to_city'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Stadt auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
		            }
		            $('#city_select').html(option);
		            //$('#city_select').select2();
		        }else {
		            $('#city_select').html('');
		            //$('#city_select').select2();
		        }
		        $('#city_loader').hide('slow');
		    }
		}
		
	    $(function(){
	    	$('.multi-category').select2();

	        $('#street').change(function(){
	            address = "";
	            var address = $(this).val();
	            // getLatitudeLongitude(showResult, address)
	        });
	    });
		
		$(function(){
		    $('#country').change(function(){
		        var country_id = $(this).val();
		        $('#state_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_state_by_country') }}',
		            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'country_to_state');
		            }
		        });
		    });
		
		    $('#vcountry').change(function(){
		        var country_id = $(this).val();
		        $('#state_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_state_by_country') }}',
		            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'vcountry_to_state');
		            }
		        });
		    });
		
		    $('#excountry').change(function(){
		        var country_id = $(this).val();
		        $('#state_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_state_by_country') }}',
		            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'excountry_to_state');
		            }
		        });
		    });
		
		    $('[name="state"]').change(function(){
		        var state_id = $(this).val();
		        $('#city_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_city_by_state') }}',
		            data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'state_to_city');
		            }
		        });
		    });
		});
	</script>

	@if(isset($ads) && $ads->latitude)
		<script type="text/javascript">
			var myLatLng = {lat: {{ $ads->latitude }}, lng: {{ $ads->longitude }} };
			var mytitle = "{{$ads->title}}";
		</script>
	@else
		<script type="text/javascript">
			var myLatLng = {lat: 48.135690, lng: 11.555480 };
			var mytitle = "You";
		</script>
	@endif

	<script type="text/javascript">
		window.einkauf_file_link_isPageReload = false;
		
	    // This example adds a search box to a map, using the Google Place Autocomplete
	    // feature. People can enter geographical searches. The search box will return a
	    // pick list containing a mix of places and predicted search terms.
		
		function initAutocomplete() {

	       	var map = new google.maps.Map(document.getElementById('dvMap'), {
	           center: myLatLng,
	           zoom: 15,
	           mapTypeId: google.maps.MapTypeId.ROADMAP
	       	});

	       	// Create the search box and link it to the UI element.
	       	var input = document.getElementById('pac-input');
	       	var searchBox = new google.maps.places.SearchBox(input);
	       	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	       	// Bias the SearchBox results towards current map's viewport.
	       	map.addListener('bounds_changed', function() {
	           	searchBox.setBounds(map.getBounds());
	       	});

	       	//Click event for getting lat lng
	       	google.maps.event.addListener(map, 'click', function (e) {
	           $('input#latitude').val(e.latLng.lat());
	           $('input#longitude').val(e.latLng.lng());
	       	});

	       	var marker = new google.maps.Marker({
	           position: myLatLng,
	           map: map,
	           title: mytitle
	       	});

	       	marker.setMap(map);

	       	var markers = [];
	       	// [START region_getplaces]
	       	// Listen for the event fired when the user selects a prediction and retrieve
	       	// more details for that place.
	       	searchBox.addListener('places_changed', function() {
	           var places = searchBox.getPlaces();

	           if (places.length == 0) {
	               return;
	           }

	           // Clear out the old markers.
	           markers.forEach(function(marker) {
	               marker.setMap(null);
	           });
	           markers = [];

	           // For each place, get the icon, name and location.
	           var bounds = new google.maps.LatLngBounds();
	           places.forEach(function(place) {
	               var icon = {
	                   url: place.icon,
	                   size: new google.maps.Size(71, 71),
	                   origin: new google.maps.Point(0, 0),
	                   anchor: new google.maps.Point(17, 34),
	                   scaledSize: new google.maps.Size(25, 25)
	               };

	               $('#latitude').val(place.geometry.location.lat());
	               $('#longitude').val(place.geometry.location.lng());

	               // Create a marker for each place.
	               markers.push(new google.maps.Marker({
	                   map: map,
	                   icon: icon,
	                   title: place.name,
	                   position: place.geometry.location
	               }));

	               if (place.geometry.viewport) {
	                   // Only geocodes have viewport.
	                   bounds.union(place.geometry.viewport);
	               } else {
	                   bounds.extend(place.geometry.location);
	               }
	           });
	           map.fitBounds(bounds);
	       	});
	       	// [END region_getplaces]

	       	var map2 = new google.maps.Map(document.getElementById('dvMap2'), {
	           center: myLatLng,
	           zoom: 15,
	           mapTypeId: google.maps.MapTypeId.ROADMAP
	       	});

	       	// Create the search box and link it to the UI element.
	       	var input = document.getElementById('pac-input2');
	       	var searchBox2 = new google.maps.places.SearchBox(input);
	       	map2.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	       	// Bias the SearchBox results towards current map's viewport.
	       	map2.addListener('bounds_changed', function() {
	           searchBox2.setBounds(map.getBounds());
	       	});

	       	//Click event for getting lat lng
	       	google.maps.event.addListener(map2, 'click', function (e) {
	           $('input#l-latitude').val(e.latLng.lat());
	           $('input#l-longitude').val(e.latLng.lng());
	       	});

	       	var marker = new google.maps.Marker({
	           position: myLatLng,
	           map: map2,
	           title: mytitle
	       	});
	       	marker.setMap(map2);

	       	var markers = [];
	       	// [START region_getplaces]
	       	// Listen for the event fired when the user selects a prediction and retrieve
	       	// more details for that place.
	       	searchBox2.addListener('places_changed', function() {
	           var places = searchBox2.getPlaces();

	           if (places.length == 0) {
	               return;
	           }

	           // Clear out the old markers.
	           markers.forEach(function(marker) {
	               marker.setMap(null);
	           });
	           markers = [];

	           // For each place, get the icon, name and location.
	           var bounds = new google.maps.LatLngBounds();
	           places.forEach(function(place) {
	               var icon = {
	                   url: place.icon,
	                   size: new google.maps.Size(71, 71),
	                   origin: new google.maps.Point(0, 0),
	                   anchor: new google.maps.Point(17, 34),
	                   scaledSize: new google.maps.Size(25, 25)
	               };

	               $('#l-latitude').val(place.geometry.location.lat());
	               $('#l-longitude').val(place.geometry.location.lng());

	               // Create a marker for each place.
	               markers.push(new google.maps.Marker({
	                   map: map2,
	                   icon: icon,
	                   title: place.name,
	                   position: place.geometry.location
	               }));

	               if (place.geometry.viewport) {
	                   // Only geocodes have viewport.
	                   bounds.union(place.geometry.viewport);
	               } else {
	                   bounds.extend(place.geometry.location);
	               }
	           });
	           map2.fitBounds(bounds);
	       	});

		}

		function init(){
			var input = document.getElementById( 'google_address' );
=======
	       }
	   }
	
	   $('body').on('click','.new-banken',function(){
	   	$('#nue-banken').modal();
	   })
	
	
	$( ".from_bond" ).keyup(function() {
		var form_bond_value = $( "from_bond" ).val()
		var value=100-form_bond_value
		$( "#bank_loan" ).val(value)
	});
	
	$( "#bank_loan" ).keyup(function() {
		var form_bond_value = $( "#bank_loan" ).val()
		var value=100-form_bond_value
		$( "#from_bond" ).val(value)
	});
	
</script>
<script>
	$('#genrate_property_pdf').on('click', function(event) {
		var formdata_pdf = new FormData($('#pdfadsPostForm')[0]);
		//var formdata_pdf = $('#pdfadsPostForm').serialize();
		//console.log(formdata_pdf);
	
	  $.ajax({
	    url: '{{ url('/genarate_property_pdf') }}', 
	    type: 'POST',
	    data: formdata_pdf, 
	    processData: false, 
	    contentType:false
	  }).done(function(){
	     var download_url = $('#download_property_pdf').attr('href');
	     window.location.href = download_url;
	  }).fail(function(){
	     //console.log("An error occurred, the files couldn't be sent!");
		});
	
	});
	var vacant_release_type = vacant_id = "";
	$('body').on('click','.vacant-release-request',function(){
		vacant_release_type= $(this).attr('data-column');
		vacant_id= $(this).closest('form').find('.tenant_id').val();
		// console.log(vacant_id);
		// console.log(vacant_release_type);
		$('.vacant-release-comment').val("")
	
		$('#vacant-release-property-modal').modal();
	
	});
	$('body').on('click','.vacant-release-submit',function(){
	    comment = $('.vacant-release-comment').val();
	    $.ajax({
	          type : 'POST',
	          url : "{{url('property/vacantreleaseprocedure') }}",
	          data : {tenant_id:vacant_id,step:vacant_release_type,  _token : '{{ csrf_token() }}',comment:comment,property_id:$('#selected_property_id').val()},
	          success : function (data) {
	              getrelbutton(vacant_id);
	          }
	      });
	  });
	
	$('body').on('click','.provision-release-request',function(){
		vacant_release_type= $(this).attr('data-column');
		vacant_id= $(this).closest('td').attr('data-id');
		$('.provision-release-comment').val("")
		$('#provision-release-property-modal').modal();
	
	});
	$('body').on('click','.provision-release-submit',function(){
	    comment = $('.provision-release-comment').val();
	    $.ajax({
	          type : 'POST',
	          url : "{{url('property/provisionreleaseprocedure') }}",
	          data : {tenant_id:vacant_id,step:vacant_release_type,  _token : '{{ csrf_token() }}',comment:comment,property_id:$('#selected_property_id').val()},
	          success : function (data) {
	              getprovisionbutton();
	          }
	      });
	  });
	
	
	$('body').on('click','.invoice-release-request',function(){
		vacant_release_type= $(this).attr('data-column');
		vacant_id= $(this).attr('data-id');
		$('.invoice-release-comment').val("")
		$('#invoice-release-property-modal').modal();
	
	});
	$('body').on('click','.invoice-release-submit',function(){
	    comment = $('.invoice-release-comment').val();
	    $.ajax({
	          type : 'POST',
	          url : "{{url('property/invoicereleaseprocedure') }}",
	          data : {id:vacant_id,step:vacant_release_type,  _token : '{{ csrf_token() }}',comment:comment,property_id:$('#selected_property_id').val()},
	          success : function (data) {
	              // getprovisionbutton();
	              $('#add_property_invoice_table').DataTable().ajax.reload();
	              $('#add_property_invoice_table2').DataTable().ajax.reload();
						
	              $('#invoice_mail_table').DataTable().ajax.reload();
						
	          }
	      });
	  });
	
	
	
	
	
	
	
	$(document).ready(function(){
		$('.non-editable').find('input').attr('readonly',true);
		$('.non-editable').find('textarea').attr('readonly',true);
		$('.non-editable').find('.add-row-rec').hide();
		$('.non-editable').find('.remove-mieter').hide();
		
	})
	
	$('.show-link-recommended').click(function(){
	      var cl = $(this).attr('data-class');
	      if($('.'+cl).hasClass('hidden')){
	          $(this).find('i').removeClass('fa-angle-down');
	          $(this).find('i').addClass('fa-angle-up');
	          $('.'+cl).removeClass('hidden');
	
	          //ajax code here
				var id = $(this).attr('data-id'); 
				getrelbutton(id);
	
	      }
	      else{
	          // $(this).html('<i class="fa fa-angle-down"></i>');
	          $(this).find('i').addClass('fa-angle-down');
	          $(this).find('i').removeClass('fa-angle-up');
	          $('.'+cl).addClass('hidden');
	          // $(this).next().addClass('hidden');
	      }
	});
	
</script>
<script type="text/javascript">
	$(document).ready(function() {
	    function init()
	{
	var input = document.getElementById( 'google_address' );

	        if ( null === input ) {
	            return;
	        }
	        autoComplete = new google.maps.places.Autocomplete(input);
	        autoComplete.setFields(['address_components', 'geometry', 'icon', 'name']);
	        autoComplete.addListener('place_changed', fillInAddress);
	    }

	    function fillInAddress() {
	        var place = autoComplete.getPlace();
	        $("#location_latitude").val(place.geometry.location.lat());
	        $("#location_longitude").val(place.geometry.location.lng());
	    }
	google.maps.event.addDomListener( window, 'load', init );
	
	
	/*----------------JS FOR ADD PROPERTY INVOICE - START-----------------*/
	
	$( document ).ajaxComplete(function() {
	if($('.invoice-comment').length)
	$('.invoice-comment').editable();
	});
	var add_property_invoice_table = $('#add_property_invoice_table').dataTable({
	"ajax": '{{ route('get_property_invoice', ['property_id' => $properties->id]) }}',
	"columnDefs": [
	{ className: "text-right", "targets": [2] }
	],
	"columns": [
	null,
	null,
	{ "type": "numeric-comma" },
	null,
	null,
	{ "type": "new-date" },
	// { "type": "new-date" },
	null,
	null,
	null,
	]
	});


	
	var add_property_invoice_table = $('#add_property_invoice_table2').dataTable({
	"ajax": '{{ route('get_release_property_invoice', ['property_id' => $properties->id]) }}',
	"columnDefs": [
	{ className: "text-right", "targets": [2] }
	],
	"columns": [
	null,
	null,
	{ "type": "numeric-comma" },
	null,
	null,
	{ "type": "new-date" },
	null,
	null,
	]
	});
	var not_release_id = 0;
	$('body').on('click','.mark-as-not-release',function(){
		not_release_id = $(this).attr('data-id');
		$('#not-release-modal').modal();
	})
	$('body').on('click','.invoice-not-release-submit',function(){
	    comment = $('.invoice-not-release-comment').val();
	    $.ajax({
	          type : 'POST',
	          url : "{{url('property/invoicenotrelease') }}",
	          data : {id:not_release_id, _token : '{{ csrf_token() }}',comment:comment,property_id:$('#selected_property_id').val()},
	          success : function (data) {
	              // getprovisionbutton();
	              $('#add_property_invoice_table').DataTable().ajax.reload();
	              $('#add_property_invoice_table3').DataTable().ajax.reload();
						
	              // $('#invoice_mail_table').DataTable().ajax.reload();
						
	          }
	      });
	  });

	var add_property_invoice_table3 = $('#add_property_invoice_table3').dataTable({
	"ajax": '{{ route('get_not_released_invoice', ['id' => $properties->id]) }}',
	"columnDefs": [
	{ className: "text-right", "targets": [2] }
	],
	"columns": [
	null,
	null,
	{ "type": "numeric-comma" },
	null,
	null,
	{ "type": "new-date" },
	null,
	]
	});
	var invoice_mail_table = $('#invoice_mail_table').dataTable({
	"ajax": '{{ route('get_invoice_logs', ['property_id' => $properties->id]) }}',
	"order": [[ 4, "desc" ]],
	"columns": [
	null,
	null,
	null,
	null,
	{ "type": "new-date" },
	]
	});

	$('body').on('click','.mark-as-paid',function(){
        var id = $(this).attr('data-id');
        $this = $(this);
        var url = "{{route('makeAsPaid')}}?id="+id;
        $.ajax({
            url: url,
        }).done(function (data) { 
            $this.removeClass('btn-primary');
            $this.removeClass('mark-as-paid');
            $this.addClass('btn-success');
        });

    })
	
	
	$(document).on('click', ".property_invoice-upload-file-icon", function(){
	$("#property_invoice_gdrive_file_upload").trigger('click');
	});
	
	$(document).on('change', "#property_invoice_gdrive_file_upload", function(e){
	    var fileName = e.target.files[0].name;
	    $("#property_invoice_gdrive_file_name_span").text(fileName);
	});
	
	$(document).on('click', ".link-button-gdrive-invoice", function(e){                        
	$("#select-gdrive-file-model").modal('show');
	$("#select-gdrive-file-type").val('property_invoice');
	$("#select-gdrive-file-callback").val('gdeiveSelectPropertyInvoice');
	$("#select-gdrive-file-data").val('');
	var workingDir = "{{$workingDir}}";
	selectGdriveFileLoad(workingDir);
	})
	window.gdeiveSelectPropertyInvoice = function(basename, dirname,curElement, dirOrFile){
	    $("#property_invoice_gdrive_file_basename").val(basename);
	    $("#property_invoice_gdrive_file_dirname").val(dirname);
	    $("#property_invoice_gdrive_file_type").val(dirOrFile);
	    var text = $(curElement).closest('tr').find('a.gdrive-file-link').text();
	    $("#property_invoice_gdrive_file_name").val(text);
	    $("#property_invoice_gdrive_file_name_span").text(text);
	    $("#select-gdrive-file-model").modal('hide');
>>>>>>> 1a8fe6806718bfe79fe660fd796021fda1d65d3e
	    
		google.maps.event.addDomListener( window, 'load', init );
	</script>

@endsection