@extends('layouts.admin')

@section('css')
    <!-- Styles -->
    <link href="{{ asset('css/property-comparison.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p>
                    <i class="icon fa fa-check"></i>{{Session::get('message')}}
                </p>
            </div>
        @endif @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p>
                    <i class="icon fa fa-check"></i>{{Session::get('error')}}
                </p>
            </div>
        @endif

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    <p style="float: left">{{__('property.property_comparison')}}</p>

                    <div style="text-align: right">
                        <div class="btn-group" role="group">
                            <a href="{{route('property_comparison').'?type=normal&status='.config('properties.status.buy')}}" type="button" class="btn btn-default">{{__('property.buy')}}</a>
                            <a href="{{route('property_comparison').'?type=normal&status='.config('properties.status.hold')}}" type="button" class="btn btn-default">{{__('property.hold')}}</a>
                            <a href="{{route('property_comparison').'?type=normal&status='.config('properties.status.decline')}}" type="button" class="btn btn-default">{{__('property.decline')}}</a>
                            <a href="{{route('property_comparison').'?type=extra'}}" type="button" class="btn btn-default">{{__('property.extra')}}</a>

                        </div>
                    </div>
                </div>

                <div class="table-responsive">

                    <table id="property-comparison" class="table table-bordered">
                        <thead>
                        <tr class="bg-gray color-red">
                            <th>{{__('property.place_or_city')}}/th>
                            <th>{{__('property.type')}}</th>
                            <th>{{__('property.business')}}</th>
                            <th>{{__('property.purchase_price')}}</th>
                            <th>{{__('property.purchase_price_total')}}</th>
                            <th>{{__('property.rental_fee')}}</th>
                            <th>{{__('property.yield')}}</th>
                            <th>{{__('property.cf')}}</th>
                            <th>{{__('property.ebt')}}</th>
                            <th>{{__('property.target_rent')}}</th>
                            <th>{{__('property.return_s')}}</th>
                            <th>{{__('property.cf')}}</th>
                            <th>{{__('property.ebt')}}</th>
                            <th>{{__('property.required_ek')}}</th>
                            <th>{{__('property.required_ek')}}</th>
                            <th>{{__('property.financing')}}</th>
                            <th>{{__('property.notaral_status')}}</th>
                            <th>{{__('property.completion')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="bg-blue">
                            <th class="color-red" colspan="18">{{__('property.extra')}}</th>
                        </tr>

                        {{--<tr class="bg-pink">--}}
                            {{--<td>Muldenstein</td>--}}
                            {{--<td>FMZ</td>--}}
                            {{--<td>1436</td>--}}
                            {{--<td>1000000</td>--}}
                            {{--<td>1090000</td>--}}
                            {{--<td>110130</td>--}}
                            {{--<td>0.11013</td>--}}
                            {{--<td>0.1251</td>--}}
                            {{--<td>0.1832</td>--}}
                            {{--<td>120000</td>--}}
                            {{--<td>0.12</td>--}}
                            {{--<td>0.1703</td>--}}
                            {{--<td>0.2284</td>--}}
                            {{--<td>218400</td>--}}
                            {{--<td></td>--}}
                            {{--<td>SPK Erzgebirge</td>--}}
                            {{--<td>7/3/2018</td>--}}
                            {{--<td></td>--}}
                        {{--</tr>--}}

                        @foreach($properties as $property)
                            <tr class="bg-pink">
                                @include('properties.comparison-row')
                            </tr>
                        @endforeach

                        <tr>
                            <td colspan="18">&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="pagination-wrapper" style="margin-left: 20px">
                        <div class="row pull-left">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
