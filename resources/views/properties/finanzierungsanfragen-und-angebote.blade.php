<div class="col-sm-12 table-responsive">
    <table class="table table-striped" id="email-template-table">
        @isset($propertyStatuses)
            <div>
                <select style="width: 25%" class="form-control property-status change-status" >
                    <option value="">Alle</option>
                    @foreach($propertyStatuses as $propertyStatus)
                        <option value="{{$propertyStatus->id}}" {{ ($status == $propertyStatus->id) ? 'selected' : '' }}>{{$propertyStatus->name}}</option>
                    @endforeach
                </select>
            </div>
            <br>
        @endisset
        <thead>
        <tr>
            <th>Objekt</th>
            <th>Status</th>
            <th>Transaction manager</th>
            <th>Auto. angefragt</th>
            <th>Bank</th>
            <th>Ort</th>
            <th>Ansprechpartner</th>
            <th>Notizen</th>
            <th>Zinsatz</th>
            <th>Tilgung</th>
            <th>FK-Anteil proz.</th>
            <th>FK-Anteil nominal</th>
            <th>Datum</th>
            <th>Uhrzeit</th>
        </tr>
        </thead>
        <tbody>
        @if($sendEmails)
            @foreach($sendEmails as $list)
                @if($status)
                    @if( $list->property)
                        <tr>
                            <td><a href="{{url('properties/'.$list->property_id.'?tab=bank-modal')}}">
                                    {{$list->property->name_of_property}}
                                </a>
                            </td>
                            <td>@if(isset($list->property->status_relation)){{$list->property->status_relation->name}}@endif</td>
                            <td>@if(isset($list->property->transaction_manager)){{$list->property->transaction_manager->name}}@endif</td>
                            <td><input data-id="{{$list->id}}" onclick="return false;" class='ck' data-val="send-email"  type='checkbox' checked></td>
                            <td>{{isset($list->getBank($list->email_to_send)->name) ? $list->getBank($list->email_to_send)->name : ''}}</td>
                            <td>{{isset($list->getBank($list->email_to_send)->address) ? $list->getBank($list->email_to_send)->address : ''}}</td>
                            <td>{{ isset($list->getBank($list->email_to_send)->firstName) ?  $list->getBank($list->email_to_send)->firstName.' '.$list->getBank($list->email_to_send)->surname: ''}}</td>
                            <td>
                                <a href="#" class="inline-edit" data-type="text" data-pk="notizen" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" >
                                    @if($list->notizen)
                                        {{ $list->notizen }}
                                    @else
                                        k.A.
                                    @endif
                                </a>
                            </td>
                            <td>
                                <a href="#" class="inline-edit" data-type="text" data-pk="interest_rate" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
                                    @if($list->interest_rate)
                                        {{ show_number($list->interest_rate,2) }}
                                    @else
                                        0
                                    @endif
                                </a>
                            </td>
                            <td>
                                <a href="#" class="inline-edit" data-type="text" data-pk="tilgung" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
                                    @if($list->tilgung)
                                        {{ show_number($list->tilgung,2) }}
                                    @else
                                        0
                                    @endif
                                </a>
                            </td>
                            <td>
                                <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_percentage" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
                                    @if($list->fk_share_percentage)
                                        {{ show_number($list->fk_share_percentage,2) }}
                                    @else
                                        0
                                    @endif
                                </a>
                            </td>
                            <td>
                                <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_nominal" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
                                    @if($list->fk_share_nominal)
                                        {{ show_number($list->fk_share_nominal,2) }}
                                    @else
                                        0
                                    @endif
                                </a>
                            </td>
                            <td>{{show_datetime_format($list->created_at,'d.m.Y')}}</td>
                            <td>{{show_datetime_format($list->created_at,'H:i')}}</td>
                        </tr>

                    @endif
                @else
                    <tr>
                        <td><a href="{{url('properties/'.$list->property_id.'?tab=bank-modal')}}">
                                {{$list->property->name_of_property}}
                            </a>
                        </td>
                        <td>@if(isset($list->property->status_relation)){{$list->property->status_relation->name}}@endif</td>
                        <td>@if(isset($list->property->transaction_manager)){{$list->property->transaction_manager->name}}@endif</td>
                        <td><input data-id="{{$list->id}}" onclick="return false;" class='ck' data-val="send-email"  type='checkbox' checked></td>
                        <td>{{isset($list->getBank($list->email_to_send)->name) ? $list->getBank($list->email_to_send)->name : ''}}</td>
                        <td>{{isset($list->getBank($list->email_to_send)->address) ? $list->getBank($list->email_to_send)->address : ''}}</td>
                        <td>{{ isset($list->getBank($list->email_to_send)->firstName) ?  $list->getBank($list->email_to_send)->firstName.' '.$list->getBank($list->email_to_send)->surname: ''}}</td>
                        <td>
                            <a href="#" class="inline-edit" data-type="text" data-pk="notizen" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" >
                                @if($list->notizen)
                                    {{ $list->notizen }}
                                @else
                                    k.A.
                                @endif
                            </a>
                        </td>
                        <td>
                            <a href="#" class="inline-edit" data-type="text" data-pk="interest_rate" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
                                @if($list->interest_rate)
                                    {{ show_number($list->interest_rate,2) }}
                                @else
                                    0
                                @endif
                            </a>
                        </td>
                        <td>
                            <a href="#" class="inline-edit" data-type="text" data-pk="tilgung" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
                                @if($list->tilgung)
                                    {{ show_number($list->tilgung,2) }}
                                @else
                                    0
                                @endif
                            </a>
                        </td>
                        <td>
                            <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_percentage" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
                                @if($list->fk_share_percentage)
                                    {{ show_number($list->fk_share_percentage,2) }}
                                @else
                                    0
                                @endif
                            </a>
                        </td>
                        <td>
                            <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_nominal" data-url="{{url('property/update/emailsendinginfo/'.$list->id) }}" data-inputclass="mask-number-input" data-title="">
                                @if($list->fk_share_nominal)
                                    {{ show_number($list->fk_share_nominal,2) }}
                                @else
                                    0
                                @endif
                            </a>
                        </td>
                        <td>{{show_datetime_format($list->created_at,'d.m.Y')}}</td>
                        <td>{{show_datetime_format($list->created_at,'H:i')}}</td>
                    </tr>

                @endif

            @endforeach
        @endif

        @foreach($banksFinancingOffers as $banksFinancingOffer)
            @if($status)
                @if( $list->property)
                    <tr>
                        <td><a href="{{url('properties/'.$banksFinancingOffer->property_id.'?tab=bank-modal')}}">
                                {{$list->property->name_of_property}}
                            </a>
                        </td>
                        <td>@if(isset($list->property->status_relation)){{$list->property->status_relation->name}}@endif</td>
                        <td>@if(isset($list->property->transaction_manager)){{$list->property->transaction_manager->name}}@endif</td>
                        <td><input data-id="{{$banksFinancingOffer->id}}" data-val="bank-finance" class='ck' type='checkbox' @if($banksFinancingOffer->is_auto_angefragt== 1)checked @endif></td>
                        <td>
                            <a href="javascript:void(0)" data-id="{{$banksFinancingOffer->id}}" class=" FinanzierungsangeboteEdit">{{$banksFinancingOffer->bank->name}}</a>
                        </td>
                        <td>{{$banksFinancingOffer->bank->address}}</td>
                        <td>{{$banksFinancingOffer->bank->contact_name}} <br>
                            {{$banksFinancingOffer->bank->contact_phone}}<br>{{$banksFinancingOffer->bank->contact_email}}
                        </td>
                        <td>
                            <a href="#" class="inline-edit" data-type="textarea" data-pk="telefonnotiz" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-title="">{{$banksFinancingOffer->telefonnotiz}}</a>
                        </td>
                        <td>
                            <a href="#" class="inline-edit" data-type="text" data-pk="interest_rate" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->interest_rate,2) }}</a>
                        </td>
                        <td>
                            <a href="#" class="inline-edit" data-type="text" data-pk="tilgung" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->tilgung,2) }}</a>
                        </td>
                        <td>
                            <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_percentage" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->fk_share_percentage,2) }}</a>
                        </td>
                        <td>
                            <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_nominal" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->fk_share_nominal,2) }}</a>
                        </td>

                        <td>{{show_datetime_format($banksFinancingOffer->created_at,'d.m.Y')}}</td>
                        <td>{{show_datetime_format($banksFinancingOffer->created_at,'H:i')}}</td>

                    </tr>

                @endif
            @else
                <tr>
                    <td><a href="{{url('properties/'.$banksFinancingOffer->property_id.'?tab=bank-modal')}}">
                            {{$list->property->name_of_property}}
                        </a>
                    </td>
                    <td>@if(isset($list->property->status_relation)){{$list->property->status_relation->name}}@endif</td>
                    <td>@if(isset($list->property->transaction_manager)){{$list->property->transaction_manager->name}}@endif</td>
                    <td><input data-id="{{$banksFinancingOffer->id}}" data-val="bank-finance" class='ck' type='checkbox' @if($banksFinancingOffer->is_auto_angefragt== 1)checked @endif></td>
                    <td>
                        <a href="javascript:void(0)" data-id="{{$banksFinancingOffer->id}}" class=" FinanzierungsangeboteEdit">{{$banksFinancingOffer->bank->name}}</a>
                    </td>
                    <td>{{$banksFinancingOffer->bank->address}}</td>
                    <td>{{$banksFinancingOffer->bank->contact_name}} <br>
                        {{$banksFinancingOffer->bank->contact_phone}}<br>{{$banksFinancingOffer->bank->contact_email}}
                    </td>
                    <td>
                        <a href="#" class="inline-edit" data-type="textarea" data-pk="telefonnotiz" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-title="">{{$banksFinancingOffer->telefonnotiz}}</a>
                    </td>
                    <td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="interest_rate" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->interest_rate,2) }}</a>
                    </td>
                    <td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="tilgung" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->tilgung,2) }}</a>
                    </td>
                    <td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_percentage" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->fk_share_percentage,2) }}</a>
                    </td>
                    <td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="fk_share_nominal" data-url="{{url('property/update/bankoffer/'.$banksFinancingOffer->id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number($banksFinancingOffer->fk_share_nominal,2) }}</a>
                    </td>

                    <td>{{show_datetime_format($banksFinancingOffer->created_at,'d.m.Y')}}</td>
                    <td>{{show_datetime_format($banksFinancingOffer->created_at,'H:i')}}</td>

                </tr>

            @endif
        @endforeach
        </tbody>
    </table>
</div>


