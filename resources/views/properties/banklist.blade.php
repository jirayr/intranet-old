<div class="col-sm-12 table-responsive">
<a href="javascript:void(0)" class="btn btn-success add-property-bank pull-right" style="margin-bottom: 10px;">Hinzufügen</a>
<a href="javascript:void(0)" class="btn btn-success pull-right new-banken" style="margin-bottom: 10px;margin-right: 5px;">Neue Bank hinzufügen</a>
<table class="table table-striped" id="list-banks">
	<thead>
	<tr>
		<th>Bank</th>
		<th>Ort</th>
		<th>Ansprechpartner</th>
		<th>Telefon</th>
		<th>Email</th>
		<th>Notizen</th>
		<th>Zinsatz</th>
		<th>Tilgung</th>
		<th>FK-Anteil proz.</th>
		<th>FK-Anteil nominal</th>		
		<th>Anhang</th>		
		<th width="200">Aktion</th> 


	</tr>
	</thead>
	<tbody>
		@foreach($data as $list)
		<tr>
			<td>{{$list->Firma}}</td>
			<td>{{$list->Ort}}</td>
			<td>{{$list->Vorname}} {{$list->Nachname}}</td>
			<td>{{$list->Telefon}}</td>
			<td>{{$list->E_Mail}}</td>
			<td><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-placement="bottom" data-url="{{url('updatebanken/'.$list->id) }}" >{{$list->comment}}</a></td>
			<td><a href="#" class="inline-edit" data-type="text" data-pk="rate" data-placement="bottom" data-url="{{url('updatebanken/'.$list->id) }}" >
				<?php if(!$list->rate)
				{	
					$list->rate = 0;
				} ?>
				{{number_format($list->rate,2,",",".")}}</a>%</td>
			<td><a href="#" class="inline-edit" data-type="text" data-pk="redemption" data-placement="bottom" data-url="{{url('updatebanken/'.$list->id) }}" >
				<?php if(!$list->redemption)
				{	
					$list->redemption = 0;
				} ?>
				{{number_format($list->redemption,2,",",".")}}</a>%</td>
			<td><a href="#" class="inline-edit" data-type="text" data-pk="share_percent" data-placement="bottom" data-url="{{url('updatebanken/'.$list->id) }}" ><?php if(!$list->share_percent)
				{	
					$list->share_percent = 0;
				} ?>
				{{number_format($list->share_percent,2,",",".")}}</a>%</td>
			<td><a href="#" class="inline-edit" data-type="text" data-pk="share_nominal" data-placement="bottom" data-url="{{url('updatebanken/'.$list->id) }}" ><?php if(!$list->share_nominal)
				{	
					$list->share_nominal = 0;
				} ?>
				{{number_format($list->share_nominal,2,",",".")}}</a> €</td>
			<td class="bank-file-upload">
	            <input type="file" data-id="{{$list->id}}">
	            @if($list->file)
	            <?php
	            $ar[0] = $list->file;
	            ?>
	            @foreach($ar as $list1)
	            <a href="{{ asset('pdf_upload/'.$list1) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
	            @endforeach
	            @endif
	          </td>
			<td>
				<a href="javascript:void(0)" class="btn btn-primary add-bank-contact-btn" data-id="{{$list->banken_id}}">Kontakt hinzufügen</a>
				<a href="javascript:void(0)" class="btn btn-danger remove-p-bank" data-id="{{$list->id}}"><i class="fa fa-trash"></i></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
</div>