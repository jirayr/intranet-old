@if ($property->is_extra == 0)
<td class="option">
    <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" style="font-size: 11px">
            @if($property->status != config('properties.status.buy'))
                <li><a href="#" class="status-option" data-status="{{config('properties.status.buy')}}">{{ __('property.buy') }}</a></li>
            @endif
            @if($property->status != config('properties.status.hold'))
                <li><a href="#" class="status-option" data-status="{{config('properties.status.hold')}}">{{ __('property.hold') }}</a></li>
            @endif
            @if($property->status != config('properties.status.decline'))
                <li><a href="#" class="status-option" data-status="{{config('properties.status.decline')}}">{{ __('property.decline') }}</a></li>
            @endif

        </ul>
    </div>
</td>
@endif
<td>{{$property->name_of_property}}</td>
<td>{{$property->city_place}}</td>
<td>Expert</td>
<td>{{$property->plots}}</td>

<td>{{number_format($property->total_purchase_price,2, ',', '.')}}&nbsp;€</td>
<td>{{number_format($property->total_nbpac,2, ',', '.')}}&nbsp;€</td>
<td>{{number_format($property->rent,2, ',', '.')}}&nbsp;€</td>
<td>{{number_format($property->gross_yield * 100,2, ',', '.')}}&nbsp;%</td>
<td>{{number_format($property->EK_Rendite_CF ,2, ',', '.')}}&nbsp;%</td>
<td>{{number_format($property->tax * 100,2, ',', '.')}}&nbsp;%</td>
<td>{{number_format($property->net_rent_empty * 100,2, ',', '.')}}&nbsp;%</td>
<td>0</td>
<td>{{number_format($property->EK_Rendite_CF,2, ',', '.')}}&nbsp;%</td>
<td>{{number_format($property->tax * 100,2, ',', '.')}}&nbsp;%</td>
<td>0</td>
<td></td>
<td></td>
<td>noch zu teuer</td>
<td></td>