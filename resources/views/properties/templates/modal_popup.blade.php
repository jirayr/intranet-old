
<div class="modal fade" id="vacant-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control vacant-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary vacant-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="vacant-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control vacant_release_comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary vacant_release_submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="vacant-pending-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control vacant_pending_comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary vacant_pending_submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="vacant-not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control vacant_not_release_comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary vacant_not_release_submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="provision-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control provision-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary provision-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="invoice-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <p class="i-message hidden">Möchten Sie die Rechnung wirklich an Falk zur Freigabe senden?</p>
            <label class="i-message2">Kommentar</label>
            <textarea class="form-control invoice-release-comment i-message2" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="invoice-release-property-modal-am" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label class="am-list">User</label>
            <select class="am-list invoice_asset_manager form-control">
               <option value="">{{__('dashboard.asset_manager')}}</option>
               @if(isset($active_users))
               @foreach($active_users as $list1)
               <option value="{{$list1->id}}">{{$list1->name}}</option>
               @endforeach
               @endif
            </select>
            <br>
            <label class="">Kommentar</label>
            <textarea class="form-control invoice-release-comment-am" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-release-submit-am" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="deal-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control deal-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary deal-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="deal-not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control deal-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary deal-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="invoice-reject-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-reject-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-reject-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="pending-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-pending-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-pending-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="pending-modal-am" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-pending-am-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-pending-am-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="add-property-bank-modal">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Banken</h4>
         </div>
         <div class="modal-body">
            <form id="add-p-bank">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <input type="hidden" name="property_id" value="{{$id}}">
               <label>Banken</label>
               <select class="form-control property-bank" name="banken_id"></select>
               <br>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary save-pbank" >Speichern</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class=" modal fade" role="dialog" id="nue-banken">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Neue Bank erstellen</h4>
         </div>
         <div class="modal-body">
            <form id="add-new-banken">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <label>Firma</label>
               <input type="text" name="Firma" class="form-control">
               <br>
               <label>Anrede</label>
               <input type="text" name="Anrede" class="form-control">
               <br>
               <label>Vorname</label>
               <input type="text" name="Vorname" class="form-control">
               <br>
               <label>Nachname</label>
               <input type="text" name="Nachname" class="form-control">
               <br>
               <label>Strasse</label>
               <input type="text" name="Strasse" class="form-control">
               <br>
               <label>Ort</label>
               <input type="text" name="Ort" class="form-control">
               <br>
               <label>Telefon</label>
               <input type="text" name="Telefon" class="form-control">
               <br>
               <label>Mobil</label>
               <input type="text" name="Fax" class="form-control">
               <br>
               <label>E-Mail</label>
               <select class="form-control" multiple="multiple" id="bank_email_contact" name="E_Mail[]" style="width: 100%">
               </select>
               <br>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary save-new-bank" >Speichern</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class=" modal fade" role="dialog" id="edit-banken">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Bank bearbeiten</h4>
         </div>
         <div class="modal-body">
            <form id="edit-banl-banken">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <input type="hidden" name="id_bank" id="edit-bank-id">
               <label>Firma</label>
               <input type="text" name="Firma" class="form-control" id="edit-Firma">
               <br>
               <label>Anrede</label>
               <input type="text" name="Anrede" class="form-control" id="edit-Anrede">
               <br>
               <label>Vorname</label>
               <input type="text" name="Vorname" class="form-control" id="edit-Vorname">
               <br>
               <label>Nachname</label>
               <input type="text" name="Nachname" class="form-control" id="edit-Nachname">
               <br>
               <label>Strasse</label>
               <input type="text" name="Strasse" class="form-control" id="edit-Strasse">
               <br>
               <label>Ort</label>
               <input type="text" name="Ort" class="form-control" id="edit-Ort">
               <br>
               <label>Telefon</label>
               <input type="text" name="Telefon" class="form-control" id="edit-Telefon">
               <br>
               <label>Mobil</label>
               <input type="text" name="Fax" class="form-control" id="edit-Fax">
               <br>
               <label>E-Mail</label>
               <select class="form-control" multiple="multiple" id="edit_bank_email_contact" name="E_Mail[]" style="width: 100%">
               </select>
               <br>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary edit-existing-bank" >Speichern</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class=" modal fade" role="dialog" id="add-city">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Ort</h4>
         </div>
         <div class="modal-body">
            <form id="add-ort">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <label>Name</label>
               <input type="text" name="name" class="form-control">
               <br>
               <label>Einwohner</label>
               <input type="text" name="e_w" class="form-control">
               <br>
               <label>Kaufkraftindex</label>
               <input type="text" name="kk_idx" class="form-control">
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary save-city" >Speichern</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
{{--
<!-- Modal -->
<div class="myImmoModal modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Immoscout24 Upload</h4>
         </div>
         <div class="modal-body">
            <form id="immoscout_form">
               <label>Eckdaten</label>
               <input type="text" name="" placeholder="PLZ" class="form-control">
               <input type="text" name="" placeholder="Ort" class="form-control">
               <input type="text" name="" placeholder="Straße" class="form-control">
               <input type="text" name="" placeholder="Nr." class="form-control">
               <label>Objektart</label>
               <select name="objektart">
                  <option value="volvo">Austellungsfläche</option>
                  <option value="saab">Einkaufszentrum</option>
                  <option value="fiat">Factory Outlet</option>
                  <option value="audi">Kaufhaus</option>
                  <option value="audi">Kiosk</option>
                  <option value="audi">Laden</option>
                  <option value="audi">SB-Markt</option>
                  <option value="audi">Verkaufsfläche</option>
                  <option value="audi">Verkaufshalle</option>
               </select>
               <br>
               <input type="text" name="" placeholder="Verkaufsfläche (m²)" class="form-control">
               <input type="text" name="" placeholder="Gesamtfläche (m²)" class="form-control">
               <input type="text" name="" placeholder="VK-fläche teilbar ab (m²)" class="form-control">
               <input type="text" name="" placeholder="Mietpreis (€/m²)" class="form-control"><br>
               <label>Kosten</label>
               <input type="text" name="" placeholder="Nebenkosten (€)" class="form-control">
               <input type="text" name="" placeholder="Kaution (€)" class="form-control"><br>
               <label>Bilder und Dokumente</label>
            <form action="{{ url('property_upload_pdf') }}" method="post" enctype="multipart/form-data">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <input type="hidden" name="property_id" id="select_property_id" value="{{ $id }}">
               <div class="form-group">
                  <label>Dokument hochladen</label>
                  <input type="file" name="upload_pdf">
               </div>
               <input type="submit" name="" value="Hochladen" class="btn btn-success">
            </form>
            <br>
            <form action="{{ url('property_upload_pdf') }}" method="post" enctype="multipart/form-data">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <input type="hidden" name="property_id" value="{{ $id }}">
               <div class="form-group">
                  <label>Bilder hochladen</label>
                  <input type="file" name="upload_pdf">
               </div>
               <br>
               <input type="submit" name="" value="Hochladen" class="btn btn-success">
            </form>
            <br>
            <label>Ausstattung</label><br>
            <label>Lageart</label>
            <select name="lageart">
               <option value="volvo">A-Lage</option>
               <option value="saab">B-Lage</option>
               <option value="fiat">Einkaufszentrum</option>
               <option value="audi">Keine Angabe</option>
            </select>
            <br>
            <input type="text" name="" placeholder="Etagenzahl" class="form-control"><br>
            <label>Bausubstanz und Energieausweis</label><br>
            <label>Objektzustand</label>
            <select name="lageart">
               <option value="volvo">Erstbezug</option>
               <option value="saab">Neuwertig</option>
               <option value="fiat">Gepflegt</option>
               <option value="audi">Modernisiert</option>
            </select>
            <input type="text" name="" placeholder="Letzte Modernisierung" class="form-control">
            <input type="text" name="" placeholder="Bauhjahr des Gebäudes" class="form-control">  <br>
            <label>Beschreibung</label>
            <input type="text" name="" placeholder="Überschrift" class="form-control">
            <input type="text" name="" placeholder="Beschreibung" class="form-control">
            <input type="text" name="" placeholder="Ausstattung" class="form-control">
            <input type="text" name="" placeholder="Lage" class="form-control">
            <input type="text" name="" placeholder="Sonstiges" class="form-control"><br>
            <label>Kontaktdaten</label>
            <input type="text" name="" placeholder="Vorname" class="form-control">
            <input type="text" name="" placeholder="Nachname" class="form-control">
            <input type="text" name="" placeholder="Telefonnummer" class="form-control">
            <input type="text" name="" placeholder="E-Mail" class="form-control">
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" >Upload</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
--}}
<div class="modal fade" id="insurance-modal-request" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-comment-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="management-modal-request" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control management-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary management-comment-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="pmanagement-modal-request" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control pmanagement-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary pmanagement-comment-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="property-management-not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <form action="{{ route('propertyManagementNotRelease') }}" id="property-management-not-release-form">
            <div class="modal-body">
               <label>Kommentar</label>
               <textarea class="form-control" name="message" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade" id="provision-not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <form action="{{ route('provision_mark_as_not_release', ['property_id' => $id]) }}" id="provision-not-release-form">
            <div class="modal-body">
               <label>Kommentar</label>
               <textarea class="form-control" name="message" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- Modal-->
<div class="modal fade" id="select-gdrive-folder-model" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <input type='hidden' name='select-gdrive-folder-type' id='select-gdrive-folder-type'>
            <input type='hidden' name='select-gdrive-folder-data' id='select-gdrive-folder-data'>
            <input type='hidden' name='select-gdrive-folder-callback' id='select-gdrive-folder-callback'>
            <a href="javascript:;" class="navbar-brand clickable hide" id="gdrive-folder-previous-button">
            <i class="fa fa-arrow-left"></i>
            <span class="hidden-xs">{{ trans('lfm.nav-back') }}</span>
            </a>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <h4 id="select-gdrive-folder-message">Select folder where to upload. </h4>
            <div id="select-gdrive-folder-content"></div>
         </div>
      </div>
      <!-- Modal content-->
   </div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="select-gdrive-file-model" role="dialog" style="z-index: 9999;">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <input type='hidden' name='workingDir' id='workingDir'>
            <input type='hidden' name='select-gdrive-file-type' id='select-gdrive-file-type'>
            <input type='hidden' name='select-gdrive-file-data' id='select-gdrive-file-data'>
            <input type='hidden' name='select-gdrive-file-callback' id='select-gdrive-file-callback'>
            <a href="javascript:;" class="navbar-brand clickable hide" id="gdrive-file-previous-button">
            <i class="fa fa-arrow-left"></i>
            <span class="hidden-xs">{{ trans('lfm.nav-back') }}</span>
            </a>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <h4>Select any file to link. </h4>
            <div id="select-gdrive-file-content"></div>
         </div>
      </div>
      <!-- Modal content-->
   </div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="viewFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Files</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <input type="hidden" id="file_id" value="0">
            <div id="view-gdrive-file-content"></div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade" id="deleteFileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Delete File</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <input type="hidden" id="file_id" value="0">
            <div id="delete-gdrive-file-content"></div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div class=" modal fade" role="dialog" id="add_new_property_comment">
   <div class="modal-dialog modal-dialog-centered " >
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Kommentare</h4>
         </div>
         <form action="{{ url('properties-comment/save') }}" method="post" id="my-comment-form">
            <div class="modal-body">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <input type="hidden"  name="property_id" value="{{$properties->id}}">
               <label>Kommentare</label>
               <textarea class="form-control p-comment" name="comment"></textarea>
               <div class="modal-footer">
                  <button type="submit" class="save-p-comment btn btn-primary" >Posten</button>
               </div>
               <br>
            </div>
         </form>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="add_recommended_comment_modal">
   <div class="modal-dialog modal-dialog-centered " >
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Kommentare</h4>
         </div>
         <form action="{{ route('add_recommended_comment') }}" id="add_recommended_comment_form">
            <div class="modal-body">
               <input type="hidden"  name="tenant_id" value="">
               <label>Kommentare</label>
               <textarea class="form-control" name="comment" required></textarea>
               <br>
            </div>
             <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Posten</button>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="add_banken_contact_employee_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Bank contact</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form action="{{ url('/addBankenContactEmployee') }}" method="post" id="add_banken_contact_employee_form">
            <div class="modal-body">
               <div class="modal-body">
                  <span id="add_banken_contact_employee_msg"></span>
                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                  <input type="hidden" id="add_banken_contact_employee_banken_id"  name="banken_id" value="">
                  <div class="form-group">
                     <label>Vorname</label>
                     <input class="form-control" name="vorname" />
                  </div>
                  <div class="form-group">
                     <label>Nachname</label>
                     <input class="form-control" name="nachname" />
                  </div>
                  <div class="form-group">
                     <label>Telefon</label>
                     <input class="form-control" name="telefon" />
                  </div>
                  <div class="form-group">
                     <label>E-mail</label>
                     <input class="form-control" name="email" />
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Posten</button>
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade" id="multiple-invoice-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control multiple-invoice-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary multiple-invoice-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<!-- Modal -->

<div class="modal fade" id="property-insurance-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control property-insurance-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary property-insurance-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="rental-activity-modal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form action="" id="rental-activity-form" data-id="">
               <div class="row">
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="rental-activity-comment" name="message" required></textarea>
                  </div>
               </div>
               <br/>
               <div class="row">
                  <div class="col-md-12 text-center">
                     <button type="submit" class="btn btn-primary">Senden</button>
                  </div>
               </div>
            </form>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <table class="table table-striped" id="rental-activity-table">
                     <thead>
                        <tr>
                           <th>Kommentar</th>
                           <th>Datum</th>
                        </tr>
                     </thead>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="provision_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form id="provision_comment_form" action="{{ route('add_provision_comment') }}">
               <div class="row">
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="provision_comment" name="comment" rows="5" required></textarea>
                  </div>
               </div>
               <br/>
               <div class="row">
                  <div class="col-md-12 text-center">
                     <button type="submit" class="btn btn-primary">Posten</button>
                  </div>
               </div>
            </form>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="provision_comments_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray">Name</th>
                          <th class="bg-light-gray">Kommentar</th>
                          <th class="bg-light-gray">Datum</th>
                          <th class="bg-light-gray">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="provision_comments_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="recommended_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form id="recommended_comment_form" action="{{ route('add_recommended_comment') }}">
               <div class="row">
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="recommended_comment" name="comment" rows="5" required></textarea>
                  </div>
               </div>
               <br/>
               <div class="row">
                  <div class="col-md-12 text-center">
                     <button type="submit" class="btn btn-primary">Posten</button>
                  </div>
               </div>
            </form>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="recommended_comments_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray">Name</th>
                          <th class="bg-light-gray">Kommentar</th>
                          <th class="bg-light-gray">Datum</th>
                          <th class="bg-light-gray">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="recommended_comments_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="insurance_detail_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form id="insurance_detail_comment_form" action="{{ route('add_property_insurance_comment') }}">
               <div class="row">
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="insurance_detail_comment" rows="5" name="comment" required></textarea>
                  </div>
               </div>
               <br/>
               <div class="row">
                  <div class="col-md-12 text-center">
                     <button type="submit" class="btn btn-primary">Posten</button>
                  </div>
               </div>
            </form>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="insurance_detail_comments_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray">Name</th>
                          <th class="bg-light-gray">Kommentar</th>
                          <th class="bg-light-gray">Datum</th>
                          <th class="bg-light-gray">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="insurance_detail_comments_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="kosten_umbau_modal" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Kosten Umbau</h4>
         </div>
         <div class="modal-body">
            <form  method="post" class="form-horizontal" id="form_kosten_umbau">
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="save_kosten_umbau">Speichern</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="insurance-not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="insurance-not-release-am-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-not-release-am-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-not-release-am-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="insurance-pending-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-pending-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-pending-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="modal-forward-to" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Weiterleiten an</h4>
      </div>
      <form id="form-forward-to" action="{{ route('mail_forward_to') }}">
         <div class="modal-body">

            <input type="hidden" name="property_id">
            <input type="hidden" name="subject">
            <input type="hidden" name="title">
            <input type="hidden" name="content">
            <input type="hidden" name="section_id">
            <input type="hidden" name="section">

            <?php 
               $all_users = DB::table('users')->select('id', 'name')->where('user_deleted', 0)->where('user_status', 1)->get();
            ?>

            <label>User</label>
            <select class="form-control" name="user" style="width: 100%;">
               <option value="">Select User</option>
               @if($all_users)
                  @foreach ($all_users as $usr)
                     <option value="{{ $usr->id }}">{{ $usr->name }}</option>
                  @endforeach
               @endif
            </select>
            <br/>
            <br/>

            <label>Oder E-Mail</label>
            <input type="text" name="email" class="form-control">
            <br/>

            <label>Kommentar</label>
            <textarea class="form-control" name="comment" rows="5" required></textarea>
            <br/>

         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-primary">Senden</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </form>
    </div>

  </div>
</div>

<div class=" modal fade" role="dialog" id="modal_sendmail_to_custom_user" style="z-index: 9999;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mail To User</h4>
         </div>
         <form action="{{ route('sendmail_to_custom_user') }}" id="form_modal_sendmail_to_custom_user">
            <input type="hidden" name="property_id">
            <input type="hidden" name="user_id">
            <input type="hidden" name="subject">
            <input type="hidden" name="content">
            <input type="hidden" name="email">
            <input type="hidden" name="mail_type">
            
            <input type="hidden" name="id">
            <input type="hidden" name="section">


            <div class="modal-body">
               <label>Nachricht</label>
               <textarea class="form-control" name="message" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="ao_cost_modal">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">ao Kosten</h4>
         </div>
         <div class="modal-body">

            <form action="{{ route('add_ao_cost', ['property_id' => $id]) }}" id="form_ao_cost">

               <div class="row">

                  <input type="hidden" name="type">

                  <div class="col-md-3">
                     <label>Frequenz</label>
                     <select class="form-control" name="frequency">
                        <option value="once">einmalig</option>
                        <option value="weekly">wöchentlich</option>
                        <option value="monthly">monatlich</option>
                        <option value="quarterly">vierteljährlich</option>
                        <option value="yearly">jährlich</option>
                     </select>
                     <!-- <input type="text" name="title" class="form-control"> -->
                  </div>
                  <div class="col-md-3">
                     <label>Notizen</label>
                     <input type="text" name="title" class="form-control">
                  </div>

                  <div class="col-md-3">
                     <label>Beträge</label>
                     <input type="text" name="amount" class="form-control mask-number-input-negetive" required>
                  </div>

                  <div class="col-md-3">
                     <label>Monat</label>
                     <select name="month" class="form-control" required>
                        @for ($i = 1; $i <= 12; $i++)
                           <option value="{{ sprintf("%02d", $i) }}">{{ sprintf("%02d", $i) }}</option>
                        @endfor
                     </select>
                  </div>

                  <div class="col-md-3">
                     <label>Jahr</label>
                     <select name="year" class="form-control" required>
                        @for ($j = 2020; $j <= date('Y', strtotime('+5 years')); $j++)
                           <option value="{{ $j }}">{{ $j }}</option>
                        @endfor
                     </select>
                  </div>

               </div>

               <div class="row">
                  <div class="col-md-12 text-center" style="padding-top: 15px;">
                     <button type="submit" class="btn btn-primary" >Speichern</button>
                  </div>
               </div>

            </form>

         </div>

         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
         
      </div>
   </div>
</div>

<div class="modal fade" id="property_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">

            <div class="row property-comment-section">
               <div class="col-md-12 form-group">
                  <label>Kommentar</label>
                  <textarea class="form-control property-comment" rows="5"></textarea>
               </div>
               <div class="col-md-12 text-center">
                  <button type="button" class="btn btn-primary btn-add-property-comment" data-reload="1" data-record-id="" data-property-id="" data-type="" data-subject="" data-content=''>Senden</button>
               </div>
            </div>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="property_comment_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray" id="th_name">Name</th>
                          <th class="bg-light-gray" id="th_comment">Kommentar</th>
                          <th class="bg-light-gray" id="th_date">Datum</th>
                          <th class="bg-light-gray" id="th_action">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="property_comment_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="tenancy-detail-modal" style="z-index: 9999;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <table class="table table-striped">
               <thead>
                  <tr>
                     <th>Name</th>
                     <th>Laufzeit</th>
                     <th>Fläche</th>
                     <th>Grundriss</th>
                  </tr>
               </thead>
               <tbody>
                  
               </tbody>
            </table>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal" id="property-status-confirm-password-modal" role="dialog" data-keyboard="false" data-backdrop="static"style="z-index: 9999">
     <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title">Bitte trage das Passwort ein</h4>
             </div>

             <form id="property-status-confirm-password-form" action="{{ route('confirm_password') }}" autocomplete="off">
                 <div class="modal-body">
                     <div class="row">
                         <div class="col-md-12">
                             <span id="property-status-confirm-password-error"></span>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-md-12">
                             <label>Passwort eintragen</label>
                             <input type="password" name="password" class="form-control" autocomplete="off" required>
                             <input type="hidden" name="check_login_password" value="1">
                         </div>
                     </div>
                 </div>
                 <div class="modal-footer">
                     <button type="submit" class="btn btn-primary">Bestätigen</button>
                     <button type="button" class="btn btn-default cancel-confirm-password" data-dismiss="modal">Abbrechen</button>
                 </div>
             </form>

         </div>
     </div>
 </div>

 <div class="modal fade" id="am-log-modal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">AM-Verlauf</h4>
         </div>
         <div class="modal-body">
            
            <div class="row">
               <div class="col-md-12">
                  <table class="table table-striped" id="am-log-table" data-file-url="{{ route('select_am_log_file') }}" data-dir-url="{{ route('select_am_log_dir') }}">
                     <thead>
                        <tr>
                           <th>AM</th>
                           <th>Übernahme des Objekts</th>
                           <th>Datei</th>
                        </tr>
                     </thead>
                  </table>
               </div>
            </div>

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="item_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">

            <div class="row item-comment-section">
               <div class="col-md-12 form-group">
                  <label>Kommentar</label>
                  <textarea class="form-control item-comment" rows="5"></textarea>
               </div>
               <div class="col-md-12 text-center">
                  <button type="button" class="btn btn-primary btn-add-item-comment">Senden</button>
               </div>
            </div>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="item_comment_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray">Name</th>
                          <th class="bg-light-gray">Kommentar</th>
                          <th class="bg-light-gray">Datum</th>
                          <th class="bg-light-gray">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="item_comment_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>


<div class="modal" id="invoice-release-am-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_am') }}" id="invoice-release-am-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-am-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-hv-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_hv') }}" id="invoice-release-hv-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-hv-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-usr-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_user') }}" id="invoice-release-usr-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-usr-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label class="am-list">User</label>
                     <select class="am-list invoice_asset_manager form-control" name="user" required>
                        <option value="">{{__('dashboard.asset_manager')}}</option>
                        @if(isset($active_users) && $active_users)
                           @foreach($active_users as $list1)
                              <option value="{{$list1->id}}">{{$list1->name}}</option>
                           @endforeach
                        @endif
                     </select>
                  </div>
               </div>

               {{-- <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div> --}}

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-falk-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_falk') }}" id="invoice-release-falk-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-falk-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>
