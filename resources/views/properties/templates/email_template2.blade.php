 
<!-- Styles -->
 <style type="text/css">
     .input_style{
    border-right: none;
    border-left: none;
    border-top: none;
    border-bottom: #000 solid 1px;
    margin-left: 20px;
     }
 </style>
 
 
<div class="row">
    @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif
    
</div>

 @if(isset($email_template2))
     <!--
<div class="row">
    <form action="{{ route('send_email_template2') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="property_id" value="{{ $id }}">
        <input type="hidden" name="bank_id" value="{{ $_GET['bank_id'] }}">




        <div class="col-sm-12">
   
    <div class="col-sm-4">
       <input type="email" placeholder="Empfänger E-Mail Adresse" class="form-control" name="email_to_send" required=""> 
    </div>
    
    <div class="col-sm-4">
       <input type="email" placeholder="Verfasser E-Mail Adresse" class="form-control" name="email_from" required="" pattern="^[A-Za-z0-9]+(.|_)+[A-Za-z0-9]+@+fcr-immobilien.de$" title=" email address allowed from @fcr-immobilien.de"> 
    </div>

    <div class="col-sm-4">
       <input type="text" placeholder="Betreff" class="form-control" name="subject" > 
    </div>

     <div class="col-sm-8">
        <br>
        <textarea class="form-control" name="message" placeholder="Nachricht"></textarea>
     </div>

    <div class="col-sm-4">
        <br>
       <input type="submit" class="btn btn-success btn-block" value="Angebot senden" > 
    </div>

    </div>

   </form> 
</div> -->
<br> 

@if($email_template2->email_status == 1)
 
<div class="alert alert-success col-md-3">
    Email already sent
</div>
 
@endif
@endif

<div class="white-box">
 
<form action="{{ route('email_template2_save') }}" method="post"  enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
     <input type="hidden" name="property_id" value="{{ $id }}">
    <input type="hidden" name="bank_id" value="{{ $_GET['bank_id'] }}">


    <br><br><br>
<div class="row">
    <?php
        if(isset($email_template2) &&  !is_null($email_template2->input1) )
        {
            echo $fullName[0];
            $fullName = explode(' || ', $email_template2->input1);

            if(isset($fullName[0]) && $fullName[0] != ""){
                ?>

                    Sehr geehrter <input type="text" class="input_style" value="<?php echo $fullName[0]; ?>" name="input1" required="">,
                <?php
            }

        }
    
        else{

        ?>
            Sehr geehrter <input type="text" class="input_style" name="input1" required="">,

    <?php
        }
    ?>
</div>

<br><br><br><br><br> 

 <div class="row">

    Gerne würden wir bei Ihnen eine Finanzierung für ein Einzelhandelsobjekt in <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input2 }} @endif" name="input2" required=""> anfragen. <br><br>
 
In <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input3 }} @endif" name="input3" required="">  werden wir einen Netto-Markt kaufen. <br><br>
 
Bei dem Objekt in  <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input4 }} @endif" name="input4" required="">  handelt es sich um ein im Jahr  <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input5 }} @endif" name="input5" required="">  errichtetes  <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input6 }} @endif" name="input6" required="">. Das Objekt befindet sich in einem gepflegten Zustand. Der Mietvertrag mit <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input7 }} @endif" name="input7" required=""> läuft noch bis zum <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input8 }} @endif" name="input8" required="">. <input type="text" value="@if(isset($email_template2)){{ $email_template2->input9 }} @endif" class="input_style" name="input9" style="width:90%; margin-left: 5px;" required=""><br><br>
Für den Objekterwerb stellen wir uns eine Non-Recourse Finanzierung vor. Wir werden für den Erwerb der Immobilie die Objektgesellschaft  <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input10 }} @endif" name="input10" required="">  gründen. Dabei würden wir <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input11 }} @endif" name="input11" required=""> Eigenkapital mitbringen und einen Fremdkapitalanteil in Höhe von <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input12 }} @endif" name="input12" required=""> bei Ihnen erbeten. Wir könnten uns einen variablen Zinssatz von  <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input13 }} @endif" name="input13" required=""> und eine Tilgung von  <input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input14 }} @endif" name="input14" required="">  vorstellen, sind aber offen für Gegenvorschläge Ihrerseits. <br><br>
 
Damit Sie zunächst einen ersten Überblick über unsere Gesellschaft und unser Vorhaben erhalten, übersende ich Ihnen in der Anlage nachfolgende Unterlagen der FCR Immobilien AG sowie die zugehörigen Objektunterlagen:
•   Jahresabschlüsse 2017
•   Halbjahresbericht 2018
•   Geschäftsbericht 2017
•   Exposé, Planungsspiegel und Mieterliste des Objektes<br><br>
 
Falls Sie weitere Informationen oder Unterlagen benötigen, stehe ich Ihnen gerne zur Verfügung. <br>
 
Es würde mich freuen, wenn Ihnen das Objekt zusagen würden und wir dazu kurzfristig Rücksprache halten könnten. <br><br>
 
Mit freundlichen Grüßen, <br><br>

Mit freundlichen Grüßen<br><br>

<input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input15 }} @endif" name="input15" required=""><br>
<input type="text" class="input_style" value="@if(isset($email_template2)){{ $email_template2->input16 }} @endif" name="input16" required="">
 
     
 </div>
 
<center>
    <button type="submit" class="btn btn-success" type="submit" style="padding: 10px 50px;" >Speichern</button>
</center>
</form>
</div>
 
 