

<style>
  .new-height{
  height: 100px !important;
  }
</style>

<div id="response"></div>

<form class="vacant-form" enctype="multipart/form-data">
 
        <div class="modal-content">
            <div class="modal-body">


                  <?php
                  $property_id =  Request::segment(2);
                  
                  $title = $description = $size = $price = $category= "";
                  $iarr = $iarr1 = $pdfs = $planpdf = array();
                  $imageid = $pdfid = $planpdfid =  0;

                  // $size = $item->vacancy_in_qm;
                  // $price = $item->vacancy_in_eur;
                  // $category = $item->use;
                  $vcid = 0;
                  if(isset($vrow) && $vrow)
                  {
                    $title = $vrow->title;
                    $description = $vrow->description;
                    $vcid = $vrow->id;
                    $category = $vrow->category;
                    $size = $vrow->size;
                    $price = $vrow->price;
                    if($vrow->image)
                      $iarr = explode(',', $vrow->image);
                    
                    $iarr1 = $vrow->favourite;

                    $pdfid = $vrow->id;
                    $planpdfid = $vrow->id;


                    $pdfs = $vrow->pdf;
                    $planpdf = $vrow->planpdf;

                  }



                  ?>
                  <!-- {{ini_get('post_max_size')}} -->

                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" class="vacant_id" name="vacant_id" value="{{$vcid}}" />
                    <input type="hidden" name="property_id" value="{{ $property_id }}" />

                    <label>Title</label>
                    <input type="text" value="{{$title}}" name="title" class="form-control" placeholder="" >
                    <br>

                    <label>Beschreibung</label>
                    <textarea class="form-control" name="description">{{$description}}</textarea>
                    <br>
                    
                     <label>Kategorie</label>
                    <select name="category[]" class="form-control multi-category" multiple="multiple" required>

                      <?php
                      $categories = array('Einzelhandel','Büro/Praxen','Wohnungen','Gastronomie','Lager','Stellplätze','Sonstiges');

                      if($category)
                      {
                        $cat_array = explode(',', $category);
                      }




                      ?>


                      @foreach($categories as $cat)
                      <option value="{{$cat}}" @if($cat_array && in_array($cat,$cat_array)) selected @endif >{{$cat}}</option>
                      @endforeach
                    

                    </select><br>

                                        
                    
                    

                    <label>Verkaufsfläche</label>
                    <input type="retail_space" value="{{ $size  }}" name="size" class="form-control" placeholder="Verkaufsfläche">

                    <label>Preis pro m²</label>
                    <input type="rental_price" value="{{ $price  }}" name="price" class="form-control" placeholder="Mietpreis" required>


                    <label>Bilder</label>
                    <input type="file" name="image" class="vacant-image">

                    @if($iarr)
                    <div class="img-preview-div im" >
                    @foreach($iarr as $imageid=>$images)
                    <div class="creating-ads-img-wrap media-common-class" >
                        <input type="hidden" name="images[]" value="{{$images}}">
                        <img src="{{ asset('ad_files_upload/'.$images) }}" class="img-responsive" />
                        <div class="img-action-wrap" data-id="{{ $imageid }}" data-item-id="{{$vcid}}" data-name="{{$images}}">
                            <a href="javascript:void(0);" class="deletevacantimage"><i class="fa fa-trash-o"></i> </a>
                            @if($iarr1==$imageid)
                            <a href="javascript:void(0);" class="setcustomimagefavourite" data-id="{{ $imageid }}"><i class="fa fa-star"></i></a>
                            @else
                            <a href="javascript:;" class="setcustomimagefavourite" data-id="{{ $imageid }}"><i class="fa fa-star-o"></i></a>
                            @endif
                        </div>
                    </div>
                    @endforeach
                    </div>
                    @endif

                    <div class="clearfix"></div>
                    <br>
                    <label>Leerstandsflächenexposé</label>
                    <input type="file" name="pdf" class="vacant-pdf">

                    @if($pdfs)
                    <div class="pdf-preview-div pd">
                    <div class="creating-ads-img-wrap media-common-class">
                        <input type="hidden" name="pdfs[]" value="{{$pdfs}}">
                        <a href="{{ asset('ad_files_upload/'.$pdfs) }}" class="img-responsive" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
                        <div class="img-action-wrap" id="{{ $pdfid }}">
                            <a href="javascript:void(0);" class="deletevacantimage"><i class="fa fa-trash-o"></i> </a>
                        </div>
                    </div>
                  </div>
                    @endif

                  <div class="clearfix"></div>

                  <br>

                  <label>Grundriss</label>
                    <input type="file" name="planpdf" class="vacant-pdf">

                    @if($planpdf)
                    <div class="pdf-preview-div planpd">
                    <div class="creating-ads-img-wrap media-common-class">
                        <input type="hidden" name="planpdfs" value="{{$planpdf}}">
                        <a href="{{ asset('ad_files_upload/'.$planpdf) }}" class="img-responsive" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
                        <div class="img-action-wrap" id="{{ $planpdfid }}">
                            <a href="javascript:void(0);" class="deletevacantimage"><i class="fa fa-trash-o"></i> </a>
                        </div>
                    </div>
                  </div>
                    @endif
                    

                    
            </div>
 

            <div class="modal-footer" style="clear: both;">
                <button type="submit" class="btn btn-primary save-vacant" data-upload="1" >Upload<i class="fa fa-circle-o-notch fa-spin immo_loader hide" style="font-size:14px"></i></button>
                <button type="submit" class="btn btn-primary save-vacant" data-upload="0" >Speichern <i class="fa fa-circle-o-notch fa-spin immo_loader hide" style="font-size:14px"></i></button>
            </div>

            </form>

            <!-- <button type="button" class="btn btn-primary vermi-upload">Veröffentlichen</button> -->
        </div>
 