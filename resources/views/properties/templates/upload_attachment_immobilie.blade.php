<?php

$property_id =  Request::segment(2);
$realsate = DB::table('property_images')->where('property_id', $property_id)->orderBy('id', 'desc')->limit(1)->first();
if(!is_null($realsate)){

    $realstate_id = $realsate->realstate_id;
}

?>

<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Immoscout24 attachment Upload</h4>
    </div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <form id="upload_attachment_images" action="#" method="post" enctype="multipart/form-data">
                <input type="file" class="form-control" name="image_upload[]" multiple><br><br>
                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                <input type="hidden" name="property_id" value="{{ $property_id }}">
                <button type="submit" class="btn btn-primary" >Upload <i class="fa fa-circle-o-notch fa-spin immo_loader hide" style="font-size:14px"></i></button>

            </form>
        </div>
        <div class="col-md-12" style="margin-top: 25px;">
            <table class="table">
                <tr>
                    <th style="width: 90%;">Images</th>
                    <th>Action</th>
                </tr>
                @if(isset($api_atachment_imgs) && count($api_atachment_imgs) > 0)
                @foreach($api_atachment_imgs as $api_imgs)
                <tr id="del_row_{{ $api_imgs->id }}">
                    <td style="width: 90%;"><img src="{{ asset('/property_images').'/'.$api_imgs->image }}" height="100" width="120"></td>
                    <td>
                        <form method="post" class="image_del">
                            <input type="hidden" value="{{ csrf_token() }}" name="_token">
                            <input type="hidden" name="property_id" value="{{ $property_id }}">
                            <input type="hidden" class="image_row" name="image_row" value="del_row_{{ $api_imgs->id }}">
                            <input type="hidden" name="image_id" value="{{ $api_imgs->id }}">
                            <button type="submit"><i class="fa fa-trash"></i></button>
                      </form>
                    </td>
                </tr>
                @endforeach
                @endif
            </table>
        </div>
    </div>
</div>
</div>