<div class="row">
	<div class="col-md-12">
		<span id="contracts_msg"></span>
	</div>
</div>
<div class="row white-box">
	<div class="col-md-12">
		<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add_contracts_modal">
			Neue Verträge
		</button>
	</div>

	<br/><br/>

	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Offene Verträge</h3>
		<div class="table-responsive">
			<table class="table table-striped" id="contracts_table" style="width: 100%;padding-left: 0px;">
				<thead>
					<tr>
						<th>#</th>
						<th>Datei</th>
						<th>Betrag</th>
						<th>Startdatum</th>
						<th>Abschluss am</th>
						<th>Kündigung spätestens</th>
						<th>Kündigung am</th>
						<th>Kommentar</th>
						<th>User</th>
						<th>Datum</th>
						<th>Kategorie</th>
						{{-- <th>Kommentar Falk</th> --}}
						<th>Freigabe Falk</th>
						<th>Ablehnen AM</th>
						<th>Ablehnen</th>
						<th>Pending</th>
						<th style="min-width: 65px;">Aktion</th>
						<th>Weiterleiten an</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	

	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Freigegeben Verträge</h3>
		<div class="table-responsive">
			<table class="table table-striped" id="release_contracts_table" style="width: 100%;padding-left: 0px;">
				<thead>
					<tr>
						<th>#</th>
						<th>Datei</th>
						<th>Betrag</th>
						<th>Startdatum</th>
						<th>Abschluss am</th>
						<th>Kündigung spätestens</th>
						<th>Kündigung am</th>
						<th>Kommentar Verträge</th>
						<th>User</th>
						<th>Datum</th>
						<th>Kategorie</th>
						{{-- <th>Kommentar Falk</th> --}}
						<th>Weiterleiten an</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Pending Verträge</h3>
		<div class="table-responsive">
			<table class="table table-striped" id="pending_contracts_table" style="width: 100%;padding-left: 0px;">
				<thead>
					<tr>
						<th>#</th>
						<th>Datei</th>
						<th>Betrag</th>
						<th>Startdatum</th>
						<th>Abschluss am</th>
						<th>Kündigung spätestens</th>
						<th>Kündigung am</th>
						<th>Kommentar Verträge</th>
						<th>User</th>
						<th>Datum</th>
						<th>Kategorie</th>
						{{-- <th>Kommentar Falk</th> --}}
						<th>Weiterleiten an</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Existierende Altverträge</h3>
		<div class="table-responsive">
			<table class="table table-striped" id="old_contracts_table" style="width: 100%;padding-left: 0px;">
				<thead>
					<tr>
						<th>#</th>
						<th>Datei</th>
						<th>Betrag</th>
						<th>Startdatum</th>
						<th>Abschluss am</th>
						<th>Kündigung spätestens</th>
						<th>Kündigung am</th>
						<th>Kommentar Verträge</th>
						<th>User</th>
						<th>Datum</th>
						<th>Kategorie</th>
						<th style="min-width: 65px;">Aktion</th>
						<th>Weiterleiten an</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Nicht Freigegeben Verträge (AM)</h3>
		<div class="table-responsive">
			<table class="table table-striped" id="not_release_am_contracts_table" style="width: 100%;padding-left: 0px;">
				<thead>
					<tr>
						<th>#</th>
						<th>Datei</th>
						<th>Betrag</th>
						<th>Startdatum</th>
						<th>Abschluss am</th>
						<th>Kündigung spätestens</th>
						<th>Kündigung am</th>
						<th>Kommentar Verträge</th>
						<th>User</th>
						<th>Datum</th>
						<th>Kategorie</th>
						{{-- <th>Kommentar Falk</th> --}}
						<th>Weiterleiten an</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	
	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Nicht Freigegeben Verträge (Falk)</h3>
		<div class="table-responsive">
			<table class="table table-striped" id="not_release_contracts_table" style="width: 100%;padding-left: 0px;">
				<thead>
					<tr>
						<th>#</th>
						<th>Datei</th>
						<th>Betrag</th>
						<th>Startdatum</th>
						<th>Abschluss am</th>
						<th>Kündigung spätestens</th>
						<th>Kündigung am</th>
						<th>Kommentar Verträge</th>
						<th>User</th>
						<th>Datum</th>
						<th>Kategorie</th>
						{{-- <th>Kommentar Falk</th> --}}
						<th>Weiterleiten an</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>


	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Verlauf</h3>
		<div class="table-responsive">
		<table class="table table-striped" id="contracts_mail_table">
    		<thead>
		      	<tr>
		        	<th class="">Name</th>
		        	<th class="">Button</th>
		        	<th>Datei</th>
		        	<th class="">Kommentar</th>
		        	<th class="">Datum</th>
		        	<th>Kündigung spätestens</th>
		        	<th>Aktion</th>
		      	</tr>
    		</thead>
    		<tbody></tbody>
  		</table>
  	</div>
	</div>
</div>



<div class=" modal fade" role="dialog" id="add_contracts_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Neue Verträge</h4>
			</div>
			<form method="post" enctype="multipart/form-data" id="add_contracts_form">
				<div class="modal-body">

					<span id="add_contracts_msg"></span>

					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="id" value="">
					<input type="hidden" name="property_id" value="{{$properties->id}}">

					<div id="contract_file_div">
						<input type="hidden" name="file_basename" id="contracts_gdrive_file_basename" value="">
						<input type="hidden" name="file_dirname" id="contracts_gdrive_file_dirname" value="">
						<input type="hidden" name="file_type" id="contracts_gdrive_file_type" value="">
						<input type="hidden" name="file_name" id="contracts_gdrive_file_name" value="">
						<input type="hidden" name="current_tab_name" id="current_tab_name" value="Verträge">

						<label>Datei</label> <span id="contracts_gdrive_file_name_span"> </span>
						<a href="javascript:void(0);" class="link-button-gdrive-contracts btn btn-info">Datei auswählen</a>


	                    <a class="contracts-upload-file-icon btn btn-info" href="javascript:void(0);">Hochladen</a>
	                    <input type="file" name="contracts_gdrive_file_upload" class="hide contracts-deal-gdrive-upload-file-control" id="contracts_gdrive_file_upload" >
						<br><br>
					</div>

					<label>Betrag</label>
					<input type="text" name="amount" class="form-control mask-input-number" required>
					<br>

					<label>Startdatum</label>
					<input type="text" name="date" class="form-control mask-input-new-date" placeholder="dd.mm.yyyy" required>
					<br>

					<label>Abschluss am</label>
					<input type="text" name="completion_date" class="form-control mask-input-new-date" placeholder="dd.mm.yyyy">
					<br>

					<label>Kündigung spätestens</label>
					<input type="text" name="termination_date" class="form-control mask-input-new-date" placeholder="dd.mm.yyyy">
					<br>

					<label>Kündigung am</label>
					<input type="text" name="termination_am" class="form-control mask-input-new-date" placeholder="dd.mm.yyyy">
					<br>

					<label>Kategorie</label>
					<select class="form-control" name="category">
						<option value="">Select</option>
						<option value="Gebäudeversicherung">Gebäudeversicherung</option>
						<option value="Haftpflichtversicherung">Haftpflichtversicherung</option>
						<option value="Hausmeister">Hausmeister</option>
						<option value="Gartenpflege">Gartenpflege</option>
						<option value="Sicherheitsheitsdienst">Sicherheitsheitsdienst</option>
						<option value="Reinigungsdienst">Reinigungsdienst</option>
						<option value="Dienstleister">Dienstleister</option>
						<option value="Sonstiges">Sonstiges</option>
					</select>
					<br>
					
					<div id="contract_comment_div">
						<label>Kommentar</label>
						<textarea class="form-control" name="comment"></textarea>
						<br>
					</div>

					<input type="checkbox" name="is_old" id="is_old">
					<label for="is_old">Altvertag</label>
					<br>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Speichern</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>

<div class=" modal fade" role="dialog" id="contracts_mark_as_not_release">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<form method="post" id="form_contracts_mark_as_not_release">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<span id="contracts_mark_as_not_release_error"></span>
						</div>
						<div class="col-md-12">
							<label>Kommentar</label>
							<textarea class="form-control" id="input_contracts_mark_as_not_release" name="comment" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Senden</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>

<div class=" modal fade" role="dialog" id="contracts_mark_as_not_release_am">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<form method="post" id="form_contracts_mark_as_not_release_am">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<span id="contracts_mark_as_not_release_am_error"></span>
						</div>
						<div class="col-md-12">
							<label>Kommentar</label>
							<textarea class="form-control" id="input_contracts_mark_as_not_release_am" name="comment" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Senden</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>

<div class=" modal fade" role="dialog" id="contracts_mark_as_pending">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<form method="post" id="form_contract_mark_as_pending">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<span id="contracts_mark_as_pending_error"></span>
						</div>
						<div class="col-md-12">
							<label>Kommentar</label>
							<textarea class="form-control" id="input_contracts_mark_as_pending" name="comment" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Senden</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>

<div class="modal fade" id="contract-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control contract-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary contract-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>