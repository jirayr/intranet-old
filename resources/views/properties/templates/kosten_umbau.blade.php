{{ csrf_field() }}
<input type="hidden" name="property_id" value="{{ $property_id }}">
<input type="hidden" name="tenant_id" value="{{ $tenant_id }}">
<table class="table">
  <?php $total = 0; ?>
  <tbody class="recommended-list">

    @if($data->count())

      @foreach($data as $k => $list)
        <?php $total +=$list->amount; ?>
        <tr>
          <td>
            <input type="text" name="name[]" class="form-control" value="{{$list->name}}" placeholder="z.B. Küche">
              <input type="hidden" name="empfehlung_id[]" value="{{$list->id}}" >
          </td>
          <td>
            <input type="text" name="amount[]" class="form-control mask-number-input-nodecimal" value="{{number_format($list->amount,2,',','.')}}">
          </td>
          <td>
            <div class="uploaded-files uploaded-files-empfehlung row-{{$list->id}}" style="float:left">

              <a href="javascript:void(0);" class="link-button-empfehlung"  id="empfehlung_file_link_{{$list->id}}" data-empfehlung_id="{{$list->id}}" data-property_id="{{$list->property_id}}" data-tenant_id="{{$list->tenant_id}}" >
                <i class="fa fa-link"></i>
              </a>

              @if( isset($list->files) )
                @foreach($list->files as $file)
                  @if($file['file_type'] == 'file')
                    <a href="{{ $file['file_href'] }}"  target="_blank" title="{{ $file['file_name'] }}">
                      <i extension="filemanager.file_icon_array.{{$file['extension']}}" class="fa {{ config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file' }}" ></i>
                    </a>
                  @else
                    <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  id="empfehlung-gdrive-link-{{ $file['id'] }}"  ><i class="fa fa-folder" ></i></a>
                  @endif
                @endforeach
              @endif 
            </div>

            <a class="listFilesForDelete" data-property_id="{{$list->property_id}}" data-id="{{$list->id}}" data-type="empfehlung">
              <i class="fa fa-trash iconstyle"></i>
            </a>

          </td>
          <td>
            @if($k==0)
              <button type="button" class="btn-success btn-sm add-row-rec">+</button>
            @else
              <button type="button" class="btn-danger btn-sm remove-mieter">-</button>
            @endif
          </td>
        </tr>
      @endforeach

    @else

      <tr>
        <td>
          <input type="text" name="name[]" class="form-control" placeholder="z.B. Küche">
          <input type="hidden" name="empfehlung_id[]" value="0" >
        </td>
        <td>
          <input type="text" name="amount[]" class="form-control mask-number-input-nodecimal">
        </td>
        <td>
          <button type="button" class="btn-success btn-sm add-row-rec">+</button>
        </td>
      </tr>

    @endif

    <tr class="gesamt_row">
      <th>Gesamt</th>
      <th class="pl-20">{{number_format($total,2,',','.')}}</th>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>