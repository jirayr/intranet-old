<div class="row">
	<div class="col-sm-12 table-responsive white-box">
	<table class="table table-striped" id="inquiries">
			<thead>
				<tr>
					<th>Name</th>
					<th>E-Mail</th>
					<th>Telefonnummer</th>
					<th>Nachricht</th>
					<th>Datum</th>
				</tr>
			</thead>
			<tbody>
				@foreach($contact_queries as $list)
				<tr>
					<td>{{$list->name}}</td>
					<td>{{$list->email}}</td>
					<td>{{$list->phone}}</td>
					<td>{{$list->message}}</td>
					<td>{{show_date_format($list->created_at)}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>