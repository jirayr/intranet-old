@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <style type="text/css">
      .change-in-user{
        width: 80px !important;
      }
      
    </style>
@endsection
<div id="tenancy-schedule">
    <h1>Leerstandsflächen</h1>
<?php
// print_r($properties);
$name = $properties->name_of_property;
$seller_id = $properties->seller_id;
$ireleased_by = "";
$isale_date = "";
$ireleased_by2 = "";
$isale_date2 = "";
$street = "";
$postcode = "";
$city = "";

if($pb_data && $pb_data->property_name)
  $name = $pb_data->property_name;

$seller_id = $properties->transaction_m_id;
if($pb_data && $pb_data->saller_id)
  $seller_id = $pb_data->saller_id;



$street = $properties->strasse;
if($pb_data && $pb_data->street)
  $street = $pb_data->street;

if($pb_data && $pb_data->city)
  $city = $pb_data->city;

if($pb_data && $pb_data->postcode)
  $postcode = $pb_data->postcode;

if($pb_data && $pb_data->released_by)
  $ireleased_by = $pb_data->released_by;

if($pb_data && $pb_data->sale_date)
  $isale_date = $pb_data->sale_date;


if($pb_data && $pb_data->released_by2)
  $ireleased_by2 = $pb_data->released_by2;

if($pb_data && $pb_data->sale_date2)
  $isale_date2 = $pb_data->sale_date2;


?>

<input type="hidden" class="property_id" value="<?php echo $id?>">
<div class="row tenancy-schedules mbuy-sheet" style="margin-top: 20px;">
  <div class="col-sm-12">
    
    <h3 class="title-tag">Einkaufsdatenblatt 
      @if(Auth::user()->email!=config('users.falk_email'))
        <button style="color: #fff;width: 170px;font-size: 16px;margin-left: 10px;" type="button" class=" pull-right btn @if($isale_date && $ireleased_by) btn-success @else btn-primary @endif isend-mail">@if($isale_date && $ireleased_by) Freigegeben  @else Zur Freigabe senden @endif</button>
        @endif

        @if(Auth::user()->email==config('users.falk_email'))
          <button style="color: #fff;width: 170px;font-size: 16px;margin-left: 10px;" type="button" class="pull-right btn @if($isale_date && $ireleased_by) btn-success @else btn-primary @endif irelease-property" data-toggle="modal" data-target="#irelease-property">@if($isale_date && $ireleased_by) Freigegeben  @else Freigeben @endif</button>
        @endif

        <a href="javascript:void(0)" data-class="sheet-1" class="btn btn-info show-link pull-right"><i class="fa fa-angle-down"></i></a>
    </h3>

    <div class="sheet-1 hidden">


    <h4>Checkliste</h4>
    Objekt
    <a href="#" class="inline-edit" data-type="text" data-pk="property_name" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="property_name">{{ $name }}</a>
    <br>
      <table class="forecast-table" style="margin-top: 10px;">
      <thead>
        <tr>
          <th class="border bg-gray">PLZ</th>
          <th class="border bg-gray">Stadt</th>
          <th class="border bg-gray">Straße, Nr.</th>
          <th class="border bg-gray">Einkäufer</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="border">{{ $properties->plz_ort }}</td>
          <td class="border">{{ $properties->ort }}</td>
          <td class="border">{{ $street }}</td>
          <td class="border"><select class="change-in-user">
                <option value="">{{__('dashboard.transaction_manager')}}</option>
                @foreach($tr_users as $list)
                @if($seller_id==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->name}}</option>
                @else
                <option value="{{$list->id}}">{{$list->name}}</option>
                @endif
                @endforeach
              
              </select></td>
        </tr>
      </tbody>
    </table>


    <table class="forecast-table" style="margin-top: 20px;">
      <thead>
        <tr>
          <th class="border checkbox-td bg-gray" colspan="3" >Mietunterlagen</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="1" 
            @if(isset($buy_data[1]['status']) &&  $buy_data[1]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Mieterbestandsliste mit MV-Laufzeiten, m² (NF, LF) und akt. Mieteinnahmen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="1" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[1]['user1']) &&  $buy_data[1]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="1" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[1]['user2']) &&  $buy_data[1]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="1" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[1]['user3']) &&  $buy_data[1]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1') }}" data-title="Kommentar">{{ @$buy_data[1]['comment'] }}</a>


          </td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="2" @if(isset($buy_data[2]['status']) &&  $buy_data[2]['status']) checked @endif ></td>
          <td class="border" colspan="2">Telefonat / pers. Gespräch mit dem Expansionsleiter / Mieter</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="2" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[2]['user1']) &&  $buy_data[2]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="2" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[2]['user2']) &&  $buy_data[2]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="2" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[2]['user3']) &&  $buy_data[2]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
           <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/2') }}" data-title="Kommentar">{{ @$buy_data[2]['comment'] }}</a>


          </td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="3" @if(isset($buy_data[3]['status']) &&  $buy_data[3]['status']) checked @endif ></td>
          <td class="border" colspan="2">Mietverträge inkl. Nachträge</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="3" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[3]['user1']) &&  $buy_data[3]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="3" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[3]['user2']) &&  $buy_data[3]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="3" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[3]['user3']) &&  $buy_data[3]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/3') }}" data-title="Kommentar">{{ @$buy_data[3]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="4" @if(isset($buy_data[4]['status']) &&  $buy_data[4]['status']) checked @endif ></td>
          <td class="border">Laufzeiten</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="4" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[4]['user1']) &&  $buy_data[4]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="4" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[4]['user2']) &&  $buy_data[4]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="4" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[4]['user3']) &&  $buy_data[4]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/4') }}" data-title="Kommentar">{{ @$buy_data[4]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="5" @if(isset($buy_data[5]['status']) &&  $buy_data[5]['status']) checked @endif ></td>
          <td class="border">Mieteinnahmen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="5" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[5]['user1']) &&  $buy_data[5]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="5" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[5]['user2']) &&  $buy_data[5]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="5" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[5]['user3']) &&  $buy_data[5]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/5') }}" data-title="Kommentar">{{ @$buy_data[5]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="6" @if(isset($buy_data[6]['status']) &&  $buy_data[6]['status']) checked @endif ></td>
          <td class="border">Overrent / Underrent</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="6" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[6]['user1']) &&  $buy_data[6]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="6" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[6]['user2']) &&  $buy_data[6]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="6" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[6]['user3']) &&  $buy_data[6]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/6') }}" data-title="Kommentar">{{ @$buy_data[6]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="7" @if(isset($buy_data[7]['status']) &&  $buy_data[7]['status']) checked @endif ></td>
          <td class="border">Formfehler</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="7" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[7]['user1']) &&  $buy_data[7]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="7" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[7]['user2']) &&  $buy_data[7]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="7" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[7]['user3']) &&  $buy_data[7]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/7') }}" data-title="Kommentar">{{ @$buy_data[7]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="8" @if(isset($buy_data[8]['status']) &&  $buy_data[8]['status']) checked @endif ></td>
          <td class="border">Notaranderkonto / Anzahlung (5% / 10% / 15%)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="8" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[8]['user1']) &&  $buy_data[8]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="8" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[8]['user2']) &&  $buy_data[8]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="8" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[8]['user3']) &&  $buy_data[8]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/8') }}" data-title="Kommentar">{{ @$buy_data[8]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="9" @if(isset($buy_data[9]['status']) &&  $buy_data[9]['status']) checked @endif ></td>
          <td class="border">Sonderkündigungsrechte</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="9" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[9]['user1']) &&  $buy_data[9]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="9" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[9]['user2']) &&  $buy_data[9]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="9" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[9]['user3']) &&  $buy_data[9]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/9') }}" data-title="Kommentar">{{ @$buy_data[9]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="10" @if(isset($buy_data[10]['status']) &&  $buy_data[10]['status']) checked @endif ></td>
          <td class="border">Garantien</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="10" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[10]['user1']) &&  $buy_data[10]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="10" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[10]['user2']) &&  $buy_data[10]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="10" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[10]['user3']) &&  $buy_data[10]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/10') }}" data-title="Kommentar">{{ @$buy_data[10]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="11" @if(isset($buy_data[11]['status']) &&  $buy_data[11]['status']) checked @endif ></td>
          <td class="border">Konkurrenzschutz</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="11" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[11]['user1']) &&  $buy_data[11]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="11" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[11]['user2']) &&  $buy_data[11]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="11" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[11]['user3']) &&  $buy_data[11]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/11') }}" data-title="Kommentar">{{ @$buy_data[11]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="12" @if(isset($buy_data[12]['status']) &&  $buy_data[12]['status']) checked @endif ></td>
          <td class="border">Rechtsstreitigkeiten</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="12" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[12]['user1']) &&  $buy_data[12]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="12" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[12]['user2']) &&  $buy_data[12]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="12" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[12]['user3']) &&  $buy_data[12]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/12') }}" data-title="Kommentar">{{ @$buy_data[12]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="13" @if(isset($buy_data[13]['status']) &&  $buy_data[13]['status']) checked @endif ></td>
          <td class="border">Sonstiges</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="13" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[13]['user1']) &&  $buy_data[13]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="13" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[13]['user2']) &&  $buy_data[13]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="13" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[13]['user3']) &&  $buy_data[13]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/13') }}" data-title="Kommentar">{{ @$buy_data[13]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="14" @if(isset($buy_data[14]['status']) &&  $buy_data[14]['status']) checked @endif ></td>
          <td class="border" colspan="2">Dauermietrechnung(en)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="14" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[14]['user1']) &&  $buy_data[14]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="14" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[14]['user2']) &&  $buy_data[14]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="14" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[14]['user3']) &&  $buy_data[14]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/14') }}" data-title="Kommentar">{{ @$buy_data[14]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="15" @if(isset($buy_data[15]['status']) &&  $buy_data[15]['status']) checked @endif ></td>
          <td class="border" colspan="2">Kontoauszug der letzten 6 Monate pro Mieter</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="15" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[15]['user1']) &&  $buy_data[15]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="15" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[15]['user2']) &&  $buy_data[15]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="15" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[15]['user3']) &&  $buy_data[15]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/15') }}" data-title="Kommentar">{{ @$buy_data[15]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="16" @if(isset($buy_data[16]['status']) &&  $buy_data[16]['status']) checked @endif ></td>
          <td class="border" colspan="2">Kündigungsschreiben der Mieter, falls notwendig</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="16" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[16]['user1']) &&  $buy_data[16]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="16" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[16]['user2']) &&  $buy_data[16]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="16" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[16]['user3']) &&  $buy_data[16]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/16') }}" data-title="Kommentar">{{ @$buy_data[16]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="17" @if(isset($buy_data[17]['status']) &&  $buy_data[17]['status']) checked @endif ></td>
          <td class="border" colspan="2">OPOS / Mietrückstandsliste</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="17" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[17]['user1']) &&  $buy_data[17]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="17" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[17]['user2']) &&  $buy_data[17]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="17" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[17]['user3']) &&  $buy_data[17]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/17') }}" data-title="Kommentar">{{ @$buy_data[17]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="18" @if(isset($buy_data[18]['status']) &&  $buy_data[18]['status']) checked @endif ></td>
          <td class="border" colspan="2">Höhe der nicht umglagefähigen Nebenkosten <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/18') }}" data-title="Kommentar">{{ @$buy_data[18]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="18" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[18]['user1']) &&  $buy_data[18]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="18" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[18]['user2']) &&  $buy_data[18]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="18" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[18]['user3']) &&  $buy_data[18]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/18') }}" data-title="Kommentar">{{ @$buy_data[18]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="19" @if(isset($buy_data[19]['status']) &&  $buy_data[19]['status']) checked @endif ></td>
          <td class="border" colspan="2">Nebenkostenabrechnung der letzten 1-3 Jahre (mind. 1 Jahr!)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="19" column-name="user1">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[19]['user1']) &&  $buy_data[19]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="19" column-name="user2">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[19]['user2']) &&  $buy_data[19]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="19" column-name="user3">
                <option value="">	</option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[19]['user3']) &&  $buy_data[19]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/19') }}" data-title="Kommentar">{{ @$buy_data[19]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="20" @if(isset($buy_data[20]['status']) &&  $buy_data[20]['status']) checked @endif ></td>
          <td class="border" colspan="2">Mietsicherheiten, falls vorhanden</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="20" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[20]['user1']) &&  $buy_data[20]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="20" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[20]['user2']) &&  $buy_data[20]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="20" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[20]['user3']) &&  $buy_data[20]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/20') }}" data-title="Kommentar">{{ @$buy_data[20]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="21" @if(isset($buy_data[21]['status']) &&  $buy_data[21]['status']) checked @endif ></td>
          <td class="border" colspan="2">Übergabeprotokolle</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="21" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[21]['user1']) &&  $buy_data[21]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="21" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[21]['user2']) &&  $buy_data[21]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="21" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[21]['user3']) &&  $buy_data[21]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/21') }}" data-title="Kommentar">{{ @$buy_data[21]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="22" @if(isset($buy_data[22]['status']) &&  $buy_data[22]['status']) checked @endif ></td>
          <td class="border" colspan="2">Mieterkorrespondenz, falls vorhanden und wichtig</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="22" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[22]['user1']) &&  $buy_data[22]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="22" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[22]['user2']) &&  $buy_data[22]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="22" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[22]['user3']) &&  $buy_data[22]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/22') }}" data-title="Kommentar">{{ @$buy_data[22]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
        </tr>
        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
        </tr>

        <tr>
          <th class="border bg-gray" colspan="3">Grundstück</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>


         <tr>  <!-- need clarification -->
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="23" @if(isset($buy_data[23]['status']) &&  $buy_data[23]['status']) checked @endif></td>
          <td class="border" colspan="2">Grundbuchauszug, Abgleich mit Flurkarte!

             <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/23') }}" data-title="Kommentar">{{ @$buy_data[23]['text_data1'] }}</a>
            <br>
              Dienstbarkeiten,
Belastungen,
 <a href="#" class="inline-edit" data-type="text" data-pk="text_data2" data-url="{{url('einkauf-item/update/'.$id.'/23') }}" data-title="Kommentar">{{ @$buy_data[23]['text_data2'] }}</a>


          </td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="23" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[23]['user1']) &&  $buy_data[23]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="23" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[23]['user2']) &&  $buy_data[23]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="23" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[23]['user3']) &&  $buy_data[23]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/23') }}" data-title="Kommentar">{{ @$buy_data[23]['comment'] }}</a></td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="24" @if(isset($buy_data[24]['status']) &&  $buy_data[24]['status']) checked @endif></td>
          <td class="border" colspan="2">Auszug aus dem Liegenschaftskataster / Flurkarte</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="24" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[24]['user1']) &&  $buy_data[24]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="24" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[24]['user2']) &&  $buy_data[24]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="24" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[24]['user3']) &&  $buy_data[24]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/24') }}" data-title="Kommentar">{{ @$buy_data[24]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="25" @if(isset($buy_data[25]['status']) &&  $buy_data[25]['status']) checked @endif></td>
          <td class="border" colspan="2">Auszug aus dem Baulastenverzeichnis</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="25" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[25]['user1']) &&  $buy_data[25]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="25" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[25]['user2']) &&  $buy_data[25]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="25" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[25]['user3']) &&  $buy_data[25]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/25') }}" data-title="Kommentar">{{ @$buy_data[25]['comment'] }}</a></td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="26" @if(isset($buy_data[26]['status']) &&  $buy_data[26]['status']) checked @endif></td>
          <td class="border" colspan="2">Auskunft aus dem Altlastenverzeichnis</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="26" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[26]['user1']) &&  $buy_data[26]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="26" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[26]['user2']) &&  $buy_data[26]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="26" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[26]['user3']) &&  $buy_data[26]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/26') }}" data-title="Kommentar">{{ @$buy_data[26]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="27" @if(isset($buy_data[27]['status']) &&  $buy_data[27]['status']) checked @endif></td>
          <td class="border" colspan="2">Lageplan</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="27" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[27]['user1']) &&  $buy_data[27]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="27" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[27]['user2']) &&  $buy_data[27]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="27" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[27]['user3']) &&  $buy_data[27]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/27') }}" data-title="Kommentar">{{ @$buy_data[27]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="28" @if(isset($buy_data[28]['status']) &&  $buy_data[28]['status']) checked @endif></td>
          <td class="border" colspan="2">Anzahl der Stellplätze <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/28') }}" data-title="Kommentar">{{ @$buy_data[28]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="28" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[28]['user1']) &&  $buy_data[28]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="28" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[28]['user2']) &&  $buy_data[28]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="28" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[28]['user3']) &&  $buy_data[28]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/28') }}" data-title="Kommentar">{{ @$buy_data[28]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="29" @if(isset($buy_data[29]['status']) &&  $buy_data[29]['status']) checked @endif></td>
          <td class="border" colspan="2">Stellplatznachweis </td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="29" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[29]['user1']) &&  $buy_data[29]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="29" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[29]['user2']) &&  $buy_data[29]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="29" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[29]['user3']) &&  $buy_data[29]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/29') }}" data-title="Kommentar">{{ @$buy_data[29]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="30" @if(isset($buy_data[30]['status']) &&  $buy_data[30]['status']) checked @endif></td>
          <td class="border" colspan="2">Auskunft zu den Erschließungskosten </td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="30" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[30]['user1']) &&  $buy_data[30]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="30" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[30]['user2']) &&  $buy_data[30]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="30" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[30]['user3']) &&  $buy_data[30]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/30') }}" data-title="Kommentar">{{ @$buy_data[30]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="31" @if(isset($buy_data[31]['status']) &&  $buy_data[31]['status']) checked @endif></td>
          <td class="border" colspan="2">Grundsteuerbescheid samt </td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="31" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[31]['user1']) &&  $buy_data[31]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="31" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[31]['user2']) &&  $buy_data[31]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="31" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[31]['user3']) &&  $buy_data[31]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/31') }}" data-title="Kommentar">{{ @$buy_data[31]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="32" @if(isset($buy_data[32]['status']) &&  $buy_data[32]['status']) checked @endif></td>
          
          <td class="border" colspan="">Grundbesitzabgabenbescheid </td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="32" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[32]['user1']) &&  $buy_data[32]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="32" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[32]['user2']) &&  $buy_data[32]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="32" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[32]['user3']) &&  $buy_data[32]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/32') }}" data-title="Kommentar">{{ @$buy_data[32]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="33" @if(isset($buy_data[33]['status']) &&  $buy_data[33]['status']) checked @endif></td>
          
          <td class="border" colspan="">Einheitswertbescheid </td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="33" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[33]['user1']) &&  $buy_data[33]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="33" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[33]['user2']) &&  $buy_data[33]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="33" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[33]['user3']) &&  $buy_data[33]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/33') }}" data-title="Kommentar">{{ @$buy_data[33]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="34" @if(isset($buy_data[34]['status']) &&  $buy_data[34]['status']) checked @endif></td>
          <td class="border" colspan="2">Auskunft zum Denkmalschutz, falls notwendig </td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="34" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[34]['user1']) &&  $buy_data[34]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="34" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[34]['user2']) &&  $buy_data[34]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="34" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[34]['user3']) &&  $buy_data[34]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/34') }}" data-title="Kommentar">{{ @$buy_data[34]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="" colspan="4"></td>
        </tr>

        <tr>
          <th class="border bg-gray" colspan="3">Stadtplanung</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="35" @if(isset($buy_data[35]['status']) &&  $buy_data[35]['status']) checked @endif></td>
          <td class="border" colspan="2">B-Plan -> mögliche Entwicklungsabsprachen z.B. Wohnbebauung </td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="35" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[35]['user1']) &&  $buy_data[35]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="35" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[35]['user2']) &&  $buy_data[35]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="35" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[35]['user3']) &&  $buy_data[35]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/35') }}" data-title="Kommentar">{{ @$buy_data[35]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="36" @if(isset($buy_data[36]['status']) &&  $buy_data[36]['status']) checked @endif></td>
          <td class="border" colspan="2">Einzelhandelsgutachten, falls vorhanden</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="36" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[36]['user1']) &&  $buy_data[36]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="36" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[36]['user2']) &&  $buy_data[36]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="36" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[36]['user3']) &&  $buy_data[36]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/36') }}" data-title="Kommentar">{{ @$buy_data[36]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="37" @if(isset($buy_data[37]['status']) &&  $buy_data[37]['status']) checked @endif></td>
          <td class="border" colspan="2">Behördliche Auskünfte, falls vorhanden</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="37" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[37]['user1']) &&  $buy_data[37]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="37" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[37]['user2']) &&  $buy_data[37]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="37" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[37]['user3']) &&  $buy_data[37]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/37') }}" data-title="Kommentar">{{ @$buy_data[37]['comment'] }}</a></td>
        </tr>




        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="" colspan="4"></td>
        </tr>

        <tr>
          <th class="border bg-gray" colspan="3">Allgemeine Unterlagen</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="38" @if(isset($buy_data[38]['status']) &&  $buy_data[38]['status']) checked @endif></td>
          <td class="border" colspan="2">Versicherungspolicen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="38" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[38]['user1']) &&  $buy_data[38]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="38" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[38]['user2']) &&  $buy_data[38]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="38" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[38]['user3']) &&  $buy_data[38]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/38') }}" data-title="Kommentar">{{ @$buy_data[38]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="39" @if(isset($buy_data[39]['status']) &&  $buy_data[39]['status']) checked @endif	></td>
          <td class="border" colspan="2">Objektbilder</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="39" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[39]['user1']) &&  $buy_data[39]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="39" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[39]['user2']) &&  $buy_data[39]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="39" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[39]['user3']) &&  $buy_data[39]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/39') }}" data-title="Kommentar">{{ @$buy_data[39]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="40" @if(isset($buy_data[40]['status']) &&  $buy_data[40]['status']) checked @endif	></td>
          <td class="border" colspan="2">Buchwert beim Verkäufer, falls notwendig (Share Deal)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="40" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[40]['user1']) &&  $buy_data[40]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="40" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[40]['user2']) &&  $buy_data[40]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="40" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[40]['user3']) &&  $buy_data[40]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/40') }}" data-title="Kommentar">{{ @$buy_data[40]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="" colspan="4"></td>
        </tr>

        <tr>
          <th class="border bg-gray" colspan="3">Technische Unterlagen</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="41" @if(isset($buy_data[41]['status']) &&  $buy_data[41]['status']) checked @endif	></td>
          <td class="border" colspan="2">Grundrisse</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="41" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[41]['user1']) &&  $buy_data[41]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="41" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[41]['user2']) &&  $buy_data[41]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="41" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[41]['user3']) &&  $buy_data[41]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/41') }}" data-title="Kommentar">{{ @$buy_data[41]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="42" @if(isset($buy_data[42]['status']) &&  $buy_data[42]['status']) checked @endif	></td>
          <td class="border" colspan="2">Nutzflächenberechnung</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="42" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[42]['user1']) &&  $buy_data[42]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="42" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[42]['user2']) &&  $buy_data[42]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="42" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[42]['user3']) &&  $buy_data[42]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/42') }}" data-title="Kommentar">{{ @$buy_data[42]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="43" @if(isset($buy_data[43]['status']) &&  $buy_data[43]['status']) checked @endif	></td>
          <td class="border" colspan="2">Baubeschreibung</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="43" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[43]['user1']) &&  $buy_data[43]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="43" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[43]['user2']) &&  $buy_data[43]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="43" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[43]['user3']) &&  $buy_data[43]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/43') }}" data-title="Kommentar">{{ @$buy_data[43]['comment'] }}</a></td>
        </tr>


         <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="44" @if(isset($buy_data[44]['status']) &&  $buy_data[44]['status']) checked @endif	></td>
          <td class="border" colspan="2">Baugenehmigung</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="44" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[44]['user1']) &&  $buy_data[44]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="44" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[44]['user2']) &&  $buy_data[44]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="44" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[44]['user3']) &&  $buy_data[44]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/44') }}" data-title="Kommentar">{{ @$buy_data[44]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="45" @if(isset($buy_data[45]['status']) &&  $buy_data[45]['status']) checked @endif	></td>
          <td class="border" colspan="2">Bauabnahmeprotokoll (s. auch Übergabeprotokoll mit Mieter)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="45" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[45]['user1']) &&  $buy_data[45]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="45" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[45]['user2']) &&  $buy_data[45]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="45" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[45]['user3']) &&  $buy_data[45]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/45') }}" data-title="Kommentar">{{ @$buy_data[45]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="46" @if(isset($buy_data[46]['status']) &&  $buy_data[46]['status']) checked @endif	></td>
          <td class="border" colspan="2">Nutzungsgenehmigung nach Flächen falls vorhanden / relevant</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="46" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[46]['user1']) &&  $buy_data[46]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="46" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[46]['user2']) &&  $buy_data[46]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="46" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[46]['user3']) &&  $buy_data[46]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/46') }}" data-title="Kommentar">{{ @$buy_data[46]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="47" @if(isset($buy_data[47]['status']) &&  $buy_data[47]['status']) checked @endif	></td>
          <td class="border" colspan="2">Energieausweis</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="47" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[47]['user1']) &&  $buy_data[47]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="47" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[47]['user2']) &&  $buy_data[47]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="47" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[47]['user3']) &&  $buy_data[47]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/47') }}" data-title="Kommentar">{{ @$buy_data[47]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="48" @if(isset($buy_data[48]['status']) &&  $buy_data[48]['status']) checked @endif	></td>
          <td class="border" colspan="2">Chronologische Auflistung der Instandhaltungsmaßnahmen der letzten 2-5 Jahre</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="48" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[48]['user1']) &&  $buy_data[48]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="48" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[48]['user2']) &&  $buy_data[48]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="48" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[48]['user3']) &&  $buy_data[48]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/48') }}" data-title="Kommentar">{{ @$buy_data[48]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="49" @if(isset($buy_data[49]['status']) &&  $buy_data[49]['status']) checked @endif	></td>
          <td class="border" colspan="2">Statik Unterlagen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="49" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[49]['user1']) &&  $buy_data[49]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="49" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[49]['user2']) &&  $buy_data[49]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="49" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[49]['user3']) &&  $buy_data[49]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/49') }}" data-title="Kommentar">{{ @$buy_data[49]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="50" @if(isset($buy_data[50]['status']) &&  $buy_data[50]['status']) checked @endif	></td>
          <td class="border" colspan="2">Brandschutzkonzept / Fluchtwegeplan / Brandbeschau</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="50" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[50]['user1']) &&  $buy_data[50]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="50" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[50]['user2']) &&  $buy_data[50]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="50" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[50]['user3']) &&  $buy_data[50]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/50') }}" data-title="Kommentar">{{ @$buy_data[50]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="51" @if(isset($buy_data[51]['status']) &&  $buy_data[51]['status']) checked @endif	></td>
          <td class="border" colspan="2">Service- und Wartungsverträge mit Wartungsprotokollen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="51" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[51]['user1']) &&  $buy_data[51]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="51" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[51]['user2']) &&  $buy_data[51]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="51" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[51]['user3']) &&  $buy_data[51]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/51') }}" data-title="Kommentar">{{ @$buy_data[51]['comment'] }}</a></td>
        </tr>

        


        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="" colspan="4"></td>
        </tr>

        <tr>
          <th class="border bg-gray" colspan="3">Finanzierung</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border" colspan="2">Übersicht angefragte Banken</td>
          <td class="border checkbox-td" colspan="4"></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="52" @if(isset($buy_data[52]['status']) &&  $buy_data[52]['status']) checked @endif	></td>
          <td class="border" colspan="">a.) <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/52') }}" data-title="Kommentar" data-placement="right">{{ @$buy_data[52]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="52" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[52]['user1']) &&  $buy_data[52]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="52" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[52]['user2']) &&  $buy_data[52]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="52" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[52]['user3']) &&  $buy_data[52]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/52') }}" data-title="Kommentar">{{ @$buy_data[52]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="53" @if(isset($buy_data[53]['status']) &&  $buy_data[53]['status']) checked @endif	></td>
          <td class="border" colspan="">b.) <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/53') }}" data-title="Kommentar" data-placement="right">{{ @$buy_data[53]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="53" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[53]['user1']) &&  $buy_data[53]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="53" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[53]['user2']) &&  $buy_data[53]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="53" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[53]['user3']) &&  $buy_data[53]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/53') }}" data-title="Kommentar" >{{ @$buy_data[53]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="54" @if(isset($buy_data[54]['status']) &&  $buy_data[54]['status']) checked @endif	></td>
          <td class="border" colspan="">c.) <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/54') }}" data-title="Kommentar" data-placement="right">{{ @$buy_data[54]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="54" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[54]['user1']) &&  $buy_data[54]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="54" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[54]['user2']) &&  $buy_data[54]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="54" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[54]['user3']) &&  $buy_data[54]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/54') }}" data-title="Kommentar">{{ @$buy_data[54]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="55" @if(isset($buy_data[55]['status']) &&  $buy_data[55]['status']) checked @endif	></td>
          <td class="border" colspan="">d.) <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/55') }}" data-title="Kommentar" data-placement="right" >{{ @$buy_data[55]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="55" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[55]['user1']) &&  $buy_data[55]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="55" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[55]['user2']) &&  $buy_data[55]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="55" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[55]['user3']) &&  $buy_data[55]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/55') }}" data-title="Kommentar">{{ @$buy_data[55]['comment'] }}</a></td>
        </tr>



        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border" colspan="2">Übersicht Bankkonditionen verhandeln</td>
          <td class="border checkbox-td" colspan="4"></td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="56" @if(isset($buy_data[56]['status']) &&  $buy_data[56]['status']) checked @endif	></td>
          <td class="border" colspan="">a.) <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/56') }}" data-title="Kommentar" data-placement="right">{{ @$buy_data[56]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="56" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[56]['user1']) &&  $buy_data[56]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="56" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[56]['user2']) &&  $buy_data[56]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="56" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[56]['user3']) &&  $buy_data[56]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/56') }}" data-title="Kommentar">{{ @$buy_data[56]['comment'] }}</a></td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="57" @if(isset($buy_data[57]['status']) &&  $buy_data[57]['status']) checked @endif	></td>
          <td class="border" colspan="">b.) <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/57') }}" data-title="Kommentar" data-placement="right">{{ @$buy_data[57]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="57" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[57]['user1']) &&  $buy_data[57]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="57" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[57]['user2']) &&  $buy_data[57]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="57" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[57]['user3']) &&  $buy_data[57]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/57') }}" data-title="Kommentar">{{ @$buy_data[57]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="58" @if(isset($buy_data[58]['status']) &&  $buy_data[58]['status']) checked @endif	></td>
          <td class="border" colspan="">c.) <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/58') }}" data-title="Kommentar" data-placement="right">{{ @$buy_data[58]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="58" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[58]['user1']) &&  $buy_data[19]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="58" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[58]['user2']) &&  $buy_data[58]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="58" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[58]['user3']) &&  $buy_data[58]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/58') }}" data-title="Kommentar">{{ @$buy_data[58]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="59" @if(isset($buy_data[59]['status']) &&  $buy_data[59]['status']) checked @endif	></td>
          <td class="border" colspan="">d.) <a href="#" class="inline-edit" data-type="text" data-pk="text_data1" data-url="{{url('einkauf-item/update/'.$id.'/59') }}" data-title="Kommentar" data-placement="right">{{ @$buy_data[59]['text_data1'] }}</a></td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="59" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[59]['user1']) &&  $buy_data[59]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="59" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[59]['user2']) &&  $buy_data[59]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="59" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[59]['user3']) &&  $buy_data[59]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/59') }}" data-title="Kommentar">{{ @$buy_data[59]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="60" @if(isset($buy_data[60]['status']) &&  $buy_data[60]['status']) checked @endif	></td>
          <td class="border" colspan="2">Finanzierungszusage senden lassen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="60" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[60]['user1']) &&  $buy_data[60]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="60" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[60]['user2']) &&  $buy_data[60]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="60" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[60]['user3']) &&  $buy_data[60]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/60') }}" data-title="Kommentar">{{ @$buy_data[60]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="61" @if(isset($buy_data[61]['status']) &&  $buy_data[61]['status']) checked @endif	></td>
          <td class="border" colspan="2">Objektgesellschaft gründen (spätestens 1 Woche vor Notartermin)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="61" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[61]['user1']) &&  $buy_data[61]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="61" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[61]['user2']) &&  $buy_data[61]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="61" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[61]['user3']) &&  $buy_data[61]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/61') }}" data-title="Kommentar">{{ @$buy_data[61]['comment'] }}</a></td>
        </tr>


         <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="62" @if(isset($buy_data[62]['status']) &&  $buy_data[62]['status']) checked @endif	></td>
          <td class="border" colspan="2">Abschrift geben lassen und im Dokumentenordner ablegen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="62" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[62]['user1']) &&  $buy_data[62]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="62" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[62]['user2']) &&  $buy_data[62]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="62" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[62]['user3']) &&  $buy_data[62]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/62') }}" data-title="Kommentar">{{ @$buy_data[62]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="63" @if(isset($buy_data[63]['status']) &&  $buy_data[63]['status']) checked @endif	></td>
          <td class="border" colspan="2">Gesellschaftervertrag durch TR signen und im Dokumentenordner ablegen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="63" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[63]['user1']) &&  $buy_data[63]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="63" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[63]['user2']) &&  $buy_data[63]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="63" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[63]['user3']) &&  $buy_data[63]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/63') }}" data-title="Kommentar">{{ @$buy_data[63]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="64" @if(isset($buy_data[64]['status']) &&  $buy_data[64]['status']) checked @endif	></td>
          <td class="border" colspan="2">Eröffnungsbilanz erstellen und durch TR signen und im Dokumentenordner ablegen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="64" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[64]['user1']) &&  $buy_data[64]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="64" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[64]['user2']) &&  $buy_data[64]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="64" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[64]['user3']) &&  $buy_data[64]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/64') }}" data-title="Kommentar">{{ @$buy_data[64]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="65" @if(isset($buy_data[65]['status']) &&  $buy_data[65]['status']) checked @endif	></td>
          <td class="border" colspan="2">HR-Auszug im Dokumentenordner ablegen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="65" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[65]['user1']) &&  $buy_data[65]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="65" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[65]['user2']) &&  $buy_data[65]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="65" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[65]['user3']) &&  $buy_data[65]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/65') }}" data-title="Kommentar">{{ @$buy_data[65]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="66" @if(isset($buy_data[66]['status']) &&  $buy_data[66]['status']) checked @endif	></td>
          <td class="border" colspan="2">Bankkonto durch TR einrichten lassen (erst Sparkasse für Start, dann Mietkonto für finanzierende Bank suchen) -> TR</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="66" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[66]['user1']) &&  $buy_data[66]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="66" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[66]['user2']) &&  $buy_data[66]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="66" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[66]['user3']) &&  $buy_data[66]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/66') }}" data-title="Kommentar">{{ @$buy_data[66]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="67" @if(isset($buy_data[67]['status']) &&  $buy_data[67]['status']) checked @endif	></td>
          <td class="border" colspan="2">Unterlagen zusammenstellen (Objekte siehe oben / Firma siehe Gesellschaftsakte)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="67" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[67]['user1']) &&  $buy_data[67]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="67" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[67]['user2']) &&  $buy_data[67]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="67" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[67]['user3']) &&  $buy_data[67]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/67') }}" data-title="Kommentar">{{ @$buy_data[67]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="68" @if(isset($buy_data[68]['status']) &&  $buy_data[68]['status']) checked @endif	></td>
          <td class="border" colspan="2">Regelmäßig nachhaken, ob und wann Angebot kommt</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="68" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[68]['user1']) &&  $buy_data[68]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="68" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[68]['user2']) &&  $buy_data[68]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="68" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[68]['user3']) &&  $buy_data[68]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/68') }}" data-title="Kommentar">{{ @$buy_data[68]['comment'] }}</a></td>
        </tr>

        


          <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="" colspan="4"></td>
        </tr>

        <tr>
          <th class="border bg-gray" colspan="3">Notar</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>

        
        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="69" @if(isset($buy_data[69]['status']) &&  $buy_data[69]['status']) checked @endif	></td>
          <td class="border" colspan="2">Kaufvertrag verhandelt, geprüft und durch FR freigegeben</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="69" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[69]['user1']) &&  $buy_data[69]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="69" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[69]['user2']) &&  $buy_data[69]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="69" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[69]['user3']) &&  $buy_data[69]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/69') }}" data-title="Kommentar">{{ @$buy_data[69]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="70" @if(isset($buy_data[70]['status']) &&  $buy_data[70]['status']) checked @endif	></td>
          <td class="border" colspan="">Käufer- / Verkäuferdaten</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="70" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[70]['user1']) &&  $buy_data[70]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="70" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[70]['user2']) &&  $buy_data[70]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="70" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[70]['user3']) &&  $buy_data[70]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/70') }}" data-title="Kommentar">{{ @$buy_data[21]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="71" @if(isset($buy_data[71]['status']) &&  $buy_data[71]['status']) checked @endif	></td>
          <td class="border" colspan="">Blattnummer / Flurstück(e)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="71" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[71]['user1']) &&  $buy_data[71]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="71" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[71]['user2']) &&  $buy_data[71]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="71" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[71]['user3']) &&  $buy_data[71]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/71') }}" data-title="Kommentar">{{ @$buy_data[71]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="72" @if(isset($buy_data[72]['status']) &&  $buy_data[72]['status']) checked @endif	></td>
          <td class="border" colspan="">Grundstücksgröße in m²</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="72" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[72]['user1']) &&  $buy_data[72]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="72" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[72]['user2']) &&  $buy_data[72]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="72" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[72]['user3']) &&  $buy_data[72]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/72') }}" data-title="Kommentar">{{ @$buy_data[72]['comment'] }}</a></td>
        </tr>



        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="73" @if(isset($buy_data[73]['status']) &&  $buy_data[73]['status']) checked @endif	></td>
          <td class="border" colspan="">Dienstbarkeiten</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="73" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[73]['user1']) &&  $buy_data[73]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="73" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[73]['user2']) &&  $buy_data[73]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="73" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[73]['user3']) &&  $buy_data[73]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/73') }}" data-title="Kommentar">{{ @$buy_data[73]['comment'] }}</a></td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="74" @if(isset($buy_data[74]['status']) &&  $buy_data[74]['status']) checked @endif	></td>
          <td class="border" colspan="">Abt III löschen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="74" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[74]['user1']) &&  $buy_data[74]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="74" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[74]['user2']) &&  $buy_data[74]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="74" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[74]['user3']) &&  $buy_data[74]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/74') }}" data-title="Kommentar">{{ @$buy_data[74]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="75" @if(isset($buy_data[75]['status']) &&  $buy_data[75]['status']) checked @endif	></td>
          <td class="border" colspan="">Kaufpreis</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="75" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[75]['user1']) &&  $buy_data[75]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="75" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[75]['user2']) &&  $buy_data[75]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="75" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[75]['user3']) &&  $buy_data[75]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/75') }}" data-title="Kommentar">{{ @$buy_data[75]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="76" @if(isset($buy_data[76]['status']) &&  $buy_data[76]['status']) checked @endif	></td>
          <td class="border" colspan="">Notaranderkonto / Anzahlung (falls notwendig)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="76" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[76]['user1']) &&  $buy_data[76]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="76" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[76]['user2']) &&  $buy_data[76]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="76" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[76]['user3']) &&  $buy_data[76]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/76') }}" data-title="Kommentar">{{ @$buy_data[76]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="77" @if(isset($buy_data[77]['status']) &&  $buy_data[77]['status']) checked @endif	></td>
          <td class="border" colspan="">BNL-Klausel (Monatsende / 10 Tage) / Kaufpreiszahlung</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="77" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[77]['user1']) &&  $buy_data[77]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="77" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[77]['user2']) &&  $buy_data[77]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="77" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[77]['user3']) &&  $buy_data[77]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/77') }}" data-title="Kommentar">{{ @$buy_data[77]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="78" @if(isset($buy_data[78]['status']) &&  $buy_data[78]['status']) checked @endif	></td>
          <td class="border" colspan="">UST-Klausel</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="78" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[78]['user1']) &&  $buy_data[78]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="78" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[78]['user2']) &&  $buy_data[78]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="78" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[78]['user3']) &&  $buy_data[78]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/78') }}" data-title="Kommentar">{{ @$buy_data[78]['comment'] }}</a></td>
        </tr>


         <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="79" @if(isset($buy_data[79]['status']) &&  $buy_data[79]['status']) checked @endif	></td>
          <td class="border" colspan="">Kontonummer Käufer</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="79" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[79]['user1']) &&  $buy_data[79]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="79" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[79]['user2']) &&  $buy_data[79]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="79" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[79]['user3']) &&  $buy_data[79]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/79') }}" data-title="Kommentar">{{ @$buy_data[79]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="80" @if(isset($buy_data[80]['status']) &&  $buy_data[80]['status']) checked @endif	></td>
          <td class="border" colspan="">Versicherung (Übernahme / Ablauf)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="80" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[80]['user1']) &&  $buy_data[80]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="80" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[80]['user2']) &&  $buy_data[80]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="80" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[80]['user3']) &&  $buy_data[80]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/80') }}" data-title="Kommentar">{{ @$buy_data[80]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="81" @if(isset($buy_data[81]['status']) &&  $buy_data[81]['status']) checked @endif	></td>
          <td class="border" colspan="">Falls vorhanden, Auflistung offener Versicherungsschäden</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="81" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[81]['user1']) &&  $buy_data[81]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="81" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[81]['user2']) &&  $buy_data[81]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="81" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[81]['user3']) &&  $buy_data[81]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/81') }}" data-title="Kommentar">{{ @$buy_data[81]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="82" @if(isset($buy_data[82]['status']) &&  $buy_data[82]['status']) checked @endif	></td>
          <td class="border" colspan="">Nebenkostenabrechnung</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="82" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[82]['user1']) &&  $buy_data[82]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="82" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[82]['user2']) &&  $buy_data[82]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="82" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[82]['user3']) &&  $buy_data[82]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/82') }}" data-title="Kommentar">{{ @$buy_data[82]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="83" @if(isset($buy_data[83]['status']) &&  $buy_data[83]['status']) checked @endif	></td>
          <td class="border" colspan="">Garantien (falls vorhanden / abgegeben)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="83" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[83]['user1']) &&  $buy_data[83]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="83" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[83]['user2']) &&  $buy_data[83]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="83" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[83]['user3']) &&  $buy_data[83]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/83') }}" data-title="Kommentar">{{ @$buy_data[83]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="84" @if(isset($buy_data[31]['status']) &&  $buy_data[31]['status']) checked @endif	></td>
          <td class="border" colspan="">Sondersituation (falls vorhanden)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="84" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[84]['user1']) &&  $buy_data[84]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="84" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[84]['user2']) &&  $buy_data[84]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="84" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[84]['user3']) &&  $buy_data[84]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/84') }}" data-title="Kommentar">{{ @$buy_data[84]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"></td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="85" @if(isset($buy_data[85]['status']) &&  $buy_data[85]['status']) checked @endif	></td>
          <td class="border" colspan="">Anlagen zum Kaufvertrag (Wartungs- &Verwaltungsverträge, Mieterliste, sonstiges, falls etwas vereinbart wurde)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="85" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[85]['user1']) &&  $buy_data[85]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="85" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[85]['user2']) &&  $buy_data[85]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="85" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[85]['user3']) &&  $buy_data[85]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/85') }}" data-title="Kommentar">{{ @$buy_data[85]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="86" @if(isset($buy_data[86]['status']) &&  $buy_data[86]['status']) checked @endif	></td>
          <td class="border" colspan="2">Notartermin vereinbaren und FR oder TR rechtzeitig informieren</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="86" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[86]['user1']) &&  $buy_data[86]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="86" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[86]['user2']) &&  $buy_data[86]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="86" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[86]['user3']) &&  $buy_data[86]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/86') }}" data-title="Kommentar">{{ @$buy_data[86]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="87" @if(isset($buy_data[87]['status']) &&  $buy_data[87]['status']) checked @endif	></td>
          <td class="border" colspan="2">ggf. Kaufpreisanzahlung rechtzeitig überweisen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="87" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[87]['user1']) &&  $buy_data[87]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="87" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[87]['user2']) &&  $buy_data[87]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="87" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[87]['user3']) &&  $buy_data[87]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/87') }}" data-title="Kommentar">{{ @$buy_data[87]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="88" @if(isset($buy_data[88]['status']) &&  $buy_data[88]['status']) checked @endif	></td>
          <td class="border" colspan="2">Kaufvertrag 2x ausdrucken und Objektordner mit aktuellen Unterlagen zum Notartermin mitnehmen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="88" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[88]['user1']) &&  $buy_data[88]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="88" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[88]['user2']) &&  $buy_data[88]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="88" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[88]['user3']) &&  $buy_data[88]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/88') }}" data-title="Kommentar">{{ @$buy_data[88]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="89" @if(isset($buy_data[89]['status']) &&  $buy_data[89]['status']) checked @endif	></td>
          <td class="border" colspan="2">Abschrift mitnehmen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="89" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[89]['user1']) &&  $buy_data[89]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="89" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[89]['user2']) &&  $buy_data[89]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="89" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[89]['user3']) &&  $buy_data[89]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/89') }}" data-title="Kommentar">{{ @$buy_data[89]['comment'] }}</a></td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="90" @if(isset($buy_data[90]['status']) &&  $buy_data[90]['status']) checked @endif	></td>
          <td class="border" colspan="2">Nach Signing Kaufvertrag als PDF im Dokumentenordner ablegen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="90" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[90]['user1']) &&  $buy_data[90]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="90" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[90]['user2']) &&  $buy_data[90]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="90" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[90]['user3']) &&  $buy_data[90]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/90') }}" data-title="Kommentar">{{ @$buy_data[90]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="" colspan="4"></td>
        </tr>

        <tr><th colspan="6">
          Im Falle eines Share Deals ist noch Folgendes zu prüfen:
        </th>
      </tr>




        <tr>
          <th class="border bg-gray" colspan="3">Share Deal</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="91" @if(isset($buy_data[91]['status']) &&  $buy_data[91]['status']) checked @endif></td>
          <td class="border" colspan="2">Handelsregisterauszug (HR-Auszug)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="91" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[91]['user1']) &&  $buy_data[91]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="91" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[91]['user2']) &&  $buy_data[91]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="91" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[91]['user3']) &&  $buy_data[91]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/91') }}" data-title="Kommentar">{{ @$buy_data[91]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="92" @if(isset($buy_data[92]['status']) &&  $buy_data[92]['status']) checked @endif  ></td>
          <td class="border" colspan="2">Gesellschaftervertrag / Satzung</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="92" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[92]['user1']) &&  $buy_data[92]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="92" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[92]['user2']) &&  $buy_data[92]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="92" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[92]['user3']) &&  $buy_data[92]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/92') }}" data-title="Kommentar">{{ @$buy_data[92]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="93" @if(isset($buy_data[93]['status']) &&  $buy_data[93]['status']) checked @endif  ></td>
          <td class="border" colspan="2">Protokolle der Gesellschafterversammlungen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="93" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[93]['user1']) &&  $buy_data[93]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="93" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[93]['user2']) &&  $buy_data[93]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="93" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[93]['user3']) &&  $buy_data[93]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/93') }}" data-title="Kommentar">{{ @$buy_data[93]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-einkauf" data-type="94" @if(isset($buy_data[94]['status']) &&  $buy_data[94]['status']) checked @endif  ></td>
          <td class="border" colspan="2">Jahresabschlüsse</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="94" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[94]['user1']) &&  $buy_data[94]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="94" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[94]['user2']) &&  $buy_data[94]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="94" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[94]['user3']) &&  $buy_data[94]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          
          <td class="border" colspan=""><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/94') }}" data-title="Kommentar">{{ @$buy_data[94]['comment'] }}</a></td>
        </tr>

        <tr>
          <td colspan="7" class="text-center">&nbsp;</td>
        </tr>
        

         @if(Auth::user()->email!=config('users.falk_email'))
        <tr>
          <td colspan="7" class="text-center"><button style="color: #fff;width: 180px;font-size: 17px;" type="button" class="btn @if($isale_date && $ireleased_by) btn-success @else btn-primary @endif isend-mail">
          @if($isale_date && $ireleased_by) Freigegeben  @else Zur Freigabe senden @endif </button></td>
        </tr>
        @endif

        @if(Auth::user()->email==config('users.falk_email'))
        <tr>
          <td colspan="7" class="text-center"><button style="color: #fff;width: 170px;font-size: 18px;" type="button" class="btn @if($isale_date && $ireleased_by) btn-success @else btn-primary @endif irelease-property" data-toggle="modal" data-target="#irelease-property">@if($isale_date && $ireleased_by) Freigegeben  @else Freigeben @endif</button></td>
        </tr>
        @endif

        
        @if($isale_date && $ireleased_by)
        <tr>
          <th class="" colspan="3"><h3 style="font-size: 16px;" class="">Freigegeben durch</h3></th>
          <td class="" colspan="4"><h3 style="font-size: 16px;" class="">{{ $ireleased_by }}</h3></td>
        </tr>

        <tr>
          <th class="" colspan="3" ><h3 style="font-size: 16px;" class="">Datum</h3></th>
          <td class="" colspan="4"><h3 style="font-size: 16px;" class="">{{ $isale_date }}</h3></td>
        </tr>
        @endif

      </tbody>
    </table>
  </div>

    <hr>

    
    <h3 class="title-tag">Ankaufsvorlage FCR Immobilien AG 

      @if(Auth::user()->email!=config('users.falk_email'))
        <button style="color: #fff;width: 170px;font-size: 16px;margin-left: 10px;" type="button" class=" pull-right btn @if($isale_date2 && $ireleased_by2) btn-success @else btn-primary @endif isend-mail2">@if($isale_date2 && $ireleased_by2) Freigegeben  @else Zur Freigabe senden @endif</button>
        @endif

        @if(Auth::user()->email==config('users.falk_email'))
          <button style="color: #fff;width: 170px;font-size: 16px;margin-left: 10px;" type="button" class="pull-right btn @if($isale_date2 && $ireleased_by2) btn-success @else btn-primary @endif irelease-property2" data-toggle="modal" data-target="#irelease-property2">@if($isale_date2 && $ireleased_by2) Freigegeben  @else Freigeben @endif</button>
        @endif
      <a href="javascript:void(0)" data-class="sheet-3" class="btn btn-info show-link pull-right"><i class="fa fa-angle-down"></i></a></h3>

    <div class="sheet-3 hidden">
      <h3>Objektdaten</h3>

      <table class="forecast-table" style="margin-top: 10px;">
      <tbody>
        <tr>
          <th class="border bg-gray">Objektadresse</th>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="address" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="Objektadresse">{{ @$pb_data->address }}</a></td>
          <th class="border bg-gray">Hauptmieter</th>
          <td class="border">
              @if(isset($propertiesExtra1s[0]['tenant']))
              {{$propertiesExtra1s[0]['tenant']}}
              @endif
          <!--<a href="#" class="inline-edit" data-type="text" data-pk="main_tenant" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="Hauptmieter">{{ @$pb_data->main_tenant }}</a>-->
          </td>
        </tr>
        <tr>
          <th class="border bg-gray">Bundesland</th>
          <td class="border">
            <?php $v = $properties->niedersachsen;
            if(@$pb_data->state)
              $v = $pb_data->state; ?>

              <a href="#" class="inline-edit" data-type="text" data-pk="state" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="Bundesland">{{ $v }}</a>

          </td>
          <th class="border bg-gray">WAULT</th>
          <td class="border">
            <?php
              $v = "";
            ?>
            @foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
            <?php $v = ($tenancy_schedule->calculations['total_business_actual_net_rent']!=0)?number_format(($tenancy_schedule->calculations['total_remaining_time_in_eur'])/(12 * $tenancy_schedule->calculations['total_business_actual_net_rent']),1) : 0  ?>
          @endforeach
          
            <!--@if(@$pb_data->wault)
              <?php $v = $pb_data->wault;?>
            @endif-->
            
            <?php
                    //declare values
                    $Q53 = 0;
                    //echo '<pre>';
                    //print_r($properties);
                    
                    ?>
                    @foreach($propertiesExtra1s as $propertiesExtra1)
                            <?php

                            if( strpos($properties1->duration_from_O38, "unbefristet") !== false){
                                    $value = 1;
                            }else{
                                    $value = ((strtotime(str_replace('/', '-', $propertiesExtra1->mv_end)) -  strtotime(str_replace('/', '-', $properties1->duration_from_O38))) / 86400) / 365;
                            }
                                    $Q53 += $value*$propertiesExtra1->net_rent_p_a;

                            ?>
                    @endforeach
            <?php
            $P53 = ($properties1->net_rent_pa == 0) ? 0 : ($Q53 / $properties1->net_rent_pa) ;
            ?>
            {{number_format($P53,1,",",".")}}
            <!--<a href="#" class="inline-edit" data-type="text" data-pk="wault" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="WAULT">{{ $v }}</a>-->
          </td>
        </tr>
        <tr>
          <th class="border bg-gray">Ankaufspreis</th>
          <td class="border"><a href="#" class="inline-edit" data-type="number" data-pk="purchase_price" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="Ankaufspreis">{{ @$pb_data->purchase_price }}</a></td>
          <th class="border bg-gray">Baujahr</th>
          <td class="border">
            <?php
            //$v = $properties->construction_year;
            //if(@$pb_data->const_year)
              //$v = $pb_data->const_year;
            ?>
            <!--<a href="#" class="inline-edit" data-type="text" data-pk="construction_year" data-url="{{url('property/update_property_ist_soll/'.$tab) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.construction_year')}}">-->
            {{$properties1->construction_year}}
            <!--</a>-->
            <!--<a href="#" class="inline-edit" data-type="number" data-pk="const_year" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="Baujahr">{{ $v }}</a>-->
          </td>
        </tr>
        <tr>
          <th class="border bg-gray">GrESt in %/ total:</th>
          <td class="border"><a href="#" class="inline-edit" data-type="number" data-pk="grest_per" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="GrESt in %">{{ @$pb_data->grest_per }}</a></td>
          <th class="border bg-gray">Bruttofaktor IST/Soll:</th>
          <td class="border"><a href="#" class="inline-edit" data-type="number" data-pk="gross_factor" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="Bruttofaktor IST/Soll">{{ @$pb_data->gross_factor }}</a></td>
        </tr>
        <tr>
        <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
        <th class="border" colspan="4">Beschreibung der Lage (Makro und Mikro)</th>
        </tr>
        <tr>
          <td class="border" colspan="4"><b>Makro</b>
            <br>
            <a href="#" class="inline-edit" data-type="text" data-pk="macro" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="Makro">{{ @$pb_data->macro }}</a>
          </td>
        </tr>
        <tr>
          <td class="border" colspan="4"><b>Mikro</b><br>
          <a href="#" class="inline-edit" data-type="text" data-pk="micro" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="Mikro">{{ @$pb_data->micro }}</a></td>
        </tr>

        <tr>
        <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
        <th class="border" colspan="4">Beschreibung des Objektes incl. objektspezifische Besonderheiten</th>
        </tr>
        <tr>
          <td class="border" colspan="4"><a href="#" class="inline-edit" data-type="text" data-pk="description" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->description }}</a>
          </td>
        </tr>

         <tr>
        <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
        <th class="border" colspan="4">Einschätzung Mieterbestand / Neuvermietung sowie Markt- und Standortanalyse incl. Vergleichsmieten und -preise</th>
        </tr>
        <tr>
          <td class="border" colspan="4"><a href="#" class="inline-edit" data-type="number" data-pk="number_of_tenants" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->number_of_tenants }}</a>
          </td>
        </tr>


        <tr>
        <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
        <th class="border" colspan="4">Strategie für das Objekt/ Portfolio incl. Exit</th>
        </tr>
        <tr>
          <td class="border" colspan="4"><a href="#" class="inline-edit" data-type="text" data-pk="strategy" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->strategy }}</a>
          </td>
        </tr>
        <tr>
          <td class="border" colspan="4">Verkauf nach 2 bis 3 Jahren. Je nach Szenario sehen wir folgenden Verkaufserlös:
          </td>
        </tr>
        <tr>
          <td class="border" colspan="4"><b>Worstcase</b><br>
            <a href="#" class="inline-edit" data-type="text" data-pk="worst_case" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->worst_case }}</a>
          </td>
        </tr>
        <tr>
          <td class="border" colspan="4"><b>Basecase</b><br>
            <a href="#" class="inline-edit" data-type="text" data-pk="base_case" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->base_case }}</a>
          </td>
        </tr>
        <tr>
          <td class="border" colspan="4"><b>Bestcase</b><br>
            <a href="#" class="inline-edit" data-type="text" data-pk="best_case" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->best_case }}</a>
          </td>
        </tr>
        <tr>
          <td  colspan="4">&nbsp;
          </td>
        </tr>
        <tr>
          <td  colspan="4">&nbsp;
          </td>
        </tr>
        <tr>
          <td  colspan="4">&nbsp;
          </td>
        </tr>


        <tr>
          <th colspan="4" >Darstellung der Finanzierung</th>
        </tr>
        <tr>
          <th class="border bg-gray">Herkunft</th>
          <th class="border bg-gray">in EUR </th>
          <th class="border bg-gray">Herkunft</th>
          <th class="border bg-gray">in EUR</th>
        </tr>
        <tr>
          <td class="border">Anteil EK IST</td>
          <td class="border"><a href="#" class="inline-edit total1" data-type="number" data-pk="loan_bank_is" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->loan_bank_is }}</a></td>
          <td class="border">Anteil EK SOLL</th>
          <td class="border"><a href="#" class="inline-edit total2" data-type="number" data-pk="share_ec_should" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->share_ec_should }}</a></td>
        </tr>
        <tr>
          <td class="border">Darlehen Bank IST</td>
          <td class="border"><a href="#" class="inline-edit total1" data-type="number" data-pk="share_ek" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->share_ek  }}</a></td>
          <td class="border">Darlehen Bank SOLL</th>
          <td class="border"><a href="#" class="inline-edit total2" data-type="number" data-pk="loan_bank_should" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->loan_bank_should }}</a></td>
        </tr>
        <tr>
          <th class="border bg-gray">TOTAL IST</th>
          <th class="border bg-gray sum_total1"></th>
          <th class="border bg-gray">TOTAL SOLL</th>
          <th class="border bg-gray sum_total2"></th>
        </tr>

        <tr>
        <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
        <th class="border" colspan="4">Darstellung der Chancen und Risiken</th>
        </tr>
        <tr>
          <td class="border" colspan="4"><b>Chancen</b>
            <br>
            <a href="#" class="inline-edit" data-type="text" data-pk="opportunities" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->opportunities  }}</a>
          </td>
        </tr>
        <tr>
          <td class="border" colspan="4"><b>Risiken</b> <br><a href="#" class="inline-edit" data-type="text" data-pk="risk" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->risk  }}</a>
          </td>
        </tr>
        <tr>
        <td colspan="4">&nbsp;</td>
        </tr>

        <!-- <tr>
          <td colspan="4"><b>Genehmigt</b>
          </td>
        </tr>

        <tr>
          <td colspan="4"><b>Der Vorstand</b>
            <a href="#" class="inline-edit" data-type="text" data-pk="approved_by" data-url="{{url('einkauf/update/'.$id) }}" data-placement="right" data-title="">{{ @$pb_data->approved_by  }}</a>
          </td>
        </tr>
        <tr>
        <td colspan="4">&nbsp;</td>
        </tr> -->

        @if(Auth::user()->email!=config('users.falk_email'))
        <tr>
          <td colspan="7" class="text-center"><button style="color: #fff;width: 180px;font-size: 17px;" type="button" class="btn @if($isale_date2 && $ireleased_by2) btn-success @else btn-primary @endif isend-mail2">@if($isale_date && $ireleased_by) Freigegeben  @else Zur Freigabe senden @endif</button></td>
        </tr>
        @endif

        @if(Auth::user()->email==config('users.falk_email'))
        <tr>
          <td colspan="7" class="text-center"><button style="color: #fff;width: 170px;font-size: 18px;" type="button" class="btn @if($isale_date2 && $ireleased_by2) btn-success @else btn-primary @endif irelease-property2" data-toggle="modal" data-target="#irelease-property2">@if($isale_date && $ireleased_by) Freigegeben  @else Freigeben @endif</button></td>
        </tr>
        @endif

        
        @if($isale_date2 && $ireleased_by2)
        <tr>
          <th class="" colspan="3"><h3 style="font-size: 16px;" class="">Freigegeben durch</h3></th>
          <td class="" colspan="4"><h3 style="font-size: 16px;" class="">{{ $ireleased_by2 }}</h3></td>
        </tr>

        <tr>
          <th class="" colspan="3" ><h3 style="font-size: 16px;" class="">Datum</h3></th>
          <td class="" colspan="4"><h3 style="font-size: 16px;" class="">{{ $isale_date2 }}</h3></td>
        </tr>
        @endif

        <tr>
          <td colspan="4"><b>*Genehmigung Fachabteilung siehe Einkaufsdatenblatt</b>
          </td>
        </tr>
        <tr>
          <td colspan="4"><b>Anlage: Einkaufsdatenblatt; Kalkulations- und Wirtschaftsplan incl. Mieterliste und Fotos</b>
          </td>
        </tr>


        
      


        
      </tbody>
    </table>

    </div>
    <hr>

    <h3 class="title-tag">Checkliste nach dem Einkauf<a href="javascript:void(0)" data-class="sheet-2" class="btn btn-info show-link pull-right" style="margin-right: 180px;"><i class="fa fa-angle-down"></i></a></h3>
    <div class="sheet-2 hidden">

    <h4>Interne ToDo</h4>
    Objekt
    <a href="#" class="inline-edit" data-type="text" data-pk="property_name" data-url="{{url('einkauf/update/'.$id) }}" data-title="property_name">{{ $name }}</a>
    <br>
      <table class="forecast-table" style="margin-top: 10px;">
      <thead>
        <tr>
          <th class="border bg-gray">PLZ</th>
          <th class="border bg-gray">Stadt</th>
          <th class="border bg-gray">Straße, Nr.</th>
          <th class="border bg-gray">Einkäufer</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="border">{{ $properties->plz_ort }}</td>
          <td class="border">{{ $properties->ort }}</td>
          <td class="border">{{ $street }}</td>
          <td class="border"><select class="change-in-user">
                <option value="">{{__('dashboard.transaction_manager')}}</option>
                @foreach($tr_users as $list)
                @if($seller_id==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->name}}</option>
                @else
                <option value="{{$list->id}}">{{$list->name}}</option>
                @endif
                @endforeach
              
              </select></td>
        </tr>
        
      </tbody>
    </table>


      <table class="forecast-table" style="margin-top: 20px;">
      <thead>
        <tr>
          <th class="border bg-gray" colspan="3" >Mit Vorbesitzer (ggfs. in Notarvertrag ergänzen)</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
      </thead>

      <tbody>
        <?php
        $j = 301;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">ggf. Nachbeglaubigung</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>
        <?php
        $j = 302;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Verträge / Adressdaten</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 303;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Kautionsliste / Kautionsübergabemodalitäten</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


         <?php
        $j = 304;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Anschriften und Verträge aller bisherigen Dienstleister geben lassen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 305;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Alte Heiz-, Strom-, Müll-, Hausmeister-, etc. Rechnungen vom Vorbesitzer</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 306;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Bei Kauf während des Kalenderjahres: Abrechnungsmodalitäten NK Vorauszahlung klären</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>
        <tr>
          <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
          <th class="border bg-gray" colspan="3" >Vorbesitzer / Hausverwaltung</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
        <?php
        $j = 307;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Info bzgl. Eigentümerwechsel und neuem Konto an alle Mieter</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 308;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Info bzgl. Eigentümerwechsel an alle Dienstleister</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 309;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Weiterleitung Kautionen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 309;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Weiterleitung noch an Vorbesitzer gezahlten Mieten</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 310;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Abrechnung auf den Übergabetag</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 311;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Originale aller Unterlagen übergeben lassen (idealerweise am Tag des Besitzübergangs)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>



        <tr>
          <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
          <th class="border bg-gray" colspan="3" >Interne ToDo</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
        <?php
        $j = 312;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Dateistruktur auf Laufwerk S:\ anlegen und befüllen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 313;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Ergänzungen in Flowfact Dokumentenordner Zuweisung anpassen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 314;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Mieterliste vom Vorbesitzer angleichen in Excelvorlage abspeichern. <b>Mieterliste</b> folgt monatlich von Hausverwaltung und ist im Objektordner abzulegen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 315;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Klären, wer die Verwaltung übernimmt (bisheriger Verwalter oder Hausmaxx)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 316;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Klären, welche Versicherung (bisherige übernehmbar? Via finanzierende Bank? AXA? Andere?</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <tr>
          <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
          <th colspan="7">Nacharbeiten zum Notartermin</th>
        </tr>
        <tr>
          <th class="border bg-gray" colspan="3" >Nacharbeit</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
        <?php
        $j = 317;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">ggf. Kaufpreisfälligkeit anmahnen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 318;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">nach Zahlung -> ggfs. Grundbuchumschreibung anmahnen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 319;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Konvolut erstellen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 320;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Negativzeugnis bezahlen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 321;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Grunderwerbsteuer bezahlen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>



        <tr>
          <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
          <th class="border bg-gray" colspan="3" >Vorbesitzer / Hausverwaltung</th>
          <th class="border text-center bg-gray" colspan="3">Asset Manager</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
        <?php
        $j = 322;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Kautionskonten eröffnen, falls notwendig</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 323;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Versicherung ab BNL vorhalten</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 324;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Neue IBAN Hausverwaltung / Vorbesitzer mitteilen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 325;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Kommanditeinlage bezahlt? TR fragen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($as_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <tr>
          <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
          <th colspan="7">Interne ToDo</th>
        </tr>
        <tr>
          <th class="border bg-gray" colspan="3" >IP / PR</th>
          <th class="border text-center bg-gray" colspan="3">Operations Manager</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>

        <?php
        $j = 326;
        $om_users = array();
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Website aktualisieren</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($om_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($om_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($om_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 327;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Präsentation aktualisieren</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($om_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($om_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($om_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 328;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Newsletter versenden</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($om_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($om_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($om_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>





        <tr>
          <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
          <th class="border bg-gray" colspan="3" >Finanzierung / Bank</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
        <?php
        $j = 329;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Notarvertrag zusenden</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 330;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Kreditverträge unterschreiben</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 331;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Modalitäten der <b>Darlehensauszahlung klären!</b> (Vorsicht: Beginn Darlehenslaufzeit vs. BNL)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 332;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Fälligkeitsmitteilung nebst Auftrag zur Kaufpreiszahlung sofort nach Erhalt zuleiten (ggf. mit Terminierung)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <tr>
          <td colspan="7">&nbsp;</td>
        </tr>
        <tr>
          <th class="border bg-gray" colspan="3" >ToDo mit Mietern</th>
          <th class="border text-center bg-gray" colspan="3">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>

        <?php
        $j = 333;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Vollständigkeit der Mieteingänge prüfen</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>


        <?php
        $j = 334;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Mietrückstände feststellen und Maßnahmen mit der Hausverwaltung einleiten. (Falls Lücken beim Übergang entstanden sind)</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>

        <?php
        $j = 335;
        ?>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-einkauf" data-type="{{$j}}" 
            @if(isset($buy_data[$j]['status']) &&  $buy_data[$j]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">ggf. Hausmeisterservice wechseln / inscourcen<br>
ggf. neuer Arbeitsvertrag mit Hausmeister
</td>
          <td class="border checkbox-td">
              <select class="change-in-user" data-type="{{$j}}" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user1']) &&  $buy_data[$j]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user2']) &&  $buy_data[$j]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-in-user" data-type="{{$j}}" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($buy_data) && isset($buy_data[$j]['user3']) &&  $buy_data[$j]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/'.$j) }}" data-title="Kommentar">{{ @$buy_data[$j]['comment'] }}</a>
          </td>
        </tr>



          </tbody>
        </table>




    </div>


  </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="irelease-property" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <h4>Möchtest du dieses Objekt wirklich freigeben?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary irelease-the-property" data-dismiss="modal" >Ja</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="irelease-property2" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <h4>Möchtest du dieses Objekt wirklich freigeben?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary irelease-the-property2" data-dismiss="modal" >Ja</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

@section('js')

 @parent
   <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
  <script type="text/javascript">
 
$(document).ready(function() {

  var total1 = 0;
  $('.total1').each(function(){
      if($(this).html() != "" && $(this).html() != "Empty")
        total1 += parseFloat($(this).html());
  })
  $('.sum_total1').html(total1);

  var total2 = 0;
  $('.total2').each(function(){
      if($(this).html() != "" && $(this).html() != "Empty")
        total2 += parseFloat($(this).html());
  })
  $('.sum_total2').html(total2);

  // $('.isend-mail').click(function(){
  //   property_id = $('.property_id').val();
  //   $.ajax({
  //       type : 'POST',
  //       url : "{{url('property/einkaufsendmail') }}",
  //       data : {property_id:property_id,  _token : '{{ csrf_token() }}' },
  //       success : function (data) {
  //           alert(data);
  //       }
  //   });
  // })

  // $('.isend-mail2').click(function(){
  //   property_id = $('.property_id').val();
  //   $.ajax({
  //       type : 'POST',
  //       url : "{{url('property/einkaufsendmail2') }}",
  //       data : {property_id:property_id,  _token : '{{ csrf_token() }}' },
  //       success : function (data) {
  //           alert(data);
  //       }
  //   });
  // })

  // $('.irelease-the-property').click(function(){
  //   property_id = $('.property_id').val();
  //   $.ajax({
  //       type : 'POST',
  //       url : "{{url('property/einkaufsendmail') }}",
  //       data : {property_id:property_id,release:1,  _token : '{{ csrf_token() }}' },
  //       success : function (data) {
  //           // alert(data);
  //           var path = $('#path-properties-show').val();
  //           window.location.href = path + '?tab=einkauf_tab';
  //       }
  //   });
  // })

  // $('.irelease-the-property2').click(function(){
  //   property_id = $('.property_id').val();
  //   $.ajax({
  //       type : 'POST',
  //       url : "{{url('property/einkaufsendmail2') }}",
  //       data : {property_id:property_id,release:1,  _token : '{{ csrf_token() }}' },
  //       success : function (data) {
  //           // alert(data);
  //           var path = $('#path-properties-show').val();
  //           window.location.href = path + '?tab=einkauf_tab';
  //       }
  //   });
  // })



  


  $.fn.editable.defaults.ajaxOptions = {type: "POST"};


    $('.mbuy-sheet').find('a.inline-edit').editable({
        success: function(response, newValue) {
          setTimeout(function(){
          var total1 = 0;
          $('.total1').each(function(){
              if($(this).html() != "" && $(this).html() != "Empty")
                total1 += parseFloat($(this).html());
          })
          $('.sum_total1').html(total1);

          var total2 = 0;
          $('.total2').each(function(){
              if($(this).html() != "" && $(this).html() != "Empty")
                total2 += parseFloat($(this).html());
          })
          $('.sum_total2').html(total2);
        },500);
        }
    });



 
$('.checkbox-einkauf').on('click', function () {
    var type = $(this).attr('data-type');
    v = 0;
    if($(this).is(':checked'))
        v = 1;

    property_id = $('.property_id').val();

    $.ajax({
        type : 'POST',
        url : "{{url('einkauf-item/update') }}/"+property_id+'/'+type,
        data : {type:type,pk : 'status',value:v, property_id:property_id,  _token : '{{ csrf_token() }}' },
        success : function (data) {
            generate_option_from_json(data, 'country_to_state');
        }
    });
});
$('.change-in-user').on('change', function () {
    type = $(this).attr('data-type');
    column = $(this).attr('column-name')
    v = $(this).val();
    
    property_id = $('.property_id').val();

    $.ajax({
        type : 'POST',
        url : "{{url('einkauf-item/update') }}/"+property_id+'/'+type,
        data : {type:type,pk : column,value:v, property_id:property_id,  _token : '{{ csrf_token() }}' },
        success : function (data) {
            generate_option_from_json(data, 'country_to_state');
        }
    });
});

$('.change-in-user').on('change', function () {
    column = 'saller_id';
    v = $(this).val();
    property_id = $('.property_id').val();

    $.ajax({
        type : 'POST',
        url : "{{url('einkauf/update') }}/"+property_id,
        data : {pk : column,value:v, property_id:property_id,  _token : '{{ csrf_token() }}' },
        success : function (data) {
            generate_option_from_json(data, 'country_to_state');
        }
    });
});

});


$('.show-link').click(function(){

      var cl = $(this).attr('data-class');
      if($('.'+cl).hasClass('hidden')){
          $(this).find('i').removeClass('fa-angle-down');
          $(this).find('i').addClass('fa-angle-up');
          $('.'+cl).removeClass('hidden');

      }
      else{
          // $(this).html('<i class="fa fa-angle-down"></i>');
          $(this).find('i').addClass('fa-angle-down');
          $(this).find('i').removeClass('fa-angle-up');
          $('.'+cl).addClass('hidden');
          // $(this).next().addClass('hidden');
      }
})
</script>
@endsection