
@if(count($mietflachen)>0)

<form action="{{route("properties.mietflachen")}}" method="post">
 <input type="hidden" name="property_id" value="{{ $id }}">
{!! csrf_field() !!}
  <div class="form-group col-sm-12">
    <label>Straße</label>
    <input type="text" name="street" value="{{ $mietflachen[0]->street }}" class="form-control" >
  </div>
  <div class="form-group col-sm-6">
     <label>POSTLEITZAHL</label>
    <input type="text" name="zip" value="{{ $mietflachen[0]->zip }}" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
       <label>Stadt</label>
    <input type="text" name="city" value="{{ $mietflachen[0]->city }}" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
       <label>Miete</label>
    <input type="text" name="rent" value="{{ $mietflachen[0]->rent }}" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
       <label>Preis auf Anfrage</label>
    <input type="text" name="demand_price" value="{{ $mietflachen[0]->demand_price }}" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
       <label>Gesamtfläche</label>
    <input type="text" name="total_area" value="{{ $mietflachen[0]->total_area }}" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
       <label>Verkaufsbereich</label>
    <input type="text" name="selling_area" value="{{ $mietflachen[0]->selling_area }}" class="form-control" >
  </div>
   <div class="form-group col-sm-6">
     <label>Mietfläche</label>
    <input type="text" name="renting_area" value="{{ $mietflachen[0]->renting_area }}" class="form-control" >
  </div>
 <div class="form-group col-sm-6">
   <label>Baujahr</label>
    <input type="text" name="year_construction" value="{{ $mietflachen[0]->year_construction }}" class="form-control" >
  </div>
 <div style="text-align: right; margin-right: 14px;">
  <button type="submit" class="btn btn-success ">einreichen</button>
 </div>
   </div>
</form>

@else

 <form action="{{route("properties.mietflachen")}}" method="post">
 <input type="hidden" name="property_id" value="{{ $id }}">
{!! csrf_field() !!}
  <div class="form-group col-sm-12">
    <input type="text" name="street" placeholder="Straße" class="form-control" >
  </div>
  <div class="form-group col-sm-6">
    <input type="text" name="zip" placeholder="POSTLEITZAHL" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
    <input type="text" name="city" placeholder="Stadt" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
    <input type="text" name="rent" placeholder="Miete" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
    <input type="text" name="demand_price" placeholder="Preis auf Anfrage" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
    <input type="text" name="total_area" placeholder="Gesamtfläche" class="form-control" >
  </div>
    <div class="form-group col-sm-6">
    <input type="text" name="selling_area" placeholder="Verkaufsbereich" class="form-control" >
  </div>
   <div class="form-group col-sm-6">
    <input type="text" name="renting_area" placeholder="Mietfläche" class="form-control" >
  </div>
 <div class="form-group col-sm-6">
    <input type="text" name="year_construction" placeholder="Baujahr" class="form-control" >
  </div>
 <div style="text-align: right; margin-right: 14px;">
  <button type="submit" class="btn btn-success ">einreichen</button>
 </div>
   </div>
</form>

@endif