<table class="table table-striped summery-table" >
    <thead>
        <tr>
            <th>Mieter</th>
            <th class="text-right">SOLL</th>
            <th class="text-right">IST</th>
            <th class="text-right">Saldo</th>
        </tr>
    </thead>
    <tbody>
        @php $total_amount = 0;$total_amount1 = 0;$total_amount2 = 0; @endphp
        @if ($data)
            @foreach ($data as $key => $value)

                @php 
                if($value->ist)
                $total_amount += $value->ist;  
                if($value->soll)
                $total_amount1 += $value->soll;
                if($value->diff)
                $total_amount2 += $value->diff; 
                @endphp
        
                <tr>
                    <td><a href="javascript:void(0);" data-url="{{ route('get_rent_paid_monthwise_data', ['id' => $value->id]) }}" class="rent-paid-month-wise-detail">{{ $value->mieter }}</a></td>
                    <td class="text-right">{{ number_format($value->soll,2,",",".").' €' }}</td>
                    <td class="text-right">{{ number_format($value->ist,2,",",".").' €' }}</td>
                    <td class="text-right">{{ number_format($value->diff,2,",",".").' €' }}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
    <tfoot>
        <tr>
            <th class="text-right"></th>
            <th class="text-right">{{ number_format($total_amount1,2,",",".").' €' }}</th>
            <th class="text-right">{{ number_format($total_amount,2,",",".").' €' }}</th>
            <th class="text-right">{{ number_format($total_amount2,2,",",".").' €' }}</th>
        </tr>
    </tfoot>
    
</table>