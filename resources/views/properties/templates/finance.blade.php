<div id="tenancy-schedule">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#darlehensspiegel">Darlehensspiegel</a></li>
        <li><a data-toggle="tab" href="#liquiplanung">Liquiplanung</a></li>
        <li><a data-toggle="tab" href="#darlehensspiegelnew">Darlehensspiegel New</a></li>
        <li><a data-toggle="tab" href="#interest_and_repayment_plan">Zins- und Tilgungsplan</a></li>
    </ul>

    <div class="tab-content">
      <div id="darlehensspiegel" class="tab-pane fade in active">
          @include('properties.templates.darlehensspiegel')
      </div>
      <div id="liquiplanung" class="tab-pane fade">
          @include('properties.templates.liquiplanung')
      </div>
      <div id="darlehensspiegelnew" class="tab-pane fade">
          @include('properties.templates.darlehensspiegelnew')
      </div>
      <div id="interest_and_repayment_plan" class="tab-pane fade">
          @include('properties.templates.interest_and_repayment_plan')
      </div>
    </div>
</div>