@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <style type="text/css">
      .change-in-user{
        width: 80px !important;
      }
      
    </style>
@endsection
<div id="tenancy-schedule">
    <h1>Leerstandsflächen</h1>
<?php
// print_r($properties);
$name = $properties->name_of_property;
$seller_id = $properties->seller_id;
$ireleased_by = "";
$isale_date = "";
$ireleased_by2 = "";
$isale_date2 = "";
$street = "";
$postcode = "";
$city = "";

if($pb_data && $pb_data->property_name)
  $name = $pb_data->property_name;

$seller_id = $properties->transaction_m_id;
if($pb_data && $pb_data->saller_id)
  $seller_id = $pb_data->saller_id;



$street = $properties->strasse;
if($pb_data && $pb_data->street)
  $street = $pb_data->street;

if($pb_data && $pb_data->city)
  $city = $pb_data->city;

if($pb_data && $pb_data->postcode)
  $postcode = $pb_data->postcode;

if($pb_data && $pb_data->released_by)
  $ireleased_by = $pb_data->released_by;

if($pb_data && $pb_data->sale_date)
  $isale_date = $pb_data->sale_date;


if($pb_data && $pb_data->released_by2)
  $ireleased_by2 = $pb_data->released_by2;

if($pb_data && $pb_data->sale_date2)
  $isale_date2 = $pb_data->sale_date2;


?>

 
<input type="hidden" class="property_id" value="<?php echo $id?>">
<div class="row tenancy-schedules lbuy-sheet" style="margin-top: 20px;">
  <div class="col-sm-12">
 @php 
$check = false;
 @endphp
@if(isset($tenancy_schedule_data))
@foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
@foreach($tenancy_schedule->items as $item)
 
@if($item->type == 3 || $item->type == 4)

@php
$check = true;
@endphp
    <h3 class="title-tag">
  
           @php

            $immo_upload =  DB::table('immo_upload')->where('property_id', $item->id)->first();
            
           if(isset($immo_upload->property_id)){
              if($immo_upload->status == 'Active'){

                  $class ="color:green;";
              }else{
                  $class ="color:yellow;";
              }
           }
           else{

                $class ="color:red;";
           }
          @endphp

        <i class="fa fa-circle" style="{{$class}}" aria-hidden="true"></i>

    @if(is_null($item->name))
      empty
    @else
    {{ $item->name }}  
    @endif

        <a style="margin-left: 10px;" href="javascript:void(0)" data-class="sheetcustom-{{ $item->id }} " class="btn btn-info show-links pull-right">Vermietungsaktivitäten  <i class="fa fa-angle-down"></i></a>
        <a href="javascript:void(0)" data-class="sheet-{{ $item->id }} " class="btn btn-info show-links pull-right">IS Upload <i class="fa fa-angle-down"></i></a>
    </h3>

  <div class="sheet-{{ $item->id }}  hidden"> 
  @include('properties.templates.Immobilie')
  </div>


  <div class="customvacancy sheetcustom-{{ $item->id }}  hidden" data-url="{{ route('vacant_uploading_statuse', ['vacant_id' => $item->id]) }}"> 
  @include('properties.templates.customvacancy')
  </div>

    <hr>
 



@endif 


@endforeach
@endforeach  
@else


Keine Daten auf dieser Registerkarte verfügbar

@endif   
  

@if($check ==  false)
<h3>Keine Leerstandsflächen gefunden.</h3>
@endif


  </div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="irelease-property" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <h4>Möchtest du dieses Objekt wirklich freigeben?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary irelease-the-property" data-dismiss="modal" >Ja</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="irelease-property2" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <h4>Möchtest du dieses Objekt wirklich freigeben?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary irelease-the-property2" data-dismiss="modal" >Ja</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

@section('js')

 @parent
   <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
  <script type="text/javascript">
 
$(document).ready(function() {

  var total1 = 0;
  $('.total1').each(function(){
      if($(this).html() != "" && $(this).html() != "Empty")
        total1 += parseFloat($(this).html());
  })
  $('.sum_total1').html(total1);

  var total2 = 0;
  $('.total2').each(function(){
      if($(this).html() != "" && $(this).html() != "Empty")
        total2 += parseFloat($(this).html());
  })
  $('.sum_total2').html(total2);

  // $('.isend-mail').click(function(){
  //   property_id = $('.property_id').val();
  //   $.ajax({
  //       type : 'POST',
  //       url : "{{url('property/einkaufsendmail') }}",
  //       data : {property_id:property_id,  _token : '{{ csrf_token() }}' },
  //       success : function (data) {
  //           alert(data);
  //       }
  //   });
  // })

  // $('.isend-mail2').click(function(){
  //   property_id = $('.property_id').val();
  //   $.ajax({
  //       type : 'POST',
  //       url : "{{url('property/einkaufsendmail2') }}",
  //       data : {property_id:property_id,  _token : '{{ csrf_token() }}' },
  //       success : function (data) {
  //           alert(data);
  //       }
  //   });
  // })

  // $('.irelease-the-property').click(function(){
  //   property_id = $('.property_id').val();
  //   $.ajax({
  //       type : 'POST',
  //       url : "{{url('property/einkaufsendmail') }}",
  //       data : {property_id:property_id,release:1,  _token : '{{ csrf_token() }}' },
  //       success : function (data) {
  //           // alert(data);
  //           var path = $('#path-properties-show').val();
  //           window.location.href = path + '?tab=einkauf_tab';
  //       }
  //   });
  // })

  // $('.irelease-the-property2').click(function(){
  //   property_id = $('.property_id').val();
  //   $.ajax({
  //       type : 'POST',
  //       url : "{{url('property/einkaufsendmail2') }}",
  //       data : {property_id:property_id,release:1,  _token : '{{ csrf_token() }}' },
  //       success : function (data) {
  //           // alert(data);
  //           var path = $('#path-properties-show').val();
  //           window.location.href = path + '?tab=einkauf_tab';
  //       }
  //   });
  // })



  


  $.fn.editable.defaults.ajaxOptions = {type: "POST"};


    $('.lbuy-sheet').find('a.inline-edit').editable({
        success: function(response, newValue) {
          setTimeout(function(){
          var total1 = 0;
          $('.total1').each(function(){
              if($(this).html() != "" && $(this).html() != "Empty")
                total1 += parseFloat($(this).html());
          })
          $('.sum_total1').html(total1);

          var total2 = 0;
          $('.total2').each(function(){
              if($(this).html() != "" && $(this).html() != "Empty")
                total2 += parseFloat($(this).html());
          })
          $('.sum_total2').html(total2);
        },500);
        }
    });



 
$('.checkbox-einkauf').on('click', function () {
    var type = $(this).attr('data-type');
    v = 0;
    if($(this).is(':checked'))
        v = 1;

    property_id = $('.property_id').val();

    $.ajax({
        type : 'POST',
        url : "{{url('einkauf-item/update') }}/"+property_id+'/'+type,
        data : {type:type,pk : 'status',value:v, property_id:property_id,  _token : '{{ csrf_token() }}' },
        success : function (data) {
            generate_option_from_json(data, 'country_to_state');
        }
    });
});
/*$('.change-in-user').on('change', function () {
    type = $(this).attr('data-type');
    column = $(this).attr('column-name')
    v = $(this).val();
    
    property_id = $('.property_id').val();

    $.ajax({
        type : 'POST',
        url : "{{url('einkauf-item/update') }}/"+property_id+'/'+type,
        data : {type:type,pk : column,value:v, property_id:property_id,  _token : '{{ csrf_token() }}' },
        success : function (data) {
            generate_option_from_json(data, 'country_to_state');
        }
    });
});




$('.change-in-user').on('change', function () {
    column = 'saller_id';
    v = $(this).val();
    property_id = $('.property_id').val();

    $.ajax({
        type : 'POST',
        url : "{{url('einkauf/update') }}/"+property_id,
        data : {pk : column,value:v, property_id:property_id,  _token : '{{ csrf_token() }}' },
        success : function (data) {
            generate_option_from_json(data, 'country_to_state');
        }
    });
});*/

});

 
</script>
@endsection


@section('email_template_javascript1')
<script>
$('body').on('click','.show-links',function(){

      var cl = $(this).attr('data-class');
      if($('.'+cl).hasClass('hidden')){
          $(this).find('i').removeClass('fa-angle-down');
          $(this).find('i').addClass('fa-angle-up');
          $('.'+cl).removeClass('hidden');

      }
      else{
          // $(this).html('<i class="fa fa-angle-down"></i>');
          $(this).find('i').addClass('fa-angle-down');
          $(this).find('i').removeClass('fa-angle-up');
          $('.'+cl).addClass('hidden');
          // $(this).next().addClass('hidden');
      }
});

$('body').on('change', '.vacant_status_checkbox', function () {
    var value = ($(this).prop('checked') == true) ? 1 : 0;
    var type = $(this).val();
    var field = $(this).attr('data-field');
    var url = $(this).closest('.customvacancy').attr('data-url');

    var data = {
      field: field,
      value: value,
      type: type,
      input: 'checkbox'
    };

    addOrUpdateVacantStatus(url, data, $(this));
    
});

$('body').on('keyup', '.vacant_status_input', function () {
  var value = $(this).val();
  var type = $(this).attr('data-type');
  var field = $(this).attr('data-field');
  var url = $(this).closest('.customvacancy').attr('data-url');

  var data = {
      field: field,
      value: value,
      type: type,
      input: 'text'
  };

  addOrUpdateVacantStatus(url, data, $(this));
});

var url_uploadfiles           = '{{ route('uploadfiles') }}';


$('body').on('click', '.delete-upload-image', function () {
  var $this = $(this);
  if(confirm('Are you sure want to delete?'))
  {
    
    var type = $(this).attr('data-value');
    var field = $(this).attr('data-field');
    var url = $(this).closest('.customvacancy').attr('data-url');
    var data = {
        field: field,
        value: "",
        type: type,
        input: 'file'
      };

      addOrUpdateVacantStatus(url, data, $this);
      $($this).closest('.row').find('.updated_date').text('');
      $(this).closest('.preview-img').html("");
  }
})
    
$('body').on('change', '.upload-status-file', function () {

    var type = $(this).attr('data-value');
    var field = $(this).attr('data-field');
    var url = $(this).closest('.customvacancy').attr('data-url');
    $this = $(this);

    var formData = new FormData();
    $.each(jQuery(this)[0].files, function(i, file) {
        formData.append('file[' + i + ']', file);
    });
    $.ajax({
        url: url_uploadfiles, //Server script to process data
        type: 'POST',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(json) {
            $($this).val('');
            $.each(json.files, function(index, value) {
                // var str = '<input type="hidden" name="img[]" value="' + json.filename[index] + '">';
                if(type == '6'){
                  $this.closest('.row').find('.preview-img').html("<a href='" + value + "' target='_blank'>Download</a><br><a href='javascript:void(0)'' class='delete-upload-image' data-field='comment' data-value='"+type+"'><i class='fa fa-times'></i></a>");
                }else{
                  $this.closest('.row').find('.preview-img').html("<a href='" + value + "' target='_blank'><img src='" + value + "' style='width:50px; height:50px;' ></a><br><a href='javascript:void(0)'' class='delete-upload-image' data-field='comment' data-value='"+type+"'><i class='fa fa-times'></i></a>");
                }

                var data = {
                  field: field,
                  value: json.filename[index],
                  type: type,
                  input: 'file'
                };

                addOrUpdateVacantStatus(url, data, $this);

            });
        }
    });


   
    
    
});

var latest_comment;
$('body').on('click', '.btn-rental-activity-comment', function () {
  latest_comment = $(this).closest('.row').find('.latest-comment');
  var url = $(this).attr('data-url');
  var id = $(this).attr('data-id');

  $('#rental-activity-form').attr('action', url);
  $('#rental-activity-form').attr('data-id', id);
  $('#rental-activity-modal').modal('show');

  loadComment(id);
});

$('#rental-activity-form').submit(function(event) {
    event.preventDefault();
    var $this = $(this);
    var url = $(this).attr('action');
    var id = $(this).attr('data-id');

    var data = {
      comment: $('#rental-activity-comment').val(),
      type: 8,
    };

    $.ajax({
      url: url,
      type: 'POST',
      data: data,
      dataType: 'json',
      success: function(result){
        if(result.status){
          $('#rental-activity-comment').val('');
          loadComment(id);
          $(latest_comment).html(data.comment);
          // $('#rental-activity-modal').modal('hide');
        }else{
          alert(result.message);
        }
      },
      error: function(error){
        alert('Somthing want wrong');
      }
    });
});

function addOrUpdateVacantStatus(url, data, $this){
  
  $.ajax({
      url: url,
      type: 'POST',
      data: data,
      dataType: 'json',
      success: function(result){
        if(result.status){
          if(data.input == 'text'){
            if(data.value != ''){
              if (!data.value.match(/^[a-zA-Z]+:\/\//))
              {
                  data.value = 'http://' + data.value;
              }
              $($this).closest('.row').find('.link').html('<a href="'+data.value+'" target="blank"><i class="fa fa-external-link" style="margin-top: 13px;"></i></a>');
            }else{
              $($this).closest('.row').find('.link').empty();
            }
          }

          if(data.field == 'comment'){
            if(data.value == ""){
              $($this).closest('.row').find('.updated_date').text('');
            }else{
              $($this).closest('.row').find('.updated_date').text(result.data.date);
            }
          }
        }else{
          alert(result.message);
        }
      },
      error: function(error){
        alert('Somthing want wrong');
      }
  });
}

function loadComment(id){
  if ( ! $.fn.DataTable.isDataTable('#rental-activity-table') ) {
    $('#rental-activity-table').dataTable({
        "ajax": {
          "url": '{{ route('get_rental_activity_comment') }}?id='+id,
          /*"data": function ( d ) {
            d.id = id;
          }*/
        },
        "order": [
            [1, 'desc']
        ],
        "columns": [
            null,
            {"type": "new-date-time"},
        ]
    });
  }else{
    $('#rental-activity-table').DataTable().ajax.url('{{ route('get_rental_activity_comment') }}?id='+id).load();
    // $('#rental-activity-table').DataTable().ajax.reload();
  }
}

$(document).ready(function(){
  $('.upload-status-section .link a').each(function(){
    v = $(this).attr('href');
    if (!v.match(/^[a-zA-Z]+:\/\//))
    {
        v = 'http://' + v;
        $(this).attr('href',v);
    }
  })
})

</script>
@endsection


