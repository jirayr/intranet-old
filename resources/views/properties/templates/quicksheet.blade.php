<div id="tenancy-schedule">


        <?php
        $bank_array = array();
        $j = 0;
        $ist = 0;
        foreach($banks as $key => $bank)
        {
            $bank_array[] = $bank->id;
        }

        array_unique($bank_array);
        $str = implode(',', $bank_array);

        $property_sheet = DB::table('properties')->whereRaw('main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('standard_property_status','desc')->first();


        ?>

    <div class="tab-content">

        <?php
        $j=0;
        ?>


            <?php

            $list_fields_percent = [
                'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
            ];
            foreach ($list_fields_percent as $field) {
                $property_sheet->$field *= 100;
            }


            $property_sheet->plz_ort = $properties->plz_ort;
            $property_sheet->ort = $properties->ort;
            $property_sheet->strasse = $properties->strasse;
            $property_sheet->hausnummer = $properties->hausnummer;
            $property_sheet->niedersachsen = $properties->niedersachsen;


            

            ?>

            <div class="tab-pane active">


                <div class="row">

                    

                    @php
                        $j = 1;
                        $sheet_property = $property_sheet;
                        $id = $property_sheet->id;
                    @endphp
                    <div style="width: 100%"  class="col-md-10 col-xs-12">

                        <form action="{{route('save_gsheet')}}" method="post" class="form-horizontal" accept-charset="UTF-8" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="main_property_id" value="{{ $property_sheet->main_property_id }}">
                            {{--First Row--}}
                            <div class="form-group m-form__group row quick-sheet-form">
                                <div class="col-lg-6">
                                    <label>Adresse:</label>
                                    <input type="text" id="google_address" name="google_address" class="form-control form-control-line input-text" value="" placeholder="Addresse suchen.">
                                    <input type="hidden" name="location_latitude" id="location_latitude" >
                                    <input type="hidden" name="location_longitude" id="location_longitude" >

                                </div>
                            </div>
                            <div class="form-group m-form__group row quick-sheet-form">
                                <div class="col-lg-3">
                                    <label>PLZ:</label>
                                    <input type="text" class="form-control" id="plz_ort" value="@if(isset($sheet_property)){{ $sheet_property->plz_ort }}@endif" name="plz_ort">
                                </div>
                                <div class="col-lg-3">
                                    <label class="">Ort:</label><br>
                                    <select class="form-control city-search" name="ort" style="width: 70%">
                                        @if(isset($sheet_property) && $sheet_property->ort)
                                            <option value="<?php echo $sheet_property->ort;?>"><?php echo $sheet_property->ort;?></option>
                                        @endif
                                    </select>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#add-city" class="btn-sm btn-success add_ort">Hinzufügen</a>
                                </div>
                                <div class="col-lg-3">
                                        <label class="">Bundesland:<br> </label>

                                        <?php
                                    $stats = array('Baden-Württemberg',
                                        'Bayern',
                                        'Berlin',
                                        'Brandenburg',
                                        'Bremen',
                                        'Hamburg',
                                        'Hessen',
                                        'Mecklenburg-Vorpommern',
                                        'Niedersachsen',
                                        'Nordrhein-Westfalen',
                                        'Rheinland-Pfalz',
                                        'Saarland',
                                        'Sachsen',
                                        'Sachsen-Anhalt',
                                        'Schleswig-Holstein',
                                        'Schweiz',
                                        'Thüringen',
                                        'Österreich','Spanien');
                                    $n_array =     array('Baden-Württemberg'=>'5,0',
                                        'Bayern'=>'3,5',
                                        'Berlin'=>'6,0',
                                        'Brandenburg'=>'6,5',
                                        'Bremen'=>'5,0',
                                        'Hamburg'=>'4,5',
                                        'Hessen'=>'6,0',
                                        'Mecklenburg-Vorpommern'=>'6,0',
                                        'Niedersachsen'=>'5,0',
                                        'Nordrhein-Westfalen'=>'6,5',
                                        'Rheinland-Pfalz'=>'5,0',
                                        'Saarland'=>'6,5',
                                        'Sachsen'=>'3,5',
                                        'Sachsen-Anhalt'=>'5,0',
                                        'Schleswig-Holstein'=>'6,5',
                                        'Schweiz'=>0,
                                        'Thüringen'=>'6,5',
                                        'Österreich'=>'3,5','Spanien'=>10);
                                    ?>
                                        <select class="form-control bundesland" name="niedersachsen" style="width: 100%" id="niedersachsen">
                                            <option value="">Bundesland</option>
                                            @foreach($stats as $list)
                                                <option data-id="{{@$n_array[$list]}}" value="{{ $list }}" @if(isset($sheet_property) && $sheet_property->niedersachsen==$list)selected @endif>{{ $list }}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" class="form-control real_estate_taxes mask-number-input" id="real_estate_taxes" value="@if(isset($sheet_property)){{ number_format($sheet_property->real_estate_taxes,2,',','.') }}@endif" name="real_estate_taxes">
                                   </div>
                                   <div class="col-lg-3">
                                        <label class="">Strasse:</label>
                                        <input type="text" class="form-control" id="strasse" value="@if(isset($sheet_property)){{ $sheet_property->strasse }}@endif" name="strasse">
                                    </div>
                                    <div class="col-lg-3">
                                       <label>Hausnummer:</label>
                                       <input type="text" class="form-control" id="hausnummer" value="@if(isset($sheet_property)){{ $sheet_property->hausnummer }}@endif" name="hausnummer">
                                   </div>
                                   <div style="margin-bottom: 10px;" class="clearfix"></div>
                                   <div class="col-lg-3">
                                        <label  style="">Netto Miete (IST) p.a.:</label>
                                        <input type="text" class="form-control mask-number-input" id="net_rent_pa" value="@if(isset($sheet_property)){{ number_format($sheet_property->net_rent_pa,2,',','.') }}@endif" name="net_rent_pa">
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Vermietbare Fläche:</label>
                                        <input type="text" class="form-control mask-number-input" id="Rental_area_in_m2" value="@if(isset($sheet_property)){{ number_format($sheet_property->Rental_area_in_m2,2,',','.') }}@endif" name="Rental_area_in_m2">
                                    </div>
                                    <div class="col-lg-3">
                                        <label  style="font-weight: bold;color: red;">Kaufpreis:</label>
                                        <input type="text" class="form-control mask-number-input" id="gesamt_in_eur" value="@if(isset($sheet_property)){{ number_format($sheet_property->gesamt_in_eur,2,',','.') }}@endif" name="gesamt_in_eur">
                                    </div>

                                    

                                    <div class="col-lg-3">
                                        <label  style="">Maklerpreis:</label>
                                        <input type="text" class="form-control mask-number-input" id="maklerpreis" value="@if(isset($sheet_property)){{ number_format($sheet_property->maklerpreis,2,',','.') }}@endif" name="maklerpreis">
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="">Angeboten über:</label>

                                        <?php
                                    $a = array('Falk','Andrea','info@','Makler','Sonstiges','Eigentümer','einkauf.fcr');
                                    
                                    ?>
                                        <select class="form-control" name="offer_from">
                                            <option value="">Angeboten über</option>
                                            @foreach($a as $list)
                                                <option value="{{ $list }}" @if(isset($sheet_property) && $sheet_property->offer_from==$list)selected @endif>{{ $list }}</option>
                                            @endforeach
                                        </select>
                                        
                                   </div>
                                     
                                <div style="margin-bottom: 10px;" class="clearfix"></div>
                                
                                
                                
                                   

                                <div class="col-lg-3">
                                    <label class="">Objektname:</label>
                                    <input type="text" class="form-control" id="name_of_property" value="@if(isset($sheet_property)){{ $sheet_property->name_of_property }}@endif" name="name_of_property">
                                </div>
                                <div class="col-lg-3">
                                        <label class="">Objekttyp:</label>

                                        <?php
                                    $a = array('shopping_mall'=>'Einkaufszentrum', 'retail_center'=>'Fachmarktzentrum','specialists'=>'Fachmarkt', 'retail_shop'=>'Einzelhandel','office'=>'Büro','logistik'=>'Logistik','commercial_space'=>'Kommerzielle Fläche','land'=>'Grundstück','apartment'=>'Wohnung','condos'=>'Eigentumswohnung','house'=>'Haus','living_and_business'=>'Wohn- und Geschäftshaus','villa'=>'Villa','hotel'=>'Hotel','nursing_home'=>'Pflegeheim', 'multifamily_house'=>'Mehrfamilienhaus','dormitory'=> 'Studentenwohnheim','other'=>'Sonstiges');
                                    
                                    ?>
                                        <select class="form-control" name="property_type">
                                            <option value="">Objekttyp</option>
                                            @foreach($a as $k=>$list)
                                                <option value="{{ $k }}" @if(isset($sheet_property) && $sheet_property->property_type==$k)selected @endif>{{ $list }}</option>
                                            @endforeach
                                        </select>
                                        
                                   </div>

                                   
                                   
                                
                                
                                <div class="col-lg-3">
                                    <label  style="">TM:</label>
                                        <select name="transaction_m_id" class="form-control">
                                        <option value="">{{__('dashboard.transaction_manager')}}</option>
                                        @foreach($tr_users as $list1)
                                        @if($sheet_property && $sheet_property->transaction_m_id==$list1->id)
                                        <option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
                                        @else
                                        <option value="{{$list1->id}}">{{$list1->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label class="">Vorname:</label>
                                    <input type="text" class="form-control" id="first_name" value="@if(isset($sheet_property)){{ $sheet_property->first_name }}@endif" name="first_name">
                                </div>
                                
                                <div class="col-lg-3">
                                    <label class="">Nachname:</label>
                                    <input type="text" class="form-control" id="anbieterkontakt" value="@if(isset($sheet_property)){{ $sheet_property->anbieterkontakt }}@endif" name="anbieterkontakt">
                                </div>
                                <div class="col-lg-3">
                                    <label class="">Firma:</label>
                                    <input type="text" class="form-control" id="firma" value="@if(isset($sheet_property)){{ $sheet_property->firma }}@endif" name="firma">
                                </div>

                                <div class="col-lg-3">
                                    <label class="">Investor:</label>
                                    <input type="text" class="form-control" id="investor_name" value="@if(isset($sheet_property)){{ $sheet_property->investor_name }}@endif" name="investor_name">
                                    
                                </div>

                                @if($sheet_property->status == 6)
                                    <div class="col-lg-3" style="padding-top: 30px;">
                                        <input type="checkbox" name="is_not_an_existing" value="1" id="is_not_an_existing" {{ (isset($sheet_property->is_not_an_existing) && $sheet_property->is_not_an_existing == 1) ? 'checked' : '' }} >
                                        <label for="is_not_an_existing">Kein Bestandsobjekt</label>
                                    </div>
                                @endif

                                <div style="margin-bottom: 10px;" class="clearfix"></div>
                                <div class="col-lg-6">
                                    <label class="">Notizen:</label>
                                    <textarea rows="5" class="form-control" name="notizen">@if(isset($sheet_property)){{ $sheet_property->notizen }}@endif</textarea>
                                </div>
                                <!-- add_new_property_comment -->

                                <div class="col-lg-6">
                                    <label class="">Kommentar TM:</label>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#add_new_property_comment" style="color: white;" class="  btn btn-primary add-new-p-comment">Neuer Kommentar </a>
                                    <br>
                                    <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;"  >
                                        <table class="forecast-table logtable">
                                            <thead>
                                                <tr>
                                                    <th class="bg-light-gray">Name</th>
                                                    <th class="bg-light-gray">Kommentar</th>
                                                    <th class="bg-light-gray">Datum</th>
                                                    <th class="bg-light-gray">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody class="propertyappenddata"></tbody>
                                        </table>
                                    </div>

                                </div>



                                


                                
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-primary">Speichern</button>

                                </div>
                            </div>

                        </form>

                        <h1>Anhänge</h1>

                        <form action="{{ url('property_upload_pdf') }}" method="post" enctype="multipart/form-data">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input type="hidden" name="property_id" value="{{ $properties->id }}">

                            <div class="form-group">
                                <label>Datei hochladen (max. 20 MB)</label>
                                <input type="file" name="upload_pdf">
                            </div>

                            <input type="submit" name="" value="Hochladen" class="btn btn-success" style="margin-bottom:  10px;">
                            
                        </form>


                        @foreach($properties_pdf as $list)
                                <div class="creating-ads-img-wrap media-common-class" style="height: 185px;width: 215px;">
                                    
                                    <a href="#" class="inline-edit pdf-edit" data-type="text" data-pk="upload_file_name" data-placement="bottom" data-url="{{url('property/update_pdf_name/'.$list->id) }}" data-title="Name des Anhangs">
                                    {{$list->upload_file_name}}</a>

                                    <a data-id="{{ $list->id }}" href="javascript:;" class="pdfDeleteBtn"><i class="fa fa-trash-o"></i> </a>
                                    
                                    <a href="{{ $list->file_href }}" class="img-responsive pdf-file-icon text-center" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
                                   
                                </div>
                        @endforeach




                    </div>
                </div>
            </div>


    </div>

    
</div>





