
<div class="">
	<div class="modal-dialog modal-lg" style="width: 100%">
	    <div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Hausverwaltung</h4>
			</div>
			<div class="modal-body">
				<div class="row ">

					<button style="margin-left: 2%" type="button" class="btn btn-success btn-xs" onclick="add_property_management(1)">Hinzufügen</button>

					@if(Auth::user()->email!=config('users.falk_email'))

            <button class="btn btn-primary btn-xs @if(isset($mails_log_data['management'])) btn-success @else btn-primary @endif @if(!isset($mails_log_data['release_management'])) management-request-button @endif" data-id="management">
              @if(!isset($mails_log_data['release_management']))  
                Zur Freigabe an Falk senden 
              @else 
                Freigegeben 
              @endif
            </button>

					@else

            <!-- <button class="btn  btn-xs   @if(!isset($mails_log_data['release_management'])) btn-primary management-request-button @else btn-success  @endif" data-id="release_management">
              @if(!isset($mails_log_data['release_management'])) 
                Freigeben 
              @else 
                Freigegeben 
              @endif 
            </button> -->

					@endif

          @if(Auth::user()->email == config('users.falk_email'))
            <!-- <button type="button" class="btn btn-primary btn-xs" id="btn-propert-management-not-release">Ablehnen</button> -->
          @endif

					<div class="management-table-list">
					</div>

				</div>
			</div>
		</div>
	</div>
</div>	

<div class="tenancy-schedules" style="display: block;">
	<h3 style="margin-top: 40px;">Verlauf</h3>
	<div class="clearfix"></div>
  <table class="forecast-table" >
    <thead>
      <tr>
        <th class="border bg-gray">Name</th>
        <th class="border bg-gray">Button</th>
        <th class="border bg-gray">Kommentar</th>
        <th class="border bg-gray">Datum</th>
        <th class="border bg-gray">Aktion</th>
      </tr>
    </thead>
    <tbody>
      @php
        $user = Auth::user();
      @endphp

      @foreach($mails_logs as $k=>$list)
      
      <?php
      $tname = "";
      $u = DB::table('users')->where('id', $list['user_id'])->first();
      if($u)
        $tname = $u->name;

      $rbutton_title = "";
      if($list['type']=="management")
      	$rbutton_title = "Zur Freigabe an Falk gesendet";
      if($list['type']=="release_management")
        $rbutton_title = "Freigegeben";
      if($list['type']=="not_release_property_management")
        $rbutton_title = "Ablehnen";
      ?>
      @if($rbutton_title)
      <tr>
        <td class="border">{{$tname}}</td>
        <td class="border">{{$rbutton_title}}</td>
        <td class="border">{{$list['comment']}}</td>
        <td class="border">{{show_datetime_format($list['created_at'])}}</td>
        <td class="border">
          @if($user->email == config('users.falk_email') && $list['user_id'] == $user->id)
            <button type="button" data-id="{{ $list['id'] }}" data-tbl="properties_mail_logs" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>
          @endif
        </td>
      </tr>
      @endif
      @endforeach
    </tbody>
  </table>
	</div>
