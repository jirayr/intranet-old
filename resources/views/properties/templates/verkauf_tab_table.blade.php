    {{-- Gewinn total value calculation --}}
    @php
       $verkaufspreis_value = 0;
       $ankaufspreis_value = $excelFileCellD42;
       $investitionskosten_value = 0;
       $vfe_value = 0;
    @endphp
 
 
    @if(isset($verkauf_tab))
    @php
    $verkaufspreis = $verkauf_tab->where('row_name','verkaufspreis')->first();
    @endphp
    @if(!is_null($verkaufspreis)) 
    @php
    $verkaufspreis_value = $verkaufspreis->tab_value;
    @endphp
    @endif
    @endif
    

    <?php
    $o = 0;
    ?>
    
    @if(isset($verkauf_tab))
    @php
        $investitionskosten = $verkauf_tab->where('row_name','investitionskosten')->first();
            if ($investitionskosten)
            {
                $investitionskosten_value = $investitionskosten->tab_value;
            }

            $vfe = $verkauf_tab->where('row_name','VFE (Athora LV)')->first();
            if ($vfe)
            {
                $vfe_value = $vfe->tab_value;
            }

    @endphp
    <?php
    
    foreach($verkauf_tab as $kauf_tab)
      if($kauf_tab->new_added == 1 && $kauf_tab->tab_value)
        $o += $kauf_tab->tab_value;


      ?>

    

    @endif

    @php 
    $total_gwn_value = ($verkaufspreis_value)-($ankaufspreis_value)-($investitionskosten_value)-($vfe_value)-$o;

    @endphp
  
    <div class="col-sm-8" id="reload_div_resp" >
     
        <label style="background-color: #808080; padding: 5px; border: 2px solid #000; width: 100%; margin-bottom: 0;">
            BNL des Objektes bei der FCR Immobilien AG
        </label>

    <div class="col-sm-8" style="background-color: #ffffff; border-left: 2px solid #000; border-right: 2px solid #000; border-bottom: 2px solid #000; width: 100%;">  
    <div class="row">
    <div class="col-sm-9">
        Verkaufspreisfaktor

    </div>
   

    <div class="col-sm-3 text-right">
      
      @php 
      $row_11_value = 1;
      @endphp

      @isset($verkauf_tab)
      @php 
      $row_11 = $verkauf_tab->where('row_name','row_11')->first();
      @endphp
      @if(!is_null($row_11)) 
        @php 
         $row_11_value =  $row_11->tab_value;
         if($row_11_value == 0){
          $row_11_value = 1;
           }
        @endphp
      @endif 
      @endisset

        <?php
        $athora = $verkauf_tab->where('row_name','VFE (Athora LV')->first();
         ?>
        <?php if(!is_null($athora)): ?>
        <?php
            $athora_value = $athora->tab_value;
            ?>

    <?php endif; ?>
        <?php
        $verkaufspreis = $verkauf_tab->where('row_name','verkaufspreis')->first();
        ?>
        @isset($verkauf_tab)

            @php
                $Verkaufspreisfaktor= null;
                $vfe = $verkauf_tab->where('row_name','VFE (Athora LV)')->first();
                $row_13 = $verkauf_tab->where('row_name','investitionskosten')->first();
                $verk = $verkauf_tab->where('row_name','verkaufspreis')->first();
                $vfe_value = 0;
            if (isset($vfe->tab_value)) {
                $vfe_value = $vfe->tab_value;
            }

            @endphp
            @if(isset($verk->tab_value) && $verk->tab_value &&  $total_ccc_verkauf)
                <?php
                $Verkaufspreisfaktor =($verk->tab_value)/$total_ccc_verkauf;
                ?>
            @endif
        @endisset

        @if($Verkaufspreisfaktor)



                <span class="notranslate">{!! number_format((float) $Verkaufspreisfaktor,2,",",".") !!}</span>

        @else
                <span class="notranslate"> </span>

        @endif

    </div>
    </div>
 
  <div class="row">
    <div class="col-sm-9">
        Gewinn/Ankaufspreis
    </div>

    <div class="col-sm-3 text-right">
      
      @php 
      $Gesamtkaufpreis_value = $excelFileCellD42;
      

        if($Gesamtkaufpreis_value == 0){
          $Gesamtkaufpreis_value = 1;
        }
        @endphp

        @if($total_gwn_value && $propertiescheckd->gesamt_in_eur)
        <span class="notranslate">
          
        {{number_format((float)($total_gwn_value/$propertiescheckd->gesamt_in_eur)*100,2,",",".")}}%</span>
        @else
            <span class="notranslate"></span>
        @endif
 
    </div>
  </div>
   
 
  <div class="row">
     <div class="col-sm-9">
        Gewinn in Mieten p.a.
    </div>

    <div class="col-sm-3 text-right">
          @php 
             $ankaufspreis_value_divide = 1;
          @endphp

          @isset($verkauf_tab)
          @php 
          $ankaufspreis_value_divide = $excelFileCellD42;
          if($ankaufspreis_value_divide == 0){
          $ankaufspreis_value_divide = 1;
         }
          @endphp
          @endisset

            @if($total_gwn_value && $total_ccc_verkauf )
            <span class="notranslate">{{ number_format((float)($total_gwn_value/$total_ccc_verkauf)*100 ,2,",",".")}}%</span>
            @else
                <span class="notranslate"></span>
            @endif

    </div>
</div> 
 
        <div class="row hidden">
     <div class="col-sm-9">
        All Cash Return (CF über LZ+ Tilgung LZ + Nettogewinn)
    </div>

    <div class="col-sm-3 text-right">
        @php
            $bestandsmonate = $verkauf_tab->where('row_name','bestandsmonate')->first();
        @endphp
    <span id="All_Cash_Return_span">

        <?php
        //$excelValueE31.'/'.$bestandsmonate->tab_value.'/'.$excelValueH49.'/'.$excelValueI49.'/'.$bestandsmonate->tab_value.'/'.$total_gwn_value
        ?>



        @if(isset($excelValueE31) && isset($excelValueI49) && isset($excelValueE29) && isset($bestandsmonate->tab_value))
            {{number_format((float) ((($excelValueE31/12)*$bestandsmonate->tab_value)+$excelValueH49+($excelValueI49/12)*($bestandsmonate->tab_value-12))+$total_gwn_value ,2,",",".")}} €
        @endif

    </span>
    </div>
</div>

 
{{--   <div class="row">
     <div class="col-sm-9">
        Trade Differenz Faktor
    </div>

    <div class="col-sm-3 text-right">
    --}}
    
     
<!--    <span id="Trade_Differenz_Faktor_span"></span>-->
{{--     <a href="#" id="tradedifferenzfaktor"></a>
    </div>
</div>
 --}}



 <div class="row">
     <div class="col-sm-9">
         Trade Differenz Faktor
    </div>

    <div class="col-sm-3 text-right">
          @php 
             $ankaufspreis_value_divide = 1;
          @endphp

          @isset($verkauf_tab)
          @php 
           $ankaufspreis = $verkauf_tab->where('row_name','ankaufspreis')->first();
          @endphp
          @if(!is_null($ankaufspreis)) 
          @php 
          $ankaufspreis_value_divide = $ankaufspreis->tab_value;
          if($ankaufspreis_value_divide == 0){
          $ankaufspreis_value_divide = 1;
           }

          $excelFileCellG59Value = 0;
          if(isset($excelFileCellG59))
          {
            $excelFileCellG59Value = $excelFileCellG59;
          }

          @endphp
          @endif
          @endisset
        @if($Verkaufspreisfaktor)
          <span class="notranslate">{{number_format((float) ($Verkaufspreisfaktor-$excelFileCellG59),2,",",".")}}</span>
          
        @endif

    </div>
</div> 


<div class="row">
     <div class="col-sm-9">
     Gewinn / EK p. a.
    </div>
    @php 
    if(isset($verkauf_tab)){
    $aaaa = $verkauf_tab->where('row_name','row_13')->first();
    }
    @endphp
    <div class="col-sm-3 text-right">
          @php 
             $row_13_value = 1;
          @endphp

          @isset($verkauf_tab)
          @php 
           $row_13 = $verkauf_tab->where('row_name','row_13')->first();
          @endphp
          @if(!is_null($row_13)) 
          @php 
            $row_13_value = $row_13->tab_value;
           
           if($row_13_value == 0){
             $row_13_value = 1;
           }
          @endphp
          @endif
          @endisset

            @if($total_gwn_value && $excelFileCellD48)
            <span class="notranslate">{{ number_format((float) (($total_gwn_value/$excelFileCellD48)*100),2,",",".")}}%</span>
            @else
             <span class="notranslate"></span>
            @endif
    </div>
</div> 

  @php
    $monat = ''; 
    if(isset($verkauf_tab)){
   $mont = $verkauf_tab->where('monat_check',1)->first();
   if(isset($mont->monat)){

      $monat = $mont->monat;
    }
   }  

  @endphp
 
 
  <div class="row">
    <div class="col-sm-9">
        Bestandsmonate des Objekts, wenn im Verkauf der BNL am 

        <?php
        $bdate = "";
        $bdate2 = "";
        ?>

        @isset($verkauf_tab)
        @php 
        $bestandsmonate_date =  $verkauf_tab->where('row_name','bestandsmonate_date')->first();


        if($bestandsmonate_date){
          $bdate = $bestandsmonate_date->string_text;

          $modal = \App\Models\PropertiesBuyDetail::where('property_id', $id)->where('type', 402)->first();

          if($modal && $modal->comment)
          {
            $bdate2 = $modal->comment;
          }

        }

        @endphp
        @endisset

        <?php
        $mnt = 0;
        if($bdate2 && $bdate)
        {
          $datetime1 = str_replace('/', '-', $bdate);
          $datetime2 = $bdate2;
        

           $d1=new DateTime($datetime1); 
           $d2=new DateTime($datetime2);                                  
           $Months = $d2->diff($d1); 
           $mnt = (($Months->y) * 12) + ($Months->m);

        }

        ?>

        <a href="#" class="inline-edit notranslate" id="bestandsmonate" data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-inputclass="mask-input-date" data-name="bestandsmonate_date" >{{$bdate}}</a>



        {{--<select name="monat" class="form-control" id="month_select" required>--}}
      {{--<option value="">---Wählen---</option>--}}
      {{--<option @if($monat == "Jan") selected @endif  value="Jan">Jan</option>--}}
      {{--<option @if($monat == "Feb") selected @endif  value="Feb">Feb</option>--}}
      {{--<option @if($monat == "Mar") selected @endif  value="Mar">Mar</option>--}}
      {{--<option @if($monat == "Apr") selected @endif  value="Apr">Apr</option>--}}
      {{--<option @if($monat == "Mai") selected @endif  value="Mai">Mai</option>--}}
      {{--<option @if($monat == "Jun") selected @endif  value="Jun">Jun</option>--}}
      {{--<option @if($monat == "Jul") selected @endif  value="Jul">Jul</option>--}}
      {{--<option @if($monat == "Aug") selected @endif  value="Aug">Aug</option>--}}
      {{--<option @if($monat == "Sep") selected @endif  value="Sep">Sep</option>--}}
      {{--<option @if($monat == "Okt") selected @endif  value="Okt">Okt</option>--}}
      {{--<option @if($monat == "Nov") selected @endif  value="Nov">Nov</option>--}}
      {{--<option @if($monat == "Dez") selected @endif  value="Dez">Dez</option>--}}


                    {{--</select>--}}
    </div>



    <div class="col-sm-3 text-right" style="margin-top: 5px;">
       
      {{$mnt}}
      {{-- @isset($verkauf_tab)
      @php 
      $bestandsmonate = $verkauf_tab->where('row_name','bestandsmonate')->first();
      @endphp
      @if(!is_null($bestandsmonate)) 
     
        <a href="#" class="inline-edit notranslate" id="bestandsmonate" data-type="text" data-pk="{{ $id }}" data-url="{{ url('property/update_verkauf_tab_row_others') }}" data-inputclass="mask-input-number" data-name="bestandsmonate" >
        {{$bestandsmonate->tab_value}}

        </a>
        @else
        <a href="#" class="inline-edit notranslate" id="bestandsmonate" data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-inputclass="mask-input-number" data-name="bestandsmonate" ></a>
         @endif 
      @else
      
       <a href="#" class="inline-edit notranslate" id="bestandsmonate" data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-inputclass="mask-input-number" data-name="bestandsmonate" ></a>
      
      @endisset --}}

    </div>
</div>
 

<div class="row">    
    <div class="col-sm-9">
     {{--@isset($verkauf_tab)
       @php 
      $ankaufspreis = $verkauf_tab->where('fixed_rows','row_11')->first();
      @endphp
      @if(!is_null($ankaufspreis)) 
 
        <a href="#"  class="inline-edit" data-type="text" data-pk="{{ $id }}" data-url="{{ url('property/update_row_11_12_name') }}" data-name="row_11" >
        {{ $ankaufspreis->row_name }} 
        </a>
         
      @else
    
       <a href="#" class="inline-edit"   data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_row_11_12_name/') }}" data-name="row_11" ></a>
       
       @endif 
      @endisset --}}
    </div>

    <div  style="display: none" class="col-sm-3 text-right">
 
      @isset($verkauf_tab)
      @php 

      $t = 0;
      foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
      $t += $tenancy_schedule->calculations['total_actual_net_rent'] * 12;


      $ankaufspreis = $verkauf_tab->where('row_name','row_11')->first();



      @endphp
      @if(!is_null($ankaufspreis)) 
    
        <a href="#" id="ankaufspreis" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-url="{{ url('property/update_verkauf_tab_row_others') }}" data-name="row_11" >
            {{ number_format((float) $ankaufspreis->tab_value, 2,",",".") }}
        </a>
        @else
         <a href="#" class="inline-edit notranslate" id="row_11"  data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="row_11" >{{$t}}</a>
           @php Session()->put('t',$t); @endphp
      @endif 
        
      @else
     
       <a href="#" class="inline-edit notranslate" id="row_11"  data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="row_11" >{{$t}}</a>
       
     
      @endisset
      €

    </div>
 </div>   

 <div class="row">    
    <div class="col-sm-9">
    </div>

    <div  style="display: none" class="col-sm-3 text-right">
 
     @isset($verkauf_tab)
      @php 
      $row_13 = $verkauf_tab->where('row_name','row_13')->first();
      @endphp
      @if(!is_null($row_13)) 
    
        <a href="#" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-url="{{ url('property/update_verkauf_tab_row_others') }}" data-name="row_13" >
        {{$row_13->tab_value}}
        </a>
       @else 
        <a href="#" class="inline-edit notranslate"   data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="row_13" ></a>
         @endif  
      @else
      <a href="#" class="inline-edit notranslate"  data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="row_13" ></a>
     
      @endisset €
 
    </div>
 </div>

        Gewinn Brechnung
 
    </div>

   <div class="col-sm-8" style="background-color: #ffffff; min-height: 100px; border-left: 2px solid #000; border-right: 2px solid #000; border-bottom: 2px solid #000; width: 100%;">  

  <div class="row">
    <div class="col-sm-9">
       Verkaufspreis
    </div>

    <div class="col-sm-3 text-right">
      <?php
      $total_amount_gw = 0;
      ?>
       
    
      @isset($verkauf_tab)
      @php 
      $verkaufspreis = $verkauf_tab->where('row_name','verkaufspreis')->first();
      @endphp
      @if(!is_null($verkaufspreis)) 
   
        <a href="#" id="verkaufspreis" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-inputclass="mask-input-number" data-url="{{ url('property/update_verkauf_tab_row_others') }}" data-name="verkaufspreis" >
        {{number_format((float) $verkaufspreis->tab_value,2,",",".")}}
        </a>
        <?php
        $total_amount_gw += $verkaufspreis->tab_value;
        ?>
        @else
         <a href="#" class="inline-edit notranslate" id="verkaufspreis"  data-type="text" data-pk="{{ $id }}" data-inputclass="mask-input-number" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="verkaufspreis" ></a>
       @endif
      @else
       
       <a href="#" class="inline-edit notranslate" id="verkaufspreis"  data-type="text" data-pk="{{ $id }}" data-inputclass="mask-input-number" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="verkaufspreis" ></a>
      
    @endisset €
    </div>


 

  </div>

       <div class="row">
           <div class="col-sm-9">

               Ankaufspreis (brutto) inkl. Kaufpreisnebenkosten
           </div>

           <div class="col-sm-3 text-right">

{{--               {{dd($excelValue50)}}--}}
{{--               {{number_format((float)$excelValue50,2)}}--}}
               {{-- @isset($verkauf_tab)--}}
                   
                   {{--@if(!is_null($ankaufspreis))--}}
               
                  {{number_format((float)$excelFileCellD42,2,",",".")}}

               {{--@endisset--}}
               €
           </div>
       </div>

       <div class="row">
           <div class="col-sm-9">
               Investitionskosten
           </div>

           <div class="col-sm-3 text-right">

               @isset($verkauf_tab)
                   @php
                       $row_13 = $verkauf_tab->where('row_name','investitionskosten')->first();
                   @endphp
                   @if(!is_null($row_13))

                       <a data-inputclass="mask-input-number"  href="#" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-url="{{ url('property/update_verkauf_tab_row_others') }}" data-name="investitionskosten" >
                           {{number_format((float)$row_13->tab_value,2,",",".")}}
                       </a>
                   @else
                       <a data-inputclass="mask-input-number" href="#" class="inline-edit notranslate"   data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="investitionskosten" ></a>
                   @endif
               @else
                   <a data-inputclass="mask-input-number" href="#" class="inline-edit notranslate"  data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="investitionskosten" ></a>

               @endisset €

           </div>
       </div>

       <div class="row">
           <div class="col-sm-9">
               VFE
           </div>

           <div class="col-sm-3 text-right">

               @isset($verkauf_tab)
                   @php
                       $row_13 = $verkauf_tab->where('row_name','VFE (Athora LV)')->first();
                   @endphp
                   @if(!is_null($row_13))

                       <a href="#" data-inputclass="mask-input-number" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-url="{{ url('property/update_verkauf_tab_row_others') }}" data-name="VFE (Athora LV)" >
                           {{number_format((float)$row_13->tab_value,2,",",".")}}
                       </a>
                   @else
                       <a href="#" data-inputclass="mask-input-number" class="inline-edit notranslate"   data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="VFE (Athora LV)" ></a>
                   @endif
               @else
                   <a href="#" data-inputclass="mask-input-number" class="inline-edit notranslate"  data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="VFE (Athora LV)" ></a>

               @endisset €

           </div>
       </div>

       <div class="row">
           <div class="col-sm-9">
               Maklerprovision
           </div>

           <div class="col-sm-3 text-right">

                   @php
                      $Maklerprovision = DB::table('verkauf_tab')->where('property_id', $id)->where('row_name','Maklerprovision')->orderBy('id', 'asc')->first();


                    @endphp
                   @if($Maklerprovision && $Maklerprovision->tab_value)

                       <a href="#" data-inputclass="mask-input-number" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-url="{{ url('property/update_verkauf_tab_row_others') }}" data-name="Maklerprovision" >
                           {{number_format((float)$Maklerprovision->tab_value,2,",",".")}}
                       </a>
                   @else
                       <a href="#" data-inputclass="mask-input-number" class="inline-edit notranslate"   data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="Maklerprovision" ></a>
                   @endif €

           </div>
       </div>



  {{--<div class="row">--}}
    {{--<div class="col-sm-9">--}}
      {{--Ankaufspreis (brutto) inkl. Kaufpreisnebenkosten--}}
    {{--</div>--}}

    {{--<div class="col-sm-3 text-right">--}}
      {{----}}
{{--<!--  <span id="Ankaufspreis_span"></span>  -->--}}
       {{----}}
       {{----}}

      {{--@isset($verkauf_tab)--}}
      {{--@php --}}
      {{--$ankaufspreis = $verkauf_tab->where('row_name','row_12')->first();--}}
      {{--@endphp--}}
      {{--@if(!is_null($ankaufspreis)) --}}

        {{----}}
        {{--@php--}}
        {{--$total_amount_gw -= $ankaufspreis->tab_value;--}}
        {{--@endphp--}}
        {{--{{ number_format((float) $ankaufspreis->tab_value,2,",",".") }} --}}
      {{--@else--}}
          {{--@endif --}}
      {{--@else--}}
      {{----}}
              {{----}}
      {{--@endisset €--}}



    {{--</div>--}}
{{--</div>--}}

@isset($verkauf_tab)
@foreach($verkauf_tab as $kauf_tab)
@if($kauf_tab->new_added == 1)
<div class="row">
    <div class="col-sm-9">
      <a data-inputclass="mask-input-number" href="{{url('property/delete_verkauf_tab_row/'.$kauf_tab->id) }}?property_id={{ $id }}" class="btn btn-danger btn-sm" style="color:white;     margin-bottom: 5px;" onclick="return confirm('Are you sure?')">Löschen</a>
     <a href="#" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row/'.$kauf_tab->id) }}" >
      {{ $kauf_tab->row_name }}
      </a> 
       
    </div>

    <div class="col-sm-3 text-right" style="margin-top: 5px;">
   
        <a data-inputclass="mask-input-number" href="#" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row/'.$kauf_tab->id) }}" data-name="value_change" >
      {{number_format((float)$kauf_tab->tab_value,2,",",".")}}
      </a> €

      @php
        $total_amount_gw -= $kauf_tab->tab_value;
      @endphp



    </div>
</div> 
@endif
@endforeach
@endisset
 
<form action="{{ url('property/verkauf_new_row') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="property_id" value="{{ $id }}">
    <button style="margin-bottom: 5px;" type="submit" class="btn btn-default btn-xs">Hinzufügen</button>
</form>
 
{{-- 
  <div class="row">
    <div class="col-sm-9">
      Provision Makler (Innenprov. 1%)
    </div>

    <div class="col-sm-3 text-right">
      <input type="hidden" name="ProvisionMakler">
        <a href="#" id="ProvisionMakler"></a>
      
    </div>
</div> --}}


    </div>

<div class="col-sm-8" style="background-color: #ffff00; border-left: 2px solid #000; border-right: 2px solid #000; border-bottom:double; width: 100%;">  

  <div class="row">
    <div class="col-sm-9">
      Gewinn
    </div>


    <div class="col-sm-3 text-right">


            @isset($verkauf_tab)

                @php
                    $gewinn="";
                    $vfe = $verkauf_tab->where('row_name','VFE (Athora LV)')->first();
                    $row_13 = $verkauf_tab->where('row_name','investitionskosten')->first();
                    //$ankaufspreis = $verkauf_tab->where('row_name','row_12')->first();
                    
                    $ankaufspreisValue = $excelFileCellD42;
                    
                    $row_13Value =0;
                    $vfeValue =0;

                 //if(isset($ankaufspreis->tab_value))
               ///{
                    //$ankaufspreisValue = $ankaufspreis->tab_value;
                //}
                 if(isset($row_13->tab_value))
                {
                    $row_13Value = $row_13->tab_value;
                }
                 if(isset($vfe->tab_value))
                {
                    $vfeValue = $vfe->tab_value;
                }
                @endphp

                    <?php
                    $gewinn =$total_amount_gw-$ankaufspreisValue - $row_13Value-$vfeValue;
                     ?>

             <span id="Gewinn_span" class="notranslate">{{number_format((float)$gewinn,2,",",".")}} €</span>

        @endisset
    </div>
</div>
    </div>

   <div class="col-sm-8" style="background-color: #ffffff; border-left: 2px solid #000; border-right: 2px solid #000; border-bottom: 2px solid #000; width: 100%;">  

  <div class="row">
    <div class="col-sm-9">
      Tilgung über LZ
    </div>

    <div class="col-sm-3 text-right">
  
<!-- <span id="zzgl_span"></span> -->
 @isset($verkauf_tab)
      @php 
      $zzgl = $verkauf_tab->where('row_name','zzgl')->first();
      @endphp
      @if(!is_null($zzgl)) 
     
        <a href="#" id="zzgl" data-inputclass="mask-input-number" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-url="{{ url('property/update_verkauf_tab_row_others') }}" data-name="zzgl" >
       {{number_format((float)$zzgl->tab_value,2,",",".")}}
        </a>
        @else
        <a href="#" class="inline-edit notranslate" data-inputclass="mask-input-number" id="zzgl"  data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="zzgl" ></a>
        @endif
      @else
       
       <a href="#" class="inline-edit notranslate" data-inputclass="mask-input-number" id="zzgl"  data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="zzgl" ></a>
  
  @endisset €
    </div>
</div>

  <div class="row">
    <div class="col-sm-9">
      CF über die Bestandsmonate
    </div>

    <div class="col-sm-3 text-right">
   
    
   
     @isset($verkauf_tab)
      @php 
      $uber = $verkauf_tab->where('row_name','uber')->first();
      @endphp
      @if(!is_null($uber)) 
      
        <a href="#" id="uber" data-inputclass="mask-input-number" class="inline-edit notranslate" data-type="text" data-pk="{{ $id }}" data-url="{{ url('property/update_verkauf_tab_row_others') }}" data-name="uber" >
        {{number_format((float)$uber->tab_value,2,",",".")}}
        </a>
        @else
        <a href="#" data-inputclass="mask-input-number" class="inline-edit notranslate" id="uber"  data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="uber" ></a>
       @endif 
      @else
     
       <a href="#" data-inputclass="mask-input-number" class="inline-edit notranslate" id="uber"  data-type="text" data-pk="{{ $id }}" data-url="{{url('property/update_verkauf_tab_row_others/') }}" data-name="uber" ></a>
 
      @endisset €
<!--    <span id="uber_span"></span> -->

    </div>
</div>

<div class="row">
    <div class="col-sm-9">
      Eigenkapitalrückfluss
    </div>

    <div class="col-sm-3 text-right">
    {{number_format((float)$excelFileCellD48,2,",",".")}} €
    </div>
</div>

  



  <div class="row">
    <div class="col-sm-9">
        <!-- Gewinnrückfluss (Gewinn+Tilgung+CF) -->
        Kapitalrückfluss gesamt

    </div>

    <div class="col-sm-3 text-right">
      @php 
      $zzgl_value = 0;
      $uber_value = 0;
      @endphp

      @isset($verkauf_tab)
      @php 
      $zzgl = $verkauf_tab->where('row_name','zzgl')->first();
      $uber = $verkauf_tab->where('row_name','uber')->first();
      @endphp
      @if(!is_null($zzgl)) 
      @php 
          $zzgl_value = $zzgl->tab_value;
      @endphp
      @endif 

      @if(!is_null($uber)) 
      @php 
          $uber_value = $uber->tab_value;
      @endphp
      @endif 

      @endisset

                <span class="notranslate">{{ number_format((float) ($gewinn)+($zzgl_value)+($uber_value)+$excelFileCellD48 ,2,",",".")}} €</span>

    </div>
</div>

    </div>
    </div>