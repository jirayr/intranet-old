<?php



$form_id = 'immoscout_form';
$externalId = '';
$title = '';
$street = $properties['strasse'];
$houseNumber = $properties['hausnummer'];
$postcode = $properties['plz_ort'];
$city = $properties['ort'];
$showAddress = 'true';
$apartmentType = $properties['type_of_property'];
$lift = '';
$cellar = '';
$handicappedAccessible = '';
$condition = '';

/*
 * price variables
 * */
$value = '';
$currency = 'EUR';
$marketingType = 'RENT';
$priceIntervalType = '';


$livingSpace = '';
$numberOfRooms = '';
$builtInKitchen = '';
$balcony = '';
$garden = '';
$hasCourtage = 'NO';
$courtage = '';


$property_id =  Request::segment(2);
$realsate = DB::table('property_images')->where('property_id', $property_id)->orderBy('id', 'desc')->limit(1)->first();
if(!is_null($realsate)){

    $form_id = 'immoscout_form_edit';
    $realsate_id = $realsate->realstate_id;

    $url =  "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/realestate/".$realsate_id;


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/realestate/".$realsate_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "authorization: OAuth oauth_consumer_key=\"08211746Key\",oauth_token=\"4aca0424-da15-42d7-bde6-87353d9d82d7\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"1555299289\",oauth_nonce=\"e6x6VQ\",oauth_version=\"1.0\",oauth_signature=\"dBbQMwhZgBFRmSi8TYwADHxyags%3D\"",
            "cache-control: no-cache",
            "postman-token: b13e992f-eb58-27c6-c1e8-dc518686f040"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
        echo "cURL Error #:" . $err;
    } else {



        if($xml = new \SimpleXMLElement($response)){
            $xml = new \SimpleXMLElement($response);
            $response = json_encode($xml, JSON_PRETTY_PRINT);
            $response = json_decode($response, TRUE);


            if(isset($response['externalId']) ){

                $externalId = $response['externalId'];
                $title = $response['title'];
                $street = $response['address']['street'];
                $houseNumber = $response['address']['houseNumber'];
                $postcode = $response['address']['postcode'];
                $city = $response['address']['city'];
                $showAddress = $response['showAddress'];
                $value = $response['price']['value'];
                $currency = $response['price']['currency'];
                $marketingType = $response['price']['marketingType'];
                $priceIntervalType = $response['price']['priceIntervalType'];
                $livingSpace = $response['livingSpace'];
                $numberOfRooms = $response['numberOfRooms'];
                $hasCourtage = $response['courtage']['hasCourtage'];
                $courtage = $response['courtage']['courtage'];
            }
//            echo '<pre>';
//            print_r($response);
//            echo '</pre>';

        }
    }

}

?>

@php 

    $immo_upload = isset($item) ?  DB::table('immo_upload')->where('property_id', $item->id)->first() : null;

    $category = $property_type = $postcode = $address = $house_no = $city = $show_address = $title = $objektnummer = $gruppennummer =  $marketing_method  =  $property = $totalarea =  $retail_space = $rental_price = $available_from = $lageart = $nebenflache = $sales_area = $objektzustand = $letzte_odernisierung = $denkmalschutzobjekt = $quality_of_facilities = $floors = $number_parking_area = $price_parking_area = $elevator = $lift = $ramp =  $cellar = $delivery = $floor_load = $energy_certificate = $certificate_date = $heating = $deposit = $description = $equipments = $location = $miscellaneous = $contact_sex = $contact_surname = $contact_country = $contact_mail = $images_pro = $floor_plan = '';

    if(isset($immo_upload)){
 
          $category =                 $immo_upload->category;
          $property_type =            $immo_upload->property_type;
          $postcode =                 $immo_upload->postcode;
          $address =                  $immo_upload->address;
          $house_no =                 $immo_upload->house_no;
          $city =                     $immo_upload->city;
          $show_address =             $immo_upload->show_address;
          $title =                    $immo_upload->title;
          $objektnummer =             $immo_upload->objektnummer;
          $gruppennummer =            $immo_upload->gruppennummer;
          $marketing_method  =        $immo_upload->marketing_method;
          $property =                 $immo_upload->property;
          $totalarea =                $immo_upload->totalarea;
          $retail_space =             $immo_upload->retail_space;
          $rental_price =             $immo_upload->rental_price;
          $available_from =           $immo_upload->available_from;
          $lageart =                  $immo_upload->lageart;
          $nebenflache =              $immo_upload->nebenflache;
          $sales_area =               $immo_upload->sales_area;
          $objektzustand =            $immo_upload->objektzustand;
          $letzte_odernisierung =     $immo_upload->letzte_odernisierung;
          $denkmalschutzobjekt =      $immo_upload->denkmalschutzobjekt;
          $quality_of_facilities =    $immo_upload->quality_of_facilities;
          $floors =                   $immo_upload->floors;
          $number_parking_area =      $immo_upload->number_parking_area;
          $price_parking_area =       $immo_upload->price_parking_area;
          $elevator =                 $immo_upload->elevator;
          $lift =                     $immo_upload->lift;   
          $ramp =                     $immo_upload->ramp;
          $cellar =                   $immo_upload->cellar;
          $delivery =                 $immo_upload->delivery;
          $floor_load =               $immo_upload->floor_load;
          $energy_certificate =       $immo_upload->energy_certificate;
          $certificate_date =         $immo_upload->certificate_date;
          $heating =                  $immo_upload->heating;
          $deposit =                  $immo_upload->deposit;
          $description =              $immo_upload->description;
          $equipments =               $immo_upload->equipments;
          $location =                 $immo_upload->location;
          $miscellaneous =            $immo_upload->miscellaneous;
          $contact_sex =              $immo_upload->contact_sex;
          $contact_surname =          $immo_upload->contact_surname;
          $contact_country =          $immo_upload->contact_country;
          $contact_mail =             $immo_upload->contact_mail;
          $images_pro =               $immo_upload->images;
          $floor_plan =               $immo_upload->floor_plan;

    }

@endphp

<style>
  .new-height{
  height: 100px !important;
  }
</style>

<div id="response"></div>
<form class="{{ $form_id }}" method="post" action="{{ url('/') }}/properties/upload_listing_to_api" enctype="multipart/form-data">

    <!-- Modal content-->

    <div id="json_resp_immoc">

    </div>
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Immoscout24 Upload</h4>
            @if($form_id == "immoscout_form_edit")
                <a type="button" data-toggle="tab" href=".uploadAttachement" class="btn btn-primary pull-right" style="margin-top: -25px;">Upload Attachment</a>
            @endif
        </div>
        <div class="modal-body">

            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="property_id" value="{{ $item->id }}" />



            <label>Kategorie</label>
            <select name="category" class="form-control" required>

                <option value="2" @if(isset($category) && $category == 2) selected @endif >Gewerbeimmobilie/Gewerbegrundstück</option>


            </select><br>


            <label>Typ</label>
            <select name="property_type" class="form-control" required>
                <option value="1"  @if(isset($property_type) && $property_type == 1) selected @endif >Büro/Praxis</option>
                <option value="2" @if(isset($property_type) && $property_type == 2) selected @endif>Gastronomie/ Hotel</option>
                <option value="3" @if(isset($property_type) && $property_type == 3) selected @endif>Halle/Produktion</option>
                <option value="4" @if(isset($property_type) && $property_type == 4) selected @endif>Einzelhandel</option>
                <option value="5" @if(isset($property_type) && $property_type == 5) selected @endif>Spezialgewerbe</option>


            </select><br>

            <label>Postleitzahl</label>
            <input type="text" value="{{ $postcode  }}" name="postcode" class="form-control" placeholder="PLZ" required>


            <label>Addresse</label>
            <input type="address" value="{{ $address  }}" name="address" class="form-control" placeholder="Addresse" required>

            <label>Hausnummer</label>
            <input type="house_no" value="{{ $house_no  }}" name="house_no" class="form-control" placeholder="Hausnummer" required>

            <label>Stadt</label>
            <input type="city" value="{{ $city  }}" name="city" class="form-control" placeholder="Stadt" required>

            <label>Addresse anzeigen</label>
            <select name="show_address" class="form-control" required>
                <option value="2">Ja</option>
                <option value="1">Nein</option>


            </select><br>

            <label>Titel</label>
            <input type="title" value="{{ $title  }}" name="title" class="form-control" placeholder="Titel" required>




            <label>Verkmarktungsart</label>
            <input type="marketing_method" value="{{ $marketing_method  }}" name="marketing_method" class="form-control" placeholder="Miete" required>

            <label>Objektart</label>
            <select name="property" class="form-control" required>
                <option value="1" @if($property == 1) selected @endif >Ausstellungsfläche</option>
                <option value="2" @if($property == 2) selected @endif >Einkaufszentrum</option>
                <option value="3" @if($property == 3) selected @endif >Factory Outlet</option>
                <option value="4" @if($property == 4) selected @endif >Laden</option>
                <option value="5" @if($property == 5) selected @endif >Verkaufsläche</option>

            </select><br>


            <label>Gesamtfläche</label>
            <input type="totalarea" value="{{ $totalarea  }}" name="totalarea" class="form-control" placeholder="Gesamtfläche" required>

            <label>Verkaufsfläche</label>
            <input type="retail_space" value="{{ $retail_space  }}" name="retail_space" class="form-control" placeholder="Verkaufsfläche" required>

            <label>Mietpreis</label>
            <input type="rental_price" value="{{ $rental_price  }}" name="rental_price" class="form-control" placeholder="Mietpreis" required>

            <label>Verfügbar ab</label>
            <input type="available_from" value="{{ $available_from  }}" name="available_from" class="form-control" placeholder="Verfügbar ab" required>

            <label>Lageart</label>
            <select name="lageart" class="form-control" required>
                <option value="1" @if($lageart == 1) selected @endif >A-Lage</option>
                <option value="2" @if($lageart == 2) selected @endif  >B-Lage</option>
                <option value="3" @if($lageart == 3) selected @endif  >Einkaufszentrum</option>

            </select><br>


            <label>Nebenfläche (m²)</label>
            <input type="nebenflache" value="{{ $nebenflache  }}" name="nebenflache" class="form-control" placeholder="Nebenfläche (m²)" required>


            <label>Verkaufsfläche teilbar ab</label>
            <input type="sales_area" value="{{ $sales_area  }}" name="sales_area" class="form-control" placeholder="Verkaufsfläche teilbar ab" required>

            <label>Objektzustand</label>
            <select name="objektzustand" class="form-control" required>
                <option value="1"  @if($objektzustand == 1) selected @endif  >Keine Angabe</option>
                <option value="2"  @if($objektzustand == 2) selected @endif  >Erstbezug</option>
                <option value="3"  @if($objektzustand == 3) selected @endif >Erstbezug nach Sanierung</option>
                <option value="4"  @if($objektzustand == 4) selected @endif >Neuwertig</option>
                <option value="5"  @if($objektzustand == 5) selected @endif >Saniert</option>
                <option value="6"  @if($objektzustand == 6) selected @endif >Modernisiert</option>
                <option value="7"  @if($objektzustand == 7) selected @endif >Vollständig renoviert</option>
                <option value="8"  @if($objektzustand == 8) selected @endif >Gepflegt</option>
                <option value="9"  @if($objektzustand == 9) selected @endif >Renovierungsbedürftig</option>
                <option value="10" @if($objektzustand == 10) selected @endif >Nach Vereinbarung</option>
                <option value="11" @if($objektzustand == 11) selected @endif >Abbruchreif</option>

            </select><br>

            <label>Letzte Modernisierung/ Sanierung (Jahr)</label>
            <input type="letzte_odernisierung" value="{{ $letzte_odernisierung  }}" name="letzte_odernisierung" class="form-control" placeholder="Letzte Modernisierung (Jahr)" required>

            <label>Denkmalschutzobjekt</label>
            <select name="denkmalschutzobjekt" class="form-control" required>
                <option value="1" @if($denkmalschutzobjekt == 1) selected @endif>Nein</option>
                <option value="2" @if($denkmalschutzobjekt == 2) selected @endif>Ja</option>


            </select><br>

            <label>Qualität der Ausstattung</label>
            <select name="quality_of_facilities" class="form-control" required>
                <option value="1" @if($quality_of_facilities == 1) selected @endif>Keine Angabe</option>
                <option value="2" @if($quality_of_facilities == 2) selected @endif>Luxus</option>
                <option value="3" @if($quality_of_facilities == 3) selected @endif>Gehoben</option>
                <option value="4" @if($quality_of_facilities == 4) selected @endif>Normal</option>
                <option value="5" @if($quality_of_facilities == 5) selected @endif>Einfach</option>

            </select><br>

            <label>Etagen</label>
            <input type="floors" value="{{ $floors  }}" name="floors" class="form-control" placeholder="Etagen" required>


            <label>Anzahl der Parkflächen</label>
            <input type="number_parking_area" value="{{ $number_parking_area  }}" name="number_parking_area" class="form-control" placeholder="Anzahl Parkplätze" required>

            <label>Preis pro Parkfläche</label>
            <input type="price_parking_area" value="{{ $price_parking_area  }}" name="price_parking_area" class="form-control" placeholder="Preis Parkplätze" required>


            <label>Lastenaufzug</label>
            <select name="lift" class="form-control" required>
                <option value="1" @if($lift == 1) selected @endif>Nein</option>
                <option value="2" @if($lift == 2) selected @endif>Ja</option>

            </select><br>

            <label>Personenaufzug</label>
            <select name="elevator" class="form-control" required>
                <option value="1" @if($elevator == 1) selected @endif>Nein</option>
                <option value="2" @if($elevator == 2) selected @endif>Ja</option>

            </select><br>


            <label>Rampe</label>
            <select name="ramp" class="form-control" required>
                <option value="1" @if($ramp == 1) selected @endif>Nein</option>
                <option value="2" @if($ramp == 2) selected @endif>Ja</option>

            </select><br>


            <label>Keller</label>
            <select name="cellar" class="form-control" required>
                <option value="1">Nein</option>
                <option value="2">Ja</option>

            </select><br>


            <label>Zulieferung</label>
            <select name="delivery" class="form-control" required>
                <option value="1" @if($delivery == 1) selected @endif >Keine Angabe</option>
                <option value="2" @if($delivery == 2) selected @endif  >Direkter Zugang</option>
                <option value="3" @if($delivery == 3) selected @endif >Keine direkte Anfahrt</option>
                <option value="4" @if($delivery == 4) selected @endif >PKW Zufahrt</option>
                <option value="5" @if($delivery == 5) selected @endif >Anfahrt von vorne</option>
                <option value="6" @if($delivery == 6) selected @endif >Anfahrt von hinten</option>
                <option value="7" @if($delivery == 7) selected @endif >Ganztägig</option>
                <option value="8" @if($delivery == 8) selected @endif >Vormittags</option>




            </select><br>

            <label>Bodenbelag</label>
            <select name="floor_load" class="form-control" required>
                <option @if($floor_load == 'string:NO_INFORMATION') selected @endif  label="Keine Angabe" value="string:NO_INFORMATION" >Keine Angabe</option>
                <option @if($floor_load == 'string:CONCRETE') selected @endif  label="Beton" value="string:CONCRETE">Beton</option>
                <option @if($floor_load == 'string:EPOXY_RESIN') selected @endif  label="Epoxidharzboden" value="string:EPOXY_RESIN">Epoxidharzboden</option>
                <option @if($floor_load == 'string:TILES') selected @endif  label="Fliesen" value="string:TILES">Fliesen</option>
                <option @if($floor_load == 'string:PLANKS') selected @endif  label="Dielen" value="string:PLANKS">Dielen</option>
                <option @if($floor_load == 'string:LAMINATE') selected @endif  label="Laminat" value="string:LAMINATE">Laminat</option>
                <option @if($floor_load == 'string:PARQUET') selected @endif  label="Parkett" value="string:PARQUET">Parkett</option>
                <option @if($floor_load == 'string:PVC') selected @endif   label="PVC" value="string:PVC">PVC</option>
                <option @if($floor_load == 'string:CARPET') selected @endif  label="Teppichboden" value="string:CARPET">Teppichboden</option>
                <option @if($floor_load == 'string:ANTISTATIC_FLOOR') selected @endif  label="Antistatischer Teppichboden" value="string:ANTISTATIC_FLOOR">Antistatischer Teppichboden</option>
                <option @if($floor_load == 'string:OFFICE_CARPET') selected @endif  label="Stuhlrollenfeste Teppichfliesen" value="string:OFFICE_CARPET">Stuhlrollenfeste Teppichfliesen</option>
                <option @if($floor_load == 'string:STONE') selected @endif  label="Stein" value="string:STONE">Stein</option>
                <option @if($floor_load == 'string:CUSTOMIZABLE') selected @endif  label="Nach Mieterwunsch" value="string:CUSTOMIZABLE">Nach Mieterwunsch</option>
                <option @if($floor_load == 'string:WITHOUT') selected @endif  label="Ohne Bodenbelag" value="string:WITHOUT">Ohne Bodenbelag</option>





            </select><br>

            <label>Energieausweis</label>
            <select  name="energy_certificate" class="form-control" required>
                <option @if($energy_certificate == 'string:NO_INFORMATION') selected @endif label="Bitte wählen" value="string:NO_INFORMATION" selected="selected">Bitte wählen</option>

                <option @if($energy_certificate == 'string:AVAILABLE') selected @endif label="Energieausweis liegt für das Gebäude vor" value="string:AVAILABLE">Energieausweis liegt für das Gebäude vor</option>

                <option @if($energy_certificate == 'string:NOT_AVAILABLE_YET') selected @endif label="Energieausweis liegt zur Besichtigung vor" value="string:NOT_AVAILABLE_YET">Energieausweis liegt zur Besichtigung vor</option>

                <option @if($energy_certificate == 'string:NOT_REQUIRED') selected @endif label="Dieses Gebäude unterliegt nicht den Anforderungen der EnEV" value="string:NOT_REQUIRED">Dieses Gebäude unterliegt nicht den Anforderungen der EnEV</option>



            </select><br>


            <label>Baujahr</label>
            <input type="certificate_date" value="{{ $certificate_date  }}" name="certificate_date" class="form-control" placeholder="Baujahr" required>

            <label>Heizungsart</label>
            <select name="heating" class="form-control" required>
                <option @if($heating == 'string:NO_INFORMATION') selected @endif label="Keine Angabe" value="string:NO_INFORMATION" selected="selected">Keine Angabe</option>

                <option @if($heating == 'string:SELF_CONTAINED_CENTRAL_HEATING') selected @endif label="Etagenheizung" value="string:SELF_CONTAINED_CENTRAL_HEATING">Etagenheizung</option>

                <option @if($heating == 'string:STOVE_HEATING') selected @endif label="Ofenheizung" value="string:STOVE_HEATING">Ofenheizung</option>

                <option @if($heating == 'string:CENTRAL_HEATING') selected @endif label="Zentralheizung" value="string:CENTRAL_HEATING">Zentralheizung</option>

                <option @if($heating == 'string:COMBINED_HEAT_AND_POWER_PLANT') selected @endif label="Blockheizkraftwerke" value="string:COMBINED_HEAT_AND_POWER_PLANT">Blockheizkraftwerke</option>

                <option @if($heating == 'string:ELECTRIC_HEATING') selected @endif label="Elektro-Heizung" value="string:ELECTRIC_HEATING">Elektro-Heizung</option>

                <option @if($heating == 'string:DISTRICT_HEATING') selected @endif label="Fernwärme" value="string:DISTRICT_HEATING">Fernwärme</option>

                <option @if($heating == 'string:FLOOR_HEATING') selected @endif label="Fußbodenheizung" value="string:FLOOR_HEATING">Fußbodenheizung</option>

                <option @if($heating == 'string:GAS_HEATING') selected @endif label="Gas-Heizung" value="string:GAS_HEATING">Gas-Heizung</option>

                <option @if($heating == 'string:WOOD_PELLET_HEATING') selected @endif label="Holz-Pelletheizung" value="string:WOOD_PELLET_HEATING">Holz-Pelletheizung</option>

                <option @if($heating == 'string:NIGHT_STORAGE_HEATER') selected @endif label="Nachtspeicheröfen" value="string:NIGHT_STORAGE_HEATER">Nachtspeicheröfen</option>

                <option @if($heating == 'string:OIL_HEATING') selected @endif label="Öl-Heizung" value="string:OIL_HEATING">Öl-Heizung</option>

                <option @if($heating == 'string:SOLAR_HEATING') selected @endif label="Solar-Heizung" value="string:SOLAR_HEATING">Solar-Heizung</option>

                <option @if($heating == 'string:HEAT_PUMP') selected @endif label="Wärmepumpe" value="string:HEAT_PUMP">Wärmepumpe</option>

            </select><br>

            <label>Kaution (€)</label>
            <input type="deposit" value="{{ $deposit  }}" name="deposit" class="form-control" placeholder="Kaution (€)" required>



            <label>Beschreibung</label>
            <textarea type="description" name="description" class="form-control" placeholder="Beschreibung"  required>{{ $description  }}</textarea>

            <label>Ausstattung</label>
            <textarea type="equipments"  name="equipments" class="form-control new-height" placeholder="Ausstattung" required>{{ $equipments  }}</textarea>

            <label>Lage</label>
            <textarea type="location"  name="location" class="form-control new-height" placeholder="Lage" rows="8" required>{{ $location  }}</textarea>

            <label>Sonstiges</label>
            <textarea type="miscellaneous"  name="miscellaneous" class="form-control new-height" placeholder="Sonstiges" required>{{ $miscellaneous  }}</textarea>

            <label>Anrede</label>
            <select name="contact_sex" class="form-control" required>
                <option value="MALE" @if($contact_sex == 'MALE') selected @endif>Herr</option>
                <option value="FEMALE" @if($contact_sex == 'FEMALE') selected @endif>Frau</option>

            </select><br>

            <label>Nachname</label>
            <input type="contact_surname" value="{{ $contact_surname  }}" name="contact_surname" class="form-control" placeholder="Nachname" required>

            <label>Land</label>
            <select name="contact_country" class="form-control" required>
                <option @if($contact_sex == 'DEUTSCHLAND') selected @endif value="DEUTSCHLAND">Deutschland</option>
            </select><br>

            <label>E-Mail</label>
            <input type="contact_mail" value="{{ $contact_mail  }}" name="contact_mail" class="form-control" placeholder="E-Mail" required><br>


            <label>Bilder Hochladen</label>
            <input type="file" name="images[]" class="form-control" multiple="" ><br>

            @if(isset($images_pro) && $images_pro != null)
                @php
                    $explode_images = explode('|', $images_pro);
                @endphp
                @foreach($explode_images as $explode_img)
                    <div style="float: left; margin-right: 15px; width: auto; width: 100px; height: 100px;">
                        <img src="https://intranet.fcr-immobilien.de/property_images/{{ $explode_img }}" width="100" height="100" style="object-fit: cover;"><br>
                        <a onclick="return confirm('Are you sure?')" href="{{ url('delete-property-single-image') }}/{{ $item->id }}/{{ $explode_img }}" class="btn btn-danger btn-sm">Löschen</a>
                    </div>

                @endforeach

                <br> <br><br><br><br> <br><br><br>
            @endif


            <label>Grundriss laden</label>
            <input type="file" name="grundriss[]" class="form-control" multiple="" ><br>

            @if(isset($floor_plan) && $floor_plan != null)
                @php
                    $explode_images_floor_plan = explode('|', $floor_plan);
                @endphp

                @foreach($explode_images_floor_plan as $explode_floor)
                    <div style="float: left; margin-right: 15px; width: auto; width: 100px; height: 100px;">
                        <img src="https://intranet.fcr-immobilien.de/property_images/{{ $explode_floor }}" width="100" height="100" style="object-fit: cover;"><br>
                        <a onclick="return confirm('Are you sure?')" href="{{ url('delete-property-single-image') }}/{{ $item->id }}/{{ $explode_floor }}?floor_plan=1" class="btn btn-danger btn-sm">Löschen</a>
                    </div>

                @endforeach

                <br><br><br><br><br><br> <br><br><br>
            @endif
        </div>


        <div class="modal-footer">

            <button type="submit" class="btn btn-primary" >Upload <i class="fa fa-circle-o-notch fa-spin immo_loader hide" style="font-size:14px"></i></button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
        </div>

</form>
</div>
 