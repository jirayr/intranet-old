
<div class="row">
    <legend>Darlehensspiegel</legend>
    <div class="col-md-10 col-xs-12">
    @if ($errors->any())
        <p class="error" style="font-weight: bold;">Please fill all required fields.</p>
    @endif

    <form action="{{ url('property/create_loans_mirror') }}" method="post" class="form-horizontal loanMirrorForm" accept-charset="UTF-8" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="property_id" value="{{ $properties->id }}">

        <legend>Übersicht</legend>
        <div class="form-group m-form__group row">
        <div class="col-lg-6"></div>
            <div class="col-lg-6">
                <label>Darlehen 30.06.2020 HGB:</label>
                @if ($errors->has('delta3'))
                    <div class="error">{{ $errors->first('delta3') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="delta3" value="@if(old('delta3')){{old('delta3')}}@elseif($loansMirrorData){{number_format($loansMirrorData->delta3,2,',','.')}}@endif" name="delta3">
            </div>
        <div class="col-lg-6">
                <label>Delta:</label>
                @if ($errors->has('delta'))
                    <div class="error">{{ $errors->first('delta') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="delta" value="@if(old('delta')){{old('delta')}}@elseif($loansMirrorData){{number_format($loansMirrorData->delta,2,',','.')}}@endif" name="delta">
            </div>
            <div class="col-lg-6">
                <label>Darlehen 31.12.2019 HGB:</label>
                @if ($errors->has('delta2'))
                    <div class="error">{{ $errors->first('delta2') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="delta2" value="@if(old('delta2')){{old('delta2')}}@elseif($loansMirrorData){{number_format($loansMirrorData->delta2,2,',','.')}}@endif" name="delta2">
            </div>
            <div class="col-lg-6">
                <label>Zins:</label>
                @if ($errors->has('zins'))
                    <div class="error">{{ $errors->first('zins') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="zins" value="@if(old('zins')){{old('zins')}}@elseif($loansMirrorData && is_numeric($loansMirrorData->zins)){{number_format($loansMirrorData->zins,2,',','.')}}@endif" name="zins">
            </div>
            
            <div class="col-lg-6">
                <label>Zinsaufwand p.a. ann. per 31.12.2019:</label>
                @if ($errors->has('interest_extend'))
                    <div class="error">{{ $errors->first('interest_extend') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="interest_extend" value="@if(old('interest_extend')){{old('interest_extend')}}@elseif($loansMirrorData){{number_format($loansMirrorData->interest_extend,2,',','.')}}@endif" name="interest_extend">
            </div>
            <div class="col-lg-6">
                <label>Kapitaldienst p.a. ann. per 31.12.2019:</label>
                @if ($errors->has('loan_service'))
                    <div class="error">{{ $errors->first('loan_service') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="loan_service" value="@if(old('loan_service')){{old('loan_service')}}@elseif($loansMirrorData){{number_format($loansMirrorData->loan_service,2,',','.')}}@endif" name="loan_service">
            </div>
            <div class="col-lg-6">
                <label>Kapitaldienst p.m. ann. per 31.12.2019:</label>
                @if ($errors->has('loan_service_month'))
                    <div class="error">{{ $errors->first('loan_service_month') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="loan_service_month" value="@if(old('loan_service_month')){{old('loan_service_month')}}@elseif($loansMirrorData){{number_format($loansMirrorData->loan_service_month,2,',','.')}}@endif" name="loan_service_month">
            </div>
        </div>    
        <legend>1.) Stammdaten</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>Darlehensnehmer:</label>
                @if ($errors->has('borrower'))
                    <div class="error">{{ $errors->first('borrower') }}</div>
                @endif
                <input type="text" class="form-control" id="borrower" value="@if(old('borrower')){{old('borrower')}}@elseif($loansMirrorData){{$loansMirrorData->borrower}}@endif" name="borrower">
            </div>
            <div class="col-lg-6">
                <label>Darlehensgeber:</label>
                @if ($errors->has('lender'))
                    <div class="error">{{ $errors->first('lender') }}</div>
                @endif
                <input type="text" class="form-control" id="lender" value="@if(old('lender')){{old('lender')}}@elseif($loansMirrorData){{$loansMirrorData->lender}}@endif" name="lender">
            </div>
        </div>


        <legend>2.) Darlehenshöhte</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>Ursprungsdarlehen:</label>
                @if ($errors->has('original_loan'))
                    <div class="error">{{ $errors->first('original_loan') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="original_loan" value="@if(old('original_loan')){{old('original_loan')}}@elseif($loansMirrorData){{number_format($loansMirrorData->original_loan,2,',','.')}}@endif" name="original_loan">
            </div>
            <div class="col-lg-6">
                <label>Auszahlungsdatum:</label>
                @if ($errors->has('payout_date'))
                    <div class="error">{{ $errors->first('payout_date') }}</div>
                @endif
                @php
                    $payout_date = "";
                    if(old('payout_date')){
                        $payout_date = show_date_format(old('payout_date'));
                    }elseif($loansMirrorData && $loansMirrorData->payout_date){
                        $payout_date = show_date_format($loansMirrorData->payout_date);
                    }

                    if(@$buy_data[402]['comment']):
                    $payout_date = show_date_format($buy_data[402]['comment']);
                    endif;


                @endphp
                <input type="text" class="form-control mask-date-input-new" id="payout_date" value="{{$payout_date}}" name="payout_date" placeholder="DD.MM.YYYY">
            </div>

            
            

        </div>

        
        <legend>3.) Konditionen</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>Anfangszins:</label>
                @if($errors->has('early_interest'))
                    <div class="error">{{ $errors->first('early_interest') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="early_interest" value="@if(old('early_interest')){{old('early_interest')}}@elseif($loansMirrorData){{number_format($loansMirrorData->early_interest,2,',','.')}}@endif" name="early_interest">
            </div>
            <div class="col-lg-6">
                <label>Berechnungsmethode:</label>
                @if ($errors->has('calculation_method'))
                    <div class="error">{{ $errors->first('calculation_method') }}</div>
                @endif
                <input type="text" class="form-control" id="calculation_method" value="@if(old('calculation_method')){{old('calculation_method')}}@elseif($loansMirrorData){{$loansMirrorData->calculation_method}}@endif" name="calculation_method" >
            </div>

            <div class="col-lg-6">
                <label>Variabler Zins:</label>
                @if ($errors->has('variable_interest'))
                    <div class="error">{{ $errors->first('variable_interest') }}</div>
                @endif
                <input type="text" class="form-control" id="variable_interest" value="@if(old('variable_interest')){{old('variable_interest')}}@elseif($loansMirrorData){{$loansMirrorData->variable_interest}}@endif" name="variable_interest" >
            </div>
            <div class="col-lg-6">
                <label>Referenzzinssatz:</label>
                @if ($errors->has('reference_interest'))
                    <div class="error">{{ $errors->first('reference_interest') }}</div>
                @endif
                <input type="text" class="form-control" id="reference_interest" value="@if(old('reference_interest')){{old('reference_interest')}}@elseif($loansMirrorData){{$loansMirrorData->reference_interest}}@endif" name="reference_interest" >
            </div>
        </div>

        <p style="font-weight: bold;">Überprüfung des RefZinses</p>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>alle drei Monate zum Ultimo:</label>
                @if ($errors->has('check_the_refinance_3'))
                    <div class="error">{{ $errors->first('check_the_refinance_3') }}</div>
                @endif
                <input type="text" class="form-control" id="check_the_refinance_3" value="@if(old('check_the_refinance_3')){{old('check_the_refinance_3')}}@elseif($loansMirrorData){{$loansMirrorData->check_the_refinance_3}}@endif" name="check_the_refinance_3" >
            </div>
            <div class="col-lg-6">
                <label>monatlich zum Ultimo:</label>
                @if ($errors->has('check_the_refinance_1'))
                    <div class="error">{{ $errors->first('check_the_refinance_1') }}</div>
                @endif
                <input type="text" class="form-control" id="check_the_refinance_1" value="@if(old('check_the_refinance_1')){{old('check_the_refinance_1')}}@elseif($loansMirrorData){{$loansMirrorData->check_the_refinance_1}}@endif" name="check_the_refinance_1" >
            </div>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>Veränderungsgrenze:</label>
                @if ($errors->has('change_limit'))
                    <div class="error">{{ $errors->first('change_limit') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="change_limit" value="@if(old('change_limit')){{old('change_limit')}}@elseif($loansMirrorData){{number_format($loansMirrorData->change_limit,2,',','.')}}@endif" name="change_limit" >
            </div>
            <div class="col-lg-6">
                <label>Behandlung bei Negativzins:</label>
                @if ($errors->has('treatment_at_negative'))
                    <div class="error">{{ $errors->first('treatment_at_negative') }}</div>
                @endif
                @php
                    $treatment_at_negative = "";
                    if(old('treatment_at_negative')){
                        $treatment_at_negative = old('treatment_at_negative');
                    }elseif($loansMirrorData && $loansMirrorData->treatment_at_negative){
                        $treatment_at_negative = $loansMirrorData->treatment_at_negative;
                    }
                @endphp 
                <select class="form-control" name="treatment_at_negative" style="width: 100%" id="treatment_at_negative">
                    <option value="">Please Select one</option>                            
                    <option value="Referenzzins = 0" @if($treatment_at_negative=="Referenzzins = 0")selected @endif > Referenzzins = 0 </option>
                    <option value="keine Besonderheit" @if($treatment_at_negative=="keine Besonderheit")selected @endif> keine Besonderheit< /option>                            
                </select>
            </div>
        </div>



        <legend>4.) Rückzahlung</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>Darlehensart:</label>
                @if ($errors->has('loan'))
                    <div class="error">{{ $errors->first('loan') }}</div>
                @endif
                <input type="text" class="form-control" id="loan" value="@if(old('loan')){{old('loan')}}@elseif($loansMirrorData){{$loansMirrorData->loan}}@endif" name="loan">
            </div>
        </div>
        <p style="font-weight: bold;">Laufzeit</p>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>Type:</label>
                @if ($errors->has('running_time_type'))
                    <div class="error">{{ $errors->first('running_time_type') }}</div>
                @endif
                @php
                    $running_time_type = "";
                    if(old('running_time_type')){
                        $running_time_type = old('running_time_type');
                    }elseif($loansMirrorData && $loansMirrorData->running_time_type){
                        $running_time_type = $loansMirrorData->running_time_type;
                    }
                @endphp 
                <select class="form-control" name="running_time_type" style="width: 100%" id="running_time_type">
                    <option value="">Please Select one</option>                            
                    <option value="Fest" @if($running_time_type=="Fest")selected @endif > Fest </option>
                    <option value="Variabel" @if($running_time_type=="Variabel")selected @endif> Variabel </option>   
                </select>
            </div>
            <div class="col-lg-6">
                <label>Laufzeit:</label>
            @if($loansMirrorData && $loansMirrorData->running_time_type=="Fest")  
                @php
                    $running_time = "";
                    if(old('running_time')){
                        $running_time = show_date_format(old('running_time'));
                    }elseif($loansMirrorData && $loansMirrorData->running_time){
                        $running_time = show_date_format($loansMirrorData->running_time);
                    }
                @endphp               
                <input type="text" class="form-control mask-date-input-new" id="running_time" value="{{$running_time}}" name="running_time" placeholder="DD.MM.YYYY">
            @else
                <input type="text" class="form-control" id="running_time" value="@if(old('running_time')){{old('running_time')}}@elseif($loansMirrorData){{$loansMirrorData->running_time}}@endif" name="running_time" >
            @endif
            </div>
        </div>


        <legend>5.) Nebenleistungen</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>Bereitstellungszins:</label>
                @if ($errors->has('standby_interest'))
                    <div class="error">{{ $errors->first('standby_interest') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="standby_interest" value="@if(old('standby_interest')){{old('standby_interest')}}@elseif($loansMirrorData){{number_format($loansMirrorData->standby_interest,2,',','.')}}@endif" name="standby_interest" >
            </div>
            <div class="col-lg-6">
                <label>Bereitstellungszins ab:</label>
                @if ($errors->has('standby_interest_from'))
                    <div class="error">{{ $errors->first('standby_interest_from') }}</div>
                @endif
                @php
                    $standby_interest_from = "";
                    if(old('standby_interest_from')){
                        $standby_interest_from = show_date_format(old('standby_interest_from'));
                    }elseif($loansMirrorData && $loansMirrorData->standby_interest_from){
                        $standby_interest_from = show_date_format($loansMirrorData->standby_interest_from);
                    }
                @endphp     
                <input type="text" class="form-control mask-date-input-new" id="standby_interest_from" value="{{$standby_interest_from}}" name="standby_interest_from" placeholder="DD.MM.YYYY">
            </div>
            <div class="col-lg-6">
                <label>Management Fee:</label>
                @if ($errors->has('management_fee'))
                    <div class="error">{{ $errors->first('management_fee') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="management_fee" value="@if(old('management_fee')){{old('management_fee')}}@elseif($loansMirrorData){{number_format($loansMirrorData->management_fee,2,',','.')}}@endif" name="management_fee" >
            </div>
            <div class="col-lg-6">
                <label>Darlehenskosten:</label>
                @if ($errors->has('loan_costs'))
                    <div class="error">{{ $errors->first('loan_costs') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="loan_costs" value="@if(old('loan_costs')){{old('loan_costs')}}@elseif($loansMirrorData){{number_format($loansMirrorData->loan_costs,2,',','.')}}@endif" name="loan_costs" >
            </div>
            <div class="col-lg-6">
                <label>Grundbuchkosten:</label>
                @if ($errors->has('registry_fees'))
                    <div class="error">{{ $errors->first('registry_fees') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="registry_fees" value="@if(old('registry_fees')){{old('registry_fees')}}@elseif($loansMirrorData){{number_format($loansMirrorData->registry_fees,2,',','.')}}@endif" name="registry_fees" >
            </div>
        </div>
        
        <p style="font-weight: bold;">Exitfee</p>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                @if ($errors->has('exit_fee'))
                    <div class="error">{{ $errors->first('exit_fee') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="exit_fee" value="@if(old('exit_fee')){{old('exit_fee')}}@elseif($loansMirrorData){{number_format($loansMirrorData->exit_fee,2,',','.')}}@endif" name="exit_fee">
            </div>
        </div>



        <legend>6.) Besicherung</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>Grundschuld:</label>
                @if ($errors->has('mortgage'))
                    <div class="error">{{ $errors->first('mortgage') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="mortgage" value="@if(old('mortgage')){{old('mortgage')}}@elseif($loansMirrorData){{number_format($loansMirrorData->mortgage,2,',','.')}}@endif" name="mortgage" >
            </div>
            <div class="col-lg-6">
                <label>Grundschuld zugunsten Anleihe:</label>
                @if ($errors->has('mortgage_in_bond'))
                    <div class="error">{{ $errors->first('mortgage_in_bond') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="mortgage_in_bond" value="@if(old('mortgage_in_bond')){{old('mortgage_in_bond')}}@elseif($loansMirrorData){{number_format($loansMirrorData->mortgage_in_bond,2,',','.')}}@endif" name="mortgage_in_bond" >
            </div>


            
        </div>
        
        <p style="font-weight: bold;">Gesellschafterdarlehensbelassung</p>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                @if ($errors->has('is_shareholder_loan'))
                    <div class="error">{{ $errors->first('is_shareholder_loan') }}</div>
                @endif
                <label><input type="radio" class="is_shareholder_radio" value="1" name="is_shareholder_loan" id="is_shareholder_loan_yes" @if( $loansMirrorData && $loansMirrorData->is_shareholder_loan) Checked="checked" @endif> : Ja</label>
                <label><input type="radio" class="is_shareholder_radio" value="0" name="is_shareholder_loan" id="is_shareholder_loan_no" @if($loansMirrorData && !$loansMirrorData->is_shareholder_loan) Checked="checked" @endif> : Nein</label>     
                
                <input type="text" class="form-control" id="shareholder_loan" value="@if(old('shareholder_loan')){{old('shareholder_loan')}}@elseif($loansMirrorData){{$loansMirrorData->shareholder_loan}}@endif" name="shareholder_loan" @if($loansMirrorData && $loansMirrorData->is_shareholder_loan)style="display: none;"@endif>
            </div>
        </div>
        
        <p style="font-weight: bold;">Drittgarantie</p>
        <div class="form-group m-form__group row">
            <div class="col-lg-6">
                <label>Garantiegeber:</label>
                @if ($errors->has('guarantor'))
                    <div class="error">{{ $errors->first('guarantor') }}</div>
                @endif
                <input type="text" class="form-control" id="guarantor" value="@if(old('guarantor')){{old('guarantor')}}@elseif($loansMirrorData){{$loansMirrorData->guarantor}}@endif" name="guarantor" >
            </div>
            <div class="col-lg-6">
                <label>Garantiehöhe:</label>
                @if ($errors->has('extent_of_guarantee'))
                    <div class="error">{{ $errors->first('extent_of_guarantee') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="extent_of_guarantee" value="@if(old('extent_of_guarantee')){{old('extent_of_guarantee')}}@elseif($loansMirrorData){{number_format($loansMirrorData->extent_of_guarantee,2,',','.')}}@endif" name="extent_of_guarantee" >
            </div>
            <div class="col-lg-6">
                <label>Garantiegeber:</label>
                @if ($errors->has('mortgage_guarantor'))
                    <div class="error">{{ $errors->first('mortgage_guarantor') }}</div>
                @endif
                <input type="text" class="form-control" id="mortgage_guarantor" value="@if(old('mortgage_guarantor')){{old('mortgage_guarantor')}}@elseif($loansMirrorData){{$loansMirrorData->mortgage_guarantor}}@endif" name="mortgage_guarantor">
            </div>
            <div class="col-lg-6">
                <label>Garantiehöhe:</label>
                @if ($errors->has('mortgage_extent_of_guarantee'))
                    <div class="error">{{ $errors->first('mortgage_extent_of_guarantee') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="mortgage_extent_of_guarantee" value="@if(old('mortgage_extent_of_guarantee')){{old('mortgage_extent_of_guarantee')}}@elseif($loansMirrorData){{number_format($loansMirrorData->mortgage_extent_of_guarantee,2,',','.')}}@endif" name="mortgage_extent_of_guarantee" >
            </div>
        </div>
                

        

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" class="btn btn-primary">Speichern</button>                   
            </div>
        </div>

        </form>
	</div>
</div>


<script>
$(document).ready(function(){
    $(".is_shareholder_radio").change(function(){
        if($('#is_shareholder_loan_yes').is(':checked')){
            $('#shareholder_loan').show();
        }else{
            $('#shareholder_loan').hide();
        }
    });

    $('#running_time_type').change(function(){
        if($(this).val()  == "Fest"){
            $('#running_time').mask('00/00/0000');
            $('#running_time').attr('placeholder', 'DD/MM/YYYY');
        }else{
            $('#running_time').unmask();
            $('#running_time').attr('placeholder', '');
        }

    })

});
</script>



