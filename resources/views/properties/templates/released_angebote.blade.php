@if($insurance_tab && count($insurance_tab) > 0)
	@foreach ($insurance_tab as $key => $value)

		@php
			$value->comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                            		->join('users as u', 'u.id', 'pc.user_id')
                            		->where('pc.record_id', $value->id)
                            		->where('pc.type', 'property_insurance_tabs')
                            		->orderBy('pc.created_at', 'desc')
                            		->first();

            $value->detail = DB::table('property_insurance_tab_details as pid')
                            ->select('pid.*','u.name as user_name', 'p.name_of_property', 'pit.property_id',
                                DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurancetab_release' AND selected_id = pid.id AND tab='insurancetab2' ) THEN 1 ELSE 0 END as is_release"),
                                DB::raw("(SELECT properties_mail_logs.created_at FROM properties_mail_logs WHERE (type = 'insurance_release' OR type = 'insurancetab_release') AND selected_id = pid.id AND (tab='insurance_tab' OR tab='insurancetab2') ORDER BY properties_mail_logs.created_at DESC LIMIT 1) as release_date")
                            )
                            ->join('property_insurance_tabs as pit', 'pit.id', '=', 'pid.property_insurance_tab_id')
                            ->join('properties as p', 'p.id', '=', 'pit.property_id')
                            ->join('users as u', 'u.id', '=', 'pid.user_id')
                            ->where('property_insurance_tab_id', $value->id)
                            ->where('pid.deleted', 0)
                            ->orderBy('release_date', 'DESC')
                            ->get();
		@endphp
		<div class="row">
			<div class="col-md-12">
				<span class="pull-left"><b>{{ $value->name_of_property }} / {{ $value->title }}</b></span>
				<button type="button" class="btn btn-primary btn-show-angebote-section-comment btn-xs pull-left" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" data-id="{{ $value->id }}" data-property-id="{{ $value->property_id }}" style="margin-left: 10px;">Show Kommentar</button>
				<div class="section_latest_comment pull-left" style="margin-left: 10px;font-size: 16px;">
					@if(isset($value->comments))
						@php
							$company = ($value->comments->role >= 6 && $value->comments->company) ? ' ('.$value->comments->company.')' : '';
							$commented_user = $value->comments->name.''.$company;
						@endphp
						<p><span class="commented_user">{{ $commented_user }}</span>: {{ $value->comments->comment }} ({{ show_datetime_format($value->comments->created_at) }})</p>
					@endif
				</div>
			</div>
			<div class="col-md-12 table-responsive">
				<table class="table table-striped tbl-insurance-tab-detail" data-tabid="{{ $value->id }}" id="insurance-table-{{ $value->id }}" data-type="VON FALK FREIGEGEBEN" data-sub="Von Falk freigegeben">
					<thead>
						<tr>
							<th>#</th>
							<th>Datei</th>
							<th>Betrag</th>
							<th>Kommentar</th>
							<th>User</th>
							<th>Datum</th>
							<th>Freigeben Datum</th>
							<th>Ablehnen AM</th>
							<th>Falk Kommantare</th>
							<th>Empfehlung</th>
							<th>Freigeben</th>
							<th>Pending</th>
							<th>Ablehnen</th>
							<th>Action</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>
						@if($value->detail)
							@foreach ($value->detail as $k => $val)

								@php
									$file_basename = '';
					                if(isset($val->file_basename))
					                    $file_basename = $val->file_basename;

									$download_path = "https://drive.google.com/drive/u/2/folders/".$file_basename;
					                $downlod_file_path = '';
					                if($val->file_type == "file"){
					                    $download_path = "https://drive.google.com/file/d/".$file_basename;
					                }

					                $comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
					                            ->join('users as u', 'u.id', 'pc.user_id')
					                            ->where('pc.record_id', $val->id)
					                            ->where('pc.type', 'property_insurance_tab_details')
					                            ->orderBy('pc.created_at', 'desc')
					                            ->limit(2)->get();
					                $latest_comment = '';
					                if($comments){
					                    $latest_comment .= '<div class="show_cmnt">';
					                    foreach ($comments as $n => $comment) {
					                        $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
					                        $commented_user = $comment->name.''.$company;
					                        $latest_comment .= '<p><span class="commented_user">'.$commented_user.'</span>: '.$comment->comment.' ('.show_datetime_format($comment->created_at).')</p>';
					                    }
					                    $latest_comment .= '</div>';
					                }
					                if($latest_comment){
					                    $latest_comment .= '<a href="javascript:void(0);" data-url="'. route('get_property_insurance_detail_comment', ['id' => $val->id]) .'" class="load_comment">Show More</a>';
					                }
					                $comment_button = '<button type="button" class="btn btn-primary btn-xs btn-ins-comment" data-id="'.$val->id.'" data-property-id="'.$value->property_id.'" data-url="'.route('get_property_insurance_detail_comment', ['id' => $val->id]).'">Kommentar</button>';
								@endphp


								@php
									$subject = 'Von Falk freigegeben: '.$val->name_of_property;

                					$mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$val->property_id.'" data-id="'.$val->id.'" data-subject="'.$subject.'" data-content="" data-title="VON FALK FREIGEGEBEN" data-reload="0" data-section="property_insurance_tab_details">Weiterleiten an</button>';

                					$delete_btn = '<button type="button" data-url="'. route('delete_property_insurance_detail', ['id' => $val->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm delete-insurance-tab-detail"><i class="icon-trash"></i></button>';

                					$edit_btn = '<button type="button" data-id="'. $val->id .'" data-url="'. route('get_property_insurance_detail_by_id', ['id' => $val->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm edit-insurance-tab-detail"><i class="fa fa-edit"></i></button>';

                					$am_checkbox = '<input data-id='.$val->id.' type="checkbox" class="am_falk_status" data-field="asset_manager_status" data-url="'. route('update_property_insurance_status', ['id' => $val->id]) .'" '. ( ($val->asset_manager_status == 1) ? 'checked' : '' ) .'>';

					                $falk_checkbox = "";
					                $not_release = "";
					                $not_release_am = "";
					                $pending = "";

                					if(in_array($val->property_insurance_tab_id, array(4,6,7,13,11,14,10,17,24,18,12,25,27,28))){
                        				$falk_checkbox = '<input data-id='.$val->id.' type="checkbox" class="" data-field="falk_status" data-url="'. route('update_property_insurance_status', ['id' => $val->id]) .'" '. ( ($val->falk_status == 1) ? 'checked' : '' ) .'>';
                					}else{
                    					if($user->email==config('users.falk_email')){
					                        $rbutton_title = "Freigeben";
					                        $release = 'btn btn-xs btn-primary property-insurance-release-request';
					                        $release_type = "insurancetab_release";
					                        $falk_checkbox = '<button data-id="'.$val->id.'" type="button" class="btn '.$release.'" data-column="'.$release_type.'">'.$rbutton_title.'</button>';

					                        if($val->not_release == 0 && $val->is_release == 0){
					                            $not_release = '<button data-id="'.$val->id.'" type="button" class="btn btn-xs btn-primary btn-not-release-insurance">Ablehnen</button>';
					                            $not_release_am = '<button data-id="'.$val->id.'" type="button" class="btn btn-xs btn-primary btn-not-release-am-insurance">Ablehnen AM</button>';
					                            $pending = '<button data-id="'.$val->id.'" type="button" class="btn btn-xs btn-primary btn-pending-insurance">Pending</button>';
					                        }
					                    }

                    					if($val->is_release){
                        					$falk_checkbox = '<button data-id="'.$val->id.'" type="button" class="btn btn-xs btn-success">Freigegeben</button>';
                    					}
                					}

                					if($val->is_release){
                    					$delete_btn = $edit_btn = "";
                    					$am_checkbox = '<input data-id='.$val->id.' type="checkbox" class="" data-field="asset_manager_status" data-url="'. route('update_property_insurance_status', ['id' => $val->id]) .'" '. ( ($val->asset_manager_status == 1) ? 'checked' : '' ) .'>';

                					}
								@endphp

								<tr class="{{ ($val->release_date) ? '' : 'hidden' }}">
									<td>{{ ($k+1) }}</td>
									<td>
										<a  target="_blank"  title="{{ $val->file_name }}"  href="{{ $download_path }}">{{ $val->file_name }}</a> <a target="_blank" href="{{ $downlod_file_path }}" title="Download"><i class="fa fa-download fa-fw"></i></a>
									</td>
									<td>{{ show_number($val->amount,2) }}</td>
									<td>{!! $latest_comment !!}<br>{!! $comment_button !!}</td>
									<td>{{ $val->user_name }}</td>
									<td>{{ show_datetime_format($val->created_at) }}</td>
									<td>{{ show_datetime_format($val->release_date) }}</td>
									<td>{!! $not_release_am !!}</td>
									<td>{{ $val->falk_comment }}</td>
									<td>{!! $am_checkbox !!}</td>
									<td>{!! $falk_checkbox !!}</td>
									<td>{!! $pending !!}</td>
									<td>{!! $not_release !!}</td>
									<td>{!! $delete_btn !!}</td>
									<td>{!! $mail_button !!}</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
	@endforeach
@else
	<p>Data not Available</p>
@endif