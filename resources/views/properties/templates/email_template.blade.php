
<!-- Styles -->

<div class="row">
    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon fa fa-times"></i></button>
            <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
        </div>
    @endif

</div>

 @if(isset($email_template))
<div class="row">

        <div class="col-sm-4">
            <br>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#send_email_modal"  class="btn btn-primary btn-block">LOI versenden</a>
        </div>

        <div class="col-sm-4">
            <br>
            <a href="{{ route('send_email_template') }}?property_id={{ $id }}" target="_blank" class="btn btn-success btn-block">Export PDF</a>
        </div>

</div>
<br>
@endif

<div class="white-box">

<div class="row list-statusloi-div">
                <table class="table table-striped" id="list-banks">
                    <thead>
                    <tr>
                        <th>Transaction Manager</th>
                        <th>Datum</th>
                        <th>Versendet</th>
                        <th>Antwort erhalten</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
                </div>


<form action="{{ route('email_template_save') }}" method="post"  enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
     <input type="hidden" name="property_id" value="{{ $id }}">
<?php
$ist_sheet = "";
if(isset($GLOBALS['ist_sheet']) && $GLOBALS['ist_sheet'])
  $ist_sheet = $GLOBALS['ist_sheet'];

$address2 = $address3 = "";
if(!isset($email_template) && $ist_sheet)
{
    $address2 = $ist_sheet->strasse;
    $address3 = $ist_sheet->plz_ort.' '.$ist_sheet->ort;
}

if(isset($email_template))
    $address2 = $email_template->address2;
if(isset($email_template))
    $address3 = $email_template->address3;


    $bank_array = array();
    $j = 0;
    $ist = 0;
    foreach($banks as $key => $bank)
    {
        $bank_array[] = $bank->id;
    }

    array_unique($bank_array);
    $str = implode(',', $bank_array);
    $property_sheet = DB::table('properties')->whereRaw('main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('standard_property_status','desc')->first();
?>


<div class="row">
    <div class="col-sm-12">
    @php
    $tm_email_value = "";
    if(isset($email_template)){
        $tm_email_value = $email_template->email;
    }else{
        if($contact_person){
            $tm_email_value = $contact_person->email;
        }
    }
    @endphp
    E-Mail Transactionmanager <input id="email" type="text" class=" form-control email_input replica-input" value="{{$tm_email_value}}" name="email" > <br>
     <div class="row">
         <div class="col-sm-6">
             Empfänger: Firmenname<input id="a" type="text" class="form-control replica-input" value="@if(isset($email_template)){{ $email_template->address1 }} @endif" name="address1" ><br>
         </div>
         <div class="col-sm-6">
            Empfänger: Ansprechpartner<input id="a2" type="text" class="form-control replica-input" value="@if(isset($email_template)){{ $email_template->address2 }} @endif" name="address2" ><br>
         </div>
        <div class="col-sm-6">
           Empfänger: Strasse<input id="a3" type="text" class="form-control replica-input" value="@if(isset($email_template)){{ $email_template->address3 }} @endif" name="address3"><br>
        </div>
        <div class="col-sm-6">
            Empfänger: Ort<input  id="a4" type="text" class="form-control replica-input" value="@if(isset($email_template)){{ $email_template->address4 }} @endif" name="address4"><br>
        </div>
        <div class="col-sm-6">
            München, <input style="height: 38px ;width: 100%" type="text" class="form-control" value="@if(isset($email_template)){{ \Carbon\Carbon::parse($email_template->date)->format('d.m.Y') }}@else{{ \Carbon\Carbon::now()->format('d.m.Y') }}@endif" name="date" id='datepicker'><br>
        </div>
        <div class="col-sm-6">
        Kaufpreis <input  type="text"  class=" form-control replica-input" value="@if(isset($email_template) && $email_template->price){{ $email_template->price }} @else {{ number_format($property_sheet->gesamt_in_eur,2,',','.') }} @endif" name="price" > <br>
        </div>
        <div class="col-sm-6">
        Kaufpreis in Worten <input   type="text" class=" form-control replica-input" value="@if(isset($email_template)){{ $email_template->random_text }} @endif" name="random_text" > <br>
        </div>

	@if(isset($email_template))
<?php
    $propertyname = $email_template->objekt;
?>
@elseif($ist_sheet)
<?php
    $propertyname = $ist_sheet->name_of_property;
?>
@endif

    <div class="col-sm-6">
        <b>Objektname<input id="objekt" value="{{$propertyname}}" type="text" class="form-control replica-input" name="objekt"></b><br>
    </div>




     </div>
    </div>







<p>
<p>
<p>
<p>
    <div class="col-sm-6" style="width: 100%; flex: left; margin-top: 20px;">
    <b style=" font-weight: bold ; display:inline-flex; width:50%;">
        <input id="plz_otr"
         value="{{($email_template->plz_ort)?$email_template->plz_ort:"Kaufpreisangebot für das Objekt in ".($properties->plz_ort)." ".($properties->ort)}}" type="text" class="form-control replica-input" name="plz_ort"></b><br><br><br>
       <div class="row">
           <div class="col-sm-6">
               <b>Objektname
                   <input id="dear_text" value="{{($email_template->dear_text)?$email_template->dear_text:'Sehr geehrte Damen und Herren'}}" type="text" class="form-control replica-input" name="dear_text">
                   <input id="objekt" value="{{($email_template->auf_grund)?$email_template->auf_grund:'auf Grund einer ersten Prüfung der uns überlassenen Unterlagen bieten wir Ihnen für das Objekt
    in' }}" type="text" class="form-control replica-input" name="auf_grund">  {{$properties->plz_ort}} {{$properties->ort}}, {{$properties->strasse}} {{$properties->hausnummer}}</b>
           </div>
           <div class="col-sm-6">
               <b>Objektname<input id="objekt" value="{{($email_template->einen)?$email_template->einen:' einen Kaufpreis in Höhe von' }}" type="text" class="form-control replica-input" name="einen"></b><br>
           </div>
       </div>

    <h1><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @if(isset($email_template)){{ $email_template->price }} EUR @endif</strong></h1>
    <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; @if(isset($email_template)) {{ $email_template->random_text }} @endif</p>

</div>





    <div class="col-sm-12">
        <b></b><textarea class="form-control" name="custom_text">
            @if(isset($email_template) && $email_template->custom_text)
                {!! $email_template->custom_text !!}
            @else
                An dieses Kaufpreisangebot halten wir uns ab Angebotsabgabe zehn Wochen gebunden.
                Der Verkäufer sichert der FCR Immobilien AG eine sechswöchige Exklusivität ab
                Kaufpreiszusage zu. Er wird bis zu diesem Zeitpunkt bereits laufende Verhandlungen nicht
                fortführen und das betreffende Objekt keinem Dritten direkt oder indirekt anbieten. Es ist geplant,
                eine Beurkundung bis zum Ende der Exklusivitätsphase durchzuführen.<br><br>

                Das Kaufpreisangebot steht unter Gremienvorbehalt und vorbehaltlich einer positiven Due
                Diligence.<br><br>

                Wir bitten Sie das Angebot an den Eigentümer / den Verkäufer weiterzuleiten und um
                schriftliche Bestätigung dieses Angebots durch den entsprechenden Handlungsbevollmächtigten.<br><br>

                Auf eine positive Antwort freuen wir uns und verbleiben,<br>
            @endif
        </textarea><br>
    </div>
    {{--<div class="col-sm-6">--}}
        {{--Anrede<input id="herr_schilke" value="@if(isset($email_template)){{ $email_template->herr_schilke }} @endif" type="text" class="form-control replica-input" name="herr_schilke"><br>--}}
    {{--</div>--}}
    {{--<div class="col-sm-6">--}}
        {{--Kaufpreis (€)<b><input id="Preis" value="@if(isset($email_template)){{ $email_template->price }} @endif" type="text" class="form-control replica-input" name="price"></b><br>--}}
    {{--</div>--}}

{{--<div class="col-sm-6">--}}
    {{--Kaufpreis in Worten<input id="line1" type="text" value="@if(isset($email_template)){{ $email_template->line1 }} @endif" class="form-control replica-input" name="line1">--}}
{{--</div>--}}

{{--<div class="col-sm-6">--}}
    {{--Zusatzbeschreibung<input id="line2" type="text" value="@if(isset($email_template)){{ $email_template->line2 }} @endif" class="form-control replica-input" name="line2"><br>--}}
{{--</div>--}}
 {{--<div class="col-sm-6">--}}
    {{--Angebotsdauer in Wochen (Zahl) <input id="Angebotsabgabe" value="@if(isset($email_template)){{ $email_template->word1 }} @endif" name="word1" type="text" class="form-control replica-input"><br>--}}
 {{--</div>--}}
 {{--<div class="col-sm-6">--}}
     {{--Der Verkäufer sichert der FCR Immobilien AG eine ... Exklusivität ab Kaufpreiszusage <input id="word2" value="@if(isset($email_template)){{ $email_template->word2 }} @endif" name="word2" type="text" class="form-control replica-input"><br>--}}
 {{--</div>--}}

 <div></div>
    <div class="col-sm-6">
        Titel Transactionmanagement
        <select class="form-control" name="transaction" id="mySelect" >
            <option @if(isset($email_template)) @if($email_template->transaction == 'Transaction Manager') selected @endif @endif value="Transaction Manager">Transaction Manager</option>
            <option @if(isset($email_template)) @if($email_template->transaction == 'Transaction Management') selected @endif @endif value="Transaction Management">Transaction Management</option>
            <option @if(isset($email_template)) @if($email_template->transaction == 'FCR Immobilien AG') selected @endif @endif value="FCR Immobilien AG">FCR Immobilien AG</option>
            <option @if(isset($email_template)) @if($email_template->transaction == 'Director Transaction Management') selected @endif @endif value="Director Transaction Management">Director Transaction Management</option>
            <option @if(isset($email_template)) @if($email_template->transaction == 'Head of Transaction Management') selected @endif @endif value="Head of Transaction Management">Head of Transaction Management</option>
            <option @if(isset($email_template)) @if($email_template->transaction == 'Finanzierungsspezialist') selected @endif @endif value="Finanzierungsspezialist">Finanzierungsspezialist</option>
        </select>
    </div>
    <div class="col-sm-6">

        @if(isset($email_template_users))
        Transaction Manager
        <select class="form-control" name="email_template_users" id="email_template_users">
        <option value="0">select</option>
        @foreach($email_template_users as $template_user)
        @php
            $if_tm_selected = "";
            if($email_template->email_template_users == $template_user->id){
                $if_tm_selected = "selected";
            }else if($contact_person && $contact_person->id == $template_user->id){
                $if_tm_selected = "selected";
            }
        @endphp
        <option {{$if_tm_selected}}  value="{{ $template_user->id }}">{{ $template_user->name }}</option>
        @endforeach

        @endif

        </select>
    </div>
    <div class="col-sm-6">
      Titel Transactionmanagement (2)
        <select class="form-control" name="second_signatory_transaction" >
            <option value="">select</option>
            <option @if(isset($email_template)) @if($email_template->second_signatory_transaction == 'Transaction Manager') selected @endif @endif value="Transaction Manager">Transaction Manager</option>
            <option @if(isset($email_template)) @if($email_template->second_signatory_transaction == 'Transaction Management') selected @endif @endif value="Transaction Management">Transaction Management</option>
            <option @if(isset($email_template)) @if($email_template->second_signatory_transaction == 'FCR Immobilien AG') selected @endif @endif value="FCR Immobilien AG">FCR Immobilien AG</option>
            <option @if(isset($email_template)) @if($email_template->second_signatory_transaction == 'Director Transaction Management') selected @endif @endif value="Director Transaction Management">Director Transaction Management</option>
            <option @if(isset($email_template)) @if($email_template->second_signatory_transaction == 'Head of Transaction Management') selected @endif @endif value="Head of Transaction Management">Head of Transaction Management</option>


        </select>
    </div>
    <div class="col-sm-6">

        @if(isset($email_template_users))
           Transaction Manager (2)
            <select class="form-control" name="second_signatory">
                <option value="">select</option>
                @foreach($email_template_users as $template_user)
                    <option @if($email_template->second_signatory == $template_user->id) selected @endif  value="{{ $template_user->id }}">{{ $template_user->name }}</option>
                @endforeach

                @endif

            </select>
    </div>
        <br><br>
</div>
    <br><br>

<center>
    <button type="submit" class="btn btn-success" type="submit" style="padding: 10px 50px;" >Speichern</button>
</center>
</form>
</div>



<!-- Styles -->
<style type="text/css">
    .noBorder{
        border:none;
     }

    input[type="date"] {
        position: relative;
        width: 150px; height: 20px;
        color: white;
    }
    input[type="date"]:before {
        position: absolute;
        top: 3px; left: 3px;
        content: attr(data-date);
        display: inline-block;
        color: black;
    }
    input[type="date"]::-webkit-datetime-edit, input[type="date"]::-webkit-inner-spin-button, input[type="date"]::-webkit-clear-button {
        display: none;
    }
    input[type="date"]::-webkit-calendar-picker-indicator {
        position: absolute;
        top: 3px;
        right: 0;
        color: black;
        opacity: 1;
    }
    .horizontal-line {
        width: 150px;
        display: inline-block;
        height: 1px;
        background: #ccc;
        margin-left: 5px;
    }
</style>


<div class="row">
    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
        </div>
    @endif

</div>



 @section('email_template_javascript')
    <script>


		function t_here(){

			$("#t_here").html('');

			var a5 = $("#a5").val();
			if(a5 == "123"){

				$("#t_here").html(a5);
			}

		}

        $(document).ready(function(){


			t_here();





            $('#datepicker').datepicker({
                dateFormat: 'dd.mm.yy'
            });
            $('#email_template_users').on('change', function () {

                $.ajax({
                    type : 'POST',
                    url : "{{ url('email_template_users') }}",
                    dataType: "json",
                    data : {user_id:$(this).val(), id:{{ $id }} ,_token : '{{ csrf_token() }}' },
                    beforeSend: function () {
                        $('.user_signature').removeAttr('src');
                        $('.user_signature').attr('src',"{{ url('/').'/img/giphy.gif' }}");
                    },
                    success : function (data) {


                        console.log(data);
                        $('.email_input').val('');
                        $('.email_input').val(data.email);
                        $('.user_signature').removeAttr('src');
                        if(data.signature=="")
                            $('.user_signature').hide();
                        else{
                            $('.user_signature').show();
                            $('.user_signature').attr('src',data.signature);
                        }
                    }
                });
            });
        });

        $('.replica-input').keypress( function () {
            // alert($(this).attr("id").concat(1));
            var x = document.getElementById($(this).attr("id")).value;
            var id = $(this).attr("id").concat(1);
            document.getElementById(id).value = x;

        });
         //
         // function myFunction()
         // {
         //
         //    var x = document.getElementById(this.id).value;
         //    document.getElementById("demo"+1).value = x;
         // }

        // $("#myId").on('change keydown paste input', function(){
        //     alert();
        // });

        CKEDITOR.replace( 'custom_text' );

     </script>
@endsection
