{{-- <ul class="nav nav-tabs">
  	<li class="active"><a data-toggle="tab" href="#mail_an_am">Mail an AM</a></li>
  	<li><a data-toggle="tab" href="#mail_a_hv_bu">Mail an HV BU</a></li>
    <li><a data-toggle="tab" href="#mail_a_hv_pm">Mail an HV PM</a></li>
    <li><a data-toggle="tab" href="#mail_an_ek">Mail an EK</a></li>
  	<li><a data-toggle="tab" href="#mail_an_user">Mail an User</a></li>
</ul>

<div class="tab-content">
  	<div id="mail_an_am" class="tab-pane fade in active">
    	@include('properties.templates.am_mail_logs')
  	</div>
  	<div id="mail_a_hv_bu" class="tab-pane fade">
    	@include('properties.templates.hv_bu_mail_logs')
  	</div>
  	<div id="mail_a_hv_pm" class="tab-pane fade">
    	@include('properties.templates.hv_pm_mail_logs')
  	</div>
    <div id="mail_an_ek" class="tab-pane fade">
      @include('properties.templates.ek_mail_logs')
    </div>
    <div id="mail_an_user" class="tab-pane fade">
      @include('properties.templates.user_mail_logs')
    </div>
</div> --}}

<!--<div class="col-sm-12 table-responsive white-box">
  <table class="table table-striped" id="am_mail_table">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Nachricht</th>
        <th>Datum/Uhrzeit</th>
      </tr>
    </thead>
    <tbody>
      @if(!empty($assetsManagerMails))
        @php
          $count = 1;
        @endphp
        @foreach ($assetsManagerMails as $key => $value)
            <tr>
              <td>{{ $count }}</td>
              <td>
                  
                  @if($value->type == 5)

                    <?php 
                      $email_arr = [];
                      if($value->custom_email){
                        $arr = explode(",", $value->custom_email);
                        $arr = array_unique($arr);
                        $email_arr = $arr;
                      }
                    ?>
                    @if(!empty($email_arr))
                      <a href="javascript:void(0);" class="custom_user" data-property-id="{{ $value->property_id }}" data-user-id="" data-subject="{{ $value->subject }}" data-content='{{ $value->message }}' data-title="{{ $value->mail_type }}" data-reload="1" data-email="{{ implode(",", $email_arr) }}">{{ implode(", ", $email_arr) }}</a>
                    @endif

                  @elseif($value->type == 6)

                    <a href="javascript:void(0);" type="button" class="btn-forward-to" data-property-id="{{ $value->property_id }}" data-id="{{ $value->section_id }}" data-subject="{{ $value->subject }}" data-content="" data-title="{{ $value->mail_type }}" data-reload="1" data-email="{{ $value->custom_email }}" data-user="{{ $value->am_id }}">{{ $value->name }}{{ ($value->custom_email) ? ', '.$value->custom_email : '' }}</a>

                  @else

                    <a href="javascript:void(0);" class="custom_user" data-property-id="{{ $value->property_id }}" data-user-id="{{ $value->am_id }}" data-subject="{{ $value->subject }}" data-content='{{ $value->message }}' data-title="{{ $value->mail_type }}" data-reload="1">{{ $value->name }}</a>

                  @endif

              </td>
              <td><?php echo $value->message; ?></td>
              <td>{{ show_datetime_format($value->created_at) }}</td>
            </tr>
            @php
              $count++;
            @endphp
        @endforeach
      @endif
    </tbody>
  </table>
</div>-->

<div class="white-box" id="all_mail" data-url="{{ route('get_property_mail') }}?property_id={{ $id }}&section=tab">
  
</div>