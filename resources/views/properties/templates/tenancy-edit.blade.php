<h1>Mieterliste</h1>

<input type="hidden" class="property_id" value="<?php echo $id?>">

<div class="row tenancy-schedules tenant-edit-tab" id="tenant-edit-tab" style="margin-top: 20px;">
  <div class="col-sm-12 tenant-master-list">
    <a href="javascript:void(0)" class="btn btn-primary add-new-area">Neue</a>
    @if(isset($tenant_master) && $tenant_master)
      @foreach($tenant_master as $item)
        <?php
          if(!$item->item_name)
            $item->item_name = 'empty';
        ?>
        <h3>
          <label style="width: 30%;float: left;">
            <a href="javascript::void(0);" class="inline-edit" data-type="text" data-pk="item_name" data-placement="right" data-url="{{ route('update_tenant_master', ['id' => $item->id]) }}" data-title="">{{ $item->item_name }}</a>
          </label>
          <div style="width: 68%;">
            <a href="javascript:void(0)" data-class="sheet-tenant-{{ $item->id }} " class="btn btn-sm btn-info show-links">Vermietungsaktivitäten  <i class="fa fa-angle-down"></i></a>
          </div>
        </h3>

        <br>

        <div class="sheet-tenant-{{ $item->id }} hidden">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe src="about:blank" data-src="{{route('iframe_tenant_master', ['id' => $item->id])}}" class="embed-responsive-item" width="100%" style="border:none;" scrolling="auto"></iframe>
          </div>
        </div>

      @endforeach
    @endif
  </div>
</div>
