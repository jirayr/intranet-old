@extends('layouts.iframe')

@section('css')
  <style type="text/css">
    .mask-number-input{
      text-align: right;
    }
  </style>
@endsection

@section('content')
  
  <div class="row">
    <div class="col-md-12">
      @include('partials.flash')
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="modal-dialog modal-lg" style="width: 100%;margin-top: 0px;margin-bottom: 0px;">
        <div class="modal-content">
          {{-- <div class="modal-header">
            <h4 class="modal-title">test</h4>
          </div> --}}
          <div class="modal-body">

            <form action="{{ route('save_tenant_master', ['id' => $tenant_master->id, 'item_id' => (isset($tenant_master->item->id)) ? $tenant_master->item->id : '']) }}" method="POST">

              {{ csrf_field() }}

              <div class="row">

                <div class="form-group col-md-4">
                  <label>Name</label>
                  <input type="text" name="item_name" class="form-control item_name" placeholder="Name" value="{{ $tenant_master->item_name }}">
                </div>

                <div class="form-group col-md-4">
                  <label>Type</label>
                  <select class="form-control item_type" name="item_type">
                    <option value="">Select Type</option>
                    <option value="{{config('tenancy_schedule.item_type.business')}}" {{ ($tenant_master->item_type == config('tenancy_schedule.item_type.business')) ? 'selected' : '' }} >Gewerbe Vermietet</option>
                    <option value="{{config('tenancy_schedule.item_type.live')}}" {{ ($tenant_master->item_type == config('tenancy_schedule.item_type.live')) ? 'selected' : '' }} >Wohnen Vermietet</option>
                    <option value="{{config('tenancy_schedule.item_type.live_vacancy')}}" {{ ($tenant_master->item_type == config('tenancy_schedule.item_type.live_vacancy')) ? 'selected' : '' }} > Wohnen Leerstand</option>
                    <option value="{{config('tenancy_schedule.item_type.business_vacancy')}}" {{ ($tenant_master->item_type == config('tenancy_schedule.item_type.business_vacancy')) ? 'selected' : '' }} >Gewerbe Leerstand</option>
                    <option value="{{config('tenancy_schedule.item_type.free_space')}}" {{ ($tenant_master->item_type == config('tenancy_schedule.item_type.free_space')) ? 'selected' : '' }}>Gewerbe Technische FF & Leerstand strukturell</option>
                  </select>
                </div>

                <div class="form-group col-md-4">
                  <label>Area Space</label>
                  <input type="text" name="area_space" class="form-control mask-number-input area_space" placeholder="Area Space" value="{{ show_number($tenant_master->area_space, 2) }}">
                </div>

                @if($type != config('tenancy_schedule.item_type.business_vacancy'))

                  <div class="form-group col-md-4">
                    <label>Mieter geschlossen</label>
                    <select class="form-control" name="tenant_closed">
                        @foreach($td1 as $k=>$list)
                          <option value="{{$k}}" {{ (isset($tenant_master->item->tenant_closed) && $tenant_master->item->tenant_closed == $k) ? 'selected' : '' }} >{{$list}}</option>
                        @endforeach
                    </select>
                  </div>

                  <div class="form-group col-md-4">
                    <label>Mietzahlung</label>
                    <select class="form-control" name="rent_payment">
                        @foreach($td2 as $list)
                            <option value="{{ $list }}" {{ (isset($tenant_master->item->rent_payment) && $tenant_master->item->rent_payment == $list) ? 'selected' : '' }} >{{$list}}</option>
                        @endforeach
                    </select>
                  </div>

                @endif

                <div class="form-group col-md-4">
                  <label>Kommentar</label>
                  <textarea name="comment3" rows="1" class="form-control" style="height: 42px;">{{ (isset($tenant_master->item->comment3)) ? $tenant_master->item->comment3 : '' }}</textarea>
                </div>

                @if($type == config('tenancy_schedule.item_type.live') || $type == config('tenancy_schedule.item_type.business'))

                  <div class="form-group col-md-4">
                    <label>Mietbeginn</label>
                    <input type="text" name="rent_begin" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->rent_begin) && $tenant_master->item->rent_begin) ? show_date_format($tenant_master->item->rent_begin, 2) : '' }}">
                  </div>

                  <div class="form-group col-md-4">
                    <label>Mietende</label>
                    <input type="text" name="rent_end" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->rent_end) && $tenant_master->item->rent_end) ? show_date_format($tenant_master->item->rent_end, 2) : '' }}">
                  </div>

                  <div class="form-group col-md-4">
                    <label>Mietfläche/m²</label>
                    <input type="text" name="rental_space" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->rental_space)) ? show_number($tenant_master->item->rental_space, 2) : '' }}">
                  </div>

                  <div class="form-group col-md-4">
                    <label>Miete/m²</label>
                    <?php
                      $actual_net_rent = (isset($tenant_master->item->actual_net_rent) && $tenant_master->item->actual_net_rent) ? $tenant_master->item->actual_net_rent : 0;
                      $rental_space = (isset($tenant_master->item->rental_space) && $tenant_master->item->rental_space) ? $tenant_master->item->rental_space : 0;
                    ?>
                    <input type="text" class="form-control mask-number-input" value="{{ show_number($actual_net_rent/$rental_space, 2) }}" disabled>
                  </div>

                  <div class="form-group col-md-4">
                    <label>IST-Nettokaltmiete p.m.</label>
                    <input type="text" name="actual_net_rent" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->actual_net_rent)) ? show_number($tenant_master->item->actual_net_rent, 2) : '' }}">
                  </div>

                  <div class="form-group col-md-4">
                    <label>IST-Nettokaltmiete p.a.</label>
                    <input type="text" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->actual_net_rent)) ? show_number($tenant_master->item->actual_net_rent*12, 2) : '' }}" disabled>
                  </div>

                  <div class="form-group col-md-4">
                    <label>NK netto p.m.</label>
                    <div class="row">
                        <div class="col-md-8">
                            <input type="text" name="nk_netto" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->nk_netto)) ? show_number($tenant_master->item->nk_netto, 2) : '' }}">
                        </div>
                        <div class="col-md-4">
                          <button type="button" class="btn-sm btn-primary" id="btn-bka">BKA anzeigen</button>
                        </div>
                    </div>
                    
                  </div>

                  <div id="bka-fields" style="display: none;">

                    {{-- BKA 2018 START --}}

                    <div class="form-group col-md-12">
                      <h4>BKA 2018</h4>
                      <hr style="margin-top: 0px;border-color: black;">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Datum BKA</label>
                      <input type="text" name="bka_date[]" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->bka_date_2018) && $tenant_master->item->bka_date_2018) ? show_date_format($tenant_master->item->bka_date_2018, 2) : '' }}" placeholder="DD.MM.JJJJ">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Betrag BKA</label>
                      <input type="text" name="bka_amount[]" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->amount_2018)) ? show_number($tenant_master->item->amount_2018, 2) : '' }}">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Bezahldatum</label>
                      <input type="text" name="bka_payment_date[]" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->payment_date_2018) && $tenant_master->item->payment_date_2018) ? show_date_format($tenant_master->item->payment_date_2018, 2) : '' }}" placeholder="DD.MM.JJJJ">
                    </div>

                    <div class="form-group col-md-4 datei_2018 datei year">
                      <label>Datei</label><br>
                      @if(isset($tenant_master->item->id) && $tenant_master->item->id)

                        <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{ $tenant_master->item->id }}" data-property-id="{{ $tenant_master->property_id }}" data-year="2018" id="datei_2018_{{ $tenant_master->item->id }}">
                          <i class="fa fa-link"></i>
                        </a>
                        @if($tenant_master->item->tenant_payments_2018 && $tenant_master->item->tenant_payments_2018->file_name)
                            <?php

                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($tenant_master->item->tenant_payments_2018->file_basename) ? $tenant_master->item->tenant_payments_2018->file_basename : '');
                              if($tenant_master->item->tenant_payments_2018->file_type == 'file'){
                                $download_path = "https://drive.google.com/file/d/".(isset($tenant_master->item->tenant_payments_2018->file_basename) ? $tenant_master->item->tenant_payments_2018->file_basename : '');

                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($tenant_master->item->tenant_payments_2018->file_basename) ? $tenant_master->item->tenant_payments_2018->file_basename : '').'&file_dir='.(isset($tenant_master->item->tenant_payments_2018->file_dirname) ? $tenant_master->item->tenant_payments_2018->file_dirname : '');
                              }
                            ?>
                            <a href="{{ $download_path }}"  target="_blank" title="{{ $tenant_master->item->tenant_payments_2018->file_name }}"><i class="fa fa-file" ></i></a>

                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $tenant_master->item->tenant_payments_2018->id]) }}"><i class="fa fa-trash"></i></a>

                        @endif
                      @endif
                    </div>

                    {{-- BKA 2018 END --}}

                    {{-- BKA 2019 START --}}

                    <div class="form-group col-md-12">
                      <h4>BKA 2019</h4>
                      <hr style="margin-top: 0px;border-color: black;">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Datum BKA</label>
                      <input type="text" name="bka_date[]" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->bka_date_2019) && $tenant_master->item->bka_date_2019) ? show_date_format($tenant_master->item->bka_date_2019, 2) : '' }}" placeholder="DD.MM.JJJJ">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Betrag BKA</label>
                      <input type="text" name="bka_amount[]" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->amount_2019)) ? show_number($tenant_master->item->amount_2019, 2) : '' }}">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Bezahldatum</label>
                      <input type="text" name="bka_payment_date[]" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->payment_date_2019) && $tenant_master->item->payment_date_2019) ? show_date_format($tenant_master->item->payment_date_2019, 2) : '' }}" placeholder="DD.MM.JJJJ">
                    </div>

                    <div class="form-group col-md-4 datei_2019 datei year">
                      <label>Datei</label><br>
                      @if(isset($tenant_master->item->id) && $tenant_master->item->id)

                        <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{ $tenant_master->item->id }}" data-property-id="{{ $tenant_master->property_id }}" data-year="2019" id="datei_2019_{{ $tenant_master->item->id }}">
                          <i class="fa fa-link"></i>
                        </a>
                        @if($tenant_master->item->tenant_payments_2019 && $tenant_master->item->tenant_payments_2019->file_name)
                            <?php

                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($tenant_master->item->tenant_payments_2019->file_basename) ? $tenant_master->item->tenant_payments_2019->file_basename : '');
                              if($tenant_master->item->tenant_payments_2019->file_type == 'file'){
                                $download_path = "https://drive.google.com/file/d/".(isset($tenant_master->item->tenant_payments_2019->file_basename) ? $tenant_master->item->tenant_payments_2019->file_basename : '');

                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($tenant_master->item->tenant_payments_2019->file_basename) ? $tenant_master->item->tenant_payments_2019->file_basename : '').'&file_dir='.(isset($tenant_master->item->tenant_payments_2019->file_dirname) ? $tenant_master->item->tenant_payments_2019->file_dirname : '');
                              }
                            ?>
                            <a href="{{ $download_path }}"  target="_blank" title="{{ $tenant_master->item->tenant_payments_2019->file_name }}"><i class="fa fa-file" ></i></a>

                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $tenant_master->item->tenant_payments_2019->id]) }}"><i class="fa fa-trash"></i></a>

                        @endif
                      @endif
                    </div>

                    {{-- BKA 2019 END --}}

                    {{-- BKA 2020 START --}}

                    <div class="form-group col-md-12">
                      <h4>BKA 2020</h4>
                      <hr style="margin-top: 0px;border-color: black;">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Datum BKA</label>
                      <input type="text" name="bka_date[]" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->bka_date_2020) && $tenant_master->item->bka_date_2020) ? show_date_format($tenant_master->item->bka_date_2020, 2) : '' }}" placeholder="DD.MM.JJJJ">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Betrag BKA</label>
                      <input type="text" name="bka_amount[]" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->amount_2020)) ? show_number($tenant_master->item->amount_2020, 2) : '' }}">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Bezahldatum</label>
                      <input type="text" name="bka_payment_date[]" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->payment_date_2020) && $tenant_master->item->payment_date_2020) ? show_date_format($tenant_master->item->payment_date_2020, 2) : '' }}" placeholder="DD.MM.JJJJ">
                    </div>

                    <div class="form-group col-md-4 datei_2020 datei year">
                      <label>Datei</label><br>
                      @if(isset($tenant_master->item->id) && $tenant_master->item->id)

                        <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{ $tenant_master->item->id }}" data-property-id="{{ $tenant_master->property_id }}" data-year="2020" id="datei_2020_{{ $tenant_master->item->id }}">
                          <i class="fa fa-link"></i>
                        </a>
                        @if($tenant_master->item->tenant_payments_2020 && $tenant_master->item->tenant_payments_2020->file_name)
                            <?php

                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($tenant_master->item->tenant_payments_2020->file_basename) ? $tenant_master->item->tenant_payments_2020->file_basename : '');
                              if($tenant_master->item->tenant_payments_2020->file_type == 'file'){
                                $download_path = "https://drive.google.com/file/d/".(isset($tenant_master->item->tenant_payments_2020->file_basename) ? $tenant_master->item->tenant_payments_2020->file_basename : '');

                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($tenant_master->item->tenant_payments_2020->file_basename) ? $tenant_master->item->tenant_payments_2020->file_basename : '').'&file_dir='.(isset($tenant_master->item->tenant_payments_2020->file_dirname) ? $tenant_master->item->tenant_payments_2020->file_dirname : '');
                              }
                            ?>
                            <a href="{{ $download_path }}"  target="_blank" title="{{ $tenant_master->item->tenant_payments_2020->file_name }}"><i class="fa fa-file" ></i></a>

                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $tenant_master->item->tenant_payments_2020->id]) }}"><i class="fa fa-trash"></i></a>

                        @endif
                      @endif
                    </div>

                    <div class="form-group col-md-12">
                      <hr style="margin-top: 0px;border-color: black;">
                    </div>

                    {{-- BKA 2020 END --}}

                  </div>

                  <?php
                      $actual_net_rent = (isset($tenant_master->item->actual_net_rent) && $tenant_master->item->actual_net_rent) ? $tenant_master->item->actual_net_rent : 0;
                      $nk_netto = (isset($tenant_master->item->nk_netto) && $tenant_master->item->nk_netto) ? $tenant_master->item->nk_netto : 0;

                      $gesamt_netto = ($actual_net_rent + $nk_netto);
                      $MwSt = ($actual_net_rent + $nk_netto)*19/100;
                      $gesamt_brutto_pm = ($gesamt_netto + $MwSt);
                      $gesamt_brutto_pa = ($gesamt_brutto_pm * 12);
                  ?>

                  <div class="form-group col-md-4">
                    <label>Gesamt Netto p.m.</label>
                    <input type="text" class="form-control mask-number-input" value="{{ show_number($gesamt_netto, 2) }}" disabled>
                  </div>

                  @if($type == config('tenancy_schedule.item_type.business'))

                    <div class="form-group col-md-4">
                      <label>19% MwSt.</label>
                      <input type="text" class="form-control mask-number-input" value="{{ show_number($MwSt, 2) }}" disabled>
                    </div>

                  @endif

                  <div class="form-group col-md-4">
                    <label>Gesamt Brutto p.m.</label>
                    <input type="text" class="form-control mask-number-input" value="{{ show_number($gesamt_brutto_pm, 2) }}" disabled>
                  </div>

                  <div class="form-group col-md-4">
                    <label>Gesamt Brutto p.a.</label>
                    <input type="text" class="form-control mask-number-input" value="{{ show_number($gesamt_brutto_pa, 2) }}" disabled>
                  </div>

                  @if($type == config('tenancy_schedule.item_type.business'))

                    <?php
                      $restlaufzeit = $restlaufzeit_in_eur = 0;
                      if(isset($tenant_master->item->rent_end) && $tenant_master->item->rent_end && substr($tenant_master->item->rent_end,0,4) != "2099"){
                          $restlaufzeit = (isset($tenant_master->item->date_diff_average) && $tenant_master->item->date_diff_average) ? $tenant_master->item->date_diff_average : 0;
                          $restlaufzeit_in_eur = $tenant_master->item->remaining_time_in_eur;
                      }
                    ?>

                    <div class="form-group col-md-4">
                      <label>Restlaufzeit</label>
                      <input type="text" class="form-control mask-number-input" value="{{ show_number($restlaufzeit, 2) }}" disabled>
                    </div>

                    <div class="form-group col-md-4">
                      <label>Restlaufzeit in EUR</label>
                      <input type="text" class="form-control mask-number-input" value="{{ show_number($restlaufzeit_in_eur, 2) }}" disabled>
                    </div>

                  @endif

                @endif

                <div class="form-group col-md-4">
                  <label>Mietvertrag</label>
                  <br>
                  @if(isset($tenant_master->item->id) && $tenant_master->item->id)

                    <a href="javascript:void(0);" class="link-button-pdf-mieterliste" data-id="{{$tenant_master->item->id}}">
                      <i class="fa fa-link"></i>
                    </a>

                    @if($tenant_master->item->file_extension && $tenant_master->item->file_href)
                      &nbsp;&nbsp;<a href="{{ $tenant_master->item->file_href }}"  id="mieterliste-gdrive-link-{{$tenant_master->item->id}}"  target="_blank" title="{{ $tenant_master->item->file_name }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $tenant_master->item->file_extension) ?: 'fa-file' }}" ></i></a>
                    @endif

                  @endif
                </div>

                <div class="form-group col-md-4">
                  <label>Domos NR</label>
                  <input type="text" name="opos_mieter" class="form-control" value="{{ (isset($tenant_master->item->opos_mieter)) ? $tenant_master->item->opos_mieter : '' }}">
                </div>

                @if(in_array($type, [config('tenancy_schedule.item_type.live'), config('tenancy_schedule.item_type.business'), config('tenancy_schedule.item_type.live_vacancy')]))

                  <div class="form-group col-md-4" style="padding-top: 2%;">
                    <input type="checkbox" name="is_new" value="1" id="is_new" class="checkbox-is-new" data-id="{{ (isset($tenant_master->item->id)) ? $tenant_master->item->id : '' }}" {{ (isset($tenant_master->item->is_new) && $tenant_master->item->is_new) ? 'checked' : '' }} >&nbsp;&nbsp;
                    <label for="is_new">Neu</label>
                  </div>

                  <div class="form-group col-md-4">
                    <label>Asset Manager</label>
                    <div class="row">
                        <div class="col-md-6">
                            <select name="asset_manager_id" class="form-control">
                              <option value=""> </option>
                              @if($as_users)
                                @foreach ($as_users as $usr)
                                  <option value="{{ $usr->id }}" {{ (isset($tenant_master->item->asset_manager_id) && $tenant_master->item->asset_manager_id == $usr->id) ? 'selected' : '' }} >{{$usr->getshortname()}}</option>
                                @endforeach
                              @endif
                            </select>
                        </div>
                        <div class="col-md-6">
                            <select name="asset_manager_id2" class="form-control">
                              <option value=""> </option>
                              @if($as_users)
                                @foreach ($as_users as $usr)
                                  <option value="{{ $usr->id }}" {{ (isset($tenant_master->item->asset_manager_id2) && $tenant_master->item->asset_manager_id2 == $usr->id) ? 'selected' : '' }} >{{$usr->getshortname()}}</option>
                                @endforeach
                              @endif
                            </select>
                        </div>
                    </div>
                  </div>

                  <div class="form-group col-md-4">
                    <label>Abschluss MV</label>
                    <input type="text" name="assesment_date" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->assesment_date) && $tenant_master->item->assesment_date) ? show_date_format($tenant_master->item->assesment_date, 2) : '' }}" placeholder="DD.MM.JJJJ">
                  </div>

                  @if($type == config('tenancy_schedule.item_type.live') || $type == config('tenancy_schedule.item_type.business'))

                    <div class="form-group col-md-4">
                      <label>Sonderkündigunsrecht</label>
                      <input type="text" name="termination_date" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->termination_date) && $tenant_master->item->termination_date) ? show_date_format($tenant_master->item->termination_date, 2) : '' }}" placeholder="DD.MM.JJJJ">
                    </div>

                    <div class="form-group col-md-4">
                      <label>Optionen</label>
                      <div class="row">
                        <div class="col-md-2" style="padding-top: 10px;">
                            <input class="form-check-input" type="radio" name="selected_option" value="0" {{ (isset($tenant_master->item->selected_option) && $tenant_master->item->selected_option == 0) ? 'checked' : '' }} >
                            <input class="form-check-input" type="radio" name="selected_option" value="1" {{ (isset($tenant_master->item->selected_option) && $tenant_master->item->selected_option == 1) ? 'checked' : '' }}>
                            <input class="form-check-input" type="radio" name="selected_option" value="2" {{ (isset($tenant_master->item->selected_option) && $tenant_master->item->selected_option == 2) ? 'checked' : '' }}>
                        </div>
                        <div class="col-md-10">
                          <input type="text" name="options" class="form-control" value="{{ (isset($tenant_master->item->options)) ? $tenant_master->item->options : '' }}">
                        </div>
                      </div>
                    </div>

                  @endif

                  <div class="form-group col-md-4">
                    <label>Nutzung</label>
                    <input type="text" name="use" class="form-control" value="{{ (isset($tenant_master->item->use)) ? $tenant_master->item->use : '' }}">
                  </div>

                  @if($type == config('tenancy_schedule.item_type.live') || $type == config('tenancy_schedule.item_type.business'))

                    <div class="form-group col-md-4">
                        <label>Kategorie</label>
                        <select name="category" class="form-control">
                          @foreach($category_list as $list)
                            <option value="{{$list}}" {{ (isset($tenant_master->item->category) && $tenant_master->item->category == $list) ? 'selected' : '' }} >{{$list}}</option>
                          @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                      <label>1. Mahnung</label>
                      <div class="row">
                        <div class="col-md-10">
                            <input type="text" name="warning_date1" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->warning_date1) && $tenant_master->item->warning_date1) ? show_date_format($tenant_master->item->warning_date1, 2) : '' }}" placeholder="DD.MM.JJJJ">
                        </div>
                        <div class="col-md-2" style="padding-top: 1%;">
                            <input type="checkbox" name="warning_price1" value="1" {{ (isset($tenant_master->item->warning_price1) && $tenant_master->item->warning_price1 == 1) ? 'checked' : '' }}>
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-md-4">
                      <label>2. Mahnung</label>
                      <div class="row">
                        <div class="col-md-10">
                            <input type="text" name="warning_date2" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->warning_date2) && $tenant_master->item->warning_date2) ? show_date_format($tenant_master->item->warning_date2, 2) : '' }}" placeholder="DD.MM.JJJJ">
                        </div>
                        <div class="col-md-2" style="padding-top: 1%;">
                            <input type="checkbox" name="warning_price2" value="1" {{ (isset($tenant_master->item->warning_price2) && $tenant_master->item->warning_price2 == 1) ? 'checked' : '' }}>
                        </div>
                      </div>
                    </div>

                    <div class="form-group col-md-4">
                      <label>Mahnbescheid</label>
                      <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="warning_date3" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->warning_date3) && $tenant_master->item->warning_date3) ? show_date_format($tenant_master->item->warning_date3, 2) : '' }}" placeholder="DD.MM.JJJJ">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="warning_price3" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->warning_price3)) ? show_number($tenant_master->item->warning_price3, 2) : '' }}">
                        </div>
                      </div>
                    </div>

                  @endif

                  <div class="form-group col-md-4">
                    <label>Indexierung</label>
                    <input type="text" name="indexierung" class="form-control" value="{{ (isset($tenant_master->item->indexierung)) ? $tenant_master->item->indexierung : '' }}">
                  </div>

                  @if($type == config('tenancy_schedule.item_type.live') || $type == config('tenancy_schedule.item_type.business'))

                    <div class="form-group col-md-4">
                      <label>Kaution</label>
                      <input type="text" name="kaution" class="form-control" value="{{ (isset($tenant_master->item->kaution)) ? $tenant_master->item->kaution : '' }}">
                    </div>

                  @endif

                @endif

                @if(in_array($type, [config('tenancy_schedule.item_type.live_vacancy'), config('tenancy_schedule.item_type.business_vacancy'), config('tenancy_schedule.item_type.free_space')]))

                  <div class="form-group col-md-4">
                      <label>Leerstand in qm</label>
                      <input type="text" name="vacancy_in_qm" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->vacancy_in_qm)) ? show_number($tenant_master->item->vacancy_in_qm, 2) : '' }}">
                  </div>

                  <div class="form-group col-md-4">
                      <label>Leerstand EUR</label>
                      <input type="text" name="vacancy_in_eur" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->vacancy_in_eur)) ? show_number($tenant_master->item->vacancy_in_eur, 2) : '' }}" readonly>
                  </div>

                  <div class="form-group col-md-4">
                      <label>Leerstand bewertet AM</label>
                      <input type="text" name="actual_net_rent2" class="form-control mask-number-input" value="{{ (isset($tenant_master->item->actual_net_rent2)) ? show_number($tenant_master->item->actual_net_rent2, 2) : '' }}">
                  </div>

                  <div class="form-group col-md-4">
                    <label>Leerstand seit</label>
                    <input type="text" name="vacant_since" class="form-control mask-input-new-date" value="{{ (isset($tenant_master->item->vacant_since) && $tenant_master->item->vacant_since) ? show_date_format($tenant_master->item->vacant_since, 2) : '' }}" placeholder="DD.MM.JJJJ">
                  </div>

                  <div class="form-group col-md-4" style="padding-top: 2%;">
                    <input type="checkbox" name="vacancy_on_purcahse" id="vacancy_on_purcahse" value="1" {{ (isset($tenant_master->item->vacancy_on_purcahse) && $tenant_master->item->vacancy_on_purcahse == 1) ? 'checked' : '' }}>
                    <label for="vacancy_on_purcahse">Leerstand bei Ankauf</label>
                  </div>

                @endif

              </div>

              <hr>

              <div class="row">
                <div class="col-md-12 text-center">
                  <button type="submit" class="btn btn-primary">Speichern</button>
                </div>
              </div>

            </form>

          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="select-gdrive-file-model" role="dialog" style="z-index: 9999;">
     <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
           <div class="modal-header">
              <input type='hidden' name='workingDir' id='workingDir'>
              <input type='hidden' name='select-gdrive-file-type' id='select-gdrive-file-type'>
              <input type='hidden' name='select-gdrive-file-data' id='select-gdrive-file-data'>
              <input type='hidden' name='select-gdrive-file-callback' id='select-gdrive-file-callback'>
              <a href="javascript:;" class="navbar-brand clickable hide" id="gdrive-file-previous-button">
              <i class="fa fa-arrow-left"></i>
              <span class="hidden-xs">{{ trans('lfm.nav-back') }}</span>
              </a>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
           </div>
           <div class="modal-body">
              <h4>Select any file to link. </h4>
              <div id="select-gdrive-file-content"></div>
           </div>
        </div>
        <!-- Modal content-->
     </div>
  </div>

@endsection

@section('js')
  
  <script src="{{ asset('file-manager/js/bootbox.min.js') }}"></script>
  <script src="{{ asset('file-manager/js/script.js') }}"></script>

  <script type="text/javascript">

      var _token        = '{{ csrf_token() }}';
      var workingDir    = "{{$workingDir}}";

      var route_prefix                  = '{{ url("file-manager") }}';
      var lfm_route                     = '{{ url("file-manager") }}';
      var url_select_mieterliste_file   = '{{ route('select_mieterliste_file', ['property_id' => $tenant_master->property_id]) }}';
      var url_saveMieterlisteFile       = '{{ route('saveMieterlisteFile') }}';

      $(document).on("focus", ".mask-number-input", function() {
        $(this).mask('#.##0,00', {
            reverse: true
        });
      });

      $(document).on("focus", ".mask-input-new-date", function() {
          $(this).mask('00.00.0000');
      });

      $('body').on('click', '#btn-bka', function(){
        $('#bka-fields').toggle();
      });

      /*-------------------------------------------------------------------------*/
        var current_ele;
        $('.link-button-pdf-mieterliste').on('click', function(e){
            current_ele = $(this);
            var id = $(this).attr('data-id');
            $("#select-gdrive-file-model").modal('show');
            $("#select-gdrive-file-type").val('mieterliste');
            $("#select-gdrive-file-callback").val('selectMieterlisteSubmit');
            $("#select-gdrive-file-data").val(id);
            selectGdriveFileLoad(workingDir);
        });

        window.selectMieterlisteSubmit = function(basename, dirname,curElement, dirOrFile){
          var id = $("#select-gdrive-file-data").val();

          var formdata = new FormData();
          formdata.append('_token', _token);
          formdata.append('id', id);
          formdata.append('basename', basename);
          formdata.append('dirname', dirname);
          formdata.append('type', dirOrFile);
          $('.preloader').show();

          $.ajax({
            type: 'POST',
            url: url_select_mieterliste_file,
            contentType: false,
            processData: false,
            data:formdata,
          }).done(function (data) {
            if(data.success){
                $('#select-gdrive-file-model').modal('hide');
                $(current_ele).closest('div').html(data.html);
            }else{
              alert(data.msg);
            }
            $('.preloader').hide();
          });
        }

      /*-------------------------------------------------------------------------*/

      /*--------------------------------BKA Upload----------------------------*/
          var link_button_datei;

          $('body').on('click','.link-button-datei',function(e) {
              link_button_datei = $(this);
              var property_id = $(this).attr('data-property-id');
              var item_id     = $(this).attr('data-item-id');
              var year        = $(this).attr('data-year');
              var obj         = $(this).attr('id');

              var data = {
                  property_id: property_id,
                  item_id: item_id,
                  year: year,
                  obj: obj
              };
              $("#select-gdrive-file-model").modal('show');
              $("#select-gdrive-file-type").val('Betriebskostenabrechnung');
              $("#select-gdrive-file-callback").val('dateiSubmit');
              $("#select-gdrive-file-data").val(JSON.stringify(data));
              selectGdriveFileLoad(workingDir);
          });

          window.dateiSubmit = function(basename, dirname, curElement, dirOrFile) {
              var dataObj = $("#select-gdrive-file-data").val();
              dataObj = JSON.parse(dataObj);
              var formdata = new FormData();
              formdata.append('_token', _token);
              formdata.append('property_id', dataObj.property_id);
              formdata.append('item_id', dataObj.item_id);
              formdata.append('year', dataObj.year);
              formdata.append('basename', basename);
              formdata.append('dirname', dirname);
              formdata.append('dirtype', 'Betriebskostenabrechnung');
              formdata.append('dirOrFile', dirOrFile);
              $('.preloader').show();
              $.ajax({
                  type: 'POST',
                  url: url_saveMieterlisteFile,
                  contentType: false,
                  processData: false,
                  data: formdata,
              }).done(function(data) {
                  if (data.success) {
                      $(link_button_datei).closest('.datei').append(data.html);
                  } else {
                      alert(data.msg);
                  }
                  $('.preloader').hide();
              });
          }
    </script>
@endsection
