<?php



$form_id = 'immoscout_form';
$externalId = '';
$title = '';
$street = $properties['strasse'];
$houseNumber = $properties['hausnummer'];
$postcode = $properties['plz_ort'];
$city = $properties['ort'];
$showAddress = 'true';
$apartmentType = $properties['type_of_property'];
$lift = '';
$cellar = '';
$handicappedAccessible = '';
$condition = '';

/*
 * price variables
 * */
$value = '';
$currency = 'EUR';
$marketingType = 'RENT';
$priceIntervalType = '';


$livingSpace = '';
$numberOfRooms = '';
$builtInKitchen = '';
$balcony = '';
$garden = '';
$hasCourtage = 'NO';
$courtage = '';


$property_id =  Request::segment(2);
$realsate = DB::table('property_images')->where('property_id', $property_id)->orderBy('id', 'desc')->limit(1)->first();
if(!is_null($realsate)){

    $form_id = 'immoscout_form_edit';
    $realsate_id = $realsate->realstate_id;

    $url =  "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/realestate/".$realsate_id;


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/realestate/".$realsate_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "authorization: OAuth oauth_consumer_key=\"08211746Key\",oauth_token=\"4aca0424-da15-42d7-bde6-87353d9d82d7\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"1555299289\",oauth_nonce=\"e6x6VQ\",oauth_version=\"1.0\",oauth_signature=\"dBbQMwhZgBFRmSi8TYwADHxyags%3D\"",
            "cache-control: no-cache",
            "postman-token: b13e992f-eb58-27c6-c1e8-dc518686f040"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
        echo "cURL Error #:" . $err;
    } else {



        if($xml = new \SimpleXMLElement($response)){
            $xml = new \SimpleXMLElement($response);
            $response = json_encode($xml, JSON_PRETTY_PRINT);
            $response = json_decode($response, TRUE);


            if(isset($response['externalId']) ){

                $externalId = $response['externalId'];
                $title = $response['title'];
                $street = $response['address']['street'];
                $houseNumber = $response['address']['houseNumber'];
                $postcode = $response['address']['postcode'];
                $city = $response['address']['city'];
                $showAddress = $response['showAddress'];
                $value = $response['price']['value'];
                $currency = $response['price']['currency'];
                $marketingType = $response['price']['marketingType'];
                $priceIntervalType = $response['price']['priceIntervalType'];
                $livingSpace = $response['livingSpace'];
                $numberOfRooms = $response['numberOfRooms'];
                $hasCourtage = $response['courtage']['hasCourtage'];
                $courtage = $response['courtage']['courtage'];
            }
//            echo '<pre>';
//            print_r($response);
//            echo '</pre>';

        }
    }

}

?>

@php 

    $immo_upload =  DB::table('immo_upload')->where('property_id', $item->id)->first();

    if(isset($immo_upload)){
 
          $category =                 $immo_upload->category;
          $property_type =            $immo_upload->property_type;
          $postcode =                 $immo_upload->postcode;
          $address =                  $immo_upload->address;
          $house_no =                 $immo_upload->house_no;
          $city =                     $immo_upload->city;
          $show_address =             $immo_upload->show_address;
          $title =                    $immo_upload->title;
          $objektnummer =             $immo_upload->objektnummer;
          $gruppennummer =            $immo_upload->gruppennummer;
          $marketing_method  =        $immo_upload->marketing_method;
          $property =                 $immo_upload->property;
          $totalarea =                $immo_upload->totalarea;
          $retail_space =             $immo_upload->retail_space;
          $rental_price =             $immo_upload->rental_price;
          $available_from =           $immo_upload->available_from;
          $lageart =                  $immo_upload->lageart;
          $nebenflache =              $immo_upload->nebenflache;
          $sales_area =               $immo_upload->sales_area;
          $objektzustand =            $immo_upload->objektzustand;
          $letzte_odernisierung =     $immo_upload->letzte_odernisierung;
          $denkmalschutzobjekt =      $immo_upload->denkmalschutzobjekt;
          $quality_of_facilities =    $immo_upload->quality_of_facilities;
          $floors =                   $immo_upload->floors;
          $number_parking_area =      $immo_upload->number_parking_area;
          $price_parking_area =       $immo_upload->price_parking_area;
          $elevator =                 $immo_upload->elevator;
          $lift =                     $immo_upload->lift;   
          $ramp =                     $immo_upload->ramp;
          $cellar =                   $immo_upload->cellar;
          $delivery =                 $immo_upload->delivery;
          $floor_load =               $immo_upload->floor_load;
          $energy_certificate =       $immo_upload->energy_certificate;
          $certificate_date =         $immo_upload->certificate_date;
          $heating =                  $immo_upload->heating;
          $deposit =                  $immo_upload->deposit;
          $description =              $immo_upload->description;
          $equipments =               $immo_upload->equipments;
          $location =                 $immo_upload->location;
          $miscellaneous =            $immo_upload->miscellaneous;
          $contact_sex =              $immo_upload->contact_sex;
          $contact_surname =          $immo_upload->contact_surname;
          $contact_country =          $immo_upload->contact_country;
          $contact_mail =             $immo_upload->contact_mail;
          $images_pro =               $immo_upload->images;
          $floor_plan =               $immo_upload->floor_plan;

    }

@endphp

<style>
  .new-height{
  height: 100px !important;
  }
</style>

<div id="response"></div>

<form class="vermi-form" enctype="multipart/form-data">
 
     <!-- Modal content-->
      
        <div id="json_resp_immoc">
          
        </div>
        <div class="modal-content">
            <div class="modal-body">


                  <?php
                  $details = DB::table('vermietung_items')->where('item_id', $item->id)->get();

                  $title = $description = $size = $price = $category= "";
                  $iarr = $iarr1 = $pdfs = $planpdf = array();
                  $imageid = $pdfid = $planpdfid =  0;

                  $size = $item->vacancy_in_qm;
                  $price = $item->vacancy_in_eur;
                  $category = $item->use;

                  foreach ($details as $key => $value) {
                    if($value->name=="title")
                      $title = $value->value;
                    if($value->name=="description")
                      $description = $value->value;
                    if($value->name=="size")
                      $size = $value->value;
                    if($value->name=="price")
                      $price = $value->value;
                    if($value->name=="category")
                      $category = $value->value;
                    if($value->name=="image"){
                      $iarr[$value->id] = $value->value;
                      $iarr1[$value->id] = $value->is_favourite;
                    }
                    if($value->name=="pdf"){
                      $pdfs = $value->value;
                      $pdfid = $value->id;

                      $item->vacant_uploaded_pdf_date = $value->created_at;
                    }
                    if($value->name=="planpdf"){
                      $planpdf = $value->value;
                      $planpdfid = $value->id;
                    }
                  }



                  $item->vacant_uploaded_pdf = $pdfs;


                  ?>
                  <!-- {{ini_get('post_max_size')}} -->

                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" class="vitem_id" name="item_id" value="{{ $item->id }}" />
                    <input type="hidden" name="property_id" value="{{ $property_id }}" />

                    <label>Title</label>
                    <input type="text" value="{{$title}}" name="title" class="form-control" placeholder="" >
                    <br>

                    <label>Beschreibung</label>
                    <textarea class="form-control" name="description">{{$description}}</textarea>
                    <br>
                    
                     <label>Kategorie</label>
                    <select name="category[]" class="form-control multi-category" multiple="multiple" required>

                      <?php
                      $categories = array('Einzelhandel','Büro/Praxen','Wohnungen','Gastronomie','Lager','Stellplätze','Sonstiges');
                      $cat_array = [];
                      if($category)
                      {
                        $cat_array = explode(',', $category);
                      }




                      ?>


                      @foreach($categories as $cat)
                      <option value="{{$cat}}" @if($cat_array && in_array($cat,$cat_array)) selected @endif >{{$cat}}</option>
                      @endforeach
                    

                    </select><br>

                                        
                    
                    

                    <label>Verkaufsfläche</label>
                    <input type="retail_space" value="{{ $size  }}" name="size" class="form-control" placeholder="Verkaufsfläche">

                    <label>Preis pro m²</label>
                    <input type="rental_price" value="{{ $price  }}" name="price" class="form-control" placeholder="Mietpreis" required>


                    <label>Bilder</label>
                    <input type="file" name="image" class="vacant-image">

                    <div class="img-preview-div im-{{ $item->id }}"">
                    @foreach($iarr as $imageid=>$images)
                    <div class="creating-ads-img-wrap media-common-class">
                        <img src="{{ asset('ad_files_upload/'.$images) }}" class="img-responsive" />
                        <div class="img-action-wrap" id="{{ $imageid }}">
                            <a href="javascript:;" class="deletevermitund"><i class="fa fa-trash-o"></i> </a>
                            @if($iarr1[$imageid])
                            <a href="javascript:;" class="setasfavourite" data-id="{{ $imageid }}"><i class="fa fa-star"></i></a>
                            @else
                            <a href="javascript:;" class="setasfavourite" data-id="{{ $imageid }}"><i class="fa fa-star-o"></i></a>
                            @endif
                        </div>
                    </div>
                    @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <br>
                    <label>Leerstandsflächenexposé</label>
                    <input type="file" name="pdf" class="vacant-pdf">

                    <div class="pdf-preview-div pd-{{ $item->id }}">
                    @if($pdfs)
                    <div class="creating-ads-img-wrap media-common-class">
                        <a href="{{ asset('ad_files_upload/'.$pdfs) }}" class="img-responsive" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
                        <div class="img-action-wrap" id="{{ $pdfid }}">
                            <a href="javascript:;" class="deletevermitund"><i class="fa fa-trash-o"></i> </a>
                        </div>
                    </div>
                    @endif
                  </div>

                  <div class="clearfix"></div>

                  <br>

                  <label>Grundriss</label>
                    <input type="file" name="planpdf" class="vacant-pdf">

                    <div class="pdf-preview-div planpd-{{ $item->id }}">
                    @if($planpdf)
                    <div class="creating-ads-img-wrap media-common-class">
                        <a href="{{ asset('ad_files_upload/'.$planpdf) }}" class="img-responsive" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
                        <div class="img-action-wrap" id="{{ $planpdfid }}">
                            <a href="javascript:;" class="deletevermitund"><i class="fa fa-trash-o"></i> </a>
                        </div>
                    </div>
                    @endif
                  </div>
                    

                    
            </div>
 

            <div class="modal-footer" style="clear: both;">
                <button type="submit" class="btn btn-primary save-upload-vermietung" >Upload<i class="fa fa-circle-o-notch fa-spin immo_loader hide" style="font-size:14px"></i></button>
                <button type="submit" class="btn btn-primary save-vermietung" >Speichern <i class="fa fa-circle-o-notch fa-spin immo_loader hide" style="font-size:14px"></i></button>
            </div>

            </form>

            <!-- <button type="button" class="btn btn-primary vermi-upload">Veröffentlichen</button> -->
        </div>
 