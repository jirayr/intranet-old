 <div class="property-details white-box table-responsive">


			<table class="property-table" id="property-table">
				<thead>
					<tr class="border">
						<th class="bg-brown border-right" colspan="2">Kalkulations- und Planungsspiegel</th>
						<th class="color-red border-no-right">Objekt</th>
						<th colspan="2" class="border-no-right"><a href="#" class="inline-edit" data-type="text" data-pk="name_of_property" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.name_of_property')}}">{{ $properties->name_of_property }}</a></th>
						<th class="color-red border-no-right">{{__('property.date_of_last_update')}}</th>
						<th class="text-right border-no-right">{{date('d/m/Y', strtotime($properties->updated_at))}}</th>
						<th class="color-red border-no-right">{{__('property.date_of_creation')}}</th>
						<th class="text-right border-no-right">{{date('d/m/Y', strtotime($properties->created_at))}}</th>
						<th class="color-red border-no-right">C{{__('property.creator_username')}}</th>
						<th class="border">{{$properties->user_name}}</th>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td> 
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<td colspan="3"></td>
						<th class="border-no-right bg-gray text-center">Jahr 1</th>
						<th class="border-no-right bg-gray text-center">Jahr 2</th>
						<th class="border-no-right bg-gray text-center">Jahr 3</th>
						<th class="border-no-right bg-gray text-center">Jahr 4</th>
						<th class="border bg-gray">Jahr 5</th>
						<td></td>
						<th colspan="2" class="bg-yellow">Gelbe Felder = Eingabefelder</th>

						<td>&nbsp;</td>
					
					    <th colspan="2" class="border text-center">Bemerkungen</th>
						<th colspan="4" class="border text-center">Notizen</th>
						
						 
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
							<td colspan="2" class="border">
 
<a href="#" class="inline-edit" data-type="textarea" data-name="Ankermieter_comment" data-pk="Ankermieter_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >
	@if(isset($comments_new) && count($comments_new)>0) {{ $comments_new[0]['Ankermieter_comment'] }} @endif
</a>
 

						</td>
						<td colspan="4" rowspan="10" class="border">
<a href="#" class="inline-edit" data-type="textarea" data-name="Ankermieter_note" data-pk="Ankermieter_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Ankermieter_note'] }}@endif</a>
						</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<th class="border-no-right bg-gray">Netto Miete (IST) p.a.</th>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent')}} (%)">{{number_format($properties->net_rent*100,2,",",".")}}</a><span>%</span></td>
						<th class="border-no-right bg-gray">Steigerung p.a.</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year1,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year2,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year3,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year4,0,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($properties->net_rent_increase_year5,0,",",".")}}</td>
						<td></td>
						<td>Ankermieter</td>
						<td class="text-right">
							<a href="#" class="inline-edit" data-type="text" data-pk="ankermieter4" data-url="{{url('property/update/'.$id) }}" data-title="Ankermieter">{{ $properties->ankermieter4 }}</a>
						</td>
						<td>&nbsp;</td>
						 
							<td colspan="2" class="border">
						
						<a href="#" class="inline-edit" data-type="textarea" data-name="Mieter_comment" data-pk="Mieter_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mieter_comment'] }}@endif</a>
					
					</td>
					{{-- 	<td colspan="4" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Mieter_note" data-pk="Mieter_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mieter_note'] }}@endif
						</a>
						</td> --}}
						<td>&nbsp;</td>
						{{--<td>{{number_format($properties->anchor_tenants,2)}}</td>--}}
					</tr>

					<tr>
						<td class="border-no-right">Netto Miete (Soll) Leerst.</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_empty" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_empty')}}(%)">{{number_format($properties->net_rent_empty*100,2,",",".")}}</a>%</td>
						{{-- <td class="border-no-right bg-yellow text-right"><a href="#" data-toggle="modal" data-target="#myModal" > Mietflächen</a></td>--}}
						<td class="border-no-right">Steigerung p.a.</td>
						<td class="border-no-right text-right">0</td>
						<td class="border-no-right text-right">0</td>
						<td class="border-no-right text-right">0</td>
						<td class="border-no-right text-right">0</td>
						<td class="border text-right">0</td>
						<td></td>
						<td>Mieter</td>
						<td class="text-right">
							<a href="#" class="inline-edit" data-type="text" data-pk="mieter" data-url="{{url('property/update/'.$id) }}" data-title="Mieter">{{ $properties->mieter }}</a>
						</td>
						<td>&nbsp;</td>
								<td colspan="2" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Mietflache_comment" data-pk="Mietflache_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mietflache_comment'] }}@endif</a>
						</td>
					{{-- 	<td colspan="4" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Mietflache_note" data-pk="Mietflache_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mietflache_note'] }}@endif</a>
						</td> --}}
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
							<td colspan="2" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Grundstuck_comment" data-pk="Grundstuck_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Grundstuck_comment'] }}@endif</a>

						</td>
					{{-- 	<td colspan="4" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Grundstuck_note" data-pk="Grundstuck_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Grundstuck_note'] }}@endif</a>
						</td> --}}
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<td class="border-no-right">Instandhaltung nichtumlfähig</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.maintenance')}} (%)">{{number_format($properties->maintenance*100,2,",",".")}}</a>%</td>
						<td class="border-no-right">Steigerung p.a.</td>
						<td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year1,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year2,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year3,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year4,0,",",".")}}</td>
						<td class="border text-right">{{number_format($properties->maintenance_increase_year5,0,",",".")}}</td>
						<td></td>
						<td>Mietfläche in m²</td>
						<td class="text-right">
						{{number_format($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg, 2,",",".")}}
						</td>
						<td>&nbsp;</td>
							<td colspan="2" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Baujahr_comment" data-pk="Baujahr_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Baujahr_comment'] }}@endif</a>

						</td>
						{{-- <td colspan="4" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Baujahr_note" data-pk="Baujahr_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Baujahr_note'] }}@endif</a>
						</td> --}}


					</tr>

					<tr>
						<td class="border-no-right">Betriebsk. nicht umlfähig</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="operating_costs" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.operating_costs')}} (%)">{{number_format($properties->operating_costs*100,2,",",".")}}</a>%</td>
						<td class="border-no-right">Steigerung p.a.</td>
						<td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year1,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year2,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year3,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year4,0,",",".")}}</td>
						<td class="border text-right">{{number_format($properties->operating_cost_increase_year5,0,",",".")}}</td>
						<td></td>
						<td>Grundstück in m²</td>
						<td class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="plot_of_land_m2" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.plot_of_land_m2')}}">{{number_format($properties->plot_of_land_m2,1,",",".")}}</a></td>
						<td>&nbsp;</td>
					 
						<td colspan="2" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Miete_comment" data-pk="Miete_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Miete_comment'] }}@endif</a>

						</td>
						{{-- <td colspan="4" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Miete_note" data-pk="Miete_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Miete_note'] }}@endif</a>
						</td> --}}
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td class="border-no-right">Objektverwalt. nichtumlfähig</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="object_management" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.object_management')}} (%)">{{number_format($properties->object_management*100,2,",",".")}}</a>%</td>
						<td class="border-no-right">Steigerung p.a.</td>
						<td class="border-no-right text-right">{{number_format($properties->property_management_increase_year1,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->property_management_increase_year2,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->property_management_increase_year3,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->property_management_increase_year4,0,",",".")}}</td>
						<td class="border text-right">{{number_format($properties->property_management_increase_year5,0,",",".")}}</td>
						<td></td>
						<td>Baujahr</td>
						<td class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="construction_year" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.construction_year')}}">{{$properties->construction_year}}</a></td>
						<td>&nbsp;</td>
							<td colspan="2" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="KP_comment" data-pk="KP_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['KP_comment'] }}@endif</a>

						</td>
						{{-- <td colspan="4" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="KP_note" data-pk="KP_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['KP_note'] }}@endif</a>
						</td> --}}
					
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="2" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Stellplatze_comment" data-pk="Stellplatze_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Stellplatze_comment'] }}@endif</a>
						</td>
						{{-- <td colspan="4" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Stellplatze_note" data-pk="Stellplatze_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Stellplatze_note'] }}@endif</a>
						</td> --}}
						<td>&nbsp;</td></tr>

					<tr>
						<th colspan="3" class="border-no-right bg-gray">EBITDA</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_1,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_2,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_3,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_4,0,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($properties->ebitda_year_5,0,",",".")}}</th>
						<td></td>
						<td>Miete (€/m²)</td>
						<td class="text-right">							
							{{number_format($L14,2,",",".")}}&nbsp;€
						</td>
						<td>&nbsp;</td>
						
							<td colspan="2" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Vermietungsstand_comment" data-pk="Vermietungsstand_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Vermietungsstand_comment'] }}@endif</a>
						</td>
						{{-- <td colspan="4" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Vermietungsstand_note" data-pk="Vermietungsstand_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Vermietungsstand_note'] }}@endif</a>
						</td> --}}
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						 
						<td colspan="2" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Maklerpreis_comment" data-pk="Maklerpreis_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Maklerpreis_comment'] }}@endif</a>
						</td>
						{{-- <td colspan="4" class="border">
							<a href="#" class="inline-edit" data-type="textarea" data-name="Maklerpreis_note" data-pk="Maklerpreis_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Maklerpreis_note'] }}@endif</a>
						</td> --}}
						<td>&nbsp;</td></tr>

					<tr>
						<td colspan="3" class="border-no-right">Abschreibung</td>
						<td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,0,",",".")}}</td>
						<td class="border text-right">{{number_format($properties->depreciation_nk_money,0,",",".")}}</td>
						<td></td>
						<td>KP (€/m²) p. NF</td>
						<td class="text-right">{{number_format($L16,2,",",".")}}&nbsp;€</td>
						<td>&nbsp;</td>
					
						
						<td>&nbsp;</td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<th colspan="3" class="border-no-right bg-gray">EBIT</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_1,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_2,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_3,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_4,0,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($properties->ebit_year_5,0,",",".")}}</th>
						<td></td>
						<td>Stellplätze</td>
						<td class="text-right">

							<a href="#" class="inline-edit" data-type="text" data-pk="plot" data-url="{{url('property/update/'.$id) }}" data-title="Stellplätze">{{number_format($properties->plots,0,",",".")}}</a>
						</td>
						<td>&nbsp;</td>
				
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<td colspan="3" class="border-no-right">Zins Bankkredit</td>
						<td class="border-no-right text-right">{{number_format($H48,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($I48,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($J48,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($K48,0,",",".")}}</td>
						<td class="border text-right">{{number_format($L48,0,",",".")}}</td>
						<td></td>
						<td>Vermietungsstand</td>
						<td class="text-right">
							{{number_format($L20 * 100, 1,",",".")}}%
						</td>
						<td>&nbsp;</td>
					
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<th colspan="3" class="border-no-right">Zins Anleihe</th>
						<td class="border-no-right text-right">{{number_format($H52,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($H52,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($H52,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($H52,0,",",".")}}</td>
						<td class="border text-right">{{number_format($H52,2,",",".")}}</td>
						<td></td>
						<td>Maklerpreis</td>
						<td class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maklerpreis" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.maklerpreis')}}">{{number_format($properties->maklerpreis,2,",",".")}}</a></td>
						<td>&nbsp;</td>
						
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<th colspan="3" class="border-no-right bg-gray">EBT</th>
						<th class="border-no-right text-right bg-gray">{{number_format($E18
						- $H48
						-$H52,0,",",".")}}</th>
						<th class="border-no-right text-right bg-gray">{{number_format($F18
							-$H52
							-$I48,0,",",".")}}</th>
						<th class="border-no-right text-right bg-gray">{{number_format($G18
							-$H52-
							$J48,0,",",".")}}</th>
						<th class="border-no-right text-right bg-gray">{{number_format($H18
							-$H52
							-$K48,0,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($I18
							-$H52
							-$L48,0,",",".")}}</th>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<td class="border-no-right">Steuern</td>
						<td class="border-no-right text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="tax" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.Tax')}} (%)">{{number_format($properties->tax*100,2,",",".")}}</a>%</td>
						<td class="border-no-right"></td>
						<td class="border-no-right text-right">{{number_format($E25,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->tax * $F23,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->tax * $G23,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->tax * $H23,0,",",".")}}</td>
						<td class="border text-right">{{number_format($properties->tax * $I23,0,",",".")}}</td>
						<td></td>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<th colspan="3" class="border-no-right bg-gray">EAT</th>
						<th class="border-no-right text-right bg-gray">{{number_format($E23 - ($properties->tax * $E23),0,",",".")}}</th>
						<th class="border-no-right text-right bg-gray">{{number_format($F23 - ($properties->tax * $F23),0,",",".")}}</th>
						<th class="border-no-right text-right bg-gray">{{number_format($G23 - ($properties->tax * $G23),0,",",".")}}</th>
						<th class="border-no-right text-right bg-gray">{{number_format($H23 - ($properties->tax * $H23),0,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($I23 - ($properties->tax * $I23),0,",",".")}}</th>
						<td></td>
						<th class="text-center">Stadt / Ort</th>
						<th class="text-center">Bundesland</th>
						<th class="text-center">Bodenrichtwert in €/m²</th>
						<th class="text-center">Einwohner</th>
						<th class="text-center">Lage</th>

					</tr>

					{{--TODO: start from here--}}
					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<td colspan="3" class="border-no-right">Tilgung Bank</td>
						<td class="border-no-right text-right">{{number_format($H49,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($I49,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($J49,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($K49,0,",",".")}}</td>
						<td class="border text-right">{{number_format($L49,2,",",".")}}</td>
						<td></td>
						<td><a href="#" class="inline-edit" data-type="text" data-pk="city_place" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.city_or_place')}}">{{$properties->city_place}}</a></td>
						<td><a href="#" class="inline-edit" data-type="text" data-pk="niedersachsen" data-url="{{url('property/update/'.$id) }}" data-title="Niedersachsen">{{$properties->niedersachsen}}</a></td>
						<td class="text-right bg-yellow">
							<a href="#" class="inline-edit" data-type="text" data-pk="ground_reference_value_in_euro_m2" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.ground_reference_value_in_euro_m2')}}">{{number_format($properties->ground_reference_value_in_euro_m2,2,",",".")}}</a>
						€</td>
						<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="population" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.population')}}">{{$properties->population}}</a></td>
						<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="position" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.position')}}">{{$properties->position}}</a></td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<th colspan="3" class="border-no-right bg-gray">Cashflow (nach Steuern)</th>
						<th class="border-no-right text-right bg-gray">{{number_format(($E23 - ($properties->tax * $E23)) - $H49 + $properties->depreciation_nk_money,0,",",".")}} </th>
						<th class="border-no-right text-right bg-gray">{{number_format(($F23 - ($properties->tax * $F23)) - $I49 + $properties->depreciation_nk_money,0,",",".")}}</th>
						<th class="border-no-right text-right bg-gray">{{number_format(($G23 - ($properties->tax * $G23)) - $J49 + $properties->depreciation_nk_money,0,",",".")}}</th>
						<th class="border-no-right text-right bg-gray">{{number_format(($H23 - ($properties->tax * $H23)) - $K49 + $properties->depreciation_nk_money,0,",",".")}}</th>
						<th class="border text-right bg-gray">{{number_format($I31 ,0,",",".")}}</th>
						<td></td>
						<th class="border text-center">{{__('property.field.address')}}</th>
						<td class="border text-center"><a href="#" class="inline-edit" data-type="text" data-pk="address" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.address')}}">{{$properties->address}}</a></td>
						<td></td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<th colspan="11" class="border bg-brown">1.) Erwerb- und Erwerbsnebenkosten</th>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<th class="border text-center">Grundstückswert</th>
						<td>&nbsp;</td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="border text-center">{{number_format($P43,2,",",".")}} €</td>
						<td>&nbsp;</td></tr>

					<tr>
						<th class="border-no-right bg-gray">Kaufpreis</th>
						<th class="border-no-right bg-gray text-center">Anteil</th>
						<th class="border bg-gray text-center">in EUR</th>
						<td></td>
						<th class="border-no-right bg-gray">Nebenkosten</th>
						<th class="border-no-right bg-gray text-center">Anteil</th>
						<th class="border bg-gray text-center">in EUR</th>
						<td></td>
						<th class="border-no-right bg-gray">Objektinfos</th>
						<th class="border-no-right bg-gray text-center">Gewerbe Fläche</th>

						<th class="border bg-gray text-center">WHG Fläche</th>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>

					</tr>

					<tr>
						<td class="border-no-right">Gebäude</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="building" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.building')}} (%)">{{number_format($properties->building*100,2,",",".")}}</a>%</td>
						<td class="border text-right">
							{{ number_format ( $properties->building * $properties->gesamt_in_eur, 0,",","." ) }}
						</td>
						<td></td>
						<td class="border-no-right">GrunderwerbsSt.</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="real_estate_taxes" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.real_estate_taxes')}} (%)">{{number_format($properties->real_estate_taxes*100,2,",",".")}}</a>%</td>
						<td class="border text-right">
							{{number_format($properties->real_estate_taxes * $properties->gesamt_in_eur,0,",",".")}}
						</td>
						<td></td>
						<td class="border-no-right">Vermietet</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="rent_retail" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.rent_retail')}}">{{number_format($properties->rent_retail,1,",",".")}}</a></td>

						<td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="rent_whg" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.rent_whg')}}">{{number_format($properties->rent_whg,1,",",".")}}</a></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td class="border-no-right">Grundstück</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="plot_of_land" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.plot_of_land')}} (%)">{{number_format($properties->plot_of_land*100,2,",",".")}}</a>%</td>
						<td class="border text-right">
							{{ number_format ( $properties->plot_of_land * $properties->gesamt_in_eur, 0 ,",",".") }}
						</td>
						<td></td>
						<td class="border-no-right">Makler</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="estate_agents" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.estate_agents')}} (%)">{{number_format($properties->estate_agents*100,2,",",".")}}</a>%</td>
						<td class="border text-right">
							{{number_format($properties->estate_agents * $properties->gesamt_in_eur,0,",",".")}}
						</td>
						<td></td>
						<td class="border-no-right">Leerstand</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="vacancy_retail" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.vacancy_retail')}}">{{number_format($properties->vacancy_retail,1,",",".")}}</a></td>

						<td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="vacancy_whg" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.vacancy_whg')}}">{{number_format($properties->vacancy_whg,1,",",".")}}</a></td>

					</tr>
					<tr>
						<th class="border-no-right">Gesamt</th>
						<td class="border bg-gray text-right"></td>
						<th class="border bg-gray text-right bg-yellow">
							<a href="#" class="inline-edit" data-type="text" data-pk="gesamt_in_eur" data-url="{{url('property/update/'.$id) }}">{{number_format($properties->gesamt_in_eur,0,",",".")}}</a>
						</th>
						<td></td>
						<td class="border-no-right">Notar/Grundbuch</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="Grundbuch" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.Grundbuch')}} (%)">{{number_format($properties->Grundbuch,2,",",".")}}</a>%</td>
						<td class="border text-right">
							{{number_format(($properties->Grundbuch * $properties->gesamt_in_eur)/100,0,",",".")}}
						</td>		<!--G38*D38-->
						<td colspan="4"></td>
						<td>&nbsp;</td>
						
					</tr>

					<tr>

					</tr>

					<tr>
						<td colspan="4"></td>
						<td class="border-no-right">Due Dilligence</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="evaluation" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.evaluation')}} (%)">{{number_format($properties->evaluation*100,2,",",".")}}</a>%</td>
						<td class="border text-right">
							{{number_format($properties->evaluation * $properties->gesamt_in_eur,0,",",".")}}
						</td>
						<td colspan="4"></td>
					</tr>

					<tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>

                    </tr>

					<tr>
						<td class="border-no-right">Gesamtkaufpreis</td>
						<td class="border-no-right">Netto KP</td>
						<td class="border text-right">
							{{number_format($properties->gesamt_in_eur,0,",",".")}}
						</td>
						<td></td>
						<td class="border-no-right">Sonstiges</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="others" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.others')}} (%)">{{number_format($properties->others*100,2,",",".")}}</a>%</td>
						<td class="border text-right">
							{{number_format($properties->others * $properties->gesamt_in_eur,0,",",".")}}
						</td>
						<td></td>
						<td class="border-no-right">WAULT</td>
						<td colspan="2" class="border bg-yellow text-center"><a href="#" class="inline-edit" data-type="text" data-pk="WAULT_of_Erwerb" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.WAULT_of_Erwerb')}}">{{number_format($properties->WAULT_of_Erwerb,2,",",".")}}</a></td>

					</tr>

					<tr>
						<td class="border-no-right"></td>
						<td class="border-no-right">AHK</td>
						<td class="border text-right">
							{{
								number_format( ($properties->real_estate_taxes * $properties->gesamt_in_eur)
								+ ($properties->estate_agents * $properties->gesamt_in_eur)
								+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
								+ ($properties->evaluation * $properties->gesamt_in_eur)
								+ ($properties->others * $properties->gesamt_in_eur)
								+ ($properties->buffer * $properties->gesamt_in_eur) , 0 ,",",".")
							}}
						</td>
						<td></td>
						<td class="border-no-right">Puffer</td>
						<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="buffer" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.buffer')}} (%)">{{number_format($properties->buffer*100,2,",",".")}}</a>%</td>
						<td class="border text-right">
							{{number_format($properties->buffer * $properties->gesamt_in_eur,0,",",".")}}
						</td>
						<td></td>
						<td class="border-no-right">Leerstandsquote</td>
						<td colspan="2" class="border bg-yellow text-center"><a href="#" class="inline-edit" data-type="text" data-pk="Leerstandsquote" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.Leerstandsquote')}} (%)">{{number_format($properties->Leerstandsquote,1,",",".")}}</a>%</td>

					</tr>

					<tr>
						<th colspan="2" class="border-no-right bg-gray">Gesamtkaufpreis</th>

						<th class="border bg-gray text-right">
							{{
								number_format( $D42 , 0 ,",",".")
							}}
						</th>
						<td></td>
						<th class="border-no-right bg-gray">Gesamt</th>
						<th class="border-no-right bg-gray text-right">{{number_format(($properties->real_estate_taxes+$properties->estate_agents+$properties->Grundbuch/100+$properties->evaluation+$properties->others+$properties->buffer)*100,2,",",".")}}%</th>
						<th class="border bg-gray text-right">
							{{
								number_format( ($properties->real_estate_taxes * $properties->gesamt_in_eur)
								+ ($properties->estate_agents * $properties->gesamt_in_eur)
								+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
								+ ($properties->evaluation * $properties->gesamt_in_eur)
								+ ($properties->others * $properties->gesamt_in_eur)
								+ ($properties->buffer * $properties->gesamt_in_eur) , 0 ,",",".")
							}}
						</th>
						<td></td>
						<td class="border-no-right">Vermietungsstand</td>
						<td colspan="2" class="border text-right">{{number_format((100-$properties->Leerstandsquote),1,",",".")}}</td>

					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>

                    </tr>

					<tr>
						<th colspan="11" class="border bg-brown">2.) Finanzierungsktruktur</th>
                       
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
                    </tr>
					<tr>
						<th class="border-no-right bg-gray">Gesamtkaufpreis</th>
						<th class="border-no-right bg-gray"></th>
						<th class="border bg-gray text-right">
							{{
								number_format( $D42 , 0 ,",",".")
							}} 
						</th>
						<td></td>
						<th colspan="2" class="border-no-right bg-gray">Finanz.Kosten Tilgung (p.a.)</th>
						<th class="border-no-right bg-gray">End of year 1</th>
						<th class="border-no-right bg-gray">End of year 2</th>
						<th class="border-no-right bg-gray">End of year 3</th>
						<th class="border-no-right bg-gray">End of year 4</th>
						<th class="border bg-gray">End of year 5</th>

					</tr>

					<tr>
						<td class="border-no-right">mit echtem EK</td>
						<td class="border-no-right bg-yellow text-right">
							<?php if($bank->id != NULL){?>
							<a href="#" class="inline-edit" data-type="text" data-pk="with_real_ek" data-url="{{url('banks/updatefield/'.$bank->id) }}" data-title="{{__('property.field.with_real_ek')}}">{{number_format($bank->with_real_ek*100,2,",",".")}}</a>%
							<?php }else{?>
							{{number_format($bank->with_real_ek*100,2,",",".")}}%
							<?php }?>
						</td>
						<td class="border text-right">
							{{number_format($D47,2,",",".")}}
						</td>
						<td></td>
						<td colspan="2" class="border-no-right">Valuta</td>
						<td class="border-no-right text-right">{{number_format($H47,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($I47,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($J47,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($K47,0,",",".")}}</td>
						<td class="border text-right">{{number_format($L47,0,",",".")}}</td>

					</tr>

					<tr>
						<th class="border-no-right">aus Anleihe</th>
						<td class="border-no-right bg-yellow text-right">
							<?php if($bank->id != NULL){?>
							<a href="#" class="inline-edit" data-type="text" data-pk="from_bond" data-url="{{url('banks/updatefield/'.$bank->id) }}" data-title="{{__('property.field.from_bond')}}">{{number_format($bank->from_bond*100,2,",",".")}}</a>%
							<?php }else{?>
							{{number_format($bank->from_bond*100,2,",",".")}}%
							<?php }?>
						</td>
						<td class="border text-right" readonly>
							{{
								number_format($D48,2,",",".")
							}}
						</td>
						<td></td>
						<td class="border-no-right">Zins Bankkredit</td>
						<td class="border-no-right bg-yellow text-right">
							<a href="#" class="inline-edit" data-type="text" data-pk="interest_bank_loan" data-url="{{url('banks/updatefield/'.$bank->id) }}" data-title="{{__('property.field.interest_bank_loan')}}">{{number_format($bank->interest_bank_loan*100,2,",",".")}}</a>%
						</td>
						<td class="border-no-right text-right">{{number_format($H48,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($I48,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($J48,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($K48,0,",",".")}}</td>
						<td class="border text-right">{{number_format($L48,0,",",".")}}</td>

					</tr>

					<tr>
						<td class="border-no-right">Bankkredit</td>
						<td class="border-no-right bg-yellow text-right">
							<?php if($bank->id != NULL){?>
							<a href="#" class="inline-edit" data-type="text" data-pk="bank_loan" data-url="{{url('banks/updatefield/'.$bank->id) }}" data-title="{{__('property.field.bank_loan')}}">{{number_format($bank->bank_loan*100,2,",",".")}}</a>%
							<?php }else{?>
							{{number_format($bank->bank_loan*100,2,",",".")}}%
							<?php }?>
						</td>
						<td class="border text-right">
							{{
								number_format($D49,2,",",".")
							}}
						</td>
						<td></td>
						<td class="border-no-right">Tilgung Bank</td>
						<td class="border-no-right bg-yellow text-right">
							<a href="#" class="inline-edit" data-type="text" data-pk="eradication_bank" data-url="{{url('banks/updatefield/'.$bank->id) }}" data-title="{{__('property.field.eradication_bank')}}">{{number_format($bank->eradication_bank*100,2,",",".")}}</a>%
						</td>
						<td class="border-no-right text-right">{{number_format($H49,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($I49,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($J49,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($K49,0,",",".")}}</td>
						<td class="border text-right">{{number_format($L49,0,",",".")}}</td>

					</tr>

					<tr>
						<th class="border-no-right">Summe</th>
						<th class="border-no-right bg-yellow text-right">
							{{number_format(($bank->with_real_ek + $bank->from_bond + $bank->bank_loan)*100,2,",",".")}}%
						</th>
						<th class="border text-right">
							{{number_format($D50,0,",",".")}}
						</th>
						<td></td>
						<th class="border-no-right bg-gray">Annuität</th>
						<th class="border-no-right bg-gray text-right">
							{{number_format(($bank->interest_bank_loan + $bank->eradication_bank)*100,2,",",".")}}% 
						</th>
						<th class="border-no-right bg-gray text-right">{{number_format($H50,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($I50,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($J50,0,",",".")}}</th>
						<th class="border-no-right bg-gray text-right">{{number_format($K50,0,",",".")}}</th>
						<th class="border bg-gray text-right">{{number_format($L50,0,",",".")}}</th>

					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>

                    </tr>

					<tr>
						<td colspan="4"></td>
						<td class="border-no-right">Zins Anleihe</td>
						<td class="border-no-right bg-yellow text-right">
							<a href="#" class="border text-right" data-type="text" data-pk="interest_bond" data-url="{{url('banks/updatefield/'.$bank->id) }}" data-title="{{__('property.field.interest_bond')}}">{{number_format($bank->interest_bond*100,2,",",".")}}</a>%
						</td>
						<td class="border text-right">{{number_format($H52,0,",",".")}}</td>
						<td colspan="4"></td>
                        <td>&nbsp;</td>

					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
                        <td>&nbsp;</td>

                    </tr>

					<tr>
						<th colspan="11" class="border bg-brown">3.) Betrieb und Verw.</th>
                        <td>&nbsp;</td>

					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
                        <td>&nbsp;</td>

                    </tr>

					<tr>
						<th colspan="2" class="border-no-right bg-gray">Netto Miete (IST) p.a.</th>
						<th class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_pa" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_pa')}}">{{number_format($properties->net_rent_pa,0,",",".")}}</a></th>
						<td></td>
						<th class="border-no-right bg-gray">Kennzahlen</th>
						<td colspan="4" class="border bg-gray" style="border-left: 0px;">Basis: Vorsteuern</td>
						<td colspan="2"></td>
                        <td>&nbsp;</td>

					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="border-no-right">Faktor (netto)</td>
						<td class="border text-center">{{number_format($L25,2,",",".")}}</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
                        <td>&nbsp;</td>

                    </tr>

					<tr>
						<th colspan="2" class="border bg-gray">Nichtumlagefähige NK</th>
						<th class="border bg-gray"></th>
						<td></td>
						<td class="border-no-right">BruttoRendite</td>
						<td class="border text-center">{{number_format(($G58 *100),2,",",".")}}%</td>
						<td></td>
						<td class="border-no-right">Ref. CF Salzgitter</td>
						<th class="border bg-yellow text-right">{{number_format($properties->Ref_CF_Salzgitter,0,",",".")}}</th>
						<td colspan="2">(nicht ändern!)</td>

					</tr>

					<tr>
						<td class="border-no-right text-right">Instandhaltung nichtumlfähig</td>
						<td class="border-no-right text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance_nk" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.maintenance_nk')}} (%)">{{number_format($properties->maintenance_nk*100,2,",",".")}}</a>%</td>
						<td class="border text-right">
							{{ $properties->maintenance_increase_year1 = number_format($properties->gesamt_in_eur
											*($properties->maintenance_nk),0,",",".")
							}}</td>
						<td></td>
						<td class="border-no-right">BruttoVervielält.</td>
						<td class="border text-center">{{number_format(($G59),2,",",".")}}</td>
						<td></td>
						<td class="border-no-right">Ref. GuV Salzgitter</td>
						<th class="border bg-yellow text-right">{{number_format($properties->Ref_GuV_Salzgitter,0,",",".")}}</th>
						<td colspan="2">(nicht ändern!)</td>
					</tr>

					<tr>
						<td class="border text-right">Betriebsk. nicht umlfähig</td>
						<td class="border text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="operating_costs_nk" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.operating_costs_nk')}} (%)">{{number_format($properties->operating_costs_nk*100,2,",",".")}}</a>%</td>
						<td class="border text-right">{{number_format($properties->operating_cost_increase_year1)}}</td>
						<td colspan="4"></td>
						<td class="border-no-right">AHK Salzgitter</td>
						<th class="border bg-yellow text-right">{{number_format($properties->AHK_Salzgitter,0,",",".")}}</th>
						<td colspan="2">(nicht ändern!)</td>
					</tr>

					<tr>
						<td class="border text-right">Objektverwalt. nichtumlfähig</td>
						<td class="border text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="object_management_nk" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.object_management_nk')}} (%)">{{number_format($properties->object_management_nk*100,2,",",".")}}</a>%</td>
						<td class="border text-right ">{{number_format($properties->property_management_increase_year1)}}</td>
						<td></td>
						<td class="border-no-right">EK-Rendite/CF</td>
						<td class="border text-right">{{number_format(($G61 * 100),2,",",".")}}%</td>			<!--=E31/D48--->
						<td></td>
						<th class="border-no-right">CF-Ref.-Faktor</th>

						<th class="border text-right">
							<?php
							//J58*(D42/J60)
							if($properties->AHK_Salzgitter != 0){
								$cf_faktor = ($properties->Ref_CF_Salzgitter*
										(($properties->gesamt_in_eur
														+ ($properties->real_estate_taxes * $properties->gesamt_in_eur)
														+ ($properties->estate_agents * $properties->gesamt_in_eur)
														+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
														+ ($properties->evaluation * $properties->gesamt_in_eur)
														+ ($properties->others * $properties->gesamt_in_eur)
														+ ($properties->buffer * $properties->gesamt_in_eur))
												/$properties->AHK_Salzgitter));
								if($cf_faktor != 0){
									echo e(number_format((($E31+$properties->tax * $E23)/$cf_faktor)*100,2,",","."));
								}else echo "0";
							}else echo "0";
							?>%
						</th> <!--(E31+E25)/(J58*(D42/J60))-->

						<td colspan="2"></td>
					</tr>

					<tr>
						<td class="border text-right">Abschreibung</td>
						<td class="border text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.depreciation_nk')}} (%)">{{number_format($properties->depreciation_nk*100,2,",",".")}}</a>%</td>
						<td class="border text-right">{{number_format($properties->depreciation_nk_money,0,",",".")}}</td>
						<td></td>
						<td class="border-no-right">EK-Rendite/GuV</td>
						<td class="border text-right">
							{{number_format(($G62 * 100),2,",",".")}}%
						<td></td>
						<th class="border-no-right">GuV-Ref.-Faktor</th>
						<th class="border text-right">
							<?php
							//J59*(D42/J60)
							if($properties->AHK_Salzgitter != 0){
								$guv_faktor = (($properties->Ref_GuV_Salzgitter)* (($properties->gesamt_in_eur
														+ ($properties->real_estate_taxes * $properties->gesamt_in_eur)
														+ ($properties->estate_agents * $properties->gesamt_in_eur)
														+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
														+ ($properties->evaluation * $properties->gesamt_in_eur)
														+ ($properties->others * $properties->gesamt_in_eur)
														+ ($properties->buffer * $properties->gesamt_in_eur))
												/($properties->AHK_Salzgitter)));
								if($guv_faktor != 0){
									echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
								}else echo "0";
							}else echo "0";
							?>%
						</th> <!--E23/(J59*(D42/J60))-->
						<td colspan="2"></td>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<th colspan="11" class="border bg-brown">4.) Exit / Rückzahlung Anleihe</th>
					</tr>

					<tr><td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td></tr>

					<tr>
						<td class="border-no-right">Wertenwicklung Immobilie</td>
						<td class="border-no-right text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="property_value" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.property_value')}}(%)">{{number_format($properties->property_value*100,2,",",".")}}</a>%</td>
						<td class="border">Steigerung p.a.</td>
						<td></td>
						<td class="border-no-right text-right">{{number_format($properties->Increase_p_a_year1,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->Increase_p_a_year2,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->Increase_p_a_year3,0,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($properties->Increase_p_a_year4,0,",",".")}}</td>
						<td class="border text-right">{{number_format($properties->Increase_p_a_year5,0,",",".")}}</td>
						<td colspan="2"></td>
					</tr>

					<tr>
						<td class="border-no-right" colspan="2">Darlehensvaluta</td>
						<td class="border">end of year</td>
						<td></td>
						<td class="border-no-right text-right">{{number_format($H47,3,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($I47,3,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($J47,3,",",".")}}</td>
						<td class="border-no-right text-right">{{number_format($K47,3,",",".")}}</td>
						<td class="border text-right">{{number_format($L47,0,",",".")}}</td>
						<td colspan="2"></td>
					</tr>

					<tr>
						<th colspan="8" class="border-no-right">a.) aus Verkaufserlös</th>
						<th class="border text-right">{{number_format(($properties->Increase_p_a_year5)-($L47),0,",",".")}}</th>
						<td colspan="2"></td>
					</tr>

					<tr>
						<th class="border-no-right">b.) aus Revalutierung</th>
						<td class="border-no-right text-right">80%</td>
						<td colspan="6" class="border-no-right">auf Wert im 5. J</td>
						<th class="border text-right">{{number_format($properties->Increase_p_a_year5 * 0.8 - $L47,0,",",".")}}</th>
						<td colspan="2"></td>
					</tr>
					
					<tr>
						<td>
							<form id="export-to-excel-form" method="post" action="{{url('property/export-to-excel')}}">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<input type="hidden" name="property_data" id="property-data" value="">
								<input type="hidden" name="tenant_data" id="tenant-data" value="">
								<input type="hidden" name="property_id" value="{{$id}}">
								<button type="button" class="btn-export-to-excel btn btn-primary" data-property-id="{{$properties->id}}">{{__('property.export')}}</button>
							</form>	
						</td>
					</tr>
					
					
					</tbody>
				</table>
				
				
			<table class="property-table" id="propertiesExtra1" style="position: relative; top:-850px; right: -1100px">
				<tr>
					<td></td>
					<td></td>
					<td>Laufzeit ab</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td class="border bg-yellow ">
						<a href="#" class="inline-edit" data-type="date" data-format="yyyy-mm-dd" data-pk="duration_from_O38" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.duration_from_O38')}}">{{date("d/m/Y", strtotime($properties->duration_from_O38))}}</a>
					</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<th class="text-right">Mieter</th>
					<td class="bg-gray border note">Netto Kaltmiete p.a.</td>
					<td class="bg-gray border">MV Ende</td>
					<td class="bg-gray border">WAULT</td>
					<td class="bg-gray border">Mieterlöse</td>
				</tr>


				<?php
				//declare values
				$Q53 = 0;
				?>
				@foreach($propertiesExtra1s as $propertiesExtra1)
				<tr>
					<td class="text-right">
						<a href="#" class="inline-edit" data-type="text" data-pk="tenant" data-url="{{url('propertiesExtra1/update/'.$propertiesExtra1->id) }}" data-title="{{__('property.field.tenant')}}">{{$propertiesExtra1->tenant}}</a>
					</td>
					<td class="border text-right bg-yellow">
						<a href="#" class="inline-edit" data-type="number" data-pk="net_rent_p_a" data-url="{{url('propertiesExtra1/update/'.$propertiesExtra1->id) }}" data-title="{{__('property.field.net_rent_p_a')}}">{{number_format($propertiesExtra1->net_rent_p_a,2,",",".")}} </a>€

					</td>
					<td class="border text-right bg-yellow">
						<a href="#" class="inline-edit" data-type="text" data-pk="mv_end" data-url="{{url('propertiesExtra1/update/'.$propertiesExtra1->id) }}" data-title="{{__('property.field.mv_end')}}">{{$propertiesExtra1->mv_end}}</a>
					</td>
					<td class="border text-center">
					<?php

					if( strpos($properties->duration_from_O38, "unbefristet") !== false){
						$value = 1;
					}else{
						$value = ((strtotime(str_replace('/', '-', $propertiesExtra1->mv_end)) -  strtotime(str_replace('/', '-', $properties->duration_from_O38))) / 86400) / 365;
					}
						$Q53 += $value*$propertiesExtra1->net_rent_p_a;

					?>

					{{number_format($value,1,",",".")}}</td>
					<td class="border text-right">{{number_format($value*$propertiesExtra1->net_rent_p_a,2,",",".")}} €</td>
				</tr>
				@endforeach



				<tr>
					<td colspan="5">
					<form action="{{route("properties.addPropertiesExtra1Row")}}" method="POST" style="text-align:right">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="property_id" value="{{$id}}">
						<button type="submit" class="btn btn-success btn-sm" >{{__('property.add_row')}}</button>
					</form>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="border-bottom" colspan="2">gesamt</td>
					<?php
						$P53 = ($properties->net_rent_pa == 0) ? 0 : ($Q53 / $properties->net_rent_pa) ;
						$Q56 = ($D59 + $D60) * $P53;
						$Q55 = $Q53+$P43;
						$Q57 = ($D42 *(-1))+$Q55-$Q56;
						$Q58 =	($D42 == 0) ? 0 : $Q57 / $D42;
					?>
					<td class="border-bottom text-center">{{number_format($P53,1,",",".")}}</td>
					<td class="border-bottom text-right">{{number_format($Q53,2,",",".")}} € </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="border-bottom" colspan="2">Grundstückswert</td>
					<td class="border-bottom text-center"></td>
					<td class="border-bottom text-right"> {{number_format($P43,2,",",".")}} € </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="border-bottom" colspan="2">Wert total</td>
					<td class="border-bottom text-center"></td>
					<td class="border-bottom text-right"> {{number_format($Q55 ,2,",",".")}} € </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="border-bottom" colspan="2">nicht umlagefähige Nebenkosten und Instandhaltungen über die WAULT</td>
					<td class="border-bottom text-center"></td>
					<td class="border-bottom text-right">{{number_format($Q56,2,",",".")}} € </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="border-bottom" colspan="2">Risikowert</td>
					<td class="border-bottom text-center"></td>
					<td class="border-bottom text-right">{{number_format($Q57,2,",",".")}} € </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="border-bottom" colspan="2">%-Risikowert</td>
					<td class="border-bottom text-center"></td>
					<td class="border-bottom text-right bg-red">{{number_format($Q58*100,1,",",".")}}%</td>
				</tr>
				

	</table>
				
			</div>