<div class="">
	<div class="modal-dialog modal-lg" style="width: 100%">
		<!-- Modal content-->
	    <div class="modal-content">
			<div class="modal-body">				
				<div class="row">
				<iframe src="{{ url('property/'.$id.'/file-manager')}}" id="file-manager-{{ $id }}" width="100%" height="1900px" style="border:none;"></iframe>  
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
			</div>
		</div>
	</div>
</div>
