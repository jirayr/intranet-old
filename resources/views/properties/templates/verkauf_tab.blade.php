
@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <style type="text/css">
      .change-v-user{
        width: 80px !important;
      }
      .pre_tab_loader_class {
         display: none;
         width:100%;
         height:100%;
         position: absolute;
         background-color: #fff;
         z-index: 99;
      }
    </style>
@endsection
<div id="tenancy-schedule">
    <h1>Verkauf</h1>

  
@php
$i=0;
@endphp
@foreach ($banks as $key => $bank)


            <?php

              $D42 = $properties->gesamt_in_eur 
        + ($properties->real_estate_taxes * $properties->gesamt_in_eur) 
        + ($properties->estate_agents * $properties->gesamt_in_eur) 
        + (($properties->Grundbuch * $properties->gesamt_in_eur)/100) 
        + ($properties->evaluation * $properties->gesamt_in_eur) 
        + ($properties->others * $properties->gesamt_in_eur) 
        + ($properties->buffer * $properties->gesamt_in_eur);
              $D46 = $D42;
              
             

              //echo "<pre>";
              //print_r($properties); exit;
  
      $D47 = $bank->with_real_ek * $D46;  //C47*D46
      $D48 = $bank->from_bond * $D46; //C48*D46
      
      $D49 = $bank->bank_loan * $D46;

    
      $D50 = $D47 + $D48 +  $D49;

      
            if($bank == ''){
                $bank_check = 0;
                $bank = $fake_bank;
            }else{
                $bank_check = 1;
      }

            $H47 = $D49 - $D49 * $bank->eradication_bank;
            $H48 = $D49 * $bank->interest_bank_loan;    //G48*D49
            $H49 = $D49 * $bank->eradication_bank;
            $H50 = $D49 * $bank->interest_bank_loan + $D49*$bank->eradication_bank;
      $H52 = $D48 * $bank->interest_bond;
           
            $I48 = $H47 * $bank->interest_bank_loan;
            $I49 = $H50 - $I48;
            $I47 = $H47 - $I49;
            $I50 = $I48 + $I49;

            $J48 = $I47 * $bank->interest_bank_loan;
            $J49 = $H50 - $J48;
            $J47 = $I47 - $J49;
            $J50 = $J48 + $J49;

            $K48 = $J47 * $bank->interest_bank_loan;
            $K49 = $I50 - $K48;
            $K47 = $J47 - $K49;
            $K50 = $K48 + $K49;

            $L48 = $K47 * $bank->interest_bank_loan;
            $L49 = $J50 - $L48;
            $L47 = $K47 - $L49;
            $L50 = $L48 + $L49;


            $E18 = ($properties->net_rent_increase_year1
      -$properties->maintenance_increase_year1
      -$properties->operating_cost_increase_year1
      -$properties->property_management_increase_year1)
      -$properties->depreciation_nk_money;
      
      $F18 = ($properties->net_rent_increase_year2
      -$properties->maintenance_increase_year2
      -$properties->operating_cost_increase_year2
      -$properties->property_management_increase_year2)
      -$properties->depreciation_nk_money;

      $G18 = ($properties->net_rent_increase_year3
      -$properties->maintenance_increase_year3
      -$properties->operating_cost_increase_year3
      -$properties->property_management_increase_year3)
      -$properties->depreciation_nk_money;

      $H18 = 
      ($properties->net_rent_increase_year4
      -$properties->maintenance_increase_year4
      -$properties->operating_cost_increase_year4
      -$properties->property_management_increase_year4)
      -$properties->depreciation_nk_money;

      $I18 = ($properties->net_rent_increase_year5
      -$properties->maintenance_increase_year5
      -$properties->operating_cost_increase_year5
      -$properties->property_management_increase_year5)
      -$properties->depreciation_nk_money;

      $E23 = $E18- $H48 -$H52;
      $F23 = $F18-($H52)-($I48);
      $G23 = $G18-($H52)-($J48);
      $H23 = $H18 - ($H52)-($K48);
      $I23 = $I18-($H52)-($L48);
      $E31 = ($E23 - ($properties->tax * $E23)) - $H49 + $properties->depreciation_nk_money;
      
      $I16= $properties->depreciation_nk_money;
      $I27 =$I23 - ($properties->tax * $I23);
      $I29 = $L49;          
      
      
      $I31 = $I27-$I29+$I16;

      $G58 = ($D42 == 0) ? 0: $properties->net_rent_pa/$D42;
      $G59 = ($properties->net_rent_pa == 0) ? 0 : $D42/ $properties->net_rent_pa;
      $E25 = $properties->tax * $E23;
      $G61 = ($D48 == 0) ? 0 : $E31/$D48;
      $G62 = ($D48 == 0) ? 0 : $E23/$D48;
      
      

      $L14 = ($properties->rent_retail + $properties->rent_whg == 0) ? 0 : ($properties->net_rent_pa / ($properties->rent_retail + $properties->rent_whg) / 12);
      $L16 = ($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg == 0) ? 0 : 
      ($properties->gesamt_in_eur)/($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg);
      $L20 = ($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg == 0) ? 0 : 
      ($properties->rent_retail + $properties->rent_whg)/($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg);
      
      $L25 = ($properties->net_rent_pa == 0) ? 0 : $properties->gesamt_in_eur / $properties->net_rent_pa;

      
      $L11 = $properties->plot_of_land_m2;
      $P31 = $properties->ground_reference_value_in_euro_m2;
      $P43 = $L11* $P31;
      $D59 = $properties->gesamt_in_eur*($properties->maintenance_nk);
      $D60 = $properties->operating_cost_increase_year1;
      
      $verkaufspreis_input = '';
      if(!empty($properties->verkaufspreis)) {
          $verkaufspreis_input = $properties->verkaufspreis;
      }
      
     // echo number_format($properties->gesamt_in_eur,0,",","."); exit;
       //echo number_format($D50,0,",",".")."----------------------------"; //exit;
       ?>
 

 <div class="row Verkauf_tab_editable" id="ajax_tab_loader" >

  @include('properties/templates/verkauf_tab_table')

 </div>
 @php
$i++;
if($i==1) break;
 @endphp
 @endforeach
</div>
<?php
// print_r($properties);
$name = $properties->name_of_property;
$seller_id = $properties->seller_id;
$released_by = "";
$sale_date = "";
$street = "";

if($ps_data && $ps_data->property_name)
  $name = $ps_data->property_name;

$seller_id = $properties->seller_id;
if($ps_data && $ps_data->saller_id)
  $seller_id = $ps_data->saller_id;

if($ps_data && $ps_data->released_by)
  $released_by = $ps_data->released_by;

if($ps_data && $ps_data->sale_date)
  $sale_date = date('d.m.Y',strtotime($ps_data->sale_date));

if($ps_data && $ps_data->street)
  $street = $ps_data->street;


$vprice = 0;
$market_value = 0;
$iiid = 0;
if(isset($GLOBALS['ist_sheet']) && $GLOBALS['ist_sheet']){
  $ist_sheet = $GLOBALS['ist_sheet'];
  $vprice = $ist_sheet->preisverk;
  $iiid = $ist_sheet->id;
  $market_value = $ist_sheet->market_value;
}

$button_class1 = $button_class2 = ' btn-primary';
$rbutton_title1 = "Freigeben";
$rbutton_title2 = "Zur Freigabe senden";
if(isset($buy_data) && isset($buy_data['vbtn1']))
{
  $button_class1 = $button_class2 =  " btn-success";
  $rbutton_title1 = "Freigegeben";
  $rbutton_title2 = "Freigegeben";
}
else if(isset($buy_data) && isset($buy_data['vbtn1_request']))
{
  $button_class1 =   " btn-success";
  $rbutton_title2 = "Zur Freigabe gesendet";
}

?>


<?php
$btn1 = 1; 
$btn2 = $btn3 = $btn4 = $btn5 = $btn6 = $btn7 = $btn8 = 1;

$button_class2 =  $button_class4 =  $button_class6 =  $button_class8 = " btn-primary ver-release-procedure";

$button_class9 = $button_class1 = $button_class3 =  $button_class5  = $button_class7  = " btn-primary ver-release-procedure-1";



$rbutton_title1 = "Zur Freigabe an Andrea senden";
$rbutton_title2 = "Zur Freigabe an Alex senden";
$rbutton_title3 = "Zur Freigabe an Andrea senden";
$rbutton_title4 = "Zur Freigabe an Falk senden";
$rbutton_title5 = "Als erledigt markieren";

$r1= $r2 =$r3 = $r4 = $r5 = "";

if(isset($sale_data) && isset($sale_data['btn5_request']))
{
  $button_class9 =  " btn-success";
  $rbutton_title5 = "Alles erledigt";
}


if($sale_date && $released_by)
{
  $r4 = $released_by.': '.$sale_date;    
  $button_class7 = $button_class8 = " btn-success";
  $rbutton_title4 = "Freigegeben";
}
else if(isset($sale_data) && isset($sale_data['btn4_request']))
{
  $button_class7 =  " btn-success ver-release-procedure-1";

  $rbutton_title4 = "Zur Freigabe gesendet";
  
}

if(isset($sale_data) && isset($sale_data['btn3']))
{
  $button_class5 = $button_class6 = " btn-success";
  $rbutton_title3 = $rbutton_title1 = $rbutton_title2 = "Freigegeben";
}
else if(isset($sale_data) && isset($sale_data['btn3_request']))
{
  $button_class5 =  " btn-success ver-release-procedure-1";
  $rbutton_title3 = "Zur Freigabe gesendet";
}

if(isset($sale_data) && isset($sale_data['btn2']))
{
  $button_class3 = $button_class4 =  " btn-success ";
  $rbutton_title2 = "Freigegeben";
}
else if(isset($sale_data) && isset($sale_data['btn2_request']))
{
  $button_class3 =  " btn-success ver-release-procedure-1";
  $rbutton_title2 = "Zur Freigabe gesendet";
}

if(isset($sale_data) && isset($sale_data['btn1']))
{
  $button_class1 = $button_class2 =  " btn-success";
  $rbutton_title1 = "Freigegeben";
}
else if(isset($sale_data) && isset($sale_data['btn1_request']))
{
  $button_class1 =   " btn-success ver-release-procedure-1";
  $rbutton_title1 = "Zur Freigabe gesendet";
}

if(isset($sale_data['btn1']['user1']) && $sale_data['btn1']['user1'])
{
  $u = DB::table('users')->where('id', $sale_data['btn1']['user1'])->first();
  if($u)
  {
    $r1 = $u->name.': '.date('d.m.Y',strtotime($sale_data['btn1']['created_at']));    
  }
}
if(isset($sale_data['btn2']['user1']) && $sale_data['btn2']['user1'])
{
  $u = DB::table('users')->where('id', $sale_data['btn2']['user1'])->first();
  if($u)
  {
    $r2 = $u->name.': '.date('d.m.Y',strtotime($sale_data['btn2']['created_at']));    
  }
}
if(isset($sale_data['btn3']['user1']) && $sale_data['btn3']['user1'])
{
  $u = DB::table('users')->where('id', $sale_data['btn3']['user1'])->first();
  if($u)
  {
    $r3 = $u->name.': '.date('d.m.Y',strtotime($sale_data['btn3']['created_at']));    
  }
}


if(isset($sale_data['btn5_request']['user1']) && $sale_data['btn5_request']['user1'])
{
  $u = DB::table('users')->where('id', $sale_data['btn5_request']['user1'])->first();
  if($u)
  {
    $r5 = $u->name.': '.date('d.m.Y',strtotime($sale_data['btn5_request']['created_at']));    
  }
}
$loginuseremail = Auth::user()->email;

$andrea = $alex = "0";
if(Auth::user()->email=="a.raudies@fcr-immobilien.de")
  $andrea= "1";
if(Auth::user()->email=="a.lauterbach@fcr-immobilien.de")
  $alex= "1";

// $accept_button = "Freigeben";

?>







<br>
<p><b>Verkaufspreis/Portal</b> : 
<a href="#" class="inline-edit preisverk" data-type="text" data-pk="preisverk" data-placement="bottom" data-url="{{url('property/update_property_ist_soll/'.$iiid) }}/Ist" data-title="Verkaufspreis">{{ number_format($vprice,2,',','.') }}</a>
€</p>

<p><b>Verkehrswert</b> : 
<a href="#" class="inline-edit preisverk" data-type="text" data-pk="market_value" data-placement="bottom" data-url="{{url('property/update_property_ist_soll/'.$iiid) }}/Ist" data-title="Verkehrswert">{{ number_format($market_value,2,',','.') }}</a>
€</p>


<input type="hidden" class="property_id" value="<?php echo $id?>">
<div class="row tenancy-schedules sale-sheet" style="margin-top: 20px;">
  <div class="col-sm-12">
    <!-- <h3 class="">Verkaufsdatenblatt</h3> -->

    <h3 class="title-tag" style="width: 60%;float: left;">1) Verkaufsentscheidung (Andrea Raudies)
        @if(Auth::user()->email!="a.raudies@fcr-immobilien.de" && Auth::user()->email!=config('users.falk_email') && Auth::user()->email!="a.lauterbach@fcr-immobilien.de")
        <button style="color: #fff;width: 190px;font-size: 12px;margin-left: 10px;" type="button"
          data-id="btn1_request" data-status="{{$btn1}}"
         class="pull-right btn {{$button_class1}}  ">{{$rbutton_title1}}</button>
        
        @elseif(Auth::user()->email=="a.raudies@fcr-immobilien.de" || Auth::user()->email==config('users.falk_email') || $button_class2==" btn-success")
        <button style="color: #fff;width: 190px;font-size: 12px;margin-left: 10px;" data-id="btn1" data-status="1"
        type="button" class=" pull-right btn {{$button_class2}}  ">
        @if($button_class2==" btn-success") Freigegeben @else
      Freigeben @endif</button>
        @endif

        <!-- <a href="javascript:void(0)" data-class="ver-sheet-1" class="btn btn-info show-link-einkauf  pull-right"><i class="fa fa-angle-down"></i></a> -->

      </h3>
      <h3 style="width: 35%;float: left;margin-left: 5px;">{{$r1}}</h3>

   
    
        <div class="clearfix"></div>
        <h3 class="title-tag" style="width: 60%;float: left;">
        2) Verkaufsformalitäten (Alexander Lauterbach)
        @if(Auth::user()->email!="a.lauterbach@fcr-immobilien.de" && Auth::user()->email!=config('users.falk_email') && Auth::user()->email!="a.lauterbach@fcr-immobilien.de")
        <button data-id="btn2_request" style="color: #fff;width: 190px;font-size: 12px;margin-left: 10px;" type="button" data-status="{{$btn3}}" class=" pull-right btn {{$button_class3}} "> {{$rbutton_title2}}</button>

        @elseif(Auth::user()->email=="a.lauterbach@fcr-immobilien.de" || Auth::user()->email==config('users.falk_email') || $button_class4==" btn-success")
        <button style="color: #fff;width: 190px;font-size: 12px;margin-left: 10px;" data-id="btn2"
        type="button" data-status="1" class=" pull-right btn {{$button_class4}}  ">@if($button_class4==" btn-success") Freigegeben @else
      Freigeben @endif</button>
        @endif

        <a href="javascript:void(0)" data-class="ver-sheet-2" class="btn btn-info show-link-einkauf  pull-right"><i class="fa fa-angle-down"></i></a>


      </h3>
      <h3 style="width: 35%;float: left;margin-left: 5px;">{{$r2}}</h3>
      <div class="clearfix"></div>




    <div class="ver-sheet-2 hidden">
      <table class="forecast-table" style="margin-top: 10px;">
        <thead>
        <tr>
          <th class="border checkbox-td bg-gray" colspan="3" >Notar/Kaufvertrag</th>
          <th class="border text-center bg-gray">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
      </thead>
        <tbody>
          <tr>
          <th class="border bg-gray" colspan="3">gesellschaftlich</th>
          <td class="border bg-gray" colspan="4"></td>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="23" @if(isset($sale_data[23]['status']) &&  $sale_data[23]['status']) checked @endif></td>
          <td class="border" colspan="2">Gesellschafterbeschluss Verkauf gem. § 179 a AKTG</td>

          <td class="border checkbox-td">
              <select class="change-v-user" data-type="23" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[23]['user1']) &&  $sale_data[23]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user2']) &&  $sale_data[1]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user3']) &&  $sale_data[1]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>

          <td class="border" ><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/23') }}" data-title="Kommentar">{{ @$sale_data[23]['comment'] }}</a></td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="24" @if(isset($sale_data[24]['status']) &&  $sale_data[24]['status']) checked @endif></td>
          
          <td class="border" colspan="2">Darlehen: Kündigungsfrist und VFE beachten</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="24" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[24]['user1']) &&  $sale_data[24]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user2']) &&  $sale_data[1]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user3']) &&  $sale_data[1]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>

          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/24') }}" data-title="Kommentar">{{ @$sale_data[24]['comment'] }}</a></td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="25" @if(isset($sale_data[25]['status']) &&  $sale_data[25]['status']) checked @endif></td>
          <td class="border" colspan="2">Ausscheiden der AG</td>

          <td class="border checkbox-td">
              <select class="change-v-user" data-type="25" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[25]['user1']) &&  $sale_data[25]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user2']) &&  $sale_data[1]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user3']) &&  $sale_data[1]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>

          <td class="border" ><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/25') }}" data-title="Kommentar">{{ @$sale_data[25]['comment'] }}</a></td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="26" @if(isset($sale_data[26]['status']) &&  $sale_data[26]['status']) checked @endif></td>
          <td class="border" colspan="2">Gesellschaft und Sitz der Gesellschaft</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="26" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[26]['user1']) &&  $sale_data[26]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user2']) &&  $sale_data[1]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user3']) &&  $sale_data[1]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/26') }}" data-title="Kommentar">{{ @$sale_data[26]['comment'] }}</a></td>
        </tr>
        </tbody>
      </table>
    </div>

    <h3 class="title-tag" style="width: 60%;float: left;">3) Verkaufsdatenblatt (Andrea Raudies) 
        @if(Auth::user()->email!="a.raudies@fcr-immobilien.de" && Auth::user()->email!=config('users.falk_email') && Auth::user()->email!="a.lauterbach@fcr-immobilien.de")
        <button style="color: #fff;width: 190px;font-size: 12px;margin-left: 10px;" type="button"
          data-id="btn3_request" data-status="{{$btn5}}"
         class="pull-right btn {{$button_class5}} ">{{$rbutton_title3}}</button>


        @elseif(Auth::user()->email=="a.raudies@fcr-immobilien.de" || Auth::user()->email==config('users.falk_email') || $button_class6==" btn-success")
        <button style="color: #fff;width: 190px;font-size: 12px;margin-left: 10px;" data-id="btn3"
        type="button" data-status="1" class=" pull-right btn {{$button_class6}}">@if($button_class6==" btn-success") Freigegeben @else
      Freigeben @endif</button>
        @endif

        <a href="javascript:void(0)" data-class="ver-sheet-3" class="btn btn-info show-link-einkauf  pull-right"><i class="fa fa-angle-down"></i></a>
    </h3>
    <h3 style="width: 35%;float: left;margin-left: 5px;">{{$r3}}</h3>
    <div class="clearfix"></div>


    <div class="ver-sheet-3 hidden">

      <h4>Checkliste</h4>
    Verkaufsobjekt
    <a href="#" class="inline-edit" data-type="text" data-pk="property_name" data-url="{{url('verkauf/update/'.$id) }}" data-title="property_name">{{ $name }}</a>
    <br>
      <table class="forecast-table" style="margin-top: 10px;">
      <thead>
        <tr>
          <th class="border bg-gray">PLZ</th>
          <th class="border bg-gray">Ort</th>
          <!-- <th class="border bg-gray">Stadt</th> -->
          <th class="border bg-gray">Straße, Nr.</th>
          <th class="border bg-gray">Verkäufer</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="plz_ort" data-url="{{url('property/update/schl/'.$id) }}" data-title="PLZ:">{{ $properties->plz_ort }}</a></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="ort" data-url="{{url('property/update/schl/'.$id) }}" data-title="Ort:">{{ $properties->ort }}</a></td>
          
           <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="strasse" data-url="{{url('property/update/schl/'.$id) }}" data-title="Stadt">{{ $properties->strasse }}</a></td> 
          <!--<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="street" data-url="{{url('verkauf/update/'.$id) }}" data-title="Straße, Nr">{{ $street }}</a></td>-->
          <td class="border">
            <select class="change-l-user">
                <option value="">{{__('dashboard.seller')}}</option>
                @foreach($tr_users as $list)
                @if($seller_id==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->name}}</option>
                @else
                <option value="{{$list->id}}">{{$list->name}}</option>
                @endif
                @endforeach
              
              </select></td>
        </tr>
      </tbody>
    </table>


    <table class="forecast-table" style="margin-top: 20px;">
      <thead>
        <tr>
          <th class="border checkbox-td bg-gray" colspan="3" >Notar/Kaufvertrag</th>
          <th class="border text-center bg-gray">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="border checkbox-td text-center">
            <input type="checkbox" class="checkbox-verkauf" data-type="1" 
            @if(isset($sale_data[1]['status']) &&  $sale_data[1]['status']) checked @endif >
          </td>
          <td class="border" colspan="2">Kaufvertrag verhandelt, geprüft und durch FR freigegeben</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="1" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user1']) &&  $sale_data[1]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user2']) &&  $sale_data[1]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="1" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[1]['user3']) &&  $sale_data[1]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/1') }}" data-title="Kommentar">{{ @$sale_data[1]['comment'] }}</a>


          </td>
        </tr>
        <tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="2" @if(isset($sale_data[2]['status']) &&  $sale_data[2]['status']) checked @endif ></td>
          <td class="border">Käufer- / Verkäuferdaten</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="2" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[2]['user1']) &&  $sale_data[2]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="2" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[2]['user2']) &&  $sale_data[2]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="2" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[2]['user3']) &&  $sale_data[2]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
           <td class="border">
                <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/2') }}" data-title="Kommentar">{{ @$sale_data[2]['comment'] }}</a>


          </td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="3" @if(isset($sale_data[3]['status']) &&  $sale_data[3]['status']) checked @endif ></td>
          <td class="border">Flurstück / Blattnummer</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="3" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[3]['user1']) &&  $sale_data[3]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="3" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[3]['user2']) &&  $sale_data[3]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="3" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[3]['user3']) &&  $sale_data[3]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/3') }}" data-title="Kommentar">{{ @$sale_data[3]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="4" @if(isset($sale_data[4]['status']) &&  $sale_data[4]['status']) checked @endif ></td>
          <td class="border">Grundstücksgröße in m²</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="4" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[4]['user1']) &&  $sale_data[4]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="4" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[4]['user2']) &&  $sale_data[4]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="4" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[4]['user3']) &&  $sale_data[4]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/4') }}" data-title="Kommentar">{{ @$sale_data[4]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="5" @if(isset($sale_data[5]['status']) &&  $sale_data[5]['status']) checked @endif ></td>
          <td class="border">Dienstbarkeiten</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="5" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[5]['user1']) &&  $sale_data[5]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="5" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[5]['user2']) &&  $sale_data[5]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="5" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[5]['user3']) &&  $sale_data[5]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/5') }}" data-title="Kommentar">{{ @$sale_data[5]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="6" @if(isset($sale_data[6]['status']) &&  $sale_data[6]['status']) checked @endif ></td>
          <td class="border">Abt III löschen</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="6" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[6]['user1']) &&  $sale_data[6]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="6" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[6]['user2']) &&  $sale_data[6]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="6" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[6]['user3']) &&  $sale_data[6]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/6') }}" data-title="Kommentar">{{ @$sale_data[6]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="7" @if(isset($sale_data[7]['status']) &&  $sale_data[7]['status']) checked @endif ></td>
          <td class="border">Kaufpreis</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="7" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[7]['user1']) &&  $sale_data[7]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="7" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[7]['user2']) &&  $sale_data[7]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="7" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[7]['user3']) &&  $sale_data[7]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/7') }}" data-title="Kommentar">{{ @$sale_data[7]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="8" @if(isset($sale_data[8]['status']) &&  $sale_data[8]['status']) checked @endif ></td>
          <td class="border">Notaranderkonto / Anzahlung (5% / 10% / 15%)</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="8" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[8]['user1']) &&  $sale_data[8]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="8" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[8]['user2']) &&  $sale_data[8]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="8" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[8]['user3']) &&  $sale_data[8]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/8') }}" data-title="Kommentar">{{ @$sale_data[8]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="9" @if(isset($sale_data[9]['status']) &&  $sale_data[9]['status']) checked @endif ></td>
          <td class="border">BNL-Klausel (Monatsende / 10 Tage-Frist) / Folgetag der Kaufpreiszahlung</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="9" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[9]['user1']) &&  $sale_data[9]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="9" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[9]['user2']) &&  $sale_data[9]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="9" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[9]['user3']) &&  $sale_data[9]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/9') }}" data-title="Kommentar">{{ @$sale_data[9]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="10" @if(isset($sale_data[10]['status']) &&  $sale_data[10]['status']) checked @endif ></td>
          <td class="border">UST-Klausel (KP ohne USt.)</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="10" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[10]['user1']) &&  $sale_data[10]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="10" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[10]['user2']) &&  $sale_data[10]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="10" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[10]['user3']) &&  $sale_data[10]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/10') }}" data-title="Kommentar">{{ @$sale_data[10]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="11" @if(isset($sale_data[11]['status']) &&  $sale_data[11]['status']) checked @endif ></td>
          <td class="border">Kontonummer</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="11" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[11]['user1']) &&  $sale_data[11]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="11" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[11]['user2']) &&  $sale_data[11]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="11" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[11]['user3']) &&  $sale_data[11]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/11') }}" data-title="Kommentar">{{ @$sale_data[11]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="12" @if(isset($sale_data[12]['status']) &&  $sale_data[12]['status']) checked @endif ></td>
          <td class="border">Versicherung (Übernahme / Ablauf)</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="12" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[12]['user1']) &&  $sale_data[12]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="12" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[12]['user2']) &&  $sale_data[12]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="12" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[12]['user3']) &&  $sale_data[12]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/12') }}" data-title="Kommentar">{{ @$sale_data[12]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="13" @if(isset($sale_data[13]['status']) &&  $sale_data[13]['status']) checked @endif ></td>
          <td class="border">Nebenkostenabrechnung</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="13" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[13]['user1']) &&  $sale_data[13]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="13" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[13]['user2']) &&  $sale_data[13]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="13" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[13]['user3']) &&  $sale_data[13]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/13') }}" data-title="Kommentar">{{ @$sale_data[13]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="14" @if(isset($sale_data[14]['status']) &&  $sale_data[14]['status']) checked @endif ></td>
          <td class="border">Garantien (falls vorhanden)</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="14" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[14]['user1']) &&  $sale_data[14]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="14" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[14]['user2']) &&  $sale_data[14]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="14" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[14]['user3']) &&  $sale_data[14]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/14') }}" data-title="Kommentar">{{ @$sale_data[14]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="15" @if(isset($sale_data[15]['status']) &&  $sale_data[15]['status']) checked @endif ></td>
          <td class="border">Rücktrittsrechte</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="15" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[15]['user1']) &&  $sale_data[15]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="15" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[15]['user2']) &&  $sale_data[15]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="15" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[15]['user3']) &&  $sale_data[15]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/15') }}" data-title="Kommentar">{{ @$sale_data[15]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center">&nbsp;</td>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="16" @if(isset($sale_data[16]['status']) &&  $sale_data[16]['status']) checked @endif ></td>
          <td class="border">Sondersituation (falls vorhanden)</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="16" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[16]['user1']) &&  $sale_data[16]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="16" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[16]['user2']) &&  $sale_data[16]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="16" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[16]['user3']) &&  $sale_data[16]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/16') }}" data-title="Kommentar">{{ @$sale_data[16]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="17" @if(isset($sale_data[17]['status']) &&  $sale_data[17]['status']) checked @endif ></td>
          <td class="border" colspan="2">Notartermin vereinbaren und FR oder TR rechtzeitig informieren</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="17" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[17]['user1']) &&  $sale_data[17]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="17" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[17]['user2']) &&  $sale_data[17]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="17" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[17]['user3']) &&  $sale_data[17]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/17') }}" data-title="Kommentar">{{ @$sale_data[17]['comment'] }}</a></td>
        </tr><tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="18" @if(isset($sale_data[18]['status']) &&  $sale_data[18]['status']) checked @endif ></td>
          <td class="border" colspan="2">Abschrift mitnehmen</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="18" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[18]['user1']) &&  $sale_data[18]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="18" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[18]['user2']) &&  $sale_data[18]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="18" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[18]['user3']) &&  $sale_data[18]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/18') }}" data-title="Kommentar">{{ @$sale_data[18]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
        </tr>
        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
        </tr>

        <tr>
          <td colspan="7">&nbsp;</td>
        </tr>

      </tbody>
    </table>
    <table class="forecast-table" style="margin-top: 20px;">
      <thead>
        <tr>
          <th class="border checkbox-td bg-gray" colspan="3" >Notar/Kaufvertrag</th>
          <th class="border text-center bg-gray">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
      </thead>
      <tbody>


          <tr>
          <th class="border bg-gray" colspan="5">steuerlich</th>
        </tr>


        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="20" @if(isset($sale_data[20]['status']) &&  $sale_data[20]['status']) checked @endif></td>
          <td class="border" colspan="2">Umsatzsteuer (Kaufpreis ohne Umsatzsteuer)</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="20" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[20]['user1']) &&  $sale_data[20]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="20" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[20]['user2']) &&  $sale_data[20]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="20" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[20]['user3']) &&  $sale_data[20]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>

          <td class="border" ><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/20') }}" data-title="Kommentar">{{ @$sale_data[20]['comment'] }}</a></td>
          
        </tr>
        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="21" @if(isset($sale_data[21]['status']) &&  $sale_data[21]['status']) checked @endif></td>
          <td class="border" colspan="2">Gewerbesteuer</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="21" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[21]['user1']) &&  $sale_data[21]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="21" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[21]['user2']) &&  $sale_data[21]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="21" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[21]['user3']) &&  $sale_data[21]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/21') }}" data-title="Kommentar">{{ @$sale_data[21]['comment'] }}</a></td>
        </tr>

        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
        </tr>
      </tbody>
    </table>

    <table class="forecast-table" style="margin-top: 20px;">
      <thead>
        <tr>
          <th class="border checkbox-td bg-gray" colspan="3" >Notar/Kaufvertrag</th>
          <th class="border text-center bg-gray">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
      </thead>
      <tbody>

        <tr>
          <th class="border bg-gray" colspan="5">juristisch</th>
        </tr>

        <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="22" @if(isset($sale_data[22]['status']) &&  $sale_data[22]['status']) checked @endif></td>
          <td class="border" colspan="2">Kanzlei</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="22" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[22]['user1']) &&  $sale_data[22]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="22" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[22]['user2']) &&  $sale_data[22]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="22" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[22]['user3']) &&  $sale_data[22]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border" ><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/22') }}" data-title="Kommentar">{{ @$sale_data[22]['comment'] }}</a></td>
        </tr>


        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td"></td>
        </tr>
        <tr>
          <td class="checkbox-td"></td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="checkbox-td">&nbsp;</td>
          <td class="">&nbsp;</td>
        </tr>
        </tbody>
      </table>
    </div>

    @if(Auth::user()->email!=config('users.falk_email'))
        <!-- <tr>
          <td colspan="7" class="text-center"><button style="color: #fff;width: 180px;font-size: 16px;" type="button" data-id="send-mail" class="btn {{$button_class2}} send-mail-confirm">{{$rbutton_title2}}</button></td>
        </tr> -->
        @endif

        @if(Auth::user()->email==config('users.falk_email'))
        <!-- <tr>
          <td colspan="7" class="text-center"><button style="color: #fff;width: 170px;font-size: 16px;" type="button" class="btn {{$button_class1}} release-property" data-toggle="modal" data-target="#release-property">{{$rbutton_title1}}</button></td>
        </tr> -->
        @endif




    <h3 class="title-tag" style="width: 60%;float: left;">4) Generelle Entscheidung (Falk Raudies)
        @if(Auth::user()->email!=config('users.falk_email') && Auth::user()->email!="a.lauterbach@fcr-immobilien.de" && Auth::user()->email!="a.raudies@fcr-immobilien.de")
        <button style="color: #fff;width: 190px;font-size: 12px;margin-left:
        10px;" data-status="{{$btn7}}" type="button" data-id="btn4_request" class=" pull-right btn
        {{$button_class7}}">@if($sale_date && $released_by)
        Freigegeben  @else {{$rbutton_title4}} @endif</button>
        @endif

        @if(Auth::user()->email==config('users.falk_email'))
          <button style="color: #fff;width: 190px;font-size: 12px;margin-left: 10px;" type="button" data-status="1"  class="pull-right btn @if($sale_date && $released_by) btn-success @else btn-primary @endif  release-property" data-toggle="modal" data-target="#release-property" >@if($sale_date && $released_by) Freigegeben  @else Freigeben @endif</button>
        @endif
      </h3>
      <h3 style="width: 35%;float: left;margin-left: 5px;">{{$r4}}</h3>
      <div class="clearfix"></div>
    @if(false && $sale_date && $released_by)
    <span style="float: left;width: 39%;margin-top: 10px;padding-left: 20px;">Freigegeben durch: {{ $released_by }}<br>
    Datum: {{ $sale_date }}</span>
    @endif

    <div class="clearfix"></div>


    <h3 style="float: left;width: 60%;" class="title-tag">5) Checkliste nach Verkauf (Verkäufer)
      <button style="color: #fff;width: 190px;font-size: 12px;margin-left: 10px;" type="button" data-status="1" class="pull-right btn {{$button_class9}}" data-id="btn5_request">{{$rbutton_title5}}</button>

      <a href="javascript:void(0)" data-class="ver-sheet-5" class="btn btn-info show-link-einkauf  pull-right" ><i class="fa fa-angle-down"></i></a>


    </h3>
    <h3 style="width: 35%;float: left;margin-left: 5px;">{{$r5}}</h3>
    <div class="clearfix"></div>

    <div class="ver-sheet-5 hidden">
      <table class="forecast-table">
        <thead>
        <tr>
          <th class="border checkbox-td bg-gray" colspan="3" >Notar/Kaufvertrag</th>
          <th class="border text-center bg-gray">geprüft durch</th>
          <th class="border text-center bg-gray">Kommentar</th>
        </tr>
      </thead>
        <tbody>
          <tr>
          <td class="border checkbox-td text-center"><input type="checkbox" class="checkbox-verkauf" data-type="19" @if(isset($sale_data[19]['status']) &&  $sale_data[19]['status']) checked @endif ></td>
          <td class="border" colspan="2">Nach Signing Kaufvertrag als PDF im Dokumentenordner ablegen</td>
          <td class="border checkbox-td">
              <select class="change-v-user" data-type="19" column-name="user1">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[19]['user1']) &&  $sale_data[19]['user1']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select>
          </td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="19" column-name="user2">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[19]['user2']) &&  $sale_data[19]['user2']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border checkbox-td">
            <select class="change-v-user" data-type="19" column-name="user3">
                <option value=""> </option>
                @foreach($tr_users as $list)
                @if(isset($sale_data) && isset($sale_data[19]['user3']) &&  $sale_data[19]['user3']==$list->id)
                <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                @else
                <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                @endif
                @endforeach
              </select></td>
          <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/19') }}" data-title="Kommentar">{{ @$sale_data[19]['comment'] }}</a></td>
        </tr>
        </tbody>
      </table>
    </div>

    
    <?php
    $j = 103;

    if(@$sale_data[$j]['comment'])
      $sale_data[$j]['comment'] = date('d.m.Y',strtotime($sale_data[$j]['comment']));

    ?>
    <br>
    Exklusivität bis: <a href="#" class="inline-edit date-c" data-type="text" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/'.$j) }}" data-inputclass="mask-input-new-date" data-title="Exklusivität bis">{{ @$sale_data[$j]['comment'] }}</a><br><br>
    <?php
    $j = 101;
    if(@$sale_data[$j]['comment'])
      $sale_data[$j]['comment'] = date('d.m.Y',strtotime($sale_data[$j]['comment']));

    ?>
    Notartermin: <a href="#" class="inline-edit date-c" data-type="text" data-inputclass="mask-input-new-date" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/'.$j) }}" data-title="Notartermin">{{ @$sale_data[$j]['comment'] }}</a><br><br>
    <?php
    $j = 102;
    if(@$sale_data[$j]['comment'])
      $sale_data[$j]['comment'] = date('d.m.Y',strtotime($sale_data[$j]['comment']));

    ?>
    BNL Termin: <a href="#" class="inline-edit date-c" data-type="text" data-inputclass="mask-input-new-date" data-pk="comment" data-url="{{url('verkauf-item/update/'.$id.'/'.$j) }}" data-title="B&L Termin">{{ @$sale_data[$j]['comment'] }}</a>


    <h3 style="margin-top: 40px;">Verlauf</h3>
  <table class="forecast-table" >
    <thead>
      <tr>
        <th class="border bg-gray">Name</th>
        <th class="border bg-gray">Button</th>
        <th class="border bg-gray">Kommentar</th>
        <th class="border bg-gray">Datum</th>
        <th class="border bg-gray">Action</th>
      </tr>
    </thead>
    <tbody>
      
      @php
        $user = Auth::user();
      @endphp

      @if(1==2)
      @foreach($buy_data2 as $k=>$list)
      
      <?php
      $tname = "";
      $u = DB::table('users')->where('id', $list['user1'])->first();
      if($u)
        $tname = $u->name;

      if($list['type']=="vbtn1_request")
        $rbutton_title = "Verkauf Zur Freigabe senden";
      else if($list['type']=="vbtn1")
        $rbutton_title = "Verkauf Freigeben";
      else{
        $rbutton_title = "";
      }

      ?>
      <tr>
        <td class="border">{{$tname}}</td>
        <td class="border">{{$rbutton_title}}</td>
        <td class="border">{{$list['comment']}}</td>
        <td class="border">{{date('d.m.Y H:i:s',strtotime($list['created_at']))}}</td>
        <td class="border">
          @if($user->email == config('users.falk_email') && $list['user1'] == $user->id)
            <button type="button" data-id="{{ $list['id'] }}" data-tbl="properties_buy_details" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>
          @endif
        </td>
      </tr>
      @endforeach
      @endif

      @foreach($sale_data1 as $k=>$list)
      
      <?php
      $tname = "";
      $u = DB::table('users')->where('id', $list['user1'])->first();
      if($u)
        $tname = $u->name;

      if($list['type']=="btn1_request")
      $rbutton_title = "1) Verkauf Zur Freigabe an Andrea gesendet";
      else if($list['type']=="btn2_request")
      $rbutton_title = "2) Verkauf Zur Freigabe an Alex gesendet";
      else if($list['type']=="btn3_request")
      $rbutton_title = "3) Verkauf Zur Freigabe an Andrea gesendet";
      else if($list['type']=="btn4_request")
      $rbutton_title = "4) Verkauf Zur Freigabe an Falk gesendet";
      else if($list['type']=="btn5_request")
      $rbutton_title = "5) Verkauf Als erledigt markieren";
      else if($list['type']=="vbtn1_request")
        $rbutton_title = "Verkauf Zur Freigabe gesendet";
      else if($list['type']=="vbtn1")
        $rbutton_title = "Verkauf Freigeben";
      else{
        $rbutton_title = str_replace('btn', '', $list['type']).") Freigeben";
      }





      ?>
      <tr>
        <td class="border">{{$tname}}</td>
        <td class="border">{{$rbutton_title}}</td>
        <td class="border">{{$list['comment']}}</td>
        <td class="border">{{date('d.m.Y H:i:s',strtotime($list['created_at']))}}</td>
        <td class="border">
          @if($user->email == config('users.falk_email') && $list['user1'] == $user->id)
            <button type="button" data-id="{{ $list['id'] }}" data-tbl="properties_sale_details" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  </div>

  

</div>




    

<!-- Modal -->
<div class="modal fade" id="release-property" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="modal-body">
          <label>Kommentar</label>
          <textarea class="form-control v-ver-release-comment" name="message"></textarea>
          <br>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary release-the-property" data-dismiss="modal" >Senden</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>
<div class="modal fade" id="v-request-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <label>Kommentar</label>
        <textarea class="form-control v-request-comment" name="message"></textarea>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary v-request-confirm" data-dismiss="modal" >Senden</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ver-irelease-property" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <!-- <h4>Möchtest du dieses Objekt wirklich freigeben?</h4> -->
        <label>Kommentar</label>
        <textarea class="form-control f-comment" name="message"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary ver-irelease-the-property" data-dismiss="modal" >Senden</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="ver-release-property-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <label>Kommentar</label>
        <textarea class="form-control ver-release-comment" name="message"></textarea>
        <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary ver-release-procedure-request" data-dismiss="modal" >Senden</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="ver-irelease-property2" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <h4>Möchtest du dieses Objekt wirklich freigeben?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary ver-irelease-the-property2" data-dismiss="modal" >Ja</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

@section('js')

 @parent
   <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
  <script type="text/javascript">
 
$(document).ready(function() {

  step = d_status = "";
  $('body').on('click','.ver-release-procedure-1',function(){
      step = $(this).attr('data-id');
      $('#ver-release-property-modal').modal();
  })

  $('body').on('click','.ver-release-procedure-request',function(){
    property_id = $('.property_id').val();
    comment = $('.ver-release-comment').val();
    $.ajax({
          type : 'POST',
          url : "{{url('property/verkaufreleaseprocedure') }}",
          data : {property_id:property_id,step:step,  _token : '{{ csrf_token() }}',comment:comment },
          success : function (data) {
              // alert(data);
              var path = $('#path-properties-show').val();
              window.location.href = path + '?tab=verkauf_tab';
          }
      });
  });

  and = "{{$andrea}}";
  alex = "{{$alex}}";
  $('body').on('click','.ver-release-procedure',function(){
    id = $(this).attr('data-id');
    property_id = $('.property_id').val();
    status = $(this).attr('data-status');
    // alert(id);


    // alert($(this).hasClass('btn-success'));
    if((status=="1" && !$(this).hasClass('btn-success')) || ($(this).hasClass('btn-success') && id=='btn1_request') || ($(this).hasClass('btn-success') && id=='btn2_request') || ($(this).hasClass('btn-success') && id=='btn3_request') || ($(this).hasClass('btn-success') && id=='btn4_request') )
    {

      step = $(this).attr('data-id');
      $('#ver-release-property-modal').modal();
    }
    
  })


  $('.ver-irelease-the-property').click(function(){
    property_id = $('.property_id').val();
    comment = $('.f-comment').val();
    $.ajax({
        type : 'POST',
        url : "{{url('property/einkaufsendmail') }}",
        data : {property_id:property_id,release:1,  _token : '{{ csrf_token() }}',comment:comment },
        success : function (data) {
            // alert(data);
            var path = $('#path-properties-show').val();
            window.location.href = path + '?tab=verkauf_tab';
        }
    });
  })



  $('.send-mail-confirm').click(function(){
    $('#v-request-modal').modal();
  })
  
  $('.v-request-confirm').click(function(){
    property_id = $('.property_id').val();
    $.ajax({
        type : 'POST',
        url : "{{url('property/sendmail') }}",
        data : {property_id:property_id,step:'vbtn1_request',  _token : '{{ csrf_token() }}',comment:$('.v-request-comment').val() },
        success : function (data) {
            alert(data);
            window.location.href = path + '?tab=verkauf_tab';
        }
    });
  })

  $('.release-the-property').click(function(){
    property_id = $('.property_id').val();
    $.ajax({
        type : 'POST',
        url : "{{url('property/sendmail') }}",
        data : {property_id:property_id,release:1,step:'vbtn1',  _token : '{{ csrf_token() }}',comment:$('.v-ver-release-comment').val() },
        success : function (data) {
            // alert(data);
            var path = $('#path-properties-show').val();
            window.location.href = path + '?tab=verkauf_tab';
        }
    });
  })

  


  $.fn.editable.defaults.ajaxOptions = {type: "POST"};
  
    $('.preisverk').editable({
        success: function(response, newValue) {
        }
    });

    $('.sale-sheet').find('a.inline-edit').editable({
        success: function(response, newValue) {
        }
    });
    
    $('.date-c').editable({
        success: function(response, newValue) {
        }
    });


$('.Verkauf_tab_editable').find('a.inline-edit').editable({

success: function(response, newValue) {
   //location.reload(true);
  ajax_verkauf_tab_load();

    }
});

  

 });     

 
$('.checkbox-verkauf').on('click', function () {
    var type = $(this).attr('data-type');
    v = 0;
    if($(this).is(':checked'))
        v = 1;

    property_id = $('.property_id').val();

    $.ajax({
        type : 'POST',
        url : "{{url('verkauf-item/update') }}/"+property_id+'/'+type,
        data : {type:type,pk : 'status',value:v, property_id:property_id,  _token : '{{ csrf_token() }}' },
        success : function (data) {
            generate_option_from_json(data, 'country_to_state');
        }
    });
});

$('.change-v-user').on('change', function () {
    type = $(this).attr('data-type');
    column = $(this).attr('column-name')
    v = $(this).val();
    property_id = $('.property_id').val();
    $.ajax({
        type : 'POST',
        url : "{{url('verkauf-item/update') }}/"+property_id+'/'+type,
        data : {type:type,pk : column,value:v, property_id:property_id,  _token : '{{ csrf_token() }}' },
        success : function (data) {
            generate_option_from_json(data, 'country_to_state');
        }
    });
});

$('.change-l-user').on('change', function () {
    column = 'saller_id';
    v = $(this).val();
    property_id = $('.property_id').val();

    $.ajax({
        type : 'POST',
        url : "{{url('verkauf/update') }}/"+property_id,
        data : {pk : column,value:v, property_id:property_id,  _token : '{{ csrf_token() }}' },
        success : function (data) {
            generate_option_from_json(data, 'country_to_state');
        }
    });
});


 function ajax_verkauf_tab_load(){
$.ajax({ 
          url : "{{ url('property/ajax_verkauf_tab_load').'/'.$id.'/'.$G59 }}",
          context: document.body,
          success: function(response){
  
          $('#ajax_tab_loader').html('');
          var ajax_tab_loader_new = $('#ajax_tab_loader').html(response);   
          ajax_tab_loader_new.find('a.inline-edit').editable({
            success: function(response, newValue) {
              //location.reload(true);
              ajax_verkauf_tab_load();

              }
            });
        }
});
}


 $('#month_select').on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;

    $.ajax({
        type : 'POST',
        url : "{{url('property/update_month_verkauf_tab').'/'.$id }}",
        data : {monat : valueSelected, _token : '{{ csrf_token() }}' },
        success : function (data) {
             console.log(data);
        }
    });
   
});

 


</script>
@endsection

 
     
   
