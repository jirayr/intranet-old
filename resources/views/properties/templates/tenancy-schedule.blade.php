@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <style type="text/css">
        .checkbox-td{
            width: 30px !important;
            min-width: auto !important;
        }
    </style>
@endsection
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>

<div id="tenancy-schedule" class="mieterliste_tab">
    <!-- <h1>{{__('tenancy_schedule.title')}}</h1> -->
    <div class="btn-group" style="float: right">
        {{--<input type="hidden" id="tenancy-schedule-zoom-value" value="">--}}
        <button type="button" class="btn btn-primary" id="tenacy-schedule-zoom-out"><i class="fa fa-search-minus"></i></button>
        <button class="btn btn-default" disabled><span id="tenancy-schedule-zoom-value"></span>&nbsp;%</button>
        <button type="button" class="btn btn-primary" id="tenacy-schedule-zoom-in"><i class="fa fa-search-plus"></i></button>
    </div>
    <ul class="nav nav-tabs">
        <?php $data = \App\RentPaidExcelUpload::where('property_id',$id)->orderBy('id','desc')->first()  ?>

        <?php $flag = 0;?>
        @foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
        <li @if($tenancy_schedule_data['selecting_tenancy_schedule'] == $tenancy_schedule->id) class="active" @endif>
          <?php $flag = 1;?>

            <a class="" data-toggle="tab" href="#tenancy-schedule-{{$tenancy_schedule->id}}">
                <!-- {{$tenancy_schedule->object ? $tenancy_schedule->object: "Empty"}} -->
                {{__('tenancy_schedule.title')}}
                <!-- <span data-id="{{$tenancy_schedule->id}}" class="">X</span> -->
            </a>
        </li>
        @endforeach
        @if($flag==0)
        <li class="">
            <form action="{{route("tenancy-schedules.create")}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="property_id" value="{{$id}}">
                <button type="submit" class="btn btn-success btn-sm" style="margin-bottom: -20px">{{__('tenancy_schedule.add_tenancy_schedule')}}</button>
            </form>
        </li>
        @endif
        <li @if(request()->get('selecting_tenancy_schedule') == 'rent_paid') class="active" @endif>
            <a data-toggle="tab" href="#rent_paid">OP Liste</a>
        </li>
    </ul>

    <div class="tab-content">
        <?php
            $leerstand_total = 0;
            $vermietet_total = 0;
            $sum_actual_net_rent = $sum_total_amount = 0;
            $wault = 0;
        ?>
        <script>
            $(function(){
                $(".wmd-view-topscroll").scroll(function(){
                    $(".wmd-view").scrollLeft($(".wmd-view-topscroll").scrollLeft());
                });
                $(".wmd-view").scroll(function(){
                    $(".wmd-view-topscroll").scrollLeft($(".wmd-view").scrollLeft());
                });
            });
        </script>

        <button class="btn btn-primary btn-fullscreen" onclick="openFullscreen();">Fullscreen</button>

        <div style="height: 20px; width: auto;overflow-x: scroll;overflow-y: hidden; padding-left: 60%" class="wmd-view-topscroll">
            <div style="width: 1000px;height:20px; overflow-x: scroll;overflow-y: hidden;" class="scroll-div1"></div>
        </div>

        <div style="height: 20px; width: auto; padding-left: 100%" class="wmd-view-topscroll">
            <div style="width: 2000px;height:20px;" class="scroll-div1"></div>
        </div>

        @foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)

            <style>
                thead th:nth-child(1) {/*first cell in the header*/
                    position: relative;
                }
                .seprate{
                    position: relative;
                    /*display: block; !*seperates the first cell in the header from the header*!*/
                }
                .seprate-blue{
                    position: relative;
                    /*display: block; !*seperates the first cell in the header from the header*!*/
                }
                .header{
                    color: black;
                }

                .header.active {
                    background: white;
                    -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
                    -moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
                    box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
                }
                .seprate.active{
                    background: white;

                }
                .gewerbe{
                    position: relative;
                }
            </style>

            <div id="tenancy-schedule-{{$tenancy_schedule->id}}" class="tab-pane fade @if($tenancy_schedule_data['selecting_tenancy_schedule'] == $tenancy_schedule->id) {{'in active'}} @endif ">

                

                <div class="row">
                  <div class="col-md-1">
                    <a style="margin-bottom: 10px;" href="{{route('properties.show', $id)}}?tab=tenancy-schedule&selecting_tenancy_schedule={{$tenancy_schedule->id}}" class="btn btn-primary">Reload</a>
                  </div>
                  <div class="col-md-3 form-group">
                      <input type="text" name="search" id="input-search-tenancy-schedule" placeholder="Name" class="form-control">
                  </div>
                  {{-- <div class="col-md-8">
                      <button type="button" class="btn btn-success" id="btn-search-tenancy-schedule">Suchen</button>
                  </div> --}}
                </div>


                <div id="scroll2" style="padding-bottom: 131px; width: 1655px; height: 1000px;overflow-x: scroll;overflow-y: scroll;" class="wmd-view  tenancy-schedules" >

                    <?php
                        $category_list = array("Nicht zugeordnet","Lebensmittel","Fashion","Hotel","Baumarkt","Drogerie","Sonderposten","Elektro / Spielwaren","Dienstleistung","Gewerbe","Gastro / Café","Logistik","Wohnen","Sonstiges");
                    ?>

                    <table class="forecast-table" style="border: none" id="tenancy-schedule-table">
                        <thead style="position: relative;display: block;">
                            <tr style="border: none!important;">
                                <th class="bg-brown border text header" style="color: white">Mieterliste</th>
                                <td class="border checkbox-td">&nbsp;</td>
                                <td class="border checkbox-td">&nbsp;</td>
                                <th class="color-red border">Objekt</th>
                                <th class="border"></th>
                                <th colspan="2" class="border text">
                                    <?php
                                        $name_of_property = "";
                                        if($propertiescheckd){
                                            $name_of_property = $propertiescheckd->name_of_property;
                                        }
                                    ?>
                                    {{ $name_of_property }}
                                </th>
                                <th class="border"></th>
                                <th class="color-red border-no-right">Aktualisierung</th>
                                <th class="text-right border-no-left">{{date('d/m/Y', strtotime($tenancy_schedule->updated_at))}}</th>
                                <th colspan="2" class="border"></th>
                                <th style="min-width: 215px;" class="color-red border-no-right">Ersteller</th>
                                <th class="border-no-left">{{$tenancy_schedule->creator}}</th>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                        </thead>
                        <tbody style="position: relative; display: block;width: 2000px; height: auto;  overflow-x: scroll">
                            <tr>
                                <td colspan="17">&nbsp;</td>
                            </tr>
                            <tr>
                               <th class=" bg-light-blue border seprate">Wohnen</th>
                               
                               <th class=" bg-red border checkbox-td">Mieter geschlossen</th>
                               <th class=" bg-red border">Mietzahlung</th>
                               <th class="text-center bg-light-blue border gewerbe test mietvertrag">Mietvertrag</th>

                               <th class="text-center bg-light-blue border gewerbe test">Mietbeginn</th>
                               <th class="text-center bg-light-blue border gewerbe test">Mietende</th>

                               <th class="text-center bg-light-blue border gewerbe test">Kündigung bis zum</th>

                               <th class="text-center bg-light-blue border gewerbe test">Mietfläche/m²</th>
                               <th class="text-center bg-light-blue border gewerbe test">Miete/m²</th>
                               <th class="text-center bg-light-blue border gewerbe test">IST-Nettokaltmiete p.m.</th>
                               <th class="text-center bg-light-blue border gewerbe test">IST-Nettokaltmiete p.a.</th>
                               <th class="text-center bg-light-blue border gewerbe test">NK netto p.m.</th>

                               <th class="text-center bg-light-blue border gewerbe test year" colspan="4">2017</th>
                               <th class="text-center bg-light-blue border gewerbe test year" colspan="4">2018</th>
                               <th class="text-center bg-light-blue border gewerbe test year" colspan="4">2019</th>
                               <th class="text-center bg-light-blue border gewerbe test year" colspan="4">2020</th>

                               <th class="text-center bg-light-blue border gewerbe test">Gesamt Netto p.m.</th>
                               <th class="text-center bg-light-blue border gewerbe test">19% MwSt.</th>
                               <th class="text-center bg-light-blue border gewerbe test">Gesamt Brutto p.m.</th>
                               <th class="text-center bg-light-blue border gewerbe test"> Gesamt Brutto p.a. </th>
                               <th class="text-center bg-gray border gewerbe test">Restlaufzeit</th>
                               <th class="text-center bg-gray border gewerbe test">Restlaufzeit</th>

                               
                               <th class="text-center bg-light-blue border gewerbe test layout">Grundriss</th>
                               
                               <th class=" bg-light-blue border gewerbe test checkbox-td">Domos NR</th>

                               <th class="checkbox-td bg-light-blue border gewerbe test">Neu</th>
                               <th class="checkbox-td text-center bg-light-blue border gewerbe test">Asset<br>Manager</th>
                               <th class="text-center bg-light-blue border gewerbe test">Abschluss MV</th>
                               <th class="text-center bg-light-blue border gewerbe test">Sonderkündigunsrecht</th>
                               <th class="text-center bg-light-blue border gewerbe test">Optionen</th>
                               <th class="text-center bg-light-blue border gewerbe test">Nutzung</th>
                               <th class="text-center bg-light-blue border gewerbe test">Kategorie</th>
                               <th class="text-center bg-light-blue border gewerbe test">Mahnung</th>
                               <th class="text-center bg-light-blue border gewerbe test">Indexierung</th>
                               <th class="text-center bg-light-blue border gewerbe test">Kaution</th>
                               <th class="text-center bg-gray border gewerbe test">Leerstand</th>
                               <th class="text-center bg-gray border gewerbe test">Leerstand</th>
                               <th class="text-center bg-gray border gewerbe test">Leerstand</th>
                               <th class="text-center bg-gray border gewerbe test">Leerstand seit</th>
                               <th class="text-center bg-gray border checkbox-td gewerbe test">Leerstand</th>
                               {{-- <th class="text-center bg-gray border gewerbe test">IS Upload</th> --}}
                               <th class="text-center border gewerbe test">Kommentar</th>
                               <th class="text-center border gewerbe test">Kommentar extern</th>
                            </tr>
                            {{--row 5--}}
                            <tr>
                               <th class=" bg-light-blue border seprate"></th>
                               
                               <th class=" bg-red border seprate checkbox-td"></th>
                               <th class=" bg-red border seprate"></th>

                               <th class="text-center bg-light-blue border gewerbe test mietvertrag"></th>

                               <th class="text-center bg-light-blue border"></th>
                               <th class="text-center bg-light-blue border"></th>

                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>

                               <th class="text-center bg-light-blue border year">Datum BKA</th>
                               <th class="text-center bg-light-blue border year">Betrag BKA</th>
                               <th class="text-center bg-light-blue border year">Bezahldatum</th>
                               <th class="text-center bg-light-blue border year">Datei</th>

                               <th class="text-center bg-light-blue border year">Datum BKA</th>
                               <th class="text-center bg-light-blue border year">Betrag BKA</th>
                               <th class="text-center bg-light-blue border year">Bezahldatum</th>
                               <th class="text-center bg-light-blue border year">Datei</th>

                               <th class="text-center bg-light-blue border year">Datum BKA</th>
                               <th class="text-center bg-light-blue border year">Betrag BKA</th>
                               <th class="text-center bg-light-blue border year">Bezahldatum</th>
                               <th class="text-center bg-light-blue border year">Datei</th>

                               <th class="text-center bg-light-blue border year">Datum BKA</th>
                               <th class="text-center bg-light-blue border year">Betrag BKA</th>
                               <th class="text-center bg-light-blue border year">Bezahldatum</th>
                               <th class="text-center bg-light-blue border year">Datei</th>



                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="border"></th>
                               <th class="border"></th>
                               
                               <th class="text-center bg-light-blue border gewerbe test layout"></th>
                               <th class="bg-light-blue border gewerbe test checkbox-td"></th>
                               <th class="checkbox-td bg-light-blue border"></th>
                               <th class="checkbox-td bg-light-blue border"></th>
                               <th class="text-center bg-light-blue border"></th>
                               <th class="text-center bg-light-blue border"></th>
                               <th class="text-center bg-light-blue border"></th>
                               <th class="text-center bg-light-blue border"></th>
                               <th class="text-center bg-light-blue border"></th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-light-blue border">Wohnen</th>
                               <th class="text-center bg-gray border">in qm</th>
                               <th class="text-center bg-gray border">EUR</th>
                               <th class="text-center bg-gray border">bewertet AM</th>
                               <th class="text-center bg-gray border"></th>
                               <th class="text-center bg-gray border checkbox-td">bei Ankauf</th>
                               {{-- <th class="text-center bg-gray border"></th> --}}
                               <th class="text-center border"></th>
                               <th class="text-center border"></th>
                            </tr>
                            {{--row 6--}}

                           <?php
                                $csum = $csum1 = $csum2 = $csum3 = 0;
                                $td1 = array('Nein','Ja');
                                $td2 = array(1=>'1 (keine Probleme)',2=>'2 (evtl. Probleme)',3=>'3 (Mieter zahlt nicht)');

                                $subject = 'Mieterliste: '.$properties->name_of_property;

                                $taburl = route('properties.show',['property' => $properties->id]).'?tab=tenancy-schedule';
                                $taburl = "<a href='".$taburl."'>".$taburl."</a>";
                           ?>

                            @foreach($tenancy_schedule->items as $item)
                                @if($item->type == config('tenancy_schedule.item_type.live'))
                                    <tr class="item-tr @if($item->rent_begin != '' && $item->rent_begin > date('Y-m-d')) item-futureactive @elseif($item->rent_end != '' && $item->rent_end < date('Y-m-d')) item-pastactive @endif">
                                        <th class="border seprate">
                                            <form action="{{url("tenancy-schedules/delete/$tenancy_schedule->id/item/$item->id")}}" method="POST" style="display: inline">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn-sm" style="color:white" onclick="return confirm('Are you sure?')">{{__('forecast.delete')}}</button>
                                            </form>
                                            <button type="button" class="btn btn-primary btn-xs new-date" style="color:white" data-toggle="modal" data-target=".mydateModal" data-id="{{$item->id}}" >Verl.</button>
                                            <span style="padding-left: 10px"></span>
                                            <a href="#" class="inline-edit" data-type="text" data-pk="name" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->name }}</a>
                                        </th>
                                        <td class="border checkbox-td">
                                            <select class="change-asset-user" data-pk="tenant_closed" data-id="{{$item->id}}">
                                                @foreach($td1 as $k=>$list)
                                                    @if($item->tenant_closed==$k)
                                                        <option selected="selected" value="{{$k}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$k}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>

                                        <td class="border checkbox-td">
                                            <select class="change-asset-user" data-pk="rent_payment" data-id="{{$item->id}}">
                                                @foreach($td2 as $list)
                                                    @if($item->rent_payment==$list)
                                                        <option selected="selected" value="{{$list}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$list}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>

                                        <th class="border mietvertrag text-center">
                                            
                                          <a href="javascript:void(0);" class="link-button-pdf-mieterliste"  data-id="{{$item->id}}" data-file_column_type="0">
                                            <i class="fa fa-link"></i>
                                          </a>

                                           @foreach($item->files()->get() as $file)
                                                    @if($file['file_column_type']==0)
                                                    @if($file['type']=='file')
                                                    &nbsp;&nbsp;<a href="{{ $file['file_href'] }}"  id="mieterliste-gdrive-link-{{$file['id']}}"  target="_blank" title="{{ $file['file_name'] }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $file['file_extension']) ?: 'fa-file' }}" ></i></a>
                                                    @else
                                                        <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  ><i class="fa fa-folder" ></i></a>
                                                    @endif
                                                    @endif
                                           @endforeach
                                            <a class="listFilesForDelete" data-property_id="" data-id="{{ $item->id }}" data-type="mieterliste"><i class="fa fa-trash iconstyle"></i></a>
                                        </th>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="rent_begin" data-placement="bottom" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-value="@if($item->rent_begin){{show_date_format($item->rent_begin)}}@endif">{{ show_date_format($item->rent_begin) }}</a>
                                        </td>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="rent_end" data-placement="bottom" data-inputclass="mask-input-new-date" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-value="@if($item->rent_end){{show_date_format($item->rent_end)}}@endif">{{ show_date_format($item->rent_end) }}</a>
                                        </td>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="termination_by" data-placement="bottom" data-inputclass="mask-input-new-date" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->termination_by)}}">{{ show_date_format($item->termination_by) }}</a>
                                        </td>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="number" data-step="0.01" data-pk="rental_space" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->rental_space, 2,",",".") }}</a>
                                        </td>

                                        <td class="border text-right">
                                            @if($item->rental_space && $item->actual_net_rent)
                                                {{ number_format($item->actual_net_rent/$item->rental_space, 2,",",".") }}
                                            @else
                                                {{ number_format(0, 2,",",".") }}
                                            @endif
                                        </td>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="actual_net_rent" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->actual_net_rent, 2,",",".") }}</a>&nbsp;€
                                        </td>

                                        <td class="border text-right">
                                            @if($item->actual_net_rent)
                                                {{ number_format(12*$item->actual_net_rent, 2,",",".") }}
                                            @else
                                                {{ number_format(0, 2,",",".") }}
                                            @endif
                                        </td>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="nk_netto" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->nk_netto, 2,",",".") }}</a>&nbsp;€
                                        </td>



                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="bka_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2017') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->bka_date_2017)}}">{{ show_date_format($item->bka_date_2017) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-number-input-negetive" data-pk="amount" data-url="{{url('tenancy-payment/update/'.$item->id.'/2017') }}" data-title="">{{ show_number($item->amount_2017, 2) }}</a>

                                        </td>
                                        <td class="border text-right year"><a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="payment_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2017') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->payment_date_2017)}}">{{ show_date_format($item->payment_date_2017) }}</a></td>

                                        <td class="border text-center datei_2017 datei year">

                                          <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{$item->id}}" data-property-id="{{$id}}" data-year="2017" id="datei_2017_{{ $item->id }}">
                                            <i class="fa fa-link"></i>
                                          </a>

                                          @if($item->tenant_payments_2017 && $item->tenant_payments_2017->file_name)

                                            <?php

                                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($item->tenant_payments_2017->file_basename) ? $item->tenant_payments_2017->file_basename : '');
                                              if($item->tenant_payments_2017->file_type == 'file'){
                                                $download_path = "https://drive.google.com/file/d/".(isset($item->tenant_payments_2017->file_basename) ? $item->tenant_payments_2017->file_basename : '');

                                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($item->tenant_payments_2017->file_basename) ? $item->tenant_payments_2017->file_basename : '').'&file_dir='.(isset($item->tenant_payments_2017->file_dirname) ? $item->tenant_payments_2017->file_dirname : '');
                                              }
                                            ?>
                                            <a href="{{ $download_path }}"  target="_blank" title="{{ $item->tenant_payments_2017->file_name }}"><i extension="filemanager.file_icon_array.{{$file['file_extension']}}" class="fa fa-file" ></i></a>

                                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $item->tenant_payments_2017->id]) }}"><i class="fa fa-trash"></i></a>

                                          @endif

                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="bka_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2018') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->bka_date_2018)}}">{{ show_date_format($item->bka_date_2018) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-number-input-negetive" data-pk="amount" data-url="{{url('tenancy-payment/update/'.$item->id.'/2018') }}" data-title="">{{ show_number($item->amount_2018, 2) }}</a>

                                        </td>
                                        <td class="border text-right year"><a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="payment_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2018') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->payment_date_2018)}}">{{ show_date_format($item->payment_date_2018) }}</a></td>

                                        <td class="border text-center datei_2018 datei year">

                                          <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{$item->id}}" data-property-id="{{$id}}" data-year="2018" id="datei_2018_{{ $item->id }}">
                                            <i class="fa fa-link"></i>
                                          </a>

                                          @if($item->tenant_payments_2018 && $item->tenant_payments_2018->file_name)

                                            <?php

                                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($item->tenant_payments_2018->file_basename) ? $item->tenant_payments_2018->file_basename : '');
                                              if($item->tenant_payments_2018->file_type == 'file'){
                                                $download_path = "https://drive.google.com/file/d/".(isset($item->tenant_payments_2018->file_basename) ? $item->tenant_payments_2018->file_basename : '');

                                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($item->tenant_payments_2018->file_basename) ? $item->tenant_payments_2018->file_basename : '').'&file_dir='.(isset($item->tenant_payments_2018->file_dirname) ? $item->tenant_payments_2018->file_dirname : '');
                                              }
                                            ?>
                                            <a href="{{ $download_path }}"  target="_blank" title="{{ $item->tenant_payments_2018->file_name }}"><i extension="filemanager.file_icon_array.{{$file['file_extension']}}" class="fa fa-file" ></i></a>

                                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $item->tenant_payments_2018->id]) }}"><i class="fa fa-trash"></i></a>

                                          @endif

                                        </td>

                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="bka_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2019') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->bka_date_2019)}}">{{ show_date_format($item->bka_date_2019) }}</a></td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-number-input-negetive" data-pk="amount" data-url="{{url('tenancy-payment/update/'.$item->id.'/2019') }}" data-title="">{{ show_number($item->amount_2019, 2) }}</a>
                                        </td>
                                        <td class="border text-right year"><a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="payment_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2019') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->payment_date_2019)}}">{{ show_date_format($item->payment_date_2019) }}</a></td>

                                        <td class="border text-center datei_2019 datei year">

                                          <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{$item->id}}" data-property-id="{{$id}}" data-year="2019" id="datei_2019_{{ $item->id }}">
                                            <i class="fa fa-link"></i>
                                          </a>

                                          @if($item->tenant_payments_2019 && $item->tenant_payments_2019->file_name)

                                            <?php

                                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($item->tenant_payments_2019->file_basename) ? $item->tenant_payments_2019->file_basename : '');
                                              if($item->tenant_payments_2019->file_type == 'file'){
                                                $download_path = "https://drive.google.com/file/d/".(isset($item->tenant_payments_2019->file_basename) ? $item->tenant_payments_2019->file_basename : '');

                                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($item->tenant_payments_2019->file_basename) ? $item->tenant_payments_2019->file_basename : '').'&file_dir='.(isset($item->tenant_payments_2019->file_dirname) ? $item->tenant_payments_2019->file_dirname : '');
                                              }
                                            ?>
                                            <a href="{{ $download_path }}"  target="_blank" title="{{ $item->tenant_payments_2019->file_name }}"><i extension="filemanager.file_icon_array.{{$file['file_extension']}}" class="fa fa-file" ></i></a>

                                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $item->tenant_payments_2019->id]) }}"><i class="fa fa-trash"></i></a>

                                          @endif

                                        </td>

                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="bka_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2020') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->bka_date_2020)}}">{{ show_date_format($item->bka_date_2020) }}</a></td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-number-input-negetive" data-pk="amount" data-url="{{url('tenancy-payment/update/'.$item->id.'/2020') }}" data-title="">{{ show_number($item->amount_2020, 2) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="payment_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2020') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->payment_date_2020)}}">{{ show_date_format($item->payment_date_2020) }}</a>
                                        </td>
                                        <td class="border text-center datei_2020 datei year">

                                          <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{$item->id}}" data-property-id="{{$id}}" data-year="2020" id="datei_2020_{{ $item->id }}">
                                            <i class="fa fa-link"></i>
                                          </a>

                                          @if($item->tenant_payments_2020 && $item->tenant_payments_2020->file_name)

                                            <?php

                                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($item->tenant_payments_2020->file_basename) ? $item->tenant_payments_2020->file_basename : '');
                                              if($item->tenant_payments_2020->file_type == 'file'){
                                                $download_path = "https://drive.google.com/file/d/".(isset($item->tenant_payments_2020->file_basename) ? $item->tenant_payments_2020->file_basename : '');

                                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($item->tenant_payments_2020->file_basename) ? $item->tenant_payments_2020->file_basename : '').'&file_dir='.(isset($item->tenant_payments_2020->file_dirname) ? $item->tenant_payments_2020->file_dirname : '');
                                              }
                                            ?>
                                            <a href="{{ $download_path }}"  target="_blank" title="{{ $item->tenant_payments_2020->file_name }}"><i extension="filemanager.file_icon_array.{{$file['file_extension']}}" class="fa fa-file" ></i></a>

                                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $item->tenant_payments_2020->id]) }}"><i class="fa fa-trash"></i></a>

                                          @endif

                                        </td>

                                        <?php
                                            $natto =$item->actual_net_rent+$item->nk_netto;
                                            $tn = ($item->actual_net_rent+$item->nk_netto)*19/100;

                                            $csum += $natto;
                                            $csum1 += $tn;
                                            $csum2 += ($natto+$tn);
                                            $csum3 += ($natto+$tn)*12;
                                        ?>

                                        <td class="border text-right">{{ number_format($natto, 2,",",".") }}&nbsp;€</td>
                                        <td class="border text-right">
                                        {{-- {{ number_format($tn, 2,",",".") }}&nbsp;€ --}}
                                        </td>
                                        <td class="border text-right">{{ number_format($natto+$tn, 2,",",".") }}&nbsp;€</td>
                                        <td class="border text-right">{{ number_format(($natto+$tn)*12, 2,",",".") }}&nbsp;€</td>
                                        <th class="border"></th>
                                        <th class="border"></th>
                                        
                                        <th class="border layout text-center">
                                            
                                          <a href="javascript:void(0);" class="link-button-pdf-mieterliste"  data-id="{{$item->id}}" data-file_column_type="1">
                                            <i class="fa fa-link"></i>
                                          </a>

                                           @foreach($item->files()->get() as $file)
                                                    @if($file['file_column_type']==1)
                                                    @if($file['type']=='file')
                                                    &nbsp;&nbsp;<a href="{{ $file['file_href'] }}"  id="mieterliste-gdrive-link-{{$file['id']}}"  target="_blank" title="{{ $file['file_name'] }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $file['file_extension']) ?: 'fa-file' }}" ></i></a>
                                                    @else
                                                        <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  ><i class="fa fa-folder" ></i></a>
                                                    @endif
                                                    @endif
                                           @endforeach
                                            <a class="listFilesForDelete" data-property_id="" data-id="{{ $item->id }}" data-type="mieterliste"><i class="fa fa-trash iconstyle"></i></a>
                                        </th>
                                        <td class="border checkbox-td">
                                          <a href="#" class="inline-edit" data-type="text" data-pk="opos_mieter" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->opos_mieter }}</a>
                                        </td>
                                        <td class="checkbox-td border text-center"><input type="checkbox" class="checkbox-is-new" data-id="{{$item->id}}" @if($item->is_new) checked @endif></td>
                                        <td class="border checkbox-td custom-asset-manager">
                                            <?php
                                                $asset_mn_id = "";
                                                if($item->asset_manager_id)
                                                    $asset_mn_id = $item->asset_manager_id;
                                            ?>
                                            <select class="change-asset-user" data-pk="asset_manager_id" data-id="{{$item->id}}">
                                                 <option value=""> </option>
                                                 @foreach($as_users as $list)
                                                 @if($asset_mn_id==$list->id)
                                                 <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                 @else
                                                 <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                 @endif
                                                 @endforeach
                                            </select>

                                            <?php
                                                $asset_mn_id2 = "";
                                                if($item->asset_manager_id2)
                                                    $asset_mn_id2 = $item->asset_manager_id2;
                                            ?>
                                            <select class="change-asset-user" data-pk="asset_manager_id2" data-id="{{$item->id}}">
                                                 <option value=""> </option>
                                                 @foreach($as_users as $list)
                                                 @if($asset_mn_id2==$list->id)
                                                 <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                 @else
                                                 <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                 @endif
                                                 @endforeach
                                            </select>
                                        </td>
                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="assesment_date" data-placement="bottom" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-value="@if($item->assesment_date){{show_date_format($item->assesment_date)}}@endif">{{ show_date_format($item->assesment_date) }}</a>
                                        </td>
                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text"  data-pk="termination_date" data-placement="bottom" data-inputclass="mask-input-new-date"        data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-value="@if($item->termination_date){{show_date_format($item->termination_date)}}@endif">{{ show_date_format($item->termination_date) }}</a>
                                        </td>
                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="options" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->options }}</a>
                                            <input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="0" @if($item->selected_option==0) checked @endif >
                                            <br>
                                            <input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="1" @if($item->selected_option==1) checked @endif >
                                            <br>
                                            <input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="2" @if($item->selected_option==2) checked @endif >
                                        </td>
                                        <td class="border">
                                            <a href="#" class="inline-edit-use" data-type="select" data-pk="use" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->use }}</a>
                                        </td>


                                        <td class="border">
                                            <select class="change-asset-user" data-pk="category" data-id="{{$item->id}}">
                                                @foreach($category_list as $list)
                                                    @if($item->category==$list)
                                                        <option selected="selected" value="{{$list}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$list}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>

                                        <td class="border">
                                            <span class="title-warning">1. Mahnung</span>
                                            <a href="#" class="inline-edit warning_date" data-type="date" data-placement="bottom"  data-pk="warning_date1" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->warning_date1 }}</a>
                                            <input type="checkbox" class="warning-price-check" data-id="{{ $item->id }}" data-pk="warning_price1" value="1" @if($item->warning_price1==1) checked @endif >
                                            <br>
                                            <span class="title-warning">2. Mahnung</span>
                                            <a href="#" class="inline-edit warning_date" data-type="date" data-placement="bottom"  data-pk="warning_date2" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->warning_date2 }}</a>
                                            <input type="checkbox" class="warning-price-check" data-id="{{ $item->id }}" data-pk="warning_price2" value="1" @if($item->warning_price2==1) checked @endif >
                                            <br>
                                            <span class="title-warning">Mahnbescheid</span>
                                            <a href="#" class="inline-edit warning_date" data-type="date" data-placement="bottom"  data-pk="warning_date3" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->warning_date3 }}</a>
                                            <span class="warning-price">€</span>
                                            <a href="#" class="inline-edit warning-price" data-type="number" data-step="0.01" data-pk="warning_price3" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->warning_price3, 2,",",".") }}</a>
                                        </td>

                                        <td class="border">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="indexierung" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->indexierung }}</a>
                                        </td>

                                        <td class="border">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="kaution" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->kaution }}</a>
                                        </td>
                                        <td >&nbsp;</td>
                                        <td >&nbsp;</td>
                                        <td >&nbsp;</td>
                                        <td >&nbsp;</td>
                                        <td class="checkbox-td" >&nbsp;</td>
                                        {{-- <td >&nbsp;</td> --}}

                                        @php
                                          $item_comment = $item->latestComment(0);
                                          $item_external_comment = $item->latestComment(1);
                                        @endphp

                                        <td class="border note">
                                            {{-- &nbsp; {{ $item->singleComment()->comment ? $item->singleComment()->comment : $item->comment}} --}}
                                            {{-- <a href="javascript:void(0)" data-toggle="modal" data-id="{{$item->id}}" data-target="#comment_modal" style="margin-top: 3%; width: 26% ; color: white" class="commentAdd   btn btn-primary btn-block">Kommentare </a> --}}
                                            
                                            <div class="show_item_cmnt_section">
                                              @if( $item_comment )
                                                @foreach ($item_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=0" class="load_item_comment_section {{ (count($item_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="0">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="0" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                        <td class="border note">
                                            {{-- &nbsp; --}}
                                            {{-- {{ $item->extComment()->external_comment ? $item->extComment()->external_comment : $item->comment2}} --}}
                                            {{-- <a href="javascript:void(0)" data-toggle="modal" data-id="{{$item->id}}" data-target="#commentTwo_modal" style="margin-top: 3%; width: 26% ; color: white" class="commentTwo   btn btn-primary btn-block">Kommentare </a> --}}
                                            
                                            <div class="show_item_cmnt_section">
                                              @if( $item_external_comment )
                                                @foreach ($item_external_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->external_comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=1" class="load_item_comment_section {{ (count($item_external_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="1">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="1" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach

                            {{--row 21--}}
                            <tr>
                               <th class="border seprate">
                                    <form action="{{url("tenancy-schedule-items/create")}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="type" value="{{config('tenancy_schedule.item_type.live')}}">
                                        <input type="hidden" name="tenancy_schedule_id" value="{{$tenancy_schedule->id}}">
                                        <input type="hidden" name="property_id" value="{{$id}}">
                                        <button type="submit" class="btn btn-default btn-xs" value="delete" name="action">{{__('tenancy_schedule.add')}}</button>
                                    </form>
                                </th>
                                <td class="border checkbox-td">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <th class="border mietvertrag"></th>
                                
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>

                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>


                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                
                                <th class="border layout"></th>
                                <td class="border checkbox-td">&nbsp;</td>
                                <td class="border checkbox-td">&nbsp;</td>
                                <td class="border checkbox-td">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                {{-- <td class="border">&nbsp;</td> --}}
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                            </tr>
                            {{--row 22--}}
                            <tr>
                                <th colspan="7" class="border bg-light-blue seprate-blue">Vermietet</th>
                                <th class="text-right border bg-light-blue">{{$tenancy_schedule->calculations['total_live_rental_space']}}</th>
                                <th class="text-right border bg-light-blue">Ø {{number_format($tenancy_schedule->
                                calculations['live_total_avg_rent'], 2,",",".")}}</th>
                                <th class="text-right border bg-light-blue">{{number_format($tenancy_schedule->calculations['total_live_actual_net_rent'], 2,",",".")}}&nbsp;€</th>
                                <th class="text-right border bg-light-blue">{{number_format($tenancy_schedule->calculations['total_live_actual_net_rent']*12, 2,",",".")}}&nbsp;€</th>
                                <th class="text-right border bg-light-blue">{{$tenancy_schedule->calculations['total_live_nk_netto']}}&nbsp;€</th>

                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>
                                <td class="border bg-light-blue year">&nbsp;</td>


                                <th class="text-right border bg-light-blue">{{ number_format($csum, 2,",",".") }}&nbsp;€</th>
                                <th class="text-right border bg-light-blue">{{ number_format($csum1, 2,",",".") }}&nbsp;€</th>
                                <th class="text-right border bg-light-blue">{{ number_format($csum2, 2,",",".") }}&nbsp;€</th>
                                <th class="text-right border bg-light-blue">{{ number_format($csum3, 2,",",".") }}&nbsp;€</th>
                                <th class="text-right border bg-light-blue"></th>
                                <th class="text-right border bg-light-blue"></th>

                                {{-- <th class="text-right border bg-light-blue mietvertrag"></th> --}}
                                <th class="text-right border bg-light-blue layout"></th>
                                <th class="text-right border bg-light-blue"></th>

                                <td class="border bg-light-blue checkbox-td">&nbsp;</td>
                                <td class="border bg-light-blue checkbox-td">&nbsp;</td>
                                <th class="text-right border bg-light-blue"></th>
                                <th class="text-right border bg-light-blue"></th>
                                <th class="text-right border bg-light-blue"></th>
                                <td class="border bg-light-blue">&nbsp;</td>
                                <th class="text-right border bg-light-blue"></th>
                                <th class="text-right border bg-light-blue"></th>
                                <th class="text-right border bg-light-blue"></th>
                                <th class="text-right border bg-light-blue"></th>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                {{-- <td class="border"></td> --}}
                                <td class="border"></td>
                                <td class="border"></td>
                            </tr>
                            <tr>
                               <td>&nbsp;</td>
                            </tr>
                            <?php
                                $sum_actual_net_rent2 = 0;
                                $sum_actual_net_rent2 +=0;
                            ?>
                            @foreach($tenancy_schedule->items as $item)
                                @if($item->type == config('tenancy_schedule.item_type.live_vacancy'))
                                    <tr class="item-tr">
                                       <td class="border">
                                            <form action="{{url("tenancy-schedules/delete/$tenancy_schedule->id/item/$item->id")}}" method="POST" style="display: inline">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn-sm" style="color:white" onclick="return confirm('Are you sure?')">{{__('forecast.delete')}}</button>
                                            </form>
                                            <span style="padding-left: 10px">
                                                <a href="#" class="inline-edit" data-type="text" data-pk="name" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->name }}</a>
                                            </span>
                                        </td>
                                        <td class="border checkbox-td">
                                            <select class="change-asset-user" data-pk="tenant_closed" data-id="{{$item->id}}">
                                                @foreach($td1 as $k=>$list)
                                                    @if($item->tenant_closed==$k)
                                                        <option selected="selected" value="{{$k}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$k}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="border checkbox-td">
                                            <select class="change-asset-user" data-pk="rent_payment" data-id="{{$item->id}}">
                                                @foreach($td2 as $list)
                                                    @if($item->rent_payment==$list)
                                                        <option selected="selected" value="{{$list}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$list}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <th class="border mietvertrag text-center">
                                            
                                          <a href="javascript:void(0);" class="link-button-pdf-mieterliste"  data-id="{{$item->id}}" data-file_column_type="0">
                                            <i class="fa fa-link"></i>
                                          </a>

                                           @foreach($item->files()->get() as $file)
                                                    @if($file['file_column_type']==0)
                                                    @if($file['type']=='file')
                                                    &nbsp;&nbsp;<a href="{{ $file['file_href'] }}"  id="mieterliste-gdrive-link-{{$file['id']}}"  target="_blank" title="{{ $file['file_name'] }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $file['file_extension']) ?: 'fa-file' }}" ></i></a>
                                                    @else
                                                        <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  ><i class="fa fa-folder" ></i></a>
                                                    @endif
                                                    @endif
                                           @endforeach
                                            <a class="listFilesForDelete" data-property_id="" data-id="{{ $item->id }}" data-type="mieterliste"><i class="fa fa-trash iconstyle"></i></a>
                                        </th>

                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="termination_by" data-placement="bottom" data-inputclass="mask-input-new-date" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->termination_by)}}">{{ show_date_format($item->termination_by) }}</a>
                                        </td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        {{-- this td --}}
                                        <td class="text-right border"></td>
                                        <td class="text-right border"></td>
                                        <th class="border"></th>

                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>
                                        <th class="border year"></th>



                                        <th class="border"></th>
                                        <th class="border"></th>
                                        <th class="border"></th>
                                        <th class="border"></th>
                                        <th class="border"></th>
                                        <th class="border"></th>
                                        <th class="border layout text-center">
                                            
                                          <a href="javascript:void(0);" class="link-button-pdf-mieterliste"  data-id="{{$item->id}}" data-file_column_type="1">
                                            <i class="fa fa-link"></i>
                                          </a>

                                           @foreach($item->files()->get() as $file)
                                                    @if($file['file_column_type']==1)
                                                    @if($file['type']=='file')
                                                    &nbsp;&nbsp;<a href="{{ $file['file_href'] }}"  id="mieterliste-gdrive-link-{{$file['id']}}"  target="_blank" title="{{ $file['file_name'] }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $file['file_extension']) ?: 'fa-file' }}" ></i></a>
                                                    @else
                                                        <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  ><i class="fa fa-folder" ></i></a>
                                                    @endif
                                                    @endif
                                           @endforeach
                                            <a class="listFilesForDelete" data-property_id="" data-id="{{ $item->id }}" data-type="mieterliste"><i class="fa fa-trash iconstyle"></i></a>
                                        </th>

                                        <td class="border checkbox-td">
                                          <a href="#" class="inline-edit" data-type="text" data-pk="opos_mieter" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->opos_mieter }}</a>
                                        </td>

                                        <td class="checkbox-td border text-center">
                                            <input type="checkbox" class="checkbox-is-new" data-id="{{$item->id}}" @if($item->is_new) checked @endif>
                                        </td>
                                        <td class="border checkbox-td custom-asset-manager">
                                            <?php
                                                $asset_mn_id = "";
                                                if($item->asset_manager_id)
                                                    $asset_mn_id = $item->asset_manager_id;
                                            ?>
                                            <select class="change-asset-user" data-pk="asset_manager_id" data-id="{{$item->id}}">
                                                <option value=""> </option>
                                                @foreach($as_users as $list)
                                                    @if($asset_mn_id==$list->id)
                                                        <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                    @else
                                                        <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <?php
                                                $asset_mn_id2 = "";
                                                if($item->asset_manager_id2)
                                                    $asset_mn_id2 = $item->asset_manager_id2;
                                            ?>
                                            <select class="change-asset-user" data-pk="asset_manager_id2" data-id="{{$item->id}}">
                                                 <option value=""> </option>
                                                 @foreach($as_users as $list)
                                                 @if($asset_mn_id2==$list->id)
                                                 <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                 @else
                                                 <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                 @endif
                                                 @endforeach
                                            </select>
                                        </td>
                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="assesment_date" data-placement="bottom" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-value="@if($item->assesment_date){{show_date_format( $item->assesment_date)}}@endif">{{ show_date_format($item->assesment_date) }}</a>
                                        </td>
                                        <td class="text-right border"></td>
                                        <td class="border"></td>
                                        <td class="border"><a href="#" class="inline-edit-use" data-type="select" data-pk="use" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->use }}</a></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="indexierung" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->indexierung }}</a></td>
                                        <th class="border"></th>
                                        <td class="text-right border">
                                            <a href="#" class="inline-edit" data-type="number" data-step="0.01" data-pk="vacancy_in_qm" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->vacancy_in_qm, 2,",",".") }}</a>&nbsp;
                                        </td>
                                         {{--  @if( ( $tenancy_schedule->calculations['total_live_rental_space']) != 0 )
                                         {{ number_format($tenancy_schedule->calculations['total_live_actual_net_rent'] / $tenancy_schedule->calculations['total_live_rental_space'], 2,",",".") }}
                                         @else
                                         0
                                         @endif --}}
                                        <?php
                                            $sum_actual_net_rent2 +=$item->actual_net_rent2;
                                        ?>
                                        <td class="text-right border">{{ number_format($item->vacancy_in_eur, 2,",",".") }}&nbsp;€</td>
                                        <td class="text-right border"><a href="#" class="inline-edit" data-type="number" data-step="0.01" data-pk="actual_net_rent2" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->actual_net_rent2, 2,",",".") }}</a></td>

                                        <td class="border text-center">
                                          <a href="#" class="inline-edit" data-type="text" data-pk="vacant_since" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-inputclass="mask-input-new-date" data-title="DD.MM.JJJJ" data-value="@if($item->vacant_since){{show_date_format($item->vacant_since)}}@endif">{{ show_date_format($item->vacant_since) }}</a>
                                        </td>

                                        <td class="checkbox-td border text-center"><input type="checkbox" class="checkbox-is-vacant" data-id="{{$item->id}}" @if($item->vacancy_on_purcahse) checked @endif></td>

                                        {{-- <td class="border text-center">
                                          <a href="#" class="inline-edit" data-type="text" data-pk="is_upload_date" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-inputclass="mask-input-new-date" data-title="DD.MM.JJJJ" data-value="@if($item->is_upload_date){{show_date_format($item->is_upload_date)}}@endif">{{ show_date_format($item->is_upload_date) }}</a>
                                        </td> --}}
                                        @php
                                          $item_comment = $item->latestComment(0);
                                          $item_external_comment = $item->latestComment(1);
                                        @endphp

                                        <td class="border note">
                                          {{-- <a href="#" class="inline-edit" data-type="textarea" data-pk="comment" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->comment }}</a> --}}
                                            <div class="show_item_cmnt_section">
                                              @if( $item_comment )
                                                @foreach ($item_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=0" class="load_item_comment_section {{ (count($item_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="0">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="0" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                        <td class="border note">
                                          {{-- <a href="#" class="inline-edit" data-type="textarea" data-pk="comment2" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->comment2 }}</a> --}}
                                            <div class="show_item_cmnt_section">
                                              @if( $item_external_comment )
                                                @foreach ($item_external_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->external_comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=1" class="load_item_comment_section {{ (count($item_external_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="1">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="1" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            {{--row 23--}}
                            <tr>
                                <th class="border seprate">
                                    <form action="{{url("tenancy-schedule-items/create")}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="type" value="{{config('tenancy_schedule.item_type.live_vacancy')}}">
                                        <input type="hidden" name="tenancy_schedule_id" value="{{$tenancy_schedule->id}}">
                                        <input type="hidden" name="property_id" value="{{$id}}">
                                        <button type="submit" class="btn btn-default btn-xs">{{__('tenancy_schedule.add')}}</button>
                                    </form>
                                </th>
                                <td class="border checkbox-td"></td>
                                <td class="border"></td>
                                <td class="border mietvertrag"></td>

                                
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>

                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>
                                <th class="border year"></th>


                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>

                                
                                <td class="border layout"></td>
                                <td class="border checkbox-td"></td>

                                <td class="border checkbox-td"></td>
                                <td class="border checkbox-td"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border checkbox-td"></td>
                                {{-- <td class="border"></td> --}}
                                <td class="border"></td>
                                <td class="border"></td>
                            </tr>
                            {{--row 24--}}
                            <tr>
                                <th colspan="45" class="border bg-light-blue seprate-blue" >Leerstand</th>
                                <th class="text-right border bg-light-blue">{{ number_format($tenancy_schedule->calculations['total_live_vacancy_in_qm'], 2,",",".") }}&nbsp;</th>
                                <th class="text-right border bg-light-blue">
                                {{ number_format($tenancy_schedule->calculations['total_live_vacancy_in_eur'], 2,",",".") }}&nbsp;€
                                </th>
                                <th class="text-right border bg-light-blue">
                                    {{ number_format($sum_actual_net_rent2, 2,",",".") }}&nbsp;€
                                </th>
                                <th  class="border bg-light-blue checkbox-td">&nbsp;</th>
                                <th  class="border bg-light-blue">&nbsp;</th>
                            </tr>
                            {{--row 25--}}
                            <tr>
                              <td>&nbsp;</td>
                              <td class="checkbox-td">&nbsp;</td>
                              <td>&nbsp;</td>
                              <td class="mietvertrag">&nbsp;</td>
                              

                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>

                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>
                              <td class="year">&nbsp;</td>



                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>

                             
                             <td class="layout">&nbsp;</td>
                             <td class="checkbox-td">&nbsp;</td>


                             <td class="checkbox-td">&nbsp;</td>
                             <td class="checkbox-td">&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             {{-- <td>&nbsp;</td> --}}
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                            </tr>
                           {{--row 26--}}
                            <tr>
                               <th class=" bg-green border seprate">Gewerbe</th>
                               <th class=" bg-red border checkbox-td">Mieter geschlossen</th>
                               <th class=" bg-red border">Mietzahlung</th>
                               <th class="text-center bg-green border mietvertrag">Mietvertrag</th>
                               

                               <th class="text-center bg-green border">Mietbeginn</th>
                               <th class="text-center bg-green border">Mietende</th>
                               <th class="text-center bg-green border">Kündigung bis zum</th>



                               <th class="text-center bg-green border">Mietfläche in m²</th>
                               <th class="text-center bg-green border">Miete/m²</th>
                               <th class="text-center bg-green border">IST-Nettokaltmiete p.m.</th>
                               <th class="text-center bg-green border">IST-Nettokaltmiete p.a.</th>
                               <th class="text-center bg-green border"><button type="button" class="btn-sm btn-primary btn-year" style="">BKA anzeigen</button><br>


                               NK netto p.m.</th>

                               <th class="text-center bg-green border year" colspan="4">2017</th>
                               <th class="text-center bg-green border year" colspan="4">2018</th>
                               <th class="text-center bg-green border year" colspan="4">2019</th>
                               <th class="text-center bg-green border year" colspan="4">2020</th>


                               <th class="text-center bg-green border"> Gesamt Netto p.m. </th>
                               <th class="text-center bg-green border">19% MwSt.</th>
                               <th class="text-center bg-green border">Gesamt Brutto p.m.</th>
                               <th class="text-center bg-green border">Gesamt Brutto p.a. </th>
                               <th class="text-center bg-gray border">Restlaufzeit</th>
                               <th class="text-center bg-gray border">Restlaufzeit</th>

                               
                               <th class="text-center bg-green border layout">Grundriss</th>
                               <th class=" bg-green border checkbox-td">Domos NR</th>

                               <th id="test" class="checkbox-td bg-green border">Neu</th>
                               <th class="text-center checkbox-td bg-green border">Asset<br>Manager</th>
                               <th class="text-center bg-green border">Abschluss MV</th>
                               <th class="text-center bg-green border">Sonderkündigunsrecht</th>
                               <th class="text-center bg-green border">Optionen</th>
                               <th class="text-center bg-green border">Nutzung</th>
                               <th class="text-center bg-green border">Kategorie</th>
                               <th class="text-center bg-green border">Mahnung</th>
                               <th class="text-center bg-green border">Indexierung</th>
                               <th class="text-center bg-green border">Kaution</th>
                               <th class="text-center bg-gray border">Leerstand</th>
                               <th class="text-center bg-gray border">Leerstand</th>
                               <th class="text-center bg-gray border">Leerstand</th>
                               <th class="text-center bg-gray border">Leerstand seit</th>
                               <th class="text-center bg-gray border checkbox-td">Leerstand</th>
                               {{-- <th class="text-center bg-gray border">IS Upload</th> --}}
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                            </tr>
                            {{--row 27--}}
                            <tr>
                               <th class=" bg-green border seprate "></th>
                               <th class=" bg-red border seprate checkbox-td">Gewerbe</th>
                               <th class=" bg-red border seprate">Gewerbe</th>
                               <th class="text-center bg-green border mietvertrag"></th>
                               

                               <th class="text-center bg-green border"></th>
                               <th class="text-center bg-green border"></th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>

                               <th class="text-center bg-green border year">Datum BKA</th>
                               <th class="text-center bg-green border year">Betrag BKA</th>
                               <th class="text-center bg-green border year">Bezahldatum</th>
                               <th class="text-center bg-green border year">Datei</th>

                               <th class="text-center bg-green border year">Datum BKA</th>
                               <th class="text-center bg-green border year">Betrag BKA</th>
                               <th class="text-center bg-green border year">Bezahldatum</th>
                               <th class="text-center bg-green border year">Datei</th>

                               <th class="text-center bg-green border year">Datum BKA</th>
                               <th class="text-center bg-green border year">Betrag BKA</th>
                               <th class="text-center bg-green border year">Bezahldatum</th>
                               <th class="text-center bg-green border year">Datei</th>

                               <th class="text-center bg-green border year">Datum BKA</th>
                               <th class="text-center bg-green border year">Betrag BKA</th>
                               <th class="text-center bg-green border year">Bezahldatum</th>
                               <th class="text-center bg-green border year">Datei</th>

                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-yellow border"><a href="#" class="inline-edit" data-type="date" data-pk="business_date" data-url="{{url('tenancy-schedules/update/'.$tenancy_schedule->id) }}" data-title="" data-placement="bottom">{{ $tenancy_schedule->business_date }}</a></th>
                               <th class="text-center bg-gray border">in EUR</th>

                               
                               <th class="text-center bg-green border layout"></th>
                               <th class="bg-green border checkbox-td"></th>

                               <th class="checkbox-td text-center bg-green border"></th>
                               <th class="checkbox-td text-center bg-green border"></th>
                               <th class="text-center bg-green border"></th>
                               <th class="text-center bg-green border"></th>
                               <th class="text-center bg-green border"></th>
                               <th class="text-center bg-green border"></th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-green border">Gewerbe</th>
                               <th class="text-center bg-gray border">in qm</th>
                               <th class="text-center bg-gray border">EUR</th>
                               <th class="text-center bg-gray border">bewertet AM</th>
                               <th class="text-center bg-gray border"></th>
                               <th class="text-center bg-gray border checkbox-td">bei Ankauf</th>
                               {{-- <th class="text-center bg-gray border"></th> --}}
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                            </tr>
                            {{--row 28--}}
                            <?php
                                $csum = $csum1 = $csum2 = $csum3 = 0;
                                $sum_of_wault = $w_count = 0;
                            ?>
                            @foreach($tenancy_schedule->items as $item)
                                @if($item->type == config('tenancy_schedule.item_type.business'))
                                    <tr class="item-tr @if($item->rent_begin > date('Y-m-d')) item-futureactive @elseif($item->rent_end < date('Y-m-d')) item-pastactive @endif">
                                        <th class="border seprate">
                                            <form action="{{url("tenancy-schedules/delete/$tenancy_schedule->id/item/$item->id")}}" method="POST" style="display: inline">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn-sm" style="color:white" onclick="return confirm('Are you sure?')">{{__('forecast.delete')}}</button>
                                            </form>
                                            <button type="button" class="btn btn-primary btn-xs new-date" style="color:white" data-toggle="modal" data-target=".mydateModal" data-id="{{$item->id}}" >Verl.</button>
                                            <span style="padding-left: 10px">
                                                <a href="#" class="inline-edit" data-type="text" data-pk="name" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->name }}</a>
                                            </span>
                                        </th>

                                        <td class="border checkbox-td">
                                            <select class="change-asset-user" data-pk="tenant_closed" data-id="{{$item->id}}">
                                                @foreach($td1 as $k=>$list)
                                                    @if($item->tenant_closed==$k)
                                                        <option selected="selected" value="{{$k}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$k}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="border checkbox-td">
                                            <select class="change-asset-user" data-pk="rent_payment" data-id="{{$item->id}}">
                                                @foreach($td2 as $list)
                                                    @if($item->rent_payment==$list)
                                                        <option selected="selected" value="{{$list}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$list}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <th class="border mietvertrag text-center">
                                            
                                          <a href="javascript:void(0);" class="link-button-pdf-mieterliste"  data-id="{{$item->id}}" data-file_column_type="0">
                                            <i class="fa fa-link"></i>
                                          </a>

                                           @foreach($item->files()->get() as $file)
                                                    @if($file['file_column_type']==0)
                                                    @if($file['type']=='file')
                                                    &nbsp;&nbsp;<a href="{{ $file['file_href'] }}"  id="mieterliste-gdrive-link-{{$file['id']}}"  target="_blank" title="{{ $file['file_name'] }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $file['file_extension']) ?: 'fa-file' }}" ></i></a>
                                                    @else
                                                        <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  ><i class="fa fa-folder" ></i></a>
                                                    @endif
                                                    @endif
                                           @endforeach
                                            <a class="listFilesForDelete" data-property_id="" data-id="{{ $item->id }}" data-type="mieterliste"><i class="fa fa-trash iconstyle"></i></a>
                                        </th>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="rent_begin" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-inputclass="mask-input-new-date" data-title="DD.MM.JJJJ" data-value="@if($item->rent_begin){{show_date_format($item->rent_begin)}}@endif">{{ show_date_format($item->rent_begin) }}</a>
                                        </td>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="rent_end" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-inputclass="mask-input-new-date" data-value="@if($item->rent_end){{show_date_format($item->rent_end)}}@endif">{{ show_date_format($item->rent_end) }}</a>
                                        </td>
                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="termination_by" data-placement="bottom" data-inputclass="mask-input-new-date" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->termination_by)}}">{{ show_date_format($item->termination_by) }}</a>
                                        </td>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="number" data-step="0.01" data-pk="rental_space" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->rental_space, 2,",",".") }}</a>
                                        </td>

                                        <td class="border text-right">
                                            @if($item->rental_space && $item->actual_net_rent)
                                                {{ number_format($item->actual_net_rent/$item->rental_space, 2,",",".") }}
                                            @else
                                                {{ number_format(0, 2,",",".") }}
                                            @endif
                                            <?php
                                                if($item->rent_end > date('Y-m-d') && $item->rent_end && substr($item->rent_end,0,4)!="2099"){
                                                    $sum_total_amount += $item->remaining_time_in_eur;
                                                }

                                                if($item->status && $item->rent_end > date('Y-m-d') && $item->rent_end && substr($item->rent_end,0,4)!="2099")
                                                    $sum_actual_net_rent += $item->actual_net_rent;
                                            ?>
                                        </td>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="actual_net_rent" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->actual_net_rent, 2,",",".") }}</a>&nbsp;€
                                        </td>

                                        <td class="border text-right">{{ number_format($item->actual_net_rent*12, 2,",",".") }}&nbsp;€</td>
                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="nk_netto" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->nk_netto, 2,",",".") }}</a>&nbsp;€
                                        </td>

                                                                                <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="bka_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2017') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->bka_date_2017)}}">{{ show_date_format($item->bka_date_2017) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-number-input-negetive" data-pk="amount" data-url="{{url('tenancy-payment/update/'.$item->id.'/2017') }}" data-title="">{{ show_number($item->amount_2017, 2) }}</a>

                                        </td>
                                        <td class="border text-right year"><a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="payment_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2017') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->payment_date_2017)}}">{{ show_date_format($item->payment_date_2017) }}</a></td>

                                        <td class="border text-center datei_2017 datei year">

                                          <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{$item->id}}" data-property-id="{{$id}}" data-year="2017" id="datei_2017_{{ $item->id }}">
                                            <i class="fa fa-link"></i>
                                          </a>

                                          @if($item->tenant_payments_2017 && $item->tenant_payments_2017->file_name)

                                            <?php

                                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($item->tenant_payments_2017->file_basename) ? $item->tenant_payments_2017->file_basename : '');
                                              if($item->tenant_payments_2017->file_type == 'file'){
                                                $download_path = "https://drive.google.com/file/d/".(isset($item->tenant_payments_2017->file_basename) ? $item->tenant_payments_2017->file_basename : '');

                                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($item->tenant_payments_2017->file_basename) ? $item->tenant_payments_2017->file_basename : '').'&file_dir='.(isset($item->tenant_payments_2017->file_dirname) ? $item->tenant_payments_2017->file_dirname : '');
                                              }
                                            ?>
                                            <a href="{{ $download_path }}"  target="_blank" title="{{ $item->tenant_payments_2017->file_name }}"><i extension="filemanager.file_icon_array.{{$file['file_extension']}}" class="fa fa-file" ></i></a>

                                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $item->tenant_payments_2017->id]) }}"><i class="fa fa-trash"></i></a>

                                          @endif

                                        </td>


                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="bka_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2018') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->bka_date_2018)}}">{{ show_date_format($item->bka_date_2018) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-number-input-negetive" data-pk="amount" data-url="{{url('tenancy-payment/update/'.$item->id.'/2018') }}" data-title="">{{ show_number($item->amount_2018, 2) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="payment_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2018') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->payment_date_2018)}}">{{ show_date_format($item->payment_date_2018) }}</a>
                                        </td>
                                        <td class="border text-center datei_2018 datei year">

                                          <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{$item->id}}" data-property-id="{{$id}}" data-year="2018" id="datei_2018_{{ $item->id }}">
                                            <i class="fa fa-link"></i>
                                          </a>

                                          @if($item->tenant_payments_2018 && $item->tenant_payments_2018->file_name)

                                            <?php

                                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($item->tenant_payments_2018->file_basename) ? $item->tenant_payments_2018->file_basename : '');
                                              if($item->tenant_payments_2018->file_type == 'file'){
                                                $download_path = "https://drive.google.com/file/d/".(isset($item->tenant_payments_2018->file_basename) ? $item->tenant_payments_2018->file_basename : '');

                                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($item->tenant_payments_2018->file_basename) ? $item->tenant_payments_2018->file_basename : '').'&file_dir='.(isset($item->tenant_payments_2018->file_dirname) ? $item->tenant_payments_2018->file_dirname : '');
                                              }
                                            ?>
                                            <a href="{{ $download_path }}"  target="_blank" title="{{ $item->tenant_payments_2018->file_name }}"><i extension="filemanager.file_icon_array.{{$file['file_extension']}}" class="fa fa-file" ></i></a>

                                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $item->tenant_payments_2018->id]) }}"><i class="fa fa-trash"></i></a>

                                          @endif

                                        </td>

                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="bka_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2019') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->bka_date_2019)}}">{{ show_date_format($item->bka_date_2019) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-number-input-negetive" data-pk="amount" data-url="{{url('tenancy-payment/update/'.$item->id.'/2019') }}" data-title="">{{ show_number($item->amount_2019, 2) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="payment_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2019') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->payment_date_2019)}}">{{ show_date_format($item->payment_date_2019) }}</a>
                                        </td>
                                        <td class="border text-center datei_2019 datei year">

                                          <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{$item->id}}" data-property-id="{{$id}}" data-year="2019" id="datei_2019_{{ $item->id }}">
                                            <i class="fa fa-link"></i>
                                          </a>

                                          @if($item->tenant_payments_2019 && $item->tenant_payments_2019->file_name)

                                            <?php

                                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($item->tenant_payments_2019->file_basename) ? $item->tenant_payments_2019->file_basename : '');
                                              if($item->tenant_payments_2019->file_type == 'file'){
                                                $download_path = "https://drive.google.com/file/d/".(isset($item->tenant_payments_2019->file_basename) ? $item->tenant_payments_2019->file_basename : '');

                                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($item->tenant_payments_2019->file_basename) ? $item->tenant_payments_2019->file_basename : '').'&file_dir='.(isset($item->tenant_payments_2019->file_dirname) ? $item->tenant_payments_2019->file_dirname : '');
                                              }
                                            ?>
                                            <a href="{{ $download_path }}"  target="_blank" title="{{ $item->tenant_payments_2019->file_name }}"><i extension="filemanager.file_icon_array.{{$file['file_extension']}}" class="fa fa-file" ></i></a>

                                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $item->tenant_payments_2019->id]) }}"><i class="fa fa-trash"></i></a>

                                          @endif
                                        </td>

                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="bka_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2020') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->bka_date_2020)}}">{{ show_date_format($item->bka_date_2020) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-number-input-negetive" data-pk="amount" data-url="{{url('tenancy-payment/update/'.$item->id.'/2020') }}" data-title="">{{ show_number($item->amount_2020, 2) }}</a>
                                        </td>
                                        <td class="border text-right year">
                                          <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="payment_date" data-placement="bottom" data-url="{{url('tenancy-payment/update/'.$item->id.'/2020') }}" data-title="DD.MM.JJJJ" data-value="{{show_date_format($item->payment_date_2020)}}">{{ show_date_format($item->payment_date_2020) }}</a>
                                        </td>
                                        <td class="border text-center datei_2020 datei year">

                                          <a href="javascript:void(0);" class="link-button-datei" data-item-id="{{$item->id}}" data-property-id="{{$id}}" data-year="2020" id="datei_2020_{{ $item->id }}">
                                            <i class="fa fa-link"></i>
                                          </a>

                                          @if($item->tenant_payments_2020 && $item->tenant_payments_2020->file_name)

                                            <?php

                                              $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($item->tenant_payments_2020->file_basename) ? $item->tenant_payments_2020->file_basename : '');
                                              if($item->tenant_payments_2020->file_type == 'file'){
                                                $download_path = "https://drive.google.com/file/d/".(isset($item->tenant_payments_2020->file_basename) ? $item->tenant_payments_2020->file_basename : '');

                                                $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($item->tenant_payments_2020->file_basename) ? $item->tenant_payments_2020->file_basename : '').'&file_dir='.(isset($item->tenant_payments_2020->file_dirname) ? $item->tenant_payments_2020->file_dirname : '');
                                              }
                                            ?>
                                            <a href="{{ $download_path }}"  target="_blank" title="{{ $item->tenant_payments_2020->file_name }}"><i extension="filemanager.file_icon_array.{{$file['file_extension']}}" class="fa fa-file" ></i></a>

                                            <a class="delete_tenant_payment_file" data-url="{{ route('delete_tenant_payment_file', ['id' => $item->tenant_payments_2020->id]) }}"><i class="fa fa-trash"></i></a>

                                          @endif

                                        </td>



                                        <?php
                                            $natto =$item->actual_net_rent+$item->nk_netto;
                                            $tn = ($item->actual_net_rent+$item->nk_netto)*19/100;

                                            $csum += $natto;
                                            $csum1 += $tn;
                                            $csum2 += ($natto+$tn);
                                            $csum3 += ($natto+$tn)*12;
                                        ?>
                                        <td class="border text-right">{{ number_format($natto, 2,",",".") }}&nbsp;€</td>
                                        <td class="border text-right">{{ number_format($tn, 2,",",".") }}&nbsp;€</td>
                                        <td class="border text-right">{{ number_format($natto+$tn, 2,",",".") }}&nbsp;€</td>
                                        <td class="border text-right">{{ number_format(($natto+$tn)*12, 2,",",".") }}&nbsp;€</td>
                                        <td class="border text-right">
                                            @if($item->rent_end && substr($item->rent_end,0,4)!="2099")
                                                {{ number_format($item->date_diff_average, 1, ",", ".") }}
                                            @endif
                                            <?php
                                                if($item->rent_end>date('Y-m-d') && substr($item->rent_end,0,4)!="2099"){
                                                    $sum_of_wault += round($item->date_diff_average,1);
                                                    $w_count +=1;
                                                }
                                            ?>
                                        </td>
                                        <td class="border text-right">
                                            @if($item->rent_end && substr($item->rent_end,0,4)!="2099")
                                            {{ number_format($item->remaining_time_in_eur, 1,",",".") }}
                                            @endif
                                        </td>

                                        
                                        <th class="border layout text-center">
                                            
                                          <a href="javascript:void(0);" class="link-button-pdf-mieterliste"  data-id="{{$item->id}}" data-file_column_type="1">
                                            <i class="fa fa-link"></i>
                                          </a>

                                           @foreach($item->files()->get() as $file)
                                                    @if($file['file_column_type']==1)
                                                    @if($file['type']=='file')
                                                    &nbsp;&nbsp;<a href="{{ $file['file_href'] }}"  id="mieterliste-gdrive-link-{{$file['id']}}"  target="_blank" title="{{ $file['file_name'] }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $file['file_extension']) ?: 'fa-file' }}" ></i></a>
                                                    @else
                                                        <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  ><i class="fa fa-folder" ></i></a>
                                                    @endif
                                                    @endif
                                           @endforeach
                                            <a class="listFilesForDelete" data-property_id="" data-id="{{ $item->id }}" data-type="mieterliste"><i class="fa fa-trash iconstyle"></i></a>
                                        </th>

                                        <td class="border checkbox-td">
                                          <a href="#" class="inline-edit" data-type="text" data-pk="opos_mieter" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->opos_mieter }}</a>
                                        </td>

                                        <td class="checkbox-td border text-center">
                                            <input type="checkbox" class="checkbox-is-new" data-id="{{$item->id}}" @if($item->is_new) checked @endif>
                                        </td>
                                        <td class="border checkbox-td custom-asset-manager">
                                            <?php
                                                $asset_mn_id = "";
                                                if($item->asset_manager_id)
                                                    $asset_mn_id = $item->asset_manager_id;
                                            ?>
                                            <select class="change-asset-user" data-pk="asset_manager_id" data-id="{{$item->id}}">
                                                <option value=""> </option>
                                                @foreach($as_users as $list)
                                                    @if($asset_mn_id==$list->id)
                                                        <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                    @else
                                                        <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            <?php
                                                $asset_mn_id2 = "";
                                                if($item->asset_manager_id2)
                                                    $asset_mn_id2 = $item->asset_manager_id2;
                                            ?>
                                            <select class="change-asset-user" data-pk="asset_manager_id2" data-id="{{$item->id}}">
                                                 <option value=""> </option>
                                                 @foreach($as_users as $list)
                                                 @if($asset_mn_id2==$list->id)
                                                 <option selected="selected" value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                 @else
                                                 <option value="{{$list->id}}">{{$list->getshortname()}}</option>
                                                 @endif
                                                 @endforeach
                                            </select>
                                        </td>

                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="assesment_date" data-placement="bottom" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-value="@if($item->assesment_date){{show_date_format($item->assesment_date)}}@endif">{{ show_date_format($item->assesment_date) }}</a>
                                        </td>
                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="termination_date" data-placement="bottom"       data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="DD.MM.JJJJ" data-inputclass="mask-input-new-date" data-value="@if($item->termination_date){{show_date_format($item->termination_date)}}@endif">{{ show_date_format($item->termination_date) }}</a>
                                        </td>
                                        <td class="border text-right">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="options" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->options }}</a>
                                            <input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="0" @if($item->selected_option==0) checked @endif >
                                            <br>
                                            <input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="1" @if($item->selected_option==1) checked @endif >
                                            <br>
                                            <input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="2" @if($item->selected_option==2) checked @endif >
                                        </td>
                                        <td class="border">
                                            <a href="#" class="inline-edit-use" data-type="select" data-pk="use" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->use }}</a>
                                        </td>
                                        <td class="border">
                                            <select class="change-asset-user" data-pk="category" data-id="{{$item->id}}">
                                                @foreach($category_list as $list)
                                                    @if($item->category==$list)
                                                        <option selected="selected" value="{{$list}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$list}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="border">
                                            <span class="title-warning">1. Mahnung</span>
                                            <a href="#" class="inline-edit warning_date" data-type="date" data-placement="bottom"  data-pk="warning_date1" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->warning_date1 }}</a>
                                            <input type="checkbox" class="warning-price-check" data-id="{{ $item->id }}" data-pk="warning_price1" value="1" @if($item->warning_price1==1) checked @endif >
                                            <br>
                                            <span class="title-warning">2. Mahnung</span>
                                            <a href="#" class="inline-edit warning_date" data-type="date" data-placement="bottom"  data-pk="warning_date2" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->warning_date2 }}</a>
                                            <input type="checkbox" class="warning-price-check" data-id="{{ $item->id }}" data-pk="warning_price2" value="1" @if($item->warning_price2==1) checked @endif >
                                            <br>
                                            <span class="title-warning">Mahnbescheid</span>
                                            <a href="#" class="inline-edit warning_date" data-type="date" data-placement="bottom"  data-pk="warning_date3" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->warning_date3 }}</a>
                                            <span class="warning-price">€</span>
                                            <a href="#" class="inline-edit warning-price" data-type="number" data-step="0.01" data-pk="warning_price3" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->warning_price3, 2,",",".") }}</a>
                                        </td>
                                        <td class="border">
                                            <a href="#" class="inline-edit" data-type="text" data-pk="indexierung" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->indexierung }}</a>
                                        </td>
                                        <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="kaution" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->kaution }}</a></td>
                                        <td class="no-bg"></td>
                                        <td class="no-bg"></td>
                                        <td class="no-bg"></td>
                                        <td class="no-bg"></td>
                                        <td class="checkbox-td no-bg"></td>
                                        {{-- <td class="no-bg"></td> --}}

                                        @php
                                          $item_comment = $item->latestComment(0);
                                          $item_external_comment = $item->latestComment(1);
                                        @endphp

                                        <td class="border note">
                                            {{-- &nbsp; {{ $item->singleComment()->comment ? $item->singleComment()->comment : $item->comment}}
                                            <a href="javascript:void(0)" data-toggle="modal" data-id="{{$item->id}}" data-target="#comment_modal" style="margin-top: 3%; width: 26% ; color: white" class="commentAdd   btn btn-primary btn-block">Kommentare </a> --}}
                                            <div class="show_item_cmnt_section">
                                              @if( $item_comment )
                                                @foreach ($item_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=0" class="load_item_comment_section {{ (count($item_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="0">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="0" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                        <td class="border note">
                                            {{-- {{ $item->extComment()->external_comment ? $item->extComment()->external_comment : $item->comment2}}
                                            <a href="javascript:void(0)" data-toggle="modal" data-id="{{$item->id}}" data-target="#commentTwo_modal" style="margin-top: 3%; width: 26% ; color: white" class="commentTwo   btn btn-primary btn-block">Kommentare </a> --}}
                                            <div class="show_item_cmnt_section">
                                              @if( $item_external_comment )
                                                @foreach ($item_external_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->external_comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=1" class="load_item_comment_section {{ (count($item_external_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="1">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="1" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            {{--row 29--}}
                            <tr>
                               <th class="border seprate">
                                    <form action="{{url("tenancy-schedule-items/create")}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="type" value="{{config('tenancy_schedule.item_type.business')}}">
                                        <input type="hidden" name="tenancy_schedule_id" value="{{$tenancy_schedule->id}}">
                                        <input type="hidden" name="property_id" value="{{$id}}">
                                        <button type="submit" class="btn btn-default btn-xs">{{__('tenancy_schedule.add')}}</button>
                                    </form>
                                </th>
                                <td class="border checkbox-td">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border mietvertrag">&nbsp;</td>
                                
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>

                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>
                                <td class="border year">&nbsp;</td>


                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>

                                
                                <td class="border layout">&nbsp;</td>
                                <td class="border checkbox-td">&nbsp;</td>

                                <td class="border checkbox-td">&nbsp;</td>
                                <td class="border checkbox-td">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border checkbox-td">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>
                                {{-- <td class="border">&nbsp;</td> --}}
                                <td class="border">&nbsp;</td>
                                <td class="border">&nbsp;</td>

                            </tr>
                            {{--row 31--}}
                            <tr>
                                <th colspan="7" class="border bg-green seprate-blue">Gewerbe Vermietet</th>
                                <th class="text-right border bg-green">
                                    {{$tenancy_schedule->calculations['total_business_rental_space']}}
                                </th>
                                <th class="border bg-green text-right">Ø
                                    @if( ( $tenancy_schedule->calculations['total_business_rental_space']) != 0 )
                                        {{ number_format($tenancy_schedule->calculations['total_business_actual_net_rent'] / $tenancy_schedule->calculations['total_business_rental_space'], 2,",",".") }}
                                    @else
                                        0
                                    @endif
                                </th>
                                <th class="text-right border bg-green">{{number_format($tenancy_schedule->calculations['total_business_actual_net_rent'], 2,",",".")}}&nbsp;€</th>
                                <th class="text-right border bg-green">{{number_format($tenancy_schedule->calculations['total_business_actual_net_rent']*12, 2,",",".")}}&nbsp;€</th>
                                <th class="text-right border bg-green">{{number_format($tenancy_schedule->calculations['total_business_nk_netto'], 2,",",".")}}&nbsp;€</th>

                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>
                                <th class="text-right border bg-green year"></th>


                                <td class="border text-right bg-green">{{ number_format($csum, 2,",",".") }}&nbsp;€</td>
                                <td class="border text-right bg-green">{{ number_format($csum1, 2,",",".") }}&nbsp;€</td>
                                <td class="border text-right bg-green">{{ number_format($csum2, 2,",",".") }}&nbsp;€</td>
                                <td class="border text-right bg-green">{{ number_format($csum3, 2,",",".") }}&nbsp;€</td>
                                <td class="border text-right bg-green" colspan="2">&nbsp;</td>

                                {{-- <td class="border bg-green mietvertrag">&nbsp;</td> --}}
                                <td class="border bg-green layout">&nbsp;</td>
                                <td class="border bg-green">&nbsp;</td>

                                <td class="border bg-green checkbox-td">&nbsp;</td>
                                <td class="border bg-green checkbox-td">&nbsp;</td>
                                <td class="border bg-green">&nbsp;</td>
                                <td class="border bg-green">&nbsp;</td>
                                <td class="border bg-green">&nbsp;</td>
                                <td class="border bg-green">&nbsp;</td>
                                <td class="border bg-green">&nbsp;</td>

                                <td class="border text-right bg-green" colspan="3">&nbsp;</td>
                                <th></th>
                                <td class=""></td>
                                <td class=""></td>
                                <td class=""></td>
                                <td class=""></td>
                                {{-- <td class=""></td> --}}
                                <td class=""></td>
                                <td class=""></td>
                            </tr>

                            <tr>
                               <td class="seprate">&nbsp;</td>
                               <td class="seprate checkbox-td">&nbsp;</td>
                            </tr>

                            <?php
                                $sum_actual_net_rent2 = 0;
                            ?>
                            @foreach($tenancy_schedule->items as $item)
                                @if($item->type == config('tenancy_schedule.item_type.business_vacancy'))
                                    <tr class="item-tr">
                                        <th class="border seprate">
                                            <form action="{{url("tenancy-schedules/delete/$tenancy_schedule->id/item/$item->id")}}" method="POST" style="display: inline">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn-sm" style="color:white" onclick="return confirm('Are you sure?')">{{__('forecast.delete')}}</button>
                                            </form>
                                            {{--<button type="button" class="btn btn-success btn-xs" style="color:white" data-toggle="modal" data-target=".myImmoModal" data-id="{{$item->id}}" >Details</button>--}}
                                            {{--<button type="button" class="btn btn-success btn-xs" style="color:white" data-toggle="modal" data-target=".myImmoModal" data-id="{{$item->id}}" >Details</button>--}}
                                            <span style="padding-left: 10px">
                                                <a href="#" class="inline-edit" data-type="text" data-pk="name" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->name }}</a>
                                            </span>
                                        </th>
                                        <td class="border checkbox-td"></td>
                                        <td class="border"></td>
                                        <th class="border mietvertrag text-center">
                                            
                                          <a href="javascript:void(0);" class="link-button-pdf-mieterliste"  data-id="{{$item->id}}" data-file_column_type="0">
                                            <i class="fa fa-link"></i>
                                          </a>

                                           @foreach($item->files()->get() as $file)
                                                    @if($file['file_column_type']==0)
                                                    @if($file['type']=='file')
                                                    &nbsp;&nbsp;<a href="{{ $file['file_href'] }}"  id="mieterliste-gdrive-link-{{$file['id']}}"  target="_blank" title="{{ $file['file_name'] }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $file['file_extension']) ?: 'fa-file' }}" ></i></a>
                                                    @else
                                                        <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  ><i class="fa fa-folder" ></i></a>
                                                    @endif
                                                    @endif
                                           @endforeach
                                            <a class="listFilesForDelete" data-property_id="" data-id="{{ $item->id }}" data-type="mieterliste"><i class="fa fa-trash iconstyle"></i></a>
                                        </th>

                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>

                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>



                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>

                                        <td class="checkbox-td border text-center"></td>
                                        <td class="border checkbox-td">

                                        </td>

                                        <td class="border text-right"></td>

                                        
                                        <th class="border layout text-center">
                                            
                                          <a href="javascript:void(0);" class="link-button-pdf-mieterliste"  data-id="{{$item->id}}" data-file_column_type="1">
                                            <i class="fa fa-link"></i>
                                          </a>

                                           @foreach($item->files()->get() as $file)
                                                    @if($file['file_column_type']==1)
                                                    @if($file['type']=='file')
                                                    &nbsp;&nbsp;<a href="{{ $file['file_href'] }}"  id="mieterliste-gdrive-link-{{$file['id']}}"  target="_blank" title="{{ $file['file_name'] }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $file['file_extension']) ?: 'fa-file' }}" ></i></a>
                                                    @else
                                                        <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  ><i class="fa fa-folder" ></i></a>
                                                    @endif
                                                    @endif
                                           @endforeach
                                            <a class="listFilesForDelete" data-property_id="" data-id="{{ $item->id }}" data-type="mieterliste"><i class="fa fa-trash iconstyle"></i></a>
                                        </th>
                                        <td class="border checkbox-td">
                                          <a href="#" class="inline-edit" data-type="text" data-pk="opos_mieter" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->opos_mieter }}</a>
                                        </td>
                                        
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border">
                                          <a href="#" class="inline-edit-use" data-type="select" data-pk="use" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->use }}</a>
                                        </td>
                                        <td class="border"></td>
                                        {{-- Second con --}}
                                        {{--@if( ( $tenancy_schedule->calculations['total_business_rental_space']) != 0 )--}}
                                        {{--{{ number_format($tenancy_schedule->calculations['total_business_actual_net_rent'] / $tenancy_schedule->calculations['total_business_rental_space'], 2,",",".") }}--}}
                                        {{--@else--}}
                                        {{--0--}}
                                        {{--@endif--}}
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>

                                        <td class="text-right border"><a href="#" class="inline-edit" data-type="number" data-step="0.01" data-pk="vacancy_in_qm" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->vacancy_in_qm, 2,",",".") }}</a>&nbsp;</td>
                                        <td class="text-right border">
                                            @if($item->vacancy_in_eur)
                                                {{ number_format($item->vacancy_in_eur, 2,",",".") }}
                                            @else
                                                {{ number_format($item->actual_net_rent2, 2,",",".") }}
                                            @endif
                                            <?php
                                                $sum_actual_net_rent2 +=$item->actual_net_rent2;
                                            ?>
                                            &nbsp;€
                                        </td>
                                        <td class="text-right border"><a href="#" class="inline-edit" data-type="number" data-step="0.01" data-pk="actual_net_rent2" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->actual_net_rent2, 2,",",".") }}</a></td>
                                        {{-- <td></td> --}}
                                        {{-- <td></td> --}}
                                        {{-- <td></td> --}}

                                        <td class="border text-center">
                                          <a href="#" class="inline-edit" data-type="text" data-pk="vacant_since" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-inputclass="mask-input-new-date" data-title="DD.MM.JJJJ" data-value="@if($item->vacant_since){{show_date_format($item->vacant_since)}}@endif">{{ show_date_format($item->vacant_since) }}</a>
                                        </td>

                                        <td class="checkbox-td border text-center"><input type="checkbox" class="checkbox-is-vacant" data-id="{{$item->id}}" @if($item->vacancy_on_purcahse) checked @endif></td>

                                        {{-- <td class="border text-center">
                                          <a href="#" class="inline-edit" data-type="text" data-pk="is_upload_date" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-inputclass="mask-input-new-date" data-title="DD.MM.JJJJ" data-value="@if($item->is_upload_date){{show_date_format($item->is_upload_date)}}@endif">{{ show_date_format($item->is_upload_date) }}</a>
                                        </td> --}}

                                        @php
                                          $item_comment = $item->latestComment(0);
                                          $item_external_comment = $item->latestComment(1);
                                        @endphp

                                        <td class="border note">
                                            {{-- &nbsp; {{ $item->singleComment()->comment ? $item->singleComment()->comment : $item->comment}}
                                            <a href="javascript:void(0)" data-toggle="modal" data-id="{{$item->id}}" data-target="#comment_modal" style="margin-top: 3%; width: 26% ; color: white" class="commentAdd   btn btn-primary btn-block">Kommentare </a> --}}
                                            <div class="show_item_cmnt_section">
                                              @if( $item_comment )
                                                @foreach ($item_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=0" class="load_item_comment_section {{ (count($item_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="0">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="0" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                        <td class="border note">
                                           {{--  {{ $item->extComment()->external_comment ? $item->extComment()->external_comment : $item->comment2}}
                                            <a href="javascript:void(0)" data-toggle="modal" data-id="{{$item->id}}" data-target="#commentTwo_modal" style="margin-top: 3%; width: 26% ; color: white" class="commentTwo   btn btn-primary btn-block">Kommentare </a> --}}
                                            <div class="show_item_cmnt_section">
                                              @if( $item_external_comment )
                                                @foreach ($item_external_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->external_comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=1" class="load_item_comment_section {{ (count($item_external_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="1">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="1" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            {{--row 32--}}
                            <tr>
                                <th class="border seprate">
                                    <form action="{{url("tenancy-schedule-items/create")}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="type" value="{{config('tenancy_schedule.item_type.business_vacancy')}}">
                                        <input type="hidden" name="tenancy_schedule_id" value="{{$tenancy_schedule->id}}">
                                        <input type="hidden" name="property_id" value="{{$id}}">
                                        <button type="submit" class="btn btn-default btn-xs">{{__('tenancy_schedule.add')}}</button>
                                    </form>
                                </th>
                                <td class="border checkbox-td"></td>
                                <td class="border"></td>
                                <td class="border mietvertrag"></td>
                                
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>

                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>

                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="text-right border"></td>
                                <td class="text-right border"></td>

                                
                                <td class="border layout"></td>
                                <td class="border checkbox-td"></td>

                                <td class="checkbox-td border"></td>
                                <td class="checkbox-td border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border checkbox-td"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                {{-- <td class="border"></td> --}}
                                <td class="border"></td>
                                <td class="border"></td>
                            </tr>
                            {{--row 34--}}
                            <tr>
                               <th colspan="44" class="border bg-green seprate-blue">Gewerbe Leerstand</th>
                               <th class="text-right border bg-green">{{ number_format($tenancy_schedule->calculations['total_business_vacancy_in_qm'], 2,",",".") }}&nbsp;</th>
                               <th class="text-right border bg-green">
                                {{ number_format($tenancy_schedule->calculations['total_business_vacancy_in_eur'], 2,",",".") }}&nbsp;€
                                </th>
                                <th class="text-right border bg-green">
                                    {{ number_format($sum_actual_net_rent2, 2,",",".") }}&nbsp;€
                                </th>
                                <th class="border bg-green checkbox-td">&nbsp;</th>
                                <th class="border bg-green">&nbsp;</th>
                            </tr>
                            <tr>
                               <td class="seprate">&nbsp;</td>
                            </tr>
                            {{-- Free Space Start --}}
                            <tr style="margin-top: 20px;">
                               <th colspan="32" class="border bg-green seprate-blue">Technische FF & Leerstand strukturell</th>
                            </tr>

                            @foreach($tenancy_schedule->items as $item)
                                @if($item->type == config('tenancy_schedule.item_type.free_space'))
                                    <tr class="item-tr">
                                       <th class="border seprate">
                                            <form action="{{url("tenancy-schedules/delete/$tenancy_schedule->id/item/$item->id")}}" method="POST" style="display: inline">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn-sm" style="color:white" onclick="return confirm('Are you sure?')">{{__('forecast.delete')}}</button>
                                            </form>
                                            <button type="button" class="btn btn-success btn-xs" style="color:white" data-toggle="modal" data-target=".myImmoModal" data-id="{{$item->id}}" >Details</button>
                                            <button type="button" class="btn btn-success btn-xs" style="color:white" data-toggle="modal" data-target=".myImmoModal" data-id="{{$item->id}}" >Details</button>
                                            <span style="padding-left: 10px">
                                              <a href="#" class="inline-edit" data-type="text" data-pk="free_space1" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->free_space1 }}</a>
                                            </span>
                                        </th>
                                        <td class="border checkbox-td">
                                            <select class="change-asset-user" data-pk="tenant_closed" data-id="{{$item->id}}">
                                                @foreach($td1 as $k=>$list)
                                                    @if($item->tenant_closed==$k)
                                                        <option selected="selected" value="{{$k}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$k}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="border checkbox-td">
                                            <select class="change-asset-user" data-pk="rent_payment" data-id="{{$item->id}}">
                                                @foreach($td2 as $list)
                                                    @if($item->rent_payment==$list)
                                                        <option selected="selected" value="{{$list}}">{{$list}}</option>
                                                    @else
                                                        <option value="{{$list}}">{{$list}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="border mietvertrag text-center">
                                          <a href="javascript:void(0);" class="link-button-pdf-mieterliste"  data-id="{{$item->id}}" >
                                            <i class="fa fa-link"></i>
                                          </a>
                                          @if($item->file_extension && $item->file_href)
                                            &nbsp;&nbsp;<a href="{{ $item->file_href }}"  id="mieterliste-gdrive-link-{{$item->id}}"  target="_blank" title="{{ $item->file_name }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $item->file_extension) ?: 'fa-file' }}" ></i></a>
                                          @endif
                                        </td>


                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="free_space2" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->free_space2 }}</a></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>

                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>
                                        <td class="border year"></td>

                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        {{-- Second con --}}
                                        <!-- @if( ( $tenancy_schedule->calculations['total_business_rental_space']) != 0 )
                                                {{ number_format($tenancy_schedule->calculations['total_business_actual_net_rent'] / $tenancy_schedule->calculations['total_business_rental_space'], 2,",",".") }}
                                            @else
                                                0
                                            @endif-->

                                        
                                        <td></td>
                                        <td class="border checkbox-td">
                                          <a href="#" class="inline-edit" data-type="text" data-pk="opos_mieter" data-placement="right" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->opos_mieter }}</a>
                                        </td>

                                        <td class="checkbox-td border text-center"></td>
                                        <td class="border checkbox-td"></td>
                                        <td class="border text-right"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border">
                                          <a href="#" class="inline-edit-use" data-type="select" data-pk="use" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->use }}</a>
                                        </td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="border"></td>
                                        <td class="text-right border">
                                            <a href="#" class="inline-edit" data-type="number" data-step="0.01" data-pk="free_space3" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ $item->free_space3 }}</a>&nbsp;
                                        </td>
                                        <td class="text-right border">{{ number_format($item->vacancy_in_eur, 2,",",".") }} &nbsp;€</td>
                                        <td class="text-right border">
                                            <a href="#" class="inline-edit" data-type="number" data-step="0.01" data-pk="actual_net_rent2" data-url="{{url('tenancy-schedule-items/update/'.$item->id) }}" data-title="">{{ number_format($item->actual_net_rent2, 2,",",".") }}</a>
                                        </td>
                                        <td class="border">&nbsp;</td>
                                        <td class="checkbox-td border text-center">
                                            <input type="checkbox" class="checkbox-is-vacant" data-id="{{$item->id}}" @if($item->vacancy_on_purcahse) checked @endif>
                                        </td>
                                        {{-- <td class="border">&nbsp;</td> --}}

                                        @php
                                          $item_comment = $item->latestComment(0);
                                          $item_external_comment = $item->latestComment(1);
                                        @endphp

                                        <td class="border note">
                                            {{-- &nbsp; {{ $item->singleComment()->comment ? $item->singleComment()->comment : $item->comment}}
                                            <a href="javascript:void(0)" data-toggle="modal" data-id="{{$item->id}}" data-target="#comment_modal" style="margin-top: 3%; width: 26% ; color: white" class="commentAdd   btn btn-primary btn-block">Kommentare </a> --}}
                                            <div class="show_item_cmnt_section">
                                              @if( $item_comment )
                                                @foreach ($item_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=0" class="load_item_comment_section {{ (count($item_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="0">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="0" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                        <td class="border note">
                                            {{-- {{ $item->extComment()->external_comment ? $item->extComment()->external_comment : $item->comment2}}
                                            <a href="javascript:void(0)" data-toggle="modal" data-id="{{$item->id}}" data-target="#commentTwo_modal" style="margin-top: 3%; width: 26% ; color: white" class="commentTwo   btn btn-primary btn-block">Kommentare </a> --}}
                                            <div class="show_item_cmnt_section">
                                              @if( $item_external_comment )
                                                @foreach ($item_external_comment as $comment)
                                                  @php
                                                    $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                                    $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                                                  @endphp
                                                  <p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->external_comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                                @endforeach
                                              @endif
                                            </div>

                                            <a href="javascript:void(0);" data-url="{{ route('getItemComment') }}?item_id={{ $item->id }}&type=1" class="load_item_comment_section {{ (count($item_external_comment) == 0) ? 'hidden' : '' }}" data-closest="td" data-type="1">Show More</a><br>

                                            <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $item->id }}" data-type="1" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach

                            <tr>
                                <th class="border seprate">
                                    <form action="{{url("tenancy-schedule-items/create")}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <!-- name="type" value="55" for freespace -->
                                        <input type="hidden" name="type" value="55">
                                        <input type="hidden" name="tenancy_schedule_id" value="{{$tenancy_schedule->id}}">
                                        <input type="hidden" name="property_id" value="{{$id}}">
                                        <button type="submit" class="btn btn-default btn-xs">{{__('tenancy_schedule.add')}}</button>
                                    </form>
                                </th>
                                <td class="border checkbox-td"></td>
                                <td class="border"></td>
                                <td class="border mietvertrag"></td>
                                
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>

                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>
                                <td class="border year"></td>


                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="text-right border"></td>
                                <td class="text-right border"></td>
                                <td class="text-right border"></td>

                                
                                <td class="border layout"></td>
                                <td class="border checkbox-td"></td>

                                <td class="checkbox-td border"></td>
                                <td class="checkbox-td border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border"></td>
                                <td class="border checkbox-td"></td>
                                {{-- <td class="border"></td> --}}
                                <td class="border"></td>
                                <td class="border"></td>
                            </tr>
                            {{-- Free Space End --}}
                            {{--row 35--}}
                            <tr>
                               <th class="seprate">&nbsp;</th>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td class="mietvertrag">&nbsp;</td>
                               
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>

                               
                               <td class="layout">&nbsp;</td>
                               <td class="checkbox-td">&nbsp;</td>

                               <td class="checkbox-td">&nbsp;</td>
                               <td class="checkbox-td">&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                           </tr>
                            {{--row 36--}}
                            <tr>
                                 {{--
                                 <td colspan="12">&nbsp;</td>
                                 --}}
                                 <td>&nbsp;</td>
                                 
                                 <td class="checkbox-td">&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td class="mietvertrag">&nbsp;</td>
                                 
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>

                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>
                                 <td class="year">&nbsp;</td>



                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>

                                 
                                 <td class="layout">&nbsp;</td>
                                 <td>&nbsp;</td>

                                 <td class="checkbox-td">&nbsp;</td>
                                 <td class="checkbox-td">&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>

                            </tr>
                            {{--row 37--}}
                            <tr>
                               <th class="border bg-gray">IST-NME</th>
                               <th class="border bg-gray text-center checkbox-td">p.m.</th>
                               <th class="border bg-gray text-center">p.a.</th>
                               <th class="border bg-gray">Anteil an IST-NME</th>
                               <th></th>
                               <th class="border bg-gray">Flächenaufteilung</th>
                               <th class="border bg-gray text-center">Objekt Insgesamt</th>
                               <th></th>
                               <th class=""></th>
                               <td></td>
                               <td></td>
                               <th class="text-center"></th>
                               <td></td>
                               <td></td>
                            </tr>
                            {{--row 38--}}
                            <tr>
                               <td class="border">Gewerbe</td>
                               <td class="border text-right checkbox-td">{{number_format($tenancy_schedule->calculations['total_business_actual_net_rent'], 2,",",".")}}€</td>
                               <td class="border text-right">{{number_format(12 * $tenancy_schedule->calculations['total_business_actual_net_rent'], 2,",",".")}}€</td>
                               <td class="border text-right">{{($tenancy_schedule->calculations['total_actual_net_rent']!=0)? number_format($tenancy_schedule->calculations['total_business_actual_net_rent']/ $tenancy_schedule->calculations['total_actual_net_rent'] * 100, 1,",",".") : 0}}%</td>
                               <td></td>
                               <td class="border">Gewerbe in qm</td>
                               <td class="border text-right">{{number_format($tenancy_schedule->calculations['mi9'], 2,",",".")}}</td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                            </tr>
                            {{--row 39--}}
                            <tr>
                               <td class="border " >Wohnen</td>
                               <td class="border text-right checkbox-td">{{number_format($tenancy_schedule->calculations['total_live_actual_net_rent'], 2,",",".")}}€</td>
                               <td class="border text-right">{{number_format(12 * $tenancy_schedule->calculations['total_live_actual_net_rent'], 2,",",".")}}€</td>
                               <td class="border text-right">{{($tenancy_schedule->calculations['total_actual_net_rent']!=0) ? number_format($tenancy_schedule->calculations['total_live_actual_net_rent']/ $tenancy_schedule->calculations['total_actual_net_rent'] * 100, 1,",",".") : 0}}%</td>
                               <td></td>
                               <td class="border">Wohnen in qm</td>
                               <td class="border text-right">{{number_format($tenancy_schedule->calculations['mi10'], 2,",",".")}}</td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                               <td></td>
                            </tr>
                            {{--row 40--}}
                            <tr>
                               <td >&nbsp;</td>
                               <th class="border text-right bg-gray checkbox-td">{{number_format($tenancy_schedule->calculations['total_actual_net_rent'], 2,",",".")}}€</th>
                               <th class="border text-right bg-gray">{{number_format($tenancy_schedule->calculations['total_actual_net_rent'] * 12, 2,",",".")}}€</th>
                               <td class="border text-right">100%</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td class="border text-right bg-gray">{{number_format($tenancy_schedule->calculations['total_rental_space'], 2,",",".")}}</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                            </tr>
                            {{--row 41--}}
                            <tr>
                               <td>&nbsp;</td>
                               <td class="checkbox-td">&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                            </tr>
                            {{--row 42--}}
                            <tr>
                               <th class="border bg-gray" >Leerstand bewertet</th>
                               <th class="border bg-gray text-center checkbox-td">p.m.</th>
                               <th class="border bg-gray text-center">p.a.</th>
                               <th></th>
                               <th></th>
                               <th class="border bg-gray">Ø €/m² Gewerbe</th>
                               <th class="border bg-gray">Ø €/m² Wohnen</th>
                               <th></th>
                               <th></th>
                               <th></th>
                               <th></th>
                               <td></td>
                               <td></td>
                            </tr>
                            {{--row 43--}}
                            <tr>
                                <td class="border">Gewerbe</td>
                                <td class="border text-right checkbox-td">{{number_format($tenancy_schedule->calculations['total_business_vacancy_in_eur'], 2,",",".")}}€</td>
                                <td class="border text-right">{{number_format(12 * $tenancy_schedule->calculations['total_business_vacancy_in_eur'], 2,",",".")}}€</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <th class="border text-center">
                                    @if( ( $tenancy_schedule->calculations['total_business_rental_space']) != 0 )
                                        {{ number_format($tenancy_schedule->calculations['total_business_actual_net_rent'] / $tenancy_schedule->calculations['total_business_rental_space'], 2,",",".") }}
                                    @else
                                        0
                                    @endif
                                </th>
                                <th class="border text-center">
                                    @if( ( $tenancy_schedule->calculations['total_live_rental_space']) != 0 )
                                        {{ number_format($tenancy_schedule->calculations['total_live_actual_net_rent'] / $tenancy_schedule->calculations['total_live_rental_space'], 2,",",".") }}
                                    @else
                                        0
                                    @endif
                                </th>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            {{--row 44--}}
                            <tr>
                               <td class="border">Wohnen</td>
                               <td class="border text-right checkbox-td">{{number_format($tenancy_schedule->calculations['total_live_vacancy_in_eur'], 2,",",".")}}€</td>
                               <td class="border text-right">{{number_format(12 * $tenancy_schedule->calculations['total_live_vacancy_in_eur'], 2,",",".")}}€</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                            </tr>
                            {{--row 45--}}
                            <tr>
                                <td>&nbsp;</td>
                                <th class="border text-right bg-gray checkbox-td">
                                    {{number_format($tenancy_schedule->calculations['potenzial_eur_monat'], 2,",",".")}}€
                                </th>
                                <th class="border text-right bg-gray">
                                    {{number_format($tenancy_schedule->calculations['potenzial_eur_jahr'], 2,",",".")}}€
                                </th>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <th class="border bg-gray">
                                    Restlaufzeit Gesamt
                                    <!--<strong style="color:red; font-size: 16px;">
                                       Leerstandsquote:
                                       @if( ( $tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10'] ) != 0 )
                                           {{ number_format( ($tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $tenancy_schedule->calculations['total_business_vacancy_in_qm']) / ($tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10']) * 100, 2 ) }}
                                       @else
                                           0
                                       @endif
                                   </strong> -->
                               </th>
                               <th class="border bg-gray">WAULT</th>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                               <td>&nbsp;</td>
                           </tr>
                           {{--row 46--}}
                           <tr>
                                <td>&nbsp;</td>
                                <td class="checkbox-td">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="border text-center">{{number_format($sum_total_amount, 2,",",".")}}€</td>
                                <th class="border text-center">
                                    @if($tenancy_schedule->calculations['wault'])
                                        {{number_format($tenancy_schedule->calculations['wault'],1)}}
                                    @else
                                        @isset($tenancy_schedule)
                                            {{($tenancy_schedule->calculations['total_business_actual_net_rent']!=0)?number_format(($tenancy_schedule->calculations['total_remaining_time_in_eur'])/(12 * $tenancy_schedule->calculations['total_business_actual_net_rent']),1) : 0 }}
                                        @endisset
                                    @endif
                                </th>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            {{--row 47--}}
                            <tr>
                               <th class="border" colspan="4">Leerstandsquote</th>
                               <th class="border text-center">
                                    @if( ( $tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10'] ) != 0 )
                                        {{ number_format( ($tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $tenancy_schedule->calculations['total_business_vacancy_in_qm']) / ($tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10']) * 100, 2 ) }}%
                                    @else
                                        0
                                    @endif
                                </th>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <form action="{{url('tenancy-schedules/delete/$tenancy_schedule->id')}}" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-danger btn-sm" style="margin-top: 20px; color:white; float: left" onclick="return confirm('Are you sure?')">{{__('forecast.delete')}}</button>
                                    </form>
                                    <button type="button" class="btn btn-primary btn-sm btn-export-tenancy-to-excel" style="color:white; margin-top: 20px; margin-left: 20px"  data-tenancy-id="{{$tenancy_schedule->id}}">{{__('forecast.export')}}</button>
                                    <!-- <a href="{{url('/tenant/pdf/'.$id)}}" class="btn btn-primary btn-sm btn-export-tenancy-to-excel" style="color:white; margin-top: 20px; margin-left: 5px"  data-tenancy-id="{{$id}}">PDF</a> -->
                                    <button type="button" class="btn btn-primary btn-sm btn-export-tenancy-to-pdf" style="color:white; margin-top: 20px;" >PDF</button>
                                </td>
                                <td class="checkbox-td"></td>
                                <td class="checkbox-td">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach


        <div id="rent_paid" class="tab-pane fade @if(request()->get('selecting_tenancy_schedule') == 'rent_paid') in active @endif">

          @include('properties.templates.default_payer')
    
            <div class="row white-box">
  
              <hr/>

                @if($rent_paid_excel)
                <?php
                $download_path = "https://drive.google.com/drive/u/2/folders/".$rent_paid_excel->file_basename;
                if($rent_paid_excel->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".$rent_paid_excel->file_basename;
                }
                ?>
                <!-- <a href="{{$download_path}}">{{$rent_paid_excel->file_name}}</a> -->
                @endif

                <!-- <a href="{{ route('delete_rent_paid_data',['property_id' => $id]) }}" class="btn btn-primary" onclick="return confirm('Are you sure to delete information?')">Delete Information</a> -->

                <h3>Forderungsmanagment</h3>

                <div class="row">
                  <div class="col-sm-6"></div>
                  <div class="col-sm-6" style="margin-bottom: 10px;">
                        <?php
                        $r_month = date('m');
                        $year = range(2018,date('Y'));
                        $months = range(1,12);
                         ?>
                         <select name="upload_month"  class="form-control hidden" id="year-select">
                           <option value="">Alle</option>
                            @foreach($months as $list)
                               <option @if($list == $r_month) selected @endif value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                            @endforeach
                         </select>

                    </div>
                </div>

                <div id="rent-paid-data"></div>
                <div id="rent-paid-data-summery"></div>

            </div>
        </div>

        <form id="export-tenancy-to-excel-form" method="post" action="{{url('property/export-tenancy-to-excel')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="tenancy_data" id="tenancy-data" value="">
            <input type="hidden" name="tenancy_class" id="tenancy-class" value="">
            <input type="hidden" name="tenancy_td" id="tenancy-td" value="">
            <input type="hidden" name="tenancy_id" id="tenancy-id" value="">
        </form>

        <form id="export-tenancy-to-pdf-form" method="post" action="{{url('property/export-tenancy-to-pdf')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <?php
                $name_of_property = "";
                if($propertiescheckd){
                    $name_of_property = $propertiescheckd->name_of_property;
                }
            ?>
            <input type="hidden" name="property_id" id="tproperty_id" value="{{$id}}">
            <input type="hidden" name="property_name" value="{{ isset($name_of_property) ? $name_of_property : '' }}">
        </form>
    </div>
</div>


<div class=" modal fade" role="dialog" id="comment_modal">
    <div class="modal-dialog modal-dialog-centered " >

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Kommentare</h4>
            </div>
            <form action="{{ url('tenancy-schedule-items/store-comments') }}" method="post">
                <div class="modal-body">

                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" id="item" name="item_id" value="">

                    <label>Kommentare</label>
                    <textarea id="com" class="form-control" name="comment" required></textarea>
                    <div class="modal-footer">
                        <button type="submit" class=" btn btn-primary" >Posten</button>
                    </div>
                    <br>
                    <div style="height: 300px ;overflow-y: auto"  class="appenddata "></div>
                </div>
            </form>
        </div>

    </div>
</div>



<div class=" modal fade" role="dialog" id="commentTwo_modal">
    <div class="modal-dialog modal-dialog-centered " >

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Kommentare</h4>
            </div>
            <form action="{{ url('tenancy-schedule-items/store-Comment2') }}" method="post">
                <div class="modal-body">

                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" id="itemId" name="item_id" value="">

                    <label>Kommentare</label>
                    <textarea id="com" class="form-control" name="external_comment" required></textarea>
                    <div class="modal-footer">
                        <button type="submit" class=" btn btn-primary" >Posten</button>
                    </div>
                    <br>
                    <div style="height: 300px ;overflow-y: auto"  class="appendcomment2 "></div>
                </div>
            </form>
        </div>

    </div>
</div>

<div class=" modal fade" role="dialog" id="rent-paid-month-wise-detail-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" id="rent-paid-month-wise-detail-div">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>
        $(".year").hide();
        $('.btn-year').on('click', function () {
          $(".year").toggle();
        });

        $('.commentAdd').on('click', function () {
            $(".appenddata").empty();
            var id = $(this).attr('data-id');
            $('#item').val(id);

            $.ajax({
                type : 'GET',
                url : "{{url('tenancy-schedule-items/get-comments') }}",
                data : { id : id,  _token : '{{ csrf_token() }}' },
                success : function (data) {
                    $.each(data, function(key) {
                        if(data[key].comment != "") {
                            $('.appenddata').append(
                                '<div><b style="font-weight:700">' + data[key].user_name + '</b></div>' +
                                '<div>' + data[key].comment +
                                '<a href="' + '/delete-comment' + '/' + data[key].id + '/item/' + data[key].item_id + '" class="btn-sm btn-danger" style="float:right" data-type="" data-pk="" data-url="" data-title="">Löschen</a>'+
                                '<div style="font-size: 11px"><b style="font-weight:700">'+'Erstellt am : '+'</b>'+data[key].created_at+'</div>'+
                                '</div><hr>'
                                );
                        }
                    });
                }
            });

            $.ajax({
                type : 'GET',
                url : "{{url('tenancy-schedule-items/find') }}",
                data : { id : id,  _token : '{{ csrf_token() }}' },
                success : function (data) {
                    if(data.comment != null) {

                        $('.appenddata').append(
                            '<div>' + data.comment +'</div><hr>'
                            );
                    }
                }
            });
        });

        $('.commentTwo').on('click', function () {
            $(".appendcomment2").empty();
            var id = $(this).attr('data-id');
            $('#itemId').val(id);
            $.ajax({
                type : 'GET',
                url : "{{url('tenancy-schedule-items/get-comments') }}",
                data : { id : id,  _token : '{{ csrf_token() }}' },
                success : function (data) {
                    console.log(data);
                    $.each(data, function(key) {
                       if(data[key].external_comment != ""){
                           $('.appendcomment2').append(
                               '<div><b style="font-weight:700">'+data[key].user_name+'</b></div>'+
                               '<div>'+data[key].external_comment+
                               '<a href="'+'/delete-comment'+'/'+ data[key].id+'/item/'+data[key].item_id+'" class="btn-sm btn-danger" style="float:right" data-type="" data-pk="" data-url="" data-title="">Löschen</a>' +
                               '<div style="font-size: 11px"><b style="font-weight:700">'+'Erstellt am : '+'</b>'+data[key].created_at+'</div>'+
                               '</div><hr>'
                               );
                       }

                   });
                }
            });
            $.ajax({
                type : 'GET',
                url : "{{url('tenancy-schedule-items/find') }}",
                data : { id : id,  _token : '{{ csrf_token() }}' },
                success : function (data) {
                    if(data.comment2 != null) {

                        $('.appendcomment2').append(
                            '<div>' + data.comment2 +'</div><hr>'
                            );
                    }
                }
            });
        });

        window.onload = function () {
            $('#scroll2').scroll( function () {
               $('.test').css("top", $('#scroll2').scrollTop());
           });
        };

        var lastScrollLeft = 0;
        $('tbody').scroll(function() {
            var documentScrollLeft = $('tbody').scrollLeft();
            console.log(documentScrollLeft);
            if (lastScrollLeft != documentScrollLeft) {
                console.log('active');
                $(".seprate").addClass("active");
            }
            if(documentScrollLeft == 0){
                console.log('disable');
                $(".seprate").removeClass("active");
            }
        });

        $(document).ready(function() {
            $('tbody').scroll(function(e) { //detect a scroll event on the tbody
                /*
              Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
              of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain 			it's relative position at the left of the table.
              */
                $('thead').css("left", -$("tbody").scrollLeft()); //fix the thead relative to the body scrolling
                $('thead th:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first cell of the header
                $('tbody th:nth-child(1)').css("left", $("tbody").scrollLeft()); //fix the first column of tdbody
            });
        });

        jQuery(document).ready(function($) {
            $('.checkbox-is-new').on('click', function () {
                var id = $(this).attr('data-id');
                v = 0;
                if($(this).is(':checked'))
                    v = 1;

                $.ajax({
                    type : 'POST',
                    url : "{{url('tenancy-schedule-items/update') }}/"+id,
                    data : { pk : 'is_new', value:v,  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                            // generate_option_from_json(data, 'country_to_state');
                        }
                    });
            });
            $('.checkbox-is-vacant').on('click', function () {
                var id = $(this).attr('data-id');
                v = 0;
                if($(this).is(':checked'))
                    v = 1;

                $.ajax({
                    type : 'POST',
                    url : "{{url('tenancy-schedule-items/update') }}/"+id,
                    data : { pk : 'vacancy_on_purcahse', value:v,  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                        // generate_option_from_json(data, 'country_to_state');
                    }
                });
            });

            $('.change-asset-user').on('change', function () {
                var id = $(this).attr('data-id');
                var pk = $(this).attr('data-pk');
                v = $(this).val();
                $.ajax({
                    type : 'POST',
                    url : "{{url('tenancy-schedule-items/update') }}/"+id,
                    data : { pk : pk, value:v,  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                            // generate_option_from_json(data, 'country_to_state');
                        }
                    });
            });

            $('.option-check').on('click', function () {
                var id = $(this).attr('data-id');
                v = -1;
                if($(this).is(':checked'))
                    v = $(this).val();

                $.ajax({
                    type : 'POST',
                    url : "{{url('tenancy-schedule-items/update') }}/"+id,
                    data : { pk : 'selected_option', value:v,  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                            // generate_option_from_json(data, 'country_to_state');
                        }
                    });

            });


            $('.warning-price-check').on('click', function () {
                var id = $(this).attr('data-id');
                v = 'NULL';
                if($(this).is(':checked'))
                    v = $(this).val();

                var pk = $(this).attr('data-pk');

                $.ajax({
                    type : 'POST',
                    url : "{{url('tenancy-schedule-items/update') }}/"+id,
                    data : { pk : pk, value:v,  _token : '{{ csrf_token() }}' },
                    success : function (data) {
                            // generate_option_from_json(data, 'country_to_state');
                        }
                    });

            });



            $('.btn-export-tenancy-to-pdf').on('click', function () {
                $('#export-tenancy-to-pdf-form').submit();
            });


            $('.btn-export-tenancy-to-excel').on('click', function () {
                var tenancyScheduleID = $(this).data('tenancy-id');
                $('input#tenancy-id').val(tenancyScheduleID);
                var selector = 'div#tenancy-schedule-' + tenancyScheduleID + ' table';
                var tenancyTableArray = getTableData(selector);

                $('input#tenancy-data').val(JSON.stringify(tenancyTableArray));
                    // console.log(JSON.stringify(tenancyTableArray));
                    tenancyTableArray = getTableClass(selector);

                    $('input#tenancy-class').val(JSON.stringify(tenancyTableArray));

                    tenancyTableArray = getTableTdClass(selector);
                    $('input#tenancy-td').val(JSON.stringify(tenancyTableArray));




                    $('#export-tenancy-to-excel-form').submit();
                });


            var tenancySchedulesTable = $('.tenancy-schedules table');
            if (isNaN($.cookie('tenancy-schedule-zoom-value'))){
                $.removeCookie('tenancy-schedule-zoom-value');
            }

            var zoomValue = 1;
            if ($.cookie('tenancy-schedule-zoom-value')){
                zoomValue = parseFloat(parseFloat($.cookie('tenancy-schedule-zoom-value')).toFixed(1));
            }
            else {
                $.cookie('tenancy-schedule-zoom-value', 1);
            }

            $('span#tenancy-schedule-zoom-value').text((zoomValue * 100).toFixed(0));
            tenancySchedulesTable.css({
                'zoom'              : zoomValue,
                '-webkit-transform' : 'scale(' + zoomValue + ')',
                '-moz-transform'    : 'scale(' + zoomValue + ')',
                '-ms-transform'     : 'scale(' + zoomValue + ')',
                '-o-transform'      : 'scale(' + zoomValue + ')',
                'transform'         : 'scale(' + zoomValue + ')',
                'transform-origin'	: 'top left'
            });

            $('#tenacy-schedule-zoom-in').on('click', function () {
                zoomValue = parseFloat(parseFloat($.cookie('tenancy-schedule-zoom-value')).toFixed(1));
                if (zoomValue < 2.0) {
                    zoomValue += 0.1;
                    $.cookie('tenancy-schedule-zoom-value', zoomValue);
                }
                $('span#tenancy-schedule-zoom-value').text((zoomValue * 100).toFixed(0));
                tenancySchedulesTable.css({
                    'zoom'              : zoomValue,
                    '-webkit-transform' : 'scale(' + zoomValue + ')',
                    '-moz-transform'    : 'scale(' + zoomValue + ')',
                    '-ms-transform'     : 'scale(' + zoomValue + ')',
                    '-o-transform'      : 'scale(' + zoomValue + ')',
                    'transform'         : 'scale(' + zoomValue + ')',
                    'transform-origin'	: 'top left'
                });
            });

            $('#tenacy-schedule-zoom-out').on('click', function () {
                zoomValue = parseFloat(parseFloat($.cookie('tenancy-schedule-zoom-value')).toFixed(1));
                if (zoomValue >= 0.6) {
                    zoomValue -= 0.1;
                    $.cookie('tenancy-schedule-zoom-value', zoomValue);
                }
                $('span#tenancy-schedule-zoom-value').text((zoomValue * 100).toFixed(0));
                tenancySchedulesTable.css({
                    'zoom'              : zoomValue,
                    '-webkit-transform' : 'scale(' + zoomValue + ')',
                    '-moz-transform'    : 'scale(' + zoomValue + ')',
                    '-ms-transform'     : 'scale(' + zoomValue + ')',
                    '-o-transform'      : 'scale(' + zoomValue + ')',
                    'transform'         : 'scale(' + zoomValue + ')',
                    'transform-origin'	: 'top left'
                });
            });
        });

        $('#import').submit(function(event) {
            if (!$('#no_opos_status').is(':checked') && $('#default_payer_gdrive_file_upload2').val() == ''){
                event.preventDefault();
                toastr.error("Fehler- Keine Datei gefunden");
            }

        });

        $('#year-select').change(function(){
            /*var month = $(this).val();
            $.ajax({
                url: '<?php echo e(route('get_rent_paid_data', ['property_id' => $properties->id])); ?>',
                data:{
                  month:month
                }
                }).done(function (data) {
                  $('#rent-paid-data').html(data);
                    var columns = [
                      null,
                      null,
                      null,
                      { "type": "numeric-comma"},
                      { "type": "numeric-comma"},
                      null,
                      { "type": "numeric-comma"},
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                  ];
                  makeDatatable($('#rent-paid-table'), columns);
            });*/
            getRentPaidData();
        });

        $('body').on('click', '#isKeineOposChecked', function() {
            var id = $(this).attr('data-id');
            v = 0;
            if ($(this).is(':checked')) v = 1;
            $.ajax({
                type: 'POST',
                url: '{{ url('keine-opos-update') }}' ,
                data: {
                    id: id,
                    value: v,
                    _token: _token
                },
                success: function(data) {
                    sweetAlert("Updated  Successfully");

                }
            });
        });

        $('body').on('keyup', '#input-search-tenancy-schedule', function(){
          var search = $('#input-search-tenancy-schedule').val().toLowerCase();
          $("#tenancy-schedule-table .item-tr").filter(function() {
              var item_name = $(this).find('a[data-pk="name"]').text().toLowerCase();
              // item_name = (item_name == 'empty') ? '' : item_name;
              $(this).toggle(item_name.indexOf(search) > -1);
          });
        });
    </script>
@endsection
