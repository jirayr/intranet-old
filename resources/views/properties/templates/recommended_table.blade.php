<style type="text/css">
	/*#recommended-table-1 select {
    	width: min-content;
	}
	#recommended-table-2 select {
    	width: min-content;
	}	*/
</style>
@if ($table == 1)
	<div class="row">
		<div class="col-md-12">
			<a href="{{route('add-new-vacant')}}?property_id={{$property_id}}&type=mv" class="btn btn-success">Neu MV</a>
			<a href="{{route('add-new-vacant')}}?property_id={{$property_id}}&type=vl" class="btn btn-success">Neu VL</a>
		</div>
		<div class="col-md-12 table-responsive">
			<table class="table table-striped" id="recommended-table-1">
				<thead>
					<tr>
						<th style="min-width: 170px;">Mieter</th>
						<th style="min-width: 170px;">Fläche</th>
						<th style="min-width: 75px;">Art</th>
						<th style="min-width: 75px;">Kosten Umbau</th>
						{{-- <th>Einnahmen p.m. Titel</th> --}}
						<th style="min-width: 100px;">Einnahmen p.m.</th>

						<th style="min-width: 75px;">€/m²</th>

						<th style="min-width: 100px;">Laufzeit in Monate</th>
						<th style="min-width: 100px;">Mietfrei in €</th>
						<th style="min-width: 100px;">Jährl. Einnahmen</th>
						<th>Gesamt-einnahmen</th>
						<th>Gesamt-einnahmen netto</th>
						<th>Anhänge</th>
						<th>Kommentar</th>

						<th>Erst. D.</th>
						<th>D. zur Freig.</th>

						<th>Freigabe Falk</th>
						<th>Pending</th>
						<th>Ablehnen</th>
						<th>Aktion</th>
					</tr>
				</thead>
				<tbody>
					@if ($data)
						@foreach ($data as $element)
							@if ($element->is_btn2 == 0 && !$element->not_release_status && isset($detail_arr[$element->id]))

								<?php

									$is_mieter_dp = true;//Mieter Dropdown
									if($element->mv_vl){
										if($element->mv_vl == 'mv'){
											$is_mieter_dp = false;
										}else{
											$is_mieter_dp = true;
										}
									}else{
										if($element->tenant_id){
											$is_mieter_dp = false;
										}else{
											$is_mieter_dp = true;
										}
									}

									if(!isset($detail_arr[$element->id]['mv_vl'])){
										if($element->tenant_id){
											$detail_arr[$element->id]['mv_vl'] = "mv";
										}else{
											$detail_arr[$element->id]['mv_vl'] = "vl";
										}
									}

									$a1 = 0;
				                    if(isset($detail_arr[$element->id]['amount1']) && $detail_arr[$element->id]['amount1'])
				                      $a1 = $detail_arr[$element->id]['amount1'];

				                    $a2 = 0;
				                    if(isset($detail_arr[$element->id]['amount2']) && $detail_arr[$element->id]['amount2'])
				                      $a2 = $detail_arr[$element->id]['amount2'];

				                    $a3 = $a1*12;
				                    // if(isset($detail_arr[$element->id]['annual_revenue']) && $detail_arr[$element->id]['annual_revenue'])
				                    	// $a3 = $detail_arr[$element->id]['annual_revenue'];

				                    $a4 = $a1*$a2;

				                    $a5 = 0;
				                    if(isset($detail_arr[$element->id]['amount5']) && $detail_arr[$element->id]['amount5'])
				                      $a5 = $detail_arr[$element->id]['amount5'];

				                    $a6 = 0;
				                    if(isset($detail_arr[$element->id]['amount6']) && $detail_arr[$element->id]['amount6'])
				                      $a6 = $detail_arr[$element->id]['amount6'];

				                  	$total = 0;
				                  	if(isset($ractivity[$element->id])){
		                          		foreach($ractivity[$element->id] as $k => $list){
		                          			$total +=$list->amount;
		                          		}
		                          	}
								?>

								<?php 
									$btn1 = $btn2 = 'btn btn-primary vacant-release-request';

									$btn1_type = "btn1_request";
									$btn2_type = "btn2_request";
									$rbutton_title1 = "Zur Freigabe an Janine senden";
									$rbutton_title2 = "Zur Freigabe an Falk senden";

									$email = strtolower($user->email);

									if($email=="j.klausch@fcr-immobilien.de")
									{	
										$btn1 =  'btn btn-primary vacant-release-request';
										$rbutton_title1 = "Freigeben";
										$btn1_type = "btn1";

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2_request']))
										{
										  $btn2 =  " btn-success vacant-release-request";
										  $rbutton_title2 = "Zur Freigabe gesendet";
										}
									}
									elseif($email==config('users.falk_email'))
									{
										$rbutton_title2 = "Freigeben";
										$btn2 = 'btn btn-primary vacant-release-request';
										$btn2_type = "btn2";

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1_request']))
										{
										  $btn1 =  " btn-success vacant-release-request";
										  $rbutton_title1 = "Zur Freigabe gesendet";
										}
									}
									else{
										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1_request']))
										{
										  $btn1 =  " btn-success vacant-release-request";
										  $rbutton_title1 = "Zur Freigabe gesendet";
										}

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2_request']))
										{
										  $btn2 =  " btn-success vacant-release-request";
										  $rbutton_title2 = "Zur Freigabe gesendet";
										}
									}

									$r1 = $r2 = "";
									if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1']))
									{
									  $btn1 =  " btn-success";
									  $rbutton_title1 = "Freigegeben";
									  $r1 = "Janine: ".show_date_format($detail_date_arr[$element->id]['btn1']);
									}

									if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2']))
									{
									  $btn2 =  " btn-success";
									  $rbutton_title2 = "Freigegeben";
									  $r2 = "Falk: ".show_date_format($detail_date_arr[$element->id]['btn2']);
									}
								?>

								<tr>
									<td>
										@if(!$is_mieter_dp)
											{{-- inout --}}
											<input type="text" class="form-control change-comment-recommended" data-id="{{$element->id}}" data-column="tenant_name" placeholder="Mieter" value="{{ ( isset($detail_arr[$element->id]['tenant_name']) ) ? $detail_arr[$element->id]['tenant_name'] : '' }}">
										@else
											{{-- <input type="text" class="form-control change-comment-recommended" data-id="{{$element->id}}" data-column="tenant_name_text" placeholder="Mieter" value="{{ ( isset($detail_arr[$element->id]['tenant_name_text']) ) ? $detail_arr[$element->id]['tenant_name_text'] : '' }}"><br> --}}
											{{-- dropdown --}}
											<select class="form-control change-comment-recommended" data-column="tenant_name" data-id="{{$element->id}}">
						                      	<option value="">Select</option>
						                      	@if($rented_list)
							                      	@foreach($rented_list as $key => $row)
							                      		<option value="{{$key}}" {{ ( isset($detail_arr[$element->id]['tenant_name']) && $detail_arr[$element->id]['tenant_name'] == $key) ? 'selected' : '' }} >{{$row['name'] }} {{ ($row['date']) ? '('.$row['date'].')' : '' }} </option>
							                      	@endforeach
							                    @endif
						                    </select>
						                    <!-- <a href="javascript:void(0);" data-title="Mieter" class="show_item_detail">Show Detail</a> -->
										@endif
									</td>

									@php
										$r_size = 0;
										if( isset($detail_arr[$element->id]['rental_space']) &&  $detail_arr[$element->id]['rental_space'] )
											$r_size = (isset($area[$detail_arr[$element->id]['rental_space']])) ? $area[$detail_arr[$element->id]['rental_space']]['size'] : 0;
									@endphp

									<td>
										<select class="form-control rental_space change-comment-recommended" data-column="rental_space" data-id="{{$element->id}}">
					                      <option value="">Select</option>
					                      @if($area)
					                      	@foreach ($area as $area_id => $area_value)
					                      		<option value="{{ $area_id }}" data-size={{ $area_value['size'] }} {{ (isset($detail_arr[$element->id]['rental_space']) && $detail_arr[$element->id]['rental_space'] == $area_id) ? 'selected' : '' }} >{{ $area_value['area'] }}</option>
					                      	@endforeach
					                      @endif
					                    </select>
					                    <a href="javascript:void(0);" data-title="Fläche" class="show_item_detail">Mehr Infos</a>
									</td>
									<td>
										<select class="form-control change-comment-recommended" data-column="mv_vl" data-id="{{$element->id}}">
					                      <option value="">Select</option>
					                      <option value="mv" {{ ($detail_arr[$element->id]['mv_vl'] == "mv") ? 'selected' : '' }}>MV</option>
					                      <option value="vl" {{ ($detail_arr[$element->id]['mv_vl'] == "vl") ? 'selected' : '' }}>VL</option>
					                    </select>
									</td>
									<td class="text-right">
										<a href="javascript:void(0);" data-property-id="{{ $property_id }}" data-tenant-id="{{ $element->id }}" class="kosten_umbau">{{number_format($total,2,',','.')}}</a>
									</td>
									{{-- <td>
										<input type="text" class="form-control change-comment-recommended revenue_title" data-id="{{$element->id}}" data-column="title1" value="{{ (isset($detail_arr[$element->id]['title1'])) ? $detail_arr[$element->id]['title1'] : '' }}">
									</td> --}}
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended revenue_amount" data-id="{{$element->id}}" data-column="amount1" value="{{number_format($a1,2,',','.')}}">
									</td>
									@php
										$m = ($r_size) ? ($a1 / $r_size) : 0;
									@endphp
									<td class="m_size">
										{{number_format($m,2,',','.')}}
									</td>
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended laufzeit" data-id="{{$element->id}}" data-column="amount2" value="{{number_format($a2,2,',','.')}}">
									</td>
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended Mietfrei in €" data-id="{{$element->id}}" data-column="amount5" value="{{number_format($a5,2,',','.')}}">
									</td>
									<td class="text-right jahrl_einnahmen">
										{{-- <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended jährl_Einnahmen p.m." data-id="{{$element->id}}" data-column="annual_revenue" value="{{number_format($a3,2,',','.')}}"> --}}
										{{number_format($a3,2,',','.')}}
									</td>
									<td class="text-right Gesamteinnahmen">
										{{number_format($a4,2,',','.')}}
									</td>
									<td class="text-right Gesamteinnahmen_netto">
										{{number_format($a4-$total-$a5,2,',','.')}}
									</td>
									<td>
										<div class="uploaded-files uploaded-files-empfehlung2 rowt-{{$element->id}}" style="float:left">
					                          <a href="javascript:void(0);" class="link-button-empfehlung2"  id="empfehlung_file_link_{{$element->id}}" data-empfehlung_id="{{$element->id}}" data-property_id="{{$property_id}}" data-tenant_id="{{$element->id}}" >
					                            <i class="fa fa-link"></i>
					                          </a>
					                          @if($element->files)
					                            @foreach($element->files as $file)
					                              @if($file->file_type == 'file')
					                                <a href="{{ $file->file_href }}"  target="_blank" title="{{ $file->file_name }}">
					                                <i extension="filemanager.file_icon_array.{{$file->extension}}" class="fa {{ config('filemanager.file_icon_array.' . $file->extension) ?: 'fa-file' }}" ></i>{{ (isset($file->file_name)) ? $file->file_name : '' }}
					                                </a>
					                              @else
					                                <a href="javascript:void(0);" title="{{ $file->file_name }}" onClick="loadDirectoryFiles('{{ $file->file_path }}');"  id="empfehlung-gdrive-link-{{ $file->id }}"  ><i class="fa fa-folder" ></i></a>
					                              @endif
					                            @endforeach
					                          @endif 
					                    </div>
									</td>
									<td>
										@php
											$comments = DB::table('empfehlung_comments as ec')->select('ec.id','ec.comment', 'ec.created_at', 'u.name', 'u.role', 'u.company')
							                        ->join('users as u', 'u.id', 'ec.user_id')
							                        ->where('ec.tenant_id', $element->id)
							                        ->orderBy('ec.created_at', 'desc')
							                        ->limit(2)->get();
										@endphp

										@if($comments && count($comments))
											<div class="show_rec_cmnt_section">
												@foreach ($comments as $comment)
													@php
														$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
														$commented_user = $comment->name.''.$company;
													@endphp
													<div class="comment-content hideContent"><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</div>
													<div class="show-more">
												        <a href="#">Show more</a>
												    </div>
												@endforeach
											</div>
											<a href="javascript:void(0);" data-url="{{ route('get_recommended_comment', ['id' => $element->id]) }}" class="load_rec_comment_section" data-closest="td">Show More</a>
										@endif
										{{-- <p>{!! nl2br($comment) !!}</p> --}}
										{{-- <p class="recommended_last_comment">{!! nl2br($comment) !!}</p> --}}
										<button type="button" class="btn btn-primary btn-xs recommended-comment" data-id="{{$element->id}}">Kommentar</button>
										
									</td>

									<td>{{ show_date_format($element->created_at) }}</td>
									<td>
										
										@php
											if($detail_date_arr[$element->id] && isset($detail_date_arr[$element->id]['btn2_request'])){
												echo show_date_format($detail_date_arr[$element->id]['btn2_request']);
											}
										@endphp

									</td>

									<td>
										<button type="button" class="btn {{$btn2}}" data-column="{{$btn2_type}}" data-id="{{ $element->id }}"> {{$rbutton_title2}}</button>{{$r2}}</h3>
									</td>
									<td>
										<button type="button" class="btn btn-primary btn-recommended-pending" data-id="{{ $element->id }}" data-url="{{ route('vacant_mark_as_pending', ['vacant_id' => $element->id]) }}">Pending</button>
									</td>
									<td>
										<button type="button" class="btn btn-primary btn-recommended-not-release" data-id="{{ $element->id }}" data-url="{{ route('vacant_mark_as_not_release') }}?tenant_id={{ $element->id }}">Ablehnen</button>
									</td>
									<td>
										<button type="button" data-id="{{ $element->id }}" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-vacant"><i class="icon-trash"></i></button>
									</td>
								</tr>
							@endif
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
@endif

@if ($table == 2)
	<div class="row">
		<div class="col-md-12 table-responsive">
			<table class="table table-striped" id="recommended-table-2">
				<thead>
					<tr>
						<th style="min-width: 170px;">Mieter</th>
						<th style="min-width: 170px;">Fläche</th>
						<th style="min-width: 75px;">Art</th>
						<th style="min-width: 75px;">Kosten Umbau</th>
						{{-- <th>Einnahmen p.m. Titel</th> --}}
						<th style="min-width: 100px;">Einnahmen p.m.</th>

						<th style="min-width: 75px;">€/m²</th>

						<th style="min-width: 100px;">Laufzeit in Monate</th>
						<th style="min-width: 100px;">Mietfrei in €</th>
						<th style="min-width: 100px;">Jährl. Einnahmen</th>
						<th>Gesamt-einnahmen</th>
						<th>Gesamt-einnahmen netto</th>
						<th>Anhänge</th>
						<th>Kommentar</th>

						<th>Erst. D.</th>
						<th>D. zur Freig.</th>

						<th>Freigabe Falk</th>
						<th>Aktion</th>
					</tr>
				</thead>
				<tbody>
					@if ($data)
						@foreach ($data as $element)
							@if ($element->is_btn2 == 1)

								<?php 
									$is_mieter_dp = true;//Mieter Dropdown
									if($element->mv_vl){
										if($element->mv_vl == 'mv'){
											$is_mieter_dp = false;
										}else{
											$is_mieter_dp = true;
										}
									}else{
										if($element->tenant_id){
											$is_mieter_dp = false;
										}else{
											$is_mieter_dp = true;
										}
									}

									if(!isset($detail_arr[$element->id]['mv_vl'])){
										if($element->tenant_id){
											$detail_arr[$element->id]['mv_vl'] = "mv";
										}else{
											$detail_arr[$element->id]['mv_vl'] = "vl";
										}
									}

									$a1 = 0;
				                    if(isset($detail_arr[$element->id]['amount1']) && $detail_arr[$element->id]['amount1'])
				                      $a1 = $detail_arr[$element->id]['amount1'];

				                    $a2 = 0;
				                    if(isset($detail_arr[$element->id]['amount2']) && $detail_arr[$element->id]['amount2'])
				                      $a2 = $detail_arr[$element->id]['amount2'];

				                    $a3 = $a1*12;
				                    // if(isset($detail_arr[$element->id]['annual_revenue']) && $detail_arr[$element->id]['annual_revenue'])
				                    	// $a3 = $detail_arr[$element->id]['annual_revenue'];

				                    $a4 = $a1*$a2;

				                    $a5 = 0;
				                    if(isset($detail_arr[$element->id]['amount5']) && $detail_arr[$element->id]['amount5'])
				                      $a5 = $detail_arr[$element->id]['amount5'];

				                    $a6 = 0;
				                    if(isset($detail_arr[$element->id]['amount6']) && $detail_arr[$element->id]['amount6'])
				                      $a6 = $detail_arr[$element->id]['amount6'];

				                  	$total = 0;
				                  	if(isset($ractivity[$element->id])){
		                          		foreach($ractivity[$element->id] as $k => $list){
		                          			$total +=$list->amount;
		                          		}
		                          	}
								?>

								<?php 
									$btn1 = $btn2 = 'btn btn-primary vacant-release-request';

									$btn1_type = "btn1_request";
									$btn2_type = "btn2_request";
									$rbutton_title1 = "Zur Freigabe an Janine senden";
									$rbutton_title2 = "Zur Freigabe an Falk senden";

									$email = strtolower($user->email);

									if($email=="j.klausch@fcr-immobilien.de")
									{	
										$btn1 =  'btn btn-primary vacant-release-request';
										$rbutton_title1 = "Freigeben";
										$btn1_type = "btn1";

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2_request']))
										{
										  $btn2 =  " btn-success vacant-release-request";
										  $rbutton_title2 = "Zur Freigabe gesendet";
										}
									}
									elseif($email==config('users.falk_email'))
									{
										$rbutton_title2 = "Freigeben";
										$btn2 = 'btn btn-primary vacant-release-request';
										$btn2_type = "btn2";

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1_request']))
										{
										  $btn1 =  " btn-success vacant-release-request";
										  $rbutton_title1 = "Zur Freigabe gesendet";
										}
									}
									else{
										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1_request']))
										{
										  $btn1 =  " btn-success vacant-release-request";
										  $rbutton_title1 = "Zur Freigabe gesendet";
										}

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2_request']))
										{
										  $btn2 =  " btn-success vacant-release-request";
										  $rbutton_title2 = "Zur Freigabe gesendet";
										}
									}

									$r1 = $r2 = "";
									if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1']))
									{
									  $btn1 =  " btn-success";
									  $rbutton_title1 = "Freigegeben";
									  $r1 = "Janine: ".show_date_format($detail_date_arr[$element->id]['btn1']);
									}

									if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2']))
									{
									  $btn2 =  " btn-success";
									  $rbutton_title2 = "Freigegeben";
									  $r2 = "Falk: ".show_date_format($detail_date_arr[$element->id]['btn2']);
									}
								?>

								<tr>
									<td>
										@if(!$is_mieter_dp)
											{{-- inout --}}
											<input type="text" class="form-control change-comment-recommended" data-id="{{$element->id}}" data-column="tenant_name" placeholder="Mieter" value="{{ ( isset($detail_arr[$element->id]['tenant_name']) ? $detail_arr[$element->id]['tenant_name'] : '' ) }}" readonly>
										@else
											{{-- <input type="text" class="form-control change-comment-recommended" data-id="{{$element->id}}" data-column="tenant_name_text" placeholder="Mieter" value="{{ ( isset($detail_arr[$element->id]['tenant_name_text']) ) ? $detail_arr[$element->id]['tenant_name_text'] : '' }}" readonly><br> --}}
											{{-- dropdown --}}
											<select class="form-control change-comment-recommended" data-column="tenant_name" data-id="{{$element->id}}" disabled>
						                      	<option value="">Select</option>
						                      	@if($rented_list)
							                      	@foreach($rented_list as $key => $row)
							                      		<option value="{{$key}}" {{ ( isset($detail_arr[$element->id]['tenant_name']) && $detail_arr[$element->id]['tenant_name'] == $key) ? 'selected' : '' }} >{{$row['name'] }} {{ ($row['date']) ? '('.$row['date'].')' : '' }} </option>
							                      	@endforeach
							                    @endif
						                    </select>
						                    <!-- <a href="javascript:void(0);" data-title="Mieter" class="show_item_detail">Show Detail</a> -->
										@endif
									</td>

									@php
										$r_size = 0;
										if( isset($detail_arr[$element->id]['rental_space']) &&  $detail_arr[$element->id]['rental_space'] )
											$r_size = (isset($area[$detail_arr[$element->id]['rental_space']])) ? $area[$detail_arr[$element->id]['rental_space']]['size'] : 0;
									@endphp

									<td>
										<select class="form-control change-comment-recommended" data-column="rental_space" data-id="{{$element->id}}" disabled>
					                      <option value="">Select</option>
					                      @if($area)
					                      	@foreach ($area as $area_id => $area_value)
					                      		<option value="{{ $area_id }}" data-size={{ $area_value['size'] }} {{ (isset($detail_arr[$element->id]['rental_space']) && $detail_arr[$element->id]['rental_space'] == $area_id) ? 'selected' : '' }} >{{  $area_value['area'] }}</option>
					                      	@endforeach
					                      @endif
					                    </select>
					                    <a href="javascript:void(0);" data-title="Fläche" class="show_item_detail">Mehr Infos</a>
									</td>
									<td>
										<select class="form-control change-comment-recommended" data-column="mv_vl" data-id="{{$element->id}}" disabled>
					                      <option value="">Select</option>
					                      <option value="mv" {{ ($detail_arr[$element->id]['mv_vl'] == "mv") ? 'selected' : '' }}>MV</option>
					                      <option value="vl" {{ ($detail_arr[$element->id]['mv_vl'] == "vl") ? 'selected' : '' }}>VL</option>
					                    </select>
									</td>
									<td class="text-right kosten_umbau_td">
										<span>{{number_format($total,2,',','.')}}</span>
										<a href="javascript:void(0);" data-property-id="{{ $property_id }}" data-tenant-id="{{ $element->id }}" class="kosten_umbau hidden">{{number_format($total,2,',','.')}}</a>
									</td>
									{{-- <td>
										<input type="text" class="form-control change-comment-recommended revenue_title" data-id="{{$element->id}}" data-column="title1" value="{{ (isset($detail_arr[$element->id]['title1'])) ? $detail_arr[$element->id]['title1'] : '' }}" readonly>
									</td> --}}
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended revenue_amount" data-id="{{$element->id}}" data-column="amount1" value="{{number_format($a1,2,',','.')}}" readonly>
									</td>

									@php
										$m = ($r_size) ? ($a1 / $r_size) : 0;
									@endphp
									<td>
										{{number_format($m,2,',','.')}}
									</td>

									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$element->id}}" data-column="amount2" value="{{number_format($a2,2,',','.')}}" readonly>
									</td>
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$element->id}}" data-column="amount5" value="{{number_format($a5,2,',','.')}}" readonly>
									</td>
									<td class="text-right">
										{{-- <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended jährl_Einnahmen p.m." data-id="{{$element->id}}" data-column="annual_revenue" value="{{number_format($a3,2,',','.')}}" readonly> --}}
										{{number_format($a3,2,',','.')}}
									</td>
									<td class="text-right">
										{{number_format($a4,2,',','.')}}
									</td>
									<td class="text-right">
										{{number_format($a4-$total-$a5,2,',','.')}}
									</td>
									<td>
										<div class="uploaded-files uploaded-files-empfehlung2 rowt-{{$element->id}}" style="float:left">
					                          <a href="javascript:void(0);" class="link-button-empfehlung2"  id="empfehlung_file_link_{{$element->id}}" data-empfehlung_id="{{$element->id}}" data-property_id="{{$property_id}}" data-tenant_id="{{$element->id}}" >
					                            <i class="fa fa-link"></i>
					                          </a>
					                          @if($element->files)
					                            @foreach($element->files as $file)
					                              @if($file->file_type == 'file')
					                                <a href="{{ $file->file_href }}"  target="_blank" title="{{ $file->file_name }}">
					                                <i extension="filemanager.file_icon_array.{{$file->extension}}" class="fa {{ config('filemanager.file_icon_array.' . $file->extension) ?: 'fa-file' }}" ></i>{{ (isset($file->file_name)) ? $file->file_name : '' }}
					                                </a>
					                              @else
					                                <a href="javascript:void(0);" title="{{ $file->file_name }}" onClick="loadDirectoryFiles('{{ $file->file_path }}');"  id="empfehlung-gdrive-link-{{ $file->id }}"  ><i class="fa fa-folder" ></i></a>
					                              @endif
					                            @endforeach
					                          @endif 
					                    </div>
									</td>
									<td>
										@php
											$comments = DB::table('empfehlung_comments as ec')->select('ec.id','ec.comment', 'ec.created_at', 'u.name', 'u.role', 'u.company')
							                        ->join('users as u', 'u.id', 'ec.user_id')
							                        ->where('ec.tenant_id', $element->id)
							                        ->orderBy('ec.created_at', 'desc')
							                        ->limit(2)->get();
										@endphp

										@if($comments && count($comments))
											<div class="show_rec_cmnt_section">
												@foreach ($comments as $comment)
													@php
														$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
														$commented_user = $comment->name.''.$company;
													@endphp
													<div class="comment-content hideContent"><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</div>
													<div class="show-more">
												        <a href="#">Show more</a>
												    </div>
												@endforeach
											</div>
											<a href="javascript:void(0);" data-url="{{ route('get_recommended_comment', ['id' => $element->id]) }}" class="load_rec_comment_section" data-closest="td">Show More</a>
										@endif
										{{-- <span>{!! nl2br($comment) !!}</span> --}}
										{{-- <span class="recommended_last_comment">{!! nl2br($element->last_comment) !!}</span> --}}
										<button type="button" class="btn btn-primary btn-xs recommended-comment" data-id="{{$element->id}}">Kommentar</button>
									</td>

									<td>{{ show_date_format($element->created_at) }}</td>
									<td>
										
										@php
											if($detail_date_arr[$element->id] && isset($detail_date_arr[$element->id]['btn2_request'])){
												echo show_date_format($detail_date_arr[$element->id]['btn2_request']);
											}
										@endphp

									</td>

									<td>
										<button type="button" class="btn {{$btn2}}" data-column="{{$btn2_type}}" data-id="{{ $element->id }}"> {{$rbutton_title2}}</button>{{$r2}}</h3>
									</td>
									<td>
										@if($user->email == config('users.falk_email'))
											<button type="button" class="btn btn-info btn-outline btn-circle btn-sm btn-edit-recommended"><i class="icon-pencil"></i></button>
										@endif
									</td>
								</tr>
							@endif
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
@endif

@if ($table == 3)
	<div class="row">
		<div class="col-md-12 table-responsive">
			<table class="table table-striped" id="recommended-table-3">
				<thead>
					<tr>
						<th style="min-width: 170px;">Mieter</th>
						<th style="min-width: 170px;">Fläche</th>
						<th style="min-width: 75px;">Art</th>
						<th style="min-width: 75px;">Kosten Umbau</th>
						{{-- <th>Einnahmen p.m. Titel</th> --}}
						<th style="min-width: 100px;">Einnahmen p.m.</th>

						<th style="min-width: 75px;">€/m²</th>

						<th style="min-width: 100px;">Laufzeit in Monate</th>
						<th style="min-width: 100px;">Mietfrei in €</th>
						<th style="min-width: 100px;">Jährl. Einnahmen</th>
						<th>Gesamt-einnahmen</th>
						<th>Gesamt-einnahmen netto</th>
						<th>Anhänge</th>
						<th>Kommentar</th>

						<th>Erst. D.</th>
						<th>D. zur Freig.</th>

						<th>Freigabe Falk</th>
						{{-- <th>Pending</th> --}}
						<th>Aktion</th>
					</tr>
				</thead>
				<tbody>
					@if ($data)
						@foreach ($data as $element)
							@if ($element->is_btn2 == 0 && $element->not_release_status == 2 && isset($detail_arr[$element->id]))

								<?php

									$is_mieter_dp = true;//Mieter Dropdown
									if($element->mv_vl){
										if($element->mv_vl == 'mv'){
											$is_mieter_dp = false;
										}else{
											$is_mieter_dp = true;
										}
									}else{
										if($element->tenant_id){
											$is_mieter_dp = false;
										}else{
											$is_mieter_dp = true;
										}
									}

									if(!isset($detail_arr[$element->id]['mv_vl'])){
										if($element->tenant_id){
											$detail_arr[$element->id]['mv_vl'] = "mv";
										}else{
											$detail_arr[$element->id]['mv_vl'] = "vl";
										}
									}

									$a1 = 0;
				                    if(isset($detail_arr[$element->id]['amount1']) && $detail_arr[$element->id]['amount1'])
				                      $a1 = $detail_arr[$element->id]['amount1'];

				                    $a2 = 0;
				                    if(isset($detail_arr[$element->id]['amount2']) && $detail_arr[$element->id]['amount2'])
				                      $a2 = $detail_arr[$element->id]['amount2'];

				                    $a3 = $a1*12;
				                    // if(isset($detail_arr[$element->id]['annual_revenue']) && $detail_arr[$element->id]['annual_revenue'])
				                    	// $a3 = $detail_arr[$element->id]['annual_revenue'];

				                    $a4 = $a1*$a2;

				                    $a5 = 0;
				                    if(isset($detail_arr[$element->id]['amount5']) && $detail_arr[$element->id]['amount5'])
				                      $a5 = $detail_arr[$element->id]['amount5'];

				                    $a6 = 0;
				                    if(isset($detail_arr[$element->id]['amount6']) && $detail_arr[$element->id]['amount6'])
				                      $a6 = $detail_arr[$element->id]['amount6'];

				                  	$total = 0;
				                  	if(isset($ractivity[$element->id])){
		                          		foreach($ractivity[$element->id] as $k => $list){
		                          			$total +=$list->amount;
		                          		}
		                          	}
								?>

								<?php 
									$btn1 = $btn2 = 'btn btn-primary vacant-release-request';

									$btn1_type = "btn1_request";
									$btn2_type = "btn2_request";
									$rbutton_title1 = "Zur Freigabe an Janine senden";
									$rbutton_title2 = "Zur Freigabe an Falk senden";

									$email = strtolower($user->email);

									if($email=="j.klausch@fcr-immobilien.de")
									{	
										$btn1 =  'btn btn-primary vacant-release-request';
										$rbutton_title1 = "Freigeben";
										$btn1_type = "btn1";

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2_request']))
										{
										  $btn2 =  " btn-success vacant-release-request";
										  $rbutton_title2 = "Zur Freigabe gesendet";
										}
									}
									elseif($email==config('users.falk_email'))
									{
										$rbutton_title2 = "Freigeben";
										$btn2 = 'btn btn-primary vacant-release-request';
										$btn2_type = "btn2";

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1_request']))
										{
										  $btn1 =  " btn-success vacant-release-request";
										  $rbutton_title1 = "Zur Freigabe gesendet";
										}
									}
									else{
										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1_request']))
										{
										  $btn1 =  " btn-success vacant-release-request";
										  $rbutton_title1 = "Zur Freigabe gesendet";
										}

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2_request']))
										{
										  $btn2 =  " btn-success vacant-release-request";
										  $rbutton_title2 = "Zur Freigabe gesendet";
										}
									}

									$r1 = $r2 = "";
									if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1']))
									{
									  $btn1 =  " btn-success";
									  $rbutton_title1 = "Freigegeben";
									  $r1 = "Janine: ".show_date_format($detail_date_arr[$element->id]['btn1']);
									}

									if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2']))
									{
									  $btn2 =  " btn-success";
									  $rbutton_title2 = "Freigegeben";
									  $r2 = "Falk: ".show_date_format($detail_date_arr[$element->id]['btn2']);
									}
								?>

								<tr>
									<td>
										@if(!$is_mieter_dp)
											{{-- inout --}}
											<input type="text" class="form-control change-comment-recommended" data-id="{{$element->id}}" data-column="tenant_name" placeholder="Mieter" value="{{ ( isset($detail_arr[$element->id]['tenant_name']) ) ? $detail_arr[$element->id]['tenant_name'] : '' }}">
										@else
											{{-- <input type="text" class="form-control change-comment-recommended" data-id="{{$element->id}}" data-column="tenant_name_text" placeholder="Mieter" value="{{ ( isset($detail_arr[$element->id]['tenant_name_text']) ) ? $detail_arr[$element->id]['tenant_name_text'] : '' }}"><br> --}}
											{{-- dropdown --}}
											<select class="form-control change-comment-recommended" data-column="tenant_name" data-id="{{$element->id}}">
						                      	<option value="">Select</option>
						                      	@if($rented_list)
							                      	@foreach($rented_list as $key => $row)
							                      		<option value="{{$key}}" {{ ( isset($detail_arr[$element->id]['tenant_name']) && $detail_arr[$element->id]['tenant_name'] == $key) ? 'selected' : '' }} >{{$row['name'] }} {{ ($row['date']) ? '('.$row['date'].')' : '' }} </option>
							                      	@endforeach
							                    @endif
						                    </select>
						                    <!-- <a href="javascript:void(0);" data-title="Mieter" class="show_item_detail">Show Detail</a> -->
										@endif
									</td>

									@php
										$r_size = 0;
										if( isset($detail_arr[$element->id]['rental_space']) &&  $detail_arr[$element->id]['rental_space'] )
											$r_size = (isset($area[$detail_arr[$element->id]['rental_space']])) ? $area[$detail_arr[$element->id]['rental_space']]['size'] : 0;
									@endphp

									<td>
										<select class="form-control rental_space change-comment-recommended" data-column="rental_space" data-id="{{$element->id}}">
					                      <option value="">Select</option>
					                      @if($area)
					                      	@foreach ($area as $area_id => $area_value)
					                      		<option value="{{ $area_id }}" data-size={{ $area_value['size'] }} {{ (isset($detail_arr[$element->id]['rental_space']) && $detail_arr[$element->id]['rental_space'] == $area_id) ? 'selected' : '' }} >{{ $area_value['area'] }}</option>
					                      	@endforeach
					                      @endif
					                    </select>
					                    <a href="javascript:void(0);" data-title="Fläche" class="show_item_detail">Mehr Infos</a>
									</td>
									<td>
										<select class="form-control change-comment-recommended" data-column="mv_vl" data-id="{{$element->id}}">
					                      <option value="">Select</option>
					                      <option value="mv" {{ ($detail_arr[$element->id]['mv_vl'] == "mv") ? 'selected' : '' }}>MV</option>
					                      <option value="vl" {{ ($detail_arr[$element->id]['mv_vl'] == "vl") ? 'selected' : '' }}>VL</option>
					                    </select>
									</td>
									<td class="text-right">
										<a href="javascript:void(0);" data-property-id="{{ $property_id }}" data-tenant-id="{{ $element->id }}" class="kosten_umbau">{{number_format($total,2,',','.')}}</a>
									</td>
									{{-- <td>
										<input type="text" class="form-control change-comment-recommended revenue_title" data-id="{{$element->id}}" data-column="title1" value="{{ (isset($detail_arr[$element->id]['title1'])) ? $detail_arr[$element->id]['title1'] : '' }}">
									</td> --}}
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended revenue_amount" data-id="{{$element->id}}" data-column="amount1" value="{{number_format($a1,2,',','.')}}">
									</td>
									@php
										$m = ($r_size) ? ($a1 / $r_size) : 0;
									@endphp
									<td class="m_size">
										{{number_format($m,2,',','.')}}
									</td>
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended laufzeit" data-id="{{$element->id}}" data-column="amount2" value="{{number_format($a2,2,',','.')}}">
									</td>
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended Mietfrei in €" data-id="{{$element->id}}" data-column="amount5" value="{{number_format($a5,2,',','.')}}">
									</td>
									<td class="text-right jahrl_einnahmen">
										{{-- <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended jährl_Einnahmen p.m." data-id="{{$element->id}}" data-column="annual_revenue" value="{{number_format($a3,2,',','.')}}"> --}}
										{{number_format($a3,2,',','.')}}
									</td>
									<td class="text-right Gesamteinnahmen">
										{{number_format($a4,2,',','.')}}
									</td>
									<td class="text-right Gesamteinnahmen_netto">
										{{number_format($a4-$total-$a5,2,',','.')}}
									</td>
									<td>
										<div class="uploaded-files uploaded-files-empfehlung2 rowt-{{$element->id}}" style="float:left">
					                          <a href="javascript:void(0);" class="link-button-empfehlung2"  id="empfehlung_file_link_{{$element->id}}" data-empfehlung_id="{{$element->id}}" data-property_id="{{$property_id}}" data-tenant_id="{{$element->id}}" >
					                            <i class="fa fa-link"></i>
					                          </a>
					                          @if($element->files)
					                            @foreach($element->files as $file)
					                              @if($file->file_type == 'file')
					                                <a href="{{ $file->file_href }}"  target="_blank" title="{{ $file->file_name }}">
					                                <i extension="filemanager.file_icon_array.{{$file->extension}}" class="fa {{ config('filemanager.file_icon_array.' . $file->extension) ?: 'fa-file' }}" ></i>{{ (isset($file->file_name)) ? $file->file_name : '' }}
					                                </a>
					                              @else
					                                <a href="javascript:void(0);" title="{{ $file->file_name }}" onClick="loadDirectoryFiles('{{ $file->file_path }}');"  id="empfehlung-gdrive-link-{{ $file->id }}"  ><i class="fa fa-folder" ></i></a>
					                              @endif
					                            @endforeach
					                          @endif 
					                    </div>
									</td>
									<td>
										@php
											$comments = DB::table('empfehlung_comments as ec')->select('ec.id','ec.comment', 'ec.created_at', 'u.name', 'u.role', 'u.company')
							                        ->join('users as u', 'u.id', 'ec.user_id')
							                        ->where('ec.tenant_id', $element->id)
							                        ->orderBy('ec.created_at', 'desc')
							                        ->limit(2)->get();
										@endphp

										@if($comments && count($comments))
											<div class="show_rec_cmnt_section">
												@foreach ($comments as $comment)
													@php
														$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
														$commented_user = $comment->name.''.$company;
													@endphp
													<div class="comment-content hideContent"><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</div>
													<div class="show-more">
												        <a href="#">Show more</a>
												    </div>
												@endforeach
											</div>
											<a href="javascript:void(0);" data-url="{{ route('get_recommended_comment', ['id' => $element->id]) }}" class="load_rec_comment_section" data-closest="td">Show More</a>
										@endif
										{{-- <p>{!! nl2br($comment) !!}</p> --}}
										{{-- <p class="recommended_last_comment">{!! nl2br($comment) !!}</p> --}}
										<button type="button" class="btn btn-primary btn-xs recommended-comment" data-id="{{$element->id}}">Kommentar</button>
										
									</td>

									<td>{{ show_date_format($element->created_at) }}</td>
									<td>
										
										@php
											if($detail_date_arr[$element->id] && isset($detail_date_arr[$element->id]['btn2_request'])){
												echo show_date_format($detail_date_arr[$element->id]['btn2_request']);
											}
										@endphp

									</td>

									<td>
										<button type="button" class="btn {{$btn2}}" data-column="{{$btn2_type}}" data-id="{{ $element->id }}"> {{$rbutton_title2}}</button>{{$r2}}</h3>
									</td>
									{{-- <td>
										<button type="button" class="btn btn-primary btn-recommended-pending" data-id="{{ $element->id }}" data-url="{{ route('vacant_mark_as_pending', ['vacant_id' => $element->id]) }}">Pending</button>
									</td> --}}
									<td>
										<button type="button" data-id="{{ $element->id }}" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-vacant"><i class="icon-trash"></i></button>
									</td>
								</tr>
							@endif
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
@endif

@if ($table == 4)
	<div class="row">
		<div class="col-md-12 table-responsive">
			<table class="table table-striped" id="recommended-table-4">
				<thead>
					<tr>
						<th style="min-width: 170px;">Mieter</th>
						<th style="min-width: 170px;">Fläche</th>
						<th style="min-width: 75px;">Art</th>
						<th style="min-width: 75px;">Kosten Umbau</th>
						{{-- <th>Einnahmen p.m. Titel</th> --}}
						<th style="min-width: 100px;">Einnahmen p.m.</th>

						<th style="min-width: 75px;">€/m²</th>

						<th style="min-width: 100px;">Laufzeit in Monate</th>
						<th style="min-width: 100px;">Mietfrei in €</th>
						<th style="min-width: 100px;">Jährl. Einnahmen</th>
						<th>Gesamt-einnahmen</th>
						<th>Gesamt-einnahmen netto</th>
						<th>Anhänge</th>
						<th>Kommentar</th>

						<th>Erst. D.</th>
						<th>D. zur Freig.</th>

						<th>Freigabe Falk</th>
						{{-- <th>Pending</th> --}}
						<th>Aktion</th>
					</tr>
				</thead>
				<tbody>
					@if ($data)
						@foreach ($data as $element)
							@if ($element->is_btn2 == 0 && $element->not_release_status == 1 && isset($detail_arr[$element->id]))

								<?php

									$is_mieter_dp = true;//Mieter Dropdown
									if($element->mv_vl){
										if($element->mv_vl == 'mv'){
											$is_mieter_dp = false;
										}else{
											$is_mieter_dp = true;
										}
									}else{
										if($element->tenant_id){
											$is_mieter_dp = false;
										}else{
											$is_mieter_dp = true;
										}
									}

									if(!isset($detail_arr[$element->id]['mv_vl'])){
										if($element->tenant_id){
											$detail_arr[$element->id]['mv_vl'] = "mv";
										}else{
											$detail_arr[$element->id]['mv_vl'] = "vl";
										}
									}

									$a1 = 0;
				                    if(isset($detail_arr[$element->id]['amount1']) && $detail_arr[$element->id]['amount1'])
				                      $a1 = $detail_arr[$element->id]['amount1'];

				                    $a2 = 0;
				                    if(isset($detail_arr[$element->id]['amount2']) && $detail_arr[$element->id]['amount2'])
				                      $a2 = $detail_arr[$element->id]['amount2'];

				                    $a3 = $a1*12;
				                    // if(isset($detail_arr[$element->id]['annual_revenue']) && $detail_arr[$element->id]['annual_revenue'])
				                    	// $a3 = $detail_arr[$element->id]['annual_revenue'];

				                    $a4 = $a1*$a2;

				                    $a5 = 0;
				                    if(isset($detail_arr[$element->id]['amount5']) && $detail_arr[$element->id]['amount5'])
				                      $a5 = $detail_arr[$element->id]['amount5'];

				                    $a6 = 0;
				                    if(isset($detail_arr[$element->id]['amount6']) && $detail_arr[$element->id]['amount6'])
				                      $a6 = $detail_arr[$element->id]['amount6'];

				                  	$total = 0;
				                  	if(isset($ractivity[$element->id])){
		                          		foreach($ractivity[$element->id] as $k => $list){
		                          			$total +=$list->amount;
		                          		}
		                          	}
								?>

								<?php 
									$btn1 = $btn2 = 'btn btn-primary vacant-release-request';

									$btn1_type = "btn1_request";
									$btn2_type = "btn2_request";
									$rbutton_title1 = "Zur Freigabe an Janine senden";
									$rbutton_title2 = "Zur Freigabe an Falk senden";

									$email = strtolower($user->email);

									if($email=="j.klausch@fcr-immobilien.de")
									{	
										$btn1 =  'btn btn-primary vacant-release-request';
										$rbutton_title1 = "Freigeben";
										$btn1_type = "btn1";

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2_request']))
										{
										  $btn2 =  " btn-success vacant-release-request";
										  $rbutton_title2 = "Zur Freigabe gesendet";
										}
									}
									elseif($email==config('users.falk_email'))
									{
										$rbutton_title2 = "Freigeben";
										$btn2 = 'btn btn-primary vacant-release-request';
										$btn2_type = "btn2";

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1_request']))
										{
										  $btn1 =  " btn-success vacant-release-request";
										  $rbutton_title1 = "Zur Freigabe gesendet";
										}
									}
									else{
										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1_request']))
										{
										  $btn1 =  " btn-success vacant-release-request";
										  $rbutton_title1 = "Zur Freigabe gesendet";
										}

										if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2_request']))
										{
										  $btn2 =  " btn-success vacant-release-request";
										  $rbutton_title2 = "Zur Freigabe gesendet";
										}
									}

									$r1 = $r2 = "";
									if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn1']))
									{
									  $btn1 =  " btn-success";
									  $rbutton_title1 = "Freigegeben";
									  $r1 = "Janine: ".show_date_format($detail_date_arr[$element->id]['btn1']);
									}

									if($detail_arr[$element->id] && isset($detail_arr[$element->id]['btn2']))
									{
									  $btn2 =  " btn-success";
									  $rbutton_title2 = "Freigegeben";
									  $r2 = "Falk: ".show_date_format($detail_date_arr[$element->id]['btn2']);
									}
								?>

								<tr>
									<td>
										@if(!$is_mieter_dp)
											{{-- inout --}}
											<input type="text" class="form-control change-comment-recommended" data-id="{{$element->id}}" data-column="tenant_name" placeholder="Mieter" value="{{ ( isset($detail_arr[$element->id]['tenant_name']) ) ? $detail_arr[$element->id]['tenant_name'] : '' }}">
										@else
											{{-- <input type="text" class="form-control change-comment-recommended" data-id="{{$element->id}}" data-column="tenant_name_text" placeholder="Mieter" value="{{ ( isset($detail_arr[$element->id]['tenant_name_text']) ) ? $detail_arr[$element->id]['tenant_name_text'] : '' }}"><br> --}}
											{{-- dropdown --}}
											<select class="form-control change-comment-recommended" data-column="tenant_name" data-id="{{$element->id}}">
						                      	<option value="">Select</option>
						                      	@if($rented_list)
							                      	@foreach($rented_list as $key => $row)
							                      		<option value="{{$key}}" {{ ( isset($detail_arr[$element->id]['tenant_name']) && $detail_arr[$element->id]['tenant_name'] == $key) ? 'selected' : '' }} >{{$row['name'] }} {{ ($row['date']) ? '('.$row['date'].')' : '' }} </option>
							                      	@endforeach
							                    @endif
						                    </select>
						                    <!-- <a href="javascript:void(0);" data-title="Mieter" class="show_item_detail">Show Detail</a> -->
										@endif
									</td>

									@php
										$r_size = 0;
										if( isset($detail_arr[$element->id]['rental_space']) &&  $detail_arr[$element->id]['rental_space'] )
											$r_size = (isset($area[$detail_arr[$element->id]['rental_space']])) ? $area[$detail_arr[$element->id]['rental_space']]['size'] : 0;
									@endphp

									<td>
										<select class="form-control rental_space change-comment-recommended" data-column="rental_space" data-id="{{$element->id}}">
					                      <option value="">Select</option>
					                      @if($area)
					                      	@foreach ($area as $area_id => $area_value)
					                      		<option value="{{ $area_id }}" data-size={{ $area_value['size'] }} {{ (isset($detail_arr[$element->id]['rental_space']) && $detail_arr[$element->id]['rental_space'] == $area_id) ? 'selected' : '' }} >{{ $area_value['area'] }}</option>
					                      	@endforeach
					                      @endif
					                    </select>
					                    <a href="javascript:void(0);" data-title="Fläche" class="show_item_detail">Mehr Infos</a>
									</td>
									<td>
										<select class="form-control change-comment-recommended" data-column="mv_vl" data-id="{{$element->id}}">
					                      <option value="">Select</option>
					                      <option value="mv" {{ ($detail_arr[$element->id]['mv_vl'] == "mv") ? 'selected' : '' }}>MV</option>
					                      <option value="vl" {{ ($detail_arr[$element->id]['mv_vl'] == "vl") ? 'selected' : '' }}>VL</option>
					                    </select>
									</td>
									<td class="text-right">
										<a href="javascript:void(0);" data-property-id="{{ $property_id }}" data-tenant-id="{{ $element->id }}" class="kosten_umbau">{{number_format($total,2,',','.')}}</a>
									</td>
									{{-- <td>
										<input type="text" class="form-control change-comment-recommended revenue_title" data-id="{{$element->id}}" data-column="title1" value="{{ (isset($detail_arr[$element->id]['title1'])) ? $detail_arr[$element->id]['title1'] : '' }}">
									</td> --}}
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended revenue_amount" data-id="{{$element->id}}" data-column="amount1" value="{{number_format($a1,2,',','.')}}">
									</td>
									@php
										$m = ($r_size) ? ($a1 / $r_size) : 0;
									@endphp
									<td class="m_size">
										{{number_format($m,2,',','.')}}
									</td>
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended laufzeit" data-id="{{$element->id}}" data-column="amount2" value="{{number_format($a2,2,',','.')}}">
									</td>
									<td>
										<input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended Mietfrei in €" data-id="{{$element->id}}" data-column="amount5" value="{{number_format($a5,2,',','.')}}">
									</td>
									<td class="text-right jahrl_einnahmen">
										{{-- <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended jährl_Einnahmen p.m." data-id="{{$element->id}}" data-column="annual_revenue" value="{{number_format($a3,2,',','.')}}"> --}}
										{{number_format($a3,2,',','.')}}
									</td>
									<td class="text-right Gesamteinnahmen">
										{{number_format($a4,2,',','.')}}
									</td>
									<td class="text-right Gesamteinnahmen_netto">
										{{number_format($a4-$total-$a5,2,',','.')}}
									</td>
									<td>
										<div class="uploaded-files uploaded-files-empfehlung2 rowt-{{$element->id}}" style="float:left">
					                          <a href="javascript:void(0);" class="link-button-empfehlung2"  id="empfehlung_file_link_{{$element->id}}" data-empfehlung_id="{{$element->id}}" data-property_id="{{$property_id}}" data-tenant_id="{{$element->id}}" >
					                            <i class="fa fa-link"></i>
					                          </a>
					                          @if($element->files)
					                            @foreach($element->files as $file)
					                              @if($file->file_type == 'file')
					                                <a href="{{ $file->file_href }}"  target="_blank" title="{{ $file->file_name }}">
					                                <i extension="filemanager.file_icon_array.{{$file->extension}}" class="fa {{ config('filemanager.file_icon_array.' . $file->extension) ?: 'fa-file' }}" ></i>{{ (isset($file->file_name)) ? $file->file_name : '' }}
					                                </a>
					                              @else
					                                <a href="javascript:void(0);" title="{{ $file->file_name }}" onClick="loadDirectoryFiles('{{ $file->file_path }}');"  id="empfehlung-gdrive-link-{{ $file->id }}"  ><i class="fa fa-folder" ></i></a>
					                              @endif
					                            @endforeach
					                          @endif 
					                    </div>
									</td>
									<td>
										@php
											$comments = DB::table('empfehlung_comments as ec')->select('ec.id','ec.comment', 'ec.created_at', 'u.name', 'u.role', 'u.company')
							                        ->join('users as u', 'u.id', 'ec.user_id')
							                        ->where('ec.tenant_id', $element->id)
							                        ->orderBy('ec.created_at', 'desc')
							                        ->limit(2)->get();
										@endphp

										@if($comments && count($comments))
											<div class="show_rec_cmnt_section">
												@foreach ($comments as $comment)
													@php
														$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
														$commented_user = $comment->name.''.$company;
													@endphp
													<div class="comment-content hideContent"><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</div>
													<div class="show-more">
												        <a href="#">Show more</a>
												    </div>
												@endforeach
											</div>
											<a href="javascript:void(0);" data-url="{{ route('get_recommended_comment', ['id' => $element->id]) }}" class="load_rec_comment_section" data-closest="td">Show More</a>
										@endif
										{{-- <p>{!! nl2br($comment) !!}</p> --}}
										{{-- <p class="recommended_last_comment">{!! nl2br($comment) !!}</p> --}}
										<button type="button" class="btn btn-primary btn-xs recommended-comment" data-id="{{$element->id}}">Kommentar</button>
										
									</td>

									<td>{{ show_date_format($element->created_at) }}</td>
									<td>
										
										@php
											if($detail_date_arr[$element->id] && isset($detail_date_arr[$element->id]['btn2_request'])){
												echo show_date_format($detail_date_arr[$element->id]['btn2_request']);
											}
										@endphp

									</td>

									<td>
										<button type="button" class="btn {{$btn2}}" data-column="{{$btn2_type}}" data-id="{{ $element->id }}"> {{$rbutton_title2}}</button>{{$r2}}</h3>
									</td>
									{{-- <td>
										<button type="button" class="btn btn-primary btn-recommended-pending" data-id="{{ $element->id }}" data-url="{{ route('vacant_mark_as_pending', ['vacant_id' => $element->id]) }}">Pending</button>
									</td> --}}
									<td>
										<button type="button" data-id="{{ $element->id }}" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-vacant"><i class="icon-trash"></i></button>
									</td>
								</tr>
							@endif
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
@endif

@if ($table == 5)

	<div class="row">
		<div class="col-md-12 table-responsive">
			<table class="table table-striped" id="recommended-table-5">
				<thead>
					<tr>
						<th>Name</th>
						<th>Button</th>
						<th>Kommentar</th>
						<th>Datum</th>
						<th>Aktion</th>
					</tr>
				</thead>
				<tbody>
					@if($log)
						@foreach ($log as $element)
							<tr>
								<td>{{ $element->name }}</td>
								<td>
									<?php 
										if($element->slug == 'delete_vacant'){
											echo 'Löschen';
										}elseif($element->slug == 'btn2'){
											echo 'Freigeben';
										}elseif($element->slug == 'btn2_request'){
											echo 'Zur Freigabe gesendet';
										}elseif($element->slug == 'vacant_pending'){
											echo 'Pending';
										}elseif($element->slug == 'vacant_not_release'){
											echo 'Ablehnen';
										}
									?>
								</td>
								<td>
									{{ ($element->slug != 'delete_vacant') ? $element->value : '' }}
								</td>
								<td>
									{{ show_datetime_format($element->created_at) }}
								</td>
								<td>
									@if($user->email == config('users.falk_email') && $element->user_id == $user->id)
										<button type="button" data-id="{{ $element->id }}" data-tbl="empfehlung_details" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>
									@endif
								</td>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>

@endif