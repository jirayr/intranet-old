<div class="col-md-12 col-lg-12 col-sm-12 {{$provision_visible_class}}">

	<button type="button" class="add-new-provision btn btn-primary">Add New</button>
	

	<div class="row">
		<div class="col-md-12">
			<div class="provision_info-table table-responsive"></div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h2>von Falk freigegeben</h2><br>
			<div class="provision_release_info-table table-responsive"></div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="provision-release-logs"></div>
		</div>
	</div>

</div>