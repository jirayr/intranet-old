
<div class="">
	<div class="modal-dialog modal-lg" style="width: 100%">
		<!-- Modal content-->
	    <div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Gebäude</h4>

			</div>
			<div class="modal-body">
				<div class="row ">
					<button style="margin-left: 2%" type="button" class="btn btn-success btn-xs" onclick="add_versicherungen(1)">Hinzufügen</button>
					@if(Auth::user()->email!=config('users.falk_email'))
					<?php
					if(isset($mails_log_data['building']) || isset($mails_log_data['release_building'])) 
						$clllls = "btn-success";
					 else
					 	$clllls = "btn-primary"; 

					 $button_title = "Zur Freigabe an Falk senden";
					 if(isset($mails_log_data['building']))  
					 	$button_title = "Zur Freigabe an Falk gesendet";

					 if(isset($mails_log_data['release_building']))  
					 	$button_title = "Freigegeben";

					?>
					<button class="btn btn-xs {{$clllls}} @if(!isset($mails_log_data['release_building'])) request-button @endif" data-id="building">{{$button_title}}</button>
					@else
					<button class="btn  btn-xs   @if(!isset($mails_log_data['release_building'])) btn-primary request-button @else btn-success  @endif" data-id="release_building">@if(!isset($mails_log_data['release_building'])) Freigeben @else Freigegeben @endif </button>
					@endif
					<div class="col-sm-12 table-responsive">
						
						<table class="table table-striped" id="user-form-data">
							<thead>
								<tr>
									<th>Name</th>
									<th>Betrag</th>
									<th>Laufzeit von -Laufzeit bis</th>
									<th>Kündigungsfrist</th>
									<th>Kommentar</th>
									<th>AM-Empfehlung</th>
									<th>Falk</th>
									<th>Aktionen</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ $properties->axa }}</td>
									<td>
										<a href="javascript:void(0);" class="insurance-inline" data-type="text" data-pk="gebaude_betrag" data-inputclass="mask-input-number" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" >{{ number_format($properties->gebaude_betrag,2,",",".") }}</a> €
									</td>
									<td>
										<a href="javascript:void(0);" class="insurance-inline" data-type="text" data-pk="gebaude_laufzeit_from" data-inputclass="mask-input-new-date" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" >{{ show_date_format($properties->gebaude_laufzeit_from) }}</a> - <a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="text" data-pk="gebaude_laufzeit_to" data-inputclass="mask-input-new-date" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" >{{ show_date_format($properties->gebaude_laufzeit_to) }}</a>
									</td>
									<td>
										<select class="insurance-notice-period form-control input-sm" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" data-field="gebaude_kundigungsfrist">
											<option>Select</option>
											<option value="3 Monate" {{ ($properties->gebaude_kundigungsfrist == '3 Monate') ? 'selected' : '' }}>3 Monate</option>
											<option value="4 Wochen" {{ ($properties->gebaude_kundigungsfrist == '4 Wochen') ? 'selected' : '' }}>4 Wochen</option>
										</select>
										@if(!in_array($properties->gebaude_kundigungsfrist, ['3 Monate', '4 Wochen']))
											{{ $properties->gebaude_kundigungsfrist }}
										@endif
									</td>
									<td>
										<a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="textarea" data-pk="gebaude_comment" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" >{{ $properties->gebaude_comment }}</a>
									</td>
									<td><input data-column="gebaude" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$properties->id}}" @if($properties->gebaude_is_approved) checked @endif ></td>
									<td><input data-column="gebaude_falk_approved" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$properties->id}}" @if($properties->gebaude_falk_approved) checked @endif ></td>

									
									<td>
										{{-- <a href="javascript:void(0)" onclick="showModalGebäude({{$properties}})"><i class="fa fa-edit"></i> </a> --}}
									</td>

								</tr>
								@if(isset($insurances) && $insurances)
									@foreach($insurances as $list)
										@if($list->type==1)
									<tr>
										<td>{{ $list->name }}</td>
										<td>
											<a href="javascript:void(0);" class="insurance-inline" data-type="text" data-pk="amount" data-inputclass="mask-input-number" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $list->id]) }}" >{{ number_format($list->amount,2,",",".") }}</a> €
										</td>
										<td>
											<a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="text" data-pk="date_from" data-inputclass="mask-input-new-date" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $list->id]) }}" >{{ show_date_format($list->date_from) }}</a> - <a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="text" data-pk="date_to" data-inputclass="mask-input-new-date" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $list->id]) }}" >{{ show_date_format($list->date_to) }}</a>
										</td>
										<td>
											<select class="insurance-notice-period form-control input-sm" data-url="{{ route('update_insurance_by_field', ['id' => $list->id]) }}" data-field="kundigungsfrist">
												<option>Select</option>
												<option value="3 Monate" {{ ($list->kundigungsfrist == '3 Monate') ? 'selected' : '' }}>3 Monate</option>
												<option value="4 Wochen" {{ ($list->kundigungsfrist == '4 Wochen') ? 'selected' : '' }}>4 Wochen</option>
											</select>
											@if(!in_array($list->kundigungsfrist, ['3 Monate', '4 Wochen']))
												{{ $list->kundigungsfrist }}
											@endif

										</td>
										<td>
											<a href="javascript:void(0);" class="insurance-inline" data-type="textarea" data-pk="note" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $list->id]) }}" >{{ $list->note }}</a>
										</td>
										<td><input data-column="insurance" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$list->id}}" @if($list->is_approved) checked @endif></td>
										<td>
											<input data-column="falk" type="checkbox" class="falk-checkbox-is-checked" data-id="{{$list->id}}" @if($list->is_checked) checked @endif>
										</td>
										<td>
											{{-- <a href="javascript:void(0)" onclick="showModalInsouranceModal({{$list}})"><i class="fa fa-edit"></i> </a> --}}
											<a href="javascript:void(0)" class="delete-record" data-id="{{$list->id}}"><i class="fa fa-trash"></i> </a>
										</td>

									</tr>
										@endif
									@endforeach
								@endif

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-dialog modal-lg" style="width: 100%">
		<!-- Modal content-->
	    <div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Haftpflicht</h4>
			</div>
			<div class="modal-body">
				<div class="row ">
					<button style="margin-left: 2%" type="button" class="btn btn-success btn-xs" onclick="add_versicherungen(2)">Hinzufügen</button>


					@if(Auth::user()->email!=config('users.falk_email'))
					<?php
					if(isset($mails_log_data['liability']) || isset($mails_log_data['release_liability'])) 
						$clllls = "btn-success";
					 else
					 	$clllls = "btn-primary"; 


					 $button_title = "Zur Freigabe an Falk senden";
					 if(isset($mails_log_data['liability']))  
					 	$button_title = "Zur Freigabe an Falk gesendet";

					 if(isset($mails_log_data['release_liability']))  
					 	$button_title = "Freigegeben";

					?>
					<button class="btn  btn-xs {{$clllls}} @if(!isset($mails_log_data['release_liability'])) request-button @endif" data-id="liability">{{$button_title}}</button>
					@else
					<button class="btn  btn-xs   @if(!isset($mails_log_data['release_liability'])) btn-primary request-button @else btn-success  @endif" data-id="release_liability">@if(!isset($mails_log_data['release_liability'])) Freigeben @else Freigegeben @endif </button>
					@endif


					<div class="col-sm-12 table-responsive">

						<table class="table table-striped" id="user-form-data1">
							<thead>
								<tr>
									<th>Name</th>
									<th>Betrag</th>
									<th>Laufzeit von -Laufzeit bis</th>
									<th>Kündigungsfrist</th>
									<th>Kommentar</th>
									<th>AM-Empfehlung</th>
									<th>Falk</th>
									<th>Aktionen</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{{ $properties->allianz }}</td>
									<td>
										<a href="javascript:void(0);" class="insurance-inline" data-type="text" data-pk="haftplicht_betrag" data-inputclass="mask-input-number" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" >{{ number_format($properties->haftplicht_betrag,2,",",".") }}</a> €
									</td>
									<td>
										<a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="text" data-pk="haftplicht_laufzeit_from" data-inputclass="mask-input-new-date" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" >{{ show_date_format($properties->haftplicht_laufzeit_from) }}</a> - <a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="text" data-pk="haftplicht_laufzeit_to" data-inputclass="mask-input-new-date" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" >{{ show_date_format($properties->haftplicht_laufzeit_to) }}</a>
									</td>
									<td>
										<select class="insurance-notice-period form-control input-sm" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" data-field="haftplicht_kundigungsfrist">
											<option>Select</option>
											<option value="3 Monate" {{ ($properties->haftplicht_kundigungsfrist == '3 Monate') ? 'selected' : '' }}>3 Monate</option>
											<option value="4 Wochen" {{ ($properties->haftplicht_kundigungsfrist == '4 Wochen') ? 'selected' : '' }}>4 Wochen</option>
										</select>
										@if(!in_array($properties->haftplicht_kundigungsfrist, ['3 Monate', '4 Wochen']))
											{{ $properties->haftplicht_kundigungsfrist }}
										@endif
									</td>
									<td>
										<a href="javascript:void(0);" class="insurance-inline" data-type="textarea" data-pk="haftplicht_comment" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" >{{ $properties->haftplicht_comment }}</a>
									</td>
									<td><input data-column="falk" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$properties->id}}" @if($properties->haftplicht_is_approved) checked @endif></td>
									<td><input data-column="haftplicht_falk_approved" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$properties->id}}" @if($properties->haftplicht_falk_approved) checked @endif ></td>
									<td>
										{{-- <a  href="javascript:void(0)" onclick="showModalHaftplicht({{$properties}})"><i class="fa fa-edit"></i> </a> --}}
									</td>
								</tr>
								@if(isset($insurances) && $insurances)
									@foreach($insurances as $list)
										@if($list->type==2)
											<tr>
												<td>{{ $list->name }}</td>
												<td>
													<a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="text" data-pk="amount" data-inputclass="mask-input-number" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $list->id]) }}" >{{ number_format($list->amount,2,",",".") }}</a> €
												</td>
												<td>
													<a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="text" data-pk="date_from" data-inputclass="mask-input-new-date" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $list->id]) }}" >{{ show_date_format($list->date_from) }}</a> - <a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="text" data-pk="date_to" data-inputclass="mask-input-new-date" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $list->id]) }}" >{{ show_date_format($list->date_to) }}</a>
												</td>
												<td>
													<select class="insurance-notice-period form-control input-sm" data-url="{{ route('update_insurance_by_field', ['id' => $list->id]) }}" data-field="kundigungsfrist">
														<option>Select</option>
														<option value="3 Monate" {{ ($list->kundigungsfrist == '3 Monate') ? 'selected' : '' }}>3 Monate</option>
														<option value="4 Wochen" {{ ($list->kundigungsfrist == '4 Wochen') ? 'selected' : '' }}>4 Wochen</option>
													</select>
													@if(!in_array($list->kundigungsfrist, ['3 Monate', '4 Wochen']))
														{{ $list->kundigungsfrist }}
													@endif

												</td>
												<td>
													<a href="javascript:void(0);" class="inline-edit insurance-inline" data-type="textarea" data-pk="note" data-placement="bottom" data-url="{{ route('update_insurance_by_field', ['id' => $properties->id]) }}" >{{ $properties->note }}</a>
												</td>
												<td><input data-column="insurance" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$list->id}}" @if($list->is_approved) checked @endif ></td>
												<td>
													<input data-column="falk" type="checkbox" class="falk-checkbox-is-checked" data-id="{{$list->id}}" @if($list->is_checked) checked @endif>
												</td>
												<td>
													{{-- <a href="javascript:void(0)" onclick="showModalInsouranceModal({{$list}})"><i class="fa fa-edit"></i> </a> --}}
													<a href="javascript:void(0)" class="delete-record" data-id="{{$list->id}}"><i class="fa fa-trash"></i> </a>
												</td>

											</tr>
										@endif
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	@php
		$user = Auth::user();
	@endphp

	<div class="tenancy-schedules" style="display: block;">
	<h3 style="margin-top: 40px;">Gebäude Verlauf</h3>
	<div class="clearfix"></div>
  <table class="forecast-table" >
    <thead>
      <tr>
        <th class="border bg-gray">Name</th>
        <th class="border bg-gray">Button</th>
        <th class="border bg-gray">Kommentar</th>
        <th class="border bg-gray">Datum</th>
        <th class="border bg-gray">Aktion</th>
      </tr>
    </thead>
    <tbody>
    	
      @foreach($mails_logs as $k=>$list)
      
      <?php
      $tname = "";
      $u = DB::table('users')->where('id', $list['user_id'])->first();
      if($u)
        $tname = $u->name;

      $rbutton_title = "";
      if($list['type']=="building")
      	$rbutton_title = "Gebäude: Zur Freigabe an Falk gesendet";
      if($list['type']=="release_building")
        $rbutton_title = "Gebäude: Freigegeben ";
      

      ?>
      @if($rbutton_title)
      <tr>
        <td class="border">{{$tname}}</td>
        <td class="border">{{$rbutton_title}}</td>
        <td class="border">{{$list['comment']}}</td>
        <td class="border">{{show_datetime_format($list['created_at'])}}</td>
        <td class="border">
        	@if($user->email == config('users.falk_email') && $list['user_id'] == $user->id)
        		<button type="button" data-id="{{ $list['id'] }}" data-tbl="properties_mail_logs" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>
        	@endif
        </td>
      </tr>
      @endif
      @endforeach
    </tbody>
  </table>
	</div>


	<div class="tenancy-schedules" style="display: block;">
	<h3 style="margin-top: 40px;">Haftpflicht Verlauf</h3>
	<div class="clearfix"></div>
  <table class="forecast-table" >
    <thead>
      <tr>
        <th class="border bg-gray">Name</th>
        <th class="border bg-gray">Button</th>
        <th class="border bg-gray">Kommentar</th>
        <th class="border bg-gray">Datum</th>
        <th class="border bg-gray">Aktion</th>
      </tr>
    </thead>
    <tbody>
      @foreach($mails_logs as $k=>$list)
      
      <?php
      $tname = "";
      $u = DB::table('users')->where('id', $list['user_id'])->first();
      if($u)
        $tname = $u->name;

      $rbutton_title = "";
      if($list['type']=="liability")
        $rbutton_title = "Haftpflicht: Zur Freigabe an Falk gesendet";
      if($list['type']=="release_liability")
        $rbutton_title = "Haftpflicht: Freigegeben";


      ?>
      @if($rbutton_title)
      <tr>
        <td class="border">{{$tname}}</td>
        <td class="border">{{$rbutton_title}}</td>
        <td class="border">{{$list['comment']}}</td>
        <td class="border">{{show_datetime_format($list['created_at'])}}</td>
        <td class="border">
        	@if($user->email == config('users.falk_email') && $list['user_id'] == $user->id)
        		<button type="button" data-id="{{ $list['id'] }}" data-tbl="properties_mail_logs" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-log"><i class="icon-trash"></i></button>
        	@endif
        </td>
      </tr>
      @endif
      @endforeach
    </tbody>
  </table>
	</div>

</div>


<div class=" modal fade" role="dialog" id="edit_versicherungen">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit</h4>
			</div>
			<form action="{{ url('/update_versicherung') }}" method="post">
				<div class="modal-body">

					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$properties->id}}">
					<input type="hidden" name="versicherungen_type" value="" id="versicherungen_type">
					<input type="hidden" name="versicherungen_insurance_id" value="" id="versicherungen_insurance_id">


					<label>Name</label>
					<input id="versicherunge_name"  type="text"  name="versicherunge_name" class="form-control" required>
					<br>

					<label>Betrag</label>
					<input id="versicherunge_betrag" type="text" name="versicherunge_betrag" class="form-control">
					<br>

					<label>Laufzeit von</label>
					<input id="versicherunge_laufzeit" style="height: 38px ;width: 100%"  type="text" name="versicherunge_laufzeit" class="form-control">
					<br>

					<label>Laufzeit bis</label>
					<input id="versicherunge_laufzeit_end"  style="height: 38px ;width: 100%"  type="text" name="versicherunge_laufzeit_end" class="form-control">
					<br>

					<label>Kündigungsfrist</label>
					{{-- <input id="versicherunge_kündigungsfrist" type="text" name="versicherunge_kündigungsfrist" class="form-control"> --}}
					<select id="versicherunge_kündigungsfrist" name="versicherunge_kündigungsfrist" class="form-control">
						<option value="">Not Selected</option>
						<option value="3 Monate">3 Monate</option>
						<option value="4 Wochen">4 Wochen</option>
					</select>
					<span id="versicherunge_kündigungsfrist_dup"></span>
					<br><br>

					<label>Kommentar</label>
					<textarea class="form-control" id="versicherunge_kommentar" name="versicherunge_kommentar"></textarea>
					<br>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Speichern</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>
<div class=" modal fade" role="dialog" id="add_versicherungen">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Hinzufügen</h4>
			</div>
			<form action="{{ url('/add_versicherung') }}" method="post">
				<div class="modal-body">

					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$properties->id}}">
					<input type="hidden" name="versicherungen_type" value="" id="add_versicherungen_type">


					<label>Name</label>
					<input   type="text"  name="versicherunge_name" class="form-control" required>
					<br>

					<label>Betrag</label>
					<input  type="text" name="versicherunge_betrag" class="form-control">
					<br>

					<label>Laufzeit von</label>
					<input  style="height: 38px ;width: 100%"  type="text" name="versicherunge_laufzeit" class="form-control versicherunge_date">
					<br>

					<label>Laufzeit bis</label>
					<input   style="height: 38px ;width: 100%"  type="text" name="versicherunge_laufzeit_end" class="form-control versicherunge_date">
					<br>

					<label>Kündigungsfrist</label>
					<select name="versicherunge_kündigungsfrist" class="form-control">
						<option value="">Not Selected</option>
						<option value="3 Monate">3 Monate</option>
						<option value="4 Wochen">4 Wochen</option>
					</select>
					{{-- <input type="text" name="versicherunge_kündigungsfrist" class="form-control"> --}}
					<br>

					<label>Kommentar</label>
					<textarea class="form-control"  name="versicherunge_kommentar"></textarea>
					<br>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Speichern</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>
