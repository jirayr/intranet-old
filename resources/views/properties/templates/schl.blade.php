<style type="text/css">
	#schl-table thead tr th,
	#schl-table tbody tr th,
	#schl-table tbody tr td{
		/*min-width: 100px !important;*/
		/*font-size: 12px;*/
	}
	#schl-table tbody tr td.no-border{
		border: 0px !important;
	}
</style>
<div class="white-box table-responsive">
	<table id="schl-table" class="table">
		<tbody>
		<!-- <tr>
			<th class="text-center border-bottom" colspan="9">
				<img src="{{ asset('img/house.png') }}" width="250px">
				<br/>
				Schleiz
			</th>
		</tr> -->
		<?php
		$wault = 0;
		$n_array = 	array('Baden-Württemberg'=>'5.0',
			           	'Bayern'=>'3.5',
			            'Berlin'=>'6.0',
			            'Brandenburg'=>'6.5',
			            'Bremen'=>'5.0',
			            'Hamburg'=>'4.5',
			            'Hessen'=>'6.0',
			            'Mecklenburg-Vorpommern'=>'6.0',
			            'Niedersachsen'=>'5.0',
			            'Nordrhein-Westfalen'=>'6.5',
			            'Rheinland-Pfalz'=>'5.0',
			            'Saarland'=>'6.5',
			            'Sachsen'=>'3.5',
			            'Sachsen-Anhalt'=>'5.0',
			            'Schleswig-Holstein'=>'6.5',
			            'Thüringen'=>'6.5');
		
			$amRes = DB::table('property_histories as ph')->selectRaw('ph.created_at, am.name')->join('users as am', 'am.id', '=', 'ph.new_value')->where('ph.field_type', 'asset_m_id')->where('property_id', $properties->id)->limit(3)->get();
			$am1 = $am2 = $am3 = '';
			$am1_date = $am2_date = $am3_date = '';
			if($amRes){
				if(isset($amRes[0])){
					$am1 = $amRes[0]->name;
					$am1_date = show_date_format($amRes[0]->created_at);
				}
				if(isset($amRes[1])){
					$am2 = $amRes[1]->name;
					$am2_date = show_date_format($amRes[1]->created_at);
				}
				if(isset($amRes[2])){
					$am3 = $amRes[2]->name;
					$am3_date = show_date_format($amRes[2]->created_at);
				}
			}
		?>
		<tr>
			<th>Strasse:</th>
			<td colspan="3">{{ $properties->strasse }},{{ $properties->hausnummer }}</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="asset_manager1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->asset_manager1 }}</a>-->
				@if(isset($properties->asset_manager->name))
					<!-- {{$properties->asset_manager->name}} -->
				@endif
			</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<th>PLZ, Ort:</th>
			<td colspan="3">{{ $properties->plz_ort }} {{ $properties->ort }}
			</td>
		    <th>Asset Manager:</th>
			<td>
				@if($am1)
					{{ $am1 }}
				@elseif(isset($properties->asset_manager->name)) 
					{{ $properties->asset_manager->name }} 
				@endif
			</td>
			<td colspan="3">
				
			</td>
		</tr>
		<?php
        $sum_actual_net_rent = $sum_total_amount = 0;
                      $vvv= $wault = 0;

                      $pm_total = 0;
			$pa_total = 0;

			$pm_total1 = 0;
			$pa_total1 = 0;


                        ?>
		<?php
		$a = $b = $c = $d = array();
		$a1 = $b1 = $c1 = $d1= array();
		$selection_array = $selection_array1 = $selection_array_total = array();



		foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule){
			foreach($tenancy_schedule->items as $item){

				if($item->status && $item->use && $item->rental_space)
				{
					if(isset($selection_array[$item->use]))
						$selection_array[$item->use] += $item->rental_space; 
					else
						$selection_array[$item->use] = $item->rental_space; 

					if(isset($selection_array_total[$item->use]))
						$selection_array_total[$item->use] += $item->rental_space; 
					else
						$selection_array_total[$item->use] = $item->rental_space; 

				}
				if($item->status && $item->use && $item->vacancy_in_qm)
				{
					if(isset($selection_array1[$item->use]))
						$selection_array1[$item->use] += $item->vacancy_in_qm; 
					else
						$selection_array1[$item->use] = $item->vacancy_in_qm; 

					if(isset($selection_array_total[$item->use]))
						$selection_array_total[$item->use] += $item->vacancy_in_qm; 
					else
						$selection_array_total[$item->use] = $item->vacancy_in_qm; 
				}

				if($item->type == config('tenancy_schedule.item_type.business'))
				{

					if($item->rent_end > date('Y-m-d') && $item->rent_end && substr($item->rent_end,0,4)!="2099")
                    {
                            $sum_actual_net_rent += $item->actual_net_rent;
                            $sum_total_amount += $item->remaining_time_in_eur;
                    }

                    if($item->status)
                    {
						$a[$item->actual_net_rent] = $item->name;
						$b[$item->actual_net_rent] = $item->rental_space;
						$c[$item->actual_net_rent] = $item->rent_begin;
						$d[$item->actual_net_rent] = $item->rent_end;
                    }
				}

				$a1[$item->actual_net_rent] = $item->name;
				$b1[$item->actual_net_rent] = $item->rental_space;
				$c1[$item->actual_net_rent] = $item->rent_begin;
				$d1[$item->actual_net_rent] = $item->rent_end;

			}

			$vermi = 0;
			if( ( $tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10'] ) != 0 )
                $vermi =  ($tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $tenancy_schedule->calculations['total_business_vacancy_in_qm']) / ($tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10']) * 100;


            $pm_total = $tenancy_schedule->calculations['total_actual_net_rent'];
            $pa_total = $tenancy_schedule->calculations['total_actual_net_rent']*12;

            $pm_total1 = $tenancy_schedule->calculations['potenzial_eur_monat'];
            $pa_total1 = $tenancy_schedule->calculations['potenzial_eur_jahr'];

            $vvv = number_format(100-$vermi,2).'%';
            
		}
		// print_r($selection_array);
		// print_r($b);
		// print_r($c);
		// print_r($d);
		
		krsort($a);
		krsort($b);
		krsort($c);
		krsort($d);
		$a  = array_values($a);
		$b  = array_values($b);
		$c  = array_values($c);
		$d  = array_values($d);
	
		// print_r($selection_array);
		?>
		<?php
				$kmkmkm = 0;
				$vermietet_total = 0;
				$leerstand_total =0;
				// print_r($selection_array_total);

				foreach ($selection_array_total as $key => $value) {
					if(in_array($key, array('Einzelhandel','Büro/Praxen','Wohnungen','Gastronomie','Lager','Stellplätze','Sonstiges')))
					$kmkmkm += $value;
				}

				foreach ($selection_array as $key => $value) {
					if(in_array($key, array('Einzelhandel','Büro/Praxen','Wohnungen','Gastronomie','Lager','Stellplätze','Sonstiges')))
					$vermietet_total += $value;
				}
				foreach ($selection_array1 as $key => $value) {
					if(in_array($key, array('Einzelhandel','Büro/Praxen','Wohnungen','Gastronomie','Lager','Stellplätze','Sonstiges')))
					$leerstand_total += $value;
				}
		?>
		
		<tr>
			<th>Gesamtfläche qm:</th>
			<?php
			// $j = 402;
			// if(isset($buy_data[$j]['comment']))
			// 	$properties->ubername_des_objekts2 = $buy_data[$j]['comment'];

			?>
			<td colspan="3">
				<!-- <a href="#" class="inline-edit" data-type="number" data-pk="gesamtflache_qm" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format( $properties->gesamtflache_qm, 2 ,",",".") }}</a> -->
				{{number_format($kmkmkm, 2,",",".")}}
				{{--{{number_format($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg, 2,",",".")}}--}}
			</td>
			<th>Übernahme des Objekts:</th>
			<td>
				{{-- <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-input-new-date" data-pk="ubername_des_objekts2" data-url="{{url('property/update/schl/'.$id) }}" data-title="DD.MM.JJJJ">
					{{show_date_format(str_replace('/','-',$properties->ubername_des_objekts2))}}
				</a> --}}
				@if($am1_date)
					{{ $am1_date }}
				@else
					{{show_date_format(str_replace('/','-',$properties->ubername_des_objekts2))}}
				@endif
			</td>
			 
			<th>Hausverwaltung:</th>
			<td colspan="2">
				{{-- <a href="#" class="inline-edit" data-type="text" data-pk="hausmaxx" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->hausmaxx }} </a>--}}
 			<select name="hausmaxx" id="hausmaxx_options" >
				<option @if($properties->hausmaxx == 'Keine Angabe') selected @endif  value="Keine Angabe">Keine Angabe</option>
				<option @if($properties->hausmaxx == 'Hausmaxx') selected @endif value="Hausmaxx">Hausmaxx</option>
				<option @if($properties->hausmaxx == 'OK') selected @endif value="OK">O.K.</option>
				<option @if($properties->hausmaxx == 'Kähler REM') selected @endif value="Kähler REM">Kähler REM</option>
				<option @if($properties->hausmaxx == 'Paul Immobilien') selected @endif value="Paul Immobilien">Paul Immobilien</option>
				<option @if($properties->hausmaxx == 'Pasquini') selected @endif value="Pasquini">Pasquini</option>
				<option @if($properties->hausmaxx == 'Eigenverwaltung') selected @endif value="Eigenverwaltung">Eigenverwaltung</option>
				<option @if($properties->hausmaxx == 'Sesar') selected @endif value="Sesar">Sesar</option>

				
				
			</select>


			</td>
			<td colspan="3" class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="hausmaxeuro_monat" data-url="{{url('property/update/schl/'.$id) }}" data-title="">@if(is_numeric($properties->hausmaxeuro_monat)){{ number_format( $properties->hausmaxeuro_monat, 2 ,",",".") }}@endif</a> €/Monat

			</td>


		</tr>
		<?php
		$cmarr = array('Kähler REM','Hausmaxx','Paul Immobilien','Pasquini','Sesar','OK');
		?>
		<tr>
			<td colspan="4"></td>
			<td colspan="2"><button type="button" class="btn btn-xs btn-primary" id="am-log" data-url="{{ route('get_am_log', ['property_id' => $properties->id]) }}">AM-Verlauf</button></td>
			<td>HV BU</td>
			<td>
			<select  class="change-column-name" data-column="hvbu_id" >
				<option>HV BU</option>
				@foreach($hvbu_users as $row_user)
				@if($properties->hvbu_id == $row_user->id || ($properties->hausmaxx &&  $row_user->company==$properties->hausmaxx) || ( ($properties->hausmaxx == 'Eigenverwaltung' || $properties->hausmaxx == 'Keine Angabe') && !in_array($row_user->company,$cmarr)))
				<option @if($properties->hvbu_id == $row_user->id) selected @endif  value="{{$row_user->id}}">{{$row_user->name}}</option>
				@endif
				@endforeach				
			</select>
			</td>
		</tr>
		<tr>
			<td colspan="6"></td>
		
			<td>HV PM</td>
			<td>
			<select  class="change-column-name" data-column="hvpm_id" >
				<option>HV PM</option>
				@foreach($hvpm_users as $row_user)
				@if($properties->hvpm_id == $row_user->id || ($properties->hausmaxx &&  $row_user->company==$properties->hausmaxx) || ( ($properties->hausmaxx == 'Eigenverwaltung' || $properties->hausmaxx == 'Keine Angabe') && !in_array($row_user->company,$cmarr)))
				<option @if($properties->hvpm_id == $row_user->id) selected @endif  value="{{$row_user->id}}">{{$row_user->name}}</option>
				@endif
				@endforeach				
			</select>
			</td>
		</tr>
		<tr>
			<td colspan="6"></td>
			<!-- <th>Asset Manager:</th>
			<td>
				{{-- <a href="#" class="inline-edit" data-type="text" data-pk="asset_manager3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->asset_manager3 }}</a> --}}
				@if($am2)
					{{ $am2 }}
				@else
					{{ $properties->asset_manager3 }}
				@endif
			</td> -->
			<th>HV Vertrag abgeschlossen:</th>
			<td>
			<a href="#" class="inline-edit" data-type="text" data-inputclass="mask-input-new-date" data-pk="hv_vertrag_abgeschlossen" data-url="{{url('property/update/schl/'.$id) }}" data-title="DD.MM.JJJJ">
					{{show_date_format($properties->hv_vertrag_abgeschlossen)}}
				</a>
			</td>
		</tr>
		<tr>
			<td colspan="6"></td>
			<!-- <th>Übernahme des Objekts:</th>
			<td>
				{{-- <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-input-new-date" data-pk="ubername_des_objekts3" data-url="{{url('property/update/schl/'.$id) }}" data-title="DD.MM.JJJJ">
					{{show_date_format($properties->ubername_des_objekts3)}}
				</a> --}}
				@if($am2_date)
					{{ $am2_date }}
				@else
					{{show_date_format($properties->ubername_des_objekts3)}}
				@endif
			</td> -->
			<th>HV kündbar bis:</th>
			<td>
			<a href="#" class="inline-edit" data-type="text" data-inputclass="mask-input-new-date" data-pk="hv_kündbar_bis" data-url="{{url('property/update/schl/'.$id) }}" data-title="DD.MM.JJJJ">
					{{show_date_format($properties->hv_kündbar_bis)}}
				</a>
			</td>
			<th>HV läuft aus zum:</th>
			
			<td>
			<a href="#" class="inline-edit" data-type="text" data-inputclass="mask-input-new-date" data-pk="hv_expire_at" data-url="{{url('property/update/schl/'.$id) }}" data-title="DD.MM.JJJJ">
					{{show_date_format($properties->hv_expire_at)}}
				</a>
			</td>

			
		</tr>
		<tr>
			<th>Grunderwerbssteuer:</th>
			<td colspan="5">
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="grunderwerbssteuer" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->grunderwerbssteuer, 2 ,",",".") }}</a> -->
				@if($properties->niedersachsen && isset($n_array[$properties->niedersachsen]))
				{{number_format($n_array[$properties->niedersachsen], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<!-- <th>Asset Manager:</th>
			<td>
				{{-- <a href="#" class="inline-edit" data-type="text" data-pk="asset_manager2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->asset_manager2 }}</a> --}}
				@if($am3)
					{{ $am3 }}
				@else
					{{ $properties->asset_manager2 }}
				@endif
			</td> -->
			
			<th>Prozent:</th>
			@php

			if($properties->hausmaxeuro_monat == 0 || is_null($properties->hausmaxeuro_monat) ){
				$properties->hausmaxeuro_monat = 1; 
			}
			
			if($properties->net_rent_pa == 0 || is_null($properties->net_rent_pa) ){
	$properties->net_rent_pa = 1; 
}
			
			
			
			
			
			@endphp
			
			
			
			
			
			<td>@if(is_numeric($properties->hausmaxeuro_monat) && $pa_total){{ number_format( $properties->hausmaxeuro_monat*12 /$pa_total*100, 2 ,",",".") }}@endif 
			</td>


		</tr>

		<tr>
			<?php
			$D42 = $properties->gesamt_in_eur 
				+ ($properties->real_estate_taxes * $properties->gesamt_in_eur) 
				+ ($properties->estate_agents * $properties->gesamt_in_eur) 
				+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100) 
				+ ($properties->evaluation * $properties->gesamt_in_eur) 
				+ ($properties->others * $properties->gesamt_in_eur) 
				+ ($properties->buffer * $properties->gesamt_in_eur);

				// if(!$properties->einkaufspreis)
					// $properties->einkaufspreis = $D42;
			?>

			<th>Einkaufspreis</th>
			<!-- <td colspan="3"><a href="#" class="inline-edit" data-type="number" data-pk="einkaufspreis" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->einkaufspreis, 2,",",".") }}</a></td> -->
			<td colspan="3">{{ number_format( $D42 , 0 ,",",".") }}</td>


			<!-- <th>Übernahme des Objekts:</th>
			<td>
				{{-- <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-input-new-date" data-pk="ubername_des_objekts" data-url="{{url('property/update/schl/'.$id) }}" data-title="DD.MM.JJJJ">
					{{show_date_format($properties->ubername_des_objekts)}}
				</a> --}}
				@if($am3_date)
					{{ $am3_date }}
				@else
					{{show_date_format($properties->ubername_des_objekts)}}
				@endif
			</td> -->

				<th>Steuerberater</th>

				<td>
					<select name="steuerberater" id="steuerberater_options" >
							<option value=""></option>
							@foreach($tax_consultant as $list)
							<option @if($properties->steuerberater == $list->id) selected @endif  value="{{$list->id}}">{{$list->name}}</option>
							@endforeach
           					
           					{{-- <option @if($properties->steuerberater == 'Keine Angabe') selected @endif  value="Keine Angabe">Keine Angabe</option>
						<option @if($properties->steuerberater == 'Barth Anselm') selected @endif  value="Barth Anselm">Barth Anselm</option>
						<option @if($properties->steuerberater == 'Brenner') selected @endif value="Brenner">Brenner</option>
						<option @if($properties->steuerberater == 'Grasegger, Magdalena') selected @endif value="Grasegger, Magdalena">Grasegger, Magdalena</option>
						<option @if($properties->steuerberater == 'Heller, Patricia') selected @endif value="Heller, Patricia">Heller, Patricia</option>
						<option @if($properties->steuerberater == 'Ortega') selected @endif value="Ortega">Ortega</option>
						<option @if($properties->steuerberater == 'Pfeffer, Maria Anna') selected @endif value="Pfeffer, Maria Anna">Pfeffer, Maria Anna</option>
						<option @if($properties->steuerberater == 'Sachsenhauser, Marina') selected @endif value="Sachsenhauser, Marina">Sachsenhauser, Marina</option>
						<option @if($properties->steuerberater == 'Schmidberger') selected @endif value="Schmidberger">Schmidberger</option>
						<option @if($properties->steuerberater == 'Steffen, Gabriel') selected @endif value="Steffen, Gabriel">Steffen, Gabriel</option>
						<option @if($properties->steuerberater == 'Zolk, Hannes') selected @endif value="Zolk, Hannes">Zolk, Hannes</option> --}}

					</select>
				</td>
				<td>
					<select name="steuerberater1" id="steuerberater1_options" >
						<option value=""></option>
						@foreach($tax_consultant as $list)
							<option value="{{$list->id}}" {{ ($properties->steuerberater1 == $list->id) ? 'selected' : '' }} >{{$list->name}}</option>
						@endforeach
					</select>
				</td>
		</tr>

		@isset($verkauf_tab)
	    @php 
	    $verkaufspreis = $verkauf_tab->where('row_name','verkaufspreis')->first();
	    @endphp
		@isset($verkaufspreis)
	    <?php $properties->verkaufspreis = $verkaufspreis->tab_value; ?>
		@endisset
	    @endisset
		
		<tr>
			<th>Verkaufspreis:</th>
			<td colspan="1">@if($properties->verkaufspreis){{ number_format($properties->verkaufspreis, 2,",",".") }}@endif</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="border">&nbsp;</td>
			<td class="border text-right">Gesamt</td>
			<td class="border text-right">Vermietet</td>
			<td class="border text-right">Leerstand</td>
			<td class="border text-right">Vermietungsstand</td>
		</tr>
		<tr>
			<th class="">Verkäufer:</th>
			<td class="" colspan="1">
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="verkaufer" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->verkaufer }}</a> -->
				@if(isset($properties->seller->name))
					{{$properties->seller->name}}
				@endif
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="border">Einzelhandel</td>
			<td class="border text-right">
				@if(isset($selection_array_total['Einzelhandel']))
				{{number_format($selection_array_total['Einzelhandel'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				<!--  -->
				@if(isset($selection_array['Einzelhandel']))
				{{number_format($selection_array['Einzelhandel'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array1['Einzelhandel']))
				{{number_format($selection_array1['Einzelhandel'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array['Einzelhandel']) && isset($selection_array_total['Einzelhandel']) && $selection_array_total['Einzelhandel'])
				{{number_format(100*$selection_array['Einzelhandel']/$selection_array_total['Einzelhandel'],2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
				%
			</td>
		</tr>
		<tr>
			<th class="">Portfolio</th>
			<td class=""><a href="#" class="inline-edit" data-type="text" data-pk="portfolio" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->portfolio }}</a></td>
			<td colspan="2">&nbsp;</td>
			<td class="border">Büro/Praxen</td>
			<td class="border text-right">
				@if(isset($selection_array_total['Büro/Praxen']))
				{{number_format($selection_array_total['Büro/Praxen'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array['Büro/Praxen']))
				{{number_format($selection_array['Büro/Praxen'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array1['Büro/Praxen']))
				{{number_format($selection_array1['Büro/Praxen'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array['Büro/Praxen']) && isset($selection_array_total['Büro/Praxen']) && $selection_array_total['Büro/Praxen'])
				{{number_format(100*$selection_array['Büro/Praxen']/$selection_array_total['Büro/Praxen'],2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
				%
			</td>
		</tr>
		<tr>
			
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="border">Wohnungen</td>
			<td class="border text-right">
				@if(isset($selection_array_total['Wohnungen']))
				{{number_format($selection_array_total['Wohnungen'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				<!--  -->
				@if(isset($selection_array['Wohnungen']))
				{{number_format($selection_array['Wohnungen'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array1['Wohnungen']))
				{{number_format($selection_array1['Wohnungen'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array['Wohnungen']) && isset($selection_array_total['Wohnungen']) && $selection_array_total['Wohnungen'])
				{{number_format(100*$selection_array['Wohnungen']/$selection_array_total['Wohnungen'],2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
				%
			</td>
		</tr>
		<tr><th>Hausmeister:</th>
			<td colspan="1">
				<a href="#" class="inline-edit" data-type="text" data-pk="hausmeister" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->hausmeister }}</a>
			</td>
			<td colspan="2">
				
			</td>
			<td class="border">Gastronomie</td>
			<td class="border text-right">
				@if(isset($selection_array_total['Gastronomie']))
				{{number_format($selection_array_total['Gastronomie'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				<!--  -->
				@if(isset($selection_array['Gastronomie']))
				{{number_format($selection_array['Gastronomie'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array1['Gastronomie']))
				{{number_format($selection_array1['Gastronomie'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array['Gastronomie']) && isset($selection_array_total['Gastronomie']) && $selection_array_total['Gastronomie'])
				{{number_format(100*$selection_array['Gastronomie']/$selection_array_total['Gastronomie'],2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
				%
			</td>
		</tr>
		<tr>
			<th>Telefon:</th>
			<td colspan="1"><a href="#" class="inline-edit" data-type="text" data-pk="telefon" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->telefon }}</a></td>
			
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td class="border">Lager</td>
			<td class="border text-right">
				@if(isset($selection_array_total['Lager']))
				{{number_format($selection_array_total['Lager'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				<!--  -->
				@if(isset($selection_array['Lager']))
				{{number_format($selection_array['Lager'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array1['Lager']))
				{{number_format($selection_array1['Lager'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array['Lager']) && isset($selection_array_total['Lager']) && $selection_array_total['Lager'])
				{{number_format(100*$selection_array['Lager']/$selection_array_total['Lager'],2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
				%
			</td>
		</tr>
		<tr>
			<td>Dienstleister 1</td>
			<td>
			<select  class="change-column-name" data-column="service_provider_id" >
				<option>Dienstleister </option>
				@foreach($sp_users as $row_user)
				<option @if($properties->service_provider_id == $row_user->id) selected @endif  value="{{$row_user->id}}">{{$row_user->name}}</option>
				@endforeach				
			</select>
			</td>
			
			<td>Dienstleister 2</td>
			<td>
			<select  class="change-column-name" data-column="service_provider_id1" >
				<option>Dienstleister </option>
				@foreach($sp_users as $row_user)
				<option @if($properties->service_provider_id1 == $row_user->id) selected @endif  value="{{$row_user->id}}">{{$row_user->name}}</option>
				@endforeach				
			</select>
			</td>
			<td class="border">Stellplätze</td>
			<td class="border text-right">
				@if(isset($selection_array_total['Stellplätze']))
				{{number_format($selection_array_total['Stellplätze'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array['Stellplätze']))
				{{number_format($selection_array['Stellplätze'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array1['Stellplätze']))
				{{number_format($selection_array1['Stellplätze'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array['Stellplätze']) && isset($selection_array_total['Stellplätze']) && $selection_array_total['Stellplätze'])
				{{number_format(100*$selection_array['Stellplätze']/$selection_array_total['Stellplätze'],2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
				%
			</td>
		</tr>
		<tr>
			<td>Dienstleister 3</td>
			<td>
			<select  class="change-column-name" data-column="service_provider_id2" >
				<option>Dienstleister </option>
				@foreach($sp_users as $row_user)
				<option @if($properties->service_provider_id2 == $row_user->id) selected @endif  value="{{$row_user->id}}">{{$row_user->name}}</option>
				@endforeach				
			</select>
			</td>
			<td>Dienstleister 4</td>
			<td>
			<select  class="change-column-name" data-column="service_provider_id3" >
				<option>Dienstleister </option>
				@foreach($sp_users as $row_user)
				<option @if($properties->service_provider_id3 == $row_user->id) selected @endif  value="{{$row_user->id}}">{{$row_user->name}}</option>
				@endforeach				
			</select>
			</td>
			
			<td class="border">Sonstiges</td>
			<td class="border text-right">
				@if(isset($selection_array_total['Sonstiges']))
				{{number_format($selection_array_total['Sonstiges'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif</td>
			<td class="border text-right">
				@if(isset($selection_array['Sonstiges']))
				{{number_format($selection_array['Sonstiges'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array1['Sonstiges']))
				{{number_format($selection_array1['Sonstiges'], 2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			</td>
			<td class="border text-right">
				@if(isset($selection_array['Sonstiges']) && isset($selection_array_total['Sonstiges']) && $selection_array_total['Sonstiges'])
				{{number_format(100*$selection_array['Sonstiges']/$selection_array_total['Sonstiges'],2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
				%
			</td>
			
		</tr>

		<tr>
			<td>Dienstleister 5</td>
			<td>
			<select  class="change-column-name" data-column="service_provider_id4" >
				<option>Dienstleister </option>
				@foreach($sp_users as $row_user)
				<option @if($properties->service_provider_id4 == $row_user->id) selected @endif  value="{{$row_user->id}}">{{$row_user->name}}</option>
				@endforeach				
			</select>
			</td>
			
			
			<td>&nbsp;</td>
			<td>&nbsp;</td>	
		
		<td class="border">Gesamt</td>
			<td class="border text-right">
				{{number_format($kmkmkm, 2,",",".")}}</td>
			<td class="border text-right">
				<!-- <a href="#" class="inline-edit" data-type="number" data-pk="vermietet" data-url="{{url('property/update/schl/'.$id) }}" data-title=""></a> -->
				{{ number_format($vermietet_total,2,",",".") }}
			</td>
			<td class="border text-right">{{ number_format($leerstand_total,2,",",".") }}</td>
			<td class="border text-right">
				{{$vvv}}
			</td>
		</tr>

		
		<tr>
			<td>Stromanbieter</td>
			<td><a href="#" class="inline-edit" data-type="text" data-pk="electricity_provider" data-url="{{url('property/update/schl2/'.$id) }}" data-title="">{{ @$custom_fields_schl_array['electricity_provider'] }}</a>
			</td>
			<td>Preis in € p.a.</td>
			<td><a href="#" class="inline-edit" data-type="text" data-pk="electricity_provider_price" data-url="{{url('property/update/schl2/'.$id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number(@$custom_fields_schl_array['electricity_provider_price'],2) }}</a>
			</td>
		</tr>

		<tr>
			<td>Ölanbieter
			</td>
			<td><a href="#" class="inline-edit" data-type="text" data-pk="oil_provider" data-url="{{url('property/update/schl2/'.$id) }}" data-title="">{{ @$custom_fields_schl_array['oil_provider'] }}</a>
			</td>
			<td>Preis in € p.a.</td>
			<td><a href="#" class="inline-edit" data-type="text" data-pk="oil_provider_price" data-url="{{url('property/update/schl2/'.$id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number(@$custom_fields_schl_array['oil_provider_price'],2) }}</a>
			</td>
		</tr>

		<tr>
		<td>Gasanbieter
		</td>
		<td><a href="#" class="inline-edit" data-type="text" data-pk="gas_provider" data-url="{{url('property/update/schl2/'.$id) }}" data-title="">{{ @$custom_fields_schl_array['gas_provider'] }}</a>
		</td>
		<td>Preis in € p.a.</td>
		<td><a href="#" class="inline-edit" data-type="text" data-pk="gas_provider_price" data-url="{{url('property/update/schl2/'.$id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number(@$custom_fields_schl_array['gas_provider_price'],2) }}</a>
		</td>	
		</tr>
		<tr>
		<td>Wärmeanbieter
		</td>
		<td><a href="#" class="inline-edit" data-type="text" data-pk="heat_provider" data-url="{{url('property/update/schl2/'.$id) }}" data-title="">{{ @$custom_fields_schl_array['heat_provider'] }}</a>
		</td>
		<td>Preis in € p.a.</td>
		<td><a href="#" class="inline-edit" data-type="text" data-pk="heat_provider_price" data-url="{{url('property/update/schl2/'.$id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number(@$custom_fields_schl_array['heat_provider_price'],2) }}</a>
		</td>	
		</tr>

		<tr>
		<td>Kälteanbieter
		</td>
		<td><a href="#" class="inline-edit" data-type="text" data-pk="refrigeration_provider" data-url="{{url('property/update/schl2/'.$id) }}" data-title="">{{ @$custom_fields_schl_array['refrigeration_provider'] }}</a>
		</td>
		<td>Preis in € p.a.</td>
		<td><a href="#" class="inline-edit" data-type="text" data-pk="refrigeration_provider_price" data-url="{{url('property/update/schl2/'.$id) }}" data-inputclass="mask-number-input" data-title="">{{ show_number(@$custom_fields_schl_array['refrigeration_provider_price'],2) }}</a>
		</td>	
		</tr>

		<tr>
			<?php
			$v1 = 0;
			


			?>
			<td class="border">WAULT</td>
			<th class="border text-center">
				{{--
				@if($wault)
				{{number_format($wault,1)}}
				@else
				{{($tenancy_schedule->calculations['total_business_actual_net_rent']!=0)?number_format(($tenancy_schedule->calculations['total_remaining_time_in_eur'])/(12 * $tenancy_schedule->calculations['total_business_actual_net_rent']),1) : 0 }}
				@endif
				--}}
				@php
				
				@endphp
				@if($sum_actual_net_rent && $sum_total_amount)
                {{number_format($sum_total_amount/(12*$sum_actual_net_rent),1)}}
                @else
					{{($tenancy_schedule->calculations['total_business_actual_net_rent']!=0)?number_format(($tenancy_schedule->calculations['total_remaining_time_in_eur'])/(12 * $tenancy_schedule->calculations['total_business_actual_net_rent']),1) : 0 }}

				@endif
			</th>
			<td></td>
			<td>&nbsp;</td>
			
			
		</tr>
		
		<tr>
			<th>Ankermieter:</th>
			<th class="text-center">
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="ankermieter1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->ankermieter1 }}</a> -->
				@if(isset($a[0]))
				{{$a[0]}}
				@endif
			</th>
			<th class="text-center">
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="ankermieter2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->ankermieter2 }}</a> -->
				@if(isset($a[1]))
				{{$a[1]}}
				@endif
			</th>
			
			<th class="text-center">
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="ankermieter3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->ankermieter3 }}</a> -->
				@if(isset($a[2]))
				{{$a[2]}}
				@endif
			</th>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<th>Fläche in qm:</th>
			<td class="text-center">
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="flache_in_qm1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->flache_in_qm1, 2,",",".") }}</a>-->
				@if(isset($b[0]))
				{{ number_format($b[0], 2,",",".") }}
				@endif

				</td> 
			<td class="text-center">
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="flache_in_qm2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->flache_in_qm2, 2,",",".") }}</a> -->
				@if(isset($b[1]))
				{{ number_format($b[1], 2,",",".") }}
				@endif
			</td>
			<!-- <td>&nbsp;</td> -->
			<td class="text-center">
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="flache_in_qm3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->flache_in_qm3, 2,",",".") }}</a> -->
				@if(isset($b[2]))
				{{ number_format($b[2], 2,",",".") }}
				@endif
			</td>
			<td colspan="5" class="border">Vermietungsstand bei Übernahme</td>
			<!-- <td>&nbsp;</td> -->
		</tr>
		<tr>
			<th>Mietbeginn:</th>
			<td class="text-center">
				<!-- <a href="#" class="inline-edit" data-type="date" data-pk="mietbeginn1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->mietbeginn1 }}</a> -->
				@if(isset($c[0]))
				{{ show_date_format($c[0]) }}
				@endif
			</td>
			<td class="text-center">
				<!-- <a href="#" class="inline-edit" data-type="date" data-pk="mietbeginn2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->mietbeginn2 }}</a> -->
				@if(isset($c[1]))
				{{ show_date_format($c[1]) }}
				@endif
			</td>
			<!-- <td>&nbsp;</td> -->
			<td class="text-center">
				<!-- <a href="#" class="inline-edit" data-type="date" data-pk="mietbeginn3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->mietbeginn3 }}</a> -->
				@if(isset($c[2]))
				{{ show_date_format($c[2]) }}
				@endif
			</td>
			<td class="border">&nbsp;</td>
			<td class="border text-right">Gesamt</td>
			<td class="border text-right">Vermietet</td>
			<td class="border text-right">Leerstand</td>
			<td class="border text-right">Vermietungsstand</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th>Mietende:</th>
			<td class="text-center">
				<!--  -->
				@if(isset($d[0]))
				{{ show_date_format($d[0]) }}
				@endif
			</td>
			<td class="text-center">
				<!--  -->
				@if(isset($d[1]))
				{{ show_date_format($d[1]) }}
				@endif
			</td>
			
			<td class="text-center">
				@if(isset($d[2]))
				{{ show_date_format($d[2]) }}
				@endif
				<!-- <a href="#" class="inline-edit" data-type="date" data-pk="mietende3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->mietende3 }}</a> -->
			</td>
			<td class="border">Einzelhandel</td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="gewerbe" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->gewerbe, 2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="gewerbe1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->gewerbe1,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="gewerbe2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->gewerbe2,2,",",".") }}</a></td>
			<td class="border text-right">
			@if($properties->gewerbe1 && $properties->gewerbe)
				{{number_format(100*$properties->gewerbe1/$properties->gewerbe,2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			%</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th>Mietanpassung:</th>
			<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="mietanpassung" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->mietanpassung }}</a></td>
			<td class="text-center">
				@if(isset($c[1]))
				<a href="#" class="inline-edit" data-type="text" data-pk="mietanpassung2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->mietanpassung2 }}</a>
				@endif
			</td>
			<td class="text-center">@if(isset($c[1]))<a href="#" class="inline-edit" data-type="text" data-pk="mietanpassung3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->mietanpassung3 }}</a>@endif</td>
			<td class="border">Büro/Praxen</td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="buropraxen" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->buropraxen,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="buropraxen1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->buropraxen1,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="buropraxen2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->buropraxen2,2,",",".") }}</a></td>
			<td class="border text-right">@if($properties->buropraxen1 && $properties->buropraxen)
				{{number_format(100*$properties->buropraxen1/$properties->buropraxen,2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			%</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th>Ansprechp.</th>
			<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="ansprechp" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->ansprechp }}</a></td>
			<td class="text-center">@if(isset($c[1]))<a href="#" class="inline-edit" data-type="text" data-pk="ansprechp2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->ansprechp2 }}</a>@endif</td>
			<td class="text-center">@if(isset($c[1]))<a href="#" class="inline-edit" data-type="text" data-pk="ansprechp3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->ansprechp3 }}</a>@endif</td>
			<td class="border">Wohnungen</td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="wohnungen" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->wohnungen,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="wohnungen1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->wohnungen1,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="wohnungen2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->wohnungen2,2,",",".") }}</a></td>
			<td class="border text-right">@if($properties->wohnungen1 && $properties->wohnungen)
				{{number_format(100*$properties->wohnungen1/$properties->wohnungen,2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			%</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th>Optionen:</th>
			<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="optionen" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->optionen }}</a></td>
			<td class="text-center">@if(isset($c[1]))<a href="#" class="inline-edit" data-type="text" data-pk="optionen2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->optionen2 }}</a>@endif</td>
			<td class="text-center">@if(isset($c[1]))<a href="#" class="inline-edit" data-type="text" data-pk="optionen3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->optionen3 }}</a>@endif</td>
			<td class="border">Lager</td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="lager" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->lager,2,",",".") }}</a></td>

			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="lager1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->lager1,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="lager2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->lager2,2,",",".") }}</a></td>
			<td class="border text-right">@if($properties->lager1 && $properties->lager)
				{{number_format(100*$properties->lager1/$properties->lager,2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			%</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td class="border">Stellplätze</td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="stellplatze" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->stellplatze,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="stellplatze1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->stellplatze1,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="stellplatze2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->stellplatze2,2,",",".") }}</a></td>
			<td class="border text-right">@if($properties->stellplatze1 && $properties->stellplatze)
				{{number_format(100*$properties->stellplatze1/$properties->stellplatze,2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			%</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td class="border">Sonstiges</td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="sonstiges" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->sonstiges,2,",",".") }}</a></td>
			
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="sonstiges1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->sonstiges1,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="sonstiges2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->sonstiges2,2,",",".") }}</a></td>
			
			<td class="border text-right">
			@if($properties->sonstiges1 && $properties->sonstiges)
				{{number_format(100*$properties->sonstiges1/$properties->sonstiges,2,",",".")}}
				@else
					{{ number_format(0,2,",",".") }}
				@endif
			
			%</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
			<td class="border">Gesamt</td>
			<td class="border text-right">
				<?php
				$lll1 = 0;
				if($properties->gewerbe)
					$lll1 += $properties->gewerbe;
				if($properties->buropraxen)
					$lll1 += $properties->buropraxen;
				if($properties->wohnungen)
					$lll1 += $properties->wohnungen;
				if($properties->lager)
					$lll1 += $properties->lager;
				if($properties->stellplatze)
					$lll1 += $properties->stellplatze;
				if($properties->sonstiges)
					$lll1 += $properties->sonstiges;
								

				

				?>
				{{ number_format($lll1,2,",",".") }}
			</td>
			
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="vermietet3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->vermietet3,2,",",".") }}</a></td>
			<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="leerstand3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->leerstand3,2,",",".") }}</a></td>
			
			<td class="border text-right"><?php
				$lll = 0;
				/*if($properties->gewerbe_flachenanteil)
					$lll += $properties->gewerbe_flachenanteil;
				if($properties->buro_praxen_flachenanteil)
					$lll += $properties->buro_praxen_flachenanteil;
				if($properties->wohnungen_flachenanteil)
					$lll += $properties->wohnungen_flachenanteil;
				if($properties->lager_flachenanteil)
					$lll += $properties->lager_flachenanteil;
				if($properties->stellplatze_flachenanteil)
					$lll += $properties->stellplatze_flachenanteil;
				if($properties->sonstiges_flachenanteil)
					$lll += $properties->sonstiges_flachenanteil;*/

				if($lll1)
				$lll = 100*$properties->vermietet3/$lll1;
								

				

					?>
					{{ number_format($lll,2,",",".") }}
				%</td>
				<td>&nbsp;</td>
			</tr>
			<!-- <tr>
				<td colspan="4">&nbsp;</td>
				<td class="border">Leerstand</td>
				<td class="border"></td>

				<td class="border"></td>
				<td class="border text-right"></td>

				<td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-pk="leerstand4" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->leerstand4,2,",",".") }}</a>%</td>
				<td>&nbsp;</td>
			</tr> -->
			<tr><td colspan="9">&nbsp;</td></tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<!--<tr>
				<td colspan="3" class="border-top border-right">&nbsp;</td>
				<td colspan="2" class="border-top border-right">&nbsp;</td>
				<td colspan="4" class="border-top">&nbsp;</td>
			</tr>
			<tr>
				<th>Leerstand:</th>
				<td class="text-center">Wohnung</td>
				<td class="text-center border-right">Sonstiges</td>
				<td class="text-center">Wohnen </td>
				<td class="text-center border-right">Laden</td>
				<td>NKA 2017</td>
				<td>erledigt Verkäufer</td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<th>Fläche:</th>
				<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="flache" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->flache,2,",",".") }}</a></td>
				<td class="text-center border-right"><a href="#" class="inline-edit" data-type="text" data-pk="flache1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->flache1,2,",",".") }}</a></td>
				<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="flache2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->flache2,2,",",".") }}</a></td>
				<td class="text-center border-right"><a href="#" class="inline-edit" data-type="text" data-pk="flache3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->flache3,2,",",".") }}</a></td>
				<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="flache4" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->flache4 }}</a></td>
				<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="flache5" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->flache5 }}</a></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<th class="border-bottom">Potentielle Miete:</th>
				<td class="text-center border-bottom"><a href="#" class="inline-edit" data-type="text" data-pk="potentiellemiete" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->potentiellemiete, 2,",",".") }}</a> €</td>
				<td class="text-center border-right border-bottom"><a href="#" class="inline-edit" data-type="text" data-pk="potentiellemiete1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->potentiellemiete1, 2,",",".") }}</a> €</td>
				<td class="text-center border-bottom"><a href="#" class="inline-edit" data-type="text" data-pk="potentiellemiete2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->potentiellemiete2, 2,",",".") }}</a></td>
				<td class="text-center border-right border-bottom"><a href="#" class="inline-edit" data-type="text" data-pk="potentiellemiete3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->potentiellemiete3, 2,",",".") }}</a></td>
				<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="potentiellemiete4" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->potentiellemiete4 }}</a></td>
				<td class="text-center"><a href="#" class="inline-edit" data-type="text" data-pk="potentiellemiete5" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->potentiellemiete5 }}</a></td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr><td colspan="9">&nbsp;</td></tr>-->
			<tr>
				<th>Vermietungstand:</th>
				<td class="text-right">
					<!-- <a href="#" class="inline-edit" data-type="text" data-pk="vermietungstand" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->vermietungstand, 2,",",".") }}</a> -->
					{{$vvv}}
				</td>
				<td>&nbsp;</td>
				<th>Miete (Ist):</th>
				<td class="text-right"><!-- <a href="#" class="inline-edit" data-type="text" data-pk="miete" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->miete, 2,",",".") }}</a> -->
					{{number_format($pm_total, 2,",",".")}}
				</td>
				<th>Miete (Soll):</th>
				<td class="text-right">
					<!-- <a href="#" class="inline-edit" data-type="text" data-pk="miete2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->miete2, 2,",",".") }}</a> -->

					{{ number_format($pm_total + $pm_total1, 2,",",".") }}
				</td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<th>Miete (Ist) p.a.:</th>
				<td class="text-right">
					<!-- <a href="#" class="inline-edit" data-type="text" data-pk="miete3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->miete3, 2,",",".") }}</a> -->
					{{number_format($pa_total, 2,",",".")}}
				</td>
				<th>Miete (Soll)p.a.:</th>
				<td class="text-right">
					<!-- <a href="#" class="inline-edit" data-type="text" data-pk="miete4" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->miete4, 2,",",".") }}</a> -->
					{{ number_format($pa_total+$pa_total1, 2,",",".") }}
				</td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<tr>
				<th>Offene Posten:</th>
				<td colspan="8">&nbsp;</td>
			</tr>
			<tr>
				<th>Instandhaltung:</th>
				<td>Maßnahme</td>
				<td></td>
				<td>&nbsp;</td>
				<td class="text-right">Betrag</td>
			</tr>
			@foreach($maintenance as $list)
			<tr>
				<td>&nbsp;</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="name" data-url="{{url('property/update/maintenance/'.$list->id) }}" data-title="">{{ $list->name }}</a></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="amount" data-url="{{url('property/update/maintenance/'.$list->id) }}" data-title="">{{ number_format($list->amount, 2,",",".") }} €</a></td>
				<td colspan="4"><a href="javascript:void(0)" data-id="{{$list->id}}" class="btn btn-xs btn-danger delete-maintainance"><i class="fa fa-trash"></i></a></td>
			</tr>
			@endforeach
			<tr>
				<td >
					@if(!isset($isShowUnWantedFields))
					<form action="{{route("create_maintenance")}}" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="property_id" value="{{$id}}">
						<button type="submit" class="btn btn-success btn-xs">Hinzufügen</button>
					</form>
					@endif
				</td>
				<td colspan="8">&nbsp;</td>
			</tr>

			<tr>
				<th>Investition Vermietung:</th>
				<td>Maßnahme</td>
				<td></td>
				<td>&nbsp;</td>
				<td class="text-right">Betrag</td>
			</tr>
			@foreach($investation as $list)
			<tr>
				<td>&nbsp;</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="name" data-url="{{url('property/update/investation/'.$list->id) }}" data-title="">{{ $list->name }}</a></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="amount" data-url="{{url('property/update/investation/'.$list->id) }}" data-title="">{{ number_format($list->amount, 2,",",".") }} €</a></td>
				<td colspan="4"><a href="javascript:void(0)" data-id="{{$list->id}}" class="btn btn-xs btn-danger delete-investation"><i class="fa fa-trash"></i></a></td>
			</tr>
			@endforeach
			<tr>
				<td>
					@if(!isset($isShowUnWantedFields))
					<form action="{{route("create_investation")}}" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="property_id" value="{{$id}}">
						<button type="submit" class="btn btn-success btn-xs">Hinzufügen</button>
					</form>
					@endif
				</td>
				<td colspan="8">&nbsp;</td>
			</tr>
			<tr class="hidden">
				<th>Investition Vermietung:</th>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="investition_vermietung" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->investition_vermietung, 2,",",".")  }} €</a></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td class="text-right">Maßnahme</td>
				<td colspan="4"><a href="#" class="inline-edit" data-type="text" data-pk="investition_vermietung_ma" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->investition_vermietung_ma }}</a></td>
			</tr>
			<tr class="hidden">
				<td>&nbsp;</td>
				<td>Entkernung</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="entkernung" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->entkernung }}</a></td>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr class="hidden">
				<td>&nbsp;</td>
				<td>Elektro, Heizung, Sprinkler</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="elektro_heizung_sprinkler" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->elektro_heizung_sprinkler }}</a></td>
				<td colspan="6">&nbsp;</td>
			</tr>
			<tr><td colspan="9" class="border-bottom">&nbsp;</td></tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<tr>
				<th>Versicherung:</th>
				<td colspan="2">&nbsp;</td>
				<td class="text-right">Betrag</td>
				<td colspan="2">Laufzeit</td>
				<td>Kündigungsfrist</td>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="axa" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->axa }}</a></td>
				<td>Gebäude:</td>
				<td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="gebaude_betrag" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format( $properties->gebaude_betrag, 2 ,",",".") }}</a> €</td>
				<td colspan="2"><a href="#" class="inline-edit" data-type="text" data-pk="gebaude_laufzeit_from" data-inputclass="mask-input-new-date" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ show_date_format($properties->gebaude_laufzeit_from) }}</a> - <a href="#" class="inline-edit" data-type="text"  data-inputclass="mask-input-new-date" data-pk="gebaude_laufzeit_to" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{  show_date_format($properties->gebaude_laufzeit_to) }}</a></td>


				<td>
					{{-- <a href="#" class="inline-edit" data-type="text" data-pk="gebaude_kundigungsfrist" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->gebaude_kundigungsfrist }}</a> --}}
					<a href="#" class="inline-edit-schl" data-type="select" data-pk="gebaude_kundigungsfrist" data-url="{{url('property/update/schl/'.$id) }}" data-title="">
						{{ $properties->gebaude_kundigungsfrist }}
					</a>
				</td>

				<td colspan="2">&nbsp;</td>
			</tr>


			<tr>
				<td>
					@if(!isset($isShowUnWantedFields))
					<form action="{{route("create_property_insurance")}}" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="property_id" value="{{$id}}">
						<button type="submit" class="btn btn-success btn-xs">Hinzufügen</button>
					</form>
					@endif
				</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="allianz" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->allianz }}</a></td>
				<td>Haftplicht:</td>
				<td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="haftplicht_betrag" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format($properties->haftplicht_betrag,2,",",".") }}</a> €</td>
				<td colspan="2"><a href="#" class="inline-edit" data-type="text" data-inputclass="mask-input-new-date"  data-pk="haftplicht_laufzeit_from" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ show_date_format($properties->haftplicht_laufzeit_from) }}</a> - <a href="#" class="inline-edit" data-inputclass="mask-input-new-date" data-type="text" data-pk="haftplicht_laufzeit_to" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ show_date_format($properties->haftplicht_laufzeit_to) }}</a></td>

				<td>
					{{-- <a href="#" class="inline-edit" data-type="text" data-pk="haftplicht_kundigungsfrist" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->haftplicht_kundigungsfrist }}</a> --}}
					<a href="#" class="inline-edit-schl" data-type="select" data-pk="haftplicht_kundigungsfrist" data-url="{{url('property/update/schl/'.$id) }}" data-title="">
						{{ $properties->haftplicht_kundigungsfrist }}
					</a>
				</td>

				<td colspan="2">


				</td>
			</tr>
			@if(isset($insurances) && $insurances)
			@foreach($insurances as $list)
			@if($list->is_checked == 1)
				<tr>
					<td>&nbsp;</td>
					<td><a href="#" class="inline-edit" data-type="text" data-pk="name" data-url="{{url('property/update/insurance/'.$list->id) }}" data-title="">{{ $list->name }}</a></td>
					<td>&nbsp;</td>
					<td><a href="#" class="inline-edit" data-type="text" data-pk="type" data-url="{{url('property/update/insurance/'.$list->id) }}" data-title="">@if($list->type==1) Gebäude @else Haftplicht @endif </a></td>
					<td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="amount" data-url="{{url('property/update/insurance/'.$list->id) }}" data-title="">{{ number_format( $list->amount, 2 ,",",".") }}</a> €</td>
					<td><a href="#" class="inline-edit" data-type="date" data-pk="date_from" data-url="{{url('property/update/insurance/'.$list->id) }}" data-title="">{{ $list->date_from }}</a> - <a href="#" class="inline-edit" data-type="date" data-pk="date_to" data-url="{{url('property/update/insurance/'.$list->id) }}" data-title="">{{ $list->date_to }}</a></td>
					<td>
						{{-- <a href="#" class="inline-edit" data-type="text" data-pk="note" data-url="{{url('property/insurance/'.$list->id) }}" data-title="">{{ $list->kundigungsfrist }}</a> --}}
						<a href="#" class="inline-edit-schl" data-type="select" data-pk="note" data-url="{{ url('property/update/insurance/'.$list->id) }}" data-title="">
							{{ $list->kundigungsfrist }}
						</a>
					</td>
					<td colspan="2"><a href="javascript:void(0)" data-id="{{$list->id}}" class="btn btn-xs btn-danger delete-insurance"><i class="fa fa-trash"></i></a></td>
				</tr>
			@endif
			@endforeach
			@endif
			<tr><td colspan="9" class="border-top">&nbsp;</td></tr>
			<tr>
				<th>Dienstleister </th>
				<td>Firma</td>
				<td >&nbsp;</td>
				<td>Dienstleistung</td>
				<td class="text-right">Betrag</td>
				<td >&nbsp;</td>
				<td>Turnus</td>
			</tr>
			@foreach($property_service_providers as $list)
			<tr>
				<td>&nbsp;</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="company" data-url="{{url('property/update/serviceprovider/'.$list->id) }}" data-title="">{{ $list->company }}</a></td>
				<td >&nbsp;</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="type" data-url="{{url('property/update/serviceprovider/'.$list->id) }}" data-title="">{{ $list->type }}</a></td>
				<td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="amount" data-url="{{url('property/update/serviceprovider/'.$list->id) }}" data-title="">{{ number_format($list->amount, 2,",",".") }}</a> €</td>
				<td >&nbsp;</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="note" data-url="{{url('property/update/serviceprovider/'.$list->id) }}" data-title="">{{ $list->note }}</a></td>
				<td colspan="2"><a href="javascript:void(0)" data-id="{{$list->id}}" class="btn btn-xs btn-danger delete-service-provider"><i class="fa fa-trash"></i></a></td>
			</tr>
			@endforeach
			<tr>
				<td>
					@if(!isset($isShowUnWantedFields))
					<form action="{{route("create_service_provider")}}" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="property_id" value="{{$id}}">
						<button type="submit" class="btn btn-success btn-xs">Hinzufügen</button>
					</form>
					@endif
				</td>
				<td colspan="8">&nbsp;</td>
			</tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<!-- <tr>
				<th>Heizung:</th>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="heizung" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->heizung }}</a></td>
				<td colspan="7">&nbsp;</td>
			</tr> -->
			<!--<tr><td colspan="9" class="border-top">&nbsp;</td></tr>
			<tr>
				<th>Exklusivität</th>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="exklusivitat1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->exklusivitat1 }}</a></td>
				<td>&nbsp;</td>
				<td>Kaufpreis</td>
				<td>Portfolio ja/nein</td>
				<td>&nbsp;</td>
				<td>Kommentar</td>
				<td><a href="#" class="inline-edit" data-type="text" data-pk="exklusivitat4" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->exklusivitat4 }}</a></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td>
					<a href="#" class="inline-edit" data-type="text" data-pk="exklusivitat2" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->exklusivitat2 }}</a>
				</td>
				<td>
					<a href="#" class="inline-edit" data-type="text" data-pk="exklusivitat3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->exklusivitat3 }}</a>
				</td>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<th>Sonstiges:</th>
				<td>
					<a href="#" class="inline-edit" data-type="text" data-pk="sonstiges3" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->sonstiges3 }}</a>
				</td>
				<td colspan="7">&nbsp;</td>
			</tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<tr><td colspan="9">&nbsp;</td></tr>
			<tr><td colspan="9" class="border-top">&nbsp;</td></tr>-->
			<?php
		// print_r($schl_banks); die;
		?>
            <tr>
				@if(!isset($isShowUnWantedFields))
				<th>Bank:</th>
			<td colspan="8">
				<select data-id="{{ $id }}" name="schl_banks" multiple="multiple" class="form-control">
					@foreach( $banks as $bank )
						@if($bank)
						<?php if($bank_check == 0){ $bank = $fake_bank; } ?>
						<option value="{{ $bank->id }}" {{ in_array( $bank->id, $schl_banks ) ? 'selected' : '' }}>{{ $bank->name }}</option>
						@endif
					@endforeach
				</select>
			</td>
				@endif

			</tr>
		<tr>
            <th>Mietkonto:</th>
            		<td>Ansprechpartner</td>
                   	<td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1001') }}" data-title="Ansprechpartner">{{ @$buy_data[1001]['comment'] }}</a>
                    </td>
                <td>&nbsp;</td>
                <th>Darlehenskonto:</th>
            		<td>Ansprechpartner</td>
                   	<td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1006') }}" data-title="Ansprechpartner">{{ @$buy_data[1006]['comment'] }}</a>
                    </td>

		   </tr>
		   <tr>
		   	<td></td>
		   	<td>Bank Name</td>
		   	<td><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1002') }}" data-title="Bank Name">{{ @$buy_data[1002]['comment'] }}</a></td>
		   	<td>&nbsp;</td>
		   	<td></td>
		   	<td>Bank Name</td>
		   	<td><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1007') }}" data-title="Bank Name">{{ @$buy_data[1007]['comment'] }}</a></td>
                
		   </tr>
		   <tr>
		   	<td></td>
		   	<td>IBAN</td>
		   	<td><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1003') }}" data-title="IBAN">{{ @$buy_data[1003]['comment'] }}</a></td>
		   	<td>&nbsp;</td>
		   	<td></td>
		   	<td>IBAN</td>
		   	<td><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1008') }}" data-title="IBAN">{{ @$buy_data[1008]['comment'] }}</a></td>
                
		   </tr>
		   <tr>
		   	<td></td>
		   	<td>BIC</td>
		   	<td><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1004') }}" data-title="BIC">{{ @$buy_data[1004]['comment'] }}</a></td>
		   	<td>&nbsp;</td>
		   	<td></td>
		   	<td>BIC</td>
		   	<td><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1009') }}" data-title="BIC">{{ @$buy_data[1009]['comment'] }}</a></td>
                
		   </tr>
		   <tr>
		   	<td></td>
		   	<td>Kontonummer</td>
		   	<td><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1005') }}" data-title="Kontonummer">{{ @$buy_data[1005]['comment'] }}</a></td>
		   	<td>&nbsp;</td>
             <td></td>
		   	<td>Kontonummer</td>
		   	<td><a href="#" class="inline-edit" data-type="text" data-pk="comment" data-url="{{url('einkauf-item/update/'.$id.'/1010') }}" data-title="Kontonummer">{{ @$buy_data[1010]['comment'] }}</a></td>   
		   </tr>




		   
		   
		<tr><td colspan="9" class="border-top">&nbsp;</td></tr>
        @if(!isset($isShowUnWantedFields))

            <tr>
                <th>Kommentar:</th>
                    <td>
                        <a href="#" class="inline-edit" data-type="text" data-pk="kommentar" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->kommentar }}</a>
                    </td>
                <td colspan="7">&nbsp;</td>
		   </tr>
         @endif




		

		<tr>
			<td>Bild</td>
		   	<td><input class="image-file" type="file" name="image-file" data-type="1011"></td>
		   	<td>
		   		@if(isset($export) && @$buy_data[1011]['files'])
		   		<img src="{{'property_images/'.$buy_data[1011]['files']}}" class="upload-image-file" height="100px" alt="">
		   		@else
		   		@if(@$buy_data[1011]['files'])
		   		<img src="{{asset('property_images/'.$buy_data[1011]['files'])}}" class="upload-image-file" height="100px" alt="">
		   		@else
		   		<img src="" class="upload-image-file" height="100px" alt="">
		   		@endif
		   		@endif
		   	</td>   

		   </tr>

		 <tr>
			<td>
                @if(!isset($isShowUnWantedFields))
                    @if(isset($id))
                    <a href="{{url('/property/export-object-data-list-excel/'. $id)}}" >
                        <button type="button" class="btn btn-primary btn-sm btn-export-tenancy-to-excel" style="color:white; margin-top: 20px; margin-left: 20px" data-tenancy-id="16">Export</button>
                    </a>
                    @endif
                @endif
			</td>
		</tr>



		</tbody>
	</table>
</div>


 
