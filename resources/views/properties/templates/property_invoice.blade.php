<div class="row">
	<div class="col-md-12">
		<span id="property_invoice_msg"></span>
	</div>
</div>
<div class="row white-box">
	<div class="col-md-12">
		<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#property_invoice_modal">
			Neue Rechnung
		</button>
		@if(Auth::user()->email==config('users.falk_email'))
		<button type="button" class="btn btn-primary multiple-invoice-release-request pull-right">Markierte Rechnungen freigeben</button>
		@endif

	</div>
	<br/><br/>
	<div class="col-md-12 main_div" style="margin-bottom: 20px;">

		<div class="row">
			<div class="col-md-12">
				<h3>Offene Rechnungen</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-striped" id="add_property_invoice_table" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th style="min-width: 300px;">Kommentar</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>User</th>
							<th>Datum</th>
							<!-- <th>User</th> -->

							{{-- <th>Freigabe</th>
							<th>Pending AM</th>
							<th>Ablehnen AM</th>
							<th>Freigabe Falk</th>
							<th>Ablehnen</th>
							<th>Pending</th> --}}

							

							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>

							<th>Aktion</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>

	</div>

	<div class="col-md-12 main_div">
		<div class="row">
			<div class="col-md-12">
				<h3>Von Falk freigegebene Rechnungen</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2">
				<select class="form-control" id="invoice-filter-field">
					<option value="rechnungsdatum" selected>Re. D.</option>
					<option value="freigabedatum">Freigabedatum</option>
				</select>
			</div>
			<div class="col-md-2">
				<input type="text" id="invoice-filter-start-date" class="form-control mask-input-new-date"  placeholder="DD.MM.YYYY">
			</div>
			<div class="col-md-2">
				<input type="text" id="invoice-filter-end-date" class="form-control mask-input-new-date" placeholder="DD.MM.YYYY">
			</div>
			<div class="col-md-6">
				<button type="button" class="btn btn-success" id="btn-invoice-filter">Filter</button>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								
				<table class="table table-striped" id="add_property_invoice_table2" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th style="min-width: 300px;">Kommentar Rechnung</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>User</th>
							<th>Freigabedatum</th>
							<th>Kommentar Falk</th>

							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>

							<th>Weiterleiten an</th>
							<th>Überweisung</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								
			</div>
		</div>

	</div>

	<div class="col-md-12 main_div">
		<div class="row">
			<div class="col-md-12">
				<h3>Pending Rechnungen (AM)</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-striped" id="pending_am_invoice_table" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th style="min-width: 300px;">Kommentar Rechnung</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>User</th>
							<th>Freigabedatum</th>

							{{-- <th>Freigabe</th>
							<th>Freigabe Falk</th> --}}

							{{-- <th>Kommentar Falk</th> --}}

							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>

							<th>Aktion</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>

	</div>

	<div class="col-md-12 main_div">
		<div class="row">
			<div class="col-md-12">
				<h3>Pending Rechnungen (HV)</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-striped" id="pending_hv_invoice_table" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th style="min-width: 300px;">Kommentar Rechnung</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>User</th>
							<th>Freigabedatum</th>
							{{-- <th>Freigabe</th> --}}
							{{-- <th>Freigabe Falk</th> --}}
							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>
							{{-- <th>Kommentar Falk</th> --}}
							<th>Aktion</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>

	</div>

	<div class="col-md-12 main_div">
		<div class="row">
			<div class="col-md-12">
				<h3>Pending Rechnungen (User)</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-striped" id="pending_user_invoice_table" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th style="min-width: 300px;">Kommentar Rechnung</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>User</th>
							<th>Freigabedatum</th>
							{{-- <th>Freigabe</th> --}}
							{{-- <th>Freigabe Falk</th> --}}
							{{-- <th>Kommentar Falk</th> --}}

							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>

							<th>Aktion</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>

	</div>

	<div class="col-md-12 main_div">
		<div class="row">
			<div class="col-md-12">
				<h3>Pending Rechnungen (Falk)</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-striped" id="add_property_invoice_table4" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th style="min-width: 300px;">Kommentar Rechnung</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>User</th>
							<th>Freigabedatum</th>
							{{-- <th>Freigabe</th> --}}
							{{-- <th>Freigabe Falk</th> --}}
							{{-- <th>Kommentar Falk</th> --}}

							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>

							<th>Aktion</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>

	</div>


	<div class="col-md-12 main_div">

		<div class="row">
			<div class="col-md-12">
				<h3>Nicht freigegeben (AM)</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-striped" id="table_rejected_invoice" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th>User</th>
							<th style="min-width: 300px;">Kommentar</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>Datum</th>
							{{-- <th>Kommentar AM</th> --}}
							
							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>

							<th>Aktion</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>

	</div>

	<div class="col-md-12 main_div">

		<div class="row">
			<div class="col-md-12">
				<h3>Nicht freigegeben (HV)</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-striped" id="table_not_release_hv_invoice" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th>User</th>
							<th style="min-width: 300px;">Kommentar</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>Datum</th>
							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>
							<th>Aktion</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>

	</div>

	<div class="col-md-12 main_div">

		<div class="row">
			<div class="col-md-12">
				<h3>Nicht freigegeben (User)</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-striped" id="table_not_release_user_invoice" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th>User</th>
							<th style="min-width: 300px;">Kommentar</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>Datum</th>
							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>
							<th>Aktion</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>

	</div>

	<div class="col-md-12 main_div">

		<div class="row">
			<div class="col-md-12">
				<h3>Nicht Freigegeben (Falk)</h3>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 table-responsive">
				<table class="table table-striped" id="add_property_invoice_table3" style="width: 100%;padding-left: 0px;">
					<thead>
						<tr>
							<th>
								<button type="button" class="btn btn-primary btn-xs download_invoice" data-url="{{ route('download_invoicezip') }}">Download</button>
								<input type="checkbox" class="row_checkall">
							</th>
							<th>#</th>
							<th>Rechnung</th>
							<th>Re.D.</th>
							<th>Re.Bet.</th>
							<th>User</th>
							<th style="min-width: 300px;">Kommentar</th>
							<th>Abbuch.</th>
							<th>Umlegb.</th>
							<th>Datum</th>
							{{-- <th>Kommentar Falk</th> --}}
							<th style="min-width: 175px;">Freigabe AM</th>
							<th style="min-width: 175px;">Freigabe HV</th>
							<th style="min-width: 175px;">Freigabe User</th>
							<th style="min-width: 175px;">Freigabe Falk</th>
							<th>Aktion</th>
							<th>Weiterleiten an</th>
						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>
		</div>

	</div>

	<div class="col-md-12" style="margin-top: 20px;">
		<h3>Verlauf</h3>
		<div class="table-responsive">

			<table class="table table-striped" id="invoice_mail_table">
		    	<thead>
		      		<tr>
				        <th class="">Name</th>
				        <th class="">Button</th>
				        <th>Invoice</th>
				        <th>Re.D.</th>
				        <th class="">Kommentar</th>
				        <th class="">Datum</th>
				        <th>Aktion</th>
		      		</tr>
		    	</thead>
		    	<tbody>
		    	</tbody>
		  	</table>
		</div>
	</div>

</div>



<div class="modal" role="dialog" id="property_invoice_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Neue Rechnung</h4>
			</div>
			<form method="post" enctype="multipart/form-data" id="add_property_invoice_form">
				<div class="modal-body">

					<span id="add_property_invoice_msg"></span>

					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$id}}">
					<input type="hidden" name="file_basename" id="property_invoice_gdrive_file_basename" value="">
					<input type="hidden" name="file_dirname" id="property_invoice_gdrive_file_dirname" value="">
					<input type="hidden" name="file_type" id="property_invoice_gdrive_file_type" value="">
					<input type="hidden" name="file_name" id="property_invoice_gdrive_file_name" value="">
					<input type="hidden" name="current_tab_name" id="current_tab_nam_invoice" value="Rechnungen">

					<label>Rechnung</label> <span id="property_invoice_gdrive_file_name_span"> </span>
					<a href="javascript:void(0);" class="link-button-gdrive-invoice btn btn-info">Datei auswählen</a>


                    <a class="property_invoice-upload-file-icon btn btn-info" href="javascript:void(0);">Hochladen</a>
                    <input type="file" name="property_invoice_gdrive_file_upload" class="hide property_invoice-gdrive-upload-file-control" id="property_invoice_gdrive_file_upload" >
					<br>
					<br>

					<label>Rechnungsdatum</label>
					<input type="text" name="date" class="form-control mask-input-new-date" placeholder="DD.MM.YYYY">
					<br>

					<label>Rechnungsbetrag</label>
					<input type="text" name="amount" class="form-control mask-input-number">
					<br>

					<label>Kommentar</label>
					<textarea class="form-control" name="comment" ></textarea>
					<br>

					<input type='checkbox' class="" name="is_paid" >
					<label>wird automatisch abgebucht</label>
					<br>

					<input type='checkbox' style="float: left;margin-right: 3px;margin-top:12px;" class="" name="towards_tenant" >
					<label style="float: left;padding-right: 10px;margin-top:8px;">auf Mieter zu</label>
					<input type="text" name="foldable" class="form-control mask-input-number" style="float: left;padding-right: 10px;width: 100px;">
					<label style="float: left;padding-right: 10px;margin-top:8px;">% umlegbar</label>
					<br>
					<div class="clearfix"></div>

					<input type="checkbox" id="is_confidential_invoice" name="is_confidential_invoice" value="1">
					<label for="is_confidential_invoice">Vertrauliche Rechnung</label>
					<br>

					<br>


				</div>
				<div class="modal-footer" style="margin-top: 15px;">
					<button type="submit" class="btn btn-primary">Speichern</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>
