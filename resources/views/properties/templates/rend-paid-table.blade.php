<div class="row">
    <div class="col-md-12">
        <table class="table table-striped" id="rent-paid-table" style="display: block;overflow-x: auto;white-space: nowrap;">
            <thead>
                <tr>
                    <th>Nr.</th>
                    <th>Type</th>
                    <th>Mieter</th>
                    <th>Records</th>
                    <th>Buchungstext</th>
                    <th>SOLL</th>
                    <th>IST</th>
                    <th>DTA</th>
                    <th>Saldo</th>
                    <th>Zahlungdatum</th>
                    <th>Datum</th>
                    <th>Bemerkung FCR</th>
                    <th>Bemerkung HV</th>
                    <th>hochgeladen von</th>
                    <th>Aktion</th>
                </tr>
            </thead>
            <tbody>
                @if ($data)
                    @foreach ($data as $key => $value)
                        <?php
                            $subject = 'OPOS '.$value->mieter.' '.$properties->name_of_property;
                            $content = '';
                        ?>
                        <tr>
                            <td>{{ $value->nr }}</td>
                            <td>{{ $value->type }}</td>
                            <td>{{ $value->mieter }}</td>
                            <td class="text-left">{{$value->sv }}</td>
                            <td>{{ $value->buchungs_text }}</td>
                            <td class="text-right">{{ number_format( $value->soll, 2 ,",",".") }} €</td>
                            <td class="text-right">{{ number_format( $value->ist, 2 ,",",".") }} €</td>
                            <td class="text-right">{{$value->dta}}</td>
                            <td class="text-right">{{ number_format( $value->diff, 2 ,",",".") }} €</td>
                            <td class="text-right">{{ show_date_format($value->zahlungsdatum) }}</td>
                            <td class="text-right">{{ show_date_format($value->datum) }}</td>
                            <td>{{ $value->bemerkung_fcr }}</td>
                            <td>{{ $value->bemerkung_hv }}</td>
                            <td>
                                <a href="javascript:void(0);" class="custom_user" data-user-id="{{ $value->user->id }}" data-property-id="{{ $properties->id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="OPOS">{{ $value->user->name }}</a>

                            </td>
                            
                            <td>
                                <a href=""  data-id="{{$value->id}}" data-fcr="{{ $value->bemerkung_fcr}}"  data-hv="{{$value->bemerkung_hv}}"   data-toggle="modal"    class="noteModal btn-sm btn-info">Notizen</a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <h3>Gesamt</h3>
        <table class="table table-striped"  style="display: block;overflow-x: auto;white-space: nowrap;">
            <thead>
                <tr>
                    <th>SOLL</th>
                    <th>IST</th>
                    <th>Saldo</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-left">{{$sol}}</td>
                    <td class="text-left">{{$ist}}</td>
                    <td class="text-left">{{$diff }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class=" modal fade" role="dialog" id="noteModal">
    <div clas="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Notizen </h4>
            </div>
            <form  class="config-form" id="config-form"   action="{{ url('/savenoteofpropertyrentpaid') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="property_rent_paids_id"  class="form-control" id="property_rent_paids_id" value="" >
                    <label>Bemerkung FCR :</label>
                    <textarea name="bemerkung_fcr" id="bemerkungFcr" class="form-control"  ></textarea>
                    <span id="subject-field"   style="visibility:hidden;color: red;">This field is Required!</span>
                    <br>
                    <label>Bemerkung HV :</label>
                    <textarea name="bemerkung_hv" id="bemerkungHv" class="form-control"  ></textarea>
                    <span id="subject-field"   style="visibility:hidden;color: red;">This field is Required!</span>
                    <br>
                </div>
                <div class="modal-footer">
                    <span id="config-loader" style="visibility:hidden"><i class="fa fa-spin fa-spinner"></i></span>
                    <button id="save-config" onsubmit="sendEmail()" class="btn btn-primary" >Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>


    $(document).ready(function () {

        $('.noteModal').on('click', function () {
            $('#bemerkungHv').val('');
            $('#bemerkungFcr').val('');
            var id = $(this).attr('data-id');
            $("#property_rent_paids_id").val(id);
            var bemerkungHv = $(this).attr('data-hv');
            $('#bemerkungHv').val(bemerkungHv);
            var bemerkungFcr = $(this).attr('data-fcr');
            $('#bemerkungFcr').val(bemerkungFcr);
            $('#noteModal').modal('show');

        });

        {{--$('#noteModal').submit(function (e) {--}}
            {{--$('#config-loader').css('visibility', 'visible');--}}
                {{--alert('asd');--}}
            {{--e.preventDefault();--}}
            {{--$.ajax({--}}
                {{--type : 'post',--}}
                {{--url : "{{url('/savenoteofpropertyrentpaid') }}",--}}
                {{--data :   $('form.config-form').serialize(),--}}
                {{--success : function (data)--}}
                {{--{--}}
                    {{--$('#config-loader').css('visibility', 'hidden');--}}
                    {{--$('#save-config').removeClass('disabled');--}}
                    {{--window.setTimeout(function(){--}}
                        {{--$('#noteModal').modal('hide');--}}
                        {{--sweetAlert("Saved Successfully");--}}
                    {{--}, 600);--}}
                    {{--location.reload();--}}
                {{--}--}}
            {{--});--}}

        {{--});--}}
    });

</script>
