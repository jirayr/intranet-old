<?php
    $form_id = "custom-vacant-form";
    $c_list = $item->getUploadStatus();

    $checkbox_status[1] = $checkbox_status[2] = 
    $checkbox_status[3] = $checkbox_status[4] = $checkbox_status[5] = $checkbox_status[6]
    = $checkbox_status[7] = "";

    $checkbox_status[9] = "";

    $comment_arrs[1] = $comment_arrs[2] = 
    $comment_arrs[3] = $comment_arrs[4] = $comment_arrs[5] = $comment_arrs[6]
    = $comment_arrs[7] = "";

    $comment_arrs[9] = "";
    
    $date_arrs[1] = $date_arrs[2] = 
    $date_arrs[3] = $date_arrs[4] = $date_arrs[5] = $date_arrs[6]
    = $date_arrs[7] = "";


    $date_arrs[9] = "";
    if(isset($item->vacant_uploaded_pdf) && $item->vacant_uploaded_pdf)
    {
        $checkbox_status[9] = "checked";
        $date_arrs[9] = show_date_format($item->vacant_uploaded_pdf_date);
        $comment_arrs[9] = asset('ad_files_upload/'.$item->vacant_uploaded_pdf);
    }



    foreach ($c_list as $key => $value) {
        if($value->status)
        $checkbox_status[$key] = "checked";
        if($value->comment)
        $comment_arrs[$key] = $value->comment;
        if($value->comment && $value->updated_date)
        $date_arrs[$key] = show_date_format($value->updated_date);
    }    

?>

<div class="modal-content">
    <div class="modal-body">

        <div class="row">
            <div class="col-md-4">

                <form class="<p>{{ $form_id }}</p>" method="post" action="<p>{{ url('/') }}</p>/properties/savecustom" enctype="multipart/form-data">

                    <h2 style="font-size: 18px;"><strong>Allgemeine Informationen</strong></h2>
                    <label>Nutzung</label>
                    <p>{{$item->use}}</p>


                    <label>Indexierung</label>
                    <p>{{ $item->indexierung }}</p>

                    <label>Leerstand in qm</label>
                    <p>{{ number_format($item->vacancy_in_qm, 2,",",".") }}</p>

                    <label>Leerstand in EUR</label>
                    <p>{{ number_format($item->vacancy_in_eur, 2,",",".") }}&nbsp;€</p>

                    <label>Leerstand bewertet AM</label>
                    <p>{{ number_format($item->actual_net_rent2, 2,",",".") }}</p>

                    <label>Leerstand seit</label>
                    <p>{{ show_date_format($item->vacant_since) }}</p>

                    <label>bei Ankauf</label>
                    <input type="checkbox" class="" data-id="<p>{{$item->id}}</p>" @if($item->vacancy_on_purcahse) checked @endif>
                    <br>
                    <label>Kommentar</label>
                    <p>{{ (isset($item->singleComment()->comment) && $item->singleComment()->comment) ? $item->singleComment()->comment : $item->comment}}</p>

                </form>
            </div>
            <div class="col-md-6 upload-status-section">
                <div class="row">
                    <div class="col-md-4">
                        <h2 style="font-size: 18px;"><strong>Vermietungsaktivitäten</strong></h2>

                        
                    </div>
                    <div class="col-md-8">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <label><input {{$checkbox_status[1]}} type="checkbox" class="vacant_status_checkbox" data-field="status" value="1"> Immoscout Upload</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="immoscout_upload" class="form-control vacant_status_input" data-field="comment" data-type="1" value="{{$comment_arrs[1]}}">
                    </div>
                    <div class="col-md-1 link">
                        @if($comment_arrs[1])
                            <a href="{{ $comment_arrs[1] }}" target="blank"><i class="fa fa-external-link" style="margin-top: 13px;"></i></a>
                        @endif
                    </div>
                    <div class="col-md-1 updated_date">{{ ($comment_arrs[1]) ? $date_arrs[1] : ''}}</div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4">
                        <label><input type="checkbox" {{$checkbox_status[2]}} class="vacant_status_checkbox" data-field="status" value="2"> Immowelt Upload</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="immowelt_Upload" class="form-control vacant_status_input" data-field="comment" data-type="2" value="{{$comment_arrs[2]}}">
                    </div>
                    <div class="col-md-1 link">
                        @if($comment_arrs[2])
                            <a href="{{ $comment_arrs[2] }}" target="blank"><i class="fa fa-external-link" style="margin-top: 13px;"></i></a>
                        @endif
                    </div>
                    <div class="col-md-1 updated_date">{{ ($comment_arrs[2]) ? $date_arrs[2] : ''}}</div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4">
                        <label><input type="checkbox" {{$checkbox_status[3]}} class="vacant_status_checkbox" data-field="status" value="3"> Ebay Upload</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="ebay_Upload" class="form-control vacant_status_input" data-field="comment" data-type="3" value="{{$comment_arrs[3]}}">
                    </div>
                    <div class="col-md-1 link">
                        @if($comment_arrs[3])
                            <a href="{{ $comment_arrs[3] }}" target="blank"><i class="fa fa-external-link" style="margin-top: 13px;"></i></a>
                        @endif
                    </div>
                    <div class="col-md-1 updated_date">{{ ($comment_arrs[3]) ? $date_arrs[3] : ''}}</div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4">
                        <label><input type="checkbox" {{$checkbox_status[4]}} class="vacant_status_checkbox" data-field="status" value="4"> Regionale Zeitung</label>
                    </div>
                    <div class="col-md-4">
                       <input type="file" name="" data-field="comment" data-value="4" class="form-control upload-status-file" accept="image/*"> 
                    </div>
                    <div class="col-md-3 preview-img">
                        @if($comment_arrs[4])
                        <a href="{{ asset('ad_files_upload/'.$comment_arrs[4]) }}" target="_blank">
                            <img src="{{ asset('ad_files_upload/'.$comment_arrs[4]) }}" class="img-responsive"  style="width: 50px;height: 50px;" />
                        </a>
                            <a href="javascript:void(0)" class="delete-upload-image" data-field="comment" data-value="4"><i class="fa fa-times"></i></a>
                        
                        @endif
                    </div>
                    <div class="col-md-1 updated_date">{{ ($comment_arrs[4]) ? $date_arrs[4] : ''}}</div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4">
                        <label><input type="checkbox" {{$checkbox_status[5]}} class="vacant_status_checkbox" data-field="status" value="5"> Schaufenster ausgehängt</label>
                    </div>
                    <div class="col-md-4">
                       <input type="file" name="" data-field="comment" data-value="5" class="form-control upload-status-file" accept="image/*"> 
                    </div>
                    <div class="col-md-3 preview-img">
                        @if($comment_arrs[5])
                        <a href="{{ asset('ad_files_upload/'.$comment_arrs[5]) }}" target="_blank">
                            <img src="{{ asset('ad_files_upload/'.$comment_arrs[5]) }}" class="img-responsive"  style="width: 50px;height: 50px;" />

                        </a>
                        <a href="javascript:void(0)" class="delete-upload-image" data-field="comment" data-value="5"><i class="fa fa-times"></i></a>
                        @endif
                    </div>

                    <div class="col-md-1 updated_date">{{ ($comment_arrs[5]) ? $date_arrs[5] : ''}}</div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4">
                        <label><input type="checkbox" {{$checkbox_status[6]}} class="vacant_status_checkbox" data-field="status" value="6"> E-Mails über Intranet</label>
                    </div>
                    <div class="col-md-4">
                       <input type="file" name="" data-field="comment" data-value="6" class="form-control upload-status-file">
                    </div>
                    <div class="col-md-3 preview-img">
                        @if($comment_arrs[6])
                        <a href="{{ asset('ad_files_upload/'.$comment_arrs[6]) }}" target="_blank">Download</a> <a href="javascript:void(0)" class="delete-upload-image" data-field="comment" data-value="6"><i class="fa fa-times"></i></a>
                        @endif
                    </div>
                    <div class="col-md-1 updated_date">{{ ($comment_arrs[6]) ? $date_arrs[6] : ''}}</div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4">
                        <label><input type="checkbox" {{$checkbox_status[7]}} class="vacant_status_checkbox" data-field="status" value="7"> Makler beauftragt (nicht exklusiv)</label>
                    </div>
                    <div class="col-md-7">
                       <input type="text" name="makler_beauftragt" class="form-control vacant_status_input" data-field="comment" data-type="7" value="{{$comment_arrs[7]}}">
                    </div>
                    <div class="col-md-1 updated_date">{{ ($comment_arrs[7]) ? $date_arrs[7] : ''}}</div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-4">
                        <label><input type="checkbox" {{$checkbox_status[9]}} class="vacant_status_checkbox" data-field="status" value="9"> Exposé</label>
                    </div>
                    <div class="col-md-4">
                       <input type="file" name="" data-field="comment" data-value="9" class="form-control upload-status-file" accept="image/*"> 
                    </div>
                    <div class="col-md-3 preview-img">
                        @if($comment_arrs[9])
                        <a href="{{ asset('ad_files_upload/'.$comment_arrs[9]) }}" target="_blank">
                            <img src="{{ asset('ad_files_upload/'.$comment_arrs[9]) }}" class="img-responsive"  style="width: 50px;height: 50px;" />

                        </a>
                        <a href="javascript:void(0)" class="delete-upload-image" data-field="comment" data-value="9"><i class="fa fa-times"></i></a>
                        @endif
                    </div>

                    <div class="col-md-1 updated_date">{{ ($comment_arrs[9]) ? $date_arrs[9] : ''}}</div>
                </div>

                <br/>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="button" class="btn btn-primary btn-xs btn-rental-activity-comment" data-url="{{ route('add_rental_activity_comment', ['vacant_id' => $item->id]) }}" data-id="{{ $item->id }}">Kommentar</button>
                    </div>
                    <div class="col-md-12">
                        <p class="latest-comment">{{ (isset($comment_arrs[8])) ? $comment_arrs[8] : '' }}</p>
                    </div>
                </div>

                


            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <!-- <button type="submit" class="btn btn-primary" >Upload <i class="fa fa-circle-o-notch fa-spin hide" style="font-size:14px"></i></button> -->
</div>

