<div class="row">
    @if (Session::has('message_expose'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon fa fa-times"></i></button>
            <p><i class="icon fa fa-check"></i>{{Session::get('message_expose')}}</p>
        </div>
    @endif
    
</div>

<h1>Anhänge</h1>

<form action="{{ url('property_upload_pdf') }}" method="post" enctype="multipart/form-data">

	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<input type="hidden" name="property_id" value="{{ $id }}">

	<div class="form-group">
		<label>Datei hochladen (max. 20 MB)</label>
		<input type="file" name="upload_pdf">
	</div>

	<input type="submit" name="" value="Hochladen" class="btn btn-success" style="margin-bottom:  10px;">
	
</form>


@foreach($properties_pdf as $list)
        <div class="creating-ads-img-wrap media-common-class" style="height: 185px;width: 215px;">
        	
        	<a href="#" class="inline-edit pdf-edit" data-type="text" data-pk="upload_file_name" data-placement="bottom" data-url="{{url('property/update_pdf_name/'.$list->id) }}" data-title="Name des Anhangs">
        	{{$list->upload_file_name}}</a>

        	<a data-id="{{ $list->id }}" href="javascript:;" class="pdfDeleteBtn"><i class="fa fa-trash-o"></i> </a>
        	
            <a href="{{ $list->file_href }}" class="img-responsive pdf-file-icon text-center" target="_blank" style="font-size: 90px;"><i class="fa fa-file-pdf-o"></i></a>
           
        </div>
@endforeach



@if(false)
<embed src="{{ asset('pdf_upload').'/'.$properties_pdf->file_name }}" width="100%" height="1000px" />
@endif
