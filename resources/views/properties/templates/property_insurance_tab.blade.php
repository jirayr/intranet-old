<div class="row">
	<div class="col-md-12">
		<span id="insurance_tab_msg"></span>
	</div>

	<div class="col-md-12">
		<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add_insurance_tab_title_modal">Neue</button>
	</div>

</div>

<div id="insurance-table-content">
	<h3>Offene Angebote</h3>
	@if (isset($insurance_tab_title) && count($insurance_tab_title) > 0)
		@foreach ($insurance_tab_title as $key => $value)
			<?php
				$value->details = DB::table('property_insurance_tab_details')->where('property_insurance_tab_id', $value->id)->where('deleted',0)->get()->count();

				$detail = DB::table('property_insurance_tab_details as pid')
                            	->select('pid.id',
                                	DB::raw("CASE WHEN EXISTS (SELECT id FROM properties_mail_logs WHERE (type = 'insurance_release' OR type = 'insurancetab_release') AND selected_id = pid.id AND (tab='insurance_tab' OR tab='insurancetab2')) THEN 1 ELSE 0 END as is_release")
                            	)
                            	->where('property_insurance_tab_id', $value->id)
                            	->where('pid.deleted', 0)
                            	->havingRaw('is_release=1')
                            	->orderBy('id', 'DESC')
                            	->get();
                $value->release2 = count($detail);

                $value->comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
                            		->join('users as u', 'u.id', 'pc.user_id')
                            		->where('pc.record_id', $value->id)
                            		->where('pc.type', 'property_insurance_tabs')
                            		->orderBy('pc.created_at', 'desc')
                            		->limit(2)->get();
                
			?>

			{{-- @if( ($value->release_status==0 && ($value->release2!=$value->details) || $value->details==0) && !in_array($value->not_release, [1,2,3])) --}}
			@if( ($value->release2 <= 0 || $value->details==0) && !in_array($value->not_release, [1,2,3]))
				<div class="row main_card" style="margin-top: -30px;">
					<div class="col-md-12">
						<div class="modal-dialog modal-lg" style="width: 100%">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">
										<span class="pull-left">{{ $value->title }}</span>
										<button type="button" class="btn btn-primary btn-show-angebote-section-comment btn-xs pull-left" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" data-id="{{ $value->id }}" style="margin-left: 10px;">Show Kommentar</button>
										<div class="section_latest_comment pull-left" style="margin-left: 10px;font-size: 16px;">
											@if(isset($value->comments[0]))
												@php
													$company = ($value->comments[0]->role >= 6 && $value->comments[0]->company) ? ' ('.$value->comments[0]->company.')' : '';
													$commented_user = $value->comments[0]->name.''.$company;
												@endphp
												<p><span class="commented_user">{{ $commented_user }}</span>: {{ $value->comments[0]->comment }} ({{ show_datetime_format($value->comments[0]->created_at) }})</p>
											@endif
										</div>
									</h4>

								</div>
								<div class="modal-body sec-{{ $value->id }}">
									<div class="row ">
										<div class="col-md-1">
											<button type="button" class="btn btn-success btn-xs btn-insurance-tab-detail uptetsntse" data-tabid="{{ $value->id }}">Hinzufügen</button>
											
										</div>
										<div class="col-md-2 ins-btn-{{ $value->id }}"></div>
										<div class="col-md-1">
											<button type="button" class="btn btn btn-xs btn-primary delete-title" data-url="{{ route('delete_property_insurance_title', ['id' => $value->id]) }}">Delete</button>
										</div>
										<div class="col-md-12 table-responsive">
											<table class="table table-striped tbl-insurance-tab-detail" data-tabid="{{ $value->id }}" id="insurance-table-{{ $value->id }}" data-type="OFFENE ANGEBOTE" data-sub="Offene Angebote">
												<thead>
													<tr>
														<th>#</th>
														<th>Datei</th>
														<th>Betrag</th>
														<th>Kommentar</th>
														<th>User</th>
														<th>Datum</th>
														<th>Freigeben Datum</th>
														<th>Ablehnen AM</th>
														<th>Falk Kommantare</th>
														<th>Empfehlung</th>
														<th>Freigeben</th>
														<th>Pending</th>
														<th>Ablehnen</th>
														<th>Action</th>
														<th>Weiterleiten an</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>

									{{-- <div class="row section_comment_div" data-id="{{$value->id}}" data-pid="{{ $value->property_id }}">
										<div class="col-md-12 {{ (count($value->comments) > 0) ? '' : 'hidden' }}">
											
											<div style="padding: 10px;border: 1px solid #d2c7c7;">
												<div class="show_cmnt_section">
													@if($value->comments && count($value->comments))
														@foreach ($value->comments as $comment)
															@php
																$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
																$commented_user = $comment->name.''.$company;
															@endphp
															<p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
														@endforeach
													@endif
												</div>
												<a href="javascript:void(0);" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" class="load_comment_section">Show More</a>
											</div>
											
										</div>
										<div class="col-md-12 form-group">
											<label>Kommentar</label>
											<textarea class="form-control angebote-section-comment" rows="5"></textarea>
										</div>
										<div class="col-md-12">
											<button type="button" class="btn btn-primary btn-add-angebote-section-comment pull-left" data-url="{{ route('add_property_insurance_comment') }}">Senden</button>
											<button type="button" class="btn btn-primary btn-show-angebote-section-comment pull-right" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}">Show Kommentar</button>
										</div>
									</div> --}}

								</div>
							</div>
						</div>
					</div>
				</div>
			@endif
		@endforeach
	@endif
</div>

<h3>Von Falk freigegeben</h3>
@if (isset($insurance_tab_title) && count($insurance_tab_title) > 0)
	@foreach ($insurance_tab_title as $key => $value)
		{{-- @if($value->details && ($value->details==$value->release2 || ($value->release_status==1))) --}}
		@if($value->details && $value->release2 > 0)
			<div class="row" style="margin-top: -30px;">
				<div class="col-md-12">
					<div class="modal-dialog modal-lg" style="width: 100%">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">
									<span class="pull-left">{{ $value->title }}</span>
									<button type="button" class="btn btn-primary btn-show-angebote-section-comment btn-xs pull-left" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" data-id="{{ $value->id }}" style="margin-left: 10px;">Show Kommentar</button>
									<div class="section_latest_comment pull-left" style="margin-left: 10px;font-size: 16px;">
										@if(isset($value->comments[0]))
											@php
												$company = ($value->comments[0]->role >= 6 && $value->comments[0]->company) ? ' ('.$value->comments[0]->company.')' : '';
												$commented_user = $value->comments[0]->name.''.$company;
											@endphp
											<p><span class="commented_user">{{ $commented_user }}</span>: {{ $value->comments[0]->comment }} ({{ show_datetime_format($value->comments[0]->created_at) }})</p>
										@endif
									</div>
								</h4>
							</div>
							<div class="modal-body">
								<div class="row ">
									<div class="col-md-3"></div>
									<div class="col-md-12 table-responsive">
										<table class="table table-striped tbl-insurance-tab-detail" data-tabid="{{ $value->id }}" id="insurance-table-{{ $value->id }}" data-type="VON FALK FREIGEGEBEN" data-sub="Von Falk freigegeben">
											<thead>
												<tr>
													<th>#</th>
													<th>Datei</th>
													<th>Betrag</th>
													<th>Kommentar</th>
													<th>User</th>
													<th>Datum</th>
													<th>Freigeben Datum</th>
													<th>Ablehnen AM</th>
													<th>Falk Kommantare</th>
													<th>Empfehlung</th>
													<th>Freigeben</th>
													<th>Pending</th>
													<th>Ablehnen</th>
													<th>Action</th>
													<th>Weiterleiten an</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
								{{-- <div class="row">
									<div class="col-md-12" style="margin-bottom: 10px;">
										<button type="button" class="btn btn-primary btn-xs btn-show-more-record">Weitere Angebote anzeigen</button>
									</div>
								</div> --}}
								{{-- <div class="row section_comment_div" data-id="{{$value->id}}" data-pid="{{ $value->property_id }}">
									<div class="col-md-12 {{ (count($value->comments) > 0) ? '' : 'hidden' }}">
										
											<div style="padding: 10px;border: 1px solid #d2c7c7;">
												<div class="show_cmnt_section">
													@if($value->comments && count($value->comments))
														@foreach ($value->comments as $comment)
															@php
																$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
																$commented_user = $comment->name.''.$company;
															@endphp
															<p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
														@endforeach
													@endif
												</div>
												<a href="javascript:void(0);" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" class="load_comment_section">Show More</a>
											</div>
										
									</div>
									<div class="col-md-12 form-group">
										<label>Kommentar</label>
										<textarea class="form-control angebote-section-comment" rows="5"></textarea>
									</div>
									<div class="col-md-12">
										<button type="button" class="btn btn-primary btn-add-angebote-section-comment pull-left" data-url="{{ route('add_property_insurance_comment') }}">Senden</button>
										<button type="button" class="btn btn-primary btn-show-angebote-section-comment pull-right" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}">Show Kommentar</button>
									</div>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
	@endforeach
@endif

<h3>Pending Angebote</h3>
@if (isset($insurance_tab_title) && count($insurance_tab_title) > 0)
	@foreach ($insurance_tab_title as $key => $value)
		@if($value->details && $value->release_status == 0 && $value->not_release == 3)
			<div class="row" style="margin-top: -30px;">
				<div class="col-md-12">
					<div class="modal-dialog modal-lg" style="width: 100%">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">
									<span class="pull-left">{{ $value->title }}</span>
									<button type="button" class="btn btn-primary btn-show-angebote-section-comment btn-xs pull-left" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" data-id="{{ $value->id }}" style="margin-left: 10px;">Show Kommentar</button>
									<div class="section_latest_comment pull-left" style="margin-left: 10px;font-size: 16px;">
										@if(isset($value->comments[0]))
											@php
												$company = ($value->comments[0]->role >= 6 && $value->comments[0]->company) ? ' ('.$value->comments[0]->company.')' : '';
												$commented_user = $value->comments[0]->name.''.$company;
											@endphp
											<p><span class="commented_user">{{ $commented_user }}</span>: {{ $value->comments[0]->comment }} ({{ show_datetime_format($value->comments[0]->created_at) }})</p>
										@endif
									</div>
								</h4>
							</div>
							<div class="modal-body">
								<div class="row ">
									<div class="col-md-12 ins-btn-{{ $value->id }}"></div>
									<div class="col-md-12 table-responsive">
										<table class="table table-striped tbl-insurance-tab-detail" data-tabid="{{ $value->id }}" id="insurance-table-{{ $value->id }}" data-type="PENDING ANGEBOTE" data-sub="Pending Angebote">
											<thead>
												<tr>
													<th>#</th>
													<th>Datei</th>
													<th>Betrag</th>
													<th>Kommentar</th>
													<th>User</th>
													<th>Datum</th>
													<th>Freigeben Datum</th>
													<th>Ablehnen AM</th>
													<th>Falk Kommantare</th>
													<th>Empfehlung</th>
													<th>Freigeben</th>
													<th>Pending</th>
													<th>Ablehnen</th>
													<th>Action</th>
													<th>Weiterleiten an</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
								{{-- <div class="row section_comment_div" data-id="{{$value->id}}" data-pid="{{ $value->property_id }}">
									<div class="col-md-12 {{ (count($value->comments) > 0) ? '' : 'hidden' }}">
										
											<div style="padding: 10px;border: 1px solid #d2c7c7;">
												<div class="show_cmnt_section">
													@if($value->comments && count($value->comments))
														@foreach ($value->comments as $comment)
															@php
																$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
																$commented_user = $comment->name.''.$company;
															@endphp
															<p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
														@endforeach
													@endif
												</div>
												<a href="javascript:void(0);" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" class="load_comment_section">Show More</a>
											</div>
										
									</div>
									<div class="col-md-12 form-group">
										<label>Kommentar</label>
										<textarea class="form-control angebote-section-comment" rows="5"></textarea>
									</div>
									<div class="col-md-12">
										<button type="button" class="btn btn-primary btn-add-angebote-section-comment pull-left" data-url="{{ route('add_property_insurance_comment') }}">Senden</button>
										<button type="button" class="btn btn-primary btn-show-angebote-section-comment pull-right" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}">Show Kommentar</button>
									</div>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
	@endforeach
@endif

<h3>Nicht freigegeben (AM)</h3>
@if (isset($insurance_tab_title) && count($insurance_tab_title) > 0)
	@foreach ($insurance_tab_title as $key => $value)
		@if($value->details && $value->release_status == 0 && $value->not_release == 2)
			<div class="row" style="margin-top: -30px;">
				<div class="col-md-12">
					<div class="modal-dialog modal-lg" style="width: 100%">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">
									<span class="pull-left">{{ $value->title }}</span>
									<button type="button" class="btn btn-primary btn-show-angebote-section-comment btn-xs pull-left" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" data-id="{{ $value->id }}" style="margin-left: 10px;">Show Kommentar</button>
									<div class="section_latest_comment pull-left" style="margin-left: 10px;font-size: 16px;">
										@if(isset($value->comments[0]))
											@php
												$company = ($value->comments[0]->role >= 6 && $value->comments[0]->company) ? ' ('.$value->comments[0]->company.')' : '';
												$commented_user = $value->comments[0]->name.''.$company;
											@endphp
											<p><span class="commented_user">{{ $commented_user }}</span>: {{ $value->comments[0]->comment }} ({{ show_datetime_format($value->comments[0]->created_at) }})</p>
										@endif
									</div>
								</h4>
							</div>
							<div class="modal-body">
								<div class="row ">
									<div class="col-md-12 ins-btn-{{ $value->id }}"></div>
									<div class="col-md-12 table-responsive">
										<table class="table table-striped tbl-insurance-tab-detail" data-tabid="{{ $value->id }}" id="insurance-table-{{ $value->id }}" data-type="NICHT FREIGEGEBEN ANGEBOTE" data-sub="Nicht freigegeben (AM)">
											<thead>
												<tr>
													<th>#</th>
													<th>Datei</th>
													<th>Betrag</th>
													<th>Kommentar</th>
													<th>User</th>
													<th>Datum</th>
													<th>Freigeben Datum</th>
													<th>Ablehnen AM</th>
													<th>Falk Kommantare</th>
													<th>Empfehlung</th>
													<th>Freigeben</th>
													<th>Pending</th>
													<th>Ablehnen</th>
													<th>Action</th>
													<th>Weiterleiten an</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
								{{-- <div class="row section_comment_div" data-id="{{$value->id}}" data-pid="{{ $value->property_id }}">
									<div class="col-md-12 {{ (count($value->comments) > 0) ? '' : 'hidden' }}">
										
											<div style="padding: 10px;border: 1px solid #d2c7c7;">
												<div class="show_cmnt_section">
													@if($value->comments && count($value->comments))
														@foreach ($value->comments as $comment)
															@php
																$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
																$commented_user = $comment->name.''.$company;
															@endphp
															<p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
														@endforeach
													@endif
												</div>
												<a href="javascript:void(0);" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" class="load_comment_section">Show More</a>
											</div>
										
									</div>
									<div class="col-md-12 form-group">
										<label>Kommentar</label>
										<textarea class="form-control angebote-section-comment" rows="5"></textarea>
									</div>
									<div class="col-md-12">
										<button type="button" class="btn btn-primary btn-add-angebote-section-comment pull-left" data-url="{{ route('add_property_insurance_comment') }}">Senden</button>
										<button type="button" class="btn btn-primary btn-show-angebote-section-comment pull-right" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}">Show Kommentar</button>
									</div>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
	@endforeach
@endif

<h3>Nicht freigegeben (Falk)</h3>
@if (isset($insurance_tab_title) && count($insurance_tab_title) > 0)
	@foreach ($insurance_tab_title as $key => $value)
		@if($value->details && $value->release_status == 0 && $value->not_release == 1)
			<div class="row" style="margin-top: -30px;">
				<div class="col-md-12">
					<div class="modal-dialog modal-lg" style="width: 100%">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">
									<span class="pull-left">{{ $value->title }}</span>
									<button type="button" class="btn btn-primary btn-show-angebote-section-comment btn-xs pull-left" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" data-id="{{ $value->id }}" style="margin-left: 10px;">Show Kommentar</button>
									<div class="section_latest_comment pull-left" style="margin-left: 10px;font-size: 16px;">
										@if(isset($value->comments[0]))
											@php
												$company = ($value->comments[0]->role >= 6 && $value->comments[0]->company) ? ' ('.$value->comments[0]->company.')' : '';
												$commented_user = $value->comments[0]->name.''.$company;
											@endphp
											<p><span class="commented_user">{{ $commented_user }}</span>: {{ $value->comments[0]->comment }} ({{ show_datetime_format($value->comments[0]->created_at) }})</p>
										@endif
									</div>
								</h4>
							</div>
							<div class="modal-body">
								<div class="row ">
									<div class="col-md-12 ins-btn-{{ $value->id }}"></div>
									<div class="col-md-12 table-responsive">
										<table class="table table-striped tbl-insurance-tab-detail" data-tabid="{{ $value->id }}" id="insurance-table-{{ $value->id }}" data-type="NICHT FREIGEGEBEN ANGEBOTE" data-sub="Nicht freigegeben (Falk)">
											<thead>
												<tr>
													<th>#</th>
													<th>Datei</th>
													<th>Betrag</th>
													<th>Kommentar</th>
													<th>User</th>
													<th>Datum</th>
													<th>Freigeben Datum</th>
													<th>Ablehnen AM</th>
													<th>Falk Kommantare</th>
													<th>Empfehlung</th>
													<th>Freigeben</th>
													<th>Pending</th>
													<th>Ablehnen</th>
													<th>Action</th>
													<th>Weiterleiten an</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
								{{-- <div class="row section_comment_div" data-id="{{$value->id}}" data-pid="{{ $value->property_id }}">
									<div class="col-md-12 {{ (count($value->comments) > 0) ? '' : 'hidden' }}">
										
											<div style="padding: 10px;border: 1px solid #d2c7c7;">
												<div class="show_cmnt_section">
													@if($value->comments && count($value->comments))
														@foreach ($value->comments as $comment)
															@php
																$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
																$commented_user = $comment->name.''.$company;
															@endphp
															<p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
														@endforeach
													@endif
												</div>
												<a href="javascript:void(0);" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" class="load_comment_section">Show More</a>
											</div>
										
									</div>
									<div class="col-md-12 form-group">
										<label>Kommentar</label>
										<textarea class="form-control angebote-section-comment" rows="5"></textarea>
									</div>
									<div class="col-md-12">
										<button type="button" class="btn btn-primary btn-add-angebote-section-comment pull-left" data-url="{{ route('add_property_insurance_comment') }}">Senden</button>
										<button type="button" class="btn btn-primary btn-show-angebote-section-comment pull-right" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}">Show Kommentar</button>
									</div>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
	@endforeach
@endif

<h3>Verlauf</h3>
<div class="row" style="margin-top: -30px;">
	<div class="col-md-12">
		<div class="modal-dialog modal-lg" style="width: 100%">
			<div class="modal-content">
				{{-- <div class="modal-header">
					<h4 class="modal-title">Title</h4>
				</div> --}}
				<div class="modal-body">
					<div class="row ">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped" id="insurance-tab-log">
								<thead>
									<tr>
										<th>#</th>
										<th>Title</th>
										<th>Name</th>
										<th>Button</th>
										<th>Datei</th>
										<th>Kommentar</th>
										<th>Datum</th>
										<th>Aktion</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="insurance-tab-hidden-html" style="display: none;">
	<div class="row main_card">
		<div class="col-md-12">
			<div class="modal-dialog modal-lg" style="width: 100%">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">##TITLE##</h4>
					</div>
					<div class="modal-body">
						<div class="row ">

							<div class="col-md-1">
								<button type="button" class="btn btn-success btn-xs btn-insurance-tab-detail uptetsntse" data-tabid="##ID##">Hinzufügen</button>
							</div>
							<div class="col-md-2 ins-btn-##ID##"></div>
							<div class="col-md-1">
								<button type="button" class="btn btn btn-xs btn-primary delete-title" data-url="##TITLE-DELETE-URL##">Delete</button>
							</div>
							<div class="col-md-12 table-responsive">
								<table class="table table-striped tbl-insurance-tab-detail hidden-table" data-tabid="##ID##" id="insurance-table-##ID##" data-type="OFFENE ANGEBOTE" data-sub="Offene Angebote">
									<thead>
										<tr>
											<th>#</th>
											<th>Datei</th>
											<th>Betrag</th>
											<th>Kommentar</th>
											<th>User</th>
											<th>Datum</th>
											<th>Ablehnen AM</th>
											<th>Falk Kommantare</th>
											<th>Empfehlung</th>
											<th>Freigeben</th>
											<th>Pending</th>
											<th>Ablehnen</th>
											<th>Action</th>
											<th>Weiterleiten an</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class=" modal fade" role="dialog" id="add_insurance_tab_title_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<form method="post" id="form_add_insurance_tab_title" action="{{ route('add_property_insurance_title', ['property_id' => $properties->id]) }}">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<span id="add_insurance_tab_title_msg"></span>
						</div>
						<div class="col-md-12">
							<label>Titel</label>
							<input type="text" name="title" class="form-control" required="">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Speichern</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>

<div class=" modal fade" role="dialog" id="add_insurance_tab_detail_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<form method="post" id="form_add_insurance_tab_detail" action="">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<span id="add_insurance_tab_detail_msg"></span>
						</div>
						<input type="hidden" id="property_insurance_tab_id" name="property_insurance_tab_id">

						<div class="col-md-12">
							<input type="hidden" name="file_basename" id="insurance_tab_gdrive_file_basename" value="">
							<input type="hidden" name="file_dirname" id="insurance_tab_gdrive_file_dirname" value="">
							<input type="hidden" name="file_type" id="insurance_tab_gdrive_file_type" value="">
							<input type="hidden" name="file_name" id="insurance_tab_gdrive_file_name" value="">
							<input type="hidden" name="current_tab_name" id="current_tab_name_insurance_tab" value="Angebote">

							<label>Datei</label> <span id="insurance_tab_gdrive_file_name_span"> </span>
							<a href="javascript:void(0);" class="link-button-gdrive-insurance-tab btn btn-info">Datei auswählen</a>


		                    <a class="insurance-tab-upload-file-icon btn btn-info" href="javascript:void(0);">Hochladen</a>
		                    <input type="file" name="insurance_tab_gdrive_file_upload" class="hide insurance-tab-gdrive-upload-file-control" id="insurance_tab_gdrive_file_upload" >
							<br><br>
						</div>

						{{-- <div class="col-md-12">
							<label>Name</label>
							<input type="text" name="name" class="form-control" required="">
						</div> --}}
						<div class="col-md-12">
							<label>Betrag</label>
							<input type="text" name="amount" class="form-control mask-input-number" required="">
						</div>
						<div class="col-md-12">
							<label>Kommentar</label>
							<textarea name="comment" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Speichern</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>