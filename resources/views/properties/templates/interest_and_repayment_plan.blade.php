<style type="text/css">
    #annuitatendarlehen-table>tbody>tr>td, #annuitatendarlehen-table>tbody>tr>th, #annuitatendarlehen-table>tfoot>tr>td, #annuitatendarlehen-table>tfoot>tr>th, #annuitatendarlehen-table>thead>tr>td, #annuitatendarlehen-table>thead>tr>th{
        border: 1px solid #28292a !important;
        min-width: min-content;
        white-space: nowrap;
    }
    #annuitatendarlehen-table>tbody>tr>th{
        font-weight: bold !important;
    }
    #annuitatendarlehen-table input{
        width: 100%;
    }

    #tilgungsdarlehen-table>tbody>tr>td, #tilgungsdarlehen-table>tbody>tr>th, #tilgungsdarlehen-table>tfoot>tr>td, #tilgungsdarlehen-table>tfoot>tr>th, #tilgungsdarlehen-table>thead>tr>td, #tilgungsdarlehen-table>thead>tr>th{
        border: 1px solid #28292a !important;
        min-width: min-content;
        white-space: nowrap;
    }
    #tilgungsdarlehen-table>tbody>tr>th{
        font-weight: bold !important;
    }
    #tilgungsdarlehen-table input{
        width: 100%;
    }
</style>

<div class="row white-box annuitatendarlehen-div {{ (isset($loan_mirror->darlehensart) && $loan_mirror->darlehensart == 'Annuitätendarlehen') ? '' : 'hidden' }}">
    <legend>Zins- und Tilgungsplan</legend>
    <div class="col-md-12 col-xs-12 table-responsive">
        <table class="table table-bordered" id="annuitatendarlehen-table">
            <thead>
                <tr>
                    {{-- <th>{{ (isset($loan_mirror->darlehensart)) ? $loan_mirror->darlehensart : '' }}</th> --}}
                    <th>Annuitätendarlehen</th>
                    <th colspan="5"></th>
                </tr>
            </thead>
            <tbody>

                @php
                    $darlehensbetrag = (isset($loan_mirror->darlehensbetrag)) ? $loan_mirror->darlehensbetrag : 0;
                    $anfangszins = (isset($loan_mirror->anfangszins)) ? $loan_mirror->anfangszins : 0;
                    $tilgungssatz = (isset($loan_mirror->tilgungssatz)) ? $loan_mirror->tilgungssatz : 0;

                    $kapitaldienst_manuelle = ($darlehensbetrag * ( ($anfangszins / 100) + ($tilgungssatz / 100) ) / 12);
                    $kapitaldienst_p_a = ( ($anfangszins + $tilgungssatz) * $darlehensbetrag ) / 100;
                @endphp

                <tr>
                    <td>{{ show_number($darlehensbetrag, 2) }} € Darlehensbetrag</td>
                    <td>{{ show_number($anfangszins, 2) }}% Anfangszins p.a.</td>
                    <td>{{ show_number($tilgungssatz, 2) }}% Anfänglicher Tilgungssatz p.a.</td>
                    <td><input type="text" class="mask-number-input kapitaldienst_manuelle" name="" value="{{ show_number($kapitaldienst_manuelle, 2) }}" readonly></td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>Zahlungsrythmus Zinsen: monatlich</td>
                    <td colspan="5"></td>
                </tr>
                <tr>
                    <td>Zahlungsrythmus Tilgung: monatlich</td>
                    <td colspan="5"></td>
                </tr>

                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>

                @php
                    $date = '';
                    $aktuelle_restschuld_manuelle = '';
                    $dateRes = DB::table('properties_custom_fields')->where('property_id', $properties->id)->where('slug', 'annuitatendarlehen_aktuelle_restschuld')->first();
                    if($dateRes){
                        $date = show_date_format($dateRes->updated_at);
                        $aktuelle_restschuld_manuelle = ($dateRes->content) ? show_number($dateRes->content, 2) : '';
                    }
                @endphp

                <tr>
                    <th>{{ $date }} <span class="text-danger">(automatisch!)</span></th>
                    <th><input type="text" class="mask-number-input aktuelle_restschuld_manuelle" name="" value="{{ $aktuelle_restschuld_manuelle }}"></th>
                    <th>Aktuelle Restschuld <span class="text-danger">(Manuelle Eingabe!)</span></th>
                    <th>Kapitaldienst</th>
                    <th>Zinsaufwand</th>
                    <th>Tilgung</th>
                </tr>

                @for ($i = 1; $i <= 12; $i++)
                    <tr id="month_row_{{$i}}">
                        <td>{{ date('M', strtotime('+'.($i-1).' month')) }}/{{ date('y', strtotime('+'.($i-1).' month')) }}</td>
                        <td></td>
                        <td><input type="text" class="mask-number-input aktuelle_restschuld_{{$i}}" name="" value="" readonly></td>
                        <td><input type="text" class="mask-number-input kapitaldienst_{{$i}}" name="" value="" readonly></td>
                        <td><input type="text" class="mask-number-input zinsaufwand_{{$i}}" name="" value="" readonly></td>
                        <td><input type="text" class="mask-number-input tilgung_{{$i}}" name="" value="" readonly></td>
                    </tr>
                @endfor

                <tr>
                    <td colspan="6">
                        <button type="button" class="btn btn-xs btn-primary pull-right z_show_more">Show More</button>
                    </td>
                </tr>

            </tbody>
            <tfoot>
                <tr>
                    <td>Kapitaldienst p.a.</td>
                        <th><input type="text" class="mask-number-input kapitaldienst_p_a" name="" value="{{ show_number($kapitaldienst_p_a, 2) }}" readonly></th>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td>Zinsaufwand p.a.</td>
                        <th><input type="text" class="mask-number-input zinsaufwand_p_a" name="" value="" readonly></th>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td>Tilgung p.a.</td>
                        <th><input type="text" class="mask-number-input tilgung_p_a" name="" value="" readonly></th>
                    <td colspan="4"></td>
                </tr>
            </tfoot>
        </table>
	</div>
</div>

<div class="row white-box tilgungsdarlehen-div {{ (isset($loan_mirror->darlehensart) && $loan_mirror->darlehensart == 'Tilgungsdarlehen') ? '' : 'hidden' }}">
    <legend>Zins- und Tilgungsplan</legend>
    <div class="col-md-12 col-xs-12 table-responsive">
        <table class="table table-bordered" id="tilgungsdarlehen-table">
            <thead>
                <tr>
                    {{-- <th>{{ (isset($loan_mirror->darlehensart)) ? $loan_mirror->darlehensart : '' }}</th> --}}
                    <th>Tilgungsdarlehen</th>
                    <th colspan="5"></th>
                </tr>
            </thead>
            <tbody>


                <tr>
                    <td>1,000,000.00</td>
                    <td>1,3% Zins</td>
                    <td><input type="text" class="mask-number-input tilgung_manuelle" name="" value="" style="width: 80%;"> € Tilgung</td>
                    {{-- <td><input type="text" class="mask-number-input kapitaldienst_manuelle" name="" value=""></td> --}}
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td>Zahlungsrythmus Zinsen: monatlich</td>
                    <td colspan="5"></td>
                </tr>
                <tr>
                    <td>Zahlungsrythmus Tilgung: monatlich</td>
                    <td colspan="5"></td>
                </tr>

                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>

                <tr>
                    <th>03.08.2020 <span class="text-danger">(automatisch!)</span></th>
                    <th><input type="text" class="mask-number-input aktuelle_restschuld_manuelle" name="" value=""></th>
                    <th>Aktuelle Restschuld <span class="text-danger">(Manuelle Eingabe!)</span></th>
                    <th>Kapitaldienst</th>
                    <th>Zins</th>
                    <th>Tilgung</th>
                </tr>

                @for ($i = 1; $i <= 12; $i++)
                    <tr id="month_row_{{$i}}">
                        <td>{{ date('M', strtotime('+'.($i-1).' month')) }}/{{ date('y', strtotime('+'.($i-1).' month')) }}</td>
                        <td></td>
                        <td><input type="text" class="mask-number-input aktuelle_restschuld_{{$i}}" name="" value="" readonly></td>
                        <td><input type="text" class="mask-number-input kapitaldienst_{{$i}}" name="" value="" readonly></td>
                        <td><input type="text" class="mask-number-input zins_{{$i}}" name="" value="" readonly></td>
                        <td><input type="text" class="mask-number-input tilgung_{{$i}}" name="" value="" readonly></td>
                    </tr>
                @endfor

                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>

                <tr>
                    <td>Kapitaldienst p.a.</td>
                        <th><input type="text" class="mask-number-input kapitaldienst_p_a" name="" value="" readonly></th>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td>Zinsaufwand p.a.</td>
                        <th><input type="text" class="mask-number-input zinsaufwand_p_a" name="" value="" readonly></th>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td>Tilgung p.a.</td>
                        <th><input type="text" class="mask-number-input tilgung_p_a" name="" value="" readonly></th>
                    <td colspan="4"></td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
