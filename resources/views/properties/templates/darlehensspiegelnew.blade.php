
<div class="row">
    <legend>Darlehensspiegel</legend>
    <div class="col-md-10 col-xs-12">
    @if ($errors->any())
        <p class="error" style="font-weight: bold;">Please fill all required fields.</p>
    @endif

    <form action="{{ route('add_loan_mirror', ['property_id' => $id]) }}" method="post" class="form-horizontal loanMirrorForm" accept-charset="UTF-8" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="property_id" value="{{ $properties->id }}">


        <?php
        $market_value = 0;
        if(isset($GLOBALS['ist_sheet']) && $GLOBALS['ist_sheet']){
          $ist_sheet = $GLOBALS['ist_sheet'];
          $vprice = $ist_sheet->preisverk;
          $market_value = $ist_sheet->market_value;
        }
        ?>

        <legend class="hidden">Übersicht</legend>
        <div class="form-group m-form__group row hidden">
        <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <label>Darlehen 30.06.2020 HGB:</label>
                @if ($errors->has('delta3'))
                    <div class="error">{{ $errors->first('delta3') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="delta3" value="@if(old('delta3')){{old('delta3')}}@elseif($loan_mirror){{number_format($loan_mirror->delta3,2,',','.')}}@endif" name="delta3">
            </div>
        <div class="col-lg-4">
                <label>Delta:</label>
                @if ($errors->has('delta'))
                    <div class="error">{{ $errors->first('delta') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="delta" value="@if(old('delta')){{old('delta')}}@elseif($loan_mirror){{number_format($loan_mirror->delta,2,',','.')}}@endif" name="delta">
            </div>
            <div class="col-lg-4">
                <label>Darlehen 31.12.2019 HGB:</label>
                @if ($errors->has('delta2'))
                    <div class="error">{{ $errors->first('delta2') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="delta2" value="@if(old('delta2')){{old('delta2')}}@elseif($loan_mirror){{number_format($loan_mirror->delta2,2,',','.')}}@endif" name="delta2">
            </div>
            <div class="col-lg-4">
                <label>Zins:</label>
                @if ($errors->has('zins'))
                    <div class="error">{{ $errors->first('zins') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="zins" value="@if(old('zins')){{old('zins')}}@elseif($loan_mirror && is_numeric($loan_mirror->zins)){{number_format($loan_mirror->zins,2,',','.')}}@endif" name="zins">
            </div>
            
            <div class="col-lg-4">
                <label>Zinsaufwand p.a. ann. per 31.12.2019:</label>
                @if ($errors->has('interest_extend'))
                    <div class="error">{{ $errors->first('interest_extend') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="interest_extend" value="@if(old('interest_extend')){{old('interest_extend')}}@elseif($loan_mirror){{number_format($loan_mirror->interest_extend,2,',','.')}}@endif" name="interest_extend">
            </div>
            <div class="col-lg-4">
                <label>Kapitaldienst p.a. ann. per 31.12.2019:</label>
                @if ($errors->has('loan_service'))
                    <div class="error">{{ $errors->first('loan_service') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="loan_service" value="@if(old('loan_service')){{old('loan_service')}}@elseif($loan_mirror){{number_format($loan_mirror->loan_service,2,',','.')}}@endif" name="loan_service">
            </div>
            <div class="col-lg-4">
                <label>Kapitaldienst p.m. ann. per 31.12.2019:</label>
                @if ($errors->has('loan_service_month'))
                    <div class="error">{{ $errors->first('loan_service_month') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="loan_service_month" value="@if(old('loan_service_month')){{old('loan_service_month')}}@elseif($loan_mirror){{number_format($loan_mirror->loan_service_month,2,',','.')}}@endif" name="loan_service_month">
            </div>
        </div>    
        <legend>1. Allgemein</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Darlehensgeber:</label>
                @if ($errors->has('lender'))
                    <div class="error">{{ $errors->first('lender') }}</div>
                @endif
                <?php 
                    $lender = '';
                    if(old('lender')){
                        $lender = old('lender');
                    }elseif($loansMirrorData && $loansMirrorData->lender){
                        $lender = $loansMirrorData->lender;
                    }elseif($loan_mirror && $loan_mirror->lender){
                        $lender = $loan_mirror->lender;
                    }
                ?>
                <input type="text" class="form-control" id="lender" value="{{ $lender }}" name="lender">
            </div>
            <div class="col-lg-4">
                <label>Darlehensnehmer:</label>
                @if ($errors->has('borrower'))
                    <div class="error">{{ $errors->first('borrower') }}</div>
                @endif
                <?php 
                    $borrower = '';
                    if(old('borrower')){
                        $borrower = old('borrower');
                    }elseif($loansMirrorData && $loansMirrorData->borrower){
                        $borrower = $loansMirrorData->borrower;
                    }elseif($loan_mirror && $loan_mirror->borrower){
                        $borrower = $loan_mirror->borrower;
                    }
                ?>
                <input type="text" class="form-control" id="borrower" value="{{ $borrower }}" name="borrower">
            </div>
            
        </div>


        <!-- <legend>2.) Darlehenshöhte</legend> -->
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Darlehensbetrag:</label>
                @if ($errors->has('darlehensbetrag'))
                    <div class="error">{{ $errors->first('darlehensbetrag') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="darlehensbetrag" value="@if(old('darlehensbetrag')){{old('darlehensbetrag')}}@elseif($loan_mirror){{number_format($loan_mirror->darlehensbetrag,2,',','.')}}@endif" name="darlehensbetrag">
            </div>
            <div class="col-lg-4">
                <label>Darlehensnummer:</label>
                @if ($errors->has('darlehensnummer'))
                    <div class="error">{{ $errors->first('darlehensnummer') }}</div>
                @endif
                <input type="text" class="form-control" id="darlehensnummer" value="@if(old('darlehensnummer')){{old('darlehensnummer')}}@elseif($loan_mirror){{ $loan_mirror->darlehensnummer }}@endif" name="darlehensnummer">
            </div>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Darlehensart:</label>
                @if ($errors->has('darlehensart'))
                    <div class="error">{{ $errors->first('darlehensart') }}</div>
                @endif
                <select class="form-control" name="darlehensart" id="darlehensart">
                    <option value="Annuitätendarlehen" {{ (old('darlehensart') == 'Annuitätendarlehen') ? 'selected' : ( ($loan_mirror && $loan_mirror->darlehensart == 'Annuitätendarlehen') ? 'selected' : '' ) }}>Annuitätendarlehen</option>
                    <option value="Tilgungsdarlehen" {{ (old('darlehensart') == 'Tilgungsdarlehen') ? 'selected' : ( ($loan_mirror && $loan_mirror->darlehensart == 'Tilgungsdarlehen') ? 'selected' : '' ) }}>Tilgungsdarlehen</option>
                    <option value="Endfälliges Darlehen" {{ (old('darlehensart') == 'Endfälliges Darlehen') ? 'selected' : ( ($loan_mirror && $loan_mirror->darlehensart == 'Endfälliges Darlehen') ? 'selected' : '' ) }}>Endfälliges Darlehen</option>
                </select>
            </div>
            <div class="col-lg-4">
                <label>Zahlungsrythmus:</label>
                <select class="form-control" name="zahlungsrythmus">
                    <option value="Monatlich" {{ (old('zahlungsrythmus') == 'Monatlich') ? 'selected' : ( ($loan_mirror && $loan_mirror->zahlungsrythmus == 'Monatlich') ? 'selected' : '' ) }}>Monatlich</option>
                    <option value="Quartalsweise" {{ (old('zahlungsrythmus') == 'Quartalsweise') ? 'selected' : ( ($loan_mirror && $loan_mirror->zahlungsrythmus == 'Quartalsweise') ? 'selected' : '' ) }}>Quartalsweise</option>
                    <option value="Endfällig" {{ (old('zahlungsrythmus') == 'Endfällig') ? 'selected' : ( ($loan_mirror && $loan_mirror->zahlungsrythmus == 'Endfällig') ? 'selected' : '' ) }}>Endfällig</option>
                </select>
            </div>

        </div>

        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Zinszahlungsbeginn:</label>
                @if($errors->has('zinszahlungsbeginn'))
                    <div class="error">{{ $errors->first('zinszahlungsbeginn') }}</div>
                @endif
                <input type="text" class="form-control mask-date-input-new" value="{{ (isset($loan_mirror->zinszahlungsbeginn)) ? show_date_format($loan_mirror->zinszahlungsbeginn) : '' }}" name="zinszahlungsbeginn" placeholder="DD.MM.YYYY">
            </div>
            <div class="col-lg-4">
                <label>Tilgungsbeginn:</label>
                @if($errors->has('tilgungsbeginn'))
                    <div class="error">{{ $errors->first('tilgungsbeginn') }}</div>
                @endif
                <input type="text" class="form-control mask-date-input-new" value="{{ (isset($loan_mirror->tilgungsbeginn)) ? show_date_format($loan_mirror->tilgungsbeginn) : '' }}" name="tilgungsbeginn" placeholder="DD.MM.YYYY">
            </div>
        </div>

        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Auszahlungsdatum:</label>
                @if ($errors->has('payout_date'))
                    <div class="error">{{ $errors->first('payout_date') }}</div>
                @endif
                @php
                    $payout_date = "";
                    if(old('payout_date')){
                        $payout_date = show_date_format(old('payout_date'));
                    }elseif($loansMirrorData && $loansMirrorData->payout_date){
                        $payout_date = show_date_format($loansMirrorData->payout_date);
                    }elseif($loan_mirror && $loan_mirror->payout_date){
                        $payout_date = show_date_format($loan_mirror->payout_date);
                    }

                    if(@$buy_data[402]['comment']):
                    $payout_date = show_date_format($buy_data[402]['comment']);
                    endif;


                @endphp
                <input type="text" class="form-control mask-date-input-new" id="payout_date" value="{{$payout_date}}" name="payout_date" placeholder="DD.MM.YYYY">
            </div>
            <div class="col-lg-4">
                <label>Laufzeitende:</label>
                @if($errors->has('laufzeitende'))
                    <div class="error">{{ $errors->first('laufzeitende') }}</div>
                @endif
                <input type="text" class="form-control mask-date-input-new" value="{{ (isset($loan_mirror->laufzeitende)) ? show_date_format($loan_mirror->laufzeitende) : '' }}" name="laufzeitende" placeholder="DD.MM.YYYY">
            </div>
        </div>


        
        <legend>2.) Konditionen</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Bereitstellungszinsen p.a.:</label>
                @if($errors->has('bereitstellungszinsen'))
                    <div class="error">{{ $errors->first('bereitstellungszinsen') }}</div>
                @endif
                <input type="text" class="form-control percent-mask-input" id="bereitstellungszinsen" value="@if(old('bereitstellungszinsen')){{old('bereitstellungszinsen')}}@elseif($loan_mirror){{number_format($loan_mirror->bereitstellungszinsen,2,',','.')}}@endif" name="bereitstellungszinsen">
            </div>
            <div class="col-lg-4">
                <label>ab:</label>
                @if($errors->has('ab'))
                    <div class="error">{{ $errors->first('ab') }}</div>
                @endif
                <input type="text" class="form-control mask-date-input-new" id="ab" name="ab" placeholder="DD.MM.YYYY" value="@if(old('ab')){{old('ab')}}@elseif($loan_mirror){{ show_date_format($loan_mirror->ab) }}@endif">
            </div>
            <br>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Anfänglicher Tilgungssatz p.a.:</label>
                @if($errors->has('tilgungssatz'))
                    <div class="error">{{ $errors->first('tilgungssatz') }}</div>
                @endif
                <input type="text" class="form-control percent-mask-input" id="tilgungssatz" value="@if(old('tilgungssatz')){{old('tilgungssatz')}}@elseif($loan_mirror){{number_format($loan_mirror->tilgungssatz,2,',','.')}}@endif" name="tilgungssatz">
            </div>
            <div class="col-lg-4">
                <label>Zinsart:</label>
                @if ($errors->has('zinsart'))
                    <div class="error">{{ $errors->first('zinsart') }}</div>
                @endif
                <select class="form-control" name="zinsart">
                    <option value="Festverzinslich" {{ (old('zinsart') == 'Festverzinslich') ? 'selected' : ( ($loan_mirror && $loan_mirror->zinsart == 'Festverzinslich') ? 'selected' : '' ) }}>Festverzinslich</option>
                    <option value="variabel" {{ (old('zinsart') == 'variabel') ? 'selected' : ( ($loan_mirror && $loan_mirror->zinsart == 'variabel') ? 'selected' : '' ) }}>variabel</option>
                </select>
            </div>
            <br>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Anfangszins p.a.:</label>
                @if ($errors->has('anfangszins'))
                    <div class="error">{{ $errors->first('anfangszins') }}</div>
                @endif
                <input type="text" class="form-control percent-mask-input" id="anfangszins" name="anfangszins" value="@if(old('anfangszins')){{old('anfangszins')}}@elseif($loan_mirror){{number_format($loan_mirror->anfangszins,2,',','.')}}@endif">
            </div>
            <div class="col-lg-4">
                <label>Aktueller Zins</label>
                @if ($errors->has('aktueller_zins'))
                    <div class="error">{{ $errors->first('aktueller_zins') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="aktueller_zins" name="aktueller_zins"  value="@if(old('aktueller_zins')){{old('aktueller_zins')}}@elseif($loan_mirror){{number_format($loan_mirror->aktueller_zins,2,',','.')}}@endif" readonly>
            </div>
            <br>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Zinsberechnungsmethode:</label>
                @if ($errors->has('zinsberechnungsmethode'))
                    <div class="error">{{ $errors->first('zinsberechnungsmethode') }}</div>
                @endif

                <select class="form-control" name="zinsberechnungsmethode">
                    <option value="Act/Act" {{ (old('zinsberechnungsmethode') == 'Act/Act') ? 'selected' : ( ($loan_mirror && $loan_mirror->zinsberechnungsmethode == 'Act/Act') ? 'selected' : '' ) }}>Act/Act</option>
                    <option value="Act/360" {{ (old('zinsberechnungsmethode') == 'Act/360') ? 'selected' : ( ($loan_mirror && $loan_mirror->zinsberechnungsmethode == 'Act/360') ? 'selected' : '' ) }}>Act/360</option>
                    <option value="Act/365" {{ (old('zinsberechnungsmethode') == 'Act/365') ? 'selected' : ( ($loan_mirror && $loan_mirror->zinsberechnungsmethode == 'Act/365') ? 'selected' : '' ) }}>Act/365</option>
                    <option value="30/360" {{ (old('zinsberechnungsmethode') == '30/360') ? 'selected' : ( ($loan_mirror && $loan_mirror->zinsberechnungsmethode == '30/360') ? 'selected' : '' ) }}>30/360</option>
                </select>
            </div>

            <div class="col-lg-4">
                <label>Zinsbindungsende:</label>
                @if($errors->has('zinsbindungsende'))
                    <div class="error">{{ $errors->first('zinsbindungsende') }}</div>
                @endif
                <input type="text" class="form-control mask-date-input-new" id="zinsbindungsende" name="zinsbindungsende" placeholder="DD.MM.YYYY" value="@if(old('zinsbindungsende')){{old('zinsbindungsende')}}@elseif($loan_mirror){{ show_date_format($loan_mirror->zinsbindungsende) }}@endif">
            </div>
            <br>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Euribor:</label>
                @if($errors->has('euribor'))
                    <div class="error">{{ $errors->first('euribor') }}</div>
                @endif
                <select class="form-control" name="euribor">
                    <option value="3M" {{ (old('euribor') == '3M') ? 'selected' : ( ($loan_mirror && $loan_mirror->euribor == '3M') ? 'selected' : '' ) }}>3M</option>
                    <option value="6M" {{ (old('euribor') == '6M') ? 'selected' : ( ($loan_mirror && $loan_mirror->euribor == '6M') ? 'selected' : '' ) }}>6M</option>
                </select>
            </div>
            {{-- <div class="col-lg-4">
                <label>Höhe Euribor:</label>
                @if($errors->has('höhe_euribor'))
                    <div class="error">{{ $errors->first('höhe_euribor') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="höhe_euribor" name="höhe_euribor"  value="@if(old('höhe_euribor')){{old('höhe_euribor')}}@elseif($loan_mirror){{number_format($loan_mirror->höhe_euribor,2,',','.')}}@endif">
            </div> --}}
            <div class="col-lg-4 {{ (!$loan_mirror) || (old('euribor') == '3M' || (isset($loan_mirror->euribor) && $loan_mirror->euribor == '3M')) ? '' : 'hidden' }}" id="höhe_euribor_3_monate_div">
                <label>Höhe Euribor 3 Monate:</label>
                @if($errors->has('höhe_euribor_3_monate'))
                    <div class="error">{{ $errors->first('höhe_euribor_3_monate') }}</div>
                @endif
                <?php
                    $behandlung_negativzins = (isset($loan_mirror->behandlung_negativzins)) ? $loan_mirror->behandlung_negativzins : '';
                    $anfangszins = (isset($loan_mirror->anfangszins)) ? $loan_mirror->anfangszins : 0;
                    $höhe_euribor_3_monate = (isset($aDataTableHeaderHTML[5])) ? $aDataTableHeaderHTML[5] : 0;
                    $höhe_euribor_3_monate = ($behandlung_negativzins == 'Euribor=0' && $höhe_euribor_3_monate < 0) ? $anfangszins : $höhe_euribor_3_monate;
                ?>
                <input type="text" class="form-control" id="höhe_euribor_3_monate" name="höhe_euribor_3_monate" readonly="readonly" data-val="{{ (isset($aDataTableHeaderHTML[5])) ? number_format($aDataTableHeaderHTML[5],2,',','.') : 0 }}" value="{{number_format($höhe_euribor_3_monate,2,',','.')}}">
            </div>

            <div class="col-lg-4 {{ (old('euribor') == '6M' || (isset($loan_mirror->euribor) && $loan_mirror->euribor == '6M')) ? '' : 'hidden' }}" id="höhe_euribor_6_monate_div">
                <label>Höhe Euribor 6 Monate:</label>
                @if($errors->has('höhe_euribor_6_monate'))
                    <div class="error">{{ $errors->first('höhe_euribor_6_monate') }}</div>
                @endif
                <?php 
                    $höhe_euribor_6_monate = (isset($aDataTableHeaderHTML[7])) ? $aDataTableHeaderHTML[7] : 0;
                    $höhe_euribor_6_monate = ($behandlung_negativzins == 'Euribor=0' && $höhe_euribor_6_monate < 0) ? $anfangszins : $höhe_euribor_6_monate;
                ?>
                <input type="text" class="form-control" id="höhe_euribor_6_monate" name="höhe_euribor_6_monate" readonly="readonly" data-val="{{ (isset($aDataTableHeaderHTML[7])) ? number_format($aDataTableHeaderHTML[7],2,',','.') : 0 }}"  value="{{number_format($höhe_euribor_6_monate,2,',','.')}}">
            </div>

            <br>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Behandlung Negativzins:</label>
                @if($errors->has('behandlung_negativzins'))
                    <div class="error">{{ $errors->first('behandlung_negativzins') }}</div>
                @endif
                <select class="form-control" name="behandlung_negativzins">
                    <option value="Euribor=0" {{ (old('behandlung_negativzins') == 'Euribor=0') ? 'selected' : ( ($loan_mirror && $loan_mirror->behandlung_negativzins == 'Euribor=0') ? 'selected' : '' ) }}>Euribor=0</option>
                    <option value='Feld "Höhe Euribor"' {{ (old('behandlung_negativzins') == 'Feld "Höhe Euribor"') ? 'selected' : ( ($loan_mirror && $loan_mirror->behandlung_negativzins == 'Feld "Höhe Euribor"') ? 'selected' : '' ) }}>Feld "Höhe Euribor"</option>
                </select>
            </div>
            <div class="col-lg-4">
                <label>Veränderungsgrenze (in %-Punkten):</label>
                @if($errors->has('veränderungsgrenze'))
                    <div class="error">{{ $errors->first('veränderungsgrenze') }}</div>
                @endif
                <input type="text" class="form-control percent-mask-input" id="veränderungsgrenze" name="veränderungsgrenze"  value="@if(old('veränderungsgrenze')){{old('veränderungsgrenze')}}@elseif($loan_mirror){{number_format($loan_mirror->veränderungsgrenze,2,',','.')}}@endif">
            </div>
            <br>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Maßgeblicher Euribor p.a.:</label>
                @if($errors->has('mabgeblicher_euribor'))
                    <div class="error">{{ $errors->first('mabgeblicher_euribor') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input-negetive" id="mabgeblicher_euribor" name="mabgeblicher_euribor"  value="@if(old('mabgeblicher_euribor')){{old('mabgeblicher_euribor')}}@elseif($loan_mirror){{number_format($loan_mirror->mabgeblicher_euribor,2,',','.')}}@endif">
            </div>
            <div class="col-lg-4">
                <label>Ermittlungsturnus:</label>
                @if($errors->has('ermittlungsturnus'))
                    <div class="error">{{ $errors->first('ermittlungsturnus') }}</div>
                @endif
                <select class="form-control" name="ermittlungsturnus">
                    <option value="Monatlich" {{ (old('ermittlungsturnus') == 'Monatlich') ? 'selected' : ( ($loan_mirror && $loan_mirror->ermittlungsturnus == 'Monatlich') ? 'selected' : '' ) }}>Monatlich</option>
                    <option value="quartalsweise" {{ (old('ermittlungsturnus') == 'quartalsweise') ? 'selected' : ( ($loan_mirror && $loan_mirror->ermittlungsturnus == 'quartalsweise') ? 'selected' : '' ) }}>quartalsweise</option>
                </select>
            </div>
            <br>
        </div>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Ermittlungsstichtag:</label>
                @if($errors->has('ermittlungsstichtag'))
                    <div class="error">{{ $errors->first('ermittlungsstichtag') }}</div>
                @endif
                <?php
                    $arr = range(1,31);
                ?>
                <select class="form-control" name="ermittlungsstichtag">
                    @foreach($arr as $l)
                        <option value="{{ $l }}" {{ (old('ermittlungsstichtag') == $l) ? 'selected' : ( ($loan_mirror && $loan_mirror->ermittlungsstichtag == $l) ? 'selected' : '' ) }}>{{ $l }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4">
                <label>Änderung wirksam per</label>
                @if($errors->has('anderung_wirksam_per'))
                    <div class="error">{{ $errors->first('anderung_wirksam_per') }}</div>
                @endif
                <input type="text" class="form-control mask-date-input-new" value="{{ (isset($loan_mirror->anderung_wirksam_per)) ? show_date_format($loan_mirror->anderung_wirksam_per) : '' }}" name="anderung_wirksam_per" placeholder="DD.MM.YYYY">
            </div>
        </div>


        <legend>3.) Liquidität</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Aktuelle Restschuld:</label>
                @if ($errors->has('aktuelle_restschuld'))
                    <div class="error">{{ $errors->first('aktuelle_restschuld') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="aktuelle_restschuld" name="aktuelle_restschuld"  value="@if(old('aktuelle_restschuld')){{old('aktuelle_restschuld')}}@elseif($loan_mirror){{number_format($loan_mirror->aktuelle_restschuld,2,',','.')}}@endif">

            </div>
            <div class="col-lg-4">
                <label>Restschuld per 30.06.:</label>
                @if ($errors->has('restschuld_per_1'))
                    <div class="error">{{ $errors->first('restschuld_per_1') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="restschuld_per_1" name="restschuld_per_1"  value="@if(old('restschuld_per_1')){{old('restschuld_per_1')}}@elseif($loan_mirror){{number_format($loan_mirror->restschuld_per_1,2,',','.')}}@endif">

            </div>
            
        </div>

        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Restschuld per 31.12.:</label>
                @if ($errors->has('restschuld_per_2'))
                    <div class="error">{{ $errors->first('restschuld_per_2') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="restschuld_per_2" name="restschuld_per_2"  value="@if(old('restschuld_per_2')){{old('restschuld_per_2')}}@elseif($loan_mirror){{number_format($loan_mirror->restschuld_per_2,2,',','.')}}@endif">

            </div>
            <div class="col-lg-4">
                <label>Zinsaufwand p.a.:</label>
                @if ($errors->has('zinsaufwand'))
                    <div class="error">{{ $errors->first('zinsaufwand') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="zinsaufwand" name="zinsaufwand"  value="@if(old('zinsaufwand')){{old('zinsaufwand')}}@elseif($loan_mirror){{number_format($loan_mirror->zinsaufwand,2,',','.')}}@endif">
            </div>
            
        </div>

        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Tilgung p.a.:</label>
                @if ($errors->has('tilgung'))
                    <div class="error">{{ $errors->first('tilgung') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="tilgung" name="tilgung"  value="@if(old('tilgung')){{old('tilgung')}}@elseif($loan_mirror){{number_format($loan_mirror->tilgung,2,',','.')}}@endif">

            </div>
            <div class="col-lg-4">
                <label>Kapitaldienst p.a.:</label>
                @if ($errors->has('kapitaldienst'))
                    <div class="error">{{ $errors->first('kapitaldienst') }}</div>
                @endif
                <?php 
                    $kapitaldienst = 0;
                    if($loan_mirror){
                        $kapitaldienst = ( $loan_mirror->zinsaufwand + $loan_mirror->tilgung );
                    }
                ?>
                <input type="text" class="form-control mask-number-input" id="kapitaldienst" name="kapitaldienst"  value="{{number_format($kapitaldienst,2,',','.')}}">

            </div>
            
        </div>


        <legend>4.) Aufvalutierung</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Verkehrswert:</label>
                <?php 
                    $verkehrswert = 0;
                    if($properties && $market_value){
                        $verkehrswert = $market_value;
                    }elseif($loan_mirror && $loan_mirror->verkehrswert){
                        $verkehrswert = $loan_mirror->verkehrswert;
                    }
                ?>
                <input type="text" class="form-control mask-number-input" id="verkehrswert" name="verkehrswert" value="{{ number_format($verkehrswert,2,',','.') }}">
                 

            </div>
            <div class="col-lg-4">
                <label>Delta:</label>
                @if ($errors->has('delta4'))
                    <div class="error">{{ $errors->first('delta4') }}</div>
                @endif
                <?php
                    $aktuelle_restschuld = ($loan_mirror) ? $loan_mirror->aktuelle_restschuld : 0;
                    $delta4 = $verkehrswert - $aktuelle_restschuld;
                ?>
                <input type="text" class="form-control mask-number-input" id="delta4" name="delta4"  value="@if(old('delta4')){{old('delta4')}}@else{{number_format($delta4,2,',','.')}}@endif">

            </div>
            
        </div>



        <legend>5.) Besicherung</legend>
        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Grundschuld:</label>
                @if ($errors->has('grundschuld'))
                    <div class="error">{{ $errors->first('grundschuld') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="grundschuld" name="grundschuld"  value="@if(old('grundschuld')){{old('grundschuld')}}@elseif($loan_mirror){{number_format($loan_mirror->grundschuld,2,',','.')}}@endif">
            </div>
            <div class="col-lg-4">
                <label>Mietabtretung:</label>
                @if ($errors->has('mietabtretung'))
                    <div class="error">{{ $errors->first('mietabtretung') }}</div>
                @endif
                <select class="form-control" name="mietabtretung">
                    <option value="1" {{ (old('mietabtretung') == '1') ? 'selected' : ( ($loan_mirror && $loan_mirror->mietabtretung == '1') ? 'selected' : '' ) }}>Ja</option>
                    <option value="0" {{ (old('mietabtretung') == '0') ? 'selected' : ( ($loan_mirror && $loan_mirror->mietabtretung == '0') ? 'selected' : '' ) }}>Nein</option>
                </select>

            </div>
            
        </div>

        <div class="form-group m-form__group row">
            <div class="col-lg-4">
                <label>Zusatzsicherheit:</label>
                @if ($errors->has('zusatzsicherheit'))
                    <div class="error">{{ $errors->first('zusatzsicherheit') }}</div>
                @endif
                <select class="form-control" name="zusatzsicherheit">
                    <option value="Keine" {{ (old('zusatzsicherheit') == 'Keine') ? 'selected' : ( ($loan_mirror && $loan_mirror->zusatzsicherheit == 'Keine') ? 'selected' : '' ) }}>Keine</option>
                    <option value="Garantie FCR" {{ (old('zusatzsicherheit') == 'Garantie FCR') ? 'selected' : ( ($loan_mirror && $loan_mirror->zusatzsicherheit == 'Garantie FCR') ? 'selected' : '' ) }}>Garantie FCR</option>
                    <option value="Garantie Falk Raudies" {{ (old('zusatzsicherheit') == 'Garantie Falk Raudies') ? 'selected' : ( ($loan_mirror && $loan_mirror->zusatzsicherheit == 'Garantie Falk Raudies') ? 'selected' : '' ) }}>Garantie Falk Raudies</option>
                    <option value="Garantie Sonstiges" {{ (old('zusatzsicherheit') == 'Garantie Sonstiges') ? 'selected' : ( ($loan_mirror && $loan_mirror->zusatzsicherheit == 'Garantie Sonstiges') ? 'selected' : '' ) }}>Garantie Sonstiges</option>
                </select>
            </div>
            <div class="col-lg-4">
                <label>Höhe Zusatzsicherheit:</label>
                @if ($errors->has('höhe_zusatzsicherheit'))
                    <div class="error">{{ $errors->first('höhe_zusatzsicherheit') }}</div>
                @endif
                <input type="text" class="form-control mask-number-input" id="höhe_zusatzsicherheit" name="höhe_zusatzsicherheit"  value="@if(old('höhe_zusatzsicherheit')){{old('höhe_zusatzsicherheit')}}@elseif($loan_mirror){{number_format($loan_mirror->höhe_zusatzsicherheit,2,',','.')}}@endif">
                
            </div>            
        </div>

        <div id="sonstiges_div" class="form-group m-form__group row {{ (old('zusatzsicherheit') == 'Garantie Sonstiges' || $loan_mirror && $loan_mirror->zusatzsicherheit == 'Garantie Sonstiges') ? '' : 'hidden' }}">
            <div class="col-lg-4">
                <label>Sonstiges:</label>
                @if ($errors->has('sonstiges'))
                    <div class="error">{{ $errors->first('sonstiges') }}</div>
                @endif
                <input type="text" class="form-control" id="sonstiges" name="sonstiges"  value="@if(old('sonstiges')){{old('sonstiges')}}@elseif($loan_mirror){{ $loan_mirror->sonstiges }}@endif">
            </div>
        </div>


        

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" class="btn btn-primary">Speichern</button>                   
            </div>
        </div>

        </form>
	</div>
</div>
