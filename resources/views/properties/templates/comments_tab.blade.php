
<div class="row">

        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif


        <ul class="nav nav-tabs">
            <li class="in active"><a data-toggle="tab" href="#comment-tabss">{{__('Aktivitäten')}}</a></li>
            <li class=""><a data-toggle="tab" href="#comment-tabss2">Vermietungsaktivitäten</a></li>
            <li class=""><a data-toggle="tab" href="#comment-tabs3">Adressensuche</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade active in" id="comment-tabss">
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="panel">
                        <div class="panel-heading">{{__('Aktivitäten')}} </div>

                        <div class="white-box">
                            <h3 class="box-title">{{__('property_comments.comments')}}</h3>


        					<div class="comment-center">
                                @foreach ($comments as $comment)
                                    @if(true || $comment->status!='PENDING' || Auth::user()->role==1 || Auth::user()->id==$comment->user->id)
                                    <div class="comment-body">
                                        <div class="user-img"> <img src="{{(config('upload.avatar_path').$comment->user->image)}}" alt="user" class="img-circle"></div>
                                        <div class="mail-contnet">
                                            <h5>{{$comment->user->name}}</h5>
        									<span class="time">{{$comment->created_at}}</span>

        									     <a data-name="created_at"
        											   class="edit-date"
        											   data-type="combodate"
        											   data-pk="{{$comment->id}}"
        											   data-url="{{route('update.comment.date')}}"
        											   data-format="YYYY-MM-DD"
        											   data-viewformat="DD/MM/YYYY"
        											   data-url="{{route('update.comment.date')}}"
        											   data-value="{{$comment->created_at}}">

        											</a>


        									<!--
                                            @if($comment->status=='PENDING')
                                            <span class="label label-rouded label-info">{{$comment->status}}</span>
                                            <br/>
                                            @endif
                                            -->
                                            @if($comment->status=='APPROVED')
                                                <span class="label label-rouded label-success">{{$comment->status}}</span>
                                                <br/>
                                            @endif
                                            @if($comment->status=='REJECTED')
                                                <span class="label label-rouded label-danger">{{$comment->status}}</span>
                                                <br/>
                                            @endif
                                            <span class="mail-desc">{!! $comment->comment !!}</span>

                                            @if($comment->pdf_file)
                                            <a href="{{ asset('pdf_upload/'.$comment->pdf_file) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a><br/>
                                            @endif

                                            <!--
                                            @if($comment->status=='PENDING' &&  Auth::user()->role==1)
                                            <a href="{{route('edit_propertycomment', ['id' => $comment->id, 'value' => 'APPROVED'])}}" class="btn btn btn-rounded btn-default btn-outline m-r-5" name="approve-comment"><i class="ti-check text-success m-r-5"></i>{{__('property_comments.confirm')}}</a>
                                            <a href="{{route('edit_propertycomment', ['id' => $comment->id, 'value' => 'REJECTED'])}}" class="btn-rounded btn btn-default btn-outline"><i class="ti-close text-danger m-r-5"></i>{{__('property_comments.decline')}}</a>
                                            @endif
                                            @if(Auth::user()->role==1 || Auth::user()->id==$comment->user->id)
                                                <a data-toggle="modal" data-target="#myModal" data-value="<?php echo $comment->id ?>" data-content="<?php echo $comment->comment; ?>" class="btn-rounded btn btn-default btn-outline edit_comment"><i class="ti-check text-success m-r-5"></i>{{__('property_comments.edit')}}</a>
                                            @endif
                                            -->
                                            @if(Auth::user()->role==1 || Auth::user()->id==$comment->user->id)
                                            <a href="{{route('edit_propertycomment', ['id' => $comment->id, 'value' => 'DELETE'])}}" class="btn-rounded btn btn-default btn-outline"><i class="ti-close text-danger m-r-5"></i>{{__('property_comments.clear')}}</a>
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                @endforeach

                            </div>


                        </div>
                        {!! Form::open(['action' => ['PropertyCommentsController@create'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal1']) !!}
                        {!! Form::token() !!}

                        <div style="margin-left: 20px;">
                                <input type="hidden" name="property_id" value="{{ $id }}">
                                <!-- <textarea style="width:80%;padding:30px" name="comment" placeholder="{{__('property_comments.leave_a_comment')}}" class="label-rounded" required></textarea> -->

                                <textarea class="form-control" name="comment_text"></textarea>

                                <input style="margin-top: 20px;" type="file" name="pdf"><br>
                                <button style="margin-bottom: 80px; margin-left: 30px" class="btn btn btn-rounded btn-default btn-outline m-r-5"><i class="ti-check text-success m-r-5"></i>{{__('property_comments.release')}}</button>
                        </div>
                        {!! Form::close() !!}

                        <div id="myModal"  class="modal fade" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">{{__('property_comments.edit_comment')}}</h4>
                                    </div>
                                    {!! Form::open(['action' => ['PropertyCommentsController@update_comment'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal1']) !!}
                                    {!! Form::token() !!}
                                    <div class="modal-body">
                                        <input type="hidden" name="comment_id_modal" class="comment_id_modal" type="text" >
                                        <textarea style="width:100%;padding:30px" name="comment_edit_modal" placeholder="{{__('property_comments.comment')}}" class="label-rounded comment_edit_modal" required></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('property_comments.close')}}</button>
                                        <button type="submit" class="btn btn-primary">{{__('property_comments.save_changes')}}</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="white-box">
                    <div class="table-responsive">
                                <form id="ccoment-form" enctype="multipart/form-data">
                                <input type="hidden" name="property_id" value="{{ $id }}">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td>Mahnung</td>
                                            <td><input data-id="1" name="status[1]" class="comment-checkbox" type="checkbox" @if(isset($ccomments[0]['status']) && $ccomments[0]['status']) checked @endif></td>
                                            <td><textarea data-id="1" name="comment[1]" class="form-control comment-text">@if(isset($ccomments[0]['comment'])){{$ccomments[0]['comment']}}@endif</textarea>
                                            <br>
                                            <input type="file" data-id="1" name="f[1]" class="comment-file1">
                                            @if(isset($ccomments[0]['files']) && $ccomments[0]['files'])
                                            <?php
                                            $ar = json_decode($ccomments[0]['files'],true)
                                            ?>
                                            @foreach($ar as $list)
                                            <div class="comment-sec">
                                            <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>

                                            <a href="javascript:void(0)" class="delete-comment-file" data-id="1" data-file="{{$list}}"  style="font-size:13px;color:red;"><i class="fa fa-times" data-file></i></a>
                                            </div>

                                            @endforeach
                                            @endif


                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gerichtliches Mahnverfahren </td>
                                            <td><input data-id="2" name="status[2]" class="comment-checkbox" type="checkbox"  @if(isset($ccomments[1]['status']) && $ccomments[1]['status']) checked @endif></td>
                                            <td><textarea data-id="2" name="comment[2]" class="form-control comment-text">@if(isset($ccomments[1]['comment'])){{$ccomments[1]['comment']}}@endif</textarea>
                                            <br>
                                            <input type="file" data-id="2" name="f[2]" class="comment-file2">
                                            @if(isset($ccomments[1]['files']) && $ccomments[1]['files'])
                                            <?php
                                            $ar = json_decode($ccomments[1]['files'],true)
                                            ?>
                                            @foreach($ar as $list)
                                            <div class="comment-sec">
                                            <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>

                                            <a href="javascript:void(0)" class="delete-comment-file" data-id="2" data-file="{{$list}}"  style="font-size:13px;color:red;"><i class="fa fa-times" data-file></i></a>
                                            </div>
                                            @endforeach
                                            @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Ratenzahlung/Kündigung</td>
                                            <td><input data-id="3" name="status[3]" class="comment-checkbox" type="checkbox"  @if(isset($ccomments[2]['status']) && $ccomments[2]['status']) checked @endif></td>
                                            <td><textarea data-id="3" name="comment[3]" class="form-control comment-text">@if(isset($ccomments[2]['comment'])){{$ccomments[2]['comment']}}@endif</textarea>
                                            <br>
                                            <input type="file" data-id="3" name="f[3]" class="comment-file3">

                                            @if(isset($ccomments[2]['files']) && $ccomments[2]['files'])
                                            <?php
                                            $ar = json_decode($ccomments[2]['files'],true)
                                            ?>
                                            @foreach($ar as $list)
                                            <div class="comment-sec">
                                            <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>

                                            <a href="javascript:void(0)" class="delete-comment-file" data-id="3" data-file="{{$list}}"  style="font-size:13px;color:red;"><i class="fa fa-times" data-file></i></a>
                                            </div>
                                            @endforeach
                                            @endif
                                            </td>
                                        </tr>
                                        <tr>
                                        <td colspan="3">
                                            <button type="button" class="btn btn-primary save-ccomment">Speichern</button>
                                        </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>

            <div class="tab-pane" id="comment-tabss2">
                  <input type="text" class="form-control searchleasecolumn" style="float: left;width: 280px;">
                <button type="button" class="btn btn-primary searchleasecolumn-btn">Suchen</button>

                <form  method="post" class="form-horizontal" id="mt-form" style="margin-top: 15px;">
                  {{ csrf_field() }}
                  <input type="hidden" name="property_id" value="{{ $id }}">
                  <input type="hidden" class="leasesort" value="asc">
                  <input type="hidden" class="leasecolumn" value="type">


                  <div class="col-md-4 col-lg-4 col-sm-12">
                    <button type="button" class="save-mieter btn btn-primary">Speichern</button>
                    <button type="button" class="btn-success btn add-more-mieter">Neu hinzufügen</button>
                  </div>


                  <div class="lease-log">
                  <table class="table table-striped dataTable">
                      <thead>
                        <tr class="head-title-sort">
                            <th class="sorting" data-column="comment_1">Pot. Mieter</th>
                            <th class="sorting" style="width: 150px;" data-column="tenant">Fläche</th>
                            <th class="sorting_asc" style="width: 150px;" data-column="type">Status</th>
                            <th class="sorting" data-column="comment_2">Kommentare</th>
                            <th class="sorting" data-column="updated_at">Date</th>
                            <th></th>
                        </tr>
                      </thead>
                      <tbody class="miter-list">
                        @if($lativity->count())
                        @foreach($lativity as $key=>$list)
                          <tr>
                            <td><input type="text" name="comment_1[]" class="form-control" value="{{$list->comment_1}}"></td>
                            <td>

                                <?php
                                $check = 0;
                                  if($list->tenant=='Sonstige Fläche')
                                    $check = 1;

                                  foreach($tenant_name as $list1){
                                    if($list->tenant==$list1){
                                      $check = 1;
                                    }
                                  }
                                ?>
                                @if($check)
                                @if($list->type=='Zusage')
                                {{$list->tenant}}
                                <input type="hidden" name="tenant[]" class="form-control" value="{{$list->tenant}}">
                                @else
                                <select class="form-control" name="tenant[]">
                                <option @if($list->tenant=='Sonstige Fläche') selected @endif value="Sonstige Fläche">Sonstige Fläche</option>
                                @foreach($tenant_name as $list1)
                                <option @if($list->tenant==$list1) selected @endif value="{{$list1}}">{{$list1}}</option>
                                @endforeach
                                </select>
                                @endif

                                @else
                                {{$list->tenant}}
                                <input type="hidden" name="tenant[]" class="form-control" value="{{$list->tenant}}">
                                @endif
                            </td>
                            <?php
                            $clss = "";
                            if($list->type=='Zusage')
                              $clss = "select-bg-green";
                            if($list->type=='Mietvertrag ausgetauscht')
                              $clss = "select-bg-yellow";
                            if($list->type=='Absage')
                              $clss = "select-bg-red";

                            ?>
                            <td>
                                <select class="form-control lease-type {{$clss}}" name="type[]">
                        <option @if($list->type=='Zusage') selected @endif value="Zusage">Zusage</option>
                        <option @if($list->type=='Mietvertrag ausgetauscht') selected @endif value="Mietvertrag ausgetauscht">Mietvertrag ausgetauscht </option>
                        <option @if($list->type=='Besichtigung') selected @endif value="Besichtigung">Besichtigung</option>
                        <option @if($list->type=='Expose verschickt') selected @endif value="Expose verschickt">Expose verschickt</option>
                        <option @if($list->type=='angefragt') selected @endif value="angefragt">angefragt</option>
                        <option @if($list->type=='vorgemerkt') selected @endif value="vorgemerkt">vorgemerkt</option>
                        <option @if($list->type=='Absage') selected @endif value="Absage">Absage</option>
                        <option @if($list->type=='In Prüfung') selected @endif value="In Prüfung">In Prüfung</option>
                    </select>
                            </td>
                            <td>
                                <textarea name="comment_2[]" class="form-control">{{$list->comment_2}}</textarea>

                            </td>
                            <td>{{date('d.m.Y',strtotime($list->updated_at))}}</td>
                            <td>
                                @if($key==0)
                                  <button type="button" class="btn-success btn-sm add-more-mieter">+</button>
                                  @else
                                  <button type="button" class="btn-danger btn-sm remove-mieter">-</button>
                                  @endif
                            </td>

                          </tr>
                          @endforeach
                          @else
                          <tr>
                              <td>
                                  <input type="text" name="comment_1[]" class="form-control" value="">
                              </td>
                              <td>
                                  <select class="form-control" name="tenant[]">
                                    <option value="Sonstige Fläche">Sonstige Fläche</option>
                                    @foreach($tenant_name as $list)
                                    <option value="{{$list}}">{{$list}}</option>
                                    @endforeach
                                </select>
                              </td>
                              <td>
                                  <select class="form-control lease-type" name="type[]">
                                        <option value="Zusage">Zusage</option>
                                        <option value="Mietvertrag ausgetauscht">Mietvertrag ausgetauscht </option>
                                        <option value="Besichtigung">Besichtigung</option>
                                        <option value="Expose verschickt">Expose verschickt</option>
                                        <option value="angefragt">angefragt</option>
                                        <option value="vorgemerkt">vorgemerkt</option>
                                        <option value="Absage">Absage</option>
                                        <option value="In Prüfung">In Prüfung</option>
                                    </select>
                              </td>
                              <td>
                                <textarea name="comment_2[]" class="form-control"></textarea>
                              </td>
                              <td></td>
                              <td>
                                  <button type="button" class="btn-success btn-sm add-more-mieter">+</button>
                              </td>
                          </tr>
                          @endif
                      </tbody>
                  </table>
              </div>



                <div class="col-md-2 col-lg-2 col-sm-12">
                    <button type="button" class="save-mieter btn btn-primary">Speichern</button>
                </div>

                </form>
            </div>
            <div class="tab-pane" id="comment-tabs3">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="card-title">Adressen</h4>
                        </div>
                        <div class="col-md-4">
                            {{--<a href="{{url('property/'.Request::segment(2).'/address/email/logs')}}" class="btn btn-primary">Email Logs</a>--}}
                            <a class="btn btn-primary addressenCreate">Hinzufügen</a>
                        </div>
                    </div>
                </div>
                <div  style="overflow-x:auto;  white-space: nowrap;" class="col-sm-12 table-responsive white-box">
                    <table  class="table table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>Keyword</th>
                            <th>Limit</th>
                            <th>Status</th>
                            <th>Scrape Type</th>
                            <th>File</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody class=" table-responsive">


                        @isset($propertyAddressesJobs)
                            @foreach($propertyAddressesJobs as $addressesJob)
                                <tr>
                                    <td>{{$addressesJob->keyword}}</td>
                                    <td>{{$addressesJob->limit_max}}</td>
                                    <td>{{$addressesJob->status}}</td>
                                    <td>{{$addressesJob->type}}</td>
                                    <td><a target="_blank" href="{{url('/excel',$addressesJob->result)}}">{{$addressesJob->result}}</a></td>
                                    <td><label><input type="checkbox" data-id="{{$addressesJob->id}}" @if($addressesJob->is_status_checked)checked @endif class="checkbox-is-sent-addressen" value=""></label></td>
                                    <td class="">

                                        @if(!$addressesJob->result)
                                            <a  href="{{route('file',$addressesJob->id)}}" class="btn-sm btn-info">Datei</a>
                                        @else
                                            <a href="{{ asset('address/'.$addressesJob->result) }}" class="btn-sm btn-info">Herunterladen</a>
                                            <a href="" data-id="{{$addressesJob->id}}" data-filename="{{$addressesJob->result}}" id="csvEmail" data-toggle="modal"  class="email-addressen btn-sm btn-info">Send Email</a>
                                        @endif
                                            <a type="button" href="{{url('address/edit',$addressesJob->id)}}" class="btn-sm btn-info">Bearbeiten</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endisset
                        </tbody>
                    </table>
                </div>



                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="card-title">Email Log</h4>
                        </div>
                    </div>
                </div>
                <div  style="overflow-x:auto;  white-space: nowrap;" class="col-sm-12 table-responsive white-box">
                    <table   class="table table-responsive table-striped" id="emailLogs">
                        <thead>
                        <tr>
                            <th>File Name</th>
                            <th>Email Sent To</th>
                            <th>Sent By</th>
                            <th>Sender Email</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        @if($propertyAddressesJobs)

                            <tbody class=" table-responsive">

                                @foreach($propertyAddressesJobs as $propertyAddressesJob)

                                    @foreach($propertyAddressesJob->emailLogs as $data)
                                        <tr>
                                        <td>{{$propertyAddressesJob->result}}</td>
                                        <td>{{$data->email}}</td>
                                        <td>{{$data->creator->name}}</td>
                                        <td>{{$data->creator->email}}</td>
                                        <td>{{$data->created_at}}</td>
                                        </tr>

                                    @endforeach
                                @endforeach

                            </tbody>
                        @endif

                    </table>
                </div>
            </div>


        </div>

        </div>

    <div class=" hidden">
    <table class="">
        <tbody class="miter">
        <tr>
          <td>
              <input type="text" name="comment_1[]" class="form-control" value="">
          </td>
          <td>
              <select class="form-control" name="tenant[]">
                <option value="Sonstige Fläche">Sonstige Fläche</option>
                @foreach($tenant_name as $list)
                <option value="{{$list}}">{{$list}}</option>
                @endforeach
            </select>
          </td>
          <td>
              <select class="form-control lease-type" name="type[]">
                    <option value="Zusage">Zusage</option>
                    <option value="Mietvertrag ausgetauscht">Mietvertrag ausgetauscht </option>
                    <option value="Besichtigung">Besichtigung</option>
                    <option value="Expose verschickt">Expose verschickt</option>
                    <option value="angefragt">angefragt</option>
                    <option value="vorgemerkt">vorgemerkt</option>
                    <option value="Absage">Absage</option>
                    <option value="In Prüfung">In Prüfung</option>
                </select>
          </td>
          <td><textarea name="comment_2[]" class="form-control"></textarea></td>
          <td></td>
          <td>
              <button type="button" class="btn-danger btn-sm remove-mieter">-</button>
          </td>
      </tr>
      </tbody>
  </table>
</div>

<div class=" modal fade" role="dialog" id="csv_email">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mail </h4>
            </div>
            <form  class="tagForm" id="tag-form"   action="{{ url('/sendEmail') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="address_id"  id="address_id" value="" >
                    <label>Subject</label>
                    <input id="subject" class="form-control"  type="text" name="subject" value="">
                    <span id="subject-field"   style="visibility:hidden;color: red;">This field is Required!</span>
                    <br>
                    <label>Body</label>
                    <textarea class="form-control" id="message"  name="message" required></textarea>
                    <span id="message-field"  style=" visibility:hidden;color: red;">This field is Required!</span>
                    <br>
                    <label for="email">Emails:</label>
                    <select  required class="form-control "  id="emailSelector" name="file_emails[]"  style="width: 100%"></select>
                    <span id="file_emails"  style=" visibility:hidden;color: red;">This field is Required!</span>
                    <br>
                    <label>Attachment</label>
                    <input type="file" id="file" />
                    <span id="attachment-field"  style="visibility:hidden;color: red;">This field is Required!</span>
                    <br>

                </div>
                <div class="modal-footer">
                    <span id="loader" style="visibility:hidden"><i class="fa fa-spin fa-spinner"></i></span>
                    <a id="tag-form-submit" class="btn btn-primary" >Senden</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                </div>
            </form>
        </div>
    </div>
</div>




<div class=" modal fade" role="dialog" id="addressCreate">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title"> Projekt erstellen</h4>
            </div>
            <div class="modal-body">
                <form id="projectForm" class="projectform" method="post" action="">
                        <div class="form-group" >
                            <label for="email">Keyword:</label>
                            <input required type="text" class="form-control" name="keyword" maxlength="105"  placeholder="Enter Keyword" id="keyword">
                        </div>
                        <div class="form-group" >
                            <label for="email">Limit:</label>
                            <input required type="number" class="form-control" name="limit_max"  id="limit_max" placeholder="Enter Limit">
                        </div>
                        <div class="form-group" >
                            <label for="email">Scrape Type:</label>
                            <select class="form-control" name="type" id="type">
                                <option value="">Select</option>
                                <option value="Web Scrape">Web Scrape</option>
                                <option value="Business Scrape">Business Scrape</option>
                            </select>
                        </div>
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                    <input type="hidden" name="property_id" id="propId" value="<?php echo Request::segment(2); ?>">
                      <button type="submit"  id="btn-save" class="btn btn-primary">Speichern</button>
                </form>
            </div>
        </div>
    </div>
</div>


<script>

    $(document).ready(function() {

        $(".search").select2({

            minimumInputLength: 2,
            multiple: true,
            tags: true,
        });

        $("#emailSelector").select2({

            multiple: true,
            tags: true,
        });


    });

    $('.addressenCreate').on('click', function () {
        $('#btn-save').val("create");
        var id = $(this).attr('data-id');
        var prop = $('#propId').val();
        var url ='/properties/'+prop+'/project';
        $('#projectForm').attr('action', url);
        $('#keyword').val('');
        $('#limit_max').val('');
        $('#type').val('');
        $('#title').html('Create Address');
        $('#addressCreate').modal('show');

    });

    $(document).ready(function () {
        $('#projectForm').submit(function (e) {
            $('#config-loader').css('visibility', 'visible');
            var state = $('#btn-save').val();
            e.preventDefault();
            var prop = $('#propId').val();
            var url ='/adressen';

            if (state == "update") {
                var id = $('#projectId').val();
                var url ='/properties/'+prop+'/project/update'+'/'+id;
            }

            $.ajax({
                type : 'post',
                url : url,
                data :   $('form.projectform').serialize(),
                success : function (data)
                {
                    $('#config-loader').css('visibility', 'hidden');
                    $('#save-config').removeClass('disabled');
                    window.setTimeout(function(){
                        $('#addressCreate').modal('hide');
                        sweetAlert("Saved Successfully");
                    }, 600);
                    location.reload();
                }
            });

        });
    });



    $('.edit').on('click', function () {
        $('#btn-save').val("update");
        $('#title').html('Edit Address');
        var id = $(this).attr('data-id');
        var url ='/demo/update'+'/'+id;
        $('.search').val('');
        $.ajax({
            type : 'get',
            url : "{{url('address/edit')}}"+'/'+id,
            success : function (data)
            {
                $('#projectForm').attr('action', url);

                $('#keyword').val(data.keyword);
                $('#limit_max').val(data.limit_max);
                $('#type').val('');
            }

        });
        $('#addressCreate').modal('show');

    });



    $('.email-addressen').on('click', function () {

        $('#attachment-field').css('visibility', 'hidden');
        $('#subject-field').css('visibility', 'hidden');
        $('#message-field').css('visibility', 'hidden');
        $('#file_emails').css('visibility', 'hidden');

        $("#subject").val('');
        $("#file").val('');
        $('#emailSelector').html('');

        CKEDITOR.replace( 'message' );
        CKEDITOR.instances['message'].destroy(true);
        CKEDITOR.replace( 'message' );


        $('#csv_email').modal('show');
        var id = $(this).attr('data-id');
        $("#address_id").val(id);
        var name = $(this).attr('data-filename');


        $.ajax({
            type : 'get',
            data: {
                name: name,
                _token: $('#token').val()
            },
            url : "{{url('/address/file-emails') }}",
            success : function (data)
            {
                $.each(data, function(key) {
                    $('#emailSelector').append('<option  value="'+data[key]+'">'+data[key]+'</option>');
                });

            }
        });
    });

    $('#tag-form-submit').on('click', function () {
        var fd = new FormData();
        var files = $('#file')[0].files[0];
        if (files){
            fd.append('attachment',files);
        }
        fd.append('token',$('#token').val());
        fd.append('address_id',$('#address_id').val());
        fd.append('subject',$('#subject').val());
        fd.append('message',CKEDITOR.instances['message'].getData());
        fd.append('emails',$('.search').val());
        fd.append('file_emails',$('#emailSelector').val());

        console.log($('#emailSelector').val());

        if ($('#subject').val() === '')
        {
            $('#subject-field').css('visibility', 'visible');
        }


        if ($('#emailSelector').val() === null)
        {
            $('#file_emails').css('visibility', 'visible');
        }

        if(CKEDITOR.instances['message'].getData() === ''){

            $('#message-field').css('visibility', 'visible');

        }
        if ($('#subject').val() !== ''  &&  CKEDITOR.instances['message'].getData() !== '' && $('#emailSelector').val()!== null) {
            $('#loader').css('visibility', 'visible');

            $('#attachment-field').hide();
            $('#subject-field').hide();
            $('#message-field').hide();
            $('#file_emails').hide();

            $('#tag-form-submit').addClass('disabled');

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            $.ajax({
                type : 'post',
                url : "{{url('/sendEmail') }}",
                data : fd,
                contentType: false,
                processData: false,
                success : function (data)
                {
                    var message = "Emails Sent Successfully";
                    $('#loader').css('visibility', 'hidden');
                    $('#csv_email').modal('hide');
                    $('#tag-form-submit').removeClass('disabled');
                    if (data){
                        message = data;
                    }
                    window.setTimeout(function(){
                        sweetAlert(message, "success");
                    }, 600);



                }
            });

        }

    });

</script>
<script>







    $('body').on('click', '.checkbox-is-sent-addressen', function() {
        var id = $(this).attr('data-id');
        v = 0;
        if ($(this).is(':checked')) v = 1;
        $.ajax({
            type: 'POST',
            url: '/demo/update' + "/" + id,
            data: {
                is_status_checked: v,
                _token: $('#token').val()
            },
            success: function(data) {
                sweetAlert("Status Updated Successfully");

            }
        });
    });

    $(document).ready(function() {


        var columns = [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ];
        $('#emailLogs').dataTable({
            "pageLength": 25

        });

    });

</script>
