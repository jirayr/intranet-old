
    <legend>Liquiplanung</legend>

    <form action="{{ url('property/save_planning_data') }}" method="post" class="form-horizontal loanMirrorForm" accept-charset="UTF-8" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="property_id" value="{{ $properties->id }}">
        <?php
        $nk = $nt = 0;
        foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
        {
            $nk +=$tenancy_schedule->calculations['total_business_nk_netto'];
            $nk +=$tenancy_schedule->calculations['total_live_nk_netto'];

            $nt = $tenancy_schedule->calculations['total_actual_net_rent'];
        }

        ?>

        <div class="row">

            <div class="col-md-6">
                
                <div class="form-group m-form__group">

                    <div class="col-lg-12" style="padding-top: 5%;">
                        <input @if(@$custom_fields_schl_array['not_consider_in_liquid']) checked @endif name="not_consider_in_liquid" class="liquiplanung_input" id="not_consider_in_liquid" type="checkbox" value="1">
                        <label for="not_consider_in_liquid">nicht für Liquiplanung relevant</label>
                    </div>

                </div>

                <div class="form-group m-form__group">
                    <div class="col-lg-6">
                        <label>Nettomiete p.m.</label>
                        <input type="text" class="form-control" readonly="readonly" value="{{show_number($nt,2)}}">
                    </div>
                    <div class="col-lg-6">
                        <label>NK netto p.m.</label>
                        <input type="text" class="form-control" readonly="readonly" value="{{show_number($nk,2)}}">
                    </div>
                </div>

                <div class="row">
                
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-primary btn-add-ao-cost" data-type="1">ao Einnahmen</button>

                        <div class="table-responsive">
                            <table class="table table-striped" id="table_ao_einnahmen" data-url="{{ route('get_ao_cost', ['property_id' => $id]) }}?type=1">
                                <thead>
                                   <tr>
                                      <th>Frequenz</th>
                                      <th>Notizen</th>
                                      <th>Beträge</th>
                                      <th>Monat</th>
                                      <th>Jahr</th>
                                      <th>Aktion</th>
                                   </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                       </div>
                    </div>

                </div>

            </div>

            <div class="col-md-6">
                    
                <div class="form-group m-form__group">

                    <div class="col-lg-6">
                        <label>Objektaufwand p.m.</label>
                        <input type="text" class="form-control mask-number-input-negetive liquiplanung_input"  name="object_effort_pm" value="{{ show_number(@$custom_fields_schl_array['object_effort_pm'],2) }}">
                    </div>

                    <div class="col-lg-6">
                        <label>Aufwand p.m.</label>
                        <input type="text" class="form-control mask-number-input-negetive liquiplanung_input"  name="effort_pm" value="{{ show_number(@$custom_fields_schl_array['effort_pm'],2) }}">
                    </div>

                </div>

                <div class="form-group m-form__group">

                    <div class="col-lg-6">
                        <label>Aktueller Kontostand</label>
                        <input type="text" class="form-control mask-number-input-negetive liquiplanung_input"  name="current_balance" value="{{ show_number(@$custom_fields_schl_array['current_balance'],2) }}">
                    </div>
                    @if($properties->id==3363)
                    <div class="col-lg-6">
                        <label>Aktiendepot</label>
                        <input type="text" class="form-control mask-number-input liquiplanung_input"  name="share_deposite" value="{{ show_number(@$custom_fields_schl_array['share_deposite'],2) }}">
                    </div>
                    @endif

                </div>

                <div class="row">
                    
                    <div class="col-lg-12">
                        <button type="button" class="btn btn-primary btn-add-ao-cost" data-type="0">ao Kosten hinzufügen</button>


                        <div class="table-responsive">
                             <table class="table table-striped" id="table_ao_cost" data-url="{{ route('get_ao_cost', ['property_id' => $id]) }}?type=0">
                                <thead>
                                   <tr>
                                      <th>Frequenz</th>
                                      <th>Notizen</th>
                                      <th>Beträge</th>
                                      <th>Monat</th>
                                      <th>Jahr</th>
                                      <th>Aktion</th>
                                   </tr>
                                </thead>
                                <tbody></tbody>
                             </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        {{--<div class="form-group m-form__group">
            <div class="col-lg-6">
                <label>Aktueller Kontostand</label>
                <input type="text" class="form-control mask-number-input-negetive"  name="current_balance" value="{{ show_number(@$custom_fields_schl_array['current_balance'],2) }}">
            </div>
        </div>--}}
        
        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <button type="submit" class="btn btn-primary">Speichern</button>                   
            </div>
        </div>

        
        <div class="form-group m-form__group">
            <div class="col-lg-2">
                <label>Cashflow</label>
            </div>
            <div class="col-lg-2">
                    <?php

                        $liquiplanung_ao_costs = DB::table('liquiplanung_ao_costs')->select('amount')->where(['property_id' => $id, 'month' => date('m'), 'year' => date('Y')])->first();
                        $custom_fields_schl_array['extra_cost_pm'] = (isset($liquiplanung_ao_costs->amount)) ? $liquiplanung_ao_costs->amount : 0;
                        

                        $cash_flow = 0;
                        $cash_flow = $nt + $nk;

                        $positive_sum = $cash_flow;

                        $negative_sum = 0;

                        if(@$custom_fields_schl_array['object_effort_pm']){
                            $cash_flow += $custom_fields_schl_array['object_effort_pm'];
                            $negative_sum += $custom_fields_schl_array['object_effort_pm'];
                        }
                        if(@$custom_fields_schl_array['effort_pm'])
                        {
                            $cash_flow += $custom_fields_schl_array['effort_pm'];
                            $negative_sum += $custom_fields_schl_array['effort_pm'];
                        }
                        if(@$custom_fields_schl_array['extra_cost_pm'])
                        {
                            $cash_flow += $custom_fields_schl_array['extra_cost_pm'];
                            $negative_sum += $custom_fields_schl_array['extra_cost_pm'];
                        }
                        if($loansMirrorData && $loansMirrorData->loan_service_month)
                        {
                            $cash_flow -= $loansMirrorData->loan_service_month;
                            $negative_sum -= $loansMirrorData->loan_service_month;
                        }

                    ?>
                @if($cash_flow>0)
                <label style="font-weight: bold;color: green;">
                @else
                <label style="font-weight: bold;color: red;">
                @endif
                    {{show_number($cash_flow,2)}} €
                </label>
             </div>
        </div>

        </form>
        <div id="chart-container" style="height: 300px;"></div>

	@section('chart')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script type="text/javascript">
        
        Highcharts.chart('chart-container', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: 'Cashflow'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
    },
    xAxis: {
        categories: [
            'Cashflow',
        ],
        plotBands: [{ // visualize the weekend
            from: 4.5,
            to: 6.5,
            color: 'rgba(68, 170, 213, .2)'
        }]
    },
    yAxis: {
        title: {
            text: '€'
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' €'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.5
        }
    },
    series: [{
        name: 'Einnahmen',
        data: [{{$positive_sum}}]
    }, {
        name: 'Kosten',
        data: [{{$negative_sum}}]
    },{
        name: 'Cashflow',
        data: [{{$cash_flow}}]
    }]
});
</script>
    @endsection