<div class="col-sm-12 table-responsive white-box">
	<table class="table table-striped" id="am_mail_table">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Nachricht</th>
				<th>Datum/Uhrzeit</th>
			</tr>
		</thead>
		<tbody>
			@if(!empty($assetsManagerMails))
				@php
					$count = 1;
				@endphp
				@foreach ($assetsManagerMails as $key => $value)
					@if($value->type == 0)
						<tr>
							<td>{{ $count }}</td>
							<td>{{ $value->name }}</td>

							<td>
								{!! $value->subject !!}<br>
								
								<?php echo $value->message; ?></td>
							<td>{{ show_datetime_format($value->created_at) }}</td>
						</tr>
						@php
							$count++;
						@endphp
					@endif
				@endforeach
			@endif
		</tbody>
	</table>
</div>