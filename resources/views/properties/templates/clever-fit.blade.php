<div class="white-box table-responsive" id="clever-fit">
    <table class="table" style="width: 100%">
        <tbody>
        <tr>
            <td colspan="11">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">Neuvermietung der Leerstandsflächen in Schleiz</td>
            <td colspan="2">ab November 2018</td>
            <td colspan="2"></td>
            <td colspan="2">ab Dezember 2018</td>
            <td colspan="2"></td>
            <td></td>
        </tr>
        <tr>
            <td>Clever Fit</td>
            <td colspan="3"></td>
            <td></td>
            <td>Tedi</td>
            <td colspan="2"></td>
            <td colspan="3"></td>
            <td></td>
        </tr>

        <tr>
            <th class="border">Jahr</th>
            <th class="border">Nettokaltmiete p.m.</th>
            <th class="border text-right">€/m² NKM</th>
            <th class="border text-right">NKM p.a.</th>
            <td>&nbsp;</td>
            <th class="border">Nettokaltmiete p.m.</th>
            <th class="border text-right">€/m² NKM</th>
            <th class="border text-right">NKM p.a.</th>
            <td colspan="3" style="border: none"></td>
            <td></td>
        </tr>

        <tr>
            <td class="border">1</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_1'] }}</a> €</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_1'] }}</a> €</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_1'] }}</a>€</td>
            <td class="text-right">1</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk=nettokaltmiete_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nettokaltmiete_1'] }}</a>€</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="em_nkm_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['em_nkm_1'] }}</a>€</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="nkm_pa_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nkm_pa_1'] }}</a>€</td>
            <td></td>
            <th style="text-decoration: underline">Kalkulation Invest</th>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td class="border">2</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_2'] }}</a> € </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_2'] }}</a> € </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_2'] }}</a>€ </td>
            <td class="text-right">2</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk=nettokaltmiete_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nettokaltmiete_2'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="em_nkm_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['em_nkm_2'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="nkm_pa_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nkm_pa_2'] }}</a>€ </td>
            <td></td>
            <td>Entkernung Erdgeschoss</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="entkernung_erdgeschoss" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['entkernung_erdgeschoss'] }}</a> €</td>
            <td></td>
        </tr>

        <tr>
            <td class="border">3</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_3'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_3'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_3'] }}</a>€ </td>
            <td class="text-right">3</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk=nettokaltmiete_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nettokaltmiete_3'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="em_nkm_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['em_nkm_3'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="nkm_pa_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nkm_pa_3'] }}</a>€ </td>
            <td></td>
            <td>Schließung der Galerie</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="schliebung_der_galerie" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['schliebung_der_galerie'] }}</a>€ </td>
            <td></td>
        </tr>

        <tr>
            <td class="border">4</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_4'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_4'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_4'] }}</a>€ </td>
            <td class="text-right">4</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk=nettokaltmiete_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nettokaltmiete_4'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="em_nkm_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['em_nkm_4'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="nkm_pa_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nkm_pa_4'] }}</a>€ </td>
            <td></td>
            <td>Ausbaukostenzuschuss Clever Fit</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="ausbaukostenzuschuss_clever_fit" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['ausbaukostenzuschuss_clever_fit'] }}</a>€</td>
            <td></td>
        </tr>

        <tr>
            <td class="border">5</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_5'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_5'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_5'] }}</a>€ </td>
            <td class="text-right">5</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk=nettokaltmiete_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nettokaltmiete_5'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="em_nkm_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['em_nkm_5'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="nkm_pa_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nkm_pa_5'] }}</a>€ </td>
            <td></td>
            <td>Neuinstallation Heizung, Elektro, Lüftung</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="neuinstallation_heizung_elektro_luftung" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['neuinstallation_heizung_elektro_luftung'] }}</a>€</td>
            <td></td>
        </tr>

        <tr>
            <td class="border">6</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_6" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_6'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_6" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_6'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_6" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_6'] }}</a>€ </td>
            <td colspan="5"></td>
            <td>Entsorgung</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="entsorgung" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['entsorgung'] }}</a>€</td>
            <td></td>
        </tr>

        <tr>
            <td class="border">7</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_7" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_7'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_7" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_7'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_7" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_7'] }}</a>€ </td>
            <td></td>
            <td class="text-center">Mittelwert</td>
            <td class="text-center">Mittelwert</td>
            <td class="text-center">Summe</td>
            <td colspan="3"></td>
            <td></td>
        </tr>

        <tr>
            <td class="border">8</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_8" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_8'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_8" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_8'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_8" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_8'] }}</a>€ </td>
            <td></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk=nettokaltmiete_8" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nettokaltmiete_8'] }}</a>€ </td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="em_nkm_8" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['em_nkm_8'] }}</a>€ </td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="nkm_pa_8" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['nkm_pa_8'] }}</a>€ </td>
            <td colspan="3"></td>
            <td></td>
        </tr>

        <tr>
            <td class="border">9</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_9" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_9'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_9" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_9'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_9" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_9'] }}</a> € </td>
            <td colspan="7"></td>
            <td></td>
        </tr>

        <tr>
            <td class="border">10</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_10" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_10'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_10" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_10'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_10" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_10'] }}</a>€ </td>
            <td colspan="6"></td>
            <th class="text-right" style="text-decoration: underline"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="calculation_invest_total" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['calculation_invest_total'] }}</a>€ </th>
            <td></td>
        </tr>

        <tr>
            <td class="border">11</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_11" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_11'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_11" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_11'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_11" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_11'] }}</a>€ </td>
            <td colspan="7"></td>
            <td></td>
        </tr>

        <tr>
            <td class="border">12</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_12" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_12'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_12" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_12'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_12" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_12'] }}</a>€ </td>
            <td colspan="7"></td>
            <td></td>
        </tr>

        <tr>
            <td class="border">13</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_13" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_13'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_13" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_13'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_13" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_13'] }}</a>€ </td>
            <td colspan="7"></td>
            <td></td>
        </tr>

        <tr>
            <td class="border">14</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_14" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_14'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_14" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_14'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_14" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_14'] }}</a>€ </td>
            <td colspan="7"></td>
            <td></td>
        </tr>

        <tr>
            <td class="border">15</td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nettokaltmiete_15" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nettokaltmiete_15'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_em_nkm_15" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_em_nkm_15'] }}</a>€ </td>
            <td class="border text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="jahr_nkm_pa_15" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['jahr_nkm_pa_15'] }}</a>€ </td>
            <td></td>
            <td colspan="6"></td>
            <td></td>
        </tr>

        <tr>
            <td colspan="12">&nbsp;</td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td class="text-center">Mittelwert</td>
            <td class="text-center">Mittelwert</td>
            <td class="text-center">Summe</td>
            <td colspan="7"></td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="mittelwert_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['mittelwert_1'] }}</a>€ </td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="mittelwert_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['mittelwert_2'] }}</a>€ </td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="summe_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['summe_1'] }}</a>€ </td>
            <td colspan="7"></td>
            <td></td>
        </tr>

        <tr>
            <td>Verkauf zu Faktor</td>
            <td></td>
            <td class="text-center">12</td>
            <td class="text-center">13</td>
            <td class="text-center">14</td>
            <td class="text-center">15</td>
            <td class="text-center">16</td>
            <td colspan="4"></td>
            <td></td>
        </tr>

        <tr>
            <td>möglicher Verkaufspreis: </td>
            <td></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="moglicher_verkaufspreis_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['moglicher_verkaufspreis_1'] }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="moglicher_verkaufspreis_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['moglicher_verkaufspreis_2'] }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="moglicher_verkaufspreis_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['moglicher_verkaufspreis_3'] }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="moglicher_verkaufspreis_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['moglicher_verkaufspreis_4'] }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="moglicher_verkaufspreis_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['moglicher_verkaufspreis_5'] }}</a></td>
            <td colspan="4"></td>
            <td></td>
        </tr>

        <tr>
            <td>./.Einkaufspreis (Buchwert)</td>
            <td></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="einkaufspreis_buchwert_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['einkaufspreis_buchwert_1'] }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="einkaufspreis_buchwert_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['einkaufspreis_buchwert_2'] }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="einkaufspreis_buchwert_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['einkaufspreis_buchwert_3'] }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="einkaufspreis_buchwert_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['einkaufspreis_buchwert_4'] }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="einkaufspreis_buchwert_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['einkaufspreis_buchwert_5'] }}</a></td>
            <td colspan="4"></td>
            <td></td>
        </tr>

        <tr>
            <td class="border-bottom">./.Invest (inkl. EK-Rückzahlung)</td>
            <td class="border-bottom"></td>
            <td class="text-right border-bottom"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="invest_inkl_ekruckzahlung_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['invest_inkl_ekruckzahlung_1'] }}</a></td>
            <td class="text-right border-bottom"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="invest_inkl_ekruckzahlung_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['invest_inkl_ekruckzahlung_2'] }}</a></td>
            <td class="text-right border-bottom"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="invest_inkl_ekruckzahlung_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['invest_inkl_ekruckzahlung_3'] }}</a></td>
            <td class="text-right border-bottom"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="invest_inkl_ekruckzahlung_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['invest_inkl_ekruckzahlung_4'] }}</a></td>
            <td class="text-right border-bottom"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="invest_inkl_ekruckzahlung_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['invest_inkl_ekruckzahlung_5'] }}</a></td>
            <td colspan="4"></td>
            <td></td>
        </tr>

        <tr>
            <th>Delta</th>
            <td></td>
            <th class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="delta_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['delta_1'] }}</a></th>
            <th class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="delta_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['delta_2'] }}</a></th>
            <th class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="delta_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['delta_3'] }}</a></th>
            <th class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="delta_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['delta_4'] }}</a></th>
            <th class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="delta_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['delta_5'] }}</a></th>
            <td></td>
            <td colspan="3"></td>
            <td></td>
        </tr>

        {{--31--}}
        <tr>
            <th class="border bg-green">Gewerbe</th>
            <th class="text-center border bg-green">Mietbeginn</th>
            <th class="text-center border bg-green">Mietende</th>
            <th class="text-center border bg-green">€/m²</th>
            <th class="text-center border bg-green">Mietfläche/m²</th>
            <th class="text-center border bg-green"> IST-Nettokaltmiete </th>
            <th class="text-center border bg-gray">Restlaufzeit</th>
            <th class="text-center border bg-gray">Restlaufzeit</th>
            <th colspan="3"></th>
            <td></td>
        </tr>

        {{--32--}}
        <tr>
            <th class="bg-green border"></th>
            <th class="text-center border bg-green"></th>
            <th class="text-center border bg-green"></th>
            <th class="text-center border bg-green"></th>
            <th class="text-center border bg-green">Gewerbe</th>
            <th class="text-center border bg-green">Gewerbe</th>
            <th class="text-center border bg-yellow">11/21/18</th>
            <th class="text-center border bg-gray">in EUR</th>
            <th colspan="3"></th>
            <td></td>
        </tr>

        <tr>
            <td class=" border">Clever Fit</td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="clever_fit_total_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['clever_fit_total_1'] }}</a></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="clever_fit_total_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['clever_fit_total_2'] }}</a></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="clever_fit_total_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['clever_fit_total_3'] }}</a></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="clever_fit_total_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['clever_fit_total_4'] }}</a></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="clever_fit_total_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['clever_fit_total_5'] }}</a>€ </td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="clever_fit_total_6" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['clever_fit_total_6'] }}</a> </td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="clever_fit_total_7" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['clever_fit_total_7'] }}</a></td>
            <td colspan="3"></td>
            <td></td>
        </tr>

        <tr>
            <td class=" border">Tedi</td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="tedi_total_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['tedi_total_1'] }}</a></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="tedi_total_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['tedi_total_2'] }}</a></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="tedi_total_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['tedi_total_3'] }}</a></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="tedi_total_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['tedi_total_4'] }}</a></td>
            <td class="text-right border"> <a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="tedi_total_5" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['tedi_total_5'] }}</a>€ </td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="tedi_total_6" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['tedi_total_6'] }}</a></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="tedi_total_7" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['tedi_total_7'] }}</a></td>
            <td colspan="3"></td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="tedi_total_8" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['tedi_total_8'] }}</a></td>
            <td></td>
        </tr>

        {{--38--}}
        <tr>
            <th class="border bg-green">Gewerbe Vermietet</th>
            <th class="text-center border bg-green"></th>
            <th class="text-center border bg-green"></th>
            <th class="text-center border bg-green"></th>
            <th class="text-right border bg-green"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="gewerbe_vermietet_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['gewerbe_vermietet_1'] }}</a></th>
            <th class="text-right border bg-green"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="gewerbe_vermietet_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['gewerbe_vermietet_2'] }}</a>€ </th>
            <th colspan="2"></th>
            <th colspan="3"></th>
            <td></td>
        </tr>

        <tr>
            <td class=" border">Pot. "Kellerflächen" f. Wohnungen</td>
            <td class="text-right border"></td>
            <td class="text-right border"></td>
            <td class="border">Lager</td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="pot_kellerflachen_f_wohnungen_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['pot_kellerflachen_f_wohnungen_1'] }}</a></td>
            <td class="text-right border"></td>
            <td class="text-right border"></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="pot_kellerflachen_f_wohnungen_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['pot_kellerflachen_f_wohnungen_2'] }}</a>€ </td>
            <td colspan="3"></td>
            <td></td>
        </tr>

        <tr>
            <td class=" border">Pot. Wohnungen</td>
            <td class="text-right border"></td>
            <td class="text-right border"></td>
            <td class="border">Wohnen</td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="pot_wohnungen_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['pot_wohnungen_1'] }}</a></td>
            <td class="text-right border"></td>
            <td class="text-right border"></td>
            <td class="text-right border"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="pot_wohnungen_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['pot_wohnungen_2'] }}</a>€ </td>
            <td colspan="3"></td>
            <td></td>
        </tr>

        {{--41--}}
        <tr>
            <th class="border bg-green">Gewerbe Leerstand</th>
            <th class="text-center border bg-green"></th>
            <th class="text-center border bg-green"></th>
            <th class="text-center border bg-green"></th>
            <th class="text-right border bg-green"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="gewerbe_leerstand_2" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['gewerbe_leerstand_2'] }}</a>€ </th>
            <th class="text-right border bg-green"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="gewerbe_leerstand_1" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['gewerbe_leerstand_1'] }}</a></th>
            <th class="text-right border bg-green"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="gewerbe_leerstand_3" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['gewerbe_leerstand_3'] }}</a></th>
            <th class="text-right border bg-green"><a href="#" class="inline-edit" data-type="text" data-step="0.01" data-pk="gewerbe_leerstand_4" data-url="{{url('property/update/clever-fit/'.$id) }}" data-title="">{{ $properties->clever_fit_fields['gewerbe_leerstand_4'] }}</a>€ </th>
            <th colspan="3"></th>
            <td></td>
        </tr>


        </tbody>
    </table>






</div>