<style type="text/css">
  #recommended-table-1 td:nth-child(4){
    padding-top: 23px;
  }
  #recommended-table-1 td:nth-child(9){
    padding-top: 23px;
  }
  #recommended-table-1 td:nth-child(10){
    padding-top: 23px;
  }
  #recommended-table-1 td:nth-child(11){
    padding-top: 23px;
  }

  #recommended-table-2 td:nth-child(4){
    padding-top: 23px;
  }
  #recommended-table-2 td:nth-child(9){
    padding-top: 23px;
  }
  #recommended-table-2 td:nth-child(10){
    padding-top: 23px;
  }
  #recommended-table-2 td:nth-child(11){
    padding-top: 23px;
  }

  /*#recommended-table-1 td:nth-child(1) select{
    width: min-content;
  }
  #recommended-table-1 td:nth-child(1) input{
    width: 100%;
  }
  #recommended-table-2 td:nth-child(1) select{
    width: min-content;
  }
  #recommended-table-2 td:nth-child(1) input{
    width: 100%;
  }*/
</style>
<div class="row">
    
  @if (Session::has('message'))
      <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
          <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
      </div>
  @endif @if (Session::has('error'))
      <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
          <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
      </div>
  @endif


  <ul class="nav nav-tabs">
      <!--<li><a data-toggle="tab" href="#rtab-tab1">Vermietung</a></li>-->
      <li class="active"><a data-toggle="tab" href="#rtab-tab2">Vermietung</a></li>
  </ul>

  <div class="tab-content">

      @if(false)
        <div class="tab-pane fade" id="rtab-tab1">
            <div class="clearfix"></div>
            <?php
              $index1 = $index2 = 1;
              $ik=0;
              $row_array = array();
            ?>
            <h3>Verlängerung</h3>
            <a href="{{route('add-new-vacant')}}?property_id={{$properties->id}}" class="btn btn-success">Neue</a>

            @foreach($tenant_name_id2['name'] as $t_id=>$list1)

              <?php
                $ik = $index1;
                if(isset($release_arr[$t_id]['btn2'])){
                  ob_start();
                  $ik = $index2;
                }

                foreach($rented_list as $kid=>$rrow):
                  if(@$EmpfehlungDetail_arr[$t_id]['tenant_name']==$kid): 
                    $list1 = $rrow['name'];
                  endif;
                endforeach;

                if(!isset($EmpfehlungDetail_arr[$t_id]['mv_vl']))
                  $EmpfehlungDetail_arr[$t_id]['mv_vl'] = "vl";
              ?>

              <div class="clearfix"></div>

              <div class="col-md-6 col-lg-6 col-sm-12">
                <h3 class="title-tag" style="float: left;">{{$ik}}. {{$list1}}
                  <a href="javascript:void(0)" data-id="{{$t_id}}" data-class="r-sheet-{{$t_id}}" class="btn btn-info show-link-recommended  pull-right"><i class="fa fa-angle-down"></i></a>
                </h3>

                <a href="javascript:void(0)" style="margin-top: 8px;" data-id="{{$t_id}}"  class="btn btn-danger pull-right delete-vacant"><i class="fa fa-times"></i></a>
                <h3 class="release-status-{{ $t_id }}" style="margin-left: 11px;float: left; font-size: 18px;">
                  @if(isset($release_arr[$t_id]['btn2']))
                    <button type="button" class="btn btn-success">Freigegeben</button>
                    {{"Falk: ".show_date_format($release_arr[$t_id]['btn2'])}}
                  @elseif(isset($release_arr[$t_id]['btn1']))
                    <button type="button" class="btn btn-success">Freigegeben</button>
                    {{"Janine: ".show_date_format($release_arr[$t_id]['btn1'])}}
                  @endif
                </h3>
              </div>

              <div class="clearfix"></div>
            
              <div class="r-sheet-{{$t_id}} hidden">
                <form  method="post" class="form-horizontal">
                  {{ csrf_field() }}
                  <input type="hidden" name="property_id" value="{{ $id }}">
                  <input type="hidden" name="tenant_id" class="tenant_id" value="{{ $t_id }}">

                  <div class="recommended-section">

                    <div class="col-md-6 col-lg-6 col-sm-12">

                      <select style="margin-left: 8px;width: 77%;" class="form-control change-comment-recommended" data-column="tenant_name" data-id="{{$t_id}}">
                        <option value="">Select</option>
                        @foreach($rented_list as $kid=>$rrow)
                        <option value="{{$kid}}" @if(@$EmpfehlungDetail_arr[$t_id]['tenant_name']==$kid) selected @endif>{{$rrow['name'] }} {{ ($rrow['date']) ? '('.$rrow['date'].')' : '' }} </option>
                        @endforeach
                      </select>

                      <br>
              
                      <select style="margin-left: 8px;width: 77%;" class="form-control change-comment-recommended" data-column="mv_vl" data-id="{{$t_id}}">
                        <option value="">Select</option>
                        <option value="mv" @if(@$EmpfehlungDetail_arr[$t_id]['mv_vl']=="mv") selected @endif>Mietvertrag</option>
                        <option value="vl" @if(@$EmpfehlungDetail_arr[$t_id]['mv_vl']=="vl") selected @endif>Verlängerung</option>
                      </select>

                      <br>
                    </div>

                    <div class="clearfix"></div>

                    <div class="clearfix"></div>

                    <div class="col-md-1">
                      <strong>Anhänge</strong>
                    </div>

                    <div class="col-md-3 col-lg-3 col-sm-12">
                      <div class="uploaded-files uploaded-files-empfehlung2 rowt-{{$t_id}}" style="float:left">
                            <a href="javascript:void(0);" class="link-button-empfehlung2"  id="empfehlung_file_link_{{$t_id}}" data-empfehlung_id="{{$t_id}}" data-property_id="{{$id}}" data-tenant_id="{{$t_id}}" >
                              <i class="fa fa-link"></i>
                            </a>
                            @if( isset($tenant_name_id['files'][$t_id]) )
                              @foreach($tenant_name_id['files'][$t_id] as $file)
                                @if($file['file_type'] == 'file')
                                  <a href="{{ $file['file_href'] }}"  target="_blank" title="{{ $file['file_name'] }}">
                                  <i extension="filemanager.file_icon_array.{{$file['extension']}}" class="fa {{ config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file' }}" ></i>
                                  </a>
                                @else
                                  <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  id="empfehlung-gdrive-link-{{ $file['id'] }}"  ><i class="fa fa-folder" ></i></a>
                                @endif
                              @endforeach
                            @endif 
                      </div>
                      <a class="listFilesForDelete" data-property_id="{{$id}}" data-id="{{$t_id}}" data-type="empfehlung2"><i class="fa fa-trash iconstyle"></i></a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-lg-6 col-sm-12">
                      <h4>Kosten Umbau</h4>
              

                      <table class="table">
                        <?php $total = 0; ?>
                        <tbody class="recommended-list">

                          @if(isset($ractivity[$t_id]))

                            @foreach($ractivity[$t_id] as $k=>$list)
                              <?php $total +=$list->amount; ?>
                              <tr>
                                <td>
                                  <input type="text" name="name[]" class="form-control" value="{{$list->name}}" placeholder="z.B. Küche">
                                    <input type="hidden" name="empfehlung_id[]" value="{{$list->id}}" >
                                </td>
                                <td>
                                  <input type="text" name="amount[]" class="form-control mask-number-input-nodecimal" value="{{number_format($list->amount,2,',','.')}}">
                                </td>
                                <td>
                                  <div class="uploaded-files uploaded-files-empfehlung row-{{$list->id}}" style="float:left">

                                    <a href="javascript:void(0);" class="link-button-empfehlung"  id="empfehlung_file_link_{{$list->id}}" data-empfehlung_id="{{$list->id}}" data-property_id="{{$list->property_id}}" data-tenant_id="{{$t_id}}" >
                                      <i class="fa fa-link"></i>
                                    </a>

                                    @if( isset($list->files) )
                                      @foreach($list->files as $file)
                                        @if($file['file_type'] == 'file')
                                          <a href="{{ $file['file_href'] }}"  target="_blank" title="{{ $file['file_name'] }}">
                                            <i extension="filemanager.file_icon_array.{{$file['extension']}}" class="fa {{ config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file' }}" ></i>
                                          </a>
                                        @else
                                          <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  id="empfehlung-gdrive-link-{{ $file['id'] }}"  ><i class="fa fa-folder" ></i></a>
                                        @endif
                                      @endforeach
                                    @endif 
                                  </div>

                                  <a class="listFilesForDelete" data-property_id="{{$list->property_id}}" data-id="{{$list->id}}" data-type="empfehlung">
                                    <i class="fa fa-trash iconstyle"></i>
                                  </a>

                                </td>
                                <td>
                                  @if($k==0)
                                    <button type="button" class="btn-success btn-sm add-row-rec">+</button>
                                  @else
                                    <button type="button" class="btn-danger btn-sm remove-mieter">-</button>
                                  @endif
                                </td>
                              </tr>
                            @endforeach

                          @else

                          <tr>
                            <td>
                              <input type="text" name="name[]" class="form-control" placeholder="z.B. Küche">
                              <input type="hidden" name="empfehlung_id[]" value="0" >
                            </td>
                            <td>
                              <input type="text" name="amount[]" class="form-control mask-number-input-nodecimal">
                            </td>
                            <td>
                              <button type="button" class="btn-success btn-sm add-row-rec">+</button>
                            </td>
                          </tr>

                          @endif

                          <tr class="gesamt_row">
                            <th>Gesamt</th>
                            <th class="pl-20">{{number_format($total,2,',','.')}}</th>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>

                    </div>

                    <div class="col-md-6 col-lg-6 col-sm-12">
                      <!-- <label>Kommentar</label> -->
                      <textarea class="form-control change-comment-recommended  hidden" rows="15" data-id="{{$t_id}}" data-column="comment">{{@$EmpfehlungDetail_arr[$t_id]['comment']}}
                        {{@$EmpfehlungDetail_arr[$t_id]['comment2']}}
                        {{@$EmpfehlungDetail_arr[$t_id]['comment3']}}
                      </textarea>
                    </div>

                    <?php

                      $a1 = 0;
                      if(isset($EmpfehlungDetail_arr[$t_id]['amount1']) && $EmpfehlungDetail_arr[$t_id]['amount1'])
                        $a1 = $EmpfehlungDetail_arr[$t_id]['amount1'];

                      $a2 = 0;
                      if(isset($EmpfehlungDetail_arr[$t_id]['amount2']) && $EmpfehlungDetail_arr[$t_id]['amount2'])
                        $a2 = $EmpfehlungDetail_arr[$t_id]['amount2'];

                      $a3 = $a1*12;
                      $a4 = $a3*$a2;

                      $a5 = 0;
                      if(isset($EmpfehlungDetail_arr[$t_id]['amount5']) && $EmpfehlungDetail_arr[$t_id]['amount5'])
                        $a5 = $EmpfehlungDetail_arr[$t_id]['amount5'];

                      $a6 = 0;
                      if(isset($EmpfehlungDetail_arr[$t_id]['amount6']) && $EmpfehlungDetail_arr[$t_id]['amount6'])
                        $a6 = $EmpfehlungDetail_arr[$t_id]['amount6'];
                    ?>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-lg-6 col-sm-12">

                      <h4>Einnahmen</h4>

                      <table class="table">
                        <tbody>
                          <tr>
                            <td>
                              <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="title1" value="{{@$EmpfehlungDetail_arr[$t_id]['title1']}}">
                            </td>
                            <td>
                              <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount1" value="{{number_format($a1,2,',','.')}}"> pro Monat
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Laufzeit in Jahren</td>
                            <td>
                              <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount2" value="{{number_format($a2,2,',','.')}}">
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Mieteinnahmen p.a. </td>
                            <td class="pl-20">
                              {{number_format($a3,2,',','.')}}
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <th>
                              Einnahmen Gesamt 
                            </th>
                            <th class="pl-20">
                              {{number_format($a4,2,',','.')}}
                            </th>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>

                      <h4>Netto Einnahmen</h4>

                      <table class="table">
                        <tbody>
                          <tr>
                            <td>&nbsp;</td>
                            <th class="pl-20">
                              {{number_format($a4-$total-$a5-$a6,2,',','.')}}
                            </th>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>

                    </div>

                    <br/>

                    <div class="col-md-6 col-lg-6 col-sm-12 recommendation_comments_section">
                      <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary btn-add-recommended-comment" data-tenant-id="{{ $t_id }}">Neuer Kommentar</button>
                        </div>

                        <div class="col-md-12">
                          <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;"  >
                            <table class="forecast-table logtable recommendation_comments_table" id="recommendation_comments_table_{{ $t_id }}" data-id="{{ $t_id }}">
                              <thead>
                                <tr>
                                  <th class="bg-light-gray">Name</th>
                                  <th class="bg-light-gray">Kommentar</th>
                                  <th class="bg-light-gray">Datum</th>
                                  <th class="bg-light-gray">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                              <tfoot>
                                <tr>
                                  <td colspan="4">
                                    <button type="button" class="btn-sm btn recommendation_comments_limit">Show More</button>
                                  </td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-lg-6 col-sm-12" style="margin-top: 15px;">

                    <h4>Tenant Incentive</h4>

                    <table class="table">
                      <tbody>
                        <tr>
                          <td>
                            <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="title2" value="{{@$EmpfehlungDetail_arr[$t_id]['title2']}}">
                          </td>
                          <td>
                            <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount5" value="{{number_format($a5,2,',','.')}}">
                          </td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>
                            <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="title3" value="{{@$EmpfehlungDetail_arr[$t_id]['title3']}}">
                          </td>
                          <td>
                            <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount6" value="{{number_format($a6,2,',','.')}}">
                          </td>
                          <td>&nbsp;</td>
                        </tr>
                      </tbody>
                    </table>

                    </div>
              
                  </div>

                  <div class="clearfix"></div>
              
                  @if(!isset($release_arr[$t_id]['btn2']))
                    <div class="col-md-2 col-lg-2 col-sm-12">
                      <button type="button" class="save-table-list btn btn-primary">Speichern</button>
                    </div>
                  @endif

                  <div class="clearfix"></div>

                  <div style="margin-top: 15px;" class="col-md-10 col-lg-10 col-sm-12">
                    <div class="tenant-release-buttons-{{$t_id}}"></div>
                  </div>
              
                  <div class="clearfix"></div>

                  <div class="col-md-10 col-lg-10 col-sm-12">
                    <div class="tenant-release-logs-{{$t_id}}"></div>
                  </div>
                </form>
              </div>
            
              <?php
            
                if(isset($release_arr[$t_id]['btn2'])){
                  $row_array[] .= ob_get_clean();
                  $index2++;
                }else{
                  $index1++;
                }

              ?>

            @endforeach

            <div class="clearfix"></div>
            
            <?php
              $ik = 0;
              $index1 = 1;
            ?>

            <h3>Neuer Mietvertrag</h3>
            
            @foreach($tenant_name_id['name'] as $t_id=>$list1)

              <?php
                $ik = $index1;
                if(isset($release_arr[$t_id]['btn2'])){
                  ob_start();
                  $ik = $index2;
                }

                if(!isset($EmpfehlungDetail_arr[$t_id]['mv_vl']))
                  $EmpfehlungDetail_arr[$t_id]['mv_vl'] = "mv";

              ?>

              <div class="clearfix"></div>

              <div class="col-md-6 col-lg-6 col-sm-12">

                <h3 class="title-tag" style="float: left;">{{$ik}}. {{$list1}}
                  <a href="javascript:void(0)" data-id="{{$t_id}}" data-class="r-sheet-{{$t_id}}" class="btn btn-info show-link-recommended  pull-right"><i class="fa fa-angle-down"></i></a>
                </h3>

                <h3 class="release-status-{{ $t_id }}" style="margin-left: 11px;float: left;font-size: 18px;">
                  @if(isset($release_arr[$t_id]['btn2']))
                  <button type="button" class="btn btn-success">Freigegeben</button>
                  {{"Falk: ".show_date_format($release_arr[$t_id]['btn2'])}}@elseif(isset($release_arr[$t_id]['btn1']))
                  <button type="button" class="btn btn-success">Freigegeben</button>
                  {{"Janine: ".show_date_format($release_arr[$t_id]['btn1'])}}@endif
                </h3>

              </div>

              <div class="clearfix"></div>
              
              <div class="r-sheet-{{$t_id}} hidden">

                <form  method="post" class="form-horizontal">
                  {{ csrf_field() }}
                  <input type="hidden" name="property_id" value="{{ $id }}">
                  <input type="hidden" name="tenant_id" class="tenant_id" value="{{ $t_id }}">

                
                  <div class="recommended-section">

                    <div class="col-md-6 col-lg-6 col-sm-12">

                      <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="tenant_name" placeholder="Mieter" value="{{@$EmpfehlungDetail_arr[$t_id]['tenant_name']}}" style="margin-left: 8px;width: 77%;" >

                      <br>
                
                      <select style="margin-left: 8px;width: 77%;" class="form-control change-comment-recommended" data-column="mv_vl" data-id="{{$t_id}}">
                        <option value="">Select</option>
                        <option value="mv" @if(@$EmpfehlungDetail_arr[$t_id]['mv_vl']=="mv") selected @endif>Mietvertrag</option>
                        <option value="vl" @if(@$EmpfehlungDetail_arr[$t_id]['mv_vl']=="vl") selected @endif>Verlängerung</option>
                      </select>

                      <br>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-1">
                      <strong>Anhänge</strong>
                    </div>

                    <div class="col-md-3 col-lg-3 col-sm-12">

                      <div class="uploaded-files uploaded-files-empfehlung2 rowt-{{$t_id}}" style="float:left">

                        <a href="javascript:void(0);" class="link-button-empfehlung2"  id="empfehlung_file_link_{{$t_id}}" data-empfehlung_id="{{$t_id}}" data-property_id="{{$id}}" data-tenant_id="{{$t_id}}" >
                          <i class="fa fa-link"></i>
                        </a>

                        @if( isset($tenant_name_id['files'][$t_id]) )
                          @foreach($tenant_name_id['files'][$t_id] as $file)
                            @if($file['file_type'] == 'file')
                              <a href="{{ $file['file_href'] }}"  target="_blank" title="{{ $file['file_name'] }}">
                              <i extension="filemanager.file_icon_array.{{$file['extension']}}" class="fa {{ config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file' }}" ></i>
                              </a>
                            @else
                              <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  id="empfehlung-gdrive-link-{{ $file['id'] }}"  ><i class="fa fa-folder" ></i></a>
                            @endif
                          @endforeach
                        @endif 
                      </div>
                      <a class="listFilesForDelete" data-property_id="{{$id}}" data-id="{{$t_id}}" data-type="empfehlung2"><i class="fa fa-trash iconstyle"></i></a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-lg-6 col-sm-12">

                      <h4>Kosten Umbau</h4>

                      <table class="table">
                        <?php $total = 0; ?>
                        <tbody class="recommended-list">
                          @if(isset($ractivity[$t_id]))
                            @foreach($ractivity[$t_id] as $k=>$list)
                              <?php $total +=$list->amount; ?>
                              <tr>
                                <td>
                                  <input type="text" name="name[]" class="form-control" value="{{$list->name}}" placeholder="z.B. Küche">
                                  <input type="hidden" name="empfehlung_id[]" value="{{$list->id}}" >
                                </td>
                                <td>
                                  <input type="text" name="amount[]" class="form-control mask-number-input-nodecimal" value="{{number_format($list->amount,2,',','.')}}">
                                </td>
                                <td>
                                  <div class="uploaded-files uploaded-files-empfehlung row-{{$list->id}}" style="float:left">

                                    <a href="javascript:void(0);" class="link-button-empfehlung"  id="empfehlung_file_link_{{$list->id}}" data-empfehlung_id="{{$list->id}}" data-property_id="{{$list->property_id}}" data-tenant_id="{{$t_id}}" >
                                      <i class="fa fa-link"></i>
                                    </a>

                                    @if( isset($list->files2) )
                                      @foreach($list->files2 as $file)
                                        @if($file['file_type'] == 'file')
                                          <a href="{{ $file['file_href'] }}"  target="_blank" title="{{ $file['file_name'] }}">
                                          <i extension="filemanager.file_icon_array.{{$file['extension']}}" class="fa {{ config('filemanager.file_icon_array.' . $file['extension']) ?: 'fa-file' }}" ></i>
                                          </a>
                                        @else
                                          <a href="javascript:void(0);" title="{{ $file['file_name'] }}" onClick="loadDirectoryFiles('{{ $file['file_path'] }}');"  id="empfehlung-gdrive-link-{{ $file['id'] }}"  ><i class="fa fa-folder" ></i></a>
                                        @endif
                                      @endforeach
                                    @endif

                                  </div>

                                  <a class="listFilesForDelete" data-property_id="{{$list->property_id}}" data-id="{{$list->id}}" data-type="empfehlung"><i class="fa fa-trash iconstyle"></i></a>
                                </td>
                                <td>
                                  @if($k==0)
                                    <button type="button" class="btn-success btn-sm add-row-rec">+</button>
                                  @else
                                    <button type="button" class="btn-danger btn-sm remove-mieter">-</button>
                                  @endif
                                </td>
                              </tr>
                            @endforeach
                          @else
                            <tr>
                              <td>
                                <input type="text" name="name[]" class="form-control" placeholder="z.B. Küche">
                                <input type="hidden" name="empfehlung_id[]" value="0" >
                              </td>
                              <td>
                                <input type="text" name="amount[]" class="form-control mask-number-input-nodecimal">
                              </td>
                              <td>
                                <button type="button" class="btn-success btn-sm add-row-rec">+</button>
                              </td>
                            </tr>
                          @endif
                          <tr class="gesamt_row">
                            <th>Gesamt</th>
                            <th class="pl-20">{{number_format($total,2,',','.')}}</th>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>

                    </div>

                    <div class="col-md-6 col-lg-6 col-sm-12">
                      <!-- <label>Kommentar</label> -->
                      <textarea class="form-control change-comment-recommended hidden" rows="15" data-id="{{$t_id}}" data-column="comment">{{@$EmpfehlungDetail_arr[$t_id]['comment']}}
                        {{@$EmpfehlungDetail_arr[$t_id]['comment2']}}
                        {{@$EmpfehlungDetail_arr[$t_id]['comment3']}}
                      </textarea>
                    </div>

                    <?php
                      $a1 = 0;
                      if(isset($EmpfehlungDetail_arr[$t_id]['amount1']) && $EmpfehlungDetail_arr[$t_id]['amount1'])
                      $a1 = $EmpfehlungDetail_arr[$t_id]['amount1'];

                      $a2 = 0;
                      if(isset($EmpfehlungDetail_arr[$t_id]['amount2']) && $EmpfehlungDetail_arr[$t_id]['amount2'])
                      $a2 = $EmpfehlungDetail_arr[$t_id]['amount2'];

                      $a3 = $a1*12;
                      $a4 = $a3*$a2;

                      $a5 = 0;
                      if(isset($EmpfehlungDetail_arr[$t_id]['amount5']) && $EmpfehlungDetail_arr[$t_id]['amount5'])
                      $a5 = $EmpfehlungDetail_arr[$t_id]['amount5'];

                      $a6 = 0;
                      if(isset($EmpfehlungDetail_arr[$t_id]['amount6']) && $EmpfehlungDetail_arr[$t_id]['amount6'])
                      $a6 = $EmpfehlungDetail_arr[$t_id]['amount6'];
                    ?>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-lg-6 col-sm-12">

                      <h4>Einnahmen</h4>

                      <table class="table">
                        <tbody>
                          <tr>
                            <td>
                              <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="title1" value="{{@$EmpfehlungDetail_arr[$t_id]['title1']}}">
                            </td>
                            <td>
                              <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount1" value="{{number_format($a1,2,',','.')}}"> pro Monat
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>Laufzeit in Jahren</td>
                            <td>
                              <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount2" value="{{number_format($a2,2,',','.')}}">
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>
                              Mieteinnahmen p.a. 
                            </td>
                            <td class="pl-20">
                              {{number_format($a3,2,',','.')}}
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <th>
                              Einnahmen Gesamt 
                            </th>
                            <th class="pl-20">
                              {{number_format($a4,2,',','.')}}
                            </th>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>

                      <h4>Netto Einnahmen</h4>

                      <table class="table">
                        <tbody>
                          <tr>
                            <td>&nbsp;</td>
                            <th class="pl-20">
                              {{number_format($a4-$total-$a5-$a6,2,',','.')}}
                            </th>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>

                    </div>

                    <br/>

                    <div class="col-md-6 col-lg-6 col-sm-12 recommendation_comments_section">
                      <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary btn-add-recommended-comment" data-tenant-id="{{ $t_id }}">Neuer Kommentar</button>
                        </div>

                        <div class="col-md-12">
                          <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;"  >
                            <table class="forecast-table logtable recommendation_comments_table" id="recommendation_comments_table_{{ $t_id }}" data-id="{{ $t_id }}">
                              <thead>
                                <tr>
                                  <th class="bg-light-gray">Name</th>
                                  <th class="bg-light-gray">Kommentar</th>
                                  <th class="bg-light-gray">Datum</th>
                                  <th class="bg-light-gray">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                              <tfoot>
                                <tr>
                                  <td colspan="4">
                                    <button type="button" class="btn-sm btn recommendation_comments_limit">Show More</button>
                                  </td>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-6 col-lg-6 col-sm-12" style="margin-top: 15px;">

                      <h4>Tenant Incentive</h4>

                      <table class="table">
                        <tbody>
                          <tr>
                            <td>
                              <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="title2" value="{{@$EmpfehlungDetail_arr[$t_id]['title2']}}">
                            </td>
                            <td>
                              <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount5" value="{{number_format($a5,2,',','.')}}">
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>
                              <input type="text" class="form-control change-comment-recommended" data-id="{{$t_id}}" data-column="title3" value="{{@$EmpfehlungDetail_arr[$t_id]['title3']}}">
                            </td>
                            <td>
                              <input type="text" class="form-control mask-number-input-nodecimal change-comment-recommended" data-id="{{$t_id}}" data-column="amount6" value="{{number_format($a6,2,',','.')}}">
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                
                  </div>

                  <div class="clearfix"></div>
                
                  @if(!isset($release_arr[$t_id]['btn2']))
                    <div class="col-md-2 col-lg-2 col-sm-12">
                        <button type="button" class="save-table-list btn btn-primary">Speichern</button>
                    </div>
                  @endif

                  <div class="clearfix"></div>

                  <div style="margin-top: 15px;" class="col-md-10 col-lg-10 col-sm-12">
                    <div class="tenant-release-buttons-{{$t_id}}"></div>
                  </div>
                  
                  <div class="clearfix"></div>

                  <div class="col-md-10 col-lg-10 col-sm-12">
                    <div class="tenant-release-logs-{{$t_id}}"></div>
                  </div>
                </form>

              </div>
            
              <?php
              
                if(isset($release_arr[$t_id]['btn2'])){
                  $row_array[] .= ob_get_clean();
                  $index2++;
                }
                else{
                  $index1++;
                }

              ?>

            @endforeach

            <div  class="clearfix"></div>

            <h2 style="margin-top: 30px;">Von Falk freigegeben</h2>
            <div class="non-editable">
              @foreach($row_array as $l)
                {!! $l !!}
              @endforeach
            </div>
        </div>
      @endif

      <div class="tab-pane fade active in" id="rtab-tab2">

        <div class="row">
          <div class="col-md-12">
            <div class="modal-dialog modal-lg" style="width: 100%">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Neuer MV / neue VL</h4>
                </div>
                <div class="modal-body" id="recommended-table-div-1">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="modal-dialog modal-lg" style="width: 100%">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Von Falk freigegeben</h4>
                </div>
                <div class="modal-body" id="recommended-table-div-2">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="modal-dialog modal-lg" style="width: 100%">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Pending</h4>
                </div>
                <div class="modal-body" id="recommended-table-div-3">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="modal-dialog modal-lg" style="width: 100%">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Nicht freigegebene</h4>
                </div>
                <div class="modal-body" id="recommended-table-div-4">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="modal-dialog modal-lg" style="width: 100%">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Verlauf</h4>
                </div>
                <div class="modal-body" id="recommended-table-div-5">
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

  </div>

</div>

<div class=" hidden">
    <table>
        <tbody class="add-row-clone">
          <tr>
            <td>
            <input type="text" name="name[]" class="form-control" placeholder="z.B. Küche">
            <input type="hidden" name="empfehlung_id[]" value="0" >
            </td>
            <td>
            <input type="text" name="amount[]" class="form-control mask-number-input-nodecimal">
            </td>
            <td>
                <button type="button" class="btn-danger btn-sm remove-mieter">-</button>
            </td>
          </tr>
      </tbody>
    </table>
</div>
