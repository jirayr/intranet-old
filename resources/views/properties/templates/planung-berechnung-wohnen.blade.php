<div class="white-box table-responsive" id="planung-berechnung-wohnen">
    <table class="table">
        <tbody>
        <tr>
            <td colspan="8">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="planung_total_area" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['planung_total_area'],0,",",".") }}</a> m²</td>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">*Nutzfläche (Flur) = <a href="#" class="inline-edit" data-type="text" data-pk="nutzflache_flur" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['nutzflache_flur'],0,",",".") }}</a> m²</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="8">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <th class="text-right">2-Zimmerwohnung</th>
            <th class="text-right">3-Zimmerwohnung</th>
            <th class="text-right">4-Zimmerwohnung</th>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_1" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_1'],0,",",".") }}</a> m²</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_2" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_2'],0,",",".") }}</a> m²</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_3" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_3'],0,",",".") }}</a> m²</td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td>Anzahl</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_4" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_4'],0,",",".") }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_5" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_5'],0,",",".") }}</a></td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_6" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_6'],0,",",".") }}</a></td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_6" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_6'],0,",",".") }}</a> m²</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_7" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_7'],0,",",".") }}</a> m²</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_8" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_8'],0,",",".") }}</a> m²</td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_9" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_9'],0,",",".") }}</a> m²</td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_10" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_10'],0,",",".") }}</a> m²</td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr>
            <td>&nbsp;</td>
            <th class="text-right">2-Zimmerwohnung</th>
            <th class="text-right">3-Zimmerwohnung</th>
            <th class="text-right">4-Zimmerwohnung</th>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_11" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_11'],2,",",".") }}</a> €/m²</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_12" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_12'],2,",",".") }}</a> €/m²</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="zimmerwohnung_13" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['zimmerwohnung_13'],2,",",".") }}</a> €/m²</td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td>Insgesamt p.m.</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="insgesamt_pm_1" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['insgesamt_pm_1'],2,",",".") }}</a> € </td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="insgesamt_pm_2" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['insgesamt_pm_2'],2,",",".") }}</a> € </td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="insgesamt_pm_3" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['insgesamt_pm_3'],2,",",".") }}</a> € </td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td>Pro Wohnung</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="pro_wohnung_1" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['pro_wohnung_1'],2,",",".") }}</a> € </td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="pro_wohnung_2" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['pro_wohnung_2'],2,",",".") }}</a> € </td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="pro_wohnung_3" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['pro_wohnung_3'],2,",",".") }}</a> € </td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr>
            <td>Insgesamt p.m.</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="insgesamt_pm_4" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['insgesamt_pm_4'],2,",",".") }}</a> €</td>
        </tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr>
            <td>Insgesamt p.a.</td>
            <td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="insgesamt_pm_5" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['insgesamt_pm_5'],2,",",".") }}</a> €</td>
        </tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr><td colspan="8">&nbsp;</td></tr>
        <tr>
            <td>&nbsp;</td>
            <td>Verkauf zu Faktor</td>
            <td>&nbsp;</td>
            <td class="text-center blue"><a href="#" class="inline-edit" data-type="text" data-pk="verkauf_zu_faktor_1" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['verkauf_zu_faktor_1'],2,",",".") }}</a></td>
            <td class="text-center green"><a href="#" class="inline-edit" data-type="text" data-pk="verkauf_zu_faktor_2" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['verkauf_zu_faktor_2'],2,",",".") }}</a></td>
            <td class="text-center dark"><a href="#" class="inline-edit" data-type="text" data-pk="verkauf_zu_faktor_3" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['verkauf_zu_faktor_3'],2,",",".") }}</a></td>
            <td class="text-center yellow"><a href="#" class="inline-edit" data-type="text" data-pk="verkauf_zu_faktor_4" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['verkauf_zu_faktor_4'],2,",",".") }}</a></td>
            <td class="text-center green"><a href="#" class="inline-edit" data-type="text" data-pk="verkauf_zu_faktor_5" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['verkauf_zu_faktor_5'],2,",",".") }}</a></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
            <td class="text-center blue">&nbsp;</td>
            <td class="text-center green">&nbsp;</td>
            <td class="text-center dark">&nbsp;</td>
            <td class="text-center yellow">&nbsp;</td>
            <td class="text-center green">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>möglicher Verkaufspreis:</td>
            <td>&nbsp;</td>
            <td class="text-center blue"><a href="#" class="inline-edit" data-type="text" data-pk="moglicher_verkaufspreis_1" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['moglicher_verkaufspreis_1'],2,",",".") }}</a> € </td>
            <td class="text-center green"><a href="#" class="inline-edit" data-type="text" data-pk="moglicher_verkaufspreis_2" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['moglicher_verkaufspreis_2'],2,",",".") }}</a> € </td>
            <td class="text-center dark"><a href="#" class="inline-edit" data-type="text" data-pk="moglicher_verkaufspreis_3" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['moglicher_verkaufspreis_3'],2,",",".") }}</a> € </td>
            <td class="text-center yellow"><a href="#" class="inline-edit" data-type="text" data-pk="moglicher_verkaufspreis_4" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['moglicher_verkaufspreis_4'],2,",",".") }}</a> € </td>
            <td class="text-center green"><a href="#" class="inline-edit" data-type="text" data-pk="moglicher_verkaufspreis_5" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['moglicher_verkaufspreis_5'],2,",",".") }}</a> € </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>./.Einkaufspreis (Buchwert)</td>
            <td>&nbsp;</td>
            <td class="text-center blue"><a href="#" class="inline-edit" data-type="text" data-pk="einkaufspreis_buchwert_1" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['einkaufspreis_buchwert_1'],2,",",".") }}</a></td>
            <td class="text-center green"><a href="#" class="inline-edit" data-type="text" data-pk="einkaufspreis_buchwert_2" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['einkaufspreis_buchwert_2'],2,",",".") }}</a></td>
            <td class="text-center dark"><a href="#" class="inline-edit" data-type="text" data-pk="einkaufspreis_buchwert_3" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['einkaufspreis_buchwert_3'],2,",",".") }}</a></td>
            <td class="text-center yellow"><a href="#" class="inline-edit" data-type="text" data-pk="einkaufspreis_buchwert_4" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['einkaufspreis_buchwert_4'],2,",",".") }}</a></td>
            <td class="text-center green"><a href="#" class="inline-edit" data-type="text" data-pk="einkaufspreis_buchwert_5" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['einkaufspreis_buchwert_5'],2,",",".") }}</a></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>./.Invest (inkl. EK-Rückzahlung)</td>
            <td>&nbsp;</td>
            <td class="text-center blue"><a href="#" class="inline-edit" data-type="text" data-pk="invest_inkl_ek_ruckzahlung_1" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['invest_inkl_ek_ruckzahlung_1'],2,",",".") }}</a></td>
            <td class="text-center green"><a href="#" class="inline-edit" data-type="text" data-pk="invest_inkl_ek_ruckzahlung_2" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['invest_inkl_ek_ruckzahlung_2'],2,",",".") }}</a></td>
            <td class="text-center dark"><a href="#" class="inline-edit" data-type="text" data-pk="invest_inkl_ek_ruckzahlung_3" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['invest_inkl_ek_ruckzahlung_3'],2,",",".") }}</a></td>
            <td class="text-center yellow"><a href="#" class="inline-edit" data-type="text" data-pk="invest_inkl_ek_ruckzahlung_4" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['invest_inkl_ek_ruckzahlung_4'],2,",",".") }}</a></td>
            <td class="text-center green"><a href="#" class="inline-edit" data-type="text" data-pk="invest_inkl_ek_ruckzahlung_5" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['invest_inkl_ek_ruckzahlung_5'],2,",",".") }}</a></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <th class="border-top">Delta</th>
            <td class="border-top">&nbsp;</td>
            <th class="text-center blue border-top"><a href="#" class="inline-edit" data-type="text" data-pk="delta_1" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['delta_1'],2,",",".") }}</a></th>
            <th class="text-center green border-top"><a href="#" class="inline-edit" data-type="text" data-pk="delta_2" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['delta_2'],2,",",".") }}</a></th>
            <th class="text-center dark border-top"><a href="#" class="inline-edit" data-type="text" data-pk="delta_3" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['delta_3'],2,",",".") }}</a></th>
            <th class="text-center yellow border-top"><a href="#" class="inline-edit" data-type="text" data-pk="delta_4" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['delta_4'],2,",",".") }}</a></th>
            <th class="text-center green border-top"><a href="#" class="inline-edit" data-type="text" data-pk="delta_5" data-url="{{url('property/update/planung-berechnung-wohnen/'.$id) }}" data-title="">{{ number_format((float)$properties->planung_fields['delta_5'],2,",",".") }}</a></th>
        </tr>
        </tbody>
    </table>
</div>