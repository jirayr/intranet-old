<div class="row">
	<div class="col-md-12">
		<span id="property_deal_msg"></span>
	</div>
</div>
<div class="row white-box">
	<div class="col-md-12">
		<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#add_property_deal_modal">
			Neue Angebote
		</button>
	</div>

	<br/><br/>

	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Offene Angebote</h3>
		<table class="table table-striped" id="property_deal_table" style="width: 100%;padding-left: 0px;">
			<thead>
				<tr>
					<th>#</th>
					<th>Datei</th>
					<th>Betrag</th>
					<th>Startdatum</th>
					<th>Kündigung frühestens</th>
					<th>Kommentar</th>
					<th>User</th>
					<th>Datum</th>
					<th>Kommentar Falk</th>
					<th></th>
					<th>Ablehnen</th>
					<th>Pending</th>
					<th>Aktion</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>

	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Pending Angebote</h3>
		<table class="table table-striped" id="pending_property_deal_table" style="width: 100%;padding-left: 0px;">
			<thead>
				<tr>
					<th>#</th>
					<th>Datei</th>
					<th>Betrag</th>
					<th>Startdatum</th>
					<th>Kündigung frühestens</th>
					<th>Kommentar Angebote</th>
					<th>User</th>
					<th>Datum</th>
					<th>Kommentar Falk</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>


	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Nicht Freigegeben Angebote</h3>
		<table class="table table-striped" id="not_release_property_deal_table" style="width: 100%;padding-left: 0px;">
			<thead>
				<tr>
					<th>#</th>
					<th>Datei</th>
					<th>Betrag</th>
					<th>Startdatum</th>
					<th>Kündigung frühestens</th>
					<th>Kommentar Angebote</th>
					<th>User</th>
					<th>Datum</th>
					<th>Kommentar Falk</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>


	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3>Verlauf</h3>
		<table class="table table-striped" id="deal_mail_table">
    		<thead>
		      	<tr>
		        	<th class="">Name</th>
		        	<th class="">Button</th>
		        	<th>Datei</th>
		        	<th class="">Kommentar</th>
		        	<th class="">Datum</th>
		        	<th>Kündigung frühestens</th>
		      	</tr>
    		</thead>
    		<tbody></tbody>
  		</table>
	</div>
</div>



<div class=" modal fade" role="dialog" id="add_property_deal_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Neue Angebote</h4>
			</div>
			<form method="post" enctype="multipart/form-data" id="add_property_deal_form">
				<div class="modal-body">

					<span id="add_property_deal_msg"></span>

					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$properties->id}}">
					<input type="hidden" name="file_basename" id="property_deal_gdrive_file_basename" value="">
					<input type="hidden" name="file_dirname" id="property_deal_gdrive_file_dirname" value="">
					<input type="hidden" name="file_type" id="property_deal_gdrive_file_type" value="">
					<input type="hidden" name="file_name" id="property_deal_gdrive_file_name" value="">

					<label>Datei</label> <span id="property_deal_gdrive_file_name_span"> </span>
					<a href="javascript:void(0);" class="link-button-gdrive-property-deal btn btn-info">Datei auswählen</a>


                    <a class="property-deal-upload-file-icon btn btn-info" href="javascript:void(0);">Hochladen</a>
                    <input type="file" name="property_deal_gdrive_file_upload" class="hide property-deal-gdrive-upload-file-control" id="property_deal_gdrive_file_upload" >
					<br><br>

					<label>Betrag</label>
					<input type="text" name="amount" class="form-control mask-input-number" required>
					<br>

					<label>Startdatum</label>
					<input type="text" name="date" class="form-control mask-input-new-date" placeholder="dd.mm.yyyy" required>
					<br>

					<label>Kündigung frühestens</label>
					<input type="text" name="termination_date" class="form-control mask-input-new-date" placeholder="dd.mm.yyyy">
					<br>
					
					<label>Kommentar</label>
					<textarea class="form-control" name="comment" required></textarea>
					<br>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Speichern</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>

<div class=" modal fade" role="dialog" id="deal_mark_as_not_release">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<form method="post" id="form_mark_as_not_release">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<span id="deal_mark_as_not_release_error"></span>
						</div>
						<div class="col-md-12">
							<label>Kommentar</label>
							<textarea class="form-control" id="input_deal_mark_as_not_release" name="comment" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Senden</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>

<div class=" modal fade" role="dialog" id="deal_mark_as_pending">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<form method="post" id="form_mark_as_pending">
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<span id="deal_mark_as_pending_error"></span>
						</div>
						<div class="col-md-12">
							<label>Kommentar</label>
							<textarea class="form-control" id="input_deal_mark_as_pending" name="comment" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Senden</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>

	</div>
</div>