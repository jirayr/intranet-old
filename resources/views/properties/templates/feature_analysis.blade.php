<style>
    div.charts {
        display: grid;
        grid-template: 1fr 1fr/ 1fr 1fr;
        gap: 20px;
        min-height: 250px;
        padding: 15px;
        margin-top: 50px;
        position: relative;
    }

    div.charts .item {
        position: relative;
        padding: 70px 15px 15px 15px;
        margin-bottom: 25px;
        background: #e9edf1;
    }

    div.charts .item .head {
        top: -20px;
        left: 0px;
        right: 0px;
        margin: auto;
        width: 95%;
        padding: 10px;
        background: linear-gradient(60deg, #26c6da, #00acc1);
        box-shadow: 0 4px 20px 0 rgba(0, 0, 0, .14), 0 7px 10px -5px rgba(0, 172, 193, .4);
        position: absolute;
        border-radius: 3px;
        color: white;
        font-size: 20px;
    }

    .hide-item {
        margin-top: 50px;
        overflow: hidden;
        display: none;
        align-items: flex-start;
        flex-wrap: nowrap;
        background: #e9edf1;
        padding: 15px;
    }

    .hide-item .form-check-inline {
        width: 20%;
    }


</style>

<div class="white-box table-responsive feature-analysis">
    @php

        $bank_array = array();
        $j = 0;
        $ist = 0;
         foreach($banks as $key => $bank)
         {
             $bank_array[] = $bank->id;
         }

         array_unique($bank_array);
         $str = implode(',', $bank_array);

       $property_sheet = DB::table('properties')->whereRaw('main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('standard_property_status','desc')->first();

       $list_fields_percent = [
             'net_rent', 'net_rent_empty', 'maintenance', 'operating_costs', 'object_management', 'tax', 'building', 'plot_of_land', 'real_estate_taxes', 'estate_agents', 'notary_land_register', 'evaluation', 'others', 'buffer', 'with_real_ek', 'from_bond', 'bank_loan', 'interest_bank_loan', 'eradication_bank', 'interest_bond', 'maintenance_nk', 'operating_costs_nk', 'object_management_nk', 'depreciation_nk', 'property_value'
         ];
         foreach ($list_fields_percent as $field) {
             $property_sheet->$field *= 100;
         }

         $property_sheet->plz_ort = $properties->plz_ort;
         $property_sheet->ort = $properties->ort;
         $property_sheet->strasse = $properties->strasse;
         $property_sheet->hausnummer = $properties->hausnummer;
         $property_sheet->niedersachsen = $properties->niedersachsen;


         $j = 1;
         $sheet_property = $property_sheet;
         $id = $property_sheet->id;
    @endphp

    <div style="overflow: hidden">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="zip">ZIP</label>
                <input style="border-color: grey" value="{{ isset($sheet_property) ? $sheet_property->plz_ort : '' }}"
                       type="text" class="form-control" id="zip" placeholder="ZIP">
            </div>
            <div class="form-group col-md-6">
                <label for="city">City</label>
                <input style="border-color: grey" type="text"
                       value="{{ isset($sheet_property) && $sheet_property->ort ? preg_replace('/^([^,]*).*$/', '$1', $sheet_property->ort): '' }}"
                       class="form-control" id="cityName" placeholder="City">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="streetName">Street Name</label>
                <input style="border-color: grey" type="text" class="form-control"
                       value="{{ isset($sheet_property) ? $sheet_property->strasse : '' }}" id="streetName"
                       placeholder="Street Name">
            </div>
        </div>

    </div>

    <div style="padding-left: 15px;">
        <button class="btn btn-primary btn-fullscreen filter">Suchen</button>
    </div>


    <div class="info" style="margin-top: 50px; display: none;">
        <div style="background: #e9edf1; padding:20px;">
            <div>
                <span style="width: 15px; height: 15px; border-radius: 50%; background: crimson; display: inline-block; border:1px solid grey"></span>
                <span style="font-weight: bold">The first score (PLZ)</span> describes your feature in relation to all
                areas that are included in the same PLZ like your searched object.
            </div>
            <div>
                <span style="width: 15px; height: 15px; border-radius: 50%; background: lightseagreen; display: inline-block; border:1px solid grey""></span>
                <span style="font-weight: bold">The second score (City)</span> describes your feature in relation to all
                areas that are included in the same City like your searched object.
            </div>
            <div>
                <span style="width: 15px; height: 15px; border-radius: 50%; background: yellow; display: inline-block; border:1px solid grey""></span>
                <span style="font-weight: bold">The third score (Deutschland)</span> describes your feature in relation
                to all areas that are included in Germany.
            </div>
        </div>
    </div>

    <div class="hide-item">
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="alle" checked="true" value="alle">
            <label class="form-check-label" for="alle">Alle</label>
        </div>

        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="centrality" checked="true" value="centrality">
            <label class="form-check-label" for="centrality">Centrality</label>
        </div>

        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="social_status" checked="true" value="social_status">
            <label class="form-check-label" for="social_status">Social status</label>
        </div>

        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="young_population" checked="true"
                   value="young_population">
            <label class="form-check-label" for="young_population">Young population</label>
        </div>

        <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="urban" checked="true" value="urban">
            <label class="form-check-label" for="urban">Urban</label>
        </div>
    </div>

    <div class="charts">

    </div>


</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script>
    $(document).ready(function () {

        let zipCodes = [];

        let cities = [];

        let streetNames = [];


        $('.filter').click(function () {

            let $this = $(this);

            $this.attr('disabled', true);

            $('.charts').empty();

            $('.info').css('display', 'none');

            $('.hide-item').css('display', 'none');

            let city = $('#cityName').val();

            let zip = $('#zip').val();

            let streetName = $('#streetName').val();

            let text = {
                'centrality': '<span style="font-weight: bold">Centrality</span> describes the nearness to the center of your choosen city/location. A score of 100 means its very near to the city center, 0 would describe a object at the edges of the city.',
                'social_status': '<span style="font-weight: bold">Social Status</span> describes the social status of the population in this area. A score of 100 reflects that the searched property is in an area with High Class Population. The score 0 would reflect the lower class.',
                'young_population': '<span style="font-weight: bold">Young Populationn</span> describes how many young people live in this area in relation to the average age.',
                'urban': '<span style="font-weight: bold">Urban</span> describes the lifestyle, multiculturality of the area, and nearness to Restaurants and co. A score of 100 reflects that its a very open world and multicultural area, 0 instead is very conservative and non-multicultural area'
            };

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: `https://ai.immowin24.de/api/get-features?city=${city}&zip=${zip}&streetname=${streetName}`,
                method: 'GET',
                success: function (result) {

                    $('.info').css('display', 'block');

                    $('.hide-item').css('display', 'flex');

                    $.each(result.data.features, function (key, value) {

                        let plz = value.plz === -1 ? 0 : value.plz;

                        let city = value.city === -1 ? 0 : value.city;

                        let country = value.country === -1 ? 0 : value.country;

                        $('.charts').append(`<div class="item item-${key}">
            <div class="head" style="text-transform: capitalize">
                ${key.replace('_', ' ')}
            </div>

            <div style="margin-bottom: 25px;">
                <div style="overflow: hidden;">
                    <p style="font-size: 20px; margin: 0px;">
                        PLZ
                    </p>
                    <span style="float: right;font-size: 12px;">${plz}</span>
                </div>
                <div class="progress" style="background-color: lightpink; width: 100%; margin:0px;">
                    <div class="progress-bar" role="progressbar" aria-valuenow="${plz}"
                         aria-valuemin="0" aria-valuemax="100" style="width:${plz}%; background-color:red">
                    </div>
                </div>
                <div style="overflow: hidden">
                    <span style="float:left; font-size: 12px">0</span>
                    <span style="float: right; font-size: 12px;">100</span>
                </div>
            </div>

            <div style="margin-bottom: 25px;">
                <div style="overflow: hidden;">
                    <p style="font-size: 20px; margin: 0px;">
                        Stadt
                    </p>
                    <span style="float: right;font-size: 12px;">${city}</span>
                </div>
                <div class="progress" style="background-color: lightgreen; width: 100%; margin:0px;">
                    <div class="progress-bar" role="progressbar" aria-valuenow="${city}"
                         aria-valuemin="0" aria-valuemax="100" style="width:${city}%; background-color:green">
                    </div>
                </div>
                <div style="overflow: hidden">
                    <span style="float:left; font-size: 12px">0</span>
                    <span style="float: right; font-size: 12px;">100</span>
                </div>
            </div>

            <div style="margin-bottom: 25px;">
                <div style="overflow: hidden;">
                    <p style="font-size: 20px; margin: 0px;">
                        Deutschland
                    </p>
                    <span style="float: right;font-size: 12px;">${country}</span>
                </div>
                <div class="progress" style="background-color: lightyellow; width: 100%; margin:0px;">
                    <div class="progress-bar" role="progressbar" aria-valuenow="${country}"
                         aria-valuemin="0" aria-valuemax="100" style="width:${country}%; background-color:yellow">
                    </div>
                </div>
                <div style="overflow: hidden">
                    <span style="float:left; font-size: 12px">0</span>
                    <span style="float: right; font-size: 12px;">100</span>
                </div>
            </div>

            <div>
                ${text[key]}
            </div>

        </div>`);

                    });

                    $this.attr('disabled', false);
                }
            });
        })

        $('.feature-analysis').on('click', '#alle', function () {
            if ($(this).is(":checked")) {
                $("#centrality").prop("checked", true);
                $("#social_status").prop("checked", true);
                $("#young_population").prop("checked", true);
                $("#urban").prop("checked", true);


                $('.item-centrality').css('display', 'block');
                $('.item-social_status').css('display', 'block');
                $('.item-young_population').css('display', 'block');
                $('.item-urban').css('display', 'block');

            } else {
                $("#centrality").prop("checked", false);
                $("#social_status").prop("checked", false);
                $("#young_population").prop("checked", false);
                $("#urban").prop("checked", false);


                $('.item-centrality').css('display', 'none');
                $('.item-social_status').css('display', 'none');
                $('.item-young_population').css('display', 'none');
                $('.item-urban').css('display', 'none');
            }
        })

        $('.feature-analysis').on('click', '#centrality', function () {
            if ($(this).is(":checked")) {
                $('.item-centrality').css('display', 'block');

            } else {
                $('.item-centrality').css('display', 'none');
            }
        });

        $('.feature-analysis').on('click', '#social_status', function () {
            if ($(this).is(":checked")) {
                $('.item-social_status').css('display', 'block');

            } else {
                $('.item-social_status').css('display', 'none');
            }
        });

        $('.feature-analysis').on('click', '#young_population', function () {
            if ($(this).is(":checked")) {
                $('.item-young_population').css('display', 'block');

            } else {
                $('.item-young_population').css('display', 'none');
            }
        });

        $('.feature-analysis').on('click', '#urban', function () {
            if ($(this).is(":checked")) {
                $('.item-urban').css('display', 'block');

            } else {
                $('.item-urban').css('display', 'none');
            }
        });

        $('#zip').keyup(function () {


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: `https://ai.immowin24.de/api/typing/zip?type=zip&q=${$(this).val()}`,
                method: 'GET',
                success: function (result) {

                    zipCodes = [];

                    if (result.data.length !== 0) {
                        $.each(result.data, function (index, item) {
                            zipCodes.push({
                                label: item.zip_city,
                                value: item.zip,
                                city: item.city
                            });
                        });
                    }

                    $("#zip").autocomplete({
                        source: zipCodes,
                        delay: 1000,

                        select: function (event, ui) {
                            $('#cityName').val(ui.item.city);
                            $('#streetName').val('');
                        }
                    });
                }
            });
        })

        $('#cityName').keyup(function () {

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: `https://ai.immowin24.de/api/typing/city?type=city&q=${$(this).val()}`,
                method: 'GET',
                success: function (result) {

                    cities = [];

                    if (result.data.length !== 0) {
                        $.each(result.data, function (index, item) {
                            cities.push({
                                label: item.city,
                            });
                        });
                    }

                    $("#cityName").autocomplete({
                        source: cities,
                        delay: 1000,
                        select: function (event, ui) {
                            $('#zip').val('');
                            $('#streetName').val('');
                        }
                    });
                }
            });
        })

        $('#streetName').keyup(function () {

            let city = $('#cityName').val();

            let zip = $('#zip').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: `https://ai.immowin24.de/api/typing/streetname?q=${$(this).val()}&city=${city}&zip=${zip}`,
                method: 'GET',
                success: function (result) {

                    streetNames = [];

                    if (result.data.length !== 0) {
                        $.each(result.data, function (index, item) {
                            streetNames.push({
                                label: item.full_street_name,
                                value: item.streetname,
                                zip: item.zip,
                                city: item.city
                            });
                        });
                    }

                    $("#streetName").autocomplete({
                        source: streetNames,
                        delay: 1000,
                        select: function (event, ui) {
                            $('#zip').val(ui.item.zip);
                            $('#cityName').val(ui.item.city);
                        }
                    });
                }
            });
        })

    });
</script>
