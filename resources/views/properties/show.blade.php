<?php
//echo "<pre>";
//print_r($properties)
$rateSwotValArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
$luser = Auth::user();
?>
@extends('layouts.admin')
@section('p_title')
{{ $properties->name_of_property }}
<?php
if ($properties->status == config('properties.status.in_purchase')):
    $status = __('property.in_purchase');
endif;

if ($properties->status == config('properties.status.offer')):
    $status = __('property.offer');
endif;

if ($properties->status == config('properties.status.exclusive')): $status = __('property.exclusive');
endif;

if ($properties->status == config('properties.status.exclusivity')): $status = __('property.exclusivity');
endif;

if ($properties->status == config('properties.status.certified')): $status = __('property.certified');
endif;

if ($properties->status == config('properties.status.duration')): $status = __('property.duration');
endif;

if ($properties->status == config('properties.status.sold')): $status = __('property.sold');
endif;

if ($properties->status == config('properties.status.declined')): $status = __('property.declined');
endif;

if ($properties->status == config('properties.status.lost')): $status = __('property.lost');
endif;

if ($properties->status == config('properties.status.hold')): $status = __('property.hold');
endif;

if ($properties->status == config('properties.status.quicksheet')): $status = __('property.quicksheet');
endif;

if ($properties->status == config('properties.status.liquiplanung')): $status = __('property.liquiplanung');
endif;

if ($properties->status == config('properties.status.externquicksheet')): $status = __('property.externquicksheet');
endif;

?>
@if(isset($properties->asset_manager))
(AM: {{$properties->asset_manager->name}}, Status: {{$status}})
@else
(Status: {{$status}})
@endif
<button type="button" id="mail-an-am" data-type="0" class="btn btn-primary btn-sm"></i> Mail an AM</button>
<button type="button" id="mail-an-hv-bu" data-type="1" class="btn btn-primary btn-sm"></i> Mail an HV BU</button>
<button type="button" id="mail-an-hv-pm" data-type="2" class="btn btn-primary btn-sm"></i> Mail an HV PM</button>
<button type="button" id="mail-an-ek" data-type="3" class="btn btn-primary btn-sm"></i> Mail an EK</button>
<button type="button" id="mail-an-sb" data-type="7" class="btn btn-primary btn-sm"></i> Mail an SB</button>
<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mail_all_modal"></i> Mails an... </button>
@endsection
@section('css')
<script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<!-- Styles -->
<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
<link rel="stylesheet"  href="{{ asset('css/tagsinput-revisited.css') }}" />
<script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
@endsection
@section('content')
<div class=" modal fade" role="dialog" id="send_email_modal">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">E-Mail senden</h4>
			</div>
			<form action="{{ route('save_status_loi') }}" method="post">
				<div class="modal-body">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$properties->id}}">
					<label>Receiver</label>
					<input id="receiver"  type="text"  name="receiver" class="form-control" required>
					<br>
					@php
					$tm_email_value = "";
					if(isset($email_template)){
					$tm_email_value = $email_template->email;
					}else{
					if($contact_person){
					$tm_email_value = $contact_person->email;
					}
					}
					@endphp
					<label>CC</label>
					<input type="text" id="cc" name="cc" class="form-control" value="{{$tm_email_value}}">
					<br>
					<label>BCC</label>
					<input type="text" id="bcc" name="bcc" class="form-control">
					<br>
					<label>Subject</label>
					<input type="text" name="subject" class="form-control" required>
					<br>
					<label>Message</label>
					<textarea class="form-control" name="message" required></textarea>
					<br>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Send</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class=" modal fade" role="dialog" id="load_all_status_loi_by_user">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div id="load_all_status_loi_by_user_content">
			</div>
		</div>
	</div>
</div>
<div class=" modal fade" role="dialog" id="mail_an_am_modal">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Mail an AM</h4>
			</div>
			<form action="{{ route('sendmail_to_am') }}" method="POST">
				<div class="modal-body">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$properties->id}}">
					{{-- <input type="hidden" name="am_id" id="am_id" value="{{$properties->asset_m_id}}"> --}}
					<input type="hidden" name="subject" value="{{ $properties->name_of_property }}">
					<input type="hidden" name="type" id="mail_type" value="0">

					<label>Nachricht</label>
					<textarea class="form-control" name="message" required></textarea>
					<br>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Senden</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class=" modal fade" role="dialog" id="mail_all_modal">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Mail</h4>
			</div>
			<form action="{{ route('sendmail_to_all') }}" method="POST">
				<div class="modal-body">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$properties->id}}">

					<label>Nachricht</label>
					<textarea class="form-control" name="message" required></textarea>
					<br>

					<label class="checkbox-inline"><input type="checkbox" name="type[]" value="0">AM</label>
					<label class="checkbox-inline"><input type="checkbox" name="type[]" value="1">HV BU</label>
					<label class="checkbox-inline"><input type="checkbox" name="type[]" value="2">HV PM</label>
					<label class="checkbox-inline"><input type="checkbox" name="type[]" value="3">EK</label>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Senden</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="{{asset('js/Sortable.js')}}"></script>
<script>
	CKEDITOR.replace( 'message' );
</script>
<?php //var_dump($this->api_url);  ?>
@if(Session::has('massage'))
<p class="alert alert-success">{{ Session::get('massage') }}</p>
@endif
@if(Session::has('error'))
<p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif
@if(Session::has('success'))
<p class="alert alert-success">{{ Session::get('success') }}</p>
@endif
<ul class="nav nav-tabs">
	<li @if($tab == 'properties') class="active" @endif><a data-active="properties" data-toggle="tab" href="#properties-tab">{{__('property.properties')}}</a></li>
	<li @if($tab == 'quick-sheet') class="active" @endif><a data-active="quick-sheet" data-toggle="tab" href="#quick-sheet">Quick Sheet</a></li>
	<li @if($tab == 'sheet') class="active" @endif><a data-active="sheet" data-toggle="tab" href="#sheet">Sheet</a></li>
	<li @if($tab == 'budget') class="active" @endif><a data-active="budget" data-toggle="tab" href="#budget">Budget</a></li>

	<li @if($tab == 'tenancy-schedule') class="active" @endif><a data-active="tenancy-schedule" data-toggle="tab" href="#tenancy-schedule">Mieterliste</a></li>

	@if($luser->email == 'yiicakephp@gmail.com' || $luser->email == 'c.wiedemann@fcr-immobilien.de' || isLocalhost())
		<li @if($tab == 'tenancy-schedule-new') class="active" @endif><a data-active="tenancy-schedule-new" data-toggle="tab" href="#tenancy-schedule-new">Mieterliste New</a></li>
	@endif

	<li @if($tab == 'swot-template') class="active" @endif><a data-active="swot-template" data-toggle="tab" href="#swot-template">{{__('property.SWOT_template')}}</a></li>
	<li @if($tab == 'schl-template') class="active" @endif><a data-active="schl-template" data-toggle="tab" href="#schl-template">Objektdatenblatt</a></li>
	{{--	<li @if($tab == 'clever-fit-rental-offer') class="active" @endif><a data-toggle="tab" href="#clever-fit-rental-offer">Clever Fit Mietangebot</a></li>--}}
	<!-- <li @if($tab == 'planung-berechnung-wohnen') class="active" @endif><a data-toggle="tab" href="#planung-berechnung-wohnen">Planung Berechnung Wohnen</a></li> -->
	<li @if($tab == 'expose')  class="active" @endif ><a data-active="expose" data-toggle="tab" href="#expose">Anhänge</a></li>
	<li @if($tab == 'bank-modal')  class="active" @endif><a data-active="bank-modal" data-toggle="tab" href="#bank-modal">{{__('user_form.Bankenanfrage')}}</a></li>
	<li  @if($tab == 'comment_tab')  class="active" @endif  ><a data-active="comment_tab" data-toggle="tab" href="#comment_tab">Maßnahmen</a></li>
	<li  @if($tab == 'recommended_tab')  class="active" @endif  ><a data-active="recommended_tab" data-toggle="tab" href="#recommended_tab">Empfehlung</a></li>
	<li  @if($tab == 'provision_tab')  class="active {{$provision_visible_class}}" @else class="{{$provision_visible_class}}" @endif  ><a data-active="provision_tab" data-toggle="tab" href="#provision_tab">Provision</a></li>
	<?php
	$email_182 = array('j.lux@fcr-immobilien.de', 'r.faudies@fcr-immobilien.de', 't.raudies@fcr-immobilien.de', 'a.raudies@fcr-immobilien.de', 's.mueller@fcr-immobilien.de','c.wiedemann@fcr-immobilien.de','yiicakephp@gmail.com',config('users.falk_email'), 'n.eschenbach@fcr-immobilien.de','a.lauterbach@fcr-immobilien.de');
	?>
	@if($properties->id!=182 || ($properties->id==182 && in_array($luser->email,$email_182)))
	<li  @if($tab == 'property_invoice')  class="active" @endif  ><a data-active="property_invoice" data-toggle="tab" href="#property_invoice">Rechnungen</a></li>
	@endif
	<li  @if($tab == 'property_insurance_tab')  class="active" @endif  ><a data-active="property_insurance_tab" data-toggle="tab" href="#property_insurance_tab">Angebote</a></li>

	{{-- <li  @if($tab == 'property_deals')  class="active" @endif  ><a data-toggle="tab" href="#property_deals">Angebote</a></li> --}}
	<li  @if($tab == 'contracts')  class="active" @endif  ><a data-active="contracts" data-toggle="tab" href="#contracts">Verträge</a></li>
	<!-- <li  @if($tab == 'default_payer')  class="active" @endif  ><a data-active="default_payer" data-toggle="tab" href="#default_payer">Offene Posten</a></li> -->

	<li @if($tab == 'einkauf_tab')  class="active" @endif><a data-active="einkauf_tab" data-toggle="tab" href="#einkauf_tab">Einkauf</a></li>
	<li @if($tab == 'vermietung')  class="active" @endif><a data-active="vermietung" data-toggle="tab" href="#vermietung">Vermietungsportal
	</a></li>
	<li @if($tab == 'verkauf_tab')  class="active" @endif><a data-active="verkauf_tab" data-toggle="tab" href="#verkauf_tab">Verkauf</a></li>
	<li @if($tab == 'exportverkauf_tab')  class="active" @endif><a data-active="exportverkauf_tab" data-toggle="tab" href="#exportverkauf_tab">Exposé</a></li>
	<li @if($tab == 'Anzeige_aufgeben')  class="active" @endif><a data-active="Anzeige_aufgeben" data-toggle="tab" href="#Anzeige_aufgeben">Verkaufsportal
	</a></li>
	<li @if($tab == 'Anfragen')  class="active" @endif><a data-active="Anfragen" data-toggle="tab" href="#Anfragen">Anfragen</a></li>
	<!---<li @if($tab == 'Mietflächen')  class="active" @endif><a data-toggle="tab" href="#Mietflächen">Mietflächen</a></li> -->
	<li @if($tab == 'Leerstandsflächen')  class="active" @endif><a data-active="Leerstandsflächen" data-toggle="tab" href="#Leerstandsflächen">Leerstandsflächen</a></li>
	<li @if($tab == 'email_template')  class="active" @endif><a data-active="email_template" data-toggle="tab" href="#email_template">LOI</a></li>
	<!--- <li @if($tab == 'email_template2')  class="active" @endif><a data-toggle="tab" href="#email_template2">email template 2</a></li>
		-->
	<!-- <li @if($tab == 'Immobilie')  class="active" @endif><a data-toggle="tab" href="#Immobilie">Laden Sie eine Immobilie hoch
		</a></li> -->
	@if($luser->role==1 || $luser->role==5 || $luser->second_role==5 || $luser->id==$properties->asset_m_id)
	<li @if($tab == 'finance')  class="active" @endif><a data-active="finance" data-toggle="tab" href="#finance">Finance</a></li>
	@endif
	<li @if($tab == 'dateien')  class="active" @endif><a data-active="dateien" data-toggle="tab" href="#dateien">Dateien
	</a></li>
	<li  @if($tab == 'insurance_tab')  class="active" @endif  ><a data-active="insurance_tab" data-toggle="tab" href="#insurance_tab">Versicherungen</a></li>
	<li  @if($tab == 'property_management')  class="active" @endif  ><a data-active="property_management" data-toggle="tab" href="#property_management">Hausverwaltung</a></li>
	<li  @if($tab == 'am_mail')  class="active" @endif  ><a data-active="am_mail" data-toggle="tab" href="#am_mail">Mails</a></li>
	<li  @if($tab == 'project')  class="active" @endif  ><a data-active="project" data-toggle="tab" href="#project">Projekte</a></li>
	<li  @if($tab == 'feature_analysis')  class="active" @endif  ><a data-active="feature_analysis" data-toggle="tab" href="#feature_analysis">Feature Analysis</a></li>
	{{--@if(Request::segment(2) == '85')--}}
	{{--<li  @if($tab == 'outlook-email')  class="active" @endif  ><a data-toggle="tab" href="#outlook-email">Email</a></li>--}}
	{{--@endif--}}
</ul>
<div class="tab-content">
	<input type="hidden" id="path-properties-show" value="{{route('properties.show', $id)}}">
	<input type="hidden" id="main-properties-path" value="{{route('properties.show', $id)}}">
	<div id="properties-tab" class="tab-pane fade {{($tab == 'properties') ? 'in active':''}}">
		<input type="hidden" id="selected_property_id" value="{{ $id }}">
		<td>
			<?php
				// print_r($properties);
				?>
			<!-- {{$properties->transaction_m_id}} -->
			<label>EK</label>
			<select name="transaction_m_id" class="change-p-user change_transaction_m_id">
				<option value="">{{__('dashboard.transaction_manager')}}</option>
				@foreach($tr_users as $list1)
				@if($properties && $properties->transaction_m_id==$list1->id)
				<option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
				@else
				<option value="{{$list1->id}}">{{$list1->name}}</option>
				@endif
				@endforeach
			</select>
			<label>AM 1</label>
			<select name="asset_m_id" class="change-p-user change_asset_m_id">
				<option value="">{{__('dashboard.asset_manager')}}</option>
				@foreach($as_users as $list1)
				@if($properties && $properties->asset_m_id==$list1->id)
				<option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
				@else
				<option value="{{$list1->id}}">{{$list1->name}}</option>
				@endif
				@endforeach
			</select>
			<label>AM 2</label>
			<select name="asset_m_id2" class="change-p-user change_asset_m_id2">
				<option value="">{{__('dashboard.asset_manager')}}</option>
				@foreach($as_users as $list1)
				@if($properties && $properties->asset_m_id2==$list1->id)
				<option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
				@else
				<option value="{{$list1->id}}">{{$list1->name}}</option>
				@endif
				@endforeach
			</select>
			<label>BL</label>
			<select name="construction_manager" class="change-p-user construction_manager">
				<option value="">{{__('dashboard.asset_manager')}}</option>
				@foreach($as_users as $list1)
				@if($properties && $properties->construction_manager==$list1->id)
				<option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
				@else
				<option value="{{$list1->id}}">{{$list1->name}}</option>
				@endif
				@endforeach
			</select>
			<label>VK</label>
			<select name="seller_id" class="change-p-user change_seller_id">
				<option value="">{{__('dashboard.seller')}}</option>
				@foreach($tr_users as $list1)
				@if($properties && $properties->seller_id==$list1->id)
				<option selected="selected" value="{{$list1->id}}">{{$list1->name}}</option>
				@else
				<option value="{{$list1->id}}">{{$list1->name}}</option>
				@endif
				@endforeach
			</select>
			<label>Art</label>
			<select name="property_status" class="change-p-user change_property_status">
			<option value="0" @if($properties->property_status==0) selected @endif>intern</option>
			<option value="1" @if($properties->property_status==1) selected @endif>extern</option>
			</select>
			<!-- {{$properties->status}} -->
			<label>Status</label>
			<select class="property-status" data-property-id="{{$properties->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
			<option value="{{config('properties.status.externquicksheet')}}" @if($properties->status == config('properties.status.externquicksheet')) {!!"selected" !!} @endif>{{__('property.externquicksheet')}}</option>
			<option value="{{config('properties.status.quicksheet')}}" @if($properties->status == config('properties.status.quicksheet')) {!!"selected" !!} @endif>{{__('property.quicksheet')}}</option>
			<option value="{{config('properties.status.in_purchase')}}" @if($properties->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
			<option value="{{config('properties.status.offer')}}" @if($properties->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
			<option value="{{config('properties.status.exclusive')}}" @if($properties->status == config('properties.status.exclusive')) {!!"selected" !!} @endif>{{__('property.exclusive')}}</option>
			<option value="{{config('properties.status.liquiplanung')}}" @if($properties->status == config('properties.status.liquiplanung')) {!!"selected" !!} @endif>{{__('property.liquiplanung')}}</option>
			<option value="{{config('properties.status.exclusivity')}}" @if($properties->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
			<option value="{{config('properties.status.certified')}}" @if($properties->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
			<option value="{{config('properties.status.duration')}}" @if($properties->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
			<option value="{{config('properties.status.hold')}}" @if($properties->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
			<option value="{{config('properties.status.sold')}}" @if($properties->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
			<option value="{{config('properties.status.declined')}}" @if($properties->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
			<option value="{{config('properties.status.lost')}}" @if($properties->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
			</select>
			<div class="btn-group" style="float: right;">
				<select class="set_as_standard" >
					<option value="">Als Standard gesetzt</option>
					<?php
						$bank_array = array();
						      foreach($banks as $key => $bank)
						      {
						          $bank_array[] = $bank->id;
						      }

						      array_unique($bank_array);
						      $str = implode(',', $bank_array);

						      $properties_banks = DB::table('properties')->select('sheet_title','properties.id','standard_property_status','Ist','soll','banks.name','properties_bank_id')->join('banks','banks.id','=','properties.properties_bank_id')->whereRaw('lock_status=0 and main_property_id='.$properties->id.' and (Ist in('.$str.') OR soll in('.$str.'))')->orderBy('properties_bank_id')->get();





						?>
					<?php
						$ar = array();
						foreach ($properties_banks as $key1 => $bank1):

						?>
					@if(!isset($ar[$bank1->properties_bank_id]))
					<optgroup label="{{$bank1->name}}">
						<?php
							$ar[$bank1->properties_bank_id] = 1;
							?>
						@if($key1>0)
					</optgroup>
					@endif
					@endif
					@if($bank1->Ist)
					<option @if($bank1->standard_property_status == 1) selected @endif value="{{ $bank1->id }}">Ist<?php if($bank1->sheet_title)echo "-".$bank1->sheet_title?>
					</option>
					@else
					<option @if($bank1->standard_property_status == 1) selected @endif value="{{ $bank1->id }}">Soll<?php if($bank1->sheet_title) echo "-".$bank1->sheet_title?>
					</option>
					@endif
					<?php endforeach; ?>
					@if($ar)
					</optgroup>
					@endif
				</select>
				<button type="button" class="btn btn-primary" id="property-zoom-out"><i class="fa fa-search-minus"></i></button>
				<button class="btn btn-default" disabled><span id="property-zoom-value"></span>&nbsp;%</button>
				<button type="button" class="btn btn-primary" id="property-zoom-in"><i class="fa fa-search-plus"></i></button>
			</div>
			<iframe src="{{route('properties.showIframe', $id)}}" id="propertyIframeParent" width="100%"  style="border:none;min-height: 2800px; "  scrolling="auto"></iframe>
			@foreach ($banks as $key => $bank)
			<?php
				if($bank == ''){
					$bank_check = 0;
					$bank = $fake_bank;
				}else{
					$bank_check = 1;
				}

				?>
			@endforeach
	</div>
	<div id="tenancy-schedule" class="tab-pane fade {{($tab == 'tenancy-schedule') ? 'in active':''}}">
	@include('properties.templates.tenancy-schedule')
	</div>

	@if($luser->email == 'yiicakephp@gmail.com' || $luser->email == 'c.wiedemann@fcr-immobilien.de' || isLocalhost())
		<div id="tenancy-schedule-new" class="tab-pane fade {{($tab == 'tenancy-schedule-new') ? 'in active':''}}">
			@include('properties.templates.tenancy-edit')
		</div>
	@endif

	<div id="Anfragen" class="tab-pane fade {{($tab == 'Anfragen') ? 'in active':''}}">
	@include('properties.templates.Anfragen')
	</div>
	<div id="tenant-listing" class="tab-pane fade {{($tab == 'tenant-listing') ? 'in active':''}}">
	<div class="white-box table-responsive" style="height: 600px; overflow: auto;">
	<table class="table">
	<tbody>
	<tr>
	<td colspan="4" class="bg-white"></td>
	<th>Laufzeit ab</th>
	<td colspan="2" class="bg-white"></td>
	</tr>
	<tr>
	<td colspan="4" class="bg-white"></td>
	<td>
	<a href="#" data-placement="bottom" class="inline-edit" data-type="date" data-pk="duration_from" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.duration_from')}}">{{$properties->duration_from}}</a>
	</td>
	<td colspan="2" class="bg-white"></td>
	</tr>
	<tr>
	<th><strong>Mieter</strong></th>
	<th>Netto Kaltmiete p.m.</th>
	<th>Netto Kaltmiete p.a.</th>
	<th>MV Beginn</th>
	<th>MV Ende</th>
	<th>WAULT</th>
	<th>Mieterlöse</th>
	</tr>
	<tr>
	<td>Ankermieter</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_pm_anchor_tenants" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_pm')}}">{{ number_format($properties->nk_pm_anchor_tenants,0,",",".") }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_anchor_tenants" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_pa')}}">{{ number_format($properties->nk_anchor_tenants,0,",",".") }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_begin_anchor_tenants" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.mv_begin')}}">{{ $properties->mv_begin_anchor_tenants }}</a>
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_anchor_tenants" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.mv_end')}}">{{ $properties->mv_anchor_tenants }}</a>
	</td>
	<td>{{number_format($properties->WAULT__of_anchor_tenants, 1,",",".")}}</td>
	<td>{{number_format($properties->Rental_income_of_anchor_tenants, 2,",",".")}}&nbsp;€</td>
	</tr>
	@for($i = 1; $i <= 5; $i++)
	<tr>
	<td>Mieter {{$i}}</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_pm_tenant{{$i}}" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_pm')}}">{{ number_format($properties["nk_pm_tenant{$i}"],0,",",".") }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_tenant{{$i}}" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.net_rent_pa')}}">{{ number_format($properties["nk_tenant{$i}"],0,",",".") }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_begin_tenant{{$i}}" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.mv_begin')}}">{{ $properties["mv_begin_tenant{$i}"] }}</a>
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_tenant{{$i}}" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.mv_end')}}">{{ $properties["mv_tenant{$i}"] }}</a>
	</td>
	<td>{{number_format($properties["WAULT__of_tenant{$i}"], 1,",",".")}}</td>
	<td>{{number_format($properties["Rental_income_of_tenant{$i}"], 2,",",".")}}&nbsp;€</td>
	</tr>
	@endfor
	@foreach($tenants as $key => $tenant)
	<tr>
	<td>Mieter {{ $key + 6 }}</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_pm" data-url="{{route('tenants.update', $tenant->id) }}" data-title="{{__('property.field.net_rent_pm')}}">{{ $tenant->nk_pm }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="number" data-pk="nk_pa" data-url="{{route('tenants.update', $tenant->id) }}" data-title="{{__('property.field.net_rent_pa')}}">{{ $tenant->nk_pa }}</a>&nbsp;€
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_begin" data-url="{{route('tenants.update', $tenant->id) }}" data-title="{{__('property.field.mv_begin')}}">{{ $tenant->mv_begin }}</a>
	</td>
	<td>
	<a href="#" class="inline-edit" data-type="date" data-pk="mv_end" data-url="{{route('tenants.update', $tenant->id) }}" data-title="{{__('property.field.mv_end')}}">{{ $tenant->mv_end }}</a>
	</td>
	<td>{{number_format($tenant->wault, 1,",",".")}}</td>
	<td>{{number_format($tenant->rental_income, 2,",",".")}}&nbsp;€</td>
	</tr>
	@endforeach
	<tr>
	<td>
	<button type="button" class="btn btn-success" id="new-tenant" data-property-id="{{$id}}">Add Tenant</button>
	</td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	</tr>
	</tbody>
	</table>
	</div>
	</div>
	<div id="swot-template" class="tab-pane fade {{($tab == 'swot-template') ? 'in active':''}}">
	<div class="white-box table-responsive" style="height: 600px; overflow: auto;">
	<table class="table" style="width: 100%">
	<tbody>
	<tr>
	<th>Stärken (Strengths)</th>
	<th>Schwächen (Weaknesses)</th>
	</tr>
	<tr style="text-align: center; font-size: 24px; height: 40vh;">
	<td style="width: 50%">
	<a href="#" class="inline-edit" data-type="textarea" data-pk="strengths" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.strengths')}}">{{ $properties->strengths }}</a>
	</td>
	<td style="width: 50%">
	<a href="#" class="inline-edit" data-type="textarea" data-pk="weaknesses" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.weaknesses')}}">{{ $properties->weaknesses }}</a>
	</td>
	</tr>
	<tr>
	<th>Chancen (Opportunities)</th>
	<th>Risiken (Threats)</th>
	</tr>
	<tr style="text-align: center; font-size: 24px; height: 40vh;">
	<td style="width: 50%">
	<a href="#" class="inline-edit" data-type="textarea" data-pk="opportunities" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.opportunities')}}">{{ $properties->opportunities }}</a>
	</td>
	<td style="width: 50%">
	<a href="#" class="inline-edit" data-type="textarea" data-pk="threats" data-url="{{url('property/update/'.$id) }}" data-title="{{__('property.field.threats')}}">{{ $properties->threats }}</a>
	</td>
	</tr>
	</tbody>
	</table>
	<div class="row">
	<div class="swot_ranking col-md-8">
	<form id="swat_form">
	<?php
		$b = $b1 = $b2 = $b3 = $b4 = $b5 = 0;
		if($properties->swot_json)
		{
			// echo $properties->swot_json;
			$nnna = json_decode($properties->swot_json,true);
			if(isset($nnna[0]))
				$b = $nnna[0];

			if(isset($nnna[1]))
				$b1 = $nnna[1];

			if(isset($nnna[2]))
				$b2= $nnna[2];

			if(isset($nnna[3]))
				$b3= $nnna[3];

			if(isset($nnna[4]))
				$b4= $nnna[4];

			if(isset($nnna[5]))
				$b5= $nnna[5];
		}

		?>
	<table class="table table-striped">
	<tr>
	<td>
	<label>Lage : </label>
	<select id="rate_box_1" name="location" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Mietermix : </label>
	<select id="rate_box_2" name="tenant" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b1)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Mieterbonität : </label>
	<select id="rate_box_3" name="tenant_credit" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b2)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Finanzierbarkeit : </label>
	<select id="rate_box_4" name="affordability" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b3)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Bausubstanz innen : </label>
	<select id="rate_box_5" name="building_substance_inside" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b4)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Bausubstanz außen : </label>
	<select id="rate_box_6" name="building_substance_outside" class="">
	<option value="0">-- Choose a value --</option>
	<?php foreach($rateSwotValArray as $val): ?>
	<option @if($val==$b5)selected @endif value="<?php echo $val;?>"><?php echo $val;?></option>
	<?php endforeach; ?>
	</select>
	</td>
	</tr>
	<tr>
	<td>
	<label>Summe : </label>
	<?php
		$bsum = $b +$b1 +$b2 +$b3 +$b4 +$b5;
		?>
	<input type="text" readonly value="{{$bsum}}" id="total_rate" name="total_rate" />
	</td>
	</tr>
	</table>
	<p>*10 ist am besten</p>
	</form>
	</div>
	</div>
	</div>
	</div>
	<?php
		$leerstand_total = 0;
		$vermietet_total = 0;
		$GLOBALS['ist_sheet'] = "";
		$GLOBALS['release_sheet'] = "";

		?>
	<div id="quick-sheet" class="tab-pane fade {{($tab == 'quick-sheet') ? 'in active':''}}">
	@include('properties.templates.quicksheet')
	</div>
	<div id="sheet" class="tab-pane fade {{($tab == 'sheet') ? 'in active':''}}">
	@include('properties.templates.sheet')
	</div>
	<div id="budget" class="tab-pane fade {{($tab == 'budget') ? 'in active':''}}">
	@include('properties.templates.budget')
	</div>
	@if($luser->role==1 || $luser->role==5 || $luser->second_role==5 || $luser->id==$properties->asset_m_id)
	<div id="finance" class="tab-pane fade {{($tab == 'finance') ? 'in active':''}}">
	@include('properties.templates.finance')
	</div>
	@endif
	<div id="insurance_tab" class="tab-pane fade {{($tab == 'insurance_tab') ? 'in active':''}}">
	@include('properties.templates.insurance_tab')
	</div>
	<div id="property_insurance_tab" class="tab-pane fade {{($tab == 'property_insurance_tab') ? 'in active':''}}">
		@include('properties.templates.property_insurance_tab')
	</div>
	{{--<div id="outlook-email" class="tab-pane fade {{($tab == 'outlook-email') ? 'in active':''}}">--}}
	{{--@include('properties.email')--}}
	{{--</div>--}}
	<div id="dateien" class="tab-pane fade {{($tab == 'dateien') ? 'in active':''}}">
	@include('properties.templates.dateien')
	</div>
	<div id="schl-template" class="tab-pane fade {{($tab == 'schl-template') ? 'in active':''}}">
	@include('properties.templates.schl')
	</div>
	<div id="clever-fit-rental-offer" class="tab-pane fade {{($tab == 'clever-fit-rental-offer') ? 'in active':''}}">
	@include('properties.templates.clever-fit')
	</div>
	<div id="planung-berechnung-wohnen" class="tab-pane fade {{($tab == 'planung-berechnung-wohnen') ? 'in active':''}}">
	@include('properties.templates.planung-berechnung-wohnen')
	</div>
	<div id="expose" class="tab-pane fade {{($tab == 'expose') ? 'in active':''}}">
	@include('properties.templates.expose')
	</div>
	<div id="comment_tab" class="tab-pane fade {{($tab == 'comment_tab') ? 'in active':''}}">
	@include('properties.templates.comments_tab')
	</div>
	<div id="recommended_tab" class="tab-pane fade {{($tab == 'recommended_tab') ? 'in active':''}}">
	@include('properties.templates.recommended_tab')
	</div>
	<div id="provision_tab" class="tab-pane fade {{($tab == 'provision_tab') ? 'in active':''}}">
	@include('properties.templates.provision_tab')
	</div>
	@if($properties->id!=182 || ($properties->id==182 && in_array($luser->email,$email_182)))
	<div id="property_invoice" class="tab-pane fade {{($tab == 'property_invoice') ? 'in active':''}}">
	@include('properties.templates.property_invoice')
	</div>
	@endif
	{{-- <div id="default_payer" class="tab-pane fade {{($tab == 'default_payer') ? 'in active':''}}">
		@include('properties.templates.default_payer')
	</div>--}}
	{{-- <div id="property_deals" class="tab-pane fade {{($tab == 'property_deals') ? 'in active':''}}">
		@include('properties.templates.property_deals')
	</div> --}}
	<div id="contracts" class="tab-pane fade {{($tab == 'contracts') ? 'in active':''}}">
		@include('properties.templates.contracts')
	</div>
	<div id="einkauf_tab" class="tab-pane fade {{($tab == 'einkauf_tab') ? 'in active':''}}">
	@include('properties.templates.einkauf_tab')
	</div>
	<div id="verkauf_tab" class="tab-pane fade {{($tab == 'verkauf_tab') ? 'in active':''}}">
	@include('properties.templates.verkauf_tab')
	</div>
	<div id="exportverkauf_tab" class="tab-pane fade {{($tab == 'exportverkauf_tab') ? 'in active':''}}">
	@include('properties.templates.exportverkauf_tab')
	</div>
	<div id="Anzeige_aufgeben" class="tab-pane fade {{($tab == 'Anzeige_aufgeben') ? 'in active':''}}">
	@include('properties.templates.Anzeige_aufgeben')
	</div>
	<div id="vermietung" class="tab-pane fade {{($tab == 'vermietung') ? 'in active':''}}">
	@include('properties.templates.vermietung')
	</div>
	<div id="Immobilie" class="tab-pane fade {{($tab == 'Immobilie') ? 'in active':''}}">
	@include('properties.templates.Immobilie')
	</div>
	<div  class="uploadAttachement tab-pane fade {{($tab == 'uploadAttachement') ? 'in active':''}}">
	@include('properties.templates.upload_attachment_immobilie')
	</div>
	<div id="Mietflächen" class="tab-pane fade {{($tab == 'Mietflächen') ? 'in active':''}}">
	@include('properties.templates.Mietflächen')
	</div>
	<div id="Leerstandsflächen" class="tab-pane fade {{($tab == 'Leerstandsflächen') ? 'in active':''}}">
	@include('properties.templates.Leerstandsflächen')
	</div>
	<div id="email_template" class="tab-pane fade {{($tab == 'email_template') ? 'in active':''}}">
	@include('properties.templates.email_template')
	</div>
	<div id="email_template2" class="tab-pane fade {{($tab == 'email_template2') ? 'in active':''}}">
	@include('properties.templates.email_template2')
	</div>
	<div id="bank-modal" class="tab-pane fade {{($tab == 'bank-modal') ? 'in active':''}}">
	@include('properties.templates.bank-modal')
	</div>
	<div id="property_management" class="tab-pane fade {{($tab == 'property_management') ? 'in active':''}}">
	@include('properties.templates.property_management')
	</div>
	<div id="am_mail" class="tab-pane fade {{($tab == 'am_mail') ? 'in active':''}}">
	@include('properties.templates.mails')
	</div>
	<div id="project" class="tab-pane fade {{($tab == 'project') ? 'in active':''}}">
		@include('properties.project.index')
	</div>

	<div id="feature_analysis" class="tab-pane fade {{($tab == 'feature_analysis') ? 'in active':''}}">
		@include('properties.templates.feature_analysis')
	</div>

	<iframe id="txtArea1" style="display:none"></iframe>
</div>
@include('properties.templates.modal_popup')

@include('properties.popup_for_submit_listing_to_api')
@endsection

@section('js')

	<script>
		var _token 			= '{{ csrf_token() }}';
		var id 				= '{{ $id }}';



		$(document).ready(function(){
			if($('#sortablelist').length>0)
			{


			new Sortable(sortablelist, {
			   animation: 150,
			   ghostClass: 'sortable-ghost',
			   onChange: function(/**Event*/evt) {

			   		var checked_arr = [];
			   		$('.dr-image').each(function(){
					    console.log($(this).val());
					    checked_arr.push($(this).val());
					})
			   		k=0;
					$('.exportsetasfavourite').each(function(){
							$(this).attr('data-id',k);
							k=k+1;
					})

					$.ajax({
		              type : 'POST',
		              url : "{{route('update_image_order')}}",
		              data : {i_arr:checked_arr, _token : _token,property_id:$('#selected_property_id').val()},
		              success : function (data) {

		              }});
			   },
			 });

			}

			if($('#sortablelist1').length>0)
			{


			new Sortable(sortablelist1, {
			   animation: 150,
			   ghostClass: 'sortable-ghost',
			   onChange: function(/**Event*/evt) {

			   		var checked_arr = [];
			   		$('.dr-image-1').each(function(){
					    console.log($(this).val());
					    checked_arr.push($(this).val());
					})
			   		k=0;
					$('.setasfavourite').each(function(){
							$(this).attr('data-id',k);
							k=k+1;
					})

					$.ajax({
		              type : 'POST',
		              url : "{{route('update_image_order1')}}",
		              data : {i_arr:checked_arr, _token : _token,property_id:$('#selected_property_id').val()},
		              success : function (data) {

		              }});
			   },
			 });

			}
		})


		var route_prefix  	= '{{ url("file-manager") }}';
		var lfm_route    	= '{{ url("file-manager") }}';
		var lang 			= {!! json_encode(trans('lfm')) !!};
		var workingDir 		= "{{$workingDir}}";
		var user_signature 	= '{{ url('/').'/img/giphy.gif' }}';
		var request_showall	= '{{ Request()->showall }}';
		var workingDir 		= '{{$workingDir}}';
		var current_property_id = '{{ $properties->id }}';
		var is_falk			= {{ ( Auth::user()->email == config('users.falk_email') ) ? 1 : 0 }};

		/*-------------------URL START------------------*/

		var url_property_saveEmpfehlungFile 	= '{{ url("property/saveEmpfehlungFile") }}';
		var url_saveMieterlisteFile 			= '{{ route('saveMieterlisteFile') }}';
		var url_saveEmpfehlungFile 				= '{{ route("saveEmpfehlungFile") }}';
		var url_property_addmaillog 			= '{{ url('property/addmaillog') }}';
		var url_properties_comment_get_comments = '{{ url('properties-comment/get-comments') }}';
		var url_properties_comment_save 		= '{{ url('properties-comment/save') }}';
		var url_getReleasebutton 				= '{{ route('getReleasebutton') }}';
		var url_getProvisionbutton 				= '{{ route('getProvisionbutton') }}';
		var url_changeempfehlungdetails 		= '{{ route('changeempfehlungdetails') }}';
		var url_changeprovisioninfo 			= '{{ route('changeprovisioninfo') }}';
		var url_deleteprovisioninfo 			= '{{ route('deleteprovisioninfo') }}';
		var url_saveractivity 					= '{{ route('saveractivity') }}';
		var url_recommended_list 				= '{{ route('recommended_list') }}';
		var url_saveleaseactivity 				= '{{ route('saveleaseactivity') }}';
		var url_lease_list 						= '{{ route('lease_list') }}';
		var url_updatelease 					= '{{ route('updatelease') }}';
		var url_save_new_post 					= '{{ route('save_new_post') }}';
		var url_add_new_vacant 					= '{{ route('add_new_vacant') }}';
		var url_delete_new_vacant 				= '{{ route('delete_new_vacant') }}';
		var url_delete_comment_file 			= '{{ route('delete_comment_file') }}';
		var url_savebankinfoformail 			= '{{ route('savebankinfoformail') }}';
		var url_save_vacant_space 				= '{{ route('save_vacant_space') }}';
		var url_save_vacant_data 				= '{{ route('save_vacant_data') }}';
		var url_properties_change_status 		= '{{ route('properties.change_status') }}';
		var url_remove_uploaded_file 			= '{{ route('remove_uploaded_file') }}';
		var url_remove_vacant_media 			= '{{ route('remove_vacant_media') }}';
		var url_set_vacant_media_favourite		= '{{ route('set_vacant_media_favourite') }}';
		var url_set_ad_image_favourite 			= '{{ route('set_ad_image_favourite') }}';
		var url_set_vacantitem_favourite 		= '{{ route('set_vacantitem_favourite') }}';
		var url_set_export_media_favourite 		= '{{ route('set_export_media_favourite') }}';
		var url_tenants_store 					= '{{ route('tenants.store') }}';
		var url_change_comment					= '{{ route('change-comment') }}?';
		var url_uploadfiles 					= '{{ route('uploadfiles') }}';
		var url_delete_ads_images 				= '{{ route('delete_ads_images') }}';
		var url_delete_ads_pdf 					= '{{ route('delete_ads_pdf') }}';
		var url_delete_property_pdf 			= '{{ route('delete_property_pdf') }}';
		var url_delete_property_insurance		= '{{ route('delete_property_insurance') }}';
		var url_delete_service_provider			= '{{ route('delete_service_provider') }}';
		var url_delete_maintenance				= '{{ route('delete_maintenance') }}';
		var url_delete_investation				= '{{ route('delete_investation') }}';
		var url_change_date						= '{{ route('change_date') }}';
		var url_property_set_as_standard 		= '{{ url('property/set_as_standard') }}/{{ $id }}';
		var url_delete_tenant 					= '{{ route('delete_tenant') }}';
		var url_properties_upload_listing_to_api = '{{ url('/properties/upload_listing_to_api') }}';
		var url_properties_upload_attachment_images = '{{ url('/properties/upload_attachment_images') }}';
		var url_email_template_users 			= '{{ url('email_template_users') }}';
		var url_updatepropertyuser 				= '{{ route('updatepropertyuser') }}';
		var url_properties_api_imag_del 		= '{{ url('/properties/api_imag_del') }}';
		var url_add_new_city 					= '{{ route('add_new_city') }}';
		var url_add_property_bank 				= '{{ route('add_property_bank') }}';
		var url_add_new_banken  				= '{{ route('add_new_banken') }}';
		var url_update_bank  					= '{{ route('update_bank') }}';
		var url_searchpbank						= '{{ route("searchpbank") }}';
		var url_searchcity 						= '{{ route("searchcity") }}';
		var url_searchbank 						= '{{ route("searchbank") }}';
		var url_property_update_steuerberater 	= '{{ url('property/update/steuerberater') }}/{{ $id }}';
		var url_property_update_schl 			= '{{ url('property/update/schl') }}/{{ $id }}';
		var url_getbanklist 					= '{{ route('getbanklist') }}';
		var url_getstatusloi 					= '{{ route('getstatusloi') }}';
		var url_property_management_add 		= '{{ url('property_management/add') }}';
		var url_property_management_update 		= '{{ route('property_management.update') }}';
		var url_getStatusLoiByUserId 			= '{{ route('getStatusLoiByUserId') }}';
		var url_schl_file 						= '{{ route('schl-file') }}';
		var url_upload_bank_file 				= '{{ route('upload-bank-file') }}';
		var url_uploadattachments 				= '{{ route('uploadattachments') }}';
		var url_remove_attachment_file 			= '{{ route('remove_attachment_file') }}';
		var url_removepropertybanken 			= '{{ route('removepropertybanken') }}';
		var url_removestatusloi 				= '{{ route('removestatusloi') }}';
		var url_statusloi_update 				= '{{ url('statusloi/update') }}';
		var url_update_versicherung_approved_status = '{{ url('/update_versicherung_approved_status') }}';
		var url_update_falk__status 			= '{{ url('/update_falk__status') }}';
		var url_delete_Property_insurance1 		= '{{ url('/delete_Property_insurance') }}';
		var url_sendemailtobankers 				= '{{ route('sendemailtobankers') }}';
		var url_property_update_schl_1 			= '{{url('property/update/schl') }}';
		var url_showresult 						= '{{ route('showresult') }}';
		var url_deletesheet			 			= '{{ route('deletesheet') }}';
		var url_genarate_property_pdf 			= '{{ url('/genarate_property_pdf') }}';
		var url_property_vacantreleaseprocedure = '{{ url('property/vacantreleaseprocedure') }}';
		var url_property_provisionreleaseprocedure = '{{ url('property/provisionreleaseprocedure') }}';
		var url_property_invoicereleaseprocedure = '{{ url('property/invoicereleaseprocedure') }}';
		var url_property_dealreleaseprocedure = '{{ url('property/dealreleaseprocedure') }}';

		var url_get_property_invoice 			= '{{ route('get_property_invoice', ['property_id' => $properties->id]) }}';
		var url_get_pending_invoice 			= '{{ route('get_pending_invoice', ['id' => $properties->id]) }}';
		var url_get_pending_am_invoice 			= '{{ route('get_pending_am_invoice', ['id' => $properties->id]) }}';
		var url_get_pending_hv_invoice 			= '{{ route('get_pending_hv_invoice', ['id' => $properties->id]) }}';
		var url_get_pending_user_invoice 		= '{{ route('get_pending_user_invoice', ['id' => $properties->id]) }}';



		var url_get_not_released_invoice 		= '{{ route('get_not_released_invoice', ['id' => $properties->id]) }}';
		var url_get_not_released_invoice_am 	= '{{ route('get_not_released_invoice_am', ['id' => $properties->id]) }}';
		var url_get_not_released_invoice_hv 	= '{{ route('get_not_released_invoice_hv', ['id' => $properties->id]) }}';
		var url_get_not_released_invoice_user 	= '{{ route('get_not_released_invoice_user', ['id' => $properties->id]) }}';


		var url_get_release_property_invoice 	= '{{ route('get_release_property_invoice', ['property_id' => $properties->id]) }}';
		var url_get_invoice_logs 				= '{{ route('get_invoice_logs', ['property_id' => $properties->id]) }}';
		var url_get_deal_logs						= '{{ route('get_deal_logs', ['property_id' => $properties->id]) }}';
		var url_makeAsPaid 						= '{{ route('makeAsPaid') }}';
		var url_add_property_invoice 			= '{{ route('add_property_invoice') }}';
		var url_add_default_payer 				= '{{ route('add_default_payer') }}';
		var url_get_default_payer 				= '{{ route('get_default_payer', ['property_id' => $properties->id]) }}';
		var url_addBankenContactEmployee 		= '{{ route('addBankenContactEmployee') }}';
		var url_delete_property_invoice 		= '{{ route("delete_property_invoice", ":id") }}';
		var url_set_property_invoice_email 		= '{{ route("set_property_invoice_email", ["id" => ":id", "email" => ":email"]) }}';
		var url_add_property_deal 				= '{{ route('add_property_deal') }}';
		var url_get_property_deals 				= '{{ route('get_property_deals', ['property_id' => $properties->id]) }}';

		var url_deal_mark_as_notrelease 		= '{{ route('deal_mark_as_notrelease') }}';
		var url_deal_mark_as_pending 			= '{{ route('deal_mark_as_pending') }}';
		var url_get_pending_deals 				= '{{ route('get_pending_deals', ['property_id' => $properties->id]) }}';
		var url_get_not_release_deals 			= '{{ route('get_not_release_deals', ['property_id' => $properties->id]) }}';

		var url_add_contract 					= '{{ route('add_contract') }}';
		var url_get_contract 					= '{{ route('get_contract', ['property_id' => $properties->id]) }}';
		var url_contract_mark_as_notrelease 		= '{{ route('contract_mark_as_notrelease') }}';
		var url_contract_mark_as_notrelease_am 		= '{{ route('contract_mark_as_notrelease_am') }}';
		var url_contract_mark_as_pending 		= '{{ route('contract_mark_as_pending') }}';
		var url_property_contractreleaseprocedure = '{{ url('property/contractreleaseprocedure') }}';
		var url_get_pending_contracts 			= '{{ route('get_pending_contracts', ['property_id' => $properties->id]) }}';
		var url_get_not_release_contracts 		= '{{ route('get_not_release_contracts', ['property_id' => $properties->id]) }}';
		var url_get_not_release__am_contracts 	= '{{ route('get_not_release_am_contracts', ['property_id' => $properties->id]) }}';
		var url_get_release_contracts 			= '{{ route('get_release_contracts', ['property_id' => $properties->id]) }}';
		var url_get_old_contracts 				= '{{ route('get_old_contracts', ['property_id' => $properties->id]) }}';
		var url_get_contract_logs				= '{{ route('get_contract_logs', ['property_id' => $properties->id]) }}';
		var url_property_invoicenotrelease 		= '{{ url('property/invoicenotrelease') }}';

		var url_property_invoicemarkpending 	= '{{ url('property/invoicemarkpending') }}';
		var url_property_invoicemarkpendingam 	= '{{ url('property/invoicemarkpendingam') }}';

		var url_property_invoicemarkreject 		= '{{ url('property/invoicemarkreject') }}';
		var url_get_property_insurance_detail 	= '{{ route("get_property_insurance_detail", ["tab_id" => ":tab_id"]) }}';
		var url_add_property_insurance_detail 	= '{{ route('add_property_insurance_detail') }}';
		var url_get_property_insurance_detail_log	= '{{ route('get_property_insurance_detail_log', ['property_id' => $properties->id]) }}';
		var url_delete_property_insurance_title = '{{ route("delete_property_insurance_title", ["id" => ":id"]) }}';
		var url_get_rent_paid_data				= '{{ route('get_rent_paid_data', ['property_id' => $properties->id]) }}';
		var url_delete_recommended_comment		= '{{ route('delete_recommended_comment', ['id' => ':id']) }}';
		var url_get_recommended_comment			= '{{ route('get_recommended_comment', ['id' => ':id']) }}';
		var url_delete_provision_comment		= '{{ route('delete_provision_comment', ['id' => ':id']) }}';
		var url_get_recommended_table			= '{{ route('get_recommended_table', ['property_id' => $properties->id]) }}';
		var url_get_kosten_umbau				= '{{ route('get_kosten_umbau', ['property_id' => ':property_id', 'tenant_id' => ':tenant_id']) }}';
		var url_insurance_mark_as_pending       = '{{ route('insurance_mark_as_pending') }}';
		var url_insurance_mark_as_notrelease_am     = '{{ route('insurance_mark_as_notrelease_am') }}';
		var url_insurance_mark_as_notrelease       	= '{{ route('insurance_mark_as_notrelease') }}';
		var url_delete_insurance_detail_comment	= '{{ route('delete_insurance_detail_comment', ['id' => ':id']) }}';
		var url_select_mieterliste_file 		= '{{ route('select_mieterliste_file', ['property_id' => $id]) }}';
		var url_select_mieterliste_dir 		= '{{ route('select_mieterliste_dir', ['property_id' => $id]) }}';
		var url_delete_property_comment         = '{{ route('delete_property_comment', ['id' => ':id']) }}';
      	var url_add_property_comment            = '{{ route('add_property_comment') }}';
      	var url_get_property_comment            = '{{ route('get_property_comment') }}';
      	var url_delete_log            			= '{{ route('delete_log') }}';

      	var url_add_provision_file 		= '{{ route('add_provision_file', ['property_id' => $id]) }}';
		var url_add_provision_dir 		= '{{ route('add_provision_dir', ['property_id' => $id]) }}';
		var url_get_tenancy_item_detail = '{{ route('get_tenancy_item_detail') }}';
		var url_update_liquiplanung_data 		= '{{ route('update_liquiplanung_data', ['property_id' => $id]) }}';

		var url_getItemComment                    = '{{ route('getItemComment') }}';
      	var url_addItemComment                    = '{{ route('addItemComment') }}';
      	var url_deleteItemComment                 = '{{ route('deleteItemComment') }}';
		var url_delete_ad_images 				= '{{ route('delete_ad_images') }}';
      	var url_update_property_custom_field 	  = '{{ route('update_property_custom_field', ['property_id' => $properties->id]) }}';
      	var url_get_all_users 				= '{{ route('get_all_users') }}';

      	var url_invoice_release_am 			= '{{ route('invoice_release_am') }}';
      	var url_invoice_release_hv 			= '{{ route('invoice_release_hv') }}';
      	var url_invoice_release_user 		= '{{ route('invoice_release_user') }}';
      	var url_invoice_release_falk 		= '{{ route('invoice_release_falk') }}';



		/*-------------------URL END------------------*/
	</script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>


	{{-- <script src="{{ asset('js/bootstrap-editable.min.js') }}"></script> --}}
	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
	<script src="{{ asset('js/custom-datatable.js') }}"></script>
	<script src="{{ asset('js/tagsinput-revisited.js') }}"></script>
	<script src="{{ asset('js/ckeditor.js') }}"></script>
	<script type='text/javascript' src='//maps.googleapis.com/maps/api/js?libraries=places&#038;key={{config("env.GOOGLEMAP_API_KEY")}}&#038;callback'></script>
	<script src="{{ asset('file-manager/js/bootbox.min.js') }}"></script>
	<script src="{{ asset('file-manager/js/script.js') }}"></script>

	{{-- Page js --}}
	<script src="{{asset('js/property-details.js')}}?{{time()}}"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key={{config('env.GOOGLEMAP_API_KEY')}}
	&libraries=places&callback=initAutocomplete" async defer></script>

	<script>
		function showResult(result) {

		    document.getElementById('latitude').value = result.geometry.location.lat();
		    document.getElementById('longitude').value = result.geometry.location.lng();
		}

		function getLatitudeLongitude(callback, address) {
	        // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
	        address = address || 'Ferrol, Galicia, Spain';
	        // Initialize the Geocoder
	        geocoder = new google.maps.Geocoder();
	        if (geocoder) {
	            geocoder.geocode({
	                'address': address
	            }, function (results, status) {
	                if (status == google.maps.GeocoderStatus.OK) {
	                    callback(results[0]);
	                }
	            });
	        }
	    }


		function generate_option_from_json(jsonData, fromLoad){
		    //Load Category Json Data To Brand Select
		    if (fromLoad === 'category_to_brand'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected> <?php echo trans('app.select_a_brand') ?> </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
		            }
		            $('#brand_select').html(option);
		            $('#brand_select').select2();
		        }else {
		            $('#brand_select').html('');
		            $('#brand_select').select2();
		        }
		        $('#brand_loader').hide('slow');
		    }else if(fromLoad === 'country_to_state'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Bundesland auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
		            }
		            $('#state_select').html(option);
		            //$('#state_select').select2();
		        }else {
		            $('#state_select').html('');
		           // $('#state_select').select2();
		        }
		        $('#state_loader').hide('slow');

		    }else if(fromLoad === 'vcountry_to_state'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Bundesland auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
		            }
		            $('#vstate_select').html(option);
		            //$('#state_select').select2();
		        }else {
		            $('#vstate_select').html('');
		           // $('#state_select').select2();
		        }
		        $('#state_loader').hide('slow');

		    }
		    else if(fromLoad === 'excountry_to_state'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Bundesland auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
		            }
		            $('#excountry_to_state').html(option);
		            //$('#state_select').select2();
		        }else {
		            $('#excountry_to_state').html('');
		           // $('#state_select').select2();
		        }
		        $('#state_loader').hide('slow');

		    }else if(fromLoad === 'state_to_city'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Stadt auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
		            }
		            $('#city_select').html(option);
		            //$('#city_select').select2();
		        }else {
		            $('#city_select').html('');
		            //$('#city_select').select2();
		        }
		        $('#city_loader').hide('slow');
		    }
		}

	    $(function(){
	    	$('.multi-category').select2();

	        $('#street').change(function(){
	            address = "";
	            var address = $(this).val();
	            // getLatitudeLongitude(showResult, address)
	        });
	    });

		$(function(){
		    $('#country').change(function(){
		        var country_id = $(this).val();
		        $('#state_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_state_by_country') }}',
		            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'country_to_state');
		            }
		        });
		    });

		    $('#vcountry').change(function(){
		        var country_id = $(this).val();
		        $('#state_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_state_by_country') }}',
		            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'vcountry_to_state');
		            }
		        });
		    });

		    $('#excountry').change(function(){
		        var country_id = $(this).val();
		        $('#state_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_state_by_country') }}',
		            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'excountry_to_state');
		            }
		        });
		    });

		    $('[name="state"]').change(function(){
		        var state_id = $(this).val();
		        $('#city_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_city_by_state') }}',
		            data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'state_to_city');
		            }
		        });
		    });
		});
	</script>

	@if(isset($ads) && $ads->latitude)
		<script type="text/javascript">
			var myLatLng = {lat: {{ $ads->latitude }}, lng: {{ $ads->longitude }} };
			var mytitle = "{{$ads->title}}";
		</script>
	@else
		<script type="text/javascript">
			var myLatLng = {lat: 48.135690, lng: 11.555480 };
			var mytitle = "You";
		</script>
	@endif

	<script type="text/javascript">
		window.einkauf_file_link_isPageReload = false;
	    // This example adds a search box to a map, using the Google Place Autocomplete
	    // feature. People can enter geographical searches. The search box will return a
	    // pick list containing a mix of places and predicted search terms.

		function initAutocomplete() {

	       	var map = new google.maps.Map(document.getElementById('dvMap'), {
	           center: myLatLng,
	           zoom: 15,
	           mapTypeId: google.maps.MapTypeId.ROADMAP
	       	});

	       	// Create the search box and link it to the UI element.
	       	var input = document.getElementById('pac-input');
	       	var searchBox = new google.maps.places.SearchBox(input);
	       	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	       	// Bias the SearchBox results towards current map's viewport.
	       	map.addListener('bounds_changed', function() {
	           	searchBox.setBounds(map.getBounds());
	       	});

	       	//Click event for getting lat lng
	       	google.maps.event.addListener(map, 'click', function (e) {
	           $('input#latitude').val(e.latLng.lat());
	           $('input#longitude').val(e.latLng.lng());
	       	});

	       	var marker = new google.maps.Marker({
	           position: myLatLng,
	           map: map,
	           title: mytitle
	       	});

	       	marker.setMap(map);

	       	var markers = [];
	       	// [START region_getplaces]
	       	// Listen for the event fired when the user selects a prediction and retrieve
	       	// more details for that place.
	       	searchBox.addListener('places_changed', function() {
	           var places = searchBox.getPlaces();

	           if (places.length == 0) {
	               return;
	           }

	           // Clear out the old markers.
	           markers.forEach(function(marker) {
	               marker.setMap(null);
	           });
	           markers = [];

	           // For each place, get the icon, name and location.
	           var bounds = new google.maps.LatLngBounds();
	           places.forEach(function(place) {
	               var icon = {
	                   url: place.icon,
	                   size: new google.maps.Size(71, 71),
	                   origin: new google.maps.Point(0, 0),
	                   anchor: new google.maps.Point(17, 34),
	                   scaledSize: new google.maps.Size(25, 25)
	               };

	               $('#latitude').val(place.geometry.location.lat());
	               $('#longitude').val(place.geometry.location.lng());

	               // Create a marker for each place.
	               markers.push(new google.maps.Marker({
	                   map: map,
	                   icon: icon,
	                   title: place.name,
	                   position: place.geometry.location
	               }));

	               if (place.geometry.viewport) {
	                   // Only geocodes have viewport.
	                   bounds.union(place.geometry.viewport);
	               } else {
	                   bounds.extend(place.geometry.location);
	               }
	           });
	           map.fitBounds(bounds);
	       	});
	       	// [END region_getplaces]

	       	var map2 = new google.maps.Map(document.getElementById('dvMap2'), {
	           center: myLatLng,
	           zoom: 15,
	           mapTypeId: google.maps.MapTypeId.ROADMAP
	       	});

	       	// Create the search box and link it to the UI element.
	       	var input = document.getElementById('pac-input2');
	       	var searchBox2 = new google.maps.places.SearchBox(input);
	       	map2.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	       	// Bias the SearchBox results towards current map's viewport.
	       	map2.addListener('bounds_changed', function() {
	           searchBox2.setBounds(map.getBounds());
	       	});

	       	//Click event for getting lat lng
	       	google.maps.event.addListener(map2, 'click', function (e) {
	           $('input#l-latitude').val(e.latLng.lat());
	           $('input#l-longitude').val(e.latLng.lng());
	       	});

	       	var marker = new google.maps.Marker({
	           position: myLatLng,
	           map: map2,
	           title: mytitle
	       	});
	       	marker.setMap(map2);

	       	var markers = [];
	       	// [START region_getplaces]
	       	// Listen for the event fired when the user selects a prediction and retrieve
	       	// more details for that place.
	       	searchBox2.addListener('places_changed', function() {
	           var places = searchBox2.getPlaces();

	           if (places.length == 0) {
	               return;
	           }

	           // Clear out the old markers.
	           markers.forEach(function(marker) {
	               marker.setMap(null);
	           });
	           markers = [];

	           // For each place, get the icon, name and location.
	           var bounds = new google.maps.LatLngBounds();
	           places.forEach(function(place) {
	               var icon = {
	                   url: place.icon,
	                   size: new google.maps.Size(71, 71),
	                   origin: new google.maps.Point(0, 0),
	                   anchor: new google.maps.Point(17, 34),
	                   scaledSize: new google.maps.Size(25, 25)
	               };

	               $('#l-latitude').val(place.geometry.location.lat());
	               $('#l-longitude').val(place.geometry.location.lng());

	               // Create a marker for each place.
	               markers.push(new google.maps.Marker({
	                   map: map2,
	                   icon: icon,
	                   title: place.name,
	                   position: place.geometry.location
	               }));

	               if (place.geometry.viewport) {
	                   // Only geocodes have viewport.
	                   bounds.union(place.geometry.viewport);
	               } else {
	                   bounds.extend(place.geometry.location);
	               }
	           });
	   		});
	   	}

		function init(){
			var input = document.getElementById( 'google_address' );

	        if ( null === input ) {
	            return;
	        }
	        autoComplete = new google.maps.places.Autocomplete(input);
	        autoComplete.setFields(['address_components', 'geometry', 'icon', 'name']);
	        autoComplete.addListener('place_changed', fillInAddress);
	    }

	    function fillInAddress() {
	        var place = autoComplete.getPlace();
	        $("#location_latitude").val(place.geometry.location.lat());
	        $("#location_longitude").val(place.geometry.location.lng());
	    }
		google.maps.event.addDomListener( window, 'load', init );


	    function getAngebotButton()
	    {
	    	var id = $('#selected_property_id').val();

    	    $.ajax({
	            type : 'GET',
	            url : '{{ route('get_angebot_button') }}',
	            data : { id : id },
	            success : function (data) {
	                // generate_option_from_json(data, 'state_to_city');
	                $.each(data.btn, function(key, value) {
		                $('.ins-btn-' + key).html(value);
		            });
	            }
	        });
	    }


	    if(is_falk==0)
	    getAngebotButton();
	</script>

@endsection
