<!DOCTYPE html>
<html lang="en" style="overflow-y: hidden;">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/plugins/images/favicon.png') }}">
    <title>{{__('app.title')}}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Menu CSS -->
    <link href="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css') }}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{ asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <!-- morris CSS -->
    <link href="{{ asset('assets/plugins/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{ asset('assets/plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="{{ asset('assets/plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet" />
	<!--jquery ui datepicker-->
	<link href="{{ asset('assets/plugins/bower_components/x-editable/dist/jquery-editable/jquery-ui-datepicker/css/redmond/jquery-ui-1.10.3.custom.min.css') }}" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('assets/css/colors/default.css') }}" id="theme" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2.css" rel="stylesheet" />
    <link href="/css/custom-style.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
     <link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">-->

    <!-- [endif]-->
    <style type="text/css">
    .select2-container {
    	width: 100px;
    }
    .remove-extra{
		color: red;
    	font-weight: bold;
    	cursor: pointer;
    	display: none;
	}
    .select2-container .select2-choice{
    	display: block;
	    height: 26px;
	    padding: 0 0 0 8px;
	    overflow: hidden;
	    position: relative;
	    border: 1px solid #aaa !important;
	    white-space: nowrap;
	    line-height: 26px;
	    color: #444;
	    text-decoration: none;
	    border-radius: 4px;
	    background-clip: padding-box;
	    -webkit-touch-callout: none;
	    -webkit-user-select: none;
	    -moz-user-select: none;
	    -ms-user-select: none;
	    user-select: none;
	    background-color: #fff;
	    background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0, #eee), color-stop(0.5, #fff));
	    background-image: -webkit-linear-gradient(center bottom, #eee 0%, #fff 50%);
    }

    .select2-dropdown-open .select2-choice {
	    border-bottom-color: transparent;
	    -webkit-box-shadow: 0 1px 0 #fff inset;
	    box-shadow: 0 1px 0 #fff inset;
	    border-bottom-left-radius: 0;
	    border-bottom-right-radius: 0;
	    background-color: #eee !important;
	    background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0, #fff), color-stop(0.5, #eee));
	    background-image: -webkit-linear-gradient(center bottom, #fff 0%, #eee 50%);
	    background-image: -moz-linear-gradient(center bottom, #fff 0%, #eee 50%);
	    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#eeeeee', endColorstr='#ffffff', GradientType=0);
	    background-image: linear-gradient(to top, #fff 0%, #eee 50%);
	}
	.select2-drop{
		min-width: 180px !important;
		margin-top: 5px;
	}
	.note-table td,.note-table th{
		min-width: 109px;
	}
	table th a.inline-edit,
	table td a.inline-edit{
		cursor: default;
		color: #797979 !important;
	}
    </style>
</head>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<body style="height: 100%">



<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->



	
	<div id="">
	 
		
		<div class="tab-content">


			<?php
					$total_ccc = 0;
					$ankdate1 = $ankdate2  = "";
					$ankname1 = $ankname2  = "";
					?>
							<?php
							
							foreach($propertiesExtra1s as $k=>$propertiesExtra1)
							{
								if($propertiesExtra1->type==config('tenancy_schedule.item_type.live_vacancy') || $propertiesExtra1->type==config('tenancy_schedule.item_type.business_vacancy'))
									continue;


								if($propertiesExtra1->is_current_net)
								$total_ccc += $propertiesExtra1->net_rent_p_a; 

								/*if($k==0)
								$ankdate1 = $propertiesExtra1->mv_end;
								if($k==1)
								$ankdate2 = $propertiesExtra1->mv_end;*/

								if($propertiesExtra1->ank_1 == 1 && $ankname1 == ""){
									$ankdate1 = $propertiesExtra1->mv_end;
									$ankname1 = $propertiesExtra1->tenant;
								}

								if($propertiesExtra1->ank_2 == 1 && $ankname2 == ""){
									$ankdate2 = $propertiesExtra1->mv_end;
									$ankname2 = $propertiesExtra1->tenant;
								}
							}

					if(!$ankname1)
					{
						if(isset($propertiesExtra1s[0]['tenant']))
							$ankname1 = $propertiesExtra1s[0]['tenant'];
					}
					if(!$ankdate1)
					{
						if(isset($propertiesExtra1s[0]['mv_end']))
							$ankdate1 = $propertiesExtra1s[0]['mv_end'];
					}
					if(!$ankname2)
					{
						if(isset($propertiesExtra1s[1]['tenant']))
							$ankname2 = $propertiesExtra1s[1]['tenant'];
					}
					if(!$ankdate2)
					{
						if(isset($propertiesExtra1s[1]['mv_end']))
							$ankdate2 = $propertiesExtra1s[1]['mv_end'];
					}


					if($total_ccc){
						$properties->net_rent_pa = $total_ccc;
					}
					else{
						$total_ccc = $properties->net_rent_pa;
					}
					// $total_ccc = 754283;
					$properties->net_rent_increase_year1 = $total_ccc;
					$properties->net_rent_increase_year2 = $properties->net_rent_increase_year1 + $properties->net_rent_increase_year1*$properties->net_rent;
					$properties->net_rent_increase_year3 = $properties->net_rent_increase_year2 + $properties->net_rent_increase_year2*$properties->net_rent;
					$properties->net_rent_increase_year4 = $properties->net_rent_increase_year3 + $properties->net_rent_increase_year3*$properties->net_rent;
					$properties->net_rent_increase_year5 = $properties->net_rent_increase_year4 + $properties->net_rent_increase_year4*$properties->net_rent;


					$properties->operating_cost_increase_year1 = $properties->net_rent_increase_year1*$properties->operating_costs_nk;

					$properties->operating_cost_increase_year2 = $properties->operating_cost_increase_year1 + $properties->operating_cost_increase_year1*$properties->operating_costs;

					// echo $properties->operating_cost_increase_year1;
					// echo "<br>";
					// echo $properties->operating_costs_nk;



					$properties->operating_cost_increase_year3 = $properties->operating_cost_increase_year2 + $properties->operating_cost_increase_year2*$properties->operating_costs;

					$properties->operating_cost_increase_year4 = $properties->operating_cost_increase_year3 + $properties->operating_cost_increase_year3*$properties->operating_costs;
					$properties->operating_cost_increase_year5 = $properties->operating_cost_increase_year4 + $properties->operating_cost_increase_year4*$properties->operating_costs;


					$properties->property_management_increase_year1 = $properties->net_rent_increase_year1*$properties->object_management_nk;

					$properties->property_management_increase_year2 =$properties->property_management_increase_year1 +  $properties->property_management_increase_year1*$properties->object_management;

					$properties->property_management_increase_year3 =$properties->property_management_increase_year2 +  $properties->property_management_increase_year2*$properties->object_management;

					$properties->property_management_increase_year4 =$properties->property_management_increase_year3 +  $properties->property_management_increase_year3*$properties->object_management;

					$properties->property_management_increase_year5 =$properties->property_management_increase_year4 +  $properties->property_management_increase_year4*$properties->object_management;


					$properties->ebitda_year_1 = $total_ccc - $properties->maintenance_increase_year1 - $properties->operating_cost_increase_year1 - $properties->property_management_increase_year1;


					//echo $total_ccc.'/'.$properties->maintenance_increase_year1.'/'.$properties->operating_cost_increase_year1.'/'.$properties->property_management_increase_year1;



					$properties->ebitda_year_2 = $properties->net_rent_increase_year2 - $properties->maintenance_increase_year2 - $properties->operating_cost_increase_year2 - $properties->property_management_increase_year2;
					$properties->ebitda_year_3 = $properties->net_rent_increase_year3 - $properties->maintenance_increase_year3 - $properties->operating_cost_increase_year3 - $properties->property_management_increase_year3;
					$properties->ebitda_year_4 = $properties->net_rent_increase_year4 - $properties->maintenance_increase_year4 - $properties->operating_cost_increase_year4 - $properties->property_management_increase_year4;
					$properties->ebitda_year_5 = $properties->net_rent_increase_year5 - $properties->maintenance_increase_year5 - $properties->operating_cost_increase_year5 - $properties->property_management_increase_year5;

					 $properties->ebit_year_1= $properties->ebitda_year_1 - $properties->depreciation_nk_money;
					 $properties->ebit_year_2= $properties->ebitda_year_2 - $properties->depreciation_nk_money;
					 $properties->ebit_year_3= $properties->ebitda_year_3 - $properties->depreciation_nk_money;
					 $properties->ebit_year_4= $properties->ebitda_year_4 - $properties->depreciation_nk_money;
					 $properties->ebit_year_5= $properties->ebitda_year_5 - $properties->depreciation_nk_money;


					 $pcomments_new = DB::table('property_comments')->where('property_id',$properties->id)->whereNotNull('Ankermieter_note')->where('status',null)->first();




					?>
	 
		{{-- @foreach ($banks as $key => $bank) --}}
            <?php

			$D42 = $properties->gesamt_in_eur 
				+ ($properties->real_estate_taxes * $properties->gesamt_in_eur) 
				+ ($properties->estate_agents * $properties->gesamt_in_eur) 
				+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100) 
				+ ($properties->evaluation * $properties->gesamt_in_eur) 
				+ ($properties->others * $properties->gesamt_in_eur) 
				+ ($properties->buffer * $properties->gesamt_in_eur);
			$D46 = $D42;

			$D47 = $properties->with_real_ek * $D46;	//C47*D46
			$D48 = $properties->from_bond * $D46;	//C48*D46
			
			$D49 = $properties->bank_loan * $D46;

		
			$D50 = $D47 + $D48 +  $D49;
	
			
            if($banks[0] == ''){
                $bank_check = 0;
                $bank = $fake_bank;
            }else{
                $bank_check = 1;
			}

            $H47 = $D49 - $D49 * $properties->eradication_bank;
            $H48 = $D49 * $properties->interest_bank_loan;    //G48*D49
            $H49 = $D49 * $properties->eradication_bank;
            $H50 = $D49 * $properties->interest_bank_loan + $D49*$properties->eradication_bank;
			$H52 = $D48 * $properties->interest_bond;
           
            $I48 = $H47 * $properties->interest_bank_loan;
            $I49 = $H50 - $I48;
            $I47 = $H47 - $I49;
            $I50 = $I48 + $I49;

            $J48 = $I47 * $properties->interest_bank_loan;
            $J49 = $H50 - $J48;
            $J47 = $I47 - $J49;
            $J50 = $J48 + $J49;

            $K48 = $J47 * $properties->interest_bank_loan;
            $K49 = $I50 - $K48;
            $K47 = $J47 - $K49;
            $K50 = $K48 + $K49;

            $L48 = $K47 * $properties->interest_bank_loan;
            $L49 = $J50 - $L48;
            $L47 = $K47 - $L49;
            $L50 = $L48 + $L49;


            $E18 = ($properties->net_rent_increase_year1
			-$properties->maintenance_increase_year1
			-$properties->operating_cost_increase_year1
			-$properties->property_management_increase_year1)
			-$properties->depreciation_nk_money;
			
			$F18 = ($properties->net_rent_increase_year2
			-$properties->maintenance_increase_year2
			-$properties->operating_cost_increase_year2
			-$properties->property_management_increase_year2)
			-$properties->depreciation_nk_money;

			$G18 = ($properties->net_rent_increase_year3
			-$properties->maintenance_increase_year3
			-$properties->operating_cost_increase_year3
			-$properties->property_management_increase_year3)
			-$properties->depreciation_nk_money;

			$H18 = 
			($properties->net_rent_increase_year4
			-$properties->maintenance_increase_year4
			-$properties->operating_cost_increase_year4
			-$properties->property_management_increase_year4)
			-$properties->depreciation_nk_money;

			$I18 = ($properties->net_rent_increase_year5
			-$properties->maintenance_increase_year5
			-$properties->operating_cost_increase_year5
			-$properties->property_management_increase_year5)
			-$properties->depreciation_nk_money;

			$E23 = $E18- $H48 -$H52;
			$F23 = $F18-($H52)-($I48);
			$G23 = $G18-($H52)-($J48);
			$H23 = $H18 - ($H52)-($K48);
			$I23 = $I18-($H52)-($L48);
			$E31 = ($E23 - ($properties->tax * $E23)) - $H49 + $properties->depreciation_nk_money;
			
			$I16= $properties->depreciation_nk_money;
			$I27 =$I23 - ($properties->tax * $I23);
			$I29 = $L49;					
			
			
			$I31 = $I27-$I29+$I16;

			$G58 = ($D42 == 0) ? 0: $properties->net_rent_pa/$D42;
			$G59 = ($properties->net_rent_pa == 0) ? 0 : $D42/ $properties->net_rent_pa;
			$E25 = $properties->tax * $E23;
			$G61 = ($D48 == 0) ? 0 : $E31/$D48;
			$G62 = ($D48 == 0) ? 0 : $E23/$D48;
			
			

			$L14 = ($properties->rent_retail + $properties->rent_whg == 0) ? 0 : ($properties->net_rent_pa / ($properties->rent_retail + $properties->rent_whg) / 12);
			$L16 = ($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg == 0) ? 0 : 
			($properties->gesamt_in_eur)/($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg);
			$L20 = ($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg == 0) ? 0 : 
			($properties->rent_retail + $properties->rent_whg)/($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg);
			
			$L25 = ($properties->net_rent_pa == 0) ? 0 : $properties->gesamt_in_eur / $properties->net_rent_pa;

			
			$L11 = $properties->plot_of_land_m2;
			$P31 = $properties->ground_reference_value_in_euro_m2;
			$P43 = $L11* $P31;
			$D59 = $properties->gesamt_in_eur*($properties->maintenance_nk);
			$D60 = $properties->operating_cost_increase_year1;


			if($properties->Ist && $D42)
			{

				$row_11 = \App\Verkauf_tab::where('row_name','row_12')->where('property_id',$properties->main_property_id)->first();
				if(!$row_11)
				{
					$row_11 = new \App\Verkauf_tab;
					$row_11->property_id = $properties->main_property_id;
					$row_11->tab_value = $D42;
					$row_11->row_name = 'row_12';
					$row_11->save();
				}
			}

			// echo $D42; 
			
		   ?>

		@if(isset($_GET['bank_id']))
			<div id="bank-{{$banks[0]->id}}" class="bank-container tab-pane fade {{($banks[0]->id == $_GET['bank_id'])?'in active':''}} " data-bank-id="{{$banks[0]->id}}">  
		@endif
			 
		<input type="hidden" id="path-properties-show" value="{{ url('banksIframe')}}/{{ $id }}/{{ $banks[0]->id }}/Ist">

		<?php
            $bank_name = "";
            foreach ($bank_all as $bnk) {
                if($properties->properties_bank_id==$bnk->id)
                    $bank_name = $bnk->name;
            }
            ?>
            <button style="margin-left: 25px;" class="btn btn-primary" onclick="openFullscreen();">Fullscreen</button>

            <h3 style="padding-left: 25px;">Finanzierung:
			<!-- <a href="#" class="bank-remote" data-type="select2" data-pk="properties_bank_id" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-placement="bottom" data-title="">{{$bank_name}}</a> -->
			{{$bank_name}}
			</h3>

			<div class="clearfix"></div>
				<script>
				var elem = document.documentElement;
				function openFullscreen() {
				  if (elem.requestFullscreen) {
				    elem.requestFullscreen();
				  } else if (elem.mozRequestFullScreen) { /* Firefox */
				    elem.mozRequestFullScreen();
				  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
				    elem.webkitRequestFullscreen();
				  } else if (elem.msRequestFullscreen) { /* IE/Edge */
				    elem.msRequestFullscreen();
				  }
				}

				function closeFullscreen() {
				  if (document.exitFullscreen) {
				    document.exitFullscreen();
				  } else if (document.mozCancelFullScreen) {
				    document.mozCancelFullScreen();
				  } else if (document.webkitExitFullscreen) {
				    document.webkitExitFullscreen();
				  } else if (document.msExitFullscreen) {
				    document.msExitFullscreen();
				  }
				}
				</script>
				<script>

					$(function(){
						$(".wmd-view-topscroll").scroll(function(){
							$(".wmd-view")
									.scrollLeft($(".wmd-view-topscroll").scrollLeft());
						});
						$(".wmd-view").scroll(function(){
							$(".wmd-view-topscroll")
									.scrollLeft($(".wmd-view").scrollLeft());
						});
					});

				</script>

				<div style="height: 20px; width: auto;overflow-x: scroll;overflow-y: hidden; padding-left: 60%" class="wmd-view-topscroll">
					<div style="width: 1000px;height:20px; overflow-x: scroll;overflow-y: hidden;" class="scroll-div1"></div>
				</div>
				<div id="scroll2" style=" padding: 6px; width: auto; height: 2500px;overflow-x: scroll;overflow-y: scroll; " class="wmd-view property-details white-box  kalkulation">
					<table class="property-table pr-zoom-in-out-table">
						<thead>
						<tr class="border">
							<th class="bg-brown border-right" colspan="2">Kalkulations- und Planungsspiegel</th>
							<th class="color-red border-no-right">Objekt</th>
							<th colspan="2" class="border-no-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="name_of_property" data-placement="bottom" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.name_of_property')}}">{{ $properties->name_of_property }}</a></th>
							<th class="color-red border-no-right">{{__('property.date_of_last_update')}}</th>
							<th class="text-right border-no-right">{{date('d.m.Y', strtotime($properties->updated_at))}}</th>
							<th class="color-red border-no-right">{{__('property.date_of_creation')}}</th>
							<th class="text-right border-no-right">{{date('d.m.Y', strtotime($properties->created_at))}}</th>
							<th class="color-red border-no-right">{{__('property.creator_username')}}</th>
							<th class="border">{{$properties->user_name}}</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<th class="border text-center">Adresse</th>

						<!--<th class="border">PLZ</th>
							<td class="border text-center bg-yellow">
								<a href="#" class="inline-edit" data-type="text" data-pk="plz_ort" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="PLZ">{{ $properties->plz_ort }}</a></td>
							<th class="border">Ort</th>
							<td class="border text-center bg-yellow">
								<a href="#" class="inline-edit" data-type="text" data-pk="ort" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Ort">{{ $properties->ort }}</a>
							</td>
							<th class="border">Strasse</th>
							<td class="border text-center bg-yellow">
								<a href="#" class="inline-edit" data-type="text" data-pk="strasse" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Strasse">{{ $properties->strasse }}</a>
							</td>-->
							<th class="border">PLZ</th>
							<td class="border text-center bg-yellow">
								<a href="#" class="inline-edit" data-type="text" data-pk="plz_ort" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="PLZ">{{ $main_properties->plz_ort }}</a></td>
							<th class="border">Ort
								<!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#add-city" class="add_ort">Add Ort</a> -->
							</th>
							<td class="border text-center bg-yellow">
								<a href="#" class="city-remote" data-type="select2" data-pk="ort" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="Ort">{{ $main_properties->ort }}</a>
							</td>
							<th class="border">Strasse</th>
							<td class="border text-center bg-yellow">
								<a href="#" class="inline-edit" data-type="text" data-pk="strasse" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="Strasse">{{ $main_properties->strasse }}</a>
							</td>
							<th class="border">Hausnummer</th>
							<td class="border text-center bg-yellow">
								<a href="#" class="inline-edit" data-type="text" data-pk="hausnummer" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="Hausnummer">{{ $main_properties->hausnummer }}</a>
							</td>
							<th class="border">Bundesland</th>
							<td class="border text-center bg-yellow">
								<a href="#" class="inline-edit-niedersachsen" data-type="select" data-pk="niedersachsen" data-url="{{url('property/update/schl/'.$main_properties->id) }}" data-title="Bundesland">{{ $main_properties->niedersachsen }}</a>
							</td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<td colspan="3"></td>
							<th class="border-no-right bg-gray text-center">Jahr 1</th>
							<th class="border-no-right bg-gray text-center">Jahr 2</th>
							<th class="border-no-right bg-gray text-center">Jahr 3</th>
							<th class="border-no-right bg-gray text-center">Jahr 4</th>
							<th class="border bg-gray text-center">Jahr 5</th>
							<td></td>
							<th colspan="2" class="bg-yellow">Gelbe Felder = Eingabefelder</th>




							<!-- <th colspan="2" class="border text-center">Bemerkungen</th> -->
							<td></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>



							<td class="">&nbsp;</td>
							<td class="">MV Ende</td>
							<td>Optionen</td>
							<td colspan="3">

							</td>
							<td>&nbsp;</td>
						</tr>


						<tr>
							<th class="border-no-right bg-gray">Netto Miete (IST) p.a.</th>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.net_rent')}} (%)">{{number_format($properties->net_rent*100,2,",",".")}}</a><span>%</span></td>
							<th class="border-no-right bg-gray">Steigerung p.a.</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year1,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year2,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year3,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->net_rent_increase_year4,2,",",".")}}</th>
							<th class="border bg-gray text-right">{{number_format($properties->net_rent_increase_year5,2,",",".")}}</td>
							<td></td>
							<td>Ankermieter 1</td>
							<td class="text-right">
								{{ $ankname1 }}
							{{-- @if(isset($propertiesExtra1s[0]['tenant']))
								{{$propertiesExtra1s[0]['tenant']}}
							@endif --}}
							<!-- <a href="#" class="inline-edit" data-type="text" data-pk="ankermieter4" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Ankermieter">{{ $properties->ankermieter4 }}</a> -->
							</td>
							<td>{{ ($ankdate1 != '') ? show_date_format(date('Y-m-d', strtotime(str_replace("/", "-", $ankdate1)))) : '' }}</td>
							<td >

								<a href="#" class="inline-edit" data-type="text" data-pk="ankermieter_option1" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Optionen">{{ $properties->ankermieter_option1 }}</a>
							</td>


							<td  >

							<!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Mieter_comment" data-pk="Mieter_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mieter_comment'] }}@endif</a> -->

							</td>
							{{-- 	<td colspan="4" class="border">
									<a href="#" class="inline-edit" data-type="textarea" data-name="Mieter_note" data-pk="Mieter_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mieter_note'] }}@endif
								</a>
								</td> --}}
							<td>&nbsp;</td>
							{{--<td>{{number_format($properties->anchor_tenants,2)}}</td>--}}
						</tr>

						<tr>
							<td class="border-no-right">Netto Miete (Soll) Leerst.</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="net_rent_empty" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.net_rent_empty')}}(%)">{{number_format($properties->net_rent_empty*100,2,",",".")}}</a>%</td>
							{{-- <td class="border-no-right bg-yellow text-right"><a href="#" data-toggle="modal" data-target="#myModal" > Mietflächen</a></td>--}}
							<td class="border-no-right">Steigerung p.a.</td>
							<td class="border-no-right text-right">{{number_format(0,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format(0,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format(0,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format(0,2,",",".")}}</td>
							<td class="border text-right">{{number_format(0,2,",",".")}}</td>
							<td></td>
							<td>Ankermieter 2</td>
							<td class="text-right">
							<!-- <a href="#" class="inline-edit" data-type="text" data-pk="mieter" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Mieter">{{ $properties->mieter }}</a> -->
								{{-- @if(isset($propertiesExtra1s[1]['tenant']))
									{{$propertiesExtra1s[1]['tenant']}}
								@endif --}}
								{{ $ankname2 }}
							</td>
							<td>{{ ($ankdate2 != '') ? show_date_format(date('Y-m-d', strtotime(str_replace("/", "-", $ankdate2)))) : '' }}</td>
							<td class="">

								<a href="#" class="inline-edit" data-type="text" data-pk="ankermieter_option2" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Optionen">{{ $properties->ankermieter_option2 }}</a>
							</td>

							<td>&nbsp;</td>

							{{-- 	<td colspan="4" class="border">
									<a href="#" class="inline-edit" data-type="textarea" data-name="Mietflache_note" data-pk="Mietflache_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Mietflache_note'] }}@endif</a>
								</td> --}}
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td colspan="2" >
							<!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Grundstuck_comment" data-pk="Grundstuck_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Grundstuck_comment'] }}@endif</a> -->

							</td>
							{{-- 	<td colspan="4" class="border">
									<a href="#" class="inline-edit" data-type="textarea" data-name="Grundstuck_note" data-pk="Grundstuck_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Grundstuck_note'] }}@endif</a>
								</td> --}}
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<td class="border-no-right">Instandhaltung nichtumlfähig</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.maintenance')}} (%)">{{number_format($properties->maintenance*100,2,",",".")}}</a>%</td>
							<td class="border-no-right">Steigerung p.a.</td>
							<td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year1,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year2,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year3,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->maintenance_increase_year4,2,",",".")}}</td>
							<td class="border text-right">{{number_format($properties->maintenance_increase_year5,2,",",".")}}</td>
							<td></td>
							<td>Mietfläche in m²</td>
							<td class="text-right">
							<!-- {{number_format($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg, 2,",",".")}} -->
							<!-- @foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
								{{number_format($tenancy_schedule->calculations['total_rental_space'], 2,",",".")}}
							@endforeach -->
								{{number_format($properties->vacancy_retail+$properties->rent_retail+$properties->vacancy_whg+$properties->rent_whg,2,",",".")}}





							</td>
							<td>&nbsp;</td>
							<td colspan="2" >
							<!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Baujahr_comment" data-pk="Baujahr_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Baujahr_comment'] }}@endif</a> -->

							</td>
							{{-- <td colspan="4" class="border">
								<a href="#" class="inline-edit" data-type="textarea" data-name="Baujahr_note" data-pk="Baujahr_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Baujahr_note'] }}@endif</a>
							</td> --}}


						</tr>

						<tr>
							<td class="border-no-right">Betriebsk. nicht umlfähig</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="operating_costs" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.operating_costs')}} (%)">{{number_format($properties->operating_costs*100,2,",",".")}}</a>%</td>
							<td class="border-no-right">Steigerung p.a.</td>
							<td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year1,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year2,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year3,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->operating_cost_increase_year4,2,",",".")}}</td>
							<td class="border text-right">{{number_format($properties->operating_cost_increase_year5,2,",",".")}}</td>
							<td></td>
							<td>Kaufpreis p. m² MF</td>
							<td class="text-right">{{number_format($L16,2,",",".")}}&nbsp;€</td>

							<td>&nbsp;</td>

							<td colspan="2" >
							<!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Miete_comment" data-pk="Miete_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Miete_comment'] }}@endif</a> -->

							</td>
							{{-- <td colspan="4" class="border">
								<a href="#" class="inline-edit" data-type="textarea" data-name="Miete_note" data-pk="Miete_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Miete_note'] }}@endif</a>
							</td> --}}
							<td>&nbsp;</td>
						</tr>

						<tr>
							<td class="border-no-right">Objektverwalt. nichtumlfähig</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="object_management" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.object_management')}} (%)">{{number_format($properties->object_management*100,2,",",".")}}</a>%</td>
							<td class="border-no-right">Steigerung p.a.</td>
							<td class="border-no-right text-right">{{number_format($properties->property_management_increase_year1,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->property_management_increase_year2,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->property_management_increase_year3,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->property_management_increase_year4,2,",",".")}}</td>
							<td class="border text-right">{{number_format($properties->property_management_increase_year5,2,",",".")}}</td>
							<td></td>
							<td>Miete (€/m²)</td>
							<td class="text-right">
								{{number_format($L14,2,",",".")}}&nbsp;€
							</td>
							<td><a href="#" class="inline-edit" data-type="text" data-pk="miete_text" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="">{{$properties->miete_text}}</a></td>
							<td colspan="2" >
							<!-- <a href="#" class="inline-edit" data-type="textarea" data-name="KP_comment" data-pk="KP_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['KP_comment'] }}@endif</a> -->

							</td>
							{{-- <td colspan="4" class="border">
								<a href="#" class="inline-edit" data-type="textarea" data-name="KP_note" data-pk="KP_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['KP_note'] }}@endif</a>
							</td> --}}

						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td colspan="2" >
							<!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Stellplatze_comment" data-pk="Stellplatze_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Stellplatze_comment'] }}@endif</a> -->
							</td>

							{{-- <td colspan="4" class="border">
								<a href="#" class="inline-edit" data-type="textarea" data-name="Stellplatze_note" data-pk="Stellplatze_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Stellplatze_note'] }}@endif</a>
							</td> --}}
							<td>&nbsp;</td></tr>

						<tr>
							<th colspan="3" class="border-no-right bg-gray">EBITDA</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_1,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_2,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_3,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->ebitda_year_4,2,",",".")}}</th>
							<th class="border bg-gray text-right">{{number_format($properties->ebitda_year_5,2,",",".")}}</th>
							<td></td>
							<td>Baujahr</td>
							<td class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="construction_year" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.construction_year')}}">{{$properties->construction_year}}</a></td>

							<td class=""><a href="#" class="inline-edit" data-type="text" data-pk="construction_year_note" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.construction_year')}}">{{$properties->construction_year_note}}</a></td>

							<td colspan="2" >
							<!-- <a href="#" class="inline-edit" data-type="textarea" data-name="Vermietungsstand_comment" data-pk="Vermietungsstand_comment" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Vermietungsstand_comment'] }}@endif</a> -->
							</td>
							{{-- <td colspan="4" class="border">
								<a href="#" class="inline-edit" data-type="textarea" data-name="Vermietungsstand_note" data-pk="Vermietungsstand_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Vermietungsstand_note'] }}@endif</a>
							</td> --}}
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>


							{{-- <td colspan="4" class="border">
								<a href="#" class="inline-edit" data-type="textarea" data-name="Maklerpreis_note" data-pk="Maklerpreis_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if(isset($comments_new) && count($comments_new)>0){{ $comments_new[0]['Maklerpreis_note'] }}@endif</a>
							</td> --}}
							<td>&nbsp;</td></tr>

						<tr>
							<td colspan="3" class="border-no-right">Abschreibung</td>
							<td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
							<td class="border text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
							<td></td>
							<td>Grundstück in m²</td>
							<td class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="plot_of_land_m2" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.plot_of_land_m2')}}">{{number_format($properties->plot_of_land_m2,2,",",".")}}</a></td>

							<td><a href="#" class="inline-edit" data-type="text" data-pk="plot_of_land_m2_text" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="">{{$properties->plot_of_land_m2_text}}</a></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>


							<td>&nbsp;</td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>



						<tr>
							<th colspan="3" class="border-no-right bg-gray">EBIT</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_1,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_2,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_3,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($properties->ebit_year_4,2,",",".")}}</th>
							<th class="border bg-gray text-right">{{number_format($properties->ebit_year_5,2,",",".")}}</th>
							<td></td>
							<td>Bodenrichtwert in €/m²</td>
							<td class="text-right bg-yellow">
								<a href="#" class="inline-edit" data-type="text" data-pk="ground_reference_value_in_euro_m2" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.ground_reference_value_in_euro_m2')}}">{{number_format($properties->ground_reference_value_in_euro_m2,2,",",".")}}</a>
								€</td>

							<td>&nbsp;</td>

							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<td colspan="3" class="border-no-right">Zins Bankkredit</td>
							<td class="border-no-right text-right">{{number_format($H48,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($I48,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($J48,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($K48,2,",",".")}}</td>
							<td class="border text-right">{{number_format($L48,2,",",".")}}</td>
							<td></td>
							<td>Grundstückswert</td>
							<td class="text-right">{{number_format($P43,2,",",".")}} €</td>

						<!-- <td>Vermietungsstand</td>
							<td class="text-right">
								{{number_format($L20 * 100, 1,",",".")}}%
							</td> -->
							<td>&nbsp;</td>


							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr>
							<th colspan="3" class="border-no-right">Eigenmittelverzinsung</th>
							<td class="border-no-right text-right">{{number_format($H52,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($H52,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($H52,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($H52,2,",",".")}}</td>
							<td class="border text-right">{{number_format($H52,2,",",".")}}</td>
							<td></td>
							<td >Stellplätze</td>
							<td class="text-right bg-yellow">

								<a href="#" class="inline-edit" data-type="text" data-pk="plot" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Stellplätze">{{number_format($properties->plots,0,",",".")}}</a>
							</td>

							<td>&nbsp;</td>

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<th colspan="3" class="border-no-right bg-gray">EBT</th>
							<th class="border-no-right text-right bg-gray">{{number_format($E18
							- $H48
							-$H52,2,",",".")}}</th>
							<th class="border-no-right text-right bg-gray">{{number_format($F18
								-$H52
								-$I48,2,",",".")}}</th>
							<th class="border-no-right text-right bg-gray">{{number_format($G18
								-$H52-
								$J48,2,",",".")}}</th>
							<th class="border-no-right text-right bg-gray">{{number_format($H18
								-$H52
								-$K48,2,",",".")}}</th>
							<th class="border text-right bg-gray">{{number_format($I18
								-$H52
								-$L48,2,",",".")}}</th>
							<td>&nbsp;</td>
							<td>Einwohner</td>
							<td class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="population_edited" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.population')}}">
									<?php
									if($properties->population_edited)
										$properties->population = $properties->population_edited;
									?>
									{{number_format($properties->population,2,",",".")}}
								</a></td>


							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<td class="border-no-right">Steuern</td>
							<td class="border-no-right text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="tax" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.Tax')}} (%)">{{number_format($properties->tax*100,2,",",".")}}</a>%</td>
							<td class="border-no-right"></td>
							<td class="border-no-right text-right">{{number_format($E25,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->tax * $F23,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->tax * $G23,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->tax * $H23,2,",",".")}}</td>
							<td class="border text-right">{{number_format($properties->tax * $I23,2,",",".")}}</td>
							<td></td>
							<td>Kaufkraftindex</td>
							<td class="text-right">
								<a href="#" class="inline-edit" data-type="text" data-pk="kk_idx" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Kaufkraftindex">
									{{$kk_idx}}
								</a>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<th colspan="3" class="border-no-right bg-gray">EAT</th>
							<th class="border-no-right text-right bg-gray">{{number_format($E23 - ($properties->tax * $E23),2,",",".")}}</th>
							<th class="border-no-right text-right bg-gray">{{number_format($F23 - ($properties->tax * $F23),2,",",".")}}</th>
							<th class="border-no-right text-right bg-gray">{{number_format($G23 - ($properties->tax * $G23),2,",",".")}}</th>
							<th class="border-no-right text-right bg-gray">{{number_format($H23 - ($properties->tax * $H23),2,",",".")}}</th>
							<th class="border text-right bg-gray">{{number_format($I23 - ($properties->tax * $I23),2,",",".")}}</th>
							<td></td>
							<td>Maklerpreis</td>
							<td class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="maklerpreis" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.maklerpreis')}}">{{number_format($properties->maklerpreis,2,",",".")}}</a></td>


							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

						</tr>

						{{--TODO: start from here--}}
						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<td colspan="3" class="border-no-right">Tilgung Bank</td>
							<td class="border-no-right text-right">{{number_format($H49,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($I49,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($J49,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($K49,2,",",".")}}</td>
							<td class="border text-right">{{number_format($L49,2,",",".")}}</td>
							<td></td>
							<td>Verkaufspreis</td>
							<td class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="preisverk" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Verkehrswert">{{number_format($properties->preisverk,2,",",".")}}</a></td>


							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<th colspan="3" class="border-no-right bg-gray">Cashflow (nach Steuern)</th>
							<th class="border-no-right text-right bg-gray">{{number_format(($E23 - ($properties->tax * $E23)) - $H49 + $properties->depreciation_nk_money,2,",",".")}} </th>
							<th class="border-no-right text-right bg-gray">{{number_format(($F23 - ($properties->tax * $F23)) - $I49 + $properties->depreciation_nk_money,2,",",".")}}</th>
							<th class="border-no-right text-right bg-gray">{{number_format(($G23 - ($properties->tax * $G23)) - $J49 + $properties->depreciation_nk_money,2,",",".")}}</th>
							<th class="border-no-right text-right bg-gray">{{number_format(($H23 - ($properties->tax * $H23)) - $K49 + $properties->depreciation_nk_money,2,",",".")}}</th>
							<th class="border text-right bg-gray">{{number_format($I31 ,2,",",".")}}</th>
							<td></td>
							<td>Exklusivität bis</td>
							<td class="bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-format="yyyy-mm-dd" data-pk="exklusivität_bis" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Exklusivität bis">{{show_date_format($properties->exklusivität_bis)}}</a></td>
						<!-- <th class="border text-center">{{__('property.field.address')}}</th>
							<td class="border text-center"><a href="#" class="inline-edit" data-type="text" data-pk="address" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.address')}}">{{$properties->address}}</a></td> -->





						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

							<td>&nbsp;</td>
						</tr>

						<tr>
							<th colspan="11" class="border bg-brown">1.) Erwerb- und Erwerbsnebenkosten</th>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<th class="text-center"></th>
							<td>&nbsp;</td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td class=" text-center"></td>
							<td>&nbsp;</td></tr>

						<tr>
							<th class="border-no-right bg-gray">Kaufpreis</th>
							<th class="border-no-right bg-gray text-center">Anteil</th>
							<th class="border bg-gray text-center">in EUR</th>
							<td></td>
							<th class="border-no-right bg-gray">Nebenkosten</th>
							<th class="border-no-right bg-gray text-center">Anteil</th>
							<th class="border bg-gray text-center">in EUR</th>
							<td></td>
							<th class="border-no-right bg-gray">Objektinfos</th>
							<th class="border-no-right bg-gray text-center">Gewerbe Fläche</th>

							<th class="border bg-gray text-center"><a href="#" class="inline-edit" data-type="text" data-pk="sonstige_flache" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="">{{$properties->sonstige_flache}}</a> Fläche</th>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

						</tr>

						<tr>
							<td class="border-no-right">Gebäude</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="building" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.building')}} (%)">{{number_format($properties->building*100,2,",",".")}}</a>%</td>
							<td class="border text-right">
								{{ number_format ( $properties->building * $properties->gesamt_in_eur, 2,",","." ) }}
							</td>
							<td></td>
							<td class="border-no-right">GrunderwerbsSt.</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="real_estate_taxes" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.real_estate_taxes')}} (%)">{{number_format($properties->real_estate_taxes*100,2,",",".")}}</a>%</td>
							<td class="border text-right">
								{{number_format($properties->real_estate_taxes * $properties->gesamt_in_eur,2,",",".")}}
							</td>
							<td></td>
							<td class="border-no-right">Vermietet</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="rent_retail" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.rent_retail')}}">{{number_format($properties->rent_retail,2,",",".")}}</a></td>

							<td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="rent_whg" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.rent_whg')}}">{{number_format($properties->rent_whg,2,",",".")}}</a></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr>
							<td class="border-no-right">Grundstück</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="plot_of_land" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.plot_of_land')}} (%)">{{number_format($properties->plot_of_land*100,2,",",".")}}</a>%</td>
							<td class="border text-right">
								{{ number_format ( $properties->plot_of_land * $properties->gesamt_in_eur, 2 ,",",".") }}
							</td>
							<td></td>
							<td class="border-no-right">Makler</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="estate_agents" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.estate_agents')}} (%)">{{number_format($properties->estate_agents*100,2,",",".")}}</a>%</td>
							<td class="border text-right">
								{{number_format($properties->estate_agents * $properties->gesamt_in_eur,2,",",".")}}
							</td>
							<td></td>
							<td class="border-no-right">Leerstand</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="vacancy_retail" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.vacancy_retail')}}">{{number_format($properties->vacancy_retail,2,",",".")}}</a></td>

							<td class="border bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="vacancy_whg" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.vacancy_whg')}}">{{number_format($properties->vacancy_whg,2,",",".")}}</a></td>

						</tr>
						<tr>
							<th class="border-no-right">Gesamt</th>
							<td class="border bg-gray text-right"></td>
							<th class="border bg-gray text-right bg-yellow">
								<a href="#" class="inline-edit" data-type="text" data-pk="gesamt_in_eur" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}">{{number_format($properties->gesamt_in_eur,2,",",".")}}</a>
							</th>
							<td></td>
							<td class="border-no-right">Notar/Grundbuch</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="Grundbuch" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.Grundbuch')}} (%)">{{number_format($properties->Grundbuch,2,",",".")}}</a>%</td>
							<td class="border text-right">
								{{number_format(($properties->Grundbuch * $properties->gesamt_in_eur)/100,2,",",".")}}
							</td>		<!--G38*D38-->
							<td>&nbsp;</td>
							<th class="border">Gesamt</th>
							<th class="border text-right">
								{{number_format($properties->vacancy_retail+$properties->rent_retail,2,",",".")}}

							</th>
							<th class="border text-right">{{number_format($properties->vacancy_whg+$properties->rent_whg,2,",",".")}}
							</th>
							<td colspan=""></td>

						</tr>


						<tr>
							<td colspan="3"></td>
							<td></td>
							<td class="border-no-right">Due Dilligence</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="evaluation" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.evaluation')}} (%)">{{number_format($properties->evaluation*100,2,",",".")}}</a>%</td>
							<td class="border text-right">
								{{number_format($properties->evaluation * $properties->gesamt_in_eur,2,",",".")}}
							</td>
							<td colspan="4"></td>
						</tr>


						<tr>
							<td class="border-no-right">Gesamtkaufpreis</td>
							<td class="border-no-right">Netto KP</td>
							<td class="border text-right">
								{{number_format($properties->gesamt_in_eur,2,",",".")}}
							</td>
							<td></td>
							<td class="border-no-right">
								<a href="#" class="inline-edit" data-type="text" data-pk="miscellaneous_title" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.others')}}">
									@if($properties->miscellaneous_title)
										{{$properties->miscellaneous_title}}
									@else
										{{'Sonstiges'}}
									@endif
								</a>
							</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="others" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.others')}} (%)">{{number_format($properties->others*100,2,",",".")}}</a>%</td>
							<td class="border text-right">
								{{number_format($properties->others * $properties->gesamt_in_eur,2,",",".")}}
							</td>
							<td></td>
							<?php
							//declare values
							$Q53 = 0;
							?>
							@foreach($propertiesExtra1s as $propertiesExtra1)
								<?php

								if($propertiesExtra1->type==config('tenancy_schedule.item_type.live_vacancy') || $propertiesExtra1->type==config('tenancy_schedule.item_type.business_vacancy'))
									continue;

								if( strpos($properties->duration_from_O38, "unbefristet") !== false){
										$value = 1;
									}else{
										$properties->duration_from_O38 = $propertiesExtra1->mv_end2;

										$date1=date_create(str_replace('/', '-', $propertiesExtra1->mv_end));
										$date2=date_create(str_replace('/', '-', $propertiesExtra1->mv_end2));

										$value = 0;
										// if($date1 && $date2){
											$diff=date_diff($date1,$date2);
											$value =  $diff->format("%a")/ 365;
										// }

										// $value = ((strtotime(str_replace('/', '-', $propertiesExtra1->mv_end)) -  strtotime(str_replace('/', '-', $properties->duration_from_O38))) / 86400) / 365;
									}

									if (strpos($propertiesExtra1->mv_end, '2099') !== false)
										$value = 0.5;

									$Q53 += $value*$propertiesExtra1->net_rent_p_a;

								?>
							@endforeach
							<?php
							$P53 = ($properties->net_rent_pa == 0) ? 0 : ($Q53 / $properties->net_rent_pa) ;

							?>

							<td class="border-no-right">WAULT</td>
							<td colspan="2" class="border  text-center wault-amount">{{number_format($P53,2,",",".")}}</td>

						</tr>

						<tr>
							<td class="border-no-right"></td>
							<td class="border-no-right">AHK</td>
							<td class="border text-right">
								{{
									number_format( ($properties->real_estate_taxes * $properties->gesamt_in_eur)
									+ ($properties->estate_agents * $properties->gesamt_in_eur)
									+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
									+ ($properties->evaluation * $properties->gesamt_in_eur)
									+ ($properties->others * $properties->gesamt_in_eur)
									+ ($properties->buffer * $properties->gesamt_in_eur) , 2 ,",",".")
								}}
							</td>
							<td></td>
							<td class="border-no-right">
								<a href="#" class="inline-edit" data-type="text" data-pk="buffer_title" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.buffer')}}">
									@if($properties->buffer_title)
										{{$properties->buffer_title}}
									@else
										{{'Puffer'}}
									@endif
								</a>
							</td>
							<td class="border-no-right bg-yellow text-right"><a href="#" class="inline-edit" data-type="text" data-pk="buffer" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.buffer')}} (%)">{{number_format($properties->buffer*100,2,",",".")}}</a>%</td>
							<td class="border text-right">
								{{number_format($properties->buffer * $properties->gesamt_in_eur,2,",",".")}}
							</td>
							<td></td>
							<td class="border-no-right">Leerstandsquote</td>
							<td colspan="2" class="border  text-center">{{number_format(100-($L20 * 100), 2,",",".")}}%</td>
						</tr>

						<tr>
							<th colspan="2" class="border-no-right bg-gray">Gesamtkaufpreis</th>

							<th class="border bg-gray text-right">
								{{
									number_format( $D42 , 2 ,",",".")
								}}
							</th>
							<td></td>
							<th class="border-no-right bg-gray">Gesamt</th>
							<th class="border-no-right bg-gray text-right">{{number_format(($properties->real_estate_taxes+$properties->estate_agents+$properties->Grundbuch/100+$properties->evaluation+$properties->others+$properties->buffer)*100,2,",",".")}}%</th>
							<th class="border bg-gray text-right">
								{{
									number_format( ($properties->real_estate_taxes * $properties->gesamt_in_eur)
									+ ($properties->estate_agents * $properties->gesamt_in_eur)
									+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
									+ ($properties->evaluation * $properties->gesamt_in_eur)
									+ ($properties->others * $properties->gesamt_in_eur)
									+ ($properties->buffer * $properties->gesamt_in_eur) , 2 ,",",".")
								}}
							</th>
							<td></td>
							<td class="border-no-right">Vermietungsstand</td>
							<td colspan="2" class="border text-center">
								{{number_format($L20 * 100, 2,",",".")}}%
							<!-- {{number_format((100-$properties->Leerstandsquote),1,",",".")}} -->
							</td>

						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

						</tr>

						<tr>
							<th colspan="11" class="border bg-brown">2.) Finanzierungsstruktur</th>

						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<th class="border-no-right bg-gray">Gesamtkaufpreis</th>
							<th class="border-no-right bg-gray"></th>
							<th class="border bg-gray text-right" id="p_total_purchase_price">
								{{
									number_format( $D42 , 2 ,",",".")
								}}
							</th>
							<td></td>
							<th colspan="2" class="border-no-right bg-gray">Finanz.Kosten Tilgung (p.a.)</th>
							<th class="border-no-right bg-gray">End of year 1</th>
							<th class="border-no-right bg-gray">End of year 2</th>
							<th class="border-no-right bg-gray">End of year 3</th>
							<th class="border-no-right bg-gray">End of year 4</th>
							<th class="border bg-gray">End of year 5</th>

						</tr>

						<tr>
							<!-- <td class="border-no-right">mit echtem EK</td>
							<td class="border-no-right bg-yellow text-right">
								<?php if($properties->id != NULL){?>
								<a href="#" class="inline-edit" data-type="text" data-pk="with_real_ek" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.with_real_ek')}}">{{number_format($properties->with_real_ek*100,2,",",".")}}</a>%
								<?php }else{?>
								{{number_format($properties->with_real_ek*100,2,",",".")}}%
								<?php }?>
							</td>
							<td class="border text-right">
								{{number_format($D47,2,",",".")}}
							</td>-->
							<th class="border-no-right">Eigenmittel</th>
							<td class="border-no-right bg-yellow text-right" style="width: 100%;">
								{{ number_format($properties->from_bond*100,2,",",".") }}
								{{-- <input type="text" name="from_bond" class="p-mask-input-number p_purchase_price" id="p_from_bond_per" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" value="{{ number_format($properties->from_bond*100,2,",",".") }}" style="width: 88%;float: left;text-align: right;">
								<span style="float: right;margin-top: 3px;">%</span> --}}
							</td>
							<td class="border text-right" readonly>
								{{-- <input type="text" name="from_bond_amount" class="p-mask-input-number p_purchase_price" id="p_from_bond_amount" value="{{ number_format($D48,2,",",".") }}" style="text-align: right;"> --}}
								{{ number_format($D48,2,",",".") }}
							</td>
							<td></td>
							<td colspan="2" class="border-no-right">Valuta</td>
							<td class="border-no-right text-right">{{number_format($H47,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($I47,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($J47,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($K47,2,",",".")}}</td>
							<td class="border text-right">{{number_format($L47,2,",",".")}}</td>

						</tr>

						<tr>
							<td class="border-no-right">Bankkredit</td>
							<td class="border-no-right bg-yellow text-right" style="width: 100%;">
								{{ number_format($properties->bank_loan*100,2,",",".") }}
								{{-- <input type="text" name="bank_loan" class="p-mask-input-number p_purchase_price" id="p_bank_loan_per" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" value="{{ number_format($properties->bank_loan*100,2,",",".") }}" style="width: 88%;float: left;text-align: right;">
								<span style="float: right;margin-top: 3px;">%</span> --}}

							</td>
							<td class="border text-right">
								{{ number_format($D49,2,",",".") }}
								{{-- <input type="text" name="bank_loan_amount" class="p-mask-input-number p_purchase_price" id="p_bank_loan_amount" value="{{ number_format($D49,2,",",".") }}" style="text-align: right;"> --}}
							</td>
							
							<td></td>
							<td class="border-no-right">Zins Bankkredit</td>
							<td class="border-no-right bg-yellow text-right">
								<a href="#" class="inline-edit" data-type="text" data-pk="interest_bank_loan" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.interest_bank_loan')}}">{{number_format($properties->interest_bank_loan*100,2,",",".")}}</a>%
							</td>
							<td class="border-no-right text-right">{{number_format($H48,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($I48,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($J48,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($K48,2,",",".")}}</td>
							<td class="border text-right">{{number_format($L48,2,",",".")}}</td>

						</tr>

						<tr>
							<th class="border-no-right">Summe</th>
							<th class="border-no-right bg-yellow text-right">
								{{number_format(($properties->with_real_ek + $properties->from_bond + $properties->bank_loan)*100,2,",",".")}}%
							</th>
							<th class="border text-right">
								{{number_format($D50,2,",",".")}}
							</th>
							<td></td>
							<td class="border-no-right">Tilgung Bank</td>
							<td class="border-no-right bg-yellow text-right">
								<a href="#" class="inline-edit" data-type="text" data-pk="eradication_bank" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.eradication_bank')}}">{{number_format($properties->eradication_bank*100,2,",",".")}}</a>%
							</td>
							<td class="border-no-right text-right">{{number_format($H49,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($I49,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($J49,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($K49,2,",",".")}}</td>
							<td class="border text-right">{{number_format($L49,2,",",".")}}</td>

						</tr>

						<tr>
							
							<td></td><td></td><td></td>
							<td></td>
							<th class="border-no-right bg-gray">Annuität</th>
							<th class="border-no-right bg-gray text-right">
								{{number_format(($properties->interest_bank_loan + $properties->eradication_bank)*100,2,",",".")}}%
							</th>
							<th class="border-no-right bg-gray text-right">{{number_format($H50,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($I50,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($J50,2,",",".")}}</th>
							<th class="border-no-right bg-gray text-right">{{number_format($K50,2,",",".")}}</th>
							<th class="border bg-gray text-right">{{number_format($L50,2,",",".")}}</th>

						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

						</tr>

						<tr>
							<td colspan="4"></td>
							<td class="border-no-right">Eigenmittelverzinsung</td>
							<td class="border-no-right bg-yellow text-right">
								<a href="#" class="inline-edit" data-type="text" data-pk="interest_bond" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.interest_bond')}}">{{number_format($properties->interest_bond*100,2,",",".")}}</a>%
							</td>
							<td class="border text-right">{{number_format($H52,2,",",".")}}</td>
							<td colspan="4"></td>
							<td>&nbsp;</td>

						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

						</tr>

						<tr>
							<th colspan="11" class="border bg-brown">3.) Betrieb und Verw.</th>
							<td>&nbsp;</td>

						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

						</tr>
						<tr>
							<th colspan="2" class="border-no-right bg-gray">Netto Miete (IST) p.a.</th>
							<th class="border  text-right">
								{{number_format($properties->net_rent_pa,2,",",".")}}</th>
							<td></td>
							<th class="border-no-right bg-gray">Kennzahlen</th>
							<td colspan="4" class="border bg-gray" style="border-left: 0px;">Basis: Vorsteuern</td>

							<td colspan="2"></td>
							<td>&nbsp;</td>

						</tr>
						<!-- inline-edit-use -->
						<tr>
							<th colspan="2" class="border-no-right"  style="background: #eaeaea;font-style: italic;">Netto Miete (IST) p.m.</th>
							<th class="border  text-right">
								{{number_format($properties->net_rent_pa/12,2,",",".")}}</th>
							<td></td>

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

						</tr>


						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>

						</tr>

						<tr>
							<th colspan="2" class="border bg-gray">Nichtumlagefähige NK</th>
							<th class="border bg-gray"></th>
							<td></td>
							<td class="border-no-right">Faktor (netto)</td>
							<td class="border text-right">{{number_format($L25,2,",",".")}}</td>

							<td></td>
							<td class="border-no-right">Ref. CF Salzgitter</td>
							<th class="border  text-right">{{number_format($properties->Ref_CF_Salzgitter,2,",",".")}}</th>
							<td colspan="2">(nicht ändern!)</td>

						</tr>

						<tr>
							<td class="border-no-right text-right">Instandhaltung nichtumlfähig</td>
							<td class="border-no-right text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="maintenance_nk" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.maintenance_nk')}} (%)">{{number_format($properties->maintenance_nk*100,2,",",".")}}</a>%</td>
							<td class="border text-right">
								{{ $properties->maintenance_increase_year1 = number_format($properties->gesamt_in_eur
												*($properties->maintenance_nk),2,",",".")
								}}</td>
							<td></td>
							<td class="border-no-right asdad ">BruttoRendite</td>
							<td class="border text-right">
							<!-- {{ $D42 }} {{ $properties->net_rent_pa }}  -->
								<!-- <br/>  -->
								{{number_format(($G58 *100),2,",",".")}}%</td>
							<td></td>
							<td class="border-no-right">Ref. GuV Salzgitter</td>
							<th class="border  text-right">{{number_format($properties->Ref_GuV_Salzgitter,2,",",".")}}</th>
							<td colspan="2">(nicht ändern!)</td>
						</tr>


						<tr>
							<td class="border text-right">Betriebsk. nicht umlfähig</td>
							<td class="border text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="operating_costs_nk" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.operating_costs_nk')}} (%)">{{number_format($properties->operating_costs_nk*100,2,",",".")}}</a>%</td>
							<td class="border text-right">{{number_format($properties->operating_cost_increase_year1,2,",",".")}}</td>
							<td colspan="1"></td>
							<td class="border-no-right">BruttoVervielält.</td>
							<td class="border text-right">{{number_format(($G59),2,",",".")}}</td>
							<td colspan="1"></td>
							<td class="border-no-right">AHK Salzgitter</td>
							<th class="border  text-right">{{number_format($properties->AHK_Salzgitter,2,",",".")}}</th>
							<td colspan="2">(nicht ändern!)</td>
						</tr>

						<tr>
							<td class="border text-right">Objektverwalt. nichtumlfähig</td>
							<td class="border text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="object_management_nk" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.object_management_nk')}} (%)">{{number_format($properties->object_management_nk*100,2,",",".")}}</a>%</td>
							<td class="border text-right ">{{number_format($properties->property_management_increase_year1,2,",",".")}}</td>
							<td></td>
							<td class="border-no-right">EK-Rendite/CF</td>
							<td class="border text-right">{{number_format(($G61 * 100),2,",",".")}}%</td>			<!--=E31/D48--->
							<td></td>
							<th class="border-no-right">CF-Ref.-Faktor</th>

							<th class="border text-right">
								<?php
								//J58*(D42/J60)
								if($properties->AHK_Salzgitter != 0){
									$cf_faktor = ($properties->Ref_CF_Salzgitter*
											(($properties->gesamt_in_eur
															+ ($properties->real_estate_taxes * $properties->gesamt_in_eur)
															+ ($properties->estate_agents * $properties->gesamt_in_eur)
															+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
															+ ($properties->evaluation * $properties->gesamt_in_eur)
															+ ($properties->others * $properties->gesamt_in_eur)
															+ ($properties->buffer * $properties->gesamt_in_eur))
													/$properties->AHK_Salzgitter));
									if($cf_faktor != 0){
										echo e(number_format((($E31+$properties->tax * $E23)/$cf_faktor)*100,2,",","."));
									}else echo "0";
								}else echo "0";
								?>%
							</th> <!--(E31+E25)/(J58*(D42/J60))-->

							<td colspan="2"></td>
						</tr>

						<tr>
							<td class="border text-right">Abschreibung</td>
							<td class="border text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="depreciation_nk" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.depreciation_nk')}} (%)">{{number_format($properties->depreciation_nk*100,2,",",".")}}</a>%</td>
							<td class="border text-right">{{number_format($properties->depreciation_nk_money,2,",",".")}}</td>
							<td></td>
							<td class="border-no-right">EK-Rendite/GuV/EBT</td>
							<td class="border text-right">
								{{number_format(($G62 * 100),2,",",".")}}%
							<td></td>
							<th class="border-no-right">GuV-Ref.-Faktor</th>
							<th class="border text-right">
								<?php
								//J59*(D42/J60)
								if($properties->AHK_Salzgitter != 0){
									$guv_faktor = (($properties->Ref_GuV_Salzgitter)* (($properties->gesamt_in_eur
															+ ($properties->real_estate_taxes * $properties->gesamt_in_eur)
															+ ($properties->estate_agents * $properties->gesamt_in_eur)
															+ (($properties->Grundbuch * $properties->gesamt_in_eur)/100)
															+ ($properties->evaluation * $properties->gesamt_in_eur)
															+ ($properties->others * $properties->gesamt_in_eur)
															+ ($properties->buffer * $properties->gesamt_in_eur))
													/($properties->AHK_Salzgitter)));
									if($guv_faktor != 0){
										echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
									}else echo "0";
								}else echo "0";
								?>%
							</th> <!--E23/(J59*(D42/J60))-->
							<td colspan="2"></td>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<th colspan="11" class="border bg-brown">4.) Exit / Rückzahlung Anleihe</th>
						</tr>

						<tr><td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td></tr>

						<tr>
							<td class="border-no-right">Wertenwicklung Immobilie</td>
							<td class="border-no-right text-right bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="property_value" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.property_value')}}(%)">{{number_format($properties->property_value*100,2,",",".")}}</a>%</td>
							<td class="border">Steigerung p.a.</td>
							<td></td>
							<td class="border-no-right text-right">{{number_format($properties->Increase_p_a_year1,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->Increase_p_a_year2,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->Increase_p_a_year3,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($properties->Increase_p_a_year4,2,",",".")}}</td>
							<td class="border text-right">{{number_format($properties->Increase_p_a_year5,2,",",".")}}</td>
							<td colspan="2"></td>
						</tr>

						<tr>
							<td class="border-no-right" colspan="2">Darlehensvaluta</td>
							<td class="border">end of year</td>
							<td></td>
							<td class="border-no-right text-right">{{number_format($H47,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($I47,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($J47,2,",",".")}}</td>
							<td class="border-no-right text-right">{{number_format($K47,2,",",".")}}</td>
							<td class="border text-right">{{number_format($L47,2,",",".")}}</td>
							<td colspan="2"></td>
						</tr>

						<tr>
							<th colspan="8" class="border-no-right">a.) aus Verkaufserlös</th>
							<th class="border text-right">{{number_format(($properties->Increase_p_a_year5)-($L47),2,",",".")}}</th>
							<td colspan="2"></td>
						</tr>

						<tr>
							<th class="border-no-right">b.) aus Revalutierung</th>
							<td class="border-no-right text-right">80%</td>
							<td colspan="6" class="border-no-right">auf Wert im 5. J</td>
							<th class="border text-right">{{number_format($properties->Increase_p_a_year5 * 0.8 - $L47,2,",",".")}}</th>
							<td colspan="2"></td>
						</tr>

						<tr>
							<th colspan="8" >&nbsp;</th>
							<th></th>
							<th></th>
							<th></th>

						</tr>


						<tr>
							<th colspan="11" class="border bg-brown">5.) Risikoberechnung</th>

						</tr>
						<tr>
							<td></td>
						</tr>
						 <!-- <td id="propertiesExtra1"> -->
							<!-- <tr>
							<td></td>
							<td></td>
							<td>&nbsp;</td>
							<td></td>
							<td></td>
							</tr> -->
						<tr>
							<?php
							if($properties->duration_from_O38=="" || $properties->duration_from_O38=="0000-00-00")
							{
								$properties->duration_from_O38 = date('Y-m-d');
							}
							// var_dump($properties->duration_from_O38);
							?>
							<td>&nbsp;</td>
							<td></td>
							<td class="">
							<!-- <a href="#" class="inline-edit" data-type="date" data-format="yyyy-mm-dd" data-pk="duration_from_O38" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.duration_from_O38')}}">{{date("d/m/Y", strtotime($properties->duration_from_O38))}}</a> -->
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<th class="text-right">Mieter</th>
							<td colspan="3" class="bg-gray border ">Netto Kaltmiete p.a.</td>
							<td class="bg-gray border">MV Ende</td>
							<td class="bg-gray border">Laufzeit ab</td>
							<td class="bg-gray border">WAULT</td>
							<td class="bg-gray border">Mieterlöse</td>
							<td class="">
								<a href="javascript:void(0)" data-class="show-mieter" class="btn btn-info show-link-mieter  pull-right">Alle anzeigen <i class="fa fa-angle-down"></i></a>
							</td>
						</tr>


						<?php
							//declare values
							$Q53 = 0;
							$sm = 0;
							?>
							@foreach($propertiesExtra1s as $propertiesExtra1)
								<tr class="show-mieter hidden">
								<td class="text-right bg-yellow">
									<a href="#" class="inline-edit" data-type="text" data-pk="tenant" data-url="{{url('propertiesExtra1/update/'.$tab.'/'.$propertiesExtra1->id) }}" data-title="{{__('property.field.tenant')}}">{{$propertiesExtra1->tenant}}</a>
								</td>
								<td colspan="3" class="border text-right bg-yellow">
									<a href="#" class="inline-edit" data-type="text" data-pk="net_rent_p_a" data-url="{{url('propertiesExtra1/update/'.$tab.'/'.$propertiesExtra1->id) }}" data-title="{{__('property.field.net_rent_p_a')}}" data-value="{{$propertiesExtra1->net_rent_p_a}}">{{number_format($propertiesExtra1->net_rent_p_a,2,",",".")}} </a>€

								</td>
								<?php
								$sm += $propertiesExtra1->net_rent_p_a;
								// if($propertiesExtra1->is_dynamic_date)
									// $propertiesExtra1->mv_end = date('d.m.Y');

								?>
								<td class="border text-right bg-yellow">
									<a href="#" class="inline-edit" data-type="text" data-pk="mv_end" data-url="{{url('propertiesExtra1/update/'.$tab.'/'.$propertiesExtra1->id) }}" data-title="{{__('property.field.mv_end')}}">
										{{ ($propertiesExtra1->mv_end) ? show_date_format(date('Y-m-d', strtotime(str_replace("/", "-", $propertiesExtra1->mv_end)))) : '' }}
									</a>
								</td>
								<?php
									if($propertiesExtra1->is_dynamic_date)
										$propertiesExtra1->mv_end2 = date('d.m.Y');

									?>

								<td class="border text-right bg-yellow">
									<a href="#" class="inline-edit" data-type="text" data-pk="mv_end2" data-url="{{url('propertiesExtra1/update/'.$tab.'/'.$propertiesExtra1->id) }}" data-title="{{__('property.field.mv_end')}}">{{$propertiesExtra1->mv_end2}}</a>
								</td>
								<td class="border text-center">
									<?php



									if( strpos($properties->duration_from_O38, "unbefristet") !== false){
										$value = 1;
									}else{
										$properties->duration_from_O38 = $propertiesExtra1->mv_end2;

										$date1=date_create(str_replace('/', '-', $propertiesExtra1->mv_end));
										$date2=date_create(str_replace('/', '-', $propertiesExtra1->mv_end2));

										$diff=date_diff($date1,$date2);

										$value =  $diff->format("%a")/ 365;

										// $value = ((strtotime(str_replace('/', '-', $propertiesExtra1->mv_end)) -  strtotime(str_replace('/', '-', $properties->duration_from_O38))) / 86400) / 365;
									}

									if (strpos($propertiesExtra1->mv_end, '2099') !== false)
										$value = 0.5;

									$Q53 += $value*$propertiesExtra1->net_rent_p_a;

									?>

									{{number_format($value,2,",",".")}}</td>
								<td class="border text-right">{{number_format($value*$propertiesExtra1->net_rent_p_a,2,",",".")}} €</td>
								<td class="">
									<a href="#" class="inline-edit" data-type="text" data-pk="mietvertrag_text" data-url="{{url('propertiesExtra1/update/'.$tab.'/'.$propertiesExtra1->id) }}" data-title="Mietvertrag">{{$propertiesExtra1->mietvertrag_text}}</a>
								</td>
								<td class="text-right" style="padding-left: 10px;">
									<a href="javascript:void(0)" class="remove-extra" data-id="{{$propertiesExtra1->id}}" data-seg="{{$tab}}"><span class="">X</span></a>
								</td>
							</tr>
							@endforeach



							{{--<tr>
                                <td colspan="5">
                                <form action="{{route("properties.addPropertiesExtra1Row")}}" method="POST" style="text-align:right">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="property_id" value="{{$properties->id}}">
                                    <input type="hidden" name="tab" value="{{$tab}}">
                                    <button type="submit" class="hidden btn btn-success btn-sm" >{{__('property.add_row')}}</button>
                                </form>
                                </td>
                            </tr>--}}
						<tr>
							<td>&nbsp;</td>
							<td class="border-bottom" colspan="2">gesamt</td>
							<td class="border-bottom text-right">{{number_format($sm,2,",",".")}} € </td>
							<?php
							$P53 = ($properties->net_rent_pa == 0) ? 0 : ($Q53 / $properties->net_rent_pa) ;
							$Q56 = ($D59 + $D60) * $P53;
							$Q55 = $Q53+$P43;
							$Q57 = ($D42 *(-1))+$Q55-$Q56;
							$Q58 =	($D42 == 0) ? 0 : $Q57 / $D42;


							$wault = floor($P53 * 100) / 100;
							if($wault!=$properties->wault){
									DB::table('properties')->where('id', $properties->id)->update(['wault' => $wault]);
							}
							?>
							<td class="border-bottom text-center"></td>
							<td class="border-bottom text-center"></td>
							
							<td class="border-bottom text-center get-wault-amount nice
							">{{number_format($P53,2,",",".")}}</td>
							<td  class="border-bottom text-right">{{number_format($Q53,2,",",".")}} € </td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td class="border-bottom" colspan="3">Grundstückswert</td>
							<td colspan="3" class="border-bottom text-center">&nbsp;</td>
							<td class="border-bottom text-right"> {{number_format($P43,2,",",".")}} € </td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td class="border-bottom" colspan="3">Wert total</td>
							<td colspan="3" class="border-bottom text-center"></td>
							<td class="border-bottom text-right"> {{number_format($Q55 ,2,",",".")}} € </td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td class="border-bottom" colspan="3">nicht umlagefähige Nebenkosten und Instandhaltungen über die WAULT</td>
							<td colspan="3" class="border-bottom text-center"></td>
							<td  class="border-bottom text-right">{{number_format($Q56,2,",",".")}} € </td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td class="border-bottom" colspan="3">Risikowert</td>
							<td colspan="3" class="border-bottom text-center"></td>
							<td  class="border-bottom text-right">{{number_format($Q57,2,",",".")}} € </td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td class="border-bottom" colspan="3">%-Risikowert</td>
							<td colspan="3" class="border-bottom text-center"></td>
							@php
								$v = $Q58*100;
							@endphp
							@if($v<-25)
								<td class="border-bottom text-right bg-red">{{number_format($Q58*100,2,",",".")}}%</td>
							@elseif($v<0 && $v>=-25)
								<td class="border-bottom text-right bg-yellow">{{number_format($Q58*100,2,",",".")}}%</td>
							@else
								<td class="border-bottom text-right bg-green">{{number_format($Q58*100,2,",",".")}}%</td>
							@endif
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						</td>
						<td class="note-table">

							<tr>
								<th colspan="11"  class="border bg-brown">6.) Lage / Notizen</th>

							</tr>
							<tr>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr>

							<tr>
								<th colspan="5" class="border text-center">Lage </th>
							</tr>
							<tr>
								<td colspan="5" class="border text-center"><a href="#" class="inline-edit" data-type="text" data-pk="position" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="{{__('property.field.position')}}">@if($properties->position){{$properties->position}}@else
											&nbsp;@endif</a></td>
							</tr>
							<tr>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr>
							<tr>
								<th colspan="5" class="border text-center">Notizen </th>
							</tr>
							<tr>
								<td colspan="5" class="border text-center">
									<a href="#" class="inline-edit" data-type="textarea" data-name="Ankermieter_note" data-pk="Ankermieter_note" data-url="{{url('property/edit_comment_iframe/'.$id) }}"  >@if($pcomments_new){{ $pcomments_new->Ankermieter_note }}@else
											&nbsp;@endif</a>

								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>

							<tr>
								<th colspan="5" class="border text-center">Datum LOI</th>
							</tr>
							<tr>
								<td colspan="5" class="border text-center">
									<a href="#" class="inline-edit" data-type="text" data-name="Datum LOI" data-pk="datum_lol" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}">
										@if($properties->datum_lol)
											{{date('d.m.Y', strtotime($properties->datum_lol))}}
										@else
											&nbsp;
										@endif
									</a>

								</td>
							</tr>


							<tr>
								<td>&nbsp;</td>
							</tr>

							<tr>
								<th colspan="5" class="border text-center">Tag der Beurkundung</th>
							</tr>
							<tr>
								<td colspan="5" class="border text-center"><a href="#" class="inline-edit" data-type="text" data-format="yyyy-mm-dd" data-pk="purchase_date" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Tag der Beurkundung">
										@if($properties->purchase_date)
											{{date('d.m.Y', strtotime($properties->purchase_date))}}

										@else
											&nbsp;
										@endif

									</a></td>
							</tr>

							<tr>
								<td colspan="5" class="border text-center"><a href="#" class="inline-edit" data-type="text" data-format="yyyy-mm-dd" data-pk="purchase_date2" data-url="{{url('property/update_property_ist_soll/'.$properties->id) }}/{{ Request::segment(3) }}" data-title="Tag der Beurkundung">
										@if($properties->purchase_date2)
											{{$properties->purchase_date2}}
										@else
											&nbsp;
										@endif
									</a></td>
							</tr>


						</td>

							<tr>
								<td  style="padding:20px">
									<form id="export-to-excel-form" method="post" action="{{url('property/export-to-excel')}}">
										<input type="hidden" name="bank" value="0">
										<input type="hidden" name="_token" value="{{csrf_token()}}">
										<input type="hidden" name="property_data" id="property-data" value="">
										<input type="hidden" name="tenant_data" id="tenant-data" value="">
										<input type="hidden" name="note_data" id="note-data" value="">
										<input type="hidden" name="property_id" value="{{$id}}" id="select_property_id">
										<input type="hidden" name="sub_property_id" value="{{$properties->id}}">
										<button type="button" class="btn-export-to-excel btn btn-primary" data-property-id="{{$properties->id}}">Gesamtexport </button>
									</form>
								</td>

								<td>
									<form id="export-to-excel-form2" method="post" action="{{url('property/export-to-excel')}}">
										<input type="hidden" name="bank" value="1">
										<input type="hidden" name="_token" value="{{csrf_token()}}">
										<input type="hidden" name="property_data" id="property-data-second" value="">
										<input type="hidden" name="tenant_data" id="tenant-data-second" value="">
										<input type="hidden" name="note_data" id="note-data-second" value="">
										<input type="hidden" name="sub_property_id" value="{{$properties->id}}">
										<input type="hidden" name="property_id" value="{{$id}}" id="select_property_id_second">
										<button type="button" class="btn-export-to-excel-second btn btn-primary" data-property-id="{{$properties->id}}">Bankexport</button>



									</form>
								</td>
								<td><a class="btn btn-primary" target="_blank" href="{{route('bankexport',$properties->id)}}" data-property-id="{{$properties->id}}">PDF</a></td>
							</tr>
						</tbody>
					</table>
					{{--<table class="property-table pr-zoom-in-out-table " >--}}
					{{--</table>--}}
					{{--<table class="property-table pr-zoom-in-out-table note-table" >--}}
					{{--</table>--}}
					<table>

					</table>
			 	</div>
		</div>
		{{-- @endforeach --}}
		</div>
	</div>



	<div class=" modal fade" role="dialog" id="add-city">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Add Ort</h4>
	      </div>
	      <div class="modal-body">
	      		<form id="add-ort">
	      			<input type="hidden" name="_token" value="{{csrf_token()}}">
	      			<label>Name</label>
			        <input type="text" name="name" class="form-control">
			        <br>
			        <label>Einwohner</label>
			        <input type="text" name="e_w" class="form-control">
			        <br>
			        <label>Kaufkraftindex</label>
			        <input type="text" name="kk_idx" class="form-control">
	      		</form>
	        
	      </div>
	      <div class="modal-footer">
	      	<button type="button" class="btn btn-primary save-city" >Speichern</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
	      </div>
	    </div>

	  </div>
	</div>
	
		
	<script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--slimscroll JavaScript -->
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset('assets/js/waves.js') }}"></script>
<!--Counter js -->
<script src="{{ asset('assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
<!-- chartist chart -->
<script src="{{ asset('assets/plugins/bower_components/chartist-js/dist/chartist.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
<!-- Sparkline chart JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{ asset('assets/js/custom.min.js') }}"></script>
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="{{ asset('assets/js/dashboard1.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>
<!--Style Switcher -->
<script src="{{ asset('assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
<!--jquery ui datepicker-->
<script src="{{ asset('assets/plugins/bower_components/x-editable/dist/jquery-editable/jquery-ui-datepicker/js/jquery-ui-1.10.3.custom.min.js') }}"></script>

<!-- Calendar JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/calendar/dist/fullcalendar.js') }}"></script>

<script src="{{ asset('assets/plugins/bower_components/calendar/dist/jquery.fullcalendar.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/calendar/dist/cal-init.js') }}"></script>

<script src="{{ asset('assets/plugins/bower_components/calendar/dist/locale-all.js') }}"></script>

<!--Morris JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/raphael/raphael-min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.3/select2.js"></script>
<script src="/js/custom-script.js"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ asset('assets/js/custom.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>

<!-- jQuery peity -->
<script src="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js') }}"></script>

<!--Style Switcher -->
<script src="{{ asset('assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>


    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>

    <script type="text/javascript">

        jQuery(document).ready(function($){

        	$(document).on("focus", ".p-mask-input-number", function() {
		        $(this).mask('#.##0,00', {
		            reverse: true
		        });
		    });

		    String.prototype.replaceAll = function (find, replace) {
			    var str = this;
			    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
			};

			function makeNumber(value){
			    value = value.replaceAll(".", "");
			    value = value.replaceAll(",", ".");
			    value = (!isNaN(value) && value != '') ? parseFloat(value) : 0;
			    return value;
			}

			function makeNumberFormat(value){
				if(value){
					value = value.toFixed(2);
					value = parseFloat(value);
				}
			    return value.toLocaleString("es-ES", {minimumFractionDigits: 2});
			}

			$('body').on('keyup', '.p_purchase_price', function() {
			    var id = $(this).attr('id');

			    var total_purchase_price = makeNumber($('#p_total_purchase_price').text());//gesamt_in_eur

			    var from_bond_per = makeNumber($('#p_from_bond_per').val());//building
			    var from_bond_amount = makeNumber($('#p_from_bond_amount').val());//building_amount

			    var bank_loan_per = makeNumber($('#p_bank_loan_per').val());//plot_of_land
			    var bank_loan_amount = makeNumber($('#p_bank_loan_amount').val());//plot_of_land_amount

			    // console.log({id, gesamt_in_eur, building, building_amount, plot_of_land_amount, plot_of_land_amount});

			    if(id == 'p_from_bond_per'){
			        if(from_bond_per > 100){
			            $('#p_from_bond_per').val(100);
			            from_bond_per = 100;
			        }
			        bank_loan_per = (100 - from_bond_per);

			        $('#p_bank_loan_per').val( makeNumberFormat(bank_loan_per) );
			        $('#p_from_bond_amount').val( makeNumberFormat(total_purchase_price * from_bond_per / 100) );
			        $('#p_bank_loan_amount').val( makeNumberFormat(total_purchase_price * bank_loan_per / 100) );

			    }else if(id == 'p_from_bond_amount'){

			        from_bond_per =  (from_bond_amount * 100 / total_purchase_price);
			        bank_loan_per = (100 - from_bond_per);

			        $('#p_from_bond_per').val( makeNumberFormat(from_bond_per) );
			        $('#p_bank_loan_per').val( makeNumberFormat(bank_loan_per) );
			        
			        $('#plot_of_land_amount').val( makeNumberFormat(total_purchase_price * bank_loan_per / 100) );

			    }else if(id == 'p_bank_loan_per'){
			        if(bank_loan_per > 100){
			            $('#plot_of_land').val(100);
			            bank_loan_per = 100;
			        }
			        from_bond_per = (100 - bank_loan_per);

			        $('#p_from_bond_per').val( makeNumberFormat(from_bond_per) );
			        $('#p_from_bond_amount').val( makeNumberFormat(total_purchase_price * from_bond_per / 100) );
			        $('#p_bank_loan_amount').val( makeNumberFormat(total_purchase_price * bank_loan_per / 100) );

			    }else if(id == 'p_bank_loan_amount'){
			        bank_loan_per = (bank_loan_amount * 100 / total_purchase_price);
			        from_bond_per = (100 - bank_loan_per);

			        $('#p_from_bond_per').val( makeNumberFormat(from_bond_per) );
			        $('#p_bank_loan_per').val( makeNumberFormat(bank_loan_per) );

			        $('#p_from_bond_amount').val( makeNumberFormat(total_purchase_price * from_bond_per / 100) );
			    }
			    
			});

			$('body').on('change', '.p_purchase_price', function() {
				var p_from_bond_per = $('#p_from_bond_per').val();
				var p_from_bond_per_url = $('#p_from_bond_per').attr('data-url');

				var p_bank_loan_per = $('#p_bank_loan_per').val();
				var p_bank_loan_per_url = $('#p_bank_loan_per').attr('data-url');

		        $.ajax({
		            url: p_from_bond_per_url,
		            type: "post",
		            data: {pk: 'from_bond', value: p_from_bond_per},
		            success: function(response) {
		            }
		        });

		        $.ajax({
		            url: p_bank_loan_per_url,
		            type: "post",
		            data: {pk: 'bank_loan', value: p_bank_loan_per},
		            success: function(response) {
		            }
		        });
			});

        	$('.show-link-mieter').click(function(){
			      var cl = $(this).attr('data-class');
			      if($('.'+cl).hasClass('hidden')){
			          $(this).find('i').removeClass('fa-angle-down');
			          $(this).find('i').addClass('fa-angle-up');
			          $('.'+cl).removeClass('hidden');
			      }
			      else{
			          $(this).find('i').addClass('fa-angle-down');
			          $(this).find('i').removeClass('fa-angle-up');
			          $('.'+cl).addClass('hidden');
			      }
			});

        	$('.wault-amount').html($('.get-wault-amount').html());


        	$('.change-p-user').on('change', function () {
                var propertyId = $('#select_property_id').val();
                $.ajax({
                    url: "{{route('updatepropertyuser')}}",
                    type: "get",
                    data : {
						'transaction_m_id': $('.transaction_m_id').val(),
						'seller_id': $('.seller_id').val(),
						'asset_m_id': $('.asset_m_id').val(),
                        'property_id' : propertyId
                    },
                    
                });

            });

            $('.save-city').on('click', function () {
                $.ajax({
                    url: "{{route('add_new_city')}}",
                    type: "post",
                    data : $('#add-ort').serialize(),
                    success: function (response) {
                    	alert(response.message);
                    	if(response.status==1){
                    		$('#add-city').modal('hide');
                    		$('#add-ort')[0].reset();
                    	}
                    	
                    }
                });

            });

			$('.btn-export-to-excel').on('click', function () {

				var propertyTableArray = getTableData("table.pr-zoom-in-out-table");
				var tenantTableArray = getTableData(".tenent-table");
				var note = getTableData(".note-table");
				$('input#property-data').val(JSON.stringify(propertyTableArray));
				$('input#tenant-data').val(JSON.stringify(tenantTableArray));
				$('input#note-data').val(JSON.stringify(note));
				console.log(tenantTableArray); 
				// return;
				$('#export-to-excel-form').submit();
			});
			
			$('.btn-export-to-excel-second').on('click', function () {
				var propertyTableArray = getTableData("table.pr-zoom-in-out-table");
				var tenantTableArray = getTableData(".tenent-table");
				var note = getTableData(".note-table");
				$('input#property-data-second').val(JSON.stringify(propertyTableArray));
				$('input#tenant-data-second').val(JSON.stringify(tenantTableArray));
				$('input#note-data-second').val(JSON.stringify(note));
				console.log(tenantTableArray); 
				// return;
				$('#export-to-excel-form2').submit();
			});

			//turn to inline mode
            // $.fn.editable.defaults.mode = 'inline';
            
            $.fn.editable.defaults.ajaxOptions = {type: "POST"};
            $.fn.editable.defaults.onblur = 'ignore';

			
			
            /*$('.property-table').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        var path = $('#path-properties-show').val();
						var bank_id = $('.bank-container.tab-pane.fade.in.active').data("bank-id");
                        location.reload();
					}
                }
            });*/

            $('.bank-remote').editable({
			        select2: {
			            placeholder: 'Banken',
			            allowClear: true,
			            minimumInputLength: 3,
			            id: function (item) {
			                return item.id;
			            },
			            ajax: {
			                url: '{{route("searchbank")}}',
			                dataType: 'json',
			                data: function (term, page) {
			                    return { query: term };
			                },
			                results: function (data, page) {
			                    return { results: data };
			                }
			            },
			            formatResult: function (item) {
			                return item.name;
			            },
			            formatSelection: function (item) {
			                return item.name;
			            },
			             
			        },
			        success: function(response, newValue) {
			                location.reload();
			        }
			    });

      //        $('.city-remote').editable({
		    //     source: '{{route("searchcity")}}',
		    //     select2: {
		    //         placeholder: 'Select Country',
		    //         minimumInputLength: 3
		    //     }
		    // });
		    $('.remove-extra').click(function(){
            	if(confirm("Are you sure want to delete this?"))
            	{
            		var i = $(this).attr('data-id');
            		var s = $(this).attr('data-seg');
            		$.ajax({
	                    url: "{{route('removeextrarow')}}",
	                    type: "GET",
	                    data : {id:i,segment:s},
	                    success: function (response) {
	                    	location.reload();
	                    }
	                });	
            	}
            	
            })
             $('.ecity-remote').editable({
        select2: {
            placeholder: 'Ort',
            allowClear: true,
            minimumInputLength: 3,
            id: function (item) {
                return item.name;
            },
            ajax: {
                url: '{{route("searchcity")}}',
                dataType: 'json',
                data: function (term, page) {
                    return { query: term };
                },
                results: function (data, page) {
                    return { results: data };
                }
            },
            formatResult: function (item) {
                return item.name;
            },
            formatSelection: function (item) {
                return item.name;
            },
             
        },
        success: function(response, newValue) {
                location.reload();
        }
    });

            /*
			 $('.bank-name').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        var path = $('#path-properties-show').val();
						var bank_id = $('.bank-container.tab-pane.fade.in.active').data("bank-id");
                        location.reload();
					}
                }
            });
			 */


            $('#tenant-listing').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        var path = $('#path-properties-show').val();
                        window.location.href = path + '?tab=tenant-listing';
                    }
                }
            });

            $('#swot-template').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        var path = $('#path-properties-show').val();
                        window.location.href = path + '?tab=swot-template';
                    }
                }
            });

            $('#new-tenant').on('click', function () {
                var propertyId = $(this).data('property-id');
                $.ajax({
                    url: "{{route('tenants.store')}}",
                    type: "post",
                    data : {
						'nk_pm': 0,
						'nk_pa': 0,
						'mv_begin': '',
						'mv_end': '',
                        'property_id' : propertyId
                    },
                    success: function (response) {
                        var path = $('#path-properties-show').val();
                        window.location.href = path + '?tab=tenant-listing';
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }

                });

            });

            $('#schl-template').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        var path = $('#path-properties-show').val();
                        window.location.href = path + '?tab=schl-template';
                    }
                }
            });

            $('#clever-fit-rental-offer').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        var path = $('#path-properties-show').val();
                        window.location.href = path + '?tab=clever-fit-rental-offer';
                    }
                }
            });

            $('#planung-berechnung-wohnen').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        var path = $('#path-properties-show').val();
                        window.location.href = path + '?tab=planung-berechnung-wohnen';
                    }
                }
            });

            $('#tenancy-schedule').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        var path = $('#path-properties-show').val();
                        var selectingTenancySchedule = response.selecting_tenancy_schedule;
                        window.location.href = path + "?tab=tenancy-schedule&selecting_tenancy_schedule=" + selectingTenancySchedule;
                    }
                }
            });

            $("#select_bank_s").on('change', function() {
                var allVals = $(this).val();
                allVals = allVals == null ? [] : allVals;

                if(allVals.length === 0){
                    $("#selectbank_create_btn_s").prop('disabled', true);
                }else{
                    $("#selectbank_create_btn_s").prop('disabled', false);
                }
            })

            $('.einline-edit-niedersachsen').editable({
		        prepend: "not selected",
		        source: [
		            {value: 'Baden-Württemberg', text: 'Baden-Württemberg'},
		            {value: 'Bayern', text: 'Bayern'},
		            {value: 'Berlin', text: 'Berlin'},
		            {value: 'Brandenburg', text: 'Brandenburg'},
		            {value: 'Bremen', text: 'Bremen'},
		            {value: 'Hamburg', text: 'Hamburg'},
		            {value: 'Hessen', text: 'Hessen'},
		            {value: 'Mecklenburg-Vorpommern', text: 'Mecklenburg-Vorpommern'},
		            {value: 'Niedersachsen', text: 'Niedersachsen'},
		            {value: 'Nordrhein-Westfalen', text: 'Nordrhein-Westfalen'},
		            {value: 'Rheinland-Pfalz', text: 'Rheinland-Pfalz'},
		            {value: 'Saarland', text: 'Saarland'},
		            {value: 'Sachsen', text: 'Sachsen'},
		            {value: 'Sachsen-Anhalt', text: 'Sachsen-Anhalt'},
		            {value: 'Schleswig-Holstein', text: 'Schleswig-Holstein'},
		            {value: 'Thüringen', text: 'Thüringen'},
		        ],
		        display: function(value, sourceData) {
		             // location.reload();
		        }   
		    });   


        });
    </script>


</body>

</html>
