@extends('layouts.admin') 

@section('css')
    <!-- Styles -->
    <link href="{{ asset('css/property-comparison.css') }}" rel="stylesheet">
    <style>
        .tablesaw-columntoggle-btnwrap{
            position: absolute;
            right: 20px;
            top: 17px;
        }
        .sindu_dragging {
            font-size: 11px!important;
        }
        .sindu_dragging #property-comparison{
            border: 1px solid #000000!important;
        }

        tr th, tr td {
            min-width: 100px!important;
            padding: 3.5px 5px !important;
            border-color: #000000 !important;
        }

        tr td.option, tr th.option {
            min-width: 10px !important;
        }

        ul.dropdown-menu li a{
            padding: 5px 20px;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        @if (Session::has('message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <p>
                <i class="icon fa fa-check"></i>{{Session::get('message')}}
            </p>
        </div>
        @endif @if (Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <p>
                <i class="icon fa fa-check"></i>{{Session::get('error')}}
            </p>
        </div>
        @endif

        <div class="white-box">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Select Properties</h3>
                    <select id="select-properties" class="form-control select2" multiple="multiple">
                        @foreach ( $all_properties as $all_property )
                            <option value="{{$all_property->id}}">{{$all_property->name_of_property}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <p style="float: left">
                                {{__('property.property_comparison')}} -
                                @if ($type == 'normal')
                                    @if($status == config('properties.status.buy'))
                                        {{__('property.buy')}}
                                    @elseif ($status == config('properties.status.hold'))
                                        {{__('property.hold')}}
                                    @else
                                        {{__('property.decline')}}
                                    @endif
                                @else
                                    {{__('property.extra')}}
                                @endif
                            </p>

                            <div style="text-align: right; margin-right: 20px">
                                <div class="btn-group" role="group">
                                    <a href="{{route('property_comparison').'?type=normal&status='.config('properties.status.buy')}}" type="button" class="btn btn-default">{{__('property.buy')}}</a>
                                    <a href="{{route('property_comparison').'?type=normal&status='.config('properties.status.hold')}}" type="button" class="btn btn-default">{{__('property.hold')}}</a>
                                    <a href="{{route('property_comparison').'?type=normal&status='.config('properties.status.decline')}}" type="button" class="btn btn-default">{{__('property.decline')}}</a>
                                    <a href="{{route('property_comparison').'?type=extra'}}" type="button" class="btn btn-default">{{__('property.extra')}}</a>
                                </div>
                            </div>

                        </div>

                        <div class="table-responsive">
                            <table id="property-comparison" class="tablesaw table-striped table-bordered table" data-tablesaw-mode="columntoggle">
                                <thead>
                                    <tr class="bg-gray color-red">
                                        @if ($type != 'extra')
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist" class="option" style="text-align: center;"><i class="ti-more-alt"></i></th>
                                        @endif
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">{{__('property.name_of_property')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="persist">{{__('property.place_or_city')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="17">{{__('property.type')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="16">{{__('property.business')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="15">{{__('property.purchase_price')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="14">{{__('property.purchase_price_total')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="13">{{__('property.rental_fee')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="12">{{__('property.yield')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="11">{{__('property.cf')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="10">{{__('property.ebt')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="9">{{__('property.target_rent')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="8">{{__('property.return_s')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="7">{{__('property.cf')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="6">{{__('property.ebt')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="5">{{__('property.required_ek')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="4">{{__('property.required_ek')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="3">{{__('property.financing')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="2">{{__('property.notaral_status')}}</th>
                                        <th scope="col" data-tablesaw-sortable-col data-tablesaw-priority="1">{{__('property.completion')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if ($type != 'extra')
                                    @if ($status == config('properties.status.buy'))
                                        <tr class="bg-blue color-red">
                                            <td class="option"></td>
                                            <th colspan="19">{{__('property.purchase')}}</th>
                                        </tr>

                                        @foreach($properties as $property)
                                            @if (abs($property->risk_value_percent) <= 0.75)
                                                <tr data-property-id="{{$property->id}}">
                                                    @include('properties.comparison-row')
                                                </tr>
                                            @endif
                                        @endforeach

                                        <tr>
                                            <td class="option"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="bolder">{{$total_purchase['sum_purchase_price']}}</td>
                                            <td class="bolder">{{$total_purchase['sum_purchase_price_total']}}</td>
                                            <td class="bolder">{{$total_purchase['sum_rental_fee']}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="bolder">0</td>
                                            <td class="bolder">0</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr class="bg-blue color-red ">
                                            <td class="option"></td>
                                            <th colspan="19">{{__('property.99_percent')}}</th>
                                        </tr>

                                        @foreach($properties as $property)
                                            @if (abs($property->risk_value_percent) > 0.99)
                                                <tr class="bg-green" data-property-id="{{$property->id}}">
                                                    @include('properties.comparison-row')
                                                </tr>
                                            @endif
                                        @endforeach
                                        <tr>
                                            <td class="option"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="bolder">{{$total_purchase['sum_purchase_price_99']}}</td>
                                            <td class="bolder">{{$total_purchase['sum_purchase_price_total_99']}}</td>
                                            <td class="bolder">{{$total_purchase['sum_rental_fee_99']}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="bolder">0</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr class="bg-blue color-red ">
                                            <td class="option"></td>
                                            <th colspan="19">{{__('property.75_percent')}}</th>
                                        </tr>

                                        @foreach($properties as $property)
                                            {{--If risk value > 75% and <= 90% --}}
                                            @if (abs($property->risk_value_percent) > 0.75)
                                                @if (abs($property->risk_value_percent) <= 0.99)
                                                <tr data-property-id="{{$property->id}}">
                                                    @include('properties.comparison-row')
                                                </tr>
                                                @endif
                                            @endif
                                        @endforeach

                                        <tr>
                                            <td class="option"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="bolder">{{$total_purchase['sum_purchase_price_75']}}</td>
                                            <td class="bolder"></td>
                                            <td class="bolder">{{$total_purchase['sum_rental_fee_75']}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr class="bg-blue">
                                            <td class="option"></td>
                                            <th class="color-red">{{__('property.total')}}</th>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="bolder">{{$total_purchase['total_purchase_price']}}</td>
                                            <td></td>
                                            <td class="bolder">{{$total_purchase['total_rental_fee']}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td class="option"></td>
                                            <td colspan="19">&nbsp;</td>
                                        </tr>

                                    @endif

                                    @if ($status == config('properties.status.hold'))

                                        <tr id="waiting-row" class="bg-blue">
                                            <td class="option"></td>
                                            <th class="color-red" colspan="19">{{__('property.waiting')}}</th>
                                        </tr>
                                        @foreach($properties as $property)
                                            @if ($property->is_in_negotiation == false)
                                                <tr class="handle" data-property-id="{{$property->id}}">
                                                    @include('properties.comparison-row')
                                                </tr>
                                            @endif
                                        @endforeach
                                        <tr>
                                            <td class="option"></td>
                                            <td colspan="19">&nbsp;</td>
                                        </tr>

                                        <tr id="in-negotiation-row" class="bg-blue">
                                            <td class="option"></td>
                                            <th class="color-red" colspan="19">{{__('property.in_negotiation')}}</th>
                                        </tr>
                                        @foreach($properties as $property)
                                            @if ($property->is_in_negotiation == true)
                                                <tr class="handle" data-property-id="{{$property->id}}">
                                                    @include('properties.comparison-row')
                                                </tr>
                                            @endif
                                        @endforeach
                                        <tr>
                                            <td class="option"></td>
                                            <td colspan="19">&nbsp;</td>
                                        </tr>
                                    @endif

                                    @if ($status == config('properties.status.decline'))
                                        <tr class="bg-blue">
                                            <td class="option"></td>
                                            <th class="color-red" colspan="19">{{__('property.away_or_no_sense')}}</th>
                                        </tr>

                                        @foreach($properties as $property)
                                            <tr data-property-id="{{$property->id}}">
                                                @include('properties.comparison-row')
                                            </tr>
                                        @endforeach

                                    @endif
                                @else
                                    <tr class="bg-blue">
                                        <th class="color-red" colspan="20">{{__('property.extra')}}</th>
                                    </tr>

                                    @foreach($properties as $property)
                                        <tr class="bg-pink handle" data-property-id="{{$property->id}}">
                                            @include('properties.comparison-row')
                                        </tr>
                                    @endforeach

                                    <tr>
                                        <td colspan="20">&nbsp;</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                            <div class="pagination-wrapper" style="margin-left: 20px">
                                <div class="row pull-left">

                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#chart-container">{{__('property.chart')}}</a></li>
                        <li><a data-toggle="tab" href="#map-container">{{__('property.map.text')}}</a></li>
                    </ul>
                    <div class="tab-content">

                        <div id="chart-container" class="tab-pane fade in active">
                            <div id="multiple-properties"></div>
                        </div>

                        <div id="map-container" class="tab-pane fade">
                            <div id="map" style="height: 500px;"></div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection



@section('js')
    {{--<script--}}
            {{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
            {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
            {{--crossorigin="anonymous"></script>--}}
    <script src="/js/table-dragger.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var el = document.getElementById('property-comparison');
            if ($('.handle').length > 0) {
                var dragger = tableDragger(el, {
                    mode: 'row',
                    dragHandler: '.handle',
                    onlyBody: true,
                    animation: 200
                });
                dragger.on('drop',function(from, to, el){
                    var rows = $('#property-comparison tr');
                    var witingIndex = rows.index($('#waiting-row'));
                    var inNegotiationIndex = rows.index($('#in-negotiation-row'));
                    var isInNegotiation = 0; //currently status is hold.waiting
                    if (to > inNegotiationIndex){
                        isInNegotiation = 1;
                    }

                    var selectedRow = $('#property-comparison').find('tr').eq(to);
                    var propertyId = selectedRow.data('property-id');
                    $.ajax({
                        url: "/index.php/property/updateStatusInHold",
                        type: "post",
                        data: {
                            status: isInNegotiation,
                            propertyId: propertyId
                        } ,
                        success: function (response) {
                            data = JSON.parse(response);
                            console.log(data);
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                        }

                    });

                });
            }

            $('a.status-option').on('click', function () {
                var selectedStatus = $(this).data('status');
                var selectedRow = $(this).closest('tr');
                var propertyId = selectedRow.data('property-id');
                $.ajax({
                    url: "/index.php/change-property-status",
                    type: "post",
                    data: {
                        status: selectedStatus,
                        property_id: propertyId
                    } ,
                    success: function (response) {
                        data = JSON.parse(response);
                        selectedRow.remove();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }

                });
                console.log(propertyId);
            });
        });


        var data = <?php echo json_encode($all_properties); ?>;
        var data_build = {};
        var locators = [];
        for (var i in data) {
            data_build[data[i].id] = data[i];
        }
        // Chart - params
        var multiple_chart;
        var properties = [];

        // Map - params
        var map;
        var geocoder;
        var markers = [];
        var bounds;
        var markerBounds = [];

        document.addEventListener("DOMContentLoaded", function(event) { 
            // Multiple properties
            multiple_chart = Morris.Line({
                element: 'multiple-properties',
                data: null,
                xkey: ['duration_from'],
                ykeys: ['purchase_price', 'total_purchase_price','rent'],
                labels: ['<?php echo __('property.purchase_price') ?>', '<?php echo __('property.purchase_price_total') ?>', '<?php echo __('property.rental_fee') ?>'],
                parseTime        : false
            });

            // Google Map
            if (document.querySelectorAll('#map').length > 0)
            {
                if (document.querySelector('html').lang)
                    lang = document.querySelector('html').lang;
                else
                    lang = 'en';

                var js_file = document.createElement('script');
                js_file.type = 'text/javascript';
                js_file.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDeWADTt3gAkkKPyafQZmy__UmjUxLutXw&callback=initMap&language=' + lang;
                document.getElementsByTagName('head')[0].appendChild(js_file);
            }
        });

        $(document).ready(function(){
            $('#select-properties').on('change', function(){
                var property_ids = $(this).val();
                property_ids = property_ids == null ? [] : property_ids;
                properties = [];

                locators = [];
                for ( var i in markers ) {
                    markers[i] = null;
                }
                if( property_ids.length > 0 ) {
                    $.each(property_ids, function(i, v){
                        if( typeof data_build[v] != 'undefined' ) {
                            properties.push({
                                duration_from: data_build[v].duration_from,
                                purchase_price: data_build[v].total_purchase_price,
                                total_purchase_price: data_build[v].total_nbpac,
                                rent: data_build[v].rent
                            });
                            locators.push({
                                'title': data_build[v].name_of_property,
                                'author': data_build[v].name_of_creator,
                                'city_place': data_build[v].city_place,
                            });
                        }
                    });

                    if( properties.length > 0 ) {
                        multiple_chart.setData(properties);
                    } else {
                        multiple_chart.setData(null);
                    }

                } else {
                    multiple_chart.setData(null);
                }

                plotMarkers(locators);
            });
            $('a[href="#map-container"]').on('shown.bs.tab', function(){
                google.maps.event.trigger(map, 'resize');
                plotMarkers(locators);
            });
            $('a[href="#chart-container"]').on('shown.bs.tab', function(){
                if( properties.length > 0 ) {
                    multiple_chart.setData(properties);
                } else {
                    multiple_chart.setData(null);
                }
            });
        });

        function initMap()
        {   
            geocoder = new google.maps.Geocoder();
            map = new google.maps.Map($('#map')[0], {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });
            var tmp = [];
            plotMarkers(tmp);
        }

        function plotMarkers(m)
        {
            bounds = new google.maps.LatLngBounds();
            var total_addresses = m.length;            
            var address_count = 0;
            m.forEach(function (marker) {
                address_count++;  

                var title = marker.title;

                var infowindow = new google.maps.InfoWindow({
                    content: '<h3>'+ marker.title +'</h3><p>{{__("property.Author")}}: '+ marker.author +'<br/>{{__("property.place_or_city")}}: '+ marker.city_place +'</p>'
                });
                geocoder.geocode( { 'address': marker.city_place}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var position = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                        markers.push(
                           new google.maps.Marker({
                                position: position,
                                map: map,
                                animation: google.maps.Animation.DROP,
                                title: title
                            }).addListener('click', function() {
                                infowindow.open(map, this);
                            })
                        );
                        // bounds.extend(position);
                        addressesBounds(total_addresses, address_count, position);
                    }
                });

            });
            map.fitBounds(bounds);
        }

        function addressesBounds(total_addresses, address_count, myLatLng) {

            markerBounds.push(myLatLng);

            // make sure you only run this function when all addresses have been geocoded
            if (total_addresses == address_count) {
                var latlngbounds = new google.maps.LatLngBounds();
                for ( var i = 0; i < markerBounds.length; i++ ) {
                    latlngbounds.extend(markerBounds[i]);
                }
                map.setCenter(latlngbounds.getCenter());
                map.fitBounds(latlngbounds);
            }
        }

    </script>
@endsection