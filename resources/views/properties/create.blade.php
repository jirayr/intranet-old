@extends('layouts.admin')
@section('css')
    <style>
        .input-text{
            height: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="row">

        {!! Form::open(['action' => ['PropertiesController@store'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
        {!! Form::token() !!}
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading"><button onclick="location.href='{{route('properties.index')}}'" type="button"
                                                   class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                class="ti-arrow-left"></i></button> {{__('property.create_property')}}
                                <div class="pull-right"> <label class="col-md-6">{{__('property.name_of_property')}}:</label>
                        <div class="col-md-6">
                            <input type="text" name="name_of_property" class="form-control form-control-line input-text" value=""
                                   placeholder=""  >
                        </div>

                </div>
                </div>

                <div class="white-box">


                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.net_rent')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="net_rent" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.net_rent_empty')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="net_rent_empty" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.maintenance')}} (%): </label>
                        <div class="col-md-2">
                            <input type="number" name="maintenance" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.operating_costs')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="operating_costs" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.object_management')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="object_management" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.tax')}} (%): </label>
                        <div class="col-md-2">
                            <input type="number" name="tax" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.plot_of_land_m2')}}:</label>
                        <div class="col-md-2">
                            <input type="number" name="plot_of_land_m2" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.construction_year')}}:</label>
                        <div class="col-md-2">
                            <input type="number" name="construction_year" class="form-control form-control-line input-text" value=""
                                   placeholder=""  >
                        </div>

                        <label class="col-md-2">{{__('property.field.land_value')}}: </label>
                        <div class="col-md-2">
                            <input type="number" name="land_value" class="form-control form-control-line input-text" value=""
                                   placeholder=""  >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.building')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="building" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.plot_of_land')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="plot_of_land" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.real_estate_taxes')}} (%): </label>
                        <div class="col-md-2">
                            <input type="number" name="real_estate_taxes" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.estate_agents')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="estate_agents" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.notary_land_register')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="notary_land_register" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.evaluation')}} (%): </label>
                        <div class="col-md-2">
                            <input type="number" name="evaluation" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.others')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="others" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.buffer')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="buffer" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.rent')}}: </label>
                        <div class="col-md-2">
                            <input type="number" name="rent" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.rent_whg')}}:</label>
                        <div class="col-md-2">
                            <input type="number" name="rent_whg" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.vacancy')}}:</label>
                        <div class="col-md-2">
                            <input type="number" name="vacancy" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.vacancy_whg')}}: </label>
                        <div class="col-md-2">
                            <input type="number" name="vacancy_whg" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.wault')}}:</label>
                        <div class="col-md-2">
                            <input type="number" name="wault" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.anchor_tenant')}}:</label>
                        <div class="col-md-2">
                            <input type="number" name="anchor_tenant" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.plot')}}: </label>
                        <div class="col-md-2">
                            <input type="number" name="plot" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.with_real_ek')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="with_real_ek" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.from_bond')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="from_bond" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.bank_loan')}} (%): </label>
                        <div class="col-md-2">
                            <input type="number" name="bank_loan" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.interest_bank_loan')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="interest_bank_loan" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.eradication_bank')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="eradication_bank" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.interest_bond')}} (%): </label>
                        <div class="col-md-2">
                            <input type="number" name="interest_bond" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.maintenance_nk')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="maintenance_nk" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.operating_costs_nk')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="operating_costs_nk" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.object_management_nk')}} (%): </label>
                        <div class="col-md-2">
                            <input type="number" name="object_management_nk" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.depreciation_nk')}}(%):</label>
                        <div class="col-md-2">
                            <input type="number" name="depreciation_nk" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.property_value')}} (%):</label>
                        <div class="col-md-2">
                            <input type="number" name="property_value" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.nk_anchor_tenants')}} : </label>
                        <div class="col-md-2">
                            <input type="number" name="nk_anchor_tenants" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.nk_tenant')}} 1:</label>
                        <div class="col-md-2">
                            <input type="number" name="nk_tenant1" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.nk_tenant')}} 2:</label>
                        <div class="col-md-2">
                            <input type="number" name="nk_tenant2" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.nk_tenant')}} 3: </label>
                        <div class="col-md-2">
                            <input type="number" name="nk_tenant3" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.nk_tenant')}} 4:</label>
                        <div class="col-md-2">
                            <input type="number" name="nk_tenant4" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.nk_tenant')}} 5:</label>
                        <div class="col-md-2">
                            <input type="number" name="nk_tenant5" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>

                        <label class="col-md-2">{{__('property.field.mv_anchor_tenants')}}: </label>
                        <div class="col-md-2">
                            <input type="text" name="mv_anchor_tenants" class="form-control form-control-line input-text datepicker" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.mv_tenant')}} 1:</label>
                        <div class="col-md-2">
                            <input type="text" name="mv_tenant1" class="form-control form-control-line input-text datepicker" value="">
                        </div>

                        <label class="col-md-2">{{__('property.field.mv_tenant')}} 2:</label>
                        <div class="col-md-2">
                            <input type="text" name="mv_tenant2" class="form-control form-control-line input-text datepicker" value="">
                        </div>

                        <label class="col-md-2">{{__('property.field.mv_tenant')}} 3: </label>
                        <div class="col-md-2">
                            <input type="text" name="mv_tenant3" class="form-control form-control-line input-text datepicker" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2">{{__('property.field.mv_tenant')}} 4:</label>
                        <div class="col-md-2">
                            <input type="text" name="mv_tenant4" class="form-control form-control-line input-text datepicker" value="">
                        </div>

                        <label class="col-md-2">{{__('property.field.mv_tenant')}} 5:</label>
                        <div class="col-md-2">
                            <input type="text" name="mv_tenant5" class="form-control form-control-line input-text datepicker" value="">
                        </div>

                        {{--<label class="col-md-2">Maintenance: </label>--}}
                        {{--<div class="col-md-2">--}}
                            {{--<input type="number" name="maintenance" class="form-control form-control-line input-text" value=""--}}
                                   {{--placeholder=""  step="0.01">--}}
                        {{--</div>--}}
						 <label class="col-md-2">{{__('property.field.total_purchase_price')}}:</label>
                        <div class="col-md-2">
                            <input type="number" name="total_purchase_price" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
                    </div>
					<div class="form-group">
						<!---  K38--->
						<label class="col-md-2">{{__('property.field.total_commercial_sqm')}} :</label>
                        <div class="col-md-2">
                            <input type="number" name="total_commercial_sqm" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
						
						<label class="col-md-2">{{__('property.field.city_or_place')}}</label>
                        <div class="col-md-2">
                            <input type="text" name="city_place" class="form-control form-control-line input-text" value=""
                                   placeholder="">
                        </div>
						<label class="col-md-2">{{__('property.field.duration_from')}}</label>
                        <div class="col-md-2">
                            <input type="text" name="duration_from" class="form-control form-control-line input-text datepicker" value="">
                        </div>
					</div>
					<div class="form-group">
						
						<label class="col-md-2">{{__('property.field.net_rent_pa')}}</label>
                        <div class="col-md-2">
                             <input type="number" name="net_rent_pa" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
						<label class="col-md-2">{{__('property.field.whg_qm_of_wault')}}</label>
                        <div class="col-md-2">
                             <input type="number" name="WHG_qm_of_WAULT" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
						<label class="col-md-2">{{__('property.field.whg_qm_of_anchor_tenant')}}</label>
                        <div class="col-md-2">
                            <input type="number" name="WHG_qm_of_anchor_tenant" class="form-control form-control-line input-text" value=""
								placeholder=""  step="0.01">
                        </div>
					</div>
					<div class="form-group">
						
						<label class="col-md-2">{{__('property.field.whg_qm_of_plot')}}</label>
                        <div class="col-md-2">
                             <input type="number" name="WHG_qm_of_plot" class="form-control form-control-line input-text" value=""
                                   placeholder=""  step="0.01">
                        </div>
					</div>
                    <div class="form-group">
                        <div class="col-lg-2 col-sm-4 col-xs-12">
                            <a href="{{route('properties.index')}}" type="button"
                               class="btn btn-block btn-danger btn-rounded">{{__('property.cancel')}}</a>
                        </div>
                        <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12">
                            <button type="submit" name="submit" class="btn btn-block btn-primary btn-rounded">{{__('property.submit')}}
                            </button>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@endsection