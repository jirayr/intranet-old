
<div class="modal-dialog modal-lg" style="width:100%">
	<div class="modal-content">
		<div class="modal-header">
			<div>
				<div class="col-md-10">
					<h4 class="modal-title">Projekte</h4>
				</div>
				<div class="col-md-2">
					<a class="btn btn-primary pojectCreate">Bearbeiten</a>
				</div>
			</div>

		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-sm-12 table-responsive white-box">
					<table class="table table-striped" id="project_table">
						<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Erstellt von </th>
							<th>Verknüpft</th>
							<th>Beschreibung</th>
							<th>Notizen</th>
							<th>Datum</th>
							<th>Aktion</th>
						</tr>
						</thead>
						<tbody>
						@if(!empty($propertyProjects))
							@foreach ($propertyProjects as $key => $propertyProject)
								<tr>
									<td>{{ ($key+1) }}</td>
									<td>{{ $propertyProject->name }}</td>
									<td>{{ $propertyProject->user->name }}</td>
									<td>
										@foreach ($propertyProject->employees as $employee)
											<span class="badge">{{ $employee->vorname}}{{ $employee->nachname}}</span>
										@endforeach
										@foreach ($propertyProject->userEmployees as $employee)
											<span class="badge">{{ $employee->name}}</span>
										@endforeach
									</td>
									<td>{{ $propertyProject->description }}</td>
									<td>{{ $propertyProject->note}}</td>
									<td>{{$propertyProject->date }}</td>
									<td>
										<a type="button" data-id="{{$propertyProject->id}}" class="btn-sm btn-info edit">Bearbeiten</a>
										<a type="button" data-id="{{$propertyProject->id}}"  class="btn-sm btn-danger delete">Löschen</a>
										<a type="button" href="{{url('properties/'.Request::segment(2).'/project/'.$propertyProject->id.'/email/type/'.'1')}}" class="btn-sm btn-primary"> View Emails</a>
									</td>
								</tr>
							@endforeach
						@endif
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>

</div>


<div class=" modal fade" role="dialog" id="projectCreate">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="title"> Projekt erstellen</h4>
			</div>
			<div class="modal-body">
				<form id="projectForm" class="projectform" method="post" action="">
					<div class="row">
						<div class="form-group col-md-6" >
							<label for="email">Name:</label>
							<input required type="text" class="form-control" name="name" maxlength="105"  placeholder="Name eingeben" id="name">
						</div>
						<div class="form-group col-md-6">
							<label for="email">Mitarbeiter:</label>
							<select  required class="form-control search" name="employee_id[]"  style="width: 100%"></select>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label for="email">Datum:</label>
							<input type='text' id='datetimepicker1' name="date" class="form-control" />
						</div>
						<div class="form-group col-md-6">
							<label for="email">Nutzer:</label>
							<select  required class="form-control user-search" name="employee_user[]"  style="width: 100%"></select>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label for="email">Beschreibung:</label>
							<textarea name="description" class="form-control" id="description" cols="30" rows="10" placeholder="Beschreibung eingeben"></textarea>
						</div>
						<div class="form-group col-md-6">
							<label for="email">Notiz:</label>
							<textarea name="note" class="form-control" id="notes" cols="30" rows="10" placeholder="Notiz eingeben"></textarea>
						</div>
					</div>
					<input type="hidden" name="property_id" id="propId" value="<?php echo Request::segment(2); ?>">
					<input type="hidden"  id="projectId" value="">
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					<button type="submit"  id="btn-save" class="btn btn-primary">Speichern</button>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>

@section('scripts')
	<script>
		$('.pojectCreate').on('click', function () {
			$('#btn-save').val("create");

			var id = $(this).attr('data-id');
			var prop = $('#propId').val();
			var url ='/properties/'+prop+'/project';
			$('#projectForm').attr('action', url);
			$('#name').val('');
			$('#description').val('');
			$('#notes').val('');
			$('.search').html('');
			$('.user-search').html('');
			$('#title').html('Create Project');

			$('#projectCreate').modal('show');

		});

		$('.edit').on('click', function () {
			$('#btn-save').val("update");
			$('#title').html('Edit Project');
			var id = $(this).attr('data-id');
			$('#projectId').val(id);
			var prop = $('#propId').val();
			var url ='/properties/'+prop+'/project/update'+'/'+id;
			$('.search').val('');
			$.ajax({
				type : 'get',
				url : "{{url('properties/'.Request::segment(2).'/project/edit')}}"+'/'+id,
				success : function (data)
				{
					$('#projectForm').attr('action', url);
					$('#name').val(data.name);
					$('#description').val(data.description);
					$('#notes').val(data.note);
					// $('#datetimepicker1').datepicker('setDate', data.date);
					$.each(data.employees, function(key) {
						$('.search').append('<option selected value="'+data.employees[key].id+'">'+data.employees[key].vorname+data.employees[key].nachname+'</option>');
					});
					$.each(data.user_employees, function(key) {
						$('.user-search').append('<option selected value="'+data.user_employees[key].id+'">'+data.user_employees[key].name+'</option>');
					});


				}

			});
			$('#projectCreate').modal('show');

		});

		$(document).ready(function () {
			$("body").on('submit', '#projectForm', function(e) {
			// $('#projectForm').submit(function (e) {
				e.preventDefault();
				$('#config-loader').css('visibility', 'visible');
				var state = $('#btn-save').val();
				var prop = $('#propId').val();
				var url ='/properties/'+prop+'/project';

				if (state == "update") {
					var id = $('#projectId').val();
					var url ='/properties/'+prop+'/project/update'+'/'+id;
				}

				$.ajax({
					type : 'post',
					url : url,
					data :   $('form.projectform').serialize(),
					success : function (data)
					{
						$('#config-loader').css('visibility', 'hidden');
						$('#save-config').removeClass('disabled');
						window.setTimeout(function(){
							$('#projectCreate').modal('hide');
							sweetAlert("Saved Successfully");
						}, 600);
						location.reload();
					}
				});

			});
		});

		$('.delete').on('click', function () {
			var id = $(this).attr('data-id');
			swal({
				title: 'Delete this Account?',
				text: "You won't be able to revert this!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then(function (isConfirm) {
				if (isConfirm.dismiss !== 'cancel' && isConfirm){
					$.ajax({
						url :  "{{url('properties/'.Request::segment(2).'/project/delete')}}"+'/'+id,
						success : function (data)
						{
							sweetAlert("Deleted Successfully");
							location.reload();
						}
					});
				}

			})

		});

	</script>
	<script>

		$(document).ready(function() {

			$(".user-search").select2({

				minimumInputLength: 2,

				language: {

					inputTooShort: function () {
						return "<?php echo 'Please enter 2 or more characters'; ?>"
					}
				},
				multiple: true,
				tags: true,

				ajax: {

					url: '{{route("autocompleteSearchUserByName")}}',
					dataType: 'json',
					data: function (term) {
						return {
							query: term
						};
					},
					processResults: function (data) {
						return {
							results: $.map(data, function (item) {
								return {
									text:  item.name,
									id: item.id,
								}

							})

						};
					}

				}

			});


			$(".search").select2({

				minimumInputLength: 2,

				language: {

					inputTooShort: function () {
						return "<?php echo 'Please enter 2 or more characters'; ?>"
					}
				},
				multiple: true,
				tags: true,

				ajax: {

					url: '{{route("autocompleteByName")}}',
					dataType: 'json',
					data: function (term) {
						return {
							query: term
						};
					},
					processResults: function (data) {
						return {
							results: $.map(data, function (item) {
								return {
									text:  item.vorname+''+item.nachname+'-'+( item.company !== null ? '('+item.company.name +')':'else'),
									id: item.id,
									slug: item.keyword

								}

							})

						};
					}

				}

			});

			$(function () {
				$('#datetimepicker1').datepicker();
			});
		});

		$(function() {
			var columns = [
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
			];
			makeDatatable($('#project_table'), columns);
		});
	</script>


@stop


