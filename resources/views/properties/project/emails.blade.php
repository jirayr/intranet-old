@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="card-header">
        <div class="row">
            <div class="col-md-10">
                @if($emailType == 1)
                <h4 class="card-title">INBOX</h4>
                @else
                <h4 class="card-title">SENT</h4>
                @endif
            </div>
            <div class="col-md-2">
{{--                @if(isset($company))--}}
                    <a type="button" style="" href="{{url('properties/'.Request::segment(2).'/project/'.Request::segment(4).'/email/type/'.'1')}}"  class="btn-sm btn-primary">INBOX</a>
                {{--@else--}}
                    <a type="button" href="{{url('properties/'.Request::segment(2).'/project/'.Request::segment(4).'/email/type/'.'2')}}" class="btn-sm btn-primary">SENT</a>
                {{--@endif--}}

            </div>
        </div>
    </div>
    <div class="col-sm-12 table-responsive white-box">
        <table  id="email_table" class="table table-striped" >
            <thead>
            <tr>
                <th>Employee Email</th>
                <th>subject</th>
                <th>email Sent From</th>
                <th>bcc</th>
                <th>cc</th>
                <th>Date</th>
                <th>Email Body</th>
            </tr>
            </thead>
            <tbody>
            @isset($data)
            @foreach($data as $email)
                @if(isset($email->emailMapping->email_type_id) == $emailType)
                    <tr>
                        <td>{{$email->emailMapping->emailAccount->email}}</td>
                        <td>{{$email->subject}}</td>
                        <td>{{$email->email_sent_from}}</td>
                        <td>{{$email->bcc}}</td>
                        <td>{{$email->cc}}</td>
                        <td>{{$email->date_of_email}}</td>
                        <td><a data-id="{{$email->body}}" class="body btn-sm btn-primary">Email</a></td>
                    </tr>
                @endif
            @endforeach
            @endisset
            </tbody>
        </table>
    </div>
{{--    {{ $data->links() }}--}}

</div>
<div class=" modal fade" role="dialog" id="modal">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="title"> Content</h4>
            </div>
            <div class="modal-body">
                    <div id="modal-body"style="overflow: auto"></div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/custom-datatable.js') }}"></script>

    <script>
        $(function() {
            var columns = [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ];
            makeDatatable($('#email_table'), columns);
        });

        $('.body').on('click', function () {
            var data = $(this).attr('data-id');
            $('#modal-body').html(data);
            $('#modal').modal('show');
        });
    </script>
@stop
