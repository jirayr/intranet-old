<?php
$btn1 = $btn2 = 'btn btn-primary vacant-release-request';

$btn1_type = "btn1_request";
$btn2_type = "btn2_request";
$rbutton_title1 = "Zur Freigabe an Janine senden";
$rbutton_title2 = "Zur Freigabe an Falk senden";

$email = strtolower($user->email);

if($email=="j.klausch@fcr-immobilien.de")
{	
	$btn1 =  'btn btn-primary vacant-release-request';
	$rbutton_title1 = "Freigeben";
	$btn1_type = "btn1";

	if($arr && isset($arr['btn2_request']))
	{
	  $btn2 =  " btn-success vacant-release-request";
	  $rbutton_title2 = "Zur Freigabe gesendet";
	}
}
elseif($email==config('users.falk_email'))
{
	$rbutton_title2 = "Freigeben";
	$btn2 = 'btn btn-primary vacant-release-request';
	$btn2_type = "btn2";

	if($arr && isset($arr['btn1_request']))
	{
	  $btn1 =  " btn-success vacant-release-request";
	  $rbutton_title1 = "Zur Freigabe gesendet";
	}
}
else{
	if($arr && isset($arr['btn1_request']))
	{
	  $btn1 =  " btn-success vacant-release-request";
	  $rbutton_title1 = "Zur Freigabe gesendet";
	}

	if($arr && isset($arr['btn2_request']))
	{
	  $btn2 =  " btn-success vacant-release-request";
	  $rbutton_title2 = "Zur Freigabe gesendet";
	}
}

$r1 = $r2 = "";
if($arr && isset($arr['btn1']))
{
  $btn1 =  " btn-success";
  $rbutton_title1 = "Freigegeben";
  foreach($arr['btn1'] as $k=>$list)
  	$r1 = "Janine: ".show_date_format($list->created_at);
}

if($arr && isset($arr['btn2']))
{
  $btn2 =  " btn-success";
  $rbutton_title2 = "Freigegeben";
  foreach($arr['btn2'] as $k=>$list)
  	$r2 = "Falk: ".show_date_format($list->created_at);
}


?>
<!-- <h3>1) Freigabe Janine Klausch <button style="margin-right:10px; " type="button" class="btn {{$btn1}}" data-column="{{$btn1_type}}"> {{$rbutton_title1}}</button>{{$r1}}</h3> -->
<!-- <div class="clearfix"></div> -->
<h3>Freigabe Falk Raudies
<button style="margin-left: 23px;margin-right:10px;" type="button" class="btn {{$btn2}}" data-column="{{$btn2_type}}"> {{$rbutton_title2}}</button>{{$r2}}</h3>

<span class="f-release hidden">@if($r2)<button type="button" class="btn btn-success">Freigegeben</button> {{$r2}}@endif</span>
