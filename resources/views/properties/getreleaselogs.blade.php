<h3 style="margin-top: 40px;">Verlauf</h3>
<table class="forecast-table" >
    <thead>
      <tr>
        <th class="border bg-gray">Name</th>
        <th class="border bg-gray">Button</th>
        <th class="border bg-gray">Kommentar</th>
        <th class="border bg-gray">Datum</th>
      </tr>
    </thead>
    <tbody>
      @foreach($d as $k=>$list)

      <?php
      $tname = "";
      $u = DB::table('users')->where('id', $list->user_id)->first();
      if($u)
        $tname = $u->name;
      
      if($list->slug=="btn1_request")
      $rbutton_title = "1) Zur Freigabe an Janine senden";
      else if($list->slug=="btn2_request")
      $rbutton_title = "2) Zur Freigabe an Falk senden";
      else if($list->slug=="vacant_not_release")
      $rbutton_title = "Ablehen";
      else{
        $number = str_replace('btn', '', $list->slug);
        $rbutton_title = $number.") Freigeben";
      }





      ?>
      <tr>
        <td class="border">{{$tname}}</td>
        <td class="border">{{$rbutton_title}}</td>
        <td class="border">{{$list->value}}</td>
        <td class="border">{{show_datetime_format($list['created_at'])}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>