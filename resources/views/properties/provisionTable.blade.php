@if($t == 1)

   <table class="table table-striped" id="provisiondatatable">
      <thead>
         <tr>
            <th>AM</th>

            {{-- <th>Mieter</th>
            <th>Miete x LFZ</th>
            <th>Kosten</th>
            <th>Nettoeinnahmen</th> --}}

            <th style="min-width: 120px;">Mieter</th>
            <th style="min-width: 120px;">Fläche</th>
            <th style="min-width: 100px;">Art</th>
            <th style="min-width: 100px;">Kosten Umbau</th>
            <th style="min-width: 100px;">Einnahmen p.m.</th>
            <th style="min-width: 100px;">Laufzeit in Monate</th>
            <th style="min-width: 100px;">Mietfrei in €</th>
            <th style="min-width: 100px;">Jährl. Einnahmen</th>
            <th style="min-width: 100px;">Gesamt-einnahmen</th>
            <th style="min-width: 100px;">Gesamt-einnahmen netto</th>

            {{-- <th>MV / VL</th> --}}
            <th style="width: 10%;">Provision in %</th>
            <th>Provision</th>
            <th>Vertrag</th>
            <th>Kommentar</th>
            <th>Freigabe Falk</th>
            <th>Ablehnen</th>
            <th></th>
         </tr>
      </thead>
      <tbody>
         @foreach($t_list as $list1)

            <?php
               if(!$list1->asset_m_id)
                  $list1->asset_m_id = $properties->asset_m_id;

               if(!$list1->mv_vl){
                  $list1->mv_vl = "MV";
               }
            ?>

            @if(isset($arr2[$list1->id]['pbtn2']))
               {{-- <tr>
                  <td>
                     @foreach($as_users as $list2)
                        @if($list1->asset_m_id==$list2->id)
                           {{$list2->getshortname()}}
                        @endif
                     @endforeach
                  </td>
                  <td>
                     {{$list1->name}}
                     @if($list1->rent_begin && $list1->rent_end)
                        <br><span>Alter MV: {{ show_date_format($list1->rent_begin) }} - {{ show_date_format($list1->rent_end) }}</span>
                     @endif
                  </td>
                  <td>{{number_format($list1->rent,2,',','.')}}</td>
                  <td>{{number_format($list1->cost,2,',','.')}}</td>
                  <td>{{number_format($list1->net_income,2,',','.')}}</td>
                  <td>{{$list1->mv_vl}}</td>
                  <td> {{$list1->commision_percent}}</td>
                  @if(is_numeric($list1->commision_percent))
                     <td class="calculated-provision-{{$list1->id}}">{{number_format($list1->net_income*$list1->commision_percent/100,2,',','.')}}</td>
                  @else
                     <td class="calculated-provision-{{$list1->id}}"></td>
                  @endif
                  <td>
                     <button type="button" class="btn btn-primary btn-sm provision-comment" data-id="{{ $list1->id }}" data-url="{{ route('get_provision_comment', ['id' => $list1->id]) }}">Kommentar</button>
                  </td>
                  <td data-id="{{$list1->id}}" class="tenantbtn2-{{$list1->id}}"></td>
                  <td>
                     @if(Auth::user()->email == config('users.falk_email') && $list1->provision_not_release_status != 1 && $list1->tenant_id)
                        <button type="button" class="btn btn-primary btn-provision-not-release" data-id="{{ $list1->id }}">Ablehnen</button>
                     @endif
                  </td>
                  <td></td>
               </tr> --}}
            @else
               <tr>
                  <td>
                     <select class="change-provision" data-id="{{$list1->id}}" data-column="asset_m_id">
                        <option value="">{{__('dashboard.asset_manager')}}</option>
                        @foreach($as_users as $list2)
                           @if($list1->asset_m_id==$list2->id)
                              <option selected="selected" value="{{$list2->id}}">{{$list2->getshortname()}}</option>
                           @else
                              <option value="{{$list2->id}}">{{$list2->getshortname()}}</option>
                           @endif
                        @endforeach
                     </select>
                  </td>

                  <td>
                     <input type="text" class="form-control change-provision" data-id="{{$list1->id}}" data-column="name" value="{{$list1->name}}" />
                     @if($list1->rent_begin && $list1->rent_end)
                        <p>Alter MV: {{ show_date_format($list1->rent_begin) }} - {{ show_date_format($list1->rent_end) }}</p>
                     @endif
                     @if($list1->mieter_item_id)
                        <!-- <a href="javascript:void(0);" data-type="provision" data-id="{{ $list1->mieter_item_id }}" data-title="Mieter" class="show_item_detail">Show Detail</a> -->
                     @endif
                  </td>
                  <td>
                     <input type="text" class="form-control change-provision" data-id="{{$list1->id}}" data-column="flache" value="{{ $list1->flache }}" />
                     @if($list1->flache_item_id)
                        <a href="javascript:void(0);" data-type="provision" data-id="{{ $list1->flache_item_id }}" data-title="Fläche" class="show_item_detail">Mehr Infos</a>
                     @endif
                  </td>
                  <td>
                     <select class="change-provision" data-id="{{$list1->id}}" data-column="mv_vl">
                        <option value="MV" @if($list1->mv_vl=="MV") selected="selected" @endif>MV</option>
                        <option value="VL" @if($list1->mv_vl=="VL") selected="selected" @endif>VL</option>
                     </select>
                  </td>
                  <td>
                     <input type="text" class="p_cost form-control mask-input-number change-provision" data-id="{{$list1->id}}" data-column="cost" value="{{number_format($list1->cost,2,',','.')}}" />
                  </td>
                  <td>
                     <input type="text" class="p_einnahmen_pm form-control mask-input-number change-provision" data-id="{{$list1->id}}" data-column="einnahmen_pm" value="{{number_format($list1->einnahmen_pm,2,',','.')}}" />
                  </td>
                  <td>
                     <input type="text" class="p_laufzeit_in_monate form-control mask-input-number change-provision" data-id="{{$list1->id}}" data-column="laufzeit_in_monate" value="{{number_format($list1->laufzeit_in_monate,2,',','.')}}" />
                  </td>
                  <td>
                     <input type="text" class="p_mietfrei form-control mask-input-number change-provision" data-id="{{$list1->id}}" data-column="mietfrei" value="{{number_format($list1->mietfrei,2,',','.')}}" />
                  </td>
                  @php
                     $jahrl_einnahmen = ($list1->einnahmen_pm * 12);
                     $rent = ($list1->einnahmen_pm * $list1->laufzeit_in_monate);
                     $net_income = ($rent - $list1->cost - $list1->mietfrei);
                  @endphp
                  <td class="text-right p_jahrl_einnahmen">
                     {{number_format($jahrl_einnahmen,2,',','.')}}
                  </td>
                  <td class="text-right p_rent">
                     {{number_format($rent,2,',','.')}}
                  </td>
                  <td class="text-right p_net_income">
                     {{number_format($net_income,2,',','.')}}
                  </td>
                  

                  <td class="editable-provision-{{$list1->id}}" style="width: 10%;">
                     @if(isset($arr2[$list1->id]['pbtn2']))
                        {{$list1->commision_percent}}
                     @else
                        <input type="text" class="form-control change-provision" data-id="{{$list1->id}}" data-column="commision_percent" value="{{number_format($list1->commision_percent,2,',','.')}}" />
                     @endif
                  </td>

                     @if(is_numeric($list1->commision_percent))
                        <td class="calculated-provision-{{$list1->id}}">{{number_format($list1->net_income*$list1->commision_percent/100,2,',','.')}}</td>
                     @else
                        <td class="calculated-provision-{{$list1->id}}"></td>
                     @endif

                  <td>
                     <a href="javascript:void(0);" class="provision-attachment" data-id="{{ $list1->id }}">
                       <i class="fa fa-link"></i>
                     </a>

                     @if($list1->file_name)
                            @if($list1->file_type == 'file')

                              &nbsp;&nbsp;<a href="{{ $list1->file_href }}"  id="provision-gdrive-link-{{ $list1->id }}"  target="_blank" title="{{ $list1->file_name }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $list1->file_extension) ?: 'fa-file' }}" ></i></a>

                            @else

                                &nbsp;&nbsp;<a href="javascript:void(0);" title="{{ $list1->file_name }}" onClick="loadDirectoryFiles('{{ $list1->file_path }}');"  ><i class="fa fa-folder" ></i></a>

                            @endif
                     @endif

                  </td>

                  <td>
                     <button type="button" class="btn btn-primary btn-sm provision-comment" data-id="{{ $list1->id }}" data-url="{{ route('get_provision_comment', ['id' => $list1->id]) }}">Kommentar</button>
                  </td>
                  
                  <td data-id="{{$list1->id}}" class="tenantbtn2-{{$list1->id}}"></td>
                  <td>
                     @if(Auth::user()->email == config('users.falk_email') && $list1->provision_not_release_status != 1 && $list1->tenant_id)
                        <button type="button" class="btn btn-primary btn-provision-not-release" data-id="{{ $list1->id }}">Ablehnen</button>
                     @endif
                  </td>
                  <td>
                     <button type="button" class="btn btn-danger btn-sm delete-provision" data-id="{{$list1->id}}"><i class="fa fa-times"></i></button>
                  </td>
               </tr>
            @endif

         @endforeach
         </tr>
      </tbody>
   </table>

@elseif($t == 2)
   
   <table class="table table-striped" id="release-provisiondatatable">
      <thead>
         <tr>
            <th>AM</th>
            {{-- <th>Mieter</th>
            <th>Miete x LFZ</th>
            <th>Kosten</th>
            <th>Nettoeinnahmen</th> --}}

            <th style="min-width: 120px;">Mieter</th>
            <th style="min-width: 120px;">Fläche</th>
            <th style="min-width: 100px;">Art</th>
            <th style="min-width: 100px;">Kosten Umbau</th>
            <th style="min-width: 100px;">Einnahmen p.m.</th>
            <th style="min-width: 100px;">Laufzeit in Monate</th>
            <th style="min-width: 100px;">Mietfrei in €</th>
            <th style="min-width: 100px;">Jährl. Einnahmen</th>
            <th style="min-width: 100px;">Gesamt-einnahmen</th>
            <th style="min-width: 100px;">Gesamt-einnahmen netto</th>

            {{-- <th>MV / VL</th> --}}
            <th style="width: 10%;">Provision in %</th>
            <th>Provision</th>
            <th>Kommentar</th>
            {{-- <th>Freigabe Falk</th>
            <th>Ablehnen</th>
            <th></th> --}}
         </tr>
      </thead>
      <tbody>
         @foreach($t_list as $list1)

            <?php
               if(!$list1->asset_m_id)
                  $list1->asset_m_id = $properties->asset_m_id;

               if(!$list1->mv_vl){
                  $list1->mv_vl = "MV";
               }
            ?>

            @if(isset($arr2[$list1->id]['pbtn2']))

               <tr>
                  <td>
                     @foreach($as_users as $list2)
                        @if($list1->asset_m_id==$list2->id)
                           {{$list2->getshortname()}}
                        @endif
                     @endforeach
                  </td>

                  <td>
                     {{$list1->name}}
                     @if($list1->rent_begin && $list1->rent_end)
                        <p>Alter MV: {{ show_date_format($list1->rent_begin) }} - {{ show_date_format($list1->rent_end) }}</p>
                     @endif
                     @if($list1->mieter_item_id)
                        <!-- <br><a href="javascript:void(0);" data-type="provision" data-id="{{ $list1->mieter_item_id }}" data-title="Mieter" class="show_item_detail">Show Detail</a> -->
                     @endif
                  </td>
                  <td>
                     {{$list1->flache}}
                     @if($list1->flache_item_id)
                        <br><a href="javascript:void(0);" data-type="provision" data-id="{{ $list1->flache_item_id }}" data-title="Fläche" class="show_item_detail">Mehr Infos</a>
                     @endif
                  </td>
                  <td>{{$list1->mv_vl}}</td>
                  <td class="text-right">{{number_format($list1->cost,2,',','.')}}</td>
                  <td class="text-right">{{number_format($list1->einnahmen_pm,2,',','.')}}</td>
                  <td class="text-right">{{number_format($list1->laufzeit_in_monate,2,',','.')}}</td>
                  <td class="text-right">{{number_format($list1->mietfrei,2,',','.')}}</td>
                  @php
                     $jahrl_einnahmen = ($list1->einnahmen_pm * 12);
                     $rent = ($list1->einnahmen_pm * $list1->laufzeit_in_monate);
                     $net_income = ($rent - $list1->cost - $list1->mietfrei);
                  @endphp
                  <td class="text-right">{{number_format($jahrl_einnahmen,2,',','.')}}</td>
                  <td class="text-right">{{number_format($rent,2,',','.')}}</td>
                  <td class="text-right">{{number_format($net_income,2,',','.')}}</td>
                  
                  
                  <td> {{$list1->commision_percent}}</td>
                  @if(is_numeric($list1->commision_percent))
                     <td class="calculated-provision-{{$list1->id}}">{{number_format($list1->net_income*$list1->commision_percent/100,2,',','.')}}</td>
                  @else
                     <td class="calculated-provision-{{$list1->id}}"></td>
                  @endif
                  <td>
                     <button type="button" class="btn btn-primary btn-sm provision-comment" data-id="{{ $list1->id }}" data-url="{{ route('get_provision_comment', ['id' => $list1->id]) }}">Kommentar</button>
                  </td>
                  {{-- <td data-id="{{$list1->id}}" class="tenantbtn2-{{$list1->id}}"></td>
                  <td>
                     @if(Auth::user()->email == config('users.falk_email') && $list1->provision_not_release_status != 1 && $list1->tenant_id)
                        <button type="button" class="btn btn-primary btn-provision-not-release" data-id="{{ $list1->id }}">Ablehnen</button>
                     @endif
                  </td>
                  <td></td> --}}
               </tr>

            @endif

         @endforeach
         </tr>
      </tbody>
   </table>

@endif
