 <table class="table table-striped" style="padding: 5px">
        <thead>
        <tr>
            <th>Transaction Manager</th>
            <th>Objekt</th>
            <th>Datum</th>
            <th>Versendet</th>
            <th>Antwort erhalten</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $list)
            <tr>
                <td>{{$list->name}}</td>
                <td>@if(isset($list->property->name_of_property))<a href="{{url('/properties/'.$list->property->main_property_id)}}">{{$list->property->name_of_property}}</a>@endif</td>
                <td>{{$list->date}}</td>
                <td><input data-column="status_sent" type="checkbox" class="checkbox-is-sent" data-id="{{$list->id}}" @if($list->status_sent) checked @endif></td>
                <td><input data-column="status_answer_received" type="checkbox" class="checkbox-is-sent" data-id="{{$list->id}}" @if($list->status_answer_received) checked @endif></td>
            </tr>
        @endforeach
        </tbody>
    </table>