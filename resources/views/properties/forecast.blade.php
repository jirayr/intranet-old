<ul class="nav nav-tabs">
	@foreach($properties->forecasts as $key => $forecast)
	<li @if($selecting_forecast == $forecast->id) class="active" @endif><a data-toggle="tab" href="#forecast-{{$forecast->id}}">{{$forecast->name}}</a></li>
	@endforeach
	<li class="">
		<form action="{{url("property/$properties->id/forecast/create")}}" method="POST">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" class="btn btn-success btn-sm" style="margin-bottom: -20px">Add Forecast</button>
		</form>
	</li>
</ul>

<div class="tab-content">

	@foreach($properties->forecasts as $key => $forecast)
	<div id="forecast-{{$forecast->id}}" class="tab-pane fade @if($selecting_forecast == $forecast->id) {{'in active'}} @endif ">
		<div class="white-box table-responsive forecast-details">
			<table class="forecast-table">
				<thead></thead>
				<tbody>

				{{--row 1--}}
				<tr>
					<th colspan="2"><a href="#" class="inline-edit" data-type="text" data-pk="name" data-placement="right" data-url="{{url('forecast/update/'.$forecast->id) }}" data-title="{{__('forecast.field.name')}} (%)">{{$forecast->name}}</a></th>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<th class="text-right border">9/3/18</th>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>

				{{--row 2--}}
				<tr>
					<th></th>
					<td class="text"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="bg-light-green border">Guthaben FCR</td>
					<td class="bg-light-green text-right border">8,590</td>
					<td class="bg-light-green border"></td>
					<td class="bg-light-green border"></td>
					<td class="bg-light-green border"></td>
					<td></td>
				</tr>

				{{--row 3--}}
				<tr>
					<th>Hinweise:</th>
					<td class="bg-yellow"><a href="#" class="inline-edit" data-type="text" data-pk="missing_info" data-url="{{url('forecast/update/'.$forecast->id) }}" data-title="{{__('forecast.field.missing_information')}}">{{$forecast->missing_info}}</a> </td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="border">Guthaben SPV</td>
					<td class="text-right border">808</td>
					<td class="border"></td>
					<td class="border"></td>
					<td class="border"></td>
					<td></td>
				</tr>

				{{--row 4--}}
				<tr>
					<td></td>
					<td class="bg-orange"><a href="#" class="inline-edit" data-type="text" data-pk="todo" data-url="{{url('forecast/update/'.$forecast->id) }}" data-title="{{__('forecast.field.todo')}}">{{$forecast->todo}}</a> </td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<th class="border">Summe Guthaben</th>
					<th class="text-right border">9,398</th>
					<th class="border"></th>
					<th class="border"></th>
					<th class="border"></th>
					<td></td>
				</tr>

				{{--row 5--}}
				<tr>
					<th></th>
					<td>ab 80 % Darstellung zur Info</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="bg-light-green border">möglicher Transfer</td>
					<td class="bg-light-green text-right border"></td>
					<td class="bg-light-green border"></td>
					<td class="bg-light-green border"></td>
					<td class="bg-light-green border"></td>
					<td></td>
				</tr>

				{{--row 6--}}
				<tr>
					<th></th>
					<td>ab 95 % Berücksichtigung in Liquiplanung</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td class="bg-light-green border">Puffer</td>
					<td class="bg-light-green text-right color-red border">-300</td>
					<td class="bg-light-green border"></td>
					<td class="bg-light-green border"></td>
					<td class="bg-light-green border"></td>
					<td></td>
				</tr>

				{{--row 7--}}
				<tr>
					<th></th>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<th class="bg-light-green border">Verfügbar</th>
					<th class="bg-light-green text-right border">8,590</th>
					<th class="bg-light-green text-center border">7,083</th>
					<th class="bg-light-green text-center border">3,478</th>
					<th class="bg-light-green text-center border">11,609</th>
					<td></td>
				</tr>

				{{--row 8--}}
				<tr>
					<th></th>
					<td></td>
					<th class="text-right">8/22/18</th>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<th></th>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>

				{{--row 9--}}
				<tr class="header">
					<th class="bg-blue border">Art</th>
					<th class="bg-blue border text">Beschreibung</th>
					<th class="bg-blue border text">Status lt. Pipeline AR</th>
					<th class="bg-blue border">Notar</th>
					<th class="bg-blue border">BNL</th>
					<th class="bg-blue border">Kaufpreis brutto</th>
					<th class="bg-blue border">FK</th>
					<th class="bg-blue border">EK</th>
					<th class="bg-blue border">Bedarf</th>
					<th class="bg-blue border">Sep</th>
					<th class="bg-blue border">Okt</th>
					<th class="bg-blue border">Nov</th>
					<th class="bg-blue border">Dez</th>
					<th class="note border-bottom">Notizen AL</th>

				</tr>

				{{--row 10--}}
				@foreach($forecast->forecast_kinds as $forecast_kind)
					@if($forecast_kind->type == config('forecast.type.pipeline_ar'))
						<tr>
							<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="kind" data-placement="right" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.kind')}}">{{$forecast_kind->kind}}</a></td>
							<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="description" data-placement="right" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.description')}}">{{$forecast_kind->description}}</a></td>
							<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="status" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.status')}}">{{$forecast_kind->status}}</a></td>
							<td class="text-right border"><a href="#" class="inline-edit" data-type="date" data-pk="notary" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.notary')}}">{{$forecast_kind->notary}}</a></td>
							<td class="text-right border"><a href="#" class="inline-edit" data-type="date" data-pk="bnl" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.bnl')}}">{{$forecast_kind->bnl}}</a></td>
							<td class="text-right border"><a href="#" class="inline-edit" data-type="number" data-pk="total_purchase_price" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.total_purchase_price')}}">{{$forecast_kind->total_purchase_price}}</a></td>
							<td class="text-right border"><a href="#" class="inline-edit" data-type="number" data-pk="fk_or_sale" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.fk')}}">{{$forecast_kind->fk_or_sale}}</a></td>
							<td class="text-right border">{{$forecast_kind->total_purchase_price - $forecast_kind->fk_or_sale}}</td>
							<td class="text-right border color-red">{{-($forecast_kind->total_purchase_price - $forecast_kind->fk_or_sale)}}</td>
							<td class="text-right bg-gray border"></td>
							<td class="text-right bg-gray border"></td>
							<td class="text-right bg-gray border"></td>
							<td class="text-right bg-gray border"></td>
							<td class="note"><a href="#" class="inline-edit" data-type="text" data-pk="note" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.note')}}">{{$forecast_kind->note}}</a></td>
						</tr>
					@endif
				@endforeach

				{{--new row button--}}
				<tr>
					<td>
						<form action="{{url("forecast/$forecast->id/forecast-kind/create")}}" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="type" value="{{config('forecast.type.pipeline_ar')}}">
							<button type="submit" class="btn btn-default btn-xs btn-add-forecast-type">{{__('forecast.add')}}</button>
						</form>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>

				{{--row 28--}}
				<tr class="header">
					<th class="bg-blue border">Art</th>
					<th class="bg-blue border text">Beschreibung</th>
					<th class="bg-blue border text">Status [AL]</th>
					<th class="bg-blue border">Notar</th>
					<th class="bg-blue border">BNL</th>
					<th class="bg-blue border">Kaufpreis brutto</th>
					<th class="bg-blue border">Verkauf</th>
					<th class="bg-blue border">Nettogewinn</th>
					<th class="bg-blue border">Liquirückfluss</th>
					<th class="bg-blue border">Sep</th>
					<th class="bg-blue border">Okt</th>
					<th class="bg-blue border">Nov</th>
					<th class="bg-blue border">Dez</th>
					<td class="note"></td>

				</tr>

				{{--row 29--}}
				@foreach($forecast->forecast_kinds as $forecast_kind)
					@if($forecast_kind->type == config('forecast.type.al'))
						<tr>
							<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="kind" data-placement="right" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.kind')}}">{{$forecast_kind->kind}}</a></td>
							<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="description" data-placement="right" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.description')}}">{{$forecast_kind->description}}</a></td>
							<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="status" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.status')}}">{{$forecast_kind->status}}</a></td>
							<td class="text-right border"><a href="#" class="inline-edit" data-type="date" data-pk="notary" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.notary')}}">{{$forecast_kind->notary}}</a></td>
							<td class="text-right border"><a href="#" class="inline-edit" data-type="date" data-pk="bnl" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.bnl')}}">{{$forecast_kind->bnl}}</a></td>
							<td class="text-right border"><a href="#" class="inline-edit" data-type="number" data-pk="total_purchase_price" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.total_purchase_price')}}">{{$forecast_kind->total_purchase_price}}</a></td>
							<td class="text-right border"><a href="#" class="inline-edit" data-type="number" data-pk="fk_or_sale" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.sale')}}">{{$forecast_kind->fk_or_sale}}</a></td>
							<td class="text-right border">{{$forecast_kind->fk_or_sale - $forecast_kind->total_purchase_price}}</td>
							<td class="text-right border"><a href="#" class="inline-edit" data-type="number" data-pk="reflux" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.reflux')}}">{{$forecast_kind->reflux}}</a></td>
							<td class="text-right bg-gray border"></td>
							<td class="text-right bg-gray border"></td>
							<td class="text-right bg-gray border"></td>
							<td class="text-right bg-gray border"></td>
							<td class="note"><a href="#" class="inline-edit" data-type="text" data-pk="note" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.note')}}">{{$forecast_kind->note}}</a></td>
						</tr>
					@endif
				@endforeach
				{{--new row button--}}
				<tr>
					<td>
						<form action="{{url("forecast/$forecast->id/forecast-kind/create")}}" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="type" value="{{config('forecast.type.al')}}">
							<button type="submit" class="btn btn-default btn-xs btn-add-forecast-type">{{__('forecast.add')}}</button>
						</form>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>

				{{--row 38--}}
				<tr class="header">
					<th class="bg-blue border">Art</th>
					<th class="bg-blue border text">Beschreibung</th>
					<th class="bg-blue border text">Status</th>
					<th class="bg-blue border"></th>
					<th class="bg-blue border"></th>
					<th class="bg-blue border"></th>
					<th class="bg-blue border"></th>
					<th class="bg-blue border"></th>
					<th class="bg-blue border"></th>
					<th class="bg-blue border">Sep</th>
					<th class="bg-blue border">Okt</th>
					<th class="bg-blue border">Nov</th>
					<th class="bg-blue border">Dez</th>
					<td class="note"></td>
				</tr>

				{{--row 39--}}
				@foreach($forecast->forecast_kinds as $forecast_kind)
					@if($forecast_kind->type == config('forecast.type.others'))
					<tr>
						<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="kind" data-placement="right" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.kind')}}">{{$forecast_kind->kind}}</a></td>
						<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="description" data-placement="right" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.description')}}">{{$forecast_kind->description}}</a></td>
						<td class="border"><a href="#" class="inline-edit" data-type="text" data-pk="status" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.status')}}">{{$forecast_kind->status}}</a></td>
						<td class="text-right bg-gray border"></td>
						<td class="text-right bg-gray border"></td>
						<td class="text-right bg-gray border"></td>
						<td class="text-right bg-gray border"></td>
						<td class="text-right bg-gray border"></td>
						<td class="text-right bg-gray border"></td>
						<td class="text-right bg-gray color-red border"></td>
						<td class="text-right bg-gray color-red border"></td>
						<td class="text-right bg-gray color-red border"></td>
						<td class="text-right bg-gray color-red border"></td>
						<td class="note"><a href="#" class="inline-edit" data-type="text" data-pk="note" data-url="{{url('forecast-kind/update/'.$forecast_kind->id) }}" data-title="{{__('forecast.field.note')}}">{{$forecast_kind->note}}</a></td>
					</tr>
					@endif
				@endforeach

				{{--new row button--}}
				<tr>
					<td>
						<form action="{{url("forecast/$forecast->id/forecast-kind/create")}}" method="POST">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="type" value="{{config('forecast.type.others')}}">
							<button type="submit" class="btn btn-default btn-xs btn-add-forecast-type">{{__('forecast.add')}}</button>
						</form>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>

				{{--row 47--}}
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<th>Liquidität</th>
					<th class="text-right">7,083</th>
					<th class="text-right">3,478</th>
					<th class="text-right">11,609</th>
					<th class="text-right color-red">-6,448</th>
					<td></td>
				</tr>


				</tbody>
			</table>
		</div>
	</div>
	@endforeach
</div>