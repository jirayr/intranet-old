<?php
$pbtn1 = $pbtn2 = 'btn btn-primary provision-release-request';

$pbtn1_type = "pbtn1_request";
$pbtn2_type = "pbtn2_request";
$rbutton_title1 = "Zur Freigabe an Janine senden";
$rbutton_title2 = "Zur Freigabe an Falk senden";

$email = strtolower($user->email);

if($email=="j.klausch@fcr-immobilien.de")
{	
	$pbtn1 =  'btn btn-primary provision-release-request';
	$rbutton_title1 = "Freigeben";
	$pbtn1_type = "pbtn1";

	if($list && isset($list['pbtn2_request']))
	{
	  $pbtn2 =  " btn-success provision-release-request";
	  $rbutton_title2 = "Zur Freigabe gesendet";
	}
}
elseif($email==config('users.falk_email'))
{
	$rbutton_title2 = "Freigeben";
	$pbtn2 = 'btn btn-primary provision-release-request';
	$pbtn2_type = "pbtn2";

	if($list && isset($list['pbtn1_request']))
	{
	  $pbtn1 =  "btn-success provision-release-request";
	  $rbutton_title1 = "Zur Freigabe gesendet";
	}
}
else{
	if($list && isset($list['pbtn1_request']))
	{
	  $pbtn1 =  " btn-success provision-release-request";
	  $rbutton_title1 = "Zur Freigabe gesendet";
	}

	if($list && isset($list['pbtn2_request']))
	{
	  $pbtn2 =  " btn-success provision-release-request";
	  $rbutton_title2 = "Zur Freigabe gesendet";
	}
}

$r1 = $r2 = "";
if($list && isset($list['pbtn1']))
{
  $pbtn1 =  " btn-success";
  $rbutton_title1 = "Freigegeben";
  foreach($list['pbtn1'] as $k=>$list2)
  	$r1 = "Janine: ".show_date_format($list2->created_at);
}

if($list && isset($list['pbtn2']))
{
  $pbtn2 =  " btn-success";
  $rbutton_title2 = "Freigegeben";
  foreach($list['pbtn2'] as $k=>$list2)
  	$r2 = "Falk: ".show_date_format($list2->created_at);
}

if($f==1):
?>
<button style="margin-right:10px; " type="button" class="btn {{$pbtn1}}" data-column="{{$pbtn1_type}}"> {{$rbutton_title1}}</button>
<?php else: ?>
<button style="margin-left: 23px;margin-right:10px;" type="button" class="btn {{$pbtn2}}" data-column="{{$pbtn2_type}}"> {{$rbutton_title2}}</button>
<?php endif; ?>
