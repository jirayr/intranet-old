@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">{{__('property.manage_property')}}</div>
 
                <div class="table-responsive">
                    <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th style="width: 70px;" class="text-center">#</th>
                            <th>{{__('property.name_of_property')}}</th>
                            <th>{{__('property.listing.creator_username')}}</th>
                            <th>mit echtem EK</th>
                            <th>aus Anleihe</th>
                            <th>Bankkredit</th>
                            <th>Zins Bankkredit</th>
                            <th>Tilgung Bank</th>
                            <th>Zins Anleihe</th>
                            
                            <th>{{__('property.listing.status')}}</th>
                            <th>{{__('property.listing.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($properties as $property)

                        <?php
                        $main_id = $property->main_property_id;
                        $name_of_property = $property->name_of_property;
                        // if(isset($_REQUEST['bank_id'])){
                        //     if($_REQUEST['bank_id']){
                        //         $property  =  DB::table('properties')->where('properties.Ist', $_REQUEST['bank_id'])->where('main_property_id',$main_id)->first();
                        //         if($property)
                        //             $name_of_property = $property->name_of_property;

                        //     }
                        // }

                        ?>
                        <tr>
                                <td class="text-center"><br>{{$main_id}}</td>
                                <td><br>{{$name_of_property}}</td>
                                <td><br>{{$property->name_of_creator}}</td>
                                <td><br>{{number_format($property->with_real_ek*100,2,",",".")}}</td>
                                <td><br>{{number_format($property->from_bond*100,2,",",".")}}</td>
                                <td><br>{{number_format($property->bank_loan*100,2,",",".")}}</td>
                                <td><br>{{number_format($property->interest_bank_loan*100,2,",",".")}}</td>
                                <td><br>{{number_format($property->eradication_bank*100,2,",",".")}}</td>
                                <td><br>{{number_format($property->interest_bond*100,2,",",".")}}</td>
                                <td>
				    <select class="form-control property-status" data-property-id="{{$main_id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
                                        <option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
                                        <option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
                                        <option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
                                        <option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
                                        <option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
                                        <option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
                                        <option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
                                        <option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
                                        <option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
                                        <option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
                                        <option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
                                        <option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
                                    </select>
                                </td>
                                <td>

                                    {!! Form::open(['action' => ['PropertiesController@destroy', $main_id],
                                    'method' => 'DELETE']) !!}
                                    <button onclick="location.href='{{route('properties.show',['property'=>$main_id])}}'"
                                            type="button"
                                            class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                                class="ti-eye"></i></button>
                                    <button onclick="location.href='{{route('properties.show',['property'=>$main_id])}}'"
                                            type="button"
                                            class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                                class="ti-pencil-alt"></i></button>
                                    <button type="submit" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"
                                            onclick="return confirm('{{__("property.confirm")}}')">
                                        <i class="icon-trash"></i></button>

                                    {!! Form::close() !!}

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper" style="margin-left: 20px">
                        <div class="row pull-left">
                                {{$properties->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="Modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Choose a Bank</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label>{{__('property.select_a_bank')}}: </label>
		<select id="bank">
		@foreach ($banks as $bank)
			<option value="{{$bank->id}}" data-with_real_ek="{{$bank->with_real_ek}}" data-from_bond="{{$bank->from_bond}}"
			data-bank_loan="{{$bank->bank_loan}}" data-interest_bank_loan="{{$bank->interest_bank_loan}}" 
			data-eradication_bank="{{$bank->eradication_bank}}" data-interest_bond="{{$bank->interest_bond}}"
			>{{$bank->name}}</option>
		@endforeach
		</select>
      </div>
      <div class="modal-footer">
        <button id="modal_save_btn" type="button" class="btn btn-primary">{{__('property.save_changes')}}</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
		
			var propertyId;
            $('.property-status').on('change', function () {
				if($(this).val()=="1"){
					$('#Modal').modal('show');
				}
                selection = $('.property-status');
                selection.prop( "disabled", true );
                propertyId = $(this).data('property-id');
                var status = $(this).val();
                var notificationType = $(this).data('notification-type');
//                console.log("Status: " + propertyId);
                var data = {
                    _token : '<?php echo csrf_token() ?>',
                    status : status,
                    property_id : propertyId
                };
                $.ajax({
                    type:'POST',
                    url:'{{route('properties.change_status')}}',
                    data: data,
                    success:function(data){
                        newNotification(propertyId, 3);
                    }
                });
            });
			$('#modal_save_btn').on('click', function () {
			$.ajax({
					method: "POST",
					url: "{{url('/property/select_bank')}}" + "/" + propertyId,
					data: { 
						with_real_ek: $('#bank option:selected').data("with_real_ek"),
						from_bond: $('#bank option:selected').data("from_bond"),
						bank_loan: $('#bank option:selected').data("bank_loan"),
						interest_bank_loan: $('#bank option:selected').data("interest_bank_loan"),
						eradication_bank: $('#bank option:selected').data("eradication_bank"),
						interest_bond: $('#bank option:selected').data("interest_bond"),
						bank: $('#bank option:selected').val()
						}
					})
					.success(function( response, newValue) {
					if( response.success === false )
                        return response.msg;
                    else
                       $('#Modal').modal('hide');
					console.log(propertyId);
				});
		})


        });
        
		
    </script>
@endsection