
<div class="row">
    <div class="card-header">
        <div class="row">
            <div class="col-md-10">
                <h4 class="card-title">Email</h4>
            </div>
        </div>
    </div>

    <div class="col-sm-12 table-responsive white-box">
        <table style="table-layout:fixed" class="table table-striped" id="inquiries">
            <thead>
            <tr>
                <th>Subject</th>
                <th>Body Preview</th>
                <th>Sender Email</th>
            </tr>
            </thead>
            <tbody>
            @isset($emails)
                @foreach($emails['value'] as $email)
                <tr>
                    <td>{{$email['subject']}}</td>
                    <td>{{$email['bodyPreview']}}</td>
                    <td>@isset($email['sender']['emailAddress']['address']){{$email['sender']['emailAddress']['address']}}@endisset</td>
                </tr>
            @endforeach
            @endisset
            </tbody>
        </table>
    </div>
</div>

