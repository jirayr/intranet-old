
    <td><input type="text" name="comment_1[]" class="form-control" value="{{$d->comment_1}}"></td>
    <td>
      @if($d->type=='Zusage')
      {{$d->tenant}}
      <input type="hidden" name="tenant[]" class="form-control" value="{{$d->tenant}}">
      @else
      <select class="form-control" name="tenant[]">    
        <option @if($d->tenant=='Sonstige Fläche') selected @endif value="Sonstige Fläche">Sonstige Fläche</option>
        @foreach($tenant_name as $d1)
        <option @if($d->tenant==$d1) selected @endif value="{{$d1}}">{{$d1}}</option>
        @endforeach
        </select>
        @endif
    </td>
    <?php
    $clss = "";
    if($d->type=='Zusage')
      $clss = "select-bg-green";
    if($d->type=='Mietvertrag ausgetauscht')
      $clss = "select-bg-yellow";
    if($d->type=='Absage')
      $clss = "select-bg-red";

    ?>
    <td>
        <select class="form-control lease-type {{$clss}}" name="type[]">
<option @if($d->type=='Zusage') selected @endif value="Zusage">Zusage</option>
<option @if($d->type=='Mietvertrag ausgetauscht') selected @endif value="Mietvertrag ausgetauscht">Mietvertrag ausgetauscht </option>
<option @if($d->type=='Besichtigung') selected @endif value="Besichtigung">Besichtigung</option>
<option @if($d->type=='Expose verschickt') selected @endif value="Expose verschickt">Expose verschickt</option>
<option @if($d->type=='angefragt') selected @endif value="angefragt">angefragt</option>
<option @if($d->type=='vorgemerkt') selected @endif value="vorgemerkt">vorgemerkt</option>
<option @if($d->type=='Absage') selected @endif value="Absage">Absage</option>
<option @if($d->type=='In Prüfung') selected @endif value="In Prüfung">In Prüfung</option>
</select>
    </td>
    <td>
      <textarea name="comment_2[]" class="form-control">{{$d->comment_2}}</textarea>
    </td>
    <td>{{date('d.m.Y',strtotime($d->created_at))}}</td>
    <td>
        
          <button type="button" class="btn-primary btn-sm save-row-mieter" data-id="{{$d->id}}">Speichern</button>

    </td>
