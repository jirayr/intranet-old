@if((sizeof($contents) > 0))
    <table class="table table-responsive table-condensed table-striped hidden-xs table-list-view">
        <thead>
            <th style='width:50%;'>{{ Lang::get('lfm.title-item') }}</th>
            <th>{{ Lang::get('lfm.title-size') }}</th>
            <th>{{ Lang::get('lfm.title-type') }}</th>
            <th>{{ Lang::get('lfm.title-modified') }}</th>
            <th>{{ Lang::get('lfm.title-action') }}</th>
        </thead>
        <tbody>
            @foreach($contents as $item)
            <tr>
                @php
                    $notPermission = $permission->where('basename', $item['basename'])->first();
                @endphp
                @if($notPermission)
                    <td>
                        <i class="fa fa-folder-o"></i>
                        <span title="{{ $item['name'] }}">
                            {{ str_limit($item['name'], $limit = 40, $end = '...') }}
                        </span>
                    </td>
                    <td></td>
                    <td>{{ Lang::get('lfm.type-folder') }}</td>
                    <td>{{ \Carbon\Carbon::createFromTimestamp($item['timestamp'], 'UTC')->setTimezone('Europe/berlin')->format('d.m.Y h:i:s') }}</td>
                    <td class="actions"> 
                        @if($user->role == 1)
                            <a href="javascript:trashDir('{{ $item['path']}}')" title="{{ Lang::get('lfm.menu-delete') }}">
                                <i class="fa fa-trash fa-fw"></i>
                            </a>
                            <a href="javascript:renameDir('{{ $item['path'] }}', '{{ $item['dirname'] }}', '{{ $item['filename'] }}')" title="{{ Lang::get('lfm.menu-rename') }}">
                                <i class="fa fa-edit fa-fw"></i>
                            </a>
                            <a data-id="{{ $item['basename'] }}" class="permission" data-icon="fa-folder-o" data-file_name="{{ $item['filename'] }}"  title="{{ Lang::get('lfm.menu-permission') }}">
                                <i class="fa fa-pencil fa-fw"></i>
                            </a>
                            <a data-id="{{ $item['basename'] }}" data-username="{{ $user->name }}" data-type="dir" class="button-open-share-model-gdrive-folder" title="{{ Lang::get('lfm.menu-share') }}">
                                <i class="fa fa-share fa-fw"></i>
                            </a>
                        @endif
                    </td>           
                
        
        
                @elseif($item['type'] == 'dir')
                    <td>
                        <i class="fa fa-folder-o"></i>
                        <a class="folder-item clickable" data-id="{{ $item['path'] }}" title="{{ $item['name'] }}">
                            {{ str_limit($item['name'], $limit = 40, $end = '...') }}
                        </a>
                    </td>
                    <td></td>
                    <td>{{ Lang::get('lfm.type-folder') }}</td>
                    <td>{{ \Carbon\Carbon::createFromTimestamp($item['timestamp'], 'UTC')->setTimezone('Europe/berlin')->format('d.m.Y h:i:s') }}</td>
                    <td class="actions"> 
                        <a href="javascript:trashDir('{{ $item['path']}}')" title="{{ Lang::get('lfm.menu-delete') }}">
                            <i class="fa fa-trash fa-fw"></i>
                        </a>
                        <a href="javascript:renameDir('{{ $item['path'] }}', '{{ $item['dirname'] }}', '{{ $item['filename'] }}')" title="{{ Lang::get('lfm.menu-rename') }}">
                            <i class="fa fa-edit fa-fw"></i>
                        </a>
                        @if($user->role == 1)
                            <a data-id="{{ $item['basename'] }}" class="permission" data-icon="fa-folder-o" data-file_name="{{ $item['filename'] }}"  title="{{ Lang::get('lfm.menu-permission') }}">
                                <i class="fa fa-pencil fa-fw"></i>
                            </a>
                        @endif
                        <a data-id="{{ $item['basename'] }}" data-username="{{ $user->name }}" data-type="dir" class="button-open-share-model-gdrive-folder" title="{{ Lang::get('lfm.menu-share') }}">
                            <i class="fa fa-share fa-fw"></i>
                        </a>
                    </td>
                @else      
                    <td>
                        <i class="fa {{ config('filemanager.file_icon_array.' . $item['extension']) ?: 'fa-file' }}" ></i>
                        <a target="_blank"  title="{{ $item['name'] }}"  href="https://drive.google.com/file/d/{{$item['basename']}}">        
                            {{ str_limit($item['name'], $limit = 40, $end = '...') }}
                        </a>
                    </td>
                    <td>{{ $item['size'] }}</td>
                    <td>{{ config('filemanager.file_type_array.' . $item['extension']) ?: 'File' }}</td>
                    <td>{{ \Carbon\Carbon::createFromTimestamp($item['timestamp'], 'UTC')->setTimezone('Europe/berlin')->format('d.m.Y h:i:s') }}</td>

                    <td class="actions">
                        <a href="javascript:download('{{ $item['basename']}}', '{{ $item['dirname']}}')" title="{{ Lang::get('lfm.menu-download') }}">
                            <i class="fa fa-download fa-fw"></i>
                        </a>
                        <a href="javascript:trash('{{ $item['path']}}')" title="{{ Lang::get('lfm.menu-delete') }}">
                            <i class="fa fa-trash fa-fw"></i>
                        </a>
                        <a href="javascript:rename('{{ $item['path'] }}', '{{ $item['dirname'] }}', '{{ $item['filename'] }}', '{{ $item['extension'] }}')" title="{{ Lang::get('lfm.menu-rename') }}">
                            <i class="fa fa-edit fa-fw"></i>
                        </a>
                        @if($user->role == 1)
                                <a data-id="{{ $item['basename'] }}" data-icon="{{ config('filemanager.file_icon_array.' . $item['extension']) ?: 'fa-file' }}" data-file_name="{{ $item['filename'] }}"   class="permission" title="{{ Lang::get('lfm.menu-rename') }}">
                                    <i class="fa fa-pencil fa-fw"></i>
                                </a>
                            @endif
                            <a 
                                data-id="{{ $item['basename'] }}" 
                                data-filename="{{ $item['name'] }}" 
                                data-mime_type="{{ $item['mimetype'] }}" 
                                data-username="{{ $user->name }}" 
                                data-type="file" 
                                class="button-open-share-model-gdrive-folder" title="{{ Lang::get('lfm.menu-share') }}">
                                <i class="fa fa-share fa-fw"></i>
                            </a>
                    </td>
                @endif     
            </tr>
            @endforeach
        </tbody>
    </table>


@else
<p>{{ trans('lfm.message-empty') }}  </p>
@endif
<!-- model  -->
<div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aia-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"> Freigabeeinstellungen</h4>
      </div>
			<form action="{{ url('/dir_permission') }}" role='form' id="permission_role" method='post' enctype='multipart/form-data'>
				{{ csrf_field() }}


				<div class="modal-body">
					<input class="form-control basename" type='hidden' name='basename' id='basename'>
                    {{--
					<div class="form-group">
						<label class="col-md-4" > Freigegeben für:</label>
						<input type="checkbox" class="chk_role role2" name="user_role" value="2" checked >{{__('user.role.user')}}
						<input type="checkbox" class="chk_role role4" name="user_role" value="4" checked>{{__('user.role.manager')}}
						<input type="checkbox" class="chk_role role5" name="user_role" value="5" checked>{{__('user.role.finance')}}
						<input type="checkbox" class="chk_role role3" name="user_role" value="3" checked>Guest
					</div>
                    --}}

                    <div class="form-group row">
						<label class="col-md-4" > Permission for: </label>
                        <div class="col-md-8" id="gdrive_permission_file_name">
                            
                        </div>

					</div>
                    <div class="form-group row">
						<label class="col-md-4" > Freigegeben für:</label>
                        <div class="col-md-8">
                            <input type="checkbox" class="user_permission_select_all" name="user_permission_all" value="0" checked >{{ trans('lfm.select-all') }}
                            <br />
                            <br />

                            @foreach($allUsers as $user)
                            <input type="checkbox" class="chk_user for_all_user_permission user{{$user->id}}"  id="gdrive_permission_for_user_{{$user->id}}" name="user_permission" value="{{$user->id}}" checked >{{$user->email}}
                            <br />
                            @endforeach
                        </div>

					</div>

      	        </div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">{{__('user.submit')}}</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('lfm.btn-close') }}</button>
				</div>
			</form>
    </div>
  </div>
</div> 
<!-- model -->
<!-- model  -->
<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aia-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="shareModalLabel"> Ordner teilen</h4>
      </div>
			

				<div class="modal-body">
					<div class="form-group">
					    <button type="button" class="btn btn-default" id="share-gdrive-folder-copy-button" >{{ trans('lfm.btn-copy') }}</button>
					    <a type="button" href="javascript: void(0);" class="btn btn-default" id="share-gdrive-folder-mail-button" >{{ trans('lfm.btn-mail') }}</a>

                        <a type="button" href="javascript: void(0);" class="btn btn-default hide" id="send-gdrive-file-as-attachment-button" >{{ trans('lfm.btn-send-mail-attachment') }}</a>
                    </div>


					<div class="form-group">
					    <input class="form-control gdrive-share-link" type='text' name='gdrive-share-link' id='gdrive-share-link'>
					</div>


                    

      	        </div>
                  <div class="modal-body send-gdrive-file-attach-model-body hide">
                    <hr style="margin: 3px 0px;">
                    <hr style="margin: 3px 0px;">
                    <span id="send-gdrive-file-attach_msg"></span>
                    <form action="{{ url('/file-manager/send-gdrive-file-attachment-mail') }}" method="POST" id="send-gdrive-file-attach-form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="file_basename" id="send-gdrive-file-attach-input-file_basename" value="">
                        <input type="hidden" name="filename" id="send-gdrive-file-attach-input-filename" value="">
                        <input type="hidden" name="mime_type" id="send-gdrive-file-attach-input-mime_type" value="">
                        <div class="form-group ">
                            <label for="send-gdrive-file-attach-input-to">To Email</label>
                            <input type="text" class="form-control" name="to_mail" id="send-gdrive-file-attach-input-to">
                        </div>
                        <div class="form-group ">
                            <label for="send-gdrive-file-attach-input-subject">Subject</label>
                            <input type="text" class="form-control" name="subject" id="send-gdrive-file-attach-input-subject" value="Ordnerfreigabe von ">
                        </div>
                        <div class="form-group ">
                            <label for="send-gdrive-file-attach-input-body">Message</label>
                            <textarea class="form-control" name="message" id="send-gdrive-file-attach-input-body">Hallo, sieh dir diesen Anhang an</textarea>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="send-gdrive-file-attach-send-button" >{{ trans('lfm.btn-send') }}</button>
                        </div>

                    </form>


                    

      	        </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('lfm.btn-close') }}</button>
				</div>
    </div>
  </div>
</div> 
<!-- model -->