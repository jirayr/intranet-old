<ul class="list-unstyled">
    @foreach($contents as $root_folder)
        @php
            $notPermission = $permission->where('basename', $root_folder['basename'])->first();
        @endphp
        @if($notPermission)
            <li>
                <span>
                    <i class="fa fa-folder"></i> 
                    {{ str_limit($root_folder['name'], $limit = 20, $end = '...') }}
                </span>
            </li>
        @else
            <li>
                <a class="clickable folder-item" data-id="{{ $root_folder['path'] }}">
                    <i class="fa fa-folder"></i> 
                    {{ str_limit($root_folder['name'], $limit = 20, $end = '...') }}
                </a>
            </li>
        @endif    
    @endforeach
</ul>
