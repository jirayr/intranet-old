@if((sizeof($files) > 0))

<table class="table table-responsive table-condensed table-striped hidden-xs table-list-view">
	<thead>
		<th style='width:80%;'>File</th>
		<th>Action</th>
	</thead>
	<tbody class="mytable">

		@foreach($files as $item)

		<tr>

			<td>
				@if($item['file_type'] == "file")
					<i class="fa {{ config('filemanager.file_icon_array.' . $item['extension']) ?: 'fa-file' }}"></i>
						<a target=“_blank” href="https://drive.google.com/file/d/{{$item['file_basename']}}">
							{{$item['file_name']}}
					</a>
				@else
					<i class="fa fa-folder }}"></i>
					{{$item['file_name']}}
				@endif
			</td>
			<td class="actions">
					<a class="deleteGdriveFileLink" href="javascript:void(0);" data-id="{{$item['id']}}" data-objData="{{ json_encode($objData)}}"><i class="fa fa-trash" style="text-align: right;width: -webkit-fill-available;display: block;font-size: 14px;"></i></a>
			</td>

		</tr>
		@endforeach
	</tbody>
</table>

@else
<p>Empty</p>
@endif