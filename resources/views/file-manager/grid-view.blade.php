@if((sizeof($contents) > 0))

<div class="row">

  @foreach($contents as $item)
  <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 img-row">
    <?php $item_name = $item['name']; ?>
    <?php $thumb_src = false; ?>
    @if($item['type'] == 'dir')
      <div class="square clickable folder-item" data-id="{{ $item['path'] }}">
        <i class="fa fa-folder-o fa-5x"></i>
      </div>
      <div class="caption text-center">
        <div class="btn-group">
          <button type="button" data-id="{{ $item['path'] }}" class="item_name btn btn-default btn-xs folder-item">
            {{ $item['name'] }}
          </button>

          <button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>


        </div>
      </div>
    @else
      <div class="square clickable folder-item" data-id="{{ $item['path'] }}">
        <i class="fa {{ config('filemanager.file_icon_array.' . $item['extension']) ?: 'fa-file' }} fa-5x"></i>
      </div>
      <div class="caption text-center">
        <div class="btn-group">
          <button type="button" data-id="{{ $item['path'] }}" class="item_name btn btn-default btn-xs"  onclick="download('{{ $item['basename']}}', '{{ $item['dirname']}}')">
              {{ $item['name'] }}
          </button>

          <button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          
        </div>
      </div>
    @endif




    

    {{--
    <div class="caption text-center">
      <div class="btn-group">
        
        <ul class="dropdown-menu" role="menu">
          <li><a href="javascript:rename('{{ $item_name }}')"><i class="fa fa-edit fa-fw"></i> {{ Lang::get('laravel-filemanager::lfm.menu-rename') }}</a></li>
          @if($item->is_file)

          <li><a href="javascript:download('{{ $item_name }}')"><i class="fa fa-download fa-fw"></i> {{ Lang::get('laravel-filemanager::lfm.menu-download') }}</a></li>
          <li class="divider"></li>
          @if($thumb_src)

          <li><a href="javascript:fileView('{{ $item_path }}', '{{ $item->updated }}')"><i class="fa fa-image fa-fw"></i> {{ Lang::get('laravel-filemanager::lfm.menu-view') }}</a></li>
          <li><a href="javascript:resizeImage('{{ $item_name }}')"><i class="fa fa-arrows fa-fw"></i> {{ Lang::get('laravel-filemanager::lfm.menu-resize') }}</a></li>
          <li><a href="javascript:cropImage('{{ $item_name }}')"><i class="fa fa-crop fa-fw"></i> {{ Lang::get('laravel-filemanager::lfm.menu-crop') }}</a></li>
          <li class="divider"></li>

          @endif
          @endif
          <li><a href="javascript:trash('{{ $item_name }}')"><i class="fa fa-trash fa-fw"></i> {{ Lang::get('laravel-filemanager::lfm.menu-delete') }}</a></li>
        </ul>
      </div>
    </div> --}}

  </div>
  @endforeach

</div>

@else
<p>{{ trans('lfm.message-empty') }}</p>
@endif
