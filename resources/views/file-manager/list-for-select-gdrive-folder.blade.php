@if((sizeof($contents) > 0))
<table class="table table-responsive table-condensed table-striped hidden-xs table-list-view">
  <thead>
    <th style='width:80%;'>{{ Lang::get('lfm.title-item') }}</th>
    <th>{{ Lang::get('lfm.title-action') }}</th>
  </thead>
  <tbody>
    @foreach($contents as $item)
      @php
        $notPermission = $permission->where('basename', $item['basename'])->first();
        if($notPermission)
        {
          continue;
        }
      @endphp
      
      <tr>
      @if($item['type'] == 'dir')
        <td>
          <i class="fa fa-folder-o"></i>
          <a class="" href="javascript:selectGdriveFolderLoad('{{ $item['path'] }}');" data-id="{{ $item['path'] }}" title="{{ $item['name'] }}">
            {{ str_limit($item['name'], $limit = 40, $end = '...') }}
          </a>
        </td>

        <td class="actions">
          <a class="dirSelect" href="javascript:void(0);" onClick="gdriveFolderSelectDir('{{ $item['basename']}}', '{{ $item['dirname']}}' )" >
            select
          </a>
        </td>
      @endif

      </tr>
    @endforeach
  </tbody>
</table>

@else
<p>{{ trans('lfm.message-empty') }}</p>
@endif
