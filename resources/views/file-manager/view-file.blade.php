@if((sizeof($propertyEinkaufFiles) > 0))

<table class="table table-responsive table-condensed table-striped hidden-xs table-list-view">

	<tbody class="mytable">

		@foreach($propertyEinkaufFiles as $item)
		@if($item['type'] == 'dir')
		@else
		<tr class="display_image">

			<td>
				<i class="fa {{ config('filemanager.file_icon_array.' . $item['extension']) ?: 'fa-file' }}"></i>
				@if($item['type'] == "file")
					<a target=“_blank” href="https://drive.google.com/file/d/{{$item['basename']}}">
				@endif
					{{$item['name']}}
				</a>
			</td>

		</tr>
		@endif
		@endforeach
	</tbody>
</table>

@else
<p>Empty</p>
@endif