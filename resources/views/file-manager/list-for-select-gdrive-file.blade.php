@if((sizeof($contents) > 0))
<table class="table table-responsive table-condensed table-striped hidden-xs table-list-view">
  <thead>
    <th style='width:80%;'>{{ Lang::get('lfm.title-item') }}</th>
    <th>Datum</th>
    <th>{{ Lang::get('lfm.title-action') }}</th>
  </thead>
  <tbody>
    @foreach($contents as $item)
      @php
        $notPermission = $permission->where('basename', $item['basename'])->first();
        if($notPermission)
        {
          continue;
        }
      @endphp

      <tr>
      @if($item['type'] == 'dir')
        <td>
          <i class="fa fa-folder-o"></i>
          <a class="" href="javascript:selectGdriveFileLoad('{{ $item['path'] }}');" data-id="{{ $item['path'] }}" title="{{ $item['name'] }}">
            {{ str_limit($item['name'], $limit = 40, $end = '...') }}
          </a>
        </td>
        <td></td>
        <td class="actions">
          <a class="dirSelect" href="javascript:void(0);" onClick="gdriveFileSelectDir('{{ $item['basename']}}', '{{ $item['dirname']}}', this )" >
            select
          </a>
        </td>
      @else
        <td>
          <i class="fa {{ config('filemanager.file_icon_array.' . $item['extension']) ?: 'fa-file' }}" ></i>
          <a target="_blank"  title="{{ $item['name'] }}" class="gdrive-file-link"  href="https://drive.google.com/file/d/{{$item['basename']}}">
          {{ str_limit($item['name'], $limit = 40, $end = '...') }}
          </a>
        </td>
        <td>{{date('m/d/Y H:i:s', $item['timestamp'])}}</td>
        <td class="actions">
            <a href="javascript:void(0);" onClick="gdriveFileSelectFile('{{ $item['basename']}}', '{{ $item['dirname']}}', this)" >
              select
            </a>
        </td>
      @endif

      </tr>
    @endforeach
  </tbody>
</table>

@else
<p>{{ trans('lfm.message-empty') }}</p>
@endif
