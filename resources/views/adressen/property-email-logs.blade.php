@extends('layouts.admin')
@section('content')

<div class="row">
    <div class="card-header">
        <div class="row">
            <div class="col-md-8">
                <h4 class="card-title">Email Logs</h4>
            </div>

        </div>
    </div>

    <div  style="overflow-x:auto;  white-space: nowrap;" class="col-sm-12 table-responsive white-box">
        <table   class="table table-responsive table-striped" id="inquiries">
            <thead>
            <tr>
                <th>File Name</th>
                <th>Email Sent To</th>
                <th>Sent By</th>
                <th>Sender Email</th>
                <th>Date</th>
            </tr>
            </thead>
            @if($propertyAddressesJobs)

            <tbody class=" table-responsive">
                <tr>
                    @foreach($propertyAddressesJobs->emailLogs as $data)
                    <td>{{$propertyAddressesJobs->result}}</td>
                    <td>{{$data->email}}</td>
                    <td>{{$data->creator->name}}</td>
                    <td>{{$data->creator->email}}</td>
                    <td>{{$data->created_at}}</td>
                    @endforeach
                </tr>
            </tbody>
            @endif

        </table>

    </div>
</div>


@endsection
@section('js')
    <script src="http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function() {


        var columns = [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ];
        $('#inquiries').dataTable({
            "pageLength": 100

        });

    });
</script>
@stop
