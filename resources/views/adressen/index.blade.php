@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8">
                    <h4 class="card-title">Adressen</h4>
                </div>

                <div class="col-md-4">
                    <a class="config btn btn-primary">Email Config</a>
                    <a href="{{url('address/email/logs')}}" class="btn btn-primary">Email Logs</a>
                    <a href="{{url('adressen/create')}}" class="btn btn-primary">Hinzufügen</a>
                </div>
            </div>
        </div>

        <div  style="overflow-x:auto;  white-space: nowrap;" class="col-sm-12 table-responsive white-box">
            <table  class="table table-responsive table-striped" id="inquiries">
                <thead>
                <tr>
                    <th>Keyword</th>
                    <th>Limit</th>
                    <th>Status</th>
                    <th>Scrape Type</th>
                    <th>File</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody class=" table-responsive">


                @isset($addressesJobs)
                    @foreach($addressesJobs as $addressesJob)
                        <tr>
                            <td>{{$addressesJob->keyword}}</td>
                            <td>{{$addressesJob->limit_max}}</td>
                            <td>{{$addressesJob->status}}</td>
                            <td>{{$addressesJob->type}}</td>
                            <td><a target="_blank" href="{{url('/excel',$addressesJob->result)}}">{{$addressesJob->result}}</a></td>
                            <td><label><input type="checkbox" data-id="{{$addressesJob->id}}" @if($addressesJob->is_status_checked)checked @endif class="checkbox-is-sent" value=""></label></td>
                            <td class="">

                                @if(!$addressesJob->result)
                                    <a  href="{{route('file',$addressesJob->id)}}" class="btn-sm btn-info">Datei</a>
                                @else
                                    <a href="{{ asset('address/'.$addressesJob->result) }}" class="btn-sm btn-info">Herunterladen</a>
                                    <a href="" data-id="{{$addressesJob->id}}" id="csvEmail"  data-file="{{$addressesJob->result}}" data-toggle="modal"  class="email btn-sm btn-info">Send Email</a>
                                @endif
                                <a type="button" href="{{url('address/edit',$addressesJob->id)}}" class="btn-sm btn-info">Bearbeiten</a>
                            </td>
                        </tr>
                    @endforeach
                @endisset
                </tbody>
            </table>
        </div>
    </div>
    <div class=" modal fade" role="dialog" id="csv_email">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mail </h4>
                </div>
                <form  class="tagForm" id="tag-form"   action="{{ url('/sendEmail') }}" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="address_id"  id="address_id" value="" >
                        <label>Subject</label>
                        <input id="subject" class="form-control"  type="text" name="subject" value="">
                        <span id="subject-field"   style="visibility:hidden;color: red;">This field is Required!</span>
                        <br>
                        <label>Body</label>
                        <textarea class="form-control" id="message"  name="message" required></textarea>
                        <span id="message-field"  style=" visibility:hidden;color: red;">This field is Required!</span>
                        <br>
                        <label for="email">Emails:</label>
                        <select  required class="form-control "  id="emailSelector" name="file_emails[]"  style="width: 100%"></select>
                        <span id="file_emails"  style=" visibility:hidden;color: red;">This field is Required!</span>
                        <br>
                        <label>Attachment</label>
                        <input type="file" id="file" />
                        <span id="attachment-field"  style="visibility:hidden;color: red;">This field is Required!</span>
                        <br>

                    </div>
                    <div class="modal-footer">
                        <span id="loader" style="visibility:hidden"><i class="fa fa-spin fa-spinner"></i></span>
                        <a id="tag-form-submit" class="btn btn-primary" >Senden</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class=" modal fade" role="dialog" id="emailConfigModel">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Configuration </h4>
                </div>
                <form  class="config-form" id="config-form"   action="{{ url('/sendEmail') }}" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="address_id"  id="address_id" value="" >
                        <label>Emails :</label>
                        <select id="multi-selector"  class="form-control  search" required name="email[]"  style="width: 100%"></select>
                        <span id="subject-field"   style="visibility:hidden;color: red;">This field is Required!</span>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <span id="config-loader" style="visibility:hidden"><i class="fa fa-spin fa-spinner"></i></span>
                        <button id="save-config" onsubmit="sendEmail()" class="btn btn-primary" >Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>



        $('.email').on('click', function () {

            $('#attachment-field').css('visibility', 'hidden');
            $('#subject-field').css('visibility', 'hidden');
            $('#message-field').css('visibility', 'hidden');
            $('#file_emails').css('visibility', 'hidden');
            $("#subject").val('');
            $("#file").val('');
            $('#emailSelector').html('');
            CKEDITOR.replace( 'message' );
            CKEDITOR.instances['message'].destroy(true);
            CKEDITOR.replace( 'message' );

            var name = $(this).attr('data-file');
            var id = $(this).attr('data-id');
            $("#address_id").val(id);

            $.ajax({
                type : 'get',
                data: {
                    name: name,
                    _token: $('#token').val()
                },
                url : "{{url('/address/file-emails') }}",
                success : function (data)
                {
                    $.each(data, function(key) {
                        $('#emailSelector').append('<option  value="'+data[key]+'">'+data[key]+'</option>');
                    });

                }
            });

            $('#csv_email').modal('show');

        });

        $('#tag-form-submit').on('click', function () {
            var fd = new FormData();
            var files = $('#file')[0].files[0];
            if (files){
                fd.append('attachment',files);
            }
            fd.append('token',$('#token').val());
            fd.append('address_id',$('#address_id').val());
            fd.append('subject',$('#subject').val());
            fd.append('message',CKEDITOR.instances['message'].getData());
            fd.append('emails',$('.search').val());
            fd.append('file_emails',$('#emailSelector').val());

            console.log($('#emailSelector').val());

            if ($('#subject').val() === '')
            {
                $('#subject-field').css('visibility', 'visible');
            }


            if ($('#emailSelector').val() === null)
            {
                $('#file_emails').css('visibility', 'visible');
            }

            if(CKEDITOR.instances['message'].getData() === ''){

                $('#message-field').css('visibility', 'visible');

            }
            if ($('#subject').val() !== ''  &&  CKEDITOR.instances['message'].getData() !== '' && $('#emailSelector').val()!== null) {
                $('#loader').css('visibility', 'visible');

                $('#attachment-field').hide();
                $('#subject-field').hide();
                $('#message-field').hide();
                $('#file_emails').hide();

                $('#tag-form-submit').addClass('disabled');

                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }

                $.ajax({
                    type : 'post',
                    url : "{{url('/sendEmail') }}",
                    data : fd,
                    contentType: false,
                    processData: false,
                    success : function (data)
                    {
                        var message = "Emails Sent Successfully";
                        $('#loader').css('visibility', 'hidden');
                        $('#csv_email').modal('hide');
                        $('#tag-form-submit').removeClass('disabled');
                        if (data){
                            message = data;
                        }
                        window.setTimeout(function(){
                            sweetAlert(message, "success");
                        }, 600);



                    }
                });

            }

        });

    </script>

@endsection
@section('scripts')


    <script>

        $(document).ready(function() {

            $(".search").select2({

                minimumInputLength: 2,
                multiple: true,
                tags: true,
            });

            $("#emailSelector").select2({

                multiple: true,
                tags: true,
            });


        });



        $(document).ready(function () {
            $('#config-form').submit(function (e) {
                $('#config-loader').css('visibility', 'visible');

                e.preventDefault();
                $.ajax({
                    type : 'post',
                    url : "{{url('/emailConfig') }}",
                    data :   $('form.config-form').serialize(),
                    success : function (data)
                    {
                        $('#config-loader').css('visibility', 'hidden');
                        $('#save-config').removeClass('disabled');
                        window.setTimeout(function(){
                            $('#emailConfigModel').modal('hide');
                            sweetAlert("Emails Saved Successfully");
                        }, 600);
                    }
                });

            });
        });

        $('.config').on('click', function () {
            document.querySelector('#multi-selector').required = true;

            $('#emailConfigModel').modal('show');
            $('#multi-selector').val('');
            $.ajax({
                type : 'get',
                url : "{{url('/emailConfig') }}",
                success : function (data)
                {
                    if(data[0] !== '' ){
                        document.querySelector('#multi-selector').required = false;
                        $.each(data, function(key) {
                            $('#multi-selector').append('<option selected value="'+data[key]+'">'+data[key]+'</option>');
                        });
                    }

                }
            });
        });


        $('body').on('click', '.checkbox-is-sent', function() {
            var id = $(this).attr('data-id');
            v = 0;
            if ($(this).is(':checked')) v = 1;
            $.ajax({
                type: 'POST',
                url: 'demo/update' + "/" + id,
                data: {
                    is_status_checked: v,
                    _token: $('#token').val()
                },
                success: function(data) {
                    sweetAlert("Status Updated Successfully");

                }
            });
        });



    </script>
@stop
