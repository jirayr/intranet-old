@extends('layouts.admin')
@section('content')

    <form method="POST" action="{{url('/demo/update',$id)}}" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group" style="width: 50%">
            <label for="email">Result:</label>
            <input required type="file" class="form-control" placeholder="Enter email" name="file_data" id="email">
        </div>
        <div class="form-group">
            <label for="pwd">Status:</label>
            <div class="form-group" style="width: 50%">
                <select required class="form-control" id="sel1" name="status">
                    <option value="unprocessed">unprocessed</option>
                    <option value="Pending">Pending</option>
                    <option value="processing">processing</option>
                    <option value="completed">completed</option>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
@stop