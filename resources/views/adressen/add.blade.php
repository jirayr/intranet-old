@extends('layouts.admin')
@section('content')
    <form  method="post" action="{{url('/adressen')}}">
         <div class="form-group" style="width: 50%">
            <label for="email">Keyword:</label>
            <input required type="text" class="form-control" name="keyword" maxlength="105"  placeholder="Enter Keyword" id="email">
        </div>
        <div class="form-group" style="width: 50%">
            <label for="email">Limit:</label>
            <input required type="number" class="form-control" name="limit_max"   placeholder="Enter Limit">
        </div>
        <div class="form-group" style="width: 50%">
            <label for="email">Scrape Type:</label>
            <select class="form-control" name="type" id="">
                <option value="">Select</option>
                <option value="Web Scrape">Web Scrape</option>
                <option value="Business Scrape">Business Scrape</option>
            </select>   
        </div>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <button type="submit" class="btn btn-primary">Save</button>
    </form>
@stop