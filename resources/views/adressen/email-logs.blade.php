@extends('layouts.admin')
@section('content')
    <style>

        .dataTables_wrapper .dataTables_processing {
            width: 50px;
            padding: 7px;
            left: 50%;
            margin-left: -100px;
            margin-top: 10px;
            text-align: center;
            /*background-color:transparent;*/
            background: none !important;
            vertical-align: middle;
            border-style: none;
            /*-webkit-box-shadow: 0 1px 8px rgba(0,0,0,.1);*/
            /*-moz-box-shadow: 0 1px 8px rgba(0,0,0,.1);*/
            /*box-shadow: 0 1px 8px rgba(0,0,0,.1);*/
        }

        .sk-chase {
            width: 40px;
            height: 40px;
            position: relative;
            animation: sk-chase 2.5s infinite linear both;
        }

        .sk-chase-dot {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
            animation: sk-chase-dot 2.0s infinite ease-in-out both;
        }

        .sk-chase-dot:before {
            content: '';
            display: block;
            width: 25%;
            height: 25%;
            background-color: #000000;
            border-radius: 100%;
            animation: sk-chase-dot-before 2.0s infinite ease-in-out both;
        }

        .sk-chase-dot:nth-child(1) { animation-delay: -1.1s; }
        .sk-chase-dot:nth-child(2) { animation-delay: -1.0s; }
        .sk-chase-dot:nth-child(3) { animation-delay: -0.9s; }
        .sk-chase-dot:nth-child(4) { animation-delay: -0.8s; }
        .sk-chase-dot:nth-child(5) { animation-delay: -0.7s; }
        .sk-chase-dot:nth-child(6) { animation-delay: -0.6s; }
        .sk-chase-dot:nth-child(1):before { animation-delay: -1.1s; }
        .sk-chase-dot:nth-child(2):before { animation-delay: -1.0s; }
        .sk-chase-dot:nth-child(3):before { animation-delay: -0.9s; }
        .sk-chase-dot:nth-child(4):before { animation-delay: -0.8s; }
        .sk-chase-dot:nth-child(5):before { animation-delay: -0.7s; }
        .sk-chase-dot:nth-child(6):before { animation-delay: -0.6s; }

        @keyframes sk-chase {
            100% { transform: rotate(360deg); }
        }

        @keyframes sk-chase-dot {
            80%, 100% { transform: rotate(360deg); }
        }

        @keyframes sk-chase-dot-before {
            50% {
                transform: scale(0.4);
            } 100%, 0% {
                  transform: scale(1.0);
              }
        }
        .select2-selection {
            overflow-y: auto!important;
            height: 70px!important;
        }
    </style>
<div class="row">
    <div class="card-header">
        <div class="row">
            <div class="col-md-8">
                <h4 class="card-title">Email Logs</h4>
            </div>

        </div>
    </div>

    <div  style="overflow-x:auto;  white-space: nowrap;" class="col-sm-12 table-responsive white-box">
        <table   class="table table-responsive table-striped" id="inquiries">
            <thead>
            <tr>
                <th>File Name</th>
                <th>Email Sent To</th>
                <th>Sent By</th>
                <th>Sender Email</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody class=" table-responsive">
            </tbody>
        </table>

    </div>
</div>


@endsection
@section('scripts')
<script>
    $(function() {
        $('#inquiries').DataTable({
            'language': {
                'loadingRecords': '&nbsp;',
                'processing':  '<div class="sk-chase">' +
                    '  <div class="sk-chase-dot"></div>' +
                    '  <div class="sk-chase-dot"></div>' +
                    '  <div class="sk-chase-dot"></div>' +
                    '  <div class="sk-chase-dot"></div>' +
                    '  <div class="sk-chase-dot"></div>' +
                    '  <div class="sk-chase-dot"></div>' +
                    '</div>'
            },

            processing: true,
            serverSide: true,
            ajax: '{!! url('address/logs') !!}',
            columns: [
                { data: 'addresses_job.result', name: 'email' },
                { data: 'email', name: 'email' },
                { data: 'creator.name', name: 'creator_name' },
                { data: 'creator.email', name: 'creator_email' },
                { data: 'created_at', name: 'created_at' }
            ]
        });
    });
</script>
@stop
