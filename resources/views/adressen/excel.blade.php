@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-sm-12 table-responsive white-box">
        <table style="" class="table table-striped" id="inquiries">
            <tbody>
            @isset($content)
            @foreach($content[0] as $contentData)
                <tr>
                @foreach($contentData as $data)
                        <td>{{$data}}</td>
                @endforeach
                </tr>
            @endforeach
            @endisset
            </tbody>
        </table>
    </div>
</div>

@endsection