@extends('layouts.admin')
@section('content')
    <form method="POST" action="{{url('/demo/update',$data->id)}}" enctype="multipart/form-data">
        <div class="form-group" style="width: 50%">
            <label>Keyword:</label>
            <input required type="text" class="form-control" name="keyword" placeholder="Enter Keyword" id="" value="{{$data->keyword}}">
        </div>
        <div class="form-group" style="width: 50%">
            <label for="email">Limit:</label>
            <input required type="number" class="form-control" name="limit_max"  value="{{$data->limit_max}}"  placeholder="Enter Limit">
        </div>
        <div class="form-group">
            <label for="pwd">Status:</label>
            <div class="form-group" style="width: 50%">
                <select required class="form-control" id="sel1" name="status">
                    <option @if($data->status == 'unprocessed')selected @endif  value="unprocessed">unprocessed</option>
                    <option @if($data->status == 'Pending')selected @endif value="Pending">Pending</option>
                    <option @if($data->status == 'processing')selected @endif value="processing">processing</option>
                    <option @if($data->status == 'completed')selected @endif value="completed">completed</option>
                </select>
            </div>
        </div>
        <div class="form-group" style="width: 50%">
            <label>Scrape Type:</label>
            <select class="form-control" name="type" id="">
                <option value="">Select</option>
                <option @if($data->type == 'Web Scrape')selected @endif value="Web Scrape">Web Scrape</option>
                <option @if($data->type == 'Business Scrape')selected @endif  value="Business Scrape">Business Scrape</option>
            </select>
        </div>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@stop