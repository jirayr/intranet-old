@extends('layouts.admin')

@section('content')
    <div class="row">

        {!! Form::open(['action' => ['QuestionController@update'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
        {!! Form::token() !!}
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">{{__('Create Question')}}</div>
                <div class="white-box">
						<div class="form-group">
                            <label class="col-md-2">Property</label>
                            <div class="col-md-10">
                                <select name="property" class="form-control" required>
									<option value="">--select property--</option>
									@foreach($properties as $property)
										<option value="{{$property->id}}">{{$property->name_of_property}}</option>
									@endforeach
								</select>
							</div>
                        </div>
						
						<div class="form-group">
                            <div class="text-center">
							<button type="button" onclick="defaultform(1)" class="btn btn-primary">Default Form 1</button>
							<button type="button" onclick="defaultform(2)" class="btn btn-primary">Default Form 2</button>
							<button type="button" onclick="defaultform(3)" class="btn btn-primary">Default Form 3</button>
								</div>
                        </div>
				
				
                        <div class="form-group">
                            <label class="col-md-2">{{__('Question heading')}}</label>
                            <div class="col-md-10">
                                <input type="text" name="qn_heading" class="form-control form-control-line" value="" placeholder="{{__('Question heading')}}"
                                       required>
								</div>
                        </div>
						
						<div class="form-group">
                            <label class="col-md-2">{{__('Question')}}</label>
                            <div class="col-md-10">
								<table class="table table-bordered qn-table">
									<thead>
										<tr class="text-center ">
											<td>Question</td>
											<td>Type</td>
											<td>Required</td>
											<td>Action</td>
										</tr>
									</thead>
									<tbody>
									<tr class="text-center "><td><input type="text" name="questions[question][]" class="form-control" value="" required></td>
										<td width="180">
											<select name="qn_type"  class="qn-type form-control">
												<option value="1">Normal questions</option>
												<option value="2">Objective questions</option>
											</select>
										</td>
										<td width="20">
											<input type="hidden" name="questions[required][]" value="0">
											<input type="checkbox"   onclick="this.previousElementSibling.value=1-this.previousElementSibling.value">
										</td>
										<td class="last-td" width="20"><span class="fa fa-plus-circle plus-color"  onclick="addmore(this)"></span></td>
									</tr>
									</tbody>
								</table>
							</div>
                        </div>
						<hr>
                        <div class="form-group">
                            <div class="col-lg-2 col-sm-4 col-xs-12">
                                <a href="{{ url('/question') }}" class="btn btn-block btn-danger btn-rounded">{{__('cancel')}}</a>
                            </div>
                            <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12">
                                <button type="submit" name="submit" class="btn btn-block btn-primary btn-rounded">{{__('Submit')}}</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
     {!! Form::close() !!}
    </div>

@endsection

@section('js')
<script>

	function addmore(plus){		
		var newrow=$(plus).parent().parent().clone();
		newrow.children().find('span').remove();
		newrow.children().find('input[type=text]').val('');
		newrow.children().find('.qn-type').val(1);
		newrow.children('td:first-child').html('<input type="text" name="questions[question][]" class="form-control" value="" required>');
		newrow.children('td[class=last-td]').append('<span class="fa fa-minus-circle  minus-color"  onclick="removeit(this)"></span>');
		$(plus).parent().parent().parent().append(newrow);
	}
	function removeit(minus){
		$(minus).parent().parent().remove();
	}
	
	function defaultform(Ftype){
		
		if(Ftype==1){
			$('[name=qn_heading]').val('Ihre Kontaktdaten');
			$('.qn-table tbody').html('<tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="Institut" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="1"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value" checked=""> </td>  <td class="last-td" width="20"> <span class="fa fa-plus-circle plus-color" onclick="addmore(this)"></span>  </td> </tr>   <tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="Anschrift" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="1"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value" checked=""> </td>  <td class="last-td" width="20"> <span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>  </td> </tr>   <tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="Ansprechpartner" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="1"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value" checked=""> </td>  <td class="last-td" width="20"> <span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>  </td> </tr>   <tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="Telefon" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="0"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value"> </td>  <td class="last-td" width="20"> <span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>  </td> </tr>   <tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="E-Mail" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="0"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value"> </td>  <td class="last-td" width="20"> <span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>  </td> </tr>');
		}
		else if(Ftype==2){
			$('[name=qn_heading]').val('Ihr Finanzierungsangebot');
			$('.qn-table tbody').html('<tr class="text-center"> <td> <input type="text" name="questions[question][0][qn]" class="form-control" value="Finanzierungstyp" required=""><br> <div class="row"> <div class="col-md-3"><input type="text" class="form-control" name="questions[question][0][op][]" value="Non-Recourse" required=""></div> <div class="col-md-3"><input type="text" class="form-control" name="questions[question][0][op][]" value="Option 2" required=""></div> <div class="col-md-3"><input type="text" class="form-control" name="questions[question][0][op][]" value="Keine Angabe" required=""></div> <div class="col-md-3"><input type="text" class="form-control" name="questions[question][0][op][]" value="Other" required=""></div>  </div> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2" selected="">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="0"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value"> </td>  <td class="last-td" width="20"> <span class="fa fa-plus-circle plus-color" onclick="addmore(this)"></span>  </td> </tr>   <tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="Kreditbetrag (€)" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="1"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value" checked=""> </td>  <td class="last-td" width="20"> <span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>  </td> </tr>   <tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="Variabler Zinssatz (%)" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="1"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value" checked=""> </td>  <td class="last-td" width="20"> <span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>  </td> </tr>   <tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="Tilgung (%)" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="1"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value" checked=""> </td>  <td class="last-td" width="20"> <span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>  </td> </tr>   <tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="Laufzeit (Jahre)" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"> <input type="hidden" name="questions[required][]" value="1"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value" checked=""> </td>  <td class="last-td" width="20"> <span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>  </td> </tr>');
		}
		else{
			$('[name=qn_heading]').val('Anmerkungen');
			$('.qn-table tbody').html('<tr class="text-center"> <td> <input type="text" name="questions[question][]" class="form-control" value="" required=""> </td>  <td width="180"> <select name="qn_type" class="qn-type form-control"> <option value="1">Normal questions</option> <option value="2">Objective questions</option> </select> </td> <td width="20"><input type="hidden" name="questions[required][]" value="0"> <input type="checkbox" onclick="this.previousElementSibling.value=1-this.previousElementSibling.value"> </td>  <td class="last-td" width="20"> <span class="fa fa-plus-circle plus-color" onclick="addmore(this)"></span>  </td> </tr>');
		}
	}
	
	
	$('body').on('change',".qn-type",function() {
		
		if($(this).val()==1)
		{
			$(this).parent().prev('td').html('<input type="text" name="questions[question][]" class="form-control" value="" required>');
		}
		else{
			var tdI=$(this).parent().parent('tr').index();
			$(this).parent().prev('td').html('<input type="text" name="questions[question]['+tdI+'][qn]" class="form-control" value="" required><br><div class="row"><div class="col-md-3"><input type="text" class="form-control" name="questions[question]['+tdI+'][op][]" required></div><div class="col-md-3"><input type="text" class="form-control" name="questions[question]['+tdI+'][op][]" required></div><div class="col-md-3"><input type="text" class="form-control" name="questions[question]['+tdI+'][op][]" required></div><div class="col-md-3"><input type="text" class="form-control" name="questions[question]['+tdI+'][op][]" required></div></div>');
		}
	});


</script>  
@endsection
