@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="panel">
                    <div class="panel-heading">{{__('Manage Questions')}} </div>
					<div class="panel-body">
						<div style="width:100%;"><a class="btn btn-primary pull-right" href="{{ url('question/create') }}">create</a></div>
						<div class="row">
							<div class="col-lg-12">
							
							
							<div class="table-responsive">
							
                    <table class="table table-hover question-table">
                        <thead>
							<tr>
								<th style="width: 70px;" class="text-center">#</th>
								<th>{{__('Question heading')}}</th>
								<th>Objekt</th>
								<th>{{__('Action')}}</th>
								
							</tr>
                        </thead>
                        <tbody>
							 @foreach($questions as $question)
								<tr>
									<td>{{$question->id}}</td>
									<td>{{$question->qn_heading}}</td>
									<td>{{$properties[$question->id]}}</td>
									<td><a class="btn btn-primary" href="{{url('question/'.$question->id)}}"><i class="icon fa fa-edit"></i></a></td>
								</tr>
							@endforeach
						</tbody>
						</table>
						
						</div>
						</div>	
						</div>
					</div>

                    
                </div>
            </div>
    </div>

@endsection


@section('js')
<script>
	$('.question-table').DataTable(/* {
		"columns": [
			null,
			{ "type": "formatted-num" },
			{ "type": "formatted-num" },
		]
	} */);
	</script>
		
@endsection
