@extends('layouts.admin')

@section('content')

    <div class="row">


                   {!! Form::open(['action' => ['QuestionController@update'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
						{!! Form::token() !!}
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="panel">
								<div class="panel-heading">{{__('Edit Question')}} #{{$question->id}}</div>
								<div class="white-box">
								
									<input type="hidden" value="{{$question->id}}" name="id"/>
									<div class="form-group">
										<label class="col-md-2">Property</label>
										<div class="col-md-10">
											<select name="property" class="form-control" required>
												<option value="">--select property--</option>
												@foreach($properties as $property)
													<option value="{{$property->id}}" {{ ($question->property_id==$property->id) ?'selected':'' }}>{{$property->name_of_property}}</option>
												@endforeach
											</select>
										</div>
									</div>
							
							
									<div class="form-group">
										<label class="col-md-2">{{__('Question heading')}}</label>
										<div class="col-md-10">
											<input type="text" name="qn_heading" class="form-control form-control-line" value="{{$question->qn_heading}}" placeholder="{{__('Question heading')}}"
												   required>
											</div>
									</div>
									@php 
										$qns=json_decode($question->questions,true);
									@endphp
									<div class="form-group">
										<label class="col-md-2">{{__('Question')}}</label>
										<div class="col-md-10">
											<table class="table table-bordered">
												<thead>
													<tr class="text-center ">
														<td>Question</td>
														<td>Type</td>
														<td>Required</td>
														<td>Action</td>
													</tr>
												</thead>
												<tbody>
												
												@foreach($qns['question'] as $key => $qs)
												
												<tr class="text-center">
													<td>
													@if(is_array($qs))
													<input type="text" name="questions[question][{{$key}}][qn]" class="form-control" value="{{$qs['qn']}}" required><br>
													<div class="row">
														@foreach($qs['op'] as $op)
															<div class="col-md-3"><input type="text" class="form-control" name="questions[question][{{$key}}][op][]" value="{{$op}}"  required></div>
														@endforeach	
													</div>
													@else
														<input type="text" name="questions[question][]" class="form-control" value="{{ $qs }}"  required>
													@endif
													</td>
													
													<td width="180">
														<select name="qn_type"  class="qn-type form-control">
															<option value="1">Normal questions</option>
															<option value="2" {{is_array($qs)?'selected':''}}>Objective questions</option>
														</select>
													</td>
													<td width="20">
													<!--<input type="checkbox" name="questions[required][]" value="1" {{ isset($qns['required'][$key]) ? 'checked':'' }}  />-->
													<input type="hidden" name="questions[required][]" value="{{ ($qns['required'][$key]) ? 1:0 }}">
													<input type="checkbox"   onclick="this.previousElementSibling.value=1-this.previousElementSibling.value" {{ ($qns['required'][$key]) ? 'checked':'' }}>
													</td>
													
													<td class="last-td" width="20">
														@if($key==0)
															<span class="fa fa-plus-circle plus-color" onclick="addmore(this)"></span>
															<span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>
														@else
															<span class="fa fa-minus-circle  minus-color" onclick="removeit(this)"></span>
														@endif
														
													</td>
												</tr>
												
												@endforeach												
												
												</tbody>
											</table>
										</div>
									</div>
									<hr>
										<div class="form-group">
											<div class="col-lg-2 col-sm-4 col-xs-12">
												<a href="{{ url('/question') }}" class="btn btn-block btn-danger btn-rounded">{{__('cancel')}}</a>
											</div>
											<div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12">
												<button type="submit" name="submit" class="btn btn-block btn-primary btn-rounded">{{__('Submit')}}</button>
											</div>
										</div>
								</div>
							</div>
						</div>
					 {!! Form::close() !!}
          
    </div>

@endsection

@section('js')
<script>

	function addmore(plus){		
		var newrow=$(plus).parent().parent().clone();
		newrow.children().find('span').remove();
		newrow.children().find('input[type=text]').val('');
		newrow.children().find('.qn-type').val(1);
		newrow.children('td:first-child').html('<input type="text" name="questions[question][]" class="form-control" value="" required>');
		newrow.children('td[class=last-td]').append('<span class="fa fa-minus-circle  minus-color"  onclick="removeit(this)"></span>');
		$(plus).parent().parent().parent().append(newrow);
	}
	function removeit(minus){
		$(minus).parent().parent().remove();
	}


	$('body').on('change',".qn-type",function() {
		
		if($(this).val()==1)
		{
			$(this).parent().prev('td').html('<input type="text" name="questions[question][]" class="form-control" value="" required>');
		}
		else{
			var tdI=$(this).parent().parent('tr').index();
			$(this).parent().prev('td').html('<input type="text" name="questions[question]['+tdI+'][qn]" class="form-control" value="" required><br><div class="row"><div class="col-md-3"><input type="text" class="form-control" name="questions[question]['+tdI+'][op][]" required></div><div class="col-md-3"><input type="text" class="form-control" name="questions[question]['+tdI+'][op][]" required></div><div class="col-md-3"><input type="text" class="form-control" name="questions[question]['+tdI+'][op][]" required></div><div class="col-md-3"><input type="text" class="form-control" name="questions[question]['+tdI+'][op][]" required></div></div>');
		}
	});

</script>  
@endsection