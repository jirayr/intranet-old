@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="panel">
                    <div class="card-header" style="padding-top: 15px ">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="panel-heading">Accounts</div>
                            </div>

                            <div class="col-md-8">
                                <form action="{{url('upload-bank-account-file')}}" method="post" enctype="multipart/form-data" >
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="file" class="form-control" required name="bank_file">
                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        </div>


                                        <div class="col-md-2">
                                            <button class="btn btn-primary">Upload</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="col-md-2">
                                <a class="btn btn-primary" href="{{url('/account/transaction/create')}}">Create <i class="fa fa-plus"></i></a>
                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table  id="bank" class="table table-hover  color-bordered-table purple-bordered-table">
                            <thead>
                            <tr>
                                <th>Property Name</th>
                                <th>Kontobezeichnung</th>
                                <th>IBAN</th>
                                <th>Kontoinhaber</th>
                                <th>Datum Buchungssaldo</th>
                                <th>Buchsaldo</th>
                                <th>Währung</th>
                                <th>Kontokategorie</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @isset($bankData)
                                    @foreach($bankData as $data)
                                        <tr>
                                            <td>{{isset($data->property)?$data->property->name_of_property :''}}</td>
                                            <td>{{$data->account_designation}}</td>
                                            <td>{{$data->iban}}</td>
                                            <td>{{$data->account_owner}}</td>
                                            <td>{{$data->date_of_booking_balance}}</td>
                                            <td>{{$data->book_balance}}€</td>
                                            <td>{{$data->currency}}</td>
                                            <td>{{$data->account_category}}</td>
                                            <td style="display: flex">
                                                <select style="width: 200px" name="property_id"  data-id="{{$data->id}}" class="property test form-control" >

                                                </select>
                                                <a style="margin-left: 10px" href="javascript:void(0)" onclick="deleteBankEmailLog({{$data->id}})"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                 @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>

@endsection
@section('js')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>

    <script>
        $(document).ready(function() {

            $('.property').select2();

            var columns = [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ];
           $('#bank').dataTable({
               "pageLength": 100

           });

            $('.property').on('change', function () {
                id = $(this).attr('data-id');
                p_id = $(this).children(":selected").val();
                comment = $('.deal-release-comment').val();
                $.ajax({
                    type: 'POST',
                    url: '{{url('/update-property-account')}}',
                    data: {
                        id: id,
                        property_id: p_id,
                        _token: '<?php echo e(csrf_token()); ?>',
                    },
                    success: function(data) {
                        sweetAlert("Property Updated Successfully");
                        // location.reload();
                    }
                });
            });

        });

        function deleteBankEmailLog(id) {
            swal({
                title: 'Delete this Record?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (isConfirm) {
                if (isConfirm.dismiss !== 'cancel' && isConfirm.dismiss !== 'overlay' && isConfirm){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: 'post',
                        url :  "{{url('delete-account')}}",
                        data: {
                            id: id,
                        },
                        success : function (data)
                        {
                            sweetAlert("Deleted Successfully");
                            location.reload();
                        }
                    });
                }

            })
        }

        $(document).ready(function() {

            $(".test").select2({

                minimumInputLength: 2,

                language: {

                    inputTooShort: function () {
                        return "<?php echo 'Please enter 2 or more characters'; ?>"
                    }
                },
                multiple: false,
                tags: false,
                ajax: {

                    url: '{{url('/get-properties')}}',
                    dataType: 'json',
                    data: function (term) {
                        return {
                            query: term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name_of_property,
                                    id: item.id,

                                }

                            })

                        };
                    }

                }

            });
        });
    </script>
@stop
