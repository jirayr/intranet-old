@extends('layouts.admin')
@section('content')
    <form  method="post" action="{{url('/account/transaction/store')}}">
        <div class="row">
            <div class="col-md-6" style="width: 50%">
                <label for="email">Kontobezeichnung:</label>
                <input required type="text" class="form-control" name="account_designation"  placeholder="Enter Kontobezeichnung" id="email">
            </div>
            <div class="col-md-6" style="width: 50%">
                <label for="email">IBAN:</label>
                <input required type="text" class="form-control" name="iban"   placeholder="Enter iban">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6" style="width: 50%">
                <label for="email">Kontoinhaber:</label>
                <input required type="text" class="form-control" name="account_owner"   placeholder="Enter Kontoinhaber">
            </div>
            <div class="col-md-6" style="width: 50%">
                <label for="email">Datum Buchungssaldo:</label>
                <input required type="date" class="form-control" name="date_of_booking_balance"   placeholder="Enter Datum Buchungssaldo">
            </div>
        </div>
       <div class="row">
           <div class="col-md-6" style="width: 50%">
               <label for="email">Buchsaldo:</label>
               <input required type="text" class="form-control" name="book_balance"   placeholder="Enter Buchsaldo">
           </div>
           <div class="col-md-6" style="width: 50%">
               <label for="email">Währung:</label>
               <input required type="text" class="form-control" name="currency"   placeholder="Enter Währung">
           </div>
       </div>
       <div class="row">
           <div class="col-md-6" style="width: 50%">
               <label for="email">Kontokategorie:</label>
               <input required type="text" class="form-control" name="account_category"   placeholder="Enter Buchsaldo">
           </div>

       </div>



        <br>

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
@stop
