<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <style>
    .expiry-overview-table {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    .expiry-overview-table td, .expiry-overview-table th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    .expiry-overview-table tr:nth-child(even){background-color: #f2f2f2;}

    .expiry-overview-table tr:hover {background-color: #ddd;}

    .expiry-overview-table th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #004F91;
      color: white;
    }
    .heading{
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    }
    .text-right{
      text-align: right;
    }
  </style>
</head>
<body>

@if(count($pending_invoice) > 0)
  
  <br>
  <h3 class="heading">PENDING RECHNUNGEN</h3>

  <table class="expiry-overview-table">
    <thead>
      <tr>
        <th>Objekt</th>
        <th>Rechnung</th>
        <th>Re. D.</th>
        <th class="text-right">Re. Bet.</th>
        <th>User</th>
        <th>Kommentar</th>
        <th>Datum</th>
        <th>Kommentar Falk</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($pending_invoice as $key => $value)
          <?php
            $download_path = "https://drive.google.com/drive/u/2/folders/".$value->file_basename;
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".$value->file_basename;
            }
          ?>
          <tr>
            <td><a href="{{route('properties.show',['property'=>$value->property_id])}}?tab=property_invoice">{{$value->name_of_property}}</a></td>
            <td>
              <a  target="_blank"  title="{{$value->invoice}}"  href="{{$download_path}}">{{$value->invoice}}</a>
            </td>
            <td>{{show_date_format($value->date)}}</td>
            <td class="text-right">{{show_number($value->amount,2)}}</td>
            <td>{{$value->name}}</td>
            <td>{{ $value->comment }}</td>
            <td>{{show_datetime_format($value->created_at)}}</td>
            <td>{{ $value->comment2 }}</td>
          </tr>
        @endforeach
    </tbody>
  </table>

@endif

@if(count($not_release_invoice) > 0)
  
  <br>
  <h3 class="heading">NICHT FREIGEGEBEN</h3>

  <table class="expiry-overview-table">
    <thead>
      <tr>
        <th>Objekt</th>
        <th>Rechnung</th>
        <th>Re. D.</th>
        <th class="text-right">Re. Bet.</th>
        <th>User</th>
        <th>Kommentar</th>
        <th>Datum</th>
        <th>Kommentar Falk</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($not_release_invoice as $key => $value)
          <?php
            $download_path = "https://drive.google.com/drive/u/2/folders/".$value->file_basename;
            if($value->file_type == "file"){
                $download_path = "https://drive.google.com/file/d/".$value->file_basename;
            }
          ?>
          <tr>
            <td><a href="{{route('properties.show',['property'=>$value->property_id])}}?tab=property_invoice">{{$value->name_of_property}}</a></td>
            <td>
              <a  target="_blank"  title="{{$value->invoice}}"  href="{{$download_path}}">{{$value->invoice}}</a>
            </td>
            <td>{{show_date_format($value->date)}}</td>
            <td class="text-right">{{show_number($value->amount,2)}}</td>
            <td>{{$value->name}}</td>
            <td>{{ $value->comment }}</td>
            <td>{{show_datetime_format($value->created_at)}}</td>
            <td>{{ $value->comment2 }}</td>
          </tr>
        @endforeach
    </tbody>
  </table>

@endif

@if(count($rental_agreements1) > 0)
  
  <br>
  <h3 class="heading">AUSLAUFENDE MIETVERTRÄGE</h3>

  <table class="expiry-overview-table">
    <thead>
      <tr>
        <th>#</th>
        <th>AM</th>
        <th>Objekt</th>
        <th>Vermietet</th>
        <th>Mietende</th>
        <th>Kündigung bis zum</th>
        <th>Fläche (m2)</th>
        <th>IST-Nkm (€)</th>
        <th>Typ</th>
        <th>Kommentare</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($rental_agreements1 as $key => $value)
          <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ user_short_name($value->am_name) }}</td>
            <td>
               <a href="{{route('properties.show',['property'=>$value->property_id])}}">
               {{$value->name_of_property}}</a>
            </td>
            <td><a href="javacript:void(0)" data-id="{{$value->id}}" class="value">{{$value->name}}</a></td>
            <td>{{show_date_format($value->rent_end)}}</td>
            <td>
              @if($value->termination_by)
                {{ ( date('Y-m-d', strtotime($value->termination_by)) < date('Y-m-d') ) ? show_date_format($value->rent_end) : show_date_format($value->termination_by) }}
              @endif
            </td>
            <td class="text-right">{{$value->rental_space ? number_format($value->rental_space,2,",",".") : 0}}</td>
            <td class="text-right">{{$value->actual_net_rent ? number_format($value->actual_net_rent,2,",",".") : 0}}</td>
            <td>{{ ($value->type == 1) ? 'Wohnen' : 'Gewerbe' }}</td>
            <td>{{($value->comment1) ? $value->comment1 : $value->comment}}</td>
          </tr>
        @endforeach
    </tbody>
  </table>

@endif

@if(count($rental_agreements2) > 0)
  
  <br>
  <h3 class="heading">ABGELAUFENE MIETVERTRÄGE</h3>

  <table class="expiry-overview-table">
    <thead>
      <tr>
        <th>#</th>
        <th>AM</th>
        <th>Objekt</th>
        <th>Vermietet</th>
        <th>Mietende</th>
        <th>Fläche (m2)</th>
        <th>IST-Nkm (€)</th>
        <th>Typ</th>
        <th>Kommentare</th>
     </tr>
    </thead>
    <tbody>
        @foreach ($rental_agreements2 as $key => $value)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{ user_short_name($value->am_name) }}</td>
            <td>{{$value->name_of_property}}</td>
            <td><a href="javacript:void(0)" data-id="{{$value->id}}" class="value">{{$value->name}}</a></td>
            <td>{{show_date_format($value->rent_end)}}</td>
            <td class="text-right">{{$value->rental_space ? number_format($value->rental_space,2,",",".") : 0}}</td>
            <td class="text-right">{{$value->actual_net_rent ? number_format($value->actual_net_rent,2,",",".") : 0}}</td>
            <td>{{ ($value->type == 1) ? 'Wohnen' : 'Gewerbe' }}</td>
            <td>{{($value->comment1) ? $value->comment1 : $value->comment}}</td>
          </tr>
        @endforeach
    </tbody>
  </table>

@endif

@if (count($building_insurance) > 0)
  
  <br>
  <h3 class="heading">AUSLAUFENDE GEBÄUDEVERSICHERUNGEN</h3>

  <table class="expiry-overview-table">
    <thead>
      <tr>
        <th>Objekt</th>
        <th>AM</th>
        <th>Name</th>
        <th>Kommentare</th>
        <th>Betrag</th>
        <th>Laufzeit</th>
        <th>Kündigungsfrist</th>
        <th>Anzahl</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($building_insurance as $key => $value)
          <tr>
            <td>
               <a href="{{route('properties.show',['property'=>$value->id])}}?tab=insurance_tab">{{$value->name_of_property}}</a>
            </td>
            <td>
               {{ (isset($value->asset_name)) ? $value->asset_name : '' }}
            </td>
            <td>{{$value->axa}}</td>
            <td>{{$value->gebaude_comment}}</td>
            <td class="text-right">
              @if(is_numeric($value->gebaude_betrag))
                {{number_format($value->gebaude_betrag,2,",",".")}}
                €
              @else
                {{$value->gebaude_betrag}}
              @endif
            </td>
            <td>
               @if($value->gebaude_laufzeit_to)
                {{date_format(  date_create(str_replace('.', '-', $value->gebaude_laufzeit_to)) , 'd.m.Y')}}
               @else
                {{$value->gebaude_laufzeit_to}}
               @endif
            </td>
            <td>{{$value->gebaude_kundigungsfrist}}</td>
            <td>
               <a href="{{route('properties.show',['property'=>$value->id])}}?tab=test_tab">{{$value->insurances($value,1)}}</a>
            </td>
          </tr>
        @endforeach
    </tbody>
  </table>

@endif

@if (count($liability_insurance) > 0)
  
  <br>
  <h3 class="heading">AUSLAUFENDE HAFTPFLICHTVERSICHERUNGEN</h3>

  <table class="expiry-overview-table">
    <thead>
      <tr>
        <th>Objekt</th>
        <th>AM</th>
        <th>Name</th>
        <th>Kommentare</th>
        <th>Betrag</th>
        <th>Laufzeit</th>
        <th>Kündigungsfrist</th>
        <th>Anzahl</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($liability_insurance as $key => $value)
          <tr>
            <td>
                <a href="{{route('properties.show',['property'=>$value->id])}}?tab=insurance_tab">{{$value->name_of_property}}</a>
            </td>
            <td> {{ (isset($value->asset_name)) ? $value->asset_name : '' }}</td>
            <td>{{$value->allianz}}</td>
            <td>{{$value->haftplicht_comment}}</td>
            <td class="text-right">    
              @if(is_numeric($value->haftplicht_betrag))
                {{number_format($value->haftplicht_betrag,2,",",".")}}
                €
              @else
                {{$value->haftplicht_betrag}}
              @endif
            </td>
            <td>
              @if($value->haftplicht_laufzeit_to)
                {{date_format(  date_create(str_replace('.', '-', $value->haftplicht_laufzeit_to)) , 'd.m.Y')}}
              @else
                {{$value->haftplicht_laufzeit_to}}
              @endif
            </td>
            <td>{{$value->haftplicht_kundigungsfrist}}</td>
            <td>
              <a href="{{route('properties.show',['property'=>$value->id])}}?tab=test_tab">{{$value->insurances($value,2)}}</a>
            </td>
         </tr>
        @endforeach
    </tbody>
  </table>

@endif

@if (count($vacancies) > 0)
  
  <br>
  <a href="{{route('dashboard').'?section=leer'}}">
  <h3 class="heading">Leerstände (€)</h3></a>

  <table class="expiry-overview-table">
    <thead>
      <tr>
        <th>#</th>
        <th>Asset Manager</th>
        <th>Objekt</th>
        <th>Leerstand</th>
        <th>Fläche (m2)</th>
        <th>Leerstand (€)</th>
        <th>Typ</th>
        <th width="20%">Kommentare</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($vacancies as $key => $value)

          <?php
            $value->vacancy_in_eur = $value->calculated_vacancy_in_eur;
          ?>
          <tr>
            <td>{{$value->id}}</td>
            <td>{{$value->creator_name}}</td>
            <td>
               <a  href="{{route('properties.show',['property'=>$value->property_id])}}?tab=tenancy-schedule">
               {{$value->object_name}}</a>
            </td>
            <td>{{$value->name}}</td>
            <td class="text-right">{{$value->vacancy_in_qm ? number_format($value->vacancy_in_qm,2,",",".") : 0}}</td>
            <td class="text-right">{{$value->vacancy_in_eur ? number_format($value->vacancy_in_eur,2,",",".") : 0}}</td>
            <td>{{$value->type}}</td>
            <td>{{ ($value->comment) ? $value->comment : $value->comment1 }}</td>
         </tr>
        @endforeach
    </tbody>
  </table>

@endif

@if(count($release_contract) > 0)
  
  <br>
  <h3 class="heading">AUSLAUFENDE VERTRÄGE</h3>

  <table class="expiry-overview-table">
    <thead>
      <tr>
        <th>#</th>
        <th>Objekt</th>
        <th>Datei</th>
        <th>Betrag</th>
        <th>Startdatum</th>
        <th>Kündigung spätestens</th>
        <th>Kommentar Verträge</th>
        <th>User</th>
        <th>Datum</th>
        <th>Kategorie</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($release_contract as $key => $value)
          
          <tr>
            <td>{{ $key+1 }}</td>
            <td><a href="{{ route('properties.show',['property'=> $value->property_id]) }}?tab=property_invoice">{{$value->name_of_property}}</a></td>
            <td>
              <?php 
                $downlod_file_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($value->file_basename) ? $value->file_basename : '').'&file_dir='.(isset($value->file_dirname) ? $value->file_dirname : '');
                $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($value->file_basename) ? $value->file_basename : '');
                if($value->file_type == "file"){
                    $download_path = "https://drive.google.com/file/d/".(isset($value->file_basename) ? $value->file_basename : '');
                }
              ?>
              <a  target="_blank"  title="{{ $value->invoice }}"  href="{{ $download_path }}">{{ $value->invoice }}</a> <a target="_blank" href="{{ $downlod_file_path }}" title="Download"><b>&#8595;</b></a>
            </td>
            <td class="text-right">{{ show_number($value->amount,2) }}</td>
            <td>{{ show_date_format($value->date) }}</td>
            <td>{{ show_date_format($value->termination_date) }}</td>
            <td>{{ $value->comment }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ show_date_format($value->created_at) }}</td>
            <td>{{ $value->category }}</td>
          </tr>

        @endforeach
    </tbody>
  </table>

@endif

</body>
</html>