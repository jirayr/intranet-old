<html>
<head>
  <style>
    body { 
    font-family: arial,sans-serif !important;
    }
    .main{
      font-size: 14px;
    }
    footer { position: fixed; left: 0px; right: 0px;  height: 50px; }
  </style>
</head>
<body>

  <footer style="bottom:0px; width:100%;font-size: 12px">
      <center>FCR Immobilien AG • HRB 210430 • Amtsgericht München</center>
      <center>FCR Immobilien AG • Vorstand: Falk Raudies • Aufsichtsratsvorsitzender: Prof. Dr. Franz-Joseph Busse</center><br>
      <center>Volksbank Raiffeisenbank Fürstenfeldbruck eG • Konto: 99899 • BLZ: 701 633 70</center>
      <center>SWIFT: GENODEF1FFB • IBAN DE32701633700000099899</center>
  </footer>
  <main>

        <div id="pdfdiv" style="font-family: arial,sans-serif;font-size: 14px; margin:35px ">
            <div style="width: 100%;">
             <?php
             $data = file_get_contents('https://intranet.fcr-immobilien.de/img/logo.png');
             $base64 = 'data:image/png;base64,' . base64_encode($data);
             ?>
                <div style="width: 60%;float: left;"></div>
                 <div style="width: 40%; float: right; text-align: left;">
                        
                           <div style="width: 100%; text-align:right;">
                           <img src="{{ $base64 }}" width="230" style="margin-right: 35px;">
                           <!-- <br> -->
                           <br>
                           <!-- FCR Immobilien AG<br><br> -->
                           <!-- Paul-Heyse-Str. 28<br> -->
                           <!-- D-80336 München -->
                           <div style="text-align: left;">
                           @if(isset($email_template) && $email_template->phoneNo != "")
                               <br> Tel.: {{ $email_template->phoneNo }}<br>
                           @endif
                           @if(isset($email_template) && $email_template->mobile != "")
                               Mob.: {{ $email_template->mobile }}<br>
                           @endif
                           <!-- Fax +49 (0) 89 / 413 2496 99<br> -->
                           E-Mail: @if(isset($email_template)){{ $email_template->email }}@endif<br>
                           Internet: www.fcr-immobilien.de<br>
                           
                           </div>
                       </div>
                 </div>

           <div style="clear: both;"></div>
           <div style="width: 75%; float: left ; margin-top: 80px">
             <span style="font-size: 10px;">
                 FCR Immobilien • Paul-Heyse-Str. 28 • D-80336 München
             </span>
               <br><br>
               <div style="width: 75%; flex: left">
                   <div class="col-sm-6">
                        @if(isset($email_template)){{ $email_template->address1 }} @endif <br>
                        @if(isset($email_template)){{ $email_template->address2 }} @endif <br>
                        @if(isset($email_template)){{ $email_template->address3 }} @endif <br>
                        @if(isset($email_template)){{ $email_template->address4 }} @endif <br>
                   </div>

               </div>
           </div>

           <div style="clear:both"></div>
           <br>
              <br>
              <!-- <br> -->

              <p style="text-align: right;">München,  @if(isset($email_template)){{ \Carbon\Carbon::parse($email_template->date)->format('d.m.Y') }}@endif</p>
              <br>
              <!-- <br> -->


               <b style=" margin-top: 10px; font-weight: bold">{{ (isset($email_template->plz_ort) && $email_template->plz_ort) ? $email_template->plz_ort : "Kaufpreisangebot für das Objekt in ".( (isset($properties->plz_ort)) ? $properties->plz_ort : '' )." ".( (isset($properties->ort)) ? $properties->ort : '' )}}</b><br>
               <div style="width: 100%; flex: left; margin-top: 20px;">
               {{isset($email_template->dear_text)?$email_template->dear_text:'Sehr geehrte Damen und Herren'}}
               <br>
               <br>
                   {{ $email_template->auf_grund }} {{ (isset($properties->plz_ort)) ? $properties->plz_ort : '' }} {{ (isset($properties->ort)) ? $properties->ort : '' }}, {{$properties->strasse}} {{$properties->hausnummer}},
                   {{ $email_template->einen }}

               <h3 style="text-align: center;"><strong>@if(isset($email_template)){{ $email_template->price }} EUR @endif</strong></h3>
              <p style="text-align: center;">@if(isset($email_template)) (in Worten: {{ $email_template->random_text }}) @endif </p>
              
               <!-- <br> -->

               @if(isset($email_template)) {!! $email_template->custom_text !!} @endif
           </div>
           <div style="width: 100%;">
               <div style="width: 30%; float: left;">
               Mit freundlichen Grüßen
               </div>
           </div>

           <br>
           <br>
           <div style="width: 100%; padding-top: 10px;">
               <div style="width: 150px; display: inline-block;">
                   @if(isset($email_template))
                       @if($email_template->signature !=null)
                           <img style="width: 100%; height: 60px; object-fit: cover;" src="{{ asset('signature/'.$email_template->signature)  }}">
                       @endif
                   @endif
               </div>
               <div style="width: 150px; display: inline-block; margin-left: 15%">
                   @if(isset($email_template))
                       @if($email_template->second_signatory_signature)
                           <img style="width: 100%; height: 60px; object-fit: cover;" src="{{ asset('signature/'.$email_template->second_signatory_signature)  }}">
                       @endif
                   @endif
               </div>
           </div>
           <div style="width: 30%; float: left;">
                 <div style="width: 100%;display: inline-block;height: 1px;background: #ccc;margin-left: 5px;"></div>
               <div style="margin-top: 20px;">
                   @if(isset($u))
                       {{ $u->name }}
                   @endif
               </div>
               <div style="margin-top: 10px;">
                   @if(isset($email_template))
                       {{ $email_template->transaction }}
                   @endif
               </div>
           </div>

                 @if(isset($email_template) && $email_template->second_signatory_signature)
             <div style="width: 30%; float: left; margin-left: 7%">
                 <div style="width: 100%;display: inline-block;height: 1px;background: #ccc;margin-left: 5px;"></div>

                 <div style="margin-top: 20px;">
                     @if(isset($u2) && $u2->name)
                         {{$u2->name }}
                     @endif
                 </div>
                 <div style="margin-top: 10px;">
                     @if(isset($email_template) && $email_template->second_signatory_transaction)
                         {{ $email_template->second_signatory_transaction }}
                     @endif
                 </div>
             </div>
             @endif


           <div style="width: 100%; margin-top: 5px;">

                <div style="width: 70%; display: inline-block; ">
               @if(isset($email_template)){{ $email_template->last1 }}  @endif
               </div>
               <div style="width: 70%; display: inline-block; font-size: 10px">
                   @if(isset($email_template)) {{ $email_template->last2 }} @endif
               </div>

           </div>

                 <style>
                     #parent {
                         overflow: hidden;
                     }
                     .right {
                         float: right;
                         width: 215px;
                         margin-right: 20% !important;
                     }
                     .left {
                             /*overflow: hidden;*/
                     }
                 </style>

                 {{--<div id="parent">--}}
                     {{--<div class="right">--}}
                         {{--@if(isset($u2) && $u2->name)--}}
                         {{--{{$u2->name }}--}}
                         {{--@endif--}}
                     {{--</div>--}}
                     {{--<div class="left">--}}
                         {{--@if(isset($u))--}}
                         {{--{{ $u->name }}--}}
                         {{--@endif--}}
                     {{--</div>--}}
                 {{--</div>--}}
                 {{--<div id="">--}}
                     {{--<div class="right" style="float: left">--}}
                         {{--@if(isset($email_template))--}}
                              {{--{{ $email_template->transaction }}--}}
                         {{--@endif--}}
                  {{--</div>--}}
                  {{--<div class="left" style="float: left">--}}
                        {{--@if(isset($email_template) && $email_template->second_signatory_transaction)--}}
                           {{--{{ $email_template->second_signatory_transaction }}--}}
                        {{--@endif--}}
                 {{--</div>--}}
             {{--</div>--}}
            </div>



           </div>
       </div>


  </main>
</body>
</html>





    <script src="{{ asset('js/sha512.js') }}" type="text/javascript" charset="utf-8"></script>
     <script type="text/javascript" src="{{ asset('js/jspdf.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-git.js') }}"></script>
