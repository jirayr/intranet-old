<h3>Hallo Falk,</h3> 
<p>{{$v_name}} benötigt deine Freigabe zur Transaktionsentscheidung von {{$name_of_property}}. Hier kannst du das Objekt freigeben: {!! $url !!}</p>

@if(isset($comment))
<br><br>Kommentar: {!! $comment !!}
@endif