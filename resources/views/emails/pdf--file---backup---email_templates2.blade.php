<?php 
require_once(public_path().'/dompdf/autoload.inc.php');
require_once(public_path().'/dompdf/lib/html5lib/Parser.php');
require_once(public_path().'/dompdf/lib/php-font-lib/src/FontLib/Autoloader.php');
require_once(public_path().'/dompdf/lib/php-svg-lib/src/autoload.php');
require_once(public_path().'/dompdf/src/Autoloader.php');
  use Dompdf\Dompdf;
  ob_start();
?>
 
<div id="pdfdiv" style="page-break-after: always;">
  <div style="width: 100%; margin:auto;">
 
  <br><br><br>
<div class="row">
Sehr geehrter  @if(isset($email_template)){{ $email_template->input1 }} @endif, 
</div>

<br><br><br><br><br> 
 
 <div class="row">

    Gerne würden wir bei Ihnen eine Finanzierung für ein Einzelhandelsobjekt in @if(isset($email_template)){{ $email_template->input2 }} @endif anfragen. <br><br>
 
In  @if(isset($email_template)){{ $email_template->input3 }} @endif werden wir einen Netto-Markt kaufen. <br><br>
 
Bei dem Objekt in @if(isset($email_template)){{ $email_template->input4 }} @endif handelt es sich um ein im Jahr @if(isset($email_template)){{ $email_template->input5 }} @endif errichtetes @if(isset($email_template)){{ $email_template->input6 }} @endif Das Objekt befindet sich in einem gepflegten Zustand. Der Mietvertrag mit @if(isset($email_template)){{ $email_template->input7 }} @endif läuft noch bis zum  @if(isset($email_template)){{ $email_template->input8 }} @endif.  @if(isset($email_template)){{ $email_template->input9 }} @endif <br><br>
Für den Objekterwerb stellen wir uns eine Non-Recourse Finanzierung vor. Wir werden für den Erwerb der Immobilie die Objektgesellschaft @if(isset($email_template)){{ $email_template->input10 }} @endif gründen. Dabei würden wir  @if(isset($email_template)){{ $email_template->input11 }} @endif Eigenkapital mitbringen und einen Fremdkapitalanteil in Höhe von @if(isset($email_template)){{ $email_template->input12 }} @endif bei Ihnen erbeten. Wir könnten uns einen variablen Zinssatz von @if(isset($email_template)){{ $email_template->input13 }} @endif und eine Tilgung von @if(isset($email_template)){{ $email_template->input14 }} @endif vorstellen, sind aber offen für Gegenvorschläge Ihrerseits. <br><br>
 
Damit Sie zunächst einen ersten Überblick über unsere Gesellschaft und unser Vorhaben erhalten, übersende ich Ihnen in der Anlage nachfolgende Unterlagen der FCR Immobilien AG sowie die zugehörigen Objektunterlagen:
•   Jahresabschlüsse 2017
•   Halbjahresbericht 2018
•   Geschäftsbericht 2017
•   Exposé, Planungsspiegel und Mieterliste des Objektes<br><br>
 
Falls Sie weitere Informationen oder Unterlagen benötigen, stehe ich Ihnen gerne zur Verfügung. <br>
 
Es würde mich freuen, wenn Ihnen das Objekt zusagen würden und wir dazu kurzfristig Rücksprache halten könnten. <br><br>
 
Mit freundlichen Grüßen, <br><br>

Mit freundlichen Grüßen<br><br>

 @if(isset($email_template)){{ $email_template->input15 }} @endif <br>
 @if(isset($email_template)){{ $email_template->input16 }} @endif 
  
 </div>
 
</div>
</div>

<?php

 
  $html = ob_get_clean();
  $dompdf = new DOMPDF();
  $dompdf->set_paper(array(0, 0, 595, 1000), 'portrait');

  $dompdf->load_html($html);
  $dompdf->render();
  //$dompdf->stream("asdasd.pdf");

  $generete_name = uniqid().'.pdf';
  session(['pdfname' => $generete_name]);
 
  $output = $dompdf->output();
  file_put_contents('/kunden/144294_82152/intranet/public/'.$generete_name, $output);
 
?>
	<script src="{{ asset('js/sha512.js') }}" type="text/javascript" charset="utf-8"></script>
	 <script type="text/javascript" src="{{ asset('js/jspdf.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery-git.js') }}"></script>  
 