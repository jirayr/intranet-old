<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <style>
    .expiry-overview-table {
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    .expiry-overview-table td, .expiry-overview-table th {
      border: 1px solid #ddd;
      padding: 8px;
    }

    .expiry-overview-table tr:nth-child(even){background-color: #f2f2f2;}

    .expiry-overview-table tr:hover {background-color: #ddd;}

    .expiry-overview-table th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #004F91;
      color: white;
    }
    .heading{
      font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    }
    .text-right{
      text-align: right;
    }
  </style>
</head>
<body>

  @if(isset($released))
    
    <br>
    <!-- <h3 class="heading">Freigegebene Rechnungen</h3> -->

    <?php
    $link_open_invoice = route('dashboard').'?section=release-invoice-box';

    ?>
    {{count($released)}} Freigegebene Rechnungen :  <a href="{{$link_open_invoice}}">{{$link_open_invoice}}</a>        
    <br><br>
    <table class="expiry-overview-table">
      <thead>
        <th>Objekt</th>
        <th>AM</th>
        <th>Rechnung</th>
        <th>Re. D.</th>
        <th>Re. Bet.</th>
        <th>Datum</th>
        <th>Kommentar</th>
      </thead>
      <tbody>

          @foreach ($released as $key => $element)
              <tr>
                <td>
                  <a href="{{route('properties.show',['property'=> $element->property_id])}}?tab=property_invoice">{{$element->name_of_property}}</a>
                </td>
                <td>{{ $element->name }}</td>
                <td>
                  <?php 
                    $download_path = "https://drive.google.com/drive/u/2/folders/".$element->file_basename;
                    if($element->file_type == "file"){
                        $download_path = "https://drive.google.com/file/d/".$element->file_basename;
                    }
                  ?>
                  <a title="{{ $element->invoice }}" href="{{ $download_path }}" target="_blank">{{ $element->invoice }}</a>
                </td>
                <td>{{ show_date_format($element->date) }}</td>
                <td class="text-right">{{ show_number($element->amount,2) }}</td>
                <td>{{ show_datetime_format($element->created_at) }}</td>
                <td>{{ $element->comment }}</td>
              </tr>
          @endforeach
      </tbody>
    </table>

  @endif

</body>
</html>