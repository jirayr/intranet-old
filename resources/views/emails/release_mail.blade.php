<h3>Hallo {{$v_name}},</h3> 
<p>Falk hat die generelle Verkaufsentscheidung für das Objekt {{$name_of_property}} freigegeben. Hier kommst du zum Objekt: {!! $url !!}</p>


@if(isset($comment))
<br>Kommentar: {!! $comment !!}
@endif

