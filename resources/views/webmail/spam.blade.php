@extends('layouts.admin')
@section('content')
<style type="text/css">
    .preloader { display: none; }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/webmail.css') }}">
 
    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

           
            <!-- ============================================================== -->
            <div class="email-app">
                <!-- ============================================================== -->
                <!-- Left Part -->
                <!-- ============================================================== -->
                @include('webmail/right_sidebar')
                <!-- ============================================================== -->
                <!-- Right Part -->
                <!-- ============================================================== -->
                <div class="right-part mail-list bg-white">
                    <div class="p-3 b-b">
                        <div class="d-flex align-items-center">
                            <div>
                                <h4>Spam </h4>
                            </div>
                           
                        </div>
                    </div>
                  
                    <div class="bg-light p-3 d-flex align-items-center do-block">
                        <div class="btn-group mt-1 mb-1">
                          {{--   <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input sl-all" id="cstall">
                                <label class="custom-control-label" for="cstall">Check All</label>
                            </div> --}}
                        </div>
                       {{--  <div class="ml-auto">
                            <div class="btn-group mr-2" role="group" aria-label="Button group with nested dropdown">
                                <button type="button" class="btn btn-outline-secondary font-18"><i class="mdi mdi-reload"></i></button>
                                <button type="button" class="btn btn-outline-secondary font-18"><i class="mdi mdi-alert-octagon"></i></button>
                                <button type="button" class="btn btn-outline-secondary font-18"><i class="mdi mdi-delete"></i></button>
                            </div>
                            <div class="btn-group mr-2" role="group" aria-label="Button group with nested dropdown">
                           
                            </div>
                        </div> --}}
                    </div>
         
                    <div class="table-responsive">
                        <table class="table email-table no-wrap table-hover v-middle">
                            <tbody>
        <!-- row -->
                      
                    @php                                    
                    // Read all messaged into an array:
                    $mailsIds = $spam->searchMailbox('ALL');

                    if(!$mailsIds) {
                      echo 'There are no messages in this mailbox.';
                    }
            
                    $emails = array_reverse($mailsIds);
                    $limit = 10;

                    $qty_items = count($emails);
                    $qty_pages = ceil($qty_items / $limit);

                    $curr_page = isset($_GET['page']) ? $_GET['page'] : 1;
                    $next_page = $curr_page < $qty_pages ? $curr_page + 1 : null;
                    $prev_page = $curr_page > 1 ? $curr_page - 1 : null;

                    $offset = ($curr_page - 1) * $limit;
                    $emails = array_slice($emails, $offset, $limit);
                     
                    @endphp 
                 
                    @foreach($emails  as $num) 
                        @php 
                             $head = $spam->getMailHeader($num);
                        @endphp
                    
                         <tr class="unread">
                                    
                                  {{--   <td class="chb pl-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="cst1">
                                            <label class="custom-control-label" for="cst1">&nbsp;</label>
                                        </div>
                                    </td> --}}

                         <td class="user-name" style="white-space: normal;">
                           <a class="link" href="{{ url('webmail-spam_read') }}/{{ $num }}"> <span class="text-muted">
                                @if(isset($head->fromName)) 
                                {{ $head->fromName }}
                                @elseif(isset($head->fromAddress)) 
                                {{ $head->fromAddress }} 
                                @endif
                            </span>
                            </a>
                        </td>
                        <!-- Message -->
                        <td class="max-texts" style="white-space: normal; padding: 1rem 25px; white-space: normal;"> <a class="link" href="{{ url('webmail-spam_read') }}/{{ $num }}"><span class="text-dark font-normal"> 
                            @if(isset($head->subject)) 
                                {{ $head->subject }}
                             @endif
                      
                        </span></a></td>
                        <!-- Attachment -->
                        
                        @php
                        $markAsSeen = false;
                        $mail = $spam->getMail($num, $markAsSeen);
                        @endphp
 
                        <td class="clip">

                        @if(count($mail->getAttachments()) > 0) 

                           <i class="fa fa-paperclip text-muted"></i>
                        @endif
                        </td> 
                        <!-- Time -->
                        <td style="width: 200px;" class="time text-muted"> {{ $head->date  }} </td>
 
                     
                    @endforeach

                                 
                                </tr>
                              
                            </tbody>
                        </table>
                    </div>
                    <div class="p-3 mt-4">


 

                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">

    <? if($prev_page): ?>
     
        <li class="page-item"><a class="page-link" href="?page=<?= $prev_page ?>">Previous</a></li>
    <? endif ?>
    <? for($i = 1; $i <= $qty_pages; $i++): ?>
      
        <li class="<?= ($i == $curr_page) ? 'active' : '' ?>"><a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
    <? endfor ?>
    <? if($next_page): ?>
     
         <li class="page-item"><a class="page-link" href="?page=<?= $next_page ?>">Next</a></li>
    <? endif ?> 
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Right Part  Mail Compose -->
                <!-- ============================================================== -->
                <div class="right-part mail-compose bg-white" style="display: none;">
                    <div class="p-4 border-bottom">
                        <div class="d-flex align-items-center">
                            <div>
                                <h4>Compose</h4>
                                <span>create new message</span>
                            </div>
                            <div class="ml-auto">
                                <button id="cancel_compose" class="btn btn-dark">Back</button>
                            </div>
                        </div>
                    </div>
                    <!-- Action part -->
                    <!-- Button group part -->
                    <div class="card-body">
                        <form>
                            <div class="form-group">
                                <input type="email" id="example-email" name="example-email" class="form-control" placeholder="To">
                            </div>
                            <div class="form-group">
                                <input type="text" id="example-subject" name="example-subject" class="form-control" placeholder="Subject">
                            </div>
                            <div id="summernote"></div>
                            <h4>Attachment</h4>
                            <div class="dropzone" id="dzid">
                                <div class="fallback">
                                    <input name="file" type="file" multiple="">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success mt-3"><i class="far fa-envelope"></i> Send</button>
                            <button type="submit" class="btn btn-dark mt-3">Discard</button>
                        </form>
                        <!-- Action part -->
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Right Part  Mail detail -->
                <!-- ============================================================== -->
                <div class="right-part mail-details bg-white" style="display: none;">
                    <div class="card-body bg-light">
                        <button type="button" id="back_to_inbox" class="btn btn-outline-secondary font-18 mr-2"><i class="mdi mdi-arrow-left"></i></button>
                        <div class="btn-group mr-2" role="group" aria-label="Button group with nested dropdown">
                            <button type="button" class="btn btn-outline-secondary font-18"><i class="mdi mdi-reply"></i></button>
                            <button type="button" class="btn btn-outline-secondary font-18"><i class="mdi mdi-alert-octagon"></i></button>
                            <button type="button" class="btn btn-outline-secondary font-18"><i class="mdi mdi-delete"></i></button>
                        </div>
                        <div class="btn-group mr-2" role="group" aria-label="Button group with nested dropdown">
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-folder font-18 "></i> </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1"> <a class="dropdown-item" href="javascript:void(0)">Dropdown link</a> <a class="dropdown-item" href="javascript:void(0)">Dropdown link</a> </div>
                            </div>
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-label font-18"></i> </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1"> <a class="dropdown-item" href="javascript:void(0)">Dropdown link</a> <a class="dropdown-item" href="javascript:void(0)">Dropdown link</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body border-bottom">
                        <h4 class="mb-0">Your Message title goes here</h4>
                    </div>
                    <div class="card-body border-bottom">
                        <div class="d-flex no-block align-items-center mb-5">
                            <div class="mr-2"><img src="../../assets/images/users/1.jpg" alt="user" class="rounded-circle" width="45"></div>
                            <div class="">
                                <h5 class="mb-0 font-16 font-medium">Hanna Gover <small> ( hgover@gmail.com )</small></h5><span>to Suniljoshi19@gmail.com</span>
                            </div>
                        </div>
                        <h4 class="mb-3">Hey Hi,</h4>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
                    </div>
                    <div class="card-body">
                        <h4><i class="fa fa-paperclip mr-2 mb-2"></i> Attachments <span>(3)</span></h4>
                        <div class="row">
                            <div class="col-md-2">
                                <a href="javascript:void(0)"> <img class="img-thumbnail img-responsive" alt="attachment" src="../../assets/images/big/img1.jpg"> </a>
                            </div>
                            <div class="col-md-2">
                                <a href="javascript:void(0)"> <img class="img-thumbnail img-responsive" alt="attachment" src="../../assets/images/big/img2.jpg"> </a>
                            </div>
                            <div class="col-md-2">
                                <a href="javascript:void(0)"> <img class="img-thumbnail img-responsive" alt="attachment" src="../../assets/images/big/img3.jpg"> </a>
                            </div>
                        </div>
                        <div class="border mt-3 p-3">
                            <p class="pb-3">click here to <a href="javascript:void(0)">Reply</a> or <a href="javascript:void(0)">Forward</a></p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
   

@endsection