@extends('layouts.admin')
@section('content')
<style type="text/css">
    .preloader { display: none; }
 
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/webmail.css') }}">
  @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif 
 
    <div class="row">
          
            <!-- ============================================================== -->
            <div class="email-app">
                <!-- ============================================================== -->
                <!-- Left Part -->
                <!-- ============================================================== -->
                 @include('webmail/right_sidebar') 
                <!-- ============================================================== -->
                <!-- Right Part -->
                <!-- ============================================================== -->
 
                        
                <div class="right-part mail-details bg-white" >   
                    <div class="card-body bg-light">
                        <div class="btn-group mr-2" role="group" aria-label="Button group with nested dropdown">
                            <button data-toggle="modal" data-target="#reply"  type="button" class="btn btn-outline-secondary font-18">
                                <i class="mdi mdi-reply"></i></button>
                        
                              <a href="{{ url('/') }}/webmail-permanent-delete/{{ Request::segment(2) }}"><button type="button" class="btn btn-outline-secondary font-18">
                                <i class="mdi mdi-delete"></i></button></a>
                        </div> 
                       
                    </div>
                    
                     @php 
 
                    //$mail = $mailbox->getMail($id, $markAsSeen);
                    //  print_r($mail);
                    @endphp
                    </pre>
                    <div class="card-body border-bottom">
                        <h4 class="mb-0">
                        @if(isset($head->subject) && $head->subject != '') 
                            {{ $head->subject }}
                         @elseif(isset($head->fromAddress)) 
                            {{ $head->fromAddress }} 
                         @endif
                        </h4>
                    </div>
                    <div class="card-body border-bottom" >
                        <div class="d-flex no-block align-items-center mb-5">
                           
                            <div class="">
                                <h5 class="mb-0 font-16 font-medium">
                                @if(isset($head->fromName)) 
                                {{ $head->fromName }}
                                @elseif(isset($head->fromAddress)) 
                                {{ $head->fromAddress }} 
                                @endif
                                    <small> 
                                    (  @if(isset($head->fromAddress)) 
                                        {{ $head->fromAddress }} 
                                    @endif )
                                  </small>
                               </h5>
                               <span>
                                    to 
                                    @if(isset($head->toString)) 
                                        {{ $head->toString }} 
                                    @endif
                                </span>
                            </div>
                        </div>
                        <div style="width: 100%;">
                          
                            @if($mail->textHtml)
                                 {!! $mail->textHtml !!} 
                            @else
                            <pre>
                                 {{   $mail->textPlain }}  
                            </pre>
                            @endif
                          
                        </div>
                    </div>
                    <div class="card-body">
                       
                       
                        @if(count($mail->getAttachments()) > 0) 
                            <h4><i class="fa fa-paperclip mr-2 mb-2"></i> Attachments <span>( {{ count($mail->getAttachments()) }} )</span></h4>
                         <div class="row">
 
                         
                        @php 
                          $attachment_find = json_decode( json_encode( $mail->getAttachments() ) , true );
                          $attachment = array_column($attachment_find, 'filePath');
                          $remove_text = '/kunden/144294_82152/intranet/public/';
                          $attachment = str_replace(array($remove_text), '',$attachment);
 
                        @endphp
                        @foreach($attachment as $attach)
                            
                            <div class="col-md-1">
                                <a href="https://intranet.fcr-immobilien.de/{{ $attach }}"> <img style="max-width: 70px;" class="img-thumbnail img-responsive" alt="attachment" src="https://intranet.fcr-immobilien.de/img/attachment.png"> </a>
                            </div>

                        @endforeach
                        </div>
                        @endif

                        <div class="border mt-3 p-3">
                            <p class="pb-3">click here to <a href="#" data-toggle="modal" data-target="#reply">Reply</a> or <a href="#" data-toggle="modal" data-target="#forward_email" >Forward</a></p>
                        </div>
                    </div>
                </div>
              
            </div>
            
        </div>


<!-- Reply Modal -->
<div id="reply" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form action="{{ url('webmail-reply') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="from_address" value="{{ $head->toString }}">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reply: Re:@if(isset($head->subject) && $head->subject != ''){{ $head->subject }} @endif</h4>
      </div>
      <div class="modal-body">
    <label>To</label>
    <input type="email" class="form-control" name="replyto" value="@if(isset($head->fromAddress)){{ $head->fromAddress }}@endif">
    <br>
    <label>Subject</label>
    <input type="text" class="form-control" name="subject" value="Re: @if(isset($head->subject) && $head->subject != ''){{ $head->subject }} @endif">
    <br>
     <label>Message</label>
         <textarea class="form-control" id="editor1" name="body_message" style="min-height: 200px;">

@if($mail->textHtml)
{!! $mail->textHtml !!}
@else
{!!  $mail->textPlain !!}
@endif
         </textarea>

         
        <br>  
        <label>Attachment</label>
        <input type="file" name="filename[]" class="form-control" multiple="">


      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace( 'editor1' );
</script>


@endsection