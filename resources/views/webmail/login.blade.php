@extends('layouts.admin')

@section('content')
<style type="text/css">
    .preloader { display: none; }
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('css/webmail.css') }}">
 

<div class="row">
     @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible float-right">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible float-right">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif
</div>

    <div class="row">
        
        <div class="col-sm-4 m-auto">
                <div class="new-login-box" style="margin-left: auto; margin-right: auto">
            
            <div class="white-box">
                <h3 class="box-title m-b-0">Webmail-Login</h3>
                <small>Hier Daten eingeben</small>
                <form class="form-horizontal" method="POST" action="{{ url('webmail-login-post') }}">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group  m-t-20">
                        <div class="col-xs-12">
                            <label>Email Addresse</label>
                            <input id="email" type="email" class="form-control" name="email" value="" required="" autofocus="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Passwort</label>
                            <input id="password" type="password" class="form-control" name="password" required="">
                                                </div>
                    </div>
               
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>
                </form>
               
            </div>
        </div>
        </div>

         
            
             
        </div>
   

@endsection