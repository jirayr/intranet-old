<style>
 
td {
  word-break: break-all;
}
</style>       
                 <div class="left-part">
                    <a class="ti-menu ti-close btn btn-success show-left-part d-block d-md-none" href="javascript:void(0)"></a>
                    <div class="scrollable ps-container ps-theme-default" style="height:100%;" data-ps-id="d89b003c-706b-a3c1-ae6b-364f916e15ec">
                        <div class="p-3">
                            <a id="compose_mail" class="waves-effect waves-light btn btn-info d-block" href="" data-toggle="modal" data-target="#compose_email">Compose</a>
                        </div>
                        <div class="divider"></div>
                        <ul class="list-group">
                            <li>
                                <small class="p-3 grey-text text-lighten-1 db">Folders</small>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ url('webmail') }}" class="active list-group-item-action"><i class="mdi mdi-inbox"></i> Inbox <span class="badge badge-success text-white font-normal badge-pill float-right">
                                @php

                                $user_data = DB::table('webmail')->where('user_id',Auth::id())->first();
                                if(!is_null($user_data)){

                                $mailbox_unseen = new \PhpImap\Mailbox('{imap.fcr-immobilien.de:993/imap/ssl/novalidate-cert}INBOX', $user_data->web_email, $user_data->web_pass);
                                }
                                @endphp
                                
                                @if(isset($mailbox_unseen))
                                {{ count($mailbox_unseen->searchMailbox('UNSEEN')) }}
                                @endif
                                </span></a>
                            </li>

                            <li class="list-group-item">
                                <a href="{{ url('webmail-sent') }}" class="list-group-item-action"> <i class="mdi mdi-email"></i> Sent Mail </a>
                            </li>
                            <li class="list-group-item">
                                <hr>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ url('webmail-spam') }}" class="list-group-item-action"> <i class="mdi mdi-block-helper"></i> Spam </a>
                            </li>
                            <li class="list-group-item">
                                <a href="{{ url('webmail-trash') }}" class="list-group-item-action"> <i class="mdi mdi-delete"></i> Trash </a>
                            </li>
                            <li class="list-group-item">
                                <hr>
                            </li>
  
                        </ul>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                </div>



<!-- Compose Email Modal -->
<div id="compose_email" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form action="{{ url('webmail-reply') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
 
        <input type="hidden" name="from_address" value="@if(!is_null($user_data)){{ $user_data->web_email }}@endif">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Compose - Identity: @if(!is_null($user_data)){{ $user_data->web_email }}@endif</h4>
      </div>
      <div class="modal-body">
    <label>To</label>
    <input type="email" class="form-control" name="replyto" >
    <br>
    <label>Subject</label>
    <input type="text" class="form-control" name="subject" >
    <br>
     <label>Message</label>
         <textarea class="form-control" id="editor2" name="body_message" style="min-height: 200px;">
         </textarea>
 
        <br>  
        <label>Attachment</label>
        <input type="file" name="filename[]" class="form-control" multiple="">
 
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </form>
  </div>
</div>


<!-- Forward Email Modal -->
@if(isset($head)) 
<div id="forward_email" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form action="{{ url('webmail-reply') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="from_address" value="{{ $head->toString }}">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Compose - Identity: @if(!is_null($user_data)){{ $user_data->web_email }}@endif</h4>
      </div>
      <div class="modal-body">
    <label>To</label>
    <input type="email" class="form-control" name="replyto" >
    <br>
    <label>Subject</label>
    <input type="text" class="form-control" name="subject" value="Fwd: @if(isset($head->subject) && $head->subject != ''){{ $head->subject }} @endif">
    <br>
     <label>Message</label>
    <textarea class="form-control" id="editor3" name="body_message" style="min-height: 200px;">


----- Forwarded message from @if(isset($head->fromName)) 
                                {{ $head->fromName }}
                                @elseif(isset($head->fromAddress)) 
                                {{ $head->fromAddress }} 
                                @endif -----
        @if($mail->textHtml)
        {!! $mail->textHtml !!}
        @else
        {!!  $mail->textPlain !!}
        @endif
----- End forwarded message -----
    </textarea>
 
        <br>  
        <label>Attachment</label>
        <input type="file" name="filename[]" class="form-control" multiple="">
 
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success">Send</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </form>
  </div>
</div>
@endif


<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>
 
CKEDITOR.replace( 'editor2' );
CKEDITOR.replace( 'editor3' ); 
</script>