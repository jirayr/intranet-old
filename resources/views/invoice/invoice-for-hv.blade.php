<table class="table table-striped" id="invoice-table-{{ $status }}">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>Rechnung</th>
			<th>Re.D.</th>
			<th>Re.Bet.</th>
			<th>Kommentar</th>
			<th>Abbuch.</th>
			<th>Umlegb.</th>
			<th>User</th>
			<th>Datum</th>
			
			{{-- @if($status != 1) --}}
				<th style="min-width: 175px;">Freigabe AM</th>
			{{-- @endif --}}

			<th style="min-width: 175px;">Freigabe HV</th>

			{{-- @if($status != 1) --}}
				<th style="min-width: 175px;">Freigabe</th>
				<th style="min-width: 175px;">Freigabe Falk</th>
			{{-- @endif --}}

			<th>Weiterleiten an</th>
			<th>Aktion</th>
		</tr>
	</thead>
	<tbody>
		@if($invoice)
			@foreach ($invoice as $key => $value)
				@php
					$title = '';
					if($status == 0){
						$title = 'OFFENE RECHNUNGEN';
					}elseif($status == 1){
						$title = 'FREIZUGEBENDE RECHNUNGEN';
					}elseif ($status == 2) {
						$title = 'FREIGEGEBENE RECHNUNGEN';
					}elseif ($status == 3) {
						$title = 'PENDING RECHNUNGEN';
					}elseif ($status == 4) {
						$title = 'NICHT FREIGEGEBEN RECHNUNGEN';
					}

					$download_path = "https://drive.google.com/drive/u/2/folders/".$value->file_basename;
		        	if($value->file_type == "file"){
		            	$download_path = "https://drive.google.com/file/d/".$value->file_basename;
		        	}
		        	$subject = $title.': '.$value->name_of_property;
					$content = 'Rechnung: <a title="'.$value->invoice.'" href="'.$download_path.'" target="_blank">'.$value->invoice.'</a>';
					if($value->comment){
						$content .= '<br/>Kommentar User: '.$value->comment;
					}

					$taburl = route('properties.show',['property' => $value->property_id]).'?tab=property_invoice';
					$taburl = "<a href='".$taburl."'>".$taburl."</a>";
				@endphp
				<tr>
					<td><a href="{{route('properties.show',['property'=> $value->property_id])}}?tab=property_invoice">{{$value->name_of_property}}</a></td>
					<td><a target="_blank"  title="{{$value->invoice}}"  href="{{$download_path}}">{{$value->invoice}}</a></td>
					<td>{{show_date_format($value->date)}}</td>
					<td class="text-right">{{show_number($value->amount,2)}}</td>
					<td>

						@php
							$comment = DB::table('properties_comments as pc')->selectRaw('pc.*, u.name, u.role, u.company')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.record_id', $value->id)->where('pc.type', 'property_invoices')->orderBy('pc.created_at', 'desc')->first();
						@endphp
						@if($comment)
							@php
								$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
								$commented_user = $comment->name.''.$company;
							@endphp
							<p style="margin-bottom: 0px;">
								<span class="commented_user">{{ $commented_user }}</span>: <span class="long-text">{{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</span>
							</p>
						@endif
						<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="{{ $value->id }}" data-property-id="{{ $value->property_id }}" data-type="property_invoices" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>

					</td>
					<td><?php echo get_paid_checkbox($value->id,$value->need_to_pay);?></td>
					<td><?php echo get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2); ?></td>
					<td>
						<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $value->property_id }}" data-user-id="{{ $value->user_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="{{ $title }}" data-id="{{ $value->id }}" data-section="property_invoices">{{ $value->user_name }}
						</a>
					</td>
					<td>{{show_date_format($value->created_at)}}</td>
					
					{{-- @if($status != 1) --}}
						<td>
							@if(isset($value->button))
								{!! $value->button['btn_release_am'] !!}
							@endif
						</td>
					{{-- @endif --}}
					
					<td>
						@if(isset($value->button))
							{!! $value->button['btn_release_hv'] !!}
						@endif
					</td>

					{{-- @if($status != 1) --}}
					
						<td>
							@if(isset($value->button))
								{!! $value->button['btn_release_usr'] !!}
							@endif
						</td>
						<td>
							@if(isset($value->button))
								{!! $value->button['btn_release_falk'] !!}
							@endif
						</td>
					{{-- @endif --}}

					<td>
						<button type="button" class="btn btn-primary btn-forward-to" data-property-id="{{ $value->property_id }}" data-id="{{ $value->id }}" data-subject="{{ $subject }}" data-content="" data-title="FREIZUGEBENDE RECHNUNGEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>
					</td>
					<td>
						@if($value->falk_status != 2)
							<button type="button" data-id="{{ $value->id }}" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>
						@endif
					</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>