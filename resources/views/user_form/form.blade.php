@extends('layouts.admin')
	@section('content')
		<section id="user_form_outer">
			<header class="col-md-12">
				<div class="col-md-12 text-center"> <img class="bg-white" src="https://fcr-immobilien.de/wp-content/uploads/2018/05/FCR_FINAL-1.png"></div>
			</header>
			<footer class="col-md-12">
				<div class="form_outer col-md-6 col-md-offset-3 no-padding">
					<h1 class="padding-left-30">
						{{__('user_form.Finanzierungsanfrage FCR')}}<br/>
						{{__('user_form.Immobilien AG')}}
					</h1>
					<form action="" method="post" id="user_form">
						{{csrf_field()}}
						<input type="hidden" name="email_id" value="{{$email_info->id}}"/>
						<input type="hidden" name="property_id" value="{{$property_id}}"/>
						<input type="hidden" name="doc_key" value="{{$doc_key}}"/>
						@if($step=='a')
							<div id="form1 form_section">
								<ul class="list-group">
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Ansprechpartner')}}:&nbsp;{{ucfirst($email_info->contact_person)}}
									</li>
								</ul>
							</div>
						@elseif($step=='b')
							<div id="form2" class="form_section">
								<ul class="list-group">
									<li class="question_section_heading question_section list-group-item padding-left-30">
										{{__('user_form.Objektdaten')}}
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Objekt')}}:&nbsp{{$email_info->object}}
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Adresse')}}:&nbsp{{$email_info->address}}
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Baujahr')}}:&nbsp{{$email_info->construction_year}}
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Typ')}}:&nbsp{{$email_info->type}}
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Hauptmieter')}}:&nbsp{{$email_info->the_main_tenant}}
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Mietlaufzeit bis')}}:&nbsp{{$email_info->rental_period_until}}
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Parkplätze')}}:&nbsp{{$email_info->parking}}
										<div class="triangle-topleft"></div>
									</li>
								</ul>
							</div>
						@elseif($step=='c')
							<div id="form3" class="form_section">
								<ul class="list-group">
									<li class="question_section_heading question_section list-group-item padding-left-30">
										{{__('user_form.Finanzierung')}}
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Finanzierungsart')}}:&nbsp <input type="text" name="financing" value="{{$email_info->financing}}"/>
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Eigenkapitalanteil')}}:&nbsp <input type="text" name="equity_share" value="{{$email_info->equity_share}}"/>
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Fremdkapitalanteil')}}:&nbsp <input type="text" name="leverage" value="{{$email_info->leverage}}"/>
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Variabler Zinssatz')}}:&nbsp <input type="text" name="variable_interest_rate" value="{{$email_info->variable_interest_rate}}"/>
										<div class="triangle-topleft"></div>
									</li>
									<li class="question_section list-group-item padding-left-30">
										{{__('user_form.Tilgung')}}:&nbsp <input type="text" name="repayment" value="{{$email_info->repayment}}"/>
										<div class="triangle-topleft"></div>
									</li>
								</ul>
							</div>
						@elseif($step=='d')
							<div id="form4" class="form_section">
								<span class="required padding-left-30">* {{__('user_form.Required')}}</span>
								<br/><br/>
								@foreach($questions as $key=>$question)
									<ul class="list-group form_{{$key}} {{($key==0) ?  : 'hidden_only'}}">
										<li class="question_section_heading question_section list-group-item padding-left-30">
											{{$question->qn_heading}}
										</li>
										@php
											$total_question_forms=$key;
											$Questions=json_decode($question->questions,true);
										@endphp
										@foreach($Questions['question'] as $key1=>$Question)
											@if(is_array($Question))
												<li class="admin_question_section list-group-item padding-left-30 objective_questions">
													{{__($Question['qn'])}}:&nbsp;
													@if($Questions['required'][$key1]==1)
														<span class="required">*</span>
													@endif
													<br/>
													@foreach($Question['op'] as $op)
														<input type="radio" name="user_form_fields[{{$question->qn_heading}}][{{$Question['qn']}}]" value="{{__($op)}}" class="{{($Questions['required'][$key1]==1) ? required : ''}}"/>{{__($op)}}
														<br/>
													@endforeach
													<span class="error_msg required hidden_only">{{__('user_form.This is a required question')}}</span>
												</li>
											@else
												<li class="admin_question_section list-group-item padding-left-30 normal_questions">
													@if(isset($Question))
														{{__($Question)}}:&nbsp;
														@if($Questions['required'][$key1]==1)
															<span class="required">*</span>
														@endif
														<br/>
													@endif
													<input type="text" name="user_form_fields[{{$question->qn_heading}}][{{$Question}}]" value="" placeholder="{{__('user_form.Your answer')}}" class="form-control {{($Questions['required'][$key1]==1) ? 'required' : ''}}"/>
													<span class="error_msg required hidden_only">{{__('user_form.This is a required question')}}</span>
												</li>
											@endif
										@endforeach
									</ul>
								@endforeach
							</div>
						@endif
						<div class="action_buttons padding-left-30">
							@if($step=='d')
								<a href_attr="{{url('forms/'.$token.'/'.$step_pre.'/viewform')}}" class="btn-button dynamic_form_pre">
									{{__('user_form.BACK')}}&nbsp;&nbsp;&nbsp;
								</a>&nbsp;&nbsp;&nbsp;
								<a class="btn-button dynamic_form_next">
									{{__('user_form.NEXT')}}
								</a>
							@else
								@if($step!='a')
									<a href="{{url('forms/'.$token.'/'.$step_pre.'/viewform')}}" class="btn-button">
										{{__('user_form.BACK')}}&nbsp;&nbsp;&nbsp;
									</a>
									&nbsp;&nbsp;&nbsp;
								@endif
								@if($step!='d')
									<a href="{{url('forms/'.$token.'/'.$step_next.'/viewform')}}" class="btn-button">
										{{__('user_form.NEXT')}}
									</a>
								@endif
							@endif
						</div>
					</form>
				</div>
				<div class="col-md-6 col-md-offset-3 text-center">
					<small>This content is neither created nor endorsed by Immobilien. Report Abuse - Terms of Service</small>
				</div>
			</footer>
			<div class="clear"></div>
		</section>
	@endsection
	@section('js')
		<script>
			var check_form=0;
			$(document).ready(function()
			{
				href=$(".dynamic_form_pre").attr('href_attr');
				$(".dynamic_form_pre").attr('href',href);
				total_question_forms="{{isset($total_question_forms) ? $total_question_forms : 0}}";
				$(".dynamic_form_next").click(function()
				{
					Error=0;
					$(".form_"+check_form+" input.required").each(function()
					{
						if(!$.trim($(this).val()))
						{
							$(this).parent().addClass('Haserror');
							$(this).parent().find('.error_msg').show();
							Error=1;
						}
						else
						{
							$(this).parent().removeClass('Haserror');
							$(this).parent().find('.error_msg').hide();
							Error=0;
						}
					});
					if(Error==0)
					{
						$(".form_"+check_form).slideUp();
						check_form++;
						if(total_question_forms==check_form)
						{
							$(this).html('SUBMIT');
						}
						if(check_form>total_question_forms)
						{
							//Code for submit form
							$("#user_form").submit();
						}
						$(".form_"+check_form).slideDown();
						$(".dynamic_form_pre").attr('href',null);
					}
				});
				
				//code for back button
				$(".dynamic_form_pre").click(function()
				{
					if(check_form==0)
					{
						href=$(this).attr('href_attr');
						$(this).attr('href',href);
					}
					$(".form_"+check_form).slideUp();
					check_form--;
					$(".form_"+check_form).slideDown();
					$(".dynamic_form_next").html('NEXT');
				});
				
				//Code for save user form data
				$("#user_form").on('submit',function(e)
				{
					var formData = new FormData(this);
					$(".preloader").show();
					$.ajax({
						url: "{{ route('saveuserForm') }}",
						type: "post",
						data:formData,
						cache:false,
						contentType: false,
						processData: false,
						success: function (response) 
						{
							$(".preloader").hide();
							
							if(response=='success')
							{
								msg='<h4 class="alert-success padding-left-30">Your response has been recorded.</h4>';
								$(".form_section").html(msg);
								$(".action_buttons").hide();
								toastr.success("Your response has been recorded.");
							}
							else
							{
								toastr.error("Please contact to administrator");
							}
							setTimeout(function()
							{
								//location.reload();
							}, 3000 );
						}
					});
					e.preventDefault();
				});
			});
		</script>
	@endsection
