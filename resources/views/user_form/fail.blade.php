<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div id="app" style="margin-top: 300px;margin-left: 100px;margin-right: 100px;">
		<div class="jumbotron jumbotron-fluid">
			<div class="container">
				<h1 class="display-4" style="padding-left: 40px;">Please fill all required fields to save your data.</h1>
			</div>
		</div>

	</div>
</body>
</html>
