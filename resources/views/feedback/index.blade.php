@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">{{__('feedback.feedback')}}</div>

                <div class="table-responsive">
                    <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th style="width: 70px;" class="text-center">#</th>
                            <th>{{__('feedback.user_name')}}</th>
                            <th>{{__('feedback.message')}}</th>
                            <th>{{__('feedback.created_date')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($feedbacks as $feedback)
                        <tr>
							<td class="text-center"><br>{{$feedback->id}}</td>
							<td><br>{{$feedback->user_name}}</td>
							<td><br>{{$feedback->message}}</td>
							<td><br>{{ show_datetime_format($feedback->created_at) }}</td>
                        </tr>
						@endforeach
                        </tbody>
                    </table>
                   <button style="margin:25px!important; width:auto !important" type="button" id="add_feedback_btn" class="new_property_btn btn-success" data-toggle="modal" data-target="#create_feedback_modal"><span class="glyphicon-plus"></span> {{__('feedback.add_feedback')}}</button>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="create_feedback_modal" tabindex="-1" role="dialog" aria-labelledby="create_feedback_modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form method="POST" action="{{route('feedback.add')}}">
		  {!! csrf_field() !!}
		  <div class="modal-header">
			<h4 class="modal-title">{{__('feedback.add_feedback')}}</h4>
		  </div>
		  <div class="modal-body">
			<h5>{{__('feedback.message')}}:</h5>
			<input type="text" name="message">
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('app.side_menu.close')}}</button>
			<button type="submit" class="btn btn-primary">{{__('app.side_menu.create')}}</button>
		  </div>
		
		</form>
    </div>
  </div>
</div>
@endsection
@section('js')
@endsection