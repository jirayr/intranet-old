<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/plugins/images/favicon.png') }}">
    <title>{{__('app.title')}}</title>
    <!-- Bootstrap Core CSS -->
    {{--    <link href="{{ asset('css/app.css') }}" type="text/css" rel="stylesheet" />--}}
    <link href="{{ asset('assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bower_components/datatables/buttons.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Menu CSS -->
    <link href="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css') }}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{ asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <!-- morris CSS -->
    <link href="{{ asset('assets/plugins/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{ asset('assets/plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="{{ asset('assets/plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet" />
	<!--jquery ui datepicker-->
	<link href="{{ asset('assets/plugins/bower_components/x-editable/dist/jquery-editable/jquery-ui-datepicker/css/redmond/jquery-ui-1.10.3.custom.min.css') }}" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('assets/css/colors/default.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('assets/plugins/select2/4.0.6-rc.0/css/select2.min.css') }}" rel="stylesheet" />
    <link href="/css/custom-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/toastr.min.css">
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
     @yield('css')
</head>
<body>

    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->

    <div id="wrapper">
        {{-- <div id="page-wrapper"> --}}
            {{-- <div class="container-fluid"> --}}
                @yield('content')
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
    <!--slimscroll JavaScript -->
    <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('assets/js/waves.js') }}"></script>
    <!--Counter js -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/dashboard1.js') }}"></script>

    <script src="{{ asset('assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
    <!-- chartist chart -->
    <script src="{{ asset('assets/plugins/bower_components/chartist-js/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="{{ asset('assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- Custom Theme JavaScript -->

    <script src="{{ asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>
    <!--Style Switcher -->
    <script src="{{ asset('assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <!--jquery ui datepicker-->
    <script src="{{ asset('assets/plugins/bower_components/x-editable/dist/jquery-editable/jquery-ui-datepicker/js/jquery-ui-1.10.3.custom.min.js') }}"></script>

    <!-- Calendar JavaScript -->
    <script src="{{ asset('assets/plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/calendar/dist/fullcalendar.js') }}"></script>

    <script src="{{ asset('assets/plugins/bower_components/calendar/dist/jquery.fullcalendar.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/calendar/dist/cal-init.js') }}"></script>

    <script src="{{ asset('assets/plugins/bower_components/calendar/dist/locale-all.js') }}"></script>

    <!--Morris JavaScript -->
    <script src="{{ asset('assets/plugins/bower_components/raphael/raphael-min.js') }}"></script>

    <script src="{{ asset('assets/plugins/select2/4.0.6-rc.0/js/select2.min.js') }}"></script>


    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>

    <!-- jQuery peity -->
    <script src="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js') }}"></script>

    <!--Style Switcher -->
    <script src="{{ asset('assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <script src="/js/toastr.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script src="/js/jquery.maskMoney.js"></script>

    {{--Jquery Cookie--}}
    <script src="{{ asset('assets/plugins/jquery-cookie/1.4.1/jquery.cookie.min.js') }}"></script>

    {{-- Custom Script --}}
    <script src="/js/custom-script.js"></script>

    @yield('js')
</body>
</html>
