<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/plugins/images/favicon.png') }}">
    <title>{{__('app.title')}}</title>
    <!-- Bootstrap Core CSS -->
    {{--    <link href="{{ asset('css/app.css') }}" type="text/css" rel="stylesheet" />--}}
    <link href="{{ asset('assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-suggest.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/plugins/bower_components/datatables/buttons.dataTables.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <!-- Menu CSS -->
    <link href="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css') }}" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{ asset('assets/plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <!-- morris CSS -->
    <link href="{{ asset('assets/plugins/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="{{ asset('assets/plugins/bower_components/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}"
          rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="{{ asset('assets/plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet"/>
    <!--jquery ui datepicker-->
    <link href="{{ asset('assets/plugins/bower_components/x-editable/dist/jquery-editable/jquery-ui-datepicker/css/redmond/jquery-ui-1.10.3.custom.min.css') }}"
          rel="stylesheet"/>
    <!-- animation CSS -->
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('assets/css/colors/default.css') }}" id="theme" rel="stylesheet">
    <link href="{{ asset('assets/plugins/select2/4.0.6-rc.0/css/select2.min.css') }}" rel="stylesheet"/>
    <link href="/css/custom-style.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/toastr.min.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!--<link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">-->
    @yield('css')

    <style type="text/css">
        .toogle-menu-custom {
            position: absolute;
            left: 240px;
        }

        @media (max-width: 767px) {
            .toogle-menu-custom {
                position: fixed;
                left: 63px;
            }
        }
    </style>
    <![endif]-->
</head>

<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
@if(!isset($slug) || (isset($slug) && $slug!='user_form'))
    <div id="wrapper">
    @include('partials.header')
    <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
    @include('partials.sidemenu')


    {{--begin--}}
    <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h4 class="page-title">
                            @if (trim($__env->yieldContent('p_title')))
                                @yield('p_title')
                            @else
                                {{__('app.dashboard_title')}}
                            @endif
                        </h4></div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                {{--end--}}
                @yield('content')

                {{--begin--}}
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
        {{--end--}}

        @include('partials.footer')

    </div>
    <!-- Modal -->
    <div class="mydateModal modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                    <form id="new_date_form">
                        <input type="hidden" name="item_id" class="date-item-id">
                        <label>Beginn der Verlängerung</label>
                        <input type="text" name="new_start_date" placeholder="dd/mm/yyyy"
                               class="form-control new_start_date mask-input-date">
                        <br>
                        <label>Mietende</label>
                        <input type="text" name="new_date" placeholder="dd/mm/yyyy"
                               class="form-control new_date mask-input-date">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary save-date">Speichern</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                </div>
            </div>

        </div>
    </div>

    <div class="modal" id="confirm-password-modal" role="dialog" data-keyboard="false" data-backdrop="static"
         style="z-index: 9999">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Bitte trage das Passwort ein</h4>
                </div>

                <form id="confirm-password-form" action="{{ route('confirm_password') }}" autocomplete="off">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <span id="confirm-password-error"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Passwort eintragen</label>
                                <input type="password" name="password" class="form-control" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Bestätigen</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

@else
    @yield('content')
@endif
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>

<script src="{{ asset('js/bootstrap-suggest.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--slimscroll JavaScript -->
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset('assets/js/waves.js') }}"></script>
<!--Counter js -->
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script src="{{ asset('assets/js/dashboard1.js') }}"></script>

<script src="{{ asset('assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
<!-- chartist chart -->
<script src="{{ asset('assets/plugins/bower_components/chartist-js/dist/chartist.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
<!-- Sparkline chart JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
<!-- Custom Theme JavaScript -->

<script src="{{ asset('assets/plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>
<!--Style Switcher -->
<script src="{{ asset('assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
<!--jquery ui datepicker-->
<script src="{{ asset('assets/plugins/bower_components/x-editable/dist/jquery-editable/jquery-ui-datepicker/js/jquery-ui-1.10.3.custom.min.js') }}"></script>

<!-- Calendar JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/moment/moment.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/calendar/dist/fullcalendar.js') }}"></script>

<script src="{{ asset('assets/plugins/bower_components/calendar/dist/jquery.fullcalendar.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/calendar/dist/cal-init.js') }}"></script>

<script src="{{ asset('assets/plugins/bower_components/calendar/dist/locale-all.js') }}"></script>

<!--Morris JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/raphael/raphael-min.js') }}"></script>

<script src="{{ asset('assets/plugins/select2/4.0.6-rc.0/js/select2.min.js') }}"></script>


<!-- Custom Theme JavaScript -->
<script src="{{ asset('assets/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>

<!-- jQuery peity -->
<script src="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw.js') }}"></script>
<script src="{{ asset('assets/plugins/bower_components/tablesaw-master/dist/tablesaw-init.js') }}"></script>

<!--Style Switcher -->
<script src="{{ asset('assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
<script src="/js/toastr.min.js"></script>
<script src="/js/jquery.mask.min.js"></script>
<script src="/js/jquery.maskMoney.js"></script>


{{--Jquery Cookie--}}
<script src="{{ asset('assets/plugins/jquery-cookie/1.4.1/jquery.cookie.min.js') }}"></script>

{{-- Custom Script --}}
<script src="/js/custom-script.js"></script>

@yield('js')
{{--begin--}}
<!-- /#wrapper -->

<!-- jQuery file upload -->
{{--<script src="{{ asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>--}}
{{--<script>--}}
{{--$(document).ready(function() {--}}
{{--// Basic--}}
{{--$('.dropify').dropify();--}}
{{--// Translated--}}
{{--$('.dropify-fr').dropify({--}}
{{--messages: {--}}
{{--default: 'Glissez-déposez un fichier ici ou cliquez',--}}
{{--replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',--}}
{{--remove: 'Supprimer',--}}
{{--error: 'Désolé, le fichier trop volumineux'--}}
{{--}--}}
{{--});--}}
{{--// Used events--}}
{{--var drEvent = $('#input-file-events').dropify();--}}
{{--drEvent.on('dropify.beforeClear', function(event, element) {--}}
{{--return confirm("Do you really want to delete \"" + element.file.name + "\" ?");--}}
{{--});--}}
{{--drEvent.on('dropify.afterClear', function(event, element) {--}}
{{--alert('File deleted');--}}
{{--});--}}
{{--drEvent.on('dropify.errors', function(event, element) {--}}
{{--console.log('Has Errors');--}}
{{--});--}}
{{--var drDestroy = $('#input-file-to-destroy').dropify();--}}
{{--drDestroy = drDestroy.data('dropify')--}}
{{--$('#toggleDropify').on('click', function(e) {--}}
{{--e.preventDefault();--}}
{{--if (drDestroy.isDropified()) {--}}
{{--drDestroy.destroy();--}}
{{--} else {--}}
{{--drDestroy.init();--}}
{{--}--}}
{{--})--}}
{{--});--}}
{{--</script>--}}
<!--Style Switcher -->

{{--end--}}

@yield('scripts')
@yield('email_template_javascript')
@yield('email_template_javascript1')
@yield('chart')
<script>

    checkNewNotification();

    function checkNewNotification() {
        setInterval(
            function () {
                var notificationIds = [];
                $('.message-center a').each(function () {
                    notificationIds.push($(this).data('id'));
                });
                $.ajax({
                    type: 'GET',
                    url: '/index.php/get-new-notification',
                    data: {'data': JSON.stringify(notificationIds)},
                    success: function (res) {
                        // $('.message-center a').css('background-color', '#edf2fa');
                        var data = JSON.parse(res);
                        if (data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                $('.message-center').prepend(
                                    '<a data-id="' + data[i]['id'] + '" href="/index.php/properties/' + data[i]['property_id'] + '" class="new">' +
                                    '<div class="user-img"> <img src="' + data[i]['created_user_avatar'] + '" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>' +
                                    '<div class="mail-contnet">' +
                                    '<h5> ' + data[i]['created_user_name'] + '</h5> ' +
                                    '<span class="mail-desc">' + data[i]['content'] + '</span> ' +
                                    '<span class="time">' + data[i]['created_at'] + '</span> ' +
                                    '</div>' +
                                    '</a>'
                                );
                            }
                            $('div.notify').append(
                                '<span class="heartbit new-notification"></span>' +
                                '<span class="point new-notification"></span>'
                            );

                        }
                        var newNotificationNumber = document.getElementsByClassName('new').length;
                        $('#notification-number').text(newNotificationNumber);
                        // console.log(data);
                        // console.log("Get new notification!");
                    },
                    error: function (f) {
                        location.reload();
                    }
                });
            }, 10000);
    }
</script>


</body>

</html>
