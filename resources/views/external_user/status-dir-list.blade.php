@extends('layouts.admin')
@section('css')
<style>
    .hide{
        display:none !important;
    }
</style>
@endsection


@section('content')

    <div class="row">

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading"> Objekte Bestand</div>
                <div class="white-box">
                    <div id="alerts"></div>

                    <div id="" style="max-height: 400px;overflow: scroll;">

                        @if((sizeof($propertiesDirectory) > 0))
                            <table class="table table-responsive table-condensed table-striped hidden-xs table-list-view" id="datatable">
                                <thead>
                                    <th></th>
                               </thead>
                                <tbody>
                                    @foreach($propertiesDirectory as $item)
                                        @php
                                        $notPermission = $permission->where('permission', $item->property_id)->first();
                                        if($notPermission && $user->email != "jana.schilling@ok-hv.de")
                                        {
                                            continue;
                                        }
                                        @endphp
                                        <tr>                                            
                                            <td class="p-8">
                                                <i class="fa fa-folder-o"></i>
                                                <a href="javascript: void(0);" data-href="{{ url('/extern_user/property/').'/'.$item->property_id }}?internal={{$internal}}" class="folder-item clickable" title="{{ $item->dir_name }}">
                                                {{ str_limit($item->dir_name, $limit = 40, $end = '...') }}
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                        <p>{{ trans('lfm.message-empty') }}</p>
                        @endif
                    </div>                    
                </div>
            </div>
                
        </div>
    </div>

    <div id="requested-frame-div" class="hide">
        <div class="modal-dialog modal-lg" style="width: 100%">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row ">
                        <iframe id="gdrive-subdir-filemanager-iframe" src=""  width="100%" height="1900px" style="border:none;"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <style>
        #datatable thead {
            display: none;
        }
    </style>
<script>
$(document).ready(function () {
    $('#datatable').DataTable();
});
$(document).on('click', '.folder-item', function(){
    var url = $(this).data('href');
    var iFrame = $('#gdrive-subdir-filemanager-iframe');
    
    iFrame.attr('src', url);
    $("#requested-frame-div").removeClass('hide');

    $([document.documentElement, document.body]).animate({
        scrollTop: $('#gdrive-subdir-filemanager-iframe').offset().top
    }, 1000);
});
</script>
@if(isset($_REQUEST['property_id']) && $_REQUEST['property_id'])
<script type="text/javascript">
    var url = "{{ url('/extern_user/property/').'/'.base64_decode($_REQUEST['property_id']) }}";
    var iFrame = $('#gdrive-subdir-filemanager-iframe');
    
    iFrame.attr('src', url);
    $("#requested-frame-div").removeClass('hide');

    $([document.documentElement, document.body]).animate({
        scrollTop: $('#gdrive-subdir-filemanager-iframe').offset().top
    }, 1000);
</script>
@endif


@endsection
