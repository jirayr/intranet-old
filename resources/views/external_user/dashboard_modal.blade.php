<div class=" modal fade" role="dialog" id="table-data-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase"></h4>
         </div>
         <div class="modal-body table-responsive" style="min-height: 20%;">

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="invoice-release-property-modal-am" role="dialog">
  <div class="modal-dialog">
     <!-- Modal content-->
     <div class="modal-content">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
           <label class="">Kommentar</label>
           <textarea class="form-control invoice-release-comment-am" name="message"></textarea>
           <br>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-primary invoice-release-submit-am" data-dismiss="modal" >Senden</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
        </div>
     </div>
  </div>
</div>

<div class=" modal fade" role="dialog" id="release-invoice-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">Freigegebene Rechnungen</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12 table-responsive">
                  <table class="table table-striped" id="release-invoice-table">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Objekt</th>
                           <th>Rechnung</th>
                           <th>Re. D.</th>
                           <th>Re. Bet.</th>
                           <th>Kommentar Rechnung</th>
                           <th>Abbuch.</th>
                           <th>User</th>
                           <th>Freigabedatum</th>
                           <th>Kommentar Falk</th>
                           <th>Überweisung</th>
                           <th>Weiterleiten an</th>
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="property_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">

            <div class="row property-comment-section">
               <div class="col-md-12 form-group">
                  <label>Kommentar</label>
                  <textarea class="form-control property-comment" rows="5"></textarea>
               </div>
               <div class="col-md-12 text-center">
                  <button type="button" class="btn btn-primary btn-add-property-comment" data-reload="1" data-record-id="" data-property-id="" data-type="" data-subject="" data-content=''>Senden</button>
               </div>
            </div>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="property_comment_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray" id="th_name">Name</th>
                          <th class="bg-light-gray" id="th_comment">Kommentar</th>
                          <th class="bg-light-gray" id="th_date">Datum</th>
                          <th class="bg-light-gray" id="th_action">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="property_comment_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="modal_sendmail_to_custom_user" style="z-index: 9999;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mail To User</h4>
         </div>
         <form action="{{ route('sendmail_to_custom_user') }}" id="form_modal_sendmail_to_custom_user">
            <input type="hidden" name="property_id">
            <input type="hidden" name="user_id">
            <input type="hidden" name="subject">
            <input type="hidden" name="content">
            <input type="hidden" name="email">
            <input type="hidden" name="mail_type">
            <input type="hidden" name="id">
            <input type="hidden" name="section">
            <div class="modal-body">
               <label>Nachricht</label>
               <textarea class="form-control" name="message" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade" id="modal-forward-to" role="dialog" style="z-index: 9999">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Weiterleiten an</h4>
      </div>
      <form id="form-forward-to" action="{{ route('mail_forward_to') }}">
         <div class="modal-body">

            <input type="hidden" name="property_id">
            <input type="hidden" name="subject">
            <input type="hidden" name="title">
            <input type="hidden" name="content">
            <input type="hidden" name="section_id">
            <input type="hidden" name="section">

            <?php 
               $all_users = DB::table('users')->select('id', 'name')->where('user_deleted', 0)->where('user_status', 1)->get();
            ?>

            <label>User</label>
            <select class="form-control" name="user" style="width: 100%;">
               <option value="">Select User</option>
               @if($all_users)
                  @foreach ($all_users as $usr)
                     <option value="{{ $usr->id }}">{{ $usr->name }}</option>
                  @endforeach
               @endif
            </select>
            <br/>
            <br/>

            <label>Oder E-Mail</label>
            <input type="text" name="email" class="form-control">
            <br/>

            <label>Kommentar</label>
            <textarea class="form-control" name="comment" rows="5" required></textarea>
            <br/>

         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-primary">Senden</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </form>
    </div>

  </div>
</div>

<div class="modal fade" id="pending-modal-am" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-pending-am-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-pending-am-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="table-released-angebote-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase"></h4>
         </div>
         <div class="modal-body table-responsive" style="min-height: 20%;">

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="insurance_detail_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form id="insurance_detail_comment_form" action="{{ route('add_property_insurance_comment') }}">
               <div class="row">
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="insurance_detail_comment" rows="5" name="comment" required></textarea>
                  </div>
               </div>
               <br/>
               <div class="row">
                  <div class="col-md-12 text-center">
                     <button type="submit" class="btn btn-primary">Posten</button>
                  </div>
               </div>
            </form>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="insurance_detail_comments_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray">Name</th>
                          <th class="bg-light-gray">Kommentar</th>
                          <th class="bg-light-gray">Datum</th>
                          <th class="bg-light-gray">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="insurance_detail_comments_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-am-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_am') }}" id="invoice-release-am-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-am-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-hv-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_hv') }}" id="invoice-release-hv-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-hv-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-usr-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_user') }}" id="invoice-release-usr-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-usr-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label class="am-list">User</label>
                     <select class="am-list invoice_asset_manager form-control" name="user" required>
                        <option value="">{{__('dashboard.asset_manager')}}</option>
                        @if(isset($active_users) && $active_users)
                           @foreach($active_users as $list1)
                              <option value="{{$list1->id}}">{{$list1->name}}</option>
                           @endforeach
                        @endif
                     </select>
                  </div>
               </div>

               {{-- <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div> --}}

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-falk-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_falk') }}" id="invoice-release-falk-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-falk-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>