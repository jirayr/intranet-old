@extends('layouts.admin')
@section('css')
<style>
    .hide{
        display:none !important;
    }
</style>
@endsection


@section('content')

    
    <!-- <div id="requested-frame-div" class="hide"> -->
        <!-- <div class="modal-dialog modal-lg" style="width: 100%"> -->
            <!-- Modal content-->
            <!-- <div class="modal-content"> -->
                <!-- <div class="modal-body"> -->
                    <div class="row ">
                        <iframe id="gdrive-subdir-filemanager-iframe" src=""  width="100%" height="1900px" style="border:none;"></iframe>
                    </div>
                <!-- </div> -->
            <!-- </div> -->
        <!-- </div> -->
    <!-- </div> -->

@endsection

@section('scripts')
    <style>
        #datatable thead {
            display: none;
        }
    </style>
<script>
$(document).ready(function () {
    $('#datatable').DataTable();
});
// $(document).on('click', '.folder-item', function(){
    var url = "{{ url('/extern_user/property/').'/'.$propertyId }}?internal={{$internal}}";
    var iFrame = $('#gdrive-subdir-filemanager-iframe');
    
    iFrame.attr('src', url);
    $("#requested-frame-div").removeClass('hide');

    $([document.documentElement, document.body]).animate({
        scrollTop: $('#gdrive-subdir-filemanager-iframe').offset().top
    }, 1000);
// });
</script>
@if(isset($_REQUEST['property_id']) && $_REQUEST['property_id'])
<script type="text/javascript">
    var url = "{{ url('/extern_user/property/').'/'.base64_decode($_REQUEST['property_id']) }}";
    var iFrame = $('#gdrive-subdir-filemanager-iframe');
    
    iFrame.attr('src', url);
    $("#requested-frame-div").removeClass('hide');

    $([document.documentElement, document.body]).animate({
        scrollTop: $('#gdrive-subdir-filemanager-iframe').offset().top
    }, 1000);
</script>
@endif


@endsection
