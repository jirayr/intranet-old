@extends('layouts.admin')
@section('p_title')
{{$properties->name_of_property}}

@if(isset($properties->asset_manager))
(AM: {{$properties->asset_manager->name}})
@endif

<button type="button" id="mail-an-am" data-type="0" class="btn btn-primary btn-sm"></i> Mail an AM</button>
@endsection

@section('css')
<script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<!-- Styles -->
<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
<link rel="stylesheet"  href="{{ asset('css/tagsinput-revisited.css') }}" />
<style type="text/css">
.navbar,
#contracts table .btn{
	display: none;
}
#contracts table .btn-delete-contract{
	display: block !important;
}
#contracts table .edit-contract{
	display: block !important;
}
#page-wrapper{
    margin: 0 !important;
}
</style>

@endsection
@section('content')

<?php $data = \App\RentPaidExcelUpload::where('property_id',$id)->first()  ?>

@if(Session::has('massage'))
<p class="alert alert-success">{{ Session::get('massage') }}</p>
@endif
@if(Session::has('error'))
<p class="alert alert-danger">{{ Session::get('error') }}</p>
@endif
@if(Session::has('success'))
<p class="alert alert-success">{{ Session::get('success') }}</p>
@endif
<ul class="nav nav-tabs">
	@if($internal)
	<li   class="active" ><a data-toggle="tab" href="#property_invoice" aria-expanded="true" >Rechnungsfreigaben</a></li>
	@endif
</ul>
<div class="tab-content">



	<div id="property_invoice" class="tab-pane fade active in">
	@include('properties.templates.property_invoice')
	</div>


</div>
		<div class="modal fade"  id="summ-model" tabindex="9999" role="dialog"  aria-hidden="true">
			<div class="modal-dialog modal-lg" style="width: 93%" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body summ-data row">
					</div>
				</div>
			</div>
		</div>


@include('properties.templates.modal_popup')


<div class=" modal fade" role="dialog" id="mail_an_am_modal">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Mail an AM</h4>
			</div>
			<form action="{{ route('sendmail_to_am') }}" method="POST">
				<div class="modal-body">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="property_id" value="{{$properties->id}}">
					{{-- <input type="hidden" name="am_id" id="am_id" value="{{$properties->asset_m_id}}"> --}}
					<input type="hidden" name="subject" value="{{ $properties->name_of_property }}">
					<input type="hidden" name="type" id="mail_type" value="0">

					<label>Nachricht</label>
					<textarea class="form-control" name="message" required></textarea>
					<br>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" >Senden</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection


@section('js')

	<script>
		var route_prefix  	= '{{ url("file-manager") }}';
		var lfm_route    	= '{{ url("file-manager") }}';
		var lang 			= {!! json_encode(trans('lfm')) !!};
		var workingDir 		= '{{$workingDir}}';
		var _token 			= '{{ csrf_token() }}';
		var id 				= '{{ $id }}';
		var user_signature 	= '{{ url('/').'/img/giphy.gif' }}';
		var request_showall	= '{{ Request()->showall }}';
		var workingDir 		= '{{$workingDir}}';
		var current_property_id = '{{ $properties->id }}';
		var is_falk			= {{ ( Auth::user()->email == config('users.falk_email') ) ? 1 : 0 }};
		/*-------------------URL START------------------*/

		var url_property_saveEmpfehlungFile 	= '{{ url("property/saveEmpfehlungFile") }}';
		var url_saveEmpfehlungFile 				= '{{ route("saveEmpfehlungFile") }}';
		var url_property_addmaillog 			= '{{ url('property/addmaillog') }}';
		var url_properties_comment_get_comments = '{{ url('properties-comment/get-comments') }}';
		var url_properties_comment_save 		= '{{ url('properties-comment/save') }}';
		var url_getReleasebutton 				= '{{ route('getReleasebutton') }}';
		var url_getProvisionbutton 				= '{{ route('getProvisionbutton') }}';
		var url_changeempfehlungdetails 		= '{{ route('changeempfehlungdetails') }}';
		var url_changeprovisioninfo 			= '{{ route('changeprovisioninfo') }}';
		var url_saveractivity 					= '{{ route('saveractivity') }}';
		var url_recommended_list 				= '{{ route('recommended_list') }}';
		var url_saveleaseactivity 				= '{{ route('saveleaseactivity') }}';
		var url_lease_list 						= '{{ route('lease_list') }}';
		var url_updatelease 					= '{{ route('updatelease') }}';
		var url_save_new_post 					= '{{ route('save_new_post') }}';
		var url_add_new_vacant 					= '{{ route('add_new_vacant') }}';
		var url_delete_new_vacant 				= '{{ route('delete_new_vacant') }}';
		var url_delete_comment_file 			= '{{ route('delete_comment_file') }}';
		var url_savebankinfoformail 			= '{{ route('savebankinfoformail') }}';
		var url_save_vacant_space 				= '{{ route('save_vacant_space') }}';
		var url_save_vacant_data 				= '{{ route('save_vacant_data') }}';
		var url_properties_change_status 		= '{{ route('properties.change_status') }}';
		var url_remove_uploaded_file 			= '{{ route('remove_uploaded_file') }}';
		var url_remove_vacant_media 			= '{{ route('remove_vacant_media') }}';
		var url_set_vacant_media_favourite		= '{{ route('set_vacant_media_favourite') }}';
		var url_set_ad_image_favourite 			= '{{ route('set_ad_image_favourite') }}';
		var url_set_vacantitem_favourite 		= '{{ route('set_vacantitem_favourite') }}';
		var url_set_export_media_favourite 		= '{{ route('set_export_media_favourite') }}';
		var url_tenants_store 					= '{{ route('tenants.store') }}';
		var url_change_comment					= '{{ route('change-comment') }}?';
		var url_uploadfiles 					= '{{ route('uploadfiles') }}';
		var url_delete_ads_images 				= '{{ route('delete_ads_images') }}';
		var url_delete_ads_pdf 					= '{{ route('delete_ads_pdf') }}';
		var url_delete_property_pdf 			= '{{ route('delete_property_pdf') }}';
		var url_delete_property_insurance		= '{{ route('delete_property_insurance') }}';
		var url_delete_service_provider			= '{{ route('delete_service_provider') }}';
		var url_delete_maintenance				= '{{ route('delete_maintenance') }}';
		var url_delete_investation				= '{{ route('delete_investation') }}';
		var url_change_date						= '{{ route('change_date') }}';
		var url_property_set_as_standard 		= '{{ url('property/set_as_standard') }}/{{ $id }}';
		var url_delete_tenant 					= '{{ route('delete_tenant') }}';
		var url_properties_upload_listing_to_api = '{{ url('/properties/upload_listing_to_api') }}';
		var url_properties_upload_attachment_images = '{{ url('/properties/upload_attachment_images') }}';
		var url_email_template_users 			= '{{ url('email_template_users') }}';
		var url_updatepropertyuser 				= '{{ route('updatepropertyuser') }}';
		var url_properties_api_imag_del 		= '{{ url('/properties/api_imag_del') }}';
		var url_add_new_city 					= '{{ route('add_new_city') }}';
		var url_add_property_bank 				= '{{ route('add_property_bank') }}';
		var url_add_new_banken  				= '{{ route('add_new_banken') }}';
		var url_searchpbank						= '{{ route("searchpbank") }}';
		var url_searchcity 						= '{{ route("searchcity") }}';
		var url_searchbank 						= '{{ route("searchbank") }}';
		var url_property_update_steuerberater 	= '{{ url('property/update/steuerberater') }}/{{ $id }}';
		var url_property_update_schl 			= '{{ url('property/update/schl') }}/{{ $id }}';
		var url_getbanklist 					= '{{ route('getbanklist') }}';
		var url_getstatusloi 					= '{{ route('getstatusloi') }}';
		var url_property_management_add 		= '{{ url('property_management/add') }}';
		var url_property_management_update 		= '{{ route('property_management.update') }}';
		var url_getStatusLoiByUserId 			= '{{ route('getStatusLoiByUserId') }}';
		var url_schl_file 						= '{{ route('schl-file') }}';
		var url_upload_bank_file 				= '{{ route('upload-bank-file') }}';
		var url_uploadattachments 				= '{{ route('uploadattachments') }}';
		var url_remove_attachment_file 			= '{{ route('remove_attachment_file') }}';
		var url_removepropertybanken 			= '{{ route('removepropertybanken') }}';
		var url_removestatusloi 				= '{{ route('removestatusloi') }}';
		var url_statusloi_update 				= '{{ url('statusloi/update') }}';
		var url_update_versicherung_approved_status = '{{ url('/update_versicherung_approved_status') }}';
		var url_update_falk__status 			= '{{ url('/update_falk__status') }}';
		var url_delete_Property_insurance1 		= '{{ url('/delete_Property_insurance') }}';
		var url_sendemailtobankers 				= '{{ route('sendemailtobankers') }}';
		var url_property_update_schl_1 			= '{{url('property/update/schl') }}';
		var url_showresult 						= '{{ route('showresult') }}';
		var url_deletesheet			 			= '{{ route('deletesheet') }}';
		var url_genarate_property_pdf 			= '{{ url('/genarate_property_pdf') }}';
		var url_property_vacantreleaseprocedure = '{{ url('property/vacantreleaseprocedure') }}';
		var url_property_provisionreleaseprocedure = '{{ url('property/provisionreleaseprocedure') }}';
		var url_property_invoicereleaseprocedure = '{{ url('property/invoicereleaseprocedure') }}';
		var url_property_dealreleaseprocedure = '{{ url('property/dealreleaseprocedure') }}';

		var url_get_property_invoice 			= '{{ route('get_property_invoice', ['property_id' => $properties->id]) }}';
		var url_get_pending_invoice 			= '{{ route('get_pending_invoice', ['id' => $properties->id]) }}';
		var url_get_pending_am_invoice 			= '{{ route('get_pending_am_invoice', ['id' => $properties->id]) }}';
		var url_get_not_released_invoice 		= '{{ route('get_not_released_invoice', ['id' => $properties->id]) }}';
		var url_get_not_released_invoice_am 	= '{{ route('get_not_released_invoice_am', ['id' => $properties->id]) }}';
		var url_get_release_property_invoice 	= '{{ route('get_release_property_invoice', ['property_id' => $properties->id]) }}';
		var url_get_invoice_logs 				= '{{ route('get_invoice_logs', ['property_id' => $properties->id]) }}';
		var url_get_deal_logs						= '{{ route('get_deal_logs', ['property_id' => $properties->id]) }}';
		var url_makeAsPaid 						= '{{ route('makeAsPaid') }}';
		var url_add_property_invoice 			= '{{ route('add_property_invoice') }}';
		var url_add_default_payer 				= '{{ route('add_default_payer') }}';
		var url_get_default_payer 				= '{{ route('get_default_payer', ['property_id' => $properties->id]) }}';
		var url_addBankenContactEmployee 		= '{{ route('addBankenContactEmployee') }}';
		var url_delete_property_invoice 		= '{{ route("delete_property_invoice", ":id") }}';
		var url_set_property_invoice_email 		= '{{ route("set_property_invoice_email", ["id" => ":id", "email" => ":email"]) }}';
		var url_add_property_deal 				= '{{ route('add_property_deal') }}';
		var url_get_property_deals 				= '{{ route('get_property_deals', ['property_id' => $properties->id]) }}';

		var url_deal_mark_as_notrelease 		= '{{ route('deal_mark_as_notrelease') }}';
		var url_deal_mark_as_pending 			= '{{ route('deal_mark_as_pending') }}';
		var url_get_pending_deals 				= '{{ route('get_pending_deals', ['property_id' => $properties->id]) }}';
		var url_get_not_release_deals 			= '{{ route('get_not_release_deals', ['property_id' => $properties->id]) }}';

		var url_add_contract 					= '{{ route('add_contract') }}';
		var url_get_contract 					= '{{ route('get_contract', ['property_id' => $properties->id]) }}';
		var url_contract_mark_as_notrelease 		= '{{ route('contract_mark_as_notrelease') }}';
		var url_contract_mark_as_pending 		= '{{ route('contract_mark_as_pending') }}';
		var url_property_contractreleaseprocedure = '{{ url('property/contractreleaseprocedure') }}';
		var url_get_pending_contracts 			= '{{ route('get_pending_contracts', ['property_id' => $id]) }}';
		var url_get_not_release_contracts 		= '{{ route('get_not_release_contracts', ['property_id' => $id]) }}';
		var url_get_not_release__am_contracts 	= '{{ route('get_not_release_am_contracts', ['property_id' => $properties->id]) }}';
        var url_get_contract_logs				= '{{ route('get_contract_logs', ['property_id' => $id]) }}';

        var url_property_invoicenotrelease 		= '{{ url('property/invoicenotrelease') }}';
		var url_property_invoicemarkpending 	= '{{ url('property/invoicemarkpending') }}';
		var url_property_invoicemarkreject 		= '{{ url('property/invoicemarkreject') }}';
		var url_get_property_insurance_detail 	= '{{ route("get_property_insurance_detail", ["tab_id" => ":tab_id"]) }}';
		var url_add_property_insurance_detail 	= '{{ route('add_property_insurance_detail') }}';
		var url_get_property_insurance_detail_log	= '{{ route('get_property_insurance_detail_log', ['property_id' => $properties->id]) }}';
		var url_delete_property_insurance_title = '{{ route("delete_property_insurance_title", ["id" => ":id"]) }}';
		var url_get_rent_paid_data				= '{{ route('get_rent_paid_data', ['property_id' => $properties->id]) }}';
		var url_get_sum_of_property_rent_by_month				= '{{ url('getSumOfPropertyRentByMonth') }}';
		var url_keine_opos_update				= '{{ url('keine-opos-update') }}';
		var url_get_release_contracts 			= '{{ route('get_release_contracts', ['property_id' => $properties->id]) }}';
		var url_get_old_contracts 				= '{{ route('get_old_contracts', ['property_id' => $properties->id]) }}';
		var url_insurance_mark_as_pending       = '{{ route('insurance_mark_as_pending') }}';
		var url_insurance_mark_as_notrelease_am     = '{{ route('insurance_mark_as_notrelease_am') }}';
		var url_insurance_mark_as_notrelease       	= '{{ route('insurance_mark_as_notrelease') }}';
		var url_delete_insurance_detail_comment	= '{{ route('delete_insurance_detail_comment', ['id' => ':id']) }}';
		var url_delete_property_comment         = '{{ route('delete_property_comment', ['id' => ':id']) }}';
      	var url_add_property_comment            = '{{ route('add_property_comment') }}';
      	var url_get_property_comment            = '{{ route('get_property_comment') }}';
      	var url_get_recommended_table			= '{{ route('get_recommended_table', ['property_id' => $properties->id]) }}';
      	var url_get_tenancy_item_detail 		= '{{ route('get_tenancy_item_detail') }}';
      	var url_get_kosten_umbau				= '{{ route('get_kosten_umbau', ['property_id' => ':property_id', 'tenant_id' => ':tenant_id']) }}';
      	var url_saveractivity 					= '{{ route('saveractivity') }}';
      	var url_property_saveEmpfehlungFile 	= '{{ url("property/saveEmpfehlungFile") }}';
      	var url_property_vacantreleaseprocedure = '{{ url('property/vacantreleaseprocedure') }}';
      	var url_delete_new_vacant 				= '{{ route('delete_new_vacant') }}';
      	var url_changeempfehlungdetails 		= '{{ route('changeempfehlungdetails') }}';
      	var url_delete_recommended_comment		= '{{ route('delete_recommended_comment', ['id' => ':id']) }}';
		var url_get_recommended_comment			= '{{ route('get_recommended_comment', ['id' => ':id']) }}';
		var url_get_pending_hv_invoice 			= '{{ route('get_pending_hv_invoice', ['id' => $properties->id]) }}';
		var url_get_pending_user_invoice 		= '{{ route('get_pending_user_invoice', ['id' => $properties->id]) }}';
		var url_get_not_released_invoice_hv 	= '{{ route('get_not_released_invoice_hv', ['id' => $properties->id]) }}';
		var url_get_not_released_invoice_user 	= '{{ route('get_not_released_invoice_user', ['id' => $properties->id]) }}';

		var url_invoice_release_am 				= '{{ route('invoice_release_am') }}';
      	var url_invoice_release_hv 				= '{{ route('invoice_release_hv') }}';
      	var url_invoice_release_user 			= '{{ route('invoice_release_user') }}';
      	var url_invoice_release_falk 			= '{{ route('invoice_release_falk') }}';
		


		/*-------------------URL END------------------*/
	</script>

	{{-- <script src="{{ asset('js/bootstrap-editable.min.js') }}"></script> --}}
	<script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
	<script src="{{ asset('js/custom-datatable.js') }}"></script>
	<script src="{{ asset('js/tagsinput-revisited.js') }}"></script>
	<script src="{{ asset('file-manager/js/bootbox.min.js') }}"></script>
	<script src="{{ asset('file-manager/js/script.js') }}"></script>


	
	
	@if($user->role == 9)
		<style type="text/css">
			.invoice-release-request-am{
				display: block !important;
			}
			.contract-release-request{
				display: block !important;
			}
		</style>
	@endif

			<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>

			<script>

				$('body').on('click', '#mail-an-am, #mail-an-hv-bu, #mail-an-hv-pm, #mail-an-ek, #mail-an-sb', function() {
				    var title = $(this).text();
				    var type = $(this).attr('data-type');
				    // var user_id = $(this).attr('data-user-id');

				    $('#mail_an_am_modal').find('.modal-title').text(title);
				    $('#mail_an_am_modal').find('#mail_type').val(type);
				    // $('#mail_an_am_modal').find('#am_id').val(user_id);
				    $('#mail_an_am_modal').modal('show');
				});

		function makeInsuranceDataTable(tabid){
		    var url = url_get_property_insurance_detail.replace(":tab_id", tabid);
		    var title = $('#insurance-table-'+tabid).attr('data-type');
		    var sub = $('#insurance-table-'+tabid).attr('data-sub');

		    // var dom = '<lf<tr>ip>';
		    var dom = "<'row'<'col-md-2'l><'col-md-3 dt_custom_info'i><'col-md-7'f>>"+
		              "<'row'<'col-md-12'tr>>"+
		              "<'row'<'col-md-12'p>>";

		    if(title == 'OFFENE ANGEBOTE'){
		        var columns = [
		            null,
		            null,
		            null,
		            null,
		            null,
		            {"type": "new-date-time"},
		            { "visible": false },
		            null,
		            { "visible": false },
		            null,
		            null,
		            null,
		            null,
		            null,
		            null,
		        ];
		    }else if(title == 'VON FALK FREIGEGEBEN'){
		        var columns = [
		            null,
		            null,
		            null,
		            null,
		            null,
		            {"type": "new-date-time"},
		            {"type": "new-date-time"},
		            null,
		            { "visible": false },
		            null,
		            null,
		            null,
		            null,
		            null,
		            null,
		        ];
		        dom = "<'row'<'col-md-2'l><'col-md-3 dt_custom_info'i><'col-md-2 custom_button'><'col-md-5'f>>"+
		              "<'row'<'col-md-12'tr>>"+
		              "<'row'<'col-md-12'p>>";
		    }else{
		        var columns = [
		            null,
		            null,
		            null,
		            null,
		            null,
		            {"type": "new-date-time"},
		            { "visible": false },
		            null,
		            null,
		            null,
		            null,
		            null,
		            null,
		            null,
		            null,
		        ];
		    }

		    var order = (title == 'VON FALK FREIGEGEBEN') ? 6 : 5;

		    var instable = $('#insurance-table-'+tabid).dataTable({
		        "ajax": {
		            "url" : url+'?type='+sub,
		            // "async": false
		        },
		        dom: dom,
		        "order": [[order, "desc"]],
		        "columns": columns,
		        "initComplete": function(settings, json) {
		            /*var node = instable.fnGetNodes();

		            $(node).find('.btn-forward-to').each(function(){
		                var s = $(this).attr('data-subject');
		                var subject = sub+': '+s;

		                $(this).attr('data-subject', subject);
		                $(this).attr('data-title', title);
		            });*/
		            if(sub == 'Von Falk freigegeben'){
		                $('#insurance-table-'+tabid).find('tbody tr').each(function(i){
		                    /*if(i != 0){
		                        $(this).addClass('hidden');
		                    }*/
		                    var release_date = $.trim($(this).find('td:eq(6)').text());
		                    if(!release_date){
		                        $(this).addClass('hidden');
		                    }
		                });
		                $('#insurance-table-'+tabid+'_paginate').addClass('hidden');
		                $('#insurance-table-'+tabid).closest('.dataTables_wrapper').find('.custom_button').html('<button type="button" class="btn btn-primary btn-xs btn-show-more-record">Weitere Angebote anzeigen</button>');
		            }
		        },
		        "drawCallback": function( settings ) {
		            $('#insurance-table-'+tabid).find('.btn-forward-to').each(function(){
		                var s = $(this).attr('data-subject');
		                var subject = sub+': '+s;

		                $(this).attr('data-subject', subject);
		                $(this).attr('data-title', title);
		            });
		        }
		    });
		}

		$('body').on('click', '.btn-show-more-record', function(){
		    var text = $(this).text();
		    var tableid = $(this).closest('.modal-body').find('table').attr('id');
		    

		    if(text == 'Weitere Angebote anzeigen'){
		        $(this).text('Freigegebenes Angebot anzeigen');
		        $('#'+tableid).find('tbody tr').removeClass('hidden');
		        $('#'+tableid+'_paginate').removeClass('hidden');
		    }else{
		        $(this).text('Weitere Angebote anzeigen');
		        // $('#'+tableid).find('tbody tr').not(':first').addClass('hidden');
		        $('#'+tableid).find('tbody tr').each(function(i){
		            var release_date = $.trim($(this).find('td:eq(6)').text());
		            if(!release_date){
		                $(this).addClass('hidden');
		            }
		        });

		        $('#'+tableid+'_paginate').addClass('hidden');
		    }
		});
		
		var vacant_release_type = "";
		var vacant_id="";
		$('body').on('click', '.invoice-release-request', function() {
	        vacant_release_type = $(this).attr('data-column');
	        vacant_id = $(this).attr('data-id');
	        $('.invoice-release-comment').val("")
	        $('#invoice-release-property-modal').modal();

	        if(vacant_release_type=="release2")
	        {
	            $('.i-message').addClass('hidden');
	            $('.i-message2').removeClass('hidden');
	        }
	        else{
	            $('.i-message').removeClass('hidden');
	            $('.i-message2').addClass('hidden');
	        }

	    });
		$('body').on('click', '.invoice-release-submit', function() {
	        comment = $('.invoice-release-comment').val();
	        $.ajax({
	            type: 'POST',
	            url: url_property_invoicereleaseprocedure+"?internal={{$internal}}",
	            data: {
	                id: vacant_id,
	                step: vacant_release_type,
	                _token: _token,
	                comment: comment,
	                property_id: $('#selected_property_id').val()
	            },
	            success: function(data) {
	                // getprovisionbutton();
	                $('#add_property_invoice_table').DataTable().ajax.reload();
	                $('#add_property_invoice_table2').DataTable().ajax.reload();
	                $('#invoice_mail_table').DataTable().ajax.reload();
	            }
	        });
	    });




	    $('#form_add_insurance_tab_title').submit(function(event) {
        event.preventDefault();

        var $this = $(this);
        var msgElement = $('#add_insurance_tab_title_msg');
        var url = $(this).attr('action');
        var formData = new FormData($(this)[0]);

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache : false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $($this)[0].reset();
                    $('#add_insurance_tab_title_modal').modal('hide');
                    showFlash($('#insurance_tab_msg'), result.message, 'success');

                    var title_delete_url = url_delete_property_insurance_title.replace(':id', result.data.id);

                    var html = $('#insurance-tab-hidden-html').html();
                    html = html.replace(/##ID##/g, result.data.id);
                    html = html.replace('##TITLE##', result.data.title);
                    html = html.replace('##TITLE-DELETE-URL##', title_delete_url);
                    html = html.replace('hidden-table', '');
                    $('#insurance-table-content').append(html);

                    makeInsuranceDataTable(result.data.id);
                    getAngebotButton();

                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    $('body').on('click', '.delete-title', function() {
        var url = $(this).attr('data-url');

        var $this = $(this);
        var msgElement = $('#insurance_tab_msg');

        if(confirm('Are you sure want to delete this title?')){
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if(result.status){
                        $($this).closest('.main_card').remove();
                        $('#insurance-tab-log').DataTable().ajax.reload();
                    }else{
                      showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error){
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                    console.log({error});
                }
            });
        }
        return false;

    });

    $('body').on('click', '.btn-insurance-tab-detail', function() {
        var tab_id = $(this).attr('data-tabid');
        if(tab_id != ''){
            $('#add_insurance_tab_detail_modal').find('#form_add_insurance_tab_detail').attr('action', url_add_property_insurance_detail);
            $('#add_insurance_tab_detail_modal').find('#property_insurance_tab_id').val(tab_id);
            $('#add_insurance_tab_detail_modal').modal('show');
        }else{
           showFlash($('#insurance_tab_msg'), 'Somthing want wrong!', 'error');
        }
    });

    $('#form_add_insurance_tab_detail').submit(function(event) {
        event.preventDefault();

        var $this = $(this);
        var msgElement = $('#add_insurance_tab_detail_msg');
        var url = $(this).attr('action');
        var formData = new FormData($(this)[0]);
        formData.append("property_id", current_property_id);

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache : false,
            beforeSend: function() {
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){

                    $($this)[0].reset();
                    $('#insurance_tab_gdrive_file_basename').val('');
                    $('#insurance_tab_gdrive_file_dirname').val('');
                    $('#insurance_tab_gdrive_file_type').val('');
                    $('#insurance_tab_gdrive_file_name').val('');
                    $('#insurance_tab_gdrive_file_name_span').text('');
                    $('#insurance_tab_gdrive_file_upload').val('');

                    $('#add_insurance_tab_detail_modal').modal('hide');
                    showFlash($('#insurance_tab_msg'), result.message, 'success');
                    $('#insurance-table-'+result.data.property_insurance_tab_id).DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    if($(".tbl-insurance-tab-detail:not(.hidden-table)").length > 0){
        $(".tbl-insurance-tab-detail:not(.hidden-table)").each(function(){
            var tabid = $(this).attr('data-tabid');
            makeInsuranceDataTable(tabid);
        });
    }

    $('body').on('click', '.edit-insurance-tab-detail', function() {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result){
                    // $('#add_insurance_tab_detail_modal').find("input[name='name']").val(result.name);
                    $('#add_insurance_tab_detail_modal').find("input[name='amount']").val(result.amount);
                    $('#add_insurance_tab_detail_modal').find("textarea[name='comment']").val(result.comment);
                }
            },
        });

        $('#add_insurance_tab_detail_modal').find('#form_add_insurance_tab_detail').attr('action', url_add_property_insurance_detail+'/'+id);
        $('#add_insurance_tab_detail_modal').modal('show');
    });

    $('body').on('click', '.delete-insurance-tab-detail', function() {
        var tab_id = $(this).closest('table').attr('data-tabid');
        var url = $(this).attr('data-url');
        var msgElement = $('#insurance_tab_msg');

        if(confirm('Are you sure want to delete this record?')){
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if(result.status){
                        $('#insurance-table-'+tab_id).DataTable().ajax.reload();
                        $('#insurance-tab-log').DataTable().ajax.reload();
                    }else{
                      showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error){
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                    console.log({error});
                }
            });
        }
        return false;
    });



    $('body').on('click', '.property-insurance-release-request', function() {

        if(is_falk==1){
            var length = $(this).closest('.modal-body').find('table').find('[data-field="falk_status"]:checked').length;
            var msg = "Bitte Checkbox Falk auswählen!";
        }else{
            var length = $(this).closest('.modal-body').find('table').find('[data-field="asset_manager_status"]:checked').length;
            var msg = "Bitte eine AM Checkbox auswählen!";
        }

        if(length > 0){
            vacant_release_type = $(this).attr('data-column');
            vacant_id = $(this).attr('data-id');
            $('.property-insurance-comment').val("")
            $('#property-insurance-release-property-modal').modal();
        }else{
            alert(msg); return false;
        }
    });

    $('body').on('click', '.property-insurance-submit', function() {
        am_id = fk_id = 0;
        $('#insurance-table-'+vacant_id+' .am_falk_status').each(function(){
                if($(this).is(':checked') && $(this).attr('data-field')=="falk_status")
                    fk_id = $(this).attr('data-id');
                if($(this).is(':checked') && $(this).attr('data-field')=="asset_manager_status")
                    am_id = $(this).attr('data-id');
        });
        comment = $('.property-insurance-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_dealreleaseprocedure,
            data: {
                id: vacant_id,
                step: vacant_release_type,
                _token: _token,
                am_id:am_id,
                fk_id:fk_id,
                comment: comment,
            },
            success: function(data) {
                if(vacant_release_type=="insurance_request")
                        getAngebotButton();
                $('#insurance-tab-log').DataTable().ajax.reload();
                $('.sec-'+vacant_id).find('.btn').remove();
                $('.sec-'+vacant_id).find('.am_falk_status').each(function(){
                    $(this).removeClass('am_falk_status');
                });
                // $('#property_deal_table').DataTable().ajax.reload();
                // $('#deal_mail_table').DataTable().ajax.reload();
            }
        });
    });

    $(document).on('click', ".insurance-tab-upload-file-icon", function(){
        $("#insurance_tab_gdrive_file_upload").trigger('click');
    });

    $(document).on('change', "#insurance_tab_gdrive_file_upload", function(e){
        var fileName = e.target.files[0].name;
        $("#insurance_tab_gdrive_file_name_span").text(fileName);
    });

    $(document).on('click', ".link-button-gdrive-insurance-tab", function(e){
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('property_deals');
        $("#select-gdrive-file-callback").val('gdeiveSelectPropertyInsurance');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir, $("#current_tab_name_insurance_tab").val());
    });

    window.gdeiveSelectPropertyInsurance = function(basename, dirname,curElement, dirOrFile){
        $("#insurance_tab_gdrive_file_basename").val(basename);
        $("#insurance_tab_gdrive_file_dirname").val(dirname);
        $("#insurance_tab_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').text();
        $("#insurance_tab_gdrive_file_name").val(text);
        $("#insurance_tab_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    var insurance_tab_log = $('#insurance-tab-log').dataTable({
        "ajax": url_get_property_insurance_detail_log,
        "order": [
            [6, "desc"]
        ],
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            null,
        ]
    });

    /*--------------------JS FOR PROPERTY INSURANCE TAB START-----------------*/



		function showResult(result) {

		    document.getElementById('latitude').value = result.geometry.location.lat();
		    document.getElementById('longitude').value = result.geometry.location.lng();
		}

		function getLatitudeLongitude(callback, address) {
	        // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
	        address = address || 'Ferrol, Galicia, Spain';
	        // Initialize the Geocoder
	        geocoder = new google.maps.Geocoder();
	        if (geocoder) {
	            geocoder.geocode({
	                'address': address
	            }, function (results, status) {
	                if (status == google.maps.GeocoderStatus.OK) {
	                    callback(results[0]);
	                }
	            });
	        }
	    }


		function generate_option_from_json(jsonData, fromLoad){
		    //Load Category Json Data To Brand Select
		    if (fromLoad === 'category_to_brand'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected> <?php echo trans('app.select_a_brand') ?> </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].brand_name +' </option>';
		            }
		            $('#brand_select').html(option);
		            $('#brand_select').select2();
		        }else {
		            $('#brand_select').html('');
		            $('#brand_select').select2();
		        }
		        $('#brand_loader').hide('slow');
		    }else if(fromLoad === 'country_to_state'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Bundesland auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
		            }
		            $('#state_select').html(option);
		            //$('#state_select').select2();
		        }else {
		            $('#state_select').html('');
		           // $('#state_select').select2();
		        }
		        $('#state_loader').hide('slow');

		    }else if(fromLoad === 'vcountry_to_state'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Bundesland auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
		            }
		            $('#vstate_select').html(option);
		            //$('#state_select').select2();
		        }else {
		            $('#vstate_select').html('');
		           // $('#state_select').select2();
		        }
		        $('#state_loader').hide('slow');

		    }
		    else if(fromLoad === 'excountry_to_state'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Bundesland auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].state_name +' </option>';
		            }
		            $('#excountry_to_state').html(option);
		            //$('#state_select').select2();
		        }else {
		            $('#excountry_to_state').html('');
		           // $('#state_select').select2();
		        }
		        $('#state_loader').hide('slow');

		    }else if(fromLoad === 'state_to_city'){
		        var option = '';
		        if (jsonData.length > 0) {
		            option += '<option value="0" selected>  Stadt auswählen  </option>';
		            for ( i in jsonData){
		                option += '<option value="'+jsonData[i].id+'"> '+jsonData[i].city_name +' </option>';
		            }
		            $('#city_select').html(option);
		            //$('#city_select').select2();
		        }else {
		            $('#city_select').html('');
		            //$('#city_select').select2();
		        }
		        $('#city_loader').hide('slow');
		    }
		}

	    $(function(){
	    	$('.multi-category').select2();

	        $('#street').change(function(){
	            address = "";
	            var address = $(this).val();
	            // getLatitudeLongitude(showResult, address)
	        });
	    });

		$(function(){
		    $('#country').change(function(){
		        var country_id = $(this).val();
		        $('#state_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_state_by_country') }}',
		            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'country_to_state');
		            }
		        });
		    });

		    $('#vcountry').change(function(){
		        var country_id = $(this).val();
		        $('#state_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_state_by_country') }}',
		            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'vcountry_to_state');
		            }
		        });
		    });

		    $('#excountry').change(function(){
		        var country_id = $(this).val();
		        $('#state_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_state_by_country') }}',
		            data : { country_id : country_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'excountry_to_state');
		            }
		        });
		    });

		    $('[name="state"]').change(function(){
		        var state_id = $(this).val();
		        $('#city_loader').show();
		        $.ajax({
		            type : 'POST',
		            url : '{{ route('get_city_by_state') }}',
		            data : { state_id : state_id,  _token : '{{ csrf_token() }}' },
		            success : function (data) {
		                generate_option_from_json(data, 'state_to_city');
		            }
		        });
		    });
		});
	</script>

	@if(isset($ads) && $ads->latitude)
		<script type="text/javascript">
			var myLatLng = {lat: {{ $ads->latitude }}, lng: {{ $ads->longitude }} };
			var mytitle = "{{$ads->title}}";
		</script>
	@else
		<script type="text/javascript">
			var myLatLng = {lat: 48.135690, lng: 11.555480 };
			var mytitle = "You";
		</script>
	@endif

	<script type="text/javascript">
		window.einkauf_file_link_isPageReload = false;

	    // This example adds a search box to a map, using the Google Place Autocomplete
	    // feature. People can enter geographical searches. The search box will return a
	    // pick list containing a mix of places and predicted search terms.

		function initAutocomplete() {

	       	var map = new google.maps.Map(document.getElementById('dvMap'), {
	           center: myLatLng,
	           zoom: 15,
	           mapTypeId: google.maps.MapTypeId.ROADMAP
	       	});

	       	// Create the search box and link it to the UI element.
	       	var input = document.getElementById('pac-input');
	       	var searchBox = new google.maps.places.SearchBox(input);
	       	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	       	// Bias the SearchBox results towards current map's viewport.
	       	map.addListener('bounds_changed', function() {
	           	searchBox.setBounds(map.getBounds());
	       	});

	       	//Click event for getting lat lng
	       	google.maps.event.addListener(map, 'click', function (e) {
	           $('input#latitude').val(e.latLng.lat());
	           $('input#longitude').val(e.latLng.lng());
	       	});

	       	var marker = new google.maps.Marker({
	           position: myLatLng,
	           map: map,
	           title: mytitle
	       	});

	       	marker.setMap(map);

	       	var markers = [];
	       	// [START region_getplaces]
	       	// Listen for the event fired when the user selects a prediction and retrieve
	       	// more details for that place.
	       	searchBox.addListener('places_changed', function() {
	           var places = searchBox.getPlaces();

	           if (places.length == 0) {
	               return;
	           }

	           // Clear out the old markers.
	           markers.forEach(function(marker) {
	               marker.setMap(null);
	           });
	           markers = [];

	           // For each place, get the icon, name and location.
	           var bounds = new google.maps.LatLngBounds();
	           places.forEach(function(place) {
	               var icon = {
	                   url: place.icon,
	                   size: new google.maps.Size(71, 71),
	                   origin: new google.maps.Point(0, 0),
	                   anchor: new google.maps.Point(17, 34),
	                   scaledSize: new google.maps.Size(25, 25)
	               };

	               $('#latitude').val(place.geometry.location.lat());
	               $('#longitude').val(place.geometry.location.lng());

	               // Create a marker for each place.
	               markers.push(new google.maps.Marker({
	                   map: map,
	                   icon: icon,
	                   title: place.name,
	                   position: place.geometry.location
	               }));

	               if (place.geometry.viewport) {
	                   // Only geocodes have viewport.
	                   bounds.union(place.geometry.viewport);
	               } else {
	                   bounds.extend(place.geometry.location);
	               }
	           });
	           map.fitBounds(bounds);
	       	});
	       	// [END region_getplaces]

	       	var map2 = new google.maps.Map(document.getElementById('dvMap2'), {
	           center: myLatLng,
	           zoom: 15,
	           mapTypeId: google.maps.MapTypeId.ROADMAP
	       	});

	       	// Create the search box and link it to the UI element.
	       	var input = document.getElementById('pac-input2');
	       	var searchBox2 = new google.maps.places.SearchBox(input);
	       	map2.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

	       	// Bias the SearchBox results towards current map's viewport.
	       	map2.addListener('bounds_changed', function() {
	           searchBox2.setBounds(map.getBounds());
	       	});

	       	//Click event for getting lat lng
	       	google.maps.event.addListener(map2, 'click', function (e) {
	           $('input#l-latitude').val(e.latLng.lat());
	           $('input#l-longitude').val(e.latLng.lng());
	       	});

	       	var marker = new google.maps.Marker({
	           position: myLatLng,
	           map: map2,
	           title: mytitle
	       	});
	       	marker.setMap(map2);

	       	var markers = [];
	       	// [START region_getplaces]
	       	// Listen for the event fired when the user selects a prediction and retrieve
	       	// more details for that place.
	       	searchBox2.addListener('places_changed', function() {
	           var places = searchBox2.getPlaces();

	           if (places.length == 0) {
	               return;
	           }

	           // Clear out the old markers.
	           markers.forEach(function(marker) {
	               marker.setMap(null);
	           });
	           markers = [];

	           // For each place, get the icon, name and location.
	           var bounds = new google.maps.LatLngBounds();
	           places.forEach(function(place) {
	               var icon = {
	                   url: place.icon,
	                   size: new google.maps.Size(71, 71),
	                   origin: new google.maps.Point(0, 0),
	                   anchor: new google.maps.Point(17, 34),
	                   scaledSize: new google.maps.Size(25, 25)
	               };

	               $('#l-latitude').val(place.geometry.location.lat());
	               $('#l-longitude').val(place.geometry.location.lng());

	               // Create a marker for each place.
	               markers.push(new google.maps.Marker({
	                   map: map2,
	                   icon: icon,
	                   title: place.name,
	                   position: place.geometry.location
	               }));

	               if (place.geometry.viewport) {
	                   // Only geocodes have viewport.
	                   bounds.union(place.geometry.viewport);
	               } else {
	                   bounds.extend(place.geometry.location);
	               }
	           });
	   });
	   }




	function init()
	{
        var input = document.getElementById( 'google_address' );

        if ( null === input ) {
            return;
        }
        autoComplete = new google.maps.places.Autocomplete(input);
        autoComplete.setFields(['address_components', 'geometry', 'icon', 'name']);
        autoComplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autoComplete.getPlace();
        $("#location_latitude").val(place.geometry.location.lat());
        $("#location_longitude").val(place.geometry.location.lng());
    }















    /*----------------JS FOR ADD PROPERTY INVOICE - START-----------------*/
    $(document).ajaxComplete(function() {
        if ($('.invoice-comment').length)
        	$('.invoice-comment').editable();
        // if ($('.default-payer-comment').length)
        	// $('.default-payer-comment').editable();
        if ($('.property-deal-comment').length)
        	$('.property-deal-comment').editable();
        if ($('.contract-comment').length)
            $('.contract-comment').editable();
    });

     // 1) open invoice
    var add_property_invoice_table = $('#add_property_invoice_table').dataTable({
        "ajax": url_get_property_invoice+"?internal={{$internal}}",
        "order": [
            [9, 'desc']
        ],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            {"type": "numeric-comma"},
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            // { "type": "new-date" },
            // null,
            // null,
            // null,
            // null,
            // null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 2) Released invoice
    var add_property_invoice_table2 = $('#add_property_invoice_table2').dataTable({
        "ajax": url_get_release_property_invoice+"?internal={{$internal}}",
        "order": [
            [9, 'desc']
        ],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            {"type": "numeric-comma"},
            null,
            null,
            null,
            null,
            {"type": "new-date-time"},
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });
    
    // 3) pending AM invoice
    var pending_am_invoice_table = $('#pending_am_invoice_table').dataTable({
        "ajax": url_get_pending_am_invoice+"?internal={{$internal}}",
        "order": [[ 9, 'desc' ]],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 4) pending Hv invoice
    var pending_hv_invoice_table = $('#pending_hv_invoice_table').dataTable({
        "ajax": url_get_pending_hv_invoice+"?internal={{$internal}}",
        "order": [[ 9, 'desc' ]],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 5) pending User invoice
    var pending_user_invoice_table = $('#pending_user_invoice_table').dataTable({
        "ajax": url_get_pending_user_invoice+"?internal={{$internal}}",
        "order": [[ 9, 'desc' ]],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            // null,
            // null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 6) Pending Falk invoice
    var add_property_invoice_table4 = $('#add_property_invoice_table4').dataTable({
        "ajax": url_get_pending_invoice+"?internal={{$internal}}",
        "order": [[ 9, 'desc' ]],
        "columnDefs": [{
            className: "text-right",
            "targets": [4]
        }],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            // null,
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 7) Not release invoice (AM)
    var table_rejected_invoice = $('#table_rejected_invoice').dataTable({
        "ajax": url_get_not_released_invoice_am+"?internal={{$internal}}",
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 8) Not release invoice (HV)
    var table_not_release_hv_invoice = $('#table_not_release_hv_invoice').dataTable({
        "ajax": url_get_not_released_invoice_hv+"?internal={{$internal}}",
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 9) Not release invoice (User)
    var table_not_release_user_invoice = $('#table_not_release_user_invoice').dataTable({
        "ajax": url_get_not_released_invoice_user+"?internal={{$internal}}",
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 10) Not release invoice (Falk)
    var add_property_invoice_table3 = $('#add_property_invoice_table3').dataTable({
        "ajax": url_get_not_released_invoice+"?internal={{$internal}}",
        "order": [[ 9, 'desc' ]],
        "columnDefs": [
            { className: "text-right", "targets": [4] }
        ],
        "columns": [
            {"orderable": false, "searchable": false},
            null,
            null,
            { "type": "new-date" },
            { "type": "numeric-comma" },
            null,
            null,
            null,
            null,
            { "type": "new-date-time" },
            // null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]
    });

    // 11) invoice mail log
    var invoice_mail_table = $('#invoice_mail_table').dataTable({
        "ajax": url_get_invoice_logs+"?internal={{$internal}}",
        "order": [
            [5, "desc"]
        ],
        "columns": [
            null,
            null,
            null,
            { "type": "new-date" },
            null,
            {"type": "new-date-time"},
            null
        ]
    });

    $('body').on('change', '.row_checkall', function() {
        var table_obj;
        var table = $(this).closest('.main_div').find('table').attr('id');

        if(table == 'add_property_invoice_table'){
            table_obj = add_property_invoice_table;
        }else if(table == 'add_property_invoice_table4'){
            table_obj = add_property_invoice_table4;
        }else if(table == 'add_property_invoice_table2'){
            table_obj = add_property_invoice_table2;
        }else if(table == 'table_rejected_invoice'){
            table_obj = table_rejected_invoice;
        }else if(table == 'add_property_invoice_table3'){
            table_obj = add_property_invoice_table3;
        }
        
        if($(this).is(':checked')){
            table_obj.$(".row_checkbox").prop('checked', true).trigger('change');
        }else{
            table_obj.$(".row_checkbox").prop('checked', false).trigger('change');
        }
    });

    $('body').on('click', '.download_invoice', function() {

        var table_id = $(this).closest('.main_div').find('table').attr('id');
        var url = $(this).attr('data-url');
        var checked_arr = [];
        var table_obj;

        if(table_id == 'add_property_invoice_table'){
            table_obj = add_property_invoice_table;
        }else if(table_id == 'add_property_invoice_table4'){
            table_obj = add_property_invoice_table4;
        }else if(table_id == 'add_property_invoice_table2'){
            table_obj = add_property_invoice_table2;
        }else if(table_id == 'table_rejected_invoice'){
            table_obj = table_rejected_invoice;
        }else if(table_id == 'add_property_invoice_table3'){
            table_obj = add_property_invoice_table3;
        }

        table_obj.$('.row_checkbox').each(function(){
            if($(this).prop('checked')){
                checked_arr.push($(this).val());
            }
        });

        if(checked_arr.length > 0){
            var ids = checked_arr.join(',');
            var url_load = url+'?id='+ids+'&tab=invoice';
            location.href = url_load;
            return false;
        }else{
            alert('Select at least one row!');
        }
    });

    $('body').on('click', '.mark-as-paid', function() {
        var id = $(this).attr('data-id');
        $this = $(this);
        var url = url_makeAsPaid + "?id=" + id;
        $.ajax({
            url: url,
        }).done(function(data) {
            $this.removeClass('btn-primary');
            $this.removeClass('mark-as-paid');
            $this.addClass('btn-success');
            $this.html("Erledigt");
        });
    });
    $(document).on('click', ".property_invoice-upload-file-icon", function() {
        $("#property_invoice_gdrive_file_upload").trigger('click');
    });
    $(document).on('change', "#property_invoice_gdrive_file_upload", function(e) {
        var fileName = e.target.files[0].name;
        $("#property_invoice_gdrive_file_name_span").text(fileName);
    });
    $(document).on('click', ".link-button-gdrive-invoice", function(e) {
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('property_invoice');
        $("#select-gdrive-file-callback").val('gdeiveSelectPropertyInvoice');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir);
    });
    window.gdeiveSelectPropertyInvoice = function(basename, dirname, curElement, dirOrFile) {
        $("#property_invoice_gdrive_file_basename").val(basename);
        $("#property_invoice_gdrive_file_dirname").val(dirname);
        $("#property_invoice_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').text();
        $("#property_invoice_gdrive_file_name").val(text);
        $("#property_invoice_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }
    $(document).on("focus", ".mask-input-new-date", function() {
        $(this).mask('00.00.0000');
    });
    $(document).on("focus", ".mask-input-number", function() {
        $(this).mask('#.##0,00', {
            reverse: true
        });
    });
    function showFlash(element, msg, type) {
	    if (type == 'error') {
	        $(element).html('<p class="alert alert-danger">' + msg + '</p>');
	    } else {
	        $(element).html('<p class="alert alert-success">' + msg + '</p>');
	    }
	    setTimeout(function() {
	        $(element).empty();
	    }, 5000);
	}
    $('#add_property_invoice_form').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var msgElement = $('#add_property_invoice_msg');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_add_property_invoice,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $this.find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                $this.find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status == true) {
                    showFlash(msgElement, result.message, 'success');
                    $($this)[0].reset();
                    $('#add_property_invoice_table').DataTable().ajax.reload();
                    setTimeout(function() {
                        $('#add_property_invoice_modal').modal('hide');
                    }, 1000);
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                // $($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({
                    error
                });
            }
        });
    });
    $('#add_banken_contact_employee_form').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var msgElement = $('#add_banken_contact_employee_msg');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_addBankenContactEmployee,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                // $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result) {
                // $($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status == true) {
                    showFlash(msgElement, result.message, 'success');
                    $($this)[0].reset();
                    $('#list-banks').DataTable().ajax.reload();
                    $('#add_banken_contact_employee_model').modal('hide');
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                // $($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({
                    error
                });
            }
        });
    });
    $(document).on('click', '.add-bank-contact-btn', function(e) {
        var id = $(this).attr('data-id');
        $("#add_banken_contact_employee_model").modal('show');
        $('#add_banken_contact_employee_banken_id').val(id);
    });
    $('body').on('click', '.btn-delete-property-invoice', function() {

        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var msgElement = $('#property_invoice_msg');

        if (confirm('Are you sure want to delete this invoice?')) {
            var url = url_delete_property_invoice;
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');
                        if(type == 'open-invoice'){
                            $('#add_property_invoice_table').DataTable().ajax.reload();
                        }
                        else if(type == 'not-release-invoice'){
                            $('#add_property_invoice_table3').DataTable().ajax.reload();
                        }
                        else if(type == 'not-release-invoice-am'){
                            $('#table_rejected_invoice').DataTable().ajax.reload();
                        }
                        else{
                            $('#add_property_invoice_table4').DataTable().ajax.reload();
                        }
                        $('#invoice_mail_table').DataTable().ajax.reload();
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });
    $('body').on('change', '.property-invoice-user', function() {
        var email = $(this).val();
        var id = $(this).attr('data-id');
        var msgElement = $('#property_invoice_msg');
        var url = url_set_property_invoice_email;
        url = url.replace(':email', email);
        url = url.replace(':id', id);
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if (result.status == true) {
                    $('#add_property_invoice_table').DataTable().ajax.reload();
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                showFlash(msgElement, 'Somthing want wrong!', 'error');
            }
        });
    });

    var not_release_id = 0;
    $('body').on('click','.mark-as-not-release',function(){
        not_release_id = $(this).attr('data-id');
        $('#not-release-modal').modal();
    });

    $('body').on('click','.invoice-not-release-submit',function(){
        comment = $('.invoice-not-release-comment').val();
        $.ajax({
              type : 'POST',
              url : url_property_invoicenotrelease,
              data : {id:not_release_id, _token : _token,comment:comment,property_id:$('#selected_property_id').val()},
              success : function (data) {
                  // getprovisionbutton();
                  $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#add_property_invoice_table3').DataTable().ajax.reload();

                  // $('#invoice_mail_table').DataTable().ajax.reload();

              }
          });
    });

    $('body').on('click','.mark-as-pending',function(){
        not_release_id = $(this).attr('data-id');
        $('#pending-modal').modal();
    });
    $('body').on('click','.invoice-pending-submit',function(){
        comment = $('.invoice-pending-comment').val();
        $.ajax({
              type : 'POST',
              url : url_property_invoicemarkpending,
              data : {id:not_release_id, _token : _token, comment:comment,property_id:$('#selected_property_id').val()},
              success : function (data) {
                  // getprovisionbutton();
                  $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#add_property_invoice_table4').DataTable().ajax.reload();
              }
          });
    });

    var invoice_reject_id;
    $('body').on('click','.btn-reject',function(){
        invoice_reject_id = $(this).attr('data-id');
        $('#invoice-reject-modal').modal();
    });

    $('body').on('click','.invoice-reject-submit',function(){
        comment = $('.invoice-reject-comment').val();
        $.ajax({
            type : 'POST',
            url : url_property_invoicemarkreject,
            data : {
                id:invoice_reject_id,
                _token : _token,
                comment:comment,
                property_id:$('#selected_property_id').val()
            },
            success : function (data) {
                  $('#add_property_invoice_table').DataTable().ajax.reload();
                  $('#table_rejected_invoice').DataTable().ajax.reload();
            }
        });
    });

    /*----------------JS FOR ADD PROPERTY INVOICE - END-----------------*/


       /*----------------JS FOR ADD DEFAULT PAYER - START------------------*/
    $(document).on('click', ".default-payer-upload-file-icon", function() {
        $("#default_payer_gdrive_file_upload").trigger('click');
    });
    $(document).on('click', ".default-payer-upload-file-icon2", function() {
        $("#default_payer_gdrive_file_upload2").trigger('click');
    });
    $(document).on('change', "#default_payer_gdrive_file_upload", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span").text(fileName);
    });
    $(document).on('change', "#default_payer_gdrive_file_upload2", function(e) {
        var fileName = e.target.files[0].name;
        $("#default_payer_gdrive_file_name_span2").text(fileName);
    });
    $(document).on('click', ".link-button-gdrive-default-payer", function(e) {
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('default_payer');
        $("#select-gdrive-file-callback").val('gdeiveSelectDefaultPayer');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_name_payer").val());
    });
    window.gdeiveSelectDefaultPayer = function(basename, dirname, curElement, dirOrFile) {
        $("#default_payer_gdrive_file_basename").val(basename);
        $("#default_payer_gdrive_file_dirname").val(dirname);
        $("#default_payer_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').text();
        $("#default_payer_gdrive_file_name").val(text);
        $("#default_payer_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }
    $('#add_default_payer_form').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var msgElement = $('#add_default_payer_msg');
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: url_add_default_payer,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            beforeSend: function() {
                $this.find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);

            },
            success: function(result) {
                $this.find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                if (result.status == true) {
                    showFlash($('#default_payer_msg'), result.message, 'success');
                    $this[0].reset();
                    $("#default_payer_gdrive_file_name_span").text("");
                    $('#default_payer_table').DataTable().ajax.reload();
                    setTimeout(function() {
                        $('#add_default_payer_modal').modal('hide');
                        $('#year-select').trigger('change');
                    }, 1000);
                } else {
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error) {
                $this.find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({
                    error
                });
            }
        });
    });
    var default_payer_table = $('#default_payer_table').dataTable({
        "ajax": url_get_default_payer,
        "order": [
            [6, 'desc']
        ],
        "columns": [
            null,
            // null,
            // null,
            null,
            null,
            null,
            null, {
                "type": "new-date-time"
            },
            // null,
            null,
        ]
    });

    $('body').on('click', '.btn-delete-default-payer', function() {
        var url = $(this).attr('data-url');
        var msgElement = $('#default_payer_msg');
        if (confirm('Are you sure want to delete this default payer?')) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');
                        $('#default_payer_table').DataTable().ajax.reload();
                        $('#year-select').trigger('change');
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });
    /*----------------JS FOR ADD DEFAULT PAYER - END------------------*/


		// $(document).on('click', ".default-payer-upload-file-icon2", function() {
		// 	$("#default_payer_gdrive_file_upload2").trigger('click');
		// });
        // $(document).on('change', "#default_payer_gdrive_file_upload2", function(e) {
        //     var fileName = e.target.files[0].name;
        //     $("#default_payer_gdrive_file_name_span2").text(fileName);
        // });

		// getRentPaidData();

		function getRentPaidData(){
			$.ajax({
				url: '<?php echo e(route('get_rent_paid_data', ['property_id' => $id])); ?>',
			}).done(function (data) {
				$('#rent-paid-data').html(data);
				var columns = [
					null,
					null,
					null,
					{ "type": "numeric-comma"},
					{ "type": "numeric-comma"},
					null,
					{ "type": "numeric-comma"},
					null,
					null,
                    null,
                    null,
                    null,
                    null,
                    null,

				];
				makeDatatable($('#rent-paid-table'), columns);
			});
		}

		$('#import').submit(function(event) {
			if (!$('#no_opos_status').is(':checked') && $('#default_payer_gdrive_file_upload2').val() == ''){
				event.preventDefault();
				toastr.error("Fehler- Keine Datei gefunden");
			}

		});

        $('#year-select').change(function(){
            var month = $(this).val();
            $.ajax({
                url: '<?php echo e(route('get_rent_paid_data', ['property_id' => $id])); ?>',
				data:{
                	month:month
				}
            }).done(function (data) {
                $('#rent-paid-data').html(data);
				var columns = [
					null,
					null,
					null,
					{ "type": "numeric-comma"},
					{ "type": "numeric-comma"},
					null,
					{ "type": "numeric-comma"},
					null,
					null,
                    null,
                    null,
                    null,
                    null,
                    null,

				];
				makeDatatable($('#rent-paid-table'), columns);
            })
        });



				$('.delete-file-data').on('click', function () {
					var id = $(this).attr('data-id');
					swal({
						title: 'OPOS Liste löschen?',
						text: "Dieser Schritt kann nicht rückgängig gemacht werden!",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ja, löschen!'
					}).then(function (isConfirm) {
						console.log(isConfirm.dismiss);
						if (isConfirm.dismiss !== 'cancel' && isConfirm.dismiss !== 'overlay' && isConfirm){
							$.ajaxSetup({
								headers: {
									'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
								}
							});
							$.ajax({
								type: 'DELETE',
								url :  "{{url('file-upload-data/delete')}}"+'/'+id,

								success : function (data)
								{
									sweetAlert("Erfolgreich gelöscht.");
									location.reload();
								}
							});
						}

					})

				});

		$('body').on('click', '#isKeineOposChecked', function() {
			var id = $(this).attr('data-id');
			v = 0;
			if ($(this).is(':checked')) v = 1;
			$.ajax({
				type: 'POST',
				url: url_keine_opos_update ,
				data: {
					id: id,
					value: v,
					_token: _token
				},
				success: function(data) {
					sweetAlert("Updated  Successfully");

				}
			});
		});



	/*--------------------Js FOR CONTRACT START------------------------*/

	$('body').on('click', '#btn-add-contract', function() {
        var modal = $('#add_contracts_modal');
        var property_id = $('#selected_property_id').val();

        $(modal).find("input[name='id']").val('');
        $(modal).find("input[name='property_id']").val(property_id);

        $(modal).find('#contract_file_div').show();
        $(modal).find('#contract_comment_div').show();

        $(modal).modal('show');
    });

    $('body').on('click', '.edit-contract', function() {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        var modal = $('#add_contracts_modal');

        $(modal).find('#contract_file_div').hide();
        $(modal).find('#contract_comment_div').hide()

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if(result.status){
                    $(modal).find("input[name='id']").val(result.data.id);
                    $(modal).find("input[name='property_id']").val(result.data.property_id);
                    $(modal).find("input[name='amount']").val(result.data.amount);
                    $(modal).find("input[name='date']").val(result.data.date);
                    $(modal).find("input[name='completion_date']").val(result.data.completion_date);
                    $(modal).find("input[name='termination_date']").val(result.data.termination_date);
                    $(modal).find("input[name='termination_am']").val(result.data.termination_am);
                    $(modal).find("select[name='category']").val(result.data.category);
                    $(modal).find("input[name='is_old']").prop('checked', (result.data.is_old == '1') ? true : false);
                }else{
                    alert(result.message);
                }
            },
            error: function(){
                alert('Somthing want wrong!');
            }
        });

        // $('#add_insurance_tab_detail_modal').find('#form_add_insurance_tab_detail').attr('action', url_add_property_insurance_detail+'/'+id);
        $(modal).modal('show');
    });

    $(document).on('click', ".contracts-upload-file-icon", function(){
        $("#contracts_gdrive_file_upload").trigger('click');
    });

    $(document).on('change', "#contracts_gdrive_file_upload", function(e){
        var fileName = e.target.files[0].name;
        $("#contracts_gdrive_file_name_span").text(fileName);
    });

    $(document).on('click', ".link-button-gdrive-contracts", function(e){
        $("#select-gdrive-file-model").modal('show');
        $("#select-gdrive-file-type").val('property_deals');
        $("#select-gdrive-file-callback").val('gdeiveSelectContracts');
        $("#select-gdrive-file-data").val('');
        selectGdriveFileLoad(workingDir,  $("#current_tab_name").val());
    });

    window.gdeiveSelectContracts = function(basename, dirname,curElement, dirOrFile){
        $("#contracts_gdrive_file_basename").val(basename);
        $("#contracts_gdrive_file_dirname").val(dirname);
        $("#contracts_gdrive_file_type").val(dirOrFile);
        var text = $(curElement).closest('tr').find('a.gdrive-file-link').text();
        $("#contracts_gdrive_file_name").val(text);
        $("#contracts_gdrive_file_name_span").text(text);
        $("#select-gdrive-file-model").modal('hide');
        // load_ractivity(data.property_id, data.tenant_id, $('#'+data.fileObjId).closest('form').find('.recommended-section'));
    }

    $('#add_contracts_form').submit(function(event) {
        event.preventDefault();

        var $this = $(this);
        var msgElement = $('#add_contracts_msg');
        var formData = new FormData($(this)[0]);
        var is_old = ($('#is_old').prop('checked')==true) ? true : false;

        $.ajax({
            url: url_add_contract,
            type: 'POST',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache : false,
            beforeSend: function() {
                //$($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                //$($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){

                    $($this)[0].reset();
                    $('#contracts_gdrive_file_basename').val('');
                    $('#contracts_gdrive_file_dirname').val('');
                    $('#contracts_gdrive_file_type').val('');
                    $('#contracts_gdrive_file_name').val('');
                    $('#contracts_gdrive_file_name_span').text('');
                    $('#contracts_gdrive_file_upload').val('');
                    
                    $('#add_contracts_modal').modal('hide');

                    if(is_old){
                        $('#old_contracts_table').DataTable().ajax.reload();
                    }else{
                        $('#contracts_table').DataTable().ajax.reload();
                    }

                    showFlash($('#contracts_msg'), result.message, 'success');
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                //$($this).find('button[type="submit"]').html('Speichern');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    $('body').on('click', '.btn-delete-contract', function() {
        var url = $(this).attr('data-url');
        var msgElement = $('#contracts_msg');
        var type = $(this).attr('data-type');

        if (confirm('Are you sure want to delete this contract?')) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    if (result.status == true) {
                        showFlash(msgElement, result.message, 'success');

                        if(type == 'old_contract'){
                            $('#old_contracts_table').DataTable().ajax.reload();
                        }else{
                            $('#contracts_table').DataTable().ajax.reload();
                        }
                    } else {
                        showFlash(msgElement, result.message, 'error');
                    }
                },
                error: function(error) {
                    showFlash(msgElement, 'Somthing want wrong!', 'error');
                }
            });
        } else {
            return false;
        }
    });

    var contract_not_release_id;
    $('body').on('click', '.contract-mark-as-not-release', function() {
        contract_not_release_id = $(this).attr('data-id');
        $('#contracts_mark_as_not_release').modal('show');
    });

    $('#form_contracts_mark_as_not_release').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_not_release_id,
            'comment': $(this).find('#input_contracts_mark_as_not_release').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#contracts_mark_as_not_release_error');
        $.ajax({
            url: url_contract_mark_as_notrelease,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_not_release').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                    $('#not_release_contracts_table').DataTable().ajax.reload();
                    $('#contracts_mail_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var contract_not_release_am_id;
    $('body').on('click', '.contract-mark-as-not-release-am', function() {
        contract_not_release_am_id = $(this).attr('data-id');
        $('#contracts_mark_as_not_release_am').modal('show');
    });

    $('#form_contracts_mark_as_not_release_am').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_not_release_am_id,
            'comment': $(this).find('#input_contracts_mark_as_not_release_am').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#contracts_mark_as_not_release_am_error');
        $.ajax({
            url: url_contract_mark_as_notrelease_am,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_not_release_am').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                    $('#not_release_am_contracts_table').DataTable().ajax.reload();
                    $('#contracts_mail_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var contract_pending_id;
    $('body').on('click', '.contract-mark-as-pending', function() {
        contract_pending_id = $(this).attr('data-id');
        $('#contracts_mark_as_pending').modal('show');
    });

    $('#form_contract_mark_as_pending').submit(function(event) {
        event.preventDefault();
        var $this = $(this);
        var data = {
            'id': contract_pending_id,
            'comment': $(this).find('#input_contracts_mark_as_pending').val(),
            'property_id': $('#selected_property_id').val(),
            '_token': _token
        }
        var msgElement = $('#contracts_mark_as_pending_error');
        $.ajax({
            url: url_contract_mark_as_pending,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $($this).find('button[type="submit"]').html('<i class="fa fa-refresh fa-spin"></i>');
                $($this).find('button[type="submit"]').prop('disabled', true);
            },
            success: function(result){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);

                if(result.status == true){
                    $('#contracts_mark_as_pending').modal('hide');
                    $($this)[0].reset();
                    $('#contracts_table').DataTable().ajax.reload();
                    $('#pending_contracts_table').DataTable().ajax.reload();
                    $('#contracts_mail_table').DataTable().ajax.reload();
                }else{
                    showFlash(msgElement, result.message, 'error');
                }
            },
            error: function(error){
                $($this).find('button[type="submit"]').html('Senden');
                $($this).find('button[type="submit"]').prop('disabled', false);
                showFlash(msgElement, 'Somthing want wrong!', 'error');
                console.log({error});
            }
        });
    });

    var contract_vacant_release_type;
    var contract_vacant_id;

    $('body').on('click', '.contract-release-request', function() {
        contract_vacant_release_type = $(this).attr('data-column');
        contract_vacant_id = $(this).attr('data-id');
        $('.contract-release-comment').val("")
        $('#contract-release-property-modal').modal();
    });

    $('body').on('click', '.contract-release-submit', function() {
        comment = $('.contract-release-comment').val();
        $.ajax({
            type: 'POST',
            url: url_property_contractreleaseprocedure,
            data: {
                id: contract_vacant_id,
                step: contract_vacant_release_type,
                _token: _token,
                comment: comment,
            },
            success: function(data) {
                $('#contracts_table').DataTable().ajax.reload();
                $('#contracts_mail_table').DataTable().ajax.reload();
            }
        });
    });


    /*--------------------Js FOR CONTRACT END--------------------------*/

    /*----------------------Property INSURANCE JS-------------------------------*/
	/*----------------------Property INSURANCE JS-------------------------------*/

	/*----------------------JS FOR Property Insurance comments - START -------------------------*/
	var insurance_detail_comment_url;
	var insurance_detail_comment_id;
	var type = '';
	var ins_table;

	$('body').on('click', '.btn-ins-comment', function() {
	    ins_table = $(this).closest('table').attr('id');
	    insurance_detail_comment_url = $(this).attr('data-url');
	    insurance_detail_comment_id  = $(this).attr('data-id');
	    type = 'property_insurance_tab_details';

	    var limit_text = $('#insurance_detail_comments_limit').text();
	    getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
	    // $('#insurance_detail_comment_modal').find('form').show();
	    $('#insurance_detail_comment_modal').modal('show');
	});

	$('#insurance_detail_comment_form').submit(function(event) {

	    event.preventDefault();
	    var $this = $(this);
	    var url = $(this).attr('action');
	    var comment = $($this).find('textarea[name="comment"]').val();
	    var data = {
	        'record_id': insurance_detail_comment_id,
	        'property_id': $('#selected_property_id').val(),
	        'comment': comment,
	        'type': type
	    };
	    
	    $.ajax({
	        url: url,
	        type: 'POST',
	        data: data,
	        dataType: 'json',
	        beforeSend: function() {
	            $($this).find('button[type="submit"]').prop('disabled', true);
	        },
	        success: function(result) {

	            $($this).find('button[type="submit"]').prop('disabled', false);
	            if (result.status == true) {
	                $($this)[0].reset();
	                $($this).find('textarea[name="comment"]').focus();

	                var limit_text = $('#insurance_detail_comments_limit').text();
	                getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);

	                if(type == 'property_insurance_tabs'){
	                    /*if($('#insurance-table-'+insurance_detail_comment_id).closest('.modal-body').find('.section_comment_div').find('.hidden').length == 1){
	                        $('#insurance-table-'+insurance_detail_comment_id).closest('.modal-body').find('.section_comment_div').find('.hidden').removeClass('hidden');
	                    }
	                    $('#insurance-table-'+insurance_detail_comment_id).closest('.modal-body').find('.show_cmnt_section').prepend('<p><span class="commented_user">'+result.data.name+'</span>: '+result.data.comment+' ('+result.data.created_at+')</p>');*/
	                    $('#insurance-table-'+insurance_detail_comment_id).closest('.main_card').find('.section_latest_comment').html('<p><span class="commented_user">'+result.data.name+'</span>: '+result.data.comment+' ('+result.data.created_at+')</p>');
	                }

	            } else {
	                alert(result.message);
	            }
	        },
	        error: function(error) {
	            $($this).find('button[type="submit"]').prop('disabled', false);
	            alert('Somthing want wrong!');
	        }
	    });

	});

	$('#insurance_detail_comment_modal').on('hidden.bs.modal', function () {
	    if(ins_table){
	        $('#'+ins_table).DataTable().ajax.reload();
	        ins_table = '';
	    }
	});

	$('body').on('click', '#insurance_detail_comments_limit', function() {
	    var text = $(this).text();
	    var limit = '';
	    if(text == 'Show More'){
	        limit = '';
	        $(this).text('Show Less');
	    }else{
	        limit = 1;
	        $(this).text('Show More');
	    }
	    getInsuranceDetailComments(insurance_detail_comment_url, limit, type);
	});

	$('body').on('click', '.remove-insurance-detail-comment', function() {
	    var url = $(this).attr('data-url');
	    var $this = $(this);
	    if(confirm('Are you sure to delete this comment?')){
	        $.ajax({
	            url: url,
	            type: 'GET',
	            dataType: 'json',
	            success: function(result) {
	                if(result.status){
	                    var limit_text = $('#insurance_detail_comments_limit').text();
	                    getInsuranceDetailComments(insurance_detail_comment_url, (limit_text == 'Show More') ? 1 : '', type);
	                }else{
	                    alert(result.message);
	                }
	            },
	            error: function(error) {
	                alert('Somthing want wrong!');
	            }
	        });
	    }else{
	        return false;
	    }
	});

	function getInsuranceDetailComments(url, limit = '', type=''){
	    var new_url = url+'/'+limit+'?type='+type;

	    $.ajax({
	        url: new_url,
	        type: 'GET',
	        dataType: 'json',
	        success: function(result) {

	            $('#insurance_detail_comments_table').find('tbody').empty();
	            if(result){
	                $.each(result, function(key, value) {
	                    var clear_url = url_delete_insurance_detail_comment.replace(":id", value.id);
	                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-insurance-detail-comment">Löschen</a></td>' : '';
	                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
	                    var commented_user = value.user_name+''+company;
	                    
	                    var tr = '\
	                    <tr>\
	                        <td>'+ commented_user +'</td>\
	                        <td>'+ value.comment +'</td>\
	                        <td>'+ value.created_at +'</td>\
	                        <td>'+delete_button+'\
	                    </tr>\
	                    ';

	                    $('#insurance_detail_comments_table').find('tbody').append(tr);
	                });
	            }
	        },
	        error: function(error) {
	            console.log({error});
	        }
	    });
	}
	/*----------------------JS FOR Property Insurance comments - END -------------------------*/

	/*----------------------JS FOR Property Insurance Section comments - START -------------------------*/
	$('body').on('click', '.btn-add-angebote-section-comment', function() {
	    var $this = $(this);
	    var url = $(this).attr('data-url');
	    var comment = $($this).closest('.row').find('.angebote-section-comment').val();
	    var record_id = $($this).closest('.row').attr('data-id');
	    var property_id = $($this).closest('.row').attr('data-pid');

	    var data = {
	        'record_id': record_id,
	        'property_id': property_id,
	        'comment': comment,
	        'type': 'property_insurance_tabs'
	    };

	    if(comment){
	      $.ajax({
	          url: url,
	          type: 'POST',
	          data: data,
	          dataType: 'json',
	          beforeSend: function() {
	              $($this).prop('disabled', true);
	          },
	          success: function(result) {

	              $($this).prop('disabled', false);

	              if (result.status == true) {

	                  $($this).closest('.row').find('.angebote-section-comment').val('');
	                  $($this).closest('.row').find('.angebote-section-comment').focus();

	                  if($($this).closest('.row').find('.hidden').length == 1){
	                    $($this).closest('.row').find('.hidden').removeClass('hidden');
	                  }
	                $($this).closest('.row').find('.show_cmnt_section').prepend('<span>'+result.data.name+': '+result.data.comment+'</span><br>');

	              } else {
	                  alert(result.message);
	              }
	          },
	          error: function(error) {
	              $($this).prop('disabled', false);
	              alert('Somthing want wrong!');
	          }
	      });
	    }else{
	      return false;
	    }
	});

	$('body').on('click', '.btn-show-angebote-section-comment', function() {
	  var url = $(this).attr('data-url');
	  var id = $(this).attr('data-id');

	  insurance_detail_comment_url = url;
	  insurance_detail_comment_id = id;

	  var limit_text = $('#insurance_detail_comments_limit').text();
	  type = 'property_insurance_tabs';
	  getInsuranceDetailComments(url, (limit_text == 'Show More') ? 1 : '', type);
	  // $('#insurance_detail_comment_modal').find('form').hide();
	  $('#insurance_detail_comment_modal').modal('show');
	});

	$('body').on('click', '.load_comment', function() {
	    var $this = $(this);
	    var url = $(this).attr('data-url');
	    var text = $(this).text();
	    var limit = '';

	    if(text == 'Show More'){
	        $($this).text('Show Less');
	        limit = '';
	    }else{
	        $($this).text('Show More');
	        limit = 2;
	    }

	    var new_url = url+'/'+limit+'?type=property_insurance_tab_details';

	    $.ajax({
	        url: new_url,
	        type: 'GET',
	        dataType: 'json',
	        success: function(result) {
	            $($this).closest('td').find('.show_cmnt').empty();
	            var html = '';
	            if(result){
	                $.each(result, function(key, value) {
	                	var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    	var commented_user = value.user_name+''+company;
	                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
	                });
	            }
	            $($this).closest('td').find('.show_cmnt').html(html);
	        },
	        error: function(error) {
	            console.log({error});
	        }
	    });
	});

	$('body').on('click', '.load_comment_section', function() {
	    var $this = $(this);
	    var url = $(this).attr('data-url');
	    var text = $(this).text();
	    var limit = '';

	    if(text == 'Show More'){
	        $($this).text('Show Less');
	        limit = '';
	    }else{
	        $($this).text('Show More');
	        limit = 2;
	    }

	    var new_url = url+'/'+limit+'?type=property_insurance_tabs';

	    $.ajax({
	        url: new_url,
	        type: 'GET',
	        dataType: 'json',
	        success: function(result) {
	            $($this).closest('div').find('.show_cmnt_section').empty();
	            var html = '';
	            if(result){
	                $.each(result, function(key, value) {
	                	var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                    	var commented_user = value.user_name+''+company;
	                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
	                });
	            }
	            $($this).closest('div').find('.show_cmnt_section').html(html);
	        },
	        error: function(error) {
	            console.log({error});
	        }
	    });
	});
	/*----------------------JS FOR Property Insurance Section comments - END -------------------------*/

	/*--------------------JS FOR PROPERTY COMMENTS- START----------------------------------*/
	var c_record_id = '';
	var c_property_id = '';
	var c_type = '';
	var c_table = '';


	$('body').on('click', '.btn-add-property-comment', function() {

	    var $this = $(this);

	    c_record_id = $($this).attr('data-record-id');
	    c_property_id = $($this).attr('data-property-id');
	    c_type = $($this).attr('data-type');

	    var comment = $($this).closest('.property-comment-section').find('.property-comment').val();
	    var reload = $(this).attr('data-reload');
	    var subject = $(this).attr('data-subject');
	    var content = $(this).attr('data-content');

	    var data = { 
	        'property_id': c_property_id,
	        'comment': comment,
	        'type': c_type,
	        'subject': subject,
	        'content': content,
	    };
	    if(c_record_id && c_record_id != ''){
	      data.record_id = c_record_id;
	    }

	    if(comment){
	      addPropertyComment($this, data, (reload == '1') ? true : false);
	    }
	});

	$('body').on('click', '.btn-show-property-comment', function() {
	  var $this = $(this);
	  var show_form = ($($this).attr('data-form') == '1') ? true : false;
	  var subject = $(this).attr('data-subject');
	  var content = $(this).attr('data-content');

	  c_record_id = $($this).attr('data-record-id');
	  c_property_id = $($this).attr('data-property-id');
	  c_type = $($this).attr('data-type');

	  if(c_type == 'property_contracts' || c_type == 'property_invoices'){
	    c_table = $($this).closest('table').attr('id');
	  }

	  if(show_form){
	      $('#property_comment_modal').find('.property-comment-section').show();
	      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-record-id', c_record_id);
	      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-property-id', c_property_id);
	      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-type', c_type);
	      
	      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-subject', subject);
	      $('#property_comment_modal').find('.btn-add-property-comment').attr('data-content', content);
	  }else{
	    $('#property_comment_modal').find('.property-comment-section').hide();
	  }

	  $('#property_comment_modal').modal('show');
	  getPropertyComment(c_record_id, c_property_id, c_type);
	});

	$('body').on('click', '#property_comment_limit', function() {
	    var text = $(this).text();
	    if(text == 'Show More'){
	        $(this).text('Show Less');
	    }else{
	        $(this).text('Show More');
	    }
	    getPropertyComment(c_record_id, c_property_id, c_type);
	});

	$('body').on('click', '.remove-property-comment', function() {
	    var url = $(this).attr('data-url');
	    var $this = $(this);
	    if(confirm('Are you sure to delete this comment?')){
	        $.ajax({
	            url: url,
	            type: 'GET',
	            dataType: 'json',
	            success: function(result) {
	                if(result.status){
	                    getPropertyComment(c_record_id, c_property_id, c_type);
	                }else{
	                    alert(result.message);
	                }
	            },
	            error: function(error) {
	                alert('Somthing want wrong!');
	            }
	        });
	    }else{
	        return false;
	    }
	});

	$('body').on('click', '.load_property_comment_section', function() {
	    var $this = $(this);
	    var url = $(this).attr('data-url');
	    var text = $(this).text();
	    var closest = $(this).attr('data-closest');
	    closest = (closest) ? closest : 'div';

	    var limit = '';

	    if(text == 'Show More'){
	        $($this).text('Show Less');
	        limit = '';
	    }else{
	        $($this).text('Show More');
	        limit = 2;
	    }

	    var new_url = url+'&limit='+limit;

	    $.ajax({
	        url: new_url,
	        type: 'GET',
	        dataType: 'json',
	        success: function(result) {
	            $($this).closest(closest).find('.show_property_cmnt_section').empty();
	            var html = '';
	            if(result){
	                $.each(result, function(key, value) {
	                	var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
						var commented_user = value.user_name+''+company;
	                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
	                });
	            }
	            $($this).closest(closest).find('.show_property_cmnt_section').html(html);
	        },
	        error: function(error) {
	            console.log({error});
	        }
	    });
	});

	$('#property_comment_modal').on('hidden.bs.modal', function () {
	    if(c_table){
	        $('#'+c_table).DataTable().ajax.reload();
	        c_table = '';
	    }
	});

	function addPropertyComment(btnobj, data, reload = false){
	  $.ajax({
	      url: url_add_property_comment,
	      type: 'POST',
	      data: data,
	      dataType: 'json',
	      beforeSend: function() {
	          $(btnobj).prop('disabled', true);
	      },
	      success: function(result) {

	          $(btnobj).prop('disabled', false);

	          if (result.status == true) {

	              $(btnobj).closest('.property-comment-section').find('.property-comment').val('');
	              $(btnobj).closest('.property-comment-section').find('.property-comment').focus();

	              if(reload){
	                  getPropertyComment(c_record_id, c_property_id, c_type);
	              }

	          } else {
	              alert(result.message);
	          }
	      },
	      error: function(error) {
	          $(btnobj).prop('disabled', false);
	          alert('Somthing want wrong!');
	      }
	  });
	}


	function getPropertyComment(record_id = '', property_id = '', type = ''){
	    var limit_text = $('#property_comment_limit').text();
	    var limit = (limit_text == 'Show More') ? 1 : '';

	    var new_url = url_get_property_comment+'?property_id='+property_id+'&record_id='+record_id+'&type='+type+'&limit='+limit;

	    $.ajax({
	        url: new_url,
	        type: 'GET',
	        dataType: 'json',
	        success: function(result) {

	            $('#property_comment_table').find('tbody').empty();
	            if(type == 'property_invoices'){
	                $('#property_comment_table').find('#th_name').hide();
	                $('#property_comment_table').find('#th_date').hide();
	            }else{
	                $('#property_comment_table').find('#th_name').show();
	                $('#property_comment_table').find('#th_date').show();
	            }

	            if(result){
	                $.each(result, function(key, value) {
	                    var clear_url = url_delete_property_comment.replace(":id", value.id);
	                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-property-comment">Löschen</a></td>' : '';
	                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
						var commented_user = value.user_name+''+company;
	                    
	                    if(type == 'property_invoices'){
	                      var tr = '\
	                        <tr>\
	                            <td>'+commented_user+' ('+value.date+') : '+ value.comment +'</td>\
	                            <td class="text-center">'+delete_button+'\
	                        </tr>\
	                      ';
	                    }else{
	                      var tr = '\
	                        <tr>\
	                            <td>'+ commented_user +'</td>\
	                            <td>'+ value.comment +'</td>\
	                            <td>'+ value.created_at +'</td>\
	                            <td>'+delete_button+'\
	                        </tr>\
	                      ';
	                    }

	                    $('#property_comment_table').find('tbody').append(tr);
	                });
	            }
	        },
	        error: function(error) {
	            console.log({error});
	        }
	    });
	}
	/*--------------------JS FOR PROPERTY COMMENTS- END----------------------------------*/

	$('body').on('click', '.invoice-release-request-am', function() {
        vacant_release_type = $(this).attr('data-column');
        vacant_id = $(this).attr('data-id');
        am_user = $(this).attr('data-user-id');
        $('.invoice-release-comment-am').val("")

        $('.invoice_asset_manager').val(am_user);
        if(am_user!="0" || vacant_release_type=="release1")
        {
            $('.am-list').addClass('hidden');
        }
        else{
            $('.am-list').removeClass('hidden');
        }
        $('#invoice-release-property-modal-am').modal();

    });
    $('body').on('click', '.invoice-release-submit-am', function() {
        comment = $('.invoice-release-comment-am').val();
        invoice_asset_manager = $('.invoice_asset_manager').val();
        if(invoice_asset_manager=="")
        {
            alert('Select AM');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: url_property_invoicereleaseprocedure,
            data: {
                id: vacant_id,
                invoice_asset_manager:invoice_asset_manager,
                step: vacant_release_type,
                _token: _token,
                comment: comment,
                property_id: $('#selected_property_id').val()
            },
            success: function(data) {
                // getprovisionbutton();
                $('.invoice_asset_manager').val("");
        
                $('#add_property_invoice_table4').DataTable().ajax.reload();
                $('#add_property_invoice_table').DataTable().ajax.reload();
                $('#add_property_invoice_table2').DataTable().ajax.reload();
                $('#invoice_mail_table').DataTable().ajax.reload();
            }
        });
    });
    
    getAngebotButton();

    function getAngebotButton()
    {
    	// var id = $('#selected_property_id').val();
    	var id = current_property_id;
    	
    	@if($user->role == 9)

		    $.ajax({
	            type : 'GET',
	            url : '{{ route('get_angebot_button') }}',
	            data : { id : id },
	            success : function (data) {
	                // generate_option_from_json(data, 'state_to_city');
	                $.each(data.btn, function(key, value) {
		                $('.ins-btn-' + key).html(value);
		            });
	            }
	        });

	    @endif
    }



    @if(in_array($user->role, [7,8]))

	    recommendedTableList();
		function recommendedTableList(){
			if($('#recommended-table-div-1').length){
			    $.ajax({
			        url: url_get_recommended_table,
			        type: 'GET',
			        dataType: 'json',
			        success: function(result) {
			            $('#recommended-table-div-1').html(result.table1);
			            $('#recommended-table-div-2').html(result.table2);
			            $('#recommended-table-div-3').html(result.table3);
			            $('#recommended-table-div-4').html(result.table4);
			            $('#recommended-table-div-5').html(result.table5);

			            $('#recommended-table-1').DataTable();
			            $('#recommended-table-2').DataTable();
			            $('#recommended-table-3').DataTable();
			            $('#recommended-table-4').DataTable();

			            var columns = [
			                null,
			                null,
			                null,
			                {"type": "new-date-time"},
			                null
			            ];
			            makeDatatable($('#recommended-table-5'), columns, 'desc', 3);

			            if($('.recommended_last_comment').length){
			              new showHideText('.recommended_last_comment', {
			                  charQty     : 30,
			                  ellipseText : "...",
			                  moreText    : "More",
			                  lessText    : " Less"
			              });
			            }
			        },
			        error: function(error) {
			            console.log({error});
			        }
			    });
			}
		}

		$('body').on('click', '.show_item_detail', function() {
	    
		    var title = $(this).attr('data-title');
		    var type = $(this).attr('data-type');

		    if(type == 'provision'){
		        var item_id = $(this).attr('data-id');
		    }else{
		        var item_id = $(this).closest('td').find('select').val();
		    }

		    $('#tenancy-detail-modal').find('.modal-title').text(title);
		    $('#tenancy-detail-modal').modal('show');

		    if(item_id){
		        var url = url_get_tenancy_item_detail+'?id='+item_id;
		        $.ajax({
		            url: url,
		            type: 'GET',
		            dataType: 'json',
		            success: function(result) {
		                if(result.status){
		                    var html = '<tr>\
		                                    <td>'+result.data.name+'</td>\
		                                    <td>'+result.data.date+'</td>\
		                                    <td>'+result.data.space+'</td>\
		                                    <td>'+result.data.files+'</td>\
		                                </tr>';
		                    $('#tenancy-detail-modal').find('tbody').html(html);
		                }else{
		                    alert(result.message);
		                }
		            },
		            error: function(error) {
		                alert('Somthing want wrong!');
		            }
		        });
		    }else{
		        $('#tenancy-detail-modal').find('tbody').empty();
		    }
		});

		$('body').on('click', '.kosten_umbau', function() {
		    var property_id = $(this).attr('data-property-id');
		    var tenant_id = $(this).attr('data-tenant-id');
		    loadKostenUmbau(property_id, tenant_id);
		    $('#kosten_umbau_modal').modal('show');
		});

		function loadKostenUmbau(property_id, tenant_id){
		    var url = url_get_kosten_umbau;
		    url = url.replace(':property_id', property_id);
		    url = url.replace(':tenant_id', tenant_id);
		    $.ajax({
		        type: 'GET',
		        url: url,
		        success: function(html) {
		            // $('#kosten_umbau_modal').find('.modal-body').html(html);
		            $('#form_kosten_umbau').html(html);
		        }
		    });
		}

		$('body').on('click', '#save_kosten_umbau', function() {
		    var data = $('#form_kosten_umbau').serialize();
		    var $this = $(this);
		    var property_id = $('#form_kosten_umbau').find("input[name='property_id']").val();
		    var tenant_id = $('#form_kosten_umbau').find("input[name='tenant_id']").val();

		    $.ajax({
		        url: url_saveractivity,
		        type: "post",
		        data: data,
		        success: function(response) {
		            loadKostenUmbau(property_id, tenant_id);
		        }
		    });
		});

		$('#kosten_umbau_modal').on('hidden.bs.modal', function () {
		    recommendedTableList();
		});

		$('body').on('click', '.add-row-rec', function() {
	        $(this).closest('tbody').find('.gesamt_row').before($('.add-row-clone').html());
	    });

	    $('body').on('click', '.remove-mieter', function() {
	        $(this).closest('tr').remove();
	    });

		$('body').on('click','.link-button-empfehlung',function(e) {
	        var property_id = $(this).data('property_id');
	        var empfehlung_id = $(this).data('empfehlung_id');
	        var tenant_id = $(this).data('tenant_id');
	        var fileObjId = $(this).attr('id');
	        var data = {
	            property_id: property_id,
	            empfehlung_id: empfehlung_id,
	            tenant_id: tenant_id,
	            fileObjId: fileObjId
	        };
	        $("#select-gdrive-file-model").modal('show');
	        $("#select-gdrive-file-type").val('empfehlung');
	        $("#select-gdrive-file-callback").val('selectempfehlungSubmit');
	        $("#select-gdrive-file-data").val(JSON.stringify(data));
	        // var workingDir = "{{$workingDir}}";
	        selectGdriveFileLoad(workingDir);
	    });

	    window.selectempfehlungSubmit = function(basename, dirname, curElement, dirOrFile) {
	        var dataObj = $("#select-gdrive-file-data").val();
	        dataObj = JSON.parse(dataObj);
	        var formdata = new FormData();
	        formdata.append('_token', _token);
	        formdata.append('property_id', dataObj.property_id);
	        formdata.append('empfehlung_id', dataObj.empfehlung_id);
	        formdata.append('basename', basename);
	        formdata.append('dirname', dirname);
	        formdata.append('dirOrFile', dirOrFile);
	        $('.preloader').show();
	        $.ajax({
	            type: 'POST',
	            url: url_property_saveEmpfehlungFile,
	            contentType: false,
	            processData: false,
	            data: formdata,
	        }).done(function(data) {
	            if (data.success) {
	                curElement.remove();
	                $('.uploaded-files-empfehlung.row-' + dataObj.empfehlung_id).append(data.element);
	                alert(data.msg);
	            } else {
	                alert(data.msg);
	            }
	            $('.preloader').hide();
	        });
	    }

	    $('body').on('click','.link-button-empfehlung2',function(e) {
	        var property_id = $(this).data('property_id');
	        var empfehlung_id = $(this).data('empfehlung_id');
	        var tenant_id = $(this).data('tenant_id');
	        var fileObjId = $(this).attr('id');
	        var data = {
	            property_id: property_id,
	            empfehlung_id: empfehlung_id,
	            tenant_id: tenant_id,
	            fileObjId: fileObjId
	        };
	        $("#select-gdrive-file-model").modal('show');
	        $("#select-gdrive-file-type").val('empfehlung2');
	        $("#select-gdrive-file-callback").val('selectempfehlungSubmit2');
	        $("#select-gdrive-file-data").val(JSON.stringify(data));
	        // var workingDir = "{{$workingDir}}";
	        selectGdriveFileLoad(workingDir);
	    });

	    window.selectempfehlungSubmit2 = function(basename, dirname, curElement, dirOrFile) {
	        var dataObj = $("#select-gdrive-file-data").val();
	        dataObj = JSON.parse(dataObj);
	        var formdata = new FormData();
	        formdata.append('_token', _token);
	        formdata.append('property_id', dataObj.property_id);
	        formdata.append('empfehlung_id', dataObj.empfehlung_id);
	        formdata.append('basename', basename);
	        formdata.append('dirname', dirname);
	        formdata.append('dirtype', 'empfehlung2');
	        formdata.append('dirOrFile', dirOrFile);
	        $('.preloader').show();
	        $.ajax({
	            type: 'POST',
	            url: url_property_saveEmpfehlungFile,
	            contentType: false,
	            processData: false,
	            data: formdata,
	        }).done(function(data) {
	            if (data.success) {
	                curElement.remove();
	                $('.uploaded-files-empfehlung2.rowt-' + dataObj.empfehlung_id).append(data.element);
	                alert(data.msg);
	            } else {
	                alert(data.msg);
	            }
	            $('.preloader').hide();
	        });
	    }

	    var vacant_release_type = vacant_id1 = "";
		$('body').on('click', '.vacant-release-request', function() {
		    vacant_release_type = $(this).attr('data-column');
		    vacant_id1 = $(this).attr('data-id');
		    $('.vacant_release_comment').val("");
		    $('#vacant-release-modal').modal('show');
		});

		$('body').on('click', '.vacant_release_submit', function() {
		    comment = $('.vacant_release_comment').val();
		    $.ajax({
		        type: 'POST',
		        url: url_property_vacantreleaseprocedure,
		        data: {
		            tenant_id: vacant_id1,
		            step: vacant_release_type,
		            _token: _token,
		            comment: comment,
		            property_id: $('#selected_property_id').val()
		        },
		        success: function(data) {
		            recommendedTableList();
		            $('#vacant-release-modal').modal('hide');
		        }
		    });
		});

		var vacant_url;
		$('body').on('click', '.btn-recommended-pending', function() {
		    
		    vacant_url = $(this).attr('data-url');

		    $('.vacant_pending_comment').val("");
		    $('#vacant-pending-modal').modal('show');
		});

		$('body').on('click', '.vacant_pending_submit', function() {
		    var comment = $('.vacant_pending_comment').val();
		    $.ajax({
		        type: 'POST',
		        url: vacant_url,
		        data: {
		            _token: _token,
		            comment: comment,
		            property_id: $('#selected_property_id').val()
		        },
		        success: function(data) {
		            recommendedTableList();
		            $('#vacant-pending-modal').modal('hide');
		        },
		        error: function(error) {
		            alert('Somthing want wrong!');
		        }
		    });
		});

		var vacant_notrelease_url;
		$('body').on('click', '.btn-recommended-not-release', function() {
		    
		    vacant_notrelease_url = $(this).attr('data-url');

		    $('.vacant_not_release_comment').val("");
		    $('#vacant-not-release-modal').modal('show');
		});

		$('body').on('click', '.vacant_not_release_submit', function() {
		    var comment = $('.vacant_not_release_comment').val();
		    $.ajax({
		        type: 'POST',
		        url: vacant_notrelease_url,
		        data: {
		            _token: _token,
		            comment: comment,
		            property_id: $('#selected_property_id').val()
		        },
		        success: function(data) {
		            recommendedTableList();
		            $('#vacant-not-release-modal').modal('hide');
		        },
		        error: function(error) {
		            alert('Somthing want wrong!');
		        }
		    });
		});

		$('body').on('click', '.btn-delete-vacant', function() {
		    $this = $(this);
		    var id = $(this).attr('data-id');
		    if (confirm('Are you sure want to delete?')) {
		        $.ajax({
		            url: url_delete_new_vacant,
		            type: "get",
		            data: {
		                id: id,
		            },
		            success: function(response) {
		                recommendedTableList();
		            }
		        });
		    }
		    return false;
		});

		$('body').on('change', '.change-comment-recommended', function() {

	        var $this = $(this);
	        var tr = $(this).closest('tr');
	        var pk = $(this).attr('data-column');

	        var data = {
	            property_id: $('#selected_property_id').val(),
	            pk: $(this).attr('data-column'),
	            value: $(this).val(),
	            _token: _token,
	            tenant_id: $(this).attr('data-id')
	        };
	        $.ajax({
	            url: url_changeempfehlungdetails,
	            type: "post",
	            data: data,
	            success: function(response) {
	                // if($this.attr('data-column')=="amount7")
	                // getprovisionbutton();

	                if(typeof tr !== 'undefined' && tr){
	                    $(tr).find('.kosten_umbau').text(response.data.kosten_umbau);
	                    if(pk != 'amount2'){
	                        $(tr).find('.laufzeit').val(response.data.laufzeit);
	                    }
	                    if(pk != 'amount5'){
	                        $(tr).find('.mietfrei').val(response.data.mietfrei);
	                    }
	                    $(tr).find('.jahrl_einnahmen').text(response.data.jahrl_einnahmen);
	                    $(tr).find('.Gesamteinnahmen').text(response.data.Gesamteinnahmen);
	                    $(tr).find('.Gesamteinnahmen_netto').text(response.data.Gesamteinnahmen_netto);
	                }
	            }
	        });
	    });

	    $('body').on('change', '.rental_space', function() {
	        setsize($(this).closest('tr'));
	    });

	    $('body').on('keyup', '.revenue_amount', function() {
	        setsize($(this).closest('tr'));
	    });

	    function setsize($tr){

	        var size        = $($tr).find('.rental_space option:selected').attr('data-size');
	        size = (typeof size !== 'undefined') ? makeNumber(size) : 0;
	        var einnahmen   = makeNumber($($tr).find('.revenue_amount').val());

	        var m = (size) ? (einnahmen / size) : 0;
	        $($tr).find('.m_size').text(makeNumberFormat(m, 2));
	    }

	    var recommended_id;
	    var recommended_comment_added = false;
		$('body').on('click', '.recommended-comment', function() {
		    recommended_id = $(this).attr('data-id');
		    var limit_text = $('#recommended_comments_limit').text();
		    getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');
		    $('#recommended_comment_modal').modal('show');
		});

		function getRecommendedCommentById(id, limit = ''){
		    var url = url_get_recommended_comment.replace(":id", id);
		    url = url+'/'+limit;

		    $.ajax({
		        url: url,
		        type: 'GET',
		        dataType: 'json',
		        success: function(result) {

		            $('#recommended_comments_table').find('tbody').empty();
		            if(result){
		                $.each(result, function(key, value) {
		                    var clear_url = url_delete_recommended_comment.replace(":id", value.id);
		                    var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-recommended-comment">Löschen</a></td>' : '';
		                    
		                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
		                    var commented_user = value.user_name+''+company;

		                    var tr = '\
		                    <tr>\
		                        <td>'+ commented_user +'</td>\
		                        <td>'+ value.comment +'</td>\
		                        <td>'+ value.created_at +'</td>\
		                        <td>'+delete_button+'\
		                    </tr>\
		                    ';

		                    $('#recommended_comments_table').find('tbody').append(tr);
		                });
		            }
		        },
		        error: function(error) {
		            console.log({error});
		        }
		    });
		}

		$('#recommended_comment_form').submit(function(event) {

		    event.preventDefault();
		    var $this = $(this);
		    var url = $(this).attr('action');
		    var comment = $($this).find('textarea[name="comment"]').val();
		    var data = {'tenant_id': recommended_id, 'comment': comment};

		    $.ajax({
		        url: url,
		        type: 'POST',
		        data: data,
		        dataType: 'json',
		        beforeSend: function() {
		            $($this).find('button[type="submit"]').prop('disabled', true);
		        },
		        success: function(result) {

		            $($this).find('button[type="submit"]').prop('disabled', false);
		            if (result.status == true) {
		                $($this)[0].reset();
		                $($this).find('textarea[name="comment"]').focus();
		                recommended_comment_added = true;
		                var limit_text = $('#recommended_comments_limit').text();
		                getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');

		            } else {
		                alert(result.message);
		            }
		        },
		        error: function(error) {
		            $($this).find('button[type="submit"]').prop('disabled', false);
		            alert('Somthing want wrong!');
		        }
		    });

		});

		$('body').on('click', '#recommended_comments_limit', function() {
		    var text = $(this).text();
		    var limit = '';
		    if(text == 'Show More'){
		        limit = '';
		        $(this).text('Show Less');
		    }else{
		        limit = 1;
		        $(this).text('Show More');
		    }
		    getRecommendedCommentById(recommended_id, limit);
		});

		$('body').on('click', '.remove-recommended-comment', function() {
		    var url = $(this).attr('data-url');
		    var $this = $(this);
		    if(confirm('Are you sure to delete this comment?')){
		        $.ajax({
		            url: url,
		            type: 'GET',
		            dataType: 'json',
		            success: function(result) {
		                if(result.status){
		                	recommended_comment_added = true;
		                    var limit_text = $('#recommended_comments_limit').text();
		                    getRecommendedCommentById(recommended_id, (limit_text == 'Show More') ? 1 : '');
		                }else{
		                    alert(result.message);
		                }
		            },
		            error: function(error) {
		                alert('Somthing want wrong!');
		            }
		        });
		    }else{
		        return false;
		    }
		});

		$('#recommended_comment_modal').on('hidden.bs.modal', function () {
			if(recommended_comment_added){
				recommended_comment_added = false;
				recommendedTableList();
			}
		});

		$('body').on('click', '.load_rec_comment_section', function() {
		    var $this = $(this);
		    var url = $(this).attr('data-url');
		    var text = $(this).text();
		    var limit = '';

		    if(text == 'Show More'){
		        $($this).text('Show Less');
		        limit = '';
		    }else{
		        $($this).text('Show More');
		        limit = 2;
		    }

		    var new_url = url+'/'+limit;

		    $.ajax({
		        url: new_url,
		        type: 'GET',
		        dataType: 'json',
		        success: function(result) {
		            $($this).closest('td').find('.show_rec_cmnt_section').empty();
		            var html = '';
		            if(result){
		                $.each(result, function(key, value) {
		                    var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
		                    var commented_user = value.user_name+''+company;
		                    html += '<p><span class="commented_user">'+commented_user+'</span>: '+value.comment+' ('+value.created_at+')</p>';
		                });
		            }
		            $($this).closest('td').find('.show_rec_cmnt_section').html(html);
		        },
		        error: function(error) {
		            console.log({error});
		        }
		    });
		});
	@endif

	/*---------------------NEW - JS FOR RELEASE INVOICE - START----------------*/

	var current_btn;

	$('body').on('click', '.btn-inv-release-am', function(){

	    var $this = $(this);
	    current_btn = $this;

	    var id = $(this).attr('data-id');
	    var status = $(this).attr('data-status');

	    var data = {'id': id, 'status': status, 'comment': ''};

	    $.ajax({
	        url: url_invoice_release_am,
	        type: 'POST',
	        data: data,
	        dataType: 'json',
	        success: function(result) {
	            if (result.status) {
	                loadButtonProcessTable();
	            }else{
	                alert(result.message);
	            }
	        },
	        error: function(error) {
	            alert('Somthing want wrong!');
	        }
	    });
	});

	$('body').on('click', '.btn-inv-release-hv', function(){

	    var $this = $(this);
	    current_btn = $this;

	    var id = $(this).attr('data-id');
	    var status = $(this).attr('data-status');

	    var data = {'id': id, 'status': status, 'comment': ''};

	    $.ajax({
	        url: url_invoice_release_hv,
	        type: 'POST',
	        data: data,
	        dataType: 'json',
	        success: function(result) {
	            if (result.status) {
	                loadButtonProcessTable();
	            }else{
	                alert(result.message);
	            }
	        },
	        error: function(error) {
	            alert('Somthing want wrong!');
	        }
	    });
	});

	var inv_usr_release_id;
	var inv_usr_release_status;

	$('body').on('click', '.btn-inv-release-usr', function(){
	    var $this = $(this);
	    current_btn = $this;
	    var modal = $('#invoice-release-usr-modal');

	    inv_usr_release_id = $(this).attr('data-id');
	    inv_usr_release_status = $(this).attr('data-status');
	    var title = $(this).attr('title');

	    if(inv_usr_release_status == '1'){
	        
	        $(modal).find('.modal-title').text(title);
	        $(modal).modal('show');
	        
	    }else{

	        var data = {'id': inv_usr_release_id, 'status': inv_usr_release_status, 'comment': ''};

	        $.ajax({
	            url: url_invoice_release_user,
	            type: 'POST',
	            data: data,
	            dataType: 'json',
	            success: function(result) {
	                if (result.status) {
	                    loadButtonProcessTable();
	                }else{
	                    alert(result.message);
	                }
	            },
	            error: function(error) {
	                alert('Somthing want wrong!');
	            }
	        });
	    }
	});

	$('#invoice-release-usr-form').on('submit', (function(e) {
	    e.preventDefault();

	    var $this = $(this);
	    var modal = $('#invoice-release-usr-modal');
	    var url = $($this).attr('action');
	    var comment = $($this).find('textarea[name="comment"]').val();
	    var user_id = $($this).find('select[name="user"]').val();

	    var data = {
	        'id': inv_usr_release_id, 
	        'status': inv_usr_release_status, 
	        'comment': '',
	        'user_id': user_id
	    };

	    $.ajax({
	        url: url,
	        type: 'POST',
	        data: data,
	        dataType: 'json',
	        beforeSend: function() {
	            $($this).find('button[type="submit"]').prop('disabled', true);
	        },
	        success: function(result) {

	            $($this).find('button[type="submit"]').prop('disabled', false);

	            if (result.status) {

	                $($this)[0].reset();
	                $('#invoice-release-usr-error').empty();
	                $(modal).modal('hide');

	                loadButtonProcessTable();

	            }else{
	              showFlash($('#invoice-release-usr-error'), result.message, 'error');
	            }
	        },
	        error: function(error) {
	            $($this).find('button[type="submit"]').prop('disabled', false);
	            showFlash($('#invoice-release-usr-error'), 'Somthing want wrong!', 'error');
	        }
	    });
	}));

	$('body').on('click', '.btn-inv-release-falk', function(){

	    var $this = $(this);
	    current_btn = $this;

	    var id = $(this).attr('data-id');
	    var status = $(this).attr('data-status');

	    var data = {'id': id, 'status': status, 'comment': ''};

	    $.ajax({
	        url: url_invoice_release_falk,
	        type: 'POST',
	        data: data,
	        dataType: 'json',
	        success: function(result) {
	            if (result.status) {
	                loadButtonProcessTable();
	            }else{
	                alert(result.message);
	            }
	        },
	        error: function(error) {
	            alert('Somthing want wrong!');
	        }
	    });
	});

	function loadButtonProcessTable(){
	    var table1 = $(current_btn).closest('table').attr('id');
	    var table2 = '';
	    var status = $(current_btn).attr('data-status');

	    if( $(current_btn).hasClass('btn-inv-release-am') ){

	        if(status == '1'){//request
	            table2 = 'add_property_invoice_table';
	        }else if(status == '2'){//release
	            table2 = 'add_property_invoice_table';
	        }else if(status == '3'){//pending
	            table2 = 'pending_am_invoice_table';
	        }else if(status == '4'){//not release
	            table2 = 'table_rejected_invoice';
	        }

	    }else if( $(current_btn).hasClass('btn-inv-release-hv') ){

	        if(status == '1'){//request
	            table2 = 'add_property_invoice_table';
	        }else if(status == '2'){//release
	            table2 = 'add_property_invoice_table';
	        }else if(status == '3'){//pending
	            table2 = 'pending_hv_invoice_table';
	        }else if(status == '4'){//not release
	            table2 = 'table_not_release_hv_invoice';
	        }

	    }else if( $(current_btn).hasClass('btn-inv-release-usr') ){

	        if(status == '1'){//request
	            table2 = 'add_property_invoice_table';
	        }else if(status == '2'){//release
	            table2 = 'add_property_invoice_table';
	        }else if(status == '3'){//pending
	            table2 = 'pending_user_invoice_table';
	        }else if(status == '4'){//not release
	            table2 = 'table_not_release_user_invoice';
	        }

	    }else if( $(current_btn).hasClass('btn-inv-release-falk') ){

	        if(status == '1'){//request
	            table2 = 'add_property_invoice_table';
	        }else if(status == '2'){//release
	            table2 = 'add_property_invoice_table2';
	        }else if(status == '3'){//pending
	            table2 = 'add_property_invoice_table4';
	        }else if(status == '4'){//not release
	            table2 = 'add_property_invoice_table3';
	        }
	    }

	    if(typeof table1 !== 'undefined' && table1){
	        $('#'+table1).DataTable().ajax.reload();
	    }
	    if(typeof table2 !== 'undefined' && table2 && table2 != table1){
	        $('#'+table2).DataTable().ajax.reload();
	    }
	    $('#invoice_mail_table').DataTable().ajax.reload();
	}
	
	/*---------------------NEW - JS FOR RELEASE INVOICE - END----------------*/
	
	</script>

@endsection
