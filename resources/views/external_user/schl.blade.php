<style type="text/css">
	#schl-table thead tr th,
	#schl-table tbody tr th,
	#schl-table tbody tr td{
		/*min-width: 100px !important;*/
		/*font-size: 12px;*/
	}
	#schl-table tbody tr td.no-border{
		border: 0px !important;
	}
</style>
<div class="white-box table-responsive">
	<table id="schl-table" class="table">
		<tbody>
		<!-- <tr>
			<th class="text-center border-bottom" colspan="9">
				<img src="{{ asset('img/house.png') }}" width="250px">
				<br/>
				Schleiz
			</th>
		</tr> -->
		<?php
		$wault = 0;
		$n_array = 	array('Baden-Württemberg'=>'5.0',
			           	'Bayern'=>'3.5',
			            'Berlin'=>'6.0',
			            'Brandenburg'=>'6.5',
			            'Bremen'=>'5.0',
			            'Hamburg'=>'4.5',
			            'Hessen'=>'6.0',
			            'Mecklenburg-Vorpommern'=>'6.0',
			            'Niedersachsen'=>'5.0',
			            'Nordrhein-Westfalen'=>'6.5',
			            'Rheinland-Pfalz'=>'5.0',
			            'Saarland'=>'6.5',
			            'Sachsen'=>'3.5',
			            'Sachsen-Anhalt'=>'5.0',
			            'Schleswig-Holstein'=>'6.5',
			            'Thüringen'=>'6.5');
		
			$amRes = DB::table('property_histories as ph')->selectRaw('ph.created_at, am.name')->join('users as am', 'am.id', '=', 'ph.new_value')->where('ph.field_type', 'asset_m_id')->where('property_id', $properties->id)->limit(3)->get();
			$am1 = $am2 = $am3 = '';
			$am1_date = $am2_date = $am3_date = '';
			if($amRes){
				if(isset($amRes[0])){
					$am1 = $amRes[0]->name;
					$am1_date = show_date_format($amRes[0]->created_at);
				}
				if(isset($amRes[1])){
					$am2 = $amRes[1]->name;
					$am2_date = show_date_format($amRes[1]->created_at);
				}
				if(isset($amRes[2])){
					$am3 = $amRes[2]->name;
					$am3_date = show_date_format($amRes[2]->created_at);
				}
			}
		?>
		<tr>
			<th>Strasse:</th>
			<td colspan="3">{{ $properties->strasse }},{{ $properties->hausnummer }}</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<!-- <a href="#" class="inline-edit" data-type="text" data-pk="asset_manager1" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->asset_manager1 }}</a>-->
				@if(isset($properties->asset_manager->name))
					<!-- {{$properties->asset_manager->name}} -->
				@endif
			</td>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<th>PLZ, Ort:</th>
			<td colspan="3">{{ $properties->plz_ort }} {{ $properties->ort }}
			</td>
		    <th>Asset Manager:</th>
			<td>
				@if($am1)
					{{ $am1 }}
				@elseif(isset($properties->asset_manager->name)) 
					{{ $properties->asset_manager->name }} 
				@endif
			</td>
			<td colspan="3">
				
			</td>
		</tr>
		<tr>
			<?php
        $sum_actual_net_rent = $sum_total_amount = 0;
                      $vvv= $wault = 0;

                      $pm_total = 0;
			$pa_total = 0;

			$pm_total1 = 0;
			$pa_total1 = 0;


                        ?>
		<?php
		$a = $b = $c = $d = array();
		$a1 = $b1 = $c1 = $d1= array();
		$selection_array = $selection_array1 = $selection_array_total = array();





		foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule){
			foreach($tenancy_schedule->items as $item){

				if($item->status && $item->use && $item->rental_space)
				{
					if(isset($selection_array[$item->use]))
						$selection_array[$item->use] += $item->rental_space; 
					else
						$selection_array[$item->use] = $item->rental_space; 

					if(isset($selection_array_total[$item->use]))
						$selection_array_total[$item->use] += $item->rental_space; 
					else
						$selection_array_total[$item->use] = $item->rental_space; 

				}
				if($item->status && $item->use && $item->vacancy_in_qm)
				{
					if(isset($selection_array1[$item->use]))
						$selection_array1[$item->use] += $item->vacancy_in_qm; 
					else
						$selection_array1[$item->use] = $item->vacancy_in_qm; 

					if(isset($selection_array_total[$item->use]))
						$selection_array_total[$item->use] += $item->vacancy_in_qm; 
					else
						$selection_array_total[$item->use] = $item->vacancy_in_qm; 
				}

				if($item->type == config('tenancy_schedule.item_type.business'))
				{

					if($item->rent_end > date('Y-m-d') && $item->rent_end && substr($item->rent_end,0,4)!="2099")
                    {
                            $sum_actual_net_rent += $item->actual_net_rent;
                            $sum_total_amount += $item->remaining_time_in_eur;
                    }

                    if($item->status)
                    {
						$a[$item->actual_net_rent] = $item->name;
						$b[$item->actual_net_rent] = $item->rental_space;
						$c[$item->actual_net_rent] = $item->rent_begin;
						$d[$item->actual_net_rent] = $item->rent_end;
                    }
				}

				$a1[$item->actual_net_rent] = $item->name;
				$b1[$item->actual_net_rent] = $item->rental_space;
				$c1[$item->actual_net_rent] = $item->rent_begin;
				$d1[$item->actual_net_rent] = $item->rent_end;

			}

			$vermi = 0;
			if( ( $tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10'] ) != 0 )
                $vermi =  ($tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $tenancy_schedule->calculations['total_business_vacancy_in_qm']) / ($tenancy_schedule->calculations['mi9'] + $tenancy_schedule->calculations['mi10']) * 100;


            $pm_total = $tenancy_schedule->calculations['total_actual_net_rent'];
            $pa_total = $tenancy_schedule->calculations['total_actual_net_rent']*12;

            $pm_total1 = $tenancy_schedule->calculations['potenzial_eur_monat'];
            $pa_total1 = $tenancy_schedule->calculations['potenzial_eur_jahr'];

            $vvv = number_format(100-$vermi,2).'%';
            
		}
		// print_r($selection_array);
		// print_r($b);
		// print_r($c);
		// print_r($d);
		
		krsort($a);
		krsort($b);
		krsort($c);
		krsort($d);
		$a  = array_values($a);
		$b  = array_values($b);
		$c  = array_values($c);
		$d  = array_values($d);
	
		// print_r($selection_array);
		?>
		<?php
				$kmkmkm = 0;
				$vermietet_total = 0;
				$leerstand_total =0;
				// print_r($selection_array_total);

				foreach ($selection_array_total as $key => $value) {
					if(in_array($key, array('Einzelhandel','Büro/Praxen','Wohnungen','Gastronomie','Lager','Stellplätze','Sonstiges')))
					$kmkmkm += $value;
				}

				foreach ($selection_array as $key => $value) {
					if(in_array($key, array('Einzelhandel','Büro/Praxen','Wohnungen','Gastronomie','Lager','Stellplätze','Sonstiges')))
					$vermietet_total += $value;
				}
				foreach ($selection_array1 as $key => $value) {
					if(in_array($key, array('Einzelhandel','Büro/Praxen','Wohnungen','Gastronomie','Lager','Stellplätze','Sonstiges')))
					$leerstand_total += $value;
				}
		?>
			<th>Gesamtfläche qm:</th>
			<?php
			// $j = 402;
			// if(isset($buy_data[$j]['comment']))
			// 	$properties->ubername_des_objekts2 = $buy_data[$j]['comment'];

			?>
			<td colspan="3">
				<!-- <a href="#" class="inline-edit" data-type="number" data-pk="gesamtflache_qm" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ number_format( $properties->gesamtflache_qm, 2 ,",",".") }}</a> -->
				{{number_format($kmkmkm, 2,",",".")}}
				{{--{{number_format($properties->rent_retail + $properties->vacancy_retail + $properties->rent_whg + $properties->vacancy_whg, 2,",",".")}}--}}
			</td>
			<th>Übernahme des Objekts:</th>
			<td>
				{{-- <a href="#" class="inline-edit" data-type="text" data-inputclass="mask-input-new-date" data-pk="ubername_des_objekts2" data-url="{{url('property/update/schl/'.$id) }}" data-title="DD.MM.JJJJ">
					{{show_date_format(str_replace('/','-',$properties->ubername_des_objekts2))}}
				</a> --}}
				@if($am1_date)
					{{ $am1_date }}
				@else
					{{show_date_format(str_replace('/','-',$properties->ubername_des_objekts2))}}
				@endif
			</td>
			 
			<th>Hausverwaltung:</th>
			<td>{{$properties->hausmaxx}}</td>
			<!-- <td colspan="2">
				{{-- <a href="#" class="inline-edit" data-type="text" data-pk="hausmaxx" data-url="{{url('property/update/schl/'.$id) }}" data-title="">{{ $properties->hausmaxx }} </a>--}}
 			<select name="hausmaxx" id="hausmaxx_options" >
				<option @if($properties->hausmaxx == 'Keine Angabe') selected @endif  value="Keine Angabe">Keine Angabe</option>
				<option @if($properties->hausmaxx == 'Hausmaxx') selected @endif value="Hausmaxx">Hausmaxx</option>
				<option @if($properties->hausmaxx == 'OK') selected @endif value="OK">O.K.</option>
				<option @if($properties->hausmaxx == 'Kähler REM') selected @endif value="Kähler REM">Kähler REM</option>
				<option @if($properties->hausmaxx == 'Paul Immobilien') selected @endif value="Paul Immobilien">Paul Immobilien</option>
				<option @if($properties->hausmaxx == 'Pasquini') selected @endif value="Pasquini">Pasquini</option>
				<option @if($properties->hausmaxx == 'Eigenverwaltung') selected @endif value="Eigenverwaltung">Eigenverwaltung</option>
				<option @if($properties->hausmaxx == 'Sesar') selected @endif value="Sesar">Sesar</option>	
			</select>
			</td> -->
			<!-- <td colspan="3" class="text-right"><a href="#" class="inline-edit" data-type="text" data-pk="hausmaxeuro_monat" data-url="{{url('property/update/schl/'.$id) }}" data-title="">@if(is_numeric($properties->hausmaxeuro_monat)){{ number_format( $properties->hausmaxeuro_monat, 2 ,",",".") }}@endif</a> €/Monat -->

			<!-- </td> -->

		<?php
		$cmarr = array('Kähler REM','Hausmaxx','Paul Immobilien','Pasquini','Sesar','OK');
		?>
		</tr>
		<tr>
			<td colspan="4"></td>
			<td colspan="2"><button type="button" class="btn btn-xs btn-primary" id="am-log" data-url="{{ route('get_am_log', ['property_id' => $properties->id]) }}">AM-Verlauf</button></td>
			<td>HV BU</td>
			<td>
			<select  class="change-column-name" data-column="hvbu_id" >
				<option>HV BU</option>
				@foreach($hvbu_users as $row_user)
				@if($properties->hvbu_id == $row_user->id || ($properties->hausmaxx &&  $row_user->company==$properties->hausmaxx) || ( ($properties->hausmaxx == 'Eigenverwaltung' || $properties->hausmaxx == 'Keine Angabe') && !in_array($row_user->company,$cmarr)))
				<option @if($properties->hvbu_id == $row_user->id) selected @endif  value="{{$row_user->id}}">{{$row_user->name}}</option>
				@endif
				@endforeach				
			</select>
			</td>
		</tr>
		<tr>
			<td colspan="6"></td>
		
			<td>HV PM</td>
			<td>
			<select  class="change-column-name" data-column="hvpm_id" >
				<option>HV PM</option>
				@foreach($hvpm_users as $row_user)
				@if($properties->hvpm_id == $row_user->id || ($properties->hausmaxx &&  $row_user->company==$properties->hausmaxx) || ( ($properties->hausmaxx == 'Eigenverwaltung' || $properties->hausmaxx == 'Keine Angabe') && !in_array($row_user->company,$cmarr)))
				<option @if($properties->hvpm_id == $row_user->id) selected @endif  value="{{$row_user->id}}">{{$row_user->name}}</option>
				@endif
				@endforeach				
			</select>
			</td>
		</tr>
		</tbody>
	</table>
</div>


 
