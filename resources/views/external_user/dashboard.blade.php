<?php
   $deactiveUser = [];
   $deactiveUserRes = DB::table('users')->select('name')->where('user_status', 0)->get();
   if($deactiveUserRes){
      foreach ($deactiveUserRes as $key => $value) {
         $deactiveUser[] = trim($value->name);
      }
   }
   $user = Auth::user();
?>
@extends('layouts.admin')

@section('css')
   <link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
   <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
   <style type="text/css">
      .text-red,
      .text-red a{
         color:red;
      }
      .loading {
        height: 0;
        width: 0;
        padding: 15px;
        border: 6px solid #ccc;
        border-right-color: #888;
        border-radius: 22px;
        -webkit-animation: rotate 1s infinite linear;
        /* left, top and position just for the demo! */
        position: absolute;
        left: 50%;
        top: 50%;
      }
      @-webkit-keyframes rotate {
        /* 100% keyframe for  clockwise.
           use 0% instead for anticlockwise */
        100% {
          -webkit-transform: rotate(360deg);
        }
      }
      #added-month-asset tbody th:first-child{
         cursor: pointer;
      }
      .insurance_tab_div table .btn{
         display: none;
      }
      @media (min-width: 992px)
      {    .modal-lg {
         width: 1320px;
      }
      }
      #vlog-table2 .fa-check,
      #vlog-table .fa-check{
         color: green;
      }
      #vlog-table2 .fa-times,
      #vlog-table .fa-times{
         color: red;
      }
      @media (min-width: 768px)
      {
         .modal-dialog.custom {
            width: 848px;
            margin: 30px auto;
         }
      }
      .list-data-banks a{
         pointer-events: none;
         color: #797979 !important;
      }
      .bank-file-upload input{
         display: none !important;
      }
      .bank-file-upload a{
         pointer-events:auto;
         color: #797979 !important;
      }
      .list-data-banks .btn{
         display: none;
      }
      ._51mz{
         /*display: none;*/
      }
      .card {
         position: relative;
         /*display: flex;*/
         flex-direction: column;
         min-width: 0;
         word-wrap: break-word;
         background-color: #fff;
         background-clip: border-box;
         border: 0 solid transparent;
         border-radius: 0;
         height: 115px !important;
      }
      .card-body {
         flex: 1 1 auto;
         padding: 8px 12px;
      }
      .card .card-title {
         position: relative;
         font-weight: 500;
         font-size: 16px;
      }
      .d-flex {
         display: flex!important;
      }
      .align-items-center {
         align-items: center!important;
      }
      .ml-auto, .mx-auto {
         margin-left: auto!important;
      }
      .custum-row .col-lg-3, .custum-row .col-md-3{
         padding: 0 5px;
      }
      .custum-row-2{
         margin-top: 25px;
      }
      .custum-row-2 .col-md-6{
         margin: 5px 0;
         padding: 0 5px;
      }
      .preloader{
         display: none;
      }
      .pb-3, .py-3{
         padding-bottom: 20px;
         margin-top: 10px;
      }
      .for-col-padding .col-md-6{
         padding: 0 5px;
      }
      .pointer-cursor{
         cursor: pointer;
      }
      .get-assetmanager-property,.get-manager-property{
         cursor: pointer;
      }
      .border-top-footer{
         border-top: 3px solid #666 !important;
      }
      .div-user-count{
         font-size: 16px !important;
         color: #313131;
         font-weight: 300 !important;
      }
      .get-verkauf-user,
      .get-vermietung-user{
         cursor: pointer;
         color: #23527c;
      }
      .col-md-3 .progress{
         display: none !important;
      }
      @media only screen and (min-width: 768px) {
         .dashboard-card-button .col-md-3{
           width: 20% !important;
         }
      }
      .blue-bg{
        background-color: #004f91;
      }
      .blue-bg h2,
      .blue-bg h5{
        color: white;
      }
      .dashboard-card-button .col-md-3{
         margin-bottom: 12px;
      }
      .yellow-bg {
         background-color: #ffff99;
      }
      .modal-body table tr td,
      .modal-body table tr th{
         font-size: 14px !important;
      }
      .modal-body{
         font-family: arial, sans-serif;
      }
   </style>
@endsection

@section('content')

@include('partials.flash')

<div class="page-content container-fluid">
   <div class="row custum-row dashboard-card-button">

      {{-- <div class="col-md-3 load-table" id="invoice" data-table="table-property-invoice-am" data-title="Meine offenen Rechnungsfreigaben" data-url="{{ route('getPropertyInvoiceRequest') }}">
        <div class="card pointer-cursor" >
           <div class="card-body">
              <h5 class="card-title text-uppercase">Offene Rechnungen HV</h5>
              <div class="text-right">
                 <h2 class="mt-2 display-7 invoice-approval-total"></h2>
              </div>
              <br>
           </div>
        </div>
      </div> --}}

      {{-- <div class="col-md-3" id="card-release-invoice" data-url="{{ route('release_invoice') }}">
         <div class="card pointer-cursor">
            <div class="card-body">
               <h5 class="card-title text-uppercase">Freigegebene Rechnungen</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 release-invoice-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div> --}}

      <div class="col-md-3 load-table" id="open_invoice" data-table="invoice-table-0" data-title="Offene Rechnungen" data-url="{{ route('get_invoice_for_hv', ['status' => 0]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">Offene Rechnungen</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 open_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="request_invoice" data-table="invoice-table-1" data-title="FREIZUGEBENDE RECHNUNGEN" data-url="{{ route('get_invoice_for_hv', ['status' => 1]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">FREIZUGEBENDE RECHNUNGEN</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 request_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="release_invoice" data-table="invoice-table-2" data-title="FREIGEGEBENE RECHNUNGEN" data-url="{{ route('get_invoice_for_hv', ['status' => 2]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">FREIGEGEBENE RECHNUNGEN</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 release_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="pending_invoice" data-table="invoice-table-3" data-title="PENDING RECHNUNGEN" data-url="{{ route('get_invoice_for_hv', ['status' => 3]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">PENDING RECHNUNGEN</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 pending_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="not_release_invoice" data-table="invoice-table-4" data-title="NICHT FREIGEGEBEN RECHNUNGEN" data-url="{{ route('get_invoice_for_hv', ['status' => 4]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">NICHT FREIGEGEBEN RECHNUNGEN</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 not_release_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3" id="released-angebote" data-table="table-released-angebote" data-title="Freigegebene Angebote" data-url="{{ route('getReleasedAngebote') }}">
        <div class="card pointer-cursor" >
           <div class="card-body">
              <h5 class="card-title text-uppercase">Freigegebene Angebote</h5>
              <div class="text-right">
                 <h2 class="mt-2 display-7 released-angebote-total"></h2>
              </div>
              <br>
           </div>
        </div>
      </div>

   </div>
</div>

 @include('external_user.dashboard_modal')

@endsection

@section('js')
   <script type="text/javascript">
      var _token                                = '{{ csrf_token() }}';
      var in_active_user                        = '{{ implode(",", $deactiveUser) }}';
      var in_active_user_arr                    = (in_active_user != '') ? in_active_user.split(",") : [];
      var url_property_invoicereleaseprocedure  = '{{ url('property/invoicereleaseprocedure') }}';
      var url_delete_property_comment           = '{{ route('delete_property_comment', ['id' => ':id']) }}';
      var url_add_property_comment              = '{{ route('add_property_comment') }}';
      var url_get_property_comment              = '{{ route('get_property_comment') }}';
      var url_property_invoicemarkpendingam     = '{{ url('property/invoicemarkpendingam') }}';
      var url_get_property_insurance_detail     = '{{ route("get_property_insurance_detail", ["tab_id" => ":tab_id"]) }}';
      var url_delete_insurance_detail_comment   = '{{ route('delete_insurance_detail_comment', ['id' => ':id']) }}';
      var url_makeAsPaid                        = '{{ route('makeAsPaid') }}';
      var url_delete_property_invoice           = '{{ route("delete_property_invoice", ":id") }}';

      var url_invoice_release_am                = '{{ route('invoice_release_am') }}';
      var url_invoice_release_hv                = '{{ route('invoice_release_hv') }}';
      var url_invoice_release_user              = '{{ route('invoice_release_user') }}';
      var url_invoice_release_falk              = '{{ route('invoice_release_falk') }}';
   </script>

   <script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
   <script src="{{asset('js/custom-datatable.js')}}"></script>
   <script src="{{asset('js/hv-dashboard.js')}}"></script>
@endsection
