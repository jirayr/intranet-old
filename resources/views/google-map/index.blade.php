@extends('layouts.admin') 

@section('css')
    <!-- Styles -->
@endsection

@section('content')
	<div class="white-box">
	    <div id="map" style="height: 500px;"></div>
    </div>
@endsection

@section('js')
    <!-- Js -->
    <script type="text/javascript">

    	document.addEventListener('DOMContentLoaded', function () {
	        if (document.querySelectorAll('#map').length > 0)
	        {
	            if (document.querySelector('html').lang)
	                lang = document.querySelector('html').lang;
	            else
	                lang = 'en';

	            var js_file = document.createElement('script');
	            js_file.type = 'text/javascript';
	            js_file.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDeWADTt3gAkkKPyafQZmy__UmjUxLutXw&callback=initMap&signed_in=true&language=' + lang;
	            document.getElementsByTagName('head')[0].appendChild(js_file);
	        }
	    });

	    var map;
	    var geocoder;

	    function initMap()
	    {	
	    	geocoder = new google.maps.Geocoder();
	        map = new google.maps.Map(document.getElementById('map'), {
	            center: {lat: -34.397, lng: 150.644},
	            zoom: 8
	        });

	        var locators = [];
	        var properties = <?php echo json_encode($properties); ?>;
	        if( properties.length > 0 ) {
		        for ( var i in properties ) {
			        locators.push({
						'title': properties[i].name_of_property,
						'author': properties[i].name_of_creator,
						'city_place': properties[i].city_place,
						'address' : properties[i].address
			        });
			    }
			}

			
			plotMarkers(locators);
	    }

	    var markers;
	    var bounds;
	    var markerBounds = [];

	    function plotMarkers(m)
	    {
	        markers = [];
	        bounds = new google.maps.LatLngBounds();
	        var total_addresses = m.length;            
        	var address_count = 0;
	        m.forEach(function (marker) {
	        	address_count++;  

	            var title = marker.title;

	            var infowindow = new google.maps.InfoWindow({
					content: '<h3>'+ marker.title +'</h3><p>{{__("map.author")}}: '+ marker.author +'<br/>{{__("map.city_place")}}: '+ marker.city_place +'<br/>{{__("map.address")}}: ' + marker.address + '</p>'
				});
	            geocoder.geocode( { 'address': marker.address + " " + marker.city_place}, function(results, status) {
	            	if (status == google.maps.GeocoderStatus.OK) {
	            		var position = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
	            		markers.push(
			               new google.maps.Marker({
			                    position: position,
			                    map: map,
			                    animation: google.maps.Animation.DROP,
			                    title: title
			                }).addListener('click', function() {
								infowindow.open(map, this);
							})
			            );
			            // bounds.extend(position);
			            addressesBounds(total_addresses, address_count, position);
	            	}
            	});

	        });
	        console.log(bounds);
	        map.fitBounds(bounds);
	    }

	    function addressesBounds(total_addresses, address_count, myLatLng) {

			markerBounds.push(myLatLng);

			// make sure you only run this function when all addresses have been geocoded
			if (total_addresses == address_count) {
				var latlngbounds = new google.maps.LatLngBounds();
				for ( var i = 0; i < markerBounds.length; i++ ) {
					latlngbounds.extend(markerBounds[i]);
				}
				map.setCenter(latlngbounds.getCenter());
				map.fitBounds(latlngbounds);
			}
		}
    </script>
@endsection
