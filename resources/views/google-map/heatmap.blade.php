@extends('layouts.admin') 

@section('css')
    <!-- Styles -->
    
    <link href="https://fcrimmo.com/plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">
 
@endsection

@section('content')
	<div class="white-box">
@php
 
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
date_default_timezone_set("Asia/Bangkok");
 
@endphp
 	 
 		@if(app('request')->input('zip'))
        @php 
        $zip = app('request')->input('zip'); 
        @endphp
        @endif

        @if(app('request')->input('type_id'))
        @php 
        $type_id = app('request')->input('type_id');
        @endphp
        @endif
        
        @if(app('request')->input('kind'))
        @php
        $kind = app('request')->input('kind');
        @endphp
        @endif

<div class="row">
             <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading"> Heatmap Suche</div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                           
                            <form method="GET" class="form-horizontal">
	                                <div class="form-body">
	                                    <div class="row">
	                                        <div class="col-md-6">
	                                            <div class="form-group">
	                                                <label class="control-label col-md-3">Ort/Postleitzahl</label>
	                                                <div class="col-md-9">
	                                                    <input class="form-control" id="search-box" name="zip" type="text" value="@if(app('request')->input('zip')){{ app('request')->input('zip') }}@endif">
	                                                     <div id="suggesstion-box"></div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                </div>
	                                <div class="form-actions">
	                                    <div class="row">
	                                        <div class="col-md-6">
	                                            <div class="row">
	                                                <div class="col-md-offset-3 col-md-9">
	                                                    <input class="btn btn-success" type="submit" value="Suchen">
	                                                    <br>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--  -->
         </div>

        <div class="fl fw">
            <h2>
               @if(app('request')->input('kind'))
                   @php
                    if ($kind == "1") {
                        echo 'Alle Inserate zum Verkauf'; 
                    } elseif ($kind == "2") {
                        echo 'Alle Inserate zur Miete'; 
               		 } else {
                   echo 'Inserat zur Miete & Kauf'; 
                	}
                    @endphp 
                @endif
            </h2>
        </div>
        
        
    
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">HeatMap</h3>
                    <div id="map" class="gmaps" style="height: 500px;"></div>
                </div>
            </div>
        </div> 
        
        @if(app('request')->input('zip'))
     
            @php
 
                if (is_numeric($zip)) {

                    $sql_statement = DB::table('zipcodes')->where('zipcode_value',$zip)->get();
                    $sql =  json_decode( $sql_statement, true );
                     
                    //$sql = "  SELECT * FROM zipcodes WHERE  zipcode_value = \"$zip\"";
                    $zoom = "13";
                } else {

                    $sql_statement = DB::table('zipcodes')->where('zipcode_place',$zip)->get();
                    $sql =  json_decode( $sql_statement, true );
 
                    //$sql = "  SELECT * FROM zipcodes WHERE  zipcode_place = \"$zip\"";
                    $zoom = "12";
                }
               $zipcode = $sql[0];

                //$result = sqlQuery($sql);
                //$zipcode = $result->fetch(PDO::FETCH_ASSOC);
            
            @endphp
            
            @else
            
            @php
                $lat_lng = (isset($_COOKIE['lat_lng'])) ? $_COOKIE['lat_lng'] : '0,0';
                $lat_lng = str_replace("(", "", $lat_lng);
                $lat_lng = str_replace(")", "", $lat_lng);
                $lat_lng = str_replace(" ", "", $lat_lng);

                $lat_lng = explode(",", $lat_lng);

                $lat = $lat_lng[0];
                $lng = $lat_lng[1];

                if(empty($lng)){
                    $zipcode['zipcode_latitude'] = "50.90992";
                    $zipcode['zipcode_longitude'] = "10.05029";
                }else{
                    $zipcode['zipcode_latitude'] = $lat;
                    $zipcode['zipcode_longitude'] = $lng;
                }
                
                if(empty($zipcode['zipcode_longitude'])){
                $zipcode['zipcode_latitude'] = "50.90992";
                $zipcode['zipcode_longitude'] = "10.05029";
                }
                $zoom = (isset($_COOKIE['zoom'])) ? $_COOKIE['zoom'] : 12;
                if(empty($zoom)){
                    $zoom = "12";
                }
            @endphp
        @endif
 

    </div>
@endsection

@section('js')
    <!-- Js -->
  
  <script src="https://fcrimmo.com/plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.min.js"></script> 
  <script src="https://fcrimmo.com/plugins/bower_components/vectormap/jquery-jvectormap-world-mill-en.js"></script>  
  <script src="https://fcrimmo.com/plugins/bower_components/vectormap/jquery-jvectormap-in-mill.js"></script>  
  <script src="https://fcrimmo.com/plugins/bower_components/vectormap/jquery-jvectormap-us-aea-en.js"></script>  
  
       <script>
         
            var map;
            var heatmap = null;
            var heatmapData;
            function initMap() {
            
                 map = new google.maps.Map(document.getElementById('map'), {
                    zoom: <?php echo $zoom; ?>,
                    center: new google.maps.LatLng(<?php echo $zipcode['zipcode_latitude']; ?>, <?php echo $zipcode['zipcode_longitude']; ?>),
                    
                });

                // This example uses a local copy of the GeoJSON stored at

                function xmlLoader(bounds) {
                    var script = document.createElement('script');
                    if (bounds === 0) {
                        script.src = "https://fcrimmo.com/heatmarkers.php?type_id=@if(app('request')->input('type_id')){{ $type_id }}@endif&kind=@if(app('request')->input('kind')){{ $kind }}@endif";
                         
                    } else {
                        script.src = "https://fcrimmo.com/heatmarkers.php?type_id=@if(app('request')->input('type_id')){{ $type_id }}@endif&kind=@if(app('request')->input('kind')){{ $kind }}@endif&bounds=" + bounds;
                        console.log('Bounds',bounds);
                    }

                    document.getElementsByTagName('head')[0].appendChild(script);
                    
                    // Set_Cookie('lat_lng', '' + map.getCenter(), '', '/', '', '');
                    // Set_Cookie('zoom', '' + map.getZoom(), '', '/', '', '');
                }
                
                //xmlLoader(0);
                
                google.maps.event.addListenerOnce(map, 'idle', function () {
                    setTimeout(function () {
                        xmlLoader(map.getBounds() + '&zoom=' + map.getZoom());
                    }, 30);
                    console.log( 'Zoom Level',map.getZoom());
                });

                map.addListener('zoom_changed', function() {
                    setTimeout(function () {
                        xmlLoader(map.getBounds() + '&zoom=' + map.getZoom());
                    }, 30);
                });
                map.addListener('dragend', function() {
                    setTimeout(function () {
                        xmlLoader(map.getBounds() + '&zoom=' + map.getZoom());
                    }, 30);
                });
            }

            function eqfeed_callback(results) {
                if( typeof( heatmapData ) == 'object' ) {
                    heatmapData.clear();
                }
                heatmapData = new google.maps.MVCArray();
                //alert (JSON.stringify(heatmapData));
                for (var i = 0; i < results.features.length; i++) {
                    var coords = results.features[i].geometry.coordinates;
                    var latLng = new google.maps.LatLng(coords[1], coords[0]);
                    var magnitude = results.features[i].properties.mag;
                    var weightedLoc = {
                        location: latLng,
                        weight: magnitude
                    };
                    heatmapData.push(weightedLoc);
                    // console.log(results.features);
                    // console.log(weightedLoc);
                    // console.log(results);
                }

                console.log('Heat Map Data', heatmapData);

                
                
                //heatmap.setMap(null);
                heatmap = new google.maps.visualization.HeatmapLayer({
                        data: heatmapData,
                        dissipating: map.getZoom() > 8 ? false : true,
                        map: map
                    });
                    heatmap.set('radius', heatmap.get('radius') ? null : 40);
                    heatmap.set('opacity', heatmap.get('opacity') ? null : 0.1);
                //alert (JSON.stringify(markers));
            }


        </script>

         
          <script async  defer  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBD__3FLI3K5P6c245SCKy-TTPFM6vu2W8&libraries=visualization&callback=initMap">
          </script> 

<script>
    // AJAX call for autocomplete 
    $(document).ready(function(){
    $("#search-box").keyup(function(){
        $.ajax({
        type: "POST",
        url: "readZipcode.php",
        data:'keyword='+$(this).val(),
        beforeSend: function(){
            $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat right");
        },
        success: function(data){
            $("#suggesstion-box").show();
            $("#suggesstion-box").html(data);
            $("#search-box").css("background","#FFF");
        }
        });
    });
});
//To select country name
function selectCountry(val) {
$("#search-box").val(val);
$("#suggesstion-box").hide();
}
    </script>

    <div id="goog-gt-tt" class="skiptranslate" dir="ltr"><div style="padding: 8px;"><div><div class="logo"><img src="https://www.gstatic.com/images/branding/product/1x/translate_24dp.png" width="20" height="20" alt="Google Translate"></div></div></div><div class="top" style="padding: 8px; float: left; width: 100%;"><h1 class="title gray">Original text</h1></div><div class="middle" style="padding: 8px;"><div class="original-text"></div></div><div class="bottom" style="padding: 8px;"><div class="activity-links"><span class="activity-link">Contribute a better translation</span><span class="activity-link"></span></div><div class="started-activity-container"><hr style="color: #CCC; background-color: #CCC; height: 1px; border: none;"><div class="activity-root"></div></div></div><div class="status-message" style="display: none;"></div></div>

    <div class="jvectormap-tip"></div>

    <div class="goog-te-spinner-pos"><div class="goog-te-spinner-animation"><svg xmlns="http://www.w3.org/2000/svg" class="goog-te-spinner" width="96px" height="96px" viewBox="0 0 66 66"><circle class="goog-te-spinner-path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg></div></div>
@endsection
