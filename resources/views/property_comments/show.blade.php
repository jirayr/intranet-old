@extends('layouts.admin') 

@section ('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" />
@stop


@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">{{__('Aktionsplanung')}} </div>
                <div style="text-align:center">
                    <label style="margin-right: 20px">{{__('property_comments.select_object')}}</label>
                    <select style="width:25%;padding:5px" id="change_property_select">
                        @foreach ($masterlistes as $masterliste)
							@if($property_id == -1)
								 <option selected disabled value=""></option>
							 @endif
                            @if($masterliste->id == $property_id)
                                <option selected value="{{$masterliste->id}}">{{$masterliste->id}} / {{$masterliste->object}} / {{$masterliste->asset_manager_name}}</option>
                            @else
                                <option value="{{$masterliste->id}}">{{$masterliste->id}} / {{$masterliste->object}} / {{$masterliste->asset_manager_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="white-box">
                    <h3 class="box-title">{{__('property_comments.comments')}}</h3>
                    <div class="comment-center p-t-10">
                        @foreach ($comments as $comment)
                            @if($comment->status!='PENDING' || Auth::user()->role==1 || Auth::user()->id==$comment->user->id)
                            <div class="comment-body">
                                <div class="user-img"> <img src="{{(config('upload.avatar_path').$comment->user->image)}}" alt="user" class="img-circle"></div>
                                <div class="mail-contnet">
                                    <h5>{{$comment->user->name}}</h5>
									{{--<span class="time">{{$comment->created_at}}</span> --}}
									
									     <a data-name="created_at"
											   class="edit-date"
											   data-type="combodate"
											   data-pk="{{$comment->id}}"
											   data-url="{{route('update.comment.date')}}"
											   data-format="YYYY-MM-DD"
											   data-viewformat="DD/MM/YYYY"
											   data-url="{{route('update.comment.date')}}"
											   data-value="{{$comment->created_at}}">
											
											</a>
											
                                    @if($comment->status=='PENDING')
                                    <span class="label label-rouded label-info">{{$comment->status}}</span>
                                    @endif
                                    @if($comment->status=='APPROVED')
                                        <span class="label label-rouded label-success">{{$comment->status}}</span>
                                    @endif
                                    @if($comment->status=='REJECTED')
                                        <span class="label label-rouded label-danger">{{$comment->status}}</span>
                                    @endif
                                    <br/><span class="mail-desc">{{$comment->comment}}</span>

                                    @if($comment->status=='PENDING' &&  Auth::user()->role==1)
                                    <a href="{{route('edit_propertycomment', ['id' => $comment->id, 'value' => 'APPROVED'])}}" class="btn btn btn-rounded btn-default btn-outline m-r-5" name="approve-comment"><i class="ti-check text-success m-r-5"></i>{{__('property_comments.confirm')}}</a>
                                    <a href="{{route('edit_propertycomment', ['id' => $comment->id, 'value' => 'REJECTED'])}}" class="btn-rounded btn btn-default btn-outline"><i class="ti-close text-danger m-r-5"></i>{{__('property_comments.decline')}}</a>
                                    @endif
                                    @if(Auth::user()->role==1 || Auth::user()->id==$comment->user->id)
                                        <a data-toggle="modal" data-target="#myModal" data-value="<?php echo $comment->id ?>" data-content="<?php echo $comment->comment; ?>" class="btn-rounded btn btn-default btn-outline edit_comment"><i class="ti-check text-success m-r-5"></i>{{__('property_comments.edit')}}</a>
                                    @endif
                                    @if(Auth::user()->role==1 || Auth::user()->id==$comment->user->id)
                                    <a href="{{route('edit_propertycomment', ['id' => $comment->id, 'value' => 'DELETE'])}}" class="btn-rounded btn btn-default btn-outline"><i class="ti-close text-danger m-r-5"></i>{{__('property_comments.clear')}}</a>
                                    @endif
                                </div>
                            </div>
                            @endif
                        @endforeach

                    </div>
                </div>
                {!! Form::open(['action' => ['PropertyCommentsController@create'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal1']) !!}
                {!! Form::token() !!}
                <div style="margin-left: 200px">
                        <input type="hidden" name="property_id" type="text" value="{{$property_id}}"required>
                        <textarea style="width:50%;padding:30px" name="comment" placeholder="{{__('property_comments.leave_a_comment')}}" class="label-rounded" required></textarea>
                        <button style="margin-bottom: 80px; margin-left: 30px" class="btn btn btn-rounded btn-default btn-outline m-r-5"><i class="ti-check text-success m-r-5"></i>{{__('property_comments.release')}}</button>
                </div>
                {!! Form::close() !!}

                <div id="myModal"  class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">{{__('property_comments.edit_comment')}}</h4>
                            </div>
                            {!! Form::open(['action' => ['PropertyCommentsController@update_comment'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal1']) !!}
                            {!! Form::token() !!}
                            <div class="modal-body">
                                <input type="hidden" name="comment_id_modal" class="comment_id_modal" type="text" >
                                <textarea style="width:100%;padding:30px" name="comment_edit_modal" placeholder="{{__('property_comments.comment')}}" class="label-rounded comment_edit_modal" required></textarea>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('property_comments.close')}}</button>
                                <button type="submit" class="btn btn-primary">{{__('property_comments.save_changes')}}</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
   
@endsection


@section ('scripts')
 <script>
        document.addEventListener("DOMContentLoaded", function (event) {
            $('#change_property_select').change(function(){
                window.location.href = "{{url('/property')}}"+"/"+$(this).val()+"/comments";
            })
        });
    </script>

@endsection
@section('js')
    <script type="text/javascript" src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
    <script>
            $(document).ready(function () {
                $('.edit_comment').click(function () {
                    var id = $(this).data();
                    $('.comment_edit_modal').val(id.content);
                    $('.comment_id_modal').val(id.value);
                })
            })
    </script>
@endsection