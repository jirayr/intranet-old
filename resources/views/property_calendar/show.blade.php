@extends('layouts.admin') @section('content')

<div class="row">
    @if (Session::has('message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <p>
            <i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
    </div>
    @endif @if (Session::has('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <p>
            <i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
    </div>
    @endif


    <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="panel">
            <div class="panel-heading">{{__('calendar.title')}} </div>
			<div style="text-align:center">
				<select style="width:25%;padding:5px" id="change_property_select">
					@foreach ($Masterlistes as $Masterliste)
						
						@if($Masterliste->id == $PropertyId)
							<option selected value="{{$Masterliste->id}}">{{$Masterliste->id}} / {{$Masterliste->object}} / {{$Masterliste->asset_manager_name}}</option>
						@else
							<option value="{{$Masterliste->id}}">{{$Masterliste->id}} / {{$Masterliste->object}} / {{$Masterliste->asset_manager_name}}</option>
						@endif
						
					@endforeach
				</select>
			</div>
            <div class="table-responsive">
                <div id="calendar_table"></div>
            </div>
        </div>
    </div>
    

    <script>
    function formatDate(date) {
        if(date==null) return "";
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [month,day,year].join('/');
}
        document.addEventListener("DOMContentLoaded", function (event) {
var arrayEvents=[];

$('#create_event').click(function(){
    if($('#start_date').val()>$('#end_date').val()){
        alert('{{__("calendar.error_create")}}');
    }
    else{
        $('#create_event').attr('type','submit');
        $('#create_event').click();
    }
})

$('#change_property_select').change(function(){
	
	window.location.href = "{{url('/propertycalendar')}}" +"/"+$(this).val();
})
$('#edit-event').click(function(){
    if($('#event-edit-start_date').val()>$('#event-edit-end_date').val()){
        alert('{{__("calendar.error_edit")}}');
    }
    else{
        $('#edit-event').attr('type','submit');
        $('#edit-event').click();
    }
})

            var data=<?php echo json_encode($PropertyCalendars)?>;
			console.log(data);
            for(var i in data){
                data[i].title=data[i].description;
                
            }
            $('#calendar_table').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
				locale: 'de',
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                events: 
                    data,
                eventClick: function (event, element) {
                    $('#my-event').modal('show');
                    $('#id_edit').val(event.id);
                    $('#event-edit-form').val(event.title);
                    $('#event-edit-description-form').val(event.description);
                    $('#event-edit-start_date').val(formatDate(event.start));
                    $('#event-edit-end_date').val(formatDate(event.end));
                },
                dayClick: function (date, jsEvent, view) {
                    $('#my-event-create').modal('show');
                    $('start').val(date._d);
                }
            });

        });
    </script>
    <style>
        .fc-event {
            background: #666666;
        }

        .fc-toolbar {
            background: white;
        }

        .fc-center {
            color: black;
        }

        .fc-toolbar h2 {
            color: black;
        }

        .fc-button {
            background: white;
            border: 1px solid black;
            color: black;
        }
        .fc-today {
            background:white !important;
        }
        .fc-unthemed .fc-today {
            background:green !important;
        }
        .fc th.fc-sat,
        .fc th.fc-sun,
        .fc th.fc-thu,
        .fc th.fc-tue {
            background: #cccccc;
        }

        .fc th.fc-fri,
        .fc th.fc-mon,
        .fc th.fc-wed {
            background: #cccccc;
        }

        a {
            color: black;
        }
    </style>
</div>

<div class="modal fade none-border" id="my-event" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    <strong>{{__('property_calendar.edit_calendar')}}</strong>
                </h4>
            </div>
            {!! Form::open(['action' => ['PropertyCalendarController@edit'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal1']) !!}
        {!! Form::token() !!}
            <div class="modal-body">
                <form  class="form-horizontal1"  action="">
                    <input type="hidden" class="form-control" id="event-create-title" name="property_id" type="text" value="{{$PropertyId}}"required>
                    <input hidden=true id="id_edit" name="id" type="text"/>
					
                    <label>{{__('property_calendar.calendar_description')}}</label></br>
                    <textarea class="form-control" id="event-edit-description-form" name="description"></textarea></br>
                    <label>{{__('property_calendar.start_date')}}</label>
                    <input type="text" name="start" id="event-edit-start_date" required autocomplete="off" class="form-control datepicker"
                           value="">
                    <label>{{__('property_calendar.end_date')}}</label>
                    <input type="text" name="end" id="event-edit-end_date" required autocomplete="off" class="form-control datepicker"
                           value="">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">{{__('property_calendar.close')}}</button>
                        <button type="button" id="edit-event" name="save" value="save" class="btn btn-success waves-effect waves-light"> {{__('property_calendar.save')}}</button>
                        <button type="submit" id="delete-event" name="delete" value="delete" class="btn btn-danger delete-event waves-effect waves-light"
                                style="display: inline-block;">{{__('property_calendar.delete')}}</button>
                    </div>
                </form>
            </div>

            </div>
            {!! Form::close() !!}

        </div>
    </div>
</div>
{!! Form::open(['action' => ['PropertyCalendarController@create'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
        {!! Form::token() !!}
<div class="modal fade none-border" id="my-event-create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <strong>{{__('property_calendar.add_calendar')}}</strong>
                </h4>
            </div>
            <div class="modal-body">
                <input type="hidden" class="form-control" id="event-create-title" name="property_id" type="text" value="{{$PropertyId}}"required>
                <label>{{__('property_calendar.calendar_description')}}</label>
                <textarea class="form-control" id="event-create-description-form" name="description"></textarea></br>
                <label>{{__('property_calendar.start_date')}}</label>
                <input type="datetime3" name="start" id="start_date" required autocomplete="off" class="form-control datepicker" >
                <label>{{__('property_calendar.end_date')}}</label>
                <input type="datetime4" name="end" id="end_date" required autocomplete="off" class="form-control datepicker" >
                <div class="modal-footer">
                <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">{{__('property_calendar.close')}}</button>
                <button type="button" id="create_event" name="create" class="btn btn-success save-event waves-effect waves-light">{{__('property_calendar.create_calendar')}}</button>
            </div>
            </div>

        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection