@extends('layouts.admin')
@section('content')
    <form  method="post" action="{{url('employee/store')}}">

        <div class="row">
            <input required type="hidden" class="form-control" name="company_id" value="{{Request::segment(2)}}" placeholder="Enter Zip">

            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Anrede:</label>
                <input required type="text" class="form-control" name="anrede" placeholder="Enter Anrede">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Vorname:</label>
                <input required type="text" class="form-control" name="vorname" maxlength="105"  placeholder="Enter Vor Name" id="email">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Nachname:</label>
                <input required type="text" class="form-control" name="nachname"   placeholder="Enter Zip">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Telefon:</label>
                <input  type="text" class="form-control" name="phone" maxlength="105"  placeholder="Enter nach Name" id="email">
            </div>

        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Fax:</label>
                <input  type="text" class="form-control" name="fax"   placeholder="Enter Fax">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">E-Mail:</label>
                <input  type="text" class="form-control" name="email" maxlength="105"  placeholder="Enter Email" id="email">
            </div>

        </div>


        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Letzter Kontakt:</label>
                <input  type="text" class="form-control" name="last_contact"  placeholder="Enter Last Contact">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Keywords:</label>
                <input  type="text" class="form-control" name="keyword" maxlength="105"  placeholder="Enter Keyword" id="email">
            </div>

        </div>

       <div class="row">
           <div class="form-group col-md-6" style="width: 50%">
               <label for="zip">Verantwortung:</label>
               <input  type="text" class="form-control" name="responsibility" placeholder="Enter Responsibility">
           </div>
           <div class="form-group col-md-6" style="width: 50%">
               <label for="email">Kategorie:</label>
               <select class="form-control" name="category" id="">
                   <option>Kategorie wählen</option>
                   <option value="Bank">Bank</option>
                   <option value="Tenant">Mieter</option>
               </select>
           </div>
       </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Internet:</label>
                <input required type="text" class="form-control" name="internet" placeholder="Enter internet">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Notizen:</label>
                <textarea class="form-control" name="notes" id="" cols="30" rows="10"></textarea>
            </div>

        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Kennung :</label>
                <input type="text" class="form-control" name="kennung" placeholder="Enter kennung">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Mitarbeiter Adresse:</label>
            </div>
        </div>
       <div class="row">
           <div class="form-group col-md-6" style="width: 50%">
               <label for="zip">PLZ:</label>
               <input required type="text" class="form-control" name="zip"  placeholder="Enter Employee Address">
           </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="street">Strasse:</label>
                <input required type="text" class="form-control" name="street" placeholder="Enter Street">
            </div>

       </div>
       <div class="row">
           <div class="form-group col-md-6" style="width: 50%">
               <label for="city">Stadt:</label>
               <input required type="text" class="form-control" name="city" placeholder="Enter City">
           </div>
           <div class="form-group col-md-6" style="width: 50%">
               <label for="part of city">Stadteil:</label>
               <input required type="text" class="form-control" name="part_of_city" placeholder="Enter Part Of City">
           </div>
       </div>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <button type="submit" class="btn btn-primary">Speichern</button>
    </form>
@stop