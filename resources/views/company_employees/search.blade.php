@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="card-header">
        {{--<div class="row">--}}
            {{--<div class="col-md-10">--}}
                {{--@if(isset($company))--}}
                {{--<h4 class="card-title">{{$company->name}}</h4>--}}
                {{--@endif--}}
            {{--</div>--}}
            {{--<div class="col-md-2">--}}
                {{--@if(isset($company))--}}
                {{--<a href="{{url('company/'.$company->id.'/employee/add')}}" class="btn btn-primary">Neu</a>--}}
                {{--@else--}}
                {{--<a href="{{url('company/'.$companyId.'/employee/add')}}" class="btn btn-primary">Neu</a>--}}
                {{--@endif--}}

            {{--</div>--}}


            <form action="{{url('/company/search')}}" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="row">

                    <div class="col-md-4">
                        <select required class="form-control  search" name="key[]"  style=""></select>
                    </div>
                    <div class="col-md-2">
                        <button style="" type="submit"  class="btn btn-success ">Suche</button>
                    </div>

                </div>
            </form>
            <br>
        {{--</div>--}}
    </div>
    <div class="col-sm-12 table-responsive white-box">
        <table class="table table-striped" >
            <thead>
            <tr>
                <th>Kategorie</th>
                <th>Anrede</th>
                <th>Vorname</th>
                <th>Nachname</th>
                <th>Telefon</th>
                <th>Fax</th>
                <th>E-Mail</th>
                <th>Internet</th>
                <th>Notizen</th>
                <th>Letzter Kontakt</th>
                <th>Keyword</th>
                <th>Verantwortung</th>
                <th>Kennung</th>
                <th>PLZ</th>
                <th>Strasse</th>
                <th>Stadt</th>
                <th>Stadteil</th>
            </tr>
            </thead>
            <tbody>
            @isset($employees)
            @foreach($employees as $employee)
                <tr>
                    <td>{{$employee->category}}</td>
                    <td>{{$employee->anrede}}</td>
                    <td>{{$employee->vorname}}</td>
                    <td>{{$employee->nachname}}</td>
                    <td>{{$employee->phone}}</td>
                    <td>{{$employee->fax}}</td>
                    <td>{{$employee->email}}</td>
                    <td>{{$employee->internet}}</td>
                    <td>{{$employee->notes}}</td>
                    <td>{{$employee->last_contact}}</td>
                    <td>{{$employee->keyword}}</td>
                    <td>{{$employee->responsibility}}</td>
                    <td>{{$employee->kennung}}</td>
                    <td>{{$employee->employeeAddress->zip}}</td>
                    <td>{{$employee->employeeAddress->street}}</td>
                    <td>{{$employee->employeeAddress->city}}</td>
                    <td>{{$employee->employeeAddress->part_of_city}}</td>
                    <td class="">
                        <a type="button" href="{{url('employee/edit',$employee->id)}}" class="btn-sm btn-info">Bearbeiten</a>
                        <a type="button" href="{{url('employee/delete',$employee->id)}}" class="btn-sm btn-danger">Löschen</a>
                    </td>
                </tr>
            @endforeach
            @endisset
            </tbody>
        </table>
    </div>
    {{ $employees->links() }}

</div>

@endsection

@section('scripts')
    <script>

        $(document).ready(function() {

            $(".search").select2({

                minimumInputLength: 2,

                language: {

                    inputTooShort: function () {
                        return "<?php echo 'Please enter 2 or more characters'; ?>"
                    }
                },
                multiple: true,
                tags: true,
                ajax: {

                    url: '{{route("autocomplete")}}',
                    dataType: 'json',
                    data: function (term) {
                        return {
                            query: term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.keyword,
                                    id: item.id,
                                    slug: item.keyword

                                }

                            })

                        };
                    }

                }

            });
        });

    </script>


@stop