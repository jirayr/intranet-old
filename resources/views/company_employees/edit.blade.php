@extends('layouts.admin')
@section('content')

    <form method="POST" action="{{url('/employee/update/'.$employee->id)}}" enctype="multipart/form-data">

        <div class="row">

            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Anrede:</label>
                <input required type="text" class="form-control" name="anrede" value="{{$employee->anrede}}"   placeholder="Enter Zip">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Vor Name:</label>
                <input required type="text" class="form-control" name="vorname"  value="{{$employee->vorname}}"  maxlength="105"  placeholder="Enter Keyword" id="email">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Nach Name:</label>
                <input required type="text" class="form-control" name="nachname" value="{{$employee->nachname}}"   placeholder="Enter Zip">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Phone:</label>
                <input required type="text" class="form-control" name="phone" maxlength="105" value="{{$employee->phone}}"  placeholder="Enter Keyword" id="email">
            </div>

        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Fax:</label>
                <input  type="text" class="form-control" name="fax"  value="{{$employee->fax}}" placeholder="Enter Zip">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Email:</label>
                <input  type="text" class="form-control" name="email" maxlength="105" value="{{$employee->email}}"  placeholder="Enter Keyword" id="email">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Last Contact:</label>
                <input  type="text" class="form-control" name="last_contact" value="{{$employee->last_contact}}"   placeholder="Enter Zip">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Keyword:</label>
                <input  type="text" class="form-control" name="keyword" maxlength="105" value="{{$employee->keyword}}"  placeholder="Enter Keyword" id="email">
            </div>

        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Responsibility:</label>
                <input  type="text" class="form-control" name="responsibility"  value="{{$employee->responsibility}}"   placeholder="Enter Zip">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Category:</label>
                <select class="form-control" name="category" id="">
                    <option>Select Category</option>
                    <option @if($employee->category == 'Bank')selected @endif   value="Bank">Bank</option>
                    <option @if($employee->category == 'Tenant')selected @endif value="Tenant">Tenant</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Internet:</label>
                <input  type="text" class="form-control" name="internet"  value="{{$employee->internet}}"  placeholder="Enter Zip">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Notes:</label>
                <textarea class="form-control" name="notes" id="" cols="30" rows="10">{{$employee->notes}}</textarea>
            </div>

        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Kennung :</label>
                <input type="text" class="form-control" name="kennung"  value="{{$employee->kennung}}" placeholder="Enter kennung">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Employee Address:</label>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">Zip:</label>
                <input required type="text" class="form-control" name="zip"  value="{{$employee->employeeAddress->zip}}" placeholder="Enter Zip">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="street">Street:</label>
                <input required type="text" class="form-control" name="street"  value="{{$employee->employeeAddress->street}}" placeholder="Enter Street">
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="city">City:</label>
                <input required type="text" class="form-control" name="city" value="{{$employee->employeeAddress->city}}"  placeholder="Enter City">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="part of city">Part Of City:</label>
                <input required type="text" class="form-control" name="part_of_city" value="{{$employee->employeeAddress->part_of_city}}"  placeholder="Enter Part Of City">
            </div>
        </div>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <button type="submit" class="btn btn-primary">update</button>
    </form>
@stop