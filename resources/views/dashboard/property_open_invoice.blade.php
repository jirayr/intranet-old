<?php
$user_email = strtolower(Auth::user()->email);
$replyemailaddress = config('users.falk_email');
		
?>
<table class="table table-striped" id="table-property-open-invoice" >
	<thead>
		<tr>
			<th>Objekt</th>
			<th>Rechnung</th>
			<th>R.Datum</th>
			<th class="text-right">Betrag</th>
			<th>Kommentar</th>
			<th>Abbuch.</th>
			<th>Umlegb.</th>
			<th>User</th>
			<th>Datum</th>
			<!-- <th>Kommentar Falk</th> -->
			{{-- <th>Aktion</th> --}}
			<th style="min-width: 175px;">Freigabe AM</th>
			<th style="min-width: 175px;">Freigabe HV</th>
			<th style="min-width: 175px;">Freigabe</th>
			<th style="min-width: 175px;">Freigabe Falk</th>
			<th>Aktion</th>
		</tr>
	</thead>
	<?php $delete=1; ?>
	<tbody>
	@foreach($res as $value)
		
		<?php

			$download_path = "https://drive.google.com/drive/u/2/folders/".$value->file_basename;
        	if($value->file_type == "file"){
            	$download_path = "https://drive.google.com/file/d/".$value->file_basename;
        	}

        	$subject = 'Offene Rechnung: '.$value->name_of_property;
			$content = 'Rechnung: <a title="'.$value->invoice.'" href="'.$download_path.'" target="_blank">'.$value->invoice.'</a>';
			if($value->comment){
				$content .= '<br/>Kommentar User: '.$value->comment;
			}

			$taburl = route('properties.show',['property' => $value->property_id]).'?tab=property_invoice';
			$taburl = "<a href='".$taburl."'>".$taburl."</a>";

		?>
		<tr>
			<td><a href="{{route('properties.show',['property'=>$value->property_id])}}?tab=property_invoice">{{$value->name_of_property}}</a></td>
			<td>
				<a  target="_blank"  title="{{$value->invoice}}"  href="{{$download_path}}">{{$value->invoice}}</a>
			</td>
			<td>{{show_date_format($value->date)}}</td>
			<td class="text-right">{{show_number($value->amount,2)}}</td>
			<td>
				@php
					$comment = DB::table('properties_comments as pc')->selectRaw('pc.*, u.name, u.role, u.company')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.record_id', $value->id)->where('pc.type', 'property_invoices')->orderBy('pc.created_at', 'desc')->first();
				@endphp
				@if($comment)
					@php
						$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
						$commented_user = $comment->name.''.$company;
					@endphp
					<p class="long-text" style="margin-bottom: 0px;">
						<span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
					</p>
				@endif
				<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="{{ $value->id }}" data-property-id="{{ $value->property_id }}" data-type="property_invoices" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
			</td>
			<td><?php echo get_paid_checkbox($value->id,$value->need_to_pay);?></td>
			<td><?php echo get_paid_checkbox($value->id,$value->towards_tenant)."<br>".show_number($value->foldable,2); ?></td>
			<td>
				<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $value->property_id }}" data-user-id="{{ $value->user_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="OFFENE  RECHNUNGEN" data-id="{{ $value->id }}" data-section="property_invoices">{{ $value->name }}</a>
			</td>
			
			<td>{{show_date_format($value->created_at)}}</td>

			<!--<td>
				
					<button data-id="<?=$value->id?>" data-property_id="<?=$value->property_id?>" type="button" class="btn btn-primary invoice-release-request2" data-column="request2">Zur Freigabe an Falk senden</button>
				
			</td>-->
			<td>
				@if(isset($value->button))
					{!! $value->button['btn_release_am'] !!}
				@endif
			</td>
			<td>
				@if(isset($value->button))
					{!! $value->button['btn_release_hv'] !!}
				@endif
			</td>
			<td>
				@if(isset($value->button))
					{!! $value->button['btn_release_usr'] !!}
				@endif
			</td>
			<td>
				@if(isset($value->button))
					{!! $value->button['btn_release_falk'] !!}
				@endif
			</td>
			<td>
				<button type="button" data-id="{{ $value->id }}" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>
			</td>
		</tr>
		
	@endforeach
	</tbody>
</table>
