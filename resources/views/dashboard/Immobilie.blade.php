<?php

$form_id = 'immoscout_form';
$externalId = '';
$title = '';
$street = $properties['strasse'];
$houseNumber = $properties['hausnummer'];
$postcode = $properties['plz_ort'];
$city = $properties['ort'];
$showAddress = 'true';
$apartmentType = $properties['type_of_property'];
$lift = '';
$cellar = '';
$handicappedAccessible = '';
$condition = '';

/*
 * price variables
 * */
$value = '';
$currency = 'EUR';
$marketingType = 'RENT';
$priceIntervalType = '';


$livingSpace = '';
$numberOfRooms = '';
$builtInKitchen = '';
$balcony = '';
$garden = '';
$hasCourtage = 'NO';
$courtage = '';


$property_id =  Request::segment(2);
$realsate = DB::table('property_images')->where('property_id', $property_id)->orderBy('id', 'desc')->limit(1)->first();
if(!is_null($realsate)){

    $form_id = 'immoscout_form_edit';
    $realsate_id = $realsate->realstate_id;

    $url =  "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/realestate/".$realsate_id;


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://rest.sandbox-immobilienscout24.de/restapi/api/offer/v1.0/user/me/realestate/".$realsate_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "authorization: OAuth oauth_consumer_key=\"08211746Key\",oauth_token=\"4aca0424-da15-42d7-bde6-87353d9d82d7\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"1555299289\",oauth_nonce=\"e6x6VQ\",oauth_version=\"1.0\",oauth_signature=\"dBbQMwhZgBFRmSi8TYwADHxyags%3D\"",
            "cache-control: no-cache",
            "postman-token: b13e992f-eb58-27c6-c1e8-dc518686f040"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);


    if ($err) {
        echo "cURL Error #:" . $err;
    } else {



        if($xml = new \SimpleXMLElement($response)){
            $xml = new \SimpleXMLElement($response);
            $response = json_encode($xml, JSON_PRETTY_PRINT);
            $response = json_decode($response, TRUE);


            if(isset($response['externalId']) ){


                $externalId = $response['externalId'];
                $title = $response['title'];
                $street = $response['address']['street'];
                $houseNumber = $response['address']['houseNumber'];
                $postcode = $response['address']['postcode'];
                $city = $response['address']['city'];
                $showAddress = $response['showAddress'];
                $value = $response['price']['value'];
                $currency = $response['price']['currency'];
                $marketingType = $response['price']['marketingType'];
                $priceIntervalType = $response['price']['priceIntervalType'];
                $livingSpace = $response['livingSpace'];
                $numberOfRooms = $response['numberOfRooms'];
                $hasCourtage = $response['courtage']['hasCourtage'];
                $courtage = $response['courtage']['courtage'];



            }
//            echo '<pre>';
//            print_r($response);
//            echo '</pre>';

        }





    }




}







?>




<div id="response"></div>

<form class="{{ $form_id }}" method="post" action="{{ url('/') }}/properties/upload_listing_to_api" enctype="multipart/form-data">
        <!-- Modal content-->
      
        <div id="json_resp_immoc">
          
        </div>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Immoscout24 Upload</h4>
                @if($form_id == "immoscout_form_edit")
                <a type="button" data-toggle="tab" href=".uploadAttachement" class="btn btn-primary pull-right" style="margin-top: -25px;">Upload Attachment</a>
                @endif
            </div>
            <div class="modal-body">


                    
                     <label>Kategorie</label>
                    <select name="category" class="form-control" required>
                        <option value="1">Wohnimmobilie/Wohngrundstück</option>
                        <option value="2">Gewerbeimmobilie/Gewerbegrundstück</option>

                    </select><br>

                                        
                    <label>Typ</label>
                    <select name="property_type" class="form-control" required>
                        <option value="1">Büro/Praxis</option>
                        <option value="2">Einzelhandel</option>
                        <option value="3">Spezialgewerbe</option>

                    
                    </select><br>

                    <label>Postleitzahl</label>
                    <input type="text" value="{{ $postcode  }}" name="postcode" class="form-control" placeholder="PLZ" required>

                    
                    <label>Addresse</label>
                    <input type="address" value="{{ $address  }}" name="address" class="form-control" placeholder="Addresse" required>

                    <label>Hausnummer</label>
                    <input type="house_no" value="{{ $house_no  }}" name="house_no" class="form-control" placeholder="Addresse" required>


                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <input type="hidden" name="property_id" value="{{ $item->id }}" />
                    
                    <label>Titel</label>
                    <input type="text" value="{{ $title  }}" name="title" class="form-control" placeholder="Titel" required>

                    <h4>Addresse</h4>

                    <label>{{-- Eckdaten --}} Postleitzahl</label>
                     <input type="number" value="{{ $postcode  }}" name="postcode" placeholder="typr here" class="form-control" required>
                     <label>Stadt</label>
                     <input type="text" name="city" value="{{ $city  }}"  placeholder="typr here" class="form-control" required>
                    <label>Strasse</label>
                     <input type="text" name="street" value="{{ $street  }}"  placeholder="typr here" class="form-control" required>
                    <label>Nummer</label>
                     <input type="text" name="houseNumber"  value="{{ $houseNumber  }}"  placeholder="typr here" class="form-control" required>


                    <label>Addrese anzeigen</label>
                    <select name="showAddress" class="form-control" required>
                        <option @if($showAddress == "true") selected @endif value="true">Ja</option>
                        <option @if($showAddress == "false") selected @endif value="false">Nein</option>
                    </select>

                     <label>Objektart</label>
                    <select name="commercializationType" class="form-control" required>
                        <option value="RENT">Miete</option>
                        <option value="BUY">Kauf</option>
                        <option value="LEASE">Pacht</option>
                        <option value="LEASEHOLD">Erbpacht</option>
                        <option value="COMPULSORY_AUCTION">Zwangsversteigerung</option>
                        <option value="RENT_AND_BUY">Miete und Kauf</option>

                    </select><br>
                    <input type="hidden" value="{{ $externalId  }}" name="externalId">
                    <label>Kategorie</label>
                    <input type="text" required name="utilizationTradeSite" placeholder="Hier eingeben" class="form-control">

                    <h4>Preis</h4>

                    <label>Preis in (€)</label>
                    <input type="text" name="value" placeholder="Hier eingeben" value="{{ $value  }}" class="form-control" required>

                    <label>Währung</label>
                    <select name="currency" class="form-control" required>
                        <option value="">---Wählen---</option>
                        <option @if($currency == "EUR") selected @endif  value="EUR">EUR</option>
                    </select>

                    <label>Marketing Typ</label>
                    <input type="text" name="marketingType"  value="{{ $marketingType }}" placeholder="Hier eingeben" class="form-control" required>

                    <label>Fläche</label>
                    <input type="text" name="plotArea" value="{{ $livingSpace }}" placeholder="Hier eingeben" class="form-control" required>

                    <h4>Provision</h4>

                    <select name="hasCourtage" class="form-control" required>
                        <option  value="volvo">---Wählen---</option>
                        <option @if($hasCourtage == "YES") selected @endif value="YES">Ja</option>
                        <option @if($hasCourtage == "NO") selected @endif value="NO">Nein</option>
                    </select>

                    <label>Maklerprovision</label>
                    <input type="text" name="courtage" value="{{ $courtage  }}" placeholder="Hier eingeben" class="form-control">

            </div>
            <div class="modal-footer">

                <button type="submit" class="btn btn-primary" >Upload <i class="fa fa-circle-o-notch fa-spin immo_loader hide" style="font-size:14px"></i></button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>

            </form>
        </div>
 