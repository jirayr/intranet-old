<div class="table-responsive">
	<div class="property-table">
    <table class="table table-striped dashboard-table category-table4">
        <thead>
        <tr>
            <th>Mietzahlung</th>
            <th class="text-right">Anzahl</th>
            <th class="text-right">Mietausfall</th>
        </tr>
        </thead>
        <tbody>
        <?php $c = $c2 = 0;?>
        @foreach($data as $k=>$row)
        <?php
        $c +=$row;
        $c2 += $data1[$k]; 
        ?>
        <tr>
            <td>@if($k=='1')
            1 (Keine Probleme)
            @elseif($k=='2')
            2 (evtl. Probleme)
            @elseif($k=='3')
            3 (Mieter zahlt nicht)
            @else
            {{$k}}
            @endif</td>
            <td class="text-right"><a href="javascript:void(0)" class="get-category-properties3" data-category="{{$k}}">{{$row}}</a></td>
            <td class="text-right">{{ number_format($data1[$k], 2,",",".") }}</td>
        </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Gesamt</th>
                <th class="text-right">{{ number_format($c, 0,",",".") }}</th>
                <th class="text-right">{{ number_format($c2, 2,",",".") }}</th>
            </tr>
        </tfoot>
    </table>
    </div>
</div>
