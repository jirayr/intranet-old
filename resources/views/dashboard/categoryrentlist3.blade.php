<div class="table-responsive">
	<div class="property-table">
    <table class="table table-striped dashboard-table category-table">
        <thead>
        <tr>
            <th>Objekt</th>
            <th>AM</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
        <tr>
            <td><a href="{{route('properties.show',['property'=>$row['main_property_id']])}}?tab=tenancy-schedule">{{$row['name_of_property']}}</a></td>
            <td>{{$row['name']}}</td>        
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
