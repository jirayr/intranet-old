<div class="table-responsive">
	<div class="property-table">
    <table class="table table-striped dashboard-table name-rent-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Objekt</th>
            <th>Mieter geschl.</th>
            <th>Mietzahlung</th>
            <th class="text-right">Mietausfall</th>
            <th>Bundesland</th>
            <th>Kategorie</th>
            <th>Kommentar</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
        <?php


        if(($row['rent_begin'] != '' && $row['rent_begin'] > date('Y-m-d')) ||($row['rent_end'] != '' && $row['rent_end'] < date('Y-m-d')))
              continue;



        if($row['actual_net_rent'] && $row['rental_space'])
        {

        
        ?>
        <tr>
            <td>{{$row['name']}}</td>
            <td><a href="{{route('properties.show',['property'=>$row['main_property_id']])}}?tab=tenancy-schedule">{{$row['name_of_property']}}</a></td>
            
            <td>@if($row['tenant_closed'])Ja @else {{'Nein'}}@endif</td>
            <td>
            @if($row['rent_payment']=='1')
            1 (Keine Probleme)
            @elseif($row['rent_payment']=='2')
            2 (evtl. Probleme)
            @elseif($row['rent_payment']=='3')
            3 (Mieter zahlt nicht)
            @else
            {{$row['rent_payment']}}
            @endif
            </td>
            <td class="text-right">@if($row['actual_net_rent']){{ number_format($row['actual_net_rent'], 2,",",".") }}@else{{'0,00'}}@endif</td>
            <td>{{$row['niedersachsen']}}</td>
            <td>{{$row['category']}}</td>
            <td>{{$row['comment3']}}</td>
        </tr>
    <?php } ?>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
