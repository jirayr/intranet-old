
<button class="btn btn-primary show-all-release-invoice m-b-10" data-url="{{ route('getPropertyReleaseInvoice') }}?all=1">Alle</button>
<table id="table-property-release-invoice" class="table table-striped">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>AM</th>
			<th>Rechnung</th>
			<th>Re. D.</th>
			<th>Re. Bet.</th>
			<th>Datum</th>
			<th>Kommentar</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		$user_email = strtolower(Auth::user()->email);
		$replyemailaddress = 't.raudies@fcr-immobilien.de';
		$pr_arr = array(3363,3417,3390,3394,2179);

		// $not_shown_array  = config('properties.not_shown_properties_id');

		?>
		
		@if (!empty($data))
			@foreach ($data as $value)

				<?php
					$amount_condition = 0;
	                if(!in_array($value->property_id, $pr_arr))
	                    $amount_condition = 10000;


	                if($value->status==6)
	                	$amount_condition = 10000;
	                else
	                	$amount_condition = 0;

	                if($value->property_id==121)
	                    $amount_condition = 30000;

	                
	                if($user_email==$replyemailaddress && (!$value->hvbu_id && !$value->hvpm_id))
						$amount_condition = 0;                	

					if(isset($value->hausmaxx) && $value->hausmaxx=="Eigenverwaltung")
	                	$amount_condition = 0;

	                if($value->status==8)
	                    $amount_condition = 0;  


					// if($value->property_id==3471  || $value->property_id==3363 || $value->property_id==3417 || $value->property_id==3394 || $value->property_id==3473 || $value->property_id==183 || $value->property_id==182 || $value->property_id==1197 || $value->property_id==3652 || $value->property_id==1194 || $value->property_id==3912 || $value->property_id==3925|| $value->property_id==4024)
	                	//$amount_condition = 0;


	                // if(in_array($value->property_id, array(3471, 3363, 3417, 3394, 3473, 183, 182, 1197, 3652, 1194, 3912, 3925,4024,4267)))
	                	// $amount_condition = 0;

	                if(in_array($value->property_id, $property_not_show)){
	                	$amount_condition = 0;
	                }

				?>

				@if($user_email==$replyemailaddress || $user_email=="j.lux@fcr-immobilien.de" || $user_email=="n.eschenbach@fcr-immobilien.de")
					@if($value->amount>$amount_condition)
						<tr>
							<td>
								<a href="{{route('properties.show',['property'=> $value->property_id])}}?tab=property_invoice">
		                            {{$value->name_of_property}}
		                        </a>
							</td>
							<td>{{ $value->name }}</td>
							<td>
								<?php 
									$download_path = "https://drive.google.com/drive/u/2/folders/".$value->file_basename;
					                if($value->file_type == "file"){
					                    $download_path = "https://drive.google.com/file/d/".$value->file_basename;
					                }
								?>
								<a title="{{ $value->invoice }}" href="{{ $download_path }}" target="_blank">{{ $value->invoice }}</a>
							</td>
							<td>{{ show_date_format($value->date) }}</td>
							<td class="text-right">{{ show_number($value->amount,2) }}</td>
							<td>{{ show_datetime_format($value->created_at) }}</td>
							<td>
								@php
									$comment = DB::table('properties_comments as pc')->selectRaw('pc.*, u.name, u.role, u.company')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.record_id', $value->id)->where('pc.type', 'property_invoices')->orderBy('pc.created_at', 'desc')->first();
								@endphp
								@if($comment)
									@php
										$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
										$commented_user = $comment->name.''.$company;
									@endphp
									<p class="long-text" style="margin-bottom: 0px;">
										<span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
									</p>
								@endif
							</td>
							<td>
							<button class="btn btn-primary mark-as-paid" data-id="{{$value->id}}">Offen</button>
							
							</td>					
						</tr>
					@endif
				@else
					<tr>
						<td>
							<a href="{{route('properties.show',['property'=> $value->property_id])}}?tab=property_invoice">
	                            {{$value->name_of_property}}
	                        </a>
						</td>
						<td>{{ $value->name }}</td>
						<td>
							<?php 
								$download_path = "https://drive.google.com/drive/u/2/folders/".$value->file_basename;
				                if($value->file_type == "file"){
				                    $download_path = "https://drive.google.com/file/d/".$value->file_basename;
				                }
							?>
							<a title="{{ $value->invoice }}" href="{{ $download_path }}" target="_blank">{{ $value->invoice }}</a>
						</td>
						<td>{{ show_date_format($value->date) }}</td>
						<td class="text-right">{{ show_number($value->amount,2) }}</td>
						<td>{{ show_datetime_format($value->created_at) }}</td>
						<td>
							@php
								$comment = DB::table('properties_comments as pc')->selectRaw('pc.*, u.name')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.record_id', $value->id)->where('pc.type', 'property_invoices')->orderBy('pc.created_at', 'desc')->first();
							@endphp
							@if($comment)
								<p class="long-text" style="margin-bottom: 0px;">
									<span class="commented_user">{{ $comment->name }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
								</p>
							@endif
						</td>
						<td>
						</td>					
					</tr>
				@endif
				
			@endforeach
		@endif
	</tbody>
</table>