<table class="table table-striped dashboard-table" id="recommendation-table">
    <thead>
        <tr>
            <th>Objekt</th>
            <th>AM</th>
            <th>Mieter</th>
            <th>Fläche</th>
            <th>Art</th>
            <th>Kosten Umbau</th>
            <th>Einnahmen p.m.</th>
            <th>Laufzeit in Monate</th>
            <th>Mietfrei in €</th>
            <th>Jährl. Einnahmen</th>
            <th>Gesamt-einnahmen</th>
            <th>Gesamt-einnahmen netto</th>
            <th>Anhänge</th>
            <th>Kommentar</th>
            <th>Datum</th>
            <th></th>
            <th></th>
            <th></th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @if($data)
            @foreach ($data as $element)
                <?php
                    $details = DB::table('empfehlung_details')->select('empfehlung_details.id', 'slug', 'value','empfehlung_details.created_at','empfehlung_details.user_id','users.name')
                    ->leftJoin('users','users.id','=','empfehlung_details.user_id')
                    ->where('tenant_id', $element->tenant_id)->get();

                    $files = DB::table('gdrive_upload_files')->selectRaw('*')->where('property_id', $element->property_id)->where('parent_id', $element->tenant_id)->where('parent_type', 'empfehlung2')->get();

                    $detail_arr = array();
                    if($details){
                        foreach ($details as $key => $value) {
                            $detail_arr[$value->slug] = $value->value;
                        }
                    }

                    if(!isset($detail_arr['mv_vl'])){
                        if($element->pvid){
                            $detail_arr['mv_vl'] = "mv";
                        }else{
                            $detail_arr['mv_vl'] = "vl";
                        }
                    }

                    $a1 = 0;
                    if(isset($detail_arr['amount1']) && $detail_arr['amount1'])
                      $a1 = $detail_arr['amount1'];

                    $a2 = 0;
                    if(isset($detail_arr['amount2']) && $detail_arr['amount2'])
                      $a2 = $detail_arr['amount2'];

                    $a3 = $a1;
                    // if(isset($detail_arr['annual_revenue']) && $detail_arr['annual_revenue'])
                        // $a3 = $detail_arr['annual_revenue'];

                    $a4 = $a3*$a2;

                    $a5 = 0;
                    if(isset($detail_arr['amount5']) && $detail_arr['amount5'])
                      $a5 = $detail_arr['amount5'];

                    $a6 = 0;
                    if(isset($detail_arr['amount6']) && $detail_arr['amount6'])
                      $a6 = $detail_arr['amount6'];

                    $total = $element->amount;

                    $tenant_name = ( isset($detail_arr['tenant_name']) ) ? $detail_arr['tenant_name'] : '';
                    if(!$element->pvid && $tenant_name){
                        $item = DB::table('tenancy_schedule_items')->select('name', 'rent_begin', 'rent_end')->where('id', $tenant_name)->first();
                        if($item){
                            $date_item = ($item->rent_begin && $item->rent_end) ? '('.show_date_format($item->rent_begin).'-'.show_date_format($item->rent_end).')' : '';
                            $tenant_name = $item->name.' '.$date_item; 
                        }
                        else{
                            $tenant_name = "";
                        }
                    }
                    if(isset($detail_arr['tenant_name_text']) && $detail_arr['tenant_name_text']){
                        $tenant_name = $detail_arr['tenant_name_text'];
                    }

                    $rental_space = '';
                    if(isset($detail_arr['rental_space']) && $detail_arr['rental_space']){
                        $item = DB::table('tenancy_schedule_items')->select('name', 'rental_space', 'vacancy_in_qm')->where('id', $detail_arr['rental_space'])->first();
                        if($item){
                            $vv = ($item->rental_space) ? $item->rental_space : $item->vacancy_in_qm;
                            if(!$vv)
                                $vv = 0;
                            $rental_space = $item->name.' ('.show_number($vv, 2).')';
                        }
                    }
                ?>

                @php
                    $comments = DB::table('empfehlung_comments as ec')->select('ec.id','ec.comment', 'ec.created_at', 'u.name', 'u.role', 'u.company')
                                                    ->join('users as u', 'u.id', 'ec.user_id')
                                                    ->where('ec.tenant_id', $element->tenant_id)
                                                    ->orderBy('ec.created_at', 'desc')
                                                    ->limit(2)->get();
                @endphp

                <?php 
                    $subject = 'Vermietungsempfehlung: '.$element->name_of_property.' '.$tenant_name;
                    $content = 'Mieter: '.$tenant_name;
                    if(isset($comments[0]->comment) && $comments[0]->comment){
                        $content .= '<br/>Kommentar User: '.nl2br($comments[0]->comment);
                    }
                ?>
                <tr>
                    <td>
                        <a href="{{route('properties.show',['property'=> $element->property_id])}}?tab=recommended_tab">{{$element->name_of_property}}</a></td>
                    <td>
                        <a href="javascript:void(0);" class="custom_user" data-property-id="{{ $element->property_id }}" data-user-id="{{ $element->user_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="VERMIETUNGSEMPFEHLUNGEN">{{ $element->name }}</a>
                    </td>
                    <td>{{ $tenant_name }}</td>
                    <td>{{ $rental_space }}</td>
                    <td>{{ strtoupper($detail_arr['mv_vl']) }}</td>
                    <td class="text-right">{{ show_number($total, 2) }}</td>
                    <td class="text-right">{{ show_number($a1, 2)}}</td>
                    <td class="text-right">{{ $a2 }}</td>
                    <td class="text-right">{{ show_number($a5, 2)}}</td>
                    <td class="text-right">{{ show_number($a3*12, 2)}}</td>
                    <td class="text-right">{{ show_number($a4, 2)}}</td>
                    <td class="text-right">{{ show_number($a4-$total-$a5-$a6, 2) }}</td>
                    <td>
                        @if($files)
                            @foreach ($files as $file)
                                 @if($file->file_type == 'file')
                                    <a href="{{ $file->file_href }}"  target="_blank" title="{{ $file->file_name }}">
                                    <i extension="filemanager.file_icon_array.{{ $file->extension }}" class="fa {{ config('filemanager.file_icon_array.' . $file->extension) ?: 'fa-file' }}" ></i>
                                    </a>
                                  @else
                                    <a href="javascript:void(0);" title="{{ $file->file_name }}" onClick="loadDirectoryFiles('{{ $file->file_path }}');"  id="empfehlung-gdrive-link-{{ $file->id }}"  ><i class="fa fa-folder" ></i></a>
                                  @endif
                            @endforeach
                        @endif
                    </td>
                    <td>
                        @if($comments && count($comments))
                            <div class="show_rec_cmnt_section">
                                @foreach ($comments as $comment)
                                    @php
                                        $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                                        $commented_user = $comment->name.''.$company;
                                    @endphp
                                    <p class="long-text"><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
                                @endforeach
                            </div>
                            <a href="javascript:void(0);" data-url="{{ route('get_recommended_comment', ['id' => $element->tenant_id]) }}" class="load_rec_comment_section" data-closest="td">Show More</a>
                        @endif
                        <button type="button" class="btn btn-primary btn-xs recommended-comment" data-id="{{$element->tenant_id}}">Kommentar</button>
                    </td>
                    <td>{{ show_date_format($element->btn2_request_date) }}</td>
                    <td><button type="button" class="btn btn btn-primary vacant-not-release" data-id="{{$element->tenant_id}}" >Ablehnen</button></td>
                    <td>
                        <button type="button" class="btn btn-primary {{ ($user->email == config('users.falk_email')) ? 'btn-recommended-pending' : '' }}" data-id="{{ $element->tenant_id }}" data-url="{{ route('vacant_mark_as_pending', ['vacant_id' => $element->tenant_id]) }}">Pending</button>
                    </td>
                    <td><button type="button" class="btn btn btn-primary vacant-release-request" data-column="btn2" data-id="{{$element->tenant_id}}" data-property_id="{{$element->property_id}}"> Freigeben</button></td>
                    <td>
                        <button type="button" data-id="{{$element->tenant_id}}" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-vacant"><i class="icon-trash"></i></button>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
<input type="hidden" value="{{ $data->count() }}" id="recommendation-total">