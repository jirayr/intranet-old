@extends('layouts.admin')
@section('css')
	<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<!-- Styles -->
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <style type="text/css">
        .table-responsive h2{
            width: 520px;
            float: left;
            font-size: 19px !important;
        }
        .table-responsive h2 label{
            width: 280px;
        }
        .table-responsive h2 a.show-link{
            width: 60px;
            font-size: 16px;
        }
        .table-responsive{
            padding: 0px 15px !important;
        }
        .totalamount{
            float: right;
            text-align: right;
        }
        .two-part li{
            width: 68% !important;
        }
        .two-part li:first-child{
            width: 30% !important;
        }
    </style>
    
@endsection
@section('content')
<?php
 function timeAgo($timestamp){
		 $datetime1=new DateTime("now");
		 $datetime2=new DateTime();
		 $datetime2->setTimestamp($timestamp);
		
		$diff=date_diff($datetime1, $datetime2);
		$timemsg='';
		if($diff->y > 0){
			$timemsg = $diff->y .' year'. ($diff->y > 1?"'s":'');

		}
		else if($diff->m > 0){
		 $timemsg = $diff->m . ' month'. ($diff->m > 1?"'s":'');
		}
		else if($diff->d > 0){
		 $timemsg = $diff->d .' day'. ($diff->d > 1?"'s":'');
		}
		else if($diff->h > 0){
		 $timemsg = $diff->h .' hour'.($diff->h > 1 ? "'s":'');
		}
		else if($diff->i > 0){
		 $timemsg = $diff->i .' minute'. ($diff->i > 1?"'s":'');
		}
		else if($diff->s > 0){
		 $timemsg = $diff->s .' second'. ($diff->s > 1?"'s":'');
		}

	$timemsg = $timemsg.' ago';
	return $timemsg;
	}

?>
    <div class="row" id="dashboard-contain">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif
		
        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif




        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">{{__('dashboard.transactionmanagement')}}</div>
					<div class="row">	
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">{{__('property.in_purchase')}}</h3>
                                <ul class="list-inline two-part">
                                    <li>
                                        <div class="sparklinedash"></div>
                                    </li>
                                    <li class="text-right">
                                        <span class="text-success">
                                            @if(isset($ek_analysis_array[config('properties.status.in_purchase')]))
                                                {{number_format( $ek_analysis_array[config('properties.status.in_purchase')] , 0 ,",",".")}}€
                                                @else
                                                {{number_format( 0 , 0 ,",",".")}}€
                                                @endif
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-xs-12">
							<div class="white-box analytics-info">
								<h3 class="box-title">{{__('property.offer')}}</h3>
								<ul class="list-inline two-part">
									<li>
										<div class="sparklinedash"></div>
									</li>
									<li class="text-right">
										<span class="text-success">
                                                
                                                @if(isset($ek_analysis_array[config('properties.status.offer')]))
                                                {{number_format( $ek_analysis_array[config('properties.status.offer')] , 0 ,",",".")}}€
                                                @else
                                                {{number_format( 0 , 0 ,",",".")}}€
                                                @endif
										</span>
									</li>
								</ul>
							</div>
						</div>

                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">{{__('property.exclusivity')}} </h3>

                                <ul class="list-inline two-part">
                                    <li>
                                        <div class="sparklinedash"></div>
                                    </li>
                                    <li class="text-right">
                                        <span class="text-success">

                                                @if(isset($ek_analysis_array[config('properties.status.exclusivity')]))
                                                {{number_format( $ek_analysis_array[config('properties.status.exclusivity')] , 0 ,",",".")}}€
                                                @else
                                                {{number_format( 0 , 0 ,",",".")}}€
                                                @endif

                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">{{__('property.certified')}}</h3>


                                <ul class="list-inline two-part">
                                    <li>
                                        <div class="sparklinedash"></div>
                                    </li>
                                    <li class="text-right">
                                        <span class="text-success">
                                            @if(isset($ek_analysis_array[config('properties.status.certified')]))
                            {{number_format( $ek_analysis_array[config('properties.status.certified')] , 0 ,",",".")}}€
                            @else
                            {{number_format( 0 , 0 ,",",".")}}€
                            @endif
                                            
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
						<div class="col-lg-4 col-sm-6 col-xs-12">
							<div class="white-box analytics-info">
								<h3 class="box-title">{{__('property.duration')}}</h3>
								<ul class="list-inline two-part">
									<li>
										<div class="sparklinedash"></div>
									</li>
									<li class="text-right">
                                        <span class="text-success">
                                            <?php
                                            $add_bestand = 0;
                                            if(isset($ek_analysis_array[6]))
                                                $add_bestand = $ek_analysis_array[6];

                                            if(isset($ek_analysis_array[config('properties.status.in_sale')]))
                                                $add_bestand +=$ek_analysis_array[config('properties.status.in_sale')];

                                            ?>
                                               EK {{number_format( $add_bestand , 0 ,",",".")}}€
                                            
                                        </span>
                                        <br>
										<span class="text-success">
                                              VK  {{number_format( $bestand_total , 0 ,",",".")}}€
                                                
                                                
										</span>
									</li>
								</ul>
							</div>
						</div>

						

						<div class="col-lg-4 col-sm-6 col-xs-12">
							<div class="white-box analytics-info">
								<h3 class="box-title">{{__('property.sold')}}</h3>
								<ul class="list-inline two-part">
									<li>
										<div class="sparklinedash"></div>
									</li>
									<li class="text-right">
										<span class="text-success">
                                            EK @if(isset($ek_analysis_array[config('properties.status.sold')]))
                                                {{number_format( $ek_analysis_array[config('properties.status.sold')] , 0 ,",",".")}}€
                                                @else
                                                {{number_format( 0 , 0 ,",",".")}}€
                                                @endif
										</span>
                                        <br>
                                        <span class="text-success">VK {{number_format( $vc_total , 0 ,",",".")}}€</span>
                                        
									</li>
								</ul>
							</div>
						</div>

						
					</div>
					<!--/.row -->

                <div class="table-responsive">
                    <h2>
						<label>{{__('dashboard.status')}}: All</label>
						<span class="totalamount">
							<?php


							$add_bestand = 0;
							if(isset($ek_analysis_array[6]))
								$add_bestand = $ek_analysis_array[6];

							if(isset($ek_analysis_array[config('properties.status.in_sale')]))
								$add_bestand +=$ek_analysis_array[config('properties.status.in_sale')];

												



							$ts = 0;
							foreach ($ek_analysis_array as $key => $value) {
								// if($key==6)
								//     $ts+=$bestand_total;
								// else
									$ts += $value;
							}
							?>
								{{number_format( $ts , 0 ,",",".")}}€
								
						</span>
						<a href="javascript:void(0)" data-status="0" class="btn btn-info test show-link">{{$total_count}} <i class="fa fa-angle-down"></i></a>
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table ">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>

									

									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($properties_all as $property)
									<?php
									$bank_ids = json_decode($property->bank_ids);
									if($bank_ids != null) {
										foreach ($banks as $b) {
											if($b->id == $bank_ids[0]){
												$bank = $b;
												break;
											}
										}
									}

									$D42 = $property->gesamt_in_eur
										+ ($property->real_estate_taxes * $property->gesamt_in_eur)
										+ ($property->estate_agents * $property->gesamt_in_eur)
										+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
										+ ($property->evaluation * $property->gesamt_in_eur)
										+ ($property->others * $property->gesamt_in_eur)
										+ ($property->buffer * $property->gesamt_in_eur);

									$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
									$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
									$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

									if(isset($bank)){
										$E18 = ($property->net_rent_increase_year1
												-$property->maintenance_increase_year1
												-$property->operating_cost_increase_year1
												-$property->property_management_increase_year1)
											-$property->depreciation_nk_money;

										$D49 = $bank->bank_loan * $D42;
										$D48 = $bank->from_bond * $D42;
										$H48 = $D49 * $bank->interest_bank_loan;
										$H49 = $D49 * $bank->eradication_bank;
										$H52 = $D48 * $bank->interest_bond;

										$E23 = $E18- $H48 -$H52;
										$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
										$G61 = ($D48 == 0) ? 0 : $E31/$D48;
									}

									if(!isset($G61)){
										$G61 = 0;
									}
									?>
									<tr>
										<td class="text-center"><br>{{$property->id}}</td>
										<td ><br>
											<a href="{{route('properties.show',['property'=>$property->id])}}">
											{{$property->name_of_property}}
											</a>
										</td>
										<td><br>{{$property->name_of_creator}}</td>
										<td class="number-right"><br>{{number_format($D42,0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
										<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
										<td class="number-right"><br>
											<?php
											//J59*(D42/J60)
											if($property->AHK_Salzgitter != 0){
												$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
															+ ($property->real_estate_taxes * $property->gesamt_in_eur)
															+ ($property->estate_agents * $property->gesamt_in_eur)
															+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
															+ ($property->evaluation * $property->gesamt_in_eur)
															+ ($property->others * $property->gesamt_in_eur)
															+ ($property->buffer * $property->gesamt_in_eur))
														/($property->AHK_Salzgitter)));
												if($guv_faktor != 0 && isset($bank)){
													echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
												}else echo "0";
											}else echo "0";
											?>%
										</td>
										<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
										<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
										<td>
										   <select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
												<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
												<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
												<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
												<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
												<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
												<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
												<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
												<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
												<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
												<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
												<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
												<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
											</select>
										</td>
										<td>


											{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
											'method' => 'DELETE']) !!}
											<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
													type="button"
													class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
														class="ti-eye"></i></button>

											{!! Form::close() !!}

										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
                
               

                <div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.in_purchase')}}</label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.in_purchase')]))
								{{number_format( $ek_analysis_array[config('properties.status.in_purchase')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
						</span>

						<!-- {{config('properties.status.in_purchase')}} -->
						<a href="javascript:void(0)" data-status="{{config('properties.status.in_purchase')}}" class="btn btn-info show-link">
							@if(isset($properties_count[config('properties.status.in_purchase')]))
								{{$properties_count[config('properties.status.in_purchase')]}}
							@else
								0
							@endif <i class="fa fa-angle-down"></i>
						</a> 
					</h2>
					<div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($properties_in_purchase as $property)
									<?php
									$bank_ids = json_decode($property->bank_ids);
									if($bank_ids != null) {
										foreach ($banks as $b) {
											if($b->id == $bank_ids[0]){
												$bank = $b;
												break;
											}
										}
									}

									$D42 = $property->gesamt_in_eur
										+ ($property->real_estate_taxes * $property->gesamt_in_eur)
										+ ($property->estate_agents * $property->gesamt_in_eur)
										+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
										+ ($property->evaluation * $property->gesamt_in_eur)
										+ ($property->others * $property->gesamt_in_eur)
										+ ($property->buffer * $property->gesamt_in_eur);

									$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
									$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
									$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

									if(isset($bank)){
										$E18 = ($property->net_rent_increase_year1
												-$property->maintenance_increase_year1
												-$property->operating_cost_increase_year1
												-$property->property_management_increase_year1)
											-$property->depreciation_nk_money;

										$D49 = $bank->bank_loan * $D42;
										$D48 = $bank->from_bond * $D42;
										$H48 = $D49 * $bank->interest_bank_loan;
										$H49 = $D49 * $bank->eradication_bank;
										$H52 = $D48 * $bank->interest_bond;

										$E23 = $E18- $H48 -$H52;
										$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
										$G61 = ($D48 == 0) ? 0 : $E31/$D48;
									}
									?>
									<tr>
									   <td class="text-center"><br>{{$property->id}}</td>
									   <td ><br>
											<a href="{{route('properties.show',['property'=>$property->id])}}">
											{{$property->name_of_property}}
											</a>
										</td>
										<td><br>@if(isset($property->transaction_manager))
											{{$property->transaction_manager->name}}
											@else
											{{$property->name_of_creator}}
											@endif</td>
										<td class="number-right"><br>{{number_format( $D42 , 0 ,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
										<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
										<td class="number-right"><br>
											<?php
											//J59*(D42/J60)
											if($property->AHK_Salzgitter != 0){
												$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
															+ ($property->real_estate_taxes * $property->gesamt_in_eur)
															+ ($property->estate_agents * $property->gesamt_in_eur)
															+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
															+ ($property->evaluation * $property->gesamt_in_eur)
															+ ($property->others * $property->gesamt_in_eur)
															+ ($property->buffer * $property->gesamt_in_eur))
														/($property->AHK_Salzgitter)));
												if($guv_faktor != 0 && isset($bank)){
													echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
												}else echo "0";
											}else echo "0";
											?>%
										</td>
										<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
										<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
										<td>
											<select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
												<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
												<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
												<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
												<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
												<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
												<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
												<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
												<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
												<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
												<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
												<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
												<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
											</select>
										</td>
										<td>


											{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
											'method' => 'DELETE']) !!}
											<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
													type="button"
													class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
														class="ti-eye"></i></button>


											{!! Form::close() !!}

										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
					</div>
				</div>

                    


				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.offer')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.offer')]))
								{{number_format( $ek_analysis_array[config('properties.status.offer')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.offer')}}" class="btn btn-info show-link">
							@if(isset($properties_count[config('properties.status.offer')]))
							{{$properties_count[config('properties.status.offer')]}}
							@else
							0
							@endif  <i class="fa fa-angle-down"></i>
						</a>                    
					</h2>
					<div class="property-table hidden">

						<table class="table table-striped dashboard-table ">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($properties_offer as $property)
									<?php
									$bank_ids = json_decode($property->bank_ids);
									if($bank_ids != null) {
										foreach ($banks as $b) {
											if($b->id == $bank_ids[0]){
												$bank = $b;
												break;
											}
										}
									}

									$D42 = $property->gesamt_in_eur
										+ ($property->real_estate_taxes * $property->gesamt_in_eur)
										+ ($property->estate_agents * $property->gesamt_in_eur)
										+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
										+ ($property->evaluation * $property->gesamt_in_eur)
										+ ($property->others * $property->gesamt_in_eur)
										+ ($property->buffer * $property->gesamt_in_eur);

									$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
									$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
									$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

									if(isset($bank)){
										$E18 = ($property->net_rent_increase_year1
												-$property->maintenance_increase_year1
												-$property->operating_cost_increase_year1
												-$property->property_management_increase_year1)
											-$property->depreciation_nk_money;

										$D49 = $bank->bank_loan * $D42;
										$D48 = $bank->from_bond * $D42;
										$H48 = $D49 * $bank->interest_bank_loan;
										$H49 = $D49 * $bank->eradication_bank;
										$H52 = $D48 * $bank->interest_bond;

										$E23 = $E18- $H48 -$H52;
										$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
										$G61 = ($D48 == 0) ? 0 : $E31/$D48;
									}

									if(!isset($G61)){
										$G61 = 0;
									}
									?>
									<tr>
										<td class="text-center"><br>{{$property->id}}</td>
										<td ><br>
											<a href="{{route('properties.show',['property'=>$property->id])}}">
											{{$property->name_of_property}}
											</a>
										</td>
										<td><br>
											@if(isset($property->transaction_manager))
											{{$property->transaction_manager->name}}
											@else
											{{$property->name_of_creator}}
											@endif
										</td>
										<td class="number-right"><br>{{number_format($D42,0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
										<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
										<td class="number-right"><br>
											<?php
											//J59*(D42/J60)
											if($property->AHK_Salzgitter != 0){
												$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
															+ ($property->real_estate_taxes * $property->gesamt_in_eur)
															+ ($property->estate_agents * $property->gesamt_in_eur)
															+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
															+ ($property->evaluation * $property->gesamt_in_eur)
															+ ($property->others * $property->gesamt_in_eur)
															+ ($property->buffer * $property->gesamt_in_eur))
														/($property->AHK_Salzgitter)));
												if($guv_faktor != 0 && isset($bank)){
													echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
												}else echo "0";
											}else echo "0";
											?>%
										</td>
										<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
										<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
										<td>
										   <select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
												<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
												<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
												<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
												<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
												<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
												<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
												<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
												<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
												<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
												<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
												<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
												<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
											</select>
										</td>
										<td>


											{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
											'method' => 'DELETE']) !!}
											<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
													type="button"
													class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
														class="ti-eye"></i></button>

											{!! Form::close() !!}

										</td>
									</tr>
								@endforeach
							</tbody>
						</table>

						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
					</div>
				</div>




				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.exclusivity')}} </label>

						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.exclusivity')]))
								{{number_format( $ek_analysis_array[config('properties.status.exclusivity')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
						</span>
							<a href="javascript:void(0)" data-status="{{config('properties.status.exclusivity')}}" class="btn btn-info show-link">@if(isset($properties_count[config('properties.status.exclusivity')]))
							{{$properties_count[config('properties.status.exclusivity')]}}
							@else
							0
							@endif <i class="fa fa-angle-down"></i>
						</a>
					</h2>
					<div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
							<tr>
								<th style="width: 70px;" class="text-center">#</th>
								<th>{{__('dashboard.property_name')}}</th>
								<th>{{__('dashboard.transaction_manager')}}</th>
								<th>{{__('dashboard.total_purchase_price')}}</th>
								<th>{{__('dashboard.gross_return')}}</th>
								<!--<th>{{__('dashboard.faktor')}}</th>-->
								<th>{{__('dashboard.guv_ref_factor')}}</th>
								<th>{{__('dashboard.ek_cf')}}</th>
								<th>{{__('dashboard.maklerpreis')}}</th>
								<th>{{__('dashboard.angebotspreis')}}</th>
								<th>{{__('dashboard.price_difference')}}</th>
								<th>{{__('property.listing.status')}}</th>
								<th>{{__('property.listing.actions')}}</th>
							</tr>
							</thead>
							<tbody>
							@foreach($properties_exclusivity as $property)
								<?php
								$bank_ids = json_decode($property->bank_ids);
								if($bank_ids != null) {
									foreach ($banks as $b) {
										if($b->id == $bank_ids[0]){
											$bank = $b;
											break;
										}
									}
								}

								$D42 = $property->gesamt_in_eur
									+ ($property->real_estate_taxes * $property->gesamt_in_eur)
									+ ($property->estate_agents * $property->gesamt_in_eur)
									+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
									+ ($property->evaluation * $property->gesamt_in_eur)
									+ ($property->others * $property->gesamt_in_eur)
									+ ($property->buffer * $property->gesamt_in_eur);

								$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
								$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
								$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

								if(isset($bank)){
									$E18 = ($property->net_rent_increase_year1
											-$property->maintenance_increase_year1
											-$property->operating_cost_increase_year1
											-$property->property_management_increase_year1)
										-$property->depreciation_nk_money;

									$D49 = $bank->bank_loan * $D42;
									$D48 = $bank->from_bond * $D42;
									$H48 = $D49 * $bank->interest_bank_loan;
									$H49 = $D49 * $bank->eradication_bank;
									$H52 = $D48 * $bank->interest_bond;

									$E23 = $E18- $H48 -$H52;
									$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
									$G61 = ($D48 == 0) ? 0 : $E31/$D48;
								}
								?>
								<tr>
									<td class="text-center"><br>{{$property->id}}</td>
									<td>
										<br>
										<a href="{{route('properties.show',['property'=>$property->id])}}">
										{{$property->name_of_property}}
										</a>
									</td>                                
									<td>
										<br>
										@if(isset($property->transaction_manager))
										{{$property->transaction_manager->name}}
										@else
										{{$property->name_of_creator}}
										@endif
									</td>
									<td class="number-right"><br>{{number_format( $D42 , 0 ,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
									<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
									<td class="number-right"><br>
										<?php
										//J59*(D42/J60)
										if($property->AHK_Salzgitter != 0){
											$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
														+ ($property->real_estate_taxes * $property->gesamt_in_eur)
														+ ($property->estate_agents * $property->gesamt_in_eur)
														+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
														+ ($property->evaluation * $property->gesamt_in_eur)
														+ ($property->others * $property->gesamt_in_eur)
														+ ($property->buffer * $property->gesamt_in_eur))
													/($property->AHK_Salzgitter)));
											if($guv_faktor != 0 && isset($bank)){
												echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
											}else echo "0";
										}else echo "0";
										?>%
									</td>
									<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
									<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
									<td>
										<select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
											<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
											<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
											<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
											<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
											<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
											<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
											<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
											<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
											<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
											<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
											<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
											<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
										</select>
									</td>
									<td>

										{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
										'method' => 'DELETE']) !!}
										<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
												type="button"
												class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
													class="ti-eye"></i></button>

										{!! Form::close() !!}

									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>


 

				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.certified')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.certified')]))
								{{number_format( $ek_analysis_array[config('properties.status.certified')] , 0 ,",",".")}}€
							@else
								{{number_format( 0 , 0 ,",",".")}}€
							@endif
						</span>
							<a href="javascript:void(0)" data-status="{{config('properties.status.certified')}}" class="btn btn-info show-link">@if(isset($properties_count[config('properties.status.certified')]))
								{{$properties_count[config('properties.status.certified')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>
					</h2>
					<div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($properties_certified as $property)
									<?php
									$bank_ids = json_decode($property->bank_ids);
									if($bank_ids != null) {
										foreach ($banks as $b) {
											if($b->id == $bank_ids[0]){
												$bank = $b;
												break;
											}
										}
									}

									$D42 = $property->gesamt_in_eur
										+ ($property->real_estate_taxes * $property->gesamt_in_eur)
										+ ($property->estate_agents * $property->gesamt_in_eur)
										+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
										+ ($property->evaluation * $property->gesamt_in_eur)
										+ ($property->others * $property->gesamt_in_eur)
										+ ($property->buffer * $property->gesamt_in_eur);

									$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
									$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
									$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

									if(isset($bank)){
										$E18 = ($property->net_rent_increase_year1
												-$property->maintenance_increase_year1
												-$property->operating_cost_increase_year1
												-$property->property_management_increase_year1)
											-$property->depreciation_nk_money;

										$D49 = $bank->bank_loan * $D42;
										$D48 = $bank->from_bond * $D42;
										$H48 = $D49 * $bank->interest_bank_loan;
										$H49 = $D49 * $bank->eradication_bank;
										$H52 = $D48 * $bank->interest_bond;

										$E23 = $E18- $H48 -$H52;
										$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
										$G61 = ($D48 == 0) ? 0 : $E31/$D48;
									}
									?>
									<tr>
										<td class="text-center"><br>{{$property->id}}</td>
										<td ><br>
											<a href="{{route('properties.show',['property'=>$property->id])}}">
											{{$property->name_of_property}}
											</a>
										</td>                                
										<td><br>@if(isset($property->transaction_manager))
											{{$property->transaction_manager->name}}
											@else
											{{$property->name_of_creator}}
											@endif</td>
										<td class="number-right"><br>{{number_format( $D42 , 0 ,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
										<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
										<td class="number-right"><br>
											<?php
											//J59*(D42/J60)
											if($property->AHK_Salzgitter != 0){
												$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
															+ ($property->real_estate_taxes * $property->gesamt_in_eur)
															+ ($property->estate_agents * $property->gesamt_in_eur)
															+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
															+ ($property->evaluation * $property->gesamt_in_eur)
															+ ($property->others * $property->gesamt_in_eur)
															+ ($property->buffer * $property->gesamt_in_eur))
														/($property->AHK_Salzgitter)));
												if($guv_faktor != 0 && isset($bank)){
													echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
												}else echo "0";
											}else echo "0";
											?>%
										</td>
										<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
										<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
										<td>
											<select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
												<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
												<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
												<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
												<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
												<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
												<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
												<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
												<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
												<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
												<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
												<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
												<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
											</select>
										</td>
										<td>

											{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
											'method' => 'DELETE']) !!}
											<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
													type="button"
													class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
														class="ti-eye"></i></button>

											{!! Form::close() !!}

										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>



				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.duration')}} </label>
						<span class="totalamount">
							{{number_format($add_bestand , 0 ,",",".")}}€
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.duration')}}" class="btn btn-info show-link">				 @if(isset($properties_count[config('properties.status.duration')]))
							{{$properties_count[config('properties.status.duration')]}}
						@else
							0
						@endif 
						<i class="fa fa-angle-down"></i>
						</a>                   
					</h2>
					<h2 style="float: left;width: 220px;text-align: right;">VK {{number_format($bestand_total , 0 ,",",".")}}€</h2>
					<div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.asset_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($properties_duration as $property)
								<?php
									$bank_ids = json_decode($property->bank_ids);
									if($bank_ids != null) {
										foreach ($banks as $b) {
											if($b->id == $bank_ids[0]){
												$bank = $b;
												break;
											}
										}
									}

									$D42 = $property->gesamt_in_eur
									+ ($property->real_estate_taxes * $property->gesamt_in_eur)
									+ ($property->estate_agents * $property->gesamt_in_eur)
									+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
									+ ($property->evaluation * $property->gesamt_in_eur)
									+ ($property->others * $property->gesamt_in_eur)
									+ ($property->buffer * $property->gesamt_in_eur);

									$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
									$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
									$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

									if(isset($bank)){
										$E18 = ($property->net_rent_increase_year1
										-$property->maintenance_increase_year1
										-$property->operating_cost_increase_year1
										-$property->property_management_increase_year1)
										-$property->depreciation_nk_money;

										$D49 = $bank->bank_loan * $D42;
										$D48 = $bank->from_bond * $D42;
										$H48 = $D49 * $bank->interest_bank_loan;
										$H49 = $D49 * $bank->eradication_bank;
										$H52 = $D48 * $bank->interest_bond;

										$E23 = $E18- $H48 -$H52;
										$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
										$G61 = ($D48 == 0) ? 0 : $E31/$D48;
									}
								?>
									<tr>
										 <td class="text-center"><br>{{$property->id}}</td>
										 <td ><br>
											<a href="{{route('properties.show',['property'=>$property->id])}}">
											{{$property->name_of_property}}
											</a>
										</td>                                
										<td><br>
										@if(isset($property->asset_manager))
										{{$property->asset_manager->name}}
										@else
										{{$property->name_of_creator}}
										@endif
										</td>
										<td class="number-right"><br>{{number_format( $D42 , 0 ,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
										<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
										<td class="number-right"><br>
										<?php
										//J59*(D42/J60)
											if($property->AHK_Salzgitter != 0){
												$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
															+ ($property->real_estate_taxes * $property->gesamt_in_eur)
															+ ($property->estate_agents * $property->gesamt_in_eur)
															+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
															+ ($property->evaluation * $property->gesamt_in_eur)
															+ ($property->others * $property->gesamt_in_eur)
															+ ($property->buffer * $property->gesamt_in_eur))
														/($property->AHK_Salzgitter)));
												if($guv_faktor != 0 && isset($bank)){
													echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
												}else echo "0";
											}else echo "0";
										?>%
										</td>
										<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
										<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
										<td>
											<select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
												<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
												<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
												<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
												<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
												<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
												<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
												<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
												<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
												<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
												<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
												<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
												<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
											</select>
										</td>
										<td>


											{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
											'method' => 'DELETE']) !!}
											<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
													type="button"
													class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
														class="ti-eye"></i></button>

											{!! Form::close() !!}

										</td>
									</tr>
								@endforeach
								<!---------->
							</tbody>
						</table>
					</div>
				</div>


                <div class="table-responsive hidden">
                    <h2>
						<label>{{__('dashboard.status')}}: {{__('property.in_sale')}} </label>
                        <span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.in_sale')]))
								{{number_format( $ek_analysis_array[config('properties.status.in_sale')] , 0 ,",",".")}}€
                            @else
								{{number_format( 0 , 0 ,",",".")}}€
                            @endif
						</span>
                        <a href="javascript:void(0)" data-status="{{config('properties.status.in_sale')}}" class="btn btn-info show-link">
							@if(isset($properties_count[config('properties.status.in_sale')]))
								{{$properties_count[config('properties.status.in_sale')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
							<tr>
								<th style="width: 70px;" class="text-center">#</th>
								<th>{{__('dashboard.property_name')}}</th>
								<th>{{__('dashboard.seller')}}</th>
								<th>{{__('dashboard.total_purchase_price')}}</th>
								<th>{{__('dashboard.gross_return')}}</th>
								<th>{{__('dashboard.faktor')}}</th>
								<th>{{__('dashboard.guv_ref_factor')}}</th>
								<th>{{__('dashboard.ek_cf')}}</th>
								<th>{{__('dashboard.maklerpreis')}}</th>
								<th>{{__('dashboard.angebotspreis')}}</th>
								<th>{{__('dashboard.price_difference')}}</th>
								<th>{{__('property.listing.status')}}</th>
								<th>{{__('property.listing.actions')}}</th>
							</tr>
							</thead>
							<tbody>
								@foreach($properties_in_sale as $property)
									<?php
									$bank_ids = json_decode($property->bank_ids);
									if($bank_ids != null) {
										foreach ($banks as $b) {
											if($b->id == $bank_ids[0]){
												$bank = $b;
												break;
											}
										}
									}

									$D42 = $property->gesamt_in_eur
										+ ($property->real_estate_taxes * $property->gesamt_in_eur)
										+ ($property->estate_agents * $property->gesamt_in_eur)
										+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
										+ ($property->evaluation * $property->gesamt_in_eur)
										+ ($property->others * $property->gesamt_in_eur)
										+ ($property->buffer * $property->gesamt_in_eur);

									$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
									$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
									$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

									if(isset($bank)){
										$E18 = ($property->net_rent_increase_year1
												-$property->maintenance_increase_year1
												-$property->operating_cost_increase_year1
												-$property->property_management_increase_year1)
											-$property->depreciation_nk_money;

										$D49 = $bank->bank_loan * $D42;
										$D48 = $bank->from_bond * $D42;
										$H48 = $D49 * $bank->interest_bank_loan;
										$H49 = $D49 * $bank->eradication_bank;
										$H52 = $D48 * $bank->interest_bond;

										$E23 = $E18- $H48 -$H52;
										$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
										$G61 = ($D48 == 0) ? 0 : $E31/$D48;
									}
									?>
									<tr>
										<td class="text-center"><br>{{$property->id}}</td>
										<td ><br>
											<a href="{{route('properties.show',['property'=>$property->id])}}">
											{{$property->name_of_property}}
											</a>
										</td>
										<td><br>@if(isset($property->seller))
											{{$property->seller->name}}
											@else
											{{$property->name_of_creator}}
											@endif</td>
										<td class="number-right"><br>{{number_format( $D42 , 0 ,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
										<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
										<td class="number-right"><br>
											<?php
											//J59*(D42/J60)
											if($property->AHK_Salzgitter != 0){
												$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
															+ ($property->real_estate_taxes * $property->gesamt_in_eur)
															+ ($property->estate_agents * $property->gesamt_in_eur)
															+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
															+ ($property->evaluation * $property->gesamt_in_eur)
															+ ($property->others * $property->gesamt_in_eur)
															+ ($property->buffer * $property->gesamt_in_eur))
														/($property->AHK_Salzgitter)));
												if($guv_faktor != 0 && isset($bank)){
													echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
												}else echo "0";
											}else echo "0";
											?>%
										</td>
										<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
										<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
										<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
										<td>
											<select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
												<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
												<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
												<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
												<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
												<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
												<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
												<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
												<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
												<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
												<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
												<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
												<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
											</select>
										</td>
										<td>

											{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
											'method' => 'DELETE']) !!}
											<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
													type="button"
													class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
														class="ti-eye"></i></button>

											{!! Form::close() !!}

										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
                </div>

                <div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.sold')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.sold')]))
								{{number_format( $ek_analysis_array[config('properties.status.sold')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.sold')}}" class="btn btn-info show-link">	
							@if(isset($properties_count[config('properties.status.sold')]))
								{{$properties_count[config('properties.status.sold')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>
                    </h2>
                    <h2 style="float: left;width: 220px;text-align: right;">VK {{number_format($vc_total , 0 ,",",".")}}€</h2> 
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
							<tr>
								<th style="width: 70px;" class="text-center">#</th>
								<th>{{__('dashboard.property_name')}}</th>
								<th>{{__('dashboard.seller')}}</th>
								<th>{{__('dashboard.total_purchase_price')}}</th>
								<th>{{__('dashboard.gross_return')}}</th>
								<!--<th>{{__('dashboard.faktor')}}</th>-->
								<th>{{__('dashboard.guv_ref_factor')}}</th>
								<th>{{__('dashboard.ek_cf')}}</th>
								<th>{{__('dashboard.maklerpreis')}}</th>
								<th>{{__('dashboard.angebotspreis')}}</th>
								<th>{{__('dashboard.price_difference')}}</th>
								<th>{{__('property.listing.status')}}</th>
								<th>{{__('property.listing.actions')}}</th>
							</tr>
							</thead>
							<tbody>
							@foreach($properties_sold as $property)
								<?php
								$bank_ids = json_decode($property->bank_ids);
								if($bank_ids != null) {
									foreach ($banks as $b) {
										if($b->id == $bank_ids[0]){
											$bank = $b;
											break;
										}
									}
								}

								$D42 = $property->gesamt_in_eur
									+ ($property->real_estate_taxes * $property->gesamt_in_eur)
									+ ($property->estate_agents * $property->gesamt_in_eur)
									+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
									+ ($property->evaluation * $property->gesamt_in_eur)
									+ ($property->others * $property->gesamt_in_eur)
									+ ($property->buffer * $property->gesamt_in_eur);

								$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
								$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
								$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

								if(isset($bank)){
									$E18 = ($property->net_rent_increase_year1
											-$property->maintenance_increase_year1
											-$property->operating_cost_increase_year1
											-$property->property_management_increase_year1)
										-$property->depreciation_nk_money;

									$D49 = $bank->bank_loan * $D42;
									$D48 = $bank->from_bond * $D42;
									$H48 = $D49 * $bank->interest_bank_loan;
									$H49 = $D49 * $bank->eradication_bank;
									$H52 = $D48 * $bank->interest_bond;

									$E23 = $E18- $H48 -$H52;
									$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
									$G61 = ($D48 == 0) ? 0 : $E31/$D48;
								}
								?>
								<tr>
									<td class="text-center"><br>{{$property->id}}</td>
									<td ><br>
										<a href="{{route('properties.show',['property'=>$property->id])}}">
										{{$property->name_of_property}}
										</a>
									</td>                                
									<td><br>@if(isset($property->seller))
										{{$property->seller->name}}
										@else
										{{$property->name_of_creator}}
										@endif</td>
									<td class="number-right"><br>{{number_format( $D42 , 0 ,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
									<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
									<td class="number-right"><br>
										<?php
										//J59*(D42/J60)
										if($property->AHK_Salzgitter != 0){
											$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
														+ ($property->real_estate_taxes * $property->gesamt_in_eur)
														+ ($property->estate_agents * $property->gesamt_in_eur)
														+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
														+ ($property->evaluation * $property->gesamt_in_eur)
														+ ($property->others * $property->gesamt_in_eur)
														+ ($property->buffer * $property->gesamt_in_eur))
													/($property->AHK_Salzgitter)));
											if($guv_faktor != 0 && isset($bank)){
												echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
											}else echo "0";
										}else echo "0";
										?>%
									</td>
									<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
									<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
									<td>
										<select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
											<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
											<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
											<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
											<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
											<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
											<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
											<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
											<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
											<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
											<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
											<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
											<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
										</select>
									</td>
									<td>


										{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
										'method' => 'DELETE']) !!}
										<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
												type="button"
												class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
													class="ti-eye"></i></button>


										{!! Form::close() !!}

									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
					</div>
                </div>

                <div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.declined')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.declined')]))
								{{number_format( $ek_analysis_array[config('properties.status.declined')] , 0 ,",",".")}}€
							@else
								{{number_format( 0 , 0 ,",",".")}}€
							@endif
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.declined')}}" class="btn btn-info show-link">
							@if(isset($properties_count[config('properties.status.declined')]))
								{{$properties_count[config('properties.status.declined')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
							@foreach($properties_declined as $property)
								<?php
								$bank_ids = json_decode($property->bank_ids);
								if($bank_ids != null) {
									foreach ($banks as $b) {
										if($b->id == $bank_ids[0]){
											$bank = $b;
											break;
										}
									}
								}

								$D42 = $property->gesamt_in_eur
									+ ($property->real_estate_taxes * $property->gesamt_in_eur)
									+ ($property->estate_agents * $property->gesamt_in_eur)
									+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
									+ ($property->evaluation * $property->gesamt_in_eur)
									+ ($property->others * $property->gesamt_in_eur)
									+ ($property->buffer * $property->gesamt_in_eur);

								$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
								$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
								$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

								if(isset($bank)){
									$E18 = ($property->net_rent_increase_year1
											-$property->maintenance_increase_year1
											-$property->operating_cost_increase_year1
											-$property->property_management_increase_year1)
										-$property->depreciation_nk_money;

									$D49 = $bank->bank_loan * $D42;
									$D48 = $bank->from_bond * $D42;
									$H48 = $D49 * $bank->interest_bank_loan;
									$H49 = $D49 * $bank->eradication_bank;
									$H52 = $D48 * $bank->interest_bond;

									$E23 = $E18- $H48 -$H52;
									$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
									$G61 = ($D48 == 0) ? 0 : $E31/$D48;
								}
								?>
								<tr>
									<td class="text-center"><br>{{$property->id}}</td>
									<td ><br>
										<a href="{{route('properties.show',['property'=>$property->id])}}">
										{{$property->name_of_property}}
										</a>
									</td>                                
									<td><br>@if(isset($property->transaction_manager))
										{{$property->transaction_manager->name}}
										@else
										{{$property->name_of_creator}}
										@endif</td>
									<td class="number-right"><br>{{number_format($D42,0,",",".")}}</td>
									<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
									<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
									<td class="number-right"><br>
										<?php
										//J59*(D42/J60)
										if($property->AHK_Salzgitter != 0){
											$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
														+ ($property->real_estate_taxes * $property->gesamt_in_eur)
														+ ($property->estate_agents * $property->gesamt_in_eur)
														+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
														+ ($property->evaluation * $property->gesamt_in_eur)
														+ ($property->others * $property->gesamt_in_eur)
														+ ($property->buffer * $property->gesamt_in_eur))
													/($property->AHK_Salzgitter)));
											if($guv_faktor != 0 && isset($bank)){
												echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
											}else echo "0";
										}else echo "0";
										?>%
									</td>
									<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
									<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
									<td>
										<select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
											<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
											<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
											<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
											<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
											<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
											<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
											<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
											<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
											<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
											<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
											<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
											<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
										</select>
									</td>
									<td>
										{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
										'method' => 'DELETE']) !!}
										<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
												type="button"
												class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
													class="ti-eye"></i></button>

										{!! Form::close() !!}

									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
                    </div>
				</div>

                <div class="table-responsive here">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.lost')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.lost')]))
								{{number_format( $ek_analysis_array[config('properties.status.lost')] , 0 ,",",".")}}€
							@else
								{{number_format( 0 , 0 ,",",".")}}€
							@endif
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.lost')}}" class="btn btn-info show-link">	
							@if(isset($properties_count[config('properties.status.lost')]))
								{{$properties_count[config('properties.status.lost')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>         
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($properties_lost as $property)
									<?php
									$bank_ids = json_decode($property->bank_ids);
									if($bank_ids != null) {
										foreach ($banks as $b) {
											if($b->id == $bank_ids[0]){
												$bank = $b;
												break;
											}
										}
									}

									$D42 = $property->gesamt_in_eur
										+ ($property->real_estate_taxes * $property->gesamt_in_eur)
										+ ($property->estate_agents * $property->gesamt_in_eur)
										+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
										+ ($property->evaluation * $property->gesamt_in_eur)
										+ ($property->others * $property->gesamt_in_eur)
										+ ($property->buffer * $property->gesamt_in_eur);

									$G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
									$L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
									$price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

									if(isset($bank)){
										$E18 = ($property->net_rent_increase_year1
												-$property->maintenance_increase_year1
												-$property->operating_cost_increase_year1
												-$property->property_management_increase_year1)
											-$property->depreciation_nk_money;

										$D49 = $bank->bank_loan * $D42;
										$D48 = $bank->from_bond * $D42;
										$H48 = $D49 * $bank->interest_bank_loan;
										$H49 = $D49 * $bank->eradication_bank;
										$H52 = $D48 * $bank->interest_bond;

										$E23 = $E18- $H48 -$H52;
										$E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
										$G61 = ($D48 == 0) ? 0 : $E31/$D48;
									}
									?>
								<tr>
									<td class="text-center"><br>{{$property->id}}</td>
									<td>
										<br>
										<a href="{{route('properties.show',['property'=>$property->id])}}">
										{{$property->name_of_property}}
										</a>
									</td>                              
									<td>
										<br>
										@if(isset($property->transaction_manager))
											{{$property->transaction_manager->name}}
										@else
											{{$property->name_of_creator}}
										@endif
									</td>
									<td class="number-right"><br>{{number_format( $D42 , 0 ,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
									<!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
									<td class="number-right"><br>
										<?php
										//J59*(D42/J60)
										if($property->AHK_Salzgitter != 0){
											$guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
														+ ($property->real_estate_taxes * $property->gesamt_in_eur)
														+ ($property->estate_agents * $property->gesamt_in_eur)
														+ (($property->Grundbuch * $property->gesamt_in_eur)/100)
														+ ($property->evaluation * $property->gesamt_in_eur)
														+ ($property->others * $property->gesamt_in_eur)
														+ ($property->buffer * $property->gesamt_in_eur))
													/($property->AHK_Salzgitter)));
											if($guv_faktor != 0 && isset($bank)){
												echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
											}else echo "0";
										}else echo "0";
										?>%
									</td>
									<td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
									<td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
									<td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
									<td>
										<select class="form-control property-status" data-property-id="{{$property->id}}" data-notification-type="{{config('notification.type.change_property_status')}}">
											<option value="{{config('properties.status.buy')}}" @if($property->status == config('properties.status.buy')) {!!"selected" !!} @endif>{{__('property.buy')}}</option>
											<option value="{{config('properties.status.hold')}}" @if($property->status == config('properties.status.hold')) {!!"selected" !!} @endif>{{__('property.hold')}}</option>
											<option value="{{config('properties.status.decline')}}" @if($property->status == config('properties.status.decline')) {!!"selected" !!} @endif>{{__('property.decline')}}</option>
											<option value="{{config('properties.status.declined')}}" @if($property->status == config('properties.status.declined')) {!!"selected" !!} @endif>{{__('property.declined')}}</option>
											<option value="{{config('properties.status.offer')}}" @if($property->status == config('properties.status.offer')) {!!"selected" !!} @endif>{{__('property.offer')}}</option>
											<option value="{{config('properties.status.duration')}}" @if($property->status == config('properties.status.duration')) {!!"selected" !!} @endif>{{__('property.duration')}}</option>
											<option value="{{config('properties.status.in_purchase')}}" @if($property->status == config('properties.status.in_purchase')) {!!"selected" !!} @endif>{{__('property.in_purchase')}}</option>
											<option value="{{config('properties.status.sold')}}" @if($property->status == config('properties.status.sold')) {!!"selected" !!} @endif>{{__('property.sold')}}</option>
											<option value="{{config('properties.status.lost')}}" @if($property->status == config('properties.status.lost')) {!!"selected" !!} @endif>{{__('property.lost')}}</option>
											<option value="{{config('properties.status.exclusivity')}}" @if($property->status == config('properties.status.exclusivity')) {!!"selected" !!} @endif>{{__('property.exclusivity')}}</option>
											<option value="{{config('properties.status.in_sale')}}" @if($property->status == config('properties.status.in_sale')) {!!"selected" !!} @endif>{{__('property.in_sale')}}</option>
											<option value="{{config('properties.status.certified')}}" @if($property->status == config('properties.status.certified')) {!!"selected" !!} @endif>{{__('property.certified')}}</option>
										</select>
									</td>
									<td>


										{!! Form::open(['action' => ['PropertiesController@destroy', $property->id],
										'method' => 'DELETE']) !!}
										<button onclick="location.href='{{route('properties.show',['property'=>$property->id])}}'"
												type="button"
												class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
													class="ti-eye"></i></button>
									   
										{!! Form::close() !!}

									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

	<div class="modal fade" id="Modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Choose a Bank</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<label>{{__('property.select_a_bank')}}: </label>
					<select id="bank">
						@foreach ($banks as $bank)
							<option value="{{$bank->id}}" data-with_real_ek="{{$bank->with_real_ek}}" data-from_bond="{{$bank->from_bond}}"
								data-bank_loan="{{$bank->bank_loan}}" data-interest_bank_loan="{{$bank->interest_bank_loan}}" 
								data-eradication_bank="{{$bank->eradication_bank}}" data-interest_bond="{{$bank->interest_bond}}"
								>{{$bank->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="modal-footer">
					<button id="modal_save_btn" type="button" class="btn btn-primary">{{__('property.save_changes')}}</button>
				</div>
			</div>
		</div>
	</div>


@endsection

@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

<script>
	jQuery.extend( jQuery.fn.dataTableExt.oSort, {
		"formatted-num-pre": function ( a ) {
			a = a.replace(/<(?:.|\n)*?>/gm, '');
			a = (a === "-" || a === "") ? 0 : a.replace(/\./g, '');
			return parseFloat( a );
		},
	 
		"formatted-num-asc": function ( a, b ) {
			return a - b;
		},
	 
		"formatted-num-desc": function ( a, b ) {
			return b - a;
		},
		
		"formatted-inline-pre": function ( a ) {
			a = a.replace(/<(?:.|\n)*?>/gm, '');
			a = (a === "-" || a === "") ? 0 : a.replace(/\./g, '');
			console.log(a);
			return parseFloat( a );
		},
	 
		"formatted-inline-asc": function ( a, b ) {
			return a - b;
		},
	 
		"formatted-inline-desc": function ( a, b ) {
			return b - a;
		}
		
	} );
	
	$(document).ready(function () 
	{
		$('.show-link').click(function()
		{
			if($(this).closest('.table-responsive').find('.property-table').hasClass('hidden'))
			{
				$(this).find('i').removeClass('fa-angle-down');
				$(this).find('i').addClass('fa-angle-up');
				$(this).closest('.table-responsive').find('.property-table').removeClass('hidden');

				// $(this).attr(id);
				$this = $(this);
				status = $(this).attr('data-status');
				var url = "{{route('getstatustable')}}?status="+status;
				$.ajax({
						url: url,
					}).done(function (data) 
					{ 
						console.log(data);
						$this.closest('.table-responsive').find('.property-table').html(data);
						if(status=="12")
						{
							$this.closest('.table-responsive').find('.dashboard-table').DataTable({
								"columns": [
									null,
									null,
									null,
									null,
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									//{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
								]
							});
						}
						else if(status=="6" || status=="11" || status=="8")
						{
							$this.closest('.table-responsive').find('.dashboard-table').DataTable({
								"columns": [
									null,
									null,
									null,
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									//{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
								]
							});
						}
						else
						{
							$this.closest('.table-responsive').find('.dashboard-table').DataTable({
								"columns": [
									null,
									null,
									null,
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									//{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
									{ "type": "formatted-num" },
								]
							});
						}	
					})
			}
			else
			{
				// $(this).html('<i class="fa fa-angle-down"></i>');
				$(this).find('i').addClass('fa-angle-down');
				$(this).find('i').removeClass('fa-angle-up');
				$(this).closest('.table-responsive').find('.property-table').addClass('hidden');
				// $(this).next().addClass('hidden');
			}
		})
		
		$('.table-responsive').find('a.inline-edit').editable({
			step: 'any',
			success: function(response, newValue) {
				if( response.success === false )
					return response.msg;
				else{
					 location.reload();
				}
			}
		});

		$('.dashboard-table').DataTable({
			 "columns": [
				null,
				null,
				null,
				{ "type": "formatted-num" },
				{ "type": "formatted-num" },
				//{ "type": "formatted-num" },
				{ "type": "formatted-num" },
				{ "type": "formatted-num" },
				{ "type": "formatted-num" },
				{ "type": "formatted-num" },
				{ "type": "formatted-num" },
				{ "type": "formatted-num" },
				{ "type": "formatted-num" },
			]
		});
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	
		var propertyId;
		$('body').on('change','.property-status', function () 
		{
			if($(this).val()=="1"){
				$('#Modal').modal('show');
			}
			selection = $('.property-status');
			selection.prop( "disabled", true );
			propertyId = $(this).data('property-id');
			var status = $(this).val();
			var notificationType = $(this).data('notification-type');
//                console.log("Status: " + propertyId);
			var data = {
				_token : '<?php echo csrf_token() ?>',
				status : status,
				property_id : propertyId
			};
			$.ajax({
				type:'POST',
				url:'{{route('properties.change_status')}}',
				data: data,
				success:function(data){
					newNotification(propertyId, 3);
					// location.reload();
				}
			});
		});
		$('#modal_save_btn').on('click', function () 
		{
			$.ajax({
				method: "POST",
				url: "{{url('/property/select_bank')}}" + "/" + propertyId,
				data: { 
					with_real_ek: $('#bank option:selected').data("with_real_ek"),
					from_bond: $('#bank option:selected').data("from_bond"),
					bank_loan: $('#bank option:selected').data("bank_loan"),
					interest_bank_loan: $('#bank option:selected').data("interest_bank_loan"),
					eradication_bank: $('#bank option:selected').data("eradication_bank"),
					interest_bond: $('#bank option:selected').data("interest_bond"),
					bank: $('#bank option:selected').val()
					}
				})
				.success(function( response, newValue) {
				if( response.success === false )
					return response.msg;
				else
				   $('#Modal').modal('hide');
				console.log(propertyId);
			});
		})
	});
    </script>
@endsection