@extends('layouts.guest')
@section('content')

<style type="text/css">
  .btn,
  .btn-primary,
  .btn-primary:hover,
  .btn:hover,
  .btn:active,
  .btn:focus,
  .btn:visited,
  .navbar-header {
    background: #004f91 !important;
    color: #fff !important;
  }
  @media only screen and (min-width: 1200px) {
  .address-div .col-md-3{
    width: 20%;
    float: left;
    }
  }
  .card-title{
    font-weight: bold;
  }
  .col-md-3 label{
    font-weight: normal;
  }
  .col-md-3{
    margin-top: 10px;
  }
  #main-wrapper{
    font-family: PT Sans,Helvetica,Arial,Lucida,sans-serif !important;
  }
  .form-control{
    box-shadow: none;
  }
  .select2-container{
    min-width: 150px;
  }
</style>

<div id="main-wrapper" data-layout="horizontal" data-navbarbg="skin6" data-sidebartype="full" data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
  <div class="page-wrapper">
    <div class="page-content container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <!-- Row -->
      <div class="row">
        <div class="col-12">

          @if(session('success'))
              <div class="alert alert-success">
                  {!! session('success') !!}
              </div>
          @endif

          @if(session('error'))
              <div class="alert alert-danger">
                  {!! session('error') !!}
              </div>
          @endif


          <div class="card">
                
            <form action="{{route('guestcreatepropertiespost')}}" method="post" class="form-horizontal r-separator" accept-charset="UTF-8" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="card-body address-div">
                <input type="hidden" name="ghghghghghg">
                <h4 class="card-title">Allgemeine Informationen*</h4>


                <div class="col-md-3">
                  <label class="">Objektname:</label>
                  <input type="text" class="form-control" id="name_of_property" value="{{ (old('name_of_property')) ? old('name_of_property') : '' }}" name="name_of_property" required>
                </div>

                <div class="col-md-3">
                  <label class="">Objekttyp:</label>
                  <?php
                     $a = array('shopping_mall'=>'Einkaufszentrum', 'retail_center'=>'Fachmarktzentrum','specialists'=>'Fachmarkt', 'retail_shop'=>'Einzelhandel','office'=>'Büro','logistik'=>'Logistik','commercial_space'=>'Kommerzielle Fläche','land'=>'Grundstück','apartment'=>'Wohnung','condos'=>'Eigentumswohnung','house'=>'Haus','living_and_business'=>'Wohn- und Geschäftshaus','villa'=>'Villa','hotel'=>'Hotel','nursing_home'=>'Pflegeheim','other'=>'Sonstiges','multifamily_house'=>'Mehrfamilienhaus');
                     
                     ?>
                  <select class="form-control" name="property_type" required>
                    <option value="">Objekttyp</option>
                    @foreach($a as $k=>$list)
                      <option value="{{ $k }}" {{ (old('property_type') && old('property_type') == $k) ? 'selected' : '' }} >{{ $list }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="col-md-3">
                  <label  style="">Netto Miete (IST) p.a. in €:</label>
                  <input type="text" class="form-control mask-number-input" id="net_rent_pa" name="net_rent_pa" value="{{ (old('net_rent_pa')) ? old('net_rent_pa') : '' }}" required>
                </div>

                <div class="col-md-3">
                  <label>Vermietbare Fläche in m²:</label>
                  <input type="text" class="form-control mask-number-input" id="Rental_area_in_m2" name="Rental_area_in_m2" value="{{ (old('Rental_area_in_m2')) ? old('Rental_area_in_m2') : '' }}" required>
                </div>

                <div class="col-md-3">
                  <label  style="">Grundstücksfläche in m²:</label>
                  <input type="text" class="form-control mask-number-input" id="land_area" name="land_area" value="{{ (old('land_area')) ? old('land_area') : '' }}" required>
                </div>

                <div class="col-md-3">
                    <label  style="font-size: 13px;">Kaufpreis (Verhandlungsbasis) in €:</label>
                    <input type="text" class="form-control mask-number-input" id="maklerpreis" name="maklerpreis" value="{{ (old('maklerpreis')) ? old('maklerpreis') : '' }}" required>
                </div>

              </div>

              <div class="clearfix"></div>

              <div class="card-body address-div" >

                <hr class="mt-0">
                <h4 class="card-title">Adresse der Immobilie*</h4>
                    
                <div class="col-md-3">
                  <label>PLZ:</label>
                  <input type="text" class="form-control" id="plz_ort"name="plz_ort" value="{{ (old('plz_ort')) ? old('plz_ort') : '' }}" required>
                </div>

                <div class="col-md-3">
                  <label class="">Ort:</label><br>
                  <select class="form-control city-search" name="ort" style="width: 70%" required>
                  </select>
                  <a href="javascript:void(0)" data-toggle="modal" data-target="#add-city" class="btn-sm btn-primary add_ort">neu</a>
                </div>

                <div class="col-md-3">
                    <label class="">Bundesland:<br> </label>
                    <?php
                       $stats = array('Baden-Württemberg',
                           'Bayern',
                           'Berlin',
                           'Brandenburg',
                           'Bremen',
                           'Hamburg',
                           'Hessen',
                           'Mecklenburg-Vorpommern',
                           'Niedersachsen',
                           'Nordrhein-Westfalen',
                           'Rheinland-Pfalz',
                           'Saarland',
                           'Sachsen',
                           'Sachsen-Anhalt',
                           'Schleswig-Holstein',
                           'Thüringen',
                           'Österreich','Spanien');
                       $n_array =     array('Baden-Württemberg'=>'5,0',
                           'Bayern'=>'3,5',
                           'Berlin'=>'6,0',
                           'Brandenburg'=>'6,5',
                           'Bremen'=>'5,0',
                           'Hamburg'=>'4,5',
                           'Hessen'=>'6,0',
                           'Mecklenburg-Vorpommern'=>'6,0',
                           'Niedersachsen'=>'5,0',
                           'Nordrhein-Westfalen'=>'6,5',
                           'Rheinland-Pfalz'=>'5,0',
                           'Saarland'=>'6,5',
                           'Sachsen'=>'3,5',
                           'Sachsen-Anhalt'=>'5,0',
                           'Schleswig-Holstein'=>'6,5',
                           'Thüringen'=>'6,5',
                           'Österreich'=>'3,5','Spanien'=>10);
                       ?>
                    <select class="form-control bundesland" name="niedersachsen" style="width: 100%" id="niedersachsen" required>
                       <option value="">Bundesland</option>
                       @foreach($stats as $list)
                        <option data-id="{{@$n_array[$list]}}" value="{{ $list }}" {{ ( old('niedersachsen') && old('niedersachsen') == $list ) ? 'selected' : '' }} >{{ $list }}</option>
                       @endforeach
                    </select>
                    <input type="hidden" class="form-control real_estate_taxes mask-number-input" id="real_estate_taxes" value="" name="real_estate_taxes">
                </div>

                <div class="col-md-3" >
                  <label class="">Straße:</label>
                  <input type="text" class="form-control" id="strasse" name="strasse" value="{{ (old('strasse')) ? old('strasse') : '' }}" required>
                </div>

                <div class="col-md-3">
                  <label>Hausnummer:</label>
                  <input type="text" class="form-control" id="hausnummer" name="hausnummer" value="{{ (old('hausnummer')) ? old('hausnummer') : '' }}" required>
                </div>

              </div>

              <div class="clearfix"></div>

              <div class="card-body address-div" >

                <hr class="mt-0">
                <h4 class="card-title">Weitere Informationen</h4>

                <div class="col-md-3" style="width: 80%;">
                  <label class="">Notizen:</label>
                  <textarea rows="5" cols="50" class="form-control" name="notizen">@if( old('notizen') ){{ old('notizen') }}@elseif(isset($sheet_property)){{ $sheet_property->notizen }}@endif
                  </textarea>
                </div>

                <div id="upload_pdf_div">
                  <div class="pdf-item">
                    <div class="clearfix"></div><br/>
                    <div class="col-md-5">
                      <div class="row">
                        <div class="col-md-8">
                          <label class="">Datei hochladen (max. 20 MB)</label>
                          <input type="file" class="form-control" value="" name="upload_pdf[]" multiple="">
                        </div>
                        {{-- <div class="col-md-4" style="padding-top: 30px;">
                          <button type="button" class="btn-xs btn-primary btn-add-more"><i class="fa fa-plus"></i></button>
                        </div> --}}
                      </div>
                    </div>
                  </div>
                </div>

              </div>


              <div class="clearfix"></div>

              <div class="card-body address-div">

                <hr class="mt-0">
                <h4 class="card-title">Ihre Kontaktdaten*</h4>

                <div class="col-md-3">
                  <label class="">Vorname:</label>
                  <input type="text" class="form-control" id="first_name" name="first_name" value="{{ (old('first_name')) ? old('first_name') : '' }}" required>
                </div>

                <div class="col-md-3">
                  <label class="">Nachname:</label>
                  <input type="text" class="form-control" id="anbieterkontakt" name="anbieterkontakt" value="{{ (old('anbieterkontakt')) ? old('anbieterkontakt') : '' }}" required>
                </div>

                <div class="col-md-3">
                    <label class="">Firma:</label>
                    <input type="text" class="form-control" id="firma" name="firma" value="{{ (old('firma')) ? old('firma') : '' }}" required>
                </div>

                <div class="col-md-3">
                    <label class="">Telefonnummer:</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ (old('phone')) ? old('phone') : '' }}" required>
                </div>

                <div class="col-md-3">
                    <label class="">E-Mail Adresse:</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ (old('email')) ? old('email') : '' }}" required>
                </div>

                <div class="col-md-3">
                    <label class="">Sie sind:</label>
                    <select class="form-control" name="contact" id="contact" style="width: 100%">
                      <option value="makler" {{ ( old('contact') && old('contact') == '' ? 'selected' : 'makler' ) }} >Makler</option>
                      <option value="eigentümer" {{ ( old('contact') && old('contact') == '' ? 'selected' : 'eigentümer' ) }} >Eigentümer</option>
                    </select>
                </div>

              </div>

              <div class="row">
                <div class="col-md-12">
                    <label></label>
                    <div class="form-check" style="padding-left: 1%;">
                      <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" required>
                      <label class="form-check-label">
                        <a href="https://einkauf.fcr-immobilien.de/datenschutzerklaerung/" target="_blank">
                        Ich stimme der Datenschutz­erklärung zu.*</a>
                      </label>
                    </div>
                </div>
              </div>
                
              <div class="clearfix"></div>

              <div class="card-body address-div" style="margin-top: 25px;">
                  <div class="form-group mb-0" style="margin-left: 15px;">
                    <button type="submit" class="btn-primary btn-lg">Daten senden</button>
                  </div>
              </div>

              <p>* Pflichtangaben</p>

            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class=" modal fade" role="dialog" id="add-city">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Neuen Ort hinzufügen</h4>
      </div>
      <div class="modal-body">
          <form id="add-ort">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <label>Name</label>
            <input type="text" name="name" class="form-control">
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary save-city" >Speichern</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')

<script type="text/javascript">
  $('.bundesland').change(function(){
      var option = $('option:selected', this).attr('data-id');
      console.log(option);
      $('.real_estate_taxes').val(option);
  });
  $('.mask-number-input').mask('#.##0,00', {reverse: true});
  $(".city-search").select2({
      minimumInputLength: 2,
      language: {
          inputTooShort: function () {
              return "<?php echo 'Please enter 2 or more characters'; ?>"
          }

      },
      tags: false,
      ajax: {
          url: '{{route("searchcity")}}',
          dataType: 'json',
          data: function (term) {
              return {
                  query: term
              };
          },
          processResults: function (data) {
              return {
                  results: $.map(data, function (item) {
                      return {
                          text: item.code,
                          slug: item.slug,
                          id: item.id
                      }
                  })
              };
          }
      }
  });
  $('.save-city').on('click', function () {
      $.ajax({
          url: "{{route('add_new_city')}}",
          type: "post",
          data : $('#add-ort').serialize(),
          success: function (response) {
            alert(response.message);
            if(response.status==1){
              $('#add-city').modal('hide');
              $('#add-ort')[0].reset();
            }

          }
      });
  });

    /*$('body').on('click', '.btn-add-more', function() {
        var html = '<div class="pdf-item">\
                <div class="clearfix"></div><br/>\
                <div class="col-md-5">\
                  <div class="row">\
                    <div class="col-md-8">\
                      <input type="file" class="form-control" name="upload_pdf[]">\
                    </div>\
                    <div class="col-md-4" style="padding-top: 10px;">\
                      <button type="button" class="btn-xs btn-primary btn-add-more"><i class="fa fa-plus"></i></button>\
                      <button type="button" class="btn-xs btn-danger btn-remove"><i class="fa fa-minus"></i></button>\
                    </div>\
                  </div>\
                </div>\
              <div>\
              ';

        $('#upload_pdf_div').append(html);
    });

    $('body').on('click', '.btn-remove', function() {
      $(this).closest('.pdf-item').remove();
    });*/


</script>
@endsection