@extends('layouts.admin')
@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">

   {{--  <link href="//wrappixel.com/ampleadmin/assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="//wrappixel.com/ampleadmin/dist/js/pages/chartist/chartist-init.css" rel="stylesheet">
    <link href="//wrappixel.com/ampleadmin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="//wrappixel.com/ampleadmin/assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="//wrappixel.com/ampleadmin/dist/css/style.min.css" rel="stylesheet"> --}}
  
    <style type="">
        .pointer-cursor{
            cursor: pointer;
        }

        @media (min-width: 992px)
        {    .modal-lg {
                width: 1320px;
            }
        }
        .card {
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 0 solid transparent;
            border-radius: 0;
        }
        .card-body {
            flex: 1 1 auto;
            padding: 8px 12px;
        }
        .card .card-title {
            position: relative;
            font-weight: 500;
            font-size: 16px;
        }
        .d-flex {
            display: flex!important;
        }
        .align-items-center {
            align-items: center!important;
        }
        .ml-auto, .mx-auto {
            margin-left: auto!important;
        }
        .custum-row .col-lg-3, .custum-row .col-md-3{
            padding: 0 5px;
        }

        .custum-row-2{
            margin-top: 25px;
        }

        .custum-row-2 .col-md-6{
            margin: 5px 0;
            padding: 0 5px;
        }
        .preloader{
            display: none;
        }
        .pb-3, .py-3{
            padding-bottom: 20px;
            margin-top: 10px;
        }
        .for-col-padding .col-md-6{
            padding: 0 5px;
        }
        .get-assetmanager-property,.get-manager-property{
            cursor: pointer;
        }
.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}
    </style>


@endsection
@section('content')
    {{-- <link rel="stylesheet" type="text/css" href="https://wrappixel.com/ampleadmin/dist/css/style.min.css">  --}}
    {{-- {{ dd($tenancy_schedule_data) }} --}}
    <div class="page-content container-fluid">


        <?php
        $year = range(2018,date('Y'));
        $months = range(1,12);
        ?>


 <div class="row">
<div class="col-sm-12" >
   
         <div class="col-sm-6 col-xs-12">
            <div class="white-box analytics-info">
                <h3 class="box-title">Anzahl</h3>
                <ul class="list-inline two-part">
                    <li>
                        <div class="sparklinedash"></div>
                    </li>
                    <li class="text-right">
                        <span class="text-success">
                             
                             @if(isset($analysis))
                             @php 
                               $analys = json_decode( json_encode($analysis),true );
                             @endphp
                             @endif

                             @if(isset($analys) && count($analys)>0)
                              {{ $analys[0]['id'] }}
                              @else
                              0
                              @endif 
                        </span>
                    </li>
                </ul>
            </div>
        </div>
       
        <div class="col-sm-6 col-xs-12">
            <div class="white-box analytics-info">
                <h3 class="box-title">Gesamtkaufpreis</h3>
                <ul class="list-inline two-part">
                    <li>
                        <div class="sparklinedash"></div>
                    </li>
                    <li class="text-right">
                        <span class="text-success">
                           @if(isset($analys) && count($analys)>0)
                           {{number_format($analys[0]['total_purchase_price1'],2,",",".")}}
                           @else
                           {{number_format(0,2,",",".")}}
                           @endif 
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        @if(Auth::user()->email == "s.mueller@fcr-immobilien.de")

        <div class="col-md-6" id="card-release-invoice" data-url="{{ route('release_invoice') }}">
         <div class="card pointer-cursor">
            <div class="card-body">
               <h5 class="card-title text-uppercase">Freigegebene Rechnungen</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 release-invoice-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>
      @endif
  
  </div>
 
        </div>
     </div>
       

        <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Neue Immobilien im Angebot 
                        <select class="pm-month change-select">
                        <option value="">All</option>
                        @foreach($months as $list)
                        <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                        @endforeach
                    </select>
                        <!-- {{__('dashboard.'.date('F'))}} -->
                    <select class="pm-year change-select">
                        @foreach($year as $list)
                        <option value="">All</option>
                        <option 
                        value="{{$list}}">{{$list}}</option>
                        @endforeach
                    </select>
                    </h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive manager-property-list">
                        
                    </div>
                </div>
            </div>

            <!-- <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box" id="bankenfinan">
                    <h3 class="box-title m-b-0">Bankenfinanzierungen</h3>
                    <div class="table-responsive bank-prop-list">
                        
                    </div>
                </div>
            </div> -->

            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box" id="properties-detail">
                    <h3 class="box-title m-b-0">Verschickte LOIs</h3>
                    <div class="table-responsive properties-detail-div">
                        
                    </div>
                </div>
            </div>

            @if(Auth::user()->email == "j.diemer@fcr-immobilien.de")

            <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
                 <div class="white-box" id="categ">
                    <h3 class="box-title m-b-0">Mieterübersicht</h3>
                    <h3 class="box-title m-b-0"><a href="{{route('getcategoryproperty')}}?export=1" class="pull-right btn btn-success">Export</a></h3>
                    <div class="clearfix"></div>
                    <div class="table-responsive category-prop-list2">
                    </div>
                 </div>
              </div>
              <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
                 <div class="white-box">
                    <h3 class="box-title m-b-0">Mieterübersicht</h3>
                    <div id="high-container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                 </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
                 <div class="white-box">
                    <h3 class="box-title m-b-0">Mieterübersicht (Corona)</h3>
                    <div class="clearfix"></div>
                    <div class="table-responsive category-prop-list4">
                    </div>
                 </div>
              </div>
              <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
                 <div class="white-box">
                    <h3 class="box-title m-b-0">Mieterübersicht (Corona)</h3>
                    <div class="clearfix"></div>
                    <div class="table-responsive category-prop-list5">
                    </div>
                 </div>
              </div>
              @endif
            @if(Auth::user()->email == "j.lux@fcr-immobilien.de" || Auth::user()->email == "n.eschenbach@fcr-immobilien.de")
              <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box" id="release-invoice-box">
                    <h3 class="box-title m-b-0">Rechnungsfreigaben</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive release-invoice-div">

                    </div>
                </div>
            </div>
            @endif
            

        </div>
 
    </div>
 
 

    <div class="modal fade" id="property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body list-data">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="cate-pro-listing" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body cate-pro-div">
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="release-invoice-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">Freigegebene Rechnungen</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12 table-responsive">
                  <table class="table table-striped" id="release-invoice-table">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Objekt</th>
                           <th>Rechnung</th>
                           <th>Rechnungsdatum</th>
                           <th>Re. Bet.</th>
                           <th>Kommentar Rechnung</th>
                           <th>Abbuch.</th>
                           <th>User</th>
                           <th>Freigabedatum</th>
                           <th>Kommentar Falk</th>
                           {{-- <th>Überweisung TR</th> --}}
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="send_mail_loi_modal" style="z-index: 9999;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Erinnerungsmail verchicken</h4>
         </div>
         <form action="{{ route('sendmail_to_loi_user') }}" id="form_sendmail_to_loi_user">
            <input type="hidden" name="property_id">
            <div class="modal-body">
               <label>Nachricht</label>
               <textarea class="form-control" name="message" rows="5" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

  

@endsection

@section('js')
 
 
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script src="{{asset('js/custom-datatable.js')}}"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    
    <script>

        var url_dashboard_counter                 = '{{ route('get_dashboard_counter') }}';
        function loadDashboardCounter(){
            $.ajax({
                url: url_dashboard_counter+'?counter=release_invoice',
                type: 'GET',
                dataType: 'json',
                beforeSend: function() {

                },
                success: function(result) {              
                  $('.release-invoice-total').text(result.release_invoice);
                },
                error: function(error) {

                }
            });
        }



        var columns = [
            null,
            null,
            null,
            { "type": "formatted-num" },
            { "type": "formatted-num" },
            { "type": "formatted-num" },
            { "type": "formatted-num" },
            null
        ];
        makeDatatable($('#tenant-table2'), columns);

        $('.change-select').change(function(){
            m = $('.pm-month').val();
            y = $('.pm-year').val();

            var url = "{{route('monthyearmanager')}}?month="+m+"&year="+y;
             $.ajax({
                url: url,
            }).done(function (data) { 
                $('.manager-property-list').html(data);

                var f = $('.manager-property-list').find('.flg').val();

                if(f == "1"){

                    var columns = [
                        null,
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" }
                    ];
                    makeDatatable($('#added-month'), columns, "asc", 0, 50);

                }else{

                    var columns = [
                        null,
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" },
                        null,
                        { "type": "numeric-comma" }
                    ];
                    makeDatatable($('#added-month'), columns, "asc", 0, 50);
                }
            })
        });

        $('.achange-select').change(function(){
            m = $('.pa-month').val();
            y = $('.pa-year').val();

            var url = "{{route('monthyearassetmanager')}}?month="+m+"&year="+y;
            $.ajax({
                url: url,
            }).done(function (data) { 
                $('.asset-manager-property-list').html(data.table);

                var columns = [
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" }
                ];
                makeDatatable($('#added-month-asset'), columns);
            });
        })

        $('body').on('click','.get-manager-property',function(){
            $('.list-data').html("");
            id= $(this).attr('data-id');
            m = $('.pm-month').val();
            y = $('.pm-year').val();
            status= $(this).attr('data-status');


            var url = "{{route('getmanagerproperty')}}?id="+id+"&month="+m+"&year="+y+"&status="+status;
            $.ajax({
                url: url,
            }).done(function (data) { 
                $('.list-data').html(data);

                if(status == 12){

                    var columns = [
                        null,
                        null,
                        { "type": "new-date" },
                        { "type": "new-date" },
                        null,
                        null,
                        null,
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        null
                    ];
                    makeDatatable($('#list-properties'), columns, "asc", 2);

                }else if(status == 14 || status == 10){

                    var columns = [
                        null,
                        null,
                        { "type": "new-date" },
                        { "type": "new-date" },
                        null,
                        null,
                        null,
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        null
                    ];
                    makeDatatable($('#list-properties'), columns, "asc", 2);
                    
                }else{

                    var columns = [
                        null,
                        null,
                        { "type": "new-date" },
                        { "type": "new-date" },
                        null,
                        null,
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        null
                    ];
                    makeDatatable($('#list-properties'), columns, "asc", 2);
                }
            })
        });

        $('body').on('click','.get-assetmanager-property',function(){
            $('.asset-list-data').html("");
            id= $(this).attr('data-id');

            m = $('.pa-month').val();
            y = $('.pa-year').val();

            t = $(this).attr('data-type');


            var url = "{{route('getassetmanagerproperty')}}?id="+id+"&month="+m+"&year="+y+"&type="+t;
             $.ajax({
                    url: url,
                }).done(function (data) { 
                    $('.asset-list-data').html(data);
            })
        });


        $(document).ready(function () {



            // var url = "{{route('getbankproperty')}}";
            // $.ajax({
            //     url: url,
            // }).done(function (data) { 
            //     $('.bank-prop-list').html(data);
            //     var columns = [
            //         null,
            //         null,
            //         { "type": "numeric-comma" },
            //         { "type": "new-date" },
            //         { "type": "numeric-comma" },
            //         { "type": "numeric-comma" },
            //         { "type": "numeric-comma" },
            //         { "type": "numeric-comma" },
            //         { "type": "numeric-comma" },
            //         { "type": "numeric-comma" },
            //         { "type": "numeric-comma" }
            //     ];
            //     makeDatatable($('#bank-properties'), columns);
                
            //     // $('.bankenfinanTotal').html($('.total-delta2').html())
                
            // });

            var url = "{{route('getPropertiesDetail')}}";
            $.ajax({
                url: url,
            }).done(function (data) { 
                $('.properties-detail-div').html(data);
                var columns = [
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "new-date" },
                    null
                ];
                makeDatatable($('#properties_detail_table'), columns);
            });

            $('.achange-select').trigger('change');

            $('.change-select').trigger('change');
        });

        function drawchart3(container,cat,mv,vl)
   {
       Highcharts.chart(container, {
             chart: {
               type: 'column'
             },
             title: {
               text: 'MIETERÜBERSICHT'
             },
             xAxis: {
               categories: cat,
               crosshair: true
             },
             yAxis: {
               min: 0,
               title: {
                 text: 'Anzahl'
               },
               stackLabels: {
                   enabled: true,
                   style: {
                       fontWeight: 'bold',
                       color: ( // theme
                           Highcharts.defaultOptions.title.style &&
                           Highcharts.defaultOptions.title.style.color
                       ) || 'gray'
                   }
               }
             },
   
             tooltip: {
               headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
               pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                 '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
               footerFormat: '</table>',
               shared: true,
               useHTML: true
             },
             plotOptions: {
               column: {
                 pointPadding: 0.2,
                 borderWidth: 0,
                 dataLabels: {
                       enabled: true
                   }
               }
             },
             series: [{
               name: 'Miete netto p.m.',
               data: mv
             },
             {
               name: 'Anzahl',
               data: vl
   
             }]
           });
   }
   
        function getcategoryproperties()
       {
            var url = "{{route('getcategoryproperty')}}";
               $.ajax({
                   url: url,
               }).done(function (data) { 
                   // $('.category-prop-list1').html(data.table1);
                   $('.category-prop-list2').html(data.table2);
       
                   if($('#high-container3').length>0)
                   drawchart3('high-container3',data.name,data.actual_net_rent,data.count);
       
                   var columns = [
                       null,
                       { "type": "numeric-comma" },
                       { "type": "numeric-comma" },
                       { "type": "numeric-comma" },
                       null,
                       { "type": "numeric-comma" },
                   ];
       
                   makeDatatable($('.category-properties'), columns, "desc", 1);
        
               });
       
       }
       function getcategoryproperties4()
       {
            var url = "{{route('getcategoryproperty3')}}";
               $.ajax({
                   url: url,
               }).done(function (data) { 
                   // $('.category-prop-list1').html(data.table1);
                   $('.category-prop-list4').html(data.table);
                   $('.category-prop-list5').html(data.table2);
                   var columns = [
                       null,
                       null,
                       { "type": "numeric-comma" }
                   ];
       
                   makeDatatable($('.category-table4'), columns, "desc", 1);
       
               });
       
       }
       getcategoryproperties();
       getcategoryproperties4();


        
       function getcategorypropertieslist(category,type,check)
   {
       $('.cate-pro-div').html("");
       $('#cate-pro-listing').modal();
        var url = "{{route('getcategoryproperty')}}?category="+category+'&type='+type+'&rent_closed='+check;
           $.ajax({
               url: url,
           }).done(function (data) { 
               
               $('.cate-pro-div').html(data);
               var columns = [
                   null,
                   null,
                   { "type": "numeric-comma" },
                   { "type": "numeric-comma" },
                   null,
                   null,
               ];
               makeDatatable($('.category-table'), columns,'desc',2);
           });
   
   }
   function getcategorypropertieslist2(category)
   {
       $('.cate-pro-div').html("");
       $('#cate-pro-listing').modal();
        var url = "{{route('getcategoryproperty3')}}?category="+category;
           $.ajax({
               url: url,
           }).done(function (data) { 
               
               $('.cate-pro-div').html(data);
               var columns = [
                   null,
                   null,
                   null,
                   { "type": "numeric-comma" },
                   null,
                   null,
               ];
               makeDatatable($('.category-table'), columns,'desc',2);
           });
   
   }
   function getcategorypropertieslist3(category)
   {
       $('.cate-pro-div').html("");
       $('#cate-pro-listing').modal();
        var url = "{{route('getcategoryproperty3')}}?category2="+category;
           $.ajax({
               url: url,
           }).done(function (data) { 
               
               $('.cate-pro-div').html(data);
               var columns = [
                   null,
                   null,
               ];
               makeDatatable($('.category-table'), columns,'desc',0);
           });
   
   }

       $('body').on('click','.get-category-properties',function(){
           category = $(this).attr('data-category');
           type = $(this).attr('data-type');
           check = $(this).attr('data-rent-closed');
           getcategorypropertieslist(category,type,check)
       })
       $('body').on('click','.get-category-properties3',function(){
           category = $(this).attr('data-category');
           getcategorypropertieslist2(category)
       })
       $('body').on('click','.get-category-properties4',function(){
           category = $(this).attr('data-category');
           getcategorypropertieslist3(category)
       })



    
       var url_getPropertyReleaseInvoice   = '{{ route('getPropertyReleaseInvoice') }}';
       var url_makeAsPaid                  = '{{ route('makeAsPaid') }}';
        

       var url = url_getPropertyReleaseInvoice+"?";
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.release-invoice-div').html(data.html);
            // $('.release-invoice-total').html(data.count);
            var columns = [
                null,
                null,
                null,
                { "type": "new-date" },
                { "type": "numeric-comma" },
                { "type": "new-date-time" },
                null,
                null,
            ];
            makeDatatable($('#table-property-release-invoice') ,columns, 'desc', 5);
        });

        $('body').on('click','.mark-as-paid',function(){
            var id = $(this).attr('data-id');
            $this = $(this);
            var url = url_makeAsPaid+"?id="+id;
            $.ajax({
                url: url,
            }).done(function (data) { 
                $this.closest('tr').remove();
            });

        });

        $('body').on('click','.btn-send-mail-loi-user',function(){
          var property_id = $(this).attr('data-property-id');
          $('#send_mail_loi_modal').find('input[name="property_id"]').val(property_id);
          $('#send_mail_loi_modal').modal('show');
        });

        $('#form_sendmail_to_loi_user').on('submit', (function(e) {
            e.preventDefault();
            var $this = $(this);
            var formData = new FormData($(this)[0]);
            var url = $(this).attr('action');

            $.ajax({
              url: url,
              type: 'POST',
              data: formData,
              dataType: 'json',
              processData: false,
              contentType: false,
              cache: false,
              beforeSend: function() {
                  $($this).find('button[type="submit"]').prop('disabled', true);
              },
              success: function(result) {
                  $($this).find('button[type="submit"]').prop('disabled', false);
                  if (result.status) {
                    $($this)[0].reset();
                    $('#send_mail_loi_modal').modal('hide');
                  }else{
                    alert(result.message);
                  }
              },
              error: function(error) {
                  $($this).find('button[type="submit"]').prop('disabled', false);
                  alert('Somthing want wrong!');
                  console.log({error});
              }
            });
        }));

    </script>
@if(Auth::user()->email == "j.diemer@fcr-immobilien.de")
<script type="text/javascript">
       getcategoryproperties();
       getcategoryproperties4();
</script>
@endif


@if(Auth::user()->email == "s.mueller@fcr-immobilien.de")
<script type="text/javascript">
    $('body').on('click', '#card-release-invoice', function() {
        var url = $(this).attr('data-url');
        $('#release-invoice-modal').modal('show');

        var release_invoice_table;

        if ( ! $.fn.DataTable.isDataTable( '#release-invoice-table' ) ) {

          release_invoice_table = $('#release-invoice-table').DataTable({
            processing: true,
            serverSide: true,
            "order": [
                [8, 'desc']
            ],
            ajax: url,
            columns: [
                {data: 'DT_RowIndex', name: 'pi.id'},
                {data: 'name_of_property', name: 'p.name_of_property'},
                {data: 'invoice', name: 'pi.invoice'},
                {data: 'date', name: 'pi.date'},
                {data: 'amount', name: 'pi.amount', className: 'text-right'},
                {data: 'invoice_comment', name: 'pi.comment'},
                {data: 'checkbox', orderable: false, searchable: false},
                {data: 'name', name: 'u.name'},
                {data: 'created_at', name: 'pml.created_at'},
                {data: 'comment', name: 'pml.comment'},
                // {data: 'btn1', orderable: false, searchable: false}
            ]
          });

        }else{
          $('#release-invoice-table').DataTable().ajax.reload(null, false);
        }

    });
    
    $(document).ready(function(){

        loadDashboardCounter();
    })
</script>
@endif

@endsection

