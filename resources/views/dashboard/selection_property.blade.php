<table class="table table-striped" id="table-property-selection" >
	<thead>
		<tr>
			<th>
				<input type="checkbox" id="check-all">
			</th>
			<th>ID</th>
			<th>Objekt</th>
			<th>Status</th>
			{{-- <th>verkaufspreis</th> --}}
			{{-- <th>netto miete</th> --}}
			{{-- <th>mietrendite</th> --}}
		</tr>
	</thead>
		@if($data)
			@foreach ($data as $element)
				<tr>
					<td>
						<input type="checkbox" class="check" value="{{ $element->id }}">
					</td>
					<td>{{ $element->id }}</td>
					<td>{{ $element->name_of_property }}</td>
					<td>
						@if($element->status == config('properties.status.in_purchase'))
                            {{__('property.in_purchase')}}
                        @elseif($element->status == config('properties.status.offer'))
                            {{__('property.offer')}}
                        @elseif($element->status == config('properties.status.exclusive'))
                            {{__('property.exclusive')}}
                        @elseif($element->status == config('properties.status.exclusivity'))
                            {{__('property.exclusivity')}}
                        @elseif($element->status == config('properties.status.certified'))
                            {{__('property.certified')}}
                        @elseif($element->status == config('properties.status.duration'))
                            {{__('property.duration')}}
                        @elseif($element->status == config('properties.status.sold'))
                            {{__('property.sold')}}
                        @elseif($element->status == config('properties.status.declined'))
                            {{__('property.declined')}}
                        @elseif($element->status == config('properties.status.lost'))
                            {{__('property.lost')}}
                        @elseif($element->status == config('properties.status.hold'))
                            {{__('property.hold')}}
                        @elseif($element->status == config('properties.status.quicksheet'))
                            {{__('property.quicksheet')}}
                        @elseif($element->status == config('properties.status.liquiplanung'))
                            {{__('property.liquiplanung')}}
                        @endif
					</td>
					{{-- <td></td> --}}
					{{-- <td></td> --}}
					{{-- <td></td> --}}
				</tr>
			@endforeach
		@endif
	</tbody>
</table>