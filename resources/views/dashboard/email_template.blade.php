@extends('layouts.admin') 

@section('css')
<!-- Styles -->
 <style type="text/css">
     .input_style{
    border-right: none;
    border-left: none;
    border-top: none;
    border-bottom: #000 solid 1px;
    margin-left: 20px;
     }
 </style>
@endsection

@section('content')

<div class="row">
    @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif
    
</div>

 @if(isset($email_template))
<div class="row">
    <form action="{{ route('send_email_template') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="col-sm-4">
       <input type="email" placeholder="Enter email here" class="form-control" name="email_to_send" > 
    </div>
    <div class="col-sm-4">
       <input type="submit" class="btn btn-success" value="Send email" > 
    </div>
   </form> 
</div>
<br> 
@endif

<div class="white-box">
 
<form action="{{ route('email_template_save') }}" method="post"  enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="row">
    <div class="col-sm-4 col-sm-offset-8">
        <img src="https://intranet.fcr-immobilien.de/img/logo.png" width="200" style="margin-left: -10px;">
    <br>
    FCR Immobilien AG<br>
    Bavariaring 24<br>
    D-80336 München
    </div>
</div>


<div class="row">
    <div class="col-sm-4 col-sm-offset-8">
    <br> Tel. +49 (0) 89 / 413 2496 21 <br>
    Fax +49 (0) 89 / 413 2496 99 <br>
    eMail <input type="text" class="input_style" value="@if(isset($email_template)){{ $email_template->email }} @endif" name="email"> <br>
    Internet www.fcr-immobilien.de  
    </div>
</div>

<div style="margin-top: -42px; position: absolute;">FCR Immobilien • Bavariaring 24 • D-80336 München</div>

<div class="row">
    <div class="col-sm-4"> 
<input type="text" class="input_style" value="@if(isset($email_template)){{ $email_template->address1 }} @endif" name="address1"><br>
<input type="text" class="input_style" value="@if(isset($email_template)){{ $email_template->address2 }} @endif" name="address2"><br>
<input type="text" class="input_style" value="@if(isset($email_template)){{ $email_template->address3 }} @endif" name="address3"><br>
<input type="text" class="input_style" value="@if(isset($email_template)){{ $email_template->address4 }} @endif" name="address4"><br>
    </div>
</div>

<div class="row">
    <div class="col-sm-4 col-sm-offset-8">
        München,  <input type="date" class="input_style" value="@if(isset($email_template)){{ $email_template->date }}@endif" name="date">
    </div>
</div>


<div class="row">
    <div class="col-sm-12">
<b>Kaufangebot für das Objekt in<input value="@if(isset($email_template)){{ $email_template->objekt }} @endif" type="text" class="input_style" name="objekt" style="width:40%; margin-left: 5px;"></b><br><br>
Sehr geehrter Herr Schilke, Sehr geehrter Herr Baumgärtel,<br><br>
auf Grund einer ersten Prüfung der uns überlassenen Unterlagen bieten wir Ihnen für das Objekt
in <input value="@if(isset($email_template)){{ $email_template->purchase }} @endif" type="text" name="purchase" class="input_style" style="width:40%; margin-left: 5px;"> einen Kaufpreis in Höhe von<br><br>

<center><b><input value="@if(isset($email_template)){{ $email_template->price }} @endif" type="text" class="input_style" name="price" style="width: 90px;"> €</b></center><br><br>

<input type="text" value="@if(isset($email_template)){{ $email_template->line1 }} @endif" class="input_style" name="line1" style="width:90%; margin-left: 5px;">
<input type="text" value="@if(isset($email_template)){{ $email_template->line2 }} @endif" class="input_style" name="line2" style="width:90%; margin-left: 5px;"><br><br>

An dieses Kaufpreisangebot halten wir uns ab Angebotsabgabe <input value="@if(isset($email_template)){{ $email_template->word1 }} @endif" name="word1" type="text" class="input_style" style="width: 90px;"> Wochen lang gebunden.
Der Verkäufer sichert der FCR Immobilien AG eine <input value="@if(isset($email_template)){{ $email_template->word2 }} @endif" name="word2" type="text" class="input_style" style="width: 90px;"> wöchige Exklusivität ab
Kaufpreiszusage zu. Sie wird bis zu diesem Zeitpunkt bereits laufende Verhandlungen nicht
fortführen und das betreffende Objekt keinem Dritten direkt oder indirekt anbieten. Es ist geplant,
eine Beurkundung bis zum Ende der Exklusivitätsphase durchzuführen.<br><br>

Das Kaufpreisangebot steht unter Gremienvorbehalt und vorbehaltlich einer positiven Due
Diligence.<br><br>

Wir bitten Sie das Angebot an den Eigentümer / den Verkäufer weiterzuleiten und schriftliche Bestätigung dieses Angebots durch den entsprechenden Handlungsbevollmächtigten.<br><br>

Auf eine positive Antwort freuen wir uns und verbleiben,<br><br>

<div class="row">
    <div class="col-sm-5">
    Mit freundlichen Grüßen
    </div>
    <div class="col-sm-7">
    Verkäufer/in <input value="@if(isset($email_template)){{ $email_template->saleswoman }} @endif" type="text" class="input_style" name="saleswoman" style="width: 160px;">, den <input value="@if(isset($email_template)){{ $email_template->saleswoman_the }} @endif" name="saleswoman_the" type="text" class="input_style" style="width: 160px;">
    </div>
</div>
<br> 

<div class="row">
    
    <div class="col-sm-3">

        @if(isset($email_template))

            @if($email_template->signature !=null)
                <img style="width: 82%; height: 81px; object-fit: cover;" src="https://intranet.fcr-immobilien.de/email_templates/{{ $email_template->signature }}">
                <br><br>
                <label>Change Signature</label>
                <input type="file"  name="signature"  >
            @else
                <input type="file"  name="signature" style="margin-top: 20px;">
            @endif

        @else
                <input type="file"  name="signature" style="margin-top: 20px;">
        @endif

    </div>

    <div class="col-sm-3">
        <img  style="width: 100%" src="{{ asset('img/signature.bmp') }}">
    </div>

   

</div>


 
<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
    <div class="col-sm-6">
    <input value="@if(isset($email_template)){{ $email_template->last1 }} @endif" type="text" class="input_style" name="last1" style="width:90%; margin-left: 5px;">
    </div>
    <div class="col-sm-6">
    <input value="@if(isset($email_template)){{ $email_template->last2 }} @endif" type="text" class="input_style" name="last2" style="width:90%; margin-left: 5px;">
    </div>

</div>
 
Transaction Manager<br>
Transaction Management<br>
FCR Immobilien AG<br>
 
</div>
</div>

<div class="row">
<br>
<center>FCR Immobilien AG • HRB 210430 • Amtsgericht München</center>
<center>Vorstand: Falk Raudies • Aufsichtsratsvorsitzender: Prof. Dr. Franz-Joseph Busse</center><br>
<center>Volksbank Raiffeisenbank Fürstenfeldbruck eG • Konto: 99899 • BLZ: 701 633 70</center>
<center>SWIFT: GENODEF1FFB • IBAN DE32701633700000099899</center>
<br>
</div>

<center>
    <button type="submit" class="btn btn-success" type="submit" style="padding: 10px 50px;" >Save</button>
</center>
</form>
</div>
 
@endsection

@section('js')
<!-- Js -->
   
@endsection
