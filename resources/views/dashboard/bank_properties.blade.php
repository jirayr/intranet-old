<div class="table-responsive">
    <div class="property-table">
        <table class="table table-striped dashboard-table" id="bank-properties">
            <thead>
                <tr>
                    <th>Objekt</th>
                    <th>Bank</th>
                    <th class="text-right">GS Bank</th>
                    <th>Auszahlungs datum</th>
                    <th class="text-right">Darlehen 30.06.2020 HGB</th>
                    <th class="text-right">Darlehen 31.12.2019 HGB</th>
                    <th class="text-right">Verkehrs-wert</th>
                    <th class="text-right">Delta</th>
                    <th class="text-right">Zins</th>
                    <th class="text-right">Zinsaufwand p.a. ann. per 31.12.2019</th>
                    <th class="text-right">Kapitaldienst p.a. ann. per 31.12.2019</th>
                </tr>
            </thead>
            <?php 
                $total_original_loan = 0;
                $total_delta2 = 0;
                $total_market_value = 0;
                $total_delta = 0;
                $total_interest_extend = 0;
                $total_loan_service = 0;
            ?>
            <tbody>
                @foreach ($data as $element)

                    <?php
                        $total_original_loan    += ($element->original_loan != '') ? $element->original_loan : 0;
                        $total_delta2           += ($element->delta2 != '') ? $element->delta2 : 0;
                        $total_market_value     += ($element->market_value != '') ? $element->market_value : 0;
                        $total_delta            += ($element->delta != '') ? $element->delta : 0;
                        $total_interest_extend  += ($element->interest_extend != '') ? $element->interest_extend : 0;
                        $total_loan_service     += ($element->loan_service != '') ? $element->loan_service : 0;
                    ?>

                    <tr>
                        <td>
                            <a href="{{route('properties.show',['property'=>$element->main_property_id])}}">{{ $element->name_of_property }}</a>
                        </td>
                        <td>{{ $element->lender }}</td>
                        <td class="text-right">{{ number_format($element->original_loan, 2 , ",", ".") }}</td>
                        <td class="text-center">
                            <?php
                                $buyd =  DB::table('properties_buy_details')->whereRaw('(type=402) and property_id='.$element->main_property_id)->first();
                                if($buyd && $buyd->comment)
                                {
                                    echo show_date_format($buyd->comment);
                                }
                            ?> 
                        </td>
                        <td class="text-right">{{ show_number($element->delta3, 2) }}</td>
                        <td class="text-right">{{ number_format($element->delta2, 2 , ",", ".") }}</td>
                        <td class="text-right">{{ ($element->market_value != '') ? number_format($element->market_value, 2 , ",", ".") : '' }}</td>
                        <td class="text-right">{{ number_format($element->delta, 2 , ",", ".") }}</td>
                        <td class="text-right">{{ $element->zins }}%</td>
                        <td class="text-right">{{ number_format($element->interest_extend, 2 , ",", ".") }}</td>
                        <td class="text-right">{{ number_format($element->loan_service, 2 , ",", ".") }}</td>
                        
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2" class="text-center border-top-footer">Gesamt</th>
                    <th class="text-right border-top-footer">{{ number_format($total_original_loan, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer"></th>
                    <th class="text-right border-top-footer"></th>
                    <th class="text-right border-top-footer total-delta2">{{ number_format($total_delta2, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer">{{ number_format($total_market_value, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer">{{ number_format($total_delta, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer"></th>
                    <th class="text-right border-top-footer">{{ number_format($total_interest_extend, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer">{{ number_format($total_loan_service, 2 , ",", ".") }}</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>