<table id="property-provision-table" class="table table-striped">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>AM</th>
			<th>Mieter</th>
			<th>Miete x LFZ</th>
			<th>Kosten</th>
			<th>Nettoeinnahmen</th>
			<th>MV / VL</th>
			<th>Provision in %</th>
			<th>Provision</th>
			<th>Aktion</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$user_email = strtolower(Auth::user()->email);
		$replyemailaddress = config('users.falk_email');
		?>
		
		@if (!empty($data))
			@foreach ($data as $element)
				<?php 
					$subject = 'Provisionsfreigaben: '.$element->name_of_property;
					$content = '';

					if($element->name){
						$content .= 'Mieter: '.$element->name;
					}
				?>
				<tr>
					<td>
						<a href="{{route('properties.show',['property'=> $element->property_id])}}?tab=provision_tab">
                            {{$element->name_of_property}}
                        </a>
					</td>
					<td>
						<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $element->property_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="PROVISIONSFREIGABEN" data-id="{{ $element->id }}" data-section="provisions">{{ ($element->am_name) ? $element->am_name : $element->am_name2 }}</a>
					</td>
					<td>{{ $element->name }}</td>
					<td>{{ show_number($element->rent) }}</td>
					<td>{{ show_number($element->cost) }}</td>
					<td>{{ number_format($element->net_income,2,',','.') }}</td>
					<td>
						@if($element->mv_vl)
						{{ $element->mv_vl }}
						@else
						{{'MV'}}
						@endif
					</td>
					<td>{{ $element->commision_percent }}</td>
					<td>
						{{ ($element->commision_percent != '' && is_numeric($element->commision_percent)) ? number_format($element->net_income*$element->commision_percent/100,2,',','.') : '' }}
					</td>
					<td>
						@if($user_email==$replyemailaddress)
						<button type="button" class="btn btn-primary provision-release-request" data-column="pbtn2" data-property_id="{{$element->property_id}}" data-id="{{$element->id}}">Freigeben</button>@endif

					</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>
<input type="hidden" class="provisioncount" value="{{count($data)}}">