<?php
$count1 = $count2 = 0;
?>
@foreach ($property as $key => $value)				
<?php $table_id = 'all_insurance_table_'.$type; ?>
<div class="table-responsive">

<h3>{{$value->name_of_property}}
<button class="btn  btn-xs btn-primary request-insurance-button" data-id="{{$value->id}}" data-type="{{$release}}" style="margin-left: 10px;">Freigeben</button>
</h3>
<table  class="table table-striped {{ $table_id }}">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>Name</th>
			<th>Betrag</th>
			<th>Laufzeit von -Laufzeit bis</th>
			<th>Kündigungsfrist</th>
			<th>Kommentar</th>
			<th>AM-Empfehlung</th>
			<th>Falk</th>
		</tr>
	</thead>
	<tbody>
			<?php
			$value->insurance = DB::table('property_insurances')->selectRaw('*')->where('property_id', $value->id)->where('type', $type)->get();
			?>

			@if ($type == 1)
			<?php
			$count1 +=1;
			?>
					@if($value->gebaude_betrag)
					<tr>
						<td>
							<a href="{{route('properties.show',['property'=> $value->id])}}?tab=insurance_tab">
                            	{{$value->name_of_property}}
                        	</a>
						</td>
						<td>{{ $value->axa }}</td>
						<td>{{ (is_numeric($value->gebaude_betrag)) ? number_format( $value->gebaude_betrag, 2 ,",",".") : '' }}  €</td>
						<td>{{ show_date_format($value->gebaude_laufzeit_from) }} - {{ show_date_format($value->gebaude_laufzeit_to) }}</td>
						<td>{{ $value->gebaude_kundigungsfrist }}</td>
						<td>{{ $value->gebaude_comment }}</td>
						<td>
							<input data-column="gebaude" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{ $value->id }}" {{ ($value->gebaude_is_approved) ? 'checked' : '' }}>
						</td>
						<td>
							<input data-column="gebaude_falk_approved" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$value->id}}" {{ ($value->gebaude_falk_approved) ? 'checked' : '' }}>
						</td>
					</tr>
					@endif
					@if(isset($value->insurance) && count($value->insurance) > 0)
						@foreach ($value->insurance as $key1 => $value1)
							<tr>
								<td>
									<a href="{{route('properties.show',['property'=> $value->id])}}?tab=insurance_tab">
		                            	{{$value->name_of_property}}
		                        	</a>
								</td>
								<td>{{ $value1->name }}</td>
								<td>{{ (is_numeric($value1->amount)) ? number_format( $value1->amount, 2 ,",",".") : '' }}  €</td>
								<td>{{ show_date_format($value1->date_from) }} - {{ show_date_format($value1->date_to) }}</td>
								<td>{{ $value1->kundigungsfrist }}</td>
								<td>{{ $value1->note }}</td>
								<td>
									<input data-column="insurance" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$value1->id}}" {{ ($value1->is_approved) ? 'checked' : '' }}>
								</td>
								<td>
									<input data-column="falk" type="checkbox" class="falk-checkbox-is-checked" data-id="{{$value1->id}}" {{ ($value1->is_checked) ? 'checked' : '' }}>
								</td>
							</tr>
						@endforeach
					@endif
			@endif

			@if ($type == 2)
			<?php
			$count2 +=1;
			?>
					@if($value->haftplicht_betrag)
					<tr>
						<td>
							<a href="{{route('properties.show',['property'=> $value->id])}}?tab=insurance_tab">
                            	{{$value->name_of_property}}
                        	</a>
						</td>
						<td>{{ $value->allianz }}</td>
						<td>{{ (is_numeric($value->haftplicht_betrag)) ? number_format($value->haftplicht_betrag,2,",",".") : '' }} €</td>
						<td>{{ show_date_format($value->haftplicht_laufzeit_from) }} - {{ show_date_format($value->haftplicht_laufzeit_to) }}</td>
						<td>{{ $value->haftplicht_kundigungsfrist }}</td>
						<td>{{ $value->haftplicht_comment }}</td>
						<td>
							<input data-column="falk" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$value->id}}" {{ ($value->haftplicht_is_approved) ? 'checked' : '' }}>
						</td>
						<td>
							<input data-column="haftplicht_falk_approved" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$value->id}}" {{ ($value->haftplicht_falk_approved) ? 'checked' : '' }}>
						</td>
					</tr>
					@endif
					@if(isset($value->insurance) && count($value->insurance) > 0)
						@foreach ($value->insurance as $key1 => $value1)
							<tr>
								<td>
									<a href="{{route('properties.show',['property'=> $value->id])}}?tab=insurance_tab">
		                            	{{$value->name_of_property}}
		                        	</a>
								</td>
								<td>{{ $value1->name }}</td>
								<td>{{ (is_numeric($value1->amount)) ? number_format( $value1->amount, 2 ,",",".") : '' }}  €</td>
								<td>{{ show_date_format($value1->date_from) }} - {{ show_date_format($value1->date_to) }}</td>
								<td>{{ $value1->kundigungsfrist }}</td>
								<td>{{ $value1->note }}</td>
								<td>
									<input data-column="insurance" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$value1->id}}" {{ ($value1->is_approved) ? 'checked' : '' }}>
								</td>
								<td>
									<input data-column="falk" type="checkbox" class="falk-checkbox-is-checked" data-id="{{$value1->id}}" {{ ($value1->is_checked) ? 'checked' : '' }}>
								</td>
							</tr>
					@endforeach
						@endif
			@endif
	</tbody>
</table>
</div>
@endforeach
@if ($type == 1)
<input type="hidden" class="{{'release_building'}}" value="{{$count1}}">
@else
<input type="hidden" class="{{'release_liability'}}" value="{{$count2}}">
@endif
