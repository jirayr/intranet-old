<?php
		$user_email = strtolower(Auth::user()->email);
		$replyemailaddress = config('users.falk_email');
		$delete = 0;
		?>
		
		@if($user_email==$replyemailaddress || $user_email=="c.wiedemann@fcr-immobilien.de")
		<?php $delete=1; ?>
		<!-- <button type="button" style="margin-bottom:20px;" class="btn btn-primary multiple-invoice-release-request pull-right">Markierte Rechnungen freigeben</button> -->
		@endif
		
<table id="table-property-invoice-am" class="table table-striped">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>AM</th>
			<th>Rechnung</th>
			<th>R. Datum</th>
			<th>Betrag</th>
			<th>Kommentar</th>
			<th>Abbuch.</th>
			<th>Umlegb.</th>
			<th>User</th>
			<th>Datum</th>
			{{-- <th></th>
			<th></th> --}}
			
			<th style="min-width: 175px;">Freigabe AM</th>
			<th style="min-width: 175px;">Freigabe HV</th>
			<th style="min-width: 175px;">Freigabe</th>
			<th style="min-width: 175px;">Freigabe Falk</th>

			<th>Aktion</th>
		</tr>
	</thead>
	<tbody>
		@if (!empty($data))
			@foreach ($data as $element)

				<?php 
					$download_path = "https://drive.google.com/drive/u/2/folders/".$element->file_basename;
	                if($element->file_type == "file"){
	                    $download_path = "https://drive.google.com/file/d/".$element->file_basename;
	                }

	                $subject = 'Rechnungsfreigabe: '.$element->name_of_property;
					$content = 'Rechnung: <a title="'.$element->invoice.'" href="'.$download_path.'" target="_blank">'.$element->invoice.'</a>';
					if($element->comment){
						$content .= '<br/>Kommentar User: '.$element->comment;
					}
				?>
				
				<tr>
					<td>
						<a href="{{route('properties.show',['property'=> $element->property_id])}}?tab=property_invoice">
                            {{$element->name_of_property}}
                        </a>
					</td>
					<td>
						<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $element->property_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="RECHNUNGSFREIGABEN">{{ $element->assetmanager }}</a>
					</td>
					<td>
						<a title="{{ $element->invoice }}" href="{{ $download_path }}" target="_blank">{{ $element->invoice }}</a>
					</td>
					<td>{{ show_date_format($element->date) }}</td>
					<td class="text-right">{{ show_number($element->amount,2) }}</td>
					<td>
						<span class="long-text">{{ $element->latest_comment }}</span>
						<!-- <br> -->
						<!-- <button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="{{ $element->id }}" data-property-id="{{ $element->property_id }}" data-type="property_invoices" data-subject="{{ $subject }}" data-content="">Kommentar</button> -->
					</td>
					<td><?php echo get_paid_checkbox($element->id,$element->need_to_pay);?></td>
					<td><?php echo get_paid_checkbox($element->id,$element->towards_tenant)."<br>".show_number($element->foldable,2) ?></td>
					<td>
						<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $element->property_id }}" data-user-id="{{ $element->user_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="RECHNUNGSFREIGABEN">{{ $element->name }}</a>
					</td>
					<td>{{ ($element->request2_date) ? show_datetime_format($element->request2_date) : show_datetime_format($element->created_at) }}</td>
					
					<!--<td>
						<button data-id="<?=$element->id?>" data-property_id="<?=$element->property_id?>" type="button" class="btn btn-success invoice-release-request-am" data-column="release1">Freigegeben</button>
						
					</td>
					<td>
						<button data-id="{{ $element->id }}" data-property_id="{{ $element->property_id }}" type="button" class="btn btn-primary mark-as-pending-am">Pending AM</button>
					</td>-->
					<td>
						@if(isset($element->button))
							{!! $element->button['btn_release_am'] !!}
						@endif
					</td>
					<td>
						@if(isset($element->button))
							{!! $element->button['btn_release_hv'] !!}
						@endif
					</td>
					<td>
						@if(isset($element->button))
							{!! $element->button['btn_release_usr'] !!}
						@endif
					</td>
					<td>
						@if(isset($element->button))
							{!! $element->button['btn_release_falk'] !!}
						@endif
					</td>
					<td>
						<button type="button" data-id="{{ $element->id }}" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>
					</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>
<input type="hidden" class="unpaidinvoice" value="{{count($data)}}">