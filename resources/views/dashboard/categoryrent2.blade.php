<div class="table-responsive">
	<div class="property-table">
    <table class="table table-striped dashboard-table category-table3">
        <thead>
        <tr>
            <th>Name</th>
            <th>Objekt</th>
            <th>Mieter geschl.</th>
            <th>Miet zahlung</th>
            <th>Kommentar</th>
            <th class="text-right">Mietausfall</th>
            <th>Kategorie</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
        <?php


        if(($row['rent_begin'] != '' && $row['rent_begin'] > date('Y-m-d')) ||($row['rent_end'] != '' && $row['rent_end'] < date('Y-m-d')))
              continue;



        if($row['actual_net_rent'] && $row['rental_space'])
        {

        if(!$row['category'])
            $row['category'] = "Nicht zugeordnet";

        ?>
        <tr>
            <td>{{$row['name']}}</td>
            <td><a href="{{route('properties.show',['property'=>$row['main_property_id']])}}?tab=">{{$row['name_of_property']}}</a></td>
            
            <td>@if($row['tenant_closed'])Ja @else {{'Nein'}}@endif</td>
            <td>{{$row['rent_payment']}}</td>
            <td>{{$row['comment3']}}</td>
            
            <td class="text-right">@if($row['actual_net_rent']){{ number_format($row['actual_net_rent'], 2,",",".") }}@else{{'0,00'}}@endif</td>
            <td>{{$row['category']}}</td>
        </tr>
    <?php } ?>
        @endforeach
        </tbody>
    </table>
    <p><strong>*1 = keine Probleme, 2 = evtl. Probleme, 3 = Mieter zahlt nicht</strong></p>
    </div>
</div>
