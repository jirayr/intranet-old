<div class="table-responsive">
	<div class="property-table">
    <a href="{{route('getcategoryproperty3')}}?category={{$_REQUEST['category']}}&export=1" class="btn btn-success pull-right" style="margin-bottom: 10px;"> Export</a>
    <table class="table table-striped dashboard-table category-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Objekt</th>
            <th>Mieter geschl.</th>
            <th class="text-right">Mietausfall</th>
            <th class="text-right">Fläche m²</th>
            <th>Bundesland</th>
            <th>Kategorie</th>
            <th>Kommentar</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
        <?php


        if(($row['rent_begin'] != '' && $row['rent_begin'] > date('Y-m-d')) ||($row['rent_end'] != '' && $row['rent_end'] < date('Y-m-d')))
              continue;



        if($row['actual_net_rent'] && $row['rental_space'])
        {

        if(!$row['category'])
            $row['category'] = "Nicht zugeordnet";

        ?>
        <tr>
            <td>{{$row['name']}}</td>
            <td><a href="{{route('properties.show',['property'=>$row['main_property_id']])}}?tab=tenancy-schedule">{{$row['name_of_property']}}</a></td>
            
            <td>@if($row['tenant_closed'])Ja @else {{'Nein'}}@endif</td>
            <td class="text-right">@if($row['actual_net_rent']){{ number_format($row['actual_net_rent'], 2,",",".") }}@else{{'0,00'}}@endif</td>
            <td class="text-right">@if($row['rental_space']){{ number_format($row['rental_space'], 2,",",".") }}@else{{'0,00'}}@endif</td>
            
            <td>{{$row['niedersachsen']}}</td>
            <td>{{$row['category']}}</td>
            <td>{{$row['comment3']}}</td>
        </tr>
    <?php } ?>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
