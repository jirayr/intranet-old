<div class="table-responsive">
    <div class="property-table">
        <table class="table table-striped dashboard-table" id="properties_detail_table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Objekt</th>
                    <th>Status</th>
                    <th>Empfänger: Firmenname</th>
                    <th>Empfänger: Ansprechpartner</th>
                    <th>Email</th>
                    <th>Kaufpreis</th>
                    <th>Maklerpreis</th>
                    <th>Datum LOI</th>
                    <th>LOI PDF</th>
                </tr>
            </thead>
            <tbody>
                <?php $sum1 = $sum2 = 0; ?>
                @if($property)
                    @foreach ($property as $key => $value)
                        <?php 
                            $value->price = ($value->price != '' && is_numeric($value->price)) ? $value->price : 0;
                            $value->gesamt_in_eur = ($value->gesamt_in_eur != '' && is_numeric($value->gesamt_in_eur)) ? $value->gesamt_in_eur : 0;
                        ?>
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td><a href="{{route('properties.show',['property'=>$value->id])}}?tab=email_template">{{ $value->name_of_property }}</a></td>
                            <td>
                                <?php 
                                    if($value->status == 15){
                                        $status = 'Quicksheet';
                                    }elseif($value->status == 7){
                                        $status = 'Angebot (<75%)';
                                    }elseif($value->status == 5){
                                        $status = 'Angebot (>75%)';
                                    }elseif($value->status == 14){
                                        $status = 'Exklusivität (95%)';
                                    }elseif($value->status == 16){
                                        $status = 'Liquiplanung (98%)';
                                    }elseif($value->status == 10){
                                        $status = 'Notartermin (99%)';
                                    }elseif($value->status == 12){
                                        $status = 'Beurkundet (100%)';
                                    }else{
                                        $status = $value->status;
                                    }
                                ?>
                                {{ $status }}
                            </td>
                            <td>{{ $value->address1 }}</td>
                            <td>{{ $value->address2 }}</td>
                            <td>
                                <?php 
                                    $receiver = '';
                                    if($value->receiver){
                                        $email_arr = explode(",", $value->receiver);
                                        $receiver_arr = (!empty($email_arr)) ? array_unique($email_arr) : [];
                                        $receiver = (!empty($receiver_arr)) ? implode(", ", $receiver_arr) : '';
                                    }
                                ?>
                                <a href="javascript:void(0);" class="btn-send-mail-loi-user" data-property-id="{{ $value->id }}">{{ $receiver }}</a>
                            </td>
                            <td class="text-right">{{ ($value->price) ? show_number($value->price) : show_number($value->gesamt_in_eur) }}

                            <?php
                            $am = ($value->price) ? $value->price : $value->gesamt_in_eur;
                            $sum1 +=$am;
                            ?>

                            </td>
                            <td class="text-right">{{ ($value->maklerpreis && is_numeric($value->maklerpreis)) ? show_number($value->maklerpreis) : 0 }}

                            <?php
                            $am = is_numeric($value->maklerpreis) ? $value->maklerpreis : 0;
                            $sum2 +=$am;
                            ?>


                            </td>
                            <td>{{ ($value->datum_lol) ? show_date_format($value->datum_lol) : '' }}</td>
                            <td>
                                <?php
                                    $pdf_arr = [];
                                    if($value->loi_pdf){
                                        $pdf_arr = explode(",", $value->loi_pdf);
                                    }
                                ?>
                                @if(!empty($pdf_arr))
                                    @foreach ($pdf_arr as $pdf)
                                        <a href="{{ asset($pdf) }}" target="_blank">{{ $pdf }} </a>
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            <tfoot>
                <th colspan="6">Gesamt</th>
                <th colspan="">{{show_number($sum1,2)}}</th>
                <th colspan="">{{show_number($sum2,2)}}</th>
                <th></th>
                <th></th>
            </tfoot>
        </table>
    </div>
</div>