<div class="table-responsive">
	<div class="property-table">
    <table class="table table-striped dashboard-table category-table4">
        <thead>
        <tr>
            <th></th>
            <th class="text-right">Anzahl</th>
            <th class="text-right">Netto Miete p.m.</th>
        </tr>
        </thead>
        <tbody>

        <tr>
            <td>Objekt geschlossen</td>
            <td class="text-right"><a href="javascript:void(0)" class="get-category-properties4" data-category="ja">{{ number_format($data2['ja_count'], 0,",",".") }}</a></td>
            <td class="text-right">{{ number_format($data2['ja_actual_net_rent'], 2,",",".") }}</td>
        </tr>
        <tr>
            <td>Objekt teilweise geschlossen</td>
            <td class="text-right"><a href="javascript:void(0)" class="get-category-properties4" data-category="other">{{ number_format($data2['other_count'], 0,",",".") }}</a></td>
            <td class="text-right">{{ number_format($data2['other_actual_net_rent'], 2,",",".") }}</td>
        </tr>
        <tr>
            <td>Objekt geöffnet</td>
            <td class="text-right"><a href="javascript:void(0)" class="get-category-properties4" data-category="nein">{{ number_format($data2['nein_count'], 0,",",".") }}</a></td>
            <td class="text-right">{{ number_format($data2['nein_actual_net_rent'], 2,",",".") }}</td>
        </tr>
        </tbody>
        <tfoot>
            <tr>
                <th>Gesamt</th>
                <th class="text-right">{{ number_format($data2['ja_count']+$data2['nein_count']+$data2['other_count'], 0,",",".") }}</th>
                <th class="text-right">{{ number_format($data2['ja_actual_net_rent']+$data2['nein_actual_net_rent']+$data2['other_actual_net_rent'], 2,",",".") }}</th>
            </tr>
        </tfoot>
    </table>
    </div>
</div>
