<div class="table-responsive">

<?php
$f = 0;
$total = 0;
$rent = 0;
?>
<table class="table table-striped" id="hausmaxx_listdata_table">
<thead>
<tr>
    <th class="text-center">#</th>
    <th>{{__('dashboard.property_name')}}</th>
    <th>AM</th>
    <th>Mieter</th>
    <th>Name</th>
    <th>Prozent</th>
    <th>Betrag</th>
    <th>Betrag/Jahr</th>
    <th>IST-NME</th>
    <th>HV Vertrag abgeschlossen</th>
    <th>HV kündbar bis</th>
    <th>HV läuft aus zum</th>
    <!-- <th>{{__('dashboard.transaction_manager')}}</th>
    <th>{{__('dashboard.total_purchase_price')}}</th>
    <th>{{__('dashboard.gross_return')}}</th>
    <th>{{__('dashboard.faktor')}}</th>
    <th>{{__('dashboard.guv_ref_factor')}}</th>
    <th>{{__('dashboard.ek_cf')}}</th>
    <th>EK Preis</th>
    <th>{{__('dashboard.maklerpreis')}}</th>
    <th>{{__('dashboard.price_difference')}}</th> -->
</tr>
</thead>
<tbody>
@foreach($data as $property)

<?php
// if(!is_numeric($property->hausmaxeuro_monat))
    // continue;

?>
@php 
            $live_total_avg_rent = 0 ;
            $m_count = 0;

            if($property->text_json)
            {
                $arr = json_decode($property->text_json,true);

                if(isset($arr['total_actual_net_rent']))
                $live_total_avg_rent = $arr['total_actual_net_rent']*12;

                if(isset($arr['businesscount']))
                $m_count = $arr['businesscount'] + $arr['livecount'];
            }

            $rent +=$live_total_avg_rent;
           @endphp 


<tr>
        <td class="text-center">{{ $property->id }}</td>
        <td >
            <a href="{{ url('/properties').'/'.$property->property_id }}">
            {{ $property->name_of_property }}
            </a>
        </td>
        
        <td class="text-right">

            <?php 
                    $subject = 'Hausverwaltungskosten: '.$property->name_of_property;
                    $content = '';
                ?>
                        <a href="javascript:void(0);" class="custom_user" data-property-id="{{ $property->id }}" data-user-id="{{ $property->asset_m_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="Hausverwaltungskosten">{{ $property->name }}</a>
        </td>
        <td class="text-right">
            <a href="javascript:void(0)" class="get-rents" data-value="0" data-type="r" data-property="{{ $property->id }}">
        {{number_format($m_count,0,",",".")}}
            </a>
        </td> 
            
        <td class="text-right">
            
         {{ $property->hausmaxx }}
        </td>
        
        <td class="text-right">
 


           

           @if(is_numeric($property->hausmaxeuro_monat) && $live_total_avg_rent)
        
            <?php

                if($live_total_avg_rent == 0){ $live_total_avg_rent = 1; }
            ?>
        
            {{ number_format( $property->hausmaxeuro_monat*12/ $live_total_avg_rent * 100, 2 ,",","."). '%' }}

           @endif
        </td>
        
        <td class="text-right">
            
             @if(is_numeric($property->hausmaxeuro_monat))
             <?php
             $total += $property->hausmaxeuro_monat;
             ?>
             {{ number_format( $property->hausmaxeuro_monat, 2 ,",",".") }}€/Monat
             @endif 
        </td>

        <td class="text-right">
            
             @if(is_numeric($property->hausmaxeuro_monat))
             {{ number_format( $property->hausmaxeuro_monat*12, 2 ,",",".") }}€/Jahr
             @endif 
        </td>
        <td class="text-right">
            
             {{ number_format( $live_total_avg_rent, 2 ,",",".") }}€
        </td>
        <td class="text-center"> {{ ($property->hv_vertrag_abgeschlossen) ? show_date_format($property->hv_vertrag_abgeschlossen) : '' }}</td>
        <td class="text-center"> {{ ($property->hv_kündbar_bis) ? show_date_format($property->hv_kündbar_bis) : '' }}</td>
        
        <td class="text-center"> {{  show_date_format($property->hv_expire_at)  }}</td>
        
    </tr>
    @endforeach
</tbody>

    <tr>
        <th>Summe</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th class="text-right">{{ number_format( $total, 2 ,",",".") }}€/Monat</th>
        <th></th>
        <th class="text-right">{{ number_format( $rent, 2 ,",",".") }}€</th>
        <th></th>
        <th></th>
    </tr>

</table>

@if(isset($property->hausmaxx))
<a href="{{route('export-properties-haus',$property->hausmaxx)}}" class="btn btn-primary">Export</a>
@endif


</div>
