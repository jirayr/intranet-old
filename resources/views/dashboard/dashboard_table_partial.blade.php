@if(isset($type) && $type==1)

    <table class="table table-striped dashboard-table ">
        <thead>
            <tr>
                <th style="width: 70px;" class="text-center">#</th>
                <th>PLZ</th>
                <th>Objekt</th>

                <!-- <th>{{__('dashboard.property_name')}}</th> -->
                @if($status==14)
                <th>Exkl.</th>
                @endif
                <th>Notar</th>
                <th>BNL</th>
                <th>TM</th>
                <th>AM</th>
                <th>GKP</th>
                <th>Eigenmittel in €</th>
                <th>Eigenmittel in %</th>
                <th>{{__('property.listing.status')}}</th>
                <th>{{__('property.listing.actions')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $property)

                <?php
                    if(!isset($property->bank_ids)){  continue; }
                    $propertiesExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();
                    $total_ccc = 0;

                    $neitoo = 0;
                    if($status==6 || $status==8){
                        $tenancy_schedule_data = \App\Services\TenancySchedulesService::get($property->property_actual_id,0);
                        foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
                            $neitoo += 12 * $tenancy_schedule->calculations['total_business_actual_net_rent'];
                            $neitoo += 12 * $tenancy_schedule->calculations['total_live_actual_net_rent'];
                    }
                ?>

                @foreach($propertiesExtra1s as $propertiesExtra1)
                    <?php
                        if($propertiesExtra1->is_current_net)
                            $total_ccc += $propertiesExtra1->net_rent_p_a;
                    ?>
                @endforeach

                <?php 
                    if($total_ccc)
                        $property->net_rent_pa = $total_ccc;
                    else
                        $total_ccc = $property->net_rent_pa;

                    $property->net_rent_increase_year1 = $total_ccc;
                    $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1*$property->net_rent;
                    $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2*$property->net_rent;
                    $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3*$property->net_rent;
                    $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4*$property->net_rent;

                    $property->operating_cost_increase_year1 = $property->net_rent_increase_year1*$property->operating_costs_nk;

                    $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1*$property->operating_costs;

                    $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2*$property->operating_costs;

                    $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3*$property->operating_costs;
                    $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4*$property->operating_costs;


                    $property->property_management_increase_year1 = $property->net_rent_increase_year1*$property->object_management_nk;

                    $property->property_management_increase_year2 =$property->property_management_increase_year1 +  $property->property_management_increase_year1*$property->object_management;

                    $property->property_management_increase_year3 =$property->property_management_increase_year2 +  $property->property_management_increase_year2*$property->object_management;

                    $property->property_management_increase_year4 =$property->property_management_increase_year3 +  $property->property_management_increase_year3*$property->object_management;

                    $property->property_management_increase_year5 =$property->property_management_increase_year4 +  $property->property_management_increase_year4*$property->object_management;


                    $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                    $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                    $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                    $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                    $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                    $property->ebit_year_1= $property->ebitda_year_1 - $property->depreciation_nk_money;
                    $property->ebit_year_2= $property->ebitda_year_2 - $property->depreciation_nk_money;
                    $property->ebit_year_3= $property->ebitda_year_3 - $property->depreciation_nk_money;
                    $property->ebit_year_4= $property->ebitda_year_4 - $property->depreciation_nk_money;
                    $property->ebit_year_5= $property->ebitda_year_5 - $property->depreciation_nk_money;

                    $D42 = $property->gesamt_in_eur
                        + ($property->real_estate_taxes * $property->gesamt_in_eur)
                        + ($property->estate_agents * $property->gesamt_in_eur)
                        + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                        + ($property->evaluation * $property->gesamt_in_eur)
                        + ($property->others * $property->gesamt_in_eur)
                        + ($property->buffer * $property->gesamt_in_eur);


                    $L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                    $price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

                    // if(isset($bank)){
                    $E18 = ($property->net_rent_increase_year1
                            -$property->maintenance_increase_year1
                            -$property->operating_cost_increase_year1
                            -$property->property_management_increase_year1)
                        -$property->depreciation_nk_money;

                    $D49 = $property->bank_loan * $D42;
                    $D48 = $property->from_bond * $D42;
                    $H48 = $D49 * $property->interest_bank_loan;
                    $H49 = $D49 * $property->eradication_bank;
                    $H52 = $D48 * $property->interest_bond;

                    $E23 = $E18- $H48 -$H52;
                    $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                    $G61 = ($D48 == 0) ? 0 : $E31/$D48;
                    $G62 = ($D48 == 0) ? 0 : $E23/$D48;
                    // }
                    $G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;

                    if(!isset($G61)){
                        $G61 = 0;
                    }
                ?>

                <tr>
                    <td class="text-center"><br>{{$property->property_actual_id}}</td>
                    <td class="" ><br>
                        <a href="{{route('properties.show',['property'=>$property->property_actual_id])}}">{{$property->plz_ort}}</a>
                    </td>
                    <td class="" ><br>
                    <a href="{{route('properties.show',['property'=>$property->property_actual_id])}}">{{$property->name_of_property}}</a>
                    </td>
                    @if($status==14)
                        <td><br>
                            @if($property->exklusivität_bis)
                                <?php echo show_date_format($property->exklusivität_bis); ?>
                            @endif
                        </td>
                    @endif
                        <td><br>
                            @if($property->purchase_date)
                                <?php echo show_date_format($property->purchase_date); ?>
                        @endif
                    </td>
                    <td>
                        <br>
                        <?php
                            $buyd =  DB::table('properties_buy_details')->whereRaw('(type=402) and property_id='.$property->property_actual_id)->first();
                            if($buyd && $buyd->comment){
                                echo show_date_format($buyd->comment);
                            }
                        ?>
                    </td>
                    <td>
                        <br>
                        @php
                            $user =  DB::table('properties')->join('users','users.id','=','properties.transaction_m_id')->where('properties.id', $property->property_actual_id)->first();
                             if($user){
                                 echo user_short_name($user->name);
                             }
                        @endphp

                    </td>
                    <td>
                        <br>
                        @php
                            $user =  DB::table('properties')->join('users','users.id','=','properties.asset_m_id')->where('properties.id', $property->property_actual_id)->first();

                            if($user){
                                echo user_short_name($user->name);
                            }
                        @endphp
                    </td>
                    <td class="number-right"><br>{{number_format($D42,0,",",".")}}€</td>
                    <td class="number-right"><br>{{number_format($D48,2,",",".")}}€</td>
                    <td class="number-right"><br>{{number_format($property->from_bond*100,2,",",".")}}%</td>
                    <td>
                        <br>
                        @if($property->property_actual_status == config('properties.status.buy'))
                            {{__('property.buy')}}
                        @elseif($property->property_actual_status == config('properties.status.hold'))
                            {{__('property.hold')}}
                        @elseif($property->property_actual_status == config('properties.status.decline'))
                            {{__('property.decline')}}
                        @elseif($property->property_actual_status == config('properties.status.declined'))
                            {{__('property.declined')}}
                        @elseif($property->property_actual_status == config('properties.status.offer'))
                            {{__('property.offer')}}
                        @elseif($property->property_actual_status == config('properties.status.duration'))
                            {{__('property.duration')}} 
                        @elseif($property->property_actual_status == config('properties.status.in_purchase'))
                              {{__('property.in_purchase')}} 
                        @elseif($property->property_actual_status == config('properties.status.sold'))
                            {{__('property.sold')}} 
                        @elseif($property->property_actual_status == config('properties.status.lost'))
                            {{__('property.lost')}} 
                        @elseif($property->property_actual_status == config('properties.status.exclusivity'))
                            {{__('property.exclusivity')}} 
                        @elseif($property->property_actual_status == config('properties.status.certified'))
                            {{__('property.certified')}} 
                        @elseif($property->property_actual_status == config('properties.status.exclusive'))
                            {{__('property.exclusive')}} 
                        @elseif($property->property_actual_status == config('properties.status.quicksheet'))
                            {{__('property.quicksheet')}} 
                        @elseif($property->property_actual_status == config('properties.status.liquiplanung'))
                            {{__('property.liquiplanung')}} 
                        @elseif($property->property_actual_status == config('properties.status.externquicksheet'))
                            {{__('property.externquicksheet')}}
                        @endif
                    </td>
                    <td>
                        {!! Form::open(['action' => ['PropertiesController@destroy', $property->property_actual_id],
                        'method' => 'DELETE']) !!}
                            <button onclick="location.href='{{route('properties.show',['property'=>$property->property_actual_id])}}'" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-eye"></i></button>
                        {!! Form::close() !!}

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@else

     @if($custom_status)
        <a href="javascript::void(0);" data-url="{{route('getstatustable')}}?export=1&status={{$status*100}}&property_status={{$property_status}}" class="pull-right btn btn-success btn-export">Export</a>
    @elseif($status)
        <a href="javascript::void(0);" data-url="{{route('getstatustable')}}?export=1&status={{$status}}&property_status={{$property_status}}" class="pull-right btn btn-success btn-export">Export</a>
    @endif

    <?php
        $f = 1;
        if($status==8){
            $custom_status=1;
            $status=6;
            $f = 0;
        }
    ?>

    <table class="table table-striped dashboard-table">
        <thead>
            <tr>
                <th style="width: 70px;" class="text-center">#</th>
                <th>PLZ</th>
                <th>Objekt</th>
                <!-- <th>{{__('dashboard.property_name')}}</th> -->

                @if($status==15)
                    <th>Erst. D.</th>
                @endif

                @if($status==16)
                    <th>Exkl. bis</th>
                    <th>Notar</th>
                    <th>BNL</th>
                @endif

                @if($custom_status)
                    @if($f)
                        <th>Exkl. bis</th>
                    @endif
                    <th>Notar</th>
                    <th>BNL</th>
                    <th>{{__('dashboard.seller')}}</th>
                @elseif($status >=1 || $status <=14)
                    <th>TM</th>
                @endif
                
                {{-- @if($status==config('properties.status.duration') || $status==12 || $status==8) --}}
            
                @if($status==15 || $status==7 || $status==5)
                    <th>LOI Datum</th>
                @else
                    <th>AM</th>
                @endif

                @if($status==config('properties.status.in_sale') || $status==config('properties.status.sold')  )
                    <th>{{__('dashboard.seller')}}</th>
                @endif

                <th>EK Preis</th>
            
                <!-- <th>{{__('dashboard.transaction_manager')}}</th> -->
                <th>{{__('dashboard.total_purchase_price')}}</th>
                <th>{{__('dashboard.gross_return')}}</th>
                <!--<th>{{__('dashboard.faktor')}}</th>-->
                <th>
                    <!-- {{__('dashboard.guv_ref_factor')}}  -->
                    EK/GuV
                </th>
                <th>{{__('dashboard.ek_cf')}}</th>

                @if($status != "6" && $status != "8")
                    @if($status != "11" )
                        <th class="{{ $status }}">{{__('dashboard.maklerpreis')}}</th>
                    @endif
                @endif

                @if($status == "6" || $status == "8" )
                    <th>Miete p.a.</th>
                    <th>Verkehrswert</th>
                    <th>VK Preis</th>
                @endif
                @if($status == "11" )
                    <th>Verkehrswert</th>
                @endif

                @if($status != "6" && $status != "8" )
                    @if($status != "11" )
                        <th class="{{ $status }}">{{__('dashboard.price_difference')}}</th>
                    @endif
                @endif

                @if($status==14)
                    <th>D. Exkl.</th>
                @endif

                @if($status==10)
                    <th>Notartermin</th>
                @endif

                <th>{{__('property.listing.status')}}</th>
                <th>{{__('property.listing.actions')}}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $property)

                @if($custom_status)
                    <?php
                        $d['101'] = $d['102'] = $d['103'] = "";
                        $dates =  DB::table('properties_sale_details')->whereRaw('(type=101 OR type=102 OR type=103) and property_id='.$property->property_actual_id)->get();

                        foreach ($dates as $key => $value) {
                            if($value->comment)
                            $d[$value->type] = show_date_format($value->comment);
                        }
                        $check_date = ($d['101'] != '') ? date('Y', strtotime($d['101'])) : '';
                    ?>
                @endif

                @if( ($actual_status != 8) || ($actual_status == 8 && $subtype == 1 && $check_date != 2020) || ($actual_status == 8 && $subtype == 2 && $check_date == 2020) )

                    <?php
                        if(!isset($property->bank_ids)){  continue; }
                        $propertiesExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();
                        $total_ccc = 0;

                        $neitoo = 0;
                        if($status==6 || $status==8){
                            $tenancy_schedule_data = \App\Services\TenancySchedulesService::get($property->property_actual_id,0);
                            foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule)
                                $neitoo += 12 * $tenancy_schedule->calculations['total_business_actual_net_rent'];
                                $neitoo += 12 * $tenancy_schedule->calculations['total_live_actual_net_rent'];
                        }
                    ?>

                    @foreach($propertiesExtra1s as $propertiesExtra1)
                        <?php
                            if($propertiesExtra1->is_current_net)
                                $total_ccc += $propertiesExtra1->net_rent_p_a;
                        ?>
                    @endforeach

                    <?php 
                        if($total_ccc)
                            $property->net_rent_pa = $total_ccc;
                        else
                            $total_ccc = $property->net_rent_pa;

                        $property->net_rent_increase_year1 = $total_ccc;
                        $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1*$property->net_rent;
                        $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2*$property->net_rent;
                        $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3*$property->net_rent;
                        $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4*$property->net_rent;


                        $property->operating_cost_increase_year1 = $property->net_rent_increase_year1*$property->operating_costs_nk;

                        $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1*$property->operating_costs;

                        $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2*$property->operating_costs;

                        $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3*$property->operating_costs;
                        $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4*$property->operating_costs;


                        $property->property_management_increase_year1 = $property->net_rent_increase_year1*$property->object_management_nk;

                        $property->property_management_increase_year2 =$property->property_management_increase_year1 +  $property->property_management_increase_year1*$property->object_management;

                        $property->property_management_increase_year3 =$property->property_management_increase_year2 +  $property->property_management_increase_year2*$property->object_management;

                        $property->property_management_increase_year4 =$property->property_management_increase_year3 +  $property->property_management_increase_year3*$property->object_management;

                        $property->property_management_increase_year5 =$property->property_management_increase_year4 +  $property->property_management_increase_year4*$property->object_management;


                        $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                        $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                        $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                        $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                        $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                        $property->ebit_year_1= $property->ebitda_year_1 - $property->depreciation_nk_money;
                        $property->ebit_year_2= $property->ebitda_year_2 - $property->depreciation_nk_money;
                        $property->ebit_year_3= $property->ebitda_year_3 - $property->depreciation_nk_money;
                        $property->ebit_year_4= $property->ebitda_year_4 - $property->depreciation_nk_money;
                        $property->ebit_year_5= $property->ebitda_year_5 - $property->depreciation_nk_money;

                        /*if(json_decode($property->bank_ids,true)){
                        $bank_ids = json_decode($property->bank_ids,true);
                        if($bank_ids != null) {
                            foreach ($banks as $b) {
                                if(isset($bank_ids[0]) && $b->id == $bank_ids[0]){
                                    $bank = $b;
                                    break;
                                }
                            }
                        }
                        }*/

                        $D42 = $property->gesamt_in_eur
                            + ($property->real_estate_taxes * $property->gesamt_in_eur)
                            + ($property->estate_agents * $property->gesamt_in_eur)
                            + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                            + ($property->evaluation * $property->gesamt_in_eur)
                            + ($property->others * $property->gesamt_in_eur)
                            + ($property->buffer * $property->gesamt_in_eur);


                        $L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                        $price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

                        // if(isset($bank)){
                        $E18 = ($property->net_rent_increase_year1
                                -$property->maintenance_increase_year1
                                -$property->operating_cost_increase_year1
                                -$property->property_management_increase_year1)
                            -$property->depreciation_nk_money;

                        $D49 = $property->bank_loan * $D42;
                        $D48 = $property->from_bond * $D42;
                        $H48 = $D49 * $property->interest_bank_loan;
                        $H49 = $D49 * $property->eradication_bank;
                        $H52 = $D48 * $property->interest_bond;

                        $E23 = $E18- $H48 -$H52;
                        $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                        $G61 = ($D48 == 0) ? 0 : $E31/$D48;
                        $G62 = ($D48 == 0) ? 0 : $E23/$D48;
                        // }
                        $G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;

                        if(!isset($G61)){
                            $G61 = 0;
                        }
                    ?>

                    <tr>
                        <td class="text-center">
                            <br>
                            @if($status==config('properties.status.in_sale') || $status==6 || $status==config('properties.status.sold')  )
                                @if($ids && $property->adid && in_array($property->adid,$ids))
                                    <i class="mdi mdi-brightness-1 text-success"></i>
                                @else
                                    <i class="mdi mdi-brightness-1 text-danger"></i>
                                @endif
                            @endif
                            {{$property->property_actual_id}}
                        </td>

                        <td class="" ><br>
                            <a href="{{route('properties.show',['property'=>$property->property_actual_id])}}">{{$property->plz_ort}}</a>
                        </td>
                        <td class="" >
                            <br>
                            <a href="{{route('properties.show',['property'=>$property->property_actual_id])}}">{{$property->name_of_property}}</a>
                        </td>

                        @if($status==15)
                            <td><br>{{show_date_format($property->created_at)}}</td>
                        @endif

                        @if($status==16)
                            <td>
                                <br>
                                @if($property->exklusivität_bis)
                                    <?php echo show_date_format($property->exklusivität_bis);?>
                                @endif
                            </td>
                            <td>
                                <br>
                                @if($property->purchase_date)
                                    <?php echo show_date_format($property->purchase_date); ?>
                                @endif
                            </td>
                            <td>
                                <br>
                                <?php
                                    $buyd =  DB::table('properties_buy_details')->whereRaw('(type=402) and property_id='.$property->property_actual_id)->first();
                                    if($buyd && $buyd->comment){
                                        echo show_date_format($buyd->comment);
                                    }
                                ?>
                            </td>
                        @endif
                        
                        <!-- <td class="" ><br>
                            <a href="{{route('properties.show',['property'=>$property->property_actual_id])}}">
                            {{$property->name_of_property}}
                            </a>
                        </td> -->

                            
                        @if($custom_status)

                            @if($f)
                                <td><br>{{$d['103']}}</td>
                            @endif
                                <td><br>{{$d['101']}}</td>
                                <td><br>{{$d['102']}}</td>
                                <td>
                                    <br>
                                    @php
                                        $user =  DB::table('properties')->join('users','users.id','=','properties.seller_id')->where('properties.id', $property->property_actual_id)->first();
                                        if($user){
                                            echo user_short_name($user->name);
                                        }
                                    @endphp
                                </td>
                        @elseif($status >=1 || $status <=14)
                            <td>
                                <br>
                                @php
                                    $user =  DB::table('properties')->join('users','users.id','=','properties.transaction_m_id')->where('properties.id', $property->property_actual_id)->first();
                                    if($user){
                                        echo user_short_name($user->name);
                                    }
                                @endphp
                            </td>
                        @endif

                        {{-- @if($status==config('properties.status.duration')  || $status==12 || $status==8) --}}
                        @if($status==15 || $status==7 || $status==5)
                            <td>
                                <br>
                                @php
                                    $loi =  DB::table('status_loi')->where('property_id', $property->property_actual_id)->first();
                                    if($loi){
                                        echo show_date_format($loi->date);
                                    }
                                @endphp
                            </td>
                        @else
                            <td>
                                <br>
                                @php
                                $user =  DB::table('properties')->join('users','users.id','=','properties.asset_m_id')->where('properties.id', $property->property_actual_id)->first();
                                    if($user){
                                        echo user_short_name($user->name);
                                    }
                                @endphp
                            </td>
                        @endif

                        @if($status==config('properties.status.in_sale') || $status==config('properties.status.sold')  )

                            <td>
                                <br>
                                @php
                                    $user =  DB::table('properties')->join('users','users.id','=','properties.seller_id')->where('properties.id', $property->property_actual_id)->first();
                                     if($user){
                                         echo user_short_name($user->name);
                                     }
                                @endphp
                            </td>

                        @endif

                        <!-- {{$property->name_of_creator}} -->
                        <td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
                        <td class="number-right"><br>{{number_format($D42,0,",",".")}}€</td>
                        <td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
                        <!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
                        <td class="number-right"><br>
                            {{number_format(($G62 * 100),2,",",".")}}%
                            <?php
                                //J59*(D42/J60)
                                /*if($property->AHK_Salzgitter != 0){
                                    $guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
                                                            + ($property->real_estate_taxes * $property->gesamt_in_eur)
                                                            + ($property->estate_agents * $property->gesamt_in_eur)
                                                            + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                                                            + ($property->evaluation * $property->gesamt_in_eur)
                                                            + ($property->others * $property->gesamt_in_eur)
                                                            + ($property->buffer * $property->gesamt_in_eur))
                                                    /($property->AHK_Salzgitter)));
                                    if($guv_faktor != 0){
                                        echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
                                    }else echo "0";
                                }else echo "0";*/
                            ?>
                        </td>
                        <td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>


                        @if($status != "6" && $status != "8" )
                            @if($status != "11" )
                                <td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
                            @endif
                        @endif

                        @if($status == "6" || $status == "8" )
                            <td class="number-right"><br>{{number_format(($neitoo),0,",",".")}}€</td>
                            <td class="number-right"><br>{{number_format(($property->market_value),0,",",".")}}€</td>
                            <td class="number-right"><br>{{number_format(($property->preisverk),0,",",".")}}€</td>
                        @endif

                        @if($status == "11" )
                            <td class="number-right"><br>{{number_format(($property->preisverk),0,",",".")}}€</td>
                        @endif
                    
                        @if($status != "6" && $status != "8" )
                            @if($status != "11" )
                                <td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
                            @endif
                        @endif

                        @if($status == "14")
                            <td class="number-right"><br>
                                @if($property->exklusivität_bis)
                                    {{show_date_format($property->exklusivität_bis)}}
                                @endif
                            </td>
                        @endif

                        @if($status == "10")
                            <td class="number-right"><br>
                                @if($property->purchase_date && $property->purchase_date!="0000-00-00")
                                    {{show_date_format($property->purchase_date)}}
                                @endif
                            </td>
                        @endif                            

                        <td>
                            <br>
                            @if($property->property_actual_status == config('properties.status.buy'))
                                {{__('property.buy')}}
                            @elseif($property->property_actual_status == config('properties.status.hold'))
                                {{__('property.hold')}}
                            @elseif($property->property_actual_status == config('properties.status.decline'))
                                {{__('property.decline')}}
                            @elseif($property->property_actual_status == config('properties.status.declined'))
                                {{__('property.declined')}}
                            @elseif($property->property_actual_status == config('properties.status.offer'))
                                {{__('property.offer')}}
                            @elseif($property->property_actual_status == config('properties.status.duration'))
                                {{__('property.duration')}} 
                            @elseif($property->property_actual_status == config('properties.status.in_purchase'))  
                                {{__('property.in_purchase')}} 
                            @elseif($property->property_actual_status == config('properties.status.sold'))
                                {{__('property.sold')}} 
                            @elseif($property->property_actual_status == config('properties.status.lost'))
                                {{__('property.lost')}} 
                            @elseif($property->property_actual_status == config('properties.status.exclusivity'))
                                {{__('property.exclusivity')}} 
                            @elseif($property->property_actual_status == config('properties.status.certified'))
                                {{__('property.certified')}} 
                            @elseif($property->property_actual_status == config('properties.status.exclusive'))
                                {{__('property.exclusive')}} 
                            @elseif($property->property_actual_status == config('properties.status.quicksheet'))
                                {{__('property.quicksheet')}} 
                            @elseif($property->property_actual_status == config('properties.status.liquiplanung'))
                                {{__('property.liquiplanung')}} 
                            @elseif($property->property_actual_status == config('properties.status.externquicksheet'))
                                {{__('property.externquicksheet')}}
                            @endif</td>
                        <td>
                            {!! Form::open(['action' => ['PropertiesController@destroy', $property->property_actual_id],
                            'method' => 'DELETE']) !!}
                                <button onclick="location.href='{{route('properties.show',['property'=>$property->property_actual_id])}}'" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-eye"></i></button>

                                @if(Auth::user()->role==1 && $status!=6 )
                                    <button type="submit" class="btn btn-info btn-outline btn-circle btn-lg m-r-5" onclick="return confirm('{{__("property.confirm")}}')"><i class="icon-trash"></i></button>
                                @endif
                            {!! Form::close() !!}
                        </td>
                    </tr>

                @endif

            @endforeach
        </tbody>
    </table>

@endif