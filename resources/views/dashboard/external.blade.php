@extends('layouts.admin')
@section('css')
	<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<!-- Styles -->
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <style type="text/css">
        .table-responsive h2{
            width: 520px;
            float: left;
            font-size: 19px !important;
        }
        .table-responsive h2 label{
            width: 280px;
        }
        .table-responsive h2 a.show-link{
            width: 60px;
            font-size: 16px;
        }
        .table-responsive{
            padding: 0px 15px !important;
        }
        .totalamount{
            float: right;
            text-align: right;
        }
        .two-part li{
            width: 68% !important;
        }
        .two-part li:first-child{
            width: 30% !important;
        }
    </style>
    
@endsection
@section('content')
<?php
 function timeAgo($timestamp){
		 $datetime1=new DateTime("now");
		 $datetime2=new DateTime();
		 $datetime2->setTimestamp($timestamp);
		
		$diff=date_diff($datetime1, $datetime2);
		$timemsg='';
		if($diff->y > 0){
			$timemsg = $diff->y .' year'. ($diff->y > 1?"'s":'');

		}
		else if($diff->m > 0){
		 $timemsg = $diff->m . ' month'. ($diff->m > 1?"'s":'');
		}
		else if($diff->d > 0){
		 $timemsg = $diff->d .' day'. ($diff->d > 1?"'s":'');
		}
		else if($diff->h > 0){
		 $timemsg = $diff->h .' hour'.($diff->h > 1 ? "'s":'');
		}
		else if($diff->i > 0){
		 $timemsg = $diff->i .' minute'. ($diff->i > 1?"'s":'');
		}
		else if($diff->s > 0){
		 $timemsg = $diff->s .' second'. ($diff->s > 1?"'s":'');
		}

	$timemsg = $timemsg.' ago';
	return $timemsg;
	}

?>
    <div class="row" id="dashboard-contain">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif
		
        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif




        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">{{__('dashboard.transactionmanagement')}}</div>
					<div class="row">	
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">{{__('property.in_purchase')}}</h3>
                                <ul class="list-inline two-part">
                                    <li>
                                        <div class="sparklinedash"></div>
                                    </li>
                                    <li class="text-right">
                                        <span class="text-success">
                                            @if(isset($ek_analysis_array[config('properties.status.in_purchase')]))
                                                {{number_format( $ek_analysis_array[config('properties.status.in_purchase')] , 0 ,",",".")}}€
                                                @else
                                                {{number_format( 0 , 0 ,",",".")}}€
                                                @endif
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-4 col-sm-6 col-xs-12">
							<div class="white-box analytics-info">
								<h3 class="box-title">{{__('property.offer')}}</h3>
								<ul class="list-inline two-part">
									<li>
										<div class="sparklinedash"></div>
									</li>
									<li class="text-right">
										<span class="text-success">
                                                
                                                @if(isset($ek_analysis_array[config('properties.status.offer')]))
                                                {{number_format( $ek_analysis_array[config('properties.status.offer')] , 0 ,",",".")}}€
                                                @else
                                                {{number_format( 0 , 0 ,",",".")}}€
                                                @endif
										</span>
									</li>
								</ul>
							</div>
						</div>

                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">{{__('property.exclusivity')}} </h3>

                                <ul class="list-inline two-part">
                                    <li>
                                        <div class="sparklinedash"></div>
                                    </li>
                                    <li class="text-right">
                                        <span class="text-success">

                                                @if(isset($ek_analysis_array[config('properties.status.exclusivity')]))
                                                {{number_format( $ek_analysis_array[config('properties.status.exclusivity')] , 0 ,",",".")}}€
                                                @else
                                                {{number_format( 0 , 0 ,",",".")}}€
                                                @endif

                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        

						<div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">{{__('property.exclusive')}}</h3>


                                <ul class="list-inline two-part">
                                    <li>
                                        <div class="sparklinedash"></div>
                                    </li>
                                    <li class="text-right">
                                        <span class="text-success">
                                            @if(isset($ek_analysis_array[config('properties.status.exclusive')]))
                            {{number_format( $ek_analysis_array[config('properties.status.exclusive')] , 0 ,",",".")}}€
                            @else
                            {{number_format( 0 , 0 ,",",".")}}€
                            @endif
                                            
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>




                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">{{__('property.certified')}}</h3>


                                <ul class="list-inline two-part">
                                    <li>
                                        <div class="sparklinedash"></div>
                                    </li>
                                    <li class="text-right">
                                        <span class="text-success">
                                            @if(isset($ek_analysis_array[config('properties.status.certified')]))
                            {{number_format( $ek_analysis_array[config('properties.status.certified')] , 0 ,",",".")}}€
                            @else
                            {{number_format( 0 , 0 ,",",".")}}€
                            @endif
                                            
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
						<div class="col-lg-4 col-sm-6 col-xs-12">
							<div class="white-box analytics-info">
								<h3 class="box-title">{{__('property.duration')}}</h3>
								<ul class="list-inline two-part">
									<li>
										<div class="sparklinedash"></div>
									</li>
									<li class="text-right">
                                        <span class="text-success">
                                            <?php
                                            $add_bestand = 0;
                                            if(isset($ek_analysis_array[6]))
                                                $add_bestand = $ek_analysis_array[6];

                                            if(isset($ek_analysis_array[config('properties.status.in_sale')]))
                                                $add_bestand +=$ek_analysis_array[config('properties.status.in_sale')];

                                            ?>
                                               EK {{number_format( $add_bestand , 0 ,",",".")}}€
                                            
                                        </span>
                                        <br>
										<span class="text-success">
                                              VK  {{number_format( $bestand_total , 0 ,",",".")}}€
                                                
                                                
										</span>
									</li>
								</ul>
							</div>
						</div>

						

						

						
					</div>
					<!--/.row -->

				<div class="table-responsive">
				<input type="text" class="form-control search-text" style="float: left;width: 280px;">
				<button type="button" class="btn btn-primary search-prop">Suchen</button>
				<!-- <a target="_blank" href="{{route('export_property_pdf')}}" class="btn btn-info">Transaction Meeting</a> -->

				<!-- <a target="_blank" href="{{route('factsheet')}}" class="btn btn-success">Export Verkaufsobjekte</a> -->

				

				<div class="search-result" style="margin-top: 10px;"></div>
				
				</div>



                <div class="table-responsive" style="margin-top: 10px;">
                    <h2>
						<label>{{__('dashboard.status')}}: All</label>
						<span class="totalamount">
							<?php


							$add_bestand = 0;
							if(isset($ek_analysis_array[6]))
								$add_bestand = $ek_analysis_array[6];

							if(isset($ek_analysis_array[config('properties.status.in_sale')]))
								$add_bestand +=$ek_analysis_array[config('properties.status.in_sale')];

												



							$ts = 0;
							foreach ($ek_analysis_array as $key => $value) {
								// if($key==6)
								//     $ts+=$bestand_total;
								// else
									$ts += $value;
							}
							?>
								{{number_format( $ts , 0 ,",",".")}}€
								
						</span>
						<a href="javascript:void(0)" data-status="0" class="btn btn-info test show-link">{{$total_count}} <i class="fa fa-angle-down"></i></a>
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table ">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>

									

									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>



				<div class="table-responsive">
                    <h2>
						<label>{{__('dashboard.status')}}: {{__('property.quicksheet')}}</label>
						<span class="totalamount">
							
								@if(isset($ek_analysis_array[config('properties.status.quicksheet')]))
								{{number_format( $ek_analysis_array[config('properties.status.quicksheet')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
								
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.quicksheet')}}" class="btn btn-info test show-link">@if(isset($properties_count[config('properties.status.quicksheet')]))
								{{$properties_count[config('properties.status.quicksheet')]}}
							@else
								0
							@endif <i class="fa fa-angle-down"></i></a>
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table ">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>

									

									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>


                
               

                <div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.in_purchase')}}</label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.in_purchase')]))
								{{number_format( $ek_analysis_array[config('properties.status.in_purchase')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
						</span>

						<!-- {{config('properties.status.in_purchase')}} -->
						<a href="javascript:void(0)" data-status="{{config('properties.status.in_purchase')}}" class="btn btn-info show-link">
							@if(isset($properties_count[config('properties.status.in_purchase')]))
								{{$properties_count[config('properties.status.in_purchase')]}}
							@else
								0
							@endif <i class="fa fa-angle-down"></i>
						</a> 
					</h2>
					<div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
					</div>
				</div>

                    


				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.offer')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.offer')]))
								{{number_format( $ek_analysis_array[config('properties.status.offer')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.offer')}}" class="btn btn-info show-link">
							@if(isset($properties_count[config('properties.status.offer')]))
							{{$properties_count[config('properties.status.offer')]}}
							@else
							0
							@endif  <i class="fa fa-angle-down"></i>
						</a>                    
					</h2>
					<div class="property-table hidden">

						<table class="table table-striped dashboard-table ">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>

						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
					</div>
				</div>
				
				
				
				
				


				



				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.exclusive')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.exclusive')]))
								{{number_format( $ek_analysis_array[config('properties.status.exclusive')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.exclusive')}}" class="btn btn-info show-link">
							@if(isset($properties_count[config('properties.status.exclusive')]))
							{{$properties_count[config('properties.status.exclusive')]}}
							@else
							0
							@endif  <i class="fa fa-angle-down"></i>
						</a>                    
					</h2>
					<div class="property-table hidden">

						<table class="table table-striped dashboard-table ">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>

						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
					</div>
				</div>

				<div class="table-responsive">
                    <h2>
						<label>{{__('dashboard.status')}}: {{__('property.liquiplanung')}}</label>
						<span class="totalamount">
							
								@if(isset($ek_analysis_array[config('properties.status.liquiplanung')]))
								{{number_format( $ek_analysis_array[config('properties.status.liquiplanung')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
								
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.liquiplanung')}}" class="btn btn-info test show-link">@if(isset($properties_count[config('properties.status.liquiplanung')]))
								{{$properties_count[config('properties.status.liquiplanung')]}}
							@else
								0
							@endif <i class="fa fa-angle-down"></i></a>
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table ">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>

									

									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>





				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.exclusivity')}} </label>

						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.exclusivity')]))
								{{number_format( $ek_analysis_array[config('properties.status.exclusivity')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
						</span>
							<a href="javascript:void(0)" data-status="{{config('properties.status.exclusivity')}}" class="btn btn-info show-link">@if(isset($properties_count[config('properties.status.exclusivity')]))
							{{$properties_count[config('properties.status.exclusivity')]}}
							@else
							0
							@endif <i class="fa fa-angle-down"></i>
						</a>
					</h2>
					<div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
							<tr>
								<th style="width: 70px;" class="text-center">#</th>
								<th>{{__('dashboard.property_name')}}</th>
								<th>{{__('dashboard.transaction_manager')}}</th>
								<th>{{__('dashboard.total_purchase_price')}}</th>
								<th>{{__('dashboard.gross_return')}}</th>
								<!--<th>{{__('dashboard.faktor')}}</th>-->
								<th>{{__('dashboard.guv_ref_factor')}}</th>
								<th>{{__('dashboard.ek_cf')}}</th>
								<th>{{__('dashboard.maklerpreis')}}</th>
								<th>{{__('dashboard.angebotspreis')}}</th>
								<th>{{__('dashboard.price_difference')}}</th>
								<th>{{__('property.listing.status')}}</th>
								<th>{{__('property.listing.actions')}}</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>


 

				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.certified')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.certified')]))
								{{number_format( $ek_analysis_array[config('properties.status.certified')] , 0 ,",",".")}}€
							@else
								{{number_format( 0 , 0 ,",",".")}}€
							@endif
						</span>
							<a href="javascript:void(0)" data-status="{{config('properties.status.certified')}}" class="btn btn-info show-link">@if(isset($properties_count[config('properties.status.certified')]))
								{{$properties_count[config('properties.status.certified')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>
					</h2>
					<div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>


				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.duration')}} </label>
						<span class="totalamount">
							{{number_format($add_bestand , 0 ,",",".")}}€
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.duration')}}" class="btn btn-info show-link">				 @if(isset($properties_count[config('properties.status.duration')]))
							{{$properties_count[config('properties.status.duration')]}}
						@else
							0
						@endif 
						<i class="fa fa-angle-down"></i>
						</a>                   
					</h2>
					<h2 style="float: left;width: 220px;text-align: right;">VK {{number_format($bestand_total , 0 ,",",".")}}€</h2>
					<div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.asset_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>


				<div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: Exklusivität (Bestand) </label>
						<span class="totalamount">
							{{number_format($bestandcustomamount , 0 ,",",".")}}€
						</span>
						<a href="javascript:void(0)" data-status="600" class="btn btn-info show-link">				 {{$bestandcustomcount}}
						<i class="fa fa-angle-down"></i>
						</a>                   
					</h2>
					<h2 style="float: left;width: 220px;text-align: right;">VK {{number_format($bestandcustomvk , 0 ,",",".")}}€</h2>
					<div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.asset_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>


                <div class="table-responsive hidden">
                    <h2>
						<label>{{__('dashboard.status')}}: {{__('property.in_sale')}} </label>
                        <span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.in_sale')]))
								{{number_format( $ek_analysis_array[config('properties.status.in_sale')] , 0 ,",",".")}}€
                            @else
								{{number_format( 0 , 0 ,",",".")}}€
                            @endif
						</span>
                        <a href="javascript:void(0)" data-status="{{config('properties.status.in_sale')}}" class="btn btn-info show-link">
							@if(isset($properties_count[config('properties.status.in_sale')]))
								{{$properties_count[config('properties.status.in_sale')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
							<tr>
								<th style="width: 70px;" class="text-center">#</th>
								<th>{{__('dashboard.property_name')}}</th>
								<th>{{__('dashboard.seller')}}</th>
								<th>{{__('dashboard.total_purchase_price')}}</th>
								<th>{{__('dashboard.gross_return')}}</th>
								<th>{{__('dashboard.faktor')}}</th>
								<th>{{__('dashboard.guv_ref_factor')}}</th>
								<th>{{__('dashboard.ek_cf')}}</th>
								<th>{{__('dashboard.maklerpreis')}}</th>
								<th>{{__('dashboard.angebotspreis')}}</th>
								<th>{{__('dashboard.price_difference')}}</th>
								<th>{{__('property.listing.status')}}</th>
								<th>{{__('property.listing.actions')}}</th>
							</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
                </div>

                <div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.sold')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.sold')]))
								{{number_format( $ek_analysis_array[config('properties.status.sold')] , 0 ,",",".")}}€
								@else
								{{number_format( 0 , 0 ,",",".")}}€
								@endif
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.sold')}}" class="btn btn-info show-link">	
							@if(isset($properties_count[config('properties.status.sold')]))
								{{$properties_count[config('properties.status.sold')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>
                    </h2>
                    <h2 style="float: left;width: 220px;text-align: right;">VK {{number_format($vc_total , 0 ,",",".")}}€</h2> 
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
							<tr>
								<th style="width: 70px;" class="text-center">#</th>
								<th>{{__('dashboard.property_name')}}</th>
								<th>{{__('dashboard.seller')}}</th>
								<th>{{__('dashboard.total_purchase_price')}}</th>
								<th>{{__('dashboard.gross_return')}}</th>
								<!--<th>{{__('dashboard.faktor')}}</th>-->
								<th>{{__('dashboard.guv_ref_factor')}}</th>
								<th>{{__('dashboard.ek_cf')}}</th>
								<th>{{__('dashboard.maklerpreis')}}</th>
								<th>{{__('dashboard.angebotspreis')}}</th>
								<th>{{__('dashboard.price_difference')}}</th>
								<th>{{__('property.listing.status')}}</th>
								<th>{{__('property.listing.actions')}}</th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
					</div>
                </div>

                <div class="table-responsive">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.declined')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.declined')]))
								{{number_format( $ek_analysis_array[config('properties.status.declined')] , 0 ,",",".")}}€
							@else
								{{number_format( 0 , 0 ,",",".")}}€
							@endif
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.declined')}}" class="btn btn-info show-link">
							@if(isset($properties_count[config('properties.status.declined')]))
								{{$properties_count[config('properties.status.declined')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
						</table>
						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
                    </div>
				</div>

                <div class="table-responsive here">
					<h2>
						<label>{{__('dashboard.status')}}: {{__('property.lost')}} </label>
						<span class="totalamount">
							@if(isset($ek_analysis_array[config('properties.status.lost')]))
								{{number_format( $ek_analysis_array[config('properties.status.lost')] , 0 ,",",".")}}€
							@else
								{{number_format( 0 , 0 ,",",".")}}€
							@endif
						</span>
						<a href="javascript:void(0)" data-status="{{config('properties.status.lost')}}" class="btn btn-info show-link">	
							@if(isset($properties_count[config('properties.status.lost')]))
								{{$properties_count[config('properties.status.lost')]}}
							@else
								0
							@endif 
							<i class="fa fa-angle-down"></i>
						</a>         
                    </h2>
                    <div class="property-table hidden">
						<table class="table table-striped dashboard-table">
							<thead>
								<tr>
									<th style="width: 70px;" class="text-center">#</th>
									<th>{{__('dashboard.property_name')}}</th>
									<th>{{__('dashboard.transaction_manager')}}</th>
									<th>{{__('dashboard.total_purchase_price')}}</th>
									<th>{{__('dashboard.gross_return')}}</th>
									<!--<th>{{__('dashboard.faktor')}}</th>-->
									<th>{{__('dashboard.guv_ref_factor')}}</th>
									<th>{{__('dashboard.ek_cf')}}</th>
									<th>{{__('dashboard.maklerpreis')}}</th>
									<th>{{__('dashboard.angebotspreis')}}</th>
									<th>{{__('dashboard.price_difference')}}</th>
									<th>{{__('property.listing.status')}}</th>
									<th>{{__('property.listing.actions')}}</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
						<div class="pagination-wrapper" style="margin-left: 20px">
							<div class="row pull-left">
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

	<div class="modal fade" id="Modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Choose a Bank</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<label>{{__('property.select_a_bank')}}: </label>
					<select id="bank">
						@foreach ($banks as $bank)
							<option value="{{$bank->id}}" data-with_real_ek="{{$bank->with_real_ek}}" data-from_bond="{{$bank->from_bond}}"
								data-bank_loan="{{$bank->bank_loan}}" data-interest_bank_loan="{{$bank->interest_bank_loan}}" 
								data-eradication_bank="{{$bank->eradication_bank}}" data-interest_bond="{{$bank->interest_bond}}"
								>{{$bank->name}}</option>
						@endforeach
					</select>
				</div>
				<div class="modal-footer">
					<button id="modal_save_btn" type="button" class="btn btn-primary">{{__('property.save_changes')}}</button>
				</div>
			</div>
		</div>
	</div>


@endsection

@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<script src="{{asset('js/custom-datatable.js')}}"></script>

<script>
	
	$(document).ready(function (){

		$('.search-text').keypress(function(e) {
		    if(e.which == 13) {
		        $('.search-prop').trigger('click');
		    }
		});

		$('.search-prop').click(function(){
			search = $('.search-text').val();
			var url = "{{route('searchproperty')}}?search="+search+"&property_status=1";
			$.ajax({
				url: url,
			}).done(function (data){
				$('.search-result').html(data);
				var columns = [
					null,
					null,
					null,
					{ "type": "new-date"},
					null,
					null,
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
					//{ "type": "formatted-num" },
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
					null,
					null,
				];
				makeDatatable($('.search-result table'), columns);
			});
		});
		
		$('.show-link').click(function(){

			if($(this).closest('.table-responsive').find('.property-table').hasClass('hidden')){

				$(this).find('i').removeClass('fa-angle-down');
				$(this).find('i').addClass('fa-angle-up');
				$(this).closest('.table-responsive').find('.property-table').removeClass('hidden');

				// $(this).attr(id);
				$this = $(this);
				status = $(this).attr('data-status');
				var url = "{{route('getstatustable')}}?status="+status+"&property_status=1";

				$.ajax({
					url: url,
				}).done(function (data){ 
					
					$this.closest('.table-responsive').find('.property-table').html(data);

					if(status == "12"){

						var table_element = $this.closest('.table-responsive').find('.dashboard-table');
						var columns = [
							null,
							// null,
							null,
							null,
							null,
							null,
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "formatted-num" },
							//{ "type": "formatted-num" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							null,
							null,
						];
						makeDatatable(table_element, columns);

					}else if(status == "6"){

						var table_element = $this.closest('.table-responsive').find('.dashboard-table');
						var columns = [
							null,
							// null,
							null,
							null,
							null,
							null,
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							//{ "type": "formatted-num" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							null,
							null,
						];
						makeDatatable(table_element, columns);

					}else if(status == "14" || status == "10"){

						var table_element = $this.closest('.table-responsive').find('.dashboard-table');
						var columns = [
							null,
							// null,
							null,
							null,
							null,
							null,
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							//{ "type": "formatted-num" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "new-date"},
							null,
							null,
						];
						makeDatatable(table_element, columns);

					}else if(status == "11"){

						var table_element = $this.closest('.table-responsive').find('.dashboard-table');
						var columns = [
							null,
							// null,
							null,
							null,
							null,
							null,
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							//{ "type": "formatted-num" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							null,
							null,
						];
						makeDatatable(table_element, columns);

					}else if(status == "8"){

						var table_element = $this.closest('.table-responsive').find('.dashboard-table');
						var columns = [
							null,
							// null,
							null,
							null,
							{ "type": "new-date"},
							{ "type": "new-date"},
							null,
							null,
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							//{ "type": "formatted-num" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							null,
							null,
						];
						makeDatatable(table_element, columns);

					}else if(status == "0"){

						var table_element = $this.closest('.table-responsive').find('.dashboard-table');
						var columns = [
							null,
							// null,
							null,
							null,
							null,
							null,
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							//{ "type": "formatted-num" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							null,
							null,
						];
						makeDatatable(table_element, columns);

					}else if(status == "600"){

						var table_element = $this.closest('.table-responsive').find('.dashboard-table');
						var columns = [
							null,
							null,
							null,
							{ "type": "new-date"},
							{ "type": "new-date"},
							{ "type": "new-date"},
							null,
							null,
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							//{ "type": "formatted-num" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
						];
						makeDatatable(table_element, columns);

					}else if(status == "16"){

						var table_element = $this.closest('.table-responsive').find('.dashboard-table');
						var columns = [
							null,
							null,
							null,
							{ "type": "new-date"},
							null,
							null,
							null,
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							//{ "type": "formatted-num" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							null,
							null,
						];
						makeDatatable(table_element, columns);

					}else{

						var table_element = $this.closest('.table-responsive').find('.dashboard-table');
						var columns = [
							null,
							// null,
							null,
							null,
							null,
							{ "type": "new-date"},
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							//{ "type": "formatted-num" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							{ "type": "numeric-comma" },
							null,
							null,
						];
						makeDatatable(table_element, columns);
					}	
				});

			}else{

				// $(this).html('<i class="fa fa-angle-down"></i>');
				$(this).find('i').addClass('fa-angle-down');
				$(this).find('i').removeClass('fa-angle-up');
				$(this).closest('.table-responsive').find('.property-table').addClass('hidden');
				// $(this).next().addClass('hidden');
			}
		});
		
		$('.table-responsive').find('a.inline-edit').editable({
			step: 'any',
			success: function(response, newValue) {
				if( response.success === false )
					return response.msg;
				else{
					 location.reload();
				}
			}
		});

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	
		var propertyId;
		$('body').on('change','.property-status', function () {
			if($(this).val()=="1"){
				$('#Modal').modal('show');
			}
			selection = $('.property-status');
			selection.prop( "disabled", true );
			propertyId = $(this).data('property-id');
			var status = $(this).val();
			var notificationType = $(this).data('notification-type');
			var data = {
				_token : '<?php echo csrf_token() ?>',
				status : status,
				property_id : propertyId
			};
			$.ajax({
				type:'POST',
				url:'{{route('properties.change_status')}}',
				data: data,
				success:function(data){
					newNotification(propertyId, 3);
					// location.reload();
				}
			});
		});

		$('#modal_save_btn').on('click', function () {
			$.ajax({
				method: "POST",
				url: "{{url('/property/select_bank')}}" + "/" + propertyId,
				data: { 
					with_real_ek: $('#bank option:selected').data("with_real_ek"),
					from_bond: $('#bank option:selected').data("from_bond"),
					bank_loan: $('#bank option:selected').data("bank_loan"),
					interest_bank_loan: $('#bank option:selected').data("interest_bank_loan"),
					eradication_bank: $('#bank option:selected').data("eradication_bank"),
					interest_bond: $('#bank option:selected').data("interest_bond"),
					bank: $('#bank option:selected').val()
					}
			})
			.success(function( response, newValue) {
				if( response.success === false )
					return response.msg;
				else
				   $('#Modal').modal('hide');
			});
		});
		
	});
    </script>
@endsection
