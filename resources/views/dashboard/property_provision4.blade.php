<table id="property-provision-table-4" class="table table-striped">
	<thead>
		<tr>
			<th>AM</th>
			<th class="text-center">Anzahl</th>
		</tr>
	</thead>
	<tbody>
		@if (!empty($count_array))
			@foreach ($count_array as $k=>$element)
				<tr>
					<td>{{$element['name']}}</td>
					<td class="text-center"><a href="javascript:void(0)" class="get-provision-property" data-id="{{$k}}">{{$element['count']}}</a></td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>