<div class="table-responsive">
   <table  class="vacant-list table table-striped">
      <thead>
         <tr>
            <th>#</th>
            <th>Asset Manager</th>
            <th>Objekt</th>
            <th>Leerstand</th>
            <th>Fläche (m2)</th>
            <th>Leerstand (€)</th>
            <th>Typ</th>
            <th>Kommentare</th>
         </tr>
      </thead>
      <tbody>
         <?php
         $tenancy_items_34_total = array();
         ?>
         @foreach($tenancy_items_34 as $tenancy_item)
         <?php

         // if(!$tenancy_item->vacancy_in_eur)
         $tenancy_item->vacancy_in_eur = $tenancy_item->calculated_vacancy_in_eur;
                     
         if(isset($tenancy_items_34_total[$tenancy_item->property_id]))
         {
            $tenancy_items_34_total[$tenancy_item->property_id]['count'] += 1;
            $tenancy_items_34_total[$tenancy_item->property_id]['amount'] += $tenancy_item->vacancy_in_eur;
            $tenancy_items_34_total[$tenancy_item->property_id]['space'] += $tenancy_item->vacancy_in_qm;
         }
         else{
            $tenancy_items_34_total[$tenancy_item->property_id]['name'] = $tenancy_item->object_name;
            $tenancy_items_34_total[$tenancy_item->property_id]['count'] = 1;
            $tenancy_items_34_total[$tenancy_item->property_id]['amount'] = $tenancy_item->vacancy_in_eur;
            $tenancy_items_34_total[$tenancy_item->property_id]['space'] = $tenancy_item->vacancy_in_qm;
         }

         ?>
         <tr>
            <td>{{$tenancy_item->id}}</td>
            <td>
               <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $tenancy_item->property_id }}">{{ $tenancy_item->creator_name }}</a>
            </td>
            <td>
               <a  href="{{route('properties.show',['property'=>$tenancy_item->property_id])}}?tab=tenancy-schedule">
               {{$tenancy_item->object_name}}</a>
            </td>
            <td>{{$tenancy_item->name}}</td>
            <td class="number-right">{{$tenancy_item->vacancy_in_qm ? number_format($tenancy_item->vacancy_in_qm,2,",",".") : 0}}</td>
            <td class="number-right">{{$tenancy_item->vacancy_in_eur ? number_format($tenancy_item->vacancy_in_eur,2,",",".") : 0}}</td>
            <td>{{$tenancy_item->type}}</td>
            <td>{{$tenancy_item->comment}}</td>
         </tr>
         @endforeach
      </tbody>
   </table>
</div>