@extends('layouts.admin')

@section('css')
   <link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
   <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
   <style type="text/css">
      .text-red,
      .text-red a{
         color:red;
      }
      .loading {
        height: 0;
        width: 0;
        padding: 15px;
        border: 6px solid #ccc;
        border-right-color: #888;
        border-radius: 22px;
        -webkit-animation: rotate 1s infinite linear;
        /* left, top and position just for the demo! */
        position: absolute;
        left: 50%;
        top: 50%;
      }
      @-webkit-keyframes rotate {
        /* 100% keyframe for  clockwise.
           use 0% instead for anticlockwise */
        100% {
          -webkit-transform: rotate(360deg);
        }
      }
      #added-month-asset tbody th:first-child{
         cursor: pointer;
      }
      .insurance_tab_div table .btn{
         display: none;
      }
      @media (min-width: 992px)
      {    .modal-lg {
         width: 1320px;
      }
      }
      #vlog-table2 .fa-check,
      #vlog-table .fa-check{
         color: green;
      }
      #vlog-table2 .fa-times,
      #vlog-table .fa-times{
         color: red;
      }
      @media (min-width: 768px)
      {
         .modal-dialog.custom {
            width: 848px;
            margin: 30px auto;
         }
      }
      .list-data-banks a{
         pointer-events: none;
         color: #797979 !important;
      }
      .bank-file-upload input{
         display: none !important;
      }
      .bank-file-upload a{
         pointer-events:auto;
         color: #797979 !important;
      }
      .list-data-banks .btn{
         display: none;
      }
      ._51mz{
         /*display: none;*/
      }
      .card {
         position: relative;
         /*display: flex;*/
         flex-direction: column;
         min-width: 0;
         word-wrap: break-word;
         background-color: #fff;
         background-clip: border-box;
         border: 0 solid transparent;
         border-radius: 0;
         height: 115px !important;
      }
      .card-body {
         flex: 1 1 auto;
         padding: 8px 12px;
      }
      .card .card-title {
         position: relative;
         font-weight: 500;
         font-size: 16px;
      }
      .d-flex {
         display: flex!important;
      }
      .align-items-center {
         align-items: center!important;
      }
      .ml-auto, .mx-auto {
         margin-left: auto!important;
      }
      .custum-row .col-lg-3, .custum-row .col-md-3{
         padding: 0 5px;
      }
      .custum-row-2{
         margin-top: 25px;
      }
      .custum-row-2 .col-md-6{
         margin: 5px 0;
         padding: 0 5px;
      }
      .preloader{
         display: none;
      }
      .pb-3, .py-3{
         padding-bottom: 20px;
         margin-top: 10px;
      }
      .for-col-padding .col-md-6{
         padding: 0 5px;
      }
      .pointer-cursor{
         cursor: pointer;
      }
      .get-assetmanager-property,.get-manager-property{
         cursor: pointer;
      }
      .border-top-footer{
         border-top: 3px solid #666 !important;
      }
      .div-user-count{
         font-size: 16px !important;
         color: #313131;
         font-weight: 300 !important;
      }
      .get-verkauf-user,
      .get-vermietung-user{
         cursor: pointer;
         color: #23527c;
      }
      .col-md-3 .progress{
         display: none !important;
      }
      @media only screen and (min-width: 768px) {
         .dashboard-card-button .col-md-3{
           width: 20% !important;
         }
      }
      .blue-bg{
        background-color: #004f91;
      }
      .blue-bg h2,
      .blue-bg h5{
        color: white;
      }

      .red-bg{
        background-color: #e03232;
      }
      .red-bg h2,
      .red-bg h5{
        color: white;
      }
      .dashboard-card-button .col-md-3{
         margin-bottom: 12px;
      }
      .yellow-bg {
         background-color: #ffff99;
      }
      .modal-body table tr td,
      .modal-body table tr th{
         font-size: 14px !important;
      }
      .modal-body{
         font-family: arial, sans-serif;
      }
   </style>
@section('content')

@include('partials.flash')

<div class="page-content container-fluid">
   <div class="row custum-row dashboard-card-button">

      <div class="col-md-3 load-table" id="leerstände" data-table="tenant-table4" data-title="Leerstände (€)" data-url="{{ route('get_vacancy') }}?is_am=1">
          <div class="card pointer-cursor red-bg" >
              <div class="card-body">
                 <h5 class="card-title text-uppercase">Leerstände</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 vacancy-total">....</h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="open_invoice" data-table="invoice-table-0" data-title="Offene Rechnungen" data-url="{{ route('get_invoice_for_am', ['status' => 0]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">Offene Rechnungen</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 open_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="request_invoice" data-table="invoice-table-1" data-title="FREIZUGEBENDE RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 1]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">FREIZUGEBENDE RECHNUNGEN</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 request_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="release_invoice" data-table="invoice-table-2" data-title="FREIGEGEBENE RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 2]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">FREIGEGEBENE RECHNUNGEN</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 release_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="pending_invoice" data-table="invoice-table-3" data-title="PENDING RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 3]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">PENDING RECHNUNGEN</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 pending_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="not_release_invoice" data-table="invoice-table-4" data-title="NICHT FREIGEGEBEN RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 4]) }}">
          <div class="card pointer-cursor">
              <div class="card-body">
                 <h5 class="card-title text-uppercase">NICHT FREIGEGEBEN RECHNUNGEN</h5>
                 <div class="text-right">
                    <h2 class="mt-2 display-7 not_release_invoice_total"></h2>
                 </div>
                 <br>
              </div>
          </div>
      </div>

      <div class="col-md-3 load-table" id="vobj" data-table="list-properties-1" data-title="Zu verteilende Objekte" data-url="{{ route('assetmanagement') }}?id=-1">
         <div class="card pointer-cursor " >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Zu verteilende Objekte</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 object-to-be-distributed-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="dashboard-table-2" data-title="BESTANDSOBJEKTE" data-url="{{ route('assetmanagement') }}?ajax=1">
         <div class="card pointer-cursor " >
            <div class="card-body">
               <h5 class="card-title text-uppercase">DIFFERENZ MIETE (SOLL/IST) IN €</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="dashboard-table-2" data-title="BESTANDSOBJEKTE" data-url="{{ route('assetmanagement') }}?ajax=1">
         <div class="card pointer-cursor " >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Wault (Durchschnitt)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="dashboard-table-2" data-title="BESTANDSOBJEKTE" data-url="{{ route('assetmanagement') }}?ajax=1">
         <div class="card pointer-cursor " >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Vermietet (%)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3" id="rental-overview">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Mieterübersicht</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 rental-overview-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="bank-properties" data-title="BANKENFINANZIERUNGEN" data-url="{{ route('getbankproperty') }}">
         <div class="card pointer-cursor " >
            <div class="card-body">
               <h5 class="card-title text-uppercase">BANKENFINANZIERUNGEN</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="insurance-table1" data-title="GEBÄUDE VERSICHERUNG" data-url="{{ route('getPropertyInsurance') }}?axa=1">
         <div class="card pointer-cursor " >
            <div class="card-body">
               <h5 class="card-title text-uppercase">GEBÄUDE VERSICHERUNG</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="expiring-building-insurance-table" data-title="AUSLAUFENDE GEBÄUDEVERSICHERUNGEN" data-url="{{ route('get_expiring_building_insurance') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">AUSLAUFENDE GEBÄUDEVERSICHERUNGEN</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 expiring-building-insurance-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="insurance-table0" data-title="HAFTPFLICHT VERSICHERUNG" data-url="{{ route('getPropertyInsurance') }}?axa=0">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">HAFTPFLICHT VERSICHERUNG</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="expiring-liability-insurance-table" data-title="AUSLAUFENDE HAFTPFLICHTVERSICHERUNGEN" data-url="{{ route('get_expiring_liability_insurance') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">AUSLAUFENDE HAFTPFLICHTVERSICHERUNGEN</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 expiring-liability-insurance-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="hausmax_table" data-title="Hausverwaltung Kosten" data-url="{{ route('get_house_management_cost') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">HAUSVERWALTUNG KOSTEN</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 house-management-cost-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="mahnung-properties" data-title="MAHNUNG" data-url="{{ route('getMahnungProperties') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Mahnungen (30 Tage)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 house-management-cost-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="expired-rental-agreement-table" data-title="ABGELAUFENE MIETVERTRÄGE" data-url="{{ route('get_expired_rental_agreement') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">ABGELAUFENE MV</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 expired-rental-agreement-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="expiring_leases_table" data-title="Auslaufende Mietverträge" data-url="{{ route('expiring_leases') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">AUSLAUFENDE MV (90 Tage)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 expiring-leases-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      @php
         $previous_month = date('n-Y');
         $previous_month_arr = explode("-", $previous_month);
         $list_month = $previous_month_arr[0];
         $list_year = $previous_month_arr[1];
      @endphp

      <div class="col-md-3 load-table " data-table="property-default-payer-table" data-title="OFFENE POSTEN" data-url="{{ route('get_default_payers') }}?month={{$list_month}}&year={{$list_year}}&status=0&amcheck=1">
         <div class="card pointer-cursor yellow-bg">
            <div class="card-body">
               <h5 class="card-title text-uppercase">OP fehlt</h5>
               <div class="text-right">
                  <h2 style="line-height: 23px;font-size: 20px;" class="mt-2 display-7 default-payer-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" id="open-angebote" data-table="table-open-angebote" data-title="Offene Angebote" data-url="{{ route('getOpenAngebote') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Offene Angebote</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 open-angebote-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>
        

   </div>
</div>

@include('dashboard.optimized.dashboard_am_modal')

@endsection
@section('js')
   <script type="text/javascript">
      var _token                                = '{{ csrf_token() }}';
      var url_getcategoryproperty               = '{{ route('getcategoryproperty') }}';
      var url_getcategoryproperty3              = '{{ route('getcategoryproperty3') }}';
   </script>
   <script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
   <script src="{{asset('js/highcharts.js')}}"></script>
   <script src="{{asset('js/echarts-en.min.js')}}"></script>
   <script src="{{asset('js/custom-datatable.js')}}"></script>
   <script src="{{asset('js/dashboard-am-optimized.js')}}"></script>

   @if(isset($_REQUEST['section']) && $_REQUEST['section'])
      <script type="text/javascript">
         $(document).ready(function(){
            $('#{{$_REQUEST['section']}}').trigger('click');
         })
      </script>
   @endif

@endsection
