@extends('layouts.admin')

@section('css')
   <style type="text/css">
      .card {
         position: relative;
         /*display: flex;*/
         flex-direction: column;
         min-width: 0;
         word-wrap: break-word;
         background-color: #fff;
         background-clip: border-box;
         border: 0 solid transparent;
         border-radius: 0;
         height: 115px !important;
      }
      .card-body {
         flex: 1 1 auto;
         padding: 8px 12px;
      }
      .card .card-title {
         position: relative;
         font-weight: 500;
         font-size: 16px;
      }
      .dashboard-card-button .col-md-3{
         width: 20% !important;
      } 
      .blue-bg{
         background-color: #004f91;
      }
      .blue-bg h2,
      .blue-bg h5{
         color: white;
      }
      .dashboard-card-button .col-md-3{
         margin-bottom: 12px;
      }
      .custum-row .col-lg-3, .custum-row .col-md-3{
         padding: 0 5px;
      }
      .custum-row-2{
         margin-top: 25px;
      }
      .custum-row-2 .col-md-6{
         margin: 5px 0;
         padding: 0 5px;
      }
      .pointer-cursor{
         cursor: pointer;
      }
      .loading {
        height: 0;
        width: 0;
        padding: 15px;
        border: 6px solid #ccc;
        border-right-color: #888;
        border-radius: 22px;
        -webkit-animation: rotate 1s infinite linear;
        /* left, top and position just for the demo! */
        position: absolute;
        left: 50%;
        top: 50%;
      }

      @-webkit-keyframes rotate {
        /* 100% keyframe for  clockwise. 
           use 0% instead for anticlockwise */
        100% {
          -webkit-transform: rotate(360deg);
        }
      }
   </style>
@section('content')

@include('partials.flash')

<div class="page-content container-fluid">
    <div class="row">
      <div class="col-sm-12">
         <div class="white-box">
            <a href="{{ route('optimize_dashboard') }}" class="btn btn-primary">Back</a>
      

            <h4>RECHNUNGSFREIGABEN</h4>
            <div class="property-invoice-div table-responsive"></div>
          </div>
      </div>
    </div>
</div>

@include('dashboard.optimized.dashboard_modal')

@endsection
@section('js')
    <script src="{{asset('js/custom-datatable.js')}}"></script>
    <script type="text/javascript">
      var _token                                = '{{ csrf_token() }}';
      var url_dashboard_counter                 = '{{ route('get_dashboard_counter') }}';
      var url_delete_property_invoice           = '{{ route("delete_property_invoice", ":id") }}';
      var url_property_invoicereleaseprocedure  = '{{ url('property/invoicereleaseprocedure') }}';
      var url_property_invoicenotrelease        = '{{ url('property/invoicenotrelease') }}';
      var url_property_invoicemarkpending       = '{{ url('property/invoicemarkpending') }}';
      var url_property_invoicereleaseprocedure  = '{{ url('property/invoicereleaseprocedure') }}';
      var url_property_contractreleaseprocedure = '{{ url('property/contractreleaseprocedure') }}';
      var url_contract_mark_as_notrelease       = '{{ route('contract_mark_as_notrelease') }}';
      var url_contract_mark_as_pending          = '{{ route('contract_mark_as_pending') }}';
      var url_getPropertyInvoice                = '{{ route('getPropertyInvoice') }}';

      function getInvoice(){
        var url = url_getPropertyInvoice+"?";
        $.ajax({
           url: url,
        }).done(function (data) { 
            $('.property-invoice-div').html(data);
            var columns = [
                null,
                null,
                null,
                null,
                { "type": "new-date" },
                { "type": "numeric-comma" },
                { "type": "new-date-time" },
                null,
                null,
                null,
                null,
                null,
                null,
            ];
            makeDatatable($('#table-property-invoice'),columns, 'desc', 5);
        });
      }
      getInvoice();

      // Property invoice js
      $('body').on('click', '.btn-delete-property-invoice', function() {
          var id = $(this).attr('data-id');
          var type = $(this).attr('data-type');
          if (confirm('Are you sure want to delete this invoice?')) {
              var url = url_delete_property_invoice;
              url = url.replace(':id', id);
              $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'json',
                  success: function(result) {
                      if(type=="request-table")
                        getInvoice();
                  },
                  error: function(error) {
                      alert('Somthing want wrong!');
                  }
              });
          } else {
              return false;
          }
      });

      $('body').on('click','.invoice-release-request',function(){
          tr = $(this).closest('tr');
          vacant_release_type= 'release2';
          invoice_id= $(this).attr('data-id');
          property_id= $(this).attr('data-property_id');
          $('.invoice-release-comment').val("")
          $('#invoice-release-property-modal').modal();
      });

      $('body').on('click','.invoice-release-submit',function(){
          comment = $('.invoice-release-comment').val();
          $.ajax({
              type : 'POST',
              url : url_property_invoicereleaseprocedure,
              data : {
                  id:invoice_id,
                  step:vacant_release_type,
                  _token : _token,
                  comment:comment,
                  property_id:property_id
                },
              success : function (data) {
                   tr.remove();      
              }
          });
      });

      var not_release_id = 0;
      $('body').on('click','.mark-as-not-release',function(){
          not_release_id = $(this).attr('data-id');
          $('#not-release-modal').modal();
      });

      $('body').on('click','.invoice-not-release-submit',function(){
          comment = $('.invoice-not-release-comment').val();
          $.ajax({
              type : 'POST',
              url : url_property_invoicenotrelease,
              data : {id:not_release_id, _token : _token, comment:comment},
              success : function (data) {
                  getInvoice();
              }
          });
      });

      $('body').on('click','.mark-as-pending',function(){
        not_release_id = $(this).attr('data-id');
        $('#pending-modal').modal();
      });

      $('body').on('click','.invoice-pending-submit',function(){
          comment = $('.invoice-pending-comment').val();
          $.ajax({
              type : 'POST',
              url : url_property_invoicemarkpending,
              data : {id:not_release_id, _token : _token,comment:comment},
              success : function (data) {
                getInvoice();
              }
          });
      });

      $('body').on('click', '.multiple-invoice-release-request', function() {
        var selcted_id_invoice = "";
        arr = [];
        selcted_id_invoice = "";

          $('.multiple-invoice-release-comment').val("");

        $('.invoice-checkbox').each(function(){
            if($(this).is(':checked'))
                arr.push($(this).attr('data-id'));
        });

        selcted_id_invoice = arr.toString();
       
        // console.log(selcted_id_invoice);
        if(selcted_id_invoice=="" || selcted_id_invoice==null){
            alert("Bitte mindestens eine Rechnung auswählen")
        }else{
            $('.multiple-invoice-release-comment').val("");
            $('#multiple-invoice-release-property-modal').modal();
        }
      });

      $('body').on('click', '.multiple-invoice-release-submit', function() {
        var arr = [];
          var comment = $('.multiple-invoice-release-comment').val();
          var selcted_id_invoice = "";

        $('.invoice-checkbox').each(function(){
            if($(this).is(':checked'))
                arr.push($(this).attr('data-id'));
        });


        selcted_id_invoice = arr.toString();
          $('.invoice-checkbox').each(function(){
            if($(this).is(':checked'))
                $(this).closest('tr').remove();
          });
       
          $.ajax({
              type: 'POST',
              url: url_property_invoicereleaseprocedure,
              data: {
                  id: selcted_id_invoice,
                  step: "release2",
                  _token: _token,
                  comment: comment,
              },
              success: function(data) {
                  getInvoice();
                  $('.multiple-invoice-release-comment').val("");
              }
          });
      });

      
    </script>
@endsection