<?php
   $deactiveUser = [];
   $deactiveUserRes = DB::table('users')->select('name')->where('user_status', 0)->get();
   if($deactiveUserRes){
      foreach ($deactiveUserRes as $key => $value) {
         $deactiveUser[] = trim($value->name);
      }
   }
   $user = Auth::user();
?>
@extends('layouts.admin')

@section('css')
   <link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
   <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
   <style type="text/css">
      .text-red,
      .text-red a{
         color:red;
      }
      .loading {
        height: 0;
        width: 0;
        padding: 15px;
        border: 6px solid #ccc;
        border-right-color: #888;
        border-radius: 22px;
        -webkit-animation: rotate 1s infinite linear;
        /* left, top and position just for the demo! */
        position: absolute;
        left: 50%;
        top: 50%;
      }
      @-webkit-keyframes rotate {
        /* 100% keyframe for  clockwise.
           use 0% instead for anticlockwise */
        100% {
          -webkit-transform: rotate(360deg);
        }
      }
      #added-month-asset tbody th:first-child{
         cursor: pointer;
      }
      .insurance_tab_div table .btn{
         display: none;
      }
      @media (min-width: 992px)
      {    .modal-lg {
         width: 1320px;
      }
      }
      #vlog-table2 .fa-check,
      #vlog-table .fa-check{
         color: green;
      }
      #vlog-table2 .fa-times,
      #vlog-table .fa-times{
         color: red;
      }
      @media (min-width: 768px)
      {
         .modal-dialog.custom {
            width: 848px;
            margin: 30px auto;
         }
      }
      .list-data-banks a{
         pointer-events: none;
         color: #797979 !important;
      }
      .bank-file-upload input{
         display: none !important;
      }
      .bank-file-upload a{
         pointer-events:auto;
         color: #797979 !important;
      }
      .list-data-banks .btn{
         display: none;
      }
      ._51mz{
         /*display: none;*/
      }
      .card {
         position: relative;
         /*display: flex;*/
         flex-direction: column;
         min-width: 0;
         word-wrap: break-word;
         background-color: #fff;
         background-clip: border-box;
         border: 0 solid transparent;
         border-radius: 0;
         height: 115px !important;
      }
      .card-body {
         flex: 1 1 auto;
         padding: 8px 12px;
      }
      .card .card-title {
         position: relative;
         font-weight: 500;
         font-size: 16px;
      }
      .d-flex {
         display: flex!important;
      }
      .align-items-center {
         align-items: center!important;
      }
      .ml-auto, .mx-auto {
         margin-left: auto!important;
      }
      .custum-row .col-lg-3, .custum-row .col-md-3{
         padding: 0 5px;
      }
      .custum-row-2{
         margin-top: 25px;
      }
      .custum-row-2 .col-md-6{
         margin: 5px 0;
         padding: 0 5px;
      }
      .preloader{
         display: none;
      }
      .pb-3, .py-3{
         padding-bottom: 20px;
         margin-top: 10px;
      }
      .for-col-padding .col-md-6{
         padding: 0 5px;
      }
      .pointer-cursor{
         cursor: pointer;
      }
      .get-assetmanager-property,.get-manager-property{
         cursor: pointer;
      }
      .border-top-footer{
         border-top: 3px solid #666 !important;
      }
      .div-user-count{
         font-size: 16px !important;
         color: #313131;
         font-weight: 300 !important;
      }
      .get-verkauf-user,
      .get-vermietung-user{
         cursor: pointer;
         color: #23527c;
      }
      .col-md-3 .progress{
         display: none !important;
      }
      @media only screen and (min-width: 768px) {
         .dashboard-card-button .col-md-3{
           width: 20% !important;
         }
      }
      .blue-bg{
        background-color: #004f91;
      }
      .blue-bg h2,
      .blue-bg h5{
        color: white;
      }

      .red-bg{
        background-color: #e03232;
      }
      .red-bg h2,
      .red-bg h5{
        color: white;
      }
      .dashboard-card-button .col-md-3{
         margin-bottom: 12px;
      }
      .yellow-bg {
         background-color: #ffff99;
      }
      .modal-body table tr td,
      .modal-body table tr th{
         font-size: 14px !important;
      }
      .modal-body{
         font-family: arial, sans-serif;
      }
   </style>
@section('content')

@include('partials.flash')

<div class="page-content container-fluid">
   <div class="row custum-row dashboard-card-button">

      <?php
         $alex_class = '';
         //if(Auth::user()->email=="a.lauterbach@fcr-immobilien.de")
            //$alex_class = 'hidden';
      ?>
      <div class="col-md-3 load-table {{$alex_class}}" id="leerstände" data-table="tenant-table4" data-title="Leerstände (€)" data-url="{{ route('get_vacancy') }}">
         <div class="card pointer-cursor red-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Leerstände</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 vacancy-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" id="vobj" data-table="list-properties-1" data-title="Zu verteilende Objekte" data-url="{{ route('assetmanagement') }}?id=-1">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Zu verteilende Objekte</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 object-to-be-distributed-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      @if($user->email==config('users.falk_email') || $user->id==4 || $user->id==84)

         <div class="col-md-3 load-table {{$alex_class}}" id="invoice" data-table="table-property-invoice" data-title="Rechnungsfreigaben" data-url="{{ route('getPropertyInvoice') }}">
            <div class="card pointer-cursor blue-bg" >
               <div class="card-body">
                  <h5 class="card-title text-uppercase">Rechnungsfreigaben</h5>
                  <div class="text-right">
                     <h2 class="mt-2 display-7 invoice-approval-total">....</h2>
                  </div>
                  <br>
               </div>
            </div>
         </div>

         <div class="col-md-3 load-table {{$alex_class}}" id="pending-invoice" data-table="table-property-invoice2" data-title="PENDING RECHNUNGEN" data-url="{{ route('getPendingNotReleaseInvoice') }}?status=2">
            <div class="card pointer-cursor blue-bg" >
               <div class="card-body">
                  <h5 class="card-title text-uppercase">PENDING RECHNUNGEN</h5>
                  <div class="text-right">
                     <h2 class="mt-2 display-7 pending-invoice-total">....</h2>
                  </div>
                  <br>
               </div>
            </div>
         </div>

         <div class="col-md-3 load-table {{$alex_class}}" id="notrelease-invoice" data-table="table-property-invoice1" data-title="NICHT FREIGEGEBEN" data-url="{{ route('getPendingNotReleaseInvoice') }}?status=1">
            <div class="card pointer-cursor blue-bg" >
               <div class="card-body">
                  <h5 class="card-title text-uppercase">NICHT FREIGEGEBEN</h5>
                  <div class="text-right">
                     <h2 class="mt-2 display-7 not-release-invoice-total">....</h2>
                  </div>
                  <br>
               </div>
            </div>
         </div>

      @else

         <div class="col-md-3 load-table" id="open_invoice" data-table="invoice-table-0" data-title="Offene Rechnungen" data-url="{{ route('get_invoice_for_am', ['status' => 0]) }}">
             <div class="card pointer-cursor blue-bg">
                 <div class="card-body">
                    <h5 class="card-title text-uppercase">Offene Rechnungen</h5>
                    <div class="text-right">
                       <h2 class="mt-2 display-7 new_open_invoice_total">....</h2>
                    </div>
                    <br>
                 </div>
             </div>
         </div>

         <div class="col-md-3 load-table" id="request_invoice" data-table="invoice-table-1" data-title="FREIZUGEBENDE RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 1]) }}">
             <div class="card pointer-cursor blue-bg">
                 <div class="card-body">
                    <h5 class="card-title text-uppercase">FREIZUGEBENDE RECHNUNGEN</h5>
                    <div class="text-right">
                       <h2 class="mt-2 display-7 new_request_invoice_total">....</h2>
                    </div>
                    <br>
                 </div>
             </div>
         </div>

         <div class="col-md-3 load-table" id="release_invoice" data-table="invoice-table-2" data-title="FREIGEGEBENE RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 2]) }}">
             <div class="card pointer-cursor blue-bg">
                 <div class="card-body">
                    <h5 class="card-title text-uppercase">FREIGEGEBENE RECHNUNGEN</h5>
                    <div class="text-right">
                       <h2 class="mt-2 display-7 new_release_invoice_total">....</h2>
                    </div>
                    <br>
                 </div>
             </div>
         </div>

         <div class="col-md-3 load-table" id="pending_invoice" data-table="invoice-table-3" data-title="PENDING RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 3]) }}">
             <div class="card pointer-cursor blue-bg">
                 <div class="card-body">
                    <h5 class="card-title text-uppercase">PENDING RECHNUNGEN</h5>
                    <div class="text-right">
                       <h2 class="mt-2 display-7 new_pending_invoice_total">....</h2>
                    </div>
                    <br>
                 </div>
             </div>
         </div>

         <div class="col-md-3 load-table" id="not_release_invoice" data-table="invoice-table-4" data-title="NICHT FREIGEGEBEN RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 4]) }}">
             <div class="card pointer-cursor blue-bg">
                 <div class="card-body">
                    <h5 class="card-title text-uppercase">NICHT FREIGEGEBEN RECHNUNGEN</h5>
                    <div class="text-right">
                       <h2 class="mt-2 display-7 not_release_invoice_total">....</h2>
                    </div>
                    <br>
                 </div>
             </div>
         </div>
      
         {{-- <div class="col-md-3 load-table" id="invoice" data-table="table-property-invoice-am" data-title="FREIZUGEBENDE RECHNUNGEN" data-url="{{ route('getPropertyInvoiceRequest') }}">
             <div class="card pointer-cursor blue-bg" >
                <div class="card-body">
                   <h5 class="card-title text-uppercase">FREIZUGEBENDE RECHNUNGEN</h5>
                   <div class="text-right">
                      <h2 class="mt-2 display-7 invoice-approval-total">....</h2>
                   </div>
                   <br>
                </div>
             </div>
         </div> --}}
         
      @endif

      <div class="col-md-3 load-table {{$alex_class}}" id="contract" data-table="table-contract-release" data-title="Vertragsfreigaben" data-url="{{ route('get_contract_release') }}">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Vertragsfreigaben</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 contract-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" id="angebot-release-section" data-table="property-insurance-table" data-title="Angebotsfreigaben" data-url="{{ route('get_property_insurance_data') }}">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Angebotsfreigaben</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 property-insurance-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" id="recommendation-section" data-table="recommendation-table" data-title="Vermietungsempfehlungen" data-url="{{ route('getRecommendation') }}">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Vermietungsempfehlungen</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 recommendation-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" id="gabuderelease" data-table="table-approval-building-insurance" data-title="FREIGABE GEBÄUDEVERSICHERUNG" data-url="{{ route('get_approval_building_insurance', ['type' => 1]) }}">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Freigabe Gebäudevers.</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 approval-building-insurance-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" id="haftrelease" data-table="table-release-liability" data-title="Freigabe Haftpflichtversicherung" data-url="{{ route('get_approval_building_insurance', ['type' => 2]) }}">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">FREIGABE HAFTPFLICHTVERS</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 release-liability-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" id="hausrelease" data-table="table-property-management-approval" data-title="FREIGABE HAUSVERWALTUNG" data-url="{{ route('get_property_management') }}">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Freigabe Hausverwaltung</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 property-management-approval-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" id="provision" data-table="property-provision-table" data-title="Provisionsfreigaben" data-url="{{ route('getPropertyProvision') }}?type=1">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Provisionsfreigaben</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 commission-release-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" id="erelease" data-table="vlog-table" data-title="Einkaufsfreigaben" data-url="{{ route('releaselogs') }}">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Einkaufsfreigaben</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 purchase-approval-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" id="vrelease" data-table="vlog-table2" data-title="Verkaufsfreigaben" data-url="{{ route('vreleaselogs') }}">
         <div class="card pointer-cursor blue-bg" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Verkaufsfreigaben</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 sales-clearance-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" id="liquiplanung-1" data-table="" data-title="Liquiplanung" data-url="{{ route('get_liquiplanung_1') }}">
         <div class="card pointer-cursor yellow-bg">
            <div class="card-body">
               <h5 class="card-title text-uppercase">Liquiplanung</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 liquiplanung-1-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="added-month" data-title="NEUE IMMOBILIEN IM ANGEBOT" data-url="{{ route('get_monthyearmanager') }}?month=&year=">
         <div class="card pointer-cursor yellow-bg">
            <div class="card-body">
               <h5 class="card-title text-uppercase">IMMOBILIEN UNTER BEOABACHTUNG</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 real-estate-observe-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="rental_activity_table" data-title="Vermietungsaktivitäten" data-url="{{ route('get_rental_activity_data') }}">
         <div class="card pointer-cursor yellow-bg">
            <div class="card-body">
               <h5 class="card-title text-uppercase">Vermietungsaktivitäten</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 rental-activity-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      @php
         $previous_month = date('n-Y');
         $previous_month_arr = explode("-", $previous_month);
         $list_month = $previous_month_arr[0];
         $list_year = $previous_month_arr[1];
      @endphp
      <div class="col-md-3 load-table {{$alex_class}}" data-table="property-default-payer-table" data-title="OFFENE POSTEN" data-url="{{ route('get_default_payers') }}?month={{$list_month}}&year={{$list_year}}&status=0">
         <div class="card pointer-cursor yellow-bg">
            <div class="card-body">
               <h5 class="card-title text-uppercase">OP fehlt</h5>
               <div class="text-right">
                  <h2 style="line-height: 23px;font-size: 20px;" class="mt-2 display-7 default-payer-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>
      <div class="col-md-3 load-table {{$alex_class}}" data-table="oplist-table" data-title="OP Übersicht" data-url="{{ route('get_oplist') }}?month={{$list_month}}&year={{$list_year}}&status=0">
         <div class="card pointer-cursor yellow-bg">
            <div class="card-body">
               <h5 class="card-title text-uppercase">OP Übersicht</h5>
               <div class="text-right">
                  <h2 style="line-height: 23px;font-size: 20px;" class="mt-2 display-7">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>
      <div class="col-md-3 {{$alex_class}}" id="card-release-contract" data-url="{{ route('release_contract') }}">
         <div class="card pointer-cursor yellow-bg">
            <div class="card-body">
               <h5 class="card-title text-uppercase">Auslaufende Verträge</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 release-contract-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" id="bka" data-table="bka-table" data-title="BKA" data-url="{{ route('get_bka') }}?year=2017">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">BKA</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 bka-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      
      @if(strtolower($user->email)=="t.raudies@fcr-immobilien.de" || strtolower($user->email)=="n.eschenbach@fcr-immobilien.de")
      
         {{-- <div class="col-md-3 load-table" id="property-release-invoice" data-table="table-property-release-invoice" data-title="Überweisungen" data-url="{{ route('getPropertyReleaseInvoice') }}">
            <div class="card pointer-cursor" >
               <div class="card-body">
                  <h5 class="card-title text-uppercase">Überweisungen</h5>
                  <div class="text-right">
                     <h2 class="mt-2 display-7 property-release-invoice-total"></h2>
                  </div>
                  <br>
               </div>
            </div>
         </div> --}}

      @endif
      


      <div class="col-md-3 load-table {{$alex_class}}" data-table="list-statusloi" data-title="LOI" data-url="{{ route('getloi', [\Carbon\Carbon::now()->month]) }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">AKTUELL LAUFENDE LOI´S (ANKAUFSGEBOTE)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 loi-purchase-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 email-finance {{$alex_class}}" data-table="funding-request-table" data-title="Finanzierungsanfragen" data-url="{{ route('get_funding_request') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Finanzierungsanfragen</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 funding-request-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      {{-- Pending --}}
      <div class="col-md-3 {{$alex_class}} neue_mv">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Neue MV</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 rental-activity-total-1">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      {{-- Pending --}}
      <div class="col-md-3 {{$alex_class}} neue_vl">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Neue VL</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 rental-activity-total-2">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="dashboard-table-2" data-title="Bestandsobjekte" data-url="{{ route('assetmanagement') }}?ajax=1">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">DIFFERENZ MIETE (SOLL/IST) IN €</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 diff_rent-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="dashboard-table-2" data-title="Bestandsobjekte" data-url="{{ route('assetmanagement') }}?ajax=1">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Vermietet (%)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 existing-property-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      {{-- <div class="col-md-3 load-table {{$alex_class}}" data-table="rental-overview-table" data-title="Mieterübersicht" data-url="{{ route('get_rental_overview') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Mieterübersicht</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 rental-overview-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div> --}}

      <div class="col-md-3 {{$alex_class}}" id="rental-overview">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Mieterübersicht</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 rental-overview-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      

      <div class="col-md-3 load-table {{$alex_class}}" data-table="vacancy-per-object-table" data-title="LEERSTÄNDE PRO OBJEKT" data-url="{{ route('get_vacance_per_object') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">LEERSTÄNDE PRO OBJEKT</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 vacancy-per-object-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="expiring_leases_table" data-title="Auslaufende Mietverträge" data-url="{{ route('expiring_leases') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">AUSLAUFENDE MV (90 Tage)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 expiring-leases-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="expired-rental-agreement-table" data-title="ABGELAUFENE MIETVERTRÄGE" data-url="{{ route('get_expired_rental_agreement') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">ABGELAUFENE MV</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 expired-rental-agreement-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="insurance-table1" data-title="Gebäude Versicherung" data-url="{{ route('get_insurance_data') }}?axa=1">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">GEBÄUDE VERSICHERUNG</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 insurance1-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="expiring-building-insurance-table" data-title="AUSLAUFENDE GEBÄUDEVERSICHERUNGEN" data-url="{{ route('get_expiring_building_insurance') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">AUSLAUFENDE GEBÄUDEVERSICHERUNGEN</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 expiring-building-insurance-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="insurance-table0" data-title="HAFTPFLICHT VERSICHERUNG" data-url="{{ route('get_insurance_data') }}?axa=0">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">HAFTPFLICHT VERSICHERUNG</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 insurance0-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="expiring-liability-insurance-table" data-title="AUSLAUFENDE HAFTPFLICHTVERSICHERUNGEN" data-url="{{ route('get_expiring_liability_insurance') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">AUSLAUFENDE HAFTPFLICHTVERSICHERUNGEN</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 expiring-liability-insurance-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class}}" data-table="hausmax_table" data-title="Hausverwaltung Kosten" data-url="{{ route('get_house_management_cost') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">HAUSVERWALTUNG KOSTEN</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 house-management-cost-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table {{$alex_class	}}" data-table="mahnung-properties" data-title="Mahnung" data-url="{{ route('getMahnungProperties') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Mahnungen (30 Tage)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 reminder-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table " data-table="bank-properties" data-title="Bankenfinanzierungen" data-url="{{ route('getbankproperty') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">BANKENFINANZIERUNGEN</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 bank-finance-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="financing-per-bank-table" data-title="Finanzierungen pro Bank" data-url="{{ route('getFinancingPerBank') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Finanzierungen pro Bank</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 bank-finance-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 {{$alex_class}}" id="released-commission-card">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">FREIGEGEBENE PROVISIONEN</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 released-commission-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 {{$alex_class}}" id="sales_portal_evaluation" data-url="{{ route('sales_portal_evaluation') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">AUSWERTUNGEN VERKAUFSPORTAL</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 sales-portal-evaluation-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" data-table="standard-land-value-table" data-title="Grund und Bodenrichtwert" data-url="{{ route('get_standard_land_value') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Grund und Bodenrichtwert</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 standard-land-value-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      @if(Auth::user()->email != config('users.falk_email'))
         {{-- <div class="col-md-3 load-table" data-table="table-property-open-invoice" data-url="{{ route('amdashboard_get_openinvoice') }}?flag=1" data-title="Offene Rechnungen">
           <div class="card pointer-cursor">
               <div class="card-body">
                   <h5 class="card-title text-uppercase">Offene Rechnungen</h5>
                   <div class="text-right">
                       <h2 class="mt-2 display-7 open-invoice-total">....</h2>
                   </div>
               </div>
           </div>
         </div> --}}
      @endif

      @if($user->email==config('users.falk_email'))

         <div class="col-md-3 {{$alex_class}}" id="card-release-invoice" data-url="{{ route('release_invoice') }}">
            <div class="card pointer-cursor">
               <div class="card-body">
                  <h5 class="card-title text-uppercase">Freigegebene Rechnungen</h5>
                  <div class="text-right">
                     <h2 class="mt-2 display-7 release-invoice-total">....</h2>
                  </div>
                  <br>
               </div>
            </div>
         </div>

      @endif

      <div class="col-md-3 load-table {{$alex_class}}" data-table="property-mail-table" data-title="Mails an AM/User" data-url="{{ route('get_property_mail') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Mails an AM/User</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 property-mail-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" id="liquiplanung" data-table="liquiplanung-table" data-title="Cashflow" data-url="{{ route('get_liquiplanung') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Cashflow</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 liquiplanung-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 load-table" id="utility-prices" data-table="table-utility-prices" data-title="Versorgerpreise" data-url="{{ route('get_utility_prices') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">Versorgerpreise</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 utility-prices-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      
    
      <div class="col-md-3 load-table {{$alex_class}}" data-table="comment-table" data-title="KOMMENTARE" data-url="{{ route('get_comments') }}">
         <div class="card pointer-cursor" >
            <div class="card-body">
               <h5 class="card-title text-uppercase">KOMMENTARE</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 comment-total"></h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 {{$alex_class}}">
         <div class="card">
            <div class="card-body">
               <h5 class="card-title text-uppercase">Newsletteranmeldungen (gesamt)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7 newsletter-subscriptions-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 {{$alex_class}}">
         <div class="card">
            <div class="card-body">
               <h5 class="card-title text-uppercase">Website FCR (30 Tage)</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7" id="website-visitor">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 {{$alex_class}}">
         <div class="card">
            <div class="card-body">
               <h5 class="card-title text-uppercase">Facebook</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7" id="facebook-likes">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

      <div class="col-md-3 {{$alex_class}}">
         <div class="card">
            <div class="card-body">
               <h5 class="card-title text-uppercase">Instagram</h5>
               <div class="text-right">
                  <h2 class="mt-2 display-7" id="instagram-followers">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>

   </div>

   <div class="row custum-row-2 {{$alex_class}}">
      <div class="col-md-6" style="padding: 0 15px;">
         <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-6">
               <div class="card" style="height: 160px !important;">
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Verkaufsportal</h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal">Anmeldungen  (ALLE) : <span id="makler_userall">....</span></h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal">Makler mit NDA (ALLE) : <span id="activecountall">....</span></h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal get-verkauf-user"  data-active="0">Anmeldungen  (7 Tage) : <span id="makler_user">....</span></h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal get-verkauf-user" data-active="1">Makler mit NDA (7 Tage) : <span id="activecount">....</span></h5>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="card" style="height: 160px !important;">
                  <div class="card-body">
                     <h5 class="card-title text-uppercase">Vermietungsportal</h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal">Anmeldungen  (ALLE) : <span id="vmakler_userall">....</span></h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal">Aktiviert (ALLE) : <span id="vactivecountall">....</span></h5>
                     <h5 class="get-vermietung-user div-user-count card-title text-uppercase font-normal" data-active="0">Anmeldungen  (7 Tage) : <span id="vmakler_user">....</span></h5>
                     <h5 class="get-vermietung-user div-user-count card-title text-uppercase font-normal" data-active="1">Aktiviert (7 Tage) : <span id="vactivecount">....</span></h5>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <?php
      $year = date("Y");
      ?>

      <div class="col-md-6 {{$alex_class}}">
         <div style="height:385px; padding: 17px 10px; background: white; margin-top: 5px;">
            <select autocomplete="off" id="selectedYear" class="dropdown">
               {{-- <option value="" selected="selected">Select Year</option> --}}
               @for($i = -5;$i<=5;$i++)
                  @if($i==0)
                  <option value="{{$year-$i}}" selected="selected">{{$year-$i}}</option>
                  @else
                     <option value="{{$year-$i}}">{{$year-$i}}</option>
                  @endif
               @endfor

            </select>
            <div id="basic-bar" style="height:328px; padding: 10px; background: white; ">
               <div class="loading"></div>
            </div>
         </div>
      </div>

      </div>
      <div class="clearfix"></div>
   </div>
</div>

@include('dashboard.optimized.dashboard_modal')

@endsection
@section('js')
   <script type="text/javascript">
      var _token                                = '{{ csrf_token() }}';
      var url_einkaufsendmail                   = "{{url('property/einkaufsendmail') }}"
      var url_releaseprocedure                   = "{{url('property/releaseprocedure') }}"
      var url_dashboard_counter                 = '{{ route('get_dashboard_counter') }}';
      var url_releaseoplist                 = '{{ route('releaseoplist') }}';

      var url_getInsurancePropertyList          = '{{ route('getInsurancePropertyList') }}';
      var url_propertyManagementNotRelease          = '{{ route('propertyManagementNotRelease') }}';

      var url_dashboard_counter1                 = '{{ route('get_dashboard_counter1') }}';
      var url_delete_property_invoice           = '{{ route("delete_property_invoice", ":id") }}';
      var url_property_invoicereleaseprocedure  = '{{ url('property/invoicereleaseprocedure') }}';
      var url_property_invoicenotrelease        = '{{ url('property/invoicenotrelease') }}';
      var url_property_invoicemarkpending       = '{{ url('property/invoicemarkpending') }}';
      var url_property_invoicereleaseprocedure  = '{{ url('property/invoicereleaseprocedure') }}';
      var url_property_contractreleaseprocedure = '{{ url('property/contractreleaseprocedure') }}';
      var url_contract_mark_as_notrelease       = '{{ route('contract_mark_as_notrelease') }}';
      var url_contract_mark_as_pending          = '{{ route('contract_mark_as_pending') }}';
      var url_update_versicherung_approved_status = '{{ url('/update_versicherung_approved_status') }}';
      var url_update_falk__status               = '{{ url('/update_falk__status') }}';
      var url_property_addmaillog               = '{{ url('property/addmaillog') }}';
      var url_setAssestManagerOnProperty        = '{{ route("setAssestManagerOnProperty", ["property_id" => ":property_id", "am_id" => ":am_id"]) }}';
      var url_assetmanagement                   = '{{ route('assetmanagement') }}';
      var url_property_dealreleaseprocedure     = '{{ url('property/dealreleaseprocedure') }}';
      var url_deal_mark_as_notrelease           = '{{ route('deal_mark_as_notrelease') }}';
      var url_insurance_mark_as_notrelease      = '{{ route('insurance_mark_as_notrelease') }}';
      var url_insurance_mark_as_pending         = '{{ route('insurance_mark_as_pending') }}';
      var url_insurance_mark_as_notrelease_am   = '{{ route('insurance_mark_as_notrelease_am') }}';
      var url_property_management_update        = '{{ route('property_management.update') }}';
      var url_property_provisionreleaseprocedure= '{{ url('property/provisionreleaseprocedure') }}';
      var url_monthyearmanager                  = '{{ route('get_monthyearmanager') }}';
      var url_getmanagerproperty                = '{{ route('getmanagerproperty') }}';
      var url_getloi                            = '{{ route('getloi', ['month' => ':month']) }}';
      var url_statusloi_update                  = '{{ url('statusloi/update') }}';
      var url_getbanklist                       = '{{ route('getbanklist') }}';
      var url_bank_finance_offers_manual        = '{{ route('getBankFinanceOffers') }}';
      var url_bank_finance_offers_automatic        = '{{ route('getAllBankMailLogsOfProperty') }}';
      var url_get_rental_overview               = '{{ route('get_rental_overview') }}';
      var url_get_default_payers                = '{{ route('get_default_payers') }}';
      var url_get_oplist                = '{{ route('get_oplist') }}';
      var url_get_hausmaxx_data                 = '{{ route('get_hausmaxx_data') }}';
      var url_getMahnungTenant                  = '{{ route('getMahnungTenant') }}';
      var url_tenant_detail                     = '{{ route('tenant-detail') }}';
      var url_mailchimp                         = '{{ route('mailchimp') }}';
      var url_getSocialFollowers                = '{{ route('getSocialFollowers') }}';
      var url_monthyearassetmanager             = '{{ route('monthyearassetmanager') }}';
      var url_getassetmanagerproperty           = '{{ route('getassetmanagerproperty') }}';
      var url_getcategoryproperty               = '{{ route('getcategoryproperty') }}';
      var url_getcategoryproperty3              = '{{ route('getcategoryproperty3') }}';
      var url_get_property_provision            = '{{ route('getPropertyProvision') }}';
      var url_getvacantlist                     = '{{ route('getVacantList') }}';
      var url_get_sales_portal_count            = '{{ route('get_sales_portal_count') }}';
      var url_getusers                          = '{{ route('getusers') }}';
      var url_get_barchart_data                 = '{{ route('get_barchart_data', ":id") }}';
      var url_getStatusLoiByUserIdDashboard     = '{{ route('getStatusLoiByUserIdDashboard') }}';
      var url_property_vacantreleaseprocedure   = '{{ url('property/vacantreleaseprocedure') }}';
      var url_property_vacantmarkednotrelease   = '{{ url('property/vacantmarkednotrelease') }}';
      var url_comment_detail                    = '{{ route('comment-detail') }}';
      var url_makeAsPaid                        = '{{ route('makeAsPaid') }}';
      var url_email_and_finance                 = '{{ route('getemailTemplateAndFinance') }}';
      var url_get_by_status                     = '{{ route('get_funding_request') }}';
      var url_getbankproperty                   = '{{ route('getbankproperty') }}';
      var in_active_user                        = '{{ implode(",", $deactiveUser) }}';
      var in_active_user_arr                    = (in_active_user != '') ? in_active_user.split(",") : [];
      var url_get_liquiplanung_1_data           = '{{ route('get_liquiplanung_1_data') }}';
      var url_delete_insurance_detail_comment   = '{{ route('delete_insurance_detail_comment', ['id' => ':id']) }}';
      var url_delete_property_comment           = '{{ route('delete_property_comment', ['id' => ':id']) }}';
      var url_add_property_comment              = '{{ route('add_property_comment') }}';
      var url_get_property_comment              = '{{ route('get_property_comment') }}';
      var url_delete_new_vacant                 = '{{ route('delete_new_vacant') }}';
      var url_getItemComment                    = '{{ route('getItemComment') }}';
      var url_addItemComment                    = '{{ route('addItemComment') }}';
      var url_deleteItemComment                 = '{{ route('deleteItemComment') }}';
      var url_get_all_users                     = '{{ route('get_all_users') }}';
      var url_delete_recommended_comment        = '{{ route('delete_recommended_comment', ['id' => ':id']) }}';
      var url_get_recommended_comment           = '{{ route('get_recommended_comment', ['id' => ':id']) }}';

      var url_invoice_release_am                = '{{ route('invoice_release_am') }}';
      var url_invoice_release_hv                = '{{ route('invoice_release_hv') }}';
      var url_invoice_release_user              = '{{ route('invoice_release_user') }}';
      var url_invoice_release_falk              = '{{ route('invoice_release_falk') }}';

   </script>
   <script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
   <script src="{{asset('js/highcharts.js')}}"></script>
   <script src="{{asset('js/echarts-en.min.js')}}"></script>
   <script src="{{asset('js/custom-datatable.js')}}"></script>
   <script src="{{asset('js/dashboard-optimized.js')}}"></script>

   @if(isset($_REQUEST['section']) && $_REQUEST['section'])
      <script type="text/javascript">
         $(document).ready(function(){
            $('#{{$_REQUEST['section']}}').trigger('click');
         })
      </script>
   @endif

@endsection
