
<div class=" modal fade" role="dialog" id="table-data-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase"></h4>
         </div>
         <div class="modal-body table-responsive" style="min-height: 20%;">

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="invoice-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="invoice-release-property-modal2" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <p class="i-message hidden">Möchten Sie die Rechnung wirklich an Falk zur Freigabe senden?</p>
            <label class="i-message2">Kommentar</label>
            <textarea class="form-control invoice-release-comment2 i-message2" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="pending-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-pending-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-pending-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="multiple-invoice-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control multiple-invoice-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary multiple-invoice-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="invoice-release-property-modal-am" role="dialog">
       <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
             <div class="modal-body">
                <label class="">Kommentar</label>
                <textarea class="form-control invoice-release-comment-am" name="message"></textarea>
                <br>
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-primary invoice-release-submit-am" data-dismiss="modal" >Senden</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
             </div>
          </div>
       </div>
    </div>

<div class="modal fade" id="contract-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control contract-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary contract-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="contracts_mark_as_not_release">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="post" id="form_contracts_mark_as_not_release">
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <span id="contracts_mark_as_not_release_error"></span>
                  </div>
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="input_contracts_mark_as_not_release" name="comment" required></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>

   </div>
</div>

<div class=" modal fade" role="dialog" id="contracts_mark_as_pending">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="post" id="form_contract_mark_as_pending">
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <span id="contracts_mark_as_pending_error"></span>
                  </div>
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="input_contracts_mark_as_pending" name="comment" required></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>

   </div>
</div>

<div class="modal fade" id="insurance-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="not-release-management-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control not-release-management-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary not-release-management-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="rent-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true" style="z-index: 99999;">
   <div class="modal-dialog custom" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Mieter</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body list-data-rent">
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="vacant-list" style="z-index:99999" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Leerstandsflächen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body vlist-data-rent">
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="property-insurance-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control property-insurance-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary property-insurance-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="deal-not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control deal-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary deal-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="insurance-not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="insurance-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="provision-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control provision-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary provision-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true" style="z-index: 9999">
   <div class="modal-dialog modal-lg" role="document" style="width: 85%;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body list-data">
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="detail_graph" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true" style="z-index: 9999">
   <div class="modal-dialog " role="document" style="width: 60%;" >
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 class="modal-title" id="exampleModalLabel" style="text-align:center; font-weight:bold; margin-bottom:30px">Einzelheiten</h2>
            <div style="max-height:400px;overflow:auto;">
            <table id="classTable" class="table table-bordered" style="text-align:center;" >
               <thead>
                  <tr>
                     <td>Immobilien-ID</td>
                     <td>Name des Anwesens</td>
                     <td>Name des Mieters</td>
                     <td>Miete</td>
                   </tr>
               </thead>
               <tbody>
                 <tr></tr>
                 <div id="rows_for_table"></div>
               </tbody>
             </table>

                <table id="testTable" class="table table-bordered" style="text-align:center;" >
                    <thead>
                    <tr>
                        <td>Name des Anwesens</td>
                        <td>Gesamtmiete</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr></tr>
                    <div id="test"></div>
                    </tbody>
                </table>
            </div>

         </div>
      </div>
   </div>
</div>

<div class="modal fade"  id="bank-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" style="width: 90%" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body list-data-banks row">
         </div>
      </div>
   </div>
</div>


<div class="modal fade"  id="email-template" tabindex="9999" role="dialog"  aria-hidden="true">
   <div class="modal-dialog modal-lg" style="width: 93%" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body email-template-data row">
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="cate-pro-listing" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true" style="z-index: 9999;">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body cate-pro-div">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="hausmax-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document" style="width: 90%;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Hausmaxx</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body hausmax-list-data">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="get-tenant-mahnung-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Mieterliste</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body get-tenant-mahnung-list-data">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="tenant-detail-modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               <!-- Mieterliste -->
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body tenant-detail">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="tenant-detail-modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               <!-- Mieterliste -->
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body tenant-detail">
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="neue_mv_modal" role="dialog" aria-hidden="true">
   <div class="modal-dialog" role="document" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Vermietungsaktivitäten</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12">
                  <?php
                     $year = range(2018,date('Y'));
                     $months = range(1,12);
                  ?>
                  Neue
                  <select class="type-selection achange-select">
                     <option value="0">All</option>
                     <option value="1">Mietverträge</option>
                     <option value="2">Verlängerung </option>
                  </select>
                  <select class="pa-month achange-select">
                     <option value="">All</option>
                     @foreach($months as $list)
                     <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                     @endforeach
                  </select>
                  <!-- {{__('dashboard.'.date('F'))}} -->
                  <select class="pa-year achange-select">
                  @foreach($year as $list)
                  <option
                  @if($list==date('Y')) selected @endif
                  value="{{$list}}">{{$list}}</option>
                  @endforeach
                  </select>
                  <a href="javascript:void(0);" data-url="{{route('monthyearassetmanager')}}?export=1&month=&year={{date('Y')}}" data-org-url="{{route('monthyearassetmanager')}}?export=1&month=&year={{date('Y')}}" class="pull-right btn btn-success btn-export">Export</a>
               </div>
            </div>
            <br>
            <div class="row">
               <div class="col-md-12" id="neue_mv_html" style="min-height: 10%;">

               </div>
            </div>
         </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="asset-property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document" style="width: 85%;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body asset-list-data">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="iproperty-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body insurance-list-data">
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="rental_overview_modal" role="dialog" aria-hidden="true">
   <div class="modal-dialog" role="document" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <h3>Mieterübersicht</h3>

            <div class="row">
               <div class="col-md-6">
                  <div class="row">
                     <div class="col-md-12">
                        <h3 class="box-title m-b-0">
                           <a href="javascript:void(0);" data-url="{{route('getcategoryproperty')}}?export=1" class="pull-right btn btn-success btn-export">Export</a>
                        </h3>
                     </div>
                     <div class="col-md-12">
                        <div class="table-responsive category-prop-list2"><div class="loading"></div></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div id="high-container3" style="min-width: 310px; height: 400px; margin: 0 auto"><div class="loading"></div></div>
               </div>
            </div>

            <h3>Mieterübersicht (Corona)</h3>
            <div class="row">
               <div class="col-md-6">
                  <div class="table-responsive category-prop-list4"><div class="loading"></div></div>
               </div>
               <div class="col-md-6">
                  <div class="table-responsive category-prop-list5"><div class="loading"></div></div>
               </div>
            </div>

            <h3>Mieterübersicht gesamt</h3>
            <div class="row">
               <div class="col-md-12">
                  <div class="table-responsive category-prop-list6"><div class="loading"></div></div>
               </div>
            </div>

         </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="released_commission_modal" role="dialog" aria-hidden="true">
   <div class="modal-dialog" role="document" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title">FREIGEGEBENE PROVISIONEN</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <?php
               $year = range(2018,date('Y'));
               $months = range(1,12);
            ?>
            <div class="row">
               <div class="col-md-12">
                  <select class="pp-month pchange-select">
                      <option value="">All</option>
                      @foreach($months as $list)
                      <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                      @endforeach
                  </select>
                      <!-- {{__('dashboard.'.date('F'))}} -->
                  <select class="pp-year pchange-select">
                      @foreach($year as $list)
                      <option
                      @if($list==date('Y')) selected @endif
                      value="{{$list}}">{{$list}}</option>
                      @endforeach
                  </select>
               </div>
            </div>
            <br>
            <div class="row">
               <div class="col-md-12 property-provision-div-4">
                  <div class="loading"></div>
               </div>
            </div>
         </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="sales-portal-evaluation-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">AUSWERTUNGEN VERKAUFSPORTAL</h4>
         </div>
         <div class="modal-body" style="min-height: 20%;">

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="send-mail-to-user-modal">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mail senden an <span id="send-mail-to-name"></span></h4>
         </div>
         <form action="{{ route('sendmail_to_user') }}" method="POST">
            <div class="modal-body">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <input type="hidden" name="email" value="">
               <input type="hidden" name="name" value="">
               <label>Betreff</label>
               <input type="text" name="subject" class="form-control" required>
               <br>
               <label>Nachricht</label>
               <textarea class="form-control" name="message" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="user-list-popup" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body user-list">
         </div>
      </div>
   </div>
</div>
<div class=" modal fade" role="dialog" id="load_all_status_loi_by_user">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div id="load_all_status_loi_by_user_content">
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="vacant-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control vacant-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary vacant-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="vacant-not-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control vacant-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary vacant-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="modal_invoice_sendmail_to_am" style="z-index: 9999;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mail an AM</h4>
         </div>
         <form action="{{ route('invoice_sendmail_to_am') }}" id="form_invoice_sendmail_to_am">
            <input type="hidden" name="property_id">
            <input type="hidden" name="content">
            <input type="hidden" name="subject">
            <input type="hidden" name="mail_type">
            <input type="hidden" name="id">
            <input type="hidden" name="section">
            <div class="modal-body">
               <label>Nachricht</label>
               <textarea class="form-control" name="message" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="modal_sendmail_to_custom_user" style="z-index: 9999;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mail To User</h4>
         </div>
         <form action="{{ route('sendmail_to_custom_user') }}" id="form_modal_sendmail_to_custom_user">
            <input type="hidden" name="property_id">
            <input type="hidden" name="user_id">
            <input type="hidden" name="subject">
            <input type="hidden" name="content">
            <input type="hidden" name="email">
            <input type="hidden" name="mail_type">
            <input type="hidden" name="id">
            <input type="hidden" name="section">
            <div class="modal-body">
               <label>Nachricht</label>
               <textarea class="form-control" name="message" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade" id="comment-detail-popup" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="comment-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body comment-detail-body">
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="release-invoice-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">Freigegebene Rechnungen</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12 table-responsive">
                  <table class="table table-striped" id="release-invoice-table">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Objekt</th>
                           <th>Rechnung</th>
                           <th>Re. D.</th>
                           <th>Re. Bet.</th>
                           <th>Kommentar Rechnung</th>
                           <th>Abbuch.</th>
                           <th>User</th>
                           <th>Freigabedatum</th>
                           <th>Kommentar Falk</th>
                           <th>Weiterleiten an</th>
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="release-contract-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">Auslaufende Verträge</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-12 table-responsive">
                  <table class="table table-striped" id="release-contract-table">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Objekt</th>
                           <th>Datei</th>
                           <th>Betrag</th>
                           <th>Startdatum</th>
                           <th>Abschluss am</th>
                           <th>Kündigung spätestens</th>
                           <th>Kündigung am</th>
                           <th>Kommentar Verträge</th>
                           <th>User</th>
                           <th>Datum</th>
                           <th>Kategorie</th>
                           <th>Weiterleiten an</th>
                        </tr>
                     </thead>
                     <tbody></tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="finance-per-bank-modal" role="dialog">
   <div class="modal-dialog" style="width: 85%;">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">BANKENFINANZIERUNGEN</h4>
         </div>
         <div class="modal-body">
           
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>


<div class="modal fade" id="irelease-property-release" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <!-- <h4>Möchtest du dieses Objekt wirklich freigeben?</h4> -->
        <label>Kommentar</label>
        <textarea class="form-control einkauf-comment" name="message"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary einkauf-release-submit" data-dismiss="modal" >Senden</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="irelease-property" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <!-- <h4>Möchtest du dieses Objekt wirklich freigeben?</h4> -->
        <label>Kommentar</label>
        <textarea class="form-control f-comment" name="message"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary irelease-the-property" data-dismiss="modal" >Senden</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

<div class="modal fade" id="modal-forward-to" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Weiterleiten an</h4>
      </div>
      <form id="form-forward-to" action="{{ route('mail_forward_to') }}">
         <div class="modal-body">

            <input type="hidden" name="property_id">
            <input type="hidden" name="subject">
            <input type="hidden" name="title">
            <input type="hidden" name="content">
            <input type="hidden" name="section_id">
            <input type="hidden" name="section">

            <?php 
               $all_users = DB::table('users')->select('id', 'name')->where('user_deleted', 0)->where('user_status', 1)->get();
            ?>

            <label>User</label>
            <select class="form-control" name="user" style="width: 100%;">
               <option value="">Select User</option>
               @if($all_users)
                  @foreach ($all_users as $usr)
                     <option value="{{ $usr->id }}">{{ $usr->name }}</option>
                  @endforeach
               @endif
            </select>
            <br/>
            <br/>

            <label>Oder E-Mail</label>
            <input type="text" name="email" class="form-control">
            <br/>

            <label>Kommentar</label>
            <textarea class="form-control" name="comment" rows="5" required></textarea>
            <br/>

         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-primary">Senden</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </form>
    </div>

  </div>
</div>


<div class="modal fade" id="modal_op_list" role="dialog">
  <div class="modal-dialog" style="width: 80%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">OP LISTE</h4>
      </div>
      <div class="modal-body">
         <h3>Forderungsmanagment</h3>
         <div id="rent-paid-data"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>

<div class=" modal fade" role="dialog" id="rent-paid-month-wise-detail-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" id="rent-paid-month-wise-detail-div">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class=" modal fade" role="dialog" id="liquiplanung-data-modal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title liquiplanung-data-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 table-responsive" id="liquiplanung-data-table-div">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="insurance-not-release-am-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-not-release-am-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-not-release-am-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="insurance-pending-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-pending-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-pending-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="insurance_detail_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form id="insurance_detail_comment_form" action="{{ route('add_property_insurance_comment') }}">
               <div class="row">
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="insurance_detail_comment" name="comment" required></textarea>
                  </div>
               </div>
               <br/>
               <div class="row">
                  <div class="col-md-12 text-center">
                     <button type="submit" class="btn btn-primary">Posten</button>
                  </div>
               </div>
            </form>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="insurance_detail_comments_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray">Name</th>
                          <th class="bg-light-gray">Kommentar</th>
                          <th class="bg-light-gray">Datum</th>
                          <th class="bg-light-gray">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="insurance_detail_comments_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="property_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">

            <div class="row property-comment-section">
               <div class="col-md-12 form-group">
                  <label>Kommentar</label>
                  <textarea class="form-control property-comment" rows="5"></textarea>
               </div>
               <div class="col-md-12 text-center">
                  <button type="button" class="btn btn-primary btn-add-property-comment" data-reload="1" data-record-id="" data-property-id="" data-type="" data-subject="" data-content=''>Senden</button>
               </div>
            </div>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="property_comment_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray" id="th_name">Name</th>
                          <th class="bg-light-gray" id="th_comment">Kommentar</th>
                          <th class="bg-light-gray" id="th_date">Datum</th>
                          <th class="bg-light-gray" id="th_action">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="property_comment_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="modal-vacancy-export" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">LEERSTÄNDE (€) EXPORT</h4>
      </div>
      <form id="form-vacancy-export" action="{{ route('vacancy_export') }}">
         <div class="modal-body">

            <input type="hidden" name="ids">

            <label>Email</label>
            <input type="email" name="email" class="form-control" required>
            <br/>

            <label>Subject</label>
            <input type="text" name="subject" class="form-control" value="Leerstandsflächen Exposé" required>
            <br/>

            <label>Nachricht</label>
            <textarea class="form-control" name="message" required></textarea>
            <br/>

         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-primary">Senden</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </form>
    </div>

  </div>
</div>

<div class="modal fade" id="show_ao_cost" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
         <div class="row">
            <div class="col-lg-12">
               <div class="table-responsive">
                   <table class="table table-striped" id="table_ao_cost">
                       <thead>
                          <tr>
                             <th>Frequenz</th>
                             <th>Notizen</th>
                             <th>Beträge</th>
                             <th>Monat</th>
                             <th>Jahr</th>
                             <th>Aktion</th>
                          </tr>
                       </thead>
                       <tbody></tbody>
                   </table>
              </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="item_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">

            <div class="row item-comment-section">
               <div class="col-md-12 form-group">
                  <label>Kommentar</label>
                  <textarea class="form-control item-comment" rows="5"></textarea>
               </div>
               <div class="col-md-12 text-center">
                  <button type="button" class="btn btn-primary btn-add-item-comment">Senden</button>
               </div>
            </div>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="item_comment_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray" id="th_name">Name</th>
                          <th class="bg-light-gray" id="th_comment">Kommentar</th>
                          <th class="bg-light-gray" id="th_date">Datum</th>
                          <th class="bg-light-gray" id="th_action">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="item_comment_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="vacant-pending-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control vacant_pending_comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary vacant_pending_submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="recommended_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form id="recommended_comment_form" action="{{ route('add_recommended_comment') }}">
               <div class="row">
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="recommended_comment" name="comment" rows="5" required></textarea>
                  </div>
               </div>
               <br/>
               <div class="row">
                  <div class="col-md-12 text-center">
                     <button type="submit" class="btn btn-primary">Posten</button>
                  </div>
               </div>
            </form>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="recommended_comments_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray">Name</th>
                          <th class="bg-light-gray">Kommentar</th>
                          <th class="bg-light-gray">Datum</th>
                          <th class="bg-light-gray">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="recommended_comments_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-am-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_am') }}" id="invoice-release-am-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-am-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-hv-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_hv') }}" id="invoice-release-hv-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-hv-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-usr-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_user') }}" id="invoice-release-usr-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-usr-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label class="am-list">User</label>
                     <select class="am-list invoice_asset_manager form-control" name="user" required>
                        <option value="">{{__('dashboard.asset_manager')}}</option>
                        @if(isset($active_users) && $active_users)
                           @foreach($active_users as $list1)
                              <option value="{{$list1->id}}">{{$list1->name}}</option>
                           @endforeach
                        @endif
                     </select>
                  </div>
               </div>

               {{-- <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div> --}}

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-falk-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_falk') }}" id="invoice-release-falk-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-falk-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>