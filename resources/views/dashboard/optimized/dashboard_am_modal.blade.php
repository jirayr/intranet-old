<div class=" modal fade" role="dialog" id="table-data-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase"></h4>
         </div>
         <div class="modal-body table-responsive" style="min-height: 20%;">

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="rental_overview_modal" role="dialog" aria-hidden="true">
   <div class="modal-dialog" role="document" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <h3>Mieterübersicht</h3>

            <div class="row">
               <div class="col-md-6">
                  <div class="row">
                     <div class="col-md-12">
                        <h3 class="box-title m-b-0">
                           <a href="javascript:void(0);" data-url="{{route('getcategoryproperty')}}?export=1" class="pull-right btn btn-success btn-export">Export</a>
                        </h3>
                     </div>
                     <div class="col-md-12">
                        <div class="table-responsive category-prop-list2"><div class="loading"></div></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div id="high-container3" style="min-width: 310px; height: 400px; margin: 0 auto"><div class="loading"></div></div>
               </div>
            </div>

            <h3>Mieterübersicht (Corona)</h3>
            <div class="row">
               <div class="col-md-6">
                  <div class="table-responsive category-prop-list4"><div class="loading"></div></div>
               </div>
               <div class="col-md-6">
                  <div class="table-responsive category-prop-list5"><div class="loading"></div></div>
               </div>
            </div>

            <h3>Mieterübersicht gesamt</h3>
            <div class="row">
               <div class="col-md-12">
                  <div class="table-responsive category-prop-list6"><div class="loading"></div></div>
               </div>
            </div>

         </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="modal-vacancy-export" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">LEERSTÄNDE (€) EXPORT</h4>
      </div>
      <form id="form-vacancy-export" action="{{ route('vacancy_export') }}">
         <div class="modal-body">

            <input type="hidden" name="ids">

            <label>Email</label>
            <input type="email" name="email" class="form-control" required>
            <br/>

            <label>Subject</label>
            <input type="text" name="subject" class="form-control" value="Leerstandsflächen Exposé" required>
            <br/>

            <label>Nachricht</label>
            <textarea class="form-control" name="message" required></textarea>
            <br/>

         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-primary">Senden</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </form>
    </div>

  </div>
</div>