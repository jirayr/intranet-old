<?php
    function short_name($string){
       if($string){
           $arr = explode(" ", $string);
           if(count($arr)>1)
               return substr($arr[0], 0,1).substr($arr[1], 0,1);
           else
               return substr($string, 0,1);
       }
       return $string;
   }
?>
<div class="table-responsive">
   <table id="expiring_leases_table" class="table table-striped">
      <thead>
         <tr>
            <th>#</th>
            <th>AM</th>
            <th>Objekt</th>
            <th>Vermietet</th>
            <th>Mietende</th>
            <th>Kündigung bis zum</th>
            <th>Fläche (m2)</th>
            <th>IST-Nkm (€)</th>
            <th>Typ</th>
            <th>Kommentare</th>
         </tr>
      </thead>
      <tbody>
         @php $sumau=$amount = 0; @endphp  
         @foreach($data as $tenancy_item)
            @if(!$tenancy_item->rent_end || $tenancy_item->rent_end>date('Y-m-d'))
                <?php

                  $comment = DB::table('tenancy_schedule_comments as tc')->selectRaw('tc.*, u.name, u.role, u.company')->leftjoin('users as u', 'u.id', '=', 'tc.user_id')->where('tc.item_id', $tenancy_item->id)->whereNotNull('tc.comment')->where('tc.comment', '!=', '')->orderBy('tc.created_at', 'desc')->first();

                  $tenancy_item->creator_name = short_name($tenancy_item->creator_name);
                  $subject = 'AUSLAUFENDE MIETVERTRÄGE: '.$tenancy_item->object_name;
                  $content = 'Vermietet: '.$tenancy_item->name;

                  $taburl = route('properties.show',['property' => $tenancy_item->property_id]).'?tab=tenancy-schedule';
                  $taburl = "<a href='".$taburl."'>".$taburl."</a>";
                ?>
               <tr>
                  <td>{{$tenancy_item->id}}</td>
                  <td>
                    <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $tenancy_item->property_id }}" data-subject="{{ $subject }}" data-content="{{ $content }}" data-title="AUSLAUFENDE MIETVERTRÄGE">{{ $tenancy_item->creator_name }}</a>
                  </td>
                  <td>
                     <a href="{{route('properties.show',['property'=>$tenancy_item->property_id])}}">
                     {{$tenancy_item->object_name}}</a>
                  </td>

                  <td><a href="javacript:void(0)" data-id="{{$tenancy_item->id}}" class="tenancy_item">{{$tenancy_item->name}}</a></td>
                  <td>{{show_date_format($tenancy_item->rent_end)}}</td>
                  <td>
                    @if($tenancy_item->termination_by)
                      {{ ( date('Y-m-d', strtotime($tenancy_item->termination_by)) < date('Y-m-d') ) ? show_date_format($tenancy_item->rent_end) : show_date_format($tenancy_item->termination_by) }}
                    @endif
                  </td>
                  <td class="text-right">{{$tenancy_item->rental_space ? number_format($tenancy_item->rental_space,2,",",".") : 0}}</td>
                  <td class="text-right">{{$tenancy_item->actual_net_rent ? number_format($tenancy_item->actual_net_rent,2,",",".") : 0}}</td>
                  <td>{{$tenancy_item->type}}</td>
                  {{-- <td><span class="long-text">{!! $tenancy_item->comment !!}</span></td> --}}
                  <td>
                     @if($comment)
                         @php
                            $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                            $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                         @endphp
                         <p class="long-text" style="margin-bottom: 0px;">
                            <span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
                         </p>
                      @endif
                      <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $tenancy_item->id }}" data-type="0" data-property-id="{{ $tenancy_item->property_id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                  </td>
               </tr>
            @endif
            @php 
               if($tenancy_item->rent_end && $tenancy_item->rent_end>=date('Y-m-d') && $tenancy_item->rent_end < date('Y-m-d',strtotime('+3 months'))){
                  $sumau += 1; 
                  $amount +=$tenancy_item->actual_net_rent;
               }
            @endphp 
         @endforeach
      </tbody>
   </table>
   <div class="sumau" style="display: none">{{number_format($amount,2,',','.')}}€ ({{ $sumau }})</div>
   <input type="hidden" id="sum-tenancy-amount" value="">
</div>