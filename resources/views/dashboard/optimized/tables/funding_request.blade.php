<div class="table-responsive">
	<table id="funding-request-table" class="table table-striped">
		@isset($propertyStatuses)
		<div>
			<select style="width: 25%" class="form-control property-status change-status" >
				<option value="">Select</option>
				@foreach($propertyStatuses as $propertyStatus)
					<option value="{{$propertyStatus->id}}" {{ ($status == $propertyStatus->id) ? 'selected' : '' }}>{{$propertyStatus->name}}</option>
				@endforeach
			</select>
		</div>
		<br>
		@endisset
	    <thead>
	       <tr>
	          <th>#</th>
	          <th>#</th>
	          <th>Objekt</th>
	          <th>Status</th>
	          <th>Transaction manager</th>
	          <th>Aktivitäten</th>
	       </tr>
	    </thead>
	    <tbody>
	    	@if($data)
		       	@foreach($data as $k=>$list)
					@if(in_array($list->status_relation->id,[15,5,7,14,16,10,12]) && ($list->propertyBankEmailLogs->count() || $list->banking_offers->count()))
			       	<tr>
						<td></td>
						<td>{{$k+1}}</td>
			          	<td><a href="{{url('properties/'.$list->main_property_id.'?tab=bank-modal')}}">
			             	{{$list->name_of_property}}</a>
			          	</td>
						<td>@if(isset($list->status_relation)){{$list->status_relation->name}}@endif</td>
						<td>@if(isset($list->transaction_manager)){{$list->transaction_manager->name}}@endif</td>
						<td>@if(isset($list->propertyBankEmailLogs) &&isset($list->banking_offers))

								<a href="javacript:void(0)" class="email-finance" data-id="{{$list->main_property_id}}"> {{$list->propertyBankEmailLogs->count()+$list->banking_offers->count()}}</a>
							@endif
						</td>
			       	</tr>
					@endif
		       	@endforeach
		    @endif
	    </tbody>
	</table>
</div>
