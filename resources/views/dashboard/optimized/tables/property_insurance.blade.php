<?php
	$rbutton_title = "Freigeben";
	$release = 'btn btn-primary property-insurance-release-request';
	$release_type = "insurancetab_release"
?>
@if (count($data))
	@foreach($data as $property_insurance)


		<?php 
			$user = Auth::user();

			$detail = DB::table('property_insurance_tab_details as pid')
	            	->select('pid.id', 'pid.amount', 'pid.comment', 'pid.created_at', 'pid.file_basename', 'pid.file_type','pid.file_name', 'pid.asset_manager_status', 'pid.falk_status','u.name as user_name', 'u.id as user_id', 'pid.falk_comment',
	                	DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurancetab_release' AND selected_id = pid.id AND tab='insurancetab2' ) THEN 1 ELSE 0 END as is_release")
	            	)
	            	->join('users as u', 'u.id', '=', 'pid.user_id')
	            	->where('property_insurance_tab_id', $property_insurance->id)
	            	->where('pid.deleted', 0)
	            	->where('pid.not_release', 0)
	            	->havingRaw('is_release=0')
	            	->orderBy('id', 'DESC')
	            	->get();


	           $detail1 = DB::table('property_insurance_tab_details as pid')
	            	->select('pid.id',
	                	DB::raw("CASE WHEN EXISTS (SELECT id FROM properties_mail_logs WHERE (type = 'insurance_release' OR type = 'insurancetab_release') AND selected_id = pid.id AND (tab='insurance_tab' OR tab='insurancetab2')) THEN 1 ELSE 0 END as is_release"))
	            	->where('property_insurance_tab_id', $property_insurance->id)
	            	->where('pid.deleted', 0)
	            	->where('pid.not_release', 0)
	            	->havingRaw('is_release=1')
	            	->orderBy('id', 'DESC')
	            	->get();
		?>
		
		@if(count($detail) && count($detail1)==0)

			<div class="row section-ins-{{$property_insurance->id}}">
				<div class="col-md-12 ">
					<div class="table-responsive">
						<h3>
							<a href="{{route('properties.show',['property'=> $property_insurance->property_id])}}?tab=property_insurance_tab">{{$property_insurance->name_of_property}}</a>

							 / {{$property_insurance->hausmaxx}} 
							 / {{$property_insurance->title}} 
							<!-- <button data-id="<?=$property_insurance->id?>" type="button" class="btn <?=$release?>" data-column="<?=$release_type?>"><?=$rbutton_title?></button> -->
							<!-- <button data-id="<?=$property_insurance->id?>" type="button" class="btn btn-xs btn-primary deal-mark-as-not-release">Ablehnen</button> -->
						</h3>
						<table class="table table-striped tbl-insurance-tab-detail property-insurance-table" data-tabid="{{ $property_insurance->id }}" >
							<thead>
								<tr>
									<th>#</th>
									<th>Objekt</th>
									<th>Datei</th>
									<th>Betrag</th>
									<th>Kommentar</th>
									<th>AM</th>
									<th>User</th>
									<th>Datum</th>
									<th>Ablehnen AM</th>
									<th>Falk Kommantare</th>
									<th>Empfehlung</th>
									<th>Freigeben</th>
									<th>Pending</th>
									<th>Ablehnen</th>
									<th></th>
									<th>Weiterleiten an</th>
								</tr>
							</thead>
							<tbody>
								

								@if ($detail)
									@foreach ($detail as $key => $value)

										@php
											$comment = DB::table('properties_comments as pc')->selectRaw('pc.*, u.name, u.role, u.company')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.record_id', $value->id)->where('pc.type', 'property_insurance_tab_details')->orderBy('pc.created_at', 'desc')->first();
										@endphp

										<?php 
											$download_path = "https://drive.google.com/drive/u/2/folders/".$value->file_basename;
							                if($value->file_type == "file"){
							                    $download_path = "https://drive.google.com/file/d/".$value->file_basename;
							                }

							                $subject = 'Angebotsfreigabe: '.$property_insurance->name_of_property;
											$content = 'Angebot: <a title="'.$value->file_name.'" href="'.$download_path.'" target="_blank">'.$value->file_name.'</a>';
											if(isset($comment->comment) && $comment->comment){
												$content .= '<br/>Kommentar User: '.$comment->comment;
											}
										?>

										<tr>
											<td>{{ $key+1 }}</td>
											<td>
												<a href="{{route('properties.show',['property'=> $property_insurance->property_id])}}?tab=property_insurance_tab">
					                            	{{$property_insurance->name_of_property}}
					                        	</a>
											</td>
											<td>
												<a  target="_blank"  title="{{ $value->file_name }}"  href="{{ $download_path }}">{{ $value->file_name }}</a>
											</td>
											<td>{{ show_number($value->amount,2) }}</td>
											<td>
												@if($comment)
													@php
														$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
														$commented_user = $comment->name.''.$company;
													@endphp
													<p class="long-text" style="margin-bottom: 0px;">
														<span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
													</p>
												@endif
												<button type="button" class="btn btn-primary btn-xs btn-ins-comment" data-id="{{ $value->id }}" data-property-id="{{ $property_insurance->property_id }}" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}">Kommentar</button>
											</td>
											<td>

												<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $property_insurance->property_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="ANGEBOTSFREIGABEN" data-id="{{ $value->id }}" data-section="property_insurance_tab_details">{{ $property_insurance->am_name }}</a>

											</td>
											<td>
												<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $property_insurance->property_id }}" data-user-id="{{ $value->user_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="ANGEBOTSFREIGABEN" data-id="{{ $value->id }}" data-section="property_insurance_tab_details">{{ $value->user_name }}</a>
											</td>
											<td>{{ show_datetime_format($value->created_at) }}</td>
											<td>
												<button data-id="<?=$value->id?>" type="button" class="btn  btn-primary btn-not-release-am-insurance">Ablehnen AM</button>
											</td>
											<td>
												<a href="javascript:void(0);" class="inline-edit {{ ($user->email == config('users.falk_email')) ? 'angebote-comment' : '' }}" data-type="textarea" data-pk="falk_comment" data-placement="bottom" data-url="{{ route('update_property_insurance_detail_by_field', ['id' => $value->id]) }}" >{{ $value->falk_comment }}</a>
											</td>
											<td>
												@if($value->is_release)
													<input data-id='{{ $value->id }}' type="checkbox" class="" data-field="asset_manager_status" data-url="{{ route('update_property_insurance_status', ['id' => $value->id]) }}" {{ ( ($value->asset_manager_status == 1) ? 'checked' : '' ) }}>;
												@else
													<input data-id='{{ $value->id }}' type="checkbox" class="am_falk_status" data-field="asset_manager_status" data-url="{{ route('update_property_insurance_status', ['id' => $value->id]) }}" {{ ( ($value->asset_manager_status == 1) ? 'checked' : '' ) }}>
												@endif
											</td>
											<td>
												<button data-id="<?=$value->id?>" type="button" class="btn <?=$release?>" data-column="<?=$release_type?>"><?=$rbutton_title?></button>
											</td>
											<td>
												<button data-id="<?=$value->id?>" type="button" class="btn  btn-primary btn-pending-insurance">Pending</button>
											</td>
											<td>

												<button data-id="<?=$value->id?>" type="button" class="btn  btn-primary btn-not-release-insurance">Ablehnen</button>
											<!-- </td> -->

												@if($value->is_release)
													<!-- <input data-id='{{ $value->id }}' type="checkbox" class="" data-field="falk_status" data-url="{{ route('update_property_insurance_status', ['id' => $value->id]) }}" {{ ( ($value->falk_status == 1) ? 'checked' : '' ) }}>; -->
												@else
													<!-- <input data-id='{{ $value->id }}' type="checkbox" class="am_falk_status" data-field="falk_status" data-url="{{ route('update_property_insurance_status', ['id' => $value->id]) }}" {{ ( ($value->falk_status == 1) ? 'checked' : '' ) }}> -->
												@endif
											</td>
											<td>
												@if(!$value->is_release)
													{{-- <button type="button" data-id="{{ $value->id }}" data-url="{{ route('get_property_insurance_detail_by_id', ['id' => $value->id]) }}" class="btn btn-info btn-outline btn-circle btn-sm edit-insurance-tab-detail"><i class="fa fa-edit"></i></button> --}}
													<button type="button" data-url="{{ route('delete_property_insurance_detail', ['id' => $value->id]) }}" class="btn btn-info btn-outline btn-circle btn-sm delete-insurance-tab-detail"><i class="icon-trash"></i></button>
												@endif
											</td>
											
											<td>
												<button type="button" class="btn btn-primary btn-forward-to" data-property-id="{{ $property_insurance->property_id }}" data-id="{{ $value->id }}" data-subject="{{ $subject }}" data-content="" data-title="ANGEBOTSFREIGABEN" data-reload="0" data-section="property_insurance_tab_details">Weiterleiten an</button>
											</td>
										</tr>
									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="row" data-id="{{$property_insurance->id}}" data-pid="{{ $property_insurance->property_id }}" style="margin-top: 15px;">
				<div class="col-md-12 form-group">
					<label>Kommentar</label>
					<textarea class="form-control angebote-section-comment" rows="5"></textarea>
				</div>
				<div class="col-md-12">
					<button type="button" class="btn btn-primary btn-add-angebote-section-comment pull-left" data-url="{{ route('add_property_insurance_comment') }}">Senden</button>
					<button type="button" class="btn btn-primary btn-show-angebote-section-comment pull-right" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $property_insurance->id]) }}">Show Kommentar</button>
				</div>
			</div>

		@endif
	@endforeach
@else
	<p class="text-center">Data not found!</p>
@endif