<div class="table-responsive">
    <div class="property-table">
        <table class="table table-striped dashboard-table" id="financing-per-bank-table">
            <thead>
                <tr>
                    <th>Bank</th>
                    <th>Anzahl</th>
                    <th class="text-right">GS Bank</th>
                    <th class="text-right">Darlehen 30.06.2020 HGB</th>
                    <th class="text-right">Darlehen 31.12.2019 HGB</th>
                    <th class="text-right">Verkehrs-wert</th>
                    <th class="text-right">Delta</th>
                    <th class="text-right">Zins</th>
                    <th class="text-right">Zinsaufwand p.a. ann. per 31.12.2019</th>
                    <th class="text-right">Kapitaldienst p.a. ann. per 31.12.2019</th>
                </tr>
            </thead>
            <?php 
                $total_original_loan = 0;
                $total_delta2 = 0;
                $total_market_value = 0;
                $total_delta = 0;
                $total_interest_extend = 0;
                $total_loan_service = 0;
                $total_property = 0;
            ?>
            <tbody>
                @if($data)
                    @foreach ($data as $element)

                        <?php
                            $total_original_loan    += $element['original_loan'];
                            $total_delta2           += $element['delta2'];
                            $total_market_value     += $element['market_value'];
                            $total_delta            += $element['delta'];
                            $total_interest_extend  += $element['interest_extend'];
                            $total_loan_service     += $element['loan_service'];
                            $total_property         += $element['total_property'];
                        ?>

                        <tr>
                            <td>{{ $element['lender'] }}</td>
                            <td><a href="javascript:void(0);" class="finance_per_property" data-lender="{{ $element['lender'] }}">{{ $element['total_property'] }}</a></td>
                            <td class="text-right">{{ show_number($element['original_loan'], 2 , ",", ".") }}</td>
                            <td class="text-right">{{ show_number($element['delta3'], 2) }}</td>
                            <td class="text-right">{{ show_number($element['delta2'], 2 , ",", ".") }}</td>
                            <td class="text-right">{{ show_number($element['market_value'], 2 , ",", ".") }}</td>
                            <td class="text-right">{{ show_number($element['delta'], 2 , ",", ".") }}</td>
                            <td class="text-right">{{ ($element['zins']) ? $element['zins'].'%' : '' }}</td>
                            <td class="text-right">{{ show_number($element['interest_extend'], 2 , ",", ".") }}</td>
                            <td class="text-right">{{ show_number($element['loan_service'], 2 , ",", ".") }}</td>
                        </tr>

                    @endforeach
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <th class="text-center border-top-footer">Gesamt</th>
                    <th class="border-top-footer">{{ show_number($total_property, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer">{{ show_number($total_original_loan, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer"></th>
                    <th class="text-right border-top-footer total-delta2">{{ show_number($total_delta2, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer">{{ show_number($total_market_value, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer">{{ show_number($total_delta, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer"></th>
                    <th class="text-right border-top-footer">{{ show_number($total_interest_extend, 2 , ",", ".") }}</th>
                    <th class="text-right border-top-footer">{{ show_number($total_loan_service, 2 , ",", ".") }}</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>