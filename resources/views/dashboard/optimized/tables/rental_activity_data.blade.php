<div class="table-responsive">
   <table class="table table-striped" id="rental_activity_table" style="width: 100%;padding-left: 0px;">
      <thead>
         <tr>
            <th>#</th>
            <th>Asset Manager</th>
            <th>Objekt</th>
            <th>Leerstand</th>
            <th>Fläche (m2)</th>
            <th>Immoscout Upload</th>
            <th>Immowelt Upload</th>
            <th>Ebay Upload</th>
            <th>Regionale Zeitung</th>
            <th>Schaufenster ausgehängt</th>
            <th>E-Mails über Intranet</th>
            <th>Makler beauftragt (nicht exklusiv)</th>
            <th>Exposé</th>
         </tr>
      </thead>
      <tbody>
         @if ($data)
            @foreach ($data as $key => $value)

               @php
                  
                  $checkbox_status = $date_arrs = $comment_arrs = [];
                  if($value->vacant_uploading_statuses->count()){
                     foreach ($value->vacant_uploading_statuses as $k => $val) {
                        if($val->status)
                          $checkbox_status[$val->type] = "checked";

                        if($val->comment)
                          $date_arrs[$val->type] = show_date_format($val->updated_date);

                        $comment_arrs[$val->type] = $val->comment;
                     }
                  }

                  $checkbox_1 = '<input disabled type="checkbox" class="vacant_status_checkbox" data-field="status" value="1" '. ((isset($checkbox_status[1])) ? $checkbox_status[1] : '') .'>';
                  $checkbox_2 = '<input disabled type="checkbox" class="vacant_status_checkbox" data-field="status" value="2" '. ((isset($checkbox_status[2])) ? $checkbox_status[2] : '') .'>';
                  $checkbox_3 = '<input disabled type="checkbox" class="vacant_status_checkbox" data-field="status" value="3" '. ((isset($checkbox_status[3])) ? $checkbox_status[3] : '') .'>';
                  $checkbox_4 = '<input disabled type="checkbox" class="vacant_status_checkbox" data-field="status" value="4" '. ((isset($checkbox_status[4])) ? $checkbox_status[4] : '') .'>';
                  $checkbox_5 = '<input disabled type="checkbox" class="vacant_status_checkbox" data-field="status" value="5" '. ((isset($checkbox_status[5])) ? $checkbox_status[5] : '') .'>';
                  $checkbox_6 = '<input disabled type="checkbox" class="vacant_status_checkbox" data-field="status" value="6" '. ((isset($checkbox_status[6])) ? $checkbox_status[6] : '') .'>';
                  $checkbox_7 = '<input disabled type="checkbox" class="vacant_status_checkbox" data-field="status" value="7" '. ((isset($checkbox_status[7])) ? $checkbox_status[7] : '') .'>';

                  $link_1 = (isset($comment_arrs[1]) && $comment_arrs[1] != '') ? ' <a href="'.$comment_arrs[1].'" target="_blank"><i class="fa fa-external-link"></i></a>' : '';
                  $link_2 = (isset($comment_arrs[2]) && $comment_arrs[2] != '') ? ' <a href="'.$comment_arrs[2].'" target="_blank"><i class="fa fa-external-link"></i></a>' : '';
                  $link_3 = (isset($comment_arrs[3]) && $comment_arrs[3] != '') ? ' <a href="'.$comment_arrs[3].'" target="_blank"><i class="fa fa-external-link"></i></a>' : '';
                  $link_4 = (isset($comment_arrs[4]) && $comment_arrs[4] != '') ? ' <a href="'. asset('ad_files_upload/'.$comment_arrs[4]) .'" target="_blank"><i class="fa fa-external-link"></i></a>' : '';
                  $link_5 = (isset($comment_arrs[5]) && $comment_arrs[5] != '') ? ' <a href="'. asset('ad_files_upload/'.$comment_arrs[5]) .'" target="_blank"><i class="fa fa-external-link"></i></a>' : '';
                  $link_6 = (isset($comment_arrs[6]) && $comment_arrs[6] != '') ? ' <a href="'. asset('ad_files_upload/'.$comment_arrs[6]) .'" target="_blank"><i class="fa fa-external-link"></i></a>' : '';
                  $link_7 = (isset($comment_arrs[7]) && $comment_arrs[7] != '') ? ' '.$comment_arrs[7] : '';

                  $date_1 = (isset($date_arrs[1]) && $date_arrs[1] != '') ? '<br>'.$date_arrs[1] : '';
                  $date_2 = (isset($date_arrs[2]) && $date_arrs[2] != '') ? '<br>'.$date_arrs[2] : '';
                  $date_3 = (isset($date_arrs[3]) && $date_arrs[3] != '') ? '<br>'.$date_arrs[3] : '';
                  $date_4 = (isset($date_arrs[4]) && $date_arrs[4] != '') ? '<br>'.$date_arrs[4] : '';
                  $date_5 = (isset($date_arrs[5]) && $date_arrs[5] != '') ? '<br>'.$date_arrs[5] : '';
                  $date_6 = (isset($date_arrs[6]) && $date_arrs[6] != '') ? '<br>'.$date_arrs[6] : '';
                  $date_7 = (isset($date_arrs[7]) && $date_arrs[7] != '') ? '<br>'.$date_arrs[7] : '';

                  if(isset($comment_arrs[9])){
                     $checkbox_9 = '<input disabled type="checkbox" class="vacant_status_checkbox" data-field="status" value="9" '. ((isset($checkbox_status[9])) ? $checkbox_status[9] : '') .'>';
                     $link_9     = (isset($comment_arrs[9]) && $comment_arrs[9] != '') ? ' <a href="'. asset('ad_files_upload/'.$comment_arrs[9]) .'" target="_blank"><i class="fa fa-external-link"></i></a>' : '';
                     $date_9     = (isset($date_arrs[9]) && $date_arrs[9] != '') ? '<br>'.$date_arrs[9] : '';
                  }else{
                     $checkbox_9 = '<input disabled type="checkbox" class="vacant_status_checkbox" data-field="status" value="9" '. ( ($value->vacant_uploaded_pdf) ? 'checked' : '') .'> ';
                     $link_9     = ($value->vacant_uploaded_pdf) ? '<a href="'. asset('ad_files_upload/'.$value->vacant_uploaded_pdf) .'" target="_blank"><i class="fa fa-external-link"></i></a>' : '';
                     $date_9     = ($value->vacant_uploaded_pdf_date) ? '<br>'.show_date_format($value->vacant_uploaded_pdf_date) : '';
                  }

               @endphp

               <?php 
                  $subject = 'Vermietungsaktivitäten: '.$value->name_of_property;
                  $content = '';

                  if($value->name){
                     $content .= 'Leerstand: '.$value->name;
                  }
               ?>

               <tr>
                  <td>{{ $key+1 }}</td>
                  <td>
                     <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $value->property_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="VERMIETUNGSAKTIVITÄTEN">{{ $value->creator_name }}</a>
                  </td>
                  <td>
                     <a  href="{{ route('properties.show',['property'=>$value->property_id]) }}?tab=Leerstandsflächen">{{ $value->name_of_property }}</a>
                  </td>
                  <td>
                     <a href="{{ route('properties.show',['property'=>$value->property_id]) }}?tab=Leerstandsflächen">{{ $value->name }}</a>
                  </td>
                  <td>{{ ($value->vacancy_in_qm) ? number_format($value->vacancy_in_qm,2,",",".") : 0 }}</td>
                  <td><?= $checkbox_1.$link_1.$date_1; ?></td>
                  <td><?= $checkbox_2.$link_2.$date_2 ?></td>
                  <td><?= $checkbox_3.$link_3.$date_3 ?></td>
                  <td><?= $checkbox_4.$link_4.$date_4 ?></td>
                  <td><?= $checkbox_5.$link_5.$date_5 ?></td>
                  <td><?= $checkbox_6.$link_6.$date_6 ?></td>
                  <td><?= $checkbox_7.$link_7.$date_7 ?></td>
                  <td><?= $checkbox_9.$link_9.$date_9 ?></td>
               </tr>
            @endforeach
         @endif
      </tbody>
   </table>
</div>