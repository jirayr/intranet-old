<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-2">
				<select class="form-control col-md-2 hidden" id="bka-year" data-url="{{ route('get_bka') }}">
					<option value="2017" {{ ($year == 2017) ? 'selected' : '' }}>2017</option>
					<option value="2018" {{ ($year == 2018) ? 'selected' : '' }}>2018</option>
					<option value="2019" {{ ($year == 2019) ? 'selected' : '' }}>2019</option>
					<option value="2020" {{ ($year == 2020) ? 'selected' : '' }}>2020</option>
				</select>
			</div>
		</div>
	</div>
	
	<div class="col-md-12" style="margin-top: 10px;">
		<table id="bka-table" class="table table-striped">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th colspan="4" class="text-center">2017</th>
					<th colspan="4" class="text-center">2018</th>
					<th colspan="4" class="text-center">2019</th>
					<th colspan="4" class="text-center">2020</th>
				</tr>
		     	<tr>
			        <th>#</th>
			        <th>Objekt</th>
			        <th>AM</th>
			        <th>Hausverwaltung</th>
			        <th>Mieter</th>
			        <th>Datum</th>
			        <th>Betrag</th>
			        <th>Bezahldatum</th>
			        <th>Datei</th>
			        <th>Datum</th>
			        <th>Betrag</th>
			        <th>Bezahldatum</th>
			        <th>Datei</th>
			        <th>Datum</th>
			        <th>Betrag</th>
			        <th>Bezahldatum</th>
			        <th>Datei</th>
			        <th>Datum</th>
			        <th>Betrag</th>
			        <th>Bezahldatum</th>
			        <th>Datei</th>
		     	</tr>
		  	</thead>
			<tbody>
				@if($data)
					@php
						$no = 1;
						$years = range(2017,2020);
					@endphp
					@foreach ($data as $key => $value)
						<?php 
							if(($value->rent_begin != '' && $value->rent_begin > date('Y-m-d')) ||($value->rent_end != '' && $value->rent_end < date('Y-m-d')))
                				continue;
						?>
						@if( $value->actual_net_rent && $value->rental_space)
							<tr>
								<td>{{ $no }}</td>
								<td>
									<a href="{{route('properties.show',['property'=> $value->property_id])}}">
			                            {{$value->name_of_property}}
			                        </a>
								</td>

								<?php
								$subject = 'BKA-'.$year.': '.$value->name_of_property;
								$content = 'Mieter: '.$value->item_name;
								?>
								<td>
									<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $value->property_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="BKA">{{ get2lettersname($value->asset_manager) }}</a>
								</td>
								<td>{{ $value->hausmaxx }}</td>
								<td>{{ $value->item_name }}</td>
								
								@foreach($years as $list)
								@if(isset($files_array[$list][$value->id]))
								<?php
								// echo "here";
								$row = $files_array[$list][$value->id];
								// echo $row->id;
								?>
								<td>{{ show_date_format($row->bka_date) }}</td>
								<td class="text-right">{{ show_number($row->amount) }}</td>
								<td>{{ show_date_format($row->payment_date) }}</td>
								<td>
									@if($row->file_name)
								        <?php
								        
								          $download_path = "https://drive.google.com/drive/u/2/folders/".(isset($row->file_basename) ? $row->file_basename : '');
								          if($row->file_type == 'file'){
								            $download_path = "https://drive.google.com/file/d/".(isset($row->file_basename) ? $row->file_basename : '');

								            $download_path = 'https://intranet.fcr-immobilien.de/fcr_drive/download_invoice?file='.(isset($row->file_basename) ? $row->file_basename : '').'&file_dir='.(isset($row->file_dirname) ? $row->file_dirname : '');
								          } 
								        ?>
								        <a href="{{ $download_path }}"  target="_blank" title="{{ $row->file_name }}"><i class="fa fa-file" ></i></a>
								    @endif
								</td>
								@else
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								@endif
								@endforeach

							</tr>
							@php
								$no++;
							@endphp
						@endif
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>


