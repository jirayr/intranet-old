@if($res && count($res))
	@foreach ($res as $element)

		@php
			$detail = DB::table('property_insurance_tab_details as pid')
                            ->select('pid.*','u.name as user_name', 'p.name_of_property', 'pit.property_id', DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'insurancetab_release' AND selected_id = pid.id AND tab='insurancetab2' ) THEN 1 ELSE 0 END as is_release"))
                            ->join('property_insurance_tabs as pit', 'pit.id', '=', 'pid.property_insurance_tab_id')
                            ->join('properties as p', 'p.id', '=', 'pit.property_id')
                            ->join('users as u', 'u.id', '=', 'pid.user_id')
                            ->where('property_insurance_tab_id', $element->id)
                            ->where('pid.deleted', 0)
                            ->orderBy('pid.created_at', 'desc')
                            ->get();

            $logs = DB::table('properties_mail_logs')->where('property_id',$element->property_id)->where('tab','insurance_tab')->get();
	        $array = array();
	        foreach ($logs as $key => $value) {
	            $array[$value->record_id][$value->type] = 1;
	        }

	        $release = 'btn btn-xs btn-primary property-insurance-release-request';
            $release_type = "insurance_request";
            $rbutton_title = "Zur Freigabe an Falk senden";

            $email = strtolower($user->email);

            $list = array();
            if(isset($array[$element->id]))
                $list = $array[$element->id];

            if($email==config('users.falk_email')){
                $rbutton_title = "Freigeben";
                $release = 'btn btn-xs btn-primary property-insurance-release-request';
                $release_type = "insurance_release";
            }else{
                if($list && isset($list['insurance_request'])){
                  $release =  " btn-xs btn-success property-insurance-release-request";
                  $rbutton_title = "Zur Freigabe gesendet";
                }
            }
            if($list && isset($list['insurance_release'])){
              	$release =  " btn-success btn-xs";
              	$rbutton_title = "Freigegeben";
            }
		@endphp

		@if( count($detail) > 0 && !in_array($element->not_release, [1,2,3]))
			<div class="row section-ins-{{$element->id}}">
				<div class="col-md-12 ">
					<div class="table-responsive">
						<h3>
							<span>
								<a href="{{route('properties.show',['property'=> $element->property_id])}}?tab=property_insurance_tab">{{$element->name_of_property}}</a>
								 / {{$element->hausmaxx}} 
								 / {{$element->title}}
							 </span>

							 @if( $email == config('users.falk_email') )
							 	<button data-id="{{$element->id}}" type="button" class="btn btn-xs btn-primary deal-mark-as-not-release">Ablehnen</button>
							 @else
							 	<button data-id="{{$element->id}}" type="button" class="btn {{ $release }}" data-column="{{ $release_type }}">{{ $rbutton_title }}</button>
							 @endif
						</h3>

						<table class="table table-striped table-open-angebote" data-tabid="{{ $element->id }}" id="insurance-table-{{ $element->id }}">
							<thead>
								<tr>
									<th>#</th>
									<th>Datei</th>
									<th>Betrag</th>
									<th>Kommentar</th>
									<th>AM</th>
									<th>User</th>
									<th>Datum</th>
									<th>Ablehnen AM</th>
									<th>Empfehlung</th>
									{{-- <th>Freigeben</th> --}}
									<th>Pending</th>
									{{-- <th>Ablehnen</th> --}}
									<th>Action</th>
									<th>Weiterleiten an</th>
								</tr>
							</thead>
							<tbody>
								@if($detail)
									@foreach ($detail as $key => $value)

										@php
											$subject = 'Offene Angebote: '.$value->name_of_property;

							                $mail_button = '<button type="button" class="btn btn-primary btn-forward-to" data-property-id="'.$value->property_id.'" data-id="'.$value->id.'" data-subject="'.$subject.'" data-content="" data-title="OFFENE ANGEBOTE" data-reload="0" data-section="property_insurance_tab_details">Weiterleiten an</button>';

							                $delete_btn = '<button type="button" data-url="'. route('delete_property_insurance_detail', ['id' => $value->id]) .'" class="btn btn-info btn-outline btn-circle btn-sm delete-insurance-tab-detail"><i class="icon-trash"></i></button>';

							                $am_checkbox = '<input data-id='.$value->id.' type="checkbox" class="am_falk_status" data-field="asset_manager_status" data-url="'. route('update_property_insurance_status', ['id' => $value->id]) .'" '. ( ($value->asset_manager_status == 1) ? 'checked' : '' ) .'>';

											$falk_checkbox = "";
							                $not_release = "";
							                $not_release_am = "";
							                $pending = "";
							                if(in_array($value->property_insurance_tab_id, array(4,6,7,13,11,14,10,17,24,18,12,25,27,28))){
							                        $falk_checkbox = '<input data-id='.$value->id.' type="checkbox" class="" data-field="falk_status" data-url="'. route('update_property_insurance_status', ['id' => $value->id]) .'" '. ( ($value->falk_status == 1) ? 'checked' : '' ) .'>';
							                }else{

							                	$not_release_am = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-primary btn-not-release-am-insurance">Ablehnen AM</button>';

							                    $pending = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-primary btn-pending-insurance">Pending</button>';

							                    if($user->email==config('users.falk_email')){
							                        $rbutton_title = "Freigeben";
							                        $release = 'btn btn-xs btn-primary property-insurance-release-request';
							                        $release_type = "insurancetab_release";
							                        $falk_checkbox = '<button data-id="'.$value->id.'" type="button" class="btn '.$release.'" data-column="'.$release_type.'">'.$rbutton_title.'</button>';

							                        if($value->not_release == 0 && $value->is_release == 0){
							                            $not_release = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-primary btn-not-release-insurance">Ablehnen</button>';
							                            // $not_release_am = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-primary btn-not-release-am-insurance">Ablehnen AM</button>';
							                            // $pending = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-primary btn-pending-insurance">Pending</button>';
							                        }
							                    }
							                    if($value->is_release){
							                        $falk_checkbox = '<button data-id="'.$value->id.'" type="button" class="btn btn-xs btn-success">Freigegeben</button>';
							                    }
							                }

							                if($value->is_release){
							                    $delete_btn = "";
							                    $am_checkbox = '<input data-id='.$value->id.' type="checkbox" class="" data-field="asset_manager_status" data-url="'. route('update_property_insurance_status', ['id' => $value->id]) .'" '. ( ($value->asset_manager_status == 1) ? 'checked' : '' ) .'>';
							                }
							                $file_basename = '';
							                if(isset($value->file_basename))
							                    $file_basename = $value->file_basename;

							                $download_path = "https://drive.google.com/drive/u/2/folders/".$file_basename;
							                $downlod_file_path = '';
							                if($value->file_type == "file"){
							                    $download_path = "https://drive.google.com/file/d/".$file_basename;
							                }	
										@endphp

										<tr>
											<td>{{ ($key+1) }}</td>
											<td>
												<a  target="_blank"  title="{{ $value->file_name }}"  href="{{ $download_path }}">{{ $value->file_name }}</a> <a target="_blank" href="{{ $downlod_file_path }}" title="Download"><i class="fa fa-download fa-fw"></i></a>
											</td>
											<td>{{ show_number($value->amount,2) }}</td>
											<td>
												@php
													$comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
						                            ->join('users as u', 'u.id', 'pc.user_id')
						                            ->where('pc.record_id', $value->id)
						                            ->where('pc.type', 'property_insurance_tab_details')
						                            ->orderBy('pc.created_at', 'desc')
						                            ->limit(2)->get();
												@endphp

												@foreach ($comments as $comment)
													@php
														$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
								                        $commented_user = $comment->name.''.$company;
													@endphp
													<p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
												@endforeach

												<button type="button" class="btn btn-primary btn-xs btn-ins-comment" data-id="{{ $value->id }}" data-url="{{ route('get_property_insurance_detail_comment', ['id' => $value->id]) }}" data-property-id="{{ $value->property_id }}">Kommentar</button>
											</td>
											<td>{{ $element->am_name }}</td>
											<td>{{ $value->user_name }}</td>
											<td>{{ show_datetime_format($value->created_at) }}</td>
											<td>{!! $not_release_am !!}</td>
											<td>{!! $am_checkbox !!}</td>
											{{-- <td>{!! $falk_checkbox !!}</td> --}}
											<td>{!! $pending !!}</td>
											{{-- <td>{!! $not_release !!}</td> --}}
											<td>{!! $delete_btn !!}</td>
											<td>{!! $mail_button !!}</td>
										</tr>

									@endforeach
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		@endif

	@endforeach
@else
	<p>Data not available!</p>
@endif