<table class="table table-striped dashboard-table" id="insurance-table{{$pr}}">
    <thead>
        <tr>
            <th>Name</th>
            <th class="text-right">Anzahl</th>
            <th class="text-right">Betrag</th>
            
        </tr>
    </thead>
    <tbody>
    @foreach($data as $property)
        <tr>
            <td><a href="javascript:void(0)" data-id="{{$pr}}" class="get-name-insurance">{{$property->axa}}</a></td>
            <td class="text-right">{{$property->id}}</td>
            <td class="text-right">{{number_format($property->gebaude_betrag,2,",",".")}}€</td>
        </tr>
        @endforeach
    </tbody>
</table>