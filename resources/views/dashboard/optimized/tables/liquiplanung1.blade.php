@php
	$total_month_column = 12;
@endphp

<?php
	$custom_fields_schl = DB::table('properties_custom_fields')->where('property_id',3363)->where('slug','current_balance')->first();
	$d1 = "";
	if($custom_fields_schl)
		$d1 = ' ('.show_date_format($custom_fields_schl->updated_at).')';

	//$d2 = DB::table('properties_custom_fields')->where('property_id','!=',3363)->where('slug','current_balance')->get()->max('updated_at');
	//if($d2)
		//$d2 = ' ('.show_date_format($d2).')';
?>

<style type="text/css">
	#liquiplanung-1-table td:nth-child(2),td:nth-child(3), td:nth-child(4), td:nth-child(5), td:nth-child(6), td:nth-child(7), td:nth-child(8) {
	    text-align: right;
	}
	#liquiplanung-1-table>tbody>tr>td, th{
		padding: 5px;
		white-space: nowrap;
	}
</style>
<div class="row">
	<div class="col-md-12 table-responsive">
		<table id="liquiplanung-1-table" class="table table-striped table-bordered" style="width: 100%;">
			<thead>
				<?php
					$ddate = $week['first_last_week']['Mon'];
					$date = new DateTime($ddate);
					$week1 = $date->format("W");

					$ddate = $week['second_last_week']['Mon'];
					$date = new DateTime($ddate);
					$week2 = $date->format("W");

					$ddate = $week['third_last_week']['Mon'];
					$date = new DateTime($ddate);
					$week3 = $date->format("W");

					$ddate = $week['fourth_last_week']['Mon'];
					$date = new DateTime($ddate);
					$week4 = $date->format("W");
				?>
		     	<tr>
			        <th></th>
			        <th class="text-right">KW: {{ $week4 }}</th>
			        <th class="text-right">KW: {{ $week3 }}</th>
			        <th class="text-right">KW: {{ $week2 }}</th>
			        <th class="text-right">KW: {{ $week1 }}</th>

			        @for ($i = 0; $i < $total_month_column; $i++)
		  				<th class="text-right">{{__('dashboard.'.date('F',strtotime('first day of '.$i.' month')))}} {{ ($i == 0) ? $d1 : '' }}</th>
		  			@endfor
		     	</tr>
		  	</thead>
		  	<tbody>
		  		{{-- f1 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="1" col="0">Guthaben AG</td>

		  			<td class="liquiplanung-data" row="1" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f1'][0])) ? show_number($week['fourth_last_week_data']['f1'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="1" col="-3">
		  				{{ (isset($week['third_last_week_data']['f1'][0])) ? show_number($week['third_last_week_data']['f1'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="1" col="-2">
		  				{{ (isset($week['second_last_week_data']['f1'][0])) ? show_number($week['second_last_week_data']['f1'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="1" col="-1">
		  				{{ (isset($week['first_last_week_data']['f1'][0])) ? show_number($week['first_last_week_data']['f1'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				@if($i == 0)
		  					<td class="liquiplanung-data" row="1" col="{{ $i+1 }}"><a href="javascript:void(0);">{{ (isset($res['f1'][$i])) ? show_number($res['f1'][$i], 2) : 0 }}</a></td>
		  				@else
		  					<td class="liquiplanung-data" row="1" col="{{ $i+1 }}"></td>
		  				@endif
		  			@endfor
		  		</tr>
		  		{{-- f2 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="2" col="0">Guthaben KG</td>

		  			<td class="liquiplanung-data" row="2" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f2'][0])) ? show_number($week['fourth_last_week_data']['f2'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="2" col="-3">
		  				{{ (isset($week['third_last_week_data']['f2'][0])) ? show_number($week['third_last_week_data']['f2'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="2" col="-2">
		  				{{ (isset($week['second_last_week_data']['f2'][0])) ? show_number($week['second_last_week_data']['f2'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="2" col="-1">
		  				{{ (isset($week['first_last_week_data']['f2'][0])) ? show_number($week['first_last_week_data']['f2'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				@if($i == 0)
		  					<td class="liquiplanung-data" row="2" col="{{ $i+1 }}"><a href="javascript:void(0);">{{ (isset($res['f2'][$i])) ? show_number($res['f2'][$i], 2) : 0 }}</a></td>
		  				@else
		  					<td class="liquiplanung-data" row="2" col="{{ $i+1 }}"></td>
		  				@endif
		  			@endfor

		  		</tr>
				{{-- f3 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="3" col="0"> ./. Guthaben Rastatt / Zeulenroda</td>

		  			<td class="liquiplanung-data" row="3" col="-4">
		  				<?php 
		  					$sum4 = (isset($week['fourth_last_week_data']['f3'][0])) ? $week['fourth_last_week_data']['f3'][0] : 0;
		  					$sum4 += (isset($week['fourth_last_week_data']['f4'][0])) ? $week['fourth_last_week_data']['f4'][0] : 0;
		  				?>
		  				{{ show_number($sum4, 2) }}
		  			</td>
		  			<td class="liquiplanung-data" row="3" col="-3">
		  				<?php 
		  					$sum3 = (isset($week['third_last_week_data']['f3'][0])) ? $week['third_last_week_data']['f3'][0] : 0;
		  					$sum3 += (isset($week['third_last_week_data']['f4'][0])) ? $week['third_last_week_data']['f4'][0] : 0;
		  				?>
		  				{{ show_number($sum3, 2) }}
		  			</td>
		  			<td class="liquiplanung-data" row="3" col="-2">
		  				<?php 
		  					$sum2 = (isset($week['second_last_week_data']['f3'][0])) ? $week['second_last_week_data']['f3'][0] : 0;
		  					$sum2 += (isset($week['second_last_week_data']['f4'][0])) ? $week['second_last_week_data']['f4'][0] : 0;
		  				?>
		  				{{ show_number($sum2, 2) }}
		  			</td>
		  			<td class="liquiplanung-data" row="3" col="-1">
		  				<?php 
		  					$sum1 = (isset($week['first_last_week_data']['f3'][0])) ? $week['first_last_week_data']['f3'][0] : 0;
		  					$sum1 += (isset($week['first_last_week_data']['f4'][0])) ? $week['first_last_week_data']['f4'][0] : 0;
		  				?>
		  				{{ show_number($sum1, 2) }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				@if($i == 0)
		  					<?php
								$sum =  (isset($res['f3'][$i])) ? $res['f3'][$i] : 0;
								$sum +=  (isset($res['f4'][$i])) ? $res['f4'][$i] : 0;
							?>
		  					<td class="liquiplanung-data" row="3" col="{{ $i+1 }}"><a href="javascript:void(0);">{{ show_number($sum, 2) }}</a></td>
		  				@else
		  					<td class="liquiplanung-data" row="3" col="{{ $i+1 }}"></td>
		  				@endif
		  			@endfor
		  		</tr>
				{{-- f4 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="4" col="0">./. Guthaben Aktiendepot</td>
		  			<td>
		  				<?php
		  				$share_deposite = 0;
		  				if(isset($week['fourth_last_week_data']['share_deposite']))
		  				$share_deposite = $week['fourth_last_week_data']['share_deposite'];
		  				?>
		  				{{ show_number($share_deposite, 2) }}
		  			</td>
		  			<td>
		  				<?php
		  				$share_deposite = 0;
		  				if(isset($week['third_last_week_data']['share_deposite']))
		  				$share_deposite = $week['third_last_week_data']['share_deposite'];
		  				?>
		  				{{ show_number($share_deposite, 2) }}
		  			</td>
		  			<td>
		  				<?php
		  				$share_deposite = 0;
		  				if(isset($week['second_last_week_data']['share_deposite']))
		  				$share_deposite = $week['second_last_week_data']['share_deposite'];
		  				?>
		  				{{ show_number($share_deposite, 2) }}
		  			</td>
		  			<td>
		  				<?php
		  				$share_deposite = 0;
		  				if(isset($week['first_last_week_data']['share_deposite']))
		  				$share_deposite = $week['first_last_week_data']['share_deposite'];
		  				?>
		  				{{ show_number($share_deposite, 2) }}
		  			</td>
		  			@for ($i = 0; $i < $total_month_column; $i++)
		  			<td class="liquiplanung-data" row="18" col="1"><a href="javascript:void(0);">{{show_number($res['share_deposite'],2)}}</a></td>
		  			@endfor
		  		</tr>
				{{-- f5 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="5" col="0">Saldo</td>

		  			<td class="liquiplanung-data" row="5" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f5'][0])) ? show_number($week['fourth_last_week_data']['f5'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="5" col="-3">
		  				{{ (isset($week['third_last_week_data']['f5'][0])) ? show_number($week['third_last_week_data']['f5'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="5" col="-2">
		  				{{ (isset($week['second_last_week_data']['f5'][0])) ? show_number($week['second_last_week_data']['f5'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="5" col="-1">
		  				{{ (isset($week['first_last_week_data']['f5'][0])) ? show_number($week['first_last_week_data']['f5'][0], 2) : 0 }}
		  			</td>

		  			
		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="5" col="{{ ($i+1) }}">{{ (isset($res['f5'][$i])) ? show_number($res['f5'][$i], 2) : 0 }}</td>
		  			@endfor
		  		</tr>
				{{-- f6 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="6" col="0">Einnahmen</td>

		  			<td class="liquiplanung-data" row="6" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f6'][0])) ? show_number($week['fourth_last_week_data']['f6'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="6" col="-3">
		  				{{ (isset($week['third_last_week_data']['f6'][0])) ? show_number($week['third_last_week_data']['f6'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="6" col="-2">
		  				{{ (isset($week['second_last_week_data']['f6'][0])) ? show_number($week['second_last_week_data']['f6'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="6" col="-1">
		  				{{ (isset($week['first_last_week_data']['f6'][0])) ? show_number($week['first_last_week_data']['f6'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="6" col="{{ $i+1 }}">
		  					<a href="javascript:void(0);">{{ (isset($res['f6'][$i])) ? show_number($res['f6'][$i], 2) : 0 }}</a>
		  				</td>
		  			@endfor
		  		</tr>
		  		{{-- f17 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="17" col="0">Einnahmen ao</td>

		  			<td class="liquiplanung-data" row="17" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f17'][0])) ? show_number($week['fourth_last_week_data']['f17'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="17" col="-3">
		  				{{ (isset($week['third_last_week_data']['f17'][0])) ? show_number($week['third_last_week_data']['f17'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="17" col="-2">
		  				{{ (isset($week['second_last_week_data']['f17'][0])) ? show_number($week['second_last_week_data']['f17'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="17" col="-1">
		  				{{ (isset($week['first_last_week_data']['f17'][0])) ? show_number($week['first_last_week_data']['f17'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="17" col="{{ $i+1 }}">
		  					<a href="javascript:void(0);">{{ (isset($res['f17'][$i])) ? show_number($res['f17'][$i], 2) : 0 }}</a>
		  				</td>
		  			@endfor
		  		</tr>
				{{-- f7 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="7" col="0">Ausgaben</td>

		  			<td class="liquiplanung-data" row="7" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f7'][0])) ? show_number($week['fourth_last_week_data']['f7'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="7" col="-3">
		  				{{ (isset($week['third_last_week_data']['f7'][0])) ? show_number($week['third_last_week_data']['f7'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="7" col="-2">
		  				{{ (isset($week['second_last_week_data']['f7'][0])) ? show_number($week['second_last_week_data']['f7'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="7" col="-1">
		  				{{ (isset($week['first_last_week_data']['f7'][0])) ? show_number($week['first_last_week_data']['f7'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="7" col="{{ $i+1 }}">
		  					<a href="javascript:void(0);">{{ (isset($res['f7'][$i])) ? show_number($res['f7'][$i], 2) : 0 }}</a>
		  				</td>
		  			@endfor
		  		</tr>
				{{-- f8 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="8" col="0">Ausgaben ao</td>

		  			<td class="liquiplanung-data" row="8" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f8'][0])) ? show_number($week['fourth_last_week_data']['f8'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="8" col="-3">
		  				{{ (isset($week['third_last_week_data']['f8'][0])) ? show_number($week['third_last_week_data']['f8'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="8" col="-2">
		  				{{ (isset($week['second_last_week_data']['f8'][0])) ? show_number($week['second_last_week_data']['f8'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="8" col="-1">
		  				{{ (isset($week['first_last_week_data']['f8'][0])) ? show_number($week['first_last_week_data']['f8'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="8" col="{{ $i+1 }}">
		  					<a href="javascript:void(0);">{{ (isset($res['f8'][$i])) ? show_number($res['f8'][$i], 2) : 0 }}</a>
		  				</td>
		  			@endfor
		  		</tr>
				{{-- f9 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="9" col="0">Kapitaldienst</td>

		  			<td class="liquiplanung-data" row="9" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f9'][0])) ? show_number($week['fourth_last_week_data']['f9'][0]*(-1), 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="9" col="-3">
		  				{{ (isset($week['third_last_week_data']['f9'][0])) ? show_number($week['third_last_week_data']['f9'][0]*(-1), 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="9" col="-2">
		  				{{ (isset($week['second_last_week_data']['f9'][0])) ? show_number($week['second_last_week_data']['f9'][0]*(-1), 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="9" col="-1">
		  				{{ (isset($week['first_last_week_data']['f9'][0])) ? show_number($week['first_last_week_data']['f9'][0]*(-1), 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="9" col="{{ $i+1 }}">
		  					<a href="javascript:void(0);">{{ (isset($res['f9'][$i])) ? show_number($res['f9'][$i]*(-1), 2) : 0 }}</a>
		  				</td>
		  			@endfor
		  		</tr>
				{{-- f10 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="10" col="0">Zwischensumme (Verwaltung)</td>

		  			<td class="liquiplanung-data" row="10" col="-1">
		  				{{ (isset($week['fourth_last_week_data']['f10'][0])) ? show_number($week['fourth_last_week_data']['f10'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="10" col="-2">
		  				{{ (isset($week['third_last_week_data']['f10'][0])) ? show_number($week['third_last_week_data']['f10'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="10" col="-3">
		  				{{ (isset($week['second_last_week_data']['f10'][0])) ? show_number($week['second_last_week_data']['f10'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="10" col="-4">
		  				{{ (isset($week['first_last_week_data']['f10'][0])) ? show_number($week['first_last_week_data']['f10'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="10" col="{{ $i+1 }}">{{ (isset($res['f10'][$i])) ? show_number($res['f10'][$i], 2) : 0 }}</td>
		  			@endfor
		  		</tr>
				{{-- f11 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="11" col="0">Einkäufe Notar</td>

		  			<td class="liquiplanung-data" row="11" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f11'][0])) ? show_number($week['fourth_last_week_data']['f11'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="11" col="-3">
		  				{{ (isset($week['third_last_week_data']['f11'][0])) ? show_number($week['third_last_week_data']['f11'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="11" col="-2">
		  				{{ (isset($week['second_last_week_data']['f11'][0])) ? show_number($week['second_last_week_data']['f11'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="11" col="-1">
		  				{{ (isset($week['first_last_week_data']['f11'][0])) ? show_number($week['first_last_week_data']['f11'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="11" col="{{ $i+1 }}">
		  					<a href="javascript:void(0);">{{ (isset($res['f11'][$i])) ? show_number($res['f11'][$i]*(-1), 2) : 0 }}</a>
		  				</td>
		  			@endfor
		  		</tr>
				{{-- f12 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="12" col="0">Einkäufe Liquiplan</td>

		  			<td class="liquiplanung-data" row="12" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f11'][0])) ? show_number( (isset($week['fourth_last_week_data']['f12'][0])) ? $week['fourth_last_week_data']['f12'][0] : 0 , 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="12" col="-3">
		  				{{ (isset($week['third_last_week_data']['f11'][0])) ? show_number( (isset($week['third_last_week_data']['f12'][0])) ? $week['third_last_week_data']['f12'][0] : 0 , 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="12" col="-2">
		  				{{ (isset($week['second_last_week_data']['f11'][0])) ? show_number( (isset($week['second_last_week_data']['f12'][0])) ? $week['second_last_week_data']['f12'][0] : 0, 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="12" col="-1">
		  				{{ (isset($week['first_last_week_data']['f11'][0])) ? show_number( (isset($week['first_last_week_data']['f12'][0])) ? $week['first_last_week_data']['f12'][0] : 0 , 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
	  					<td class="liquiplanung-data" row="12" col="{{ $i+1 }}">
	  						<a href="javascript:void(0);">{{ (isset($res['f12'][$i])) ? show_number($res['f12'][$i]*(-1), 2) : 0 }}</a>
	  					</td>
		  			@endfor
		  		</tr>
				{{-- f13 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="13" col="0">Verkäufe</td>

		  			<td class="liquiplanung-data" row="13" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f13'][0])) ? show_number($week['fourth_last_week_data']['f13'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="13" col="-3">
		  				{{ (isset($week['third_last_week_data']['f13'][0])) ? show_number($week['third_last_week_data']['f13'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="13" col="-2">
		  				{{ (isset($week['second_last_week_data']['f13'][0])) ? show_number($week['second_last_week_data']['f13'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="13" col="-1">
		  				{{ (isset($week['first_last_week_data']['f13'][0])) ? show_number($week['first_last_week_data']['f13'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="13" col="{{ $i+1 }}">
		  					<a href="javascript:void(0);">{{ (isset($res['f13'][$i])) ? show_number($res['f13'][$i], 2) : 0 }}</a>
		  				</td>
		  			@endfor
		  		</tr>
				{{-- f14 --}}
		  		<tr style="font-weight: bold;">
		  			<td class="liquiplanung-data" row="14" col="0">Summe</td>

		  			<td class="liquiplanung-data" row="14" col="-4" style="color:{{ (isset($week['fourth_last_week_data']['f14'][0]) && $week['fourth_last_week_data']['f14'][0] >= 0) ? 'green' : 'red' }};">
		  				{{ (isset($week['fourth_last_week_data']['f14'][0])) ? show_number($week['fourth_last_week_data']['f14'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="14" col="-3" style="color:{{ (isset($week['third_last_week_data']['f14'][0]) && $week['third_last_week_data']['f14'][0] >= 0) ? 'green' : 'red' }};">
		  				{{ (isset($week['third_last_week_data']['f14'][0])) ? show_number($week['third_last_week_data']['f14'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="14" col="-2" style="color:{{ (isset($week['second_last_week_data']['f14'][0]) && $week['second_last_week_data']['f14'][0] >= 0) ? 'green' : 'red' }};">
		  				{{ (isset($week['second_last_week_data']['f14'][0])) ? show_number($week['second_last_week_data']['f14'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="14" col="-1" style="color:{{ (isset($week['first_last_week_data']['f14'][0]) && $week['first_last_week_data']['f14'][0] >= 0) ? 'green' : 'red' }};">
		  				{{ (isset($week['first_last_week_data']['f14'][0])) ? show_number($week['first_last_week_data']['f14'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="14" col="{{ $i+1 }}" style="color: {{ $res['f14'][$i] >= 0 ? 'green' : 'red' }};">
		  					{{ (isset($res['f14'][$i])) ? show_number($res['f14'][$i], 2) : 0 }}
		  				</td>
		  			@endfor
		  		</tr>
				{{-- f15 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="15" col="0">zur Info: </td>

		  			<td class="liquiplanung-data" row="15" col="-4"></td>
		  			<td class="liquiplanung-data" row="15" col="-3"></td>
		  			<td class="liquiplanung-data" row="15" col="-2"></td>
		  			<td class="liquiplanung-data" row="15" col="-1"></td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				<td class="liquiplanung-data" row="15" col="{{ $i+1 }}"></td>
		  			@endfor
		  		</tr>
				{{-- f16 --}}
		  		<tr>
		  			<td class="liquiplanung-data" row="16" col="0">Summe Exklusivität</td>

		  			<td class="liquiplanung-data" row="16" col="-4">
		  				{{ (isset($week['fourth_last_week_data']['f16'][0])) ? show_number($week['fourth_last_week_data']['f16'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="16" col="-3">
		  				{{ (isset($week['third_last_week_data']['f16'][0])) ? show_number($week['third_last_week_data']['f16'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="16" col="-2">
		  				{{ (isset($week['second_last_week_data']['f16'][0])) ? show_number($week['second_last_week_data']['f16'][0], 2) : 0 }}
		  			</td>
		  			<td class="liquiplanung-data" row="16" col="-1">
		  				{{ (isset($week['first_last_week_data']['f16'][0])) ? show_number($week['first_last_week_data']['f16'][0], 2) : 0 }}
		  			</td>

		  			@for ($i = 0; $i < $total_month_column; $i++)
		  				@if($i == 0)
		  					<td class="liquiplanung-data" row="16" col="{{ $i+1 }}">
		  						<a href="javascript:void(0);">{{ (isset($res['f16'][$i])) ? show_number($res['f16'][$i], 2) : 0 }}</a>
		  					</td>
		  				@else
		  					<td class="liquiplanung-data" row="16" col="{{ $i+1 }}"></td>
		  				@endif
		  			@endfor
		  		</tr>

		  	</tbody>
		</table>
	</div>
</div>