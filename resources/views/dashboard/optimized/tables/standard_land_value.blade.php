<table id="standard-land-value-table" class="table table-striped">
	<thead>
     	<tr>
	        <th>#</th>
	        <th>Objekt</th>
	        <th>AM</th>
	        <th class="text-right">Gebäude</th>
	        <th class="text-right">Grundstück</th>
     	</tr>
  	</thead>
	<tbody>
		@if($data)
			@foreach ($data as $key => $value)
				<tr>
					<td>{{ ($key+1) }}</td>
					<td>
						<a href="{{route('properties.show',['property'=> $value->id])}}?tab=properties">
                        	{{$value->name_of_property}}
                    	</a>
					</td>
					<?php
					$subject = 'GRUND UND BODENRICHTWERT: '.$value->name_of_property;
					$content = "";
					?>
					
					<td>
						<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $value->id }}" data-subject="{{ $subject }}" data-content="{{ $content }}" data-title="GRUND UND BODENRICHTWERT">{{ $value->asset_manager }}</a>
					</td>
					<td class="text-right">{{number_format($value->building*100,2,",",".")}} %</td>
					<td class="text-right">{{number_format($value->plot_of_land*100,2,",",".")}} %</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>