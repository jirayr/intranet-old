<?php
	function short_name($string){
	    if($string){
	        $arr = explode(" ", $string);
	        if(count($arr)>1)
	            return substr($arr[0], 0,1).$arr[1];
	        else
	            return $string;
	    }
	    return $string;
	}

   $year = range(2018,date('Y'));
   $months = range(1,12);
?>
<div class="row">
	<div class="col-md-12">
		<select class="pm-month change-select">
            <option value="">All</option>
            @foreach($months as $list)
            <option value="{{$list}}" {{ ($r_month == $list) ? 'selected' : '' }}>{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
            @endforeach
         </select>
         <!-- {{__('dashboard.'.date('F'))}} -->
         <select class="pm-year change-select">
            <option value="">All</option>
            @foreach($year as $list)
            <option value="{{$list}}" {{ ($r_year == $list) ? 'selected' : '' }}>{{$list}}</option>
            @endforeach
         </select>
	</div>
</div>
<table id="added-month" class="table table-striped">
    <thead>
    <tr>
        <th></th>
        <th style="font-size: 16px;"  colspan="2">Quicksheet</th>
        <th style="font-size: 16px;"  colspan="2">Im Angebot <75%</th>
        <th style="font-size: 16px;"  colspan="2">Im Angebot >75%</th>
        <th style="font-size: 16px;"  colspan="2">In Exklusivität (95%)</th>
        <th style="font-size: 16px;"  colspan="2">Liquiplanung (98%)</th>
        <th style="font-size: 16px;"  colspan="2">In Notartermin (99%) </th>
        <th style="font-size: 16px;"  colspan="2">In Beurkundet</th>
        <th style="font-size: 16px;"  colspan="2">Im Bestand</th>
        @if($flag)
        <th style="font-size: 16px;"  colspan="2">Verkauft</th>
        <th style="font-size: 16px;"  colspan="2">Abgelehnt</th>
        <th style="font-size: 16px;"  colspan="2">Verloren</th>
        @endif

    </tr>
    <tr>
        <th>TM</th>
        <!-- <th>Anzahl</th>
        <th>{{__('dashboard.total_purchase_price')}}</th> -->
        <th>Anzahl</th>
        <th>GKP</th>

        <th>Anzahl</th>
        <th>GKP</th>

        <th>Anzahl</th>
        <th>GKP</th>

        <th>Anzahl</th>
        <th>GKP</th>

        <th>Anzahl</th>
        <th>GKP</th>

        <th>Anzahl</th>
        <th>GKP</th>
        <th>Anzahl</th>
        <th>GKP</th>
        <th>Anzahl</th>
        <th>GKP</th>

        @if($flag)
        
        <th>Anzahl</th>
        <th>GKP</th>

        <th>Anzahl</th>
        <th>GKP</th>

        <th>Anzahl</th>
        <th>GKP</th>
        @endif

    </tr>
    </thead>
    <tbody>
    <?php
    $t1 = $t2 = 0;

    $a7 = $b7 = $a5 = $b5 = $a10 = $b10 = $a6 = $b6 = 0;
    $a12 = $b12 = 0;
    $a14 = $b14 = 0;

    $a15=$a16=$a8=$a4=$a9 = 0;
    $b15=$b16=$b8=$b4=$b9 = 0;

    // print "<pre>";
    // print_r($status_array);

    ?>
	@foreach($analysis_array as $id=>$tenancy_item)

	<?php


	$t1 += $tenancy_item['count'];
	$t2 += $tenancy_item['amount'];

	?>
	    <tr>
	        <td><a class="get-manager-property" data-id="{{$id}}" data-status="0" data-toggle="modal" data-target="#property-list">{{short_name($tenancy_item['name'])}}</td></a>
	        <!-- <td><a class="get-manager-property" data-id="{{$id}}" data-status="0" data-toggle="modal" data-target="#property-list">{{$tenancy_item['count']}}</a></td>
	        <td>{{number_format($tenancy_item['amount'],0,",",".")}}€</td> -->
	        <td class="text-right">
	        	@if(isset($status_array[15][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="15" data-toggle="modal" data-target="#property-list">{{$status_array[15][$id]['count']}}
	        	</a>
	        	<?php
					$a15 += $status_array[15][$id]['count'];
					$b15 += $status_array[15][$id]['price'];
				?>
	        	@else
	        	0
	        	@endif
	        </td>
	        <td class="text-right">
	        @if(isset($status_array[15][$id]['price']))
	        {{number_format($status_array[15][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>
	        <td class="text-right">
	        	@if(isset($status_array[7][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="7" data-toggle="modal" data-target="#property-list">{{$status_array[7][$id]['count']}}
	        	</a>

	        	<?php
					$a7 += $status_array[7][$id]['count'];
					$b7 += $status_array[7][$id]['price'];
				?>


	        	@else
	        	0
	        	@endif
	        	</td>
	        <td class="text-right">
	        @if(isset($status_array[7][$id]['price']))
	        {{number_format($status_array[7][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>


	        <td class="text-right">
	        	@if(isset($status_array[5][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="5" data-toggle="modal" data-target="#property-list">{{$status_array[5][$id]['count']}}
	        	</a>

	        	<?php
					$a5 += $status_array[5][$id]['count'];
					$b5 += $status_array[5][$id]['price'];
				?>


	        	@else
	        	0
	        	@endif
	        	</td>
	        <td class="text-right">
	        @if(isset($status_array[5][$id]['price']))
	        {{number_format($status_array[5][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>


	    	<td class="text-right">
	        	@if(isset($status_array[14][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="14" data-toggle="modal" data-target="#property-list">{{$status_array[14][$id]['count']}}
	        	</a>

	        	<?php
					$a14 += $status_array[14][$id]['count'];
					$b14 += $status_array[14][$id]['price'];
				?>


	        	@else
	        	0
	        	@endif
	        	</td>
	        <td class="text-right">
	        @if(isset($status_array[14][$id]['price']))
	        {{number_format($status_array[14][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>

	    	<td class="text-right">
	        	@if(isset($status_array[16][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="16" data-toggle="modal" data-target="#property-list">{{$status_array[16][$id]['count']}}
	        	</a>
	        	<?php
					$a16 += $status_array[16][$id]['count'];
					$b16 += $status_array[16][$id]['price'];
				?>
	        	@else
	        	0
	        	@endif
	        </td>
	        <td class="text-right">
	        @if(isset($status_array[16][$id]['price']))
	        {{number_format($status_array[16][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>


	        <td class="text-right">
	        	@if(isset($status_array[10][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="10" data-toggle="modal" data-target="#property-list">{{$status_array[10][$id]['count']}}
	        	</a>
	        	<?php
					$a10 += $status_array[10][$id]['count'];
					$b10 += $status_array[10][$id]['price'];
				?>
	        	@else
	        	0
	        	@endif
	        </td>
	        <td class="text-right">
	        @if(isset($status_array[10][$id]['price']))
	        {{number_format($status_array[10][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>


	    	<td class="text-right">
	        	@if(isset($status_array[12][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="12" data-toggle="modal" data-target="#property-list">{{$status_array[12][$id]['count']}}
	        	</a>
	        	<?php
					$a12 += $status_array[12][$id]['count'];
					$b12 += $status_array[12][$id]['price'];
				?>
	        	@else
	        	0
	        	@endif
	        </td>
	        <td class="text-right">
	        @if(isset($status_array[12][$id]['price']))
	        {{number_format($status_array[12][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>


	        <td class="text-right">
	        	@if(isset($status_array[6][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="6" data-toggle="modal" data-target="#property-list">{{$status_array[6][$id]['count']}}
	        	</a>
	        	<?php
					$a6 += $status_array[6][$id]['count'];
					$b6 += $status_array[6][$id]['price'];
				?>
	        	@else
	        	0
	        	@endif
	        </td>
	        <td class="text-right">
	        @if(isset($status_array[6][$id]['price']))
	        {{number_format($status_array[6][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>


	    	@if($flag)
	    	

	    	


	    	<td class="text-right">
	        	@if(isset($status_array[8][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="8" data-toggle="modal" data-target="#property-list">{{$status_array[8][$id]['count']}}
	        	</a>
	        	<?php
					$a8 += $status_array[8][$id]['count'];
					$b8 += $status_array[8][$id]['price'];
				?>
	        	@else
	        	0
	        	@endif
	        </td>
	        <td class="text-right">
	        @if(isset($status_array[8][$id]['price']))
	        {{number_format($status_array[8][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>

	    	<td class="text-right">
	        	@if(isset($status_array[4][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="4" data-toggle="modal" data-target="#property-list">{{$status_array[4][$id]['count']}}
	        	</a>
	        	<?php
					$a4 += $status_array[4][$id]['count'];
					$b4 += $status_array[4][$id]['price'];
				?>
	        	@else
	        	0
	        	@endif
	        </td>
	        <td class="text-right">
	        @if(isset($status_array[4][$id]['price']))
	        {{number_format($status_array[4][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>

	    	<td class="text-right">
	        	@if(isset($status_array[9][$id]['count']))
	        	<a class="get-manager-property" data-id="{{$id}}" data-status="9" data-toggle="modal" data-target="#property-list">{{$status_array[9][$id]['count']}}
	        	</a>
	        	<?php
					$a9 += $status_array[9][$id]['count'];
					$b9 += $status_array[9][$id]['price'];
				?>
	        	@else
	        	0
	        	@endif
	        </td>
	        <td class="text-right">
	        @if(isset($status_array[9][$id]['price']))
	        {{number_format($status_array[9][$id]['price'],0,",",".")}}
	        @else
	        	{{number_format(0,0,",",".")}}
	        @endif

	    		€</td>

	    	@endif




	    </tr>
	@endforeach
	</tbody>
	<tfoot>
	<tr>
        <th class="border-top-footer"></th>
        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="15" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a15,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b15,0,",",".")}}€</th>

        <!-- <th class="border-top-footer"><a class="get-manager-property" data-status="0" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($t1,0,",",".")}}</a></th>
        <th class="border-top-footer">{{number_format($t2,0,",",".")}}€</th> -->
        <th class="border-top-footer text-right "><a class="get-manager-property" data-status="7" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a7,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b7,0,",",".")}}€</th>

        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="5" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a5,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b5,0,",",".")}}€</th>

        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="14" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a14,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b14,0,",",".")}}€</th>

        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="16" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a16,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b16,0,",",".")}}€</th>

        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="10" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a10,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b10,0,",",".")}}€</th>

        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="12" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a12,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b12,0,",",".")}}€</th>

        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="6" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a6,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b6,0,",",".")}}€</th>

        @if($flag)
        
        

        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="8" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a8,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b8,0,",",".")}}€</th>

        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="4" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a4,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b4,0,",",".")}}€</th>

        <th class="border-top-footer text-right"><a class="get-manager-property" data-status="9" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($a9,0,",",".")}}</a></th>
        <th class="border-top-footer text-right">{{number_format($b9,0,",",".")}}€</th>
        @endif



    </tr>
    <tr>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	
    	@if($flag)
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	<td style="border-top: 0;"></td>
    	@endif
    </tr>

    <tr>
    	<th class="border-top-footer"><a class="get-manager-property" data-status="0" data-id="0" data-toggle="modal" data-target="#property-list">Gesamt</a></th>
    	<th class="border-top-footer text-right"><a class="get-manager-property" data-status="0" data-id="0" data-toggle="modal" data-target="#property-list">{{number_format($t1,0,",",".")}}</a></th>

        <th class="border-top-footer total_mv_vl">{{number_format($t2,0,",",".")}}€</th>

        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        
        @if($flag)
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        <td style="border-top: 0;"></td>
        @endif

    </tr>

	</tfoot>
</table>
<input type="hidden" class="flg" value="{{$flag}}">
