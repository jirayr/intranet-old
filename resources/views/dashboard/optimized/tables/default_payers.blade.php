<div class="row" id="default_payer">
	<div class="col-md-12">
		<?php
			$year = range(2018,date('Y'));
			$months = range(1,12);
       ?>
       <select class="bb-month bchange-select">
          <option value="">All</option>
          @foreach($months as $list)
             <option @if($list == $r_month) selected @endif value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
          @endforeach
       </select>
           <!-- {{__('dashboard.'.date('F'))}} -->
       <?php
       $year = range(2020,date('Y')+5);
       $user = Auth::user();

       
   		// $modal = new PropertiesMailLog;
     //    $modal->property_id = $contract->property_id;
     //    $modal->record_id = $request->id;
     //    $modal->type = 'contract_notrelease_am';
     //    $modal->tab = 'contracts';
     //    if($request->comment)
     //          $modal->comment = $request->comment;
     //    $modal->user_id = Auth::user()->id;
     //    $modal->save();






       ?>
       <select class="bb-year bchange-select">
          @foreach($year as $list)
             <option @if($list == $r_year) selected @endif value="{{$list}}">{{$list}}</option>
          @endforeach
       </select>
       {{-- <button type="button" data-id="1"  class="btn btn-primary hide-checkbox">Keine OPOS ausblenden</button> --}}

       <?php
       $btn_class0 = "btn-primary";
       $btn_class1 = "btn-primary";
       $btn_class2 = "btn-primary";
       $btn_class3 = "btn-primary";
       $btn_class4 = "btn-primary";

       if($status==0)
       $btn_class0 = "btn-success";
       if($status==1)
       $btn_class1 = "btn-success";
       if($status==2)
       $btn_class2 = "btn-success";
       if($status==3)
       $btn_class3 = "btn-success";
       if($status==4)
       $btn_class4 = "btn-success";

   		$status_count[0] = $status_count[1] = $status_count[2] = $status_count[3] = $status_count[4] = 0;

   		foreach ($data as $element):

   		$status_count[0] +=1;

   		if(!isset($release[$element->id][1])){
			
   		}
   		else{
   			$status_count[3] +=1;
   		}

		if(isset($release[$element->id][1]))
		{}
		else
			$status_count[4] +=1;



		if($element->no_opos_status){
			
		}
		else
			$status_count[1] +=1;

		if(!$element->no_opos_status){
			
		}
		else
			$status_count[2] +=1;

		endforeach;
       ?>


       	<button type="button" data-id="0"  class="btn {{$btn_class0}} hide-checkbox">Alle ({{$status_count[0]}})</button>
       	<button type="button" data-id="1"  class="btn {{$btn_class1}} hide-checkbox">OPOS ({{$status_count[1]}})</button>
        <button type="button" data-id="2"  class="btn {{$btn_class2}} hide-checkbox">Keine OPOS ({{$status_count[2]}})</button>
        <button type="button" data-id="3"  class="btn {{$btn_class3}} hide-checkbox">Von NE freigegeben ({{$status_count[3]}})</button>
        <button type="button" data-id="4"  class="btn {{$btn_class4}} hide-checkbox">Nicht freigegeben ({{$status_count[4]}})</button>
        
	</div>
</div>
<br>
<table id="property-default-payer-table" class="table table-striped">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>AM</th>
			<th>User</th>
			<th>Hausverwaltung</th>
			<th>Datum</th>
			<!-- <th>Monat</th> -->
			<!-- <th>Jahr</th> -->
			<th>Keine OPOS</th>
			<th>Excel</th>
			<th>Freigabe</th>
			<th>Freigabe</th>
			<th>Freigabedatum</th>
			<th>Kommentar</th>
			<th class="text-right">SOLL</th>
			<th class="text-right">IST</th>
			<th class="text-right">Saldo</th>
		</tr>
	</thead>
	<tbody>
		<?php $count = $total_soll = $total_ist = $total_diff = 0; ?>
		@if (!empty($data))
			@foreach ($data as $element)
				<?php
					$fkbtn_title = "FR";
					$btn_title = "NE";
					$fkclass_release = $class_release = "btn-primary";

			       if($user->role==1 || $user->role==5)
			       $class_release = "btn-primary release-op-list";

			   		if($user->email==config('users.falk_email'))
			       $fkclass_release = "btn-primary release-op-list";
			       

			   		$v1 = $v2 = "";
			   		$n1 = 0;
			   		$n2 = 0;
					if(isset($release[$element->id][1]))
					{	
						$n1 = 1;
						$v1 = $release[$element->id][1];
						$class_release = "btn-success";
					}
					if(isset($release[$element->id][2]))
					{	
						$n2 = 1;
						$v2 = $release[$element->id][2];
						$fkclass_release = "btn-success";
					}

					if($status==3 && !isset($release[$element->id][1]))
						continue;

					if($status==4 && isset($release[$element->id][1]))
						continue;



					if($status==1 && $element->no_opos_status){
						continue;
					}
					if($status==2 && !$element->no_opos_status){
						continue;
					}

					$download_path = "";
					if($element->invoice){
						$download_path = "https://drive.google.com/drive/u/2/folders/".$element->file_basename;
		                if($element->file_type == "file"){
		                    $download_path = "https://drive.google.com/file/d/".$element->file_basename;
		                }
		                $download_path = '<a  target="_blank"  title="'.$element->invoice.'"  href="'.$download_path.'">'.$element->invoice.'</a>';
		           	}

		           	$subject = 'Offene Posten: '.$element->name_of_property;
		           	$content = '';

		           	if($download_path != ""){
		           		$content .= 'Excel: '.$download_path;
		           	}
					if($element->comment){
						$content .= '<br/>Kommentar User: '.$element->comment;
					}

					$taburl = route('properties.show',['property' => $element->id]).'?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid';
        			$taburl = "<a href='".$taburl."'>".$taburl."</a>";
				?>
				<tr>
					<td>
						<a href="{{route('properties.show',['property'=> $element->id])}}?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid">
                            {{$element->name_of_property}}
                        </a>
					</td>
					<td>
						<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $element->id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="OFFENE POSTEN" data-id="{{ $element->pdp_id }}" data-section="properties_default_payers">{{ $element->name }}</a>

					</td>
					<td>
						<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $element->id }}" data-user-id="{{ $element->up_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="OFFENE POSTEN" data-id="{{ $element->pdp_id }}" data-section="properties_default_payers">{{ $element->up_name }}</a>
					</td>
					<th>{{ $element->hausmaxx }}</th>
					<td>{{show_datetime_format($element->created_at)}}</td>
					<!-- <td> -->
						<?php /*if($element->month && strlen($element->month)!=2)
								echo '0';
								echo $element->month;

								if(!$element->month)
									$count +=1;*/
						?>
					<!-- </td> -->
					<!-- <td>{{$element->year}}</td> -->
					<td><?php echo get_paid_checkbox($element->id,$element->no_opos_status);?></td>
					<td>{!! $download_path !!}</td>
					<td>
						<span style="display: none;">{{$n1}}</span>
						<button data-id="{{$element->id}}" data-type="oplist_{{$r_year}}_{{$r_month}}" class="btn btn-xs {{$class_release}}">{{$btn_title}}</button>
					</td>

					<td>
						<span style="display: none;">{{$n2}}</span>
						<button data-id="{{$element->id}}" data-type="falkoplist_{{$r_year}}_{{$r_month}}" class="btn btn-xs {{$fkclass_release}}">{{$fkbtn_title}}</button>
					</td>
					<td>
					NE: {{show_datetime_format($v1)}}<br>
					FR: {{show_datetime_format($v2)}}
					</td>
					<td>
						{{-- <span class="long-text">{!! $element->comment !!}</span> --}}
							@if($element->pdp_id)
							
							@php
								$comment = DB::table('properties_comments as pc')->selectRaw('pc.*, u.name, u.role, u.company')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.property_id', $element->id)->where('pc.type', 'properties_default_payers')->orderBy('pc.created_at', 'desc')->first();
							@endphp

							@if($comment)
								@php
									$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
									$commented_user = $comment->name.''.$company;
								@endphp
								<p class="long-text" style="margin-bottom: 0px;">
									<span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
								</p>
							@endif
							
							<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="{{$element->pdp_id}}" data-property-id="{{ $element->id }}" data-type="properties_default_payers" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
							@endif

					</td>
					<td class="text-right">
						<?php 
							$soll = \App\Models\Properties::getSumOfSolRentPaid($element->id, $r_month, $r_year);
							$total_soll += $soll;
						?>
						{{ show_number($soll,2) }}
					</td>
					<td class="text-right">
						<?php 
							$ist = \App\Models\Properties::getSumOfIstRentPaid($element->id, $r_month, $r_year);
							$total_ist += $ist;
						?>
						{{ show_number($ist,2) }}
					</td>
					<td class="text-right">
						<a href="javascript:void(0);" class="diff" data-url="{{ route('get_rent_paid_data', ['property_id' => $element->id]) }}?rmonth={{$r_month}}&ryear={{$r_year}}">
							<?php 
								$diff = \App\Models\Properties::getSumOfDiffRentPaid($element->id, $r_month, $r_year);
								$total_diff += $diff;
							?>
							{{ show_number($diff,2) }}
						</a>
					</td>
				</tr>
			@endforeach
		@endif
	</tbody>
	<tfoot>
		<tr>
			<th class="text-center" colspan="11">Gesamt</th>
			<th class="text-right">{{ show_number($total_soll, 2) }}</th>
			<th class="text-right">{{ show_number($total_ist, 2) }}</th>
			<th class="text-right">{{ show_number($total_diff, 2) }}</th>
		</tr>
	</tfoot>
</table>

