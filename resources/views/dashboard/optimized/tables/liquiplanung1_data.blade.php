
@if(in_array($row, [1,2,3,4,6,7,8,9,17,18]))
	<table id="liquiplanung-data-table" class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Objekt</th>
				<th>AM</th>
				<?php
				$p_month = date('m');
				$p_year = date('Y');
				if($p_month==1){
					$p_month = 12;
					$p_year = date('Y')-1;
				}
				else
					$p_month = date('m')-1;

				if(strlen($p_month)==1){
					$p_month = '0'.$p_month;
				}

				$premonth_data = \App\Models\PropertyAccountBalance::where('created_at','like','%'.$p_year.'-'.$p_month.'-%')->groupBy('property_id')->get();


				$pre_array = array();
				foreach ($premonth_data as $key => $value) {
					$pre_array[$value->property_id] = $value->current_balance; 
				}



				?>
				@if($row==18)
				<th class="text-right">Aktiendepot</th>
				@elseif(in_array($row, [1,2,3,4]))
					<th class="text-right">
					KW:<?php  $week['first_last_week'] = getWeekMonSun(-1); 
					$ddate = $week['first_last_week']['Mon'];
					$date = new DateTime($ddate);
					echo $week1 = $date->format("W");


					?>
					</th>
					<th class="text-right">Aktueller Kontostand</th>
					<th class="text-right">Datum</th>
				@elseif($row == 6)
					<th class="text-right">Nettomiete p.m.</th>
					<th class="text-right">NK netto p.m.</th>
				@elseif($row == 7)
					<th class="text-right">Objektaufwand p.m.</th>
					<th class="text-right">Verwaltung p.m.</th>
				@elseif($row == 8 || 17)
					@if($col == 1)
						<th class="text-right">Außerordentliche Kosten p.m.</th>
					@else
						<th class="text-right">ao Kosten Monat +{{ $col-1 }}</th>
					@endif
				@elseif($row == 9)
					<th class="text-right">Kapitaldienst p.m. ann. per 31.12.2019</th>
				@endif
			</tr>
		</thead>
		<tbody>
			@php
			$p_balance_total = 0;
				$total_current_balance = $total_nt = $total_nk = $total_object_effort_pm = $total_effort_pm = $total_loan_service_month = $total_ao_cost_month = $total_ao_einnahmen_month = 0;
			@endphp
			@if($res)
				<?php $no=0;
				if($row==2)
					$not_shown_array = array(3363);
				?>
				@foreach ($res as $no => $element)
					@if( !in_array($element->id, $not_shown_array) || in_array($row, [1,3,4,6,7,8,9,18]) )
						<tr>
							<td><?php echo $element->id;
							$no +=1;
							?></td>
							<td>
								<a href="{{route('properties.show',['property'=> $element->id])}}">{{$element->name_of_property}}</a>
							</td>
							<td>
								@if(isset($element->am_id))
									<?php 
										$subject = $element->name_of_property.': '.$header;
										$content = '';
									?>
									<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $element->id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="LIQUIPLANUNG">{{ $element->am_name }}</a>
								@endif
							</td>
							@if($row==18)
							<td>
								{{show_number($element->share_deposite)}}
							</td>
							@endif

							@if(in_array($row, [1,2,3,4]))
								<td class="text-right">
									<?php
									$p_balance = 0;
									if(isset($pre_array[$element->id]))
										$p_balance = $pre_array[$element->id];
									$p_balance_total +=$p_balance;
									?>
									{{ show_number($p_balance, 2) }}
								</td>
								<td class="text-right" style="color:@if($p_balance<$element->current_balance) green @else red @endif">
									{{ show_number($element->current_balance, 2) }}
									@php
										$total_current_balance += $element->current_balance;
									@endphp
								</td>
								<td>{{ (isset($element->current_balance_updated) && $element->current_balance_updated) ? show_date_format($element->current_balance_updated) : '' }}</td>
							@elseif($row == 6)
								<?php 
									if($col == 1){
										$element->nt = $element->nt;
										$element->nk = $element->nk;
									}else{
										$nt_name = 'nt_'.$col;
										$nk_name = 'nk_'.$col;
										$element->nt = $element->$nt_name;
										$element->nk = $element->$nk_name;
									}
								?>
								<td class="text-right">
									{{ show_number($element->nt, 2) }}
									@php
										$total_nt += $element->nt;
									@endphp
								</td>
								<td class="text-right">
									{{ show_number($element->nk, 2) }}
									@php
										$total_nk += $element->nk;
									@endphp
								</td>
							@elseif($row == 7)
								<td class="text-right">
									@php
										$object_effort_pm = ($col == 1) ? calculateAusgaben($element->object_effort_pm) : $element->object_effort_pm;
										$total_object_effort_pm += $object_effort_pm;
									@endphp
									{{ show_number($object_effort_pm, 2) }}
								</td>
								<td class="text-right">
									@php
										$effort_pm = ($col == 1) ? calculateAusgaben($element->effort_pm) : $element->effort_pm;
										$total_effort_pm += $effort_pm;
									@endphp
									{{ show_number($effort_pm, 2) }}
								</td>
							@elseif($row == 8)
								@php
									$ao_cost_month_name = 'ao_cost_month'.($col);
									$ao_cost = (isset($element->$ao_cost_month_name)) ? $element->$ao_cost_month_name : 0;
									$total_ao_cost_month += $ao_cost;
								@endphp
								<td class="text-right">
									<a href="javascript:void(0);" class="show_ao_cost" data-url="{{ route('get_ao_cost', ['property_id' => $element->id]) }}?type=0&month={{ $column_month }}&year={{ $column_year }}">{{ show_number($ao_cost, 2) }}</a>
								</td>
							@elseif($row == 17)
								@php
									$ao_einnahmen_month_name = 'ao_einnahmen_month'.($col);
									$ao_einnahmen = (isset($element->$ao_einnahmen_month_name)) ? $element->$ao_einnahmen_month_name : 0;
									$total_ao_einnahmen_month += $ao_einnahmen;
								@endphp
								<td class="text-right">
									<a href="javascript:void(0);" class="show_ao_cost" data-url="{{ route('get_ao_cost', ['property_id' => $element->id]) }}?type=1&month={{ $column_month }}&year={{ $column_year }}">{{ show_number($ao_einnahmen, 2) }}</a>
								</td>
							@elseif($row == 9)
								<td class="text-right">
									{{ show_number($element->loan_service_month, 2) }}
									@php
										$total_loan_service_month += $element->loan_service_month;
									@endphp
								</td>
							@endif
						</tr>
					@endif
				@endforeach
			@endif
		</tbody>
		@if(in_array($row, [2,6,7,8,9,17]))
			<tfoot>
				<tr>
					<th colspan="3">Gesamt</th>
					@if($row == 2)

						<th class="text-right">{{ show_number($p_balance_total, 2) }}</th>
						<th class="text-right">{{ show_number($total_current_balance, 2) }}</th>
						<th></th>
					@elseif($row == 6)
						<th class="text-right">{{ show_number($total_nt, 2) }}</th>
						<th class="text-right">{{ show_number($total_nk, 2) }}</th>
					@elseif($row == 7)
						<th class="text-right">{{ show_number($total_object_effort_pm, 2) }}</th>
						<th class="text-right">{{ show_number($total_effort_pm, 2) }}</th>
					@elseif($row == 8)
						<th class="text-right">{{ show_number($total_ao_cost_month, 2) }}</th>
					@elseif($row == 17)
						<th class="text-right">{{ show_number($total_ao_einnahmen_month, 2) }}</th>
					@elseif($row == 9)
						<th class="text-right">{{ show_number($total_loan_service_month, 2) }}</th>
					@endif
				</tr>
			</tfoot>
		@endif
	</table>
@endif

@if(in_array($row, [11,12,13,16]))
	<table id="liquiplanung-data-table" class="table table-striped">
		<thead>
			<tr>
				<td>#</td>
				<td>Objekt</td>
				
				@if($row != 13 && $row != 12)
					<td>AM</td>
				@endif

				@if(in_array($row, [11,12,16]))
					@if($row == 12)
						<td>Einkäufer</td>
					@endif
					@if($row == 11 || $row == 12)
						<td>BNL</td>
					@endif
					@if($row == 12)
						<td>Notartermin</td>
					@endif
					<td class="text-right">Eigenmittel</td>
				@elseif($row == 13)
					<td>Verkäufer</td>
					<td>BNL</td>
					<td>Verkaufspreis</td>
					<td class="text-right">Cashflow Rückfluss</td>
				@endif
			</tr>
		</thead>
		<tbody>
			@php
				$total_gkp = $total_vk_preis = $total_verkaufspreis = 0;
			@endphp
			@if ($res)
				@foreach ($res as $no => $element)
					<tr>
						<td>{{ $no+1 }}</td>
						<td>
							<a href="{{route('properties.show',['property'=> $element['property_actual_id']])}}">{{$element['name_of_property']}}</a>
						</td>

						@if($row != 13 && $row != 12)
							<td>
								@if(isset($element['am_id']))
									<?php 
										$subject = $element['name_of_property'].': '.$header;
										$content = '';
									?>
									<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $element['property_actual_id'] }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="LIQUIPLANUNG">{{ $element['am_name'] }}</a>
								@endif
							</td>
						@endif

						@if(in_array($row, [11,12,16]))
							
							@if ($row == 12)
								<td>
									<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $element['property_actual_id'] }}" data-user-id="{{ $element['ek_id'] }}" data-subject="Einkäufe Liquiplan: {{ $element['name_of_property'] }}" data-content="" data-title="Verkäufe" data-id="" data-section="">{{ $element['ek_name'] }}</a>
								</td>
							@endif

							@if($row == 11 || $row == 12)
								<td>{{ show_date_format($element['bnl_date']) }}</td>
							@endif

							@if ($row == 12)
								<td>{{ show_date_format($element['notartermin']) }}</td>
							@endif

							<td class="text-right">
								{{ show_number($element['gkp'], 2) }}
								@php
									$total_gkp += $element['gkp'];
								@endphp
							</td>
						@elseif($row == 13)
							<td>
								<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $element['property_actual_id'] }}" data-user-id="{{ $element['vk_id'] }}" data-subject="Verkäufe: {{ $element['name_of_property'] }}" data-content="" data-title="Verkäufe" data-id="" data-section="">{{ $element['vk_name'] }}</a>
							</td>
							<td>{{ show_date_format($element['bnl']) }}</td>
							<td>
								{{ show_number($element['verkaufspreis'], 2) }}
								@php
									$total_verkaufspreis += $element['verkaufspreis'];
								@endphp
							</td>
							<td>
								{{ show_number($element['vk_preis'], 2) }}
								@php
									$total_vk_preis += $element['vk_preis'];
								@endphp
							</td>
						@endif
					</tr>
				@endforeach
			@endif
		</tbody>
		<tfoot>
			<tr>
				<?php 
					$colspan = 3;
					if($row == 11)
						$colspan = 4;
					if($row == 12)
						$colspan = 5;
				?>
				@if(in_array($row, [11,12,16]))
					<th colspan="{{ $colspan }}">Gesamt</th>
					<th class="text-right">{{ show_number($total_gkp, 2) }}</th>
				@elseif($row == 13)
					<th colspan="4">Gesamt</th>
					<th class="text-right">{{ show_number($total_verkaufspreis, 2) }}</th>
					<th class="text-right">{{ show_number($total_vk_preis, 2) }}</th>
				@endif
			</tr>
		</tfoot>
	</table>
@endif