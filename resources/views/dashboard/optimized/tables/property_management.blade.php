@if(count($data))
    @foreach($data as $property_management)
        <div class="row">
            <div class="col-md-12 table-responsive">
                <h3>{{$property_management->name_of_property}}
                    <!-- <button class="btn  btn-xs btn-primary request-insurance-button" data-id="{{$property_management->property_id}}" data-type="{{ $release }}" style="margin-left: 10px;">Freigeben</button> -->
                </h3>
                <?php
            		$jsonarray = array();
            		$total_actual_net_rent = 0;
                    if ($property_management->text_json) {
                        $jsonarray = json_decode($property_management->text_json, true);
                    }
                    if (isset($jsonarray['total_actual_net_rent'])) {
                        $total_actual_net_rent = $jsonarray['total_actual_net_rent'];
                    }
                    $data = DB::table('property_managements')->select('property_managements.*',DB::raw("CASE WHEN EXISTS (SELECT * FROM properties_mail_logs WHERE type = 'release_management' AND properties_mail_logs.record_id = property_managements.property_id) THEN 1 ELSE 0 END as is_release"))
                    ->where('not_release',0)
                    ->where('property_id', $property_management->property_id)->get();
                ?>

                <div class="table-responsive">
            	   <div class="property-table">
                        <table class="table table-striped table-property-management-approval">
                            <thead>
                                <tr>

                                    <th>Objekt</th>
                                    <th>Name</th>
                                    <th class="text-right">IST Netto Miete p.m.</th>
                                    <th class="text-right">Prozent(%)</th>
                                    <th class="text-right"> Fee</th>
                                    <th>Kommentar</th>
                                    <th>AM-Empfehlung</th>
                                    <th>Falk</th>
                                    <th>Ablehnen</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key=>$row)
                                    <tr>
                                    	<td>
                            				<a href="{{route('properties.show',['property'=> $property_management->property_id])}}?tab=property_management">
                            	            	{{$property_management->name_of_property}}
                            	        	</a>
                            			</td>
                                        <td>{{$row->name}}</td>
                                        <td class="text-right">{{ show_number($total_actual_net_rent, 2) }}</td>
                                        <td class="text-right">{{show_number($row->percent,2)}}</td>
                                        <td class="text-right">@if($row->percent) {{show_number($total_actual_net_rent*$row->percent/100,2)}}@endif</td>
                                        <td><span class="long-text">{{$row->comment}}</span></td>
                                        <td class="text-center"><input data-column="release_status1" type="checkbox" class="change-property-manage-status" data-id="{{$row->id}}" data-property_id="{{$row->property_id}}" @if($row->release_status1) checked @endif ></td>
                                        <!-- <td class="text-center"><input data-column="release_status2" type="checkbox" class="change-property-manage-status" data-id="{{$row->id}}" data-property_id="{{$row->property_id}}" @if($row->release_status2) checked @endif ></td> -->
                                        <td>
                                                <button class="btn  btn-primary request-insurance-button" data-id="{{$row->id}}" data-type="{{ $release }}">Freigeben</button>
                                            </td>
                                            <td>

                                                <button data-id="<?=$row->id?>" type="button" class="btn  btn-primary btn-not-release-management">Ablehnen</button>
                                            
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <div class="row property-comment-section">
            <div class="col-md-12 form-group">
                <label>Kommentar</label>
                <textarea class="form-control property-comment" rows="5"></textarea>
            </div>
            <div class="col-md-12">
                    <?php
                        $subject = 'FREIGABE HAUSVERWALTUNG: '.$property_management->name_of_property;
                        $content = '';
                    ?>
                    <button type="button" class="btn btn-primary btn-add-property-comment pull-left" data-reload="0" data-record-id="" data-property-id="{{ $property_management->property_id }}" data-type="freigabe_hausverwaltung" data-subject="{{ $subject }}" data-content='{{ $content }}'>Senden</button>

                    <button type="button" class="btn btn-primary btn-show-property-comment pull-right" data-form="0" data-record-id="" data-property-id="{{ $property_management->property_id }}" data-type="freigabe_hausverwaltung" data-subject="{{ $subject }}" data-content='{{ $content }}'>Show Kommentar</button>

            </div>
        </div>
    @endforeach
@else
    <p class="text-center">Data not found!</p>
@endif