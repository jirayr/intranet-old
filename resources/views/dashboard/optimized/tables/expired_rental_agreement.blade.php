<?php
    function short_name($string){
       if($string){
           $arr = explode(" ", $string);
           if(count($arr)>1)
               return substr($arr[0], 0,1).substr($arr[1], 0,1);
           else
               return substr($string, 0,1);
       }
       return $string;
   }
?>
<div class="table-responsive">
   <table id="expired-rental-agreement-table" class="table table-striped">
      <thead>
         <tr>
            <th>#</th>
            <th>AM</th>
            <th>Objekt</th>
            <th>Vermietet</th>
            <th>Mietende</th>
            <th>Fläche (m2)</th>
            <th>IST-Nkm (€)</th>
            <th>Typ</th>
            <th>Kommentare</th>
         </tr>
      </thead>
      <tbody>
         @php $sumau = $amount = 0; @endphp  
         @foreach($tenancy_items_2 as $tenancy_item)
            {{-- @if($tenancy_item->status && $tenancy_item->rent_end && $tenancy_item->rent_end<=date('Y-m-d')) --}}
               <?php
                  $tenancy_item->creator_name = short_name($tenancy_item->creator_name);
               ?>
               <tr>
                  <td>{{$tenancy_item->id}}</td>
                  <?php
                  $subject = 'ABGELAUFENE MIETVERTRÄGE: '.$tenancy_item->object_name;
                  $content = 'Vermietet: '.$tenancy_item->name;
               ?>
                  <td>
                    <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $tenancy_item->property_id }}" data-subject="{{ $subject }}" data-content="{{ $content }}" data-title="ABGELAUFENE MIETVERTRÄGE">{{ $tenancy_item->creator_name }}</a>
                  </td>
                  <td>{{$tenancy_item->object_name}}</td>
                  <td><a href="javacript:void(0)" data-id="{{$tenancy_item->id}}" class="tenancy_item">{{$tenancy_item->name}}</a></td>
                  <td>{{show_date_format($tenancy_item->rent_end)}}</td>
                  <td class="text-right">{{$tenancy_item->rental_space ? number_format($tenancy_item->rental_space,2,",",".") : 0}}</td>
                  <td class="text-right">{{$tenancy_item->actual_net_rent ? number_format($tenancy_item->actual_net_rent,2,",",".") : 0}}</td>
                  <td>{{$tenancy_item->type}}</td>
                  <td><span class="long-text">{!! ($tenancy_item->comment) ? $tenancy_item->comment : $tenancy_item->comment1 !!}</span></td>
               </tr>
               <?php
                  $sumau += 1; 
                  $amount +=$tenancy_item->actual_net_rent;
               ?>
            {{-- @endif --}}
         @endforeach
      </tbody>
   </table>
   <div class="sumaus" style="display: none">{{number_format($amount,2,',','.')}}€ ({{ $sumau }})</div>
</div>