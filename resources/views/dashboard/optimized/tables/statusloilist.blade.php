<?php $months = range(1,12); ?>
<div class="row">
	<div class="col-md-12">
		<select  id="loi-month">
            <option value="0" >all</option>
            @foreach($months as $list)
            <option value="{{$list}}" {{ ($r_month == $list) ? 'selected' : '' }}>{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
            @endforeach
         </select>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 table-responsive">
		<table class="table table-striped" id="list-statusloi">
			<thead>
			<tr>
				<th>Transaction Manager</th>
		        <th>Amount</th>
		        <th>Last sent</th>
			</tr>
			</thead>
			<tbody>
				@foreach($data as $lists)
					@foreach($lists as $list)
						<tr>
							<td><a href="javascript:void(0)" onclick="loadAllStatusLoiByUser({{$list->user_id}})">{{$list->name}}</a></td>
							<td>{{$lists->count()}}</td>
							<td>{{ show_date_format($list->date) }}</td>
						</tr>
						@break
					@endforeach
				@endforeach
			</tbody>
		</table>
	</div>
</div>