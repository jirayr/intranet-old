<div class="table-responsive">
   <table class="table table-striped" id="expiring-building-insurance-table">
      <thead>
         <tr>
            <th>Objekt</th>
            <th>AM</th>
            <th>Name</th>
            <th>Kommentare</th>
            <th>Betrag</th>
            <th>Laufzeit</th>
            <th>Kündigungsfrist</th>
            <th>Anzahl</th>
         </tr>
      </thead>
      <tbody>
         @if($data)
            @foreach($data as $list)
               <tr>
                  <td>
                     <a href="{{route('properties.show',['property'=>$list->id])}}?tab=insurance_tab">
                     {{$list->name_of_property}}</a>
                  </td>
                  <?php
                  $subject = 'AUSLAUFENDE GEBÄUDEVERSICHERUNGEN: '.$list->name_of_property;
                  $content = 'Name: '.$list->axa;
               ?>
                  <td>
                     <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $list->id }}" data-subject="{{ $subject }}" data-content="{{ $content }}" data-title="AUSLAUFENDE GEBÄUDEVERSICHERUNGEN">{{ $list->asset_manager }}</a>
                  </td>
                  <td>{{$list->axa}}</td>
                  <td>{{$list->gebaude_comment}}</td>
                  <td>
                     @if(is_numeric($list->gebaude_betrag))
                     {{number_format($list->gebaude_betrag,2,",",".")}}
                     €
                     @else
                     {{$list->gebaude_betrag}}
                     @endif
                  </td>
                  <td>
                     @if($list->gebaude_laufzeit_to)
                     {{date_format(  date_create(str_replace('.', '-', $list->gebaude_laufzeit_to)) , 'd.m.Y')}}
                     @else
                     {{$list->gebaude_laufzeit_to}}
                     @endif
                  </td>
                  <td>{{$list->gebaude_kundigungsfrist}}</td>
                  <td>
                     <a href="{{route('properties.show',['property'=>$list->id])}}?tab=test_tab">
                     {{$list->insurances($list,1)}}</a>
                  </td>
               </tr>
            @endforeach
         @endif
      </tbody>
   </table>
</div>