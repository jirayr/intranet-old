<div class="row">
   <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
      <div class="white-box">
         {{-- <h3 class="box-title m-b-0">Kommentare</h3> --}}
         {{-- <p class="text-muted m-b-30">&nbsp;</p> --}}
         <div class="row">
            <div class="col-sm-6" style="height:440px; overflow-y: scroll;">
               @foreach($comments as $comment)
               <div class="d-flex flex-row comment-row mt-0 mb-0">
                  <div class="p-2">
                     <img src="{{(config('upload.avatar_path').$comment->user->image)}}" alt="user" width="40" class="rounded-circle" style="border-radius: 50%;">
                  </div>
                  <div class="comment-text w-100">
                     <h5 class="font-normal mb-1">{{$comment->user->name}}</h5>
                     <span class="text-muted mr-2 font-12">
                     {{ show_datetime_format($comment->created_at) }}
                     </span>
                     <span class="hidden badge @if($comment->status == 'Steht aus') badge-info @endif @if($comment->status == 'Akzeptiert') badge-success @endif @if($comment->status == 'Abgelehnt') badge-danger @endif badge-rounded text-uppercase font-medium">{{ $comment->status }}</span>
                     <p class="mb-2 d-block font-14 text-muted font-light mt-3" style="">
                        <a  href="{{route('properties.show',['property'=>$comment->property_id])}}?tab=comment_tab">
                        {{ $comment->name_of_property }} :
                        </a>
                        <a href="{{route('properties.show',['property'=>$comment->property_id])}}?tab=comment_tab">
                        {!! $comment->comment !!}
                        </a>
                     </p>
                     @if($comment->pdf_file)
                     <a href="{{ asset('pdf_upload/'.$comment->pdf_file) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a><br/>
                     @endif
                     <hr>
                     {{-- 
                     <div class="mt-3">
                        <a href="javacript:void(0)" class="btn btn btn-rounded btn-outline-success mr-2 btn-sm"><i class="ti-check mr-1"></i>Akzeptieren</a>
                        <a href="javacript:void(0)" class="btn-rounded btn btn-outline-danger btn-sm"><i class="ti-close mr-1"></i> Ablehnen</a>
                     </div>
                     --}}
                  </div>
               </div>
               @endforeach
            </div>
            <div class="col-sm-6">
               <div class="table-responsive">
                  <table class="table table-striped" id="comment-table">
                     <tbody>
                        @foreach($ccomments as $comment)
                        <tr>
                           <td>
                              <a  href="{{route('properties.show',['property'=>$comment->property_id])}}?tab=comment_tab">
                              {{ $comment->name_of_property }} :
                              </a>
                           </td>
                           <td>{{$comment->name}}</td>
                           <td>
                              <a class="comment-detail" href="javacript:void(0)" data-id="{{$comment->id}}">
                              @if($comment->type==1)
                              Mahnung
                              @elseif($comment->type==2)
                              Gerichtliches Mahnverfahren
                              @elseif($comment->type==3)
                              Ratenzahlung/Kündigung
                              @endif
                              </a>
                           </td>
                           <td>{{show_datetime_format($comment->created_at)}}</td>
                        </tr>
                        @endforeach
                        @if(1==2)
                        <tr>
                           <td>Mahnung</td>
                           <td><input type="checkbox" @if(isset($ccomments[0]['status']) && $ccomments[0]['status']) checked @endif></td>
                           <td>@if(isset($ccomments[0]['comment'])){{$ccomments[0]['comment']}}@endif
                              <br>
                              @if(isset($ccomments[0]['files']) && $ccomments[0]['files'])
                              <?php
                                 $ar = json_decode($ccomments[0]['files'],true)
                                 ?>
                              @foreach($ar as $list)
                              <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
                              @endforeach
                              @endif
                           </td>
                        </tr>
                        <tr>
                           <td>Gerichtliches Mahnverfahren </td>
                           <td><input type="checkbox" name="" @if(isset($ccomments[1]['status']) && $ccomments[1]['status']) checked @endif></td>
                           <td>@if(isset($ccomments[1]['comment'])){{$ccomments[1]['comment']}}@endif
                              <br>
                              @if(isset($ccomments[1]['files']) && $ccomments[1]['files'])
                              <?php
                                 $ar = json_decode($ccomments[1]['files'],true)
                                 ?>
                              @foreach($ar as $list)
                              <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
                              @endforeach
                              @endif
                           </td>
                        </tr>
                        <tr>
                           <td>Ratenzahlung /Kündige</td>
                           <td><input type="checkbox" name="" @if(isset($ccomments[2]['status']) && $ccomments[2]['status']) checked @endif></td>
                           <td>@if(isset($ccomments[2]['comment'])){{$ccomments[2]['comment']}}@endif
                              <br>
                              @if(isset($ccomments[2]['files']) && $ccomments[2]['files'])
                              <?php
                                 $ar = json_decode($ccomments[2]['files'],true)
                                 ?>
                              @foreach($ar as $list)
                              <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
                              @endforeach
                              @endif
                           </td>
                        </tr>
                        @endif
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>