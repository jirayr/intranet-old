@if(count($data))
	@foreach($data as $property)
		<div class="row">
			<div class="col-md-12 table-responsive">
				<h3>{{$property->name_of_property}}
					<button class="btn  btn-xs btn-primary request-insurance-button" data-id="{{$property->id}}" data-type="{{$release}}" style="margin-left: 10px;">Freigeben</button>
				</h3>
				<table  class="table table-striped {{ ($type == 1) ? 'table-approval-building-insurance' : 'table-release-liability' }}">
					<thead>
						<tr>
							<th>Objekt</th>
							<th>Name</th>
							<th>Betrag</th>
							<th>Laufzeit von -Laufzeit bis</th>
							<th>Kündigungsfrist</th>
							<th>Kommentar</th>
							<th>AM-Empfehlung</th>
							<th>Falk</th>
						</tr>
					</thead>
					<tbody>
							<?php $property->insurance = DB::table('property_insurances')->selectRaw('*')->where('property_id', $property->id)->where('type', $type)->get(); ?>

							@if ($type == 1)
								@if($property->gebaude_betrag)
									<tr>
										<td>
											<a href="{{route('properties.show',['property'=> $property->id])}}?tab=insurance_tab">
				                            	{{$property->name_of_property}}
				                        	</a>
										</td>
										<td>{{ $property->axa }}</td>
										<td>{{ (is_numeric($property->gebaude_betrag)) ? number_format( $property->gebaude_betrag, 2 ,",",".") : '' }}  €</td>
										<td>{{ show_date_format($property->gebaude_laufzeit_from) }} - {{ show_date_format($property->gebaude_laufzeit_to) }}</td>
										<td>{{ $property->gebaude_kundigungsfrist }}</td>
										<td><span class="long-text">{{ $property->gebaude_comment }}</span></td>
										<td>
											<input data-column="gebaude" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{ $property->id }}" {{ ($property->gebaude_is_approved) ? 'checked' : '' }}>
										</td>
										<td>
											<input data-column="gebaude_falk_approved" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$property->id}}" {{ ($property->gebaude_falk_approved) ? 'checked' : '' }}>
										</td>
									</tr>
								@endif
								@if(isset($property->insurance) && count($property->insurance) > 0)
									@foreach ($property->insurance as $key1 => $value1)
										<tr>
											<td>
												<a href="{{route('properties.show',['property'=> $property->id])}}?tab=insurance_tab">
					                            	{{$property->name_of_property}}
					                        	</a>
											</td>
											<td>{{ $value1->name }}</td>
											<td>{{ (is_numeric($value1->amount)) ? number_format( $value1->amount, 2 ,",",".") : '' }}  €</td>
											<td>{{ show_date_format($value1->date_from) }} - {{ show_date_format($value1->date_to) }}</td>
											<td>{{ $value1->kundigungsfrist }}</td>
											<td><span class="long-text">{{ $value1->note }}</span></td>
											<td>
												<input data-column="insurance" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$value1->id}}" {{ ($value1->is_approved) ? 'checked' : '' }}>
											</td>
											<td>
												<input data-column="falk" type="checkbox" class="falk-checkbox-is-checked" data-id="{{$value1->id}}" {{ ($value1->is_checked) ? 'checked' : '' }}>
											</td>
										</tr>
									@endforeach
								@endif
							@endif

							@if ($type == 2)
								@if($property->haftplicht_betrag)
									<tr>
										<td>
											<a href="{{route('properties.show',['property'=> $property->id])}}?tab=insurance_tab">
				                            	{{$property->name_of_property}}
				                        	</a>
										</td>
										<td>{{ $property->allianz }}</td>
										<td>{{ (is_numeric($property->haftplicht_betrag)) ? number_format($property->haftplicht_betrag,2,",",".") : '' }} €</td>
										<td>{{ show_date_format($property->haftplicht_laufzeit_from) }} - {{ show_date_format($property->haftplicht_laufzeit_to) }}</td>
										<td>{{ $property->haftplicht_kundigungsfrist }}</td>
										<td><span class="long-text">{{ $property->haftplicht_comment }}</span></td>
										<td>
											<input data-column="falk" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$property->id}}" {{ ($property->haftplicht_is_approved) ? 'checked' : '' }}>
										</td>
										<td>
											<input data-column="haftplicht_falk_approved" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$property->id}}" {{ ($property->haftplicht_falk_approved) ? 'checked' : '' }}>
										</td>
									</tr>
								@endif
								@if(isset($property->insurance) && count($property->insurance) > 0)
									@foreach ($property->insurance as $key1 => $value1)
										<tr>
											<td>
												<a href="{{route('properties.show',['property'=> $property->id])}}?tab=insurance_tab">
					                            	{{$property->name_of_property}}
					                        	</a>
											</td>
											<td>{{ $value1->name }}</td>
											<td>{{ (is_numeric($value1->amount)) ? number_format( $value1->amount, 2 ,",",".") : '' }}  €</td>
											<td>{{ show_date_format($value1->date_from) }} - {{ show_date_format($value1->date_to) }}</td>
											<td>{{ $value1->kundigungsfrist }}</td>
											<td><span class="long-text">{{ $value1->note }}</span></td>
											<td>
												<input data-column="insurance" type="checkbox" class="versicherunge-checkbox-is-approved" data-id="{{$value1->id}}" {{ ($value1->is_approved) ? 'checked' : '' }}>
											</td>
											<td>
												<input data-column="falk" type="checkbox" class="falk-checkbox-is-checked" data-id="{{$value1->id}}" {{ ($value1->is_checked) ? 'checked' : '' }}>
											</td>
										</tr>
									@endforeach
								@endif
							@endif
					</tbody>
				</table>
			</div>
		</div>
		<div class="row property-comment-section">
			<div class="col-md-12 form-group">
				<label>Kommentar</label>
				<textarea class="form-control property-comment" rows="5"></textarea>
			</div>
			<div class="col-md-12">
					<?php
						if($type == 1){
							$subject = 'FREIGABE GEBÄUDEVERSICHERUNG: '.$property->name_of_property;
							$type = 'freigabe_gebäudevers';
						}else{
							$subject = 'FREIGABE HAFTPFLICHTVERSICHERUNG: '.$property->name_of_property;
							$type = 'freigabe_haftpflichtversicherung';
						}
						$content = '';
					?>
					<button type="button" class="btn btn-primary btn-add-property-comment pull-left" data-reload="0" data-record-id="" data-property-id="{{ $property->id }}" data-type="{{ $type }}" data-subject="{{ $subject }}" data-content='{{ $content }}'>Senden</button>

					<button type="button" class="btn btn-primary btn-show-property-comment pull-right" data-form="0" data-record-id="" data-property-id="{{ $property->id }}" data-type="{{ $type }}" data-subject="{{ $subject }}" data-content='{{ $content }}'>Show Kommentar</button>

			</div>
		</div>
	@endforeach
@else
	<p class="text-center"> Data not found!</p>
@endif
