<div class="table-responsive ">
   <table class="table table-striped dashboard-table" id="hausmax_table">
      <thead>
         <tr>
            <th>Name</th>
            <th class="text-right">Anzahl</th>
            <th class="text-right">Betrag/Monat</th>
            <th class="text-right">Betrag/Jahr</th>
            <th class="text-right">IST-NME</th>
         </tr>
      </thead>
      <tbody>
         @if(isset($hausmax_properties))
            @php $sum=0; $rent_sum = 0;@endphp
            @foreach($hausmax_properties as $key=>$hausmax)
               <?php
                  if(is_numeric($hausmax['amount']))
                     $sum +=$hausmax['amount'];    
                  
               ?>
               <tr>
                  <td>
                     @php 
                        $tendency_division = 0;
                        $tendency_item = \App\Http\Controllers\HomeController::hausmaxx_tenancy_schedule_items($key);
                        if($tendency_item > 0){
                           if($hausmax['amount'] > 0){
                              $tendency_division = $hausmax['amount']*12/$tendency_item*100;
                           }
                        }
                        $rent_sum +=$tendency_item;
                     @endphp
                     <a href="javascript:void(0)" class="get-hausmaxx-data" data-id="{{ $key }}" >{{ $key }}</a>
                  </td>
                  <td class="text-right">{{$hausmax['count']}}</td>
                  <td class="text-right">{{number_format($hausmax['amount'],2,",",".")}}€</td>
                  <td class="text-right">{{number_format($hausmax['amount']*12,2,",",".")}}€</td>
                  <td class="text-right">{{number_format($tendency_item,2,",",".")}}€</td>
               </tr>
            @endforeach
         @endif
      </tbody>
      <tr>
         <th>Summe</th>
         <th></th>
         <th class="text-right">{{ number_format( $sum, 2 ,",",".") }}€</th>
         <th class="text-right">{{ number_format( $sum*12, 2 ,",",".") }}€</th>
         <th class="text-right">{{ number_format( $rent_sum, 2 ,",",".") }}€</th>
      </tr>
   </table>
   <form id="export-to-excel-form" method="post" action="{{url('export-to-excel-haus')}}">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <input type="hidden" name="property_data" id="property-data" value="">
      {{-- <button type="button" class="btn-export-to-excel btn btn-primary" >Export </button> --}}
      <button type="button" data-url="" data-name="HAUSVERWALTUNG KOSTEN" data-post="1" data-form-id="export-to-excel-form" class="btn btn-primary btn-export">Export</button>
   </form>
</div>