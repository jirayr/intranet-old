<table id="table-contract-release" class="table table-striped">
	<thead>
     	<tr>
	        <th>#</th>
	        <th>Objekt</th>
	        <th>Datei</th>
	        <th>Betrag</th>
	        <th>Startdatum</th>
	        <th>Abschluss am</th>
	        <th>Kündigung spätestens</th>
	        <th>Kündigung am</th>
	        <th>Kommentar</th>
	        <th>User</th>
	        <th>Datum</th>
	        <th>Kategorie</th>
	        {{-- <th>Kommentar Falk</th> --}}
	        <th></th>
	        <th>Ablehnen</th>
	        <th>Pending</th>
	        <th>Weiterleiten an</th>
	        {{-- <th>Aktion</th> --}}
     	</tr>
  	</thead>
	<tbody>
		@if($data)
			@foreach ($data as $key => $value)

				<?php 

					$release2 = 'btn btn-primary contract-release-request';

	                $release2_type = "contract_request";
	                $rbutton_title2 = "Zur Freigabe an Falk senden";

	                $email = strtolower($user->email);

	                if($email==config('users.falk_email')){
	                    $rbutton_title2 = "Freigeben";
	                    $release2 = 'btn btn-primary contract-release-request';
	                    $release2_type = "contract_release";
	                }else{
	                    if($value->contract_request == 1){
	                      $release2 =  " btn-success contract-release-request";
	                      $rbutton_title2 = "Zur Freigabe gesendet";
	                    }
	                }

	                $r1 = $r2 = "";
	                
	                if($value->contract_release == 1){
	                  $release2 =  " btn-success";
	                  $rbutton_title2 = "Freigegeben";
	                  continue;
	                }

				?>

				<?php

					$taburl = route('properties.show',['property' => $value->property_id]).'?tab=contracts';
        			$taburl = "<a href='".$taburl."'>".$taburl."</a>";

					$download_path = "https://drive.google.com/drive/u/2/folders/".$value->file_basename;
	                if($value->file_type == "file"){
	                    $download_path = "https://drive.google.com/file/d/".$value->file_basename;
	                }

	                $subject = 'Vertragsfreigabe: '.$value->name_of_property;
					$content = 'Vertrag: <a title="'.$value->invoice.'" href="'.$download_path.'" target="_blank">'.$value->invoice.'</a>';
					if($value->comment){
						$content .= '<br/>Kommentar User: '.$value->comment;
					}
				?>

				<tr>
					<td>{{ $key+1 }}</td>
					<td>
						<a href="{{route('properties.show',['property'=> $value->property_id])}}?tab=contracts">
                        	{{$value->name_of_property}}
                    	</a>
					</td>
					<td>
						<a  target="_blank"  title="{{ $value->invoice }}"  href="{{ $download_path }}">{{ $value->invoice }}</a>
					</td>
					<td>{{ show_number($value->amount,2) }}</td>
					<td>{{ show_date_format($value->date) }}</td>
					<td>{{ ($value->completion_date != '') ? show_date_format($value->completion_date) : '' }}</td>
					<td>{{ ($value->termination_date != '') ? show_date_format($value->termination_date) : '' }}</td>
					<td>{{ ($value->termination_am != '') ? show_date_format($value->termination_am) : '' }}</td>
					<td>
						@php
							$comment = DB::table('properties_comments as pc')->selectRaw('pc.*, u.name, u.role, u.company')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.record_id', $value->id)->where('pc.type', 'property_contracts')->orderBy('pc.created_at', 'desc')->first();
						@endphp
						@if($comment)
							@php
								$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
								$commented_user = $comment->name.''.$company;
							@endphp
							<p class="long-text" style="margin-bottom: 0px;">
								<span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
							</p>
						@endif
						<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="{{ $value->id }}" data-property-id="{{ $value->property_id }}" data-type="property_contracts" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>

					</td>
					<td>
						<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $value->property_id }}" data-user-id="{{ $value->user_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="VERTRAGSFREIGABEN" data-id="{{ $value->id }}" data-section="property_contracts">{{ $value->name }}</a>
					</td>
					<td>{{ show_datetime_format($value->created_at) }}</td>
					<td>
						{{ $value->category }}
					</td>
					{{-- <td>
						<a href="javascript:void(0);" class="inline-edit contract-comment" data-type="textarea" data-pk="comment2" data-placement="bottom" data-url="{{ url('property/update/contract/'.$value->id) }}" >{{ $value->comment2 }}</a>
					</td> --}}

					<td>
						<button data-id="{{ $value->id }}" type="button" class="btn {{ $release2 }}" data-column="{{ $release2_type }}">{{ $rbutton_title2 }}</button>
					</td>
					<td>
						@if($email == config('users.falk_email'))
							<button data-id="{{ $value->id }}" data-property-id="{{ $value->property_id }}" type="button" class="btn btn-primary contract-mark-as-not-release">Ablehnen</button>
						@endif
					</td>
					<td>
						@if($email == config('users.falk_email'))
							<button data-id="{{ $value->id }}" data-property-id="{{ $value->property_id }}" type="button" class="btn btn-primary contract-mark-as-pending">Pending</button>
						@endif
					</td>
					<td>
						<button type="button" class="btn btn-primary btn-forward-to" data-property-id="{{ $value->property_id }}" data-id="{{ $value->id }}" data-subject="{{ $subject }}" data-content="" data-title="VERTRAGSFREIGABEN" data-reload="0" data-section="property_contracts">Weiterleiten an</button>
					</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>