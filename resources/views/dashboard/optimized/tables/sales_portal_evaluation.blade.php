<div class="row">
   <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
      <div class="white-box">
         {{-- <h3 class="box-title m-b-0">Auswertungen Verkaufsportal</h3> --}}
         <h4>Online Benutzer</h4>
         <div class="table-responsive ">
            <table class="table table-striped dashboard-table verkauf-table" >
               <thead>
                  <tr>
                     <th>Name</th>
                     <th>Aktiv</th>
                     <th>Mail senden</th>
                  </tr>
               </thead>
               <tbody>
                  @if(isset($v_analytic['onlineuser']))
                  @foreach($v_analytic['onlineuser'] as $key => $value)
                  <tr @if($key >= 3) class="hide-records hidden" @endif>
                  <td>
                     {{ (isset($value['first_name'])) ? $value['first_name'] : '' }} {{ (isset($value['last_name'])) ? $value['last_name'] : '' }}
                  </td>
                  <td>
                     {{ ($value['last_active_time']) ? show_datetime_format($value['last_active_time']) : '' }}
                  </td>
                  <td>
                     @if(isset($value['email']) && $value['email'] != '')
                     <button type="button" class="btn btn-primary btn-xs send-mail" data-email="{{ $value['email'] }}">Mail senden</button>
                     @endif
                  </td>
                  </tr>
                  @endforeach
                  @endif
               </tbody>
            </table>
            <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
         </div>
         <h4>Am häufigsten angesehen</h4>
         <div class="table-responsive ">
            <table class="table table-striped dashboard-table verkauf-table">
               <thead>
                  <tr>
                     <th>Anzeigen</th>
                     <th>Anzahl</th>
                  </tr>
               </thead>
               <tbody>
                  @if(isset($v_analytic['most_viewed']))
                  @foreach($v_analytic['most_viewed'] as $k=>$list)
                  <tr @if($k>=3) class="hide-records hidden" @endif>
                  <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/ad/{{$list['slug']}}">{{$list['title']}}</a></td>
                  <td>{{$list['view']}}</td>
                  </tr>
                  @endforeach
                  @endif
               </tbody>
            </table>
            <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
         </div>
         <h4>Am häufigsten verschickt (Exposé)</h4>
         <div class="table-responsive ">
            <table class="table table-striped dashboard-table verkauf-table" >
               <thead>
                  <tr>
                     <th>Anzeigen</th>
                     <th>Anzahl</th>
                  </tr>
               </thead>
               <tbody>
                  @if(isset($v_analytic['most_sent']))
                  @foreach($v_analytic['most_sent'] as $k=>$list)
                  <tr @if($k>=3) class="hide-records hidden" @endif>
                  <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/ad/{{$list['slug']}}">{{$list['title']}}</a></td>
                  <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/ad-activities/{{$list['ad_id']}}/8">{{$list['id']}}</a></td>
                  </tr>
                  @endforeach
                  @endif
               </tbody>
            </table>
            <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
         </div>
         <h4>Makler mit Logo</h4>
         <div class="table-responsive ">
            <table class="table table-striped" >
               <tbody>
                  <tr>
                     <th>Makler mit Logo</th>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agents">{{ number_format($logo_users) }}</a></td>
                  </tr>
                  <tr>
                     <th>Anzahl heruntergeladener Exposés mit Logo</th>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agents">{{ number_format($download_users) }}</a></td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
      <div class="white-box">
         <h4>letzte Aktivität</h4>
         <div class="table-responsive ">
            <table class="table table-striped dashboard-table verkauf-table" >
               <thead>
                  <tr>
                     <th>Name</th>
                     <th>letzte Aktivität</th>
                     <th>Mail senden</th>
                  </tr>
               </thead>
               <tbody>
                  @if(isset($v_analytic['last_active']))
                  @foreach($v_analytic['last_active'] as $key => $value)
                  <tr @if($key >= 3) class="hide-records hidden" @endif>
                  <td>
                     {{ (isset($value['first_name'])) ? $value['first_name'] : '' }} {{ (isset($value['last_name'])) ? $value['last_name'] : '' }}
                  </td>
                  <td>
                     {{ ($value['last_active_time']) ? show_datetime_format($value['last_active_time']) : '' }}
                  </td>
                  <td>
                     @if(isset($value['email']) && $value['email'] != '')
                     <button type="button" class="btn btn-primary btn-xs send-mail" data-email="{{ $value['email'] }}">Mail senden</button>
                     @endif
                  </td>
                  </tr>
                  @endforeach
                  @endif
               </tbody>
            </table>
            <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
         </div>
         <h4>Am häufigsten heruntergeladen (Exposé)</h4>
         <div class="table-responsive ">
            <table class="table table-striped dashboard-table verkauf-table" >
               <thead>
                  <tr>
                     <th>Anzeigen</th>
                     <th>Anzahl</th>
                  </tr>
               </thead>
               <tbody>
                  @if(isset($v_analytic['most_downloaded']))
                  @foreach($v_analytic['most_downloaded'] as $k=>$list)
                  <tr @if($k>=3) class="hide-records hidden" @endif>
                  <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/ad/{{$list['slug']}}">{{$list['title']}}</a></td>
                  <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/ad-activities/{{$list['ad_id']}}/7">{{$list['id']}}</a></td>
                  </tr>
                  @endforeach
                  @endif
               </tbody>
            </table>
            <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
         </div>
         <h4>Aktivster Makler</h4>
         <div class="table-responsive ">
            <table class="table table-striped dashboard-table verkauf-table" >
               <thead>
                  <tr>
                     <th>Anzeigen</th>
                     <th>Anzahl</th>
                     <th>Am häufigsten heruntergeladen (Exposé)</th>
                     <th>Am häufigsten verschickt (Exposé)</th>
                  </tr>
               </thead>
               <tbody>
                  @if(isset($v_analytic['most_active']))
                  @foreach($v_analytic['most_active'] as $k=>$list)
                  <tr @if($k>=3) class="hide-records hidden" @endif>
                  <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agent-activities/{{$list['user_id']}}">{{$list['first_name']}} {{$list['last_name']}}</a></td>
                  <td>{{$list['id']}}</td>
                  <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agent-activities/{{$list['user_id']}}/7">{{$list['downloaded']}}</a></td>
                  <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agent-activities/{{$list['user_id']}}/8">{{$list['sent']}}</a></td>
                  </tr>
                  @endforeach
                  @endif
               </tbody>
            </table>
            <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
         </div>
      </div>
   </div>
</div>