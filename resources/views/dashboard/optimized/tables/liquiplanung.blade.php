<div class="row">
	<div class="col-md-12">
		<a href="javascript:void(0);" data-url="{{ route('get_liquiplanung_excel') }}" class="btn btn-success pull-right btn-export">Export</a>
	</div>
</div>
<br>
<div class="row">
	<div class="col-md-12">
		<table id="liquiplanung-table" class="table table-striped">
			<thead>
		     	<tr>
			        <th>#</th>
			        <th>Objekt</th>
			        <th>AM</th>
			        <th>Nettomiete p.m.</th>
			        <th>NK netto p.m.</th>
			        <th>Objektaufwand p.m.</th>
			        <th>Aufwand p.m</th>
			        <th>Außerordentliche Kosten p.m.</th>
			        <th>Kapitaldienst p.m. ann. per 31.12.2019</th>
			        <th>Aktueller Kontostand</th>
			        <th>Datum</th>
			        <th>Cashflow</th>
		     	</tr>
		  	</thead>
			<tbody>
				@if ($data)
					<?php
					$key = 0;
					?>
					@foreach ($data as $value)
						<?php 
							$subject = 'Liquiplanung: '.$value->name_of_property;
							$content = '';

							// if($value->id==3363)
							// 	continue;
						?>
						<tr>
							<td>{{ $key+1 }}</td>
							<?php
							$key += 1;
							?>		
							<td>
								<a href="{{route('properties.show',['property'=> $value->id])}}?tab=finance">{{$value->name_of_property}}</a>
							</td>
							<td>
								<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $value->id }}" data-subject="{{ $subject }}" data-content="{{ $content }}" data-title="LIQUIPLANUNG">{{ $value->am_name }}</a>
							</td>
							<td class="text-right">{{ show_number($value->nt, 2) }}</td>
							<td class="text-right">{{ show_number($value->nk, 2) }}</td>
							<td class="text-right">{{ show_number($value->object_effort_pm, 2) }}</td>
							<td class="text-right">{{ show_number($value->effort_pm, 2) }}</td>
							<td class="text-right">{{ show_number($value->extra_cost_pm, 2) }}</td>
							<td class="text-right">{{ show_number($value->loan_service_month, 2) }}</td>
							<td class="text-right">{{ show_number($value->current_balance, 2) }}</td>
							<td>{{ ($value->current_balance_updated) ? show_date_format($value->current_balance_updated) : '' }}</td>
							<td class="text-right">
								@if($value->cash_flow > 0 || $value->cash_flow == 0)
									<span style="color: green;">{{ show_number($value->cash_flow, 2) }}</span>
								@else
									<span style="color: red;">{{ show_number($value->cash_flow, 2) }}</span>
								@endif
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
			<tfoot>
				<tr>
					<th colspan="3" class="text-center">Gesamt</th>
					<th class="text-right">{{ show_number($total_nt, 2) }}</th>
					<th class="text-right">{{ show_number($total_nk, 2) }}</th>
					<th class="text-right">{{ show_number($total_object_effort_pm, 2) }}</th>
					<th class="text-right">{{ show_number($total_effort_pm, 2) }}</th>
					<th class="text-right">{{ show_number($total_extra_cost_pm, 2) }}</th>
					<th class="text-right">{{ show_number($total_loan_service_month, 2) }}</th>
					<th class="text-right">{{ show_number($total_current_balance, 2) }}</th>
					<th></th>
					<th class="text-right">
						@if($cash_flow_total > 0 || $cash_flow_total == 0)
							<span style="color: green;">{{ show_number($cash_flow_total, 2) }}</span>
						@else
							<span style="color: red;">{{ show_number($cash_flow_total, 2) }}</span>
						@endif
					</th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>