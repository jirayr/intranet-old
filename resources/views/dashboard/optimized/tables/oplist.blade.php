<div class="row" id="default_payer">
	<div class="col-md-12">
		<?php
			$year = range(2018,date('Y'));
			$months = range(1,12);
       	?>
       	<select class="oplist-month oplistchange-select">
          	@foreach($months as $list)
             	<option @if($list == $r_month) selected @endif value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
          	@endforeach
       	</select>
           <!-- {{__('dashboard.'.date('F'))}} -->
       	<?php
       		$year = range(2020,date('Y')+5);
       		$user = Auth::user();
       
	   		// $modal = new PropertiesMailLog;
	     	//    $modal->property_id = $contract->property_id;
	     	//    $modal->record_id = $request->id;
	     	//    $modal->type = 'contract_notrelease_am';
	     	//    $modal->tab = 'contracts';
	     	//    if($request->comment)
	     	//          $modal->comment = $request->comment;
	     	//    $modal->user_id = Auth::user()->id;
	     	//    $modal->save();
       ?>
       	<select class="oplist-year oplistchange-select">
          	@foreach($year as $list)
             	<option @if($list == $r_year) selected @endif value="{{$list}}">{{$list}}</option>
          	@endforeach
       	</select>
	</div>
</div>
<br>
<table id="oplist-table" class="table table-striped">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>AM</th>
			<th>User</th>
			<th>Hausverwaltung</th>
			<th>Datum</th>
			<th>Excel</th>
			<th>Mietvertrag</th>
			<th>Kommentar</th>
			<th>Type</th>
            <th>Mieter</th>
            <th>Records</th>
            <th>Buchungstext</th>
            <th class="sum">SOLL</th>
            <th class="sum">IST</th>
            <th>DTA</th>
            <th class="sum">Saldo</th>
            <th>Zahlungdatum</th>
            <th>Datum</th>
            <th>Bemerkung FCR</th>
            <th>Bemerkung HV</th>
		</tr>
	</thead>
	<tbody>
		<?php $count = $total_soll = $total_ist = $total_diff = 0; ?>
		@if (!empty($data))
			@foreach ($data as $element)
				<?php
					$download_path = "";
					if($element->invoice){
						$download_path = "https://drive.google.com/drive/u/2/folders/".$element->file_basename;
		                if($element->file_type == "file"){
		                    $download_path = "https://drive.google.com/file/d/".$element->file_basename;
		                }
		                $download_path = '<a  target="_blank"  title="'.$element->invoice.'"  href="'.$download_path.'">'.$element->invoice.'</a>';
		           	}

		           	$subject = 'OP Übersicht: '.$element->name_of_property;
		           	$content = "";


					$taburl = route('properties.show',['property' => $element->id]).'?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid';
        			$taburl = "<a href='".$taburl."'>".$taburl."</a>";
				?>
				<tr>
					<td>
						<a href="{{route('properties.show',['property'=> $element->id])}}?tab=tenancy-schedule&selecting_tenancy_schedule=rent_paid">
                            {{$element->name_of_property}}
                        </a>
					</td>
					<td>
						<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $element->id }}" data-subject="{{ $subject }}"  data-title="OFFENE POSTEN" data-id="{{ $element->pdp_id }}" data-section="properties_default_payers" >{{ $element->name }}</a>
					</td>
					<td>
						<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $element->id }}" data-user-id="{{ $element->up_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="OFFENE POSTEN" data-id="{{ (isset($element->item->id)) ? $element->item->id : '' }}" data-section="op_overview">{{ $element->up_name }}</a>
					</td>
					<th>{{ $element->hausmaxx }}</th>
					<td>{{show_datetime_format($element->created_at)}}</td>
					</td>
					<td>{!! $download_path !!}</td>
					<td>
						@if(isset($element->mietvertrag) && count($element->mietvertrag))
							@foreach($element->mietvertrag as $file)
                                @if($file->type == 'file')
                          			&nbsp;&nbsp;<a href="{{ $file->file_href }}"  id="mieterliste-gdrive-link-{{$file->id}}"  target="_blank" title="{{ $file->file_name }}"><i  class="fa {{ config('filemanager.file_icon_array.' . $file->extension) ?: 'fa-file' }}" ></i></a>
                                @else
                                    <a href="javascript:void(0);" title="{{ $file->file_name }}" onClick="loadDirectoryFiles('{{ $file->file_path }}');"  ><i class="fa fa-folder" ></i></a>
                                @endif
	                       @endforeach
	                    @endif
					</td>
					<td>
						
						{{-- @if($element->pdp_id)
						
						@php
							$comment = DB::table('properties_comments as pc')->selectRaw('pc.*, u.name, u.role, u.company')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.property_id', $element->id)->where('pc.type', 'properties_default_payers')->orderBy('pc.created_at', 'desc')->first();
						@endphp

						@if($comment)
							@php
								$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
								$commented_user = $comment->name.''.$company;
							@endphp
							<p class="long-text" style="margin-bottom: 0px;">
								<span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
							</p>
						@endif
						
						<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="{{$element->pdp_id}}" data-property-id="{{ $element->id }}" data-type="properties_default_payers" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
						@endif --}}

						@if(isset($element->item) && $element->item->id)
							@php
								$comment = DB::table('tenancy_schedule_comments as tc')->selectRaw('tc.*, u.name, u.role, u.company')->leftjoin('users as u', 'u.id', '=', 'tc.user_id')->where('tc.item_id', $element->item->id)->whereNotNull('tc.comment')->where('tc.comment', '!=', '')->orderBy('tc.created_at', 'desc')->first();
							@endphp
							@if($comment)
		                         @php
		                            $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
		                            $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
		                         @endphp
		                         <p class="long-text" style="margin-bottom: 0px;">
		                            <span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
		                         </p>
		                    @endif
		                    <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $element->item->id }}" data-type="0" data-property-id="{{ $element->item->property_id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
						@endif
							
					</td>

					<?php
                        $subject = 'OPOS '.$element->mieter.' '.$element->name_of_property;
                        $content = '';

                        $total_ist +=$element->ist;
                        $total_soll +=$element->soll;
                        $total_diff +=$element->diff;
                    ?>

                    <td>{{ $element->type }}</td>
                    <td>{{ $element->mieter }}</td>
                    <td class="text-left">{{$element->sv }}</td>
                    <td>{{ $element->buchungs_text }}</td>
                    <td class="text-right">{{ number_format( $element->soll, 2 ,",",".") }}€</td>
                    <td class="text-right">{{ number_format( $element->ist, 2 ,",",".") }}€</td>
                    <td class="text-right">{{$element->dta}}</td>
                    <td class="text-right">{{ number_format( $element->diff, 2 ,",",".") }}€</td>
                    <td class="text-right">{{ show_date_format($element->zahlungsdatum) }}</td>
                    <td class="text-right">{{ show_date_format($element->datum) }}</td>
                    <td>{{ $element->bemerkung_fcr }}</td>
                    <td>{{ $element->bemerkung_hv }}</td>
				</tr>
			@endforeach
		@endif
	</tbody>
	<tfoot>
		<tr>
		<th colspan="12">Gesamt</th>	
		<th class="show-searched-sum">{{show_number($total_soll,2)}}€</th>
		<th class="show-searched-sum">{{show_number($total_ist,2)}}€</th>
		<th></th>
		<th class="show-searched-sum">{{show_number($total_diff,2)}}€</th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		</tr>
	</tfoot>
</table>

