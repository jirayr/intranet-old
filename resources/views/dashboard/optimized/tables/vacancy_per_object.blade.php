<div class="table-responsive">
   <table id="vacancy-per-object-table" class="table table-striped">
      <thead>
         <tr>
            <th>Objekt</th>
            <th>AM</th>
            <th class="text-right">Anzahl</th>
            <th class="text-right">Fläche (m2)</th>
            <th class="text-right">Quote</th>
            <th class="text-right">Leerstand (€)</th>
            <th class="text-right">Miete Netto p.a.</th>
            <th class="text-right">Anteil Leerstand € in %</th>
         </tr>
      </thead>
      <?php
         $v1 = $v2 = $v3 = $v4 = $v5 = $flat_in_qm = $vacancy = 0;
      ?>
      <tbody>
         @if(!empty($tenancy_items_34_total))
            @foreach($tenancy_items_34_total as $property_id=>$tenancy_item)
               <?php
                  $v1 += $tenancy_item['count'];
                  $v2 += $tenancy_item['amount'];
                  $v3 += $tenancy_item['space'];
                  $v4 += (12 * $tenancy_item['actual_net_rent']);

                  $flat_in_qm += $tenancy_item['flat_in_qm'];
                  $vacancy += $tenancy_item['vacancy'];

                  if( $flat_in_qm != 0 )
                     $v5 = $vacancy / $flat_in_qm * 100;
                  else
                     $v5 = 0;
               ?>
               <tr>
                  <td>
                     <a  href="{{route('properties.show',['property'=>$property_id])}}?tab=tenancy-schedule">
                     {{$tenancy_item['name']}}</a>
                  </td>
                  <?php
                  $subject = 'LEERSTÄNDE PRO OBJEKT: '.$tenancy_item['name'];
                  $content = '';
               ?>
                  <td>
                     <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $property_id }}" data-subject="{{ $subject }}" data-content="{{ $content }}" data-title="LEERSTÄNDE PRO OBJEKT">{{ $tenancy_item['creator_name'] }}</a>
                  </td>
                  <td class="number-right">
                     <a href="javacript:void(0)" class="get-vacancy-list" data-id="{{$property_id}}">{{number_format($tenancy_item['count'],0,",",".")}}</a></td>
                  <td class="number-right">{{number_format($tenancy_item['space'],2,",",".")}}</td>
                  <td class="text-right">
                     {{ show_number($tenancy_item['quote'],2)}}%
                  </td>
                  <td class="number-right">{{number_format($tenancy_item['amount'],2,",",".")}}</td>
                  <td class="text-right">
                     {{ number_format(12*$tenancy_item['actual_net_rent'], 2,",",".") }}
                  </td>
                  <td class="text-right">
                     @if($tenancy_item['actual_net_rent'])
                     {{ number_format($tenancy_item['amount']*100/(12*$tenancy_item['actual_net_rent']), 2,",",".") }}%
                     @else
                     {{'0,00'}}%
                     @endif
                  </td>
                  
               </tr>
            @endforeach
         @endif
      </tbody>
      <tfoot>
         <tr>
            <th colspan="2">Gesamt</th>
            <th class="text-right">{{number_format($v1,0,",",".")}}</th>
            <th class="text-right">{{number_format($v3,2,",",".")}}</th>
            <th class="text-right">{{number_format($v5,2,",",".")}}%</th>
            <th class="text-right">{{number_format($v2,2,",",".")}}</th>
            <th class="text-right">{{ number_format($v4, 2,",",".") }}</th>
            <th></th>
         </tr>
      </tfoot>
   </table>
</div>