<a href="{{route('get_rental_overview')}}?export=1" class="btn btn-success">Export</a>
<div class="table-responsive">
	<div class="property-table">
        <table class="table table-striped dashboard-table" id="rental-overview-table">
            <thead>
                <tr>
                    <th>Kategorie</th>
                    <th>Miete netto p.m.</th>
                    <th>Miete netto p.a.</th>
                    <th>Anzahl</th>
                    <th>Geschl.</th>
                    <th>Geschl. in %</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $t1 = $t2 = $t3 = $t4 = 0;
                    $array = array(1,2);
                ?>
                @foreach($data as $key=>$row)
                    <?php
                        $t1 +=$row;
                        $t2 +=$data1[$key];
                        $t3 += ($row*12);
                    ?>
                    <tr>
                        <td>
                        <a href="javascript:void(0)" class="get-category-properties" data-type="0" data-category="{{$key}}">{{$key}}</a></td>
                        <td class="text-right">@if($row){{ number_format($row, 2,",",".") }}@else{{'0,00'}}@endif</td>
                        <td class="text-right">@if($row){{ number_format($row*12, 2,",",".") }}@else{{'0,00'}}@endif</td>
                        <td class="text-right"><a href="javascript:void(0)" class="get-category-properties" data-category="{{$key}}" data-type="0" data-rent-closed="0">{{ number_format($data1[$key], 0,",",".") }}</a></td>
                        <td class="text-right"><a href="javascript:void(0)" class="get-category-properties" data-category="{{$key}}" data-type="0" data-rent-closed="1">@if(isset($data2[$key])){{ show_number($data2[$key]) }}
                        <?php
                        $t4 += $data2[$key]; 
                        ?>
                        @else{{'0'}}@endif</a></td>
                        <td class="text-right">@if(isset($data2[$key]) && $data1[$key]){{ show_number($data2[$key]*100/$data1[$key]) }}@else{{'0,00'}}@endif</td>
                        
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                <th>Gesamt</th>
                <th class="ajax-total-category text-right">{{ number_format($t1, 2,",",".") }}</th>
                <th class="ajax-total-category text-right">{{ number_format($t3, 2,",",".") }}</th>
                <th class="text-right">{{ number_format($t2, 0,",",".") }}</th>
                <th class="text-right">{{ number_format($t4, 0,",",".") }}</th>
                <th class="text-right"></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
