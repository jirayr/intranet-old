<div class="table-responsive">
   <table class="table table-striped" id="expiring-liability-insurance-table">
      <thead>
         <tr>
            <th>Objekt</th>
            <th>AM</th>
            <th>Name</th>
            <th>Kommentare</th>
            <th>Betrag</th>
            <th>Laufzeit</th>
            <th>Kündigungsfrist</th>
            <th>Anzahl</th>
         </tr>
      </thead>
      <tbody>
         @if($data)
            @foreach($data as $list)
               <tr>
                  <td><a href="{{route('properties.show',['property'=>$list->id])}}?tab=insurance_tab">
                     {{$list->name_of_property}}</a>
                  </td>
                  <?php
                  $subject = 'AUSLAUFENDE HAFTPFLICHTVERSICHERUNGEN: '.$list->name_of_property;
                  $content = 'Name: '.$list->allianz;
               ?>
                  <td>
                     <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $list->id }}" data-subject="{{ $subject }}" data-content="{{ $content }}" data-title="AUSLAUFENDE HAFTPFLICHTVERSICHERUNGEN">{{ $list->asset_manager }}</a>
                  </td>
                  <td>{{$list->allianz}}</td>
                  <td>{{$list->haftplicht_comment}}</td>
                  <td>    
                     @if(is_numeric($list->haftplicht_betrag))
                     {{number_format($list->haftplicht_betrag,2,",",".")}}
                     €
                     @else
                     {{$list->haftplicht_betrag}}
                     @endif
                  </td>
                  <td>
                     @if($list->haftplicht_laufzeit_to)
                     {{date_format(  date_create(str_replace('.', '-', $list->haftplicht_laufzeit_to)) , 'd.m.Y')}}
                     @else
                     {{$list->haftplicht_laufzeit_to}}
                     @endif
                  </td>
                  <td>{{$list->haftplicht_kundigungsfrist}}</td>
                  <td>
                     <a href="{{route('properties.show',['property'=>$list->id])}}?tab=test_tab">
                     {{$list->insurances($list,2)}}</a>
                  </td>
               </tr>
            @endforeach
         @endif
      </tbody>
   </table>
</div>