<div class="table-responsive">
        <table id="table-utility-prices" class="table table-striped">
        	<thead>
                     	<tr>
                	        <th>#</th>
                	        <th>Objekt</th>
                                <th>AM</th>
                	        <th>Stromanbieter</th>
                	        <th>Strompreis</th>
                	        <th>Ölanbieter</th>
                	        <th>Ölpreis</th>
                	        <th>Gasanbieter</th>
                	        <th>Gaspreis</th>
                	        <th>Wärmeanbieter</th>
                	        <th>Wärmepreis</th>
                	        <th>Kälteanbieter</th>
                	        <th>Kältepreis</th>
                     	</tr>
          	</thead>
        	<tbody>
        		@if($properties)
        			@foreach ($properties as $key => $property)
        				<tr>
        					<td>{{ $key+1 }}</td>
        					<td>
                        				<a href="{{route('properties.show',['property'=> $property->id])}}?tab=schl-template">{{$property->name_of_property}}</a>
                         			</td>

                                                <td>
                                                        <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $property->id }}">{{ $property->assetmanager }}</a>
                                                </td>

                         			<td>
                         				{{ (isset($custom_fields_schl_array[$property->id]['electricity_provider'])) ? $custom_fields_schl_array[$property->id]['electricity_provider'] : '' }}
                         			</td>
                         			<td class="text-right">
                         				{{ (isset($custom_fields_schl_array[$property->id]['electricity_provider_price'])) ? show_number($custom_fields_schl_array[$property->id]['electricity_provider_price'], 2) : '' }}
                         			</td>

                         			<td>
                         				{{ (isset($custom_fields_schl_array[$property->id]['oil_provider'])) ? $custom_fields_schl_array[$property->id]['oil_provider'] : '' }}
                         			</td>
                         			<td class="text-right">
                         				{{ (isset($custom_fields_schl_array[$property->id]['oil_provider_price'])) ? show_number($custom_fields_schl_array[$property->id]['oil_provider_price'], 2) : '' }}
                         			</td>

                         			<td>
                         				{{ (isset($custom_fields_schl_array[$property->id]['gas_provider'])) ? $custom_fields_schl_array[$property->id]['gas_provider'] : '' }}
                         			</td>
                         			<td class="text-right">
                         				{{ (isset($custom_fields_schl_array[$property->id]['gas_provider_price'])) ? show_number($custom_fields_schl_array[$property->id]['gas_provider_price'], 2) : '' }}
                         			</td>

                         			<td>
                         				{{ (isset($custom_fields_schl_array[$property->id]['heat_provider'])) ? $custom_fields_schl_array[$property->id]['heat_provider'] : '' }}
                         			</td>
                         			<td class="text-right">
                         				{{ (isset($custom_fields_schl_array[$property->id]['heat_provider_price'])) ? show_number($custom_fields_schl_array[$property->id]['heat_provider_price'], 2) : '' }}
                         			</td>

                         			<td>
                         				{{ (isset($custom_fields_schl_array[$property->id]['refrigeration_provider'])) ? $custom_fields_schl_array[$property->id]['refrigeration_provider'] : '' }}
                         			</td>
                         			<td class="text-right">
                         				{{ (isset($custom_fields_schl_array[$property->id]['refrigeration_provider_price'])) ? show_number($custom_fields_schl_array[$property->id]['refrigeration_provider_price'], 2) : '' }}
                         			</td>

        				</tr>
        			@endforeach
        		@endif
        	</tbody>
        </table>
</div>