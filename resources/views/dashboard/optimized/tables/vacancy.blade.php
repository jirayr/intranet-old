<div class="row">

   <div class="col-sm-2 form-group">
      <label>Fläche von:</label>
      <input type="text" class="form-control" id="vacancy_start" value="{{ $start }}">
   </div>

   <div class="col-sm-2 form-group">
      <label>Fläche bis:</label>
      <input type="text" class="form-control" id="vacancy_end" value="{{ $end }}">
   </div>

   <div class="col-sm-2" style="padding-top: 25px;">
      <button type="btn" class="btn btn-success" id="vacancy_search">Filter nach Fläche</button>
   </div>

   <div class="col-sm-6">
      <button type="button" class="btn btn-primary pull-right" id="btn-export-vacancy" data-url="{{ route('vacancy_export') }}">Mail mit Exposés</button>
   </div>

</div>

<div class="row">
   <div class="col-md-12 table-responsive">
      <table id="tenant-table4" class="table table-striped">
         <thead>
            <tr>
               <th>
                  <input type="checkbox" class="vacancy_check_all">
               </th>
               <th>#</th>
               <th>Asset Manager</th>
               <th>Objekt</th>
               <th>Leerstand</th>
               <th>Fläche (m2)</th>
               <th>Leerstand (€)</th>
               <th>Typ</th>
               <th>Exposé</th>
               <th>Kommentare</th>
            </tr>
         </thead>
         <tbody>
            @if ($data)
               @foreach($data as $tenancy_item)
                  <?php

                     $comment = DB::table('tenancy_schedule_comments as tc')->selectRaw('tc.*, u.name, u.role, u.company')->leftjoin('users as u', 'u.id', '=', 'tc.user_id')->where('tc.item_id', $tenancy_item->id)->whereNotNull('tc.comment')->where('tc.comment', '!=', '')->orderBy('tc.created_at', 'desc')->first();

                     // if(!$tenancy_item->vacancy_in_eur)
                     $tenancy_item->vacancy_in_eur = $tenancy_item->calculated_vacancy_in_eur;
                     //$comment = ($tenancy_item->comment) ? $tenancy_item->comment : $tenancy_item->comment1;

                     $content = '';
                     $subject = 'Leerstände: '.$tenancy_item->object_name;
                     if($tenancy_item->name){
                        $content .= 'Leerstand: '.$tenancy_item->name;
                     }
                     if(isset($comment->comment) && $comment->comment){
                        $content .= '<br/>Kommentar User: '.$comment->comment;
                     }

                     $taburl = route('properties.show',['property' => $tenancy_item->property_id]).'?tab=tenancy-schedule';
                     $taburl = "<a href='".$taburl."'>".$taburl."</a>";
                  ?>
                  <tr>
                     <td>
                        <input type="checkbox" class="vacancy_check" value="{{ $tenancy_item->id }}">
                     </td>
                     <td>{{$tenancy_item->id}}</td>
                     <td>
                        <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $tenancy_item->property_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="LEERSTÄNDE">{{ $tenancy_item->creator_name }}</a>
                     </td>
                     <td>
                        <a  href="{{route('properties.show',['property'=>$tenancy_item->property_id])}}?tab=tenancy-schedule">
                        {{$tenancy_item->object_name}}</a>
                     </td>
                     <td>{{$tenancy_item->name}}</td>
                     <td class="number-right">{{$tenancy_item->vacancy_in_qm ? number_format($tenancy_item->vacancy_in_qm,2,",",".") : 0}}</td>
                     <td class="number-right">{{$tenancy_item->vacancy_in_eur ? number_format($tenancy_item->vacancy_in_eur,2,",",".") : 0}}</td>
                     <td>{{$tenancy_item->type}}</td>
                     <td>
                        @if($tenancy_item->value)
                        <a href="{{ asset('ad_files_upload/'.$tenancy_item->value) }}" target="_blank">Download</a>
                        @else
                        <?php
                           if(isset($attachments[$tenancy_item->id])){ ?>
                              <a href="{{ $attachments[$tenancy_item->id] }}" target="_blank">Download</a>
                        <?php } ?>
                        @endif
                     </td>
                     {{-- <td><span class="long-text">{!! $comment !!}</span></td> --}}
                     <td>
                        @if($comment)
                           @php
                              $company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                              $commented_user = ( ($comment->name) ? $comment->name : $comment->user_name ).''.$company;
                           @endphp
                           <p class="long-text" style="margin-bottom: 0px;">
                              <span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
                           </p>
                        @endif
                        <button type="button" class="btn btn-primary btn-xs btn-show-item-comment" data-id="{{ $tenancy_item->id }}" data-type="0" data-property-id="{{ $tenancy_item->property_id }}" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
                     </td>
                  </tr>
               @endforeach
            @endif
         </tbody>
      </table>
   </div>
</div>