<div class="row">
	<div class="col-md-12 table-responsive">
		<table id="property-mail-table" class="table table-striped">
			<thead>
		     	<tr>
			        <th>#</th>
			        <th>Objekt</th>
			        <th>From</th>
			        <th>Name</th>
			        <th>Nachricht</th>
			        <th>Kommentar</th>
			        <th>Datum/Uhrzeit</th>
			        <th>Offen</th>
		     	</tr>
		  	</thead>
			<tbody>
				@if($data)
					@foreach ($data as $key => $value)
						<tr>
							<td>{{ $key+1 }}</td>
							<td>
								<a href="{{route('properties.show',['property'=> $value->property_id])}}?tab=am_mail">{{$value->name_of_property}}</a>
							</td>
							<td>{{ $value->from_name }}</td>
							<td>
								<!--@if($value->type == 5)

									<?php 
										$email_arr = [];
										if($value->custom_email){
											$arr = explode(",", $value->custom_email);
											$arr = array_unique($arr);
											$email_arr = $arr;
										}
									?>
									@if(!empty($email_arr))
										<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $value->property_id }}" data-user-id="" data-subject="{{ $value->subject }}" data-content='{{ $value->message }}' data-title="{{ $value->mail_type }}" data-reload="1" data-email="{{ implode(",", $email_arr) }}">{{ implode(", ", $email_arr) }}</a>
									@endif

								@elseif($value->type == 6)

									<a href="javascript:void(0);" type="button" class="btn-forward-to" data-property-id="{{ $value->property_id }}" data-id="{{ $value->section_id }}" data-subject="{{ $value->subject }}" data-content="" data-title="{{ $value->mail_type }}" data-reload="1" data-email="{{ $value->custom_email }}" data-user="{{ $value->am_id }}">{{ $value->name }}{{ ($value->custom_email) ? ', '.$value->custom_email : '' }}</a>

								@else

									<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $value->property_id }}" data-user-id="{{ $value->am_id }}" data-subject="{{ $value->subject }}" data-content='{{ $value->message }}' data-title="{{ $value->mail_type }}" data-reload="1">{{ $value->name }}</a>

								@endif-->

								@if($value->type == 5)

									<?php 
										$email_arr = [];
										if($value->custom_email){
											$arr = explode(",", $value->custom_email);
											$arr = array_unique($arr);
											$email_arr = $arr;
										}
									?>
									@if(!empty($email_arr))
										{{ implode(", ", $email_arr) }}
									@endif

								@elseif($value->type == 6)

									{{ $value->name }}{{ ($value->custom_email) ? ', '.$value->custom_email : '' }}

								@else

									{{ $value->name }}

								@endif


							</td>
							<td>
								{!! str_replace($value->name_of_property,'',$value->subject) !!}<br>
								{!! $value->message !!}
							</td>
							<td>
								@php
									$comments = DB::table('properties_comments as pc')->select('pc.id','pc.comment', 'u.name', 'pc.created_at', 'u.role', 'u.company')
			                            ->join('users as u', 'u.id', 'pc.user_id')
			                            ->where('pc.record_id', $value->id)
			                            ->where('pc.type', 'send_mail_to_ams')
			                            ->orderBy('pc.created_at', 'desc')
			                            ->limit(2)->get();

			                        $taburl = route('properties.show',['property' => $value->property_id]).'?tab=am_mail';
        							$taburl = "<a href='".$taburl."'>".$taburl."</a>";
								@endphp
								@if($comments && count($comments))
									<div class="show_property_cmnt_section">
										@foreach ($comments as $comment)
											@php
												$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
                        						$commented_user = $comment->name.''.$company;
											@endphp
											<p><span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})</p>
										@endforeach
									</div>
									<a href="javascript:void(0);" data-url="{{ route('get_property_comment') }}?property_id={{ $value->property_id }}&record_id={{ $value->id }}&type=send_mail_to_ams" class="load_property_comment_section" data-closest="td">Show More</a>
								@endif

								<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="{{ $value->id }}" data-property-id="{{ $value->property_id }}" data-type="send_mail_to_ams" data-subject="{{ $value->subject }}" data-content="{{ $taburl }}">Kommentar</button>
							</td>
							<td>{{ show_datetime_format($value->created_at) }}</td>
							<td>
								<button type="button" class="btn btn-primary mail-mark-as-done" data-url="{{ route('mail_mark_as_done', ['id' => $value->id]) }}">Offen</button>
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>


