<table class="table table-striped" id="{{$table_id}}">
    <thead>
    <tr>
        <th>PLZ</th>
        <th>Ort</th>
        <th>AM</th>
        <th>TM</th>
        <!-- <th>Einkaufsanfrage (AR)</th> -->
        <th>Einkaufsentscheidung (AR)</th>
        <th>Liquiditätsentscheidung (AL)</th>
        <th>Transaktionsentscheidung (FR)</th>
        <th>Einkaufsdatenblatt (AR)</th>
        <th>Checkliste n. Einkauf (TM)</th>
    </tr>
	</thead>
	<?php 
	$c = 0;
	$email = Auth::user()->email;
	$f = $an = $al = 0;
	if($email=="a.raudies@fcr-immobilien.de") $an = 1;
	if($email==config('users.falk_email')) $f = 1;
	if($email=="a.lauterbach@fcr-immobilien.de") $al = 1;
	?>
	<tbody>
		@foreach($name as $k=>$list)
		<?php
			$subject = 'Einkaufsentscheidung : '.$list['name'];
			$content = '';
		?>
		<tr>
			<td><a href="{{route('properties.show',['property'=>$k])}}">{{$list['plz']}}</a></td>
			<td><a href="{{route('properties.show',['property'=>$k])}}">{{$list['ort']}}</a></td>
            <td>
				@if(isset($list['am']['id']) && $list['am']['id'])
					<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $list['am']['p_id'] }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="EINKAUFSFREIGABEN">{{ $list['am']['name'] }}</a>
				@endif
			</td>

			<td>
				@if(isset($list['tm']['id']) && $list['tm']['id'])
					<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $list['tm']['p_id'] }}" data-user-id="{{ $list['tm']['id'] }}" data-subject="{{ $subject }}" data-content="{{ $content }}" data-title="EINKAUFSFREIGABEN">{{ $list['tm']['name'] }}</a>
				@endif
			</td>
			<td class="text-center">
				@if(isset($list['btn1']))
				<i class="fa fa-check"></i> {{$list['btn1']}}
				@else
				<i class="fa fa-times"></i>
				@if($an)
				<br>
				<a href="javascript:void(0)" class="einkauf-release-proc btn btn-xs btn-primary" data-btn='btn1' data-property_id="{{$k}}"  >Freigeben</a>@endif
				@endif
			</td>
			<td class="text-center">
				@if(isset($list['btn2']))
				<i class="fa fa-check"></i> {{$list['btn2']}}
				@else
				<i class="fa fa-times"></i>
				@if($al)
				<br>
				<a href="javascript:void(0)" class="einkauf-release-proc btn btn-xs btn-primary" data-btn='btn2' data-property_id="{{$k}}"  >Freigeben</a>@endif
				@endif
			</td>
			<td class="text-center">
				@if(isset($list['btn4']))
				<i class="fa fa-check"></i> {{$list['btn4']}}
				@else
				<?php $c++;?>
				<i class="fa fa-times"></i>
				@if($f)
				<br>
				<a href="javascript:void(0)" class="einkauf-release btn btn-xs btn-primary" data-property_id="{{$k}}"  >Freigeben</a>@endif
				@endif
			</td>
			<td class="text-center">
				@if(isset($list['btn3']))
				<i class="fa fa-check"></i> {{$list['btn3']}}
				@else
				<i class="fa fa-times"></i>
				@if($an)
				<br>
				<a href="javascript:void(0)" class="einkauf-release-proc btn btn-xs btn-primary" data-btn='btn3' data-property_id="{{$k}}"  >Freigeben</a>@endif
				@endif
			</td>
			<td class="text-center">
				@if(isset($list['btn5']))
				<i class="fa fa-check"></i> {{$list['btn5']}}
				@else
				<i class="fa fa-times"></i>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<input type="hidden" class="ereleasecount" value="{{$c}}">