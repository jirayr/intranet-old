<div class="col-sm-12 table-responsive">
<table class="table table-striped" id="list-statusloi">
	<thead>
	<tr>
		<th>Objekt</th>
		<th>Transaction Manager</th>
        <th>Datum</th>
        <th>Versendet</th>
	</tr>
	</thead>
	<tbody>
		<?php $s = 0; ?>
		@foreach($data as $list)
		<tr>
			<?php
			$s +=1;
			?>
			<td><a href="{{url('/properties/'.$list->property_id.'?tab=email_template')}}">{{$list->name_of_property}}</a></td>
			<td>{{$list->name}}</td>
			<td>{{$list->date}}</td>
			<td class="text-center"><input data-column="status_sent" type="checkbox" class="checkbox-is-sent" data-id="{{$list->id}}" @if($list->status_sent) checked @endif></td>
		</tr>
		@endforeach
	</tbody>
</table>

</div>