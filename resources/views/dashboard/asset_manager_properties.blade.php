
                    <div class="table-responsive">
                        <table id="tenant-table_22" class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Asset Manager</th>
                                <th>Objekt</th>
                                <th>Vermietet</th>
                                <th>Abschluss MV</th>
                                <th>Mietbeginn</th>
                                <th>Mietende</th>
                                <th>Mietfläche (m2)</th>
                                <th>IST-Nettokaltmiete</th>
                                <th>Netto Miete p.a.</th>
                                <th>Mieteinnahmen</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tenancy_items_4 as $tenancy_item_33)
                                @if($tenancy_item_33->creator_name)
                                <?php 
                                    $subject = 'Neue mv: '.$tenancy_item_33->object_name;
                                    $content = 'Vermietet: '.$tenancy_item_33->name;
                                ?>
                                <tr>
                                    <td>{{$tenancy_item_33->id}}</td>
                                    <td>
                                        <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $tenancy_item_33->property_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="NEUE MV">{{ $tenancy_item_33->creator_name }}</a> MV
                                    </td>
                                    <td >
                                    <a href="{{route('properties.show',['property'=>$tenancy_item_33->property_id])}}">
                                    {{$tenancy_item_33->object_name}}
                                    </a>
                                    </td>
                                    <td>{{$tenancy_item_33->name}}</td>
                                    <td>{{ show_date_format($tenancy_item_33->assesment_date) }}</td>
                                    <td>{{ show_date_format($tenancy_item_33->rent_begin) }}</td>
                                    <td>{{ show_date_format($tenancy_item_33->rent_end) }}</td>
                                    <td class="number-right">{{$tenancy_item_33->rental_space ? number_format($tenancy_item_33->rental_space,2,",",".") : 0}}</td>
                                    <td class="number-right">{{$tenancy_item_33->actual_net_rent ? number_format($tenancy_item_33->actual_net_rent,2,",",".") : 0}}€</td>
                                    <td>{{ ($tenancy_item_33->actual_net_rent) ? number_format( ($tenancy_item_33->actual_net_rent * 12) ,2,",",".") : 0 }}</td>
                                    <td class="number-right">

                                    <?php

                                    // if ($item->type == config('tenancy_schedule.item_type.business'))

                                    $date1 = $tenancy_item_33->rent_begin;
                                    $date2 = $tenancy_item_33->rent_end;
                                    

                                    $date1 = date_create($date1);
                                    $date2 = date_create($date2);

                                    $diff = date_diff($date1,$date2);

                                    $diff =  $diff->days/365;

                                    echo number_format($tenancy_item_33->actual_net_rent * $diff *12 ,2,",",".").'€';




                                    

                                    /*$years = floor($diff / (365*60*60*24));
                                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                    $str = "";
                                    if($years)
                                        $str = "$years years";

                                    if($months){
                                        if($str)
                                            $str .= ", ";

                                        $str .= "$months months";

                                    }

                                    if($days){
                                        if($str)
                                            $str .= ", ";

                                        $str .= "$days days";

                                    }
                                    echo $str;*/

                                    ?>
                                    </td>
                                </tr>
                                @endif
                            @endforeach


                            @foreach($tenancy_items_41 as $tenancy_item_33)
                                <?php 
                                    $subject = 'Neue vl: '.$tenancy_item_33->object_name;
                                    $content = 'Vermietet: '.$tenancy_item_33->name;
                                ?>
                                <tr>
                                    <td>{{$tenancy_item_33->id}}</td>
                                    <td>
                                        <a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $tenancy_item_33->property_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="NEUE VL">{{ $tenancy_item_33->creator_name }}</a> VL
                                    </td>
                                    <td >
                                    <a href="{{route('properties.show',['property'=>$tenancy_item_33->property_id])}}">
                                    {{$tenancy_item_33->object_name}}
                                    </a>
                                    </td>
                                    <td>{{$tenancy_item_33->name}}</td>
                                    <td>{{ show_date_format($tenancy_item_33->assesment_date) }}</td>
                                    <td>{{ show_date_format($tenancy_item_33->rent_begin) }}</td>
                                    <td>{{ show_date_format($tenancy_item_33->rent_end) }}</td>
                                    <td class="number-right">{{$tenancy_item_33->rental_space ? number_format($tenancy_item_33->rental_space,2,",",".") : 0}}</td>
                                    <td class="number-right">{{$tenancy_item_33->actual_net_rent ? number_format($tenancy_item_33->actual_net_rent,2,",",".") : 0}}€</td>
                                    <td>{{ ($tenancy_item_33->actual_net_rent) ? number_format( ($tenancy_item_33->actual_net_rent * 12) ,2,",",".") : 0 }}</td>
                                    <td class="number-right">

                                    <?php

                                    // if ($item->type == config('tenancy_schedule.item_type.business'))

                                    $date1 = $tenancy_item_33->rent_begin;
                                    $date2 = $tenancy_item_33->rent_end;
                                    

                                    $date1 = date_create($date1);
                                    $date2 = date_create($date2);

                                    $diff = date_diff($date1,$date2);

                                    $diff =  $diff->days/365;

                                    echo number_format($tenancy_item_33->actual_net_rent * $diff *12 ,2,",",".").'€';




                                    

                                    /*$years = floor($diff / (365*60*60*24));
                                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                                    $str = "";
                                    if($years)
                                        $str = "$years years";

                                    if($months){
                                        if($str)
                                            $str .= ", ";

                                        $str .= "$months months";

                                    }

                                    if($days){
                                        if($str)
                                            $str .= ", ";

                                        $str .= "$days days";

                                    }
                                    echo $str;*/

                                    ?>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
