@extends('layouts.admin')
@section('css')
<?php
   function short_name($string)
   {
       if($string){
           $arr = explode(" ", $string);
           if(count($arr)>1)
               return substr($arr[0], 0,1).substr($arr[1], 0,1);
           else
               return substr($string, 0,1);
       }
       return $string;
   }
?>
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<!-- Styles -->
<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
<style type="">
   #added-month-asset tbody th:first-child{
   cursor: pointer;
   }
   .insurance_tab_div table .btn{
      display: none;
   }
   @media (min-width: 992px)
   {    .modal-lg {
   width: 1320px;
   }
   }
   #vlog-table2 .fa-check,
   #vlog-table .fa-check{
   color: green;
   }
   #vlog-table2 .fa-times,
   #vlog-table .fa-times{
   color: red;
   }
   @media (min-width: 768px)
   {
   .modal-dialog.custom {
   width: 848px;
   margin: 30px auto;
   }
   }
   .list-data-banks a{
   pointer-events: none;
   color: #797979 !important;
   }
   .bank-file-upload input{
   display: none;
   }
   .bank-file-upload a{
   pointer-events:auto;   
   color: #797979 !important;
   }
   .list-data-banks .btn{
   display: none;
   }
   ._51mz{
   /*display: none;*/
   }
   .card {
   position: relative;
   /*display: flex;*/
   flex-direction: column;
   min-width: 0;
   word-wrap: break-word;
   background-color: #fff;
   background-clip: border-box;
   border: 0 solid transparent;
   border-radius: 0;
   height: 115px !important;
   }
   .card-body {
   flex: 1 1 auto;
   padding: 8px 12px;
   }
   .card .card-title {
   position: relative;
   font-weight: 500;
   font-size: 16px;
   }
   .d-flex {
   display: flex!important;
   }
   .align-items-center {
   align-items: center!important;
   }
   .ml-auto, .mx-auto {
   margin-left: auto!important;
   }
   .custum-row .col-lg-3, .custum-row .col-md-3{
   padding: 0 5px;
   }
   .custum-row-2{
   margin-top: 25px;
   }
   .custum-row-2 .col-md-6{
   margin: 5px 0;
   padding: 0 5px;
   }
   .preloader{
   display: none;
   }
   .pb-3, .py-3{
   padding-bottom: 20px;
   margin-top: 10px;
   }
   .for-col-padding .col-md-6{
   padding: 0 5px;
   }
   .pointer-cursor{
   cursor: pointer;
   }
   .get-assetmanager-property,.get-manager-property{
   cursor: pointer;
   }
   .border-top-footer{
   border-top: 3px solid #666 !important;
   }
   .div-user-count{
   font-size: 16px !important;
   color: #313131;
   font-weight: 300 !important;
   }
   .get-verkauf-user,
   .get-vermietung-user{
   cursor: pointer;
   color: #23527c;
   }
   .col-md-3 .progress{
   display: none !important;
   }
   .dashboard-card-button .col-md-3{
    width: 20% !important;
   } 
   .blue-bg{
    background-color: #004f91;
   }
   .yellow-bg{
    background-color: #ffff99;
   }
   .blue-bg h2,
   .blue-bg h5{
    color: white;
   }
   .dashboard-card-button .col-md-3{
      margin-bottom: 12px;
   }
</style>
@endsection
@section('content')
@include('partials.flash')
<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
  <div class="tradingview-widget-container__widget"></div>
  <div class="tradingview-widget-copyright"><a href="https://de.tradingview.com/symbols/XETR-FC9/" rel="noopener" target="_blank"><span class="blue-text">FC9 Symbol Info</span></a>
  

  <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-symbol-info.js" async>
  {
  "symbol": "XETR:FC9",
  "width": 1500,
  "locale": "de_DE",
  "colorTheme": "light",
  "isTransparent": true
}
  </script>
</div>
</div>
<!-- TradingView Widget END -->
<div class="page-content container-fluid">
   <div class="row custum-row dashboard-card-button">
      <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body" onclick="moveTodiv('vobj')">
                     <h5 class="card-title text-uppercase">Zu verteilende Objekte </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 vobj-total">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body " onclick="moveTodiv('invoice')">
                     <h5 class="card-title text-uppercase">Rechnungsfreigaben</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 invoiceTotal">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body " onclick="moveTodiv('pending-invoice')">
                     <h5 class="card-title text-uppercase">PENDING RECHNUNGEN</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 invoice-count-2">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body " onclick="moveTodiv('notrelease-invoice')">
                     <h5 class="card-title text-uppercase">NICHT FREIGEGEBEN</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 invoice-count-1">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body " onclick="moveTodiv('contract')">
                     <h5 class="card-title text-uppercase">Vertragsfreigaben</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 contract-total">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body " onclick="moveTodiv('angebot-release-section')">
                     <h5 class="card-title text-uppercase">Angebotsfreigaben</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 angebot-release-count">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>

            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body " onclick="moveTodiv('recommendation-section')">
                     <h5 class="card-title text-uppercase">Vermietungsempfehlungen</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 recommendation-count">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            
            
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body" onclick="moveTodiv('gabuderelease')">
                     <h5 class="card-title text-uppercase">Freigabe Gebäudevers.</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 gebaTotal">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body " onclick="moveTodiv('haftrelease')">
                     <h5 class="card-title text-uppercase">Freigabe Haftpflichtvers.</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 haftTotal">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" >
                  <div class="card-body " onclick="moveTodiv('hausrelease')">
                     <h5 class="card-title text-uppercase">Freigabe Hausverwaltung</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 hausTotal">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            
            
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" onclick="moveTodiv('provision')">
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Provisionsfreigaben </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 provisionTotal">....
                        </h2>
                     </div>
                     <br>
                  </div>
               </div>
            </div>

            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" onclick="moveTodiv('erelease')">
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Einkaufsfreigaben </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 ereleaseTotal">....
                        </h2>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor blue-bg" onclick="moveTodiv('vrelease')">
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Verkaufsfreigaben </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 vreleaseTotal">....
                        </h2>
                     </div>
                     <br>
                  </div>
               </div>
            </div>


            <div class="col-md-3">
               <div class="card pointer-cursor" onclick="moveTodiv('angebot')">
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">IMMOBILIEN UNTER BEOABACHTUNG </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 angebotTotal">....
                        </h2>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            

            
            <div class="col-md-3">
               <div class="card pointer-cursor" onclick="moveTodiv('statusloi')">
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">AKTUELL LAUFENDE LOI´S (ANKAUFSGEBOTE)</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 loi-list-total">....
                        </h2>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor" onclick="moveTodiv('bank-list-to')">
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Finanzierungsanfragen </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 bank-list-to">....
                        </h2>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3" >
               <div class="card pointer-cursor"  onclick="moveTodiv('neue-div')">
                  <div class="card-body">
                     <h5 class="card-title text-uppercase">Neue MV</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 mv-count">...</h2>
                     </div>
                     
                  </div>
               </div>
            </div>
            
            <div class="col-md-3" >
               <div class="card pointer-cursor"  onclick="moveTodiv('neue-div')">
                  <div class="card-body">
                     <h5 class="card-title text-uppercase">Neue VL</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 mv-total-gesamt">...</h2>
                     </div>
                     
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor"   onclick="moveTodiv('vermietet')">
                  <div class="card-body">
                     <h5 class="card-title text-uppercase">DIFFERENZ MIETE (SOLL/IST) IN €</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7">{{ number_format($summ_differenz_miete_in_euro,2,",",".") }}</h2>
                     </div>
                     
                  </div>
               </div>
            </div>
            
            <div class="col-md-3">
               <div class="card pointer-cursor"  onclick="moveTodiv('vermietet')">
                  <div class="card-body">
                     <h5 class="card-title text-uppercase">Vermietet (%)</h5>
                     <div class="text-right">
                        <br>
                        <h2 class="mt-2 display-7 vermi_percent"><sup></sup>....</h2>
                     </div>
                     <div class="progress">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor" >
                  <div class="card-body " onclick="moveTodiv('categ')">
                     <h5 class="card-title text-uppercase">Mieterübersicht</h5>
                     <div class="text-right">
                        <br>
                        <h2 class="mt-2 display-7 categTotal">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            @php
         $previous_month = date('n-Y');
         $previous_month_arr = explode("-", $previous_month);
         $list_month = $previous_month_arr[0];
         $list_year = $previous_month_arr[1];
      @endphp
      <div class="col-md-3 load-table " data-table="property-default-payer-table" data-title="OFFENE POSTEN" data-url="{{ route('get_default_payers') }}?month={{$list_month}}&year={{$list_year}}&status=0">
         <div class="card pointer-cursor yellow-bg">
            <div class="card-body">
               <h5 class="card-title text-uppercase">OP fehlt</h5>
               <div class="text-right">
                  <h2 style="line-height: 23px;font-size: 20px;" class="mt-2 display-7 default-payer-total">....</h2>
               </div>
               <br>
            </div>
         </div>
      </div>
            
            <div class="col-md-3">
               <div class="card pointer-cursor" >
                  <div class="card-body " onclick="moveTodiv('bankenfinan')">
                     <h5 class="card-title text-uppercase">BANKENFINANZIERUNGEN</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 bankenfinanTotal">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>

            <div class="col-md-3">
               <div class="card pointer-cursor" >
                  <div class="card-body " onclick="moveTodiv('bankenfinan-pro')">
                     <h5 class="card-title text-uppercase">Finanzierungen pro Bank</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 bankenfinanTotal">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>

            <div class="col-md-3">
               <div class="card pointer-cursor" >
                  <div class="card-body " onclick="moveTodiv('geb')">
                     <h5 class="card-title text-uppercase">GEBÄUDE VERSICHERUNG </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 axa1Total">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor" onclick="moveTodiv('haft')">
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">HAFTPFLICHT VERSICHERUNG </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 axa0Total">....
                        </h2>
                     </div>
                     <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            
            <div class="col-md-3">
               <div class="card pointer-cursor" >
                  <div class="card-body" onclick="moveTodiv('hau')">
                     <h5 class="card-title text-uppercase">HAUSVERWALTUNG KOSTEN </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 HAUSVERWALTUNGTOTAL">....
                        </h2>
                     </div>
                     <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor" >
                  <div class="card-body" onclick="moveTodiv('mahn')">
                     <h5 class="card-title text-uppercase">Mahnungen (30 Tage)</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 mahnungen-30-days"></h2>
                     </div>
                     <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3" >
               <div class="card pointer-cursor" >
                  <div class="card-body" onclick="moveTodiv('aus')">
                     <h5 class="card-title text-uppercase">ABGELAUFENE MV</h5>
                     <div class="text-right">
                        <br>
                        <h2 class="mt-2 display-7 sumausTotal">....
                        </h2>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3" >
               <div class="card pointer-cursor" >
                  <div class="card-body " onclick="moveTodiv('leer')">
                     <h5 class="card-title text-uppercase">Leerstände</h5>
                     <div class="text-right">
                        <br>
                        <h2 class="mt-2 display-7 leeramount">{{count($tenancy_items_34)}}
                        </h2>
                     </div>
                     <div class="progress hidden">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3 " >
               <div class="card pointer-cursor yellow-bg" >
                  <div class="card-body " onclick="moveTodiv('rental_activity_div')">
                     <h5 class="card-title text-uppercase">Vermietungsaktivitäten</h5>
                     <div class="text-right">
                        <br>
                        <h2 class="mt-2 display-7 rental_activity_total"></h2>
                     </div>
                     <div class="progress hidden">
                        <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3" >
               <div class="card pointer-cursor" >
                  <div class="card-body " onclick="moveTodiv('auslm')">
                     <h5 class="card-title text-uppercase">AUSLAUFENDE MV (90 Tage) </h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 sumauTotal">....
                        </h2>
                     </div>
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3" >
               <div class="card" >
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Newsletteranmeldungen (gesamt) </h5>
                     <div class="text-right">
                        {{-- <span class="text-muted">Today's Income</span> --}}
                        <h2 class="mt-2 display-7 mcount">0
                        </h2>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3" >
               <div class="card" >
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Website FCR (30 Tage)</h5>
                     <div class="text-right">
                        <!-- <br> -->
                        <h2 class="mt-2 display-7" id="website-visitor">....</h2>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3" >
               <div class="card" >
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Facebook</h5>
                     <div class="text-right">
                        <br>
                        <h2 class="mt-2 display-7" id="facebook-likes">....</h2>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3" >
               <div class="card" >
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Instagram</h5>
                     <div class="text-right">
                        <br>
                        <h2 class="mt-2 display-7" id="instagram-followers">....</h2>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3" >
            <a class="btn btn-primary pull-right" href="https://login.sumax.de/user.html" target="_blank">SUMAX</a>
            </div>
            <div class="clearfix" style="margin-bottom: 10px;"></div>

         </div>
         <div class="clearfix"></div>
      <div class="row custum-row-2">
      <div class="col-md-6" style="padding: 0 15px;">
         <div class="row">
            <div class="clearfix"></div>
            <div class="col-md-6">
               <div class="card" style="height: 160px !important;">
                  <div class="card-body ">
                     <h5 class="card-title text-uppercase">Verkaufsportal</h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal">Anmeldungen  (ALLE) : {{ number_format($makler_userall) }}</h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal">Makler mit NDA (ALLE) : {{ number_format($activecountall) }}</h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal get-verkauf-user"  data-active="0">Anmeldungen  (7 Tage) : {{ number_format($makler_user) }}</h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal get-verkauf-user" data-active="1">Makler mit NDA (7 Tage) : {{ number_format($activecount) }}</h5>
                     <!-- <h5 class="div-user-count card-title text-uppercase font-normal" >Anzahl Makler mit Logo : {{ number_format($logo_users) }}</h5> -->
                     <!-- <h5 class="div-user-count card-title text-uppercase font-normal" >Makler mit NDA (7 Tage) : {{ number_format($download_users) }}</h5> -->
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="card" style="height: 160px !important;">
                  <div class="card-body">
                     <h5 class="card-title text-uppercase">Vermietungsportal</h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal">Anmeldungen  (ALLE) : {{ number_format($vmakler_userall) }}</h5>
                     <h5 class="div-user-count card-title text-uppercase font-normal">Aktiviert (ALLE) : {{ number_format($vactivecountall) }}</h5>
                     <h5 class="get-vermietung-user div-user-count card-title text-uppercase font-normal" data-active="0">Anmeldungen  (7 Tage) : {{ number_format($vmakler_user) }}</h5>
                     <h5 class="get-vermietung-user div-user-count card-title text-uppercase font-normal" data-active="1">Aktiviert (7 Tage) : {{ number_format($vactivecount) }}
                     </h5>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6">
         <div id="basic-bar" style="height:328px; padding: 17px 10px; background: white; margin-top: 5px;"></div>
      </div>
      <div class="clearfix"></div>
   </div>
   <div class="row">
      <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box">
            <h3 class="box-title m-b-0">Auswertungen Verkaufsportal</h3>
            <h4>Online Benutzer</h4>
            <div class="table-responsive ">
               <table class="table table-striped dashboard-table verkauf-table" >
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Aktiv</th>
                        <th>Mail senden</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($v_analytic['onlineuser']))
                     @foreach($v_analytic['onlineuser'] as $key => $value)
                     <tr @if($key >= 3) class="hide-records hidden" @endif>
                     <td>
                        {{ (isset($value['first_name'])) ? $value['first_name'] : '' }} {{ (isset($value['last_name'])) ? $value['last_name'] : '' }}
                     </td>
                     <td>
                        {{ ($value['last_active_time']) ? show_datetime_format($value['last_active_time']) : '' }}
                     </td>
                     <td>
                        @if(isset($value['email']) && $value['email'] != '')
                        <button type="button" class="btn btn-primary btn-xs send-mail" data-email="{{ $value['email'] }}">Mail senden</button>
                        @endif
                     </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
               <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
            </div>
            <h4>Am häufigsten angesehen</h4>
            <div class="table-responsive ">
               <table class="table table-striped dashboard-table verkauf-table">
                  <thead>
                     <tr>
                        <th>Anzeigen</th>
                        <th>Anzahl</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($v_analytic['most_viewed']))
                     @foreach($v_analytic['most_viewed'] as $k=>$list)
                     <tr @if($k>=3) class="hide-records hidden" @endif>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/ad/{{$list['slug']}}">{{$list['title']}}</a></td>
                     <td>{{$list['view']}}</td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
               <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
            </div>
            <h4>Am häufigsten verschickt (Exposé)</h4>
            <div class="table-responsive ">
               <table class="table table-striped dashboard-table verkauf-table" >
                  <thead>
                     <tr>
                        <th>Anzeigen</th>
                        <th>Anzahl</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($v_analytic['most_sent']))
                     @foreach($v_analytic['most_sent'] as $k=>$list)
                     <tr @if($k>=3) class="hide-records hidden" @endif>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/ad/{{$list['slug']}}">{{$list['title']}}</a></td>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/ad-activities/{{$list['ad_id']}}/8">{{$list['id']}}</a></td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
               <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
            </div>
            <h4>Makler mit Logo</h4>
            <div class="table-responsive ">
               <table class="table table-striped" >
                  <tbody>
                     <tr>
                        <th>Makler mit Logo</th>
                        <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agents">{{ number_format($logo_users) }}</a></td>
                     </tr>
                     <tr>
                        <th>Anzahl heruntergeladener Exposés mit Logo</th>
                        <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agents">{{ number_format($download_users) }}</a></td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box">
            <h4>letzte Aktivität</h4>
            <div class="table-responsive ">
               <table class="table table-striped dashboard-table verkauf-table" >
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>letzte Aktivität</th>
                        <th>Mail senden</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($v_analytic['last_active']))
                     @foreach($v_analytic['last_active'] as $key => $value)
                     <tr @if($key >= 3) class="hide-records hidden" @endif>
                     <td>
                        {{ (isset($value['first_name'])) ? $value['first_name'] : '' }} {{ (isset($value['last_name'])) ? $value['last_name'] : '' }}
                     </td>
                     <td>
                        {{ ($value['last_active_time']) ? show_datetime_format($value['last_active_time']) : '' }}
                     </td>
                     <td>
                        @if(isset($value['email']) && $value['email'] != '')
                        <button type="button" class="btn btn-primary btn-xs send-mail" data-email="{{ $value['email'] }}">Mail senden</button>
                        @endif
                     </td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
               <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
            </div>
            <h4>Am häufigsten heruntergeladen (Exposé)</h4>
            <div class="table-responsive ">
               <table class="table table-striped dashboard-table verkauf-table" >
                  <thead>
                     <tr>
                        <th>Anzeigen</th>
                        <th>Anzahl</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($v_analytic['most_downloaded']))
                     @foreach($v_analytic['most_downloaded'] as $k=>$list)
                     <tr @if($k>=3) class="hide-records hidden" @endif>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/ad/{{$list['slug']}}">{{$list['title']}}</a></td>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/ad-activities/{{$list['ad_id']}}/7">{{$list['id']}}</a></td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
               <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
            </div>
            <h4>Aktivster Makler</h4>
            <div class="table-responsive ">
               <table class="table table-striped dashboard-table verkauf-table" >
                  <thead>
                     <tr>
                        <th>Anzeigen</th>
                        <th>Anzahl</th>
                        <th>Am häufigsten heruntergeladen (Exposé)</th>
                        <th>Am häufigsten verschickt (Exposé)</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($v_analytic['most_active']))
                     @foreach($v_analytic['most_active'] as $k=>$list)
                     <tr @if($k>=3) class="hide-records hidden" @endif>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agent-activities/{{$list['user_id']}}">{{$list['first_name']}} {{$list['last_name']}}</a></td>
                     <td>{{$list['id']}}</td>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agent-activities/{{$list['user_id']}}/7">{{$list['downloaded']}}</a></td>
                     <td><a target="_blank" href="https://verkauf.fcr-immobilien.de/dashboard/agent-activities/{{$list['user_id']}}/8">{{$list['sent']}}</a></td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
               </table>
               <a href="javacript:void(0)" class="pull-right show-more">Show more</a>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   $year = range(2018,date('Y'));
   $months = range(1,12);
   ?>
<div class="row">
<div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
   <div class="white-box">
      <h3 class="box-title m-b-0" id="angebot">
         Neue Immobilien im Angebot 
         <select class="pm-month change-select">
            <option value="">All</option>
            @foreach($months as $list)
            <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
            @endforeach
         </select>
         <!-- {{__('dashboard.'.date('F'))}} -->
         <select class="pm-year change-select">
            <option value="">All</option>
            @foreach($year as $list)
            <option 
               value="{{$list}}">{{$list}}</option>
            @endforeach
         </select>
      </h3>
      <p class="text-muted m-b-30">&nbsp;</p>
      <div class="table-responsive manager-property-list">
      </div>
   </div>
</div>
<div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;" id="statusloi">
   <div class="white-box">
      <h3 class="box-title m-b-0">
         LOI
         <select  id="loi-month" onchange="refreshLoiTablebyMonth()">
            <option value="0" >all</option>
            @foreach($months as $list)
            <option value="{{$list}}" @if($list==\Carbon\Carbon::now()->month) selected @endif>{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
            @endforeach
         </select>
      </h3>
      <div class="table-responsive LOI-list">
      </div>
   </div>
</div>
<div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;" id="bank-list-to">
   <div class="white-box" id="">
      <h3 class="box-title m-b-0">Finanzierungsanfragen</h3>
      <p class="text-muted m-b-30">&nbsp;</p>
      <div class="table-responsive">
         <table id="banken" class="table table-striped">
            <thead>
               <tr>
                  <th>#</th>
                  <th>Objekt</th>
                  <th>Anzahl</th>
               </tr>
            </thead>
            <tbody>
               <?php 
                  $bank_sum_to  = 0;
                  ?>
               @foreach($bankens as $k=>$list)
               <tr>
                  <td>{{$k+1}}</td>
                  <td><a href="{{route('properties.show',['property'=>$list->main_property_id])}}">
                     {{$list->name_of_property}}</a>
                  </td>
                  <td class="text-right"><a href="javacript:void(0)" class="get-bank-list" data-id="{{$list->main_property_id}}"> {{$list->id}}</a></td>
                  <?php
                     $bank_sum_to +=$list->id;
                     ?>
               </tr>
               @endforeach
            </tbody>
         </table>
         <input type="hidden" class="sum-bank-list-to" value="{{$bank_sum_to}}">
      </div>
   </div>
</div>
<div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
   <div class="white-box" id="erelease">
      <h3 class="box-title m-b-0">Einkaufsfreigaben</h3>
      <p class="text-muted m-b-30">&nbsp;</p>
      <div class="table-responsive vlog">
      </div>
   </div>
   <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
      <div class="white-box" id="vrelease">
         <h3 class="box-title m-b-0">Verkaufsfreigaben</h3>
         <p class="text-muted m-b-30">&nbsp;</p>
         <div class="table-responsive vklog">
         </div>
      </div>
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box" id="vobj">
            <h3 class="box-title m-b-0">Zu verteilende Objekte</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive table-not-beur-div">
            </div>
         </div>
      </div>
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" id="neue-div">
         <div class="white-box">
            <h3 class="box-title m-b-0">Vermietungsaktivitäten</h3>
            <h3 class="box-title m-b-0">
               Neue 
               <select class="type-selection achange-select">
                  <option value="0">All</option>
                  <option value="1">Mietverträge</option>
                  <option value="2">Verlängerung </option>
               </select>
               <select class="pa-month achange-select">
                  <option value="">All</option>
                  @foreach($months as $list)
                  <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                  @endforeach
               </select>
               <!-- {{__('dashboard.'.date('F'))}} -->
               <select class="pa-year achange-select">
               @foreach($year as $list)
               <option 
               @if($list==date('Y')) selected @endif 
               value="{{$list}}">{{$list}}</option>
               @endforeach
               </select>
               <a href="{{route('monthyearassetmanager')}}?export=1&month=&year={{date('Y')}}" class="pull-right btn btn-success">Export</a>
            </h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive asset-manager-property-list">
            </div>
            <!-- <div class="table-responsive ">
               </div> -->
         </div>
      </div>
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" id="vermietet">
         <div class="white-box">
            <h3 class="box-title m-b-0">Bestandsobjekte</h3>
            <div class="table-responsive assetmanagement-list">
            </div>
         </div>
      </div>
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" >
         <div class="white-box">
            <h3 class="box-title m-b-0">Rent Compare</h3>
            <select class="pp-month2">
                @foreach($months as $list)
                <option value="{{$list}}" @if($list==date('m')) selected="selected" @endif>{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                @endforeach
            </select>

            <button class="btn btn-primary hide-same-rent">nur neue und ausgelaufene MV anzeigen</button>
               
            <div class="table-responsive rent-comapare-list">
            </div>
         </div>
      </div>
      <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box" id="categ">
            <h3 class="box-title m-b-0">Mieterübersicht</h3>
            <h3 class="box-title m-b-0"><a href="{{route('getcategoryproperty')}}?export=1" class="pull-right btn btn-success">Export</a></h3>
            <div class="clearfix"></div>
            <div class="table-responsive category-prop-list2">
            </div>
         </div>
      </div>
      <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box">
            <h3 class="box-title m-b-0">Mieterübersicht</h3>
            <div id="high-container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box">
            <h3 class="box-title m-b-0">Mieterübersicht (Corona)</h3>
            <div class="clearfix"></div>
            <div class="table-responsive category-prop-list4">
            </div>
         </div>
      </div>
      <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box">
            <h3 class="box-title m-b-0">Mieterübersicht (Corona)</h3>
            <div class="clearfix"></div>
            <div class="table-responsive category-prop-list5">
            </div>
         </div>
      </div>
      <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box">
            <h3 class="box-title m-b-0">Mieterübersicht gesamt</h3>
            <div class="clearfix"></div>
            <div class="table-responsive category-prop-list6">
            </div>
         </div>
      </div>
      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="invoice">
            <h3 class="box-title m-b-0">Rechnungsfreigaben</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive property-invoice-div">
            </div>
         </div>
      </div>
      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="pending-invoice">
            <h3 class="box-title m-b-0">Pending Rechnungen</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive property-pending-invoice-div">
            </div>
         </div>
      </div>
      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="notrelease-invoice">
            <h3 class="box-title m-b-0">Nicht Freigegeben</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive property-notrelease-invoice-div">
            </div>
         </div>
      </div>

      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="contract">
            <h3 class="box-title m-b-0">Vertragsfreigaben</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive">
               <table class="table table-striped" id="contracts_table" style="width: 100%;padding-left: 0px;">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Datei</th>
                        <th>Betrag</th>
                        <th>Startdatum</th>
                        <th>Kündigung spätestens</th>
                        <th>Kommentar</th>
                        <th>User</th>
                        <th>Datum</th>
                        <!-- <th>Kommentar Falk</th> -->
                        <th>Freigabe Falk</th>
                        <th>Ablehnen</th>
                        <th>Pending</th>
                        {{-- <th>Aktion</th> --}}
                     </tr>
                  </thead>
                  <tbody></tbody>
               </table>
            </div>
         </div>
      </div>

      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="angebot-release-section">
            <h3 class="box-title m-b-0">Angebotsfreigaben</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="insurance_tab_div">
            </div>
         </div>
      </div>

      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="recommendation-section">
            <h3 class="box-title m-b-0">Vermietungsempfehlungen</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="recommendation_div table-responsive">
            </div>
         </div>
      </div>

      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="provision">
            <h3 class="box-title m-b-0">Provisionsfreigaben</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive property-provision-div">
            </div>
         </div>
      </div>

      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
           <div class="white-box" id="provision-3">
              <h3 class="box-title m-b-0">Freigegebene Provisionen</h3>
               <h3 class="box-title m-b-0"> 
                   
               <select class="pp-month pchange-select">
                   <option value="">All</option>
                   @foreach($months as $list)
                   <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                   @endforeach
               </select>
                   <!-- {{__('dashboard.'.date('F'))}} -->
               <select class="pp-year pchange-select">
                   @foreach($year as $list)
                   <option 
                   @if($list==date('Y')) selected @endif 
                   value="{{$list}}">{{$list}}</option>
                   @endforeach
               </select>
           </h3>
              <p class="text-muted m-b-30">&nbsp;</p>
              <div class="table-responsive property-provision-div-4">
              </div>
           </div>
       </div>


       


      
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box" id="bankenfinan">
            <h3 class="box-title m-b-0">Bankenfinanzierungen</h3>
            <div class="table-responsive bank-prop-list">
            </div>
         </div>
      </div>

      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box" id="bankenfinan-pro">
            <h3 class="box-title m-b-0">Finanzierungen pro Bank</h3>
            <div class="table-responsive bankenfinan-prop-list">
            </div>
         </div>
      </div>

   </div>
   <div class="">
      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="gabuderelease">
            <h3 class="box-title m-b-0">Freigabe Gebäudeversicherung</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="insurance_type_1_div">
            </div>
         </div>
      </div>

      
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box " id="geb">
            <h3 class="box-title m-b-0">Gebäude Versicherung</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive insurance-div1">
            </div>
         </div>
      </div>
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box " id="geb2">
            <h3 class="box-title m-b-0">Auslaufende Gebäudeversicherungen</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive">
               <table class="table table-striped dashboard-table class-data-table">
                  <thead>
                     <tr>
                        <th>Objekt</th>
                        <th>AM</th>
                        <th>Name</th>
                        <th>Kommentare</th>
                        <th>Betrag</th>
                        <th>Laufzeit</th>
                        <th>Kündigungsfrist</th>
                        <th>Anzahl</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($g1 as $list)
                     <tr>
                        <td>
                           <a href="{{route('properties.show',['property'=>$list->id])}}?tab=insurance_tab">
                           {{$list->name_of_property}}</a>
                        </td>
                        <td>@if(isset($list->asset_manager))
                           {{$list->asset_manager->name}}
                           @endif
                        </td>
                        <td>{{$list->axa}}</td>
                        <td>{{$list->gebaude_comment}}</td>
                        <td>
                           @if(is_numeric($list->gebaude_betrag))
                           {{number_format($list->gebaude_betrag,2,",",".")}}
                           €
                           @else
                           {{$list->gebaude_betrag}}
                           @endif
                        </td>
                        <td>
                           @if($list->gebaude_laufzeit_to)
                           {{date_format(  date_create(str_replace('.', '-', $list->gebaude_laufzeit_to)) , 'd.m.Y')}}
                           @else
                           {{$list->gebaude_laufzeit_to}}
                           @endif
                        </td>
                        <td>{{$list->gebaude_kundigungsfrist}}</td>
                        <td>
                           <a href="{{route('properties.show',['property'=>$list->id])}}?tab=test_tab">
                           {{$list->insurances($list,1)}}</a>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="haftrelease">
            <h3 class="box-title m-b-0">Freigabe Haftpflichtversicherung</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="insurance_type_2_div">
            </div>
         </div>
      </div>

      



      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="hausrelease">
            <h3 class="box-title m-b-0">Freigabe Hausverwaltung</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="release-management-div">
            </div>
         </div>
      </div>
      
      
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box" id="haft">
            <h3 class="box-title m-b-0">HAFTPFLICHT VERSICHERUNG</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive insurance-div2">
            </div>
         </div>
      </div>
      <div class="col-sm-12" style="padding: 0 5px;">
         <div class="white-box" id="haft2">
            <h3 class="box-title m-b-0">Auslaufende Haftpflichtversicherungen</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive">
               <table class="table table-striped dashboard-table class-data-table">
                  <thead>
                     <tr>
                        <th>Objekt</th>
                        <th>AM</th>
                        <th>Name</th>
                        <th>Kommentare</th>
                        <th>Betrag</th>
                        <th>Laufzeit</th>
                        <th>Kündigungsfrist</th>
                        <th>Anzahl</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($g2 as $list)
                     <tr>
                        <td><a href="{{route('properties.show',['property'=>$list->id])}}?tab=insurance_tab">
                           {{$list->name_of_property}}</a>
                        </td>
                        <td>
                           @if(isset($list->asset_manager))
                           {{$list->asset_manager->name}}
                           @endif
                        </td>
                        <td>{{$list->allianz}}</td>
                        <td>{{$list->haftplicht_comment}}</td>
                        <td>    
                           @if(is_numeric($list->haftplicht_betrag))
                           {{number_format($list->haftplicht_betrag,2,",",".")}}
                           €
                           @else
                           {{$list->haftplicht_betrag}}
                           @endif
                        </td>
                        <td>
                           @if($list->haftplicht_laufzeit_to)
                           {{date_format(  date_create(str_replace('.', '-', $list->haftplicht_laufzeit_to)) , 'd.m.Y')}}
                           @else
                           {{$list->haftplicht_laufzeit_to}}
                           @endif
                        </td>
                        <td>{{$list->haftplicht_kundigungsfrist}}</td>
                        <td>
                           <a href="{{route('properties.show',['property'=>$list->id])}}?tab=test_tab">
                           {{$list->insurances($list,2)}}</a>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box" id="hau">
            <h3 class="box-title m-b-0">Hausverwaltung Kosten</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive ">
               <table class="table table-striped dashboard-table" id="hausmax_table">
                  <thead>
                     <tr>
                        <th>Name</th>
                        <th>Anzahl</th>
                        <th>Betrag/Monat</th>
                        <th>Betrag/Jahr</th>
                        <th>IST-NME</th>
                     </tr>
                  </thead>
                  <tbody>
                     @if(isset($hausmax_properties))
                     @php $sum=0; $rent_sum = 0;@endphp
                     @foreach($hausmax_properties as $hausmax)
                     <?php
                        if(is_numeric($hausmax->sumhausmaxeuro_monat))
                         $sum +=$hausmax->sumhausmaxeuro_monat;    
                        
                        ?>
                     @php
                     if(is_null($hausmax->hausmaxx))
                     {
                     continue;
                     }
                     @endphp
                     <tr>
                        <td>
                           @php 
                           $tendency_division = 0;
                           $tendency_item = \App\Http\Controllers\HomeController::hausmaxx_tenancy_schedule_items($hausmax->hausmaxx);
                           if($tendency_item > 0){
                           if($hausmax->sumhausmaxeuro_monat > 0){
                           $tendency_division = $hausmax->sum*12/$tendency_item*100;
                           }
                           }
                           $rent_sum +=$tendency_item;
                           @endphp
                           <a href="javascript:void(0)" class="get-hausmaxx-data" data-id="{{ $hausmax->hausmaxx }}" >{{ $hausmax->hausmaxx }}</a>
                        </td>
                        <td class="text-right">{{$hausmax->properties_count}}</td>
                        <td class="text-right">{{number_format($hausmax->sumhausmaxeuro_monat,2,",",".")}}€</td>
                        <td class="text-right">{{number_format($hausmax->sumhausmaxeuro_monat*12,2,",",".")}}€</td>
                        <td class="text-right">{{number_format($tendency_item,2,",",".")}}€</td>
                     </tr>
                     @endforeach
                     @endif
                  </tbody>
                  <tr>
                     <th>Summe</th>
                     <th></th>
                     <th class="text-right">{{ number_format( $sum, 2 ,",",".") }}€</th>
                     <th class="text-right">{{ number_format( $sum*12, 2 ,",",".") }}€</th>
                     <th class="text-right">{{ number_format( $rent_sum, 2 ,",",".") }}€</th>
                  </tr>
               </table>
               <form id="export-to-excel-form" method="post" action="{{url('export-to-excel-haus')}}">
                  <input type="hidden" name="_token" value="{{csrf_token()}}">
                  <input type="hidden" name="property_data" id="property-data" value="">
                  <button type="button" class="btn-export-to-excel btn btn-primary" >Export </button>
               </form>
            </div>
            <div class="HAUSVERWALTUNG" style="display: none">{{number_format($sum*12,2,",",".")}}€</div>
         </div>
      </div>
      <div class="col-sm-6" style=" padding: 0 5px;">
         <div class="white-box" id="mahn">
            <h3 class="box-title m-b-0">Mahnung</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive mahnung-div">
            </div>
         </div>
      </div>
   </div>
   <?php
      $role = Auth::user()->role;
      ?>
   <div class="row">
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box" id="aus">
            <h3 class="box-title m-b-0">ABGELAUFENE MIETVERTRÄGE</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive">
               <table id="tenant-table3" class="table table-striped">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>AM</th>
                        <th>Objekt</th>
                        <th>Vermietet</th>
                        <th>Mietende</th>
                        <th>Fläche (m2)</th>
                        <th>IST-Nkm (€)</th>
                        <th>Typ</th>
                        <th>Kommentare</th>
                     </tr>
                  </thead>
                  <tbody>
                     @php $sumau=$amount = 0; @endphp  
                     @foreach($tenancy_items_2 as $tenancy_item)
                     @if($tenancy_item->status && $tenancy_item->rent_end && $tenancy_item->rent_end<=date('Y-m-d'))
                     <?php
                        $tenancy_item->creator_name = short_name($tenancy_item->creator_name);
                        ?>
                     <tr>
                        <td>{{$tenancy_item->id}}</td>
                        <td> {{$tenancy_item->creator_name}}</td>
                        <td>{{$tenancy_item->object_name}}</td>
                        <td><a href="javacript:void(0)" data-id="{{$tenancy_item->id}}" class="tenancy_item">{{$tenancy_item->name}}</a></td>
                        <td>{{show_date_format($tenancy_item->rent_end)}}</td>
                        <td class="text-right">{{$tenancy_item->rental_space ? number_format($tenancy_item->rental_space,2,",",".") : 0}}</td>
                        <td class="text-right">{{$tenancy_item->actual_net_rent ? number_format($tenancy_item->actual_net_rent,2,",",".") : 0}}</td>
                        <td>{{$tenancy_item->type}}</td>
                        <td>{{$tenancy_item->comment}}</td>
                     </tr>
                     <?php
                        $sumau += 1; 
                        $amount +=$tenancy_item->actual_net_rent;
                        ?>
                     @endif
                     @endforeach
                  </tbody>
               </table>
               <div class="sumaus" style="display: none">{{number_format($amount,2,',','.')}}€ ({{ $sumau }})</div>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box" id="auslm">
            <h3 class="box-title m-b-0">Auslaufende Mietverträge</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive">
               <table id="tenant-table2" class="table table-striped">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>AM</th>
                        <th>Objekt</th>
                        <th>Vermietet</th>
                        <th>Mietende</th>
                        <th>Fläche (m2)</th>
                        <th>IST-Nkm (€)</th>
                        <th>Typ</th>
                        <th>Kommentare</th>
                     </tr>
                  </thead>
                  <tbody>
                     @php $sumau=$amount = 0; @endphp  
                     @foreach($tenancy_items_2 as $tenancy_item)
                     @if(!$tenancy_item->rent_end || $tenancy_item->rent_end>date('Y-m-d'))
                     <?php
                        $tenancy_item->creator_name = short_name($tenancy_item->creator_name);
                        ?>
                     <tr>
                        <td>{{$tenancy_item->id}}</td>
                        <td>{{$tenancy_item->creator_name}}</td>
                        <td>
                           <a href="{{route('properties.show',['property'=>$tenancy_item->property_id])}}">
                           {{$tenancy_item->object_name}}</a>
                        </td>
                        <td><a href="javacript:void(0)" data-id="{{$tenancy_item->id}}" class="tenancy_item">{{$tenancy_item->name}}</a></td>
                        <td>{{show_date_format($tenancy_item->rent_end)}}</td>
                        <td class="text-right">{{$tenancy_item->rental_space ? number_format($tenancy_item->rental_space,2,",",".") : 0}}</td>
                        <td class="text-right">{{$tenancy_item->actual_net_rent ? number_format($tenancy_item->actual_net_rent,2,",",".") : 0}}</td>
                        <td>{{$tenancy_item->type}}</td>
                        <td>{{$tenancy_item->comment}}</td>
                     </tr>
                     @endif
                     @php 
                     if($tenancy_item->rent_end && $tenancy_item->rent_end>=date('Y-m-d') && $tenancy_item->rent_end < date('Y-m-d',strtotime('+3 months'))){
                     $sumau += 1; 
                     $amount +=$tenancy_item->actual_net_rent;
                     }
                     @endphp 
                     @endforeach
                  </tbody>
               </table>
               <div class="sumau" style="display: none">{{number_format($amount,2,',','.')}}€ ({{ $sumau }})</div>
               <input type="hidden" id="sum-tenancy-amount" value="">
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box" id="leer">
            <h3 class="box-title m-b-0">Leerstände (€)</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive">
               <table id="tenant-table4" class="table table-striped">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Asset Manager</th>
                        <th>Objekt</th>
                        <th>Leerstand</th>
                        <th>Fläche (m2)</th>
                        <th>Leerstand (€)</th>
                        <th>Typ</th>
                        <th>Kommentare</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     $tenancy_items_34_total = array();
                     ?>
                     @foreach($tenancy_items_34 as $tenancy_item)
                     <?php
                     // if(!$tenancy_item->vacancy_in_eur)
                     $tenancy_item->vacancy_in_eur = $tenancy_item->calculated_vacancy_in_eur;

                     
                                            

                     if(isset($tenancy_items_34_total[$tenancy_item->property_id]))
                     {
                        $tenancy_items_34_total[$tenancy_item->property_id]['count'] += 1;
                        $tenancy_items_34_total[$tenancy_item->property_id]['amount'] += $tenancy_item->vacancy_in_eur;
                        $tenancy_items_34_total[$tenancy_item->property_id]['space'] += $tenancy_item->vacancy_in_qm;
                     }
                     else{
                        $tenancy_items_34_total[$tenancy_item->property_id]['count'] = 1;
                        $tenancy_items_34_total[$tenancy_item->property_id]['amount'] = $tenancy_item->vacancy_in_eur;
                        $tenancy_items_34_total[$tenancy_item->property_id]['space'] = $tenancy_item->vacancy_in_qm;
                     }
                     $tenancy_items_34_total[$tenancy_item->property_id]['name'] = $tenancy_item->object_name;
                     $tenancy_items_34_total[$tenancy_item->property_id]['creator_name'] = $tenancy_item->creator_name;
                        

                     ?>
                     <tr>
                        <td>{{$tenancy_item->id}}</td>
                        <td>{{$tenancy_item->creator_name}}</td>
                        <td>
                           <a  href="{{route('properties.show',['property'=>$tenancy_item->property_id])}}?tab=tenancy-schedule">
                           {{$tenancy_item->object_name}}</a>
                        </td>
                        <td>{{$tenancy_item->name}}</td>
                        <td class="number-right">{{$tenancy_item->vacancy_in_qm ? number_format($tenancy_item->vacancy_in_qm,2,",",".") : 0}}</td>
                        <td class="number-right">{{$tenancy_item->vacancy_in_eur ? number_format($tenancy_item->vacancy_in_eur,2,",",".") : 0}}</td>
                        <td>{{$tenancy_item->type}}</td>
                        <td>{{$tenancy_item->comment}}</td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box">
            <h3 class="box-title m-b-0">Leerstände pro Objekt</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive">
               <table id="sum-tenant-table4" class="table table-striped">
                  <thead>
                     <tr>
                        <th>Objekt</th>
                        <th>AM</th>
                        <th class="text-right">Anazal</th>
                        <th class="text-right">Fläche (m2)</th>
                        <th class="text-right">Leerstand (€)</th>
                     </tr>
                  </thead>
                  <?php
                  $v1 = $v2 = $v3 = 0;
                  ?>
                  <tbody>
                     @foreach($tenancy_items_34_total as $property_id=>$tenancy_item)
                     <?php
                     $v1 += $tenancy_item['count'];
                     $v2 += $tenancy_item['amount'];
                     $v3 += $tenancy_item['space'];
                     ?>
                     <tr>
                        <td>
                           <a  href="{{route('properties.show',['property'=>$property_id])}}?tab=tenancy-schedule">
                           {{$tenancy_item['name']}}</a>
                        </td>
                        <td>{{$tenancy_item['creator_name']}}</td>
                        <td class="number-right">
                           <a href="javacript:void(0)" class="get-vacancy-list" data-id="{{$property_id}}">{{number_format($tenancy_item['count'],0,",",".")}}</a></td>
                        <td class="number-right">{{number_format($tenancy_item['space'],2,",",".")}}</td>
                        <td class="number-right">{{number_format($tenancy_item['amount'],2,",",".")}}</td>
                     </tr>
                     @endforeach
                  </tbody>
                  <tfoot>
                     <tr>
                        <th colspan="2">Gesamt</th>
                        <th class="text-right">{{number_format($v1,0,",",".")}}</th>
                        <th class="text-right">{{number_format($v2,2,",",".")}}</th>
                        <th class="text-right">{{number_format($v3,2,",",".")}}</th>
                     </tr>
                  </tfoot>
               </table>
            </div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-sm-12" style=" padding: 0 5px;">
         <div class="white-box" id="rental_activity_div">
            <h3 class="box-title m-b-0">Vermietungsaktivitäten</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive">
               <table class="table table-striped" id="rental_activity_table" style="width: 100%;padding-left: 0px;">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Asset Manager</th>
                        <th>Objekt</th>
                        <th>Leerstand</th>
                        <th>Fläche (m2)</th>
                        <th>Immoscout Upload</th>
                        <th>Immowelt Upload</th>
                        <th>Ebay Upload</th>
                        <th>Regionale Zeitung</th>
                        <th>Schaufenster ausgehängt</th>
                        <th>E-Mails über Intranet</th>
                        <th>Makler beauftragt (nicht exklusiv)</th>
                     </tr>
                  </thead>
                  <tbody></tbody>
               </table>
            </div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
         <div class="white-box">
            <h3 class="box-title m-b-0">Kommentare</h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="row">
               <div class="col-sm-6" style="height:440px; overflow-y: scroll;">
                  @foreach($comments as $comment)
                  <div class="d-flex flex-row comment-row mt-0 mb-0">
                     <div class="p-2">
                        <img src="{{(config('upload.avatar_path').$comment->user->image)}}" alt="user" width="40" class="rounded-circle" style="border-radius: 50%;">
                     </div>
                     <div class="comment-text w-100">
                        <h5 class="font-normal mb-1">{{$comment->user->name}}</h5>
                        <span class="text-muted mr-2 font-12">
                        {{ show_datetime_format($comment->created_at) }}
                        </span>
                        <span class="hidden badge @if($comment->status == 'Steht aus') badge-info @endif @if($comment->status == 'Akzeptiert') badge-success @endif @if($comment->status == 'Abgelehnt') badge-danger @endif badge-rounded text-uppercase font-medium">{{ $comment->status }}</span>
                        <p class="mb-2 d-block font-14 text-muted font-light mt-3" style="">
                           <a  href="{{route('properties.show',['property'=>$comment->property_id])}}?tab=comment_tab">
                           {{ $comment->name_of_property }} :
                           </a>
                           <a href="{{route('properties.show',['property'=>$comment->property_id])}}?tab=comment_tab">
                           {!! $comment->comment !!}
                           </a>
                        </p>
                        @if($comment->pdf_file)
                        <a href="{{ asset('pdf_upload/'.$comment->pdf_file) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a><br/>
                        @endif
                        <hr>
                        {{-- 
                        <div class="mt-3">
                           <a href="javacript:void(0)" class="btn btn btn-rounded btn-outline-success mr-2 btn-sm"><i class="ti-check mr-1"></i>Akzeptieren</a>
                           <a href="javacript:void(0)" class="btn-rounded btn btn-outline-danger btn-sm"><i class="ti-close mr-1"></i> Ablehnen</a>
                        </div>
                        --}}
                     </div>
                  </div>
                  @endforeach
               </div>
               <div class="col-sm-6">
                  <div class="table-responsive">
                     <table class="table table-striped">
                        <tbody>
                           @foreach($ccomments as $comment)
                           <tr>
                              <td>
                                 <a  href="{{route('properties.show',['property'=>$comment->property_id])}}?tab=comment_tab">
                                 {{ $comment->name_of_property }} :
                                 </a>
                              </td>
                              <td>{{$comment->name}}</td>
                              <td>
                                 <a class="comment-detail" href="javacript:void(0)" data-id="{{$comment->id}}">
                                 @if($comment->type==1)
                                 Mahnung
                                 @elseif($comment->type==2)
                                 Gerichtliches Mahnverfahren
                                 @elseif($comment->type==3)
                                 Ratenzahlung/Kündigung
                                 @endif
                                 </a>
                              </td>
                              <td>{{show_datetime_format($comment->created_at)}}</td>
                           </tr>
                           @endforeach
                           @if(1==2)
                           <tr>
                              <td>Mahnung</td>
                              <td><input type="checkbox" @if(isset($ccomments[0]['status']) && $ccomments[0]['status']) checked @endif></td>
                              <td>@if(isset($ccomments[0]['comment'])){{$ccomments[0]['comment']}}@endif
                                 <br>
                                 @if(isset($ccomments[0]['files']) && $ccomments[0]['files'])
                                 <?php
                                    $ar = json_decode($ccomments[0]['files'],true)
                                    ?>
                                 @foreach($ar as $list)
                                 <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
                                 @endforeach
                                 @endif
                              </td>
                           </tr>
                           <tr>
                              <td>Gerichtliches Mahnverfahren </td>
                              <td><input type="checkbox" name="" @if(isset($ccomments[1]['status']) && $ccomments[1]['status']) checked @endif></td>
                              <td>@if(isset($ccomments[1]['comment'])){{$ccomments[1]['comment']}}@endif
                                 <br>
                                 @if(isset($ccomments[1]['files']) && $ccomments[1]['files'])
                                 <?php
                                    $ar = json_decode($ccomments[1]['files'],true)
                                    ?>
                                 @foreach($ar as $list)
                                 <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
                                 @endforeach
                                 @endif
                              </td>
                           </tr>
                           <tr>
                              <td>Ratenzahlung /Kündige</td>
                              <td><input type="checkbox" name="" @if(isset($ccomments[2]['status']) && $ccomments[2]['status']) checked @endif></td>
                              <td>@if(isset($ccomments[2]['comment'])){{$ccomments[2]['comment']}}@endif
                                 <br>
                                 @if(isset($ccomments[2]['files']) && $ccomments[2]['files'])
                                 <?php
                                    $ar = json_decode($ccomments[2]['files'],true)
                                    ?>
                                 @foreach($ar as $list)
                                 <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
                                 @endforeach
                                 @endif
                              </td>
                           </tr>
                           @endif
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @if(1==2)
   <div class="col-md-6">
      <div class="card">
         <div class="p-4 border-bottom" style="padding: 10px 15px;">
            <div class="d-flex align-items-center">
               <div>
                  <h4 class="mb-0 font-light">Mieteinnahmen Monat</h4>
                  <h2 class="mb-0 font-medium">XXX.XX€</h2>
               </div>
               {{--  
               <div class="ml-auto">
                  <select class="form-control">
                     <option>January 2018</option>
                     <option>February 2018</option>
                     <option>March 2018</option>
                  </select>
               </div>
               --}}
            </div>
         </div>
         <div class="card-body bg-light" style="padding-bottom: 25px;     height: 400px;overflow-y: scroll;">
            @foreach($total_rarnings as $total_earn)
            <div class="d-flex align-items-center py-3">
               @if($total_earn->image != null)
               <img src="{{(config('upload.avatar_path').$total_earn->image)}}" class="rounded-circle" width="60" style="border-radius: 50%;">
               @else
               <img src="{{ url('user-images/2.png') }}" class="rounded-circle" width="60" style="border-radius: 50%;">
               @endif
               <div class="ml-3">
                  <h4 class="font-normal mb-0">{{ $total_earn->name }}</h4>
                  {{-- <span class="text-muted">10-11-2016</span> --}}
               </div>
               <div class="ml-auto">
                  <h2 class="mb-0 text-info font-medium">${{  number_format($total_earn->total_sum) }}</h2>
               </div>
            </div>
            @endforeach
         </div>
      </div>
   </div>
   @endif
   @if(1==2)
   <div class="row">
      <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" >
         <div class="white-box">
            <h3 class="box-title m-b-0">Neue Mietverträge </h3>
            <p class="text-muted m-b-30">&nbsp;</p>
            <div class="table-responsive">
               <table id="tenant-table_22" class="table table-striped">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Asset Manager</th>
                        <th>Objekt</th>
                        <th>Vermietet</th>
                        <th>Datum</th>
                        <th>Mietbeginn</th>
                        <th>Mietfläche (m2)</th>
                        <th>IST-Nettokaltmiete (€)</th>
                        <th>Typ</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($tenancy_items_4 as $tenancy_item_33)
                     <tr>
                        <td>{{$tenancy_item_33->id}}</td>
                        <td>{{$tenancy_item_33->creator_name}}</td>
                        <td>{{$tenancy_item_33->object_name}}</td>
                        <td>{{$tenancy_item_33->name}}</td>
                        <td>{{$tenancy_item_33->created_at}}</td>
                        <td>{{$tenancy_item_33->rent_begin}}</td>
                        <td>{{$tenancy_item_33->rental_space ? number_format($tenancy_item_33->rental_space,2,",",".") : 0}}</td>
                        <td>{{$tenancy_item_33->actual_net_rent ? number_format($tenancy_item_33->actual_net_rent,2,",",".") : 0}}</td>
                        <td>{{$tenancy_item_33->type}}</td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   @endif
</div>


@include('dashboard.dashboard_popup_partial')

@endsection
@section('js')
   <script type="text/javascript">

      var _token = '{{ csrf_token() }}';

      {{-- ROUTE VARIABLE START --}}
      var url_get_default_payers                = '{{ route('get_default_payers') }}';
      var url_delete_property_invoice     = '{{ route("delete_property_invoice", ":id") }}';
      var url_property_addmaillog         = '{{ url('property/addmaillog') }}';
      var url_property_management_update     = '{{ route('property_management.update') }}';
           
      var url_property_vacantreleaseprocedure = '{{ url('property/vacantreleaseprocedure') }}';
      var url_property_vacantmarkednotrelease = '{{ url('property/vacantmarkednotrelease') }}';
      
      var url_get_property_provision            = '{{ route('getPropertyProvision') }}';
      var url_getvacantlist                     = '{{ route('getVacantList') }}';
      var url_property_invoicereleaseprocedure  = '{{ url('property/invoicereleaseprocedure') }}';
      var url_getMahnungAmount                  = '{{ route('getMahnungAmount') }}';
      var url_getRentCompare                  = '{{ route('getRentCompare') }}';
      var url_getRentCompareList                  = '{{ route('getRentCompareList') }}';
      var url_tenant_detail                     = '{{ route('tenant-detail') }}';
      var url_assetmanagement                   = '{{ route('assetmanagement') }}';
      var url_getMahnungTenant                  = '{{ route('getMahnungTenant') }}';
      var url_monthyearmanager                  = '{{ route('monthyearmanager') }}';
      var url_monthyearassetmanager             = '{{ route('monthyearassetmanager') }}';
      var url_getmanagerproperty                = '{{ route('getmanagerproperty') }}';
      var url_getassetmanagerproperty           = '{{ route('getassetmanagerproperty') }}';
      var url_getInsurancePropertyList          = '{{ route('getInsurancePropertyList') }}';
      var url_get_hausmaxx_data                 = '{{ route('get_hausmaxx_data') }}';
      var url_statusloi_update                  = '{{ url('statusloi/update') }}';
      var url_getstatusloidahboard              = '{{ url('/getstatusloidahboard/') }}';
      var url_getStatusLoiByUserIdDashboard     = '{{ route('getStatusLoiByUserIdDashboard') }}';
      var url_getPropertyInvoice                = '{{ route('getPropertyInvoice') }}';
      var url_getInsurance                      = '{{ route('getInsurance') }}';
      var url_getManagement                     = '{{ route('getManagement') }}';
      var url_releaselogs                       = '{{ route('releaselogs') }}';
      var url_getbankproperty                   = '{{ route('getbankproperty') }}';
      var url_vreleaselogs                      = '{{ route('vreleaselogs') }}';
      var url_monthyearassetmanager             = '{{ route('monthyearassetmanager') }}';
      var url_getstatusloidahboard_month        = '{{ route('getstatusloidahboard', [\Carbon\Carbon::now()->month]) }}';
      var url_change_comment                    = '{{ route('change-comment') }}';
      var url_getbanklist                       = '{{ route('getbanklist') }}';
      var url_getusers                          = '{{ route('getusers') }}';
      var url_comment_detail                    = '{{ route('comment-detail') }}';
      var url_getMahnungProperties              = '{{ route('getMahnungProperties') }}';
      var url_getInsuranceReleaseLogs           = '{{ route('getInsuranceReleaseLogs') }}';
      var url_getPropertyInsurance              = '{{ route('getPropertyInsurance') }}';
      var url_getPropertyProvision              = '{{ route('getPropertyProvision') }}';
      var url_mailchimp                         = '{{ route('mailchimp') }}';
      var url_setAssestManagerOnProperty        = '{{ route("setAssestManagerOnProperty", ["property_id" => ":property_id", "am_id" => ":am_id"]) }}';
      var url_getcategoryproperty               = '{{ route('getcategoryproperty') }}';
      var url_getcategoryproperty3              = '{{ route('getcategoryproperty3') }}';
      var url_property_provisionreleaseprocedure= '{{ url('property/provisionreleaseprocedure') }}';
      var url_getSocialFollowers                = '{{ route('getSocialFollowers') }}';
      var url_property_invoicemarkpending       = '{{ url('property/invoicemarkpending') }}';
      var url_property_invoicenotrelease        = '{{ url('property/invoicenotrelease') }}';
      var url_getPendingNotReleaseInvoice       = '{{ route('getPendingNotReleaseInvoice') }}';
      var url_getPropertyDefaultPayers          = '{{ route('getPropertyDefaultPayers') }}';
      var url_getPropertyInsuranceTab           = '{{ route('getPropertyInsuranceTab') }}';
      var url_update_versicherung_approved_status = '{{ url('/update_versicherung_approved_status') }}';
      var url_update_falk__status               = '{{ url('/update_falk__status') }}';
      var url_get_property_insurance_detail     = '{{ route("get_property_insurance_detail", ["tab_id" => ":tab_id"]) }}';
      var url_property_dealreleaseprocedure     = '{{ url('property/dealreleaseprocedure') }}';
      var url_deal_mark_as_notrelease           = '{{ route('deal_mark_as_notrelease') }}';
      var url_getContract                       = '{{ route('getContract') }}';
      var url_property_contractreleaseprocedure = '{{ url('property/contractreleaseprocedure') }}';
      var url_contract_mark_as_notrelease       = '{{ route('contract_mark_as_notrelease') }}';
      var url_contract_mark_as_pending          = '{{ route('contract_mark_as_pending') }}';
      var url_get_retnal_activity               = '{{ route('get_retnal_activity') }}';
      var url_getRecommendation                 = '{{ route('getRecommendation') }}';
      var url_getFinancingPerBank               = '{{ route('getFinancingPerBank') }}';
      
      {{-- ROUTE VARIABLE END --}}
   </script>

   <script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
   <script src="{{asset('js/echarts-en.min.js')}}"></script>
   <script src="{{asset('js/highcharts.js')}}"></script>
   <script src="{{asset('js/exporting.js')}}"></script>
   {{-- <script src="https://wrappixel.com/ampleadmin/assets/libs/echarts/dist/echarts-en.min.js"></script> --}}
   {{-- <script src="https://code.highcharts.com/highcharts.js"></script> --}}
   {{-- <script src="https://code.highcharts.com/modules/exporting.js"></script> --}}
   <script src="{{asset('js/custom-datatable.js')}}"></script>
   <script src="{{asset('js/dashboard.js')}}"></script>

   <script type="text/javascript">
      $( document ).ready(function() {
          //$(function() {
          "use strict";
          // ------------------------------
          // Basic bar chart
          // ------------------------------
          // based on prepared DOM, initialize echarts instance
         var myChart = echarts.init(document.getElementById('basic-bar'));
      
          // specify chart configuration item and data
         var option = {
              // Setup grid
            grid: {
               left: '1%',
               right: '2%',
               bottom: '3%',
               containLabel: true
            },
      
            // Add Tooltip
            tooltip : {
               trigger: 'axis'
            },
            legend: {
               data:['Mieteinnahmen pro Monat (€)','Site B']
            },
            toolbox: {
               show : true,
               feature : {
                  magicType : {show: true, type: ['line', 'bar']},
                  restore : {show: true},
                  saveAsImage : {show: true}
               }
            },
            color: ["#2962FF", "#4fc3f7"],
            calculable : true,
            xAxis : [
               {
                  type : 'category',
                  // data : ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sept','Okt','Nov','Dez']
                  data : [{!! implode($displayMonth, ',') !!}]
               }
            ],
            yAxis : [
               {
                   type : 'value',
                   min : '1500000'
               }
            ],
            series : [
               {
                  name:'Mieteinnahmen pro Monat (€)',
                  type:'bar',
                  data:[{{ implode($year_graph, ',')  }}],
                  markPoint : {
                     data : [
                        {type : 'max', name: 'Max'},
                        {type : 'min', name: 'Min'}
                     ]
                  },
                  markLine : {
                     data : [
                        {type : 'average', name: 'Average'}
                     ]
                  }
               },
               /*{
                   name:'Site B',
                   type:'bar',
                   data:[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
                   markPoint : {
                       data : [
                           {name : 'The highest year', value : 182.2, xAxis: 7, yAxis: 183, symbolSize:18},
                           {name : 'Year minimum', value : 2.3, xAxis: 11, yAxis: 3}
                       ]
                   },
                   markLine : {
                       data : [
                           {type : 'average', name : 'Average'}
                       ]
                   }
               }*/
            ]
         };
      
         // use configuration item and data specified to show chart
         myChart.setOption(option);
         //}
      });
   </script>


   @if(isset($_REQUEST['section']) && $_REQUEST['section'])
   <script type="text/javascript">
      $(document).ready(function(){
         setTimeout(function(){
            moveTodiv("{{$_REQUEST['section']}}");
         },5000)
      })
   </script>
   @endif
@endsection