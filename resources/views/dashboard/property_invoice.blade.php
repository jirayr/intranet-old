<?php
		$user_email = strtolower(Auth::user()->email);
		$replyemailaddress = config('users.falk_email');
		$delete = 0;
		?>
		
		@if($user_email==$replyemailaddress || $user_email=="c.wiedemann@fcr-immobilien.de")
		<?php $delete=1; ?>
		<button type="button" style="margin-bottom:20px;" class="btn btn-primary multiple-invoice-release-request pull-right">Markierte Rechnungen freigeben</button>
		@endif
		
<table id="table-property-invoice" class="table table-striped">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>AM</th>
			<th>Rechnung</th>
			<th>R. Datum</th>
			<th>Betrag</th>
			<th>Kommentar</th>
			<th>Abbuch.</th>
			<th>Umlegb.</th>
			<th>User</th>
			<th>Datum</th>
			{{-- <th></th> --}}
			{{-- <th></th> --}}
			{{-- <th></th> --}}
			{{-- <th></th> --}}
			<th style="min-width: 175px;">Freigabe Falk</th>
			<th></th>
			<th></th>
			<th>Weiterleiten an</th>
		</tr>
	</thead>
	<tbody>
		@if (!empty($data))
			@foreach ($data as $element)
				
				<?php 
					$download_path = "https://drive.google.com/drive/u/2/folders/".$element->file_basename;
	                if($element->file_type == "file"){
	                    $download_path = "https://drive.google.com/file/d/".$element->file_basename;
	                }

	                $subject = 'Rechnungsfreigabe: '.$element->name_of_property;
					$content = 'Rechnung: <a title="'.$element->invoice.'" href="'.$download_path.'" target="_blank">'.$element->invoice.'</a>';
					if($element->comment){
						$content .= '<br/>Kommentar User: '.$element->comment;
					}

					$taburl = route('properties.show',['property' => $element->property_id]).'?tab=property_invoice';
        			$taburl = "<a href='".$taburl."'>".$taburl."</a>";
				?>
				
				<tr>
					<td>
						<a href="{{route('properties.show',['property'=> $element->property_id])}}?tab=property_invoice">
                            {{$element->name_of_property}}
                        </a>
					</td>
					<td>
						<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $element->property_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="RECHNUNGSFREIGABEN" data-id="{{ $element->id }}" data-section="property_invoices">{{ $element->assetmanager }}</a>
					</td>
					<td>
						<a title="{{ $element->invoice }}" href="{{ $download_path }}" target="_blank">{{ $element->invoice }}</a>
					</td>
					<td>{{ show_date_format($element->date) }}</td>
					<td class="text-right">{{ show_number($element->amount,2) }}</td>
					<td>
						@php
							$comment = DB::table('properties_comments as pc')->selectRaw('pc.*, u.name, u.role, u.company')->join('users as u', 'u.id', '=', 'pc.user_id')->where('pc.record_id', $element->id)->where('pc.type', 'property_invoices')->orderBy('pc.created_at', 'desc')->first();
						@endphp
						@if($comment)
							@php
								$company = ($comment->role >= 6 && $comment->company) ? ' ('.$comment->company.')' : '';
								$commented_user = $comment->name.''.$company;
							@endphp
							<p class="long-text" style="margin-bottom: 0px;">
								<span class="commented_user">{{ $commented_user }}</span>: {{ $comment->comment }} ({{ show_datetime_format($comment->created_at) }})
							</p>
						@endif
						<button type="button" class="btn btn-primary btn-xs btn-show-property-comment" data-form="1" data-record-id="{{ $element->id }}" data-property-id="{{ $element->property_id }}" data-type="property_invoices" data-subject="{{ $subject }}" data-content="{{ $taburl }}">Kommentar</button>
					</td>
					<td><?php echo get_paid_checkbox($element->id,$element->need_to_pay);?></td>
					<td><?php echo get_paid_checkbox($element->id,$element->towards_tenant)."<br>".show_number($element->foldable,2) ?></td>
					<td>
						<a href="javascript:void(0);" class="custom_user" data-property-id="{{ $element->property_id }}" data-user-id="{{ $element->user_id }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="RECHNUNGSFREIGABEN" data-id="{{ $element->id }}" data-section="property_invoices">{{ $element->name }}</a>
					</td>
					<td>{{ ($element->request2_date) ? show_datetime_format($element->request2_date) : show_datetime_format($element->created_at) }}</td>
					
					<!-- <td>
						@if($user_email==$replyemailaddress)
							<input class="invoice-checkbox" type="checkbox" data-id="{{$element->id}}">
						@endif
					</td>
					<td>
						@if($user_email==$replyemailaddress)
							<button data-id="<?=$element->id?>" data-property_id="<?=$element->property_id?>" type="button" class="btn btn-primary invoice-release-request" data-column="release2">Freigegeben</button>
						@endif
					</td>
					<td>
						@if($user_email==$replyemailaddress)
							<button data-id="<?=$element->id?>" type="button" class="btn btn-primary mark-as-not-release">Ablehnen</button>
						@endif
					</td>
					<td>
						@if($user_email==$replyemailaddress)
                    		<button data-id="<?=$element->id?>" type="button" class="btn btn-primary mark-as-pending">Pending</button>
                    	@endif
                	</td> -->
                	<td>
						@if(isset($element->button))
							{!! $element->button['btn_release_falk'] !!}
						@endif
					</td>
					<td>
						<input type="checkbox" class="falk_release_check form-control" data-id="{{ $element->id }}">
					</td>
                	<td>
                		<?php echo ($delete) ? '<button type="button" data-type="request-table" data-id="'.$element->id.'" class="btn btn-info btn-outline btn-circle btn-sm btn-delete-property-invoice"><i class="icon-trash"></i></button>' : ""?>
                	</td>
                	<td>
						<button type="button" class="btn btn-primary btn-forward-to" data-property-id="{{ $element->property_id }}" data-id="{{ $element->id }}" data-subject="{{ $subject }}" data-content="" data-title="RECHNUNGSFREIGABEN" data-reload="0" data-section="property_invoices">Weiterleiten an</button>
					</td>
				</tr>
				
			@endforeach
		@endif
	</tbody>
</table>
<input type="hidden" class="unpaidinvoice" value="{{count($data)}}">