@extends('layouts.admin')
@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
 
    <style type="">
    #added-month-asset tbody th:first-child{
        cursor: pointer;
    }
    @media (min-width: 992px)
    {    .modal-lg {
            width: 1320px;
        }
    }
    @media (min-width: 768px)
    {
        .modal-dialog.custom {
            width: 848px;
            margin: 30px auto;
        }
    }

    .insurance_type_2_div .btn,
    .insurance_type_1_div .btn{
        display: none;
    }
        ._51mz{
            /*display: none;*/
        }
        .card {
            position: relative;
            /*display: flex;*/
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 0 solid transparent;
            border-radius: 0;
            height: 115px !important;
        }
        .card-body {
            flex: 1 1 auto;
            padding: 8px 12px;
        }
        .card .card-title {
            position: relative;
            font-weight: 500;
            font-size: 16px;
        }
        .d-flex {
            display: flex!important;
        }
        .align-items-center {
            align-items: center!important;
        }
        .ml-auto, .mx-auto {
            margin-left: auto!important;
        }
        .custum-row .col-lg-3, .custum-row .col-md-3{
            padding: 0 5px;
        }

        .custum-row-2{
            margin-top: 25px;
        }

        .custum-row-2 .col-md-6{
            margin: 5px 0;
            padding: 0 5px;
        }
        .preloader{
            display: none;
        }
        .pb-3, .py-3{
            padding-bottom: 20px;
            margin-top: 10px;
        }
        .for-col-padding .col-md-6{
            padding: 0 5px;
        }
        .pointer-cursor{
            cursor: pointer;
        }
        .get-assetmanager-property,.get-manager-property{
            cursor: pointer;
        }
        .border-top-footer{
            border-top: 3px solid #666 !important;
        }
        .div-user-count{
            font-size: 16px !important;
            color: #313131;
            font-weight: 300 !important;
        }
        .get-verkauf-user,
        .get-vermietung-user{
            cursor: pointer;
            color: #23527c;
        }
        .col-md-3 .progress{
            display: none !important;
        }
        .blue-bg{
            background-color: #004f91;
        }
        .blue-bg h2,
        .blue-bg h5{
            color: white;
        }
        .red-bg{
        background-color: #e03232;
      }
      .red-bg h2,
      .red-bg h5{
        color: white;
      }

        .custum-row .col-md-3{
            margin-bottom: 10px;
        }

        .loading {
            height: 0;
            width: 0;
            padding: 15px;
            border: 6px solid #ccc;
            border-right-color: #888;
            border-radius: 22px;
            -webkit-animation: rotate 1s infinite linear;
            /* left, top and position just for the demo! */
            position: absolute;
            left: 50%;
            top: 50%;
        }
        @-webkit-keyframes rotate {
            /* 100% keyframe for  clockwise.
               use 0% instead for anticlockwise */
            100% {
              -webkit-transform: rotate(360deg);
            }
        }

    </style>


@endsection
@section('content')
    {{-- <link rel="stylesheet" type="text/css" href="https://wrappixel.com/ampleadmin/dist/css/style.min.css">  --}}
    {{-- {{ dd($tenancy_schedule_data) }} --}}
    
 <?php

function short_name($string)
{
    if($string){
        $arr = explode(" ", $string);
        if(count($arr)>1)
            return substr($arr[0], 0,1).substr($arr[1], 0,1);
        else
            return substr($string, 0,1);
    }
    return $string;
}
?>   
        <!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
  
<!-- TradingView Widget END -->
    
    <div class="page-content container-fluid">


        <div class="row custum-row">

                <div class="col-md-3 load-table" id="leerstände" data-table="tenant-table4" data-title="Leerstände (€)" data-url="{{ route('get_vacancy') }}?is_am=1">
                    <div class="card pointer-cursor red-bg" >
                        <div class="card-body">
                           <h5 class="card-title text-uppercase">Leerstände</h5>
                           <div class="text-right">
                              <h2 class="mt-2 display-7 vacancy-total">....</h2>
                           </div>
                           <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 load-table" id="open_invoice" data-table="invoice-table-0" data-title="Offene Rechnungen" data-url="{{ route('get_invoice_for_am', ['status' => 0]) }}">
                    <div class="card pointer-cursor">
                        <div class="card-body">
                           <h5 class="card-title text-uppercase">Offene Rechnungen</h5>
                           <div class="text-right">
                              <h2 class="mt-2 display-7 open_invoice_total"></h2>
                           </div>
                           <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 load-table" id="request_invoice" data-table="invoice-table-1" data-title="FREIZUGEBENDE RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 1]) }}">
                    <div class="card pointer-cursor">
                        <div class="card-body">
                           <h5 class="card-title text-uppercase">FREIZUGEBENDE RECHNUNGEN</h5>
                           <div class="text-right">
                              <h2 class="mt-2 display-7 request_invoice_total"></h2>
                           </div>
                           <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 load-table" id="release_invoice" data-table="invoice-table-2" data-title="FREIGEGEBENE RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 2]) }}">
                    <div class="card pointer-cursor">
                        <div class="card-body">
                           <h5 class="card-title text-uppercase">FREIGEGEBENE RECHNUNGEN</h5>
                           <div class="text-right">
                              <h2 class="mt-2 display-7 release_invoice_total"></h2>
                           </div>
                           <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 load-table" id="pending_invoice" data-table="invoice-table-3" data-title="PENDING RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 3]) }}">
                    <div class="card pointer-cursor">
                        <div class="card-body">
                           <h5 class="card-title text-uppercase">PENDING RECHNUNGEN</h5>
                           <div class="text-right">
                              <h2 class="mt-2 display-7 pending_invoice_total"></h2>
                           </div>
                           <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 load-table" id="not_release_invoice" data-table="invoice-table-4" data-title="NICHT FREIGEGEBEN RECHNUNGEN" data-url="{{ route('get_invoice_for_am', ['status' => 4]) }}">
                    <div class="card pointer-cursor">
                        <div class="card-body">
                           <h5 class="card-title text-uppercase">NICHT FREIGEGEBEN RECHNUNGEN</h5>
                           <div class="text-right">
                              <h2 class="mt-2 display-7 not_release_invoice_total"></h2>
                           </div>
                           <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 load-table" id="open-angebote" data-table="table-open-angebote" data-title="Offene Angebote" data-url="{{ route('getOpenAngebote') }}">
                     <div class="card pointer-cursor" >
                        <div class="card-body">
                           <h5 class="card-title text-uppercase">Offene Angebote</h5>
                           <div class="text-right">
                              <h2 class="mt-2 display-7 open-angebote-total"></h2>
                           </div>
                           <br>
                        </div>
                     </div>
                </div>

                  <div class="col-md-3 load-table" id="vobj" data-table="list-properties-1" data-title="Zu verteilende Objekte" data-url="{{ route('assetmanagement') }}?id=-1">
                     <div class="card pointer-cursor " >
                        <div class="card-body">
                           <h5 class="card-title text-uppercase">Zu verteilende Objekte</h5>
                           <div class="text-right">
                              <h2 class="mt-2 display-7 object-to-be-distributed-total">....</h2>
                           </div>
                           <br>
                        </div>
                     </div>
                  </div>

                

                <div class="col-md-3" >
                    <div class="card pointer-cursor"  onclick="moveTodiv('neue-div')">
                        <div class="card-body">
                            <h5 class="card-title text-uppercase">Neue Mietverträge <br>({{__('dashboard.'.date('F')).'-'.date('Y')}})</h5>
                            <div class="text-right">
                                <?php
                                $c = 0;
                                // foreach($asset_manager_array as $key=>$tenancy_item)
                                    // $c += $tenancy_item['count'];

                                ?>
                                <h2 class="mt-2 display-7 mv-count">...</h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3" >
                    <div class="card pointer-cursor" onclick="moveTodiv('vermietet')" >
                        <div class="card-body">
                            <h5 class="card-title text-uppercase">DIFFERENZ MIETE (SOLL/IST) IN €</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7"><sup></sup>{{ number_format($summ_differenz_miete_in_euro,2,",",".") }}</h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-inverse" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card pointer-cursor"  onclick="moveTodiv('vermietet')">
                        <div class="card-body">
                            <h5 class="card-title text-uppercase">Wault (Durchschnitt)</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7"><sup></sup>{{round($avg_wault,1)}}</h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card pointer-cursor"  onclick="moveTodiv('vermietet')">
                        <div class="card-body">
                            <h5 class="card-title text-uppercase">Vermietet (%)</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7 vermi_percent"><sup></sup>....</h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-info" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card pointer-cursor" >
                        <div class="card-body " onclick="moveTodiv('categ')">
                            <h5 class="card-title text-uppercase">Mieterübersicht</h5>
                            <div class="text-right">
                                
                                <h2 class="mt-2 display-7 categTotal">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
                
                {{-- <div class="col-md-3">
                    <div class="card pointer-cursor">
                        <div class="card-body" onclick="moveTodiv('category-prop-list4-div')">
                            <h5 class="card-title text-uppercase">MIETERÜBERSICHT (CORONA)</h5>
                            <div class="text-right">
                                
                                <h2 class="mt-2 display-7 category-prop-list4-total">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div> --}}

                {{-- <div class="col-md-3">
                    <div class="card pointer-cursor">
                        <div class="card-body" onclick="moveTodiv('category-prop-list5-div')">
                            <h5 class="card-title text-uppercase">Mieterübersicht (Corona)</h5>
                            <small style="float: left;">Netto Miete p.m.</small>
                            <div class="text-right">
                                
                                <h2 class="mt-2 display-7 category-prop-list5-total">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div> --}}

                {{-- <div class="col-md-3">
                    <div class="card pointer-cursor">
                        <div class="card-body" onclick="moveTodiv('category-prop-list6-div')">
                            <h5 class="card-title text-uppercase">Mieterübersicht gesamt</h5>
                            <div class="text-right">
                                
                                <h2 class="mt-2 display-7 category-prop-list6-total">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div> --}}

                <div class="col-md-3">
                    <div class="card pointer-cursor" >
                        <div class="card-body " onclick="moveTodiv('bankenfinan')">
                            <h5 class="card-title text-uppercase">BANKENFINANZIERUNGEN</h5>
                            <div class="text-right">
                                
                                <h2 class="mt-2 display-7 bankenfinanTotal">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card pointer-cursor" >
                        <div class="card-body " onclick="moveTodiv('geb')">
                            <h5 class="card-title text-uppercase">GEBÄUDE VERSICHERUNG </h5>
                            <div class="text-right">
                                
                                <h2 class="mt-2 display-7 axa1Total">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card pointer-cursor">
                        <div class="card-body" onclick="moveTodiv('expiring-building-insurance-div')">
                            <h5 class="card-title text-uppercase">Auslaufende Gebäudeversicherungen</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7">{{ ($g1) ? count($g1) : '....' }}</h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card pointer-cursor" onclick="moveTodiv('haft')">
                        <div class="card-body ">
                            <h5 class="card-title text-uppercase">HAFTPFLICHT VERSICHERUNG </h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7 axa0Total">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card pointer-cursor">
                        <div class="card-body" onclick="moveTodiv('expired-liability-insurance-div')">
                            <h5 class="card-title text-uppercase">Auslaufende Haftpflichtversicherungen</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7">{{ ($g2) ? count($g2) : '....' }}</h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>

                {{-- <div class="col-md-3">
                    <div class="card pointer-cursor" >
                        <div class="card-body" onclick="moveTodiv('hau')">
                            <h5 class="card-title text-uppercase">HAUSVERWALTUNG KOSTEN </h5>
                            <div class="text-right">
                              
                                <h2 class="mt-2 display-7 HAUSVERWALTUNGTOTAL">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div> --}}

                <div class="col-md-3 load-table" data-table="hausmax_table" data-title="Hausverwaltung Kosten" data-url="{{ route('get_house_management_cost') }}">
                    <div class="card pointer-cursor" >
                        <div class="card-body">
                           <h5 class="card-title text-uppercase">HAUSVERWALTUNG KOSTEN</h5>
                           <div class="text-right">
                              <h2 class="mt-2 display-7 house-management-cost-total">....</h2>
                           </div>
                           <br>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card pointer-cursor" onclick="moveTodiv('mahnung')" >
                        <div class="card-body">
                            <h5 class="card-title text-uppercase">Mahnungen (30 Tage)</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7 mahnungen-30-days"></h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <div class="col-md-3">
                    <div class="card pointer-cursor">
                        <div class="card-body" onclick="moveTodiv('expired-rental-agreement')">
                            <h5 class="card-title text-uppercase">ABGELAUFENE MIETVERTRÄGE</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7" id="total_expired_rental_agreement">....</h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div> --}}

                <div class="col-md-3" >
                    <div class="card pointer-cursor" >
                        <div class="card-body" onclick="moveTodiv('abs')">
                            <h5 class="card-title text-uppercase">ABGELAUFENE MV</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7 sumausTotal">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>

                {{-- <div class="col-md-3">
                    <div class="card pointer-cursor">
                        <div class="card-body" onclick="moveTodiv('expiring_leases_div')">
                            <h5 class="card-title text-uppercase">Auslaufende Mietverträge</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7" id="total_expiring_leases">....</h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div> --}}

                <div class="col-md-3">
                    <div class="card pointer-cursor" >
                        <div class="card-body" onclick="moveTodiv('aus')">
                            <h5 class="card-title text-uppercase">AUSLAUFENDE MV (90 Tage) </h5>
                            <div class="text-right">
                              
                                <h2 class="mt-2 display-7 sumauTotal">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>

                {{-- <div class="col-md-3">
                    <div class="card pointer-cursor">
                        <div class="card-body" onclick="moveTodiv('release-invoice-box')">
                            <h5 class="card-title text-uppercase">Freigegebene Rechnungen</h5>
                            <div class="text-right">
                                
                                <h2 class="mt-2 display-7 release-invoice-total">....
                                </h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div> --}}


            @if(Auth::user()->email != "t.raudies@fcr-immobilien.de")
                {{-- <div class="col-md-3">
                    <div class="card pointer-cursor">
                        <div class="card-body" onclick="moveTodiv('open-invoice-box')">
                            <h5 class="card-title text-uppercase">Offene Rechnungen AM</h5>
                            <div class="text-right">
                                <h2 class="mt-2 display-7 open-invoice-total">....</h2>
                            </div>
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div> --}}
            @endif
            {{-- <div class="col-md-3">
               <div class="card pointer-cursor " >
                  <div class="card-body " onclick="moveTodiv('pending-invoice')">
                     <h5 class="card-title text-uppercase">PENDING RECHNUNGEN</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 invoice-count-2">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="card pointer-cursor" >
                  <div class="card-body " onclick="moveTodiv('notrelease-invoice')">
                     <h5 class="card-title text-uppercase">NICHT FREIGEGEBEN</h5>
                     <div class="text-right">
                        <h2 class="mt-2 display-7 invoice-count-1">....
                        </h2>
                     </div>
                     
                     <br>
                  </div>
               </div>
            </div> --}}

                @if(Auth::user()->email == "t.raudies@fcr-immobilien.de")

                    <div class="col-md-3">
                       <div class="card pointer-cursor" >
                          <div class="card-body " onclick="moveTodiv('liquiplanung')">
                             <h5 class="card-title text-uppercase">Cashflow</h5>
                             <div class="text-right">
                                <h2 class="mt-2 display-7 liquiplanung-total"></h2>
                             </div>
                             <br>
                          </div>
                       </div>
                    </div>

                    <div class="col-md-3">
                       <div class="card pointer-cursor" >
                          <div class="card-body " onclick="moveTodiv('liquiplanung-1')">
                             <h5 class="card-title text-uppercase">LIQUIPLANUNG</h5>
                             <div class="text-right">
                                <h2 class="mt-2 display-7 liquiplanung-1-total"></h2>
                             </div>
                             <br>
                          </div>
                       </div>
                    </div>
                    
                @endif

            @if(Auth::user()->email == "u.wallisch@fcr-immobilien.de")

                <div class="col-md-3">
                  <div class="card pointer-cursor" onclick="moveTodiv('provision-2')">
                     <div class="card-body ">
                        <h5 class="card-title text-uppercase">Provisionsfreigaben</h5>
                        <div class="text-right">
                           <h2 class="mt-2 display-7 provisionTotal-2">....
                           </h2>
                        </div>
                        <br>
                     </div>
                  </div>
               </div>

            @endif  

            @if(Auth::user()->email == "j.klausch@fcr-immobilien.de")
               {{-- <div class="col-md-3">
                  <div class="card pointer-cursor" onclick="moveTodiv('provision-2')">
                     <div class="card-body ">
                        <h5 class="card-title text-uppercase">Provisionsfreigaben</h5>
                        <div class="text-right">
                           <h2 class="mt-2 display-7 provisionTotal-2">....
                           </h2>
                        </div>
                        <br>
                     </div>
                  </div>
               </div>--}}
            @endif

            @if(Auth::user()->email == "u.wallisch@fcr-immobilien.de")
                <div class="col-md-3">
                  <div class="card pointer-cursor" onclick="moveTodiv('provision-5')">
                     <div class="card-body ">
                        <h5 class="card-title text-uppercase">Freigegebene Provisionen</h5>
                        <div class="text-right">
                           <h2 class="mt-2 display-7 provisionTotal-5">....
                           </h2>
                        </div>
                        <br>
                     </div>
                  </div>
               </div>

               <div class="col-md-3">
                  <div class="card pointer-cursor" onclick="moveTodiv('provision-3')">
                     <div class="card-body ">
                        <h5 class="card-title text-uppercase">Freigegebene Provisionen</h5>
                        <div class="text-right">
                           <h2 class="mt-2 display-7 provisionTotal-3"></h2>
                        </div>
                        <br>
                     </div>
                  </div>
               </div>

            @endif

            @php
             $previous_month = date('n-Y');
             $previous_month_arr = explode("-", $previous_month);
             $list_month = $previous_month_arr[0];
             $list_year = $previous_month_arr[1];
          @endphp
          <div class="col-md-3 load-table " data-table="property-default-payer-table" data-title="OFFENE POSTEN" data-url="{{ route('get_default_payers') }}?month={{$list_month}}&year={{$list_year}}&status=0&amcheck=1">
             <div class="card pointer-cursor yellow-bg">
                <div class="card-body">
                   <h5 class="card-title text-uppercase">OP fehlt</h5>
                   <div class="text-right">
                      <h2 style="line-height: 23px;font-size: 20px;" class="mt-2 display-7 default-payer-total">....</h2>
                   </div>
                   <br>
                </div>
             </div>
          </div>

          {{-- <div class="col-md-3 load-table" id="invoice" data-table="table-property-invoice-am" data-title="Freizugebende Rechnungen" data-url="{{ route('getPropertyInvoiceRequest') }}">
             <div class="card pointer-cursor" >
                <div class="card-body">
                   <h5 class="card-title text-uppercase">Freizugebende Rechnungen</h5>
                   <div class="text-right">
                      <h2 class="mt-2 display-7 invoice-approval-total">....</h2>
                   </div>
                   <br>
                </div>
             </div>
          </div> --}}

        

        </div>

        <div class="clearfix"></div>
        

        <?php
        $year = range(2018,date('Y'));
        $months = range(1,12);
        ?>
        

        <div class="row">
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" id="neue-div">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Vermietungsaktivitäten</h3>
                    <h3 class="box-title m-b-0"> Neue 
                        <select class="type-selection achange-select achange-select-trigger">
                            <option value="0">All</option>
                            <option value="1">Mietverträge</option>
                            <option value="2">Verlängerung </option>
                        </select>

                    <select class="pa-month achange-select">
                        <option value="">All</option>
                        @foreach($months as $list)
                        <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                        @endforeach
                    </select>
                        <!-- {{__('dashboard.'.date('F'))}} -->
                    <select class="pa-year achange-select">
                        @foreach($year as $list)
                        <option 
                        @if($list==date('Y')) selected @endif 
                        value="{{$list}}">{{$list}}</option>
                        @endforeach
                    </select>
                    <a href="javascript:void(0);" data-url="{{route('monthyearassetmanager')}}?export=1&month=&year={{date('Y')}}&section_type=Vermietungsaktivitäten" class="pull-right btn btn-success btn-export">Export</a>
                </h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    
                    <div class="table-responsive asset-manager-property-list">
                        
                    </div>
                    <!-- <div class="table-responsive ">
                        
                    </div> -->
                </div>
            </div>
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" id="vermietet">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Bestandsobjekte</h3>
                    <div class="table-responsive assetmanagement-list">
                        
                    </div>
                </div>
            </div>
        </div>




        <div class="">
            <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box" id="categ">
                    <h3 class="box-title m-b-0">Mieterübersicht</h3>
                    <h3 class="box-title m-b-0">
                        <a href="javascript:void(0);" data-url="{{route('getcategoryproperty')}}?export=1" class="pull-right btn btn-success btn-export">Export</a>
                    </h3>
                    <div class="clearfix"></div>
                    <div class="table-responsive category-prop-list2">
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Mieterübersicht</h3>
                    <div id="high-container3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;" id="category-prop-list4-div">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Mieterübersicht (Corona)</h3>
                    <div class="clearfix"></div>
                    <div class="table-responsive category-prop-list4">
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;" id="category-prop-list5-div">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Mieterübersicht (Corona)</h3>
                    <div class="clearfix"></div>
                    <div class="table-responsive category-prop-list5">
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;" id="category-prop-list6-div">
                 <div class="white-box">
                    <h3 class="box-title m-b-0">Mieterübersicht gesamt</h3>
                    <div class="clearfix"></div>
                    <div class="table-responsive category-prop-list6">
                    </div>
                 </div>
              </div>
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box" id="bankenfinan">
                    <h3 class="box-title m-b-0">Bankenfinanzierungen</h3>
                    <div class="table-responsive bank-prop-list">
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box " id="geb">
                    <h3 class="box-title m-b-0">Gebäude Versicherung</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive insurance-div1">

                    </div>
                </div>
            </div>
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" id="expiring-building-insurance-div">

                <div class="white-box " id="geb2">
                    <h3 class="box-title m-b-0">Auslaufende Gebäudeversicherungen</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive">
                        <table class="table table-striped dashboard-table class-data-table">
                        <thead>
                        <tr>
                            <th>Objekt</th>
                            <th>AM</th>
                            <th>Name</th>
                            <th>Kommentare</th>
                            <th>Betrag</th>
                            <th>Laufzeit</th>
                            <th>Kündigungsfrist</th>
                            <th>Anzahl</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($g1 as $list)
                            <tr>
                                <td>
                                    <a href="{{route('properties.show',['property'=>$list->id])}}?tab=insurance_tab">
                                        {{$list->name_of_property}}</a>
                                    </td>
                                <td>@if(isset($list->asset_manager))
                                    {{$list->asset_manager->name}}
                                    @endif</td>
                                <td>{{$list->axa}}</td>
                                <td>{{$list->gebaude_comment}}</td>
                                <td>
                                    @if(is_numeric($list->gebaude_betrag))
                                    {{number_format($list->gebaude_betrag,2,",",".")}}
                                    €
                                    @else
                                    {{$list->gebaude_betrag}}
                                    @endif
                                </td>
                                <td>
                                    @if($list->gebaude_laufzeit_to)
                                    {{date_format(  date_create(str_replace('.', '-', $list->gebaude_laufzeit_to)) , 'd.m.Y')}}
                                    @else
                                    {{$list->gebaude_laufzeit_to}}
                                    @endif

                                </td>
                                <td>{{$list->gebaude_kundigungsfrist}}</td>
                                <td>
                                    <a href="{{route('properties.show',['property'=>$list->id])}}?tab=test_tab">
                                        {{$list->insurances($list,1)}}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>

                
                </div>
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box" id="haft">
                    <h3 class="box-title m-b-0">HAFTPFLICHT VERSICHERUNG</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive insurance-div2">
                        
                    </div>
                </div>
            </div>
            @if(Auth::user()->email != "l.liebscher@fcr-immobilien.de")
            
                <div class="col-sm-12" style=" padding: 0 5px;">
                    <div class="white-box" id="gabuderelease">
                        <h3 class="box-title m-b-0">Freigabe Gebäudeversicherung</h3>
                        <p class="text-muted m-b-30">&nbsp;</p>
                        <div class="insurance_type_1_div">
                        </div>
                    </div>
                </div>

                <div class="col-sm-12" style=" padding: 0 5px;">
                    <div class="white-box" id="haftrelease">
                        <h3 class="box-title m-b-0">Freigabe Haftpflichtversicherung</h3>
                        <p class="text-muted m-b-30">&nbsp;</p>
                        <div class="insurance_type_2_div">
                        </div>
                    </div>
                </div>

                <!-- <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                    <div class="white-box" id="hau">
                        <h3 class="box-title m-b-0">Hausverwaltung Kosten</h3>
                        <p class="text-muted m-b-30">&nbsp;</p>
                        <div class="table-responsive ">
                           <table class="table table-striped dashboard-table" id="hausmax_table">
                              <thead>
                                 <tr>
                                    <th>Name</th>
                                    <th>Anzahl</th>
                                    <th>Betrag/Monat</th>
                                    <th>Betrag/Jahr</th>
                                    <th>IST-NME</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 @if(isset($hausmax_properties))
                                 @php $sum=0; $rent_sum = 0;@endphp
                                 @foreach($hausmax_properties as $hausmax)
                                 <?php
                                    if(is_numeric($hausmax->sumhausmaxeuro_monat))
                                     $sum +=$hausmax->sumhausmaxeuro_monat;    
                                    
                                    ?>
                                 @php
                                 if(is_null($hausmax->hausmaxx))
                                 {
                                 continue;
                                 }
                                 @endphp
                                 <tr>
                                    <td>
                                       @php 
                                       $tendency_division = 0;
                                       $tendency_item = \App\Http\Controllers\HomeController::hausmaxx_tenancy_schedule_items($hausmax->hausmaxx);
                                       if($tendency_item > 0){
                                       if($hausmax->sumhausmaxeuro_monat > 0){
                                       $tendency_division = $hausmax->sum*12/$tendency_item*100;
                                       }
                                       }
                                       $rent_sum +=$tendency_item;
                                       @endphp
                                       <a href="javascript:void(0)" class="get-hausmaxx-data" data-id="{{ $hausmax->hausmaxx }}" >{{ $hausmax->hausmaxx }}</a>
                                    </td>
                                    <td class="text-right">{{$hausmax->properties_count}}</td>
                                    <td class="text-right">{{number_format($hausmax->sumhausmaxeuro_monat,2,",",".")}}€</td>
                                    <td class="text-right">{{number_format($hausmax->sumhausmaxeuro_monat*12,2,",",".")}}€</td>
                                    <td class="text-right">{{number_format($tendency_item,2,",",".")}}€</td>
                                 </tr>
                                 @endforeach
                                 @endif
                              </tbody>
                              <tr>
                                 <th>Summe</th>
                                 <th></th>
                                 <th class="text-right">{{ number_format( $sum, 2 ,",",".") }}€</th>
                                 <th class="text-right">{{ number_format( $sum*12, 2 ,",",".") }}€</th>
                                 <th class="text-right">{{ number_format( $rent_sum, 2 ,",",".") }}€</th>
                              </tr>
                           </table>
                           <form id="export-to-excel-form" method="post" action="{{url('export-to-excel-haus')}}">
                              <input type="hidden" name="_token" value="{{csrf_token()}}">
                              <input type="hidden" name="property_data" id="property-data" value="">
                              {{-- <button type="button" class="btn-export-to-excel btn btn-primary" >Export </button> --}}
                              <button type="button" data-url="" data-name="HAUSVERWALTUNG KOSTEN" data-post="1" data-form-id="export-to-excel-form" class="btn btn-primary btn-export">Export</button>
                           </form>
                        </div>
                        <div class="HAUSVERWALTUNG" style="display: none">{{number_format($sum*12,2,",",".")}}€</div>
                    </div>
                </div> -->

            @endif
            


            <div class="col-sm-12" style="padding: 0 5px;" id="expired-liability-insurance-div">
                <div class="white-box" id="haft2">
                    <h3 class="box-title m-b-0">Auslaufende Haftpflichtversicherungen</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive">

                        <table class="table table-striped dashboard-table class-data-table">
                        <thead>
                        <tr>
                            <th>Objekt</th>
                            <th>AM</th>
                            <th>Name</th>
                            <th>Kommentare</th>
                            <th>Betrag</th>
                            <th>Laufzeit</th>
                            <th>Kündigungsfrist</th>
                            <th>Anzahl</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($g2 as $list)
                            <tr>
                                <td><a href="{{route('properties.show',['property'=>$list->id])}}?tab=insurance_tab">
                                        {{$list->name_of_property}}</a></td>
                                
                                <td>
                                    @if(isset($list->asset_manager))
                                    {{$list->asset_manager->name}}
                                    @endif
                                    </td>
                                    <td>{{$list->allianz}}</td>
                                    <td>{{$list->haftplicht_comment}}</td>
                                <td>    
                                    @if(is_numeric($list->haftplicht_betrag))
                                    {{number_format($list->haftplicht_betrag,2,",",".")}}
                                    €
                                    @else
                                    {{$list->haftplicht_betrag}}
                                    @endif
                                </td>
                                <td>
                                    @if($list->haftplicht_laufzeit_to)
                                    {{date_format(  date_create(str_replace('.', '-', $list->haftplicht_laufzeit_to)) , 'd.m.Y')}}
                                    @else
                                    {{$list->haftplicht_laufzeit_to}}
                                    @endif
                                </td>
                                <td>{{$list->haftplicht_kundigungsfrist}}</td>

                                <td>
                                    <a href="{{route('properties.show',['property'=>$list->id])}}?tab=test_tab">
                                        {{$list->insurances($list,2)}}</a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                        
                    </div>
                </div>
            </div>

            <!--<div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box" id="hau">
                    <h3 class="box-title m-b-0">Hausverwaltung Kosten</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive ">

                        <table class="table table-striped dashboard-table" id="hausmax_table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Anzahl</th>
                            <th>Betrag/Monat</th>
                            <th>Betrag/Jahr</th>
                            <th>IST-NME</th>
                            
                        </tr>
                        </thead>
                        <tbody>
                    @if(isset($hausmax_properties))
                     @php $sum=0; $rent_sum = 0;@endphp
                    @foreach($hausmax_properties as $hausmax)
                    

                    <?php

                    if(is_numeric($hausmax->sumhausmaxeuro_monat))
                     $sum +=$hausmax->sumhausmaxeuro_monat;    
                    
                    ?>
                    @php
                    
                        if(is_null($hausmax->hausmaxx))
                        {
                        continue;
                        }
                    @endphp
                            
                    <tr>
                        <td>
                    @php 
                     $tendency_division = 0;
                     $tendency_item = \App\Http\Controllers\HomeController::hausmaxx_tenancy_schedule_items($hausmax->hausmaxx);
                       if($tendency_item > 0){
                            if($hausmax->sumhausmaxeuro_monat > 0){
                            $tendency_division = $hausmax->sum*12/$tendency_item*100;
                            }
                         }
                        $rent_sum +=$tendency_item;
                     @endphp
                            <a href="javascript:void(0)" class="get-hausmaxx-data" data-id="{{ $hausmax->hausmaxx }}" >{{ $hausmax->hausmaxx }}</a></td>
                            <td class="text-right">{{$hausmax->properties_count}}</td>
                        <td class="text-right">{{number_format($hausmax->sumhausmaxeuro_monat,2,",",".")}}€</td>

                        <td class="text-right">{{number_format($hausmax->sumhausmaxeuro_monat*12,2,",",".")}}€</td>
                        <td class="text-right">{{number_format($tendency_item,2,",",".")}}€</td>
                        
                    </tr>
                    @endforeach
                    @endif
                        
                        </tbody>
                        <tr>
                        <th>Summe</th>
                        <th></th>
                        <th class="text-right">{{ number_format( $sum, 2 ,",",".") }}€</th>
                        <th class="text-right">{{ number_format( $sum*12, 2 ,",",".") }}€</th>
                        <th class="text-right">{{ number_format( $rent_sum, 2 ,",",".") }}€</th>
                        </tr>
                    </table>
                    <form id="export-to-excel-form" method="post" action="{{url('export-to-excel-haus')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="property_data" id="property-data" value="">
                        {{-- <button type="button" class="btn-export-to-excel btn btn-primary" >Export </button> --}}
                        <button type="button" data-url="" data-name="HAUSVERWALTUNG KOSTEN" data-post="1" data-form-id="export-to-excel-form" class="btn btn-primary btn-export">Export</button>
                    </form>
                    </div>
                    <div class="HAUSVERWALTUNG" style="display: none">{{number_format($sum*12,2,",",".")}}€</div>
                </div>
            </div>-->


            <div class="col-sm-6" style=" padding: 0 5px;">
                <div class="white-box" id="mahnung">
                    <h3 class="box-title m-b-0">Mahnung</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive mahnung-div">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" id="expired-rental-agreement">
                <div class="white-box" id="abs">
                    <h3 class="box-title m-b-0">ABGELAUFENE MIETVERTRÄGE</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive">
                        <table id="tenant-table3" class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>AM</th>
                                <th>Objekt</th>
                                <th>Vermietet</th>
                                <th>Mietende</th>
                                <th>Fläche (m2)</th>
                                <th>IST-Nkm (€)</th>
                                <th>Typ</th>
                                <th>Kommentare</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php 
                                    $sumau = $amount = $total_expired_rental_agreement = 0; 
                                @endphp  
                                @foreach($tenancy_items_2 as $tenancy_item)
                                    @if($tenancy_item->status && $tenancy_item->rent_end && $tenancy_item->rent_end <= date('Y-m-d'))
                                        <?php
                                            $tenancy_item->creator_name = short_name($tenancy_item->creator_name);
                                        ?>
                                        <tr>
                                            <td>{{$tenancy_item->id}}</td>
                                            <td>{{$tenancy_item->creator_name}}</td>
                                            <td>
                                                <a href="{{route('properties.show',['property'=>$tenancy_item->property_id])}}">
                                                {{$tenancy_item->object_name}}</a></td>
                                            <td><a href="javacript:void(0)" data-id="{{$tenancy_item->id}}" class="tenancy_item">{{$tenancy_item->name}}</a></td>
                                            <td>{{$tenancy_item->rent_end}}</td>
                                            <td class="text-right">{{$tenancy_item->rental_space ? number_format($tenancy_item->rental_space,2,",",".") : 0}}</td>
                                            <td class="text-right">{{$tenancy_item->actual_net_rent ? number_format($tenancy_item->actual_net_rent,2,",",".") : 0}}</td>
                                            <td>{{$tenancy_item->type}}</td>
                                            <td>{{$tenancy_item->comment}}</td>
                                        </tr>
                                        <?php
                                            $sumau += 1; 
                                            $amount +=$tenancy_item->actual_net_rent;
                                            $total_expired_rental_agreement++;
                                        ?>
                                    @endif
                                @endforeach

                            </tbody>
                        </table>
                        {{-- <script> document.getElementById('total_expired_rental_agreement').innerHTML = '{{ $total_expired_rental_agreement }}'; </script> --}}
                        <div class="sumaus" style="display: none">{{number_format($amount,2,',','.')}}€ ({{ $sumau }})</div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" id="expiring_leases_div">
                <div class="white-box" id="aus">
                    <h3 class="box-title m-b-0">Auslaufende Mietverträge</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive">
                        <table id="tenant-table2" class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Asset Manager</th>
                                <th>Objekt</th>
                                <th>Vermietet</th>
                                <th>Mietende</th>
                                <th>Mietfläche (m2)</th>
                                <th>IST-Nettokaltmiete (€)</th>
                                <th>Typ</th>
                                <th>Kommentare</th>
                            </tr>
                            </thead>
                            <tbody>
                                @php $sumau = $amount = $total_expiring_leases = 0; @endphp  
                                @foreach($tenancy_items_2 as $tenancy_item)
                                    @if(!$tenancy_item->rent_end || $tenancy_item->rent_end>date('Y-m-d'))
                                        <?php $tenancy_item->creator_name = short_name($tenancy_item->creator_name); ?>
                                        <tr>
                                            <td>{{$tenancy_item->id}}</td>
                                            <td>{{$tenancy_item->creator_name}}</td>
                                            <td>
                                                <a href="{{route('properties.show',['property'=>$tenancy_item->property_id])}}">
                                                {{$tenancy_item->object_name}}</a></td>
                                            <td><a href="javacript:void(0)" data-id="{{$tenancy_item->id}}" class="tenancy_item">{{$tenancy_item->name}}</a></td>
                                            <td>{{$tenancy_item->rent_end}}</td>
                                            <td class="text-right">{{$tenancy_item->rental_space ? number_format($tenancy_item->rental_space,2,",",".") : 0}}</td>
                                            <td class="text-right">{{$tenancy_item->actual_net_rent ? number_format($tenancy_item->actual_net_rent,2,",",".") : 0}}</td>
                                            <td>{{$tenancy_item->type}}</td>
                                            <td>{{$tenancy_item->comment}}</td>
                                        </tr>
                                        @php
                                            $total_expiring_leases++;
                                        @endphp
                                    @endif
                                    @php 
                                        if($tenancy_item->rent_end && $tenancy_item->rent_end>=date('Y-m-d') && $tenancy_item->rent_end < date('Y-m-d',strtotime('+3 months'))){
                                            $sumau += 1; 
                                            $amount +=$tenancy_item->actual_net_rent;
                                        }
                                    @endphp 
                                @endforeach
                            </tbody>
                        </table>
                        {{-- <script> document.getElementById('total_expiring_leases').innerHTML = '{{ $total_expiring_leases }}'; </script> --}}
                        <div class="sumau" style="display: none">{{number_format($amount,2,',','.')}}€ ({{ $sumau }})</div>
                        <input type="hidden" id="sum-tenancy-amount" value="">
                    </div>
                </div>
            </div>

            
            {{-- <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box" id="release-invoice-box">
                    <h3 class="box-title m-b-0">Freigegebene Rechnungen</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive release-invoice-div">

                    </div>
                </div>
            </div> --}}

            @if(Auth::user()->email != "t.raudies@fcr-immobilien.de")
                <!--<div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                    <div class="white-box" id="open-invoice-box">
                        <h3 class="box-title m-b-0">Offene Rechnungen AM</h3>
                        <p class="text-muted m-b-30">&nbsp;</p>
                        <div class="table-responsive">
                            <table id="table-property-open-invoice" class="table table-striped" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Objekt</th>
                                        <th>Rechnung</th>
                                        <th>Re.D.</th>
                                        <th>Re.Bet.</th>
                                        <th>Kommentar</th>
                                        <th>User</th>
                                        <th>Datum</th>
                                        {{-- <th>Ablehnen AM</th> --}}
                                        {{-- <th>Freigabe Falk</th> --}}
                                        <th style="min-width: 175px;">Freigabe AM</th>
                                        <th style="min-width: 175px;">Freigabe HV</th>
                                        <th style="min-width: 175px;">Freigabe</th>
                                        <th style="min-width: 175px;">Freigabe Falk</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>-->
            @endif
            {{-- <div class="col-sm-12" style=" padding: 0 5px;">
                <div class="white-box" id="pending-invoice">
                    <h3 class="box-title m-b-0">Pending Rechnungen</h3>
                        <p class="text-muted m-b-30">&nbsp;</p>
                        <div class="table-responsive property-pending-invoice-div">
                        </div>
                </div>
            </div>

            <div class="col-sm-12" style=" padding: 0 5px;">
                <div class="white-box" id="notrelease-invoice">
                    <h3 class="box-title m-b-0">Nicht Freigegeben</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive property-notrelease-invoice-div">
                    </div>
                </div>
            </div> --}}

                @if(Auth::user()->email == "t.raudies@fcr-immobilien.de")

                  <div class="col-sm-12" style=" padding: 0 5px;">
                     <div class="white-box" id="liquiplanung">
                        <h3 class="box-title m-b-0">Cashflow</h3>
                        {{-- <p class="text-muted m-b-30">&nbsp;</p> --}}
                        <div class="table-responsive liquiplanung-div">
                        </div>
                     </div>
                  </div>

                  <div class="col-sm-12" style=" padding: 0 5px;">
                    <div class="white-box" id="liquiplanung-1">
                        <h3 class="box-title m-b-0">Liquiplanung</h3>
                        {{-- <button type="button" class="btn btn-primary btn-sm" id="reload-liquiplanung-1-div">reload</button> --}}
                        {{-- <p class="text-muted m-b-30">&nbsp;</p> --}}
                        <div class="table-responsive liquiplanung-1-div">
                        </div>
                    </div>
                </div>

                @endif

            @if(false && Auth::user()->email == "u.wallisch@fcr-immobilien.de")

                <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                    <div class="white-box" id="provision-2">
                       <h3 class="box-title m-b-0">Provisionsfreigaben</h3>
                       <p class="text-muted m-b-30">&nbsp;</p>
                       <div class="table-responsive property-provision-div-2">
                       </div>
                    </div>
                </div>

            @endif    

            
            @if(Auth::user()->email == "j.klausch@fcr-immobilien.de")

                {{-- <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                    <div class="white-box" id="provision-2">
                       <h3 class="box-title m-b-0">Provisionsfreigaben</h3>
                       <p class="text-muted m-b-30">&nbsp;</p>
                       <div class="table-responsive property-provision-div-2">
                       </div>
                    </div>
                </div> --}}

                {{-- <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                    <div class="white-box" id="provision-3">
                       <h3 class="box-title m-b-0">Freigegebene Provisionen</h3>
                       <p class="text-muted m-b-30">&nbsp;</p>
                       <div class="table-responsive property-provision-div-3">
                       </div>
                    </div>
                </div> --}}

                {{-- <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                    <div class="white-box" id="provision-3">
                       <h3 class="box-title m-b-0">Freigegebene Provisionen</h3>
                        <h3 class="box-title m-b-0"> 
                            
                        <select class="pp-month pchange-select">
                            <option value="">All</option>
                            @foreach($months as $list)
                            <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                            @endforeach
                        </select>
                            <!-- {{__('dashboard.'.date('F'))}} -->
                        <select class="pp-year pchange-select">
                            @foreach($year as $list)
                            <option 
                            @if($list==date('Y')) selected @endif 
                            value="{{$list}}">{{$list}}</option>
                            @endforeach
                        </select>
                    </h3>
                       <p class="text-muted m-b-30">&nbsp;</p>
                       <div class="table-responsive property-provision-div-4">
                       </div>
                    </div>
                </div> --}}

                <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                   <div class="white-box" id="default_payer">
                      <h3 class="box-title m-b-0">Offene Posten</h3>
                       <h3 class="box-title m-b-0">

                       <?php 
                          $previous_month = date('m-Y', strtotime('-1 month'));
                          $previous_month_arr = explode("-", $previous_month);
                          $list_month = $previous_month_arr[0];
                          $list_year = $previous_month_arr[1];
                       ?>
                        
                           
                       <select class="bb-month bchange-select">
                           <option value="">All</option>
                           @foreach($months as $list)
                           <option
                           @if($list==$list_month) selected @endif
                            value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                           @endforeach
                       </select>
                           <!-- {{__('dashboard.'.date('F'))}} -->
                       <?php
                       $year = range(2020,date('Y')+5);
           
                       ?>
                       <select class="bb-year bchange-select">
                           @foreach($year as $list)
                           <option 
                           @if($list==$list_year) selected @endif 
                           value="{{$list}}">{{$list}}</option>
                           @endforeach
                       </select>
                   </h3>
                      <p class="text-muted m-b-30">&nbsp;</p>
                      <div class="table-responsive property-default-payer-div">
                      </div>
                   </div>
               </div>


            @endif

            @if(Auth::user()->email == "u.wallisch@fcr-immobilien.de")

                <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                    <div class="white-box" id="provision-5">
                       <h3 class="box-title m-b-0">Freigegebene Provisionen</h3>
                       <p class="text-muted m-b-30">&nbsp;</p>
                       <div class="table-responsive property-provision-div-5">
                       </div>
                    </div>
                </div>
                <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                    <div class="white-box" id="provision-3">
                       <h3 class="box-title m-b-0">Freigegebene Provisionen</h3>
                        <h3 class="box-title m-b-0"> 
                            
                        <select class="pp-month pchange-select">
                            <option value="">All</option>
                            @foreach($months as $list)
                            <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                            @endforeach
                        </select>
                            <!-- {{__('dashboard.'.date('F'))}} -->
                        <select class="pp-year pchange-select">
                            @foreach($year as $list)
                            <option 
                            @if($list==date('Y')) selected @endif 
                            value="{{$list}}">{{$list}}</option>
                            @endforeach
                        </select>
                    </h3>
                       <p class="text-muted m-b-30">&nbsp;</p>
                       <div class="table-responsive property-provision-div-4">
                       </div>
                    </div>
                </div>

            @endif

        </div>


            
            @if(1==2)
            <div class="col-md-6">
                <div class="card">
                    <div class="p-4 border-bottom" style="padding: 10px 15px;">
                        <div class="d-flex align-items-center">
                            <div>
                                <h4 class="mb-0 font-light">Mieteinnahmen Monat</h4>
                                <h2 class="mb-0 font-medium">XXX.XX€</h2>
                            </div>
                            {{--  <div class="ml-auto">
                                 <select class="form-control">
                                     <option>January 2018</option>
                                     <option>February 2018</option>
                                     <option>March 2018</option>
                                 </select>
                             </div> --}}
                        </div>
                    </div>
                    <div class="card-body bg-light" style="padding-bottom: 25px;     height: 400px;overflow-y: scroll;">
                        @foreach($total_rarnings as $total_earn)
                            <div class="d-flex align-items-center py-3">
                                @if($total_earn->image != null)
                                    <img src="{{(config('upload.avatar_path').$total_earn->image)}}" class="rounded-circle" width="60" style="border-radius: 50%;">
                                @else
                                    <img src="{{ url('user-images/2.png') }}" class="rounded-circle" width="60" style="border-radius: 50%;">
                                @endif
                                <div class="ml-3">
                                    <h4 class="font-normal mb-0">{{ $total_earn->name }}</h4>
                                    {{-- <span class="text-muted">10-11-2016</span> --}}
                                </div>
                                <div class="ml-auto">
                                    <h2 class="mb-0 text-info font-medium">${{  number_format($total_earn->total_sum) }}</h2>
                                </div>
                            </div>



                        @endforeach



                    </div>
                </div>
            </div>
            @endif


            <div class="row">
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Kommentare</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="row">
                        <div class="col-sm-6" style="height:440px; overflow-y: scroll;">
                            @foreach($comments as $comment)


                            <div class="d-flex flex-row comment-row mt-0 mb-0">
                                <div class="p-2">
                                    <img src="{{(config('upload.avatar_path').$comment->user->image)}}" alt="user" width="40" class="rounded-circle" style="border-radius: 50%;">
                                </div>
                                <div class="comment-text w-100">
                                    <h5 class="font-normal mb-1">{{$comment->user->name}}</h5>
                                    <span class="text-muted mr-2 font-12">{{ date('d M Y h.iA', strtotime($comment->created_at)) }}</span>
                                    <span class="hidden badge @if($comment->status == 'Steht aus') badge-info @endif @if($comment->status == 'Akzeptiert') badge-success @endif @if($comment->status == 'Abgelehnt') badge-danger @endif badge-rounded text-uppercase font-medium">{{ $comment->status }}</span>
                                    <p class="mb-2 d-block font-14 text-muted font-light mt-3" style="">

                                    <a  href="{{route('properties.show',['property'=>$comment->property_id])}}?tab=comment_tab">
                                    {{ $comment->name_of_property }} :
                                    </a>
                                    <a href="{{route('properties.show',['property'=>$comment->property_id])}}?tab=comment_tab">
                                    {!! $comment->comment !!}
                                    </a>


                                    
                                    </p>



                                    @if($comment->pdf_file)
                                    <a href="{{ asset('pdf_upload/'.$comment->pdf_file) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a><br/>
                                    @endif

                                    <hr>
                                    {{-- <div class="mt-3">
                                        <a href="javacript:void(0)" class="btn btn btn-rounded btn-outline-success mr-2 btn-sm"><i class="ti-check mr-1"></i>Akzeptieren</a>
                                        <a href="javacript:void(0)" class="btn-rounded btn btn-outline-danger btn-sm"><i class="ti-close mr-1"></i> Ablehnen</a>
                                    </div> --}}
                                </div>
                            </div>

                    @endforeach
                        </div>

                        <div class="col-sm-6">
                            <div class="table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                @foreach($ccomments as $comment)
                                <tr>
                                    <td>
                                        <a  href="{{route('properties.show',['property'=>$comment->property_id])}}?tab=comment_tab">
                                        {{ $comment->name_of_property }} :
                                        </a>
                                        
                                    </td>
                                    <td>{{$comment->name}}</td>
                                    <td>
                                        <a class="comment-detail" href="javacript:void(0)" data-id="{{$comment->id}}">
                                        @if($comment->type==1)
                                        Mahnung
                                        @elseif($comment->type==2)
                                        Gerichtliches Mahnverfahren
                                        @elseif($comment->type==3)
                                        Ratenzahlung/Kündigung
                                        @endif
                                        </a>
                                    </td>
                                    <td>{{$comment->created_at}}</td>
                                </tr>
                                @endforeach
                                @if(1==2)
                                <tr>
                                    <td>Mahnung</td>
                                    <td><input type="checkbox" @if(isset($ccomments[0]['status']) && $ccomments[0]['status']) checked @endif></td>
                                    <td>@if(isset($ccomments[0]['comment'])){{$ccomments[0]['comment']}}@endif
                                    <br>
                                    @if(isset($ccomments[0]['files']) && $ccomments[0]['files'])
                                    <?php
                                    $ar = json_decode($ccomments[0]['files'],true)
                                    ?>
                                    @foreach($ar as $list)
                                    <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
                                    @endforeach
                                    @endif


                                    </td>
                                </tr>
                                <tr>
                                    <td>Gerichtliches Mahnverfahren </td>
                                    <td><input type="checkbox" name="" @if(isset($ccomments[1]['status']) && $ccomments[1]['status']) checked @endif></td>
                                    <td>@if(isset($ccomments[1]['comment'])){{$ccomments[1]['comment']}}@endif
                                    <br>
                                    @if(isset($ccomments[1]['files']) && $ccomments[1]['files'])
                                    <?php
                                    $ar = json_decode($ccomments[1]['files'],true)
                                    ?>
                                    @foreach($ar as $list)
                                    <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
                                    @endforeach
                                    @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ratenzahlung /Kündige</td>
                                    <td><input type="checkbox" name="" @if(isset($ccomments[2]['status']) && $ccomments[2]['status']) checked @endif></td>
                                    <td>@if(isset($ccomments[2]['comment'])){{$ccomments[2]['comment']}}@endif
                                    <br>
                                    @if(isset($ccomments[2]['files']) && $ccomments[2]['files'])
                                    <?php
                                    $ar = json_decode($ccomments[2]['files'],true)
                                    ?>
                                    @foreach($ar as $list)
                                    <a href="{{ asset('pdf_upload/'.$list) }}"  target="_blank" style="font-size: 20px;"><i class="fa fa-file-pdf-o"></i></a>
                                    @endforeach
                                    @endif
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        
        </div>
        
        </div>




        @if(1==2)
        <div class="row">
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;" >
                <div class="white-box">
                    <h3 class="box-title m-b-0">Neue Mietverträge </h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive">
                        <table id="tenant-table_22" class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Asset Manager</th>
                                <th>Objekt</th>
                                <th>Vermietet</th>
                                <th>Datum</th>
                                <th>Mietbeginn</th>
                                <th>Mietfläche (m2)</th>
                                <th>IST-Nettokaltmiete (€)</th>
                                <th>Typ</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tenancy_items_4 as $tenancy_item_33)
                                <tr>
                                    <td>{{$tenancy_item_33->id}}</td>
                                    <td>{{$tenancy_item_33->creator_name}}</td>
                                    <td>{{$tenancy_item_33->object_name}}</td>
                                    <td>{{$tenancy_item_33->name}}</td>
                                    <td>{{$tenancy_item_33->created_at}}</td>
                                    <td>{{$tenancy_item_33->rent_begin}}</td>
                                    <td>{{$tenancy_item_33->rental_space ? number_format($tenancy_item_33->rental_space,2,",",".") : 0}}</td>
                                    <td>{{$tenancy_item_33->actual_net_rent ? number_format($tenancy_item_33->actual_net_rent,2,",",".") : 0}}</td>
                                    <td>{{$tenancy_item_33->type}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endif

        
       



    </div>


    <div class="modal fade" id="modal_op_list" role="dialog" style="z-index: 9999;">
  <div class="modal-dialog" style="width: 80%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">OP LISTE</h4>
      </div>
      <div class="modal-body">
         <h3>Forderungsmanagment</h3>
         <div id="rent-paid-data"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>


    <div class=" modal fade" role="dialog" id="table-data-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase"></h4>
         </div>
         <div class="modal-body table-responsive" style="min-height: 100px;">

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="modal-vacancy-export" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">LEERSTÄNDE (€) EXPORT</h4>
      </div>
      <form id="form-vacancy-export" action="{{ route('vacancy_export') }}">
         <div class="modal-body">

            <input type="hidden" name="ids">

            <label>Email</label>
            <input type="email" name="email" class="form-control" required>
            <br/>

            <label>Subject</label>
            <input type="text" name="subject" class="form-control" value="Leerstandsflächen Exposé" required>
            <br/>

            <label>Nachricht</label>
            <textarea class="form-control" name="message" required></textarea>
            <br/>

         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-primary">Senden</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </form>
    </div>

  </div>
</div>



    <div class="modal fade" id="Modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Choose a Bank</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label>{{__('property.select_a_bank')}}: </label>
                    <select id="bank">

                    </select>
                </div>
                <div class="modal-footer">
                    <button id="modal_save_btn" type="button" class="btn btn-primary">{{__('property.save_changes')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tenant-detail-modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><!-- Mieterliste --></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body tenant-detail">
                    
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="get-tenant-mahnung-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Mieterliste</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body get-tenant-mahnung-list-data">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="cate-pro-listing" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body cate-pro-div">
                    
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="comment-detail-popup" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="comment-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body comment-detail-body">
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="user-list-popup" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body user-list">
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body list-data">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="iproperty-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body insurance-list-data">
                    
                </div>
            </div>
        </div>
    </div>

    {{-- Hausmaxx Modal --}}
    {{-- <div class="modal fade" id="hausmax-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hausmaxx</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body hausmax-list-data">
                    
                </div>
            </div>
        </div>
    </div> --}}

    <div class="modal fade" id="hausmax-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
       <div class="modal-dialog modal-lg" role="document" style="width: 90%;">
          <div class="modal-content">
             <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hausmaxx</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
             </div>
             <div class="modal-body hausmax-list-data">
             </div>
          </div>
       </div>
    </div>

    <div class="modal fade" id="invoice-release-property-modal-am" role="dialog">
       <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
             <div class="modal-body">
                <label class="">Kommentar</label>
                <textarea class="form-control invoice-release-comment-am" name="message"></textarea>
                <br>
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-primary invoice-release-submit-am" data-dismiss="modal" >Senden</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
             </div>
          </div>
       </div>
    </div>
    <div class="modal fade" id="asset-property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body asset-list-data">
                    
                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade" id="rent-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog custom" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Mieter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body list-data-rent">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="vacant-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog custom" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Leerstandsflächen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body vlist-data-rent">
                    
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="invoice-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <p class="i-message hidden">Möchten Sie die Rechnung wirklich an Falk zur Freigabe senden?</p>
            <label class="i-message2">Kommentar</label>
            <textarea class="form-control invoice-release-comment i-message2" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="invoice-reject-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-reject-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-reject-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class=" modal fade" role="dialog" id="liquiplanung-data-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title liquiplanung-data-title"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 table-responsive" id="liquiplanung-data-table-div">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="property_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">

            <div class="row property-comment-section">
               <div class="col-md-12 form-group">
                  <label>Kommentar</label>
                  <textarea class="form-control property-comment" rows="5"></textarea>
               </div>
               <div class="col-md-12 text-center">
                  <button type="button" class="btn btn-primary btn-add-property-comment" data-reload="1" data-record-id="" data-property-id="" data-type="" data-subject="" data-content=''>Senden</button>
               </div>
            </div>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="property_comment_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray" id="th_name">Name</th>
                          <th class="bg-light-gray" id="th_comment">Kommentar</th>
                          <th class="bg-light-gray" id="th_date">Datum</th>
                          <th class="bg-light-gray" id="th_action">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="property_comment_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="insurance_detail_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <form id="insurance_detail_comment_form" action="{{ route('add_property_insurance_comment') }}">
               <div class="row">
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="insurance_detail_comment" name="comment" required></textarea>
                  </div>
               </div>
               <br/>
               <div class="row">
                  <div class="col-md-12 text-center">
                     <button type="submit" class="btn btn-primary">Posten</button>
                  </div>
               </div>
            </form>

            <br>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="insurance_detail_comments_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray">Name</th>
                          <th class="bg-light-gray">Kommentar</th>
                          <th class="bg-light-gray">Datum</th>
                          <th class="bg-light-gray">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="insurance_detail_comments_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="insurance-not-release-am-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-not-release-am-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-not-release-am-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="property-insurance-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control property-insurance-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary property-insurance-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="insurance-pending-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-pending-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-pending-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="insurance-not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="modal-forward-to" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Weiterleiten an</h4>
      </div>
      <form id="form-forward-to" action="{{ route('mail_forward_to') }}">
         <div class="modal-body">

            <input type="hidden" name="property_id">
            <input type="hidden" name="subject">
            <input type="hidden" name="title">
            <input type="hidden" name="content">
            <input type="hidden" name="section_id">
            <input type="hidden" name="section">

            <?php 
               $all_users = DB::table('users')->select('id', 'name')->where('user_deleted', 0)->where('user_status', 1)->get();
            ?>

            <label>User</label>
            <select class="form-control" name="user" style="width: 100%;">
               <option value="">Select User</option>
               @if($all_users)
                  @foreach ($all_users as $usr)
                     <option value="{{ $usr->id }}">{{ $usr->name }}</option>
                  @endforeach
               @endif
            </select>
            <br/>
            <br/>

            <label>Oder E-Mail</label>
            <input type="text" name="email" class="form-control">
            <br/>

            <label>Kommentar</label>
            <textarea class="form-control" name="comment" rows="5" required></textarea>
            <br/>

         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-primary">Senden</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </form>
    </div>

  </div>
</div>

<div class="modal fade" id="pending-modal-am" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-pending-am-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-pending-am-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="item_comment_modal" role="dialog">
   <div class="modal-dialog" style="width: 50%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">

            <div class="row item-comment-section">
               <div class="col-md-12 form-group">
                  <label>Kommentar</label>
                  <textarea class="form-control item-comment" rows="5"></textarea>
               </div>
               <div class="col-md-12 text-center">
                  <button type="button" class="btn btn-primary btn-add-item-comment">Senden</button>
               </div>
            </div>

            <br>

<div class="modal" id="invoice-release-am-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_am') }}" id="invoice-release-am-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-am-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-hv-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_hv') }}" id="invoice-release-hv-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-hv-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-usr-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_user') }}" id="invoice-release-usr-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-usr-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label class="am-list">User</label>
                     <select class="am-list invoice_asset_manager form-control" name="user" required>
                        <option value="">{{__('dashboard.asset_manager')}}</option>
                        @if(isset($active_users) && $active_users)
                           @foreach($active_users as $list1)
                              <option value="{{$list1->id}}">{{$list1->name}}</option>
                           @endforeach
                        @endif
                     </select>
                  </div>
               </div>

               {{-- <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div> --}}

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal" id="invoice-release-falk-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="POST" action="{{ route('invoice_release_falk') }}" id="invoice-release-falk-form">
            <div class="modal-body">

               <div class="row">
                  <div class="col-md-12" id="invoice-release-falk-error"></div>
               </div>

               <div class="row">
                  <div class="col-md-12 form-group">
                     <label>Kommentar</label>
                     <textarea class="form-control" name="comment"></textarea>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary">Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

            <div class="row">
               <div class="col-md-12">
                  <div style="max-height: 300px ;overflow-y: auto;margin-top: 10px;">
                    <table class="forecast-table logtable" id="item_comment_table">
                      <thead>
                        <tr>
                          <th class="bg-light-gray" id="th_name">Name</th>
                          <th class="bg-light-gray" id="th_comment">Kommentar</th>
                          <th class="bg-light-gray" id="th_date">Datum</th>
                          <th class="bg-light-gray" id="th_action">Action</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">
                            <button type="button" class="btn-sm btn" id="item_comment_limit">Show More</button>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
@endsection

@section('js')

    <script type="text/javascript">

        var m           = '{{ date('m') }}';
        var y           = '{{ date('Y') }}';
        var _token      = '{{ csrf_token() }}';
        var login_email = '{{ Auth::user()->email }}';
        var is_falk     = {{ ( Auth::user()->email == config('users.falk_email') ) ? 1 : 0 }};
        
        {{-- URL VARIABLE START --}}
        var url_getPropertyDefaultPayers          = '{{ route('getPropertyDefaultPayers') }}';
        var url_getInsurance                      = '{{ route('getInsurance') }}';
        var url_get_default_payers                = '{{ route('get_default_payers') }}';

        var url_dashboard_counter =  '{{ route('get_dashboard_counter') }}?am=1&counter=default_payer,vacancy,house_management_cost,open_angebote';
      
      
        var url_getcategoryproperty3        = '{{ route('getcategoryproperty3') }}';
        var url_getMahnungAmount            = '{{ route('getMahnungAmount') }}';
        var url_tenant_detail               = '{{ route('tenant-detail') }}';
        var url_assetmanagement             = '{{ route('assetmanagement') }}';
        var url_getMahnungTenant            = '{{ route('getMahnungTenant') }}';
        var url_monthyearmanager            = '{{ route('monthyearmanager') }}';
        var url_monthyearassetmanager       = '{{ route('monthyearassetmanager') }}';
        var url_getmanagerproperty          = '{{ route('getmanagerproperty') }}';
        var url_getassetmanagerproperty     = '{{ route('getassetmanagerproperty') }}';
        var url_getInsurancePropertyList    = '{{ route('getInsurancePropertyList') }}';
        var url_get_hausmaxx_data           = '{{ route('get_hausmaxx_data') }}';
        var url_getbankproperty             = '{{ route('getbankproperty') }}';
        var url_change_comment              = '{{ route('change-comment') }}';
        var url_getusers                    = '{{ route('getusers') }}';
        var url_comment_detail              = '{{ route('comment-detail') }}';
        var url_getMahnungProperties        = '{{ route('getMahnungProperties') }}';
        var url_getPropertyInsurance        = '{{ route('getPropertyInsurance') }}';
        var url_mailchimp                   = '{{ route('mailchimp') }}';
        var url_getPropertyReleaseInvoice   = '{{ route('getPropertyReleaseInvoice') }}';
        var url_makeAsPaid                  = '{{ route('makeAsPaid') }}';
        var url_getPropertyProvision        = '{{ route('getPropertyProvision') }}';
        var url_getcategoryproperty         = '{{ route('getcategoryproperty') }}';
        var url_amdashboard_get_openinvoice = '{{ route('amdashboard_get_openinvoice') }}';
        var url_getAssetPendingNotReleaseInvoice = '{{ route('getAssetPendingNotReleaseInvoice') }}';
        var url_property_invoicereleaseprocedure = '{{ url('property/invoicereleaseprocedure') }}';
        var url_property_invoicemarkreject  = '{{ url('property/invoicemarkreject') }}';
        var url_get_liquiplanung            = '{{ route('get_liquiplanung') }}';
        var url_get_liquiplanung1           = '{{ route('get_liquiplanung_1') }}';
        var url_get_liquiplanung_1_data     = '{{ route('get_liquiplanung_1_data') }}';
        var url_delete_property_comment     = '{{ route('delete_property_comment', ['id' => ':id']) }}';
        var url_add_property_comment        = '{{ route('add_property_comment') }}';
        var url_get_property_comment        = '{{ route('get_property_comment') }}';
        var url_delete_insurance_detail_comment   = '{{ route('delete_insurance_detail_comment', ['id' => ':id']) }}';
        var url_insurance_mark_as_notrelease_am     = '{{ route('insurance_mark_as_notrelease_am') }}';
        var url_insurance_mark_as_notrelease        = '{{ route('insurance_mark_as_notrelease') }}';
        var url_property_dealreleaseprocedure = '{{ url('property/dealreleaseprocedure') }}';
        var url_insurance_mark_as_pending       = '{{ route('insurance_mark_as_pending') }}';
        var url_get_all_users               = '{{ route('get_all_users') }}';
        var url_property_invoicemarkpendingam     = '{{ url('property/invoicemarkpendingam') }}';
        var url_invoice_release_am           = '{{ route('invoice_release_am') }}';
        var url_invoice_release_hv           = '{{ route('invoice_release_hv') }}';
        var url_invoice_release_user         = '{{ route('invoice_release_user') }}';
        var url_invoice_release_falk         = '{{ route('invoice_release_falk') }}';
        var url_getItemComment                    = '{{ route('getItemComment') }}';
      var url_addItemComment                    = '{{ route('addItemComment') }}';
      var url_deleteItemComment                 = '{{ route('deleteItemComment') }}';
      var url_get_all_users                     = '{{ route('get_all_users') }}';
      var url_delete_recommended_comment        = '{{ route('delete_recommended_comment', ['id' => ':id']) }}';
      var url_get_recommended_comment           = '{{ route('get_recommended_comment', ['id' => ':id']) }}';
      var url_delete_property_invoice           = '{{ route("delete_property_invoice", ":id") }}';
        {{-- URL VARIABLE END --}}

    </script>

    <script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
    <script src="{{asset('js/echarts-en.min.js')}}"></script>
    <script src="{{asset('js/highcharts.js')}}"></script>
    <script src="{{asset('js/exporting.js')}}"></script>
    <script src="{{asset('js/custom-datatable.js')}}"></script>
    <script src="{{asset('js/am-dashboard.js')}}"></script>

    {{-- <script src="https://code.highcharts.com/highcharts.js"></script> --}}
    {{-- <script src="https://code.highcharts.com/modules/exporting.js"></script> --}}

    <script>
        $( document ).ready(function() {



            /*------------------ITEM COMMENT START----------------*/
            var item_id;
            var item_type;
            var item_property_id;
            var item_subject;
            var item_content;
            var is_comment_added = false;

            $('body').on('click', '.btn-show-item-comment', function() {
                item_id   = $(this).attr('data-id'); //item id
                item_type = $(this).attr('data-type');//1 = external and 0 = comment

                item_property_id = $(this).attr('data-property-id');
                item_subject     = $(this).attr('data-subject');
                item_content     = $(this).attr('data-content');

                getItemComment();
                $('#item_comment_modal').modal('show');
            });

            $('body').on('click', '.btn-add-item-comment', function() {
                var $this = $(this);
                var comment = $.trim($('.item-comment').val());
                if(comment){

                  var data = {
                      'item_id': item_id,
                      'comment': comment,
                      'type': item_type,
                      'property_id': item_property_id,
                      'subject': item_subject,
                      'content': item_content,
                    };

                  $.ajax({
                    url: url_addItemComment,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                        $($this).prop('disabled', true);
                    },
                    success: function(result) {

                        $($this).prop('disabled', false);

                        if (result.status == true) {

                            $($this).closest('.item-comment-section').find('.item-comment').val('');
                            $($this).closest('.item-comment-section').find('.item-comment').focus();

                            is_comment_added = true;

                            getItemComment();

                        } else {
                            alert(result.message);
                        }
                    },
                    error: function(error) {
                        $($this).prop('disabled', false);
                        alert('Somthing want wrong!');
                    }
                });

                }
            });

            $('body').on('click', '#item_comment_limit', function() {
                var text = $(this).text();
                if(text == 'Show More'){
                    $(this).text('Show Less');
                }else{
                    $(this).text('Show More');
                }
                getItemComment();
            });

            $('body').on('click', '.remove-item-comment', function() {
                var url = $(this).attr('data-url');
                var $this = $(this);
                if(confirm('Are you sure to delete this comment?')){
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'json',
                        success: function(result) {
                            if(result.status){
                                is_comment_added = true;
                                getItemComment();
                            }else{
                                alert(result.message);
                            }
                        },
                        error: function(error) {
                            alert('Somthing want wrong!');
                        }
                    });
                }else{
                    return false;
                }
            });

            $('#item_comment_modal').on('hidden.bs.modal', function () {
              if(is_comment_added){
                is_comment_added = false;
                loadTable(true);
              }
            });

            function getItemComment(){
                var limit_text = $('#item_comment_limit').text();
                var limit = (limit_text == 'Show More') ? 1 : '';

                var new_url = url_getItemComment+'?item_id='+item_id+'&limit='+limit+'&type='+item_type;

                $.ajax({
                    url: new_url,
                    type: 'GET',
                    dataType: 'json',
                    success: function(result) {

                        $('#item_comment_table').find('tbody').empty();

                        if(result){
                            $.each(result, function(key, value) {
                                var clear_url = url_deleteItemComment+'?id='+value.id;
                                var delete_button = (value.user_id == value.login_id) ? '<a href="javascript:void(0);" data-url="'+ clear_url +'" class="btn-xs btn-danger remove-item-comment">Löschen</a></td>' : '';
                                var company = (value.role >= 6 && value.company) ? ' ('+value.company+')' : '';
                                var commented_user = ( (value.name) ? value.name : value.user_name )+''+company;
                                var comment = (item_type == '1') ? value.external_comment : value.comment;
                                var tr = '\
                                    <tr>\
                                        <td>'+ commented_user +'</td>\
                                        <td>'+ comment +'</td>\
                                        <td>'+ value.created_at +'</td>\
                                        <td>'+delete_button+'\
                                    </tr>\
                                ';

                                $('#item_comment_table').find('tbody').append(tr);
                            });
                        }
                    },
                    error: function(error) {
                        console.log({error});
                    }
                });
            }

            /*------------------ITEM COMMENT END----------------*/



            if($('#basic-bar').length > 0){
                //$(function() {
                "use strict";
                // ------------------------------
                // Basic bar chart
                // ------------------------------
                // based on prepared DOM, initialize echarts instance
                var myChart = echarts.init(document.getElementById('basic-bar'));

                // specify chart configuration item and data
                var option = {
                    // Setup grid
                    grid: {
                        left: '1%',
                        right: '2%',
                        bottom: '3%',
                        containLabel: true
                    },

                    // Add Tooltip
                    tooltip : {
                        trigger: 'axis'
                    },

                    legend: {
                        data:['Mieteinnahmen pro Monat (€)','Site B']
                    },
                    toolbox: {
                        show : true,
                        feature : {

                            magicType : {show: true, type: ['line', 'bar']},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    color: ["#2962FF", "#4fc3f7"],
                    calculable : true,
                    xAxis : [
                        {
                            type : 'category',
                            // data : ['Jan','Feb','Mär','Apr','Mai','Jun','Jul','Aug','Sept','Okt','Nov','Dez']
                            data : [{!! implode($displayMonth, ',') !!} ]
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        {
                            name:'Mieteinnahmen pro Monat (€)',
                            type:'bar',
                            data:[{{ implode($year_graph, ',')  }}],
                            markPoint : {
                                data : [
                                    {type : 'max', name: 'Max'},
                                    {type : 'min', name: 'Min'}
                                ]
                            },
                            markLine : {
                                data : [
                                    {type : 'average', name: 'Average'}
                                ]
                            }
                        },
                        // {
                        //     name:'Site B',
                        //     type:'bar',
                        //     data:[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
                        //     markPoint : {
                        //         data : [
                        //             {name : 'The highest year', value : 182.2, xAxis: 7, yAxis: 183, symbolSize:18},
                        //             {name : 'Year minimum', value : 2.3, xAxis: 11, yAxis: 3}
                        //         ]
                        //     },
                        //     markLine : {
                        //         data : [
                        //             {type : 'average', name : 'Average'}
                        //         ]
                        //     }
                        // }
                    ]
                };

                // use configuration item and data specified to show chart
                myChart.setOption(option);
                //}
            }
        });
    </script>

    @if(isset($_REQUEST['section']) && $_REQUEST['section'])
   <script type="text/javascript">
      $(document).ready(function(){
         setTimeout(function(){
            moveTodiv("{{$_REQUEST['section']}}");
         },5000)
      })
   </script>
   @endif
   @if(Auth::user()->email == "j.klausch@fcr-immobilien.de")
   <script type="text/javascript">
   getdbayer();    
   </script>
   @endif
   
@endsection
