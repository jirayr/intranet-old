<?php
function short_name($string)
{
    if($string){
        $arr = explode(" ", $string);
        if(count($arr)>1)
            return substr($arr[0], 0,1).substr($arr[1], 0,1);
        else
            return substr($string, 0,1);
    }
    return $string;
}

?>
{{-- <div class="row">
		  <div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">{{__('dashboard.users')}}</h3>
				<ul class="list-inline two-part">
					<li>
						<div id="sparklinedash"></div>
					</li>
					<li class="">
						{{<i class="ti-arrow-up text-success"></i>}}
						<span class="counter text-success">{{$user}}</span>
					</li>
				</ul>
			</div>
		</div>  

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Vermietet(m2)</h3>
				<ul class="list-inline two-part">
					<li>
						<div class="sparklinedash"></div>
					</li>
					<li class="">
						<span class="text-success">
							{{ number_format($summ_vermietet_m2,0,",",".") }}
						</span>
					</li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Vermietet (%)</h3>
				<ul class="list-inline two-part">
					<li>
						<div class="sparklinedash"></div>
					</li>
					<li class="">
						<span class="text-success">
							{{ number_format($summ_vermietet_percent,2,",",".") }}
						</span>
					</li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Leerstand(m2)</h3>
				<ul class="list-inline two-part">
					<li>
						<div class="sparklinedash"></div>
					</li>
					<li class="">
						<span class="text-success">
							{{ number_format($summ_leerstand_m2,0,",",".") }}
						</span>
					</li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Leerstand(%)</h3>
				<ul class="list-inline two-part">
					<li>
						<div class="sparklinedash"></div>
					</li>
					<li class="">
						<span class="text-success">
							{{ number_format($summ_leerstand_percent,2,",",".") }}
						</span>
					</li>
				</ul>
			</div>
		</div>
	</div>  
	--}}
	<!--/.row -->

<table id="added-month-asset" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Asset Manager</th>
                                <th class="text-right">Anzahl</th>
                                {{-- <th>Leerstand</th> --}}
                                <th class="text-right">Kosten Umbau</th>
                                <th class="text-right">Mietfrei in €</th>

                                <th class="text-right">Miete  p.m</th>
                                <th class="text-right">Miete  p.a.</th>
                                <th class="text-right">Mieteinnahmen</th>
                                <th class="text-right">Leerstand m2</th>
                                <th class="text-right">Potenzial p.a.</th>
                                
                                
                                <!-- <th>1</th> -->
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $sum1 = $sum2 = $sum3 = 0;

                            $total_kosten_umbau = $total_mietfrei = 0;

                            $role = Auth::user()->role;


                            $asset_mng_array = array();

                            $row_array = array();

                            ?>


                            @foreach($asset_manager_array as $key=>$tenancy_item)
                                <?php
                                // if($role==1)
                                    $tenancy_item['name'] = short_name($tenancy_item['name']);


                                if(!isset($asset_mng_array[$key]))
                                {
                                    $asset_mng_array[$key]['name'] = $tenancy_item['name'];
                                    $asset_mng_array[$key]['sum'] = 0;
                                    $asset_mng_array[$key]['sum1'] = 0;
                                    $asset_mng_array[$key]['sum2'] = 0;
                                    $asset_mng_array[$key]['sum3'] = 0;
                                    $asset_mng_array[$key]['sum4'] = 0;
                                    $asset_mng_array[$key]['sum5'] = 0;
                                    $asset_mng_array[$key]['sum6'] = 0;

                                    $asset_mng_array[$key]['kosten_umbau'] = 0;
                                    $asset_mng_array[$key]['mietfrei'] = 0;

                                    $row_array[$key] = "";
                                }

                                $asset_mng_array[$key]['sum'] +=$tenancy_item['count'];
                                if(isset($analytics_array[$key]['vacants']))
                                $asset_mng_array[$key]['sum1'] +=$analytics_array[$key]['vacants'];
                                $asset_mng_array[$key]['sum2'] +=$tenancy_item['amount'];
                                $asset_mng_array[$key]['sum3'] +=$tenancy_item['amount']*12;
                                $asset_mng_array[$key]['sum4'] +=$tenancy_item['camount'];
                                if(isset($analytics_array[$key]['vacancy']))
                                $asset_mng_array[$key]['sum5'] +=$analytics_array[$key]['vacancy'];
                                if(isset($analytics_array[$key]['potential_pm']))
                                $asset_mng_array[$key]['sum6'] +=$analytics_array[$key]['potential_pm'];

                                $asset_mng_array[$key]['kosten_umbau'] +=$tenancy_item['kosten_umbau'];
                                $asset_mng_array[$key]['mietfrei'] +=$tenancy_item['mietfrei'];

                                $total_kosten_umbau += $tenancy_item['kosten_umbau'];
                                $total_mietfrei += $tenancy_item['mietfrei'];

                                ob_start();

                                ?>
                                <tr class="mv hidden">
                                    <td>
                                        <a  data-type="1" class="get-assetmanager-property" data-id="{{$key}}" data-toggle="modal" data-target="#asset-property-list">{{$tenancy_item['name']}} MV</td></a>
                                    <td class="text-right"><a data-type="1" class="get-assetmanager-property" data-id="{{$key}}" data-toggle="modal" data-target="#asset-property-list">{{$tenancy_item['count']}}</a></td>
                                    <!--<td class="text-right">
                                        @if(isset($analytics_array[$key]['vacants']))
                                            <a href="javascript:void(0)" class="get-rents" data-value="{{$key}}" data-type="v" data-property="0">
                                                {{number_format($analytics_array[$key]['vacants'],0,",",".")}}
                                            </a>
                                            <?php $sum2 += $analytics_array[$key]['vacants']; ?>

                                        @else
                                            0
                                        @endif
                                    </td>-->
                                    <td class="text-right">{{number_format($tenancy_item['kosten_umbau'],0,",",".")}}</td>
                                    <td class="text-right">{{number_format($tenancy_item['mietfrei'],0,",",".")}}</td>

                                    <td class="text-right">{{number_format($tenancy_item['amount'],0,",",".")}}€</td>
                                    <td class="text-right">{{number_format($tenancy_item['amount']*12,0,",",".")}}€</td>
                                    <td class="text-right">{{number_format($tenancy_item['camount'],0,",",".")}}€</td>
                                    <!-- <td>{{$tenancy_item['camount']}}</td> -->
                                    <td class="text-right">
                                    @if(isset($analytics_array[$key]['vacancy']))
                                    {{number_format($analytics_array[$key]['vacancy'],2,",",".")}}
                                    <?php
									$sum1 += $analytics_array[$key]['vacancy'];
									?>
                                    @else
                                    0
                                    @endif
                                    </td>

                                    

                                    <td class="text-right">
                                    @if(isset($analytics_array[$key]['potential_pm']))
                                    {{number_format($analytics_array[$key]['potential_pm'],2,",",".")}}
                                    <?php
									$sum3 += $analytics_array[$key]['potential_pm'];
									?>
                                    @else
                                    0
                                    @endif
	                                </td>

                                </tr>
                                <?php
                                $row_array[$key] .= ob_get_clean();
                                ?>
                            @endforeach

                            @foreach($asset_manager_array1 as $key=>$tenancy_item)
                                <?php
                                 // if($role==1)
                                    $tenancy_item['name'] = short_name($tenancy_item['name']);

                                if(!isset($asset_mng_array[$key]))
                                {
                                    $asset_mng_array[$key]['name'] = $tenancy_item['name'];
                                    $asset_mng_array[$key]['sum'] = 0;
                                    $asset_mng_array[$key]['sum1'] = 0;
                                    $asset_mng_array[$key]['sum2'] = 0;
                                    $asset_mng_array[$key]['sum3'] = 0;
                                    $asset_mng_array[$key]['sum4'] = 0;
                                    $asset_mng_array[$key]['sum5'] = 0;
                                    $asset_mng_array[$key]['sum6'] = 0;

                                    $asset_mng_array[$key]['kosten_umbau'] = 0;
                                    $asset_mng_array[$key]['mietfrei'] = 0;

                                    $row_array[$key] = "";
                                }

                                $asset_mng_array[$key]['sum'] +=$tenancy_item['count'];
                                $asset_mng_array[$key]['sum2'] +=$tenancy_item['amount'];
                                $asset_mng_array[$key]['sum3'] +=$tenancy_item['amount']*12;
                                $asset_mng_array[$key]['sum4'] +=$tenancy_item['camount'];

                                $asset_mng_array[$key]['kosten_umbau'] +=$tenancy_item['kosten_umbau'];
                                $asset_mng_array[$key]['mietfrei'] +=$tenancy_item['mietfrei'];

                                $total_kosten_umbau += $tenancy_item['kosten_umbau'];
                                $total_mietfrei += $tenancy_item['mietfrei'];

                                ob_start();
                                ?>
                                <tr class="vl hidden">
                                    <td class="text-right">
                                        <a data-type="2" class="get-assetmanager-property" data-id="{{$key}}" data-toggle="modal" data-target="#asset-property-list">{{$tenancy_item['name']}} VL</td></a>
                                    <td class="text-right"><a data-type="2" class="get-assetmanager-property" data-id="{{$key}}" data-toggle="modal" data-target="#asset-property-list">{{$tenancy_item['count']}}</a></td>
                                    {{-- <td class="text-right">0</td> --}}

                                    <td class="text-right">{{number_format($tenancy_item['kosten_umbau'],0,",",".")}}</td>
                                    <td class="text-right">{{number_format($tenancy_item['mietfrei'],0,",",".")}}</td>


                                    <td class="text-right">{{number_format($tenancy_item['amount'],0,",",".")}}€</td>
                                    <td class="text-right">{{number_format($tenancy_item['amount']*12,0,",",".")}}€</td>
                                    <td class="text-right">{{number_format($tenancy_item['camount'],0,",",".")}}€</td>
                                    
                                    <td class="text-right">0</td>
                                    <td class="text-right">0</td>
                                </tr>
                                <?php
                                $row_array[$key] .= ob_get_clean();
                                ?>
                            @endforeach



                            @foreach($asset_mng_array as $kea=>$row)
                                <tr role="row">
                                <th>{{$row['name']}}</th>
                                <th class="text-right">{{number_format($row['sum'],0,",",".")}}</th>
                                {{-- <th class="text-right">
                                    <a href="javascript:void(0)" class="get-rents" data-value="{{$kea}}" data-type="v" data-property="0">
                                        {{number_format($row['sum1'],0,",",".")}}
                                    </a>
                                </th> --}}

                                <th class="text-right">{{number_format($row['kosten_umbau'],0,",",".")}}</th>
                                <th class="text-right">{{number_format($row['mietfrei'],0,",",".")}}</th>


                                <th class="text-right">{{number_format($row['sum2'],0,",",".")}}€</th>
                                <th class="text-right">{{number_format($row['sum3'],0,",",".")}}€</th>
                                <th class="text-right">{{number_format($row['sum4'],0,",",".")}}€</th>
                                <th class="text-right">{{number_format($row['sum5'],0,",",".")}}</th>
                                <th class="text-right">{{number_format($row['sum6'],0,",",".")}}</th>
                                </tr>

                                {!! $row_array[$kea] !!}
                                
                            

                            @endforeach
                            </tbody>
                            <tfoot>
                            @if($type==0 || $type==1)
                            <tr>
                                <th class="border-top-footer"><a data-type="1" class="get-assetmanager-property " data-id="0" data-toggle="modal" data-target="#asset-property-list">Gesamt MV</a></th>
                                <th class="border-top-footer text-right"><a data-type="1" class="get-assetmanager-property mv-total" data-id="0" data-toggle="modal" data-target="#asset-property-list">
                                {{number_format($sum_mv_count,0,",",".")}}</a></th>
                                {{-- <th class="border-top-footer text-right ">{{number_format($sum2,0,",",".")}}</th> --}}

                                <th class="border-top-footer text-right">{{number_format($total_kosten_umbau,0,",",".")}}</th>
                                <th class="border-top-footer text-right">{{number_format($total_mietfrei,0,",",".")}}</th>

                                <th class="border-top-footer text-right">{{number_format($sum_mv_pm,0,",",".")}}€</th>
                                <th class="border-top-footer text-right">{{number_format($sum_mv_pa,0,",",".")}}€</th>
                                <th class="border-top-footer text-right">{{number_format($sum_mv_inc,0,",",".")}}€</th>
                                <th class="border-top-footer text-right">{{number_format($sum1,0,",",".")}}</th>
                                
                                <th class="border-top-footer text-right">{{number_format($sum3,0,",",".")}}</th>
                            </tr>
                            @endif
                            <?php
                            $clll = "";
                            ?>
                            @if($type==0 || $type==2)
                            <?php
                            
                            if($type==2)
                                $clll = "border-top-footer";

                            ?>
                            <tr>
                                <th class="{{$clll}}"><a data-type="2" class="get-assetmanager-property" data-id="0" data-toggle="modal" data-target="#asset-property-list">Gesamt VL</a></th>
                                <th class="{{$clll}} text-right"><a data-type="2" class="get-assetmanager-property mv-total-gesamt" data-id="0" data-toggle="modal" data-target="#asset-property-list">
                                {{number_format($sum_vl_count,0,",",".")}}</a></th>
                                {{-- <th class="{{$clll}} text-right">0</th> --}}
                                
                                <th class="border-top-footer text-right">{{number_format($total_kosten_umbau,0,",",".")}}</th>
                                <th class="border-top-footer text-right">{{number_format($total_mietfrei,0,",",".")}}</th>

                                <th class="{{$clll}} text-right">{{number_format($sum_vl_pm,0,",",".")}}€</th>
                                <th class="{{$clll}} text-right">{{number_format($sum_vl_pa,0,",",".")}}€</th>
                                <th class="{{$clll}} text-right">{{number_format($sum_vl_inc,0,",",".")}}€</th>
                                <th class="{{$clll}} text-right">0</th>
                                
                                <th class="{{$clll}} text-right">0</th>
                                
                            </tr>
                            @endif

                            <tr>
                                <th class="{{$clll}}">Gesamt</th>
                                <th class="{{$clll}} text-right">
                                {{number_format(($sum_vl_count+$sum_mv_count),0,",",".")}}</th>
                                {{-- <th class="{{$clll}} text-right">{{number_format($sum2,0,",",".")}}</th> --}}
                                
                                <th class="{{$clll}} text-right">{{number_format($total_kosten_umbau,0,",",".")}}</th>
                                <th class="{{$clll}} text-right">{{number_format($total_mietfrei,0,",",".")}}</th>

                                <th class="{{$clll}} text-right">{{number_format($sum_mv_pm+$sum_vl_pm,0,",",".")}}€</th>
                                <th class="{{$clll}} text-right">{{number_format($sum_mv_pa+$sum_vl_pa,0,",",".")}}€</th>
                                <th class="{{$clll}} text-right">{{number_format($sum_vl_inc+$sum_mv_inc,0,",",".")}}€</th>
                                <th class="{{$clll}} text-right">{{number_format($sum1,0,",",".")}}</th>
                                
                                <th class="{{$clll}} text-right">{{number_format($sum3,0,",",".")}}</th>
                                
                            </tr>

                        </tfoot>
                        </table>

                        <div id="high-container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


                        <div id="high-container2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>