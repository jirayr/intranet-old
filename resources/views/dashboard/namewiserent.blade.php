<div class="table-responsive">
	<div class="property-table">
    <table class="table table-striped dashboard-table category-table6">
        <thead>
        <tr>
            <th>Mieter</th>
            <th class="text-right">Anzahl</th>
            <th class="text-right">Miete p.m.</th>
        </tr>
        </thead>
        <tbody>
        <?php $c = $c2 = 0;?>
        @foreach($data3 as $k=>$row)
        <?php
        $c +=$row['count'];
        $c2 += $row['amount']; 
        ?>
        <tr>
            <td>{{$row['name']}}</td>
            <td class="text-right"><a href="javascript:void(0)" class="get-name-properties" data-category="{{base64_encode($row['name'])}}">{{$row['count']}}</a></td>
            <td class="text-right">{{ number_format($row['amount'], 2,",",".") }}</td>
        </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>Gesamt</th>
                <th class="text-right">{{ number_format($c, 0,",",".") }}</th>
                <th class="text-right">{{ number_format($c2, 2,",",".") }}</th>
            </tr>
        </tfoot>
    </table>
    </div>
</div>
