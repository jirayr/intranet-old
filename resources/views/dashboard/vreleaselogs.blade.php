<table class="table table-striped" id="{{$table_id}}">
    <thead>
    <tr>
        <th>PLZ</th>
        <th>Ort</th>
        <th>Verkaufsentscheidung  (AR)</th>
        <th>Verkaufsformalitäten  (AL)</th>
        <th>Verkaufsdatenblatt  (AR)</th>
        <th> Generelle Entscheidung (FR)</th>
        <th>Checkliste nach Verkauf (Verkäufer)</th>
    </tr>
	</thead>
	<?php $c = 0;?>
	<tbody>
		@foreach($name as $k=>$list)
		<tr>
			<td><a href="{{route('properties.show',['property'=>$k])}}">
                                    {{$list['plz']}}</a></td>
			<td><a href="{{route('properties.show',['property'=>$k])}}">
                                    {{$list['ort']}}</a></td>
			<td class="text-center">
				@if(isset($list['btn1']))
				<i class="fa fa-check"></i> {{$list['btn1']}}
				@else
				<i class="fa fa-times"></i>
				@endif
			</td>
			<td class="text-center">
				@if(isset($list['btn2']))
				<i class="fa fa-check"></i> {{$list['btn2']}}
				@else
				<i class="fa fa-times"></i>
				@endif
			</td>
			<td class="text-center">
				@if(isset($list['btn3']))
				<i class="fa fa-check"></i> {{$list['btn3']}}
				@else
				<i class="fa fa-times"></i>
				@endif
			</td>
			<td class="text-center">
				@if(isset($list['btn4']))
				<i class="fa fa-check"></i> {{$list['btn4']}}
				@else
				<?php $c++;?>
				<i class="fa fa-times"></i>
				@endif
			</td>
			<td class="text-center">
				@if(isset($list['btn5']))
				<i class="fa fa-check"></i> {{$list['btn5']}}
				@else
				<i class="fa fa-times"></i>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<input type="hidden" class="vreleasecount" value="{{$c}}">