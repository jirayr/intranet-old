@extends('layouts.admin')
@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">

   {{--  <link href="//wrappixel.com/ampleadmin/assets/libs/chartist/dist/chartist.min.css" rel="stylesheet">
    <link href="//wrappixel.com/ampleadmin/dist/js/pages/chartist/chartist-init.css" rel="stylesheet">
    <link href="//wrappixel.com/ampleadmin/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="//wrappixel.com/ampleadmin/assets/libs/morris.js/morris.css" rel="stylesheet">
    <link href="//wrappixel.com/ampleadmin/dist/css/style.min.css" rel="stylesheet"> --}}
  
    <style type="">
        #added-month-asset tbody th:first-child{
            cursor: pointer;
        }
        @media (min-width: 992px)
        {    .modal-lg {
                width: 1320px;
            }
        }
        @media (min-width: 768px)
        {
            .modal-dialog.custom {
                width: 848px;
                margin: 30px auto;
            }
        }
        .card {
            position: relative;
            display: flex;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 0 solid transparent;
            border-radius: 0;
        }
        .card-body {
            flex: 1 1 auto;
            padding: 8px 12px;
        }
        .card .card-title {
            position: relative;
            font-weight: 500;
            font-size: 16px;
        }
        .d-flex {
            display: flex!important;
        }
        .align-items-center {
            align-items: center!important;
        }
        .ml-auto, .mx-auto {
            margin-left: auto!important;
        }
        .custum-row .col-lg-3, .custum-row .col-md-3{
            padding: 0 5px;
        }

        .custum-row-2{
            margin-top: 25px;
        }

        .custum-row-2 .col-md-6{
            margin: 5px 0;
            padding: 0 5px;
        }
        .preloader{
            display: none;
        }
        .pb-3, .py-3{
            padding-bottom: 20px;
            margin-top: 10px;
        }
        .for-col-padding .col-md-6{
            padding: 0 5px;
        }
        .get-assetmanager-property,.get-manager-property{
            cursor: pointer;
        }
.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}
    </style>


@endsection
@section('content')
    {{-- <link rel="stylesheet" type="text/css" href="https://wrappixel.com/ampleadmin/dist/css/style.min.css">  --}}
    {{-- {{ dd($tenancy_schedule_data) }} --}}
    <div class="page-content container-fluid">


        <?php
        $year = range(2018,date('Y'));
        $months = range(1,12);
        ?>

        {{-- <div class="row">
            <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box">
                    <h3 class="box-title m-b-0"> Neue Mietverträge
                        <select class="pa-month achange-select">
                        <option value="">All</option>
                        @foreach($months as $list)
                        <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                        @endforeach
                    </select>
                         {{__('dashboard.'.date('F'))}} 
                    <select class="pa-year achange-select">
                        @foreach($year as $list)
                        <option 
                        @if($list==date('Y')) selected @endif 
                        value="{{$list}}">{{$list}}</option>
                        @endforeach
                    </select></h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive asset-manager-property-list">
                        
                    </div>
                </div>
            </div>
        </div>--}}


 <div class="row">
<div class="col-sm-8" >
<?php
$sum['count'] = 0;
$sum['flat_in_qm'] = 0;
$sum['rented_area'] = 0;
$sum['vacancy'] = 0;
$sum['Quote'] = 0;
$sum['Miete'] = 0;
$sum['Potenzial'] = 0;

$user = Auth::user();
?>


@foreach($analytics_array as $key => $masterliste)
    @if(strtolower($user->email)=="j.klausch@fcr-immobilien.de" || strtolower($user->email)=="c.musacchio@fcr-immobilien.de" || $masterliste['asset_manager'] == $user->id)   
    <?php
    $sum['count'] += $masterliste['count'];
    $sum['flat_in_qm'] += $masterliste['flat_in_qm'];
    $sum['rented_area'] += $masterliste['rented_area'];
    $sum['vacancy'] += $masterliste['vacancy'];
    if( $sum['flat_in_qm'] != 0 )
        $sum['Quote'] = $sum['vacancy'] / $sum['flat_in_qm'] * 100;
    else
        $sum['Quote'] = 0;

     $sum['Miete'] += $masterliste['rent_net_pm'];
    $sum['Potenzial']+= $masterliste['potential_pm'];
    ?>
         
        @if($is_asset_manager)
        <?php
        if(isset($pa_array2[$masterliste['id']])){
            $sum['Miete'] +=12*$pa_array2[$masterliste['id']];
              number_format(12*$pa_array2[$masterliste['id']],2,",",".");
        }
        ?>
        @else
        <?php 
        if(isset($pa_array[$key])){
            $sum['Miete'] += 12*$pa_array[$key];
              number_format(12*$pa_array[$key],2,",",".");
        }
        else{
              number_format($masterliste['rent_net_pm'],2,",",".");
        }
        ?>
        
        @endif
         
    @endif
@endforeach
 
    <td class="number-right">
         <div class="col-sm-6 col-xs-12">
            <div class="white-box analytics-info">
                <h3 class="box-title">Fläche in qm</h3>
                <ul class="list-inline two-part">
                    <li>
                        <div class="sparklinedash"></div>
                    </li>
                    <li class="text-right">
                        <span class="text-success">
                            {{number_format($sum['flat_in_qm'],2,",",".")}}
                        </span>
                    </li>
                </ul>
            </div>
        </div>
        </td>
 

        <div class="col-sm-6 col-xs-12">
            <div class="white-box analytics-info">
                <h3 class="box-title">Fläche vermietet</h3>
                <ul class="list-inline two-part">
                    <li>
                        <div class="sparklinedash"></div>
                    </li>
                    <li class="text-right">
                        <span class="text-success">
                           {{number_format($sum['rented_area'],2,",",".")}}
                        </span>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-sm-6 col-xs-12">
            <div class="white-box analytics-info">
                <h3 class="box-title">Leerstand</h3>
                <ul class="list-inline two-part">
                    <li>
                        <div class="sparklinedash"></div>
                    </li>
                    <li class="text-right">
                        <span class="text-success">
                            {{number_format($sum['vacancy'],2,",",".")}}                          
                        </span>
                    </li>
                </ul>
            </div>
        </div>
  
            <div class="col-sm-6 col-xs-12">
            <div class="white-box analytics-info">
                <h3 class="box-title">Potenzial p.a.</h3>
                <ul class="list-inline two-part">
                    <li>
                        <div class="sparklinedash"></div>
                    </li>
                    <li class="text-right">
                        <span class="text-success">
                             {{number_format($sum['Potenzial'],2,",",".")}}
                        </span>
                    </li>
                </ul>
            </div>
        </div>
  </div>
    
  <div class="col-sm-4">
        <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body border-bottom" style="padding: 13px 12px;">
                                <h5 class="card-title text-uppercase">Quote</h5>
                                <h2 class="font-medium">{{number_format($sum['Quote'],2)}}% </h2>
                                <div class="gaugejs-box d-flex justify-content-center mt-5 mb-4">
                                    <canvas id="foo" class="gaugejs" height="150" width="300">guage</canvas>
                                </div>
                            </div>
                            <div class="d-flex align-items-center p-3">
                               {{--  <div>
                                    <span class="mb-0 display-7"><span class="font-medium">26.30</span></span>
                                    <h4 class="mb-0 font-light">AMps Used</h4>
                                </div> --}}
                                <div class="ml-auto">
                                    {{-- <button class="btn btn-info btn-circle btn-lg text-white">
                                        <i class="icon-speedometer"></i>
                                    </button> --}}
                                    <br><br>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
     </div>


        <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Leerstandsflächen</h3>
                    <div class="table-responsive">
                        <table id="vacant-table2" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Objekt</th>
                                <th>Leerstand</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($property_count_array as $id=>$list)
                                @if($list['count'])
                                <tr>
                                <td >
                                    <a href="{{route('properties.show',['property'=>$id])}}">
                                    {{$list['name']}}
                                    </a>
                                </td>
                                <td><a href="javascript:void(0)" class="get-rents" data-value="0" data-type="v" data-property="{{$id}}"> {{$list['count']}}
                                </a></td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>

       
        <div class="col-sm-12" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Vermietungsaktivitäten</h3>
                    <h3 class="box-title m-b-0"> Neue 
                        <select class="type-selection achange-select">
                            <option value="0">All</option>
                            <option value="1">Mietverträge</option>
                            <option value="2">Verlängerung </option>
                        </select>

                        <select class="pa-month achange-select">
                        <option value="">All</option>
                        @foreach($months as $list)
                        <option value="{{$list}}">{{__('dashboard.'.date('F',strtotime('2000-'.$list.'-01')))}}</option>
                        @endforeach
                    </select>
                        <!-- {{__('dashboard.'.date('F'))}} -->
                    <select class="pa-year achange-select">
                        @foreach($year as $list)
                        <option 
                        @if($list==date('Y')) selected @endif 
                        value="{{$list}}">{{$list}}</option>
                        @endforeach
                    </select></h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    
                    <div class="table-responsive asset-manager-property-list">
                        
                    </div>
                    <!-- <div class="table-responsive ">
                        
                    </div> -->
                </div>
            </div>
            

            <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Gebäude</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive insurance-div1">
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-6" style="margin-top: 25px; padding: 0 5px;">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Haftplicht</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive insurance-div2">
                        
                    </div>
                </div>
            </div>
            <div class="col-sm-12" style="">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Auslaufende Mietverträge</h3>
                    <p class="text-muted m-b-30">&nbsp;</p>
                    <div class="table-responsive">
                        <table id="tenant-table2" class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Asset Manager</th>
                                <th>Objekt</th>
                                <th>Vermietet</th>
                                <th>Mietende</th>
                                <th>Mietfläche (m2)</th>
                                <th>IST-Nettokaltmiete (€)</th>
                                <th>Typ</th>
                                <th>Kommentare</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tenancy_items_2 as $tenancy_item)
                                <tr>
                                    <td>{{$tenancy_item->id}}</td>
                                    <td>{{$tenancy_item->creator_name}}</td>
                                    <td>
                                        <a href="{{route('properties.show',['property'=>$tenancy_item->property_id])}}">
                                        {{$tenancy_item->object_name}}</a></td>
                                    <td>{{$tenancy_item->name}}</td>
                                    <td>{{$tenancy_item->rent_end}}</td>
                                    <td>{{$tenancy_item->rental_space ? number_format($tenancy_item->rental_space,2,",",".") : 0}}</td>
                                    <td>{{$tenancy_item->actual_net_rent ? number_format($tenancy_item->actual_net_rent,2,",",".") : 0}}</td>
                                    <td>{{$tenancy_item->type}}</td>
                                    <td>{{$tenancy_item->comment}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        


       


        



    </div>





    <div class="modal fade" id="Modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Choose a Bank</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label>{{__('property.select_a_bank')}}: </label>
                    <select id="bank">

                    </select>
                </div>
                <div class="modal-footer">
                    <button id="modal_save_btn" type="button" class="btn btn-primary">{{__('property.save_changes')}}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body list-data">
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="asset-property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body asset-list-data">
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="iproperty-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body insurance-list-data">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="vacant-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog custom" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Leerstandsflächen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body vlist-data-rent">
                    
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
 
 
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script src="{{asset('js/custom-datatable.js')}}"></script>
    
    <script>

        /*
                $(document).ready(function () {

        			$('.table-responsive').find('a.inline-edit').editable({
                        step: 'any',
                        success: function(response, newValue) {
                            if( response.success === false )
                                return response.msg;
                            else{
                                 location.reload();
                            }
                        }
                    });


        			 $('.dashboard-table').DataTable({
        				 "columns": [
        					null,
        					null,
        					null,
        					{ "type": "formatted-num" },
        					{ "type": "formatted-num" },
        					{ "type": "formatted-num" },
        					{ "type": "formatted-num" },
                             { "type": "formatted-num" },
                             { "type": "formatted-num" },
                             { "type": "formatted-num" },
                             { "type": "formatted-num" },
                             { "type": "formatted-num" },
                             { "type": "formatted-num" },
        				]
        			});


        				$.ajaxSetup({
        					headers: {
        						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        					}
        				});

        			var propertyId;
                    $('.property-status').on('change', function () {
        				if($(this).val()=="1"){
        					$('#Modal').modal('show');
        				}
                        selection = $('.property-status');
                        selection.prop( "disabled", true );
                        propertyId = $(this).data('property-id');
                        var status = $(this).val();
                        var notificationType = $(this).data('notification-type');
        //                console.log("Status: " + propertyId);
                        var data = {
                            _token : '<?php echo csrf_token() ?>',
                            status : status,
                            property_id : propertyId
                        };
                        $.ajax({
                            type:'POST',
                            url:'{{route('properties.change_status')}}',
                            data: data,
                            success:function(data){
                                newNotification(propertyId, 3);
        						location.reload();
                            }
                        });
                    });
        			$('#modal_save_btn').on('click', function () {
        			$.ajax({
        					method: "POST",
        					url: "{{url('/property/select_bank')}}" + "/" + propertyId,
        					data: {
        						with_real_ek: $('#bank option:selected').data("with_real_ek"),
        						from_bond: $('#bank option:selected').data("from_bond"),
        						bank_loan: $('#bank option:selected').data("bank_loan"),
        						interest_bank_loan: $('#bank option:selected').data("interest_bank_loan"),
        						eradication_bank: $('#bank option:selected').data("eradication_bank"),
        						interest_bond: $('#bank option:selected').data("interest_bond"),
        						bank: $('#bank option:selected').val()
        						}
        					})
        					.success(function( response, newValue) {
        					if( response.success === false )
                                return response.msg;
                            else
                               $('#Modal').modal('hide');
        					console.log(propertyId);
        				});
        		})


                });
        */

        



        $('body').on('click','.get-rents',function(){
            $('.list-data-rent').html("");
            $('.vlist-data-rent').html("");
            id= $(this).attr('data-value');
            property_id= $(this).attr('data-property');
            type= $(this).attr('data-type');
            
            $('#vacant-list').modal();

            var url = "{{route('assetmanagement')}}?asset_manager="+id+"&property_id="+property_id+"&type="+type;
             $.ajax({
                url: url,
            }).done(function (data) { 
                if(type == "r")
                    $('.list-data-rent').html(data);
                else
                    $('.vlist-data-rent').html(data);

                var columns = [
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" }
                ];
                makeDatatable($('#list-rent'), columns);
            });
        });

        var columns = [
            null,
            null,
            null,
            { "type": "formatted-num" },
            { "type": "non-empty-string" },
            { "type": "formatted-num" },
            { "type": "formatted-num" },
            null,
            null
        ];
        makeDatatable($('#tenant-table2'), columns, "asc", 4);

        var url = "{{route('getPropertyInsurance')}}?axa=1";
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.insurance-div1').html(data);
            setTimeout(function(){
                var columns = [
                    null,
                    null,
                    { "type": "numeric-comma" }
                ];
                makeDatatable($('#insurance-table1'), columns, "desc", 2);
            },1000);
        });

        var url = "{{route('getPropertyInsurance')}}?axa=0";
        $.ajax({
            url: url,
        }).done(function (data) { 
            $('.insurance-div2').html(data);
            setTimeout(function(){
                var columns = [
                    null,
                    null,
                    { "type": "numeric-comma" },
                ];
                makeDatatable($('#insurance-table0'), columns, "desc", 2);
            },1000);
        });


        $('body').on('click','.get-name-insurance',function(){
            id = $(this).attr('data-id');
            t = $(this).html();
            var url = "{{route('getInsurancePropertyList')}}?axa="+id+'&title='+t;
            $.ajax({
                url: url,
            }).done(function (data) { 
                $('#iproperty-list').modal();
                $('.insurance-list-data').html(data);
                setTimeout(function(){
                    makeDatatable($('#insurance-property-table'));
                },1000);
            });  
        });

        $('.change-select').change(function(){
            m = $('.pm-month').val();
            y = $('.pm-year').val();

            var url = "{{route('monthyearmanager')}}?month="+m+"&year="+y;
            $.ajax({
                url: url,
            }).done(function (data) {

                $('.manager-property-list').html(data);
                var columns = [
                    null,
                    null,
                    { "type": "numeric-comma" },
                    null,
                    { "type": "numeric-comma" },
                    null,
                    { "type": "numeric-comma" },
                    null,
                    { "type": "numeric-comma" },
                    null,
                    { "type": "numeric-comma" },
                    null,
                    { "type": "numeric-comma" },
                    null,
                    { "type": "numeric-comma" }
                ];
                makeDatatable($('#added-month'), columns, "asc", 0, 50);
            });
        });

        $('.achange-select').change(function(){
            m = $('.pa-month').val();
            y = $('.pa-year').val();

            var url = "{{route('monthyearassetmanager')}}?month="+m+"&year="+y;
            $.ajax({
                url: url,
            }).done(function (data) { 
                $('.asset-manager-property-list').html(data.table);
                var columns = [
                    null,
                    null,
                    null,
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" },
                    { "type": "numeric-comma" }
                ];
                table = makeDatatable($('#added-month-asset'), columns, "asc", 0, 25);

                // drawchart(data.cat,data.mv,data.vl,data.sum);
                $('#high-container').remove();
            });
        });

        $('body').on('click','.get-manager-property',function(){
            $('.list-data').html("");
            id= $(this).attr('data-id');
            m = $('.pm-month').val();
            y = $('.pm-year').val();
            status= $(this).attr('data-status');


            var url = "{{route('getmanagerproperty')}}?id="+id+"&month="+m+"&year="+y+"&status="+status;
            $.ajax({
                url: url,
            }).done(function (data) { 
                $('.list-data').html(data);
                if(status == 12){

                    var columns = [
                        null,
                        null,
                        { "type": "new-date" },
                        { "type": "new-date" },
                        null,
                        null,
                        null,
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        null
                    ];
                    makeDatatable($('#list-properties'), columns, "asc", 2);

                }else if(status == 14 || status == 10){

                    var columns = [
                        null,
                        null,
                        { "type": "new-date" },
                        { "type": "new-date" },
                        null,
                        null,
                        null,
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        null
                    ];
                    makeDatatable($('#list-properties'), columns, "asc", 2);

                }else{

                    var columns = [
                        null,
                        null,
                        { "type": "new-date" },
                        { "type": "new-date" },
                        null,
                        null,
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        { "type": "numeric-comma" },
                        null
                    ];
                    makeDatatable($('#list-properties'), columns, "asc", 2);
                }
            });
        });

        $('body').on('click','.get-assetmanager-property',function(){
            $('.asset-list-data').html("");
            id= $(this).attr('data-id');

            m = $('.pa-month').val();
            y = $('.pa-year').val();

            t = $(this).attr('data-type');


            var url = "{{route('getassetmanagerproperty')}}?id="+id+"&month="+m+"&year="+y+"&type="+t;
            $.ajax({
                url: url,
            }).done(function (data) { 
                $('.asset-list-data').html(data);
            });
        });


        $(document).ready(function () {
            $('.achange-select').trigger('change');
            makeDatatable($('#vacant-table2'));
        });

        $('body').on('click', '#added-month-asset tbody th:first-child', function () {
              var tr = $(this).closest('tr').next();
              if(tr.hasClass('mv'))
              {
                if(tr.hasClass('hidden'))
                    tr.removeClass('hidden'); 
                else
                    tr.addClass('hidden'); 

                var tr = tr.next();
                if(tr.hasClass('vl'))
                {
                    if(tr.hasClass('hidden'))
                        tr.removeClass('hidden'); 
                    else
                        tr.addClass('hidden'); 
                }


              }
        });


    </script>

    <script src="https://wrappixel.com/ampleadmin/assets/libs/echarts/dist/echarts-en.min.js"></script>

     <script src="//wrappixel.com/ampleadmin/assets/libs/gaugeJS/dist/gauge.min.js"></script>
<script>
    var opts = {
  angle: 0, // The span of the gauge arc
  lineWidth: 0.22, // The line thickness
  radiusScale: 1, // Relative radius
  pointer: {
    length: 0.68, // // Relative to gauge radius
    strokeWidth: 0.068, // The thickness
    color: '#000000' // Fill color
  },
  limitMax: true,     // If false, max value increases automatically if value > maxValue
  limitMin: true,     // If true, the min value of the gauge will be fixed
  colorStart: '#53e69d',   // Colors
  colorStop: '#53E69D',    // just experiment with them
  strokeColor: '#E0E0E0',  // to see which ones work best for you
  generateGradient: true,
  highDpiSupport: true,     // High resolution support
  
};
var target = document.getElementById('foo'); // your canvas element
var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
gauge.maxValue = 100; // set max gauge value
gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
gauge.animationSpeed = 15; // set animation speed (32 is default value)
gauge.set({{number_format($sum['Quote'],2)}}); // set actual value
</script>

@endsection

