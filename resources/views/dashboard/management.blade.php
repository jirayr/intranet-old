@foreach ($property as $key => $value)				
<div class="table-responsive">
<h3>{{$value->name_of_property}}
<button class="btn  btn-xs btn-primary request-insurance-button" data-id="{{$value->property_id}}" data-type="{{$release}}" style="margin-left: 10px;">Freigeben</button>
</h3>
<?php
		$jsonarray = array();
		$total_actual_net_rent = 0;
        if ($value->text_json) {
            $jsonarray = json_decode($value->text_json, true);
        }
        if (isset($jsonarray['total_actual_net_rent'])) {
            $total_actual_net_rent = $jsonarray['total_actual_net_rent'];
        }
$data = DB::table('property_managements')->selectRaw('*')->where('property_id', $value->property_id)->get();
?>

<div class="table-responsive">
	<div class="property-table">
    <table class="table table-striped property-management-table">
        <thead>
        <tr>

            <th>Objekt</th>
            <th>Name</th>
            <th class="text-right">IST Netto Miete p.m.</th>
            <th class="text-right">Prozent(%)</th>
            <th class="text-right"> Fee</th>
            <th>Kommentar</th>
            <th>AM-Empfehlung</th>
            <th>Falk</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $key=>$row)
        <tr>
        	<td>
				<a href="{{route('properties.show',['property'=> $value->property_id])}}?tab=property_management">
	            	{{$value->name_of_property}}
	        	</a>
			</td>
            <td>{{$row->name}}</td>
            <td class="text-right">{{ show_number($total_actual_net_rent, 2) }}</td>
            <td class="text-right">{{show_number($row->percent,2)}}</td>
            <td class="text-right">@if($row->percent) {{show_number($total_actual_net_rent*$row->percent/100,2)}}@endif</td>
            <td>{{$row->comment}}</td>
            <td class="text-center"><input data-column="release_status1" type="checkbox" class="change-property-manage-status" data-id="{{$row->id}}" data-property_id="{{$row->property_id}}" @if($row->release_status1) checked @endif ></td>
            <td class="text-center"><input data-column="release_status2" type="checkbox" class="change-property-manage-status" data-id="{{$row->id}}" data-property_id="{{$row->property_id}}" @if($row->release_status2) checked @endif ></td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>

</div>
@endforeach
<input type="hidden" class="release_management" value="{{count($property)}}">