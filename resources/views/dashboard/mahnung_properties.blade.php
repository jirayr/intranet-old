<table id="mahnung-properties" class="table table-striped">
    <thead>
    <tr>
        <th>Objekt</th>
        <th>Anzahl</th>
        <th>Mahnungsbeträge Gesamt</th>
    </tr>
    </thead>
    <tbody>
    <?php
    ?>
	@foreach($data as $id=>$property)

		<?php
		$pr = \App\Models\Properties::select('name_of_property')->where('standard_property_status',1)->where('main_property_id',$property->property_id)->first();

		if(!$pr)
			continue;
		?>

		@php 
           $amount = 0;
           $tenancy_schedule_data =  \App\Services\TenancySchedulesService::get($property->property_id);
            foreach($tenancy_schedule_data['tenancy_schedules'] as $key => $tenancy_schedule){ 
            foreach($tenancy_schedule->items as $item){
                    if($item->warning_price3)
                    $amount += $item->warning_price3;
                    elseif($item->warning_price2)
                    $amount += $item->actual_net_rent;
            		    elseif($item->warning_price1)
                    $amount += $item->actual_net_rent;
               }
                    
            }
           @endphp 
           <td>
           	<a class="get-tenant-mahnung" data-id="{{$property->property_id}}" data-toggle="modal" data-target="#get-tenant-mahnung-list">{{ $pr->name_of_property}}</a>
           	</td>
	       <td class="text-right">{{ $property->id }}</td>
         <td class="text-right">{{ number_format( $amount, 2 ,",",".") }}€</td>

	    </tr>
	@endforeach
	</tbody>
</table>
