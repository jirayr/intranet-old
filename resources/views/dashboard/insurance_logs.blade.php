<table class="table table-striped release-table">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>AM</th>
		</tr>
	</thead>
	<tbody>
		<?php $count = 0;
		$tab = 'insurance_tab';
		if($type=="management")
			$tab = 'property_management';
		?>
		@if (!empty($data))
			@foreach ($data as $element)
				@if($element->type==$type && !isset($arr['release_'.$type][$element->property_id]))
				<?php $count += 1;?>
		
				<tr>
					<td>
						<a href="{{route('properties.show',['property'=> $element->property_id])}}?tab={{$tab}}">
                            {{$element->name_of_property}}
                        </a>
					</td>
					<td>{{ $element->name }}</td>
				</tr>
				@endif
			@endforeach
		@endif
	</tbody>
</table>
<input type="hidden" class="{{'release_'.$type}}" value="{{$count}}">