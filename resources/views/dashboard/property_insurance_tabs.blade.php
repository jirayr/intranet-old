<?php
$rbutton_title = "Freigeben";
$release = 'btn btn-xs btn-primary property-insurance-release-request';
$release_type = "insurance_release"
?>
@foreach($data as $value)
<div class="row section-ins-{{$value->id}}">
<div class="col-md-12 ">
	<div class="table-responsive">
	<h3><a href="{{route('properties.show',['property'=> $value->property_id])}}?tab=property_insurance_tab">
		{{$value->name_of_property}}</a>
	{{$value->title}} <button data-id="<?=$value->id?>" type="button" class="btn <?=$release?>" data-column="<?=$release_type?>"><?=$rbutton_title?></button>
	<button data-id="<?=$value->id?>" type="button" class="btn btn-xs btn-primary deal-mark-as-not-release">Ablehnen</button>
            

</h3>
	<table class="table table-striped tbl-insurance-tab-detail" data-tabid="{{ $value->id }}" id="insurance-table-{{ $value->id }}">
		<thead>
			<tr>
				<th>#</th>
				<th>Datei</th>
				<th>Betrag</th>
				<th>Kommentar</th>
				<th>User</th>
				<th>Datum</th>
				<th>AM</th>
				<th>Falk</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
</div>
</div>
@endforeach