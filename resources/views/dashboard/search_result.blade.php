<?php
function short_name($string)
{
    if($string){
        $arr = explode(" ", $string);
        if(count($arr)>1)
            return substr($arr[0], 0,1).$arr[1];
        else
            return $string;
    }
    return $string;
}

?>

<table class="table table-striped dashboard-table ">
    <thead>
        <tr>
            <th style="width: 70px;" class="text-center">#</th>
            <th>PLZ</th>
            <th>Objekt</th>
            <th>Erst. Datum</th>
            <th>TM</th>
            <th>AM</th>
            <th>EK Preis</th>
            <th>{{__('dashboard.total_purchase_price')}}</th>
            <th>{{__('dashboard.gross_return')}}</th>
            <th>EK/GuV</th>
            <th>{{__('dashboard.ek_cf')}}</th>
            <th >{{__('dashboard.maklerpreis')}}</th>
            <th>LOI Datum</th>
            <th>{{__('dashboard.price_difference')}}</th>
            <th>{{__('property.listing.status')}}</th>
            <th>{{__('property.listing.actions')}}</th>
        </tr>
    </thead>
    <tbody>

        @foreach($data as $property)
            <?php
                $propertiesExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();
                $total_ccc = 0;

                $neitoo = 0;

            ?>
            @foreach($propertiesExtra1s as $propertiesExtra1)
                <?php
                if($propertiesExtra1->is_current_net)
                $total_ccc += $propertiesExtra1->net_rent_p_a;
                ?>
            @endforeach
            <?php 
                if($total_ccc)
                    $property->net_rent_pa = $total_ccc;
                else
                    $total_ccc = $property->net_rent_pa;
                            


                $property->net_rent_increase_year1 = $total_ccc;
                $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1*$property->net_rent;
                $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2*$property->net_rent;
                $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3*$property->net_rent;
                $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4*$property->net_rent;


                $property->operating_cost_increase_year1 = $property->net_rent_increase_year1*$property->operating_costs_nk;

                $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1*$property->operating_costs;

                $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2*$property->operating_costs;

                $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3*$property->operating_costs;
                $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4*$property->operating_costs;


                $property->property_management_increase_year1 = $property->net_rent_increase_year1*$property->object_management_nk;

                $property->property_management_increase_year2 =$property->property_management_increase_year1 +  $property->property_management_increase_year1*$property->object_management;

                $property->property_management_increase_year3 =$property->property_management_increase_year2 +  $property->property_management_increase_year2*$property->object_management;

                $property->property_management_increase_year4 =$property->property_management_increase_year3 +  $property->property_management_increase_year3*$property->object_management;

                $property->property_management_increase_year5 =$property->property_management_increase_year4 +  $property->property_management_increase_year4*$property->object_management;


                $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                $property->ebit_year_1= $property->ebitda_year_1 - $property->depreciation_nk_money;
                $property->ebit_year_2= $property->ebitda_year_2 - $property->depreciation_nk_money;
                $property->ebit_year_3= $property->ebitda_year_3 - $property->depreciation_nk_money;
                $property->ebit_year_4= $property->ebitda_year_4 - $property->depreciation_nk_money;
                $property->ebit_year_5= $property->ebitda_year_5 - $property->depreciation_nk_money;


                $D42 = $property->gesamt_in_eur
                        + ($property->real_estate_taxes * $property->gesamt_in_eur)
                        + ($property->estate_agents * $property->gesamt_in_eur)
                        + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                        + ($property->evaluation * $property->gesamt_in_eur)
                        + ($property->others * $property->gesamt_in_eur)
                        + ($property->buffer * $property->gesamt_in_eur);


                $L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                $price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

                    
                $E18 = ($property->net_rent_increase_year1
                        -$property->maintenance_increase_year1
                        -$property->operating_cost_increase_year1
                        -$property->property_management_increase_year1)
                    -$property->depreciation_nk_money;

                $D49 = $property->bank_loan * $D42;
                $D48 = $property->from_bond * $D42;
                $H48 = $D49 * $property->interest_bank_loan;
                $H49 = $D49 * $property->eradication_bank;
                $H52 = $D48 * $property->interest_bond;

                $E23 = $E18- $H48 -$H52;
                $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                $G61 = ($D48 == 0) ? 0 : $E31/$D48;
                $G62 = ($D48 == 0) ? 0 : $E23/$D48;
                    // }
                $G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;

                if(!isset($G61)){
                    $G61 = 0;
                }
            ?>
            
            <tr>
                <td class="text-center"><br>{{$property->main_property_id}}</td>
                <td class="" ><br>
                    <a href="{{route('properties.show',['property'=>$property->main_property_id])}}">
                        {{$property->plz_ort}}
                    </a>
                </td>
                <td class="" >
                    <br>
                    <a href="{{route('properties.show',['property'=>$property->main_property_id])}}">
                        {{$property->name_of_property}}
                    </a>
                </td>
                <td>
                    <br>
                    {{show_date_format($property->created_at)}}
                </td>
                <td>
                    <br>
                    @php
                        $user =  DB::table('properties')->join('users','users.id','=','properties.transaction_m_id')->where('properties.id', $property->main_property_id)->first();
                         if($user){
                             echo short_name($user->name);
                         }
                    @endphp
                </td>               
                <td>
                    <br>
                    @php
                        $user =  DB::table('properties')->join('users','users.id','=','properties.asset_m_id')->where('properties.id', $property->main_property_id)->first();
                        if($user){
                            echo short_name($user->name);
                        }
                    @endphp
                </td>           
                <td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
                <td class="number-right"><br>{{number_format($D42,0,",",".")}}€</td>
                <td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
                <!--<td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>-->
                <td class="number-right">
                    <br>
                    {{number_format(($G62 * 100),2,",",".")}}%
                </td>
                <td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
                <td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
                <td>
                    <br>
                    @php
                        $loi =  DB::table('status_loi')->where('property_id', $property->main_property_id)->first();
                        if($loi){
                            echo show_date_format($loi->date);
                        }
                    @endphp
                </td>
                <td class="number-right"><br>{{number_format(($price_difference)*100,2,",",".")}}%</td>
                <td>
                    <br>
                    @if($property->status == config('properties.status.buy')){{__('property.buy')}}
                    @elseif($property->status == config('properties.status.hold')){{__('property.hold')}}@elseif($property->status == config('properties.status.decline')){{__('property.decline')}}@elseif($property->status == config('properties.status.declined')){{__('property.declined')}}
                    @elseif($property->status == config('properties.status.offer')){{__('property.offer')}}
                    @elseif($property->status == config('properties.status.duration')){{__('property.duration')}} 
                    @elseif($property->status == config('properties.status.in_purchase'))  {{__('property.in_purchase')}} 
                    @elseif($property->status == config('properties.status.sold')){{__('property.sold')}} 
                    @elseif($property->status == config('properties.status.lost')){{__('property.lost')}} 
                    @elseif($property->status == config('properties.status.exclusivity')){{__('property.exclusivity')}} 
                    @elseif($property->status == config('properties.status.certified')){{__('property.certified')}} 
                    @elseif($property->status == config('properties.status.exclusive')){{__('property.exclusive')}} 
                    @elseif($property->status == config('properties.status.quicksheet')){{__('property.quicksheet')}} 
                    @elseif($property->status == config('properties.status.liquiplanung')){{__('property.liquiplanung')}} 
                    @endif
                </td>
                <td>
                    {!! Form::open(['action' => ['PropertiesController@destroy', $property->main_property_id],
                    'method' => 'DELETE']) !!}
                    <button onclick="location.href='{{route('properties.show',['property'=>$property->main_property_id])}}'"
                            type="button"
                            class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                class="ti-eye"></i></button>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
