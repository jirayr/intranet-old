<?php
function short_name($string)
{
    if($string){
        $arr = explode(" ", $string);
        if(count($arr)>1)
            return substr($arr[0], 0,1).$arr[1];
        else
            return $string;
    }
    return $string;
}

?>

<div class="table-responsive">
					<div class="property-table">
                    <?php
                    $f = 0;
                    $f1 = 0;
                    $f2 = 0;
                    ?>
                    <table class="table table-striped dashboard-table" id="list-properties">
                        <thead>
                        <tr>
                            <th style="width: 70px;" class="text-center">#</th>
                            <th>{{__('dashboard.property_name')}}</th>
                            <th>Erst. D.</th>
                            @if(isset($data[0]['status']) && $data[0]['status']==12)
                            <th>D. Kauf</th>
                            <?php
                            $f =1;
                            ?>
                            @endif
                            <th>D. LOI</th>
                            <th>{{__('dashboard.transaction_manager')}}</th>
                            @if(isset($data[0]['status']) && $data[0]['status']==7)
                            <th>Angeboten über</th>
                            @else
                            <th>AM</th>
                            @endif
                            @if(isset($data[0]['status']) && $data[0]['status']==14)
                            <th>D. Exkl.</th>
                            <?php
                            $f1 =1;
                            ?>
                            @endif

                            @if(isset($data[0]['status']) && $data[0]['status']==10)
                            <th>Notartermin</th>
                            <?php
                            $f2 =1;
                            ?>
                            @endif


                            <th>GKP
                            <!-- {{__('dashboard.total_purchase_price')}} -->
                            </th>
                            <th>{{__('dashboard.gross_return')}}</th>
                            <th>{{__('dashboard.faktor')}}</th>
                            <th>
                                <!-- {{__('dashboard.guv_ref_factor')}}  -->
                                EK/GuV

                            </th>
                            <th>{{__('dashboard.ek_cf')}}</th>
                            <th>EK</th>
                            <th>{{__('dashboard.maklerpreis')}}</th>
                            <th>Kommentar TM</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $property)
                            <?php
                            
                            $main_id = $property->id;
                            $name_of_property = $property->name_of_property;
                            $name_of_creator = $property->name_of_creator;
                            $aname_of_creator = "";
                            if(isset($property->transaction_manager))
                                $name_of_creator = $property->transaction_manager->name;

                            if(isset($property->asset_manager->name))
                                $aname_of_creator = $property->asset_manager->name;

                            
                            $propert  =  DB::table('properties')->where('standard_property_status', 1)->where('main_property_id',$property->id)->first();
                            if($propert)
                                $property = $propert;


                            $propertyExtra1s = \DB::table('properties_tenants')->where('propertyId', $property->id)->get();

                            // $propertyExtra1s = \DB::table('properties_extra1')->where('propertyId', $main_id)->get();
                            $total_ccc = 0;
                            ?>
                            @foreach($propertyExtra1s as $propertyExtra1)
                                <?php
                                $total_ccc += $propertyExtra1->net_rent_p_a;
                                ?>
                            @endforeach
                            <?php 
                            $property->net_rent_pa = $total_ccc;


                            $property->net_rent_increase_year1 = $total_ccc;
                    $property->net_rent_increase_year2 = $property->net_rent_increase_year1 + $property->net_rent_increase_year1*$property->net_rent;
                    $property->net_rent_increase_year3 = $property->net_rent_increase_year2 + $property->net_rent_increase_year2*$property->net_rent;
                    $property->net_rent_increase_year4 = $property->net_rent_increase_year3 + $property->net_rent_increase_year3*$property->net_rent;
                    $property->net_rent_increase_year5 = $property->net_rent_increase_year4 + $property->net_rent_increase_year4*$property->net_rent;


                    $property->operating_cost_increase_year1 = $property->net_rent_increase_year1*$property->operating_costs_nk;

                    $property->operating_cost_increase_year2 = $property->operating_cost_increase_year1 + $property->operating_cost_increase_year1*$property->operating_costs;

                    // echo $property->operating_cost_increase_year1;
                    // echo "<br>";
                    // echo $property->operating_costs_nk;



                    $property->operating_cost_increase_year3 = $property->operating_cost_increase_year2 + $property->operating_cost_increase_year2*$property->operating_costs;

                    $property->operating_cost_increase_year4 = $property->operating_cost_increase_year3 + $property->operating_cost_increase_year3*$property->operating_costs;
                    $property->operating_cost_increase_year5 = $property->operating_cost_increase_year4 + $property->operating_cost_increase_year4*$property->operating_costs;


                    $property->property_management_increase_year1 = $property->net_rent_increase_year1*$property->object_management_nk;

                    $property->property_management_increase_year2 =$property->property_management_increase_year1 +  $property->property_management_increase_year1*$property->object_management;

                    $property->property_management_increase_year3 =$property->property_management_increase_year2 +  $property->property_management_increase_year2*$property->object_management;

                    $property->property_management_increase_year4 =$property->property_management_increase_year3 +  $property->property_management_increase_year3*$property->object_management;

                    $property->property_management_increase_year5 =$property->property_management_increase_year4 +  $property->property_management_increase_year4*$property->object_management;


                    $property->ebitda_year_1 = $total_ccc - $property->maintenance_increase_year1 - $property->operating_cost_increase_year1 - $property->property_management_increase_year1;
                    $property->ebitda_year_2 = $property->net_rent_increase_year2 - $property->maintenance_increase_year2 - $property->operating_cost_increase_year2 - $property->property_management_increase_year2;
                    $property->ebitda_year_3 = $property->net_rent_increase_year3 - $property->maintenance_increase_year3 - $property->operating_cost_increase_year3 - $property->property_management_increase_year3;
                    $property->ebitda_year_4 = $property->net_rent_increase_year4 - $property->maintenance_increase_year4 - $property->operating_cost_increase_year4 - $property->property_management_increase_year4;
                    $property->ebitda_year_5 = $property->net_rent_increase_year5 - $property->maintenance_increase_year5 - $property->operating_cost_increase_year5 - $property->property_management_increase_year5;

                     $property->ebit_year_1= $property->ebitda_year_1 - $property->depreciation_nk_money;
                     $property->ebit_year_2= $property->ebitda_year_2 - $property->depreciation_nk_money;
                     $property->ebit_year_3= $property->ebitda_year_3 - $property->depreciation_nk_money;
                     $property->ebit_year_4= $property->ebitda_year_4 - $property->depreciation_nk_money;
                     $property->ebit_year_5= $property->ebitda_year_5 - $property->depreciation_nk_money;

                            $D42 = $property->gesamt_in_eur
                                + ($property->real_estate_taxes * $property->gesamt_in_eur)
                                + ($property->estate_agents * $property->gesamt_in_eur)
                                + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                                + ($property->evaluation * $property->gesamt_in_eur)
                                + ($property->others * $property->gesamt_in_eur)
                                + ($property->buffer * $property->gesamt_in_eur);

                            $G58 = ($D42 == 0) ? 0: $property->net_rent_pa/$D42;
                            $L25 = ($property->net_rent_pa == 0) ? 0 : $property->gesamt_in_eur / $property->net_rent_pa;
                            $price_difference = ($property->maklerpreis == 0) ? 1 : 1-($property->gesamt_in_eur / $property->maklerpreis);

                            $E23 = 0;

                                $E18 = ($property->net_rent_increase_year1
                                        -$property->maintenance_increase_year1
                                        -$property->operating_cost_increase_year1
                                        -$property->property_management_increase_year1)
                                    -$property->depreciation_nk_money;

                                $D49 = $property->bank_loan * $D42;
                                $D48 = $property->from_bond * $D42;
                                $H48 = $D49 * $property->interest_bank_loan;
                                $H49 = $D49 * $property->eradication_bank;
                                $H52 = $D48 * $property->interest_bond;

                                $E23 = $E18- $H48 -$H52;
                                $E31 = ($E23 - ($property->tax * $E23)) - $H49 + $property->depreciation_nk_money;
                                $G61 = ($D48 == 0) ? 0 : $E31/$D48;
                                $G62 = ($D48 == 0) ? 0 : $E23/$D48;

                            if(!isset($G61)){
                                $G61 = 0;
                            }
                            ?>
                        <tr>
                                <td class="text-center"><br>{{$main_id}}</td>
                                <td ><br>
                                    <a href="{{route('properties.show',['property'=>$main_id])}}">
                                    {{$property->name_of_property}}
                                    </a>
                                </td>
                                <td ><br>{{show_date_format($property->created_at)}}</td>
                                @if($f==1)
                                <td><br>@if($property->purchase_date){{show_date_format($property->purchase_date)}}@endif</td>
                                @endif
                                <td><br>@if($property->datum_lol){{show_date_format($property->datum_lol)}}@else
                                <?php
                                $email_template= DB::table('email_template')->where('property_id', $main_id)->first();
                                if($email_template){
                                  echo show_date_format($email_template->date);
                                }
                                ?>
                                @endif</td>
                                
                                <td><br>{{short_name($name_of_creator)}}</td>
                                @if(isset($data[0]['status']) && $data[0]['status']==7)
                                <td><br>{{$property->offer_from}}</td>
                                @else
                                <td><br>{{short_name($aname_of_creator)}}</td>
                                @endif                                

                                @if($f1==1)
                                <td><br>{{show_date_format($property->exklusivität_bis)}}</td>
                                @endif                                

                                @if($f2==1)
                                <td><br>{{show_date_format($property->purchase_date)}}</td>
                                @endif                                

                                <td class="number-right"><br>{{number_format($D42,0,",",".")}}€</td>
                                <td class="number-right"><br>{{number_format(($G58 *100),2,",",".")}}%</td>
                                <td class="number-right"><br>{{number_format(($L25),2,",",".")}}</td>
                                <td class="number-right"><br>
                                    {{number_format(($G62 * 100),2,",",".")}}%
                                    <?php
                                    //J59*(D42/J60)
                                    /*if($property->AHK_Salzgitter != 0){
                                        $guv_faktor = (($property->Ref_GuV_Salzgitter)* (($property->gesamt_in_eur
                                                                + ($property->real_estate_taxes * $property->gesamt_in_eur)
                                                                + ($property->estate_agents * $property->gesamt_in_eur)
                                                                + (($property->Grundbuch * $property->gesamt_in_eur)/100)
                                                                + ($property->evaluation * $property->gesamt_in_eur)
                                                                + ($property->others * $property->gesamt_in_eur)
                                                                + ($property->buffer * $property->gesamt_in_eur))
                                                        /($property->AHK_Salzgitter)));
                                        if($guv_faktor != 0){
                                            echo e(number_format(($E23)/$guv_faktor*100,2,",","."));
                                        }else echo "0";
                                    }else echo "0";*/
                                    ?>
                                </td>
                                <td class="number-right"><br>{{number_format(($G61 * 100),2,",",".")}}%</td>
                                <td class="number-right"><br>{{number_format(($property->gesamt_in_eur),0,",",".")}}€</td>
                                <td class="number-right"><br>{{number_format(($property->maklerpreis),0,",",".")}}€</td>
                                <td><br>
                                    <span class="long-text">
                                        <?php
                                        $response  =  DB::table('properties_comments')->where('property_id', $main_id)->orderBy('properties_comments.created_at','desc')->first();
                                        if($response)
                                            echo $response->comment.' ('.show_date_format($response->created_at).')';
                                        else
                                            echo "";
                                        ?>
                                    </span>
                                </td>
                                
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    
                    </div>
					</div>
