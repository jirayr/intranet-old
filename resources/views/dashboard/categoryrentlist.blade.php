<div class="table-responsive">
    <div class="property-table">
    <table class="table table-striped dashboard-table category-table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Objekt</th>
            <th>AM</th> 
            <th class="text-right">Miete netto p.m.</th>
            <th class="text-right">Miete netto p.a.</th>
            <th>Bundesland</th>
            <th>Kategorie</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $row)
        <?php


        if(($row['rent_begin'] != '' && $row['rent_begin'] > date('Y-m-d')) ||($row['rent_end'] != '' && $row['rent_end'] < date('Y-m-d')))
              continue;



        if($row['actual_net_rent'] && $row['rental_space'])
        {

        if(!$row['category'])
            $row['category'] = "Nicht zugeordnet";

        $subject = "Mieterübersicht ".$row['name']." ".$row['name_of_property'];

        $taburl = route('properties.show',['property' => $row['main_property_id']]).'?tab=am_mail';
        $taburl = "<a href='".$taburl."'>".$taburl."</a>";

        $content = "Bitte nicht per E-Mail antworten, sondern direkt im Intranet: ".$taburl;
        ?>
        <tr>
            <td>{{$row['name']}}</td>
            <td><a href="{{route('properties.show',['property'=>$row['main_property_id']])}}?tab=tenancy-schedule">{{$row['name_of_property']}}</a></td>
            

            <td><a href="javascript:void(0);" class="asset_manager" data-property-id="{{$row['main_property_id'] }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="Mieterübersicht" data-id="{{ $row['id'] }}" data-section="tenancy_schedule_items">{{$row['assetmanager']}}</a></td>


            
            <td class="text-right">@if($row['actual_net_rent']){{ number_format($row['actual_net_rent'], 2,",",".") }}@else{{'0,00'}}@endif</td>
            <td class="text-right">@if($row['actual_net_rent']){{ number_format($row['actual_net_rent']*12, 2,",",".") }}@else{{'0,00'}}@endif</td>
            <td>{{$row['niedersachsen']}}</td>
            <td>{{$row['category']}}</td>
        </tr>
    <?php } ?>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
