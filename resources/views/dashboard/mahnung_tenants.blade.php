<table id="mahnung-tenants" class="table table-striped">
    <thead>
    <tr>
        <th>Mieter</th>
        <th>Letzte Mahnung</th>
        <th>Datum 1. Mahnung</th>
        <th>Datum 2. Mahnung</th>
        <th>Datum Mahnbescheid</th>
    </tr>
    </thead>
    <tbody>
    <?php
    ?>
	@foreach($data as $id=>$item)

		@php 
           $amount = 0;
			if($item->warning_price3)
                    $amount += $item->warning_price3;
                    elseif($item->warning_price2)
                    $amount += $item->actual_net_rent;
            		elseif($item->warning_price1)
                    $amount += $item->actual_net_rent;
               
        @endphp 
           <td>{{ $item->name}}</td>
	       <td class="text-right">{{ number_format( $amount, 2 ,",",".") }}€</td>
	       <td>{{ show_date_format($item->warning_date1) }}</td>
	       <td>{{ show_date_format($item->warning_date2) }}</td>
	       <td>{{ show_date_format($item->warning_date3) }}</td>
	    </tr>
	@endforeach
	</tbody>
</table>
