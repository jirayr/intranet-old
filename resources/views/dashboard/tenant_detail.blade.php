<table class="table table-striped">
	<tbody>
		<tr>
			<th class="">Vermietet</th>
			<td>{{ $item->name }}</td>
		</tr>
		<?php
            $asset_mn_id = "";
            if($item->asset_manager_id)
                $asset_mn_id = $item->asset_manager_id;
        ?>
		<tr>
			<th class="">Asset<br>Manager</th>
			<td>
				@foreach($as_users as $list)
                @if($asset_mn_id==$list->id)
                {{$list->getshortname()}}
                @endif
                @endforeach
			</td>
		</tr>
		<tr>
			<th class="">Abschluss MV</th>
			<td>{{ show_date_format($item->assesment_date) }}</td>
		</tr>
		<tr>
			<th class="">Mietbeginn</th>
			<td>{{ show_date_format($item->rent_begin) }}</td>
		</tr>
		<tr>
			<th class="">Sonderkündigunsrecht</th>
			<td>{{ show_date_format($item->termination_date) }}</td>
		</tr>
		<tr>
			<th class="">Mietende</th>
			<td>{{ show_date_format($item->rent_end) }}</td>
		</tr>
		<tr>
			<th class="">Optionen</th>
			<td>	
				
                <input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="0" @if($item->selected_option==0) checked @endif >
                {{ $item->options }}
                <br>
				<input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="1" @if($item->selected_option==1) checked @endif ><br>
                <input type="checkbox" class="option-check" data-id="{{ $item->id }}" value="2" @if($item->selected_option==2) checked @endif >

			</td>
		</tr>
		<tr>
			<th class="">Nutzung</th>
			<td>{{ $item->use }}</td>
		</tr>
		<tr>
			<th class="">Mietfläche/m²</th>
			<td>{{ number_format($item->rental_space, 2,",",".") }}</td>
		</tr>
		<tr>
			<th class="">Miete/m²</th>
			<td>@if($item->rental_space && $item->actual_net_rent)
	            {{ number_format($item->actual_net_rent/$item->rental_space, 2,",",".") }}
	            @else
	            {{ number_format(0, 2,",",".") }}
	            @endif
			</td>
		</tr>
		<tr>
			<th class="">IST-Nettokaltmiete p.m.</th>
			<td>{{ number_format($item->actual_net_rent, 2,",",".") }}&nbsp;€</td>
		</tr>
		<tr>
			<th class="">IST-Nettokaltmiete p.a.</th>
			<td>{{ number_format($item->actual_net_rent*12, 2,",",".") }}&nbsp;€</td>
		</tr>
		<tr>
			<th class="">NK netto p.m.</th>
			<td>{{ number_format($item->nk_netto, 2,",",".") }}&nbsp;€</td>
		</tr>
		<tr>
			<th class=""> Gesamt Netto p.m. </th>
			<td><?php
				$natto =$item->actual_net_rent+$item->nk_netto; 
                $tn = ($item->actual_net_rent+$item->nk_netto)*19/100;
                ?>
                {{ number_format($natto, 2,",",".") }}&nbsp;€
			</td>
		</tr>
		<tr>
			<th class="">19% MwSt.</th>
			<td>{{ number_format($tn, 2,",",".") }}&nbsp;€</td>
		</tr>
		<tr>
			<th class="">Gesamt Brutto p.m.</th>
			<td>{{ number_format($natto+$tn, 2,",",".") }}&nbsp;€</td>
		</tr>
		<tr>
			<th class="">Gesamt Brutto p.a. </th>
			<td>{{ number_format(($natto+$tn)*12, 2,",",".") }}&nbsp;€</td>
		</tr>
		<tr>
			<th class="">Indexierung</th>
			<td>{{ $item->indexierung }}</td>
		</tr>
		<tr>
			<th class="">Kaution</th>
			<td>{{ $item->kaution }}</td>
		</tr>
		<tr>
			<th class="">Kommentar</th>
			<td>{{ $item->comment }}</td>
		</tr>
		
	</tbody>
</table>