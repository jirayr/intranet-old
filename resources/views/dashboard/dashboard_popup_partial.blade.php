
<div class=" modal fade" role="dialog" id="table-data-modal">
   <div class="modal-dialog" style="width: 93%;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase"></h4>
         </div>
         <div class="modal-body table-responsive" style="min-height: 20%;">

         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="modal_op_list" role="dialog">
  <div class="modal-dialog" style="width: 80%;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">OP LISTE</h4>
      </div>
      <div class="modal-body">
         <h3>Forderungsmanagment</h3>
         <div id="rent-paid-data"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
      </div>
    </div>

  </div>
</div>
<div class="modal fade" id="not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="pending-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-pending-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-pending-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="multiple-invoice-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control multiple-invoice-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary multiple-invoice-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="invoice-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control invoice-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary invoice-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="Modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Choose a Bank</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <label>{{__('property.select_a_bank')}}: </label>
            <select id="bank">
            </select>
         </div>
         <div class="modal-footer">
            <button id="modal_save_btn" type="button" class="btn btn-primary">{{__('property.save_changes')}}</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="get-tenant-mahnung-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Mieterliste</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body get-tenant-mahnung-list-data">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="tenant-detail-modal" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
               <!-- Mieterliste -->
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body tenant-detail">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="comment-detail-popup" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="comment-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body comment-detail-body">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="user-list-popup" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body user-list">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body list-data">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="iproperty-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body insurance-list-data">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="vacant-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control vacant-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary vacant-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="vacant-not-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control vacant-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary vacant-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>

{{-- Hausmaxx Modal --}}
<div class="modal fade" id="hausmax-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Hausmaxx</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body hausmax-list-data">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="asset-property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body asset-list-data">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="rent-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog custom" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Mieter</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body list-data-rent">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="bank-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body list-data-banks row">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="cate-pro-listing" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body cate-pro-div">
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="vacant-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
   <div class="modal-dialog custom" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Leerstandsflächen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body vlist-data-rent">
         </div>
      </div>
   </div>
</div>
<div class=" modal fade" role="dialog" id="load_all_status_loi_by_user">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div id="load_all_status_loi_by_user_content">
         </div>
      </div>
   </div>
</div>
<div class=" modal fade" role="dialog" id="send-mail-to-user-modal">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mail senden an <span id="send-mail-to-name"></span></h4>
         </div>
         <form action="{{ route('sendmail_to_user') }}" method="POST">
            <div class="modal-body">
               <input type="hidden" name="_token" value="{{csrf_token()}}">
               <input type="hidden" name="email" value="">
               <input type="hidden" name="name" value="">
               <label>Betreff</label>
               <input type="text" name="subject" class="form-control" required>
               <br>
               <label>Nachricht</label>
               <textarea class="form-control" name="message" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="provision-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control provision-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary provision-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>


<div class=" modal fade" role="dialog" id="modal_invoice_sendmail_to_am">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mail an AM</h4>
         </div>
         <form action="{{ route('invoice_sendmail_to_am') }}" id="form_invoice_sendmail_to_am">
            <input type="hidden" name="property_id">
            <div class="modal-body">
               <label>Nachricht</label>
               <textarea class="form-control" name="message" required></textarea>
               <br>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>
   </div>
</div>

<div class="modal fade" id="insurance-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control insurance-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary insurance-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->

<div class="modal fade" id="property-insurance-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control property-insurance-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary property-insurance-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="deal-not-release-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control deal-not-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary deal-not-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="contract-release-property-modal" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <label>Kommentar</label>
            <textarea class="form-control contract-release-comment" name="message"></textarea>
            <br>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary contract-release-submit" data-dismiss="modal" >Senden</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>
<div class=" modal fade" role="dialog" id="contracts_mark_as_not_release">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="post" id="form_contracts_mark_as_not_release">
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <span id="contracts_mark_as_not_release_error"></span>
                  </div>
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="input_contracts_mark_as_not_release" name="comment" required></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>

   </div>
</div>

<div class=" modal fade" role="dialog" id="contracts_mark_as_pending">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <form method="post" id="form_contract_mark_as_pending">
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <span id="contracts_mark_as_pending_error"></span>
                  </div>
                  <div class="col-md-12">
                     <label>Kommentar</label>
                     <textarea class="form-control" id="input_contracts_mark_as_pending" name="comment" required></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary" >Senden</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
            </div>
         </form>
      </div>

   </div>
</div>

<div class="modal fade" id="finance-per-bank-modal" role="dialog">
   <div class="modal-dialog" style="width: 85%;">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">BANKENFINANZIERUNGEN</h4>
         </div>
         <div class="modal-body">
           
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
         </div>
      </div>
   </div>
</div>