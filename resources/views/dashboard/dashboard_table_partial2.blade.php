<?php
function short_name($string)
{
    if($string){
        $arr = explode(" ", $string);
        if(count($arr)>1)
            return substr($arr[0], 0,1).$arr[1];
        else
            return $string;
    }
    return $string;
}

?>

<table class="table table-striped" id="table-not-beur">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Objekt</th>
                            <th>AM</th>
                            <th>TM</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($data as $property)
                            <?php


                            if(!isset($property->bank_ids)){  continue; }

                            ?>
                           
                        <tr>
                                <td class="text-center"><br>{{$property->property_actual_id}}</td>

                                <td class="" ><br>
                                    <a href="{{route('properties.show',['property'=>$property->property_actual_id])}}">
                                    {{$property->name_of_property}}
                                    </a>
                                </td>
                                <td class="" ><br>
                                    @php
                                        $user =  DB::table('properties')->join('users','users.id','=','properties.transaction_m_id')->where('properties.id', $property->property_actual_id)->first();
                                         if($user){
                                             echo short_name($user->name);
                                         }
                                    @endphp
                                    </td>

                                    
                                    <td><br>
                                        @php
                                        $user =  DB::table('properties')->join('users','users.id','=','properties.asset_m_id')->where('properties.id', $property->property_actual_id)->first();

                                            if($user){
                                                echo short_name($user->name);
                                            }
                                        @endphp

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
