<div class="table-responsive">
					<div class="property-table">
                    <?php
                    $f = 0;
                    ?>
                    <table class="table table-striped dashboard-table" id="insurance-property-table">
                        <thead>
                        <tr>
                            <th style="width: 70px;" class="text-center">#</th>
                            <th>{{__('dashboard.property_name')}}</th>
                            <th>Name</th>
                            <th>Laufzeit</th>
                            <th>Betrag</th>
                            <!-- <th>{{__('dashboard.transaction_manager')}}</th>
                            <th>{{__('dashboard.total_purchase_price')}}</th>
                            <th>{{__('dashboard.gross_return')}}</th>
                            <th>{{__('dashboard.faktor')}}</th>
                            <th>{{__('dashboard.guv_ref_factor')}}</th>
                            <th>{{__('dashboard.ek_cf')}}</th>
                            <th>EK Preis</th>
                            <th>{{__('dashboard.maklerpreis')}}</th>
                            <th>{{__('dashboard.price_difference')}}</th> -->
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $property)
                            <?php
                            $bank_ids = json_decode($property->bank_ids,true);
                            if($bank_ids != null) {
                                foreach ($banks as $b) {
                                    if(isset($bank_ids[0]) && $b->id == $bank_ids[0]){
                                        $bank = $b;
                                        break;
                                    }
                                }
                            }
                            $main_id = $property->id;
                            $name_of_property = $property->name_of_property;
                            $name_of_creator = $property->name_of_creator;
                            if(isset($property->transaction_manager))
                                $name_of_creator = $property->transaction_manager->name;

                            if(isset($bank)){
                                if($bank){
                                    // echo $main_id.'->'.$bank;
                                    $propert  =  DB::table('properties')->where('properties.Ist', $bank->id)->where('main_property_id',$property->id)->first();
                                     if($propert)
                                         $name_of_property = $propert->name_of_property;

                                     // echo $main_id.'->'.$property->id;
                                     // echo "<br>";

                                }
                            } ?>

                        <tr>
                                <td class="text-center"><br>{{$main_id}}</td>
                                <td ><br>
                                    <a href="{{route('properties.show',['property'=>$main_id])}}">
                                    {{$name_of_property}}
                                    </a>
                                </td>
                                <td>
                                    <br>
                                    @if($pr==1)
                                    {{$property->axa}}
                                    @else
                                    {{$property->allianz}}
                                    @endif
                                </td>
                                <td>
                                    <br>
                                    @if($pr==1)
                                    @if($property->gebaude_laufzeit_from)
                                    {{show_date_format($property->gebaude_laufzeit_from)}}
                                    @else
                                    Empty
                                    @endif
                                    -
                                    @if($property->gebaude_laufzeit_to)
                                    {{show_date_format($property->gebaude_laufzeit_to)}}
                                    @else
                                    Empty
                                    @endif
                                    @else
                                    @if($property->haftplicht_laufzeit_from)
                                    {{show_date_format($property->haftplicht_laufzeit_from)}}
                                    @else
                                    Empty
                                    @endif
                                    -
                                    @if($property->haftplicht_laufzeit_to)
                                    {{show_date_format($property->haftplicht_laufzeit_to)}}
                                    @else
                                    Empty
                                    @endif
                                    @endif
                                </td>
                                <td>
                                    <br>                                    
                                    @if($pr==1)
                                    {{show_number($property->gebaude_betrag,2)}}€
                                    @else
                                    {{show_number($property->haftplicht_betrag,2)}}€
                                    @endif
                                </td>
                                
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    
                    </div>
					</div>