<table id="property-default-payer-table" class="table table-striped">
	<thead>
		<tr>
			<th>Objekt</th>
			<th>AM</th>
			<th>Datum</th>
			<th>Monat</th>
			<th>Jahr</th>
			<th>Keine OPOS</th>
			<th>Excel</th>
			<th>Kommentar</th>					
		</tr>
	</thead>
	<tbody>
		<?php
		$count = 0;
		?>
		@if (!empty($data))
			@foreach ($data as $element)
				<?php 

				if($status==1 && $element->no_opos_status)
				{
					continue;
				}
				if($status==2 && !$element->no_opos_status)
				{
					continue;
				}

				$download_path = "";
				if($element->invoice){
						$download_path = "https://drive.google.com/drive/u/2/folders/".$element->file_basename;
		                if($element->file_type == "file"){
		                    $download_path = "https://drive.google.com/file/d/".$element->file_basename;
		                }
		                $download_path = '<a  target="_blank"  title="'.$element->invoice.'"  href="'.$download_path.'">'.$element->invoice.'</a>';
		           }
				?>
				<tr>
					<td>
						<a href="{{route('properties.show',['property'=> $element->id])}}?tab=default_payer">
                            {{$element->name_of_property}}
                        </a>
					</td>
					<td>{{ $element->name }}</td>
					<td>{{show_datetime_format($element->created_at)}}</td>
					<td>
						<?php if($element->month && strlen($element->month)!=2)
								echo '0';
								echo $element->month;

								if(!$element->month)
									$count +=1;
						?>
					</td>
					<td>{{$element->year}}</td>
					<td><?php echo get_paid_checkbox($element->id,$element->no_opos_status);?></td>
					<td>{!! $download_path !!}</td>
					<td>{{ $element->comment }}</td>
					
				</tr>
			@endforeach
		@endif
	</tbody>
</table>
<?php
$str = $m[0].' '.$count.'<br>';
$str .= $m[1].' '.$second_last.'<br>';
$str .= $m[2].' '.$third_last;
?>
<input type="hidden" class="input-default_payerTotal" value="{!! $str !!}">
<input type="hidden" class="selected-input-default_payerTotal" value="{!! $count !!}">
