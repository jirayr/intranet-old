@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="panel">
                    <div class="panel-heading">Kontakte </div>

                    <form action="{{route('contacts')}}" method="get">
                    <div class="col-sm-4">
                    <input type="text" name="q" class="form-control" value="{{@$_REQUEST['q']}}">
                    </div>
                    <div class="col-sm-4">
                    <button type="submit" class="btn btn-success">Search</button>
                    <button type="button" data-toggle="modal" data-target="#nue-banken" class="btn btn-success">Add</button>
                    </div>
                    </form>

                    <div class="clearfix"></div>

                    <div class="table-responsive">
                        <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                            <thead>
                            <tr>
                                <th style="width: 70px;" class="text-center">#</th>
                                <th>Firma</th>
                                <th>Anrede</th>
                                <th>Vorname</th>
                                <th>Nachname</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($contacts as $user)
                                <tr>
                                    <td class="text-center"><br>{{$user->id}}</td>
                                    <td><br>{{$user->Firma}}</td>
                                    <td><br>{{$user->Anrede}}</td>
                                    <td><br>{{$user->Vorname}}</td>
                                    <td><br>{{$user->Nachname}}</td>
                                    <td><br>
                                        <a data-id="{{$user->id}}" href="javascript:void(0)" class="btn btn-success get-contact-detail" ><i class="ti-pencil"></i></a>
                                        <a href="{{route('contact-view', ['id' => $user->id ])}}" class="btn btn-info" ><i class="ti-eye"></i></a>

                                        {!! Form::open(['action' => ['ContactController@delete'], 'method' => 'POST', 'style' => 'display: inline !important;']) !!}
                                        {!! Form::token() !!}
                                            <input type="hidden" name="contact_id" value="{{$user->id}}" />
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')"> <i class="icon-trash"></i></button>
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper" style="margin-left: 20px">
                            <div class="row pull-left">
                                {{$contacts->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>


    <div class=" modal fade" role="dialog" id="nue-banken">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add New Kontakte</h4>
          </div>
          <div class="modal-body">
                <form id="add-new-contact">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="0" class="contact_id">
                    
                    
                    <label>Firma</label>
                    <input type="text" name="Firma" class="form-control Firma">
                    <br>

                    <label>Anrede</label>
                    <input type="text" name="Anrede" class="form-control Anrede">
                    <br>

                    <label>Vorname</label>
                    <input type="text" name="Vorname" class="form-control Vorname">
                    <br>

                    <label>Nachname</label>
                    <input type="text" name="Nachname" class="form-control Nachname">
                    <br>

                    <label>Strasse</label>
                    <input type="text" name="Strasse" class="form-control Strasse">
                    <br>
                    <label>PLZ</label>
                    <input type="text" name="PLZ" class="form-control PLZ">
                    <br>

                    <label>Ortsteil</label>
                    <input type="text" name="Ortsteil" class="form-control Ortsteil">
                    <br>

                    <label>Ort</label>
                    <input type="text" name="Ort" class="form-control Ort">
                    <br>

                    <label>Telefon</label>
                    <input type="text" name="Telefon" class="form-control Telefon">
                    <br>

                    <label>Fax</label>
                    <input type="text" name="Fax" class="form-control Fax">
                    <br>
                    <label>E-Mail</label>
                    <input type="text" name="E_Mail" class="form-control E_Mail">
                    <br>

                    <label>Internet</label>
                    <input type="text" name="Internet" class="form-control Internet">
                    <br>

                    <label>Notiz</label>
                    <input type="text" name="Notiz" class="form-control Notiz">
                    <br>

                    <label>Stichwort</label>
                    <input type="text" name="Stichwort" class="form-control Stichwort">
                    <br>

                    <label>Stichwort2</label>
                    <input type="text" name="Stichwort2" class="form-control Stichwort2">
                    <br>
                    
                    
                </form>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary save-new-contact" >Speichern</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
          </div>
        </div>

      </div>
    </div>


@endsection

@section('js')
<script type="text/javascript">
$('.save-new-contact').on('click', function () {
    $.ajax({
    url: "{{route('add_new_contact')}}",
        type: "post",
        data : $('#add-new-contact').serialize(),
        success: function (response) {
            alert(response.message);
            if(response.status==1){
                $('#nue-banken').modal('hide');
                // $('#add-new-contact')[0].reset();
                location.reload();
            }

        }
    });

});
$('.get-contact-detail').on('click', function () {
    $.ajax({
    url: "{{route('contact-detail')}}",
        type: "get",
        data : {id:$(this).attr('data-id')},
        success: function (response) {
            $('#nue-banken').modal();
            $('.contact_id').val(response.id);
            $('.Firma').val(response.Firma);
            $('.Anrede').val(response.Anrede);
            $('.Vorname').val(response.Vorname);
            $('.Nachname').val(response.Nachname);
            $('.Strasse').val(response.Strasse);
            $('.PLZ').val(response.PLZ);
            $('.Ortsteil').val(response.Ortsteil);
            $('.Ort').val(response.Ort);
            $('.Telefon').val(response.Telefon);

            $('.Fax').val(response.Fax);
            $('.E_Mail').val(response.E_Mail);
            $('.Internet').val(response.Internet);
            $('.Notiz').val(response.Notiz);
            $('.Stichwort').val(response.Stichwort);
            $('.Stichwort2').val(response.Stichwort2);
        }
    });
});



</script>
@endsection