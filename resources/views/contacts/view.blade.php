@extends('layouts.admin')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <style type="text/css">
        .white-box{
            float: left;
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        

        
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    
                    Kontakte
                </div>

                <div class="white-box">

                    <div class="form-group">
                        @if($contact->Firma)
                        <div class="col-sm-6">
                            <label>Firma</label>
                            <br>{{$contact->Firma}}
                        </div>
                        @endif
                        @if($contact->Anrede)
                        <div class="col-sm-6">
                            <label>Anrede</label>
                            <br>{{ $contact->Anrede }}
                        </div>
                        @endif
                        
                    </div>

                    <div class="form-group">
                        @if($contact->Vorname)
                        <div class="col-sm-6">
                            <label>Vorname</label>
                            <br>{{$contact->Vorname}}
                        </div>
                        @endif
                        @if($contact->Nachname)
                        <div class="col-sm-6">
                            <label>Nachname</label>
                            <br>{{ $contact->Nachname }}
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        @if($contact->Strasse)
                        <div class="col-sm-6">
                            <label>Strasse</label>
                            <br>{{$contact->Strasse}}
                        </div>
                        @endif
                        @if($contact->PLZ)
                        <div class="col-sm-6">
                            <label>PLZ</label>
                            <br>{{ $contact->PLZ }}
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        @if($contact->Ort)
                        <div class="col-sm-6">
                            <label>Ort</label>
                            <br>{{$contact->Ort}}
                        </div>
                        @endif
                        @if($contact->Ortsteil)
                        <div class="col-sm-6">
                            <label>Ortsteil</label>
                            <br>{{ $contact->Ortsteil }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        @if($contact->Telefon)
                        <div class="col-sm-6">
                            <label>Telefon</label>
                            <br>{{$contact->Telefon}}
                        </div>
                        @endif
                        @if($contact->Fax)
                        <div class="col-sm-6">
                            <label>Fax</label>
                            <br>{{ $contact->Fax }}
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        @if($contact->E_Mail)
                        <div class="col-sm-6">
                            <label>E-Mail</label>
                            <br>{{$contact->E_Mail}}
                        </div>
                        @endif
                        @if($contact->Internet)
                        <div class="col-sm-6">
                            <label>Internet</label>
                            <br>{{ $contact->Internet }}
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        @if($contact->Stichwort)
                        <div class="col-sm-6">
                            <label>Stichwort</label>
                            <br>{{$contact->Stichwort}}
                        </div>
                        @endif
                        @if($contact->Stichwort2)
                        <div class="col-sm-6">
                            <label>Stichwort2</label>
                            <br>{{ $contact->Stichwort2 }}
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        @if($contact->Notiz)
                        <div class="col-sm-6">
                            <label>Notiz</label>
                            <br>{{$contact->Notiz}}
                        </div>
                        @endif

                    </div>


                </div>
            </div>
        </div>

        
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });
        });
    </script>
@endsection