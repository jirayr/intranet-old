<?php
return [
    'status' => [
        'buy'       => 1,
        'hold'      => 2,
        'decline'   => 3
    ],
];