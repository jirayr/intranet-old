@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    <button onclick="location.href='{{route('banks.search')}}'" type="button"
                                                   class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                class="ti-arrow-left"></i></button> 
                    {{ 'Back' }}
                </div>
                    
                @if($banks->count() > 0)
                <div class="table-responsive">
                    <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th style="width: 70px;" class="text-center">#</th>
                            <!--<th>{{__('banks.index.picture')}}</th>-->
                            <th>{{__('banks.index.bank_name')}}</th>
                            <th>Connected Properties</th>
                            <th>Contact Name</th>
                            <th>Contact Phone</th>
                            <th>{{__('banks.index.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach( $banks as $i => $bank )
                            <tr>
                                <td>{{$i += 1}}</td>
                                <!--<td>-->
                                <!--    @if ( $bank->picture != '' )-->
                                <!--        <img src="{{$bank->picture}}" width="150" />-->
                                <!--    @endif-->
                                <!--</td>-->
                                <!--<td>{{ $bank->name }}</td>-->

                                <?php
                                $name_of_property = "";
                                $arr = array();
                                $response  =  DB::table('properties')->where('properties.Ist', $bank->id)->get();
                                foreach ($response as $key => $value) {

                                    $pr  =  DB::table('properties')->select('id')->where('Ist', 0)
                                            ->where('soll', 0)
                                            ->where(function($q) use($bank) {
                                            $q->where('bank_ids','like', '%['.$bank->id.',%')
                                                ->orWhere('bank_ids','like', '['.$bank->id.']')
                                              ->orWhere('bank_ids','like', '%,'.$bank->id.']%')
                                              ->orWhere('bank_ids','like', '%,'.$bank->id.',%');
                                            })->first();
                                    if($pr)
                                    {
                                        $arr[]= '<a href="'.route('properties.show',['property'=>$pr->id]).'">'.$value->name_of_property.'</a>';    
                                    }
                                    else{
                                        $arr[]= $value->name_of_property;    
                                    }

                                    
                                }

                                if($arr)
                                    $name_of_property = implode(',', $arr);


                                ?>
                                
                                <td><a href="{{route('properties.index', ['bank_id' => $bank->id ])}}" >{{ $bank->name }}</a></td>
                                <td>{!! $name_of_property !!}</td>
                                <td>{{ $bank->contact_name }}</td>
                                <td>{{ $bank->contact_phone }}</td>
                                
                                <!--<td>{{ $bank->user_id }}</td>-->
                                <td>
                                    <button onclick="location.href='{{route('bank-view', ['id' => $bank->id ])}}'" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-eye"></i></button>
                                    <button onclick="location.href='{{route('bank-edit', ['id' => $bank->id ])}}'" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-pencil-alt"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper" style="margin-left: 20px">
                        <div class="row pull-left">
                            {{ $banks->links() }}
                        </div>
                    </div>
                </div>
                @else
                    <div class="table-responsive">
                        No result found
                    </div>
                @endif
            </div>
        </div>
    </div>


@endsection

@section('js')

@endsection