@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

            <div class="col-md-12 col-lg-12 col-sm-12">

                <div class="panel">
                    <div class="panel-heading">Bank Connection
                        <div style="float: right">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#comment_modal" style=" color: white" class="commentAdd   btn btn-primary ">Import Connection </a>
                            <a href="{{url('/bank_accounts_balance')}}" class="btn btn-primary">All Banks</a>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Bank Id</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @isset($bankConnections['connections'])
                                @if(count($bankConnections['connections']))
                                @foreach($bankConnections['connections'] as $bankConnection)
                                        <tr>
                                            <td>{{$bankConnection['id']}}</td>
                                            <td>{{$bankConnection['name']}}</td>
                                            <td>{{$bankConnection['bankId']}}</td>
                                            <td style="display:inline-flex">
                                                <a  class="btn-sm btn-primary" href="{{url('/bank'.'/'.$bankConnection['bankId'])}}">View Bank</a> &nbsp;
                                                <form  method="get" action="{{url('/account')}}">
                                                    <input type="hidden" name="accountIds" value="{{implode(',',$bankConnection['accountIds'])}}">
                                                    <button class="btn-sm btn-primary" type="submit">View Account</button>
                                                </form>
                                            </td>
                                        </tr>
                                @endforeach
                                @else
                                    <tr>
                                        <td></td>
                                        <td style="text-align: center">
                                            No Data Found
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>


    <div class=" modal fade" role="dialog" id="comment_modal">
        <div class="modal-dialog modal-dialog-centered " >

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add connection</h4>
                </div>
                <form  method="post" action="{{url('/import_bank_connection')}}">
                    <div class="modal-body">

                    <label for="email">Bank Id:</label>
                        <input required type="text" class="form-control" name="bankId" maxlength="105"  placeholder="Enter Bank Id" id="">
                        <br> <label for="email">Connection Name:</label>
                        <input required type="text" class="form-control" name="connectionName" maxlength="105"  placeholder="Enter Connection Name" id="">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <div class="modal-footer">
                             <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection