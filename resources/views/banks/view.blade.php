@extends('layouts.admin')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['action' => ['BanksController@' . $type, isset( $banks->id ) ? $banks->id : null], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
        {!! Form::token() !!}

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    <button onclick="location.href='{{URL::previous()}}'" type="button"
                                                   class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                class="ti-arrow-left"></i></button> 
                    Banken
                </div>

                <div class="white-box">

                    <div class="form-group">
                        <label class="col-sm-12">{{__('banks.add_edit.name')}}</label>
                        <div class="col-sm-12">
                            {{ $banks->name}}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>Ansprechpartner</label>
                            <br>{{$banks->contact_name}}
                        </div>

                        <div class="col-sm-6">
                            <label>Telefonnummer</label>
                            <br>{{ $banks->contact_phone }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>Adresse</label>
                            <br>{{$banks->address}}
                        </div>

                        <div class="col-sm-6">
                            <label>Notizen</label>
                            <br>{{ $banks->notes }}
                        </div>
                    </div>
                    
                    {{--<div class="form-group">
                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.With_real_ek')}} (%)</label>
                            <br> {{ $banks->with_real_ek * 100 }}
                        </div>

                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.from_bond')}} (%)</label>
                            <br>{{ $banks->from_bond * 100 }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.bank_loan')}} (%)</label>
                            <br>{{ $banks->bank_loan *100 }}
                        </div>

                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.interest_bank_loan')}} (%)</label>
                            <br>{{ $banks->interest_bank_loan * 100 }}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.eradication_bank')}} (%)</label>
                            <br>{{ $banks->eradication_bank * 100 }}
                        </div>

                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.interest_bond')}} (%)</label>
                            <br>{{ $banks->interest_bond * 100 }}
                        </div>
                    </div>--}}


                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });
        });
    </script>
@endsection