@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="panel">
                    <div class="panel-heading">Account</div>

                    <div class="table-responsive">
                        <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Bank Connection Id</th>
                                <th>Account Name</th>
                                <th>Balance</th>
                                <th>Account Number</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                @isset($account)
                                    <tr>
                                        <td>{{$account['id']}}</td>
                                        <td>{{$account['bankConnectionId']}}</td>
                                        <td>{{$account['accountName']}}</td>
                                        <td>{{$account['balance']}}</td>
                                        <td>{{$account['accountNumber']}}</td>
                                        <td>{{$account['status']}}</td>
                                    </tr>
                                 @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>


@endsection