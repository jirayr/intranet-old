@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

        

        <div class="col-md-12 col-lg-12 col-sm-12">
            {!! Form::open(['action' => ['BanksController@search_result'], 'method' => 'GET', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
                    {!! Form::token() !!}
            
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                Suche
                            </div>
                            <div class="white-box">
                                <div class="form-group">
                                    <label class="col-sm-12">{{__('banks.add_edit.name')}}</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="name" class="form-control form-control-line input-text" value="{{ isset( $banks->name ) ? $banks->name : old('name') }}" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-lg-2 col-xs-12">
                                        <button type="submit" name="submit" class="btn btn-block btn-primary btn-rounded">{{__('banks.add_edit.submit')}}
                                        </button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
            
                    {!! Form::close() !!}
            <div class="panel">
                <div class="panel-heading">
                    {{__('banks.index.manage_banks')}}
                    <button onclick="location.href= '{{route('bank-add')}}'" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-plus"></i></button>
                </div>

                <div class="table-responsive">
                    <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th style="width: 70px;" class="text-center">#</th>
                            <th>{{__('banks.index.picture')}}</th>
                            <th>{{__('banks.index.bank_name')}}</th>
                            <th>Objekt</th>
                            <th>Ansprechpartner</th>
                            <th>Telefonnr</th>
                            <th>{{__('banks.index.user_creator')}}</th>
                            <th>Update contact Details</th>
                            <th>{{__('banks.index.actions')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach( $banks as $i => $bank )
                            <tr>
                                <td>{{$i += 1}}</td>

                                <?php
                                $name_of_property = "";
                                $arr = array();
                                $response  =  DB::table('properties')->where('properties.Ist', $bank->id)->get();
                                foreach ($response as $key => $value) {
                                    if($value->main_property_id)
                                    {
                                        $arr[]= '<a href="'.route('properties.show',['property'=>$value->main_property_id]).'">'.$value->name_of_property.'</a>';    
                                    }
                                    else{
                                        $arr[]= $value->name_of_property;    
                                    }

                                    
                                }

                                if($arr){
                                    $arr = array_unique($arr);
                                    $name_of_property = implode('<br>', $arr);
                                }


                                ?>
                                <td>
                                    @if ( $bank->picture != '' )
                                        <img src="{{$bank->picture}}" width="150" />
                                    @endif
                                </td>
                                <td><a href="{{route('bank-view', ['id' => $bank->id ])}}" >
                                        @if(!is_null($bank->name))
                                            {!! str_replace(' || ', "<br/>", $bank->name ) !!}
                                        @endif
                                            </a></td>
                                <td>{!! $name_of_property !!}</td>
                                <td>@if(!is_null($bank->contact_name)) {!! str_replace(' || ', "<br/>", $bank->contact_name ) !!} @else {!! str_replace(' || ', "<br/>", $bank->surname ) !!} @endif</td>
                                <td>@if(!is_null($bank->contact_phone)) {!! str_replace(' || ', "<br/>", $bank->contact_phone ) !!} @endif </td>
                                <td>{{ $bank->user_id }}</td>
                                <td><a href="{{ url('/banks/contact-delails'.'/'.$bank->id) }}" class="btn btn-primary">Update</a></td>
                                <td>  
                                    <button onclick="location.href='{{route('properties.index', ['bank_id' => $bank->id ])}}'" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-eye"></i></button>
                                    
                                    <button onclick="location.href='{{route('bank-edit', ['id' => $bank->id ])}}'" type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i class="ti-pencil-alt"></i></button>
                                    
                                    {!! Form::open(['action' => ['BanksController@delete'], 'method' => 'POST', 'style' => 'display: inline !important;']) !!}
                                    {!! Form::token() !!}
                                        <input type="hidden" name="bank_id" value="{{$bank->id}}" />
                                        <button type="submit" class="btn btn-info btn-outline btn-circle btn-lg m-r-5" onclick="return confirm('Are you sure?')"> <i class="icon-trash"></i></button>
                                    {!! Form::close() !!}

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper" style="margin-left: 20px">
                        <div class="row pull-left">
                            {{ $banks->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')

@endsection
