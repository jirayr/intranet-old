@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="">
                    {!! Form::open(['action' => ['BanksController@'.$type], 'method' => 'GET', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
                    {!! Form::token() !!}
            
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                {{ 'Search' }} {{__('banks.add_edit.banks')}}
                            </div>
                            <div class="white-box">
                                <div class="form-group">
                                    <label class="col-sm-12">{{__('banks.add_edit.name')}}</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="name" class="form-control form-control-line input-text" value="{{ isset( $banks->name ) ? $banks->name : old('name') }}" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-lg-2 col-xs-12">
                                        <button type="submit" name="submit" class="btn btn-block btn-primary btn-rounded">{{__('banks.add_edit.submit')}}
                                        </button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
            
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')

@endsection