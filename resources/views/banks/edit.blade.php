@extends('layouts.admin')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
@endsection

@section('content')
    <div class="row">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['action' => ['BanksController@' . $type, isset( $banks->id ) ? $banks->id : null], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
        {!! Form::token() !!}

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">
                    <button onclick="location.href='{{URL::previous()}}'" type="button"
                                                   class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                class="ti-arrow-left"></i></button> 
                    {{ $type == 'add' ? 'ADD' : 'EDIT' }} {{__('banks.add_edit.banks')}}
                </div>

                <div class="white-box">
                    <div class="form-group">
                        <label class="col-sm-12">{{__('banks.add_edit.picture')}}</label>
                        <div class="col-sm-6">
                            <div class="white-box">
                                <label for="picture">{{__('banks.add_edit.description_picture')}}</label>
                                <input name="picture" style="height: 300px;width: 450px" type="file" id="picture" class="dropify"
                                       data-default-file="{{isset($banks->picture) ? asset($banks->picture) : ''}}" accept="image/*"/> </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12">{{__('banks.add_edit.name')}}</label>
                        <div class="col-sm-12">
                            <input type="text" name="name" class="form-control form-control-line input-text" value="{{ isset( $banks->name ) ? $banks->name : old('name') }}" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>Contact Name</label>
                            <input type="text" name="contact_name" class="form-control form-control-line input-text" value="{{ isset( $banks->contact_name ) ? $banks->contact_name : old('contact_name') }}"
                                       placeholder="">
                        </div>

                        <div class="col-sm-6">
                            <label>Contact Phone</label>
                            <input type="text" name="contact_phone" class="form-control form-control-line input-text" value="{{ isset( $banks->contact_phone ) ? $banks->contact_phone : old('contact_phone') }}"
                                   placeholder="">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>Contact Email</label>
                            <input type="text" name="contact_email" class="form-control form-control-line input-text" value="{{ isset( $banks->contact_email ) ? $banks->contact_email: old('contact_email') }}"
                                       placeholder="">
                        </div>

                        <div class="col-sm-6">
                            <label>Address</label>
                            <textarea name="address" class="form-control form-control-line input-text">{{ isset( $banks->address ) ? $banks->address : old('address') }}</textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>Notes</label>
                            <textarea name="notes" class="form-control form-control-line input-text">{{ isset( $banks->notes ) ? $banks->notes : old('notes') }}</textarea>
                        </div>

                        <div class="col-sm-6">
                            <label>Select location</label>
                            <input type="text" id="google_address" name="google_address" class="form-control form-control-line input-text" value=""
                                   placeholder="Addresse suchen.">
                                   <input type="hidden" name="location_latitude" id="location_latitude" >
                                   <input type="hidden" name="location_longitude" id="location_longitude" >
                        </div>
                    
                    </div>
                    {{-- <div class="form-group">
                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.With_real_ek')}} (%)</label>
                            <input type="number" name="with_real_ek" class="form-control form-control-line input-text" value="{{ isset( $banks->with_real_ek ) ? $banks->with_real_ek * 100 : old('with_real_ek') }}"
                                       placeholder=""  step="0.01">
                        </div>
                    
                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.from_bond')}} (%)</label>
                            <input type="number" name="from_bond" class="form-control form-control-line input-text" value="{{ isset( $banks->from_bond ) ? $banks->from_bond * 100 : old('from_bond') }}"
                                   placeholder=""  step="0.01">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.bank_loan')}} (%)</label>
                            <input type="number" name="bank_loan" class="form-control form-control-line input-text" value="{{ isset( $banks->bank_loan ) ? $banks->bank_loan * 100 : old('bank_loan') }}"
                                   placeholder=""  step="0.01">
                        </div>
                    
                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.interest_bank_loan')}} (%)</label>
                            <input type="number" name="interest_bank_loan" class="form-control form-control-line input-text" value="{{ isset( $banks->interest_bank_loan ) ? $banks->interest_bank_loan * 100 : old('interest_bank_loan') }}"
                                   placeholder=""  step="0.01">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.eradication_bank')}} (%)</label>
                            <input type="number" name="eradication_bank" class="form-control form-control-line input-text" value="{{ isset( $banks->eradication_bank ) ? $banks->eradication_bank * 100 : old('eradication_bank') }}"
                                   placeholder=""  step="0.01">
                        </div>
                    
                        <div class="col-sm-6">
                            <label>{{__('banks.add_edit.interest_bond')}} (%)</label>
                            <input type="number" name="interest_bond" class="form-control form-control-line input-text" value="{{ isset( $banks->interest_bond ) ? $banks->interest_bond * 100 : old('interest_bond') }}"
                                   placeholder=""  step="0.01">
                        </div>
                    </div>--}}


                    <div class="form-group">
                        <div class="col-lg-2 col-sm-4 col-xs-12">
                            <a href="{{route('banks')}}" type="button"
                               class="btn btn-block btn-danger btn-rounded">{{__('banks.add_edit.cancel')}}</a>
                        </div>
                        <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12">
                            <button type="submit" name="submit" class="btn btn-block btn-primary btn-rounded">{{__('banks.add_edit.submit')}}
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

@endsection

@section('js')
    <script type='text/javascript' src='//maps.googleapis.com/maps/api/js?libraries=places&#038;key=AIzaSyA5o1eFEZtJOyhgsDdJp0IHMwMIIi8LPgw&#038;callback'></script>
    <script src="{{ asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            
            function testMapAutoCompleteInit() {
            
            var input = document.getElementById( 'google_address' );

            // Exit if no dropdown div is found.
            if ( null === input ) {
                return;
            }

            autoComplete = new google.maps.places.Autocomplete(input);

            autoComplete.setFields(['address_components', 'geometry', 'icon', 'name']);
            // Avoid paying for data that you don't need by restricting the set of
            // place fields that are returned to just the address components.
            // autoComplete.setFields(['address_component']);

            // When the user selects an address from the drop-down, populate the
            // address fields in the form.
            autoComplete.addListener('place_changed', fillInAddress);
            
            }
        
            function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autoComplete.getPlace();
            console.log(place.geometry.location.lat());
            console.log(place.geometry.location.lng());
            $("#location_latitude").val(place.geometry.location.lat());
            $("#location_longitude").val(place.geometry.location.lng());
            }

            // Trigger the listener.
            google.maps.event.addDomListener( window, 'load', testMapAutoCompleteInit );
        });
    </script>
@endsection