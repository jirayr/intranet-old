@extends('layouts.guest')

@section('content')
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="new-login-register">
        <div class="new-login-box" style="margin-left: auto; margin-right: auto">
            <div class="white-box" style="padding: 0px;margin-bottom: 0px;padding-left: 25px; margin-top: -90px"> <img src="/img/logo.png" alt="" style="width: 100%"> </div>
            <div class="white-box">

                <form class="form-horizontal" method="POST" action="{{ route('checkPassword') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>{{__('auth.login.password')}}</label>
                            <input id="password" type="password" class="form-control" name="password" required>
                            @if (session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif               </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">get access</button>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform" action="index.html">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>{{__('auth.login.recover_password')}}</h3>
                            <p class="text-muted">{{__('auth.login.enter_email')}} </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="{{__('auth.login.email')}}">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{__('auth.login.reset')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </section>
@endsection