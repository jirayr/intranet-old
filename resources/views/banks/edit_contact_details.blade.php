@extends('layouts.admin')

@section('css')

    <link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">

@endsection



@section('content')

    <div class="row">

        @if ($errors->any())

            <div class="alert alert-danger">

                <ul>

                    @foreach ($errors->all() as $error)

                        <li>{{ $error }}</li>

                    @endforeach

                </ul>

            </div>

        @endif



         @if (session()->has('success'))

            <div class="alert alert-success">

                {{ session()->get('success') }}

            </div>

        @endif

        <form method="post" action="{{ url('banks/contact_delails_edit') }}">

        {{-- {!! Form::open(['action' => ['BanksController@' . $type, isset( $banks->id ) ? $banks->id : null], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!} --}}

        {{-- {!! Form::token() !!} --}}

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <input type="hidden" name="bank_id" value="{{ $banks->id }}">

        <input type="hidden" name="contact_id" value="0" id="contact_id">



        <div class="col-md-12 col-lg-12 col-sm-12">

            <div class="panel">

               

                <div>

               

                    <div class="form-group">

                       

                        <div class="col-sm-4">

                            <label>First Name</label>

                            <input type="text" name="firstName" class="form-control form-control-line input-text" value="@if(old('firstName')) {{ old('firstName') }} @endif"

                                   placeholder="" id="firstName">

                        </div>



                        <div class="col-sm-4">

                            <label>Last Name</label>

                            <input type="text" name="surname" class="form-control form-control-line input-text" value="@if(old('surname')) {{ old('surname') }} @endif"

                                   placeholder="" id="surname">

                        </div>

                    </div>





                    <div class="form-group">

                        <div class="col-sm-4">

                            <label>Contact Name</label>

                            <input type="text" name="contact_name" class="form-control form-control-line input-text" value="@if(old('contact_name')) {{ old('contact_name') }} @endif"

                                       placeholder="" id="contact_name">

                        </div>



                        <div class="col-sm-3">

                            <label>Contact Phone</label>

                            <input type="text" name="contact_phone" class="form-control form-control-line input-text" value="@if(old('contact_phone')) {{ old('contact_phone') }} @endif"

                                   placeholder="" id="contact_phone">

                        </div>





                        <div class="col-sm-3">

                            <label>fax</label>

                            <input type="text" name="fax" class="form-control form-control-line input-text" value="@if(old('fax')) {{ old('fax') }} @endif"

                                   placeholder="" id="fax">

                        </div>



                    </div>

                 

                    <div class="form-group">

                        <div class="col-sm-6">

                            <label>Contact Email</label>

                            <input type="text" name="contact_email" class="form-control form-control-line input-text" value="@if(old('contact_email')) {{ old('contact_email') }} @endif"

                                       placeholder="" id="contact_email">

                        </div>



                        <div class="col-sm-6">

                            <label>Address</label>

                            <textarea name="address" class="form-control form-control-line input-text" id="address">@if(old('address')) {{ old('address') }} @endif</textarea>

                        </div>

                    </div>



                    <div class="form-group">

                        <div class="col-sm-6">

                            <label>city</label>

                            <input type="text" name="city" class="form-control form-control-line input-text" value="@if(old('city')) {{ old('city') }} @endif"

                                   placeholder="" id="city">

                        </div>

                      </div>



                      <div class="form-group">

                          <div class="col-sm-6" style="margin-top: 35px;">

                            <label>Postal Code</label>

                            <input type="text" name="postal_code" class="form-control form-control-line input-text" value="@if(old('postal_code')) {{ old('postal_code') }} @endif"

                                   placeholder="" id="postal_code">

                        </div>

                        <div class="col-sm-6" style="margin-top: -70px;">

                            <label>district</label>

                            <textarea name="district" class="form-control form-control-line input-text" id="district">@if(old('district')) {{ old('district') }} @endif</textarea>

                        </div>

                      </div>







                    <div class="form-group">

                        <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12" style="margin-top: 25px;">

                            <button type="submit" class="btn btn-block btn-primary btn-rounded" id="submit">{{-- {{__('banks.add_edit.submit')}} --}}Submit

                            </button>

                        </div>

                        <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12" style="margin-top: 25px;">

                            <button type="button" style="display: none;" id="update" class="btn btn-block btn-danger btn-rounded" >{{-- {{__('banks.add_edit.submit')}} --}}Update

                            </button>

                        </div>

                    </div>



                </div>

            </div>

        </div>

</form>

        {{-- {!! Form::close() !!} --}}

    </div>



    <hr>



    <div class="row">

        <div class="col-md-12">

            <div class="table-responsive">

                    <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">

                        <thead>

                        <tr>

                            <th>Full Name</th>

                            <th>Contact Email</th>

                            <th>Contact Name</th>

                            <th>Contact phone</th>

                            <th>City</th>

                            <th>Distric</th>

                            <th>Address</th>

                            <th>Postal Code</th>

                            <th>Fax</th>

                            <th>Action</th>

                        </tr>

                        </thead>

                        <tbody>

                            @if(isset($contact) && count($contact) > 0)

                            @foreach( $contact as $i => $info )

                            <tr>                

                                <td>{{ $info->firstName }} {{ $info->surname }}</td>

                                <td>{{ $info->contact_email }}</td>

                                <td>{{ $info->contact_name }}</td>

                                <td>{{ $info->contact_phone }}</td>

                                <td>{{ $info->city }}</td>

                                <td>{{ $info->district }}</td>

                                <td>{{ $info->address }}</td>

                                <td>{{ $info->postal_code }}</td>

                                <td>{{ $info->fax }}</td>

                                <td>

                                    <button type="button" class="btn btn-info btn-outline btn-circle btn-lg m-r-5 edit_contact" edit_id="{{ $info->id }}"><i class="ti-pencil-alt"></i></button>

                                    <a href="{{ url('/banks/delete_contact_info'.'/'.$info->id) }}" class="btn btn-info btn-outline btn-circle btn-lg m-r-5 edit_contact"><i class="ti-trash"></i></a>

                                </td>

                            </tr>

                            @endforeach

                        </tbody>

                    </table>

                    <div class="pagination-wrapper" style="margin-left: 20px">

                        <div class="row pull-left">

                            {{ $contact->links() }}

                        </div>

                    </div>

                    @else

                    <style>

                        .footer{

                            display: none;

                        }

                    </style>

                    @endif



                </div>

        </div>

    </div>



@endsection



@section('js')

    <script src="{{ asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            // Basic

            $('.dropify').dropify();

            // Translated

            $('.dropify-fr').dropify({

                messages: {

                    default: 'Glissez-déposez un fichier ici ou cliquez',

                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',

                    remove: 'Supprimer',

                    error: 'Désolé, le fichier trop volumineux'

                }

            });

        });

    </script>



<script>

    var app_url = "{{ url('/') }}";

    ////////// edit appliction

$(document).on("click", '.edit_contact', function(e) { 



  var edit_id = $(this).attr('edit_id');



  $.ajax({ 

    

    url: app_url + "/banks/contact_fetch",

    data: {edit_id: edit_id, _token: "{{ csrf_token() }}"},

    type: "post",

    success: function(response) {

      $("#contact_id").val('');

      $("#contact_id").val(response.id);

      $("#firstName").val(response.firstName);

      $("#surname").val(response.surname);

      $("#contact_name").val(response.contact_name);

      $("#contact_phone").val(response.contact_phone);

      $("#fax").val(response.fax);

      $("#contact_email").val(response.contact_email);

      $("#address").val(response.address);

      $("#city").val(response.city);

      $("#district").val(response.district);

      $("#postal_code").val(response.postal_code);

      

   

    } 

  });

});

</script>







@endsection