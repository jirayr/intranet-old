@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="card-header">
            <div class="row">
                <div class="col-md-8">
                    <h4 class="card-title">@if(isset($bankTransactions[0])){{$bankTransactions[0]->account->name_of_property}} @endif</h4>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="table-responsive">
                    <table id="bankTransactionsTable" class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th>Date of Transaction</th>
                            <th>Amount</th>
                            <th>Bank Transaction Type</th>
                            <th>Remaining Balance</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bankTransactions as $bankTransaction)
                            <tr>
                                <td>{{date('Y-m-d',strtotime($bankTransaction->date_of_transaction))}}</td>
                                <td>{{number_format($bankTransaction->amount,2,',','.')}} €</td>
                                <td>@if($bankTransaction->bank_transaction_type == 'D') Debit @else Credit @endif</td>
                                <td>{{number_format($bankTransaction->remaining_balance,2,',','.')}}€</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper" style="margin-left: 20px">
                        <div class="row pull-left">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('js')

    <script>
        $(document).ready(function () {
            $('#bankTransactionsTable').DataTable({
                "pageLength": 25
            });
        });
    </script>

@stop

