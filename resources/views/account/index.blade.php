@extends('layouts.admin')
@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="panel">
                    <div class="panel-heading">Account</div>

                    <div class="table-responsive">
                        <table id="accountTable" class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Property Name</th>
                                <th>Bank Account</th>
                                <th>Iban</th>
                                <th>Latest Balance</th>
                                <th>Last Update Date</th>
                            </tr>
                            </thead>
                            <tbody>
                                @isset($accounts)
                                        @foreach($accounts as $account)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$account->name_of_property}}</td>
                                                <td><a href="{{url('/bank-transaction/'.$account->id)}}">{{$account->account_owner}}</a></td>
                                                <td>{{$account->iban}}</td>
                                                <td>{{number_format($account->balance,2,',','.')}}€</td>
                                                <td>{{$account->last_updated_at}}</td>
                                            </tr>
                                        @endforeach
                                 @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section('js')

<script>
    $(document).ready(function () {
        $('#accountTable').DataTable({
            "pageLength": 25
        });
    });
</script>

@stop
