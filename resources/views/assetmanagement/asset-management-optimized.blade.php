@extends('layouts.admin')

@section('css')

    <link href="{{ asset('assets/x-editable/bootstrap-editable.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
    <style type="text/css">
    	@media (min-width: 768px)
		{
			.modal-dialog.custom {
			    width: 848px;
			    margin: 30px auto;
			}
		}
    	@media (min-width: 992px)
	    {    .modal-lg {
	            width: 1320px;
	        }
	    }
    </style>

@endsection

@section('content')

	<div class="row">

		<?php
			$s1 = $s2 = 0;
			$sum_flat_in_qm = 0;

			foreach($analytics_array as $key => $masterliste){
				$s1 += $masterliste['rented_area'];
				$s2 += $masterliste['vacancy'];
				$sum_flat_in_qm +=$masterliste['flat_in_qm'];
			}

			if($sum_flat_in_qm != 0){
				$summ_vermietet_percent = $s1 / $sum_flat_in_qm * 100;
        		$summ_leerstand_percent = $s2 / $sum_flat_in_qm * 100;	
			}
		?>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Vermietet(m2)</h3>
				<ul class="list-inline two-part">
					<li><div class="sparklinedash"></div></li>
					<li class="text-right"><span class="text-success">{{ number_format($s1,0,",",".") }}</span></li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Vermietet (%)</h3>
				<ul class="list-inline two-part">
					<li><div class="sparklinedash"></div></li>
					<li class="text-right"><span class="text-success">{{ number_format($summ_vermietet_percent,2,",",".") }}</span></li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Leerstand(m2)</h3>
				<ul class="list-inline two-part">
					<li><div class="sparklinedash"></div></li>
					<li class="text-right"><span class="text-success">{{ number_format($s2,0,",",".") }}</span></li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Leerstand(%)</h3>
				<ul class="list-inline two-part">
					<li><div class="sparklinedash"></div></li>
					<li class="text-right">
						<span class="text-success">{{ number_format($summ_leerstand_percent,2,",",".") }}</span>
					</li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Wault</h3>
				<ul class="list-inline two-part">
					<li><div class="sparklinedash"></div></li>
					<li class="text-right"><span class="text-success">{{ number_format($avg_wault,1,",",".") }}</span>
					</li>
				</ul>
			</div>
		</div>

	</div>

	<div class="row" id="assetmanagement">
		<div class="col-sm-12">
			<div class="white-box">
				<div class="table-responsive" style="margin-bottom: 30px;">
					<form action="{{route('assetmanagementfilter')}}" method="POST">
						{{ csrf_field() }}
						<table class="table table-striped" id="dashboard-table-2">
							<thead>
								<tr>
									<th><br>Asset Manager Bestand</th>
									@if($is_asset_manager)	
		 								<th><br>Objekt</th>	
									@else	
										<th>Anzahl</th>	
									@endif
									<th>Miete Netto p.a.</th>
									<th>Fläche in m<sup>2</sup></th>
									<th>Vermietet m<sup>2</sup></th>
									<th>Leerstand m<sup>2</sup></th>
									<th>Quote</th>
									<th>Mieter</th>
									<th>Leerstand</th>
									<th>Potenzial p.a.</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sum['count'] = 0;
									$sum['flat_in_qm'] = 0;
									$sum['rented_area'] = 0;
									$sum['vacancy'] = 0;
									$sum['Quote'] = 0;
									$sum['Miete'] = 0;
									$sum['Potenzial'] = 0;
									$sum['Rents'] = 0;
									$sum['Vacants'] = 0;
								?>

								@foreach($analytics_array as $key => $masterliste)

									<?php
										$sum['count'] += $masterliste['count'];
										$sum['flat_in_qm'] += $masterliste['flat_in_qm'];
										$sum['rented_area'] += $masterliste['rented_area'];
										$sum['vacancy'] += $masterliste['vacancy'];
										if( $sum['flat_in_qm'] != 0 )
											$sum['Quote'] = $sum['vacancy'] / $sum['flat_in_qm'] * 100;
										else
											$sum['Quote'] = 0;

										$sum['Potenzial']+= $masterliste['potential_pm'];
										$sum['Rents'] += $masterliste['rents'];
										$sum['Vacants'] += $masterliste['vacants'];
									?>

									<tr>
										<td>
											<a href="javascript:void(0)" class="get-asset-property" data-value="{{$masterliste['asset_manager']}}">
											{{$masterliste['asset_manager_name']}}</a>
										</td>

										@if($is_asset_manager)	
		 									<td>
		                                    	<a href="{{route('properties.show',['property'=>$masterliste['id']])}}">{!!$masterliste['object']!!}</a>	
											</td>	
										@else	
											<td>{{number_format($masterliste['count'],0,",",".")}}</td>	
										@endif

										<td class="number-right">

											@if($is_asset_manager)
												<?php
													if(isset($pa_array2[$masterliste['id']])){
														$sum['Miete'] +=12*$pa_array2[$masterliste['id']];
														echo number_format(12*$pa_array2[$masterliste['id']],2,",",".");
													}
												?>
											@else
												<?php 
													if(isset($pa_array[$key])){
														$sum['Miete'] += 12*$pa_array[$key];
														echo number_format(12*$pa_array[$key],2,",",".");
													}else{
														echo number_format($masterliste['rent_net_pm'],2,",",".");
													}
												?>
									
											@endif
										</td>

										<td class="number-right">{{number_format($masterliste['flat_in_qm'],2,",",".")}}</td>
										<td class="number-right">{{number_format($masterliste['rented_area'],2,",",".")}}</td>
										<td class="number-right">{{number_format($masterliste['vacancy'],2,",",".")}}</td>
										<td class="number-right">
											{{$masterliste['flat_in_qm'] > 0 ? number_format($masterliste['vacancy']/$masterliste['flat_in_qm']*100,2,",",".") :0}}%
										</td>
										<td class="number-right">
											<a href="javascript:void(0)" class="get-rents" data-value="{{$masterliste['asset_manager']}}" data-type="r" data-property="0">{{number_format($masterliste['rents'],0,",",".")}}</a>
										</td>
										<td class="number-right">
											<a href="javascript:void(0)" class="get-rents" data-value="{{$masterliste['asset_manager']}}" data-type="v" data-property="0">{{number_format($masterliste['vacants'],0,",",".")}}</a>
										</td>
										<td class="number-right">{{number_format($masterliste['potential_pm'],2,",",".")}}</td>
									</tr>
								@endforeach
							</tbody>
							<tr>
								<th>Summe</th>
								<th>{{number_format($sum['count'],0,",",".")}}</th>
								<td class="number-right">{{number_format($sum['Miete'],2,",",".")}}</td>
								
								<td class="number-right">{{number_format($sum['flat_in_qm'],2,",",".")}}</td>
								<td class="number-right">{{number_format($sum['rented_area'],2,",",".")}}</td>
								<td class="number-right">{{number_format($sum['vacancy'],2,",",".")}}</td>
								<td class="number-right">{{number_format($sum['Quote'],2,",",".")}}%</td>
								<td class="number-right">
								<a href="javascript:void(0)" class="get-rents" data-value="0" data-type="r" data-property="0">{{number_format($sum['Rents'],0,",",".")}}</a></td>
								<td class="number-right">
									<a href="javascript:void(0)" class="get-rents" data-value="0" data-type="v" data-property="0">{{number_format($sum['Vacants'],0,",",".")}}</a></td>
								<td class="number-right">{{number_format($sum['Potenzial'],2,",",".")}}</td>
							</tr>
							<tr>
								<th colspan="10"></th>
							</tr>
						</table>
					</form>
				</div>
			
			</div>
		</div>

		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Differenz Miete (Soll/Ist) (€)</h3>
				<p class="text-muted m-b-30">&nbsp;</p>
				<div class="table-responsive">
					<table id="tenant-table1" class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Objekt</th>
								<th>Differenz Miete (Soll/Ist) in €</th>
								<th>Differenz Miete (Soll/Ist) p.a. in €</th>
							</tr>
						</thead>
						<tbody>
							@foreach($masterlistes as $masterliste)
								<tr>
									<td>{{$masterliste->id}}</td>
									<td>
										@if(isset($diffff[$masterliste->id]['property_id']))
											<a href="{{route('properties.show',['property'=>$diffff[$masterliste->id]['property_id']])}}">{{ $masterliste->object }}</a>
										@else
											<a href="#" class="inline-edit" data-type="text" data-pk="object" data-url="{{route('masterliste.update', $masterliste->id) }}">{{ $masterliste->object }}</a>
										@endif
									</td>
									<td class="number-right">
										@if(isset($diffff[$masterliste->id]['amount']))
											{{  number_format($diffff[$masterliste->id]['amount'],2,",",".") }}
										@else
											0
										@endif
									</td>
									<td class="number-right">
										@if(isset($diffff[$masterliste->id]['amount']))
											{{  number_format($diffff[$masterliste->id]['amount']*12,2,",",".") }}
										@else
											0
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Auslaufende Mietverträge</h3>
				<p class="text-muted m-b-30">&nbsp;</p>
				<div class="table-responsive">
					<table id="tenant-table2" class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Asset Manager</th>
								<th>Objekt</th>
								<th>Vermietet</th>
								<th>Mietende</th>
								<th>Mietfläche (m2)</th>
								<th>IST-Nettokaltmiete (€)</th>
								<th>Typ</th>
								<th>Kommentare</th>
							</tr>
						</thead>
						<tbody>
							@foreach($tenancy_items_2 as $tenancy_item)
								<tr>
									<td>{{$tenancy_item->id}}</td>
									<td>{{$tenancy_item->creator_name}}</td>
									<td>{{$tenancy_item->object_name}}</td>
									<td>{{$tenancy_item->name}}</td>
									<td>{{show_date_format($tenancy_item->rent_end)}}</td>
									<td class="number-right">{{$tenancy_item->rental_space ? number_format($tenancy_item->rental_space,2,",",".") : 0}}</td>
									<td class="number-right">{{$tenancy_item->actual_net_rent ? number_format($tenancy_item->actual_net_rent,2,",",".") : 0}}</td>
									<td>{{$tenancy_item->type}}</td>
									<td>{{ ($tenancy_item->comment1) ? $tenancy_item->comment1 : $tenancy_item->comment }}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Leerstände (€)</h3>
				<p class="text-muted m-b-30">&nbsp;</p>
				<div class="table-responsive">
					<table id="tenant-table3" class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Asset Manager</th>
								<th>Objekt</th>
								<th>Leerstand</th>
								<th>Fläche (m2)</th>
								<th>Leerstand (€)</th>
								<th>Typ</th>
								<th>Kommentare</th>
							</tr>
						</thead>
						<tbody>
							@foreach($tenancy_items as $tenancy_item)
								<tr>
									<td>{{$tenancy_item->id}}</td>
									<td>{{$tenancy_item->creator_name}}</td>
									<td>{{$tenancy_item->object_name}}</td>
									<td>{{$tenancy_item->name}}</td>
									<td class="number-right">{{$tenancy_item->vacancy_in_qm ? number_format($tenancy_item->vacancy_in_qm,2,",",".") : 0}}</td>
									<td class="number-right">{{$tenancy_item->vacancy_in_eur ? number_format($tenancy_item->vacancy_in_eur,2,",",".") : 0}}</td>
									<td>{{$tenancy_item->type}}</td>
									<td>{{$tenancy_item->comment}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="property-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Property List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body list-data"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="rent-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog custom" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Mieter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body list-data-rent"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vacant-list" tabindex="9999" role="dialog" aria-labelledby="Bank" aria-hidden="true">
        <div class="modal-dialog custom" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Leerstandsflächen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body vlist-data-rent"></div>
            </div>
        </div>
    </div>
	
@endsection

@section('js')
	<script type="text/javascript">
		var _token = '{{ csrf_token() }}';

		var url_assetmanagement_optimized = '{{ route('assetmanagement_optimized') }}';
	</script>

    <script src="{{ asset('assets/x-editable/bootstrap-editable.min.js') }}" ></script>
    <script src="{{asset('js/custom-datatable.js')}}"></script>
	<script src="{{asset('js/morris.min.js')}}"></script>
    <script src="{{asset('js/asset-management-optimized.js')}}"></script>
@endsection
