
<table class="table table-striped rent-compare-list">
	<thead>
	<tr>
		<th>Objekt</th>
		<th>AM</th>
		<th>Miete Netto p.m.</th>
		<th>Miete Netto p.a.</th>
		<th>Fläche in m<sup>2</sup></th>
		<th>Mieter</th>
		<th>Mietbegin</th>
		<th>Mietende</th>
							
	</tr>
	</thead>
	<tbody>
		<?php
		$total1 = $total2 = 0;
		?>
		@foreach($rental_agreements as $value)
		<tr>
			<td><a href="{{route('properties.show',['property'=>$value->property_id])}}">

										{!!$value->name_of_property!!}
									</a></td>
			<td>{{user_short_name($value->am_name)}}</td>
			<?php
			if($value->actual_net_rent)
			$total1 += $value->actual_net_rent;
			if($value->rental_space)
			$total2 += $value->rental_space;
			?>
			<td>{{show_number($value->actual_net_rent,2)}}</td>
			<td>{{show_number($value->actual_net_rent*12,2)}}</td>
			<td>{{show_number($value->rental_space,2)}}</td>
			<td>{{$value->name}}</td>
			<td>{{show_date_format($value->rent_begin)}}</td>
			<td>{{show_date_format($value->rent_end)}}</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th colspan="2">Gesamt</th>
			<th colspan="">{{show_number($total1,2)}}</th>
			<th colspan="">{{show_number($total1*12,2)}}</th>
			<th colspan="">{{show_number($total2,2)}}</th>
			<th colspan=""></th>
			<th colspan=""></th>
			<th colspan=""></th>
		</tr>
	</tfoot>
</table>
						