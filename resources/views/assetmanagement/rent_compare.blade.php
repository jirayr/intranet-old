<table class="table table-striped rent-compare">
	<thead>
	<tr>
		<th>AM</th>
		<th>2017</th>
		<th>Anzahl</th>
		<th>2018</th>
		<th>Anzahl</th>
		<th>in %</th>
		<th>2019</th>
		<th>Anzahl</th>
		<th>in %</th>
		<th>2020</th>
		<th>Anzahl</th>
		<th>in %</th>
	</tr>
	</thead>
	<tbody>
		@foreach($rent_array as $key=>$list)
		<tr>
			<td>{{$key}}</td>
			<td><a href="javascript:void(0)" data-month="{{$month}}" data-year="2017" data-user-id="{{$rent_array2[$key]}}" class="get-rent-list-details">{{show_number(@$list[2017],2)}}</a></td>

			<td>{{@$rent_array3[$key][2017]}}</td>

			<?php
			$amount1 = 0;
			if(@$list[2017])
			$amount1 = $list[2017];

			$amount2 = 0;
			if(@$list[2018])
			$amount2 = $list[2018];


			$per = 0;
			if($amount1 && $amount2)
			{
				$per = $amount2*100/$amount1;	
			}
			?>

			<td><a href="javascript:void(0)" data-month="{{$month}}" data-year="2018" data-user-id="{{$rent_array2[$key]}}" class="get-rent-list-details">{{show_number(@$list[2018],2)}}</a></td>
			<td>{{@$rent_array3[$key][2018]}}</td>

			<td>{{show_number($per,2)}}</td>
			<td><a href="javascript:void(0)" data-month="{{$month}}" data-year="2019" data-user-id="{{$rent_array2[$key]}}" class="get-rent-list-details">{{show_number(@$list[2019],2)}}</a></td>
			<td>{{@$rent_array3[$key][2019]}}</td>

			<?php
			$amount1 = 0;
			if(@$list[2018])
			$amount1 = $list[2018];

			$amount2 = 0;
			if(@$list[2019])
			$amount2 = $list[2019];


			$per = 0;
			if($amount1 && $amount2)
			{
				$per = $amount2*100/$amount1;	
			}
			?>
			<td>{{show_number($per,2)}}</td>
			
			<td><a href="javascript:void(0)" data-month="{{$month}}" data-year="2020" data-user-id="{{$rent_array2[$key]}}" class="get-rent-list-details">{{show_number(@$list[2020],2)}}</a></td>
			<td>{{@$rent_array3[$key][2020]}}</td>

			<?php
			$amount1 = 0;
			if(@$list[2019])
			$amount1 = $list[2019];

			$amount2 = 0;
			if(@$list[2020])
			$amount2 = $list[2020];


			$per = 0;
			if($amount1 && $amount2)
			{
				$per = $amount2*100/$amount1;	
			}
			?>
			<td>{{show_number($per,2)}}</td>
			
		</tr>
		@endforeach
	</tbody>
</table>
						