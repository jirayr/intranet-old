						<table class="table table-striped" id="list-rent">
						<thead>
						<tr>
							<th><br>Objekt</th>
							<th><br>Mieter</th>
							<th class="text-right">Mietfläche</th>
							<th class="text-right"> Miete netto p.a.</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$sum1 = 0;
						$sum2 = 0;
						?>


						@foreach($tenant_data as $key => $row)
							@foreach($row['tenant']['tenancy_schedules'] as $key => $tenancy_schedule)

							@foreach($tenancy_schedule->items as $item)


							<?php

							if(($item->rent_begin != '' && $item->rent_begin > date('Y-m-d')) ||($item->rent_end != '' && $item->rent_end < date('Y-m-d')))
                   				continue;


							if ($item->rental_space && $item->actual_net_rent && ($item->type == config('tenancy_schedule.item_type.live') || $item->type == config('tenancy_schedule.item_type.business'))):

								$i_arr[] = $item->id;

								?>
							<tr>
								<td>
									<a href="{{route('properties.show',['property'=>$row['id']])}}">
									{{$row['name']}}
									</a>
								</td>
								<td>
									{{$item->name}}
								</td>
								<td class="text-right">
									{{ number_format($item->rental_space, 2,",",".") }}
								</td>
								<td class="text-right">
									{{ number_format($item->actual_net_rent*12, 2,",",".") }}&nbsp;€
								</td>
							</tr>
							<?php endif; ?>
							@endforeach		
							@endforeach
						@endforeach
						</tbody>

						<tr>
							<th colspan="10"></th>
						</tr>
					</table>
					<?php //echo json_encode($i_arr); ?>
					