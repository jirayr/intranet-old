<?php
function short_name($string)
{
    if($string){
        $arr = explode(" ", $string);
        if(count($arr)>1)
            return substr($arr[0], 0,1).$arr[1];
        else
            return substr($string, 0,1);
    }
    return $string;
}

?>

						<table class="table table-striped" id="@if(isset($id) && $id==-1)list-properties{{$id}}@else {{'list-properties'}} @endif">
						<thead>
						<tr>
							<th>Objekt</th>
							<th>AM</th>
							<th>TM</th>
							<th>Notartermin</th>
							<th>Miete Netto p.a.</th>
							<th>Fläche in m<sup>2</sup></th>
							<th>Leerstand m<sup>2</sup></th>
							<th>Mieter</th>
							<th>Anzahl Leerstand</th>
							<th>Potenzial p.a.</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$sum['count'] = 0;
						$sum['flat_in_qm'] = 0;
						$sum['rented_area'] = 0;
						$sum['vacancy'] = 0;
						$sum['Quote'] = 0;
						$sum['Miete'] = 0;
						$sum['Potenzial'] = 0;
						$sum['Rents'] = 0;
						$sum['Vacants'] = 0;
						?>
						<?php $s = 0; ?>

						@foreach($analytics_array as $key => $masterliste)
							<?php

							if($masterliste['asset_manager'])
									continue;
							
							$s += 1;
							$sum['count'] += 1;
							$sum['flat_in_qm'] += $masterliste['flat_in_qm'];
							$sum['rented_area'] += $masterliste['rented_area'];
							$sum['vacancy'] += $masterliste['vacancy'];
							if( $sum['flat_in_qm'] != 0 )
								$sum['Quote'] = $sum['vacancy'] / $sum['flat_in_qm'] * 100;
							else
								$sum['Quote'] = 0;

							$sum['Miete'] += $masterliste['rent_net_pm'];
							$sum['Potenzial']+= $masterliste['potential_pm'];
							$sum['Rents'] += $masterliste['rents'];
							$sum['Vacants'] += $masterliste['vacants'];
							
							?>
							<tr>
								<td>
                                    <a href="{{route('properties.show',['property'=>$masterliste['id']])}}">

										{!!$masterliste['object']!!}
									</a>
									</td>
									<td>
									@if(Auth::user()->email == config('users.falk_email') || Auth::user()->email == "c.wiedemann@fcr-immobilien.de")
										<select class="asset-manager form-control input-sm" data-property="{{ $masterliste['id'] }}">
											<option value="">Wählen</option>
											@if(!empty($AssetManager))
												@foreach ($AssetManager as $am)
													<option value="{{ $am->id }}" {{ ($masterliste['asset_manager'] == $am->id) ? 'selected' : '' }} > {{ short_name($am->name) }} </option>
												@endforeach
											@endif
										</select>
									@else
										{{ short_name($masterliste['asset_manager_name']) }}
									@endif
								</td>
								<td>

								@if($masterliste['transaction_m_id'])

								@php
                                $user =  DB::table('users')->where('id', $masterliste['transaction_m_id'])->first();

                                    if($user){
                                        echo short_name($user->name);
                                    }
                                @endphp

                                @endif
                                </td>
                                <td>{{show_date_format($masterliste['purchase_date'])}}</td>
									
									<td class="number-right">
								<?php
									echo number_format($masterliste['rent_net_pm']*12,2,",",".");
								?>
								</td>
								
								<td class="number-right">{{number_format($masterliste['flat_in_qm'],2,",",".")}}</td>
								<td class="number-right">{{number_format($masterliste['vacancy'],2,",",".")}}</td>
								
								<!-- <td class="number-right">{{number_format($masterliste['flat_in_qm'],2,",",".")}}</td>
								<td class="number-right">{{number_format($masterliste['rented_area'],2,",",".")}}</td>
								<td class="number-right">{{number_format($masterliste['vacancy'],2,",",".")}}</td>
								<td class="number-right">{{$masterliste['flat_in_qm'] > 0 ? number_format($masterliste['vacancy']/$masterliste['flat_in_qm']*100,2,",",".") :0}}%</td> -->
								<td class="number-right">
									<a href="javascript:void(0)" class="get-rents" data-value="0" data-type="@if(isset($id) && $id==-1){{'r1'}}@else{{'r'}}@endif" data-property="{{$masterliste['id']}}">							{{number_format($masterliste['rents'],0,",",".")}}</a></td>
								<td class="number-right">
									<a href="javascript:void(0)" class="get-rents" data-value="0" data-type="@if(isset($id) && $id==-1){{'v1'}}@else{{'v'}}@endif" data-property="{{$masterliste['id']}}">							{{number_format($masterliste['vacants'],0,",",".")}}</a></td>
								
								
								<td class="number-right">{{number_format($masterliste['potential_pm'],2,",",".")}}</td>
							</tr>
						@endforeach
						</tbody>
						@if(isset($id) && $id==-1)
						@else
						<tr>
							<th>Summe</th>
							<th>{{number_format($sum['count'],0,",",".")}}</th>
							<td class="number-right">{{number_format($sum['Miete'],2,",",".")}}</td>
							<td class="number-right">{{number_format($sum['flat_in_qm'],2,",",".")}}</td>
							<td class="number-right">{{number_format($sum['rented_area'],2,",",".")}}</td>
							<td class="number-right">{{number_format($sum['vacancy'],2,",",".")}}</td>
							<td class="number-right">{{number_format($sum['Quote'],2,",",".")}}%</td>
							
							<td class="number-right">{{number_format($sum['Rents'],0,",",".")}}</td>
							<td class="number-right">{{number_format($sum['Vacants'],0,",",".")}}</td>
							<td class="number-right">{{number_format($sum['Potenzial'],2,",",".")}}</td>
						</tr>
						@endif
						<tr>
							<th colspan="11"></th>
						</tr>
					</table>
					<input type="hidden" class="not-beur-sum" value="{{$s}}">
					
					