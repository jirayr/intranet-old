<?php
function short_name($string)
{
    if($string){
        $arr = explode(" ", $string);
        if(count($arr)>1)
            return substr($arr[0], 0,1).substr($arr[1], 0,1);
        else
            return substr($string, 0,1);
    }
    return $string;
}

?>

<form action="{{route('assetmanagementfilter')}}" method="POST">
	{{ csrf_field() }}
	<table class="table table-striped" id="dashboard-table-2">
	<thead>
	<tr>
	<th><br>Asset Manager Bestand</th>
		@if($is_asset_manager)	
 			<th><br>Objekt</th>	
		@else	
			<th>Anzahl</th>	
		@endif
		<th>Miete Netto p.a.</th>
		<th>Fläche in m<sup>2</sup></th>
		<th>Vermietet m<sup>2</sup></th>
		<th>Leerstand m<sup>2</sup></th>
		<th>Quote</th>
		<th>Mieter</th>
		<th>Leerstand</th>
		<th>Potenzial p.a.</th>
		<th>Potenzial p.a.(%)</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$sum['count'] = 0;
	$sum['flat_in_qm'] = 0;
	$sum['rented_area'] = 0;
	$sum['vacancy'] = 0;
	$sum['Quote'] = 0;
	$sum['Miete'] = 0;
	$sum['Potenzial'] = 0;
	$sum['Rents'] = 0;
	$sum['Vacants'] = 0;
	$total_pa = 0;

	$role = Auth::user()->role;
	foreach($analytics_array as $key => $masterliste)
	$total_pa += $masterliste['potential_pm'];

	?>


	@foreach($analytics_array as $key => $masterliste)
		<?php
		

		$sum['count'] += $masterliste['count'];
		$sum['flat_in_qm'] += $masterliste['flat_in_qm'];
		$sum['rented_area'] += $masterliste['rented_area'];
		$sum['vacancy'] += $masterliste['vacancy'];
		if( $sum['flat_in_qm'] != 0 )
			$sum['Quote'] = $sum['vacancy'] / $sum['flat_in_qm'] * 100;
		else
			$sum['Quote'] = 0;

		// $sum['Miete'] += $masterliste['rent_net_pm'];
		$sum['Potenzial']+= $masterliste['potential_pm'];

		$sum['Rents'] += $masterliste['rents'];
		$sum['Vacants'] += $masterliste['vacants'];

        $masterliste['asset_manager_name'] = short_name($masterliste['asset_manager_name']);

		?>
		<tr>
			<td>
				<a href="javascript:void(0)" class="get-asset-property" data-value="{{$masterliste['asset_manager']}}">
				{{$masterliste['asset_manager_name']}}</a>

				<!-- <button style="border:none; background-color:transparent;" name="asset_manager" value="{{$masterliste['asset_manager']}}"></button> -->
			</td>
			@if($is_asset_manager)	
 				<td>	
                <a href="{{route('properties.show',['property'=>$masterliste['id']])}}">	

 					{!!$masterliste['object']!!}	
				</a>	
				</td>	
			@else	
				<td class="number-right">{{number_format($masterliste['count'],0,",",".")}}</td>	
			@endif
			

			<td class="number-right">
			<?php
			$sum['Miete'] += $masterliste['rent_net_pm']*12;
			echo number_format($masterliste['rent_net_pm']*12,2,",",".");
			
			?>
			@if($is_asset_manager)
			<?php
			// if(isset($pa_array2[$masterliste['id']])){
				// $sum['Miete'] +=12*$pa_array2[$masterliste['id']];
				// echo number_format(12*$pa_array2[$masterliste['id']],2,",",".");
			// }
			?>
			@else
			<?php 
			// if(isset($pa_array[$key])){
			// 	$sum['Miete'] += 12*$pa_array[$key];
			// 	echo number_format(12*$pa_array[$key],2,",",".");
			// }
			// else{
				// }
			?>
			
			@endif
			</td>

			<td class="number-right">{{number_format($masterliste['flat_in_qm'],2,",",".")}}</td>
			<td class="number-right">{{number_format($masterliste['rented_area'],2,",",".")}}</td>
			<td class="number-right">{{number_format($masterliste['vacancy'],2,",",".")}}</td>
			<td class="number-right">{{$masterliste['flat_in_qm'] > 0 ? number_format($masterliste['vacancy']/$masterliste['flat_in_qm']*100,2,",",".") :0}}%</td>
			
			<!-- <td class="number-right"><br>{{number_format($masterliste['flat_in_qm'],2,",",".")}}</td>
			<td class="number-right"><br>{{number_format($masterliste['rented_area'],2,",",".")}}</td>
			<td class="number-right"><br>{{number_format($masterliste['vacancy'],2,",",".")}}</td>
			<td class="number-right"><br>{{$masterliste['flat_in_qm'] > 0 ? number_format($masterliste['vacancy']/$masterliste['flat_in_qm']*100,2,",",".") :0}}%</td> -->
			<td class="number-right">

			<a href="javascript:void(0)" class="get-rents" data-value="{{$masterliste['asset_manager']}}" data-type="r" data-property="0">							{{number_format($masterliste['rents'],0,",",".")}}
			</a>

			</td>
			<td class="number-right">
			<a href="javascript:void(0)" class="get-rents" data-value="{{$masterliste['asset_manager']}}" data-type="v" data-property="0">							{{number_format($masterliste['vacants'],0,",",".")}}
			</a>
			</td>
			
			<td class="number-right">{{number_format($masterliste['potential_pm'],2,",",".")}}</td>
			<td class="number-right">{{number_format(($masterliste['potential_pm']*100/$total_pa),2,",",".")}}%</td>
		</tr>
	@endforeach
	</tbody>

	<tr>
		<th ><br>Summe</th>
		<th class="number-right"><br>{{number_format($sum['count'],0,",",".")}}</th>
		<td class="number-right"><br>{{number_format($sum['Miete'],2,",",".")}}</td>
		
		<td class="number-right"><br>{{number_format($sum['flat_in_qm'],2,",",".")}}</td>
		<td class="number-right"><br>{{number_format($sum['rented_area'],2,",",".")}}</td>
		<td class="number-right"><br>{{number_format($sum['vacancy'],2,",",".")}}</td>
		<td class="number-right"><br>{{number_format($sum['Quote'],2,",",".")}}%</td>
		<td class="number-right"><br>
		<a href="javascript:void(0)" class="get-rents" data-value="0" data-type="r" data-property="0">{{number_format($sum['Rents'],0,",",".")}}</a></td>
		<td class="number-right"><br>
			<a href="javascript:void(0)" class="get-rents" data-value="0" data-type="v" data-property="0">{{number_format($sum['Vacants'],0,",",".")}}</a></td>
		<td class="number-right"><br>{{number_format($sum['Potenzial'],2,",",".")}}</td>
		<td></td>
	</tr>
	<tr>
		<th colspan="11"></th>
	</tr>
</table>
<input type="hidden" value='{{number_format(100-$sum["Quote"],2,",",".")}}' id="vermi_percent">
</form>
				