@extends('layouts.admin')
@section('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <!-- Styles -->
    <link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
@endsection
@section('content')
	<!-- .row -->
	<div class="row">
		<!-- <div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">{{__('dashboard.users')}}</h3>
				<ul class="list-inline two-part">
					<li>
						<div id="sparklinedash"></div>
					</li>
					<li class="text-right">
						{{--<i class="ti-arrow-up text-success"></i> --}}
						<span class="counter text-success">{{$user}}</span>
					</li>
				</ul>
			</div>
		</div> -->

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Vermietet(m2)</h3>
				<ul class="list-inline two-part">
					<li>
						<div class="sparklinedash"></div>
					</li>
					<li class="text-right">
						<span class="text-success">
							{{ number_format($summ_vermietet_m2,0,",",".") }}
						</span>
					</li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Vermietet (%)</h3>
				<ul class="list-inline two-part">
					<li>
						<div class="sparklinedash"></div>
					</li>
					<li class="text-right">
						<span class="text-success">
							{{ number_format($summ_vermietet_percent,2,",",".") }}
						</span>
					</li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Leerstand(m2)</h3>
				<ul class="list-inline two-part">
					<li>
						<div class="sparklinedash"></div>
					</li>
					<li class="text-right">
						<span class="text-success">
							{{ number_format($summ_leerstand_m2,0,",",".") }}
						</span>
					</li>
				</ul>
			</div>
		</div>

		<div class="col-lg-4 col-sm-6 col-xs-12">
			<div class="white-box analytics-info">
				<h3 class="box-title">Leerstand(%)</h3>
				<ul class="list-inline two-part">
					<li>
						<div class="sparklinedash"></div>
					</li>
					<li class="text-right">
						<span class="text-success">
							{{ number_format($summ_leerstand_percent,2,",",".") }}
						</span>
					</li>
				</ul>
			</div>
		</div>



		
	</div>
	<!--/.row -->


	<!-- .row -->
	{{--<div class="row">--}}
		{{--<div class="col-md-12 col-lg-12 col-xs-12">--}}
			{{--<div class="white-box">--}}
				{{--<h3 class="box-title">Bar Chart</h3>--}}
				{{--<div id="morris-bar-chart"></div>--}}
			{{--</div>--}}
		{{--</div>--}}
	{{--</div>--}}
	<!-- /.row -->






	<div class="row" id="assetmanagement">

		<div class="col-sm-12">
			<div class="white-box">
				<div class="table-responsive" style="margin-bottom: 30px;">
					<form action="{{route('assetmanagementfilter')}}" method="POST">
						{{ csrf_field() }}
						<table class="table table-striped" id="dashboard-table-2">
						<thead>
						<tr>
						<th><br>Asset Manager Bestand</th>
							@if($is_asset_manager)
								<th><br>Objekt</th>
							@else
								<th>Anzahl</th>
							@endif

							<th>Miete Netto p.a.</th>
							<th>Fläche in m<sup>2</sup></th>
							<th>Fläche vermietet m<sup>2</sup></th>
							<th>Leerstand m<sup>2</sup></th>
							<th>Quote</th>
							
							<th>Potenzial p.a.</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$sum['count'] = 0;
						$sum['flat_in_qm'] = 0;
						$sum['rented_area'] = 0;
						$sum['vacancy'] = 0;
						$sum['Quote'] = 0;
						$sum['Miete'] = 0;
						$sum['Potenzial'] = 0;
						?>


						@foreach($analytics_array as $key => $masterliste)
							<?php
							

							$sum['count'] += $masterliste['count'];
							$sum['flat_in_qm'] += $masterliste['flat_in_qm'];
							$sum['rented_area'] += $masterliste['rented_area'];
							$sum['vacancy'] += $masterliste['vacancy'];
							if( $sum['flat_in_qm'] != 0 )
								$sum['Quote'] = $sum['vacancy'] / $sum['flat_in_qm'] * 100;
							else
								$sum['Quote'] = 0;

							// $sum['Miete'] += $masterliste['rent_net_pm'];
							$sum['Potenzial']+= $masterliste['potential_pm'];
							?>
							<tr>
								<td><button style="border:none; background-color:transparent;" name="asset_manager" value="{{$masterliste['asset_manager']}}">{{$masterliste['asset_manager_name']}}</button></td>
								@if($is_asset_manager)
									<td>
                                    <a href="{{route('properties.show',['property'=>$masterliste['id']])}}">

										{!!$masterliste['object']!!}
									</a>
									</td>
								@else
									<td>{{number_format($masterliste['count'],0,",",".")}}</td>
								@endif

								<td class="number-right"><br>{{number_format($masterliste['flat_in_qm'],2,",",".")}}</td>
								<td class="number-right"><br>{{number_format($masterliste['rented_area'],2,",",".")}}</td>
								<td class="number-right"><br>{{number_format($masterliste['vacancy'],2,",",".")}}</td>
								<td class="number-right"><br>{{$masterliste['flat_in_qm'] > 0 ? number_format($masterliste['vacancy']/$masterliste['flat_in_qm']*100,2,",",".") :0}}%</td>
								<td class="number-right"><br>
								@if($is_asset_manager)
								<?php
								if(isset($pa_array2[$masterliste['id']])){
									$sum['Miete'] +=12*$pa_array2[$masterliste['id']];
									echo number_format(12*$pa_array2[$masterliste['id']],2,",",".");
								}
								?>
								@else
								<?php 
								if(isset($pa_array[$key])){
									$sum['Miete'] += 12*$pa_array[$key];
									echo number_format(12*$pa_array[$key],2,",",".");
								}
								else{
									echo number_format($masterliste['rent_net_pm'],2,",",".");
								}
								?>
								
								@endif
								</td>
								<!-- <td class="number-right"><br>{{number_format($masterliste['flat_in_qm'],2,",",".")}}</td>
								<td class="number-right"><br>{{number_format($masterliste['rented_area'],2,",",".")}}</td>
								<td class="number-right"><br>{{number_format($masterliste['vacancy'],2,",",".")}}</td>
								<td class="number-right"><br>{{$masterliste['flat_in_qm'] > 0 ? number_format($masterliste['vacancy']/$masterliste['flat_in_qm']*100,2,",",".") :0}}%</td> -->
								
								<td class="number-right"><br>{{number_format($masterliste['potential_pm'],2,",",".")}}</td>
							</tr>
						@endforeach
						</tbody>

						<tr>
							<th>Summe</th>
							<th>{{number_format($sum['count'],0,",",".")}}</th>
							<td class="number-right"><br>{{number_format($sum['flat_in_qm'],2,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['rented_area'],2,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['vacancy'],2,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['Quote'],2,",",".")}}%</td>
							<td class="number-right"><br>{{number_format($sum['Miete'],2,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['Potenzial'],2,",",".")}}</td>
						</tr>
						<tr>
							<th colspan="8"></th>
						</tr>
					</table>
					</form>
				</div>
			
			</div>
		</div>


		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Differenz Miete (Soll/Ist) (€)</h3>
				<p class="text-muted m-b-30">&nbsp;</p>
				<div class="table-responsive">
					<table id="tenant-table1" class="table table-striped">
						<thead>
						<tr>
							<th>#</th>
							<th>Objekt</th>
							<th>Differenz Miete (Soll/Ist) in €</th>
							<th>Differenz Miete (Soll/Ist) p.a. in €</th>
						</tr>
						</thead>
						<tbody>
						@foreach($masterlistes as $masterliste)
							<tr>
								<td>{{$masterliste->id}}</td>
								<td>
									<!--  -->
									@if(isset($diffff[$masterliste->id]['property_id']))
									<a href="{{route('properties.show',['property'=>$diffff[$masterliste->id]['property_id']])}}">
										{{ $masterliste->object }}
									</a>
									@else
									<a href="#" class="inline-edit" data-type="text" data-pk="object" data-url="{{route('masterliste.update', $masterliste->id) }}">{{ $masterliste->object }}</a>
									@endif
								</td>
								<td>
									@if(isset($diffff[$masterliste->id]['amount']))
									{{  number_format($diffff[$masterliste->id]['amount'],2,",",".") }}
									@else
									0
									@endif

								</td>
								<td>@if(isset($diffff[$masterliste->id]['amount']))
									{{  number_format($diffff[$masterliste->id]['amount']*12,2,",",".") }}
									@else
									0
									@endif</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>



		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Auslaufende Mietverträge</h3>
				<p class="text-muted m-b-30">&nbsp;</p>
				<div class="table-responsive">
					<table id="tenant-table2" class="table table-striped">
						<thead>
						<tr>
							<th>#</th>
							<th>Asset Manager</th>
							<th>Objekt</th>
							<th>Vermietet</th>
							<th>Mietende</th>
							<th>Mietfläche (m2)</th>
							<th>IST-Nettokaltmiete (€)</th>
							<th>Typ</th>
						</tr>
						</thead>
						<tbody>
						@foreach($tenancy_items_2 as $tenancy_item)
							<tr>
								<td>{{$tenancy_item->id}}</td>
								<td>{{$tenancy_item->creator_name}}</td>
								<td>{{$tenancy_item->object_name}}</td>
								<td>{{$tenancy_item->name}}</td>
								<td>{{$tenancy_item->rent_end}}</td>
								<td>{{$tenancy_item->rental_space ? number_format($tenancy_item->rental_space,2,",",".") : 0}}</td>
								<td>{{$tenancy_item->actual_net_rent ? number_format($tenancy_item->actual_net_rent,2,",",".") : 0}}</td>
								<td>{{$tenancy_item->type}}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>



		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Leerstände (€)</h3>
				<p class="text-muted m-b-30">&nbsp;</p>
				<div class="table-responsive">
					<table id="tenant-table3" class="table table-striped">
						<thead>
						<tr>
							<th>#</th>
							<th>Asset Manager</th>
							<th>Objekt</th>
							<th>Leerstand</th>
							<th>Fläche (m2)</th>
							<th>Leerstand (€)</th>
							<th>Typ</th>
						</tr>
						</thead>
						<tbody>
						@foreach($tenancy_items as $tenancy_item)
							<tr>
								<td>{{$tenancy_item->id}}</td>
								<td>{{$tenancy_item->creator_name}}</td>
								<td>{{$tenancy_item->object_name}}</td>
								<td>{{$tenancy_item->name}}</td>
								<td>{{$tenancy_item->vacancy_in_qm ? number_format($tenancy_item->vacancy_in_qm,2,",",".") : 0}}</td>
								<td>{{$tenancy_item->vacancy_in_eur ? number_format($tenancy_item->vacancy_in_eur,2,",",".") : 0}}</td>
								<td>{{$tenancy_item->type}}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>


	


		
	</div>
	
@endsection

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
	<script src="{{asset('js/morris.min.js')}}"></script>
	{{--<script>--}}
		{{--$(document).ready(function () {--}}
			{{--// Morris bar chart--}}
			{{--Morris.Bar({--}}
				{{--element: 'morris-bar-chart',--}}
				{{--data: [{--}}
					{{--y: '2006',--}}
					{{--a: 100,--}}
					{{--b: 90,--}}
					{{--c: 60--}}
				{{--}, {--}}
					{{--y: '2007',--}}
					{{--a: 75,--}}
					{{--b: 65,--}}
					{{--c: 40--}}
				{{--}, {--}}
					{{--y: '2008',--}}
					{{--a: 50,--}}
					{{--b: 40,--}}
					{{--c: 30--}}
				{{--}, {--}}
					{{--y: '2009',--}}
					{{--a: 75,--}}
					{{--b: 65,--}}
					{{--c: 40--}}
				{{--}, {--}}
					{{--y: '2010',--}}
					{{--a: 50,--}}
					{{--b: 40,--}}
					{{--c: 30--}}
				{{--}, {--}}
					{{--y: '2011',--}}
					{{--a: 75,--}}
					{{--b: 65,--}}
					{{--c: 40--}}
				{{--}, {--}}
					{{--y: '2012',--}}
					{{--a: 100,--}}
					{{--b: 90,--}}
					{{--c: 40--}}
				{{--}],--}}
				{{--xkey: 'y',--}}
				{{--ykeys: ['a', 'b', 'c'],--}}
				{{--labels: ['A', 'B', 'C'],--}}
				{{--barColors:['#b8edf0', '#b4c1d7', '#fcc9ba'],--}}
				{{--hideHover: 'auto',--}}
				{{--gridLineColor: '#eef0f2',--}}
				{{--resize: true--}}
			{{--});--}}
		{{--})--}}
	{{--</script>--}}
    <script>
	jQuery.extend( jQuery.fn.dataTableExt.oSort, {
		"numeric-comma-pre": function ( a ) {
            var x = (a === "-") ? 0 : a.split('.').join("");
            	x = x.split('€').join("")
                x = x.split('%').join("")
                x = x.replace(',',".")
                x = x.split('<br>').join("")
             	console.log(x);
                return parseFloat( x );
        },

        "numeric-comma-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },

        "numeric-comma-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        },
		"formatted-num-pre": function ( a ) {
			a = (a === "-" || a === "") ? 0 : a.replace(/\./g, '');
			return parseFloat( a );
		},
	 
		"formatted-num-asc": function ( a, b ) {

			return a - b;
		},
	 
		"formatted-num-desc": function ( a, b ) {
			return b - a;
		},
		
		"formatted-inline-pre": function ( a ) {
			a = a.replace(/<(?:.|\n)*?>/gm, '');
			a = (a === "-" || a === "") ? 0 : a.replace(/\./g, '');
			console.log(a);
			return parseFloat( a );
		},
	 
		"formatted-inline-asc": function ( a, b ) {
			return a - b;
		},
	 
		"formatted-inline-desc": function ( a, b ) {
			return b - a;
		},
		"non-empty-string-asc": function (str1, str2) {
            if(str1 == "")
                return 1;
            if(str2 == "")
                return -1;
            return ((str1 < str2) ? -1 : ((str1 > str2) ? 1 : 0));
        },
     
        "non-empty-string-desc": function (str1, str2) {
            if(str1 == "")
                return 1;
            if(str2 == "")
                return -1;
            return ((str1 < str2) ? 1 : ((str1 > str2) ? -1 : 0));
        }
		
	} );
	
	
        $(document).ready(function () {
		
			$('#tenant-table1').DataTable({
				 "columns": [
					null,
					null,
					{ "type": "formatted-num" },
					{ "type": "formatted-num" }
				]
			});
			$('#tenant-table2').DataTable({
				"order": [[ 4, "asc" ]],
                
				 "columns": [
					null,
					null,
					null,
					{ "type": "formatted-num" },
					{ "type": "non-empty-string" },
					{ "type": "formatted-num" },
					{ "type": "formatted-num" },
					null

				]
			});
			$('#tenant-table3').DataTable({
				 "columns": [
					null,
					null,
					null,
					{ "type": "formatted-num" },
					{ "type": "formatted-num" },
					{ "type": "formatted-num" },
					null
				]
			});
			
			
			
   //          $('#dashboard-table-1').DataTable({
			// 	"columns": [
			// 		null,
			// 		null,
			// 		{ "type": "formatted-num" },
			// 		{ "type": "formatted-num" },
			// 		{ "type": "formatted-num" },
			// 		{ "type": "formatted-num" },
			// 		{ "type": "formatted-num" },
			// 		{ "type": "formatted-num" },
			// 	]
			// });
			$('#dashboard-table-2').DataTable({
				"columns": [
					null,
					null,
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
					{ "type": "numeric-comma" },
				]
			});
            $('#assetmanagement').find('a.inline-edit').editable({
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        location.reload();
                    }
                }
            });

        });

	</script>
@endsection
