						<table class="table table-striped" id="list-rent">
						<thead>
						<tr>
							<th><br>Objekt</th>
							<th><br>Leerstand</th>
							<th class="text-right">Mietfläche</th>
							<th class="text-right">Potenzial p.a.</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$sum1 = 0;
						$sum2 = 0;
						?>


						@foreach($tenant_data as $key => $row)
							@foreach($row['tenant']['tenancy_schedules'] as $key => $tenancy_schedule)

							@foreach($tenancy_schedule->items as $item)

							<?php
							if(($item->rent_begin != '' && $item->rent_begin > date('Y-m-d')) ||
                   				($item->rent_end != '' && $item->rent_end < date('Y-m-d')))
                   				continue;

							
							if (($item->type == config('tenancy_schedule.item_type.live_vacancy') || $item->type == config('tenancy_schedule.item_type.business_vacancy'))):

								?>
							<tr>
								<td>
									<a href="{{route('properties.show',['property'=>$row['id']])}}">
									{{$row['name']}}
									</a>
								</td>
								<td>
									<a href="{{route('properties.show',['property'=>$row['id']])}}?tab=comment_tab">
									{{$item->name}}</a>
								</td>
								<td class="text-right">
									{{ number_format($item->vacancy_in_qm, 2,",",".") }}
								</td>
								<td class="text-right">
									@if($item->vacancy_in_eur)
                                    {{ number_format($item->vacancy_in_eur*12, 2,",",".") }}
                                    @else
                                    {{ number_format($item->actual_net_rent2*12, 2,",",".") }}
                                    @endif

                                     &nbsp;€
								</td>
							</tr>
							<?php endif; ?>
							@endforeach		
							@endforeach
						@endforeach
						</tbody>

						<tr>
							<th colspan="10"></th>
						</tr>
					</table>
					