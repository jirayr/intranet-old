						<?php 
							if(isset($id) && $id == -1){
								$table_id = "list-properties"+$id;
							}else{
								$table_id = "list-properties";
							}
						?>
						<table class="table table-striped" id="{{ $table_id }}">
						<thead>
						<tr>
						<th>
							<br>
							@if(isset($id) && $id==-1)
							Asset Manager
							@else
							Asset Manager Bestand
							@endif
							</th>
							@if(isset($id) && $id==-1)
							<th><br>TR Manager</th>
							@endif
							<th><br>Objekt</th>
							<th>Miete Netto p.a.</th>
							<th>Fläche in m<sup>2</sup></th>
							<th>Vermietet m<sup>2</sup></th>
							<th>Leerstand m<sup>2</sup></th>
							<th>Quote</th>
							<th>Mieter</th>
							<th>Anzahl Leerstand</th>
							<th>Potenzial p.a.</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$sum['count'] = 0;
						$sum['flat_in_qm'] = 0;
						$sum['rented_area'] = 0;
						$sum['vacancy'] = 0;
						$sum['Quote'] = 0;
						$sum['Miete'] = 0;
						$sum['Potenzial'] = 0;
						$sum['Rents'] = 0;
						$sum['Vacants'] = 0;
						?>


						@foreach($analytics_array as $key => $masterliste)
							<?php
							

							$sum['count'] += 1;
							$sum['flat_in_qm'] += $masterliste['flat_in_qm'];
							$sum['rented_area'] += $masterliste['rented_area'];
							$sum['vacancy'] += $masterliste['vacancy'];
							if( $sum['flat_in_qm'] != 0 )
								$sum['Quote'] = $sum['vacancy'] / $sum['flat_in_qm'] * 100;
							else
								$sum['Quote'] = 0;

							// $sum['Miete'] += $masterliste['rent_net_pm'];
							$sum['Potenzial']+= $masterliste['potential_pm'];
							$sum['Rents'] += $masterliste['rents'];
							$sum['Vacants'] += $masterliste['vacants'];

							$subject = 'BESTANDSOBJEKTE: '.$masterliste['object'];
							$content = '';
							
							?>
							<tr>
								<td>
									<a href="javascript:void(0);" class="asset_manager" data-property-id="{{ $masterliste['id'] }}" data-subject="{{ $subject }}" data-content='{{ $content }}' data-title="BESTANDSOBJEKTE">{{ $masterliste['asset_manager_name'] }}</a>
								</td>
								@if(isset($id) && $id==-1)
								<td><br>

								@if($masterliste['transaction_m_id'])

								@php
                                $user =  DB::table('users')->where('id', $masterliste['transaction_m_id'])->first();

                                    if($user){
                                        echo $user->name;
                                    }
                                @endphp

                                @endif
                                </td>
								@endif
									<td>
                                    <a href="{{route('properties.show',['property'=>$masterliste['id']])}}">

										{!!$masterliste['object']!!}
									</a>
									</td>
									<td class="number-right"><br>
								<?php
								// if(isset($pa_array2[$masterliste['id']])){
									// $sum['Miete'] +=12*$pa_array2[$masterliste['id']];
									echo number_format(12*$masterliste['rent_net_pm'],2,",",".");
								// }

								$sum['Miete'] +=12*$masterliste['rent_net_pm'];
								?>
								</td>
								
								<td class="number-right"><br>{{number_format($masterliste['flat_in_qm'],2,",",".")}}</td>
								<td class="number-right"><br>{{number_format($masterliste['rented_area'],2,",",".")}}</td>
								<td class="number-right"><br>{{number_format($masterliste['vacancy'],2,",",".")}}</td>
								<td class="number-right"><br>{{$masterliste['flat_in_qm'] > 0 ? number_format($masterliste['vacancy']/$masterliste['flat_in_qm']*100,2,",",".") :0}}%</td>
								
								<!-- <td class="number-right"><br>{{number_format($masterliste['flat_in_qm'],2,",",".")}}</td>
								<td class="number-right"><br>{{number_format($masterliste['rented_area'],2,",",".")}}</td>
								<td class="number-right"><br>{{number_format($masterliste['vacancy'],2,",",".")}}</td>
								<td class="number-right"><br>{{$masterliste['flat_in_qm'] > 0 ? number_format($masterliste['vacancy']/$masterliste['flat_in_qm']*100,2,",",".") :0}}%</td> -->
								<td class="number-right"><br>
									<a href="javascript:void(0)" class="get-rents" data-value="0" data-type="@if(isset($id) && $id==-1){{'r1'}}@else{{'r'}}@endif" data-property="{{$masterliste['id']}}">							{{number_format($masterliste['rents'],0,",",".")}}</a></td>
								<td class="number-right"><br>
									<a href="javascript:void(0)" class="get-rents" data-value="0" data-type="@if(isset($id) && $id==-1){{'v1'}}@else{{'v'}}@endif" data-property="{{$masterliste['id']}}">							{{number_format($masterliste['vacants'],0,",",".")}}</a></td>
								
								
								<td class="number-right"><br>{{number_format($masterliste['potential_pm'],2,",",".")}}</td>
							</tr>
						@endforeach
						</tbody>
						@if(isset($id) && $id==-1)
						@else
						<tr>
							<th><br>Summe</th>
							<th><br>{{number_format($sum['count'],0,",",".")}}</th>
							<td class="number-right"><br>{{number_format($sum['Miete'],2,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['flat_in_qm'],2,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['rented_area'],2,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['vacancy'],2,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['Quote'],2,",",".")}}%</td>
							
							<td class="number-right"><br>{{number_format($sum['Rents'],0,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['Vacants'],0,",",".")}}</td>
							<td class="number-right"><br>{{number_format($sum['Potenzial'],2,",",".")}}</td>
						</tr>
						@endif
						<tr>
							<th colspan="11"></th>
						</tr>
					</table>
					