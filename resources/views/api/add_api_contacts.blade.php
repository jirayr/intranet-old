@extends('layouts.admin')

@section('content')

<style>
	.white-box{
		padding: 0 10px;
		margin-bottom: 0;
	}
</style>

<section class="contact_form">

	<div class="container-fluid">
		<div class="panel">
			<div class="panel-heading">Contact Form</div>
<div class="row"> 
<a href="{{ url('/api_contacts_list') }}" class="btn btn-lg btn-success" style="float: right; margin-right: 25px;
margin-top: -40px; ">Contact List</a>
</div>
			



@if(Session::has('massage')) 
<p class="alert alert-success">{{ Session::get('massage') }}</p>
@endif
			
				<form action="{{ url('/post_api_contacts') }}" method="post">
					 <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Salutation</label>
								<select name="salutation" class="form-control" required>
									<option value="">---Select---</option>
									<option value="MALE">Male</option>
									<option value="FEMALE">Female</option>
								</select>
							</div>
						</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>First name</label>
								<input type="text" name="firstname" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Last name</label>
								<input type="text" name="lastname" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Fax Number Country Code</label>
								<input type="text" name="faxNumberCountryCode" class="form-control" placeholder="type here" pattern="\+[1-9]\d{0,3}">
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Fax Number Area Code</label>
								<input type="number" name="faxNumberAreaCode" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Fax Number Subscriber</label>
								<input type="number" name="faxNumberSubscriber" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Phone Number Country Code</label>
								<input type="text" name="phoneNumberCountryCode" class="form-control" placeholder="type here" pattern="\+[1-9]\d{0,3}">
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Phone Number Area Code</label>
								<input type="number" name="phoneNumberAreaCode" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Phone Number Subscriber</label>
								<input type="number" name="phoneNumberSubscriber" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Cell Phone Number Country Code</label>
								<input type="text" name="cellPhoneNumberCountryCode" class="form-control" placeholder="type here" pattern="\+[1-9]\d{0,3}">
							</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Cell Phone Number AreaCode</label>
								<input type="number" name="cellPhoneNumberAreaCode" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Cell Phone Number Subscriber</label>
								<input type="number" name="cellPhoneNumberSubscriber" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-8">
							<div class="white-box">
							<div class="form-group">
								<label>Street</label>
								<input type="text" name="street" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>House Number</label>
								<input type="text" name="houseNumber" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Post Code</label>
								<input type="number" name="postcode" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>City</label>
								<input type="text" name="city" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Country Code</label>
								<!-- <input type="text" name="countryCode" class="form-control" placeholder="type here" required> -->

								<select name="countryCode" class="form-control" required>
									<option value="ABW">ABW</option>
									<option value="AFG">AFG</option>
									<option value="AGO">AGO</option>
									<option value="AIA">AIA</option>
									<option value="ALB">ALB</option>
									<option value="AND">AND</option>
									<option value="ANT">ANT</option>
									<option value="ARE">ARE</option>
									<option value="ARG">ARG</option>
									<option value="ARM">ARM</option>
									<option value="ASM">ASM</option>
									<option value="ATA">ATA</option>
									<option value="ATG">ATG</option>
									<option value="AUS">AUS</option>
									<option value="AUT">AUT</option>
									<option value="AZE">AZE</option>
									<option value="BDI">BDI</option>
									<option value="BEL">BEL</option>
									<option value="BEN">BEN</option>
									<option value="BFA">BFA</option>
									<option value="BGD">BGD</option>
									<option value="BGR">BGR</option>
									<option value="BHR">BHR</option>
									<option value="BHS">BHS</option>
									<option value="BIH">BIH</option>
									<option value="BLR">BLR</option>
									<option value="BLZ">BLZ</option>
									<option value="BMU">BMU</option>
									<option value="BOL">BOL</option>
									<option value="BRA">BRA</option>
									<option value="BRB">BRB</option>
									<option value="BRN">BRN</option>
									<option value="BTN">BTN</option>
									<option value="BWA">BWA</option>
									<option value="CAF">CAF</option>
									<option value="CAN">CAN</option>
									<option value="CCK">CCK</option>
									<option value="CHE">CHE</option>
									<option value="CHL">CHL</option>
									<option value="CHN">CHN</option>
									<option value="CIV">CIV</option>
									<option value="CMR">CMR</option>
									<option value="COD">COD</option>
									<option value="COG">COG</option>
									<option value="COK">COK</option>
									<option value="COL">COL</option>
									<option value="COM">COM</option>
									<option value="CPV">CPV</option>
									<option value="CRI">CRI</option>
									<option value="CUB">CUB</option>
									<option value="CXR">CXR</option>
									<option value="CYM">CYM</option>
									<option value="CYP">CYP</option>
									<option value="CZE">CZE</option>
									<option value="DEU">DEU</option>
									<option value="DJI">DJI</option>
									<option value="DMA">DMA</option>
									<option value="DNK">DNK</option>
									<option value="DOM">DOM</option>
									<option value="DZA">DZA</option>
									<option value="ECU">ECU</option>
									<option value="EGY">EGY</option>
									<option value="ERI">ERI</option>
									<option value="ESH">ESH</option>
									<option value="ESP">ESP</option>
									<option value="EST">EST</option>
									<option value="ETH">ETH</option>
									<option value="FIN">FIN</option>
									<option value="FJI">FJI</option>
									<option value="FLK">FLK</option>
									<option value="FRA">FRA</option>
									<option value="FRO">FRO</option>
									<option value="FSM">FSM</option>
									<option value="GAB">GAB</option>
									<option value="GBR">GBR</option>
									<option value="GEO">GEO</option>
									<option value="GHA">GHA</option>
									<option value="GIB">GIB</option>
									<option value="GIN">GIN</option>
									<option value="GLP">GLP</option>
									<option value="GMB">GMB</option>
									<option value="GNB">GNB</option>
									<option value="GNQ">GNQ</option>
									<option value="GRC">GRC</option>
									<option value="GRD">GRD</option>
									<option value="GRL">GRL</option>
									<option value="GTM">GTM</option>
									<option value="GUF">GUF</option>
									<option value="GUM">GUM</option>
									<option value="GUY">GUY</option>
									<option value="HKG">HKG</option>
									<option value="HMD">HMD</option>
									<option value="HND">HND</option>
									<option value="HRV">HRV</option>
									<option value="HTI">HTI</option>
									<option value="HUN">HUN</option>
									<option value="IDN">IDN</option>
									<option value="IMN">IMN</option>
									<option value="IND">IND</option>
									<option value="IRL">IRL</option>
									<option value="IRN">IRN</option>
									<option value="IRQ">IRQ</option>
									<option value="ISL">ISL</option>
									<option value="ISR">ISR</option>
									<option value="ITA">ITA</option>
									<option value="JAM">JAM</option>
									<option value="JOR">JOR</option>
									<option value="JPN">JPN</option>
									<option value="KAZ">KAZ</option>
									<option value="KEN">KEN</option>
									<option value="KGZ">KGZ</option>
									<option value="KHM">KHM</option>
									<option value="KIR">KIR</option>
									<option value="KNA">KNA</option>
									<option value="KOR">KOR</option>
									<option value="KWT">KWT</option>
									<option value="LAO">LAO</option>
									<option value="LBN">LBN</option>
									<option value="LBR">LBR</option>
									<option value="LBY">LBY</option>
									<option value="LCA">LCA</option>
									<option value="LIE">LIE</option>
									<option value="LKA">LKA</option>
									<option value="LSO">LSO</option>
									<option value="LTU">LTU</option>
									<option value="LUX">LUX</option>
									<option value="LVA">LVA</option>
									<option value="MAC">MAC</option>
									<option value="MAR">MAR</option>
									<option value="MCO">MCO</option>
									<option value="MDA">MDA</option>
									<option value="MDG">MDG</option>
									<option value="MDV">MDV</option>
									<option value="MEX">MEX</option>
									<option value="MHL">MHL</option>
									<option value="MKD">MKD</option>
									<option value="MLI">MLI</option>
									<option value="MLT">MLT</option>
									<option value="MMR">MMR</option>
									<option value="MNE">MNE</option>
									<option value="MNG">MNG</option>
									<option value="MNP">MNP</option>
									<option value="MOZ">MOZ</option>
									<option value="MRT">MRT</option>
									<option value="MSR">MSR</option>
									<option value="MTQ">MTQ</option>
									<option value="MUS">MUS</option>
									<option value="MWI">MWI</option>
									<option value="MYS">MYS</option>
									<option value="MYT">MYT</option>
									<option value="NAM">NAM</option>
									<option value="NCL">NCL</option>
									<option value="NER">NER</option>
									<option value="NFK">NFK</option>
									<option value="NGA">NGA</option>
									<option value="NIC">NIC</option>
									<option value="NIU">NIU</option>
									<option value="NLD">NLD</option>
									<option value="NOR">NOR</option>
									<option value="NPL">NPL</option>
									<option value="NRU">NRU</option>
									<option value="NZL">NZL</option>
									<option value="OMN">OMN</option>
									<option value="PAK">PAK</option>
									<option value="PAN">PAN</option>
									<option value="PCN">PCN</option>
									<option value="PER">PER</option>
									<option value="PHL">PHL</option>
									<option value="PLW">PLW</option>
									<option value="PNG">PNG</option>
									<option value="POL">POL</option>
									<option value="PRI">PRI</option>
									<option value="PRK">PRK</option>
									<option value="PRT">PRT</option>
									<option value="PRY">PRY</option>
									<option value="PYF">PYF</option>
									<option value="QAT">QAT</option>
									<option value="REU">REU</option>
									<option value="ROU">ROU</option>
									<option value="RUS">RUS</option>
									<option value="RWA">RWA</option>
									<option value="SAU">SAU</option>
									<option value="SDN">SDN</option>
									<option value="SEN">SEN</option>
									<option value="SGP">SGP</option>
									<option value="SHN">SHN</option>
									<option value="SJM">SJM</option>
									<option value="SLB">SLB</option>
									<option value="SLE">SLE</option>
									<option value="SLV">SLV</option>
									<option value="SMR">SMR</option>
									<option value="SOM">SOM</option>
									<option value="SPM">SPM</option>
									<option value="SRB">SRB</option>
									<option value="STP">STP</option>
									<option value="SUR">SUR</option>
									<option value="SVK">SVK</option>
									<option value="SVN">SVN</option>
									<option value="SWE">SWE</option>
									<option value="SWZ">SWZ</option>
									<option value="SYC">SYC</option>
									<option value="SYR">SYR</option>
									<option value="TCA">TCA</option>
									<option value="TCD">TCD</option>
									<option value="TGO">TGO</option>
									<option value="THA">THA</option>
									<option value="TJK">TJK</option>
									<option value="TKL">TKL</option>
									<option value="TKM">TKM</option>
									<option value="TMP">TMP</option>
									<option value="TON">TON</option>
									<option value="TTO">TTO</option>
									<option value="TUN">TUN</option>
									<option value="TUR">TUR</option>
									<option value="TUV">TUV</option>
									<option value="TWN">TWN</option>
									<option value="TZA">TZA</option>
									<option value="UGA">UGA</option>
									<option value="UKR">UKR</option>
									<option value="URY">URY</option>
									<option value="USA">USA</option>
									<option value="UZB">UZB</option>
									<option value="VAT">VAT</option>
									<option value="VCT">VCT</option>
									<option value="VEN">VEN</option>
									<option value="VGB">VGB</option>
									<option value="VIR">VIR</option>
									<option value="VNM">VNM</option>
									<option value="VUT">VUT</option>
									<option value="WLF">WLF</option>
									<option value="WSM">WSM</option>
									<option value="YEM">YEM</option>
									<option value="ZAF">ZAF</option>
									<option value="ZMB">ZMB</option>
									<option value="ZWE">ZWE</option>
									<option value="XKO">XKO</option>
								</select>


							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Title</label>
								<input type="text" name="title" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Addition Name</label>
								<input type="text" name="additionName" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="white-box">
							<div class="form-group">
								<label>Home Page Url</label>
								<input type="url" name="homepageUrl" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="white-box">
							<div class="form-group">
								<label>Position</label>
								<input type="text" name="position" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Default Contact</label>
								<select name="defaultContact" class="form-control" required>
									<option value="">---Select---</option>
									<option value="true">Yes</option>
									<option value="false">No</option>
								</select>
							</div>
						</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Local Partner Contact</label>
								<select name="localPartnerContact" class="form-control" required>
									<option value="">---Select---</option>
									<option value="true">Yes</option>
									<option value="false">No</option>
								</select>
							</div>
						</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Business Card Contact</label>
								<select name="businessCardContact" class="form-control" required>
									<option value="">---Select---</option>
									<option value="true">Yes</option>
									<option value="false">No</option>
								</select>
							</div>
						</div>
						</div>


						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Real Estate Reference Count</label>
								<input type="text" name="realEstateReferenceCount" class="form-control" placeholder="type here" required>
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>External Id</label>
								<input type="text" name="externalId" class="form-control" placeholder="type here" readonly value="a-00{{ rand(0,10000) }}">
							</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="white-box">
							<div class="form-group">
								<label>Show On Profile Page</label>
								<select name="showOnProfilePage" class="form-control" required>
									<option value="">---Select---</option>
									<option value="true">Yes</option>
									<option value="false">No</option>
								</select>
							</div>
						</div>
						</div>

					<div class="col-sm-3" style="margin-bottom: 25px;">
						<div class="white-box">
						<input type="submit" class="btn btn-lg btn-success" name="" value="Submit">
					</div>
					</div>


					</div>

					
					
				</form>
			
		</div>
	</div>
	
</section>


@endsection
