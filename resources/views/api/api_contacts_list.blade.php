
  @extends('layouts.admin')

@section('content')

<style>
	.white-box{
		padding: 0 10px;
		margin-bottom: 0;
	}
</style>

<section class="contact_form">

	<div class="container-fluid">
		<div class="panel">
				


<div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">Contact Form List</div>

  @if(Session::has('massage')) 
<p class="alert alert-success">{{ Session::get('massage') }}</p>
@endif	
 
                <div class="table-responsive">
                    <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th style="width: 70px;" class="text-center">#</th>
                            <th>Email</th>
                            <th>Salutation</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Fax Number Country Code</th>
                            <th>Fax Number Area Code</th>
                            <th>Fax Number Subscriber</th>
                            <th>Phone Number Country Code</th>
                            <th>Phone Number Area Code</th>
                            <th>Phone Number Subscriber</th>
                            <th>Cell Phone Number Country Code</th>
                            <th>Cell Phone Number Area Code</th>
                            <th>Cell Phone Number Subscriber</th>
                            <th>Street</th>
                            <th>House Number</th>
                            <th>Post Code</th>
                            <th>City</th>
                            <th>Country Code</th>
                            <th>Title</th>
                            <th>Addition Name</th>
                            <th>Homepage Url</th>
                            <th>Position</th>
                            <th>Default Contact</th>
                            <th>Local Partner Contact</th>
                            <th>Business Card Contact</th>
                            <th>Real Estate Reference Count</th>
                            <th>External Id</th>
                            <th>Show On Profile Page</th>
                            <th>Action</th>
                        </tr>
                        </thead>
             {{-- {{ dd($contact_list) }} --}}
            {{--  @php
             	echo "<pre>";
                print_r($contact_list);
             	echo "</pre>";
             	die();
             @endphp
 --}}
                        <tbody>
                        	@foreach($contact_list['realtorContactDetails'] as $key => $list)

                  {{--       	@php
             	echo "<pre>";
                var_dump($list['firstname']);
             	echo "</pre>";
             	continue;
                @endphp --}}

          <tr>
            <td class="text-center"><br>{{ $key+1 }}</td>
            <td><br>{{ (isset($list['email'])) ? $list['email'] : '---'  }}</td>
            <td><br>{{ (isset($list['salutation'])) ? $list['salutation'] : '---' }}</td>
            <td><br>{{ (isset($list['firstname'])) ? $list['firstname'] : '---' }}</td>
            <td><br>{{ (isset($list['lastname'])) ? $list['lastname'] : '---' }}</td>
            <td><br>{{ (isset($list['faxNumberCountryCode'])) ? $list['faxNumberCountryCode'] : '---' }}</td>
            <td><br>{{ (isset($list['faxNumberAreaCode'])) ? $list['faxNumberAreaCode'] : '---' }}</td>
            <td><br>{{ (isset($list['faxNumberSubscriber'])) ? $list['faxNumberSubscriber'] : '---' }}</td>
            <td><br>{{ (isset($list['phoneNumberCountryCode'])) ? $list['phoneNumberCountryCode'] : '---' }}</td>
            <td><br>{{ (isset($list['phoneNumberAreaCode'])) ? $list['phoneNumberAreaCode'] : '---' }}</td>
           {{--  <td><br>{{ (isset($list['phoneNumberAreaCode'])) ? $list['phoneNumberAreaCode'] : '---' }}</td> --}}
            <td><br>{{ (isset($list['phoneNumberSubscriber'])) ? $list['phoneNumberSubscriber'] : '---' }}</td>
            <td><br>{{ (isset($list['cellPhoneNumberCountryCode'])) ? $list['cellPhoneNumberCountryCode'] : '---' }}</td>
            <td><br>{{ (isset($list['cellPhoneNumberAreaCode'])) ? $list['cellPhoneNumberAreaCode'] : '---' }}</td>
            <td><br>{{ (isset($list['cellPhoneNumberSubscriber'])) ? $list['cellPhoneNumberSubscriber'] : '---' }}</td>
            <td><br>{{ (isset($list['address']['street'])) ? $list['address']['street'] : '---' }}</td>
            <td><br>{{ (isset($list['address']['houseNumber'])) ? $list['address']['houseNumber'] : '---' }}</td>
            <td><br>{{ (isset($list['address']['postcode'])) ? $list['address']['postcode'] : '---' }}</td>
            <td><br>{{ (isset($list['address']['city'])) ? $list['address']['city'] : '---' }}</td>
            <td><br>{{ (isset($list['countryCode'])) ? $list['countryCode'] : '---' }}</td>
            <td><br>{{ (isset($list['title'])) ? $list['title'] : '---' }}</td>
            <td><br>{{ (isset($list['additionName'])) ? $list['additionName'] : '---' }}</td>
            <td><br>{{ (isset($list['homepageUrl'])) ? $list['homepageUrl'] : '---' }}</td>
            <td><br>{{ (isset($list['position'])) ? $list['position'] : '---' }}</td>
            <td><br>{{ (isset($list['defaultContact'])) ? $list['defaultContact'] : '---' }}</td>
            <td><br>{{ (isset($list['localPartnerContact'])) ? $list['localPartnerContact'] : '---' }}</td>
            <td><br>{{ (isset($list['businessCardContact'])) ? $list['businessCardContact'] : '---' }}</td>
            <td><br>{{ (isset($list['realEstateReferenceCount'])) ? $list['realEstateReferenceCount'] : '---' }}</td>
            <td><br>{{ (isset($list['externalId'])) ? $list['externalId'] : '---' }}</td>
            <td><br>{{ (isset($list['showOnProfilePage'])) ? $list['showOnProfilePage'] : '---' }}</td>

                                
                                <td>

                                   <a href="{{ url('/delete_user').'/'.$list['@attributes']['id'] }}" class="btn btn-info btn-outline btn-circle btn-lg m-r-5" onclick="return confirm('property.confirm')">
                                        <i class="icon-trash"></i></a>

                                   

                                </td>
                            </tr>

                            @endforeach
                              
                                              
                        </tbody>
                    </table>
                 
                </div>
            </div>
        </div>

			
		</div>
	</div>
	
</section>


@endsection
