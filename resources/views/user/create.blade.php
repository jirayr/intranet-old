@extends('layouts.admin')

@section('content')
    <div class="row">

        {!! Form::open(['action' => ['UserController@store'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
        {!! Form::token() !!}
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading"><button onclick="location.href='{{route('users.index')}}'" type="button"
                                              class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                class="ti-arrow-left"></i></button> {{__('user.create_user')}}
                </div>
                <div class="white-box">

                    <form class="form-horizontal" action="" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-12">{{__('user.field.name')}}</label>
                            <div class="col-md-12">
                                <input type="text" name="name" class="form-control form-control-line" value="" placeholder="{{__('user.field.name')}}"
                                       required></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="example-company">{{__('user.field.company')}}</label>
                            <div class="col-md-12">
                                <input type="text" name="company" class="form-control form-control-line" placeholder="Company"
                                       value=""></div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12" for="example-email">{{__('user.field.email')}}</label>
                            <div class="col-md-12">
                                <input type="email" name="email" class="form-control form-control-line" placeholder="example@gmail.com"
                                       required value=""></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="example-phone">Phone</label>
                            <div class="col-md-12">
                                <input type="text" name="phone" class="form-control form-control-line" placeholder="Phone" required
                                       value=""></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="example-phone">Mobil</label>
                            <div class="col-md-12">
                                <input type="text" name="mobile" class="form-control form-control-line" placeholder="Mobile"
                                       value=""></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">{{__('user.field.password')}}</label>
                            <div class="col-md-12">
                                <input type="password" name="password" class="form-control form-control-line" value=""
                                       placeholder="{{__('user.field.password')}}" required></div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-12">Signatur Ändern</label>
                            <div class="col-md-12">
                                <input type="file" name="signature" class="form-control form-control-line"></div>
                        </div>
                        @if(Auth()->user()->id == 2)
                        <div class="form-group col-md-12">
                            <label for="email">Property Name:</label>
                            <select   class="form-control property-name" name="property_id[]" style="width: 100%;" ></select>
                        </div>
                        @endif

                        <div class="form-group col-md-12">
                            <label for="user_status">Status</label>
                            <select class="form-control form-control-line" name="user_status" style="width: 100%;" >
                                <option value="1">Active</option>
                                <option value="0">Deactive</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-2 col-sm-4 col-xs-12">
                                <a href="{{route('users.index')}}" type="button"
                                   class="btn btn-block btn-danger btn-rounded">{{__('user.cancel')}}</a>
                            </div>
                            <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12">
                                <button type="submit" name="submit" class="btn btn-block btn-primary btn-rounded">{{__('user.submit')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
     {!! Form::close() !!}
    </div>

@endsection
@section('scripts')
    <script>

        $(document).ready(function() {

            $(".property-name").select2({

                minimumInputLength: 2,

                language: {

                    inputTooShort: function () {
                        return "<?php echo 'Please enter 2 or more characters'; ?>"
                    }
                },
                multiple: true,
                tags: false,

                ajax: {

                    url : "{{url('/get-properties')}}",
                    dataType: 'json',
                    data: function (term) {
                        return {
                            query: term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text:  item.name_of_property,
                                    id: item.id,
                                }

                            })

                        };
                    }

                }

            });





        });


    </script>
@stop
