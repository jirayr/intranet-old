@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading">{{__('user.user_activities')}}
                    <div class="button-group" style="float: right">
                        <a href="{{route('user-activities') . '?type=day'}}" type="button" class="btn btn-default">{{__('user.activity.day')}}</a>
                        <a href="{{route('user-activities') . '?type=week'}}" type="button" class="btn btn-default">{{__('user.activity.week')}}</a>
                        <a href="{{route('user-activities') . '?type=month'}}" type="button" class="btn btn-default">{{__('user.activity.month')}}</a>
                    </div>
                </div>


                <div class="table-responsive">
                    <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th style="width: 70px;" class="text-center">#</th>
                            <th>{{__('user.activity.name')}}</th>
                            <th>{{__('user.activity.email')}}</th>
                            <th>{{__('user.activity.number_of_properties')}}</th>
                            <th>{{__('property.declined')}}</th>
                            <th>{{__('property.offer')}}</th>
                            <th>{{__('property.duration')}}</th>
                            <th>{{__('property.in_purchase')}}</th>
                            <th>{{__('property.sold')}}</th>
                            <th>{{__('property.lost')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td class="text-center"><br>{{$user->id}}</td>
                                <td><br>{{$user->name}}</td>
                                <td><br>{{$user->email}}</td>
                                <td><br>{{$user->number_of_properties}}</td>
                                <td><br>{{ count($user->_properties_declined) }}</td>
                                <td><br>{{ count($user->_properties_offer) }}</td>
                                <td><br>{{ count($user->_properties_duration) }}</td>
                                <td><br>{{ count($user->_properties_in_purchase) }}</td>
                                <td><br>{{ count($user->_properties_sold) }}</td>
                                <td><br>{{ count($user->_properties_lost) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper" style="margin-left: 20px">
                        <div class="row pull-left">
                            {{$users->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection