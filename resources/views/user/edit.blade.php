@extends('layouts.admin')

@section('content')

    <div class="row">

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading"><button onclick="location.href='{{route('users.index')}}'" type="button"
                                              class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                class="ti-arrow-left"></i></button> {{__('user.edit_user')}} #{{$user->id}}
                </div>
                <div class="white-box">

                    {!! Form::open(['action' => ['UserController@update',$user->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
                    {!! Form::token() !!}
                        <div class="form-group">
                            <label class="col-md-12">{{__('user.field.name')}}</label>
                            <div class="col-md-12">
                                <input type="text" name="name" class="form-control form-control-line" value="{{$user->name}}" placeholder="{{__('user.field.name')}}"
                                       required></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="example-company">{{__('user.field.company')}}</label>
                            <div class="col-md-12">
                                <input type="text" name="company" class="form-control form-control-line" placeholder="Company"
                                       value="{{$user->company}}"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="example-email">{{__('user.field.email')}}</label>
                            <div class="col-md-12">
                                <input type="email" name="email" class="form-control form-control-line" placeholder="Email" required
                                       value="{{$user->email}}"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="example-phone">Phone</label>
                            <div class="col-md-12">
                                <input type="text" name="phone" class="form-control form-control-line" placeholder="Phone" required
                                       value="{{$user->phone}}"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="example-phone">Mobil</label>
                            <div class="col-md-12">
                                <input type="text" name="mobile" class="form-control form-control-line" placeholder="Mobile" required
                                       value="{{$user->mobile}}"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">{{__('user.field.password')}}</label>
                            <div class="col-md-12">
                                <input type="password" name="password" class="form-control form-control-line" value=""
                                       placeholder="{{__('user.field.password')}}"></div>
                        </div>
                        <div  class="form-group">
                            <label class="col-md-12" >Role</label>
                            <select class="col-md-12 form-control form-control-line" name="role">
                                <option value="1" @if($user->role == config('auth.role.admin')) selected @endif>{{__('user.role.admin')}}</option>
                                <option value="2" @if($user->role == config('auth.role.user')) selected @endif>{{__('user.role.user')}}</option>
                                <option value="4" @if($user->role == config('auth.role.manager')) selected @endif>{{__('user.role.manager')}}</option>
                                <option value="5" @if($user->role == config('auth.role.finance')) selected @endif>{{__('user.role.finance')}}</option>
                                <option value="3" @if($user->role == config('auth.role.guest')) selected @endif>guest</option>
                                <option value="6" @if($user->role == config('auth.role.extern')) selected @endif>Extern</option>

                                <option value="7" @if($user->role == config('auth.role.hvbu')) selected @endif>HV BU</option>
                                <option value="8" @if($user->role == config('auth.role.hvpm')) selected @endif>HV PM</option>
                                <option value="9" @if($user->role == config('auth.role.service_provider')) selected @endif>Dienstleister</option>
                                <option value="10" @if($user->role == config('auth.role.tax_consultant')) selected @endif>Steuerberater</option>
                            </select>

                        </div>

                        <div  class="form-group">
                            <label class="col-md-12" >Secondry Role</label>
                            <select class="col-md-12 form-control form-control-line" name="second_role">
                                <option value="">Select Role</option>
                                <option value="1" @if($user->second_role == config('auth.role.admin')) selected @endif>{{__('user.role.admin')}}</option>
                                <option value="2" @if($user->second_role == config('auth.role.user')) selected @endif>{{__('user.role.user')}}</option>
                                <option value="4" @if($user->second_role == config('auth.role.manager')) selected @endif>{{__('user.role.manager')}}</option>
                                <option value="5" @if($user->second_role == config('auth.role.finance')) selected @endif>{{__('user.role.finance')}}</option>
                                <option value="3" @if($user->second_role == config('auth.role.guest')) selected @endif>guest</option>

                                <option value="7" @if($user->second_role == config('auth.role.hvbu')) selected @endif>HV BU</option>
                                <option value="8" @if($user->second_role == config('auth.role.hvpm')) selected @endif>HV PM</option>
                            </select>

                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Signatur Ändern</label>
                            <div class="col-md-12">
                                <input type="file" name="signature" class="form-control form-control-line"></div>
                        </div>
                        @if($user->signature)
                        <div class="form-group">
                            <img src="{{asset('signature/'.$user->signature)}}" height="100px;">
                        </div>
                        @endif

                       @if(Auth()->user()->id == 2)
                            <div class="form-group col-md-12">
                                <label for="email">Property Name:</label>

                                <select multiple  class="form-control property-name" name="property_id[]" style="width: 100%;" >
                                    @foreach($user->properties as $property)
                                        <option value="{{$property->property->id}}" selected>{{$property->property->name_of_property}}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif

                        @if(Auth()->user()->id != $user->id)
                            <div class="form-group col-md-12">
                                <label for="user_status">Status</label>
                                <select class="form-control form-control-line" name="user_status" style="width: 100%;" >
                                    <option value="1" {{ ($user->user_status == 1) ? 'selected' : '' }}>Active</option>
                                    <option value="0" {{ ($user->user_status == 0) ? 'selected' : '' }}>Deactive</option>
                                </select>
                            </div>
                        @endif

                    <div class="form-group">
                            <div class="col-lg-2 col-sm-4 col-xs-12">
                                <a href="{{route('users.index')}}" type="button"
                                   class="btn btn-block btn-danger btn-rounded">{{__('user.cancel')}}</a>
                            </div>
                            <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12">
                                <button type="submit" class="btn btn-block btn-primary btn-rounded">{{__('user.submit')}}</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

        $(document).ready(function() {

            $(".property-name").select2({

                minimumInputLength: 2,

                language: {

                    inputTooShort: function () {
                        return "<?php echo 'Please enter 2 or more characters'; ?>"
                    }
                },
                multiple: true,
                tags: false,

                ajax: {

                    url : "{{url('/get-properties')}}",
                    dataType: 'json',
                    data: function (term) {
                        return {
                            query: term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text:  item.name_of_property,
                                    id: item.id,
                                }

                            })

                        };
                    }

                }

            });





        });


    </script>
@stop
