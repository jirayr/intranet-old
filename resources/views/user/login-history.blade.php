@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">

                @if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'])
                    <a style="margin:10px; " href="{{route('login_history')}}" class="btn btn-primary pull-right">Back</a>
                    <div class="clearfix"></div>
                @endif
                <div class="panel-heading">
                    Login-Verlauf
                    <!-- <a href="{{ route('property_history') }}" class="btn btn-primary btn-sm pull-right">Objekt-status-Verlauf</a> -->
                </div>
                <div class="table-responsive">
                    <table class="table table-hover color-bordered-table purple-bordered-table" id="log-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>IP Address</th>
                                <th>User Agent<th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                @if(isset($_REQUEST['export_user_id']) && $_REQUEST['export_user_id'])
                    <a style="margin:10px; " href="{{route('login_history')}}" class="btn btn-primary pull-right">Back</a>
                    <div class="clearfix"></div>
                @endif
                <div class="panel-heading">Exporthistorie</div>
                <div class="table-responsive">
                    <table class="table table-hover color-bordered-table purple-bordered-table" id="export-log-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Export</th>
                                <th>File<th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

        <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">

                @if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'])
                    <a style="margin:10px; " href="{{route('property_history')}}" class="btn btn-primary pull-right">Back</a>
                    <div class="clearfix"></div>
                @endif
                <div class="panel-heading">Objekt-status-Verlauf</div>
                <div class="table-responsive">
                    <table class="table table-hover color-bordered-table purple-bordered-table" id="property-history-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Objekt</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>alter Status</th>
                                <th>Neuer Status<th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php
    $user_id = 0;
    if(isset($_REQUEST['user_id']) && $_REQUEST['user_id']){
        $user_id = $_REQUEST['user_id'];
    }

    $export_user_id = 0;
    if(isset($_REQUEST['export_user_id']) && $_REQUEST['export_user_id']){
        $export_user_id = $_REQUEST['export_user_id'];
    }
    $status_user_id = 0;
    if(isset($_REQUEST['status_user_id']) && $_REQUEST['status_user_id']){
        $status_user_id = $_REQUEST['status_user_id'];
    }
?>
@endsection

@section('js')
    <script type="text/javascript">
        

        var log_table = $('#log-table').DataTable({
            processing: true,
            serverSide: true,
            order: [
                [2, 'desc']
            ],
            ajax: '{{ route('login_history_data') }}?user_id={{$user_id}}',
            columns: [
                {data: 'DT_RowIndex', name: 'lg.id'},
                {data: 'name', name: 'u.name'},
                {data: 'created_at', name: 'lg.created_at'},
                {data: 'ip_address', name: 'lg.ip_address'},
                {data: 'agent', name: 'lg.agent'},
            ],
            "drawCallback": function( settings ) {
            }
        });

        var export_log_table = $('#export-log-table').DataTable({
            processing: true,
            serverSide: true,
            order: [
                [2, 'desc']
            ],
            ajax: '{{ route('export_history_data') }}?user_id={{$export_user_id}}',
            columns: [
                {data: 'DT_RowIndex', name: 'eh.id'},
                {data: 'name', name: 'u.name'},
                {data: 'created_at', name: 'eh.created_at'},
                {data: 'section_type', name: 'eh.section_type'},
                {data: 'file_name', name: 'eh.file_name'},
            ],
            "drawCallback": function( settings ) {
            }
        });


        $('#property-history-table').DataTable({
            processing: true,
            serverSide: true,
            order: [
                [3, 'desc']
            ],
            ajax: '{{ route('property_history_data') }}?status_user_id={{$status_user_id}}',
            columns: [
                {data: 'DT_RowIndex', name: 'ph.id'},
                {data: 'name_of_property', name: 'p.name_of_property'},
                {data: 'name', name: 'u.name'},
                {data: 'created_at', name: 'ph.created_at'},
                {data: 'old_value', name: 'ph.old_value'},
                {data: 'new_value', name: 'ph.new_value'},
            ],
            "drawCallback": function( settings ) {
            }
        });



    </script>
@endsection


