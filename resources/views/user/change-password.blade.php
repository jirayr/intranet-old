@extends('layouts.admin')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">


    <!-- Date picker plugins css -->
    <link href="{{ asset('assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="{{ asset('assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

@endsection

@section('content')

    <div class="row">
        @if(Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i> <?php echo e(Session::get('message')); ?></p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-times"></i> {{Session::get('error')}}</p>
            </div>
        @endif
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading"> {{__('user.change_password')}}
                </div>
                <div class="white-box">

                    {!! Form::open(['action' => ['UserController@postChangePassword'], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                    {!! Form::token() !!}
                    <div class="form-group">
                        <label class="col-md-12">{{__('user.password.current_password')}}</label>
                        <div class="col-md-12">
                            <input type="password" name="current_password" class="form-control form-control-line" placeholder="{{__('user.password.current_password_hint')}}"
                                   required></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">{{__('user.password.new_password')}}</label>
                        <div class="col-md-12">
                            <input type="password" name="new_password1" required class="form-control form-control-line" placeholder="{{__('user.password.new_password_hint')}}"
                            ></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">{{__('user.password.confirm_new_password')}}</label>
                        <div class="col-md-12">
                            <input type="password" name="new_password2" required class="form-control form-control-line" placeholder="{{__('user.password.confirm_new_password_hint')}}"
                            ></div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-2 col-sm-4 col-xs-12">
                            <a href="{{route('index')}}" type="button"
                               class="btn btn-block btn-danger btn-rounded">{{__('user.cancel')}}</a>
                        </div>
                        <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12">
                            <button type="submit" class="btn btn-block btn-primary btn-rounded">{{__('user.submit')}}</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection