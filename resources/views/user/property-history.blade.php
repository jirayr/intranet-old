@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">

                @if(isset($_REQUEST['user_id']) && $_REQUEST['user_id'])
                    <a style="margin:10px; " href="{{route('property_history')}}" class="btn btn-primary pull-right">Back</a>
                    <div class="clearfix"></div>
                @endif
                <div class="panel-heading">Objekt-status-Verlauf</div>
                <div class="table-responsive">
                    <table class="table table-hover color-bordered-table purple-bordered-table" id="property-history-table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Objekt</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>alter Status</th>
                                <th>Neuer Status<th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php
    $user_id = 0;
    if(isset($_REQUEST['user_id']) && $_REQUEST['user_id']){
        $user_id = $_REQUEST['user_id'];
    }
?>
@endsection

@section('js')
    <script type="text/javascript">
        

        $('#property-history-table').DataTable({
            processing: true,
            serverSide: true,
            order: [
                [3, 'desc']
            ],
            ajax: '{{ route('property_history_data') }}?user_id={{$user_id}}',
            columns: [
                {data: 'DT_RowIndex', name: 'ph.id'},
                {data: 'name_of_property', name: 'p.name_of_property'},
                {data: 'name', name: 'u.name'},
                {data: 'created_at', name: 'ph.created_at'},
                {data: 'old_value', name: 'ph.old_value'},
                {data: 'new_value', name: 'ph.new_value'},
            ],
            "drawCallback": function( settings ) {
            }
        });

    </script>
@endsection


