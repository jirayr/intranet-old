@extends('layouts.admin')
@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">


    <!-- Date picker plugins css -->
    <link href="{{ asset('assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="{{ asset('assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

@endsection

@section('content')

    <div class="row">
        @if(Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i><?php echo e(Session::get('message')); ?></p>
            </div>
        @endif



        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading"><button onclick="location.href='{{route('index')}}'" type="button"
                                              class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                class="ti-arrow-left"></i></button> {{__('user.edit_profile')}}
                </div>
                <div class="white-box">

                    {!! Form::open(['action' => ['UserController@postProfile',$id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal']) !!}
                    {!! Form::token() !!}
                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.name')}}</label>
                        <div class="col-md-12">
                            <input type="text" name="name" class="form-control form-control-line" value="{{$user->name}}" placeholder="{{__('user.field.name')}}"
                                   required></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.first_name')}}</label>
                        <div class="col-md-12">
                            <input type="text" name="first_name" class="form-control form-control-line" value="{{$user->first_name}}" placeholder="{{__('user.field.first_name')}}"
                                   ></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.last_name')}}</label>
                        <div class="col-md-12">
                            <input type="text" name="last_name" class="form-control form-control-line" value="{{$user->last_name}}" placeholder="{{__('user.field.last_name')}}"
                                   ></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.birthday')}}</label>
                        <div class="col-md-12">
                            <input type="text" value="<?php $date=date_create($user->birthday);echo date_format($date,"m/d/Y");?>" name="birthday" class="form-control mydatepicker" placeholder="mm/dd/yyyy">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.profile_picture')}}</label>
                        <div class="col-sm-6 ol-md-6 col-xs-12">
                            <div class="white-box">
                                <label for="input-file-now-custom-1">{{__('user.message.change_avatar')}}</label>
                                <input name="image" style="height: 300px;width: 450px" type="file" id="input-file-now-custom-1" class="dropify"
                                       data-default-file="{{$url_image_profile}}" accept="image/*"/> </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.phone')}}</label>
                        <div class="col-md-12">
                            <input type="text" name="phone" class="form-control form-control-line" value="{{$user->phone}}"
                                   placeholder="+ xx xxx xxx"
                            ></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Mobil</label>
                        <div class="col-md-12">
                            <input type="text" name="mobile" class="form-control form-control-line" value="{{$user->mobile}}"
                                   placeholder="+ xx xxx xxx"
                            ></div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.address')}}</label>
                        <div class="col-md-12">

                            <textarea name="address" rows="5" class="form-control form-control-line" placeholder="{{__('user.example')}}:Cecilia Chapman,711-2880 Nulla St.Mankato Mississippi 96522 (257) 563-7401">{{$user->address}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.gender')}}</label>
                        <div class="col-md-12">
                            <select class="form-control form-control-line" name="gender">
                                <option value="">{{__('user.gender.unknown')}}</option>
                                @if($user->gender ==1)
                                    <option value="1" selected>{{__('user.gender.male')}}</option>
                                    <option value="0">{{__('user.gender.female')}}</option>
                                @else
                                    @if($user->gender == 0)
                                        <option value="1">{{__('user.gender.male')}}</option>
                                        <option value="0" selected>{{__('user.gender.female')}}</option>
                                    @endif

                                @endif


                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.website')}}</label>
                        <div class="col-md-12">
                            <input type="text" name="website" class="form-control form-control-line" value="{{$user->website}}"
                                   placeholder="{{__('user.example')}}: http://example.com"
                            ></div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-12">{{__('user.field.postal_code')}}</label>
                        <div class="col-md-12">
                            <input type="text" name="postal_code" class="form-control form-control-line" value="{{$user->postal_code}}"
                                   placeholder="{{__('user.field.postal_code')}}"
                            ></div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="example-email">{{__('user.field.email')}}</label>
                        <div class="col-md-12">
                            <input type="email" name="email" class="form-control form-control-line" placeholder="{{__('user.example')}}: example@gmail.com" required
                                   value="{{$user->email}}"></div>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label class="col-md-12">Password</label>--}}
                        {{--<div class="col-md-12">--}}
                            {{--<input type="password" name="password" class="form-control form-control-line" value=""--}}
                                   {{--placeholder="Password" required></div>--}}
                    {{--</div>--}}


                    <div class="form-group">
                        <div class="col-lg-2 col-sm-4 col-xs-12">
                            <a href="{{route('index')}}" type="button"
                               class="btn btn-block btn-danger btn-rounded">{{__('user.cancel')}}</a>
                        </div>
                        <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12">
                            <button type="submit" class="btn btn-block btn-primary btn-rounded">{{__('user.submit')}}</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script src="{{ asset('assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset('assets/plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>

    {{--<script src="{{ asset('assets/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>--}}

    <script src="{{ asset('assets/plugins/bower_components/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();
            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });
            // Used events
            var drEvent = $('#input-file-events').dropify();
            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });
            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });
            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>

    <script>

        // Date Picker
        jQuery('.mydatepicker, #datepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('#date-range').datepicker({
            toggleActive: true
        });
        jQuery('#datepicker-inline').datepicker({
            todayHighlight: true
        });

    </script>
@endsection