@extends('layouts.admin')

@section('content')

    <div class="row">
        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('message')}}</p>
            </div>
        @endif

        @if (Session::has('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <p><i class="icon fa fa-check"></i>{{Session::get('error')}}</p>
            </div>
        @endif

            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="panel">
                    <div class="panel-heading">{{__('user.manage_users')}} <button onclick="location.href= '{{route('users.create')}}'" type="button"
                                                               class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                    class="ti-plus"></i></button></div>

                    <div class="table-responsive">
                        <table class="table table-hover manage-u-table color-bordered-table purple-bordered-table" id="user-table">
                            <thead>
                            <tr>
                                <th style="width: 70px;" class="text-center">#</th>
                                <th>{{__('user.activity.name')}}</th>
                                <th>{{__('user.activity.email')}}</th>
                                <th>{{__('user.activity.role')}}</th>
                                <th>Status</th>
                                <th>{{__('user.activity.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td class="text-center"><br>{{$user->id}}</td>
                                    <td><br>{{$user->name}}</td>
                                    <td><br>{{$user->email}}</td>

                                    @if($user->role == config('auth.role.admin'))
                                        <td><br>{{__('user.role.admin')}}
                                    @elseif($user->role == config('auth.role.manager'))
                                        <td><br>{{__('user.role.manager')}}
                                    @elseif($user->role == config('auth.role.finance'))
                                        <td><br>{{__('user.role.finance')}}
                                    @elseif($user->role == config('auth.role.guest'))
                                        <td><br>guest
                                    @elseif($user->role == config('auth.role.extern'))
                                        <td><br>Extern
                                    @elseif($user->role == config('auth.role.hvbu'))
                                        <td><br>HV BU
                                    @elseif($user->role == config('auth.role.hvpm'))
                                        <td><br>HV PM
                                    @elseif($user->role == config('auth.role.service_provider'))
                                        <td><br>Dienstleister
                                    @elseif($user->role == config('auth.role.tax_consultant'))
                                        <td><br>Steuerberater
                                    @else
                                        <td><br>{{__('user.role.user')}}
                                    @endif

                                    @if($user->second_role)
                                    @if($user->second_role == config('auth.role.admin'))
                                        <br>{{__('user.role.admin')}}
                                    @elseif($user->second_role == config('auth.role.manager'))
                                        <br>{{__('user.role.manager')}}
                                    @elseif($user->second_role == config('auth.role.finance'))
                                        <br>{{__('user.role.finance')}}
                                    @elseif($user->second_role == config('auth.role.guest'))
                                        <br>guest
                                    @elseif($user->second_role == config('auth.role.hvbu'))
                                        <br>HV BU
                                    @elseif($user->second_role == config('auth.role.hvpm'))
                                        <br>HV PM
                                    @else
                                        <br>{{__('user.role.user')}}
                                    @endif
                                    @endif
                                    </td>
                                    <td><br>
                                        {{ ($user->user_status == 1) ? 'Active' : 'Deactive' }}
                                    </td>
                                    <td>

                                        @if($user->id != Auth::user()->id)
                                            {!! Form::open(['action' => ['UserController@destroy', $user->id],
                                            'method' => 'DELETE']) !!}
                                            <button onclick="location.href='{{route('users.edit',['users'=>$user->id])}}'" type="button"
                                               class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                                        class="ti-pencil-alt"></i></button>
                                            <button type="submit" class="btn btn-info btn-outline btn-circle btn-lg m-r-5"
                                                    onclick="return confirm('{{__("user.confirm")}}')">
                                                <i class="icon-trash"></i></button>

                                            {!! Form::close() !!}
                                        @else
                                            <button  onclick="location.href= '{{route('users.edit',['users'=>$user->id])}}'" type="button"
                                               class="btn btn-info btn-outline btn-circle btn-lg m-r-5"><i
                                                        class="ti-pencil-alt"></i></button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{-- <div class="pagination-wrapper" style="margin-left: 20px">
                            <div class="row pull-left">
                                {{$users->links()}}
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
    </div>


@endsection

@section('js')
    <script type="text/javascript">
        $('#user-table').DataTable();
    </script>
@endsection


