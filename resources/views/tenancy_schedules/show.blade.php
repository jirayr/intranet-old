@extends('layouts.admin')

@section('css')
	<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
	<!-- Styles -->
	<link href="{{ asset('css/property-details.css') }}" rel="stylesheet">
	<style type="text/css">
    	@media print {
		    /* Hide everything in the body when printing... */
		    body.printing * { display: none; }
		    /* ...except our special div. */
		    body.printing .print-me { display: block; }

		    #page-wrapper{
		    	margin-left:0px !important;
		    }
		    .sidebar{
		    	display: none !important;
		    }
		}
		
    </style>
@endsection
@section('content')
	<div id="tenancy-schedule" class="print-me">
		<h1>Meine Objekte</h1>
		<div class="white-box table-responsive">
			<button type="button" style="margin-bottom: 15px;" class="btn btn-success print-button pull-left">Print</button>
			<div class="clearfix"></div>
			<table id="tenancy-schedule-table" class="table table-striped dashboard-table">

				<thead>
				<tr>
					<th>#</th>
					<th>Objekt</th>
					<th>Asset Manager</th>
					<th>Fläche in qm</th>
					<th>Leerstand</th>
					<th>Anzahl Leerstand</th>
					<!-- <th>Technische FF</th> -->
					<th>Fläche vermietet</th>
					<th>Miete Netto p.m.</th>
					<th>durchschn. Miete</th>
					<th>Potenzial p.a.</th>
					<th class="text">&nbsp;</th>
					<th class="text">&nbsp;</th>
					<th class="text">&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				{{-- @foreach($masterlistes as $key => $masterliste)
					<tr>
						<td >{{$masterliste->id}}.</td>
						<td>
							<a href="#" class="inline-edit" data-type="text" data-pk="object" data-url="{{route('masterliste.update', $masterliste->id) }}">{{ $masterliste->object }}</a>
						</td>
						<td>
							<a href="#" class="inline-edit" data-type="select" data-pk="asset_manager" data-url="{{route('masterliste.update', $masterliste->id) }}" data-source="{{$users_data}}">{{ $masterliste->asset_manager_name }}</a>
						</td>
						<td class="">
							<a href="#" class="inline-edit" data-type="number" data-pk="flat_in_qm" data-url="{{route('masterliste.update', $masterliste->id) }}">{{ number_format($masterliste->flat_in_qm,2,",",".") }}</a>
						</td>
						<td class="number-right">
							<a href="#" class="inline-edit" data-type="number" data-pk="vacancy" data-url="{{route('masterliste.update', $masterliste->id) }}" >{{ number_format($masterliste->vacancy,2,",",".") }}</a>
						</td>
						<td class="number-right">
							<a href="#" class="inline-edit" data-type="number" data-pk="vacancy_structurally" data-url="{{route('masterliste.update', $masterliste->id) }}" >{{ number_format($masterliste->vacancy_structurally,2,",",".") }}</a>
						</td>
						
						<td class="number-right">
							<a href="#" class="inline-edit" data-type="number" data-pk="rented_area" data-url="{{route('masterliste.update', $masterliste->id) }}" >{{ number_format($masterliste->rented_area,2,",",".") }}</a>
						</td>
						<td class="number-right">
							<a href="#" class="inline-edit" data-type="number" data-pk="rent_net_pm" data-url="{{route('masterliste.update', $masterliste->id) }}">{{ number_format($masterliste->rent_net_pm,2,",",".") }}</a>
						</td>
						<td class="number-right">
							<a href="#" class="inline-edit" data-type="number" data-pk="avg_rental_fee" data-url="{{route('masterliste.update', $masterliste->id) }}">{{ number_format($masterliste->avg_rental_fee,2,",",".") }}</a>
						</td>
						<td class="number-right">
							<a href="#" class="inline-edit" data-type="number" data-pk="potential_pm" data-url="{{route('masterliste.update', $masterliste->id) }}">{{ number_format($masterliste->potential_pm,2,",",".") }}</a>
						</td>
						<td><button type="button" class="btn btn-success delete-masterliste" data-id="{{$masterliste->id}}">Delete</button></td>
					</tr>
				@endforeach--}}


				@foreach($masterlistes as $key => $masterliste)
					<?php
						if( $masterliste->property == null )
							 continue;
					?>
					<tr>
						<td >{{$masterliste->id}}.</td>
						<td>
							<?php
							$bank = 0;
							$bank_ids = json_decode($masterliste->property->bank_ids,true);
                            if($bank_ids != null) {
                                foreach ($banks as $b) {
                                    if(isset($bank_ids[0]) && $b->id == $bank_ids[0]){
                                        $bank = $b;
                                        break;
                                    }
                                }
                            }
                            if($bank){
                                    $propert  =  DB::table('properties')->where('properties.Ist', $bank->id)->where('main_property_id',$masterliste->property->id)->first();
                                     if($propert)
                                         $masterliste->property->name_of_property = $propert->name_of_property;

                                }
							echo  $masterliste->property->name_of_property;
							?>
							<?php
							
							/**/
                            
							?>
						</td>
						<td>
							<a href="#" class="inline-edit" data-type="select" data-pk="asset_manager" data-url="{{route('masterliste.update', $masterliste->id) }}" 
							data-source="{{$users_data}}"
							
							
							>{{ $masterliste->asset_manager_name }}</a>

						</td>

						<td class="number-right">
							@if( $masterliste->tenancy_schedule != null )
								{{number_format($masterliste->tenancy_schedule->calculations['mi9'] + $masterliste->tenancy_schedule->calculations['mi10'], 2,",",".")}}
							@else
								{{number_format(0, 2,",",".")}}
							@endif

						</td>

						<td class="number-right">
							@if( $masterliste->tenancy_schedule != null )
								{{ number_format($masterliste->tenancy_schedule->calculations['total_live_vacancy_in_qm'] + $masterliste->tenancy_schedule->calculations['total_business_vacancy_in_qm'], 2,",",".") }}
								
							@else
								{{number_format(0, 2,",",".")}}
							@endif

						</td>

						<td class="number-right">
							@if( $masterliste->tenancy_schedule != null )
							{{ number_format($masterliste->tenancy_schedule->calculations['vbusinesscount'] + $masterliste->tenancy_schedule->calculations['vlivecount'],0,",",".") }}
							@else
								{{number_format(0, 2,",",".")}}
							@endif
							<!-- <a href="#" class="inline-edit" data-type="number" data-pk="vacancy_structurally" data-url="{{route('masterliste.update', $masterliste->id) }}" ></a> -->
						</td>
						

						<td class="number-right">
							@if( $masterliste->tenancy_schedule != null )
								{{number_format($masterliste->tenancy_schedule->calculations['total_live_rental_space'] + $masterliste->tenancy_schedule->calculations['total_business_rental_space'],2,",",".")}}
							@else
								{{number_format(0, 2,",",".")}}
							@endif

						</td>

						<td class="number-right">
							@if( $masterliste->tenancy_schedule != null )
								{{number_format($masterliste->tenancy_schedule->calculations['total_actual_net_rent'],2,",",".")}}
							@else
								{{number_format(0, 2,",",".")}}
							@endif

						</td>

						<td class="number-right">
							{{--avg_rental_fee--}}
							@if( $masterliste->tenancy_schedule != null )

								@if( ( $masterliste->tenancy_schedule->calculations['total_business_rental_space']) != 0 )
                                    {{ number_format($masterliste->tenancy_schedule->calculations['total_business_actual_net_rent'] / $masterliste->tenancy_schedule->calculations['total_business_rental_space'], 2,",",".") }}
                                @else
                                    0
                                @endif
								
							@else
								{{number_format(0, 2,",",".")}}
							@endif
						</td>

						<td class="number-right">
							@if( $masterliste->tenancy_schedule != null )
                                {{ number_format( $masterliste->tenancy_schedule->calculations['potenzial_eur_jahr'] , 2,",",".") }}
                            @else
                                {{ number_format(0, 2,",",".") }}
                            @endif
						</td>

						@if(!$masterliste->property_id)
						<td>
							<button type="button" class="btn btn-success btn-property-create" data-toggle="modal" data-target="#selectbank_Modal" value="{{$masterliste->id}}">{{__('masterliste.property_info')}}</button>
						</td>
						@else
						<td>
							<a class="btn btn-success" href="{{route('properties.show',['property'=>$masterliste->property_id])}}">Info</a>
						</td>
						@endif
						<td>
							<a class="btn btn-success" href="{{route('propertycomments.show', ['id' => $masterliste->id])}}">Aktionsplan</a>
						</td>
						<td>
							<button type="button" class="btn btn-success delete-masterliste" data-id="{{$masterliste->id}}">Löschen</button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<br/>

			<!-- <button type="button" class="btn btn-success" id="new-masterliste">Add Masterliste</button> -->

		</div>

	</div>

@endsection

@section('js')
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

	<script type="text/javascript">
		
		$('.print-button').click(function(){
		 	page = $('#tenancy-schedule-table');
		 	var pageCls = page.prop('class');
	     	page.prop('class', '');

	     	$('.print-button').hide();
	     	$('.dataTables_info').hide();
	     	$('.dataTables_paginate').hide();
	     	$('.btn').hide();

	     	// $('body').addClass('printing');
		 	// $('body').addClass('hide-sidebar');

 
	     	$('.dataTables_length').hide();
	     	$('.dataTables_filter').hide();

	     	window.print();
	     	page.prop('class', pageCls);

	     	$('.dataTables_length').show();
	     	$('.dataTables_filter').show();
	     	$('.print-button').show();

	     	// $('body').removeClass('printing');

	     	// $('body').removeClass('show-sidebar');
		 	// $('body').removeClass('hide-sidebar');


	     	$('.dataTables_info').show();
	     	$('.dataTables_paginate').show();
	     	$('.btn').show();

     	});
	
        jQuery(document).ready(function($){
            $('#tenancy-schedule').find('a.inline-edit').editable({
                step: 'any',
                success: function(response, newValue) {
                    if( response.success === false )
                        return response.msg;
                    else{
                        location.reload();
                    }
                }
            });

            $('#new-masterliste').on('click', function () {
                $.ajax({
                    url: "{{route('masterliste.store')}}",
                    type: "post",
                    data : {

                    },
                    success: function (response) {
                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }

                });
            });
            $('.delete-masterliste').on('click', function () {
                $.ajax({
                    url: "{{route('masterliste.delete')}}",
                    type: "post",
                    data : {
                        'id':$(this).data('id')
                    },
                    success: function (response) {
                        location.reload();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }

                });
            });

            var tenancy_schedule_table = $('#tenancy-schedule-table').DataTable({
				"columns": [
					null,
					null,
					null,
					{ "type": "formatted-num" },
					{ "type": "formatted-num" },
					{ "type": "formatted-inline" },
					{ "type": "formatted-num" },
					{ "type": "formatted-num" },
					{ "type": "formatted-num" },
					{ "type": "formatted-num" },
					null,
					null,
					null
				],
				"bStateSave": true,
		        "fnStateSave": function (oSettings, oData) {
		            localStorage.setItem( 'tenancy_schedule_table_dataTables', JSON.stringify(oData) );
		        },
		        "fnStateLoad": function (oSettings) {
		            return JSON.parse( localStorage.getItem('tenancy_schedule_table_dataTables') );
		        }
			});
        });
	</script>
@endsection