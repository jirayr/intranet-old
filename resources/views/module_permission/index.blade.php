@extends('layouts.admin')

@section('content')

    <div class="row">

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="panel">
                <div class="panel-heading"> Module Permission</div>
                <div class="white-box">
                    <div id="flash_alerts"></div>

                    {!! Form::open(['action' => ['ModulePermissionController@update'], 'class' => 'form-horizontal', 'id'=> 'module_permission_form']) !!}
                        <div  class="form-group row">
                            <label class="col-md-2" >Permission:</label>
                            <select class="col-md-8 form-control-line module-select-permission" name="permission" id="permission">
                                <option value="" >select</option>
                                @foreach($propertiesDirectory as $item)
                                    <option value="{{$item->property_id}}">{{ $item->dir_name }}</option>
                                @endforeach
                            </select>
                        
                        </div>
                        <div class="form-group row">
						    <label class="col-md-2" > Freigegeben für:</label>
                            <div class="col-md-10">
                                <input type="checkbox" class="permission_select_all" name="permission_select_all" value="0"  >{{ trans('lfm.select-all') }}
                                        <br />
                                        <br />

                                @foreach($allUsers as $role => $users)
                                <div class="form-group row">
                                    <label class="col-md-2" > {{$roles[$role]}}:</label>
                                    <div class="col-md-10">
                                        <input type="checkbox" class="permission_role_select_all for_all_permission {{$roles[$role]}}" data-role="{{$roles[$role]}}" name="permission_role_select_all" value="0"  >{{ trans('lfm.select-all') }}
                                        <br />
                                        <br />
                                        @foreach($users as $user)
                                        <input type="checkbox" class="chk_user for_all_permission for_role_{{$roles[$role]}}_permission user{{$user->id}}"  id="module_permission_for_user_{{$user->id}}" data-role="{{$roles[$role]}}" name="user_permission" value="{{$user->id}}"  >{{$user->email}}
                                        <br />
                                        @endforeach

                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>
                        <div class="form-group">                            
                            <div class="col-lg-offset-8 col-lg-2 col-sm-offset-4 col-sm-4 col-xs-12">
                                <button type="submit" class="btn btn-block btn-primary btn-rounded">{{__('user.submit')}}</button>
                            </div>
                        </div>
                    {!! Form::close() !!}

                    
            </div>
                
        </div>
    </div>

    

@endsection

@section('scripts')
<script>
		var _token 			= '{{ csrf_token() }}';

$(document).ready(function () {
});
$(document).on('click', '.permission_select_all', function(){
    $('.for_all_permission').prop('checked', this.checked);
});
$(document).on('click', '.permission_role_select_all', function(){
    var role = $(this).data('role');
    $('.for_role_'+role+'_permission').prop('checked', this.checked);
});
$(document).on('click', '.chk_user', function(){
    if(!this.checked){
        var role = $(this).data('role');
        $('.permission_select_all').prop('checked', false);
        $('.permission_role_select_all.'+role).prop('checked', false);
    }
});
$(document).on('change', '.module-select-permission', function(){
    var permission = $(this).val();
    var msgElement = $('#flash_alerts');
    $.ajax({
        url : '{{ url("module_permission") }}/get_user_permission',
        type: "GET",
        data: { permission: permission},
        success: function(res) {            
            $('.permission_select_all').prop("checked", false);
            $('.permission_role_select_all').prop("checked", false);
            $('.chk_user').prop("checked", true);
            $.each(res.data, function (index, value) {
                $('#module_permission_for_user_'+value.user_id).prop("checked", false);
            });
        }
    });
});
$(document).on('submit', '#module_permission_form', function(){
    event.preventDefault();
    var permission = $("#permission").val();
    var msgElement = $('#flash_alerts');
    if(permission == ""){
        showFlash(msgElement, 'Please select permission first.', 'error');
        return false;
    }
    var roles = [];
    $('input[name="user_permission"]:checked').each(function() {
            roles.push($(this).val());
    });
    var $form = $( this ),
    url = $form.attr( 'action' );
    $('.preloader').show();
    $.ajax({
        url: url,
        type: "POST",
        data: { permission: permission,user_role:roles, _token: _token},
        success: function(res) {
            showFlash(msgElement, res.message, 'success');
            return false;
        }
    })
    .always(function(jqXHR, textStatus) {
        $('.preloader').hide();
    });
});
function showFlash(element, msg, type) {
    if (type == 'error') {
        $(element).html('<p class="alert alert-danger">' + msg + '</p>');
    } else {
        $(element).html('<p class="alert alert-success">' + msg + '</p>');
    }
    $([document.documentElement, document.body]).animate({
        scrollTop: ($(element).offset().top - 100)
    }, 500);
    setTimeout(function() {
        $(element).empty();
    }, 5000);
}
</script>
@endsection