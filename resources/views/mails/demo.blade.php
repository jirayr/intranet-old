<meta charset = "UTF-8" />
<div style="font-family: Arial, sans-serif">
{{--Sehr geehrte {{ $demo->receiver }},--}}
 
{!! $demo->message !!}



<!--
<strong>Bitte füllen Sie das unverbindliche Finanzierungsangebot aus oder antworten Sie auf diese E-Mail</strong>
<br/>
<?php 
	$data = $demo->formData ;
	$email_info = $data['email_info'];
	$contactDetail = $demo->contact_person_detail;
?>

<form action="{{$demo->form_submit_url}}" method="post" id="user_form" style="font-size: 16px;">
	
	{{csrf_field()}}
	<input type="hidden" name="email_id" value="{{$email_info->id}}"/>
	<input type="hidden" name="property_id" value="{{$data['property_id']}}"/>
	<input type="hidden" name="doc_key" value="{{$data['doc_key']}}"/>
	<input type="hidden" name="contact_email" value="{{ $contactDetail['email'] }}"/>
	
	<div id="form1" class="form_section">
		<ul style="padding: 0px">
			<li style="padding: 10px;font-size: 14px;text-decoration: none;background: #3452ff;color: #ffffff;">
				Ansprechpartner:&nbsp;{{$email_info->contact_person}}
			</li>
			<br>
			<li style="padding: 10px;font-size: 14px;text-decoration: none;background: #3452ff;color: #ffffff;">
				Objektdaten
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Objekt:&nbsp;{{$email_info->object}}
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Adresse:&nbsp;{{$email_info->address}}
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Baujahr:&nbsp;{{$email_info->construction_year}}
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Typ:&nbsp;{{$email_info->type}}
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Hauptmieter:&nbsp;{{$email_info->the_main_tenant}}
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Mietlaufzeit bis:&nbsp;{{$email_info->rental_period_until}}
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Parkplätze:&nbsp;{{$email_info->parking}}
			</li>
			<br>
			<li style="padding: 10px;font-size: 14px;text-decoration: none;background: #3452ff;color: #ffffff;">
				Finanzierung
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Finanzierungsart:&nbsp; {{$email_info->financing}}
				<div class="triangle-topleft"></div>
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Eigenkapitalanteil:{{$email_info->equity_share}}
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Fremdkapitalanteil:&nbsp; {{$email_info->leverage}}
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Variabler Zinssatz:&nbsp;{{$email_info->variable_interest_rate}}
			</li>
			<li style="margin-top: 10px;margin-left: 40px;">
				Tilgung:&nbsp; {{$email_info->repayment}}
			</li>
		</ul>
	</div>
		
	<div id="form4" class="form_section">
		<br/><br/>
		<?php $questions = $data['questions']; ?>
		@foreach($questions as $key=>$question)
			<ul class="list-group form_{{$key}} {{($key==0) ?  : 'hidden_only'}}" style="padding: 0px">
				<li style="padding: 10px;font-size: 14px;text-decoration: none;background: #3452ff;color: #ffffff;">
					{{$question->qn_heading}}
				</li>
				<br>
				@php
					$total_question_forms=$key;
					$Questions=json_decode($question->questions,true);
				@endphp
				@foreach($Questions['question'] as $key1=>$Question)
					@if(is_array($Question))
						<li style="margin-top: 10px;margin-left: 40px;">
							{{__($Question['qn'])}}:&nbsp;
							@if($Questions['required'][$key1]==1)
								<span class="required">*</span>
							@endif
							<br/>
							@foreach($Question['op'] as $op)
								<input type="radio" name="user_form_fields[{{$question->qn_heading}}][{{$Question['qn']}}]" value="{{__($op)}}"/>&nbsp;&nbsp;{{__($op)}}
								<br/>
							@endforeach
						</li>
					@else
						<li style="margin-top: 10px;margin-left: 40px;">
							@if(isset($Question))
								{{__($Question)}}:&nbsp;
								@if($Questions['required'][$key1]==1)
									<span class="required">*</span>
								@endif
								<br/>
							@endif

							@if($Questions['required'][$key1]==1)
								<input type="text" name="user_form_fields[{{$question->qn_heading}}][{{$Question}}]" value="" placeholder="Ihre Angabe" style="width: 300px; height: 30px;"/>
							@else
								<input type="text" name="user_form_fields[{{$question->qn_heading}}][{{$Question}}]" value="" placeholder="Ihre Angabe" style="width: 300px; height: 30px;"/>
							@endif
						</li>
					@endif
				@endforeach
			</ul>
		@endforeach
	</div>
	<button type="submit" style="padding: 10px;margin-left: 10px auto;font-size: 14px;text-decoration: none;background: #3452ff;color: #ffffff;">
	Einreichen
	</button>	
</form> 

<br/>
<br/>
<br/>
<br/>

-->

Mit freundlichen Grüßen
<br/><br/>
<p>{{ $contactDetail['name'] }}</p>

{{ $contactDetail['role'] }}
<br/>


<br/>
	<br>
	<img alt="" height="61"
		 src="https://ci4.googleusercontent.com/proxy/1QAk8EMdQISD2PBeW_OUnnMJze5Hc8x3WwAD-2FLZoEKkL4a8xCnAtbFk43wGhdT_haO7RpN34MhWkzXChLq3wQ6xlkoIxA8ktxdPraLCd0r-3E-un8=s0-d-e1-ft#http://fcr-immobilien.de/wp-content/uploads/2018/05/logo-300x61.png"
		 width="300" class="CToWUd">
	<p>
		Office München
		<br>
		Paul-Heyse-Straße 28
		<br>
		D-80336 München
		<br>
		www.fcr-immobilien.de
	</p>
	<br/>
<p>
Telefon:	{{ $contactDetail['phone'] }} <br>
Mobil:	{{ $contactDetail['mobile'] }} <br>
Fax:  +49 89 413 2496 99 <br>
E-Mail:	{{ $contactDetail['email'] }}
</p>
<br/>
	<br/>
<p>
FCR Immobilien AG, Paul-Heyse-Straße 28, D-80336 München
<br>
Vorstand: Falk Raudies, Aufsichtsratsvorsitzender: Prof. Dr. Franz-Joseph Busse, HRB 210430 | Amtsgericht München
<br/>
<p>
Disclaimer: <br>
		This email is confidential. If you are not the intended recipient, you must not disclose or use the information contained in it. If you have received this mail in error, please tell us immediately by return email and delete the document. <br>
		Diese E-Mail ist vertraulich und ausschließlich für den angegebenen Empfänger gedacht. Sollten Sie diese E-Mail fälschlicherweise erhalten haben, so benachrichtigen Sie uns bitte und vernichten den Inhalt dieser Nachricht.
</div>

