<div style="font-family: Arial, sans-serif">
Sehr geehrtre,
 
<strong>Neues Finanzierungsangebot von *BANKNAME für das Objekt *PROPERTYNAME (*PROPERTYLINK)</strong>
<br/>
<?php 
	$email_info = $data['email_info'];
	$formData = $data['user_form_data'];
?>
	<div id="form1" class="form_section">
		<ul>
			<li style="padding: 10px;margin: 0px auto;font-size: 14px;text-decoration: none;background: #3452ff;color: #ffffff;">
				Ansprechpartner:&nbsp;{{$email_info->contact_person}}
			</li>
			<br>
			<li style="padding: 10px;margin: 0px auto;font-size: 14px;text-decoration: none;background: #3452ff;color: #ffffff;">
				Objektdaten
			</li>
			<li style="margin-top: 10px;">
				Objekt:&nbsp;{{$email_info->object}}
			</li>
			<li style="margin-top: 10px;">
				Adresse:&nbsp;{{$email_info->address}}
			</li>
			<li style="margin-top: 10px;">
				Baujahr:&nbsp;{{$email_info->construction_year}}
			</li>
			<li style="margin-top: 10px;">
				Typ:&nbsp;{{$email_info->type}}
			</li>
			<li style="margin-top: 10px;">
				Hauptmieter:&nbsp;{{$email_info->the_main_tenant}}
			</li>
			<li style="margin-top: 10px;">
				Mietlaufzeit bis:&nbsp;{{$email_info->rental_period_until}}
			</li>
			<li style="margin-top: 10px;">
				Parkplätze:&nbsp;{{$email_info->parking}}
			</li>
			<br>
			<li style="padding: 10px;margin: 0px auto;font-size: 14px;text-decoration: none;background: #3452ff;color: #ffffff;">
				Finanzierung
			</li>
			<li style="margin-top: 10px;">
				Finanzierungsart:&nbsp; {{$email_info->financing}}
				<div class="triangle-topleft"></div>
			</li>
			<li style="margin-top: 10px;">
				Eigenkapitalanteil:{{$email_info->equity_share}}
			</li>
			<li style="margin-top: 10px;">
				Fremdkapitalanteil:&nbsp; {{$email_info->leverage}}
			</li>
			<li style="margin-top: 10px;">
				Variabler Zinssatz:&nbsp;{{$email_info->variable_interest_rate}}
			</li>
			<li style="margin-top: 10px;">
				Tilgung:&nbsp; {{$email_info->repayment}}
			</li>
		</ul>
	</div>
		
	<div id="form4" class="form_section">
		<br/><br/>
		@foreach($formData as $key=>$fData)
			<ul class="list-group form_{{$key}} {{($key==0) ?  : 'hidden_only'}}">
				<li style="padding: 10px;margin: 0px auto;font-size: 14px;text-decoration: none;background: #3452ff;color: #ffffff;">
					{{$key}}
				</li>
				<br>
				@foreach($fData as $key1=>$dt)
					<li style="margin-top:10px;">
						{{ $key1 }}:&nbsp;{{ $dt }}						
					</li>
				@endforeach
			</ul>
		@endforeach
	</div>	


Mit freundlichen Grüßen,
<br/>
<p>
FCR Immobilien AG, Bavariaring 24, D-80336 München
<br>
Vorstand: Falk Raudies, Aufsichtsratsvorsitzender: Prof. Dr. Franz-Joseph Busse, HRB 210430 | Amtsgericht München
<br/>
<p>
Disclaimer:
This email is confidential. If you are not the intended recipient, you must not disclose or use the information contained in it. If you have received this mail in error, please tell us immediately by return email and delete the document.
Diese E-Mail ist vertraulich und ausschließlich für den angegebenen Empfänger gedacht. Sollten Sie diese E-Mail fälschlicherweise erhalten haben, so benachrichtigen Sie uns bitte und vernichten den Inhalt dieser Nachricht.
</p>
</div>

