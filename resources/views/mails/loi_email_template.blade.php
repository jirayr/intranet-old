<div style="font-family: Arial, sans-serif">


<p>{!!$data['custom_message']!!}</p>


Mit freundlichen Grüßen <br/>
<br/>
{{ $data['sender_name']  }}
<br/>
{{ $data['sender_title']  }}
<br/>
<br>

<img alt="" height="61"
     src="https://ci4.googleusercontent.com/proxy/1QAk8EMdQISD2PBeW_OUnnMJze5Hc8x3WwAD-2FLZoEKkL4a8xCnAtbFk43wGhdT_haO7RpN34MhWkzXChLq3wQ6xlkoIxA8ktxdPraLCd0r-3E-un8=s0-d-e1-ft#http://fcr-immobilien.de/wp-content/uploads/2018/05/logo-300x61.png"
     width="300" class="CToWUd">


<p>
    Office München
    <br>
    Paul-Heyse-Straße 28
    <br>
    D-80336 München
    <br>
    www.fcr-immobilien.de
</p>
<br/>
<p>
    Telefon:   {{$data['number']}} <br>
    Mobil:  {{$data['sender_mobile']}} <br>
    Fax:  +49 89 413 2496 99 <br>
    E-Mail:  {{$data['sender_email']}}
</p>
<br>


    <!--
<p style="margin-top:0;margin-bottom:0;font-family:'Arial';font-size:10pt"><a href="https://fcr-immobilien.de/anleihe/"
                                                                              target="_blank"
                                                                              data-saferedirecturl="https://www.google.com/url?q=https://fcr-immobilien.de/anleihe/&amp;source=gmail&amp;ust=1566812585248000&amp;usg=AFQjCNHNpfeDv4U5ZLqShL9RSXlhQNgK2g"><img
                alt="" height="90"
                src="https://ci3.googleusercontent.com/proxy/TdTiQ1-jKcIxltEFIZ_roDqaxMwVhsPQIYd5zJIKadNU6uDLgSwJ5ewSvfmNH7WzJFEjoN__-hv-rVmYv9xJm7FwQp7-gn8lnFo5Kmn6JpURrzyapWYm0d-mcqnAA7FwzlkOiZJU4H4fUNfzGgsTK-PyYxstZ5bWPoyPqgTVH7fFWO7Lvou9=s0-d-e1-ft#https://fcr-immobilien.de/wp-content/uploads/2019/04/Banner-90x728-E-Mail-Signatur_525-Anleihe-FCR-Immobilien-AG.png"
                width="728" class="CToWUd"></a></p>
<br/>
-->
<p>
    FCR Immobilien AG, Bavariaring 24, D-80336 München
    <br>
    Vorstand: Falk Raudies, Aufsichtsratsvorsitzender: Prof. Dr. Franz-Joseph Busse, HRB 210430 | Amtsgericht München
    <br/>
<p>
    Disclaimer: <br>
    This email is confidential. If you are not the intended recipient, you must not disclose or use the information contained in it. If you have received this mail in error, please tell us immediately by return email and delete the document. <br>
    Diese E-Mail ist vertraulich und ausschließlich für den angegebenen Empfänger gedacht. Sollten Sie diese E-Mail fälschlicherweise erhalten haben, so benachrichtigen Sie uns bitte und vernichten den Inhalt dieser Nachricht.
</p>

</div>