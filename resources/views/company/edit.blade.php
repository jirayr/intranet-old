@extends('layouts.admin')
@section('content')
    <form method="POST" action="{{url('/company/update/'.$company->id)}}" enctype="multipart/form-data">
         <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="email">Firmenname:</label>
                <input required type="text" class="form-control" name="name" maxlength="105" value="{{$company->name}}" placeholder="Enter Keyword" id="email">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="zip">PLZ:</label>
                <input required type="text" class="form-control" name="zip" value="{{$company->companyAddress->zip}}"  placeholder="Enter Zip">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="street">Strasse:</label>
                <input required type="text" class="form-control" name="street"  value="{{$company->companyAddress->street}}" placeholder="Enter Street">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="city">Stadt:</label>
                <input required type="text" class="form-control" name="city" value="{{$company->companyAddress->city}}"  placeholder="Enter City">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="part of city">Stadtteil:</label>
                <input required type="text" class="form-control" name="part_of_city"  value="{{$company->companyAddress->part_of_city}}" placeholder="Enter Part Of City">
            </div>
        </div>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <button type="submit" class="btn btn-primary">Speichern</button>
    </form>
@stop