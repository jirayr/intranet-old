@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="card-header">
        <div class="row">
            <div class="col-md-10">
                @if($emailType == 1)
                <h4 class="card-title">INBOX</h4>
                @else
                <h4 class="card-title">SENT</h4>
                @endif
            </div>
            <div class="col-md-2">
{{--                @if(isset($company))--}}
                    <a type="button" style="" href="{{url('company/'.Request::segment(2).'/email/type/'.'1')}}"  class="btn-sm btn-primary">INBOX</a>
                {{--@else--}}
                    <a type="button" href="{{url('company/'.Request::segment(2).'/email/type/'.'2')}}" class="btn-sm btn-primary">SENT</a>
                {{--@endif--}}

            </div>
        </div>
    </div>
    <div class="col-sm-12 table-responsive white-box">
        <table class="table table-striped" >
            <thead>
            <tr>
                <th>Employee Email</th>
                <th>subject</th>
                <th>email Sent From</th>
                <th>bcc</th>
                <th>cc</th>
                <th>Date</th>
                <th>Email Body</th>
            </tr>
            </thead>
            <tbody>
            @isset($data)
            @foreach($data as $email)
                @if(isset($email->emailMapping->email_type_id) == $emailType)
                    <tr>
                        <td>{{$email->emailMapping->emailAccount->email}}</td>
                        <td>{{$email->subject}}</td>
                        <td>{{$email->email_sent_from}}</td>
                        <td>{{$email->bcc}}</td>
                        <td>{{$email->cc}}</td>
                        <td>{{$email->date_of_email}}</td>
                        <td>{{$email->body}}</td>
                    </tr>
                @endif
            @endforeach
            @endisset
            </tbody>
        </table>
    </div>
    {{ $data->links() }}

</div>

@endsection
