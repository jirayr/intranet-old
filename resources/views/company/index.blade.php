@extends('layouts.admin')
@section('content')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>



    <div class="row">
    <div class="card-header">
        <div class="row">
            <div class="col-md-10">
                <h4 class="card-title"><a href="{{url('/company')}}">Adressen</a></h4>

            </div>
            <div class="col-md-2">
                <a href="{{route('company.add')}}" class="btn btn-primary">Neu</a>
            </div>
        </div>
    </div>

    <form action="{{url('/company/search')}}" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="row">

            <div class="col-md-4">
                  <select required class="form-control  search" name="key[]"  style=""></select>
            </div>
            <div class="col-md-2">
                <button style="" type="submit"  class="btn btn-success ">Suche</button>
            </div>

        </div>
    </form>
    <br>

    <div class="col-sm-12 table-responsive white-box">
        <table style="table-layout:fixed" class="table table-striped" id="inquiries">
            <thead>
            <tr>
                <th>Firma</th>
                <th>PLZ</th>
                <th>Strasse</th>
                <th>Stadt</th>
                <th>Stadtteil</th>
                <th>action</th>
            </tr>
            </thead>
            <tbody>
            @isset($companies)
            @foreach($companies as $company)
                <tr>
                    <td>{{$company->name}}</td>
                    @if(isset($company->companyAddress))
                    <td>{{$company->companyAddress->zip}}</td>
                    <td>{{$company->companyAddress->street}}</td>
                    <td>{{$company->companyAddress->city}}</td>
                    <td>{{$company->companyAddress->part_of_city}}</td>
                    @else
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    @endif


                    <td class="">
                        <a type="button" href="{{url('company/'.$company->id.'/email/type/'.'1')}}" class="btn-sm btn-primary"> View Emails</a>
                        <a type="button" href="{{url('company/employee/'.$company->id)}}" class="btn-sm btn-primary"> Anzeigen</a>
                        <a type="button" href="{{url('company/edit',$company->id)}}" class="btn-sm btn-info">Bearbeiten</a>
                        <a type="button" href="{{url('company/delete',$company->id)}}" class="btn-sm btn-danger">Löschen</a>
                    </td>
                </tr>
            @endforeach
            @endisset
            </tbody>
        </table>
    </div>
    {{ $companies->links() }}

</div>

@endsection
@section('scripts')
    <script>

        $(document).ready(function() {

        $(".search").select2({

            minimumInputLength: 2,

            language: {

                inputTooShort: function () {
                    return "<?php echo 'Please enter 2 or more characters'; ?>"
                }
            },
            multiple: true,
            tags: true,
            ajax: {

                url: '{{route("autocomplete")}}',
                dataType: 'json',
                data: function (term) {
                    return {
                        query: term
                    };
                },
                processResults: function (data) {
                     return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.keyword,
                                id: item.id,
                                slug: item.keyword

                            }

                        })

                    };
                }

            }

        });
        });

    </script>


@stop
