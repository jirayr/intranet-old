@extends('layouts.admin')
@section('content')
    <form  method="post" action="{{route('company.store')}}">
       <div class="row">
           <div class="form-group col-md-6" style="width: 50%">
               <label for="email">Firmenname:</label>
               <input required type="text" class="form-control" name="name" maxlength="105"  placeholder="Enter Keyword" id="email">
           </div>
           <div class="form-group col-md-6" style="width: 50%">
               <label for="zip">PLZ:</label>
               <input required type="text" class="form-control" name="zip"   placeholder="Enter Zip">
           </div>
       </div>
       <div class="row">
            <div class="form-group col-md-6" style="width: 50%">
                <label for="street">Strasse:</label>
                <input required type="text" class="form-control" name="street"   placeholder="Enter Street">
            </div>
            <div class="form-group col-md-6" style="width: 50%">
                <label for="city">Stadt:</label>
                <input required type="text" class="form-control" name="city"   placeholder="Enter City">
            </div>
       </div>
       <div class="row">
           <div class="form-group col-md-6" style="width: 50%">
               <label for="part of city">Stadtteil:</label>
               <input required type="text" class="form-control" name="part_of_city"   placeholder="Enter Part Of City">
           </div>
       </div>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <button type="submit" class="btn btn-primary">Speichern</button>
    </form>
@stop