ALTER TABLE  `properties` ADD  `properties_bank_id` INT NULL AFTER  `main_property_id` ;

UPDATE `properties` SET properties_bank_id=Ist where Ist>0;
UPDATE `properties` SET properties_bank_id=soll where soll>0;


ALTER TABLE `properties` ADD `sheet_title` VARCHAR(100) NULL AFTER `datum_lol`;
ALTER TABLE `sheetproperties` ADD `sheet_title` VARCHAR(100) NULL AFTER `datum_lol`;


ALTER TABLE `tenancy_schedule_items`  ADD `warning_date1` DATE NULL  AFTER `indexierung`,  ADD `warning_date2` DATE NULL  AFTER `warning_date1`,  ADD `warning_date3` DATE NULL  AFTER `warning_date2`,  ADD `warning_price1` DOUBLE NULL  AFTER `warning_date3`,  ADD `warning_price2` DOUBLE NULL  AFTER `warning_price1`,  ADD `warning_price3` DOUBLE NULL  AFTER `warning_price2`;


ALTER TABLE `property_comments` ADD `pdf_file` VARCHAR(255) NULL AFTER `Miete_note`;

ALTER TABLE  `properties_buy_details` ADD  `files` TEXT NULL AFTER  `comment` ;


ALTER TABLE `properties` ADD `portfolio` VARCHAR(255) NULL AFTER `sheet_title`;


ALTER TABLE `tenancy_schedule_items` ADD `assesment_date` DATE NULL AFTER `status`;


ALTER TABLE `export_ads` ADD `image_title` TEXT NULL AFTER `images`;


ALTER TABLE `export_ads` ADD `state` VARCHAR(255) NULL AFTER `updated_at`, ADD `address2` VARCHAR(255) NULL AFTER `state`, ADD `telephone` VARCHAR(20) NULL AFTER `address2`, ADD `ach_tenants` VARCHAR(255) NULL AFTER `telephone`;


ALTER TABLE `users` ADD `mobile` VARCHAR(20) NULL AFTER `phone`;


ALTER TABLE `tenancy_schedule_items` ADD `kaution` VARCHAR(255) NULL AFTER `assesment_date`;

ALTER TABLE `tenancy_schedule_items` ADD `vacancy_on_purcahse` TINYINT NOT NULL DEFAULT '0' AFTER `kaution`;

ALTER TABLE `properties_tenants` ADD `type` INT NULL AFTER `mietvertrag_text`;

ALTER TABLE `users` ADD `signature` VARCHAR(255) NULL AFTER `birthday`;

ALTER TABLE `properties_tenants` ADD `mv_end2` VARCHAR(255) NULL AFTER `mv_end`;


ALTER TABLE `properties_tenants` ADD `is_dynamic_date` TINYINT NOT NULL DEFAULT '0' COMMENT '0=Changed Everyday 1=Fixed' AFTER `type`;

ALTER TABLE `properties_tenants` ADD `is_current_net` TINYINT NOT NULL DEFAULT '1' AFTER `net_rent_p_a`;


ALTER TABLE `verkauf_tab` ADD `string_text` VARCHAR(30) NOT NULL AFTER `monat_check`;

ALTER TABLE `vacant_spaces` ADD `favourite` TINYINT NOT NULL DEFAULT '0' AFTER `image`;



ALTER TABLE `properties` ADD `property_type` VARCHAR(30) NULL AFTER `steuerberater`;

ALTER TABLE  `ads` CHANGE  `type`  `type` ENUM(  '',  'apartment',  'condos',  'house',  'land',  'commercial_space',  'villa',  'shopping_mall',  'specialists',  'hotel',  'retail_shop',  'office',  'store',  'logistik' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL



ALTER TABLE `properties` ADD `quick_status` TINYINT NOT NULL DEFAULT '0' AFTER `property_type`;
ALTER TABLE `sheetproperties` ADD `quick_status` TINYINT NOT NULL DEFAULT '0' AFTER `property_type`;


ALTER TABLE `properties` ADD `notizen` TEXT NULL AFTER `quick_status`;


ALTER TABLE `properties_tenants` ADD `flache` DOUBLE NULL AFTER `is_dynamic_date`;



ALTER TABLE  `ads` CHANGE  `type`  `type` VARCHAR( 40 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE  `vads` CHANGE  `type`  `type` VARCHAR( 40 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE  `export_ads` CHANGE  `type`  `type` VARCHAR( 40 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;


ALTER TABLE `ads` ADD `stellplatze` DECIMAL(12,2) NOT NULL DEFAULT '0' AFTER `type`;
ALTER TABLE `vads` ADD `stellplatze` DECIMAL(12,2) NOT NULL DEFAULT '0' AFTER `type`;
ALTER TABLE `export_ads` ADD `stellplatze` DECIMAL(12,2) NOT NULL DEFAULT '0' AFTER `type`;


ALTER TABLE `tenancy_schedule_items` ADD `common_item_id` INT NULL AFTER `vacancy_on_purcahse`;





SELECT property_id,ort,plz_ort,json_extract(`text_json`, '$.wault') as wault FROM `tenancy_schedules` join properties on properties.id=property_id where status=6 and Ist=0 and soll=0 order by properties.id


SELECT * FROM properties where status=6 and Ist=0 and soll=0

ALTER TABLE `properties` ADD `property_status` TINYINT(4) NOT NULL DEFAULT '0';


ALTER TABLE `properties` CHANGE `quick_status` `lock_status` TINYINT(4) NOT NULL DEFAULT '0';
UPDATE `properties` SET lock_status=0;


ALTER TABLE `budgets` CHANGE `object_management` `object_management_nk` DOUBLE NULL DEFAULT NULL;


ALTER TABLE `budgets` CHANGE `object_management_increase_year1` `property_management_increase_year1` DOUBLE NULL DEFAULT NULL, CHANGE `object_management_increase_year2` `property_management_increase_year2` DOUBLE NULL DEFAULT NULL, CHANGE `object_management_increase_year3` `property_management_increase_year3` DOUBLE NULL DEFAULT NULL, CHANGE `object_management_increase_year4` `property_management_increase_year4` DOUBLE NULL DEFAULT NULL, CHANGE `object_management_increase_year5` `property_management_increase_year5` DOUBLE NULL DEFAULT NULL;


ALTER TABLE `empfehlung_details` ADD `user_id` INT NULL AFTER `id`;


update `properties_default_payers` set no_opos_status=1 WHERE `month` LIKE '4' and invoice is null;



ALTER TABLE `property_invoices` ADD `am_status` TINYINT NOT NULL DEFAULT '0' AFTER `not_release_status`;
ALTER TABLE `property_invoices` ADD `hv_status` TINYINT NOT NULL DEFAULT '0' AFTER `am_status`;

ALTER TABLE `property_invoices` ADD `user_status` TINYINT NOT NULL DEFAULT '0' AFTER `hv_status`;

ALTER TABLE `property_invoices` ADD `falk_status` TINYINT NOT NULL DEFAULT '0' AFTER `user_status`;


ALTER TABLE `property_invoices`  ADD `last_process_type` VARCHAR(45) NULL AFTER `falk_status`;
