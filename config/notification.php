<?php

return [
    'type' => [
        'create_property'           => 1,
        'edit_property'             => 2,
        'change_property_status'    => 3,
    ],

];
