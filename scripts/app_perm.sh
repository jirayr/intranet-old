#!/bin/bash

# Enter app directory
cd /var/www/Intranet

rsync -a /var/www/Intranet/ /var/www/html/Intranet/
#rm -rf /var/www/html/public/*
#cp -R /var/www/public_copy/* /var/www/html/public/


chmod 777 -R /var/www/html/Intranet/storage
chmod 777 -R /var/www/html/Intranet/public
chmod 777 -R /var/www/html/Intranet/bootstrap/cache
